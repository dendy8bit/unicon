﻿using System.Xml.Serialization;
using BEMN.Forms.ValidatingClasses.New;

namespace ITKZ.Structures
{
    public class DevNumStruct : StructBase
    {
        [Layout(0)] private ushort _devNum;

        [XmlElement(ElementName = "Номер_устройства")]
        [BindingProperty(0)]
        public ushort DvNum
        {
            get { return _devNum; }
            set { _devNum = value; }
        }
    }
}
