﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.ItkzV8.Structures
{
    public class UstavkiStruct : StructBase
    {
        [Layout(0)] private ushort _iaLimit;
        [Layout(1)] private ushort _ibLimit;
        [Layout(2)] private ushort _icLimit;
        [Layout(3)] private ushort _iNLimit;
        [Layout(4)] private ushort _iN5Limit;
        [Layout(5)] private ushort _itaLimit;
        [Layout(6)] private ushort _itbLimit;
        [Layout(7)] private ushort _itcLimit;
        [Layout(8)] private ushort _itNLimit;
        [Layout(9)] private ushort _itN5Limit;

        [XmlElement(ElementName = "Уставка_фазы_А")]
        [BindingProperty(0)]
        public double IaLimit
        {
            get { return (double)this._iaLimit/100; }
            set { this._iaLimit = (ushort)Math.Round(value*100); }
        }

        [XmlElement(ElementName = "Уставка_фазы_B")]
        [BindingProperty(1)]
        public double IbLimit
        {
            get { return (double)this._ibLimit/100; }
            set { this._ibLimit = (ushort)Math.Round(value * 100); }
        }

        [XmlElement(ElementName = "Уставка_фазы_C")]
        [BindingProperty(2)]
        public double IcLimit
        {
            get { return (double)this._icLimit/100; }
            set { this._icLimit = (ushort)Math.Round(value * 100); }
        }

        [XmlElement(ElementName = "Уставка_фазы_N")]
        [BindingProperty(3)]
        public double InLimit
        {
            get { return (double) this._iNLimit/100; }
            set { this._iNLimit = (ushort) Math.Round(value*100); }
        }

        [XmlElement(ElementName = "Уставка_фазы_N5")]
        [BindingProperty(4)]
        public double In5Limit
        {
            get { return (double) this._iN5Limit/100; }
            set { this._iN5Limit = (ushort) Math.Round(value*100); }
        }

        [XmlElement(ElementName = "Уставка_времени_фазы_A")]
        [BindingProperty(5)]
        public ushort ItaLimit
        {
            get { return this._itaLimit; }
            set { this._itaLimit = value; }
        }

        [XmlElement(ElementName = "Уставка_времени_фазы_B")]
        [BindingProperty(6)]
        public ushort ItbLimit
        {
            get { return this._itbLimit; }
            set { this._itbLimit = value; }
        }

        [XmlElement(ElementName = "Уставка_времени_фазы_C")]
        [BindingProperty(7)]
        public ushort ItcLimit
        {
            get { return this._itcLimit; }
            set { this._itcLimit = value; }
        }

        [XmlElement(ElementName = "Уставка_времени_фазы_N")]
        [BindingProperty(8)]
        public ushort ItNLimit
        {
            get { return this._itNLimit; }
            set { this._itNLimit = value; }
        }

        [XmlElement(ElementName = "Уставка_времени_фазы_N5")]
        [BindingProperty(9)]
        public ushort ItN5Limit
        {
            get { return this._itN5Limit; }
            set { this._itN5Limit = value; }
        }
    }
}
