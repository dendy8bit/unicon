﻿using System;
using System.Collections;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.ItkzV8.Structures
{
    public class ItkzMeasuringStruct : StructBase
    {
        [Layout(0)] private ushort _status;
        [Layout(1)] private ushort _iaValue;
        [Layout(2)] private ushort _ibValue;
        [Layout(3)] private ushort _icValue;
        [Layout(4)] private ushort _iNValue;
        [Layout(5)] private ushort _iN5Value;
        [Layout(6)] private ushort _iaLastexcessValue;
        [Layout(7)] private ushort _ibLastexcessValue;
        [Layout(8)] private ushort _icLastexcessValue;
        [Layout(9)] private ushort _iNLastexcessValue;
        [Layout(10)] private ushort _iN5LastexcessValue;
      
        public BitArray Status
        {
            get { return new BitArray(BitConverter.GetBytes(this._status)); }
        }

        public ushort IaValue
        {
            get { return this._iaValue; }
        }

        public ushort IbValue
        {
            get { return this._ibValue; }
        }

        public ushort IcValue
        {
            get { return this._icValue; }
        }

        public ushort InValue
        {
            get { return this._iNValue; }
        }

        public ushort In5Value
        {
            get { return this._iN5Value; }
        }

        public ushort IaLastexcessValue
        {
            get { return this._iaLastexcessValue; }
        }

        public ushort IbLastexcessValue
        {
            get { return this._ibLastexcessValue; }
        }

        public ushort IcLastexcessValue
        {
            get { return this._icLastexcessValue; }
        }

        public ushort InLastexcessValue
        {
            get { return this._iNLastexcessValue; }
        }

        public ushort In5LastexcessValue
        {
            get { return this._iN5LastexcessValue; }
        }
    }
}
