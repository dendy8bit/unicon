﻿namespace BEMN.ItkzV8
{
    partial class ItkzMeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this._lastFazaN5 = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this._lastFazaN = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this._lastFazaC = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this._lastFazaB = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this._lastFazaA = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this._curFazaN5 = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this._curFazaN = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this._curFazaC = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this._curFazaB = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this._statusLed3 = new BEMN.Forms.LedControl();
            this._statusLed2 = new BEMN.Forms.LedControl();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this._statusLedN = new BEMN.Forms.LedControl();
            this._statusLedN5 = new BEMN.Forms.LedControl();
            this._stupen1 = new BEMN.Forms.LedControl();
            this._stupen2 = new BEMN.Forms.LedControl();
            this._stupen3 = new BEMN.Forms.LedControl();
            this._stupenN = new BEMN.Forms.LedControl();
            this._stupenN5 = new BEMN.Forms.LedControl();
            this.label10 = new System.Windows.Forms.Label();
            this._statusLed1 = new BEMN.Forms.LedControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this._curFazaA = new System.Windows.Forms.TextBox();
            this._relayLed = new BEMN.Forms.LedControl();
            this._btnModINInput = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this._btnModINOutput = new System.Windows.Forms.Button();
            this._modN = new BEMN.Forms.LedControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._modN5 = new BEMN.Forms.LedControl();
            this._btnModIN5Output = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this._btnModIN5Input = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._kvitBtn = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "(срабатывание ступени)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Реле срабатывания";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 63);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ia";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Фазы";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(95, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 39);
            this.label6.TabIndex = 0;
            this.label6.Text = "Состояние измерительного органа";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.68047F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.85207F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.47337F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.98553F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 213F));
            this.tableLayoutPanel1.Controls.Add(this.panel10, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel9, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel8, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel7, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label16, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this._statusLed3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this._statusLed2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label7, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this._statusLedN, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this._statusLedN5, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this._stupen1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this._stupen2, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this._stupen3, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this._stupenN, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this._stupenN5, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.label10, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this._statusLed1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 3, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.09091F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.46586F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.46586F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.46586F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.67068F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.66265F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(722, 251);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.label24);
            this.panel10.Controls.Add(this._lastFazaN5);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(508, 214);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(209, 32);
            this.panel10.TabIndex = 38;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(127, 8);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(56, 13);
            this.label24.TabIndex = 3;
            this.label24.Text = "0.001Imax";
            // 
            // _lastFazaN5
            // 
            this._lastFazaN5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._lastFazaN5.Location = new System.Drawing.Point(69, 5);
            this._lastFazaN5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._lastFazaN5.Name = "_lastFazaN5";
            this._lastFazaN5.ReadOnly = true;
            this._lastFazaN5.Size = new System.Drawing.Size(55, 20);
            this._lastFazaN5.TabIndex = 2;
            this._lastFazaN5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label23);
            this.panel9.Controls.Add(this._lastFazaN);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(508, 171);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(209, 35);
            this.panel9.TabIndex = 37;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(127, 8);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(56, 13);
            this.label23.TabIndex = 3;
            this.label23.Text = "0.001Imax";
            // 
            // _lastFazaN
            // 
            this._lastFazaN.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._lastFazaN.Location = new System.Drawing.Point(69, 6);
            this._lastFazaN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._lastFazaN.Name = "_lastFazaN";
            this._lastFazaN.ReadOnly = true;
            this._lastFazaN.Size = new System.Drawing.Size(55, 20);
            this._lastFazaN.TabIndex = 2;
            this._lastFazaN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label22);
            this.panel8.Controls.Add(this._lastFazaC);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(508, 131);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(209, 32);
            this.panel8.TabIndex = 36;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(127, 8);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(14, 13);
            this.label22.TabIndex = 3;
            this.label22.Text = "А";
            // 
            // _lastFazaC
            // 
            this._lastFazaC.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._lastFazaC.Location = new System.Drawing.Point(69, 5);
            this._lastFazaC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._lastFazaC.Name = "_lastFazaC";
            this._lastFazaC.ReadOnly = true;
            this._lastFazaC.Size = new System.Drawing.Size(55, 20);
            this._lastFazaC.TabIndex = 2;
            this._lastFazaC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label21);
            this.panel7.Controls.Add(this._lastFazaB);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(508, 91);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(209, 32);
            this.panel7.TabIndex = 35;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(127, 8);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(14, 13);
            this.label21.TabIndex = 3;
            this.label21.Text = "А";
            // 
            // _lastFazaB
            // 
            this._lastFazaB.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._lastFazaB.Location = new System.Drawing.Point(69, 5);
            this._lastFazaB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._lastFazaB.Name = "_lastFazaB";
            this._lastFazaB.ReadOnly = true;
            this._lastFazaB.Size = new System.Drawing.Size(55, 20);
            this._lastFazaB.TabIndex = 2;
            this._lastFazaB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this._lastFazaA);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(508, 51);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(209, 32);
            this.panel6.TabIndex = 34;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(127, 8);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(14, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "А";
            // 
            // _lastFazaA
            // 
            this._lastFazaA.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._lastFazaA.Location = new System.Drawing.Point(69, 5);
            this._lastFazaA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._lastFazaA.Name = "_lastFazaA";
            this._lastFazaA.ReadOnly = true;
            this._lastFazaA.Size = new System.Drawing.Size(55, 20);
            this._lastFazaA.TabIndex = 2;
            this._lastFazaA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this._curFazaN5);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(362, 214);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(138, 32);
            this.panel5.TabIndex = 33;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(76, 8);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 13);
            this.label19.TabIndex = 3;
            this.label19.Text = "0.001Imax";
            // 
            // _curFazaN5
            // 
            this._curFazaN5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._curFazaN5.Location = new System.Drawing.Point(18, 5);
            this._curFazaN5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curFazaN5.Name = "_curFazaN5";
            this._curFazaN5.ReadOnly = true;
            this._curFazaN5.Size = new System.Drawing.Size(55, 20);
            this._curFazaN5.TabIndex = 2;
            this._curFazaN5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this._curFazaN);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(362, 171);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(138, 35);
            this.panel4.TabIndex = 32;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(76, 8);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "0.001Imax";
            // 
            // _curFazaN
            // 
            this._curFazaN.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._curFazaN.Location = new System.Drawing.Point(18, 6);
            this._curFazaN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curFazaN.Name = "_curFazaN";
            this._curFazaN.ReadOnly = true;
            this._curFazaN.Size = new System.Drawing.Size(55, 20);
            this._curFazaN.TabIndex = 2;
            this._curFazaN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this._curFazaC);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(362, 131);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(138, 32);
            this.panel3.TabIndex = 31;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(76, 8);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(14, 13);
            this.label17.TabIndex = 3;
            this.label17.Text = "А";
            // 
            // _curFazaC
            // 
            this._curFazaC.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._curFazaC.Location = new System.Drawing.Point(18, 5);
            this._curFazaC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curFazaC.Name = "_curFazaC";
            this._curFazaC.ReadOnly = true;
            this._curFazaC.Size = new System.Drawing.Size(55, 20);
            this._curFazaC.TabIndex = 2;
            this._curFazaC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this._curFazaB);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(362, 91);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(138, 32);
            this.panel2.TabIndex = 30;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(76, 8);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "А";
            // 
            // _curFazaB
            // 
            this._curFazaB.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._curFazaB.Location = new System.Drawing.Point(18, 5);
            this._curFazaB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curFazaB.Name = "_curFazaB";
            this._curFazaB.ReadOnly = true;
            this._curFazaB.Size = new System.Drawing.Size(55, 20);
            this._curFazaB.TabIndex = 2;
            this._curFazaB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(29, 226);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "In5";
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(32, 103);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(16, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Ib";
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(32, 143);
            this.label16.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(16, 13);
            this.label16.TabIndex = 5;
            this.label16.Text = "Ic";
            // 
            // _statusLed3
            // 
            this._statusLed3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLed3.Location = new System.Drawing.Point(136, 141);
            this._statusLed3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLed3.Name = "_statusLed3";
            this._statusLed3.Size = new System.Drawing.Size(13, 13);
            this._statusLed3.State = BEMN.Forms.LedState.Off;
            this._statusLed3.TabIndex = 6;
            // 
            // _statusLed2
            // 
            this._statusLed2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLed2.Location = new System.Drawing.Point(136, 101);
            this._statusLed2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLed2.Name = "_statusLed2";
            this._statusLed2.Size = new System.Drawing.Size(13, 13);
            this._statusLed2.State = BEMN.Forms.LedState.Off;
            this._statusLed2.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(546, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "При послед. сработке, А";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(384, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Текущ. значение";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(32, 184);
            this.label9.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(16, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "In";
            // 
            // _statusLedN
            // 
            this._statusLedN.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLedN.Location = new System.Drawing.Point(136, 183);
            this._statusLedN.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLedN.Name = "_statusLedN";
            this._statusLedN.Size = new System.Drawing.Size(13, 13);
            this._statusLedN.State = BEMN.Forms.LedState.Off;
            this._statusLedN.TabIndex = 18;
            // 
            // _statusLedN5
            // 
            this._statusLedN5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLedN5.Location = new System.Drawing.Point(136, 224);
            this._statusLedN5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLedN5.Name = "_statusLedN5";
            this._statusLedN5.Size = new System.Drawing.Size(13, 13);
            this._statusLedN5.State = BEMN.Forms.LedState.Off;
            this._statusLedN5.TabIndex = 17;
            // 
            // _stupen1
            // 
            this._stupen1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._stupen1.Location = new System.Drawing.Point(275, 61);
            this._stupen1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._stupen1.Name = "_stupen1";
            this._stupen1.Size = new System.Drawing.Size(13, 13);
            this._stupen1.State = BEMN.Forms.LedState.Off;
            this._stupen1.TabIndex = 24;
            // 
            // _stupen2
            // 
            this._stupen2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._stupen2.Location = new System.Drawing.Point(275, 101);
            this._stupen2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._stupen2.Name = "_stupen2";
            this._stupen2.Size = new System.Drawing.Size(13, 13);
            this._stupen2.State = BEMN.Forms.LedState.Off;
            this._stupen2.TabIndex = 25;
            // 
            // _stupen3
            // 
            this._stupen3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._stupen3.Location = new System.Drawing.Point(275, 141);
            this._stupen3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._stupen3.Name = "_stupen3";
            this._stupen3.Size = new System.Drawing.Size(13, 13);
            this._stupen3.State = BEMN.Forms.LedState.Off;
            this._stupen3.TabIndex = 26;
            // 
            // _stupenN
            // 
            this._stupenN.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._stupenN.Location = new System.Drawing.Point(275, 183);
            this._stupenN.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._stupenN.Name = "_stupenN";
            this._stupenN.Size = new System.Drawing.Size(13, 13);
            this._stupenN.State = BEMN.Forms.LedState.Off;
            this._stupenN.TabIndex = 27;
            // 
            // _stupenN5
            // 
            this._stupenN5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._stupenN5.Location = new System.Drawing.Point(275, 224);
            this._stupenN5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._stupenN5.Name = "_stupenN5";
            this._stupenN5.Size = new System.Drawing.Size(13, 13);
            this._stupenN5.State = BEMN.Forms.LedState.Off;
            this._stupenN5.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(228, 11);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 26);
            this.label10.TabIndex = 23;
            this.label10.Text = "Состояние ступени индикации";
            // 
            // _statusLed1
            // 
            this._statusLed1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLed1.Location = new System.Drawing.Point(136, 61);
            this._statusLed1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLed1.Name = "_statusLed1";
            this._statusLed1.Size = new System.Drawing.Size(13, 13);
            this._statusLed1.State = BEMN.Forms.LedState.Off;
            this._statusLed1.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this._curFazaA);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(362, 51);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(138, 32);
            this.panel1.TabIndex = 29;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(76, 8);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "А";
            // 
            // _curFazaA
            // 
            this._curFazaA.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._curFazaA.Location = new System.Drawing.Point(18, 5);
            this._curFazaA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curFazaA.Name = "_curFazaA";
            this._curFazaA.ReadOnly = true;
            this._curFazaA.Size = new System.Drawing.Size(55, 20);
            this._curFazaA.TabIndex = 2;
            this._curFazaA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _relayLed
            // 
            this._relayLed.Location = new System.Drawing.Point(142, 19);
            this._relayLed.Name = "_relayLed";
            this._relayLed.Size = new System.Drawing.Size(13, 13);
            this._relayLed.State = BEMN.Forms.LedState.Off;
            this._relayLed.TabIndex = 8;
            // 
            // _btnModINInput
            // 
            this._btnModINInput.Location = new System.Drawing.Point(6, 35);
            this._btnModINInput.Name = "_btnModINInput";
            this._btnModINInput.Size = new System.Drawing.Size(67, 26);
            this._btnModINInput.TabIndex = 16;
            this._btnModINInput.Text = "Ввести";
            this._btnModINInput.UseVisualStyleBackColor = true;
            this._btnModINInput.Click += new System.EventHandler(this._btnModINInput_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 19);
            this.label11.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(154, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Режим ступени индикации In";
            // 
            // _btnModINOutput
            // 
            this._btnModINOutput.Location = new System.Drawing.Point(79, 35);
            this._btnModINOutput.Name = "_btnModINOutput";
            this._btnModINOutput.Size = new System.Drawing.Size(67, 26);
            this._btnModINOutput.TabIndex = 17;
            this._btnModINOutput.Text = "Вывести";
            this._btnModINOutput.UseVisualStyleBackColor = true;
            this._btnModINOutput.Click += new System.EventHandler(this._btnModINOutput_Click);
            // 
            // _modN
            // 
            this._modN.Location = new System.Drawing.Point(172, 19);
            this._modN.Name = "_modN";
            this._modN.Size = new System.Drawing.Size(13, 13);
            this._modN.State = BEMN.Forms.LedState.Off;
            this._modN.TabIndex = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._modN);
            this.groupBox1.Controls.Add(this._btnModINOutput);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this._btnModINInput);
            this.groupBox1.Location = new System.Drawing.Point(198, 269);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(191, 73);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._modN5);
            this.groupBox2.Controls.Add(this._btnModIN5Output);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this._btnModIN5Input);
            this.groupBox2.Location = new System.Drawing.Point(395, 269);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(191, 73);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            // 
            // _modN5
            // 
            this._modN5.Location = new System.Drawing.Point(172, 19);
            this._modN5.Name = "_modN5";
            this._modN5.Size = new System.Drawing.Size(13, 13);
            this._modN5.State = BEMN.Forms.LedState.Off;
            this._modN5.TabIndex = 10;
            // 
            // _btnModIN5Output
            // 
            this._btnModIN5Output.Location = new System.Drawing.Point(79, 35);
            this._btnModIN5Output.Name = "_btnModIN5Output";
            this._btnModIN5Output.Size = new System.Drawing.Size(67, 26);
            this._btnModIN5Output.TabIndex = 17;
            this._btnModIN5Output.Text = "Вывести";
            this._btnModIN5Output.UseVisualStyleBackColor = true;
            this._btnModIN5Output.Click += new System.EventHandler(this._btnModIN5Output_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 19);
            this.label12.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(160, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Режим ступени индикации In5";
            // 
            // _btnModIN5Input
            // 
            this._btnModIN5Input.Location = new System.Drawing.Point(6, 35);
            this._btnModIN5Input.Name = "_btnModIN5Input";
            this._btnModIN5Input.Size = new System.Drawing.Size(67, 26);
            this._btnModIN5Input.TabIndex = 16;
            this._btnModIN5Input.Text = "Ввести";
            this._btnModIN5Input.UseVisualStyleBackColor = true;
            this._btnModIN5Input.Click += new System.EventHandler(this._btnModIN5Input_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._relayLed);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(12, 269);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(180, 73);
            this.groupBox3.TabIndex = 26;
            this.groupBox3.TabStop = false;
            // 
            // _kvitBtn
            // 
            this._kvitBtn.Location = new System.Drawing.Point(638, 269);
            this._kvitBtn.Name = "_kvitBtn";
            this._kvitBtn.Size = new System.Drawing.Size(95, 26);
            this._kvitBtn.TabIndex = 17;
            this._kvitBtn.Text = "Квитирование";
            this._kvitBtn.UseVisualStyleBackColor = true;
            this._kvitBtn.Click += new System.EventHandler(this.KvitirovanieClick);
            // 
            // ItkzMeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 355);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this._kvitBtn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ItkzMeasuringForm";
            this.Text = "Measuring";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ItkzMeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.ItkzMeasuringForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private BEMN.Forms.LedControl _relayLed;
        private System.Windows.Forms.Label label4;
        private BEMN.Forms.LedControl _statusLed3;
        private BEMN.Forms.LedControl _statusLed2;
        private System.Windows.Forms.Label label2;
        private BEMN.Forms.LedControl _statusLed1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private BEMN.Forms.LedControl _statusLedN;
        private BEMN.Forms.LedControl _statusLedN5;
        private System.Windows.Forms.Label label10;
        private BEMN.Forms.LedControl _stupen1;
        private BEMN.Forms.LedControl _stupen2;
        private BEMN.Forms.LedControl _stupen3;
        private BEMN.Forms.LedControl _stupenN;
        private BEMN.Forms.LedControl _stupenN5;
        private System.Windows.Forms.Button _btnModINInput;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button _btnModINOutput;
        private BEMN.Forms.LedControl _modN;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private BEMN.Forms.LedControl _modN5;
        private System.Windows.Forms.Button _btnModIN5Output;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button _btnModIN5Input;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox _lastFazaN5;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox _lastFazaN;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox _lastFazaC;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox _lastFazaB;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox _lastFazaA;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox _curFazaN5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox _curFazaN;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox _curFazaC;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox _curFazaB;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox _curFazaA;
        private System.Windows.Forms.Button _kvitBtn;
    }
}