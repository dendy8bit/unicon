﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Interfaces;

namespace BEMN.ItkzV8
{
    public partial class ItkzMeasuringForm : Form, IFormView
    {
        private readonly ItkzDeviceVer8 _device;
        private readonly LedControl[] _statusLeds;

        private enum Command
        {
            NONE,
            KVITIROVANIE,
            ENTER_IN,
            REMOVE_IN,
            ENTER_IN5,
            REMOVE_IN5
        }

        private Command _currentCmd;

        public ItkzMeasuringForm()
        {
            this.Multishow = false;
            this.InitializeComponent();
        }

        public ItkzMeasuringForm(ItkzDeviceVer8 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopLoad;
            this.Multishow = false;
            this._currentCmd = Command.NONE;
            this._device.Measuring.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringReadOk);
            this._device.Measuring.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringReadFail);

            this._device.Command.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.SendCommandOk);
            this._device.Command.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.SendCommandFail);
            this._statusLeds = new[]
            {
                this._statusLed1, this._statusLed2, this._statusLed3, this._statusLedN,
                this._statusLedN5, this._stupen1, this._stupen2, this._stupen3, this._stupenN, this._stupenN5,
                this._modN, this._modN5, new LedControl(), this._relayLed
            };
        }

        private void SendCommandOk()
        {
            switch (this._currentCmd)
            {
                case Command.KVITIROVANIE:
                    MessageBox.Show(@"Квитирование выполнено успешно", @"Квитирование", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.ENTER_IN:
                    MessageBox.Show(@"Режим ступени индикации In введен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.REMOVE_IN:
                    MessageBox.Show(@"Режим ступени индикации In выведен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.ENTER_IN5:
                    MessageBox.Show(@"Режим ступени индикации In5 введен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.REMOVE_IN5:
                    MessageBox.Show(@"Режим ступени индикации In5 выведен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
            this.Process = false;
        }

        private void SendCommandFail()
        {
            switch (this._currentCmd)
            {
                case Command.KVITIROVANIE:
                    MessageBox.Show(@"Невозможно выполнить квитирование", @"Квитирование", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.ENTER_IN:
                    MessageBox.Show(@"Невозможно выполнить команду ввода режима ступени индикации In!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.REMOVE_IN:
                    MessageBox.Show(@"Невозможно выполнить команду вывода режима ступени индикации In!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.ENTER_IN5:
                    MessageBox.Show(@"Невозможно выполнить команду ввода режима ступени индикации In5!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.REMOVE_IN5:
                    MessageBox.Show(@"Невозможно выполнить команду вывода режима ступени индикации In5!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
            this.Process = false;
        }

        private bool Process
        {
            set
            {
                this._btnModINInput.Enabled = !value;
                this._btnModINOutput.Enabled = !value;
                this._btnModIN5Input.Enabled = !value;
                this._btnModIN5Output.Enabled = !value;
                this._kvitBtn.Enabled = !value;
            }
        }

        private void MeasuringReadOk()
        {
            LedManager.SetLeds(this._statusLeds, this._device.Measuring.Value.Status);
            this._curFazaA.Text = this._device.Measuring.Value.IaValue.ToString();
            this._curFazaB.Text = this._device.Measuring.Value.IbValue.ToString();
            this._curFazaC.Text = this._device.Measuring.Value.IcValue.ToString();
            this._curFazaN.Text = this._device.Measuring.Value.InValue.ToString();
            this._curFazaN5.Text = this._device.Measuring.Value.In5Value.ToString();
            this._lastFazaA.Text = this._device.Measuring.Value.IaLastexcessValue.ToString();
            this._lastFazaB.Text = this._device.Measuring.Value.IbLastexcessValue.ToString();
            this._lastFazaC.Text = this._device.Measuring.Value.IcLastexcessValue.ToString();
            this._lastFazaN.Text = this._device.Measuring.Value.InLastexcessValue.ToString();
            this._lastFazaN5.Text = this._device.Measuring.Value.In5LastexcessValue.ToString();
        }

        private void MeasuringReadFail()
        {
            LedManager.TurnOffLeds(this._statusLeds);
            this._curFazaA.Text = string.Empty;
            this._curFazaB.Text = string.Empty;
            this._curFazaC.Text = string.Empty;
            this._curFazaN.Text = string.Empty;
            this._curFazaN5.Text = string.Empty;
            this._lastFazaA.Text = string.Empty;
            this._lastFazaB.Text = string.Empty;
            this._lastFazaC.Text = string.Empty;
            this._lastFazaN.Text = string.Empty;
            this._lastFazaN5.Text = string.Empty;
        }

        private void ItkzMeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopLoad();
        }

        private void ItkzMeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.Measuring.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopLoad;
        }

        private void StartStopLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.Measuring.LoadStructCycle();
            }
            else
            {
                this._device.Measuring.RemoveStructQueries();
                this.MeasuringReadFail();
            }
        }

        private void _btnModINInput_Click(object sender, EventArgs e)
        {
            this._currentCmd = Command.ENTER_IN;
            this.SendCmd(0x04AA);
        }

        private void _btnModINOutput_Click(object sender, EventArgs e)
        {
            this._currentCmd = Command.REMOVE_IN;
            this.SendCmd(0x0455);
        }

        private void _btnModIN5Input_Click(object sender, EventArgs e)
        {
            this._currentCmd = Command.ENTER_IN5;
            this.SendCmd(0x05AA);
        }

        private void _btnModIN5Output_Click(object sender, EventArgs e)
        {
            this._currentCmd = Command.REMOVE_IN5;
            this.SendCmd(0x0555);
        }

        private void KvitirovanieClick(object sender, EventArgs e)
        {
            this._currentCmd = Command.KVITIROVANIE;
            this.SendCmd(0xAAAA);
        }

        private void SendCmd(ushort command)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.Process = true;
            this._device.Command.Value.Word = command;
            this._device.Command.SaveStruct6();
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof (ItkzDeviceVer8); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof (ItkzMeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Измерения"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}
