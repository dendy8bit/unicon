﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules.Byte;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.ItkzV8.Structures;
using BEMN.MBServer;

namespace BEMN.ItkzV8
{
    public partial class ItkzConfigurationForm : Form, IFormView
    {
        #region Feilds
        private ItkzDeviceVer8 _device;
        private readonly NewStructValidator<ConfigStruct> _devNumValidator;
        private readonly NewStructValidator<UstavkiStruct> _ustavkiValidator;
        #endregion

        #region Constructor
        public ItkzConfigurationForm()
        {
            this.InitializeComponent();
        }

        public ItkzConfigurationForm(ItkzDeviceVer8 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.Config.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadDevNumOk);
            this._device.Config.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
               MessageBox.Show("Невозможно прочитать номер и скорость устройства", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error));
            this._device.Config.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._device.Accept.Value.Word = 0x5555;
                this._device.Accept.SaveStruct6();
            });

            this._device.Accept.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show(
                    "Запись в устройство прошла успешно.\nЧтобы изменения вступили в силу, требуется отключить питание",
                    "Запись в устройство", MessageBoxButtons.OK, MessageBoxIcon.Information);
            });
            this._device.Accept.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show("Невозможно выполнить команду подтверждения записи", "Внимание",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            });

            this._device.Ustavki.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadConfigOk);
            this._device.Ustavki.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this,() =>
                MessageBox.Show("Конфигурация записана", "Запись", MessageBoxButtons.OK, MessageBoxIcon.Information));
            this._device.Ustavki.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => 
                MessageBox.Show("Ошибка чтения конфигурации", "Чтение", MessageBoxButtons.OK, MessageBoxIcon.Error));
            this._device.Ustavki.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Ошибка записи конфигурации", "Запись", MessageBoxButtons.OK, MessageBoxIcon.Error));

            ToolTip toolTip = new ToolTip();
            this._devNumValidator = new NewStructValidator<ConfigStruct>
                (
                toolTip,
                    new ControlInfoText(this._devNumBox, new CustomByteRule(1, 247)),
                    new ControlInfoCombo(this._baundRateCmb, ConfigStruct.Rate)
                );
            this._ustavkiValidator = new NewStructValidator<UstavkiStruct>
                (
                toolTip,
                new ControlInfoText(this._fazaA, new CustomDoubleRule(0.05, 1)),
                new ControlInfoText(this._fazaB, new CustomDoubleRule(0.05, 1)),
                new ControlInfoText(this._fazaC, new CustomDoubleRule(0.05, 1)),
                new ControlInfoText(this._fazaN, new CustomDoubleRule(0.05, 1)),
                new ControlInfoText(this._fazaN5, new CustomDoubleRule(0.05, 1)),
                new ControlInfoText(this._fazatA, new CustomUshortRule(10, 30000)),
                new ControlInfoText(this._fazatB, new CustomUshortRule(10, 30000)),
                new ControlInfoText(this._fazatC, new CustomUshortRule(10, 30000)),
                new ControlInfoText(this._fazatN, new CustomUshortRule(10, 30000)),
                new ControlInfoText(this._fazatN5, new CustomUshortRule(10, 30000))
                );
        }
        #endregion

        #region Handlers
        private void ReadDevNumOk()
        {
            this._devNumValidator.Set(this._device.Config.Value);
        }

        private void ReadConfigOk()
        {
            this._ustavkiValidator.Set(this._device.Ustavki.Value);
        }

        private void ItkzConfigurationForm_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig)
            {
                this.StartLoad();
            }
        }

        private void StartLoad()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.Config.LoadStruct();
            this._device.Ustavki.LoadStruct();
        }
        
        private void _readBtn_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this._device.Ustavki.LoadStruct();
        }

        private void _writeBtn_Click(object sender, EventArgs e)
        {
            this.WriteSetpoints();
        }

        private void WriteSetpoints()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            string str;
            if (this._ustavkiValidator.Check(out str, true))
            {
                this._device.Ustavki.Value = this._ustavkiValidator.Get();
                this._device.Ustavki.SaveStruct();
            }
            else
            {
                MessageBox.Show("Введены неверные уставки. Невозможно записать конфигурацию", "Внимание",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void _readDevNumBtn_Click(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this._device.Config.LoadStruct();
        }

        private void _acceptBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this._devNumValidator.Check())
            {
                this._device.Config.SaveOneWord(Convert.ToUInt16(this._devNumBox.Text));
            }
            else
            {
                MessageBox.Show("Неверно задан номер устройства", "Внимание", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
        }

        private void _acceptRateBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.Accept.Value.Word = ConfigStruct.ComandRateList[this._baundRateCmb.SelectedIndex];
            this._device.Accept.SaveStruct();
        }
        
        private void readFromFileBtn_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
          if (this.openFileDialog.ShowDialog() != DialogResult.OK) return;
            this.Deserialize(this.openFileDialog.FileName, "ITKZ");
        }
        private void writeToFileBtn_Click(object sender, EventArgs e)
        {
            this.SaveinFile();
        }

        private void SaveinFile()
        {
            if (this.saveFileDialog.ShowDialog() != DialogResult.OK) return;
            string message;
            if (this._ustavkiValidator.Check(out message, true) && this._devNumValidator.Check(out message, true))
            {
                var ustavki = (StructBase) this._ustavkiValidator.Get();
                var devNum = (StructBase) this._devNumValidator.Get();
                this.Serialize(this.saveFileDialog.FileName, ustavki, devNum, "ITKZ");
            }
            else
            {
                MessageBox.Show("Обнаружены неккоректные уставки. Конфигурация не может быть сохранена.",
                    "Сохранение уставок", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            } 
        }

        private void Serialize(string binFileName, StructBase config, StructBase devNum, string head)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement(head));

                List<ushort> values = new List<ushort>(config.GetValues());
                values.AddRange(devNum.GetValues());

                XmlElement element = doc.CreateElement(string.Format("{0}_SET_POINTS", head));
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values.ToArray(), false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);
                doc.Save(binFileName);

                MessageBox.Show("Запись конфигурации прошла успешно", "Запись конфигурации", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Ошибка записи конфигурации", "Сохранение крнфигурации", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void Deserialize(string binFileName, string head)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);

                XmlNode a = doc.FirstChild.SelectSingleNode(string.Format("{0}_SET_POINTS", head));
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);
                int ustLen = this._ustavkiValidator.Get().GetValues().Length*2;
                int devNumLen = this._devNumValidator.Get().GetValues().Length*2;
                byte[] ustavkiValues = new byte[ustLen];
                byte[] devNumValues = new byte[devNumLen];
                Array.ConstrainedCopy(values, 0, ustavkiValues, 0, ustLen);
                Array.ConstrainedCopy(values, ustLen, devNumValues, 0, devNumLen);
                UstavkiStruct ustavki = new UstavkiStruct();
                ustavki.InitStruct(ustavkiValues);
                this._ustavkiValidator.Set(ustavki);
                ConfigStruct devNum = new ConfigStruct();
                devNum.InitStruct(devNumValues);
                this._devNumValidator.Set(devNum);

                MessageBox.Show("Файл конфигурации загружен успешно", "Загрузка конфигурации", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Невозможно загрузить файл конфигурации", "Загрузка конфигурации", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(ItkzDeviceVer8); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(ItkzConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void ITKZConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteSetpoints();
                    break;
                case Keys.R:
                    this.StartLoad();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteSetpoints();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }

        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled =
                this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }
    }
}
