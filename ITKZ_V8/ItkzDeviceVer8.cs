﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Interfaces;
using BEMN.ItkzV8.Structures;
using BEMN.MBServer;

namespace BEMN.ItkzV8
{
    public class ItkzDeviceVer8: Device, IDeviceView
    {
        #region [Fields]
        private MemoryEntity<UstavkiStruct> _ustavki;
        private MemoryEntity<ConfigStruct> _configStruct;
        private MemoryEntity<OneWordStruct> _accept;
        private MemoryEntity<ItkzMeasuringStruct> _measuring;
        private MemoryEntity<OneWordStruct> _command;
        #endregion [Fields]

        #region [Constructor]
        public ItkzDeviceVer8() { }

        public ItkzDeviceVer8(Modbus mb)
        {
            HaveVersion = false;
            MB = mb;
            this._configStruct = new MemoryEntity<ConfigStruct>("Конфигурация усройства", this, 0x000B);
            this._accept = new MemoryEntity<OneWordStruct>("Команда подтверждение", this, 0x0000);
            this._ustavki = new MemoryEntity<UstavkiStruct>("ИТКЗ уставки", this, 0x0001);
            this._measuring = new MemoryEntity<ItkzMeasuringStruct>("ИТКЗ измерения", this, 0x000D);
            this._command = new MemoryEntity<OneWordStruct>("Команда ввода/вывода ступеней", this, 0x0000);
        }

        #endregion [Constructor]

        #region [Properties]
        public MemoryEntity<UstavkiStruct> Ustavki => this._ustavki;
        public MemoryEntity<ConfigStruct> Config => this._configStruct;
        public MemoryEntity<OneWordStruct> Accept => this._accept;
        public MemoryEntity<ItkzMeasuringStruct> Measuring => this._measuring;
        public MemoryEntity<OneWordStruct> Command => this._command;
        #endregion [Properties]

        #region [INodeView Members]
        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType => typeof(ItkzDeviceVer8);

        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow => false;

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage => Framework.Properties.Resources.itkz;

        [Browsable(false)]
        public string NodeName => "ИТКЗ исполнение 2 (без Uo)";

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes => new INodeView[] { };

        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable => true;
        #endregion [INodeView Members]
    }
}
