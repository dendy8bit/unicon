﻿using System;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules.Double;

namespace BEMN.ItkzV8.Calibrate
{
    public partial class CoefficientsPanel : UserControl
    {
        private MemoryEntity<CalibrateCoefficients> _calibrationCoef;
        private NewStructValidator<CalibrateCoefficients> _validator;
        private MaskedTextBox[] _boxes;
        private Action<bool> _callBack;

        public CoefficientsPanel(ItkzDeviceVer8 device)
        {
            this.InitializeComponent();
            this._boxes = new[]
            {
                this._ka1, this._ka2, this._ka3, this._kb1, this._kb2, this._kb3, this._kc1, this._kc2, this._kc3
            };
            this._calibrationCoef = new MemoryEntity<CalibrateCoefficients>("Калибровочные коэффициенты", device, 0x23);
            this._calibrationCoef.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.OnReadCoefOk);
            this._calibrationCoef.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => this._callBack?.Invoke(false));
            this._calibrationCoef.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () => this._callBack?.Invoke(true));
            this._calibrationCoef.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () => this._callBack?.Invoke(false));

            this._validator = new NewStructValidator<CalibrateCoefficients>(new ToolTip(),
                new ControlInfoText(this._ka1, new CustomDoubleRule(-7.99, 7.99, 2)),
                new ControlInfoText(this._ka2, new CustomDoubleRule(-7.99, 7.99, 2)),
                new ControlInfoText(this._ka3, new CustomDoubleRule(-7.99, 7.99, 2)),
                new ControlInfoText(this._kb1, new CustomDoubleRule(-7.99, 7.99, 2)),
                new ControlInfoText(this._kb2, new CustomDoubleRule(-7.99, 7.99, 2)),
                new ControlInfoText(this._kb3, new CustomDoubleRule(-7.99, 7.99, 2)),
                new ControlInfoText(this._kc1, new CustomDoubleRule(-7.99, 7.99, 2)),
                new ControlInfoText(this._kc2, new CustomDoubleRule(-7.99, 7.99, 2)),
                new ControlInfoText(this._kc3, new CustomDoubleRule(-7.99, 7.99, 2)));
        }

        public void SetStartState()
        {
            this._infoLabel.Text = Info.PANEL1_INFO1;
            foreach (MaskedTextBox box in this._boxes)
            {
                box.ReadOnly = true;
            }
        }

        public void SetReadCoefficients(CalibrateCoefficients str)
        {
            foreach (MaskedTextBox box in this._boxes)
            {
                box.ReadOnly = false;
            }
            this._validator.Set(str);
        }

        public bool ErrorFunc
        {
            get
            {
                double _ka1Double = Convert.ToDouble(this._ka1.Text);
                double _ka2Double = Convert.ToDouble(this._ka2.Text);
                double _ka3Double = Convert.ToDouble(this._ka3.Text);
                double _kb1Double = Convert.ToDouble(this._kb1.Text);
                double _kb2Double = Convert.ToDouble(this._kb2.Text);
                double _kb3Double = Convert.ToDouble(this._kb3.Text);
                double _kc1Double = Convert.ToDouble(this._kc1.Text);
                double _kc2Double = Convert.ToDouble(this._kc2.Text);
                double _kc3Double = Convert.ToDouble(this._kc3.Text);

                if (_ka1Double <= -8 || _ka1Double >= 8) return true;
                if (_ka2Double <= -8 || _ka2Double >= 8) return true;
                if (_ka3Double <= -8 || _ka3Double >= 8) return true;
                if (_kb1Double <= -8 || _kb1Double >= 8) return true;
                if (_kb2Double <= -8 || _kb2Double >= 8) return true;
                if (_kb3Double <= -8 || _kb3Double >= 8) return true;
                if (_kc1Double <= -8 || _kc1Double >= 8) return true;
                if (_kc2Double <= -8 || _kc2Double >= 8) return true;
                if (_kc3Double <= -8 || _kc3Double >= 8) return true;
                return false;
            }
        }

        public void ReadCoeff(Action<bool> callBack)
        {
            this._callBack = callBack;
            this._calibrationCoef.LoadStruct();
        }

        private void OnReadCoefOk()
        {
            this._validator.Set(this._calibrationCoef.Value);
            this._callBack?.Invoke(true);
        }

        public void WriteDefaultCoefficients(Action<bool> callBack)
        {
            this._callBack = callBack;
            CalibrateCoefficients str = new CalibrateCoefficients
            {
                Ka1 = 1,
                Kb2 = 1,
                Kc3 = 1,
                Ka2 = 0,
                Ka3 = 0,
                Kb1 = 0,
                Kb3 = 0,
                Kc1 = 0,
                Kc2 = 0
            };
            this._calibrationCoef.Value = str;
            this._calibrationCoef.SaveStruct();
        }

        public void WriteCoeffInDevice(Action<bool> callBack)
        {
            this._callBack = callBack;
            this._calibrationCoef.Value = this._validator.Get();
            this._calibrationCoef.SaveStruct();
        }

        public bool IsDefault
        {
            get
            {
                CalibrateCoefficients str = this._calibrationCoef.Value;
                return str.Ka1 == 1 && str.Kb2 == 1 && str.Kc3 == 1
                       && str.Ka2 == 0 && str.Ka3 == 0
                       && str.Kb1 == 0 && str.Kb3 == 0
                       && str.Kc1 == 0 && str.Kc2 == 0;
            }
        }
    }
}
