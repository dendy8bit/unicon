﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Interfaces;

namespace BEMN.ItkzV8.Calibrate
{
    public partial class CalibrateForm : Form, IFormView
    {
        private CoefficientsPanel _coefPanel;
        private MeasuringPanel _measuringPanel;
        private ItkzDeviceVer8 _device;

        internal enum Step
        {
            Step1,Step2,Step3,Step4,Step5,Step6
        }

        private Step _currentStep;

        public CalibrateForm()
        {
            this.InitializeComponent();
        }

        public CalibrateForm(ItkzDeviceVer8 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._coefPanel = new CoefficientsPanel(device);
            this._coefPanel.Dock = DockStyle.Fill;
            this._measuringPanel = new MeasuringPanel(device);
            this._measuringPanel.Dock = DockStyle.Fill;
        }

        private void CalibrateForm_Load(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
                this.SetStartState();
        }

        private void SetStartState()
        {
            this._reset.Enabled = false;
            this._continueBtn.Enabled = true;
            this._coefPanel.SetStartState();
            this._coefPanel.ReadCoeff(this.StepDone);
            this.pannelGroupBox.Controls.Clear();
            this.pannelGroupBox.Controls.Add(this._coefPanel);
            if (this._coefPanel.ErrorFunc)
            {
                MessageBox.Show("Невозможно выполнить шаг. Дальнейшая калибровка невозможна, \nт.к. коэффициенты находятся вне разрешенного диапазона", "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this._reset.Enabled = false;
                this._continueBtn.Enabled = true;
                this._coefPanel.SetStartState();
                this._coefPanel.ReadCoeff(this.StepDone);
                this.pannelGroupBox.Controls.Clear();
                return;
            }
            this._currentStep = Step.Step1;
        }

        private void _continueBtn_Click(object sender, EventArgs e)
        {
            this._continueBtn.Enabled = false;
            switch (this._currentStep)
            {
                case Step.Step2:
                    {
                        if (!this._coefPanel.IsDefault)
                        {
                            DialogResult res = MessageBox.Show("Устройство уже скалибровано. Будут записаны коэффициенты по умолчанию. Начать калибровку?",
                                "Калибровка", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (res == DialogResult.No)
                            {
                                this.SetStartState();
                                return;
                            }
                        }
                        this._reset.Enabled = true;
                        this._coefPanel.WriteDefaultCoefficients(this.StepDone);
                    }
                    break;
                case Step.Step4:
                    {
                        this._measuringPanel.CalculateCoefficients();
                        this.StepDone(true);
                    }
                    break;
                case Step.Step5:
                    {
                        this._reset.Enabled = false;
                        this._coefPanel.WriteCoeffInDevice(this.StepDone);
                    }
                    break;
                case Step.Step6:
                    this._continueBtn.Text = "Далее";
                    this.SetStartState();
                    break;
                default:
                    this.SetStartState();
                    break;
            }
        }

        private void StepDone(bool result)
        {
            this._continueBtn.Enabled = true;
            if (result)
            {
                this.IncrementStep();
            }
            else
            {
                if (this._currentStep == Step.Step1)
                {
                    MessageBox.Show("Невозможно прочитать текщие коэффициенты в устройстве.", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Невозможно выполнить шаг. Калибровка будет сброшена", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.SetStartState();
                }
            }
        }

        private void IncrementStep()
        {
            switch (this._currentStep)
            {
                case Step.Step1:
                    {
                        this._currentStep = Step.Step2;
                        this._infoRichTextBox.Text = Info.STEP2_INFO;
                    }
                    break;
                case Step.Step2:
                    {
                        this._currentStep = Step.Step3;
                        this._continueBtn.Enabled = false;
                        this.pannelGroupBox.Controls.Remove(this._coefPanel);
                        this.pannelGroupBox.Controls.Add(this._measuringPanel);
                        this._measuringPanel.SetStartParametersMeasuring(this.StepDone);
                        this._infoRichTextBox.Text = Info.STEP3_INFO;
                    }
                    break;
                case Step.Step3:
                    {
                        this._currentStep = Step.Step4;
                        this._infoRichTextBox.Text = Info.STEP4_INFO;
                    }
                    break;
                case Step.Step4:
                    {
                        this._currentStep = Step.Step5;
                        this._infoRichTextBox.Text = Info.STEP5_INFO;
                        this._coefPanel.SetReadCoefficients(this._measuringPanel.Coefficients.Coefficients);
                        this.pannelGroupBox.Controls.Remove(this._measuringPanel);
                        this.pannelGroupBox.Controls.Add(this._coefPanel);
                    }
                    break;
                case Step.Step5:
                    this._currentStep = Step.Step6;
                        this._infoRichTextBox.Text = Info.STEP6_INFO;
                        this._continueBtn.Text = "Завершить";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void CalibrateForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._measuringPanel.StopMeasuring();
        }

        private void _reset_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Вы уверены, что хотите начать заново калибровку устройства? Все текущие данные будут утеряны.", "Сброс калибровки", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (res == DialogResult.Yes)
            {
                this.SetStartState();
            }
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof (ItkzDeviceVer8); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof (CalibrateForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.Calibrate; }
        }

        public string NodeName
        {
            get { return "Калибровка"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}
