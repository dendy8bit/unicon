﻿namespace BEMN.ItkzV8.Calibrate
{
    partial class CalibrateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SplitContainer = new System.Windows.Forms.SplitContainer();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._infoRichTextBox = new BEMN.Forms.RichTextBox.AdvRichTextBox();
            this.pannelGroupBox = new System.Windows.Forms.GroupBox();
            this._saveInFile = new System.Windows.Forms.Button();
            this._saveInDevice = new System.Windows.Forms.Button();
            this._reset = new System.Windows.Forms.Button();
            this._continueBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer)).BeginInit();
            this.SplitContainer.Panel1.SuspendLayout();
            this.SplitContainer.Panel2.SuspendLayout();
            this.SplitContainer.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SplitContainer);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(475, 315);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Калибровка каналов";
            // 
            // SplitContainer
            // 
            this.SplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer.Location = new System.Drawing.Point(3, 16);
            this.SplitContainer.Name = "SplitContainer";
            this.SplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SplitContainer.Panel1
            // 
            this.SplitContainer.Panel1.Controls.Add(this.groupBox3);
            this.SplitContainer.Panel1.Controls.Add(this.pannelGroupBox);
            // 
            // SplitContainer.Panel2
            // 
            this.SplitContainer.Panel2.Controls.Add(this._saveInFile);
            this.SplitContainer.Panel2.Controls.Add(this._saveInDevice);
            this.SplitContainer.Panel2.Controls.Add(this._reset);
            this.SplitContainer.Panel2.Controls.Add(this._continueBtn);
            this.SplitContainer.Size = new System.Drawing.Size(469, 296);
            this.SplitContainer.SplitterDistance = 248;
            this.SplitContainer.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._infoRichTextBox);
            this.groupBox3.Location = new System.Drawing.Point(291, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(175, 242);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Информация";
            // 
            // _infoRichTextBox
            // 
            this._infoRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._infoRichTextBox.Location = new System.Drawing.Point(3, 16);
            this._infoRichTextBox.Name = "_infoRichTextBox";
            this._infoRichTextBox.ReadOnly = true;
            this._infoRichTextBox.SelectionAlignment = BEMN.Forms.RichTextBox.TextAlign.Justify;
            this._infoRichTextBox.Size = new System.Drawing.Size(169, 223);
            this._infoRichTextBox.TabIndex = 0;
            this._infoRichTextBox.Text = "";
            // 
            // pannelGroupBox
            // 
            this.pannelGroupBox.Location = new System.Drawing.Point(3, 3);
            this.pannelGroupBox.Name = "pannelGroupBox";
            this.pannelGroupBox.Size = new System.Drawing.Size(282, 242);
            this.pannelGroupBox.TabIndex = 0;
            this.pannelGroupBox.TabStop = false;
            // 
            // _saveInFile
            // 
            this._saveInFile.Location = new System.Drawing.Point(147, 3);
            this._saveInFile.Name = "_saveInFile";
            this._saveInFile.Size = new System.Drawing.Size(138, 23);
            this._saveInFile.TabIndex = 0;
            this._saveInFile.Text = "Сохранить в файл";
            this._saveInFile.UseVisualStyleBackColor = true;
            this._saveInFile.Visible = false;
            // 
            // _saveInDevice
            // 
            this._saveInDevice.Location = new System.Drawing.Point(3, 3);
            this._saveInDevice.Name = "_saveInDevice";
            this._saveInDevice.Size = new System.Drawing.Size(138, 23);
            this._saveInDevice.TabIndex = 0;
            this._saveInDevice.Text = "Сохранить в устройство";
            this._saveInDevice.UseVisualStyleBackColor = true;
            this._saveInDevice.Visible = false;
            // 
            // _reset
            // 
            this._reset.Enabled = false;
            this._reset.Location = new System.Drawing.Point(310, 3);
            this._reset.Name = "_reset";
            this._reset.Size = new System.Drawing.Size(75, 23);
            this._reset.TabIndex = 0;
            this._reset.Text = "Сбросить";
            this._reset.UseVisualStyleBackColor = true;
            this._reset.Click += new System.EventHandler(this._reset_Click);
            // 
            // _continueBtn
            // 
            this._continueBtn.Location = new System.Drawing.Point(391, 3);
            this._continueBtn.Name = "_continueBtn";
            this._continueBtn.Size = new System.Drawing.Size(75, 23);
            this._continueBtn.TabIndex = 0;
            this._continueBtn.Text = "Далее";
            this._continueBtn.UseVisualStyleBackColor = true;
            this._continueBtn.Click += new System.EventHandler(this._continueBtn_Click);
            // 
            // CalibrateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 315);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "CalibrateForm";
            this.Text = "Calibrate";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CalibrateForm_FormClosing);
            this.Load += new System.EventHandler(this.CalibrateForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.SplitContainer.Panel1.ResumeLayout(false);
            this.SplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer)).EndInit();
            this.SplitContainer.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.SplitContainer SplitContainer;
        private System.Windows.Forms.Button _saveInFile;
        private System.Windows.Forms.Button _saveInDevice;
        private System.Windows.Forms.Button _continueBtn;
        private System.Windows.Forms.GroupBox groupBox3;
        private BEMN.Forms.RichTextBox.AdvRichTextBox _infoRichTextBox;
        private System.Windows.Forms.Button _reset;
        private System.Windows.Forms.GroupBox pannelGroupBox;
    }
}