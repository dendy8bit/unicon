﻿using System;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.MBServer;
using System.Xml.Serialization;
using System.ComponentModel;
using BEMN.MBServer.Queries;
using System.Drawing;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MR902.Big.AlarmJournal;
using BEMN.MR902.Big.Configuration;
using BEMN.MR902.Big.Configuration.Structures;
using BEMN.MR902.Big.Configuration.Structures.Osc;
using BEMN.MR902.Big.Measuring;
using BEMN.MR902.Big.Osc;
using BEMN.MR902.Old.BSBGL;
using BEMN.MR902.Old.Configuration;
using BEMN.MR902.Old.Configuration.Structures;
using BEMN.MR902.Old.ConfigurationV203;
using BEMN.MR902.Old.Forms;
using BEMN.MR902.Old.Structures.OscillogramStruct;
using BEMN.MR902.Old.SystemJournal;
using BEMN.MR902.Properties;
using BEMN.Framework;

namespace BEMN.MR902
{
    public class Mr902 : Device, IDeviceView, IDeviceVersion
    {
        #region Константы
        public const int RELE_COUNT = 18;
        public const int INDICATOR_COUNT = 12;
        private const string LOW_VERSION = "1.00 - 1.03";
        private const string HIGH_VERSION = "2.00 - 2.02";
        #endregion

        #region Поля и события
        private MemoryEntity<ConfigurationStruct> _configuration;
        private MemoryEntity<ConfigurationStructV203> _configurationV203;
        private EntityKeeper _keeper;
        private MemoryEntity<OscOptionsStruct> _oscOptions;
        private bool _devicesInitialised;

        public event Action ConfigWriteOk;
        public event Action ConfigWriteFail;
        #endregion

        #region Programming
        private MemoryEntity<ProgramPageStruct> _programPageStruct;
        private MemoryEntity<SourceProgramStruct> _sourceProgramStruct;
        private MemoryEntity<StartStruct> _programStartStruct;
        private MemoryEntity<ProgramSignalsStruct> _programSignalsStruct;
        private MemoryEntity<OneWordStruct> _startSpl;//структуры для кнопок остановить и запустить логику в устройстве
        private MemoryEntity<OneWordStruct> _stopSpl;
        private MemoryEntity<OneWordStruct> _isSplOn;

        private MemoryEntity<SomeStruct> _stateSpl;
        public MemoryEntity<ProgramPageStruct> ProgramPage
        {
            get { return _programPageStruct; }
        }

        public MemoryEntity<OneWordStruct> StopSpl
        {
            get { return _stopSpl; }
        }

        public MemoryEntity<OneWordStruct> StartSpl
        {
            get { return _startSpl; }
        }

        public MemoryEntity<SomeStruct> StateSpl
        {
            get { return _stateSpl; }
        }
        public MemoryEntity<OneWordStruct> IsSplOn
        {
            get { return _isSplOn; }
        }


        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return _programStartStruct; }
        }
        public MemoryEntity<ProgramSignalsStruct> ProgramSignalsStruct
        {
            get { return _programSignalsStruct; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return _sourceProgramStruct; }
        }
        #endregion

        #region Свойства
        public EntityKeeper Keeper
        {
            get { return _keeper; }
        }

        public MemoryEntity<OscOptionsStruct> OscOptions
        {
            get { return _oscOptions; }
        }

        public MemoryEntity<OscilloscopeSettingsBigStructV213> AllChannels { get; private set; }

        public MemoryEntity<ConfigurationStruct> Configuration
        {
            get { return _configuration; }
        }
        
        public MemoryEntity<ConfigurationStructV203> ConfigurationV203
        {
            get { return _configurationV203; }
        }

        public MemoryEntity<ConfigurationStructBigV210> ConfigurationBigV210 { get; private set; }

        public MemoryEntity<ConfigurationStructBigV213> ConfigurationBigV213 { get; private set; }
        #endregion

        #region Конструкторы и инициализация

        public Mr902()
        {
            HaveVersion = true;
        }

        public Mr902(Modbus mb)
        {
            MB = mb;
            this.InitAddr();
            HaveVersion = true;
        }

        [XmlIgnore]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        public override Modbus MB
        {
            get
            {
                return mb;
            }
            set
            {
                mb = value;
                if (null != mb)
                {
                    mb.CompleteExchange += this.mb_CompleteExchange;
                }
            }
        }

        public void ConfirmConstraint()
        {
            SetBit(DeviceNumber, 0x0D00, true, "MR902ConfirmConfig" + DeviceNumber, this);
        }

        private void InitAddr()
        {
            this._keeper = new EntityKeeper(this);
            this._oscOptions = new MemoryEntity<OscOptionsStruct>("Параметры осциллографа", this, 0x5A0);
            this._configuration = new MemoryEntity<ConfigurationStruct>("Конфигурация МР902", this, 0x1000);
            this._configurationV203 = new MemoryEntity<ConfigurationStructV203>("Конфигурация МР902", this, 0x1000);
            this._sourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0x4300);
            this._programStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0E00);
            this._programSignalsStruct = new MemoryEntity<ProgramSignalsStruct>("LoadProgramSignals_", this, 0x4100);
            this._programPageStruct = new MemoryEntity<ProgramPageStruct>("SaveProgrammPage", this, 0x4000);

            this._stopSpl = new MemoryEntity<OneWordStruct>("Останов логической программы", this, 0x0D0C);
            this._startSpl = new MemoryEntity<OneWordStruct>("Старт логической программы", this, 0x0D08);
            this._stateSpl = new MemoryEntity<SomeStruct>("Состояние ошибок логики", this, 0x0D12);
            ushort[] values = new ushort[2];
            this._stateSpl.Slots = HelperFunctions.SetSlots(values, 0x0D12);
            this._stateSpl.Values = values;
            this._isSplOn = new MemoryEntity<OneWordStruct>("Состояние логики", this, 0x0D10);

            this.AllChannels = new MemoryEntity<OscilloscopeSettingsBigStructV213>("Все каналы осциллографа", this, 0x10FE);
        }
        #endregion

        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(Mr902); }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage => Framework.Properties.Resources.mrBig;

        [Browsable(false)]
        public string NodeName
        {
            get { return "МР902 (до в.2.13)"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion

        #region IDeviceVersionMembers
        private static string[] _deviceApparatConfig =
        {
            "T24N0D40R35(A2)",
            "T16N0D24R19(М3)"
        };

        private static string[] _deviceOutString = { "A2", string.Empty };

        public Type[] Forms
        {
            get
            {
                if (DeviceVersion == LOW_VERSION)
                {
                    DeviceVersion = "1.03";
                }
                if (DeviceVersion == HIGH_VERSION)
                {
                    DeviceVersion = "2.00";
                }
                double vers = Common.VersionConverter(DeviceVersion);
                StringData.Version = vers;
                if (vers >= 2.10)
                {
                    //if ((!this.DeviceDlgInfo.IsConnectionMode || !this.IsConnect) && !Framework.Framework.IsProjectOpening)
                    //{
                    //    ChoiceDeviceTypeForMr902 choice = new ChoiceDeviceTypeForMr902(_deviceApparatConfig, _deviceOutString);
                    //    choice.ShowDialog();
                    //    DevicePlant = choice.DevicePlant;                       
                    //}
                    StringData.DevicePlant = DevicePlant;

                    /*if (StringData.Version < 2.13)*/ this.ConfigurationBigV210 = new MemoryEntity<ConfigurationStructBigV210>("Конфигурация от вер. 2.10", this, 0x1000);
                    /*else  */                         this.ConfigurationBigV213 = new MemoryEntity<ConfigurationStructBigV213>("Конфигурация от вер. 2.13", this, 0x1000);
                    return new[]
                    {
                        typeof(BSBGLEF),
                        typeof(AlarmJournalBigForm),
                        typeof(BigConfigurationForm),
                        typeof(MeasuringBigForm),
                        typeof(OscilloscopeNewForm),
                        typeof(Mr902SystemJournalForm)
                    };
                }
                if (vers >= 2.03)
                {
                    return new[]
                    {
                        typeof(BSBGLEF),
                        typeof(Mr902ConfigurationFormV203),
                        typeof(Mr902MeasuringForm),
                        typeof(Mr902AlarmJournalForm),
                        typeof(Mr902SystemJournalForm),
                        typeof(OscilloscopeForm)
                    };
                }
                return new[]
                {
                    typeof(BSBGLEF),
                    typeof(Mr902ConfigurationForm),
                    typeof(Mr902MeasuringForm),
                    typeof(Mr902AlarmJournalForm),
                    typeof(Mr902SystemJournalForm),
                    typeof(OscilloscopeForm)
                };
            }
        }

        public List<string> Versions 
        {
            get
            {
                return new List<string>
                {
                    LOW_VERSION,
                    HIGH_VERSION,
                    "2.03",
                    "2.04",
                    "2.05",
                    "2.06",
                    "2.10",
                    "2.13"
                };
            }
        } 

        #endregion

        private new void mb_CompleteExchange(object sender, Query query)
        {
            if ("MR902ConfirmConfig" + DeviceNumber == query.name)
            {
                if (query.fail == 0)
                {
                    this.ConfigWriteOk?.Invoke();
                }
                else
                {
                    this.ConfigWriteFail?.Invoke();
                }
            }
            base.mb_CompleteExchange(sender, query);
        }
    }
}
