﻿namespace BEMN.MR902.Old.OldClasses
{
    public enum ConstraintKoefficient
    {
        K_100 = 100,
        K_128 = 128,
        K_1000 = 1000,
        K_100000 = 100000,
        K_10000 = 10000,
        K_4000 = 4000,
        K_4001 = 4001,
        K_25600 = 25600,
        K_65536 = 65536,
    };
}