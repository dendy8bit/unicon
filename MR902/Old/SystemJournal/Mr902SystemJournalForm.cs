﻿using System;
using System.IO;
using System.Windows.Forms;
using BEMN.Forms.Export;
using BEMN.Forms.SystemJournal;
using BEMN.MBServer;

namespace BEMN.MR902.Old.SystemJournal
{
    public class Mr902SystemJournalForm : Mr900SystemJournalBaseForm<SystemJournalStruct>
    {
        public override Type FormDevice
        {
            get { return typeof(Mr902); }
        }

        public override Type ClassType
        {
            get { return this.GetType(); }
        }

        public Mr902SystemJournalForm()
        {

        }

        public Mr902SystemJournalForm(Mr902 device) : base(device, device.Keeper.RefreshSystemJournal, device.Keeper.SystemJournal)
        {
            Activated += OnActivated;
            if (Common.VersionConverter(_device.DeviceVersion) >= 2.03)
            {
                _saveToHtmlBtn.Visible = true;
                _saveToHtmlBtn.Click += SaveToHtmlBtnOnClick;
            }
        }

        private void OnActivated(object sender, EventArgs eventArgs)
        {
            SystemJournalStruct.CurrentVersion = Common.VersionConverter(_device.DeviceVersion);
            SystemJournalStruct.CurrentConfig = _device.DevicePlant;
        }

        private void SaveToHtmlBtnOnClick(object sender, EventArgs eventArgs)
        {
            if (_dataTable.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }

            if (_saveHtmlDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string xml;

                    using (StringWriter writer = new StringWriter())
                    {
                        _dataTable.WriteXml(writer);
                        xml = writer.ToString();
                    }
                    HtmlExport.Export(_saveHtmlDialog.FileName, "МР902. Журнал системы", xml);
                    _statusLabel.Text = "Журнал успешно сохранен.";

                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }
    }
}
