﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR902.Old.Configuration.Structures;
using BEMN.MR902.Old.Configuration.Structures.Primary;
using BEMN.MR902.Old.Configuration.Structures.Second;
using BEMN.MR902.Properties;

namespace BEMN.MR902.Old.Configuration
{
    public partial class Mr902ConfigurationForm : Form, IFormView
    {
        #region Констансты
        private const string READ_OK = "Конфигурация успешно прочитана";
        private const string READ_FAIL = "Не удалось прочитать конфигурацию";
        private const string WRITE_OK = "Конфигурация успешно записана";
        private const string WRITE_FAIL = "Невозможно записать конфигурацию";
        #endregion

        #region Поля
        private readonly MemoryEntity<ConfigurationStruct> _configuration;
        private ConfigurationStruct _currentConfig;
        private readonly Mr902 _device;
        private ComboBox[] _oscCombos;
        private CheckedListBox[] _vlsChecBoxListArray;
        private DataGridView[] _inpSignalsArray;
        #endregion

        #region Конструкторы
        public Mr902ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public Mr902ConfigurationForm(Mr902 device)
        {
            this.InitializeComponent();
            this._device = device;
            StringData.Version = Common.VersionConverter(this._device.DeviceVersion);
            this._device.ConfigWriteOk += HandlerHelper.CreateActionHandler(this, this.AllWriteEnd);
            this._device.ConfigWriteFail += HandlerHelper.CreateActionHandler(this, this.AllWriteEndFail);
            this._configuration = device.Configuration;
            this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._device.ConfirmConstraint);
            this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.AllReadEnd);
            this._configuration.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._configuration.RemoveStructQueries();
                this.AllWriteEndFail();
            });
            this._configuration.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._configuration.RemoveStructQueries();
                this.AllReadEndFail();
            });
            this._configuration.ReadOk += HandlerHelper.CreateHandler(this, this._configProgressBar.PerformStep);
            this._configuration.WriteOk += HandlerHelper.CreateHandler(this, this._configProgressBar.PerformStep);

            this._configProgressBar.Maximum = this._configuration.Slots.Count;
            this._currentConfig = new ConfigurationStruct();
            this.PrepareTtGrid();
            this.PrepareDZH();
            this.PrepareOSC();
            this.PrepareOscchannels();
            this.PrepareUrovJoinGrid();
            this.PrepareJoinGrid();
            this.PrepareParamautomat();
            this.PrepareElssygnal();
            this.PrepareINPSIGNAL();
            this._grUstComboBox.DataSource = StringData.InputSignals;
            this._indComboBox.DataSource = StringData.InputSignals;
            this.PrepareCorrentProtAll();
            this.ShowConfiguration();
        }
        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Mr902); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(Mr902); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        #region УРОВ ДЗШ
        public void PrepareDZH()
        {
            this._DZHTUrov1.Text = "0";
            this._DZHTUrov2.Text = "0";
            this._DZHTUrov3.Text = "0";
            this._DZHModes.DataSource = StringData.ModesLight;
            this._DZHKontr.DataSource = StringData.KONTR;
            this._DZHDiff.DataSource = StringData.Forbidden;
            this._DZHSelf.DataSource = StringData.Forbidden;
            this._DZHSign.DataSource = StringData.Forbidden;
            this._DZHSH1.DataSource = StringData.InputSignals;
            this._DZHSH2.DataSource = StringData.InputSignals;
            this._DZHPO.DataSource = StringData.InputSignals;
        }

        public void ReadDZH()
        {
            UrovStruct dhz = this._currentConfig.UrovStruct;
            this._DZHModes.SelectedItem = dhz.DzhModes;
            this._DZHKontr.SelectedItem = dhz.DzhControl;
            this._DZHDiff.SelectedItem = dhz.DzhDiff;
            this._DZHSelf.SelectedItem = dhz.DzhSelf;
            this._DZHTUrov1.Text = dhz.DzhT1.ToString();
            this._DZHTUrov2.Text = dhz.DzhT2.ToString();
            this._DZHTUrov3.Text = dhz.DzhT3.ToString();
            this._DZHSign.SelectedItem = dhz.DzhSign;
            this._DZHSH1.SelectedItem = dhz.DzhSh1;
            this._DZHSH2.SelectedItem = dhz.DzhSh2;
            this._DZHPO.SelectedItem = dhz.DzhPo;
        }

        public void WriteDZH()
        {
            UrovStruct dhz = new UrovStruct();
            dhz.DzhModes = this._DZHModes.SelectedItem.ToString();
            dhz.DzhControl = this._DZHKontr.SelectedItem.ToString();
            dhz.DzhDiff = this._DZHDiff.SelectedItem.ToString();
            dhz.DzhSelf = this._DZHSelf.SelectedItem.ToString();
            dhz.DzhT1 = Convert.ToInt32(this._DZHTUrov1.Text);
            dhz.DzhT2 = Convert.ToInt32(this._DZHTUrov2.Text);
            dhz.DzhT3 = Convert.ToInt32(this._DZHTUrov3.Text);
            dhz.DzhSign = this._DZHSign.SelectedItem.ToString();
            dhz.DzhSh1 = this._DZHSH1.SelectedItem.ToString();
            dhz.DzhSh2 = this._DZHSH2.SelectedItem.ToString();
            dhz.DzhPo = this._DZHPO.SelectedItem.ToString();
            this._currentConfig.UrovStruct = dhz;
        }

        #endregion

        #region Осциллограф
        public void PrepareOSC()
        {
            this._oscCombos = new [] 
            { 
                this._oscLength, this._oscFix
            };
            this.ClearCombos(this._oscCombos);
            this._oscWriteLength.Text = "0";
            this._oscLength.DataSource = StringData.OscLength;
            this._oscFix.DataSource = StringData.OscFix;
            this.SubscriptCombos(this._oscCombos);
        }
        
        public void ReadOsc()
        {
            this._oscLength.SelectedItem = this._currentConfig.Osc.OscLength;
            this._oscWriteLength.Text = this._currentConfig.Osc.OscWLength.ToString();
            this._oscFix.SelectedItem = this._currentConfig.Osc.OscFix;
        }
  
        #endregion

        #region Осциллограф Каналы
        public void PrepareOscchannels()
        {
            this._oscSignal.DataSource = StringData.SignalSrab;
            if (this._oscChannels.Rows.Count == 0)
            {
                string[] channels = this._currentConfig.Osc.Channels;
                for (int i = 0; i < channels.Length; i++)
                {
                    this._oscChannels.Rows.Add((i + 1).ToString(), StringData.SignalSrab[0]);
                }
            }
        }

        public void ReadOSCCHANNELS()
        {
            string[] channels = this._currentConfig.Osc.Channels;
            for (int i = 0; i < channels.Length; i++)
            {
                this._oscChannels.Rows[i].Cells["_oscSignal"].Value = channels[i];
            }
        }

        public void WriteALLOSC()
        {
            OscilloscopeSettingsStruct config = new OscilloscopeSettingsStruct();
            config.OscLength = this._oscLength.SelectedItem.ToString();
            config.OscWLength = Convert.ToUInt16(this._oscWriteLength.Text);
            config.OscFix = this._oscFix.SelectedItem.ToString();

            string[] channels = new string[8];
            for (int i = 0; i < channels.Length; i++)
            {
                channels[i] = this._oscChannels.Rows[i].Cells["_oscSignal"].Value.ToString();
               
            }
            config.Channels = channels;
            this._currentConfig.Osc = config;
        }
        #endregion

        #region УРОВ Присоединения
        public void PrepareUrovJoinGrid()
        {
            if (this._UROVJoinData.Rows.Count == 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    this._UROVJoinData.Rows.Add("Присоединение " + (i + 1), 0, 0);
                }
                this._UROVJoinData.Rows.Add("In ", 0, 0);
            }
        }

        public void ReadUrovJoinGrid()
        {
            for (int i = 0; i < 6; i++)
            {
                this._UROVJoinData.Rows[i].Cells["_JoinIUROV"].Value = this._currentConfig.UrovConn.Ust[i].UrovJoinI;
                this._UROVJoinData.Rows[i].Cells["_JoinTUROV"].Value = this._currentConfig.UrovConn.Ust[i].UrovJoinT;
            }
        }

        public void WriteUrovJoinGrid()
        {
            
            for (int i = 0; i < 6; i++)
            {
                this._currentConfig.UrovConn.Ust[i].UrovJoinI =
                    Convert.ToDouble(this._UROVJoinData.Rows[i].Cells["_JoinIUROV"].Value);
                this._currentConfig.UrovConn.Ust[i].UrovJoinT =
                    Convert.ToInt32(this._UROVJoinData.Rows[i].Cells["_JoinTUROV"].Value);
            }
        }

        #endregion

        #region Присоединения
        public void PrepareJoinGrid()
        {
            this._joinSwitchOFF.DataSource = StringData.InputSignals;
            this._joinSwitchOn.DataSource = StringData.InputSignals;
            this._joinJoin.DataSource = StringData.Join;
            this._joinEnter.DataSource = StringData.InputSignals;
            if (this._joinData.Rows.Count == 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    this._joinData.Rows.Add("Присоединение " + (i + 1), 0, StringData.InputSignals[0], StringData.InputSignals[0], StringData.Join[0], StringData.InputSignals[0], false, 0);
                }
                this._joinData.Rows.Add("In", 0, StringData.InputSignals[0], StringData.InputSignals[0], StringData.Join[0], StringData.InputSignals[0], false, 0);
                List<string> emptyValues = new List<string>{"-"};

                this._joinData.Rows[5].Cells[4].ReadOnly = true;
                this._joinData.Rows[5].Cells[4].Style.BackColor = SystemColors.Control;

                (this._joinData.Rows[5].Cells[4] as DataGridViewComboBoxCell).DataSource = emptyValues;
                this._joinData.Rows[5].Cells[4].Value = "-";
                this._joinData.Rows[5].Cells[5].ReadOnly = true;
                this._joinData.Rows[5].Cells[5].Style.BackColor = SystemColors.Control;
                (this._joinData.Rows[5].Cells[5] as DataGridViewComboBoxCell).DataSource = emptyValues;
                this._joinData.Rows[5].Cells[5].Value = "-";
                this._joinData.Rows[5].Cells[6].ReadOnly = true;
                this._joinData.Rows[5].Cells[6].Style.BackColor = SystemColors.Control;
                this._joinData.Rows[5].Cells[7].ReadOnly = true;
                this._joinData.Rows[5].Cells[7].Style.BackColor = SystemColors.Control;
            }
        }

        public void ReadJoinGrid()
        {
            for (int i = 0; i < 6; i++)
            {
                ConnectionStruct connection = this._currentConfig.Conn.ConnMain[i];
                
                this._joinData.Rows[i].Cells["_joinITT"].Value = connection.Inom;
                this._joinData.Rows[i].Cells["_joinSwitchOFF"].Value = connection.JoinSwitchoff;
                this._joinData.Rows[i].Cells["_joinSwitchOn"].Value = connection.JoinSwitchon;

                if (i != 5)
                {
                    this._joinData.Rows[i].Cells["_joinJoin"].Value = connection.JoinJoin;
                    this._joinData.Rows[i].Cells["_joinEnter"].Value = connection.JoinEnter;
                    this._joinData.Rows[i].Cells["_joinResetColumn"].Value = connection.ResetJoin;
                    this._joinData.Rows[i].Cells["_timeResetJoinColumn"].Value = connection.ResetDeley;
                }
            }
        }

        public void WriteJoinGrid()
        {
                AllConnectionsStruct allConnections = new AllConnectionsStruct();
                for (int i = 0; i < 6; i++)
                {
                    var connection = new ConnectionStruct();
                    connection.Inom = Convert.ToUInt16(this._joinData.Rows[i].Cells["_joinITT"].Value);
                    connection.JoinSwitchoff = this._joinData.Rows[i].Cells["_joinSwitchOFF"].Value.ToString();
                    connection.JoinSwitchon = this._joinData.Rows[i].Cells["_joinSwitchOn"].Value.ToString();



                    if (i != 5)
                    {
                        connection.JoinJoin = this._joinData.Rows[i].Cells["_joinJoin"].Value.ToString();
                        connection.JoinEnter = this._joinData.Rows[i].Cells["_joinEnter"].Value.ToString();
                        connection.ResetJoin =Convert.ToBoolean(this._joinData.Rows[i].Cells["_joinResetColumn"].Value);
                        connection .ResetDeley=int.Parse(this._joinData.Rows[i].Cells["_timeResetJoinColumn"].Value.ToString());

                        
                    }
                    allConnections.ConnMain[i] = connection;
                }
                this._currentConfig.Conn= allConnections;
        }
        #endregion

        #region Реле и индикаторы
        private void PrepareParamautomat()
        {
            this._outputReleGrid.Rows.Clear();
            this._outputIndicatorsGrid.Rows.Clear();
            this._releSignalCol.Items.Clear();
            this._releTypeCol.Items.Clear();
            this._outIndSignalCol.Items.Clear();
            this._outIndTypeCol.Items.Clear();

            this._releSignalCol.DataSource = StringData.SignalSrab;
            this._releTypeCol.DataSource =StringData.SignalType;
            this._outIndSignalCol.DataSource =StringData.SignalSrab;
            this._outIndTypeCol.DataSource =StringData.SignalType;

            for (int i = 0; i < Mr902.RELE_COUNT; i++)
            {
                this._outputReleGrid.Rows.Add(i + 1, "Повторитель", "Нет", 0);
            }
            this._outputIndicatorsGrid.EditingControlShowing += this.DataGrid_EditingControlShowing;
            this._outputReleGrid.EditingControlShowing += this.DataGrid_EditingControlShowing;
            this._outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < Mr902.INDICATOR_COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(i + 1, "Повторитель", "Нет", "");
                this._outputIndicatorsGrid.Rows[i].Cells["_outIndColorCol"].Style.BackColor = Color.Red;
            }

            this._neispr1CB.SelectedIndex = 0;
            this._neispr2CB.SelectedIndex = 0;
            this._neispr3CB.SelectedIndex = 0;
            this._neispr4CB.SelectedIndex = 0;
            this._impTB.Text = "0";
        }

        private void ReadPARAMAUTOMAT()
        {
            for (int i = 0; i < Mr902.RELE_COUNT; i++)
            {
                this._outputReleGrid[2, i].Value = this._currentConfig.ParamAutomat.RelayStruct[i].Signal;
                this._outputReleGrid[1, i].Value = this._currentConfig.ParamAutomat.RelayStruct[i].Type;
                this._outputReleGrid[3, i].Value = this._currentConfig.ParamAutomat.RelayStruct[i].Wait;
            }

            for (int i = 0; i < Mr902.INDICATOR_COUNT; i++)
            {
                this._outputIndicatorsGrid[1, i].Value = this._currentConfig.ParamAutomat.IndicatorStruct[i].Type;
                this._outputIndicatorsGrid[2, i].Value = this._currentConfig.ParamAutomat.IndicatorStruct[i].Signal;
                this._outputIndicatorsGrid[3, i].Style.BackColor = this._currentConfig.ParamAutomat.IndicatorStruct[i].Color ? Color.Green : Color.Red;
                this._outputIndicatorsGrid[3, i].Style.SelectionBackColor = this._outputIndicatorsGrid[3, i].Style.BackColor;
            }
            this._neispr1CB.SelectedItem = this._currentConfig.ParamAutomat.OutputN1;
            this._neispr2CB.SelectedItem = this._currentConfig.ParamAutomat.OutputN2;
            this._neispr3CB.SelectedItem = this._currentConfig.ParamAutomat.OutputN3;
            this._neispr4CB.SelectedItem = this._currentConfig.ParamAutomat.OutputN4;
            this._impTB.Text = this._currentConfig.ParamAutomat.OutputImp.ToString();
        }

        private void WriteParamAutomat()
        {
            ParamAutomatStruct str = new ParamAutomatStruct
            {
                OutputN1 = this._neispr1CB.SelectedItem.ToString(),
                OutputN2 = this._neispr2CB.SelectedItem.ToString(),
                OutputN3 = this._neispr3CB.SelectedItem.ToString(),
                OutputN4 = this._neispr4CB.SelectedItem.ToString(),
                OutputImp = Convert.ToInt32(this._impTB.Text)
            };

            RelayStruct[] relays = new RelayStruct[Mr902.RELE_COUNT];
            for (int i = 0; i < Mr902.RELE_COUNT; i++)
            {
                relays[i] = new RelayStruct
                {
                    Signal = this._outputReleGrid[2, i].Value.ToString(),
                    Type = this._outputReleGrid[1, i].Value.ToString(),
                    Wait = Convert.ToInt32(this._outputReleGrid[3, i].Value)
                };
            }
            str.RelayStruct = relays;
            IndicatorStruct[] indicators = new IndicatorStruct[Mr902.INDICATOR_COUNT];
            for (int i = 0; i < Mr902.INDICATOR_COUNT; i++)
            {
                indicators[i] = new IndicatorStruct
                {
                    Signal = this._outputIndicatorsGrid[2, i].Value.ToString(),
                    Type = this._outputIndicatorsGrid[1, i].Value.ToString(),
                    Color = this._outputIndicatorsGrid[3, i].Style.BackColor == Color.Green
                };
            }
            str.IndicatorStruct = indicators;
            this._currentConfig.ParamAutomat = str;
        }

        void _outputIndicatorsGrid_Click(object sender, EventArgs e)
        {
            if (this._outputIndicatorsGrid.CurrentCell.ColumnIndex == 3)
            {
                if (this._outputIndicatorsGrid.CurrentCell.Style.BackColor == Color.Red)
                {
                    this._outputIndicatorsGrid.CurrentCell.Style.BackColor = Color.Green;
                    this._outputIndicatorsGrid.CurrentCell.Style.SelectionBackColor = this._outputIndicatorsGrid.CurrentCell.Style.BackColor;
                }
                else
                {
                    this._outputIndicatorsGrid.CurrentCell.Style.BackColor = Color.Red;
                    this._outputIndicatorsGrid.CurrentCell.Style.SelectionBackColor = this._outputIndicatorsGrid.CurrentCell.Style.BackColor;
                }
            }
        }
        #endregion

        #region ВЛС
        public void PrepareElssygnal()
        {
            this._vlsChecBoxListArray = new[]
            {
                this.VLScheckedListBox1, this.VLScheckedListBox2, this.VLScheckedListBox3, this.VLScheckedListBox4,
                this.VLScheckedListBox5, this.VLScheckedListBox6, this.VLScheckedListBox7, this.VLScheckedListBox8,
                this.VLScheckedListBox9, this.VLScheckedListBox10, this.VLScheckedListBox11, this.VLScheckedListBox12,
                this.VLScheckedListBox13, this.VLScheckedListBox14, this.VLScheckedListBox15, this.VLScheckedListBox16
            };

            foreach (CheckedListBox checkedList in this._vlsChecBoxListArray)
            {
                checkedList.DataSource = StringData.VLSSignals;
            }
        }

        public void WriteElssygnal()
        {
            ushort[] vls = new ushort[AllOutputSignalsStruct.COUNT * OutputSignalStruct.COUNT];
            for (int i = 0; i < AllOutputSignalsStruct.COUNT; i++) //Счетчик закладок ВЛС 1-16
            {
                int byteIndex = OutputSignalStruct.COUNT * i;

                for (int j = 0; j < StringData.VLSSignals.Count; j += AllOutputSignalsStruct.COUNT)
                    //Счетчик элементов одного ВЛС 1-125
                {
                    int itemIndex = j; //индекс элемента списка
                    for (int bitIndex = 0; bitIndex < 16; bitIndex++) //перебираем биты слова
                    {
                        if (itemIndex < (StringData.VLSSignals.Count /*- 1*/))
                            //Чтоб индекс элемента списка не зашкалил за длинну списка
                        {
                            vls[byteIndex] = Common.SetBit(vls[byteIndex], bitIndex,
                                this._vlsChecBoxListArray[i].GetItemChecked(itemIndex));
                            itemIndex++; //переходим к сл. элементу списка
                        }
                    }
                    byteIndex++; //переходим к следующему байту массива данного ВЛС
                }
            }
            AllOutputSignalsStruct signals = new AllOutputSignalsStruct {VLS = vls};
            this._currentConfig.Els = signals;
        }

        public void ReadElssygnal()
        {
            ushort[] vls = this._currentConfig.Els.VLS;
            for (int i = 0; i < AllOutputSignalsStruct.COUNT; i++) //Счетчик закладок ВЛС 1-16
            {
                int byteIndex = 18 * i;

                for (int j = 0; j < StringData.VLSSignals.Count; j += AllOutputSignalsStruct.COUNT) //Счетчик элементов одного ВЛС 1-125
                {
                    int itemIndex = j; //индекс элемента списка
                    for (int bitIndex = 0; bitIndex < 16; bitIndex++)//перебираем биты слова
                    {
                        if (itemIndex < (StringData.VLSSignals.Count /*- 1*/))//Чтоб индекс элемента списка не зашкалил за длинну списка
                        {
                            this._vlsChecBoxListArray[i].SetItemChecked(itemIndex, Common.GetBit(vls[byteIndex], bitIndex));
                            itemIndex++;//переходим к сл. элементу списка
                        }
                    }
                    byteIndex++;//переходим к следующему байту массива данного ВЛС
                }
            }
        }
        #endregion

        #region И Или
        public void PrepareINPSIGNAL()
        {
            this._inpSignalsArray = new[]
            {
                this._inputSignals1, this._inputSignals2, this._inputSignals3, this._inputSignals4,
                this._inputSignals5, this._inputSignals6, this._inputSignals7, this._inputSignals8,
                this._inputSignals9, this._inputSignals10, this._inputSignals11, this._inputSignals12,
                this._inputSignals13, this._inputSignals14, this._inputSignals15, this._inputSignals16
            };

            foreach (DataGridView dgv in this._inpSignalsArray)
            {
                if (dgv.Rows.Count == 0)
                {
                    for (int i = 0; i < StringData.LogicSignals.Count; i++)
                    {
                        this._signalValueCol.DataSource = StringData.LogicValues;
                        dgv.Rows.Add("Д" + (i + 1), StringData.LogicValues[0]);
                    }
                }
            }
        }

        public void ReadInpsignal()
        {
            ushort[] inpSignals = this._currentConfig.Inp.InpSignals;
            for (int j = 0; j < this._inpSignalsArray.Length; j++)
            {
                int inpSignalsIndex = j * 4;
                int indexLs = 0;
                do
                {
                    for (int i = 0; i < this._inpSignalsArray.Length; i += 2)
                    {
                        int IND = Common.GetBits(inpSignals[inpSignalsIndex], i, i + 1) >> i;
                        this._inpSignalsArray[j][1, indexLs].Value = StringData.LogicValues[IND];
                        indexLs++;
                    }
                    inpSignalsIndex++;
                } while (indexLs < StringData.LogicSignals.Count);
            }
        }

        public void WriteInpsignal()
        {
            ushort[] inpSignals = new ushort[12*16/2];
            for (int j = 0; j < this._inpSignalsArray.Length; j++)
            {
                int inpSignalsIndex = j*4;
                int indexLs = 0;
                do
                {
                    for (int i = 0; i < this._inpSignalsArray.Length; i += 2)
                    {
                        inpSignals[inpSignalsIndex] = Common.SetBits(inpSignals[inpSignalsIndex],
                            (ushort) StringData.LogicValues.IndexOf(this._inpSignalsArray[j][1, indexLs].Value.ToString()),
                            i, i + 1);
                        indexLs++;
                    }
                    inpSignalsIndex++;
                } 
                while (indexLs < StringData.LogicSignals.Count);
            }
            AllInputSignalsStruct allInputSignals = new AllInputSignalsStruct();
            allInputSignals.InpSignals = inpSignals;

            this._currentConfig.Inp = allInputSignals;
        }

        #endregion

        #region Диф защиты действующие
        public void PrepareDifDGrid()
        {
            this.FillDifDGrid();
            List<string> names = new List<string>();
            names.Add("Iд1 СШ1");
            names.Add("Iд2 СШ2");
            names.Add("Iд3 ПО");
            if (this._difDDataGrid.Rows.Count == 0)
            {
                for (int i = 0; i < 3; i++)
                {
                    this._difDDataGrid.Rows.Add(names[i], StringData.ModesLightMode[0], StringData.InputSignals[0],
                        0, 0, 0, 0, 0, StringData.YesNo[0], 0, StringData.YesNo[0], 0, StringData.YesNo[0], 
                        StringData.YesNo[0], 0, 0, StringData.InputSignals[0], StringData.ModesLightOsc[0], StringData.ModesLight[0]);
                }
            }
        }

        public void FillDifDGrid()
        {
            this._difDModesColumn.DataSource = StringData.ModesLightMode;
            this._difDBlockColumn.DataSource = StringData.InputSignals;
            this._difDBlockGColumn.DataSource = StringData.YesNo;
            this._difDBlock5GColumn.DataSource = StringData.YesNo;
            this._difDOprNasColumn.DataSource = StringData.YesNo;
            this._difDOchColumn.DataSource = StringData.YesNo;
            this._difDEnterOchColumn.DataSource = StringData.InputSignals;
            this._difDOscColumn.DataSource = StringData.ModesLightOsc;
            this._difDUrovColumn.DataSource = StringData.ModesLight;
        }

        public void ReadDifDGrid(DefenseStruct defense)
        {
            for (int i = 0; i < defense.DiffDstv.Length; i++)
            {
                var def = defense.DiffDstv[i];
                this._difDDataGrid.Rows[i].Cells["_difDModesColumn"].Value = def.DIFD_MODE;
                this._difDDataGrid.Rows[i].Cells["_difDBlockColumn"].Value = def.DIFD_BLOCK;
                this._difDDataGrid.Rows[i].Cells["_difDIcpColumn"].Value = def.DIFD_ICP;
                this._difDDataGrid.Rows[i].Cells["_difDIdoColumn"].Value = def.DIFD_IDO;
                this._difDDataGrid.Rows[i].Cells["_difDtcpColumn"].Value = def.DIFD_TCP;
                this._difDDataGrid.Rows[i].Cells["_difDIbColumn"].Value = def.DIFD_IB;
                this._difDDataGrid.Rows[i].Cells["_difDfColumn"].Value = def.DIFD_F;
                this._difDDataGrid.Rows[i].Cells["_difDBlockGColumn"].Value = def.DIFD_BLOCK_G;
                this._difDDataGrid.Rows[i].Cells["_difDI2gColumn"].Value = def.DIFD_I2G;

                this._difDDataGrid.Rows[i].Cells["_difDBlock5GColumn"].Value = def.DIFD_BLOCK_5G;
                this._difDDataGrid.Rows[i].Cells["_difDI5gColumn"].Value = def.DIFD_I5G;
                this._difDDataGrid.Rows[i].Cells["_difDOprNasColumn"].Value = def.DIFD_BLOCK_OPR_NAS;

                this._difDDataGrid.Rows[i].Cells["_difDOchColumn"].Value = def.DIFD_OCH;
                this._difDDataGrid.Rows[i].Cells["_difDtochColumn"].Value = def.DIFD_T_OCH;
                this._difDDataGrid.Rows[i].Cells["_difDEnterOchColumn"].Value = def.DIFD_ENTER_OCH;
                this._difDDataGrid.Rows[i].Cells["_difDIochColumn"].Value = def.DIFD_I_OCH;
                this._difDDataGrid.Rows[i].Cells["_difDOscColumn"].Value = def.DIFD_OSC;
                this._difDDataGrid.Rows[i].Cells["_difDUrovColumn"].Value = def.DIFD_UROV;
                //Пуск от По
                if (i == 0)
                    this._actualRunForPoId1.Checked = def.DefenceActualRunForPo;
                if (i == 1)
                    this._actualRunForPoId2.Checked = def.DefenceActualRunForPo;
            }
        }

        #endregion

        #region Диф защиты мгновенные
        public void PrepareDifMGrid()
        {
            this.FillDifMGrid();
            List<string> names = new List<string>
                {
                    "Iд1м СШ1",
                    "Iд2м СШ2",
                    "Iд3м ПО"
                };
            if (this._difMDataGrid.Rows.Count == 0)
            {
                for (int i = 0; i < 3; i++)
                {
                    this._difMDataGrid.Rows.Add(names[i], StringData.ModesLightMode[0], StringData.InputSignals[0],
                        0, 0, 0, 0, StringData.YesNo[0], 0, 0, StringData.InputSignals[0], 
                        StringData.ModesLightOsc[0], StringData.ModesLight[0]);
                }
            }
        }

        public void FillDifMGrid()
        {
            this._difMModesColumn.DataSource = StringData.ModesLightMode;
            this._difMBlockColumn.DataSource = StringData.InputSignals;
            this._difMOchColumn.DataSource = StringData.YesNo;
            this._difMEnterOchColumn.DataSource = StringData.InputSignals;
            this._difMOscColumn.DataSource = StringData.ModesLightOsc;
            this._difMUrovColumn.DataSource = StringData.ModesLight;
        }

        public void ReadDifMGrid(DefenseStruct defense)
        {
            for (int i = 0; i < defense.DiffMgn.Length; i++)
            {
                var def = defense.DiffMgn[i];
                this._difMDataGrid.Rows[i].Cells["_difMModesColumn"].Value = def.DIFM_MODE;
                this._difMDataGrid.Rows[i].Cells["_difMBlockColumn"].Value = def.DIFM_BLOCK;
                this._difMDataGrid.Rows[i].Cells["_difMIcpColumn"].Value = def.DIFM_ICP;
                this._difMDataGrid.Rows[i].Cells["_difMIdoColumn"].Value = def.DIFM_IDO;
                this._difMDataGrid.Rows[i].Cells["_difMIbColumn"].Value = def.DIFM_IB;
                this._difMDataGrid.Rows[i].Cells["_difMfColumn"].Value = def.DIFM_F;
                this._difMDataGrid.Rows[i].Cells["_difMOchColumn"].Value = def.DIFM_OCH;
                this._difMDataGrid.Rows[i].Cells["_difMtochColumn"].Value = def.DIFM_T_OCH;
                this._difMDataGrid.Rows[i].Cells["_difMEnterOchColumn"].Value = def.DIFM_ENTER_OCH;
                this._difMDataGrid.Rows[i].Cells["_difMIochColumn"].Value = def.DIFM_I_OCH;
                this._difMDataGrid.Rows[i].Cells["_difMOscColumn"].Value = def.DIFM_OSC;
                this._difMDataGrid.Rows[i].Cells["_difMUrovColumn"].Value = def.DIFM_UROV;
                //Пуск от По
                if (i == 0)
                    this._instantaneousRunForPoId1.Checked = def.DefenceInstantaneousRunForPo;
                if (i == 1)
                    this._instantaneousRunForPoId2.Checked = def.DefenceInstantaneousRunForPo;
            }
        }
        #endregion

        #region МТЗ
        public void PrepareMtzDifensesGrid()
        {
            this._mtzModesColumn.DataSource = StringData.ModesLightMode;
            this._mtzBlockingColumn.DataSource = StringData.InputSignals;
            this._mtzMeasureColumn.DataSource = StringData.MTZJoin;
            this._mtzCharColumn.DataSource = StringData.Characteristic;
            this._logicImaxColumn.DataSource = StringData.TokParameter;
            this._mtzOscColumn.DataSource = StringData.ModesLightOsc;
            this._mtzUROVColumn.DataSource = StringData.ModesLight;

            if (this._MTZDifensesDataGrid.Rows.Count == 0)
            {
                for (int i = 0; i < 32; i++)
                {
                    this._MTZDifensesDataGrid.Rows.Add("Ступень I> " + (i+1), StringData.ModesLightMode[0],
                        StringData.InputSignals[0], StringData.TokParameter[0], StringData.MTZJoin[0], 0, 
                        StringData.Characteristic[0], 0, 0, StringData.ModesLightOsc[0], StringData.ModesLight[0]);
                }
            }
        }
        
        public void ReadMtzGrid(DefenseStruct defense)
        {
            for (int i = 0; i < defense.MtzDefStruct.Length; i++)
            {
                var def = defense.MtzDefStruct[i];
                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzModesColumn"].Value = def.MTZ_MODE;
                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzBlockingColumn"].Value = def.MTZ_BLOCK;
                this._MTZDifensesDataGrid.Rows[i].Cells["_logicImaxColumn"].Value = def.Logic;

                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzMeasureColumn"].Value = def.MTZ_MEASURE;
                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzICPColumn"].Value = def.MTZ_ICP;
                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzCharColumn"].Value = def.MTZ_CHAR;
                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzTColumn"].Value = def.MTZ_T;
                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzkColumn"].Value = def.MTZ_K;
                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzOscColumn"].Value = def.MTZ_OSC;
                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzUROVColumn"].Value = def.MTZ_UROV;
            }
        }
        #endregion

        #region Внешние
        public void PrepareExternalDifensesGrid()
        {
            this.FillExternalDifensesGrid();
            if (this._externalDifensesDataGrid.Rows.Count == 0)
            {
                for (int i = 0; i < 16; i++)
                {
                    this._externalDifensesDataGrid.Rows.Add("Внешняя " + (i+1), StringData.ModesLightMode[0], 
                        StringData.Otkl[0], StringData.SignalSrabExternal[0], StringData.SignalSrabExternal[0], 0, 0,
                        StringData.SignalSrabExternal[0], StringData.YesNo[0], StringData.ModesLightOsc[0], StringData.ModesLight[0]);
                }
            }
        }

        public void FillExternalDifensesGrid()
        {
            this._externalDifModesColumn.DataSource = StringData.ModesLightMode;
            this._externalDifOtklColumn.DataSource = StringData.Otkl;
            this._externalDifBlockingColumn.DataSource = StringData.SignalSrabExternal;
            this._externalDifSrabColumn.DataSource = StringData.SignalSrabExternal;
            this._externalDifVozvrColumn.DataSource = StringData.SignalSrabExternal;
            this._externalDifVozvrYNColumn.DataSource = StringData.YesNo;
            this._externalDifOscColumn.DataSource = StringData.ModesLightOsc;
            this._externalDifUROVColumn.DataSource = StringData.ModesLight;
        }

        public void ReadExternalGrid(DefenseStruct defense)
        {
            for (int i = 0; i < defense.MtzExt.Length; i++)
            {
                var def = defense.MtzExt[i];
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifModesColumn"].Value = def.EXTERNAL_MODE;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifOtklColumn"].Value = def.EXTERNAL_OTKL;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifBlockingColumn"].Value = def.EXTERNAL_BLOCK;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifSrabColumn"].Value = def.EXTERNAL_SRAB;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifTsrColumn"].Value = def.EXTERNAL_TSR;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifTvzColumn"].Value = def.EXTERNAL_TVZ;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifVozvrColumn"].Value = def.EXTERNAL_VOZVR;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifVozvrYNColumn"].Value = def.EXTERNAL_VOZVR_YN;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifOscColumn"].Value = def.EXTERNAL_OSC;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifUROVColumn"].Value = def.EXTERNAL_UROV;
            }
        }
        #endregion
        
        #region [Контроль цепей ТТ]
        public void PrepareTtGrid()
        {
            this._faultTtColumn.DataSource = StringData.TtFault;
            if (this._configTtDgv.Rows.Count != 0) return;

            for (int i = 0; i < 3; i++)
            {
                this._configTtDgv.Rows.Add
                    (
                        StringData.TtNames[i],
                        0,
                        0,
                        StringData.TtFault[0]
                    );
            }
        }

        public void ReadTtGrid()
        {
            this._configTtDgv.Rows.Clear();

            for (int i = 0;  i < 3; i++)
            {

                this._configTtDgv.Rows.Add
                    (
                        StringData.TtNames[i],
                        this._currentConfig.Ctt.TtChannels[i].Idmin,
                        this._currentConfig.Ctt.TtChannels[i].Tsrab,
                        this._currentConfig.Ctt.TtChannels[i].Fault
                    );
            }
        }

        public void WriteTtGrid()
        {
            AllConfigTtStruct str = new AllConfigTtStruct();
            for (int i = 0; i < 3; i++)
            {
                str.TtChannels[i].Idmin = Convert.ToDouble(this._configTtDgv[1, i].Value);
                str.TtChannels[i].Tsrab = Convert.ToInt32(this._configTtDgv[2, i].Value);
                str.TtChannels[i].Fault = this._configTtDgv[3, i].Value.ToString();
            }
            this._currentConfig.Ctt = str;
        }

        #endregion [Контроль цепей ТТ]

        public void PrepareCorrentProtAll()
        {
            this.PrepareDifDGrid();
            this.PrepareDifMGrid();
            this.PrepareMtzDifensesGrid();
            this.PrepareExternalDifensesGrid();
        }

        public void ReadCurrentProtMain(DefenseStruct defense)
        {
            this.ReadDifDGrid(defense);
            this.ReadDifMGrid(defense);
            this.ReadMtzGrid(defense);
            this.ReadExternalGrid(defense);
        }
        
        public DefenseStruct WriteCurrentProtMain()
        {
            DefenseStruct defense = new DefenseStruct();

            #region [Действующие]
            for (int i = 0; i < defense.DiffDstv.Length; i++)
            {
                DifDefStruct def = new DifDefStruct();
                def.DIFD_MODE = this._difDDataGrid.Rows[i].Cells["_difDModesColumn"].Value.ToString();
                def.DIFD_BLOCK = this._difDDataGrid.Rows[i].Cells["_difDBlockColumn"].Value.ToString();
                def.DIFD_ICP = Convert.ToDouble(this._difDDataGrid.Rows[i].Cells["_difDIcpColumn"].Value);
                def.DIFD_IDO = Convert.ToDouble(this._difDDataGrid.Rows[i].Cells["_difDIdoColumn"].Value);
                def.DIFD_TCP = Convert.ToInt32(this._difDDataGrid.Rows[i].Cells["_difDtcpColumn"].Value);
                def.DIFD_IB = Convert.ToDouble(this._difDDataGrid.Rows[i].Cells["_difDIbColumn"].Value);
                def.DIFD_F = Convert.ToUInt16(this._difDDataGrid.Rows[i].Cells["_difDfColumn"].Value);
                def.DIFD_BLOCK_G = this._difDDataGrid.Rows[i].Cells["_difDBlockGColumn"].Value.ToString();
                def.DIFD_I2G = Convert.ToUInt16(this._difDDataGrid.Rows[i].Cells["_difDI2gColumn"].Value);

                def.DIFD_BLOCK_5G = this._difDDataGrid.Rows[i].Cells["_difDBlock5GColumn"].Value.ToString();
                def.DIFD_I5G = Convert.ToUInt16(this._difDDataGrid.Rows[i].Cells["_difDI5gColumn"].Value);
                def.DIFD_BLOCK_OPR_NAS = this._difDDataGrid.Rows[i].Cells["_difDOprNasColumn"].Value.ToString();

                def.DIFD_OCH = this._difDDataGrid.Rows[i].Cells["_difDOchColumn"].Value.ToString();
                def.DIFD_T_OCH = Convert.ToInt32(this._difDDataGrid.Rows[i].Cells["_difDtochColumn"].Value);
                def.DIFD_ENTER_OCH = this._difDDataGrid.Rows[i].Cells["_difDEnterOchColumn"].Value.ToString();
                def.DIFD_I_OCH = Convert.ToDouble(this._difDDataGrid.Rows[i].Cells["_difDIochColumn"].Value);
                def.DIFD_OSC = this._difDDataGrid.Rows[i].Cells["_difDOscColumn"].Value.ToString();
                def.DIFD_UROV = this._difDDataGrid.Rows[i].Cells["_difDUrovColumn"].Value.ToString();
                //Пуск от По
                if (i == 0)
                    def.DefenceActualRunForPo = this._actualRunForPoId1.Checked;
                if (i == 1)
                    def.DefenceActualRunForPo = this._actualRunForPoId2.Checked;

                defense.DiffDstv[i] = def;
            } 
            #endregion [Действующие]
            
            #region [Мгновенные]
            for (int i = 0; i < defense.DiffMgn.Length; i++)
            {
                DifDefStruct def = new DifDefStruct();
                def.DIFM_MODE = this._difMDataGrid.Rows[i].Cells["_difMModesColumn"].Value.ToString();
                def.DIFM_BLOCK = this._difMDataGrid.Rows[i].Cells["_difMBlockColumn"].Value.ToString();
                def.DIFM_ICP = Convert.ToDouble(this._difMDataGrid.Rows[i].Cells["_difMIcpColumn"].Value);
                def.DIFM_IDO = Convert.ToDouble(this._difMDataGrid.Rows[i].Cells["_difMIdoColumn"].Value);
                def.DIFM_IB = Convert.ToDouble(this._difMDataGrid.Rows[i].Cells["_difMIbColumn"].Value);
                def.DIFM_F = Convert.ToUInt16(this._difMDataGrid.Rows[i].Cells["_difMfColumn"].Value);
                def.DIFM_OCH = this._difMDataGrid.Rows[i].Cells["_difMOchColumn"].Value.ToString();
                def.DIFM_T_OCH = Convert.ToInt32(this._difMDataGrid.Rows[i].Cells["_difMtochColumn"].Value);
                def.DIFM_ENTER_OCH = this._difMDataGrid.Rows[i].Cells["_difMEnterOchColumn"].Value.ToString();
                def.DIFM_I_OCH = Convert.ToDouble(this._difMDataGrid.Rows[i].Cells["_difMIochColumn"].Value);
                def.DIFM_OSC = this._difMDataGrid.Rows[i].Cells["_difMOscColumn"].Value.ToString();
                def.DIFM_UROV = this._difMDataGrid.Rows[i].Cells["_difMUrovColumn"].Value.ToString();
                //Пуск от По
                if (i == 0)
                    def.DefenceInstantaneousRunForPo = this._instantaneousRunForPoId1.Checked;
                if (i == 1)
                    def.DefenceInstantaneousRunForPo = this._instantaneousRunForPoId2.Checked;
                defense.DiffMgn[i] = def;
            } 
            #endregion [Мгновенные]
            
            #region [МТЗ]
            for (int i = 0; i < defense.MtzDefStruct.Length; i++)
            {
                MtzDefStruct def = new MtzDefStruct();
                def.MTZ_MODE = this._MTZDifensesDataGrid.Rows[i].Cells["_mtzModesColumn"].Value.ToString();
                def.MTZ_BLOCK = this._MTZDifensesDataGrid.Rows[i].Cells["_mtzBlockingColumn"].Value.ToString();
                def.MTZ_MEASURE = this._MTZDifensesDataGrid.Rows[i].Cells["_mtzMeasureColumn"].Value.ToString();
                def.MTZ_ICP = Convert.ToDouble(this._MTZDifensesDataGrid.Rows[i].Cells["_mtzICPColumn"].Value);
                def.Logic   =  this._MTZDifensesDataGrid.Rows[i].Cells["_logicImaxColumn"].Value.ToString() ;
                def.MTZ_CHAR = this._MTZDifensesDataGrid.Rows[i].Cells["_mtzCharColumn"].Value.ToString();
                def.MTZ_T = Convert.ToInt32(this._MTZDifensesDataGrid.Rows[i].Cells["_mtzTColumn"].Value);
                def.MTZ_K = Convert.ToUInt16(this._MTZDifensesDataGrid.Rows[i].Cells["_mtzkColumn"].Value);
                def.MTZ_OSC = this._MTZDifensesDataGrid.Rows[i].Cells["_mtzOscColumn"].Value.ToString();
                def.MTZ_UROV = this._MTZDifensesDataGrid.Rows[i].Cells["_mtzUROVColumn"].Value.ToString();
                defense.MtzDefStruct[i] = def;
            } 
            #endregion [МТЗ]

            #region [Внешние]
            for (int i = 0; i < defense.MtzExt.Length; i++)
            {
                ExtDefStruct def = new ExtDefStruct();
                def.EXTERNAL_MODE = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifModesColumn"].Value.ToString();
                def.EXTERNAL_SRAB = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifSrabColumn"].Value.ToString();
                def.EXTERNAL_OTKL = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifOtklColumn"].Value.ToString();
                def.EXTERNAL_BLOCK = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifBlockingColumn"].Value.ToString();
                def.EXTERNAL_TSR = Convert.ToInt32(this._externalDifensesDataGrid.Rows[i].Cells["_externalDifTsrColumn"].Value);
                def.EXTERNAL_TVZ = Convert.ToInt32(this._externalDifensesDataGrid.Rows[i].Cells["_externalDifTvzColumn"].Value);
                def.EXTERNAL_VOZVR = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifVozvrColumn"].Value.ToString();
                def.EXTERNAL_VOZVR_YN = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifVozvrYNColumn"].Value.ToString();
                def.EXTERNAL_OSC = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifOscColumn"].Value.ToString();
                def.EXTERNAL_UROV = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifUROVColumn"].Value.ToString();

                defense.MtzExt[i] = def;
            }
            #endregion

            return defense;
        }

        #region Группа уставок и Сброс индикации
        public void ReadInputsygnal()
        {
            this._grUstComboBox.Text = this._currentConfig.Impsg.GrUst;
            this._indComboBox.Text = this._currentConfig.Impsg.SbInd;
        }

        public void WriteInputsygnal()
        {
            InputSignalsConfigStruct input = new InputSignalsConfigStruct();
            input.GrUst = this._grUstComboBox.SelectedItem.ToString();
            input.SbInd = this._indComboBox.SelectedItem.ToString();
            this._currentConfig.Impsg = input;
        }

        #endregion

        #region Обработчики событий

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled = this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }

        private void Configuration_Load(object sender, EventArgs e)
        {
            if(Device.AutoloadConfig)
                this.LoadConfigurationBlocks();
        }

        private void AllWriteEndFail()
        {
            this._configProgressBar.Value = this._configProgressBar.Maximum;
            this._processLabel.Text = WRITE_FAIL;
            MessageBox.Show(WRITE_FAIL);
            this.IsProcess = false;
        }

        private void AllWriteEnd()
        {
            this._readConfigBut.Enabled = true;
            this._processLabel.Text = WRITE_OK;
            MessageBox.Show(WRITE_OK);
            this.IsProcess = false;
        }

        private void AllReadEndFail()
        {
            this._configProgressBar.Value = this._configProgressBar.Maximum;
            this._processLabel.Text = READ_FAIL;
            MessageBox.Show(READ_FAIL);
            this.IsProcess = false;
        }

        private void AllReadEnd()
        {
            this._processLabel.Text = READ_OK;
            this._currentConfig = this._configuration.Value;
            this.ShowConfiguration();
            MessageBox.Show(READ_OK);
            this.IsProcess = false;
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.LoadConfigurationBlocks();
        }

        private void Combo_DropDown(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            combo.DropDown -= this.Combo_DropDown;
            if (combo.Items.Contains("ХХХХХ"))
            {
                combo.Items.Remove("ХХХХХ");
            }
        }

        private void Combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            combo.SelectedIndexChanged -= this.Combo_SelectedIndexChanged;
            if (combo.Items.Contains("ХХХХХ"))
            {
                combo.Items.Remove("ХХХХХ");
            }
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void DataGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                DataGridViewComboBoxEditingControl combo = e.Control as DataGridViewComboBoxEditingControl;
                if (combo.Items[combo.Items.Count - 1].ToString() == "ХХХХХ")
                {
                    combo.Items.RemoveAt(combo.Items.Count - 1);
                }
            }
        }

        private void _mainRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (this._mainRadioButton.Checked)
            {
                this._currentConfig.AllDefences.ReserveGroup = this.WriteCurrentProtMain();
                this.ReadCurrentProtMain(this._currentConfig.AllDefences.MainGroup);
                this._groupChangeButton.Text = "Основная -> Резервная";
            }
        }

        private void _reserveRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (this._reserveRadioButton.Checked)
            {
                this._currentConfig.AllDefences.MainGroup = this.WriteCurrentProtMain();
                this.ReadCurrentProtMain(this._currentConfig.AllDefences.ReserveGroup);
                this._groupChangeButton.Text = "Резервная -> Основная";
            }
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveinFile();
        }
        
        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void _joinData_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex != 7)
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        private void _MTZDifensesDataGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex != 7)
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        private void _externalDifensesDataGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if ((e.ColumnIndex != 6) & (e.ColumnIndex != 5))
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        private void _outputReleGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if ((e.ColumnIndex != 3))
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        private void _DZHTUrov1_Validating(object sender, CancelEventArgs e)
        {
            var tb = sender as MaskedTextBox;
            if (tb == null)
                return;

            string message;
            if (this.TimeValidate(tb.Text, out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        private void _difDDataGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if ((e.ColumnIndex != 5) & (e.ColumnIndex != 15))
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        private void _difMDataGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if ((e.ColumnIndex != 9))
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        private void _UROVJoinData_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if ((e.ColumnIndex != 2))
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        private void _oscLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            int osclen = Common.VersionConverter(this._device.DeviceVersion) < 2.0 ? 26168 * 2 : 55751 * 2;
            int index = this._oscLength.SelectedIndex;
            this._oscSizeTextBox.Text = (osclen / (index + 2)).ToString();
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtml();
        }

        private void SaveToHtml()
        {
            try
            {
                if (this.WriteConfiguration())
                {
                    this._currentConfig.DeviceVersion = Common.VersionConverter(this._device.DeviceVersion);
                    this._currentConfig.DeviceNumber = this._device.DeviceNumber.ToString();
                    this._processLabel.Text = HtmlExport.Export(Resources.MR902Main, Resources.MR902Res,
                        this._currentConfig, this._currentConfig.DeviceType, this._device.DeviceVersion);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения");
            }
        }
        private void Mr902ConfigurationForm_Activated(object sender, EventArgs e)
        {
            StringData.Version = Common.VersionConverter(this._device.DeviceVersion);
        }

        private void Mr902ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.LoadConfigurationBlocks();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.LoadConfigurationBlocks();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtml();
            }
        }
        #endregion

        #region Дополнительные функции

        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }

        private void LoadConfigurationBlocks()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.IsProcess = true;
            this._configProgressBar.Value = 0;
            this._processLabel.Text = "Идёт чтение";
            this._configuration.LoadStruct();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this.WriteConfiguration())
            {
                if (DialogResult.Yes ==
                    MessageBox.Show("Записать конфигурацию МР902 №" + this._device.DeviceNumber + " ?", "Внимание",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                {
                    this._configProgressBar.Value = 0;
                    this.IsProcess = true;
                    this._processLabel.Text = "Идет запись";
                    this._configuration.SaveStruct();
                }
            }
        }

        private void ClearCombos(ComboBox[] combos)
        {
            foreach (ComboBox c in combos)
            {
                c.Items.Clear();
            }
        }

        private void SubscriptCombos(ComboBox[] combos)
        {
            foreach (ComboBox c in combos)
            {
                c.DropDown += this.Combo_DropDown;
                c.SelectedIndexChanged += this.Combo_SelectedIndexChanged;
                c.SelectedIndex = 0;
            }
        }

        private bool WriteConfiguration()
        {
            try
            {
                this.WriteDZH();
                this.WriteALLOSC();
                this.WriteUrovJoinGrid();
                this.WriteJoinGrid();
                this.WriteTtGrid();
                this.WriteParamAutomat();
                this.WriteElssygnal();
                this.WriteInpsignal();
                this.WriteInputsygnal();

                if (this._mainRadioButton.Checked)
                {
                    this._currentConfig.AllDefences.MainGroup = this.WriteCurrentProtMain();
                }
                else
                {
                    this._currentConfig.AllDefences.ReserveGroup = this.WriteCurrentProtMain();
                }
                this._configuration.Value = this._currentConfig;
            }
            catch (Exception)
            {
                MessageBox.Show("Найдены неккоректные уставки");
                return false;
            }
            return true;
        }

        private void SaveinFile()
        {
            this._saveConfigurationDlg.FileName = string.Format("МР902_Уставки_версия {0:F1}.bin", this._device.DeviceVersion);
            if (this.WriteConfiguration())
            {
                if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
                {
                    this.Serialize(this._saveConfigurationDlg.FileName);
                }
            }
        }
        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MR902"));
                ushort[] values = this._currentConfig.GetValues();

                XmlElement element = doc.CreateElement("MR902_SET_POINTS");
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                MessageBox.Show(string.Format("Файл {0} успешно сохранён", binFileName));
            }
            catch
            {
                MessageBox.Show("Невозможно сохранить файл");
            }
        }

        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName);
            }
        }

        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);
                XmlNode a = doc.FirstChild.SelectSingleNode("MR902_SET_POINTS");
                if (a == null)
                    throw new NullReferenceException();
                byte[] values = Convert.FromBase64String(a.InnerText);
                if (this._currentConfig.GetSize() - this._currentConfig.NetConfSize < values.Length)
                {
                    List<byte> list = new List<byte>();
                    list.AddRange(values.Take(this._currentConfig.AboveNetSize));
                    list.AddRange(values.Skip(this._currentConfig.AboveNetSize+this._currentConfig.NetConfSize));
                    values = list.ToArray();
                }
                this._currentConfig.InitStruct(values);
                this.ShowConfiguration();
                MessageBox.Show(string.Format("Файл {0} успешно загружен", binFileName));
            }
            catch
            {
                MessageBox.Show("Невозможно загрузить файл");
            }
        }

        private bool TimeValidate(string time, out string message)
        {
            message = string.Empty;
            uint value;

            var flag = uint.TryParse(time, out value);

            if ((value > 3276700) | (!flag))
            {
                message = "Поле должно содержать положительное целое число <= 3276700";
                return false;
            }

            if (value == 0)
            {
                return true;
            }

            if (((value >= 1000000) & (value % 100 != 0)) | ((value < 1000000) & (value % 10 != 0)))
            {
                message = "Для значений меньше 1000000 значение должно быть кратно 10, для больших кратно 100";
                return false;
            }
            return true;
        }

        private void ShowConfiguration()
        {
            this.ReadOsc();
            this.ReadOSCCHANNELS();
            this.ReadDZH();
            this.ReadUrovJoinGrid();
            this.ReadJoinGrid();
            this.ReadInputsygnal();
            this.ReadInpsignal();
            this.ReadElssygnal();
            this.ReadTtGrid();
            if (this._mainRadioButton.Checked)
            {
                this.ReadCurrentProtMain(this._currentConfig.AllDefences.MainGroup);
            }
            if (this._reserveRadioButton.Checked)
            {
                this.ReadCurrentProtMain(this._currentConfig.AllDefences.ReserveGroup);
            }
            this.ReadPARAMAUTOMAT();
        }
        #endregion

        private void _groupChangeButton_Click(object sender, EventArgs e)
        {
            var res = MessageBox.Show("Вы уверены, что хотите копировать уставки?", _groupChangeButton.Text , MessageBoxButtons.YesNo);
            if (res == DialogResult.Yes)
            {
                if (this._mainRadioButton.Checked)
                {
                    this._currentConfig.AllDefences.ReserveGroup = this.WriteCurrentProtMain();
                }
                else
                {
                    this._currentConfig.AllDefences.MainGroup = this.WriteCurrentProtMain();
                }
            }
        }
    }
}
