﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR902.Old.Configuration.Structures.Second
{
    public class IndicatorStruct : StructBase
    {
        [Layout(0)] private ushort signal;
        [Layout(1)] private ushort type;

        public string Signal
        {
            get { return StringData.SignalSrab[this.signal]; }
            set { this.signal = (ushort) StringData.SignalSrab.IndexOf(value); }
        }

        public string Type
        {
            get
            {
                var index = Common.GetBits(this.type, 0);
                return StringData.SignalType[index];
            }
            set
            {
                var index = (ushort) StringData.SignalType.IndexOf(value);
                this.type = Common.SetBits(this.type, index, 0);
            }
        }

        public bool Color
        {
            get { return Common.GetBit(this.type, 8); }
            set { this.type = Common.SetBit(this.type, 8, value); }
        }
    }
}