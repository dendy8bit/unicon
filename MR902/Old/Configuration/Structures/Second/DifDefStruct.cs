﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;
using BEMN.MR902.Old.OldClasses;

namespace BEMN.MR902.Old.Configuration.Structures.Second
{
    public class DifDefStruct : StructBase
    {
        [Layout(0)] private ushort config;					//конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort config1;				//конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort block;					//вход блокировки
        [Layout(3)] private ushort ust;						//уставка срабатывания_
        [Layout(4)] private ushort time;					//время срабатывания_
        //характеристика торможения
        [Layout(5)] private ushort Ib1;			// начало  1
        [Layout(6)] private ushort K1;			// тангенс 1
        [Layout(7)] private ushort IOch;					//уставка очувствления
        [Layout(8)] private ushort TOch;					//время очувствления
        [Layout(9)] private ushort InputOch;				//вход очувствления
        [Layout(10)] private ushort I21;						//уставка по току 2-ой гармонике
        [Layout(11)] private ushort Ido;
        [Layout(12)] private ushort I51;
        [Layout(13)] private ushort rez;


        #region Диф защиты мгновенные
        public bool DefenceInstantaneousRunForPo
        {
            get { return Common.GetBit(this.config, 11); }
            set { this.config = Common.SetBit(this.config, 11, value); }
        }

        public string DIFM_MODE
        {
            get
            {
                return StringData.ModesLightMode[Common.GetBits(this.config, 0, 1) >> 0];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public string DIFM_BLOCK
        {
            get
            {
                return StringData.InputSignals[this.block];
            }
            set
            {
                this.block = (ushort)StringData.InputSignals.IndexOf(value);
            }
        }

        public double DIFM_ICP
        {
            get
            {
                return Measuring.GetConstraintOnly(this.ust, ConstraintKoefficient.K_4000);
            }
            set
            {
                this.ust = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        public double DIFM_IDO
        {
            get
            {
                return Measuring.GetConstraintOnly(this.Ido, ConstraintKoefficient.K_4000);
            }
            set
            {
                this.Ido = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        public ushort DIFM_TCP
        {
            get
            {
                return (ushort)(this.time * 10);
            }
            set
            {
                this.time = (ushort)(value / 10);
            }
        }

        public double DIFM_IB
        {
            get
            {
                return Measuring.GetConstraintOnly(this.Ib1, ConstraintKoefficient.K_4000);
            }
            set
            {
                this.Ib1 = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        public ushort DIFM_F
        {
            get
            {
                return this.K1;
            }
            set
            {
                this.K1 = value;
            }
        }

        public string DIFM_BLOCK_G
        {
            get
            {
                return StringData.YesNo[Common.GetBits(this.config, 4) >> 4];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.YesNo.IndexOf(value), 4);
            }
        }

        public double DIFM_I2G
        {
            get
            {
                return Measuring.GetConstraintOnly(this.I21, ConstraintKoefficient.K_65536);
            }
            set
            {
                this.I21 = Measuring.SetConstraint(value, ConstraintKoefficient.K_65536);
            }
        }

        public string DIFM_OCH
        {
            get
            {
                return StringData.YesNo[Common.GetBits(this.config, 3) >> 3];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.YesNo.IndexOf(value), 3);
            }
        }

        public int DIFM_T_OCH
        {
            get { return ValuesConverterCommon.GetWaitTime(this.TOch); }
            set { this.TOch = ValuesConverterCommon.SetWaitTime(value); }
        }



        public string DIFM_ENTER_OCH
        {
            get
            {
                return StringData.InputSignals[this.InputOch];
            }
            set
            {
                this.InputOch = (ushort)StringData.InputSignals.IndexOf(value);
            }
        }

        public double DIFM_I_OCH
        {
            get
            {
                return Measuring.GetConstraintOnly(this.IOch, ConstraintKoefficient.K_4000);
            }
            set
            {
                this.IOch = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        public string DIFM_OSC
        {
            get
            {
                return StringData.ModesLightOsc[Common.GetBits(this.config, 14, 15) >> 14];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.ModesLightOsc.IndexOf(value), 14, 15);
            }
        }

        public string DIFM_UROV
        {
            get
            {
                return StringData.ModesLight[Common.GetBits(this.config, 2) >> 2];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.ModesLight.IndexOf(value), 2);
            }
        }
        #endregion

        #region Диф защиты действующие
        public string DIFD_MODE
        {
            get
            {
                return StringData.ModesLightMode[Common.GetBits(this.config, 0, 1) >> 0];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public string DIFD_BLOCK
        {
            get
            {
                return StringData.InputSignals[this.block];
            }
            set
            {
                this.block = (ushort)StringData.InputSignals.IndexOf(value);
            }
        }

        public double DIFD_ICP
        {
            get
            {
                return Measuring.GetConstraintOnly(this.ust, ConstraintKoefficient.K_4000);
            }
            set
            {
                this.ust = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        public double DIFD_IDO
        {
            get
            {
                return Measuring.GetConstraintOnly(this.Ido, ConstraintKoefficient.K_4000);
            }
            set
            {
                this.Ido = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        public int DIFD_TCP
        {
            get { return ValuesConverterCommon.GetWaitTime(this.time); }
            set { this.time = ValuesConverterCommon.SetWaitTime(value); }
        }


        public double DIFD_IB
        {
            get
            {
                return Measuring.GetConstraintOnly(this.Ib1, ConstraintKoefficient.K_4000);
            }
            set
            {
                this.Ib1 = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        public ushort DIFD_F
        {
            get
            {
                return this.K1;
            }
            set
            {
                this.K1 = value;
            }
        }

        public string DIFD_BLOCK_G
        {
            get
            {
                return StringData.YesNo[Common.GetBits(this.config, 4) >> 4];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.YesNo.IndexOf(value), 4);
            }
        }

        public ushort DIFD_I2G
        {
            get
            {
                return this.I21;
            }
            set
            {
                this.I21 = value;
            }
        }

        public string DIFD_BLOCK_5G
        {
            get
            {
                return StringData.YesNo[Common.GetBits(this.config, 6) >> 6];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.YesNo.IndexOf(value), 6);
            }
        }

        public ushort DIFD_I5G
        {
            get
            {
                return this.I51;
            }
            set
            {
                this.I51 = value;
            }
        }

        public string DIFD_BLOCK_OPR_NAS
        {
            get
            {
                return StringData.YesNo[Common.GetBits(this.config, 8) >> 8];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.YesNo.IndexOf(value), 8);
            }
        }

        public string DIFD_OCH
        {
            get
            {
                return StringData.YesNo[Common.GetBits(this.config, 3) >> 3];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.YesNo.IndexOf(value), 3);
            }
        }

        public int DIFD_T_OCH
        {
            get { return ValuesConverterCommon.GetWaitTime(this.TOch); }
            set { this.TOch = ValuesConverterCommon.SetWaitTime(value); }
        }



        public string DIFD_ENTER_OCH
        {
            get
            {
                return StringData.InputSignals[this.InputOch];
            }
            set
            {
                this.InputOch = (ushort)StringData.InputSignals.IndexOf(value);
            }
        }

        public double DIFD_I_OCH
        {
            get
            {
                return Measuring.GetConstraintOnly(this.IOch, ConstraintKoefficient.K_4000);
            }
            set
            {
                this.IOch = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        public string DIFD_OSC
        {
            get
            {
                return StringData.ModesLightOsc[Common.GetBits(this.config, 14, 15) >> 14];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.ModesLightOsc.IndexOf(value), 14, 15);
            }
        }

        public bool DefenceActualRunForPo
        {
            get { return Common.GetBit(this.config, 11); }
            set { this.config = Common.SetBit(this.config, 11, value); }
        }



        public string DIFD_UROV
        {
            get
            {
                return StringData.ModesLight[Common.GetBits(this.config, 2) >> 2];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.ModesLight.IndexOf(value), 2);
            }
        }
        #endregion

    }
}