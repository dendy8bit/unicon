﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;

namespace BEMN.MR902.Old.Configuration.Structures.Second
{
    public class ExtDefStruct : StructBase
    {
        [Layout(0)] private ushort config;				//конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort config1;				//конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort block;					//вход блокировки
        [Layout(3)] private ushort ust;					//уставка срабатывания_
        [Layout(4)] private ushort time;					//время срабатывания_
        [Layout(5)] private ushort u;						//уставка возврата
        [Layout(6)] private ushort tu;					//время возврата
        [Layout(7)] private ushort rez;					//резерв

        #region Внешние защиты
        public string EXTERNAL_MODE
        {
            get
            {
                return StringData.ModesLightMode[Common.GetBits(this.config, 0, 1) >> 0];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public string EXTERNAL_OTKL
        {
            get
            {
                return StringData.Otkl[Common.GetBits(this.config, 4, 5, 6, 7, 8) >> 4];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.Otkl.IndexOf(value), 4, 5, 6, 7, 8);
            }
        }

        public string EXTERNAL_BLOCK
        {
            get
            {
                return StringData.SignalSrabExternal[this.block];
            }
            set
            {
                this.block = (ushort)StringData.SignalSrabExternal.IndexOf(value);
            }
        }

        public string EXTERNAL_SRAB
        {
            get
            {
                return StringData.SignalSrabExternal[this.ust];
            }
            set
            {
                this.ust = (ushort)StringData.SignalSrabExternal.IndexOf(value);
            }
        }

        public int EXTERNAL_TSR
        {
            get { return ValuesConverterCommon.GetWaitTime(this.time); }
            set { this.time = ValuesConverterCommon.SetWaitTime(value); }
        }


        public int EXTERNAL_TVZ
        {
            get { return ValuesConverterCommon.GetWaitTime(this.tu); }
            set { this.tu = ValuesConverterCommon.SetWaitTime(value); }
        }


        public string EXTERNAL_VOZVR
        {
            get
            {
                return StringData.SignalSrabExternal[this.u];
            }
            set
            {
                this.u = (ushort)StringData.SignalSrabExternal.IndexOf(value);
            }
        }


        public string EXTERNAL_VOZVR_YN
        {
            get
            {
                return StringData.YesNo[Common.GetBits(this.config, 3) >> 3];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.YesNo.IndexOf(value), 3);
            }
        }


        public string EXTERNAL_OSC
        {
            get
            {
                return StringData.ModesLightOsc[Common.GetBits(this.config, 14, 15) >> 14];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.ModesLightOsc.IndexOf(value), 14, 15);
            }
        }

        public string EXTERNAL_UROV
        {
            get
            {
                return StringData.ModesLight[Common.GetBits(this.config, 2) >> 2];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.ModesLight.IndexOf(value), 2);
            }
        }



        #endregion
    }
}