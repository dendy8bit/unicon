﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR902.Old.Configuration.Structures.Second
{
    /// <summary>
    /// конфигурация цепей ТТ
    /// </summary>
    public class ControlTtStruct : StructBase
    {
        [Layout(0)] private ushort _config; //	выведена, неисправность, неисправность + блокировка
        [Layout(1)] private ushort _ust;    //	уставка минимального дифференциального тока
        [Layout(2)] private ushort _time;   //	выдержка времени
        [Layout(3)] private ushort _rez;    //	резерв

        /// <summary>
        /// Неисправность
        /// </summary>
        public string Fault
        {
            get { return StringData.TtFault[this._config]; }
            set { this._config = (ushort) StringData.TtFault.IndexOf(value); }
        }
        /// <summary>
        /// Iдmin
        /// </summary>
        public double Idmin
        {
            get { return ValuesConverterCommon.GetIn(this._ust); }
            set { this._ust = ValuesConverterCommon.SetIn(value); }
        }
        /// <summary>
        /// Tср
        /// </summary>
        public int Tsrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }
    }
}
