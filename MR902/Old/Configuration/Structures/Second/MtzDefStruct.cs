﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;
using BEMN.MR902.Old.OldClasses;

namespace BEMN.MR902.Old.Configuration.Structures.Second
{
    public class MtzDefStruct : StructBase
    {
        [Layout(0)] private ushort config;				//конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort config1;				//конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort block;				//вход блокировки
        [Layout(3)] private ushort ust;					//уставка срабатывания_
        [Layout(4)] private ushort time;				//время срабатывания_ тср
        [Layout(5)] private ushort k;					//коэфиц. зависимой хар-ки
        [Layout(6)] private ushort u;					//уставка пуска по напряжению Упуск
        [Layout(7)] private ushort tu;					//время ускорения

        #region Защиты I
        public string MTZ_MODE
        {
            get
            {
                return StringData.ModesLightMode[Common.GetBits(this.config, 0, 1) >> 0];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.ModesLightMode.IndexOf(value), 0, 1);
            }
        }


        public string Logic
        {
            get
            {
                return StringData.TokParameter.Count > Common.GetBits(this.config, 5) >> 5
                    ? StringData.TokParameter[Common.GetBits(this.config, 5) >> 5]
                    : "XXXXX";
            }
            set
            {
                var input = StringData.TokParameter.IndexOf(value);
                this.config = Common.SetBits(this.config, (ushort) input, 5);
            }
        }

        public string MTZ_BLOCK
        {
            get
            {
                return StringData.InputSignals[this.block];
            }
            set
            {
                this.block = (ushort)StringData.InputSignals.IndexOf(value);
            }
        }

        public string MTZ_MEASURE
        {
            get
            {
                return StringData.MTZJoin.Count > Common.GetBits(this.config, 6, 7, 8, 9) >> 6
                    ? StringData.MTZJoin[Common.GetBits(this.config, 6, 7, 8, 9) >> 6]
                    : null;
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.MTZJoin.IndexOf(value), 6, 7, 8, 9);
            }
        }

        public double MTZ_ICP
        {
            get
            {
                return Measuring.GetConstraintOnly(this.ust, ConstraintKoefficient.K_4000);
            }
            set
            {
                this.ust = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        public string MTZ_CHAR
        {
            get
            {
                return StringData.Characteristic.Count > Common.GetBits(this.config, 3) >> 3
                    ? StringData.Characteristic[Common.GetBits(this.config, 3) >> 3]
                    : "XXXXX";
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.Characteristic.IndexOf(value), 3);
            }
        }

        public int MTZ_T
        {
            get { return ValuesConverterCommon.GetWaitTime(this.time); }
            set { this.time = ValuesConverterCommon.SetWaitTime(value); }

        }

        public ushort MTZ_K
        {
            get
            {
                return this.k;
            }
            set
            {
                this.k = value;
            }
        }

        public int MTZ_TY
        {
            get { return ValuesConverterCommon.GetWaitTime(this.tu); }
            set { this.tu = ValuesConverterCommon.SetWaitTime(value); }
        }



        public string MTZ_TY_YN
        {
            get
            {
                return StringData.YesNo[Common.GetBits(this.config, 4) >> 4];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.YesNo.IndexOf(value), 4);
            }
        }

        public string MTZ_OSC
        {
            get
            {
                return StringData.ModesLightOsc[Common.GetBits(this.config, 14, 15) >> 14];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.ModesLightOsc.IndexOf(value), 14, 15);
            }
        }

        public string MTZ_UROV
        {
            get
            {
                return StringData.ModesLight[Common.GetBits(this.config, 2) >> 2];
            }
            set
            {
                this.config = Common.SetBits(this.config, (ushort)StringData.ModesLight.IndexOf(value), 2);
            }
        }
        #endregion
    }
}