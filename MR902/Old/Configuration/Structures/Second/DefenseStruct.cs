﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR902.Old.Configuration.Structures.Second
{
    public class DefenseStruct : StructBase
    {
        [Layout(0, Count = 3)] private DifDefStruct[] _diffDstv;		//токовая диффзащита для действующих значений
        [Layout(1, Count = 3)] private DifDefStruct[] _diffMgn;		    //токовая диффзащита для мгновенных значений
        [Layout(2, Count = 32)] private MtzDefStruct[] _mtzDefStruct;	//мтз основная
        [Layout(3, Count = 16)] private ExtDefStruct[] _mtzExt;			//внешние


        public DifDefStruct[] DiffDstv
        {
            get { return this._diffDstv; }
            set { this._diffDstv = value; }
        }

        public DifDefStruct[] DiffMgn
        {
            get { return this._diffMgn; }
            set { this._diffMgn = value; }
        }

        public MtzDefStruct[] MtzDefStruct
        {
            get { return this._mtzDefStruct; }
            set { this._mtzDefStruct = value; }
        }

        public ExtDefStruct[] MtzExt
        {
            get { return this._mtzExt; }
            set { this._mtzExt = value; }
        }
    }
}