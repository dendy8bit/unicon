﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MR902.Old.OldClasses;

namespace BEMN.MR902.Old.Configuration.Structures.Second
{
    /// <summary>
    /// конфигурация уров присоединения
    /// </summary>
    public class UrovConnectionStruct : StructBase
    {
        [Layout(0)] private ushort _ust;		//ток уров
        [Layout(1)] private ushort _time;		//время уров

        #region Уров Присоединения
        public double UrovJoinI
        {
            get
            {
                return Measuring.GetConstraintOnly(this._ust, ConstraintKoefficient.K_4000);
            }
            set
            {
                this._ust = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }
        public int UrovJoinT
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._time);
            }
            set
            {
                this._time = ValuesConverterCommon.SetWaitTime(value);
            }
        }
        #endregion
    }
}