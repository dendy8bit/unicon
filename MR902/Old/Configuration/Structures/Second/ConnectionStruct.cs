﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;

namespace BEMN.MR902.Old.Configuration.Structures.Second
{
    /// <summary>
    /// Присоединение
    /// </summary>
    public class ConnectionStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _config;					    //  конфигурация присоединения
        [Layout(1)] private ushort _inom;					    //	номинальный ток измерительного ТТ
        [Layout(2)] private ushort _off;						//	определение отключенного положения выключателя
        [Layout(3)] private ushort _on;						    //	определение включенного положения выключателя
        [Layout(4)] private ushort _inp;						//	вход, если конфигурация присоединения == "От входа"
        [Layout(5)] private ushort _tclr;                       //  задержка обнуления 
        #endregion [Private fields]
        
        #region [Properties]
        /// <summary>
        /// Привязка
        /// </summary>
        public string JoinJoin
        {
            get
            {
                var join = Common.GetBits(this._config, 0, 1, 2) >> 0;
                return StringData.Join[join];
            }
            set
            {
                var join = (ushort)StringData.Join.IndexOf(value);
                this._config = Common.SetBits(this._config, join, 0, 1, 2);

            }
        }
        /// <summary>
        /// Вход
        /// </summary>
        public string JoinEnter
        {
            get
            {
                return StringData.InputSignals[this._inp];
            }
            set
            {
                this._inp = (ushort)StringData.InputSignals.IndexOf(value);
            }
        }
        /// <summary>
        /// Обнуление
        /// </summary>
        public bool ResetJoin
        {
            get { return Common.GetBit(this._config, 15); }
            set { this._config = Common.SetBit(this._config, 15, value); }
        }
        /// <summary>
        /// Включение
        /// </summary>
        public string JoinSwitchon
        {
            get
            {
                return StringData.InputSignals[this._on];
            }
            set
            {
                this._on = (ushort)StringData.InputSignals.IndexOf(value);
            }
        }
        /// <summary>
        /// Отключение
        /// </summary>
        public string JoinSwitchoff
        {
            get
            {
                return StringData.InputSignals[this._off];
            }
            set
            {
                this._off = (ushort)StringData.InputSignals.IndexOf(value);
            }
        }
        /// <summary>
        /// Iтт
        /// </summary>
        public ushort Inom
        {
            get { return this._inom; }
            set { this._inom = value; }
        }
        /// <summary>
        /// Задержка обнуления
        /// </summary>
        public int ResetDeley
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tclr); }
            set { this._tclr = ValuesConverterCommon.SetWaitTime(value); }

        }
        #endregion [Properties]
    }
}