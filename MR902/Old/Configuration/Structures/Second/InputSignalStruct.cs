﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR902.Old.Configuration.Structures.Second
{
    /// <summary>
    /// конфигурациия входных логических сигналов
    /// </summary>
    public class InputSignalStruct : StructBase
    {
        [Layout(0)] private ushort a1;
        [Layout(1)] private ushort a2;
        [Layout(2)] private ushort a3;
        [Layout(3)] private ushort a4;
    }
}