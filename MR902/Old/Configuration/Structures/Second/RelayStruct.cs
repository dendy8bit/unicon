﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR902.Old.Configuration.Structures.Second
{
    public class RelayStruct : StructBase
    {
        [Layout(0)] private ushort signal;
        [Layout(1)] private ushort type;
        [Layout(2)] private ushort wait;
        [Layout(3)] private ushort rez;

        public  string Signal
        {
            get { return StringData.SignalSrab[this.signal]; }
            set { this.signal = (ushort) StringData.SignalSrab.IndexOf(value); }
        }

        public string Type
        {
            get { return StringData.SignalType[this.type]; }
            set { this.type = (ushort)StringData.SignalType.IndexOf(value); }
        }

        public int Wait
        {
            get { return ValuesConverterCommon.GetWaitTime(this.wait); }
            set { this.wait = ValuesConverterCommon.SetWaitTime(value); }
        }
    }
}