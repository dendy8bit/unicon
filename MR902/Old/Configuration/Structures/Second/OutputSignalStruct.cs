﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR902.Old.Configuration.Structures.Second
{
    /// <summary>
    /// конфигурациия выходных логических сигналов
    /// </summary>
    public class OutputSignalStruct : StructBase
    {
        public const int COUNT = 18;
        [Layout(0, Count = COUNT)] private ushort[] _a;

        public ushort[] A
        {
            get { return this._a; }
            set { this._a = value; }
        }
    }
}