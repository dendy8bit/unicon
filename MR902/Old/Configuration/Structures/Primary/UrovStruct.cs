﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;

namespace BEMN.MR902.Old.Configuration.Structures.Primary
{
    /// <summary>
    /// конфигурация уров
    /// </summary>
    public class UrovStruct : StructBase
    {
        [Layout(0)] private ushort _config; //конфигурация
        [Layout(1)] private ushort _ust; //уставка срабатывания
        [Layout(2)] private ushort _time1; //время срабатывания
        [Layout(3)] private ushort _time2; //время срабатывания
        [Layout(4)] private ushort _time3; //время срабатывания
        [Layout(5)] private ushort _urovSh1;
        [Layout(6)] private ushort _urovSh2;
        [Layout(7)] private ushort _urovSh12;

        #region ДЗШ

        public string DzhModes
        {
            get { return StringData.ModesLight[Common.GetBits(this._config, 0) >> 0]; }
            set { this._config = Common.SetBits(this._config, (ushort) StringData.ModesLight.IndexOf(value), 0); }
        }

        public string DzhControl
        {
            get { return StringData.KONTR[Common.GetBits(this._config, 1) >> 1]; }
            set { this._config = Common.SetBits(this._config, (ushort) StringData.KONTR.IndexOf(value), 1); }
        }

        public string DzhDiff
        {
            get { return StringData.Forbidden[Common.GetBits(this._config, 2) >> 2]; }
            set { this._config = Common.SetBits(this._config, (ushort) StringData.Forbidden.IndexOf(value), 2); }
        }

        public string DzhSelf
        {
            get { return StringData.Forbidden[Common.GetBits(this._config, 3) >> 3]; }
            set { this._config = Common.SetBits(this._config, (ushort) StringData.Forbidden.IndexOf(value), 3); }
        }

        public int DzhT1
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time1); }
            set { this._time1 = ValuesConverterCommon.SetWaitTime(value); }
        }

        public int DzhT2
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time2); }
            set { this._time2 = ValuesConverterCommon.SetWaitTime(value); }
        }

        public int DzhT3
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time3); }
            set { this._time3 = ValuesConverterCommon.SetWaitTime(value); }
        }

        public string DzhSign
        {
            get { return StringData.Forbidden[Common.GetBits(this._config, 4) >> 4]; }
            set { this._config = Common.SetBits(this._config, (ushort) StringData.Forbidden.IndexOf(value), 4); }
        }

        public string DzhSh1
        {
            get { return StringData.InputSignals[this._urovSh1]; }
            set { this._urovSh1 = (ushort) StringData.InputSignals.IndexOf(value); }
        }

        public string DzhSh2
        {
            get { return StringData.InputSignals[this._urovSh2]; }
            set { this._urovSh2 = (ushort) StringData.InputSignals.IndexOf(value); }
        }

        public string DzhPo
        {
            get { return StringData.InputSignals[this._urovSh12]; }
            set { this._urovSh12 = (ushort) StringData.InputSignals.IndexOf(value); }
        }

        #endregion
    }
}