﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;
using BEMN.MR902.Old.Configuration.Structures.Second;

namespace BEMN.MR902.Old.Configuration.Structures.Primary
{
    public class ParamAutomatStruct : StructBase
    {
        [Layout(0, Count = 18)] private RelayStruct[] _relayStruct;		 //реле
        [Layout(1, Count = 12)] private IndicatorStruct[] _indicatorStruct;//индикаторы
        [Layout(2)] private ushort disrepair;					//реле неисправность
        [Layout(3)] private ushort disrepairImp;				    //импульс реле неисправность

        
        #region Параметры неисправности
        public string OutputN1
        {
            get
            {
                string rez = "Нет";
                bool bit = Common.GetBit(this.disrepair, 0);
                if (bit)
                {
                    rez = "Есть";
                }

                return rez;
            }
            set
            {
                bool rez = false;
                if (value == "Есть")
                {
                    rez = true;
                }

                this.disrepair = Common.SetBit(this.disrepair, 0, rez);
            }
        }

        public string OutputN2
        {
            get
            {
                string rez = "Нет";
                bool bit = Common.GetBit(this.disrepair, 1);
                if (bit)
                {
                    rez = "Есть";
                }

                return rez;
            }
            set
            {
                bool rez = false;
                if (value == "Есть")
                {
                    rez = true;
                }

                this.disrepair = Common.SetBit(this.disrepair, 1, rez);
            }
        }

        public string OutputN3
        {
            get
            {
                string rez = "Нет";
                bool bit = Common.GetBit(this.disrepair, 2);
                if (bit)
                {
                    rez = "Есть";
                }

                return rez;
            }
            set
            {
                bool rez = false;
                if (value == "Есть")
                {
                    rez = true;
                }

                this.disrepair = Common.SetBit(this.disrepair, 2, rez);
            }
        }

        public string OutputN4
        {
            get
            {
                string rez = "Нет";
                bool bit = Common.GetBit(this.disrepair, 3);
                if (bit)
                {
                    rez = "Есть";
                }

                return rez;
            }
            set
            {
                bool rez = false;
                if (value == "Есть")
                {
                    rez = true;
                }

                this.disrepair = Common.SetBit(this.disrepair, 3, rez);
            }
        }

        public int OutputImp
        {
            get { return ValuesConverterCommon.GetWaitTime(this.disrepairImp); }
            set { this.disrepairImp = ValuesConverterCommon.SetWaitTime(value); }
        }

        public RelayStruct[] RelayStruct
        {
            get { return this._relayStruct; }
            set { this._relayStruct = value; }
        }

        public IndicatorStruct[] IndicatorStruct
        {
            get { return this._indicatorStruct; }
            set { this._indicatorStruct = value; }
        }

        #endregion
    }
}