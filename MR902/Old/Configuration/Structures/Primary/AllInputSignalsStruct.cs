﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;
using BEMN.MR902.Old.Configuration.Structures.Second;

namespace BEMN.MR902.Old.Configuration.Structures.Primary
{
    public class AllInputSignalsStruct : StructBase
    {
        [Layout(0, Count = 16)] private InputSignalStruct[] _inputSignal;

        #region Входные сигналы
        [XmlIgnore]
        public ushort[] InpSignals
        {
            get
            {
                List<ushort> res = new List<ushort>();
                for (int i = 0; i < this._inputSignal.Length; i++)
                {
                    res.AddRange(this._inputSignal[i].GetValues());
                }

                return res.ToArray();
            }
            set
            {
                ushort[] res = value;
                int index = 0;
                for (int i = 0; i < this._inputSignal.Length; i++)
                {
                    StructInfo sInfo = this._inputSignal[i].GetStructInfo();
                    ushort[] tmp = new ushort[sInfo.FullSize];
                    Array.ConstrainedCopy(res, index, tmp, 0, tmp.Length);
                    this._inputSignal[i].InitStruct(Common.TOBYTES(tmp, false));
                    index += tmp.Length;
                }
            }
        }

        public string[][] ToXml
        {
            get
            {
                int _inpSignalsIndex;
                ushort[] inpSignals = this.InpSignals;
                var res = new string[16][];
                for (int j = 0; j < 16; j++)
                {
                    res[j] = new string[24];
                    _inpSignalsIndex = j * 4;
                    int indexLs = 0;
                    do
                    {
                        for (int i = 0; i < 16; i += 2)
                        {
                            int IND = Common.GetBits(inpSignals[_inpSignalsIndex], i, i + 1) >> i;
                            res[j][ indexLs] = StringData.LogicValues[IND];
                            indexLs++;
                        }
                        _inpSignalsIndex++;
                    } while (indexLs < 24);
                }
                return res;
            }
        }

        #endregion
    }
}