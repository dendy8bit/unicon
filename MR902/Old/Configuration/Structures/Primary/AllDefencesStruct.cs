﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR902.Old.Configuration.Structures.Second;

namespace BEMN.MR902.Old.Configuration.Structures.Primary
{
    public class AllDefencesStruct : StructBase
    {
        [Layout(0)] private DefenseStruct _mainGroup;
        [Layout(1)] private DefenseStruct _reserveGroup;

        public DefenseStruct MainGroup
        {
            get { return this._mainGroup; }
            set { this._mainGroup = value; }
        }

        public DefenseStruct ReserveGroup
        {
            get { return this._reserveGroup; }
            set { this._reserveGroup = value; }
        }
    }
}
