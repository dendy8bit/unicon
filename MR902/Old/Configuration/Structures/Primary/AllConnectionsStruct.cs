﻿using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR902.Old.Configuration.Structures.Second;

namespace BEMN.MR902.Old.Configuration.Structures.Primary
{
    /// <summary>
    /// конфигурация присоединений
    /// </summary>
    public class AllConnectionsStruct : StructBase
    {
        private const int CONNECTION_LEN = 16;
        public const int CONNECTIONS_MR902 = 6;

        [Layout(0, Count = CONNECTION_LEN)] private ConnectionStruct[] _connMain;

        public ConnectionStruct[] ConnMain
        {
            get { return this._connMain; }
            set { this._connMain = value; }
        }

        #region [Properties]
        /// <summary>
        /// Токи присоединений
        /// </summary>
        public ushort[] GetAllItt
        {
            get
            {
                ushort[] result = new ushort[CONNECTIONS_MR902];
                for (int i = 0; i < CONNECTIONS_MR902; i++)
                {
                    result[i] = this._connMain[i].Inom;
                }
                return result;
            }
        }

        /// <summary>
        /// Токи присоединений
        /// </summary>
        public ushort[] GetAllIttNew
        {
            get
            {
                List<ushort> result = new List<ushort> {this._connMain.Take(CONNECTIONS_MR902).Max(o => o.Inom)};

                for(int i=0; i < CONNECTIONS_MR902; i++)
                {
                    result.Add(this._connMain[i].Inom); 
                }
                return result.ToArray();
            }
        }

        #endregion [Properties]
    }
}