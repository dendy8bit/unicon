﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;
using BEMN.MR902.Old.Configuration.Structures.Second;

namespace BEMN.MR902.Old.Configuration.Structures.Primary
{
    /// <summary>
    /// структура выходных логических сигналов
    /// </summary>
    public class AllOutputSignalsStruct : StructBase, IXmlSerializable
    {
        public const int COUNT = 16;

        [Layout(0, Count = 16)] private OutputSignalStruct[] _outputSignal;

        #region ВЛС
       // [XmlIgnore]
        public ushort[] VLS
        {
            get
            {
                ushort[] res = new ushort[this._outputSignal.Length * this._outputSignal[0].A.Length];
                int index = 0;
                for (int i = 0; i < this._outputSignal.Length; i++)
                {
                    Array.ConstrainedCopy(this._outputSignal[i].A, 0, res, index, this._outputSignal[i].A.Length);
                    index += this._outputSignal[i].A.Length;
                }

                return res;
            }
            set
            {
                ushort[] res = value;
                int index = 0;
                for (int i = 0; i < this._outputSignal.Length; i++)
                {
                    Array.ConstrainedCopy(res, index, this._outputSignal[i].A, 0, this._outputSignal[i].A.Length);
                    index += this._outputSignal[i].A.Length;
                }
            }
        }

        #endregion

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            ushort[] _vls = this.VLS;
            var res = new string[16][];

            int byteIndex = 0;

            for (int i = 0; i < this._outputSignal.Length; i++) //Счетчик закладок ВЛС 1-16
            {
                List<string> temp = new List<string>();
                byteIndex = 18 * i; //индекс массива 1ВЛС = 16 байт

                for (int j = 0; j < StringData.VLSSignals.Count; j += this._outputSignal.Length) //Счетчик элементов одного ВЛС 1-125
                {
                    int itemIndex = j; //индекс элемента списка
                    for (int bitIndex = 0; bitIndex < 16; bitIndex++)//перебираем биты слова
                    {
                        if (itemIndex < (StringData.VLSSignals.Count /*- 1*/))//Чтоб индекс элемента списка не зашкалил за длинну списка
                        {
                            if (Common.GetBit(_vls[byteIndex], bitIndex))
                            {
                                writer.WriteElementString(string.Format("ВЛС{0}", i+1), StringData.VLSSignals[itemIndex]);
                            }

                            itemIndex++;//переходим к сл. элементу списка
                        }
                    }
                    byteIndex++;//переходим к следующему байту массива данного ВЛС
                }
                if (temp.Count == 0)
                {
                    temp.Add("Нет");
                }
                res[i] = temp.ToArray();
                
            }
        }
    }
}