﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR902.Old.Configuration.Structures.Second;

namespace BEMN.MR902.Old.Configuration.Structures.Primary
{
    /// <summary>
    /// конфигурация цепей ТТ
    /// </summary>
    public class AllConfigTtStruct : StructBase
    {
        public const int TT_COUNT = 3;
        [Layout(0, Count = TT_COUNT)] private ControlTtStruct[] _ttChannels;

        public ControlTtStruct[] TtChannels
        {
            get { return this._ttChannels; }
            set { this._ttChannels = value; }
        }
    }
}
