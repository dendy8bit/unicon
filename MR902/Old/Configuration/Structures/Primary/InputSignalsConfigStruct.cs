﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR902.Old.Configuration.Structures.Primary
{
    public class InputSignalsConfigStruct : StructBase
    {
        [Layout(0)] private ushort _groopUst;			//вход аварийная группа уставок
        [Layout(1)] private ushort _clrInd;				//вход сброс индикации
        
        public string GrUst
        {
            get
            {
                return StringData.InputSignals[this._groopUst];
            }
            set
            {
                this._groopUst = (ushort)StringData.InputSignals.IndexOf(value);
            }
        }

        public string SbInd
        {
            get
            {
                return StringData.InputSignals[this._clrInd];
            }
            set
            {
                this._clrInd = (ushort)StringData.InputSignals.IndexOf(value);
            }
        }
    }
}