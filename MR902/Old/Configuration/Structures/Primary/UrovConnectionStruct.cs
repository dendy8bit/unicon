﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR902.Old.Configuration.Structures.Second;

namespace BEMN.MR902.Old.Configuration.Structures.Primary
{
    /// <summary>
    /// конфигурация уров присоединений
    /// </summary>
    public class UrovConnectionsStruct : StructBase
    {
        [Layout(0, Count = 16)] private UrovConnectionStruct[] _ust;

        public UrovConnectionStruct[] Ust
        {
            get { return this._ust; }
            set { this._ust = value; }
        }
    }
}