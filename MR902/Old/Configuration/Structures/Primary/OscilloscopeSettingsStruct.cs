﻿using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR902.Old.Configuration.Structures.Primary
{
    /// <summary>
    /// Уставки осциллографа
    /// </summary>
    public class OscilloscopeSettingsStruct : StructBase
    {
        [Layout(0)] private ushort _config;				        //0 - фиксация по первой аварии 1 - фиксация по последней аварии
        [Layout(1)] private ushort _size;				            //размер осциллограмы
        [Layout(2)] private ushort _percent;			            //процент от размера осциллограммы
        [Layout(3, Count = 8)] private ushort[] _kanal;           //конфигурация канала осциллографирования
        [Layout(4, Count = 5)] private ushort[] _rez;

        #region Конфигурация Осциллографа
        public string OscLength
        {
            get
            {
                if (this._size == 0)
                {
                  return  StringData.OscLength[0];
                }
                return StringData.OscLength[this._size - 1];
            }
            set
            {
                this._size = (ushort)(StringData.OscLength.IndexOf(value) + 1);
            }
        }

        public ushort OscWLength
        {
            get
            {
                return (ushort)(this._percent);
            }
            set
            {
                this._percent = (ushort)(value);
            }
        }

        public string OscFix
        {
            get
            {
                return StringData.OscFix[this._config];
            }
            set
            {
                this._config = (ushort)StringData.OscFix.IndexOf(value);
            }
        }
        #endregion




        public string[] Channels
        {
            get { return this._kanal.Select(o => StringData.SignalSrab[o]).ToArray(); }
            set
            {
                for (int i = 0; i < value.Length; i++)
                {

                    this._kanal[i] = (ushort) StringData.SignalSrab.IndexOf(value[i]);
                }
            }
        }

        public ushort[] Kanal
        {
            get { return this._kanal; }
            set { this._kanal = value; }
        }
    }
}