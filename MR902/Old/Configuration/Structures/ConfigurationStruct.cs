﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR902.Old.Configuration.Structures.Primary;

namespace BEMN.MR902.Old.Configuration.Structures
{
    public class ConfigurationStruct : StructBase
    {
        [XmlElement(ElementName = "Версия")]
        public double DeviceVersion { get; set; }

        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }

        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType 
        {
            get => "МР902";
            set { }
        }

        [Layout(0)] private UrovStruct _urovStruct;					//конфигурация уров
        [Layout(1)] private UrovConnectionsStruct _urovConn;		///конфигурация уров присоединений
        [Layout(2)] private AllConnectionsStruct _conn;				//конфигурация присоединений
        [Layout(3, Count = 2, Ignore = true)] private ushort[] _allCfg;	//общие настройки
        [Layout(4)] private InputSignalsConfigStruct _impsg;		//конфигурациия входных сигналов
        [Layout(5)] private OscilloscopeSettingsStruct _osc;	    //конфигурациия осцилографа
        [Layout(6)] private AllConfigTtStruct _ctt;			        //конфигурация цепей ТТ
        [Layout(7)] private AllInputSignalsStruct _inp;				//структура входных логических сигналов
        [Layout(8)] private AllOutputSignalsStruct _els;			//структура выходных логических сигналов
        [Layout(9)] private AllDefencesStruct _allDefences;           //все защиты
        [Layout(10)] private ParamAutomatStruct _paramAutomat;		    //параметры автоматики

        public int NetConfSize => 4;

        public int AboveNetSize
        {
            get { return this._urovStruct.GetSize() + this._urovConn.GetSize() + this._conn.GetSize(); }
        }

        public UrovStruct UrovStruct
        {
            get { return this._urovStruct; }
            set { this._urovStruct = value; }
        }

        public UrovConnectionsStruct UrovConn
        {
            get { return this._urovConn; }
            set { this._urovConn = value; }
        }

        ///конфигурация уров присоединений
        public AllConnectionsStruct Conn
        {
            get { return this._conn; }
            set { this._conn = value; }
        }
        
        public InputSignalsConfigStruct Impsg
        {
            get { return this._impsg; }
            set { this._impsg = value; }
        }

        public OscilloscopeSettingsStruct Osc
        {
            get { return this._osc; }
            set { this._osc = value; }
        }

        public AllConfigTtStruct Ctt
        {
            get { return this._ctt; }
            set { this._ctt = value; }
        }

        public AllInputSignalsStruct Inp
        {
            get { return this._inp; }
            set { this._inp = value; }
        }

        public AllOutputSignalsStruct Els
        {
            get { return this._els; }
            set { this._els = value; }
        }

        public AllDefencesStruct AllDefences
        {
            get { return this._allDefences; }
            set { this._allDefences = value; }
        }

        public ParamAutomatStruct ParamAutomat
        {
            get { return this._paramAutomat; }
            set { this._paramAutomat = value; }
        }
    }
}