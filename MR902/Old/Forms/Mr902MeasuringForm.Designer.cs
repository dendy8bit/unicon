﻿namespace BEMN.MR902.Old.Forms
{
    partial class Mr902MeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._dataBaseTabControl = new System.Windows.Forms.TabControl();
            this._analogTabPage = new System.Windows.Forms.TabPage();
            this._dateTimeControl = new BEMN.Forms.DateTimeControl();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this._i16TB = new System.Windows.Forms.TextBox();
            this.label290 = new System.Windows.Forms.Label();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.label276 = new System.Windows.Forms.Label();
            this.label283 = new System.Windows.Forms.Label();
            this.label284 = new System.Windows.Forms.Label();
            this._i15TB = new System.Windows.Forms.TextBox();
            this._i14TB = new System.Windows.Forms.TextBox();
            this._i13TB = new System.Windows.Forms.TextBox();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label271 = new System.Windows.Forms.Label();
            this.label272 = new System.Windows.Forms.Label();
            this._i12TB = new System.Windows.Forms.TextBox();
            this._i11TB = new System.Windows.Forms.TextBox();
            this._i10TB = new System.Windows.Forms.TextBox();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this._i9TB = new System.Windows.Forms.TextBox();
            this._i8TB = new System.Windows.Forms.TextBox();
            this._i7TB = new System.Windows.Forms.TextBox();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this._i6TB = new System.Windows.Forms.TextBox();
            this._i5TB = new System.Windows.Forms.TextBox();
            this._i4TB = new System.Windows.Forms.TextBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this._i3TB = new System.Windows.Forms.TextBox();
            this.label280 = new System.Windows.Forms.Label();
            this._i2TB = new System.Windows.Forms.TextBox();
            this.label281 = new System.Windows.Forms.Label();
            this._i1TB = new System.Windows.Forms.TextBox();
            this.label282 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._itc3TB = new System.Windows.Forms.TextBox();
            this.label154 = new System.Windows.Forms.Label();
            this._itb3TB = new System.Windows.Forms.TextBox();
            this.label155 = new System.Windows.Forms.Label();
            this._ita3TB = new System.Windows.Forms.TextBox();
            this.label156 = new System.Windows.Forms.Label();
            this._idc3TB = new System.Windows.Forms.TextBox();
            this.label234 = new System.Windows.Forms.Label();
            this._idb3TB = new System.Windows.Forms.TextBox();
            this.label235 = new System.Windows.Forms.Label();
            this._ida3TB = new System.Windows.Forms.TextBox();
            this.label236 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._itc2TB = new System.Windows.Forms.TextBox();
            this.label136 = new System.Windows.Forms.Label();
            this._itb2TB = new System.Windows.Forms.TextBox();
            this.label137 = new System.Windows.Forms.Label();
            this._ita2TB = new System.Windows.Forms.TextBox();
            this.label138 = new System.Windows.Forms.Label();
            this._idc2TB = new System.Windows.Forms.TextBox();
            this.label142 = new System.Windows.Forms.Label();
            this._idb2TB = new System.Windows.Forms.TextBox();
            this.label143 = new System.Windows.Forms.Label();
            this._ida2TB = new System.Windows.Forms.TextBox();
            this.label150 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._itc1TB = new System.Windows.Forms.TextBox();
            this.label130 = new System.Windows.Forms.Label();
            this._itb1TB = new System.Windows.Forms.TextBox();
            this.label131 = new System.Windows.Forms.Label();
            this._ita1TB = new System.Windows.Forms.TextBox();
            this.label132 = new System.Windows.Forms.Label();
            this._idc1TB = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._idb1TB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this._ida1TB = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._discretTabPage = new System.Windows.Forms.TabPage();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.label223 = new System.Windows.Forms.Label();
            this._difDefenceActualIoPo = new BEMN.Forms.LedControl();
            this._difDefenceActualIoSh2 = new BEMN.Forms.LedControl();
            this._difDefenceActualIoSh1 = new BEMN.Forms.LedControl();
            this.label216 = new System.Windows.Forms.Label();
            this.label217 = new System.Windows.Forms.Label();
            this.label218 = new System.Windows.Forms.Label();
            this._difDefenceActualChtoPo = new BEMN.Forms.LedControl();
            this._difDefenceActualChtoSh2 = new BEMN.Forms.LedControl();
            this._difDefenceActualChtoSh1 = new BEMN.Forms.LedControl();
            this._difDefenceActualSrabPo = new BEMN.Forms.LedControl();
            this._difDefenceActualSrabSh2 = new BEMN.Forms.LedControl();
            this._difDefenceActualSrabSh1 = new BEMN.Forms.LedControl();
            this.label219 = new System.Windows.Forms.Label();
            this.label220 = new System.Windows.Forms.Label();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.label215 = new System.Windows.Forms.Label();
            this.label214 = new System.Windows.Forms.Label();
            this.label213 = new System.Windows.Forms.Label();
            this._difDefenceInstantaneousChtoPo = new BEMN.Forms.LedControl();
            this._difDefenceInstantaneousChtoSh2 = new BEMN.Forms.LedControl();
            this._difDefenceInstantaneousChtoSh1 = new BEMN.Forms.LedControl();
            this._difDefenceInstantaneousSrabPo = new BEMN.Forms.LedControl();
            this._difDefenceInstantaneousSrabSh2 = new BEMN.Forms.LedControl();
            this._difDefenceInstantaneousSrabSh1 = new BEMN.Forms.LedControl();
            this.label212 = new System.Windows.Forms.Label();
            this.label211 = new System.Windows.Forms.Label();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this._reservedGroupOfSetpoints = new BEMN.Forms.LedControl();
            this.label208 = new System.Windows.Forms.Label();
            this._mainGroupOfSetpoints = new BEMN.Forms.LedControl();
            this.label209 = new System.Windows.Forms.Label();
            this._fault = new BEMN.Forms.LedControl();
            this.label210 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label191 = new System.Windows.Forms.Label();
            this.label199 = new System.Windows.Forms.Label();
            this._i32Io = new BEMN.Forms.LedControl();
            this.label221 = new System.Windows.Forms.Label();
            this._i31Io = new BEMN.Forms.LedControl();
            this.label222 = new System.Windows.Forms.Label();
            this._i30Io = new BEMN.Forms.LedControl();
            this.label229 = new System.Windows.Forms.Label();
            this._i29Io = new BEMN.Forms.LedControl();
            this.label230 = new System.Windows.Forms.Label();
            this._i28Io = new BEMN.Forms.LedControl();
            this.label270 = new System.Windows.Forms.Label();
            this._i27Io = new BEMN.Forms.LedControl();
            this.label269 = new System.Windows.Forms.Label();
            this._i26Io = new BEMN.Forms.LedControl();
            this._i32 = new BEMN.Forms.LedControl();
            this._i25Io = new BEMN.Forms.LedControl();
            this.label95 = new System.Windows.Forms.Label();
            this._i31 = new BEMN.Forms.LedControl();
            this.label96 = new System.Windows.Forms.Label();
            this._i30 = new BEMN.Forms.LedControl();
            this.label97 = new System.Windows.Forms.Label();
            this._i29 = new BEMN.Forms.LedControl();
            this.label98 = new System.Windows.Forms.Label();
            this._i28 = new BEMN.Forms.LedControl();
            this.label99 = new System.Windows.Forms.Label();
            this._i27 = new BEMN.Forms.LedControl();
            this.label100 = new System.Windows.Forms.Label();
            this._i24Io = new BEMN.Forms.LedControl();
            this._i26 = new BEMN.Forms.LedControl();
            this.label101 = new System.Windows.Forms.Label();
            this._i23Io = new BEMN.Forms.LedControl();
            this._i25 = new BEMN.Forms.LedControl();
            this.label102 = new System.Windows.Forms.Label();
            this._i22Io = new BEMN.Forms.LedControl();
            this._i24 = new BEMN.Forms.LedControl();
            this.label103 = new System.Windows.Forms.Label();
            this._i21Io = new BEMN.Forms.LedControl();
            this._i23 = new BEMN.Forms.LedControl();
            this.label104 = new System.Windows.Forms.Label();
            this._i20Io = new BEMN.Forms.LedControl();
            this._i22 = new BEMN.Forms.LedControl();
            this.label105 = new System.Windows.Forms.Label();
            this._i19Io = new BEMN.Forms.LedControl();
            this._i21 = new BEMN.Forms.LedControl();
            this.label106 = new System.Windows.Forms.Label();
            this._i18Io = new BEMN.Forms.LedControl();
            this._i20 = new BEMN.Forms.LedControl();
            this.label107 = new System.Windows.Forms.Label();
            this._i17Io = new BEMN.Forms.LedControl();
            this._i19 = new BEMN.Forms.LedControl();
            this.label108 = new System.Windows.Forms.Label();
            this._i18 = new BEMN.Forms.LedControl();
            this.label109 = new System.Windows.Forms.Label();
            this._i16Io = new BEMN.Forms.LedControl();
            this._i17 = new BEMN.Forms.LedControl();
            this.label110 = new System.Windows.Forms.Label();
            this._i15Io = new BEMN.Forms.LedControl();
            this._i16 = new BEMN.Forms.LedControl();
            this.label79 = new System.Windows.Forms.Label();
            this._i14Io = new BEMN.Forms.LedControl();
            this._i15 = new BEMN.Forms.LedControl();
            this.label80 = new System.Windows.Forms.Label();
            this._i13Io = new BEMN.Forms.LedControl();
            this._i14 = new BEMN.Forms.LedControl();
            this.label81 = new System.Windows.Forms.Label();
            this._i12Io = new BEMN.Forms.LedControl();
            this._i13 = new BEMN.Forms.LedControl();
            this.label82 = new System.Windows.Forms.Label();
            this._i11Io = new BEMN.Forms.LedControl();
            this._i12 = new BEMN.Forms.LedControl();
            this.label83 = new System.Windows.Forms.Label();
            this._i10Io = new BEMN.Forms.LedControl();
            this._i11 = new BEMN.Forms.LedControl();
            this.label84 = new System.Windows.Forms.Label();
            this._i9Io = new BEMN.Forms.LedControl();
            this._i10 = new BEMN.Forms.LedControl();
            this.label85 = new System.Windows.Forms.Label();
            this._i8Io = new BEMN.Forms.LedControl();
            this._i9 = new BEMN.Forms.LedControl();
            this.label86 = new System.Windows.Forms.Label();
            this._i7Io = new BEMN.Forms.LedControl();
            this._i8 = new BEMN.Forms.LedControl();
            this.label87 = new System.Windows.Forms.Label();
            this._i6Io = new BEMN.Forms.LedControl();
            this._i7 = new BEMN.Forms.LedControl();
            this.label88 = new System.Windows.Forms.Label();
            this._i5Io = new BEMN.Forms.LedControl();
            this._i6 = new BEMN.Forms.LedControl();
            this.label89 = new System.Windows.Forms.Label();
            this._i4Io = new BEMN.Forms.LedControl();
            this._i5 = new BEMN.Forms.LedControl();
            this.label90 = new System.Windows.Forms.Label();
            this._i3Io = new BEMN.Forms.LedControl();
            this._i4 = new BEMN.Forms.LedControl();
            this.label91 = new System.Windows.Forms.Label();
            this._i2Io = new BEMN.Forms.LedControl();
            this._i3 = new BEMN.Forms.LedControl();
            this.label92 = new System.Windows.Forms.Label();
            this._i1Io = new BEMN.Forms.LedControl();
            this._i2 = new BEMN.Forms.LedControl();
            this.label93 = new System.Windows.Forms.Label();
            this._i1 = new BEMN.Forms.LedControl();
            this.label94 = new System.Windows.Forms.Label();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._ssl32 = new BEMN.Forms.LedControl();
            this.label237 = new System.Windows.Forms.Label();
            this._ssl31 = new BEMN.Forms.LedControl();
            this.label238 = new System.Windows.Forms.Label();
            this._ssl30 = new BEMN.Forms.LedControl();
            this.label239 = new System.Windows.Forms.Label();
            this._ssl29 = new BEMN.Forms.LedControl();
            this.label240 = new System.Windows.Forms.Label();
            this._ssl28 = new BEMN.Forms.LedControl();
            this.label241 = new System.Windows.Forms.Label();
            this._ssl27 = new BEMN.Forms.LedControl();
            this.label242 = new System.Windows.Forms.Label();
            this._ssl26 = new BEMN.Forms.LedControl();
            this.label243 = new System.Windows.Forms.Label();
            this._ssl25 = new BEMN.Forms.LedControl();
            this.label244 = new System.Windows.Forms.Label();
            this._ssl24 = new BEMN.Forms.LedControl();
            this.label245 = new System.Windows.Forms.Label();
            this._ssl23 = new BEMN.Forms.LedControl();
            this.label246 = new System.Windows.Forms.Label();
            this._ssl22 = new BEMN.Forms.LedControl();
            this.label247 = new System.Windows.Forms.Label();
            this._ssl21 = new BEMN.Forms.LedControl();
            this.label248 = new System.Windows.Forms.Label();
            this._ssl20 = new BEMN.Forms.LedControl();
            this.label249 = new System.Windows.Forms.Label();
            this._ssl19 = new BEMN.Forms.LedControl();
            this.label250 = new System.Windows.Forms.Label();
            this._ssl18 = new BEMN.Forms.LedControl();
            this.label251 = new System.Windows.Forms.Label();
            this._ssl17 = new BEMN.Forms.LedControl();
            this.label252 = new System.Windows.Forms.Label();
            this._ssl16 = new BEMN.Forms.LedControl();
            this.label253 = new System.Windows.Forms.Label();
            this._ssl15 = new BEMN.Forms.LedControl();
            this.label254 = new System.Windows.Forms.Label();
            this._ssl14 = new BEMN.Forms.LedControl();
            this.label255 = new System.Windows.Forms.Label();
            this._ssl13 = new BEMN.Forms.LedControl();
            this.label256 = new System.Windows.Forms.Label();
            this._ssl12 = new BEMN.Forms.LedControl();
            this.label257 = new System.Windows.Forms.Label();
            this._ssl11 = new BEMN.Forms.LedControl();
            this.label258 = new System.Windows.Forms.Label();
            this._ssl10 = new BEMN.Forms.LedControl();
            this.label259 = new System.Windows.Forms.Label();
            this._ssl9 = new BEMN.Forms.LedControl();
            this.label260 = new System.Windows.Forms.Label();
            this._ssl8 = new BEMN.Forms.LedControl();
            this.label261 = new System.Windows.Forms.Label();
            this._ssl7 = new BEMN.Forms.LedControl();
            this.label262 = new System.Windows.Forms.Label();
            this._ssl6 = new BEMN.Forms.LedControl();
            this.label263 = new System.Windows.Forms.Label();
            this._ssl5 = new BEMN.Forms.LedControl();
            this.label264 = new System.Windows.Forms.Label();
            this._ssl4 = new BEMN.Forms.LedControl();
            this.label265 = new System.Windows.Forms.Label();
            this._ssl3 = new BEMN.Forms.LedControl();
            this.label266 = new System.Windows.Forms.Label();
            this._ssl2 = new BEMN.Forms.LedControl();
            this.label267 = new System.Windows.Forms.Label();
            this._ssl1 = new BEMN.Forms.LedControl();
            this.label268 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._vz16 = new BEMN.Forms.LedControl();
            this.label111 = new System.Windows.Forms.Label();
            this._vz15 = new BEMN.Forms.LedControl();
            this.label112 = new System.Windows.Forms.Label();
            this._vz14 = new BEMN.Forms.LedControl();
            this.label113 = new System.Windows.Forms.Label();
            this._vz13 = new BEMN.Forms.LedControl();
            this.label114 = new System.Windows.Forms.Label();
            this._vz12 = new BEMN.Forms.LedControl();
            this.label115 = new System.Windows.Forms.Label();
            this._vz11 = new BEMN.Forms.LedControl();
            this.label116 = new System.Windows.Forms.Label();
            this._vz10 = new BEMN.Forms.LedControl();
            this.label117 = new System.Windows.Forms.Label();
            this._vz9 = new BEMN.Forms.LedControl();
            this.label118 = new System.Windows.Forms.Label();
            this._vz8 = new BEMN.Forms.LedControl();
            this.label119 = new System.Windows.Forms.Label();
            this._vz7 = new BEMN.Forms.LedControl();
            this.label120 = new System.Windows.Forms.Label();
            this._vz6 = new BEMN.Forms.LedControl();
            this.label121 = new System.Windows.Forms.Label();
            this._vz5 = new BEMN.Forms.LedControl();
            this.label122 = new System.Windows.Forms.Label();
            this._vz4 = new BEMN.Forms.LedControl();
            this.label123 = new System.Windows.Forms.Label();
            this._vz3 = new BEMN.Forms.LedControl();
            this.label124 = new System.Windows.Forms.Label();
            this._vz2 = new BEMN.Forms.LedControl();
            this.label125 = new System.Windows.Forms.Label();
            this._vz1 = new BEMN.Forms.LedControl();
            this.label126 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this._faultMeasuring = new BEMN.Forms.LedControl();
            this.label204 = new System.Windows.Forms.Label();
            this._faultUrov = new BEMN.Forms.LedControl();
            this.label207 = new System.Windows.Forms.Label();
            this._faultAlarmJournal = new BEMN.Forms.LedControl();
            this.label192 = new System.Windows.Forms.Label();
            this._faultOsc = new BEMN.Forms.LedControl();
            this.label193 = new System.Windows.Forms.Label();
            this._faultModule5 = new BEMN.Forms.LedControl();
            this.label194 = new System.Windows.Forms.Label();
            this._faultModule4 = new BEMN.Forms.LedControl();
            this.label195 = new System.Windows.Forms.Label();
            this._faultModule3 = new BEMN.Forms.LedControl();
            this.label196 = new System.Windows.Forms.Label();
            this._faultModule2 = new BEMN.Forms.LedControl();
            this.label197 = new System.Windows.Forms.Label();
            this._faultModule1 = new BEMN.Forms.LedControl();
            this.label198 = new System.Windows.Forms.Label();
            this._faultSystemJournal = new BEMN.Forms.LedControl();
            this.label200 = new System.Windows.Forms.Label();
            this._faultTt2 = new BEMN.Forms.LedControl();
            this._faultGroupsOfSetpoints = new BEMN.Forms.LedControl();
            this.fault2Label = new System.Windows.Forms.Label();
            this.label201 = new System.Windows.Forms.Label();
            this._faultTt1 = new BEMN.Forms.LedControl();
            this.fault1Label = new System.Windows.Forms.Label();
            this._faultSetpoints = new BEMN.Forms.LedControl();
            this._faultTt3 = new BEMN.Forms.LedControl();
            this.label202 = new System.Windows.Forms.Label();
            this.fault3Label = new System.Windows.Forms.Label();
            this._faultLogic = new BEMN.Forms.LedControl();
            this.label203 = new System.Windows.Forms.Label();
            this._faultSoftware = new BEMN.Forms.LedControl();
            this.label205 = new System.Windows.Forms.Label();
            this._faultHardware = new BEMN.Forms.LedControl();
            this.label206 = new System.Windows.Forms.Label();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._indicator11 = new BEMN.Forms.LedControl();
            this.label190 = new System.Windows.Forms.Label();
            this._indicator6 = new BEMN.Forms.LedControl();
            this.label189 = new System.Windows.Forms.Label();
            this._indicator10 = new BEMN.Forms.LedControl();
            this.label179 = new System.Windows.Forms.Label();
            this._indicator9 = new BEMN.Forms.LedControl();
            this.label180 = new System.Windows.Forms.Label();
            this._indicator8 = new BEMN.Forms.LedControl();
            this.label181 = new System.Windows.Forms.Label();
            this._indicator7 = new BEMN.Forms.LedControl();
            this.label182 = new System.Windows.Forms.Label();
            this._indicator12 = new BEMN.Forms.LedControl();
            this.label183 = new System.Windows.Forms.Label();
            this._indicator5 = new BEMN.Forms.LedControl();
            this.label184 = new System.Windows.Forms.Label();
            this._indicator4 = new BEMN.Forms.LedControl();
            this.label185 = new System.Windows.Forms.Label();
            this._indicator3 = new BEMN.Forms.LedControl();
            this.label186 = new System.Windows.Forms.Label();
            this._indicator2 = new BEMN.Forms.LedControl();
            this.label187 = new System.Windows.Forms.Label();
            this._indicator1 = new BEMN.Forms.LedControl();
            this.label188 = new System.Windows.Forms.Label();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this._module10 = new BEMN.Forms.LedControl();
            this.label164 = new System.Windows.Forms.Label();
            this._module9 = new BEMN.Forms.LedControl();
            this.label165 = new System.Windows.Forms.Label();
            this._module8 = new BEMN.Forms.LedControl();
            this.label166 = new System.Windows.Forms.Label();
            this._module7 = new BEMN.Forms.LedControl();
            this.label167 = new System.Windows.Forms.Label();
            this._module6 = new BEMN.Forms.LedControl();
            this.label168 = new System.Windows.Forms.Label();
            this._module5 = new BEMN.Forms.LedControl();
            this.label172 = new System.Windows.Forms.Label();
            this._module4 = new BEMN.Forms.LedControl();
            this.label173 = new System.Windows.Forms.Label();
            this._module3 = new BEMN.Forms.LedControl();
            this.label174 = new System.Windows.Forms.Label();
            this._module2 = new BEMN.Forms.LedControl();
            this.label175 = new System.Windows.Forms.Label();
            this._module1 = new BEMN.Forms.LedControl();
            this.label176 = new System.Windows.Forms.Label();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this._module18 = new BEMN.Forms.LedControl();
            this.label161 = new System.Windows.Forms.Label();
            this._module17 = new BEMN.Forms.LedControl();
            this.label162 = new System.Windows.Forms.Label();
            this._module16 = new BEMN.Forms.LedControl();
            this.label163 = new System.Windows.Forms.Label();
            this._module15 = new BEMN.Forms.LedControl();
            this.label169 = new System.Windows.Forms.Label();
            this._module14 = new BEMN.Forms.LedControl();
            this.label170 = new System.Windows.Forms.Label();
            this._module13 = new BEMN.Forms.LedControl();
            this.label171 = new System.Windows.Forms.Label();
            this._module12 = new BEMN.Forms.LedControl();
            this.label177 = new System.Windows.Forms.Label();
            this._module11 = new BEMN.Forms.LedControl();
            this.label178 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this._urovPO = new BEMN.Forms.LedControl();
            this.label158 = new System.Windows.Forms.Label();
            this._urovSH2 = new BEMN.Forms.LedControl();
            this.label159 = new System.Windows.Forms.Label();
            this._urovSH1 = new BEMN.Forms.LedControl();
            this.label160 = new System.Windows.Forms.Label();
            this._urovPr6 = new BEMN.Forms.LedControl();
            this.label144 = new System.Windows.Forms.Label();
            this._urovPr5 = new BEMN.Forms.LedControl();
            this.label145 = new System.Windows.Forms.Label();
            this._urovPr4 = new BEMN.Forms.LedControl();
            this.label146 = new System.Windows.Forms.Label();
            this._urovPr3 = new BEMN.Forms.LedControl();
            this.label147 = new System.Windows.Forms.Label();
            this._urovPr2 = new BEMN.Forms.LedControl();
            this.label148 = new System.Windows.Forms.Label();
            this._urovPr1 = new BEMN.Forms.LedControl();
            this.label149 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._vls16 = new BEMN.Forms.LedControl();
            this.label63 = new System.Windows.Forms.Label();
            this._vls15 = new BEMN.Forms.LedControl();
            this.label64 = new System.Windows.Forms.Label();
            this._vls14 = new BEMN.Forms.LedControl();
            this.label65 = new System.Windows.Forms.Label();
            this._vls13 = new BEMN.Forms.LedControl();
            this.label66 = new System.Windows.Forms.Label();
            this._vls12 = new BEMN.Forms.LedControl();
            this.label67 = new System.Windows.Forms.Label();
            this._vls11 = new BEMN.Forms.LedControl();
            this.label68 = new System.Windows.Forms.Label();
            this._vls10 = new BEMN.Forms.LedControl();
            this.label69 = new System.Windows.Forms.Label();
            this._vls9 = new BEMN.Forms.LedControl();
            this.label70 = new System.Windows.Forms.Label();
            this._vls8 = new BEMN.Forms.LedControl();
            this.label71 = new System.Windows.Forms.Label();
            this._vls7 = new BEMN.Forms.LedControl();
            this.label72 = new System.Windows.Forms.Label();
            this._vls6 = new BEMN.Forms.LedControl();
            this.label73 = new System.Windows.Forms.Label();
            this._vls5 = new BEMN.Forms.LedControl();
            this.label74 = new System.Windows.Forms.Label();
            this._vls4 = new BEMN.Forms.LedControl();
            this.label75 = new System.Windows.Forms.Label();
            this._vls3 = new BEMN.Forms.LedControl();
            this.label76 = new System.Windows.Forms.Label();
            this._vls2 = new BEMN.Forms.LedControl();
            this.label77 = new System.Windows.Forms.Label();
            this._vls1 = new BEMN.Forms.LedControl();
            this.label78 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._ls16 = new BEMN.Forms.LedControl();
            this.label47 = new System.Windows.Forms.Label();
            this._ls15 = new BEMN.Forms.LedControl();
            this.label48 = new System.Windows.Forms.Label();
            this._ls14 = new BEMN.Forms.LedControl();
            this.label49 = new System.Windows.Forms.Label();
            this._ls13 = new BEMN.Forms.LedControl();
            this.label50 = new System.Windows.Forms.Label();
            this._ls12 = new BEMN.Forms.LedControl();
            this.label51 = new System.Windows.Forms.Label();
            this._ls11 = new BEMN.Forms.LedControl();
            this.label52 = new System.Windows.Forms.Label();
            this._ls10 = new BEMN.Forms.LedControl();
            this.label53 = new System.Windows.Forms.Label();
            this._ls9 = new BEMN.Forms.LedControl();
            this.label54 = new System.Windows.Forms.Label();
            this._ls8 = new BEMN.Forms.LedControl();
            this.label55 = new System.Windows.Forms.Label();
            this._ls7 = new BEMN.Forms.LedControl();
            this.label56 = new System.Windows.Forms.Label();
            this._ls6 = new BEMN.Forms.LedControl();
            this.label57 = new System.Windows.Forms.Label();
            this._ls5 = new BEMN.Forms.LedControl();
            this.label58 = new System.Windows.Forms.Label();
            this._ls4 = new BEMN.Forms.LedControl();
            this.label59 = new System.Windows.Forms.Label();
            this._ls3 = new BEMN.Forms.LedControl();
            this.label60 = new System.Windows.Forms.Label();
            this._ls2 = new BEMN.Forms.LedControl();
            this.label61 = new System.Windows.Forms.Label();
            this._ls1 = new BEMN.Forms.LedControl();
            this.label62 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._d24 = new BEMN.Forms.LedControl();
            this.label39 = new System.Windows.Forms.Label();
            this._d23 = new BEMN.Forms.LedControl();
            this.label40 = new System.Windows.Forms.Label();
            this._d22 = new BEMN.Forms.LedControl();
            this.label41 = new System.Windows.Forms.Label();
            this._d21 = new BEMN.Forms.LedControl();
            this.label42 = new System.Windows.Forms.Label();
            this._d20 = new BEMN.Forms.LedControl();
            this.label43 = new System.Windows.Forms.Label();
            this._d19 = new BEMN.Forms.LedControl();
            this.label44 = new System.Windows.Forms.Label();
            this._d18 = new BEMN.Forms.LedControl();
            this.label45 = new System.Windows.Forms.Label();
            this._d17 = new BEMN.Forms.LedControl();
            this.label46 = new System.Windows.Forms.Label();
            this._d16 = new BEMN.Forms.LedControl();
            this.label31 = new System.Windows.Forms.Label();
            this._d15 = new BEMN.Forms.LedControl();
            this.label32 = new System.Windows.Forms.Label();
            this._d14 = new BEMN.Forms.LedControl();
            this.label33 = new System.Windows.Forms.Label();
            this._d13 = new BEMN.Forms.LedControl();
            this.label34 = new System.Windows.Forms.Label();
            this._d12 = new BEMN.Forms.LedControl();
            this.label35 = new System.Windows.Forms.Label();
            this._d11 = new BEMN.Forms.LedControl();
            this.label36 = new System.Windows.Forms.Label();
            this._d10 = new BEMN.Forms.LedControl();
            this.label37 = new System.Windows.Forms.Label();
            this._d9 = new BEMN.Forms.LedControl();
            this.label38 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._d8 = new BEMN.Forms.LedControl();
            this.label30 = new System.Windows.Forms.Label();
            this._d7 = new BEMN.Forms.LedControl();
            this.label29 = new System.Windows.Forms.Label();
            this._d6 = new BEMN.Forms.LedControl();
            this.label28 = new System.Windows.Forms.Label();
            this._d5 = new BEMN.Forms.LedControl();
            this.label27 = new System.Windows.Forms.Label();
            this._d4 = new BEMN.Forms.LedControl();
            this.label26 = new System.Windows.Forms.Label();
            this._d3 = new BEMN.Forms.LedControl();
            this.label25 = new System.Windows.Forms.Label();
            this._d2 = new BEMN.Forms.LedControl();
            this.label24 = new System.Windows.Forms.Label();
            this._d1 = new BEMN.Forms.LedControl();
            this.label23 = new System.Windows.Forms.Label();
            this._controlSignalsTabPage = new System.Windows.Forms.TabPage();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this._reservedControl = new BEMN.Forms.LedControl();
            this._reserveGroupButton = new System.Windows.Forms.Button();
            this._mainGroupButton = new System.Windows.Forms.Button();
            this.label224 = new System.Windows.Forms.Label();
            this.label228 = new System.Windows.Forms.Label();
            this._reservedGroupOfSetpointsFromInterface = new BEMN.Forms.LedControl();
            this._mainGroupOfSetpointsFromInterface = new BEMN.Forms.LedControl();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.groupBox40 = new System.Windows.Forms.GroupBox();
            this._logicState = new BEMN.Forms.LedControl();
            this.stopLogic = new System.Windows.Forms.Button();
            this.startLogic = new System.Windows.Forms.Button();
            this.label314 = new System.Windows.Forms.Label();
            this._availabilityFaultSystemJournal = new BEMN.Forms.LedControl();
            this._newRecordOscJournal = new BEMN.Forms.LedControl();
            this._oscStart = new System.Windows.Forms.Button();
            this._resetTt = new System.Windows.Forms.Button();
            this._newRecordAlarmJournal = new BEMN.Forms.LedControl();
            this._newRecordSystemJournal = new BEMN.Forms.LedControl();
            this._resetAnButton = new System.Windows.Forms.Button();
            this._resetAvailabilityFaultSystemJournalButton = new System.Windows.Forms.Button();
            this._resetOscJournalButton = new System.Windows.Forms.Button();
            this._resetAlarmJournalButton = new System.Windows.Forms.Button();
            this._resetSystemJournalButton = new System.Windows.Forms.Button();
            this.label227 = new System.Windows.Forms.Label();
            this.label225 = new System.Windows.Forms.Label();
            this.label226 = new System.Windows.Forms.Label();
            this.label231 = new System.Windows.Forms.Label();
            this._dataBaseTabControl.SuspendLayout();
            this._analogTabPage.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this._discretTabPage.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this._controlSignalsTabPage.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox40.SuspendLayout();
            this.SuspendLayout();
            // 
            // _dataBaseTabControl
            // 
            this._dataBaseTabControl.Controls.Add(this._analogTabPage);
            this._dataBaseTabControl.Controls.Add(this._discretTabPage);
            this._dataBaseTabControl.Controls.Add(this._controlSignalsTabPage);
            this._dataBaseTabControl.Location = new System.Drawing.Point(0, 0);
            this._dataBaseTabControl.Name = "_dataBaseTabControl";
            this._dataBaseTabControl.SelectedIndex = 0;
            this._dataBaseTabControl.Size = new System.Drawing.Size(835, 685);
            this._dataBaseTabControl.TabIndex = 1;
            // 
            // _analogTabPage
            // 
            this._analogTabPage.Controls.Add(this._dateTimeControl);
            this._analogTabPage.Controls.Add(this.groupBox5);
            this._analogTabPage.Controls.Add(this.groupBox1);
            this._analogTabPage.Location = new System.Drawing.Point(4, 22);
            this._analogTabPage.Name = "_analogTabPage";
            this._analogTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._analogTabPage.Size = new System.Drawing.Size(827, 659);
            this._analogTabPage.TabIndex = 0;
            this._analogTabPage.Text = "Аналоговая БД";
            this._analogTabPage.UseVisualStyleBackColor = true;
            // 
            // _dateTimeControl
            // 
            this._dateTimeControl.Location = new System.Drawing.Point(6, 302);
            this._dateTimeControl.Name = "_dateTimeControl";
            this._dateTimeControl.Size = new System.Drawing.Size(201, 166);
            this._dateTimeControl.TabIndex = 43;
            this._dateTimeControl.TimeChanged += new System.Action(this.dateTimeControl_TimeChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.groupBox31);
            this.groupBox5.Controls.Add(this.groupBox30);
            this.groupBox5.Controls.Add(this.groupBox29);
            this.groupBox5.Controls.Add(this.groupBox26);
            this.groupBox5.Controls.Add(this.groupBox25);
            this.groupBox5.Controls.Add(this.groupBox13);
            this.groupBox5.Location = new System.Drawing.Point(6, 159);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(811, 137);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Токи присоединений";
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this._i16TB);
            this.groupBox31.Controls.Add(this.label290);
            this.groupBox31.Location = new System.Drawing.Point(670, 19);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(125, 52);
            this.groupBox31.TabIndex = 35;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "Присоед. ln";
            // 
            // _i16TB
            // 
            this._i16TB.Location = new System.Drawing.Point(34, 23);
            this._i16TB.Name = "_i16TB";
            this._i16TB.ReadOnly = true;
            this._i16TB.Size = new System.Drawing.Size(61, 20);
            this._i16TB.TabIndex = 1;
            // 
            // label290
            // 
            this.label290.AutoSize = true;
            this.label290.Location = new System.Drawing.Point(6, 26);
            this.label290.Name = "label290";
            this.label290.Size = new System.Drawing.Size(16, 13);
            this.label290.TabIndex = 0;
            this.label290.Text = "In";
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.label276);
            this.groupBox30.Controls.Add(this.label283);
            this.groupBox30.Controls.Add(this.label284);
            this.groupBox30.Controls.Add(this._i15TB);
            this.groupBox30.Controls.Add(this._i14TB);
            this.groupBox30.Controls.Add(this._i13TB);
            this.groupBox30.Location = new System.Drawing.Point(539, 19);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(125, 108);
            this.groupBox30.TabIndex = 38;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "Присоед. 5";
            // 
            // label276
            // 
            this.label276.AutoSize = true;
            this.label276.Location = new System.Drawing.Point(12, 78);
            this.label276.Name = "label276";
            this.label276.Size = new System.Drawing.Size(16, 13);
            this.label276.TabIndex = 14;
            this.label276.Text = "Ic";
            // 
            // label283
            // 
            this.label283.AutoSize = true;
            this.label283.Location = new System.Drawing.Point(12, 52);
            this.label283.Name = "label283";
            this.label283.Size = new System.Drawing.Size(16, 13);
            this.label283.TabIndex = 13;
            this.label283.Text = "Ib";
            // 
            // label284
            // 
            this.label284.AutoSize = true;
            this.label284.Location = new System.Drawing.Point(12, 26);
            this.label284.Name = "label284";
            this.label284.Size = new System.Drawing.Size(16, 13);
            this.label284.TabIndex = 12;
            this.label284.Text = "Ia";
            // 
            // _i15TB
            // 
            this._i15TB.Location = new System.Drawing.Point(34, 75);
            this._i15TB.Name = "_i15TB";
            this._i15TB.ReadOnly = true;
            this._i15TB.Size = new System.Drawing.Size(61, 20);
            this._i15TB.TabIndex = 5;
            // 
            // _i14TB
            // 
            this._i14TB.Location = new System.Drawing.Point(34, 49);
            this._i14TB.Name = "_i14TB";
            this._i14TB.ReadOnly = true;
            this._i14TB.Size = new System.Drawing.Size(61, 20);
            this._i14TB.TabIndex = 3;
            // 
            // _i13TB
            // 
            this._i13TB.Location = new System.Drawing.Point(34, 23);
            this._i13TB.Name = "_i13TB";
            this._i13TB.ReadOnly = true;
            this._i13TB.Size = new System.Drawing.Size(61, 20);
            this._i13TB.TabIndex = 1;
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.label22);
            this.groupBox29.Controls.Add(this.label271);
            this.groupBox29.Controls.Add(this.label272);
            this.groupBox29.Controls.Add(this._i12TB);
            this.groupBox29.Controls.Add(this._i11TB);
            this.groupBox29.Controls.Add(this._i10TB);
            this.groupBox29.Location = new System.Drawing.Point(408, 19);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(125, 108);
            this.groupBox29.TabIndex = 37;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "Присоед. 4";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(12, 78);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(16, 13);
            this.label22.TabIndex = 11;
            this.label22.Text = "Ic";
            // 
            // label271
            // 
            this.label271.AutoSize = true;
            this.label271.Location = new System.Drawing.Point(12, 52);
            this.label271.Name = "label271";
            this.label271.Size = new System.Drawing.Size(16, 13);
            this.label271.TabIndex = 10;
            this.label271.Text = "Ib";
            // 
            // label272
            // 
            this.label272.AutoSize = true;
            this.label272.Location = new System.Drawing.Point(12, 26);
            this.label272.Name = "label272";
            this.label272.Size = new System.Drawing.Size(16, 13);
            this.label272.TabIndex = 9;
            this.label272.Text = "Ia";
            // 
            // _i12TB
            // 
            this._i12TB.Location = new System.Drawing.Point(34, 75);
            this._i12TB.Name = "_i12TB";
            this._i12TB.ReadOnly = true;
            this._i12TB.Size = new System.Drawing.Size(61, 20);
            this._i12TB.TabIndex = 5;
            // 
            // _i11TB
            // 
            this._i11TB.Location = new System.Drawing.Point(34, 49);
            this._i11TB.Name = "_i11TB";
            this._i11TB.ReadOnly = true;
            this._i11TB.Size = new System.Drawing.Size(61, 20);
            this._i11TB.TabIndex = 3;
            // 
            // _i10TB
            // 
            this._i10TB.Location = new System.Drawing.Point(34, 23);
            this._i10TB.Name = "_i10TB";
            this._i10TB.ReadOnly = true;
            this._i10TB.Size = new System.Drawing.Size(61, 20);
            this._i10TB.TabIndex = 1;
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.label16);
            this.groupBox26.Controls.Add(this.label17);
            this.groupBox26.Controls.Add(this.label18);
            this.groupBox26.Controls.Add(this._i9TB);
            this.groupBox26.Controls.Add(this._i8TB);
            this.groupBox26.Controls.Add(this._i7TB);
            this.groupBox26.Location = new System.Drawing.Point(277, 19);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(125, 108);
            this.groupBox26.TabIndex = 36;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Присоед. 3";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 78);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(16, 13);
            this.label16.TabIndex = 11;
            this.label16.Text = "Ic";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 52);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(16, 13);
            this.label17.TabIndex = 10;
            this.label17.Text = "Ib";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(12, 26);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(16, 13);
            this.label18.TabIndex = 9;
            this.label18.Text = "Ia";
            // 
            // _i9TB
            // 
            this._i9TB.Location = new System.Drawing.Point(34, 75);
            this._i9TB.Name = "_i9TB";
            this._i9TB.ReadOnly = true;
            this._i9TB.Size = new System.Drawing.Size(61, 20);
            this._i9TB.TabIndex = 5;
            // 
            // _i8TB
            // 
            this._i8TB.Location = new System.Drawing.Point(34, 49);
            this._i8TB.Name = "_i8TB";
            this._i8TB.ReadOnly = true;
            this._i8TB.Size = new System.Drawing.Size(61, 20);
            this._i8TB.TabIndex = 3;
            // 
            // _i7TB
            // 
            this._i7TB.Location = new System.Drawing.Point(34, 23);
            this._i7TB.Name = "_i7TB";
            this._i7TB.ReadOnly = true;
            this._i7TB.Size = new System.Drawing.Size(61, 20);
            this._i7TB.TabIndex = 1;
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.label10);
            this.groupBox25.Controls.Add(this.label11);
            this.groupBox25.Controls.Add(this.label12);
            this.groupBox25.Controls.Add(this._i6TB);
            this.groupBox25.Controls.Add(this._i5TB);
            this.groupBox25.Controls.Add(this._i4TB);
            this.groupBox25.Location = new System.Drawing.Point(146, 19);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(125, 108);
            this.groupBox25.TabIndex = 35;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "Присоед. 2";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Ic";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 52);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Ib";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(16, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "Ia";
            // 
            // _i6TB
            // 
            this._i6TB.Location = new System.Drawing.Point(34, 75);
            this._i6TB.Name = "_i6TB";
            this._i6TB.ReadOnly = true;
            this._i6TB.Size = new System.Drawing.Size(61, 20);
            this._i6TB.TabIndex = 5;
            // 
            // _i5TB
            // 
            this._i5TB.Location = new System.Drawing.Point(34, 49);
            this._i5TB.Name = "_i5TB";
            this._i5TB.ReadOnly = true;
            this._i5TB.Size = new System.Drawing.Size(61, 20);
            this._i5TB.TabIndex = 3;
            // 
            // _i4TB
            // 
            this._i4TB.Location = new System.Drawing.Point(34, 23);
            this._i4TB.Name = "_i4TB";
            this._i4TB.ReadOnly = true;
            this._i4TB.Size = new System.Drawing.Size(61, 20);
            this._i4TB.TabIndex = 1;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this._i3TB);
            this.groupBox13.Controls.Add(this.label280);
            this.groupBox13.Controls.Add(this._i2TB);
            this.groupBox13.Controls.Add(this.label281);
            this.groupBox13.Controls.Add(this._i1TB);
            this.groupBox13.Controls.Add(this.label282);
            this.groupBox13.Location = new System.Drawing.Point(15, 19);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(125, 108);
            this.groupBox13.TabIndex = 34;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Присоед. 1";
            // 
            // _i3TB
            // 
            this._i3TB.Location = new System.Drawing.Point(34, 75);
            this._i3TB.Name = "_i3TB";
            this._i3TB.ReadOnly = true;
            this._i3TB.Size = new System.Drawing.Size(61, 20);
            this._i3TB.TabIndex = 5;
            // 
            // label280
            // 
            this.label280.AutoSize = true;
            this.label280.Location = new System.Drawing.Point(12, 78);
            this.label280.Name = "label280";
            this.label280.Size = new System.Drawing.Size(16, 13);
            this.label280.TabIndex = 4;
            this.label280.Text = "Ic";
            // 
            // _i2TB
            // 
            this._i2TB.Location = new System.Drawing.Point(34, 49);
            this._i2TB.Name = "_i2TB";
            this._i2TB.ReadOnly = true;
            this._i2TB.Size = new System.Drawing.Size(61, 20);
            this._i2TB.TabIndex = 3;
            // 
            // label281
            // 
            this.label281.AutoSize = true;
            this.label281.Location = new System.Drawing.Point(12, 52);
            this.label281.Name = "label281";
            this.label281.Size = new System.Drawing.Size(16, 13);
            this.label281.TabIndex = 2;
            this.label281.Text = "Ib";
            // 
            // _i1TB
            // 
            this._i1TB.Location = new System.Drawing.Point(34, 23);
            this._i1TB.Name = "_i1TB";
            this._i1TB.ReadOnly = true;
            this._i1TB.Size = new System.Drawing.Size(61, 20);
            this._i1TB.TabIndex = 1;
            // 
            // label282
            // 
            this.label282.AutoSize = true;
            this.label282.Location = new System.Drawing.Point(12, 26);
            this.label282.Name = "label282";
            this.label282.Size = new System.Drawing.Size(16, 13);
            this.label282.TabIndex = 0;
            this.label282.Text = "Ia";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(6, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(811, 145);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Дифференциальные и тормозные токи";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._itc3TB);
            this.groupBox4.Controls.Add(this.label154);
            this.groupBox4.Controls.Add(this._itb3TB);
            this.groupBox4.Controls.Add(this.label155);
            this.groupBox4.Controls.Add(this._ita3TB);
            this.groupBox4.Controls.Add(this.label156);
            this.groupBox4.Controls.Add(this._idc3TB);
            this.groupBox4.Controls.Add(this.label234);
            this.groupBox4.Controls.Add(this._idb3TB);
            this.groupBox4.Controls.Add(this.label235);
            this.groupBox4.Controls.Add(this._ida3TB);
            this.groupBox4.Controls.Add(this.label236);
            this.groupBox4.Location = new System.Drawing.Point(544, 27);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(259, 108);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "ПО";
            // 
            // _itc3TB
            // 
            this._itc3TB.Location = new System.Drawing.Point(168, 75);
            this._itc3TB.Name = "_itc3TB";
            this._itc3TB.ReadOnly = true;
            this._itc3TB.Size = new System.Drawing.Size(61, 20);
            this._itc3TB.TabIndex = 14;
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(140, 78);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(21, 13);
            this.label154.TabIndex = 13;
            this.label154.Text = "Iтc";
            // 
            // _itb3TB
            // 
            this._itb3TB.Location = new System.Drawing.Point(168, 49);
            this._itb3TB.Name = "_itb3TB";
            this._itb3TB.ReadOnly = true;
            this._itb3TB.Size = new System.Drawing.Size(61, 20);
            this._itb3TB.TabIndex = 12;
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(140, 52);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(21, 13);
            this.label155.TabIndex = 11;
            this.label155.Text = "Iтb";
            // 
            // _ita3TB
            // 
            this._ita3TB.Location = new System.Drawing.Point(168, 23);
            this._ita3TB.Name = "_ita3TB";
            this._ita3TB.ReadOnly = true;
            this._ita3TB.Size = new System.Drawing.Size(61, 20);
            this._ita3TB.TabIndex = 10;
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(140, 26);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(21, 13);
            this.label156.TabIndex = 9;
            this.label156.Text = "Iтa";
            // 
            // _idc3TB
            // 
            this._idc3TB.Location = new System.Drawing.Point(34, 75);
            this._idc3TB.Name = "_idc3TB";
            this._idc3TB.ReadOnly = true;
            this._idc3TB.Size = new System.Drawing.Size(61, 20);
            this._idc3TB.TabIndex = 5;
            // 
            // label234
            // 
            this.label234.AutoSize = true;
            this.label234.Location = new System.Drawing.Point(6, 78);
            this.label234.Name = "label234";
            this.label234.Size = new System.Drawing.Size(22, 13);
            this.label234.TabIndex = 4;
            this.label234.Text = "Iдc";
            // 
            // _idb3TB
            // 
            this._idb3TB.Location = new System.Drawing.Point(34, 49);
            this._idb3TB.Name = "_idb3TB";
            this._idb3TB.ReadOnly = true;
            this._idb3TB.Size = new System.Drawing.Size(61, 20);
            this._idb3TB.TabIndex = 3;
            // 
            // label235
            // 
            this.label235.AutoSize = true;
            this.label235.Location = new System.Drawing.Point(6, 52);
            this.label235.Name = "label235";
            this.label235.Size = new System.Drawing.Size(22, 13);
            this.label235.TabIndex = 2;
            this.label235.Text = "Iдb";
            // 
            // _ida3TB
            // 
            this._ida3TB.Location = new System.Drawing.Point(34, 23);
            this._ida3TB.Name = "_ida3TB";
            this._ida3TB.ReadOnly = true;
            this._ida3TB.Size = new System.Drawing.Size(61, 20);
            this._ida3TB.TabIndex = 1;
            // 
            // label236
            // 
            this.label236.AutoSize = true;
            this.label236.Location = new System.Drawing.Point(6, 26);
            this.label236.Name = "label236";
            this.label236.Size = new System.Drawing.Size(22, 13);
            this.label236.TabIndex = 0;
            this.label236.Text = "Iдa";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._itc2TB);
            this.groupBox3.Controls.Add(this.label136);
            this.groupBox3.Controls.Add(this._itb2TB);
            this.groupBox3.Controls.Add(this.label137);
            this.groupBox3.Controls.Add(this._ita2TB);
            this.groupBox3.Controls.Add(this.label138);
            this.groupBox3.Controls.Add(this._idc2TB);
            this.groupBox3.Controls.Add(this.label142);
            this.groupBox3.Controls.Add(this._idb2TB);
            this.groupBox3.Controls.Add(this.label143);
            this.groupBox3.Controls.Add(this._ida2TB);
            this.groupBox3.Controls.Add(this.label150);
            this.groupBox3.Location = new System.Drawing.Point(279, 27);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(259, 108);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "СШ2";
            // 
            // _itc2TB
            // 
            this._itc2TB.Location = new System.Drawing.Point(168, 75);
            this._itc2TB.Name = "_itc2TB";
            this._itc2TB.ReadOnly = true;
            this._itc2TB.Size = new System.Drawing.Size(61, 20);
            this._itc2TB.TabIndex = 14;
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(140, 78);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(21, 13);
            this.label136.TabIndex = 13;
            this.label136.Text = "Iтc";
            // 
            // _itb2TB
            // 
            this._itb2TB.Location = new System.Drawing.Point(168, 49);
            this._itb2TB.Name = "_itb2TB";
            this._itb2TB.ReadOnly = true;
            this._itb2TB.Size = new System.Drawing.Size(61, 20);
            this._itb2TB.TabIndex = 12;
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(140, 52);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(21, 13);
            this.label137.TabIndex = 11;
            this.label137.Text = "Iтb";
            // 
            // _ita2TB
            // 
            this._ita2TB.Location = new System.Drawing.Point(168, 23);
            this._ita2TB.Name = "_ita2TB";
            this._ita2TB.ReadOnly = true;
            this._ita2TB.Size = new System.Drawing.Size(61, 20);
            this._ita2TB.TabIndex = 10;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(140, 26);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(21, 13);
            this.label138.TabIndex = 9;
            this.label138.Text = "Iтa";
            // 
            // _idc2TB
            // 
            this._idc2TB.Location = new System.Drawing.Point(34, 75);
            this._idc2TB.Name = "_idc2TB";
            this._idc2TB.ReadOnly = true;
            this._idc2TB.Size = new System.Drawing.Size(61, 20);
            this._idc2TB.TabIndex = 5;
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(6, 78);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(22, 13);
            this.label142.TabIndex = 4;
            this.label142.Text = "Iдc";
            // 
            // _idb2TB
            // 
            this._idb2TB.Location = new System.Drawing.Point(34, 49);
            this._idb2TB.Name = "_idb2TB";
            this._idb2TB.ReadOnly = true;
            this._idb2TB.Size = new System.Drawing.Size(61, 20);
            this._idb2TB.TabIndex = 3;
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(6, 52);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(22, 13);
            this.label143.TabIndex = 2;
            this.label143.Text = "Iдb";
            // 
            // _ida2TB
            // 
            this._ida2TB.Location = new System.Drawing.Point(34, 23);
            this._ida2TB.Name = "_ida2TB";
            this._ida2TB.ReadOnly = true;
            this._ida2TB.Size = new System.Drawing.Size(61, 20);
            this._ida2TB.TabIndex = 1;
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(6, 26);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(22, 13);
            this.label150.TabIndex = 0;
            this.label150.Text = "Iдa";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._itc1TB);
            this.groupBox2.Controls.Add(this.label130);
            this.groupBox2.Controls.Add(this._itb1TB);
            this.groupBox2.Controls.Add(this.label131);
            this.groupBox2.Controls.Add(this._ita1TB);
            this.groupBox2.Controls.Add(this.label132);
            this.groupBox2.Controls.Add(this._idc1TB);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this._idb1TB);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this._ida1TB);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(6, 27);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(259, 108);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "СШ1";
            // 
            // _itc1TB
            // 
            this._itc1TB.Location = new System.Drawing.Point(168, 75);
            this._itc1TB.Name = "_itc1TB";
            this._itc1TB.ReadOnly = true;
            this._itc1TB.Size = new System.Drawing.Size(61, 20);
            this._itc1TB.TabIndex = 14;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(140, 78);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(21, 13);
            this.label130.TabIndex = 13;
            this.label130.Text = "Iтc";
            // 
            // _itb1TB
            // 
            this._itb1TB.Location = new System.Drawing.Point(168, 49);
            this._itb1TB.Name = "_itb1TB";
            this._itb1TB.ReadOnly = true;
            this._itb1TB.Size = new System.Drawing.Size(61, 20);
            this._itb1TB.TabIndex = 12;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(140, 52);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(21, 13);
            this.label131.TabIndex = 11;
            this.label131.Text = "Iтb";
            // 
            // _ita1TB
            // 
            this._ita1TB.Location = new System.Drawing.Point(168, 23);
            this._ita1TB.Name = "_ita1TB";
            this._ita1TB.ReadOnly = true;
            this._ita1TB.Size = new System.Drawing.Size(61, 20);
            this._ita1TB.TabIndex = 10;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(140, 26);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(21, 13);
            this.label132.TabIndex = 9;
            this.label132.Text = "Iтa";
            // 
            // _idc1TB
            // 
            this._idc1TB.Location = new System.Drawing.Point(34, 75);
            this._idc1TB.Name = "_idc1TB";
            this._idc1TB.ReadOnly = true;
            this._idc1TB.Size = new System.Drawing.Size(61, 20);
            this._idc1TB.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Iдc";
            // 
            // _idb1TB
            // 
            this._idb1TB.Location = new System.Drawing.Point(34, 49);
            this._idb1TB.Name = "_idb1TB";
            this._idb1TB.ReadOnly = true;
            this._idb1TB.Size = new System.Drawing.Size(61, 20);
            this._idb1TB.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Iдb";
            // 
            // _ida1TB
            // 
            this._ida1TB.Location = new System.Drawing.Point(34, 23);
            this._ida1TB.Name = "_ida1TB";
            this._ida1TB.ReadOnly = true;
            this._ida1TB.Size = new System.Drawing.Size(61, 20);
            this._ida1TB.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Iдa";
            // 
            // _discretTabPage
            // 
            this._discretTabPage.Controls.Add(this.groupBox22);
            this._discretTabPage.Controls.Add(this.groupBox20);
            this._discretTabPage.Controls.Add(this.groupBox11);
            this._discretTabPage.Controls.Add(this.groupBox21);
            this._discretTabPage.Controls.Add(this.groupBox12);
            this._discretTabPage.Controls.Add(this.groupBox19);
            this._discretTabPage.Controls.Add(this.groupBox18);
            this._discretTabPage.Controls.Add(this.groupBox17);
            this._discretTabPage.Controls.Add(this.groupBox14);
            this._discretTabPage.Controls.Add(this.groupBox10);
            this._discretTabPage.Controls.Add(this.groupBox9);
            this._discretTabPage.Controls.Add(this.groupBox6);
            this._discretTabPage.Location = new System.Drawing.Point(4, 22);
            this._discretTabPage.Name = "_discretTabPage";
            this._discretTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._discretTabPage.Size = new System.Drawing.Size(827, 659);
            this._discretTabPage.TabIndex = 1;
            this._discretTabPage.Text = "Дискретная БД";
            this._discretTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.groupBox24);
            this.groupBox22.Controls.Add(this.groupBox23);
            this.groupBox22.Location = new System.Drawing.Point(77, 415);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(173, 235);
            this.groupBox22.TabIndex = 16;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Дифференциальные защиты";
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.label223);
            this.groupBox24.Controls.Add(this._difDefenceActualIoPo);
            this.groupBox24.Controls.Add(this._difDefenceActualIoSh2);
            this.groupBox24.Controls.Add(this._difDefenceActualIoSh1);
            this.groupBox24.Controls.Add(this.label216);
            this.groupBox24.Controls.Add(this.label217);
            this.groupBox24.Controls.Add(this.label218);
            this.groupBox24.Controls.Add(this._difDefenceActualChtoPo);
            this.groupBox24.Controls.Add(this._difDefenceActualChtoSh2);
            this.groupBox24.Controls.Add(this._difDefenceActualChtoSh1);
            this.groupBox24.Controls.Add(this._difDefenceActualSrabPo);
            this.groupBox24.Controls.Add(this._difDefenceActualSrabSh2);
            this.groupBox24.Controls.Add(this._difDefenceActualSrabSh1);
            this.groupBox24.Controls.Add(this.label219);
            this.groupBox24.Controls.Add(this.label220);
            this.groupBox24.Location = new System.Drawing.Point(6, 123);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(136, 98);
            this.groupBox24.TabIndex = 1;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "По действ. значениям";
            // 
            // label223
            // 
            this.label223.AutoSize = true;
            this.label223.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label223.Location = new System.Drawing.Point(3, 15);
            this.label223.Name = "label223";
            this.label223.Size = new System.Drawing.Size(19, 13);
            this.label223.TabIndex = 74;
            this.label223.Text = "ио";
            // 
            // _difDefenceActualIoPo
            // 
            this._difDefenceActualIoPo.Location = new System.Drawing.Point(6, 72);
            this._difDefenceActualIoPo.Name = "_difDefenceActualIoPo";
            this._difDefenceActualIoPo.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualIoPo.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualIoPo.TabIndex = 73;
            // 
            // _difDefenceActualIoSh2
            // 
            this._difDefenceActualIoSh2.Location = new System.Drawing.Point(6, 53);
            this._difDefenceActualIoSh2.Name = "_difDefenceActualIoSh2";
            this._difDefenceActualIoSh2.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualIoSh2.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualIoSh2.TabIndex = 72;
            // 
            // _difDefenceActualIoSh1
            // 
            this._difDefenceActualIoSh1.Location = new System.Drawing.Point(6, 34);
            this._difDefenceActualIoSh1.Name = "_difDefenceActualIoSh1";
            this._difDefenceActualIoSh1.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualIoSh1.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualIoSh1.TabIndex = 71;
            // 
            // label216
            // 
            this.label216.AutoSize = true;
            this.label216.Location = new System.Drawing.Point(63, 72);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(41, 13);
            this.label216.TabIndex = 62;
            this.label216.Text = "Iд3 ПО";
            // 
            // label217
            // 
            this.label217.AutoSize = true;
            this.label217.Location = new System.Drawing.Point(63, 53);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(47, 13);
            this.label217.TabIndex = 61;
            this.label217.Text = "Iд2 СШ2";
            // 
            // label218
            // 
            this.label218.AutoSize = true;
            this.label218.Location = new System.Drawing.Point(63, 34);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(47, 13);
            this.label218.TabIndex = 60;
            this.label218.Text = "Iд1 СШ1";
            // 
            // _difDefenceActualChtoPo
            // 
            this._difDefenceActualChtoPo.Location = new System.Drawing.Point(25, 72);
            this._difDefenceActualChtoPo.Name = "_difDefenceActualChtoPo";
            this._difDefenceActualChtoPo.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualChtoPo.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualChtoPo.TabIndex = 58;
            // 
            // _difDefenceActualChtoSh2
            // 
            this._difDefenceActualChtoSh2.Location = new System.Drawing.Point(25, 53);
            this._difDefenceActualChtoSh2.Name = "_difDefenceActualChtoSh2";
            this._difDefenceActualChtoSh2.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualChtoSh2.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualChtoSh2.TabIndex = 56;
            // 
            // _difDefenceActualChtoSh1
            // 
            this._difDefenceActualChtoSh1.Location = new System.Drawing.Point(25, 34);
            this._difDefenceActualChtoSh1.Name = "_difDefenceActualChtoSh1";
            this._difDefenceActualChtoSh1.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualChtoSh1.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualChtoSh1.TabIndex = 54;
            // 
            // _difDefenceActualSrabPo
            // 
            this._difDefenceActualSrabPo.Location = new System.Drawing.Point(44, 72);
            this._difDefenceActualSrabPo.Name = "_difDefenceActualSrabPo";
            this._difDefenceActualSrabPo.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualSrabPo.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualSrabPo.TabIndex = 59;
            // 
            // _difDefenceActualSrabSh2
            // 
            this._difDefenceActualSrabSh2.Location = new System.Drawing.Point(44, 53);
            this._difDefenceActualSrabSh2.Name = "_difDefenceActualSrabSh2";
            this._difDefenceActualSrabSh2.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualSrabSh2.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualSrabSh2.TabIndex = 57;
            // 
            // _difDefenceActualSrabSh1
            // 
            this._difDefenceActualSrabSh1.Location = new System.Drawing.Point(44, 34);
            this._difDefenceActualSrabSh1.Name = "_difDefenceActualSrabSh1";
            this._difDefenceActualSrabSh1.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualSrabSh1.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualSrabSh1.TabIndex = 55;
            // 
            // label219
            // 
            this.label219.AutoSize = true;
            this.label219.Location = new System.Drawing.Point(41, 15);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(31, 13);
            this.label219.TabIndex = 1;
            this.label219.Text = "сраб";
            // 
            // label220
            // 
            this.label220.AutoSize = true;
            this.label220.Location = new System.Drawing.Point(22, 15);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(23, 13);
            this.label220.TabIndex = 0;
            this.label220.Text = "что";
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.label215);
            this.groupBox23.Controls.Add(this.label214);
            this.groupBox23.Controls.Add(this.label213);
            this.groupBox23.Controls.Add(this._difDefenceInstantaneousChtoPo);
            this.groupBox23.Controls.Add(this._difDefenceInstantaneousChtoSh2);
            this.groupBox23.Controls.Add(this._difDefenceInstantaneousChtoSh1);
            this.groupBox23.Controls.Add(this._difDefenceInstantaneousSrabPo);
            this.groupBox23.Controls.Add(this._difDefenceInstantaneousSrabSh2);
            this.groupBox23.Controls.Add(this._difDefenceInstantaneousSrabSh1);
            this.groupBox23.Controls.Add(this.label212);
            this.groupBox23.Controls.Add(this.label211);
            this.groupBox23.Location = new System.Drawing.Point(6, 19);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(136, 98);
            this.groupBox23.TabIndex = 0;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "По мгн. значениям";
            // 
            // label215
            // 
            this.label215.AutoSize = true;
            this.label215.Location = new System.Drawing.Point(50, 73);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(49, 13);
            this.label215.TabIndex = 62;
            this.label215.Text = "Iд3м ПО";
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Location = new System.Drawing.Point(50, 54);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(55, 13);
            this.label214.TabIndex = 61;
            this.label214.Text = "Iд2м СШ2";
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Location = new System.Drawing.Point(50, 35);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(55, 13);
            this.label213.TabIndex = 60;
            this.label213.Text = "Iд1м СШ1";
            // 
            // _difDefenceInstantaneousChtoPo
            // 
            this._difDefenceInstantaneousChtoPo.Location = new System.Drawing.Point(9, 73);
            this._difDefenceInstantaneousChtoPo.Name = "_difDefenceInstantaneousChtoPo";
            this._difDefenceInstantaneousChtoPo.Size = new System.Drawing.Size(13, 13);
            this._difDefenceInstantaneousChtoPo.State = BEMN.Forms.LedState.Off;
            this._difDefenceInstantaneousChtoPo.TabIndex = 58;
            // 
            // _difDefenceInstantaneousChtoSh2
            // 
            this._difDefenceInstantaneousChtoSh2.Location = new System.Drawing.Point(9, 54);
            this._difDefenceInstantaneousChtoSh2.Name = "_difDefenceInstantaneousChtoSh2";
            this._difDefenceInstantaneousChtoSh2.Size = new System.Drawing.Size(13, 13);
            this._difDefenceInstantaneousChtoSh2.State = BEMN.Forms.LedState.Off;
            this._difDefenceInstantaneousChtoSh2.TabIndex = 56;
            // 
            // _difDefenceInstantaneousChtoSh1
            // 
            this._difDefenceInstantaneousChtoSh1.Location = new System.Drawing.Point(9, 35);
            this._difDefenceInstantaneousChtoSh1.Name = "_difDefenceInstantaneousChtoSh1";
            this._difDefenceInstantaneousChtoSh1.Size = new System.Drawing.Size(13, 13);
            this._difDefenceInstantaneousChtoSh1.State = BEMN.Forms.LedState.Off;
            this._difDefenceInstantaneousChtoSh1.TabIndex = 54;
            // 
            // _difDefenceInstantaneousSrabPo
            // 
            this._difDefenceInstantaneousSrabPo.Location = new System.Drawing.Point(31, 73);
            this._difDefenceInstantaneousSrabPo.Name = "_difDefenceInstantaneousSrabPo";
            this._difDefenceInstantaneousSrabPo.Size = new System.Drawing.Size(13, 13);
            this._difDefenceInstantaneousSrabPo.State = BEMN.Forms.LedState.Off;
            this._difDefenceInstantaneousSrabPo.TabIndex = 59;
            // 
            // _difDefenceInstantaneousSrabSh2
            // 
            this._difDefenceInstantaneousSrabSh2.Location = new System.Drawing.Point(31, 54);
            this._difDefenceInstantaneousSrabSh2.Name = "_difDefenceInstantaneousSrabSh2";
            this._difDefenceInstantaneousSrabSh2.Size = new System.Drawing.Size(13, 13);
            this._difDefenceInstantaneousSrabSh2.State = BEMN.Forms.LedState.Off;
            this._difDefenceInstantaneousSrabSh2.TabIndex = 57;
            // 
            // _difDefenceInstantaneousSrabSh1
            // 
            this._difDefenceInstantaneousSrabSh1.Location = new System.Drawing.Point(31, 35);
            this._difDefenceInstantaneousSrabSh1.Name = "_difDefenceInstantaneousSrabSh1";
            this._difDefenceInstantaneousSrabSh1.Size = new System.Drawing.Size(13, 13);
            this._difDefenceInstantaneousSrabSh1.State = BEMN.Forms.LedState.Off;
            this._difDefenceInstantaneousSrabSh1.TabIndex = 55;
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Location = new System.Drawing.Point(28, 16);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(31, 13);
            this.label212.TabIndex = 1;
            this.label212.Text = "сраб";
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.Location = new System.Drawing.Point(6, 16);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(23, 13);
            this.label211.TabIndex = 0;
            this.label211.Text = "что";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this._reservedGroupOfSetpoints);
            this.groupBox20.Controls.Add(this.label208);
            this.groupBox20.Controls.Add(this._mainGroupOfSetpoints);
            this.groupBox20.Controls.Add(this.label209);
            this.groupBox20.Controls.Add(this._fault);
            this.groupBox20.Controls.Add(this.label210);
            this.groupBox20.Location = new System.Drawing.Point(630, 128);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(128, 84);
            this.groupBox20.TabIndex = 15;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Состояние";
            // 
            // _reservedGroupOfSetpoints
            // 
            this._reservedGroupOfSetpoints.Location = new System.Drawing.Point(6, 59);
            this._reservedGroupOfSetpoints.Name = "_reservedGroupOfSetpoints";
            this._reservedGroupOfSetpoints.Size = new System.Drawing.Size(13, 13);
            this._reservedGroupOfSetpoints.State = BEMN.Forms.LedState.Off;
            this._reservedGroupOfSetpoints.TabIndex = 11;
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Location = new System.Drawing.Point(25, 59);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(68, 13);
            this.label208.TabIndex = 10;
            this.label208.Text = "Рез. гр. уст.";
            // 
            // _mainGroupOfSetpoints
            // 
            this._mainGroupOfSetpoints.Location = new System.Drawing.Point(6, 40);
            this._mainGroupOfSetpoints.Name = "_mainGroupOfSetpoints";
            this._mainGroupOfSetpoints.Size = new System.Drawing.Size(13, 13);
            this._mainGroupOfSetpoints.State = BEMN.Forms.LedState.Off;
            this._mainGroupOfSetpoints.TabIndex = 9;
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.Location = new System.Drawing.Point(25, 40);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(69, 13);
            this.label209.TabIndex = 8;
            this.label209.Text = "Осн. гр. уст.";
            // 
            // _fault
            // 
            this._fault.Location = new System.Drawing.Point(6, 21);
            this._fault.Name = "_fault";
            this._fault.Size = new System.Drawing.Size(13, 13);
            this._fault.State = BEMN.Forms.LedState.Off;
            this._fault.TabIndex = 7;
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Location = new System.Drawing.Point(25, 21);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(86, 13);
            this.label210.TabIndex = 6;
            this.label210.Text = "Неисправность";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label191);
            this.groupBox11.Controls.Add(this.label199);
            this.groupBox11.Controls.Add(this._i32Io);
            this.groupBox11.Controls.Add(this.label221);
            this.groupBox11.Controls.Add(this._i31Io);
            this.groupBox11.Controls.Add(this.label222);
            this.groupBox11.Controls.Add(this._i30Io);
            this.groupBox11.Controls.Add(this.label229);
            this.groupBox11.Controls.Add(this._i29Io);
            this.groupBox11.Controls.Add(this.label230);
            this.groupBox11.Controls.Add(this._i28Io);
            this.groupBox11.Controls.Add(this.label270);
            this.groupBox11.Controls.Add(this._i27Io);
            this.groupBox11.Controls.Add(this.label269);
            this.groupBox11.Controls.Add(this._i26Io);
            this.groupBox11.Controls.Add(this._i32);
            this.groupBox11.Controls.Add(this._i25Io);
            this.groupBox11.Controls.Add(this.label95);
            this.groupBox11.Controls.Add(this._i31);
            this.groupBox11.Controls.Add(this.label96);
            this.groupBox11.Controls.Add(this._i30);
            this.groupBox11.Controls.Add(this.label97);
            this.groupBox11.Controls.Add(this._i29);
            this.groupBox11.Controls.Add(this.label98);
            this.groupBox11.Controls.Add(this._i28);
            this.groupBox11.Controls.Add(this.label99);
            this.groupBox11.Controls.Add(this._i27);
            this.groupBox11.Controls.Add(this.label100);
            this.groupBox11.Controls.Add(this._i24Io);
            this.groupBox11.Controls.Add(this._i26);
            this.groupBox11.Controls.Add(this.label101);
            this.groupBox11.Controls.Add(this._i23Io);
            this.groupBox11.Controls.Add(this._i25);
            this.groupBox11.Controls.Add(this.label102);
            this.groupBox11.Controls.Add(this._i22Io);
            this.groupBox11.Controls.Add(this._i24);
            this.groupBox11.Controls.Add(this.label103);
            this.groupBox11.Controls.Add(this._i21Io);
            this.groupBox11.Controls.Add(this._i23);
            this.groupBox11.Controls.Add(this.label104);
            this.groupBox11.Controls.Add(this._i20Io);
            this.groupBox11.Controls.Add(this._i22);
            this.groupBox11.Controls.Add(this.label105);
            this.groupBox11.Controls.Add(this._i19Io);
            this.groupBox11.Controls.Add(this._i21);
            this.groupBox11.Controls.Add(this.label106);
            this.groupBox11.Controls.Add(this._i18Io);
            this.groupBox11.Controls.Add(this._i20);
            this.groupBox11.Controls.Add(this.label107);
            this.groupBox11.Controls.Add(this._i17Io);
            this.groupBox11.Controls.Add(this._i19);
            this.groupBox11.Controls.Add(this.label108);
            this.groupBox11.Controls.Add(this._i18);
            this.groupBox11.Controls.Add(this.label109);
            this.groupBox11.Controls.Add(this._i16Io);
            this.groupBox11.Controls.Add(this._i17);
            this.groupBox11.Controls.Add(this.label110);
            this.groupBox11.Controls.Add(this._i15Io);
            this.groupBox11.Controls.Add(this._i16);
            this.groupBox11.Controls.Add(this.label79);
            this.groupBox11.Controls.Add(this._i14Io);
            this.groupBox11.Controls.Add(this._i15);
            this.groupBox11.Controls.Add(this.label80);
            this.groupBox11.Controls.Add(this._i13Io);
            this.groupBox11.Controls.Add(this._i14);
            this.groupBox11.Controls.Add(this.label81);
            this.groupBox11.Controls.Add(this._i12Io);
            this.groupBox11.Controls.Add(this._i13);
            this.groupBox11.Controls.Add(this.label82);
            this.groupBox11.Controls.Add(this._i11Io);
            this.groupBox11.Controls.Add(this._i12);
            this.groupBox11.Controls.Add(this.label83);
            this.groupBox11.Controls.Add(this._i10Io);
            this.groupBox11.Controls.Add(this._i11);
            this.groupBox11.Controls.Add(this.label84);
            this.groupBox11.Controls.Add(this._i9Io);
            this.groupBox11.Controls.Add(this._i10);
            this.groupBox11.Controls.Add(this.label85);
            this.groupBox11.Controls.Add(this._i8Io);
            this.groupBox11.Controls.Add(this._i9);
            this.groupBox11.Controls.Add(this.label86);
            this.groupBox11.Controls.Add(this._i7Io);
            this.groupBox11.Controls.Add(this._i8);
            this.groupBox11.Controls.Add(this.label87);
            this.groupBox11.Controls.Add(this._i6Io);
            this.groupBox11.Controls.Add(this._i7);
            this.groupBox11.Controls.Add(this.label88);
            this.groupBox11.Controls.Add(this._i5Io);
            this.groupBox11.Controls.Add(this._i6);
            this.groupBox11.Controls.Add(this.label89);
            this.groupBox11.Controls.Add(this._i4Io);
            this.groupBox11.Controls.Add(this._i5);
            this.groupBox11.Controls.Add(this.label90);
            this.groupBox11.Controls.Add(this._i3Io);
            this.groupBox11.Controls.Add(this._i4);
            this.groupBox11.Controls.Add(this.label91);
            this.groupBox11.Controls.Add(this._i2Io);
            this.groupBox11.Controls.Add(this._i3);
            this.groupBox11.Controls.Add(this.label92);
            this.groupBox11.Controls.Add(this._i1Io);
            this.groupBox11.Controls.Add(this._i2);
            this.groupBox11.Controls.Add(this.label93);
            this.groupBox11.Controls.Add(this._i1);
            this.groupBox11.Controls.Add(this.label94);
            this.groupBox11.Location = new System.Drawing.Point(256, 448);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(312, 193);
            this.groupBox11.TabIndex = 4;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Защиты I>";
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.ForeColor = System.Drawing.Color.Blue;
            this.label191.Location = new System.Drawing.Point(248, 16);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(32, 13);
            this.label191.TabIndex = 71;
            this.label191.Text = "Сраб";
            // 
            // label199
            // 
            this.label199.AutoSize = true;
            this.label199.ForeColor = System.Drawing.Color.Red;
            this.label199.Location = new System.Drawing.Point(229, 16);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(23, 13);
            this.label199.TabIndex = 70;
            this.label199.Text = "ИО";
            // 
            // _i32Io
            // 
            this._i32Io.Location = new System.Drawing.Point(232, 168);
            this._i32Io.Name = "_i32Io";
            this._i32Io.Size = new System.Drawing.Size(13, 13);
            this._i32Io.State = BEMN.Forms.LedState.Off;
            this._i32Io.TabIndex = 63;
            // 
            // label221
            // 
            this.label221.AutoSize = true;
            this.label221.ForeColor = System.Drawing.Color.Blue;
            this.label221.Location = new System.Drawing.Point(172, 16);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(32, 13);
            this.label221.TabIndex = 69;
            this.label221.Text = "Сраб";
            // 
            // _i31Io
            // 
            this._i31Io.Location = new System.Drawing.Point(232, 149);
            this._i31Io.Name = "_i31Io";
            this._i31Io.Size = new System.Drawing.Size(13, 13);
            this._i31Io.State = BEMN.Forms.LedState.Off;
            this._i31Io.TabIndex = 61;
            // 
            // label222
            // 
            this.label222.AutoSize = true;
            this.label222.ForeColor = System.Drawing.Color.Red;
            this.label222.Location = new System.Drawing.Point(153, 16);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(23, 13);
            this.label222.TabIndex = 68;
            this.label222.Text = "ИО";
            // 
            // _i30Io
            // 
            this._i30Io.Location = new System.Drawing.Point(232, 130);
            this._i30Io.Name = "_i30Io";
            this._i30Io.Size = new System.Drawing.Size(13, 13);
            this._i30Io.State = BEMN.Forms.LedState.Off;
            this._i30Io.TabIndex = 59;
            // 
            // label229
            // 
            this.label229.AutoSize = true;
            this.label229.ForeColor = System.Drawing.Color.Blue;
            this.label229.Location = new System.Drawing.Point(95, 16);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(32, 13);
            this.label229.TabIndex = 67;
            this.label229.Text = "Сраб";
            // 
            // _i29Io
            // 
            this._i29Io.Location = new System.Drawing.Point(232, 111);
            this._i29Io.Name = "_i29Io";
            this._i29Io.Size = new System.Drawing.Size(13, 13);
            this._i29Io.State = BEMN.Forms.LedState.Off;
            this._i29Io.TabIndex = 57;
            // 
            // label230
            // 
            this.label230.AutoSize = true;
            this.label230.ForeColor = System.Drawing.Color.Red;
            this.label230.Location = new System.Drawing.Point(76, 16);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(23, 13);
            this.label230.TabIndex = 66;
            this.label230.Text = "ИО";
            // 
            // _i28Io
            // 
            this._i28Io.Location = new System.Drawing.Point(232, 92);
            this._i28Io.Name = "_i28Io";
            this._i28Io.Size = new System.Drawing.Size(13, 13);
            this._i28Io.State = BEMN.Forms.LedState.Off;
            this._i28Io.TabIndex = 55;
            // 
            // label270
            // 
            this.label270.AutoSize = true;
            this.label270.ForeColor = System.Drawing.Color.Blue;
            this.label270.Location = new System.Drawing.Point(25, 16);
            this.label270.Name = "label270";
            this.label270.Size = new System.Drawing.Size(32, 13);
            this.label270.TabIndex = 65;
            this.label270.Text = "Сраб";
            // 
            // _i27Io
            // 
            this._i27Io.Location = new System.Drawing.Point(232, 73);
            this._i27Io.Name = "_i27Io";
            this._i27Io.Size = new System.Drawing.Size(13, 13);
            this._i27Io.State = BEMN.Forms.LedState.Off;
            this._i27Io.TabIndex = 53;
            // 
            // label269
            // 
            this.label269.AutoSize = true;
            this.label269.ForeColor = System.Drawing.Color.Red;
            this.label269.Location = new System.Drawing.Point(6, 16);
            this.label269.Name = "label269";
            this.label269.Size = new System.Drawing.Size(23, 13);
            this.label269.TabIndex = 64;
            this.label269.Text = "ИО";
            // 
            // _i26Io
            // 
            this._i26Io.Location = new System.Drawing.Point(232, 54);
            this._i26Io.Name = "_i26Io";
            this._i26Io.Size = new System.Drawing.Size(13, 13);
            this._i26Io.State = BEMN.Forms.LedState.Off;
            this._i26Io.TabIndex = 51;
            // 
            // _i32
            // 
            this._i32.Location = new System.Drawing.Point(251, 168);
            this._i32.Name = "_i32";
            this._i32.Size = new System.Drawing.Size(13, 13);
            this._i32.State = BEMN.Forms.LedState.Off;
            this._i32.TabIndex = 63;
            // 
            // _i25Io
            // 
            this._i25Io.Location = new System.Drawing.Point(232, 35);
            this._i25Io.Name = "_i25Io";
            this._i25Io.Size = new System.Drawing.Size(13, 13);
            this._i25Io.State = BEMN.Forms.LedState.Off;
            this._i25Io.TabIndex = 49;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(270, 168);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(28, 13);
            this.label95.TabIndex = 62;
            this.label95.Text = "I>32";
            // 
            // _i31
            // 
            this._i31.Location = new System.Drawing.Point(251, 149);
            this._i31.Name = "_i31";
            this._i31.Size = new System.Drawing.Size(13, 13);
            this._i31.State = BEMN.Forms.LedState.Off;
            this._i31.TabIndex = 61;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(270, 149);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(28, 13);
            this.label96.TabIndex = 60;
            this.label96.Text = "I>31";
            // 
            // _i30
            // 
            this._i30.Location = new System.Drawing.Point(251, 130);
            this._i30.Name = "_i30";
            this._i30.Size = new System.Drawing.Size(13, 13);
            this._i30.State = BEMN.Forms.LedState.Off;
            this._i30.TabIndex = 59;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(270, 130);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(28, 13);
            this.label97.TabIndex = 58;
            this.label97.Text = "I>30";
            // 
            // _i29
            // 
            this._i29.Location = new System.Drawing.Point(251, 111);
            this._i29.Name = "_i29";
            this._i29.Size = new System.Drawing.Size(13, 13);
            this._i29.State = BEMN.Forms.LedState.Off;
            this._i29.TabIndex = 57;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(270, 111);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(28, 13);
            this.label98.TabIndex = 56;
            this.label98.Text = "I>29";
            // 
            // _i28
            // 
            this._i28.Location = new System.Drawing.Point(251, 92);
            this._i28.Name = "_i28";
            this._i28.Size = new System.Drawing.Size(13, 13);
            this._i28.State = BEMN.Forms.LedState.Off;
            this._i28.TabIndex = 55;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(270, 92);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(28, 13);
            this.label99.TabIndex = 54;
            this.label99.Text = "I>28";
            // 
            // _i27
            // 
            this._i27.Location = new System.Drawing.Point(251, 73);
            this._i27.Name = "_i27";
            this._i27.Size = new System.Drawing.Size(13, 13);
            this._i27.State = BEMN.Forms.LedState.Off;
            this._i27.TabIndex = 53;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(270, 73);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(28, 13);
            this.label100.TabIndex = 52;
            this.label100.Text = "I>27";
            // 
            // _i24Io
            // 
            this._i24Io.Location = new System.Drawing.Point(156, 168);
            this._i24Io.Name = "_i24Io";
            this._i24Io.Size = new System.Drawing.Size(13, 13);
            this._i24Io.State = BEMN.Forms.LedState.Off;
            this._i24Io.TabIndex = 47;
            // 
            // _i26
            // 
            this._i26.Location = new System.Drawing.Point(251, 54);
            this._i26.Name = "_i26";
            this._i26.Size = new System.Drawing.Size(13, 13);
            this._i26.State = BEMN.Forms.LedState.Off;
            this._i26.TabIndex = 51;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(270, 54);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(28, 13);
            this.label101.TabIndex = 50;
            this.label101.Text = "I>26";
            // 
            // _i23Io
            // 
            this._i23Io.Location = new System.Drawing.Point(156, 149);
            this._i23Io.Name = "_i23Io";
            this._i23Io.Size = new System.Drawing.Size(13, 13);
            this._i23Io.State = BEMN.Forms.LedState.Off;
            this._i23Io.TabIndex = 45;
            // 
            // _i25
            // 
            this._i25.Location = new System.Drawing.Point(251, 35);
            this._i25.Name = "_i25";
            this._i25.Size = new System.Drawing.Size(13, 13);
            this._i25.State = BEMN.Forms.LedState.Off;
            this._i25.TabIndex = 49;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(270, 35);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(28, 13);
            this.label102.TabIndex = 48;
            this.label102.Text = "I>25";
            // 
            // _i22Io
            // 
            this._i22Io.Location = new System.Drawing.Point(156, 130);
            this._i22Io.Name = "_i22Io";
            this._i22Io.Size = new System.Drawing.Size(13, 13);
            this._i22Io.State = BEMN.Forms.LedState.Off;
            this._i22Io.TabIndex = 43;
            // 
            // _i24
            // 
            this._i24.Location = new System.Drawing.Point(175, 168);
            this._i24.Name = "_i24";
            this._i24.Size = new System.Drawing.Size(13, 13);
            this._i24.State = BEMN.Forms.LedState.Off;
            this._i24.TabIndex = 47;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(194, 168);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(28, 13);
            this.label103.TabIndex = 46;
            this.label103.Text = "I>24";
            // 
            // _i21Io
            // 
            this._i21Io.Location = new System.Drawing.Point(156, 111);
            this._i21Io.Name = "_i21Io";
            this._i21Io.Size = new System.Drawing.Size(13, 13);
            this._i21Io.State = BEMN.Forms.LedState.Off;
            this._i21Io.TabIndex = 41;
            // 
            // _i23
            // 
            this._i23.Location = new System.Drawing.Point(175, 149);
            this._i23.Name = "_i23";
            this._i23.Size = new System.Drawing.Size(13, 13);
            this._i23.State = BEMN.Forms.LedState.Off;
            this._i23.TabIndex = 45;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(194, 149);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(28, 13);
            this.label104.TabIndex = 44;
            this.label104.Text = "I>23";
            // 
            // _i20Io
            // 
            this._i20Io.Location = new System.Drawing.Point(156, 92);
            this._i20Io.Name = "_i20Io";
            this._i20Io.Size = new System.Drawing.Size(13, 13);
            this._i20Io.State = BEMN.Forms.LedState.Off;
            this._i20Io.TabIndex = 39;
            // 
            // _i22
            // 
            this._i22.Location = new System.Drawing.Point(175, 130);
            this._i22.Name = "_i22";
            this._i22.Size = new System.Drawing.Size(13, 13);
            this._i22.State = BEMN.Forms.LedState.Off;
            this._i22.TabIndex = 43;
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(194, 130);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(28, 13);
            this.label105.TabIndex = 42;
            this.label105.Text = "I>22";
            // 
            // _i19Io
            // 
            this._i19Io.Location = new System.Drawing.Point(156, 73);
            this._i19Io.Name = "_i19Io";
            this._i19Io.Size = new System.Drawing.Size(13, 13);
            this._i19Io.State = BEMN.Forms.LedState.Off;
            this._i19Io.TabIndex = 37;
            // 
            // _i21
            // 
            this._i21.Location = new System.Drawing.Point(175, 111);
            this._i21.Name = "_i21";
            this._i21.Size = new System.Drawing.Size(13, 13);
            this._i21.State = BEMN.Forms.LedState.Off;
            this._i21.TabIndex = 41;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(194, 111);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(28, 13);
            this.label106.TabIndex = 40;
            this.label106.Text = "I>21";
            // 
            // _i18Io
            // 
            this._i18Io.Location = new System.Drawing.Point(156, 54);
            this._i18Io.Name = "_i18Io";
            this._i18Io.Size = new System.Drawing.Size(13, 13);
            this._i18Io.State = BEMN.Forms.LedState.Off;
            this._i18Io.TabIndex = 35;
            // 
            // _i20
            // 
            this._i20.Location = new System.Drawing.Point(175, 92);
            this._i20.Name = "_i20";
            this._i20.Size = new System.Drawing.Size(13, 13);
            this._i20.State = BEMN.Forms.LedState.Off;
            this._i20.TabIndex = 39;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(195, 92);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(28, 13);
            this.label107.TabIndex = 38;
            this.label107.Text = "I>20";
            // 
            // _i17Io
            // 
            this._i17Io.Location = new System.Drawing.Point(156, 35);
            this._i17Io.Name = "_i17Io";
            this._i17Io.Size = new System.Drawing.Size(13, 13);
            this._i17Io.State = BEMN.Forms.LedState.Off;
            this._i17Io.TabIndex = 33;
            // 
            // _i19
            // 
            this._i19.Location = new System.Drawing.Point(175, 73);
            this._i19.Name = "_i19";
            this._i19.Size = new System.Drawing.Size(13, 13);
            this._i19.State = BEMN.Forms.LedState.Off;
            this._i19.TabIndex = 37;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(194, 73);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(28, 13);
            this.label108.TabIndex = 36;
            this.label108.Text = "I>19";
            // 
            // _i18
            // 
            this._i18.Location = new System.Drawing.Point(175, 54);
            this._i18.Name = "_i18";
            this._i18.Size = new System.Drawing.Size(13, 13);
            this._i18.State = BEMN.Forms.LedState.Off;
            this._i18.TabIndex = 35;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(194, 54);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(28, 13);
            this.label109.TabIndex = 34;
            this.label109.Text = "I>18";
            // 
            // _i16Io
            // 
            this._i16Io.Location = new System.Drawing.Point(79, 168);
            this._i16Io.Name = "_i16Io";
            this._i16Io.Size = new System.Drawing.Size(13, 13);
            this._i16Io.State = BEMN.Forms.LedState.Off;
            this._i16Io.TabIndex = 31;
            // 
            // _i17
            // 
            this._i17.Location = new System.Drawing.Point(175, 35);
            this._i17.Name = "_i17";
            this._i17.Size = new System.Drawing.Size(13, 13);
            this._i17.State = BEMN.Forms.LedState.Off;
            this._i17.TabIndex = 33;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(194, 35);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(28, 13);
            this.label110.TabIndex = 32;
            this.label110.Text = "I>17";
            // 
            // _i15Io
            // 
            this._i15Io.Location = new System.Drawing.Point(79, 149);
            this._i15Io.Name = "_i15Io";
            this._i15Io.Size = new System.Drawing.Size(13, 13);
            this._i15Io.State = BEMN.Forms.LedState.Off;
            this._i15Io.TabIndex = 29;
            // 
            // _i16
            // 
            this._i16.Location = new System.Drawing.Point(98, 168);
            this._i16.Name = "_i16";
            this._i16.Size = new System.Drawing.Size(13, 13);
            this._i16.State = BEMN.Forms.LedState.Off;
            this._i16.TabIndex = 31;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(117, 168);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(28, 13);
            this.label79.TabIndex = 30;
            this.label79.Text = "I>16";
            // 
            // _i14Io
            // 
            this._i14Io.Location = new System.Drawing.Point(79, 130);
            this._i14Io.Name = "_i14Io";
            this._i14Io.Size = new System.Drawing.Size(13, 13);
            this._i14Io.State = BEMN.Forms.LedState.Off;
            this._i14Io.TabIndex = 27;
            // 
            // _i15
            // 
            this._i15.Location = new System.Drawing.Point(98, 149);
            this._i15.Name = "_i15";
            this._i15.Size = new System.Drawing.Size(13, 13);
            this._i15.State = BEMN.Forms.LedState.Off;
            this._i15.TabIndex = 29;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(117, 149);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(28, 13);
            this.label80.TabIndex = 28;
            this.label80.Text = "I>15";
            // 
            // _i13Io
            // 
            this._i13Io.Location = new System.Drawing.Point(79, 111);
            this._i13Io.Name = "_i13Io";
            this._i13Io.Size = new System.Drawing.Size(13, 13);
            this._i13Io.State = BEMN.Forms.LedState.Off;
            this._i13Io.TabIndex = 25;
            // 
            // _i14
            // 
            this._i14.Location = new System.Drawing.Point(98, 130);
            this._i14.Name = "_i14";
            this._i14.Size = new System.Drawing.Size(13, 13);
            this._i14.State = BEMN.Forms.LedState.Off;
            this._i14.TabIndex = 27;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(117, 130);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(28, 13);
            this.label81.TabIndex = 26;
            this.label81.Text = "I>14";
            // 
            // _i12Io
            // 
            this._i12Io.Location = new System.Drawing.Point(79, 92);
            this._i12Io.Name = "_i12Io";
            this._i12Io.Size = new System.Drawing.Size(13, 13);
            this._i12Io.State = BEMN.Forms.LedState.Off;
            this._i12Io.TabIndex = 23;
            // 
            // _i13
            // 
            this._i13.Location = new System.Drawing.Point(98, 111);
            this._i13.Name = "_i13";
            this._i13.Size = new System.Drawing.Size(13, 13);
            this._i13.State = BEMN.Forms.LedState.Off;
            this._i13.TabIndex = 25;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(117, 111);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(28, 13);
            this.label82.TabIndex = 24;
            this.label82.Text = "I>13";
            // 
            // _i11Io
            // 
            this._i11Io.Location = new System.Drawing.Point(79, 73);
            this._i11Io.Name = "_i11Io";
            this._i11Io.Size = new System.Drawing.Size(13, 13);
            this._i11Io.State = BEMN.Forms.LedState.Off;
            this._i11Io.TabIndex = 21;
            // 
            // _i12
            // 
            this._i12.Location = new System.Drawing.Point(98, 92);
            this._i12.Name = "_i12";
            this._i12.Size = new System.Drawing.Size(13, 13);
            this._i12.State = BEMN.Forms.LedState.Off;
            this._i12.TabIndex = 23;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(117, 92);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(28, 13);
            this.label83.TabIndex = 22;
            this.label83.Text = "I>12";
            // 
            // _i10Io
            // 
            this._i10Io.Location = new System.Drawing.Point(79, 54);
            this._i10Io.Name = "_i10Io";
            this._i10Io.Size = new System.Drawing.Size(13, 13);
            this._i10Io.State = BEMN.Forms.LedState.Off;
            this._i10Io.TabIndex = 19;
            // 
            // _i11
            // 
            this._i11.Location = new System.Drawing.Point(98, 73);
            this._i11.Name = "_i11";
            this._i11.Size = new System.Drawing.Size(13, 13);
            this._i11.State = BEMN.Forms.LedState.Off;
            this._i11.TabIndex = 21;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(117, 73);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(28, 13);
            this.label84.TabIndex = 20;
            this.label84.Text = "I>11";
            // 
            // _i9Io
            // 
            this._i9Io.Location = new System.Drawing.Point(79, 35);
            this._i9Io.Name = "_i9Io";
            this._i9Io.Size = new System.Drawing.Size(13, 13);
            this._i9Io.State = BEMN.Forms.LedState.Off;
            this._i9Io.TabIndex = 17;
            // 
            // _i10
            // 
            this._i10.Location = new System.Drawing.Point(98, 54);
            this._i10.Name = "_i10";
            this._i10.Size = new System.Drawing.Size(13, 13);
            this._i10.State = BEMN.Forms.LedState.Off;
            this._i10.TabIndex = 19;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(117, 54);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(28, 13);
            this.label85.TabIndex = 18;
            this.label85.Text = "I>10";
            // 
            // _i8Io
            // 
            this._i8Io.Location = new System.Drawing.Point(9, 168);
            this._i8Io.Name = "_i8Io";
            this._i8Io.Size = new System.Drawing.Size(13, 13);
            this._i8Io.State = BEMN.Forms.LedState.Off;
            this._i8Io.TabIndex = 15;
            // 
            // _i9
            // 
            this._i9.Location = new System.Drawing.Point(98, 35);
            this._i9.Name = "_i9";
            this._i9.Size = new System.Drawing.Size(13, 13);
            this._i9.State = BEMN.Forms.LedState.Off;
            this._i9.TabIndex = 17;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(117, 35);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(22, 13);
            this.label86.TabIndex = 16;
            this.label86.Text = "I>9";
            // 
            // _i7Io
            // 
            this._i7Io.Location = new System.Drawing.Point(9, 149);
            this._i7Io.Name = "_i7Io";
            this._i7Io.Size = new System.Drawing.Size(13, 13);
            this._i7Io.State = BEMN.Forms.LedState.Off;
            this._i7Io.TabIndex = 13;
            // 
            // _i8
            // 
            this._i8.Location = new System.Drawing.Point(28, 168);
            this._i8.Name = "_i8";
            this._i8.Size = new System.Drawing.Size(13, 13);
            this._i8.State = BEMN.Forms.LedState.Off;
            this._i8.TabIndex = 15;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(47, 168);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(22, 13);
            this.label87.TabIndex = 14;
            this.label87.Text = "I>8";
            // 
            // _i6Io
            // 
            this._i6Io.Location = new System.Drawing.Point(9, 130);
            this._i6Io.Name = "_i6Io";
            this._i6Io.Size = new System.Drawing.Size(13, 13);
            this._i6Io.State = BEMN.Forms.LedState.Off;
            this._i6Io.TabIndex = 11;
            // 
            // _i7
            // 
            this._i7.Location = new System.Drawing.Point(28, 149);
            this._i7.Name = "_i7";
            this._i7.Size = new System.Drawing.Size(13, 13);
            this._i7.State = BEMN.Forms.LedState.Off;
            this._i7.TabIndex = 13;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(47, 149);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(22, 13);
            this.label88.TabIndex = 12;
            this.label88.Text = "I>7";
            // 
            // _i5Io
            // 
            this._i5Io.Location = new System.Drawing.Point(9, 111);
            this._i5Io.Name = "_i5Io";
            this._i5Io.Size = new System.Drawing.Size(13, 13);
            this._i5Io.State = BEMN.Forms.LedState.Off;
            this._i5Io.TabIndex = 9;
            // 
            // _i6
            // 
            this._i6.Location = new System.Drawing.Point(28, 130);
            this._i6.Name = "_i6";
            this._i6.Size = new System.Drawing.Size(13, 13);
            this._i6.State = BEMN.Forms.LedState.Off;
            this._i6.TabIndex = 11;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(47, 130);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(22, 13);
            this.label89.TabIndex = 10;
            this.label89.Text = "I>6";
            // 
            // _i4Io
            // 
            this._i4Io.Location = new System.Drawing.Point(9, 92);
            this._i4Io.Name = "_i4Io";
            this._i4Io.Size = new System.Drawing.Size(13, 13);
            this._i4Io.State = BEMN.Forms.LedState.Off;
            this._i4Io.TabIndex = 7;
            // 
            // _i5
            // 
            this._i5.Location = new System.Drawing.Point(28, 111);
            this._i5.Name = "_i5";
            this._i5.Size = new System.Drawing.Size(13, 13);
            this._i5.State = BEMN.Forms.LedState.Off;
            this._i5.TabIndex = 9;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(47, 111);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(22, 13);
            this.label90.TabIndex = 8;
            this.label90.Text = "I>5";
            // 
            // _i3Io
            // 
            this._i3Io.Location = new System.Drawing.Point(9, 73);
            this._i3Io.Name = "_i3Io";
            this._i3Io.Size = new System.Drawing.Size(13, 13);
            this._i3Io.State = BEMN.Forms.LedState.Off;
            this._i3Io.TabIndex = 5;
            // 
            // _i4
            // 
            this._i4.Location = new System.Drawing.Point(28, 92);
            this._i4.Name = "_i4";
            this._i4.Size = new System.Drawing.Size(13, 13);
            this._i4.State = BEMN.Forms.LedState.Off;
            this._i4.TabIndex = 7;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(48, 92);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(22, 13);
            this.label91.TabIndex = 6;
            this.label91.Text = "I>4";
            // 
            // _i2Io
            // 
            this._i2Io.Location = new System.Drawing.Point(9, 54);
            this._i2Io.Name = "_i2Io";
            this._i2Io.Size = new System.Drawing.Size(13, 13);
            this._i2Io.State = BEMN.Forms.LedState.Off;
            this._i2Io.TabIndex = 3;
            // 
            // _i3
            // 
            this._i3.Location = new System.Drawing.Point(28, 73);
            this._i3.Name = "_i3";
            this._i3.Size = new System.Drawing.Size(13, 13);
            this._i3.State = BEMN.Forms.LedState.Off;
            this._i3.TabIndex = 5;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(47, 73);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(22, 13);
            this.label92.TabIndex = 4;
            this.label92.Text = "I>3";
            // 
            // _i1Io
            // 
            this._i1Io.Location = new System.Drawing.Point(9, 35);
            this._i1Io.Name = "_i1Io";
            this._i1Io.Size = new System.Drawing.Size(13, 13);
            this._i1Io.State = BEMN.Forms.LedState.Off;
            this._i1Io.TabIndex = 1;
            // 
            // _i2
            // 
            this._i2.Location = new System.Drawing.Point(28, 54);
            this._i2.Name = "_i2";
            this._i2.Size = new System.Drawing.Size(13, 13);
            this._i2.State = BEMN.Forms.LedState.Off;
            this._i2.TabIndex = 3;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(47, 54);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(22, 13);
            this.label93.TabIndex = 2;
            this.label93.Text = "I>2";
            // 
            // _i1
            // 
            this._i1.Location = new System.Drawing.Point(28, 35);
            this._i1.Name = "_i1";
            this._i1.Size = new System.Drawing.Size(13, 13);
            this._i1.State = BEMN.Forms.LedState.Off;
            this._i1.TabIndex = 1;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(47, 35);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(22, 13);
            this.label94.TabIndex = 0;
            this.label94.Text = "I>1";
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._ssl32);
            this.groupBox21.Controls.Add(this.label237);
            this.groupBox21.Controls.Add(this._ssl31);
            this.groupBox21.Controls.Add(this.label238);
            this.groupBox21.Controls.Add(this._ssl30);
            this.groupBox21.Controls.Add(this.label239);
            this.groupBox21.Controls.Add(this._ssl29);
            this.groupBox21.Controls.Add(this.label240);
            this.groupBox21.Controls.Add(this._ssl28);
            this.groupBox21.Controls.Add(this.label241);
            this.groupBox21.Controls.Add(this._ssl27);
            this.groupBox21.Controls.Add(this.label242);
            this.groupBox21.Controls.Add(this._ssl26);
            this.groupBox21.Controls.Add(this.label243);
            this.groupBox21.Controls.Add(this._ssl25);
            this.groupBox21.Controls.Add(this.label244);
            this.groupBox21.Controls.Add(this._ssl24);
            this.groupBox21.Controls.Add(this.label245);
            this.groupBox21.Controls.Add(this._ssl23);
            this.groupBox21.Controls.Add(this.label246);
            this.groupBox21.Controls.Add(this._ssl22);
            this.groupBox21.Controls.Add(this.label247);
            this.groupBox21.Controls.Add(this._ssl21);
            this.groupBox21.Controls.Add(this.label248);
            this.groupBox21.Controls.Add(this._ssl20);
            this.groupBox21.Controls.Add(this.label249);
            this.groupBox21.Controls.Add(this._ssl19);
            this.groupBox21.Controls.Add(this.label250);
            this.groupBox21.Controls.Add(this._ssl18);
            this.groupBox21.Controls.Add(this.label251);
            this.groupBox21.Controls.Add(this._ssl17);
            this.groupBox21.Controls.Add(this.label252);
            this.groupBox21.Controls.Add(this._ssl16);
            this.groupBox21.Controls.Add(this.label253);
            this.groupBox21.Controls.Add(this._ssl15);
            this.groupBox21.Controls.Add(this.label254);
            this.groupBox21.Controls.Add(this._ssl14);
            this.groupBox21.Controls.Add(this.label255);
            this.groupBox21.Controls.Add(this._ssl13);
            this.groupBox21.Controls.Add(this.label256);
            this.groupBox21.Controls.Add(this._ssl12);
            this.groupBox21.Controls.Add(this.label257);
            this.groupBox21.Controls.Add(this._ssl11);
            this.groupBox21.Controls.Add(this.label258);
            this.groupBox21.Controls.Add(this._ssl10);
            this.groupBox21.Controls.Add(this.label259);
            this.groupBox21.Controls.Add(this._ssl9);
            this.groupBox21.Controls.Add(this.label260);
            this.groupBox21.Controls.Add(this._ssl8);
            this.groupBox21.Controls.Add(this.label261);
            this.groupBox21.Controls.Add(this._ssl7);
            this.groupBox21.Controls.Add(this.label262);
            this.groupBox21.Controls.Add(this._ssl6);
            this.groupBox21.Controls.Add(this.label263);
            this.groupBox21.Controls.Add(this._ssl5);
            this.groupBox21.Controls.Add(this.label264);
            this.groupBox21.Controls.Add(this._ssl4);
            this.groupBox21.Controls.Add(this.label265);
            this.groupBox21.Controls.Add(this._ssl3);
            this.groupBox21.Controls.Add(this.label266);
            this.groupBox21.Controls.Add(this._ssl2);
            this.groupBox21.Controls.Add(this.label267);
            this.groupBox21.Controls.Add(this._ssl1);
            this.groupBox21.Controls.Add(this.label268);
            this.groupBox21.Location = new System.Drawing.Point(224, 218);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(289, 191);
            this.groupBox21.TabIndex = 14;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Сигналы СП-логики";
            // 
            // _ssl32
            // 
            this._ssl32.Location = new System.Drawing.Point(219, 166);
            this._ssl32.Name = "_ssl32";
            this._ssl32.Size = new System.Drawing.Size(13, 13);
            this._ssl32.State = BEMN.Forms.LedState.Off;
            this._ssl32.TabIndex = 63;
            // 
            // label237
            // 
            this.label237.AutoSize = true;
            this.label237.Location = new System.Drawing.Point(238, 166);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(41, 13);
            this.label237.TabIndex = 62;
            this.label237.Text = "ССЛ32";
            // 
            // _ssl31
            // 
            this._ssl31.Location = new System.Drawing.Point(219, 147);
            this._ssl31.Name = "_ssl31";
            this._ssl31.Size = new System.Drawing.Size(13, 13);
            this._ssl31.State = BEMN.Forms.LedState.Off;
            this._ssl31.TabIndex = 61;
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.Location = new System.Drawing.Point(238, 147);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(41, 13);
            this.label238.TabIndex = 60;
            this.label238.Text = "ССЛ31";
            // 
            // _ssl30
            // 
            this._ssl30.Location = new System.Drawing.Point(219, 128);
            this._ssl30.Name = "_ssl30";
            this._ssl30.Size = new System.Drawing.Size(13, 13);
            this._ssl30.State = BEMN.Forms.LedState.Off;
            this._ssl30.TabIndex = 59;
            // 
            // label239
            // 
            this.label239.AutoSize = true;
            this.label239.Location = new System.Drawing.Point(238, 128);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(41, 13);
            this.label239.TabIndex = 58;
            this.label239.Text = "ССЛ30";
            // 
            // _ssl29
            // 
            this._ssl29.Location = new System.Drawing.Point(219, 109);
            this._ssl29.Name = "_ssl29";
            this._ssl29.Size = new System.Drawing.Size(13, 13);
            this._ssl29.State = BEMN.Forms.LedState.Off;
            this._ssl29.TabIndex = 57;
            // 
            // label240
            // 
            this.label240.AutoSize = true;
            this.label240.Location = new System.Drawing.Point(238, 109);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(41, 13);
            this.label240.TabIndex = 56;
            this.label240.Text = "ССЛ29";
            // 
            // _ssl28
            // 
            this._ssl28.Location = new System.Drawing.Point(219, 90);
            this._ssl28.Name = "_ssl28";
            this._ssl28.Size = new System.Drawing.Size(13, 13);
            this._ssl28.State = BEMN.Forms.LedState.Off;
            this._ssl28.TabIndex = 55;
            // 
            // label241
            // 
            this.label241.AutoSize = true;
            this.label241.Location = new System.Drawing.Point(238, 90);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(41, 13);
            this.label241.TabIndex = 54;
            this.label241.Text = "ССЛ28";
            // 
            // _ssl27
            // 
            this._ssl27.Location = new System.Drawing.Point(219, 71);
            this._ssl27.Name = "_ssl27";
            this._ssl27.Size = new System.Drawing.Size(13, 13);
            this._ssl27.State = BEMN.Forms.LedState.Off;
            this._ssl27.TabIndex = 53;
            // 
            // label242
            // 
            this.label242.AutoSize = true;
            this.label242.Location = new System.Drawing.Point(238, 71);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(41, 13);
            this.label242.TabIndex = 52;
            this.label242.Text = "ССЛ27";
            // 
            // _ssl26
            // 
            this._ssl26.Location = new System.Drawing.Point(219, 52);
            this._ssl26.Name = "_ssl26";
            this._ssl26.Size = new System.Drawing.Size(13, 13);
            this._ssl26.State = BEMN.Forms.LedState.Off;
            this._ssl26.TabIndex = 51;
            // 
            // label243
            // 
            this.label243.AutoSize = true;
            this.label243.Location = new System.Drawing.Point(238, 52);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(41, 13);
            this.label243.TabIndex = 50;
            this.label243.Text = "ССЛ26";
            // 
            // _ssl25
            // 
            this._ssl25.Location = new System.Drawing.Point(219, 33);
            this._ssl25.Name = "_ssl25";
            this._ssl25.Size = new System.Drawing.Size(13, 13);
            this._ssl25.State = BEMN.Forms.LedState.Off;
            this._ssl25.TabIndex = 49;
            // 
            // label244
            // 
            this.label244.AutoSize = true;
            this.label244.Location = new System.Drawing.Point(238, 33);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(41, 13);
            this.label244.TabIndex = 48;
            this.label244.Text = "ССЛ25";
            // 
            // _ssl24
            // 
            this._ssl24.Location = new System.Drawing.Point(142, 166);
            this._ssl24.Name = "_ssl24";
            this._ssl24.Size = new System.Drawing.Size(13, 13);
            this._ssl24.State = BEMN.Forms.LedState.Off;
            this._ssl24.TabIndex = 47;
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.Location = new System.Drawing.Point(161, 166);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(41, 13);
            this.label245.TabIndex = 46;
            this.label245.Text = "ССЛ24";
            // 
            // _ssl23
            // 
            this._ssl23.Location = new System.Drawing.Point(142, 147);
            this._ssl23.Name = "_ssl23";
            this._ssl23.Size = new System.Drawing.Size(13, 13);
            this._ssl23.State = BEMN.Forms.LedState.Off;
            this._ssl23.TabIndex = 45;
            // 
            // label246
            // 
            this.label246.AutoSize = true;
            this.label246.Location = new System.Drawing.Point(161, 147);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(41, 13);
            this.label246.TabIndex = 44;
            this.label246.Text = "ССЛ23";
            // 
            // _ssl22
            // 
            this._ssl22.Location = new System.Drawing.Point(142, 128);
            this._ssl22.Name = "_ssl22";
            this._ssl22.Size = new System.Drawing.Size(13, 13);
            this._ssl22.State = BEMN.Forms.LedState.Off;
            this._ssl22.TabIndex = 43;
            // 
            // label247
            // 
            this.label247.AutoSize = true;
            this.label247.Location = new System.Drawing.Point(161, 128);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(41, 13);
            this.label247.TabIndex = 42;
            this.label247.Text = "ССЛ22";
            // 
            // _ssl21
            // 
            this._ssl21.Location = new System.Drawing.Point(142, 109);
            this._ssl21.Name = "_ssl21";
            this._ssl21.Size = new System.Drawing.Size(13, 13);
            this._ssl21.State = BEMN.Forms.LedState.Off;
            this._ssl21.TabIndex = 41;
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.Location = new System.Drawing.Point(161, 109);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(41, 13);
            this.label248.TabIndex = 40;
            this.label248.Text = "ССЛ21";
            // 
            // _ssl20
            // 
            this._ssl20.Location = new System.Drawing.Point(142, 90);
            this._ssl20.Name = "_ssl20";
            this._ssl20.Size = new System.Drawing.Size(13, 13);
            this._ssl20.State = BEMN.Forms.LedState.Off;
            this._ssl20.TabIndex = 39;
            // 
            // label249
            // 
            this.label249.AutoSize = true;
            this.label249.Location = new System.Drawing.Point(162, 90);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(41, 13);
            this.label249.TabIndex = 38;
            this.label249.Text = "ССЛ20";
            // 
            // _ssl19
            // 
            this._ssl19.Location = new System.Drawing.Point(142, 71);
            this._ssl19.Name = "_ssl19";
            this._ssl19.Size = new System.Drawing.Size(13, 13);
            this._ssl19.State = BEMN.Forms.LedState.Off;
            this._ssl19.TabIndex = 37;
            // 
            // label250
            // 
            this.label250.AutoSize = true;
            this.label250.Location = new System.Drawing.Point(161, 71);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(41, 13);
            this.label250.TabIndex = 36;
            this.label250.Text = "ССЛ19";
            // 
            // _ssl18
            // 
            this._ssl18.Location = new System.Drawing.Point(142, 52);
            this._ssl18.Name = "_ssl18";
            this._ssl18.Size = new System.Drawing.Size(13, 13);
            this._ssl18.State = BEMN.Forms.LedState.Off;
            this._ssl18.TabIndex = 35;
            // 
            // label251
            // 
            this.label251.AutoSize = true;
            this.label251.Location = new System.Drawing.Point(161, 52);
            this.label251.Name = "label251";
            this.label251.Size = new System.Drawing.Size(41, 13);
            this.label251.TabIndex = 34;
            this.label251.Text = "ССЛ18";
            // 
            // _ssl17
            // 
            this._ssl17.Location = new System.Drawing.Point(142, 33);
            this._ssl17.Name = "_ssl17";
            this._ssl17.Size = new System.Drawing.Size(13, 13);
            this._ssl17.State = BEMN.Forms.LedState.Off;
            this._ssl17.TabIndex = 33;
            // 
            // label252
            // 
            this.label252.AutoSize = true;
            this.label252.Location = new System.Drawing.Point(161, 33);
            this.label252.Name = "label252";
            this.label252.Size = new System.Drawing.Size(41, 13);
            this.label252.TabIndex = 32;
            this.label252.Text = "ССЛ17";
            // 
            // _ssl16
            // 
            this._ssl16.Location = new System.Drawing.Point(71, 166);
            this._ssl16.Name = "_ssl16";
            this._ssl16.Size = new System.Drawing.Size(13, 13);
            this._ssl16.State = BEMN.Forms.LedState.Off;
            this._ssl16.TabIndex = 31;
            // 
            // label253
            // 
            this.label253.AutoSize = true;
            this.label253.Location = new System.Drawing.Point(90, 166);
            this.label253.Name = "label253";
            this.label253.Size = new System.Drawing.Size(41, 13);
            this.label253.TabIndex = 30;
            this.label253.Text = "ССЛ16";
            // 
            // _ssl15
            // 
            this._ssl15.Location = new System.Drawing.Point(71, 147);
            this._ssl15.Name = "_ssl15";
            this._ssl15.Size = new System.Drawing.Size(13, 13);
            this._ssl15.State = BEMN.Forms.LedState.Off;
            this._ssl15.TabIndex = 29;
            // 
            // label254
            // 
            this.label254.AutoSize = true;
            this.label254.Location = new System.Drawing.Point(90, 147);
            this.label254.Name = "label254";
            this.label254.Size = new System.Drawing.Size(41, 13);
            this.label254.TabIndex = 28;
            this.label254.Text = "ССЛ15";
            // 
            // _ssl14
            // 
            this._ssl14.Location = new System.Drawing.Point(71, 128);
            this._ssl14.Name = "_ssl14";
            this._ssl14.Size = new System.Drawing.Size(13, 13);
            this._ssl14.State = BEMN.Forms.LedState.Off;
            this._ssl14.TabIndex = 27;
            // 
            // label255
            // 
            this.label255.AutoSize = true;
            this.label255.Location = new System.Drawing.Point(90, 128);
            this.label255.Name = "label255";
            this.label255.Size = new System.Drawing.Size(41, 13);
            this.label255.TabIndex = 26;
            this.label255.Text = "ССЛ14";
            // 
            // _ssl13
            // 
            this._ssl13.Location = new System.Drawing.Point(71, 109);
            this._ssl13.Name = "_ssl13";
            this._ssl13.Size = new System.Drawing.Size(13, 13);
            this._ssl13.State = BEMN.Forms.LedState.Off;
            this._ssl13.TabIndex = 25;
            // 
            // label256
            // 
            this.label256.AutoSize = true;
            this.label256.Location = new System.Drawing.Point(90, 109);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(41, 13);
            this.label256.TabIndex = 24;
            this.label256.Text = "ССЛ13";
            // 
            // _ssl12
            // 
            this._ssl12.Location = new System.Drawing.Point(71, 90);
            this._ssl12.Name = "_ssl12";
            this._ssl12.Size = new System.Drawing.Size(13, 13);
            this._ssl12.State = BEMN.Forms.LedState.Off;
            this._ssl12.TabIndex = 23;
            // 
            // label257
            // 
            this.label257.AutoSize = true;
            this.label257.Location = new System.Drawing.Point(90, 90);
            this.label257.Name = "label257";
            this.label257.Size = new System.Drawing.Size(41, 13);
            this.label257.TabIndex = 22;
            this.label257.Text = "ССЛ12";
            // 
            // _ssl11
            // 
            this._ssl11.Location = new System.Drawing.Point(71, 71);
            this._ssl11.Name = "_ssl11";
            this._ssl11.Size = new System.Drawing.Size(13, 13);
            this._ssl11.State = BEMN.Forms.LedState.Off;
            this._ssl11.TabIndex = 21;
            // 
            // label258
            // 
            this.label258.AutoSize = true;
            this.label258.Location = new System.Drawing.Point(90, 71);
            this.label258.Name = "label258";
            this.label258.Size = new System.Drawing.Size(41, 13);
            this.label258.TabIndex = 20;
            this.label258.Text = "ССЛ11";
            // 
            // _ssl10
            // 
            this._ssl10.Location = new System.Drawing.Point(71, 52);
            this._ssl10.Name = "_ssl10";
            this._ssl10.Size = new System.Drawing.Size(13, 13);
            this._ssl10.State = BEMN.Forms.LedState.Off;
            this._ssl10.TabIndex = 19;
            // 
            // label259
            // 
            this.label259.AutoSize = true;
            this.label259.Location = new System.Drawing.Point(90, 52);
            this.label259.Name = "label259";
            this.label259.Size = new System.Drawing.Size(41, 13);
            this.label259.TabIndex = 18;
            this.label259.Text = "ССЛ10";
            // 
            // _ssl9
            // 
            this._ssl9.Location = new System.Drawing.Point(71, 33);
            this._ssl9.Name = "_ssl9";
            this._ssl9.Size = new System.Drawing.Size(13, 13);
            this._ssl9.State = BEMN.Forms.LedState.Off;
            this._ssl9.TabIndex = 17;
            // 
            // label260
            // 
            this.label260.AutoSize = true;
            this.label260.Location = new System.Drawing.Point(90, 33);
            this.label260.Name = "label260";
            this.label260.Size = new System.Drawing.Size(35, 13);
            this.label260.TabIndex = 16;
            this.label260.Text = "ССЛ9";
            // 
            // _ssl8
            // 
            this._ssl8.Location = new System.Drawing.Point(6, 166);
            this._ssl8.Name = "_ssl8";
            this._ssl8.Size = new System.Drawing.Size(13, 13);
            this._ssl8.State = BEMN.Forms.LedState.Off;
            this._ssl8.TabIndex = 15;
            // 
            // label261
            // 
            this.label261.AutoSize = true;
            this.label261.Location = new System.Drawing.Point(25, 166);
            this.label261.Name = "label261";
            this.label261.Size = new System.Drawing.Size(35, 13);
            this.label261.TabIndex = 14;
            this.label261.Text = "ССЛ8";
            // 
            // _ssl7
            // 
            this._ssl7.Location = new System.Drawing.Point(6, 147);
            this._ssl7.Name = "_ssl7";
            this._ssl7.Size = new System.Drawing.Size(13, 13);
            this._ssl7.State = BEMN.Forms.LedState.Off;
            this._ssl7.TabIndex = 13;
            // 
            // label262
            // 
            this.label262.AutoSize = true;
            this.label262.Location = new System.Drawing.Point(25, 147);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(35, 13);
            this.label262.TabIndex = 12;
            this.label262.Text = "ССЛ7";
            // 
            // _ssl6
            // 
            this._ssl6.Location = new System.Drawing.Point(6, 128);
            this._ssl6.Name = "_ssl6";
            this._ssl6.Size = new System.Drawing.Size(13, 13);
            this._ssl6.State = BEMN.Forms.LedState.Off;
            this._ssl6.TabIndex = 11;
            // 
            // label263
            // 
            this.label263.AutoSize = true;
            this.label263.Location = new System.Drawing.Point(25, 128);
            this.label263.Name = "label263";
            this.label263.Size = new System.Drawing.Size(35, 13);
            this.label263.TabIndex = 10;
            this.label263.Text = "ССЛ6";
            // 
            // _ssl5
            // 
            this._ssl5.Location = new System.Drawing.Point(6, 109);
            this._ssl5.Name = "_ssl5";
            this._ssl5.Size = new System.Drawing.Size(13, 13);
            this._ssl5.State = BEMN.Forms.LedState.Off;
            this._ssl5.TabIndex = 9;
            // 
            // label264
            // 
            this.label264.AutoSize = true;
            this.label264.Location = new System.Drawing.Point(25, 109);
            this.label264.Name = "label264";
            this.label264.Size = new System.Drawing.Size(35, 13);
            this.label264.TabIndex = 8;
            this.label264.Text = "ССЛ5";
            // 
            // _ssl4
            // 
            this._ssl4.Location = new System.Drawing.Point(6, 90);
            this._ssl4.Name = "_ssl4";
            this._ssl4.Size = new System.Drawing.Size(13, 13);
            this._ssl4.State = BEMN.Forms.LedState.Off;
            this._ssl4.TabIndex = 7;
            // 
            // label265
            // 
            this.label265.AutoSize = true;
            this.label265.Location = new System.Drawing.Point(26, 90);
            this.label265.Name = "label265";
            this.label265.Size = new System.Drawing.Size(35, 13);
            this.label265.TabIndex = 6;
            this.label265.Text = "ССЛ4";
            // 
            // _ssl3
            // 
            this._ssl3.Location = new System.Drawing.Point(6, 71);
            this._ssl3.Name = "_ssl3";
            this._ssl3.Size = new System.Drawing.Size(13, 13);
            this._ssl3.State = BEMN.Forms.LedState.Off;
            this._ssl3.TabIndex = 5;
            // 
            // label266
            // 
            this.label266.AutoSize = true;
            this.label266.Location = new System.Drawing.Point(25, 71);
            this.label266.Name = "label266";
            this.label266.Size = new System.Drawing.Size(35, 13);
            this.label266.TabIndex = 4;
            this.label266.Text = "ССЛ3";
            // 
            // _ssl2
            // 
            this._ssl2.Location = new System.Drawing.Point(6, 52);
            this._ssl2.Name = "_ssl2";
            this._ssl2.Size = new System.Drawing.Size(13, 13);
            this._ssl2.State = BEMN.Forms.LedState.Off;
            this._ssl2.TabIndex = 3;
            // 
            // label267
            // 
            this.label267.AutoSize = true;
            this.label267.Location = new System.Drawing.Point(25, 52);
            this.label267.Name = "label267";
            this.label267.Size = new System.Drawing.Size(35, 13);
            this.label267.TabIndex = 2;
            this.label267.Text = "ССЛ2";
            // 
            // _ssl1
            // 
            this._ssl1.Location = new System.Drawing.Point(6, 33);
            this._ssl1.Name = "_ssl1";
            this._ssl1.Size = new System.Drawing.Size(13, 13);
            this._ssl1.State = BEMN.Forms.LedState.Off;
            this._ssl1.TabIndex = 1;
            // 
            // label268
            // 
            this.label268.AutoSize = true;
            this.label268.Location = new System.Drawing.Point(25, 33);
            this.label268.Name = "label268";
            this.label268.Size = new System.Drawing.Size(35, 13);
            this.label268.TabIndex = 0;
            this.label268.Text = "ССЛ1";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._vz16);
            this.groupBox12.Controls.Add(this.label111);
            this.groupBox12.Controls.Add(this._vz15);
            this.groupBox12.Controls.Add(this.label112);
            this.groupBox12.Controls.Add(this._vz14);
            this.groupBox12.Controls.Add(this.label113);
            this.groupBox12.Controls.Add(this._vz13);
            this.groupBox12.Controls.Add(this.label114);
            this.groupBox12.Controls.Add(this._vz12);
            this.groupBox12.Controls.Add(this.label115);
            this.groupBox12.Controls.Add(this._vz11);
            this.groupBox12.Controls.Add(this.label116);
            this.groupBox12.Controls.Add(this._vz10);
            this.groupBox12.Controls.Add(this.label117);
            this.groupBox12.Controls.Add(this._vz9);
            this.groupBox12.Controls.Add(this.label118);
            this.groupBox12.Controls.Add(this._vz8);
            this.groupBox12.Controls.Add(this.label119);
            this.groupBox12.Controls.Add(this._vz7);
            this.groupBox12.Controls.Add(this.label120);
            this.groupBox12.Controls.Add(this._vz6);
            this.groupBox12.Controls.Add(this.label121);
            this.groupBox12.Controls.Add(this._vz5);
            this.groupBox12.Controls.Add(this.label122);
            this.groupBox12.Controls.Add(this._vz4);
            this.groupBox12.Controls.Add(this.label123);
            this.groupBox12.Controls.Add(this._vz3);
            this.groupBox12.Controls.Add(this.label124);
            this.groupBox12.Controls.Add(this._vz2);
            this.groupBox12.Controls.Add(this.label125);
            this.groupBox12.Controls.Add(this._vz1);
            this.groupBox12.Controls.Add(this.label126);
            this.groupBox12.Location = new System.Drawing.Point(574, 449);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(141, 192);
            this.groupBox12.TabIndex = 5;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Внешние защиты";
            // 
            // _vz16
            // 
            this._vz16.Location = new System.Drawing.Point(71, 168);
            this._vz16.Name = "_vz16";
            this._vz16.Size = new System.Drawing.Size(13, 13);
            this._vz16.State = BEMN.Forms.LedState.Off;
            this._vz16.TabIndex = 31;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(90, 168);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(36, 13);
            this.label111.TabIndex = 30;
            this.label111.Text = "ВЗ-16";
            // 
            // _vz15
            // 
            this._vz15.Location = new System.Drawing.Point(71, 149);
            this._vz15.Name = "_vz15";
            this._vz15.Size = new System.Drawing.Size(13, 13);
            this._vz15.State = BEMN.Forms.LedState.Off;
            this._vz15.TabIndex = 29;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(90, 149);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(36, 13);
            this.label112.TabIndex = 28;
            this.label112.Text = "ВЗ-15";
            // 
            // _vz14
            // 
            this._vz14.Location = new System.Drawing.Point(71, 130);
            this._vz14.Name = "_vz14";
            this._vz14.Size = new System.Drawing.Size(13, 13);
            this._vz14.State = BEMN.Forms.LedState.Off;
            this._vz14.TabIndex = 27;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(90, 130);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(36, 13);
            this.label113.TabIndex = 26;
            this.label113.Text = "ВЗ-14";
            // 
            // _vz13
            // 
            this._vz13.Location = new System.Drawing.Point(71, 111);
            this._vz13.Name = "_vz13";
            this._vz13.Size = new System.Drawing.Size(13, 13);
            this._vz13.State = BEMN.Forms.LedState.Off;
            this._vz13.TabIndex = 25;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(90, 111);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(36, 13);
            this.label114.TabIndex = 24;
            this.label114.Text = "ВЗ-13";
            // 
            // _vz12
            // 
            this._vz12.Location = new System.Drawing.Point(71, 92);
            this._vz12.Name = "_vz12";
            this._vz12.Size = new System.Drawing.Size(13, 13);
            this._vz12.State = BEMN.Forms.LedState.Off;
            this._vz12.TabIndex = 23;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(90, 92);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(36, 13);
            this.label115.TabIndex = 22;
            this.label115.Text = "ВЗ-12";
            // 
            // _vz11
            // 
            this._vz11.Location = new System.Drawing.Point(71, 73);
            this._vz11.Name = "_vz11";
            this._vz11.Size = new System.Drawing.Size(13, 13);
            this._vz11.State = BEMN.Forms.LedState.Off;
            this._vz11.TabIndex = 21;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(90, 73);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(36, 13);
            this.label116.TabIndex = 20;
            this.label116.Text = "ВЗ-11";
            // 
            // _vz10
            // 
            this._vz10.Location = new System.Drawing.Point(71, 54);
            this._vz10.Name = "_vz10";
            this._vz10.Size = new System.Drawing.Size(13, 13);
            this._vz10.State = BEMN.Forms.LedState.Off;
            this._vz10.TabIndex = 19;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(90, 54);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(36, 13);
            this.label117.TabIndex = 18;
            this.label117.Text = "ВЗ-10";
            // 
            // _vz9
            // 
            this._vz9.Location = new System.Drawing.Point(71, 35);
            this._vz9.Name = "_vz9";
            this._vz9.Size = new System.Drawing.Size(13, 13);
            this._vz9.State = BEMN.Forms.LedState.Off;
            this._vz9.TabIndex = 17;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(90, 35);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(30, 13);
            this.label118.TabIndex = 16;
            this.label118.Text = "ВЗ-9";
            // 
            // _vz8
            // 
            this._vz8.Location = new System.Drawing.Point(6, 168);
            this._vz8.Name = "_vz8";
            this._vz8.Size = new System.Drawing.Size(13, 13);
            this._vz8.State = BEMN.Forms.LedState.Off;
            this._vz8.TabIndex = 15;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(25, 168);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(30, 13);
            this.label119.TabIndex = 14;
            this.label119.Text = "ВЗ-8";
            // 
            // _vz7
            // 
            this._vz7.Location = new System.Drawing.Point(6, 149);
            this._vz7.Name = "_vz7";
            this._vz7.Size = new System.Drawing.Size(13, 13);
            this._vz7.State = BEMN.Forms.LedState.Off;
            this._vz7.TabIndex = 13;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(25, 149);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(30, 13);
            this.label120.TabIndex = 12;
            this.label120.Text = "ВЗ-7";
            // 
            // _vz6
            // 
            this._vz6.Location = new System.Drawing.Point(6, 130);
            this._vz6.Name = "_vz6";
            this._vz6.Size = new System.Drawing.Size(13, 13);
            this._vz6.State = BEMN.Forms.LedState.Off;
            this._vz6.TabIndex = 11;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(25, 130);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(30, 13);
            this.label121.TabIndex = 10;
            this.label121.Text = "ВЗ-6";
            // 
            // _vz5
            // 
            this._vz5.Location = new System.Drawing.Point(6, 111);
            this._vz5.Name = "_vz5";
            this._vz5.Size = new System.Drawing.Size(13, 13);
            this._vz5.State = BEMN.Forms.LedState.Off;
            this._vz5.TabIndex = 9;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(25, 111);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(30, 13);
            this.label122.TabIndex = 8;
            this.label122.Text = "ВЗ-5";
            // 
            // _vz4
            // 
            this._vz4.Location = new System.Drawing.Point(6, 92);
            this._vz4.Name = "_vz4";
            this._vz4.Size = new System.Drawing.Size(13, 13);
            this._vz4.State = BEMN.Forms.LedState.Off;
            this._vz4.TabIndex = 7;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(25, 92);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(30, 13);
            this.label123.TabIndex = 6;
            this.label123.Text = "ВЗ-4";
            // 
            // _vz3
            // 
            this._vz3.Location = new System.Drawing.Point(6, 73);
            this._vz3.Name = "_vz3";
            this._vz3.Size = new System.Drawing.Size(13, 13);
            this._vz3.State = BEMN.Forms.LedState.Off;
            this._vz3.TabIndex = 5;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(25, 73);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(30, 13);
            this.label124.TabIndex = 4;
            this.label124.Text = "ВЗ-3";
            // 
            // _vz2
            // 
            this._vz2.Location = new System.Drawing.Point(6, 54);
            this._vz2.Name = "_vz2";
            this._vz2.Size = new System.Drawing.Size(13, 13);
            this._vz2.State = BEMN.Forms.LedState.Off;
            this._vz2.TabIndex = 3;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(25, 54);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(30, 13);
            this.label125.TabIndex = 2;
            this.label125.Text = "ВЗ-2";
            // 
            // _vz1
            // 
            this._vz1.Location = new System.Drawing.Point(6, 35);
            this._vz1.Name = "_vz1";
            this._vz1.Size = new System.Drawing.Size(13, 13);
            this._vz1.State = BEMN.Forms.LedState.Off;
            this._vz1.TabIndex = 1;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(25, 35);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(30, 13);
            this.label126.TabIndex = 0;
            this.label126.Text = "ВЗ-1";
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this._faultMeasuring);
            this.groupBox19.Controls.Add(this.label204);
            this.groupBox19.Controls.Add(this._faultUrov);
            this.groupBox19.Controls.Add(this.label207);
            this.groupBox19.Controls.Add(this._faultAlarmJournal);
            this.groupBox19.Controls.Add(this.label192);
            this.groupBox19.Controls.Add(this._faultOsc);
            this.groupBox19.Controls.Add(this.label193);
            this.groupBox19.Controls.Add(this._faultModule5);
            this.groupBox19.Controls.Add(this.label194);
            this.groupBox19.Controls.Add(this._faultModule4);
            this.groupBox19.Controls.Add(this.label195);
            this.groupBox19.Controls.Add(this._faultModule3);
            this.groupBox19.Controls.Add(this.label196);
            this.groupBox19.Controls.Add(this._faultModule2);
            this.groupBox19.Controls.Add(this.label197);
            this.groupBox19.Controls.Add(this._faultModule1);
            this.groupBox19.Controls.Add(this.label198);
            this.groupBox19.Controls.Add(this._faultSystemJournal);
            this.groupBox19.Controls.Add(this.label200);
            this.groupBox19.Controls.Add(this._faultTt2);
            this.groupBox19.Controls.Add(this._faultGroupsOfSetpoints);
            this.groupBox19.Controls.Add(this.fault2Label);
            this.groupBox19.Controls.Add(this.label201);
            this.groupBox19.Controls.Add(this._faultTt1);
            this.groupBox19.Controls.Add(this.fault1Label);
            this.groupBox19.Controls.Add(this._faultSetpoints);
            this.groupBox19.Controls.Add(this._faultTt3);
            this.groupBox19.Controls.Add(this.label202);
            this.groupBox19.Controls.Add(this.fault3Label);
            this.groupBox19.Controls.Add(this._faultLogic);
            this.groupBox19.Controls.Add(this.label203);
            this.groupBox19.Controls.Add(this._faultSoftware);
            this.groupBox19.Controls.Add(this.label205);
            this.groupBox19.Controls.Add(this._faultHardware);
            this.groupBox19.Controls.Add(this.label206);
            this.groupBox19.Location = new System.Drawing.Point(519, 218);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(239, 225);
            this.groupBox19.TabIndex = 12;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Неисправности";
            // 
            // _faultMeasuring
            // 
            this._faultMeasuring.Location = new System.Drawing.Point(6, 147);
            this._faultMeasuring.Name = "_faultMeasuring";
            this._faultMeasuring.Size = new System.Drawing.Size(13, 13);
            this._faultMeasuring.State = BEMN.Forms.LedState.Off;
            this._faultMeasuring.TabIndex = 33;
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Location = new System.Drawing.Point(25, 147);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(57, 13);
            this.label204.TabIndex = 32;
            this.label204.Text = "ТТ общая";
            // 
            // _faultUrov
            // 
            this._faultUrov.Location = new System.Drawing.Point(6, 71);
            this._faultUrov.Name = "_faultUrov";
            this._faultUrov.Size = new System.Drawing.Size(13, 13);
            this._faultUrov.State = BEMN.Forms.LedState.Off;
            this._faultUrov.TabIndex = 31;
            // 
            // label207
            // 
            this.label207.AutoSize = true;
            this.label207.Location = new System.Drawing.Point(25, 71);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(37, 13);
            this.label207.TabIndex = 30;
            this.label207.Text = "УРОВ";
            // 
            // _faultAlarmJournal
            // 
            this._faultAlarmJournal.Location = new System.Drawing.Point(120, 147);
            this._faultAlarmJournal.Name = "_faultAlarmJournal";
            this._faultAlarmJournal.Size = new System.Drawing.Size(13, 13);
            this._faultAlarmJournal.State = BEMN.Forms.LedState.Off;
            this._faultAlarmJournal.TabIndex = 29;
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Location = new System.Drawing.Point(139, 147);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(71, 13);
            this.label192.TabIndex = 28;
            this.label192.Text = "Журнала ав.";
            // 
            // _faultOsc
            // 
            this._faultOsc.Location = new System.Drawing.Point(120, 128);
            this._faultOsc.Name = "_faultOsc";
            this._faultOsc.Size = new System.Drawing.Size(13, 13);
            this._faultOsc.State = BEMN.Forms.LedState.Off;
            this._faultOsc.TabIndex = 27;
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Location = new System.Drawing.Point(139, 128);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(48, 13);
            this.label193.TabIndex = 26;
            this.label193.Text = "Осцилл.";
            // 
            // _faultModule5
            // 
            this._faultModule5.Location = new System.Drawing.Point(120, 109);
            this._faultModule5.Name = "_faultModule5";
            this._faultModule5.Size = new System.Drawing.Size(13, 13);
            this._faultModule5.State = BEMN.Forms.LedState.Off;
            this._faultModule5.TabIndex = 25;
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Location = new System.Drawing.Point(139, 109);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(54, 13);
            this.label194.TabIndex = 24;
            this.label194.Text = "Модуля 5";
            // 
            // _faultModule4
            // 
            this._faultModule4.Location = new System.Drawing.Point(120, 90);
            this._faultModule4.Name = "_faultModule4";
            this._faultModule4.Size = new System.Drawing.Size(13, 13);
            this._faultModule4.State = BEMN.Forms.LedState.Off;
            this._faultModule4.TabIndex = 23;
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Location = new System.Drawing.Point(139, 90);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(54, 13);
            this.label195.TabIndex = 22;
            this.label195.Text = "Модуля 4";
            // 
            // _faultModule3
            // 
            this._faultModule3.Location = new System.Drawing.Point(120, 71);
            this._faultModule3.Name = "_faultModule3";
            this._faultModule3.Size = new System.Drawing.Size(13, 13);
            this._faultModule3.State = BEMN.Forms.LedState.Off;
            this._faultModule3.TabIndex = 21;
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Location = new System.Drawing.Point(139, 71);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(54, 13);
            this.label196.TabIndex = 20;
            this.label196.Text = "Модуля 3";
            // 
            // _faultModule2
            // 
            this._faultModule2.Location = new System.Drawing.Point(120, 52);
            this._faultModule2.Name = "_faultModule2";
            this._faultModule2.Size = new System.Drawing.Size(13, 13);
            this._faultModule2.State = BEMN.Forms.LedState.Off;
            this._faultModule2.TabIndex = 19;
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Location = new System.Drawing.Point(139, 52);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(54, 13);
            this.label197.TabIndex = 18;
            this.label197.Text = "Модуля 2";
            // 
            // _faultModule1
            // 
            this._faultModule1.Location = new System.Drawing.Point(120, 33);
            this._faultModule1.Name = "_faultModule1";
            this._faultModule1.Size = new System.Drawing.Size(13, 13);
            this._faultModule1.State = BEMN.Forms.LedState.Off;
            this._faultModule1.TabIndex = 17;
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Location = new System.Drawing.Point(139, 33);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(54, 13);
            this.label198.TabIndex = 16;
            this.label198.Text = "Модуля 1";
            // 
            // _faultSystemJournal
            // 
            this._faultSystemJournal.Location = new System.Drawing.Point(120, 166);
            this._faultSystemJournal.Name = "_faultSystemJournal";
            this._faultSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._faultSystemJournal.State = BEMN.Forms.LedState.Off;
            this._faultSystemJournal.TabIndex = 13;
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Location = new System.Drawing.Point(139, 166);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(82, 13);
            this.label200.TabIndex = 12;
            this.label200.Text = "Журнала сист.";
            // 
            // _faultTt2
            // 
            this._faultTt2.Location = new System.Drawing.Point(6, 185);
            this._faultTt2.Name = "_faultTt2";
            this._faultTt2.Size = new System.Drawing.Size(13, 13);
            this._faultTt2.State = BEMN.Forms.LedState.Off;
            this._faultTt2.TabIndex = 11;
            // 
            // _faultGroupsOfSetpoints
            // 
            this._faultGroupsOfSetpoints.Location = new System.Drawing.Point(6, 109);
            this._faultGroupsOfSetpoints.Name = "_faultGroupsOfSetpoints";
            this._faultGroupsOfSetpoints.Size = new System.Drawing.Size(13, 13);
            this._faultGroupsOfSetpoints.State = BEMN.Forms.LedState.Off;
            this._faultGroupsOfSetpoints.TabIndex = 11;
            // 
            // fault2Label
            // 
            this.fault2Label.AutoSize = true;
            this.fault2Label.Location = new System.Drawing.Point(25, 185);
            this.fault2Label.Name = "fault2Label";
            this.fault2Label.Size = new System.Drawing.Size(46, 13);
            this.fault2Label.TabIndex = 10;
            this.fault2Label.Text = "ТТ СШ2";
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Location = new System.Drawing.Point(25, 109);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(79, 13);
            this.label201.TabIndex = 10;
            this.label201.Text = "Групп уставок";
            // 
            // _faultTt1
            // 
            this._faultTt1.Location = new System.Drawing.Point(6, 166);
            this._faultTt1.Name = "_faultTt1";
            this._faultTt1.Size = new System.Drawing.Size(13, 13);
            this._faultTt1.State = BEMN.Forms.LedState.Off;
            this._faultTt1.TabIndex = 9;
            // 
            // fault1Label
            // 
            this.fault1Label.AutoSize = true;
            this.fault1Label.Location = new System.Drawing.Point(25, 166);
            this.fault1Label.Name = "fault1Label";
            this.fault1Label.Size = new System.Drawing.Size(46, 13);
            this.fault1Label.TabIndex = 8;
            this.fault1Label.Text = "ТТ СШ1";
            // 
            // _faultSetpoints
            // 
            this._faultSetpoints.Location = new System.Drawing.Point(6, 90);
            this._faultSetpoints.Name = "_faultSetpoints";
            this._faultSetpoints.Size = new System.Drawing.Size(13, 13);
            this._faultSetpoints.State = BEMN.Forms.LedState.Off;
            this._faultSetpoints.TabIndex = 9;
            // 
            // _faultTt3
            // 
            this._faultTt3.Location = new System.Drawing.Point(6, 204);
            this._faultTt3.Name = "_faultTt3";
            this._faultTt3.Size = new System.Drawing.Size(13, 13);
            this._faultTt3.State = BEMN.Forms.LedState.Off;
            this._faultTt3.TabIndex = 7;
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Location = new System.Drawing.Point(25, 90);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(50, 13);
            this.label202.TabIndex = 8;
            this.label202.Text = "Уставок";
            // 
            // fault3Label
            // 
            this.fault3Label.AutoSize = true;
            this.fault3Label.Location = new System.Drawing.Point(25, 204);
            this.fault3Label.Name = "fault3Label";
            this.fault3Label.Size = new System.Drawing.Size(40, 13);
            this.fault3Label.TabIndex = 6;
            this.fault3Label.Text = "ТТ ПО";
            // 
            // _faultLogic
            // 
            this._faultLogic.Location = new System.Drawing.Point(6, 128);
            this._faultLogic.Name = "_faultLogic";
            this._faultLogic.Size = new System.Drawing.Size(13, 13);
            this._faultLogic.State = BEMN.Forms.LedState.Off;
            this._faultLogic.TabIndex = 7;
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Location = new System.Drawing.Point(25, 128);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(44, 13);
            this.label203.TabIndex = 6;
            this.label203.Text = "Логики";
            // 
            // _faultSoftware
            // 
            this._faultSoftware.Location = new System.Drawing.Point(6, 52);
            this._faultSoftware.Name = "_faultSoftware";
            this._faultSoftware.Size = new System.Drawing.Size(13, 13);
            this._faultSoftware.State = BEMN.Forms.LedState.Off;
            this._faultSoftware.TabIndex = 3;
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Location = new System.Drawing.Point(25, 52);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(78, 13);
            this.label205.TabIndex = 2;
            this.label205.Text = "Программная";
            // 
            // _faultHardware
            // 
            this._faultHardware.Location = new System.Drawing.Point(6, 33);
            this._faultHardware.Name = "_faultHardware";
            this._faultHardware.Size = new System.Drawing.Size(13, 13);
            this._faultHardware.State = BEMN.Forms.LedState.Off;
            this._faultHardware.TabIndex = 1;
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Location = new System.Drawing.Point(25, 33);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(67, 13);
            this.label206.TabIndex = 0;
            this.label206.Text = "Аппаратная";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this._indicator11);
            this.groupBox18.Controls.Add(this.label190);
            this.groupBox18.Controls.Add(this._indicator6);
            this.groupBox18.Controls.Add(this.label189);
            this.groupBox18.Controls.Add(this._indicator10);
            this.groupBox18.Controls.Add(this.label179);
            this.groupBox18.Controls.Add(this._indicator9);
            this.groupBox18.Controls.Add(this.label180);
            this.groupBox18.Controls.Add(this._indicator8);
            this.groupBox18.Controls.Add(this.label181);
            this.groupBox18.Controls.Add(this._indicator7);
            this.groupBox18.Controls.Add(this.label182);
            this.groupBox18.Controls.Add(this._indicator12);
            this.groupBox18.Controls.Add(this.label183);
            this.groupBox18.Controls.Add(this._indicator5);
            this.groupBox18.Controls.Add(this.label184);
            this.groupBox18.Controls.Add(this._indicator4);
            this.groupBox18.Controls.Add(this.label185);
            this.groupBox18.Controls.Add(this._indicator3);
            this.groupBox18.Controls.Add(this.label186);
            this.groupBox18.Controls.Add(this._indicator2);
            this.groupBox18.Controls.Add(this.label187);
            this.groupBox18.Controls.Add(this._indicator1);
            this.groupBox18.Controls.Add(this.label188);
            this.groupBox18.Location = new System.Drawing.Point(6, 6);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(65, 254);
            this.groupBox18.TabIndex = 11;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Инд.";
            // 
            // _indicator11
            // 
            this._indicator11.Location = new System.Drawing.Point(6, 209);
            this._indicator11.Name = "_indicator11";
            this._indicator11.Size = new System.Drawing.Size(13, 13);
            this._indicator11.State = BEMN.Forms.LedState.Off;
            this._indicator11.TabIndex = 29;
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Location = new System.Drawing.Point(25, 209);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(19, 13);
            this.label190.TabIndex = 28;
            this.label190.Text = "11";
            // 
            // _indicator6
            // 
            this._indicator6.Location = new System.Drawing.Point(6, 114);
            this._indicator6.Name = "_indicator6";
            this._indicator6.Size = new System.Drawing.Size(13, 13);
            this._indicator6.State = BEMN.Forms.LedState.Off;
            this._indicator6.TabIndex = 27;
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Location = new System.Drawing.Point(25, 114);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(13, 13);
            this.label189.TabIndex = 26;
            this.label189.Text = "6";
            // 
            // _indicator10
            // 
            this._indicator10.Location = new System.Drawing.Point(6, 190);
            this._indicator10.Name = "_indicator10";
            this._indicator10.Size = new System.Drawing.Size(13, 13);
            this._indicator10.State = BEMN.Forms.LedState.Off;
            this._indicator10.TabIndex = 25;
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(25, 190);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(19, 13);
            this.label179.TabIndex = 24;
            this.label179.Text = "10";
            // 
            // _indicator9
            // 
            this._indicator9.Location = new System.Drawing.Point(6, 171);
            this._indicator9.Name = "_indicator9";
            this._indicator9.Size = new System.Drawing.Size(13, 13);
            this._indicator9.State = BEMN.Forms.LedState.Off;
            this._indicator9.TabIndex = 23;
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Location = new System.Drawing.Point(25, 171);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(13, 13);
            this.label180.TabIndex = 22;
            this.label180.Text = "9";
            // 
            // _indicator8
            // 
            this._indicator8.Location = new System.Drawing.Point(6, 152);
            this._indicator8.Name = "_indicator8";
            this._indicator8.Size = new System.Drawing.Size(13, 13);
            this._indicator8.State = BEMN.Forms.LedState.Off;
            this._indicator8.TabIndex = 21;
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Location = new System.Drawing.Point(25, 152);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(13, 13);
            this.label181.TabIndex = 20;
            this.label181.Text = "8";
            // 
            // _indicator7
            // 
            this._indicator7.Location = new System.Drawing.Point(6, 133);
            this._indicator7.Name = "_indicator7";
            this._indicator7.Size = new System.Drawing.Size(13, 13);
            this._indicator7.State = BEMN.Forms.LedState.Off;
            this._indicator7.TabIndex = 19;
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(25, 133);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(13, 13);
            this.label182.TabIndex = 18;
            this.label182.Text = "7";
            // 
            // _indicator12
            // 
            this._indicator12.Location = new System.Drawing.Point(6, 228);
            this._indicator12.Name = "_indicator12";
            this._indicator12.Size = new System.Drawing.Size(13, 13);
            this._indicator12.State = BEMN.Forms.LedState.Off;
            this._indicator12.TabIndex = 17;
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Location = new System.Drawing.Point(25, 228);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(19, 13);
            this.label183.TabIndex = 16;
            this.label183.Text = "12";
            // 
            // _indicator5
            // 
            this._indicator5.Location = new System.Drawing.Point(6, 95);
            this._indicator5.Name = "_indicator5";
            this._indicator5.Size = new System.Drawing.Size(13, 13);
            this._indicator5.State = BEMN.Forms.LedState.Off;
            this._indicator5.TabIndex = 9;
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Location = new System.Drawing.Point(25, 95);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(13, 13);
            this.label184.TabIndex = 8;
            this.label184.Text = "5";
            // 
            // _indicator4
            // 
            this._indicator4.Location = new System.Drawing.Point(6, 76);
            this._indicator4.Name = "_indicator4";
            this._indicator4.Size = new System.Drawing.Size(13, 13);
            this._indicator4.State = BEMN.Forms.LedState.Off;
            this._indicator4.TabIndex = 7;
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(25, 76);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(13, 13);
            this.label185.TabIndex = 6;
            this.label185.Text = "4";
            // 
            // _indicator3
            // 
            this._indicator3.Location = new System.Drawing.Point(6, 57);
            this._indicator3.Name = "_indicator3";
            this._indicator3.Size = new System.Drawing.Size(13, 13);
            this._indicator3.State = BEMN.Forms.LedState.Off;
            this._indicator3.TabIndex = 5;
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(25, 57);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(13, 13);
            this.label186.TabIndex = 4;
            this.label186.Text = "3";
            // 
            // _indicator2
            // 
            this._indicator2.Location = new System.Drawing.Point(6, 38);
            this._indicator2.Name = "_indicator2";
            this._indicator2.Size = new System.Drawing.Size(13, 13);
            this._indicator2.State = BEMN.Forms.LedState.Off;
            this._indicator2.TabIndex = 3;
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Location = new System.Drawing.Point(25, 38);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(13, 13);
            this.label187.TabIndex = 2;
            this.label187.Text = "2";
            // 
            // _indicator1
            // 
            this._indicator1.Location = new System.Drawing.Point(6, 19);
            this._indicator1.Name = "_indicator1";
            this._indicator1.Size = new System.Drawing.Size(13, 13);
            this._indicator1.State = BEMN.Forms.LedState.Off;
            this._indicator1.TabIndex = 1;
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Location = new System.Drawing.Point(25, 19);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(13, 13);
            this.label188.TabIndex = 0;
            this.label188.Text = "1";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.groupBox15);
            this.groupBox17.Controls.Add(this.groupBox16);
            this.groupBox17.Location = new System.Drawing.Point(77, 6);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(184, 206);
            this.groupBox17.TabIndex = 10;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Реле";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this._module10);
            this.groupBox15.Controls.Add(this.label164);
            this.groupBox15.Controls.Add(this._module9);
            this.groupBox15.Controls.Add(this.label165);
            this.groupBox15.Controls.Add(this._module8);
            this.groupBox15.Controls.Add(this.label166);
            this.groupBox15.Controls.Add(this._module7);
            this.groupBox15.Controls.Add(this.label167);
            this.groupBox15.Controls.Add(this._module6);
            this.groupBox15.Controls.Add(this.label168);
            this.groupBox15.Controls.Add(this._module5);
            this.groupBox15.Controls.Add(this.label172);
            this.groupBox15.Controls.Add(this._module4);
            this.groupBox15.Controls.Add(this.label173);
            this.groupBox15.Controls.Add(this._module3);
            this.groupBox15.Controls.Add(this.label174);
            this.groupBox15.Controls.Add(this._module2);
            this.groupBox15.Controls.Add(this.label175);
            this.groupBox15.Controls.Add(this._module1);
            this.groupBox15.Controls.Add(this.label176);
            this.groupBox15.Location = new System.Drawing.Point(6, 19);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(93, 124);
            this.groupBox15.TabIndex = 8;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Модуль 1";
            // 
            // _module10
            // 
            this._module10.Location = new System.Drawing.Point(49, 95);
            this._module10.Name = "_module10";
            this._module10.Size = new System.Drawing.Size(13, 13);
            this._module10.State = BEMN.Forms.LedState.Off;
            this._module10.TabIndex = 25;
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Location = new System.Drawing.Point(68, 95);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(19, 13);
            this.label164.TabIndex = 24;
            this.label164.Text = "10";
            // 
            // _module9
            // 
            this._module9.Location = new System.Drawing.Point(49, 76);
            this._module9.Name = "_module9";
            this._module9.Size = new System.Drawing.Size(13, 13);
            this._module9.State = BEMN.Forms.LedState.Off;
            this._module9.TabIndex = 23;
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Location = new System.Drawing.Point(68, 76);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(13, 13);
            this.label165.TabIndex = 22;
            this.label165.Text = "9";
            // 
            // _module8
            // 
            this._module8.Location = new System.Drawing.Point(49, 57);
            this._module8.Name = "_module8";
            this._module8.Size = new System.Drawing.Size(13, 13);
            this._module8.State = BEMN.Forms.LedState.Off;
            this._module8.TabIndex = 21;
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(68, 57);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(13, 13);
            this.label166.TabIndex = 20;
            this.label166.Text = "8";
            // 
            // _module7
            // 
            this._module7.Location = new System.Drawing.Point(49, 38);
            this._module7.Name = "_module7";
            this._module7.Size = new System.Drawing.Size(13, 13);
            this._module7.State = BEMN.Forms.LedState.Off;
            this._module7.TabIndex = 19;
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(68, 38);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(13, 13);
            this.label167.TabIndex = 18;
            this.label167.Text = "7";
            // 
            // _module6
            // 
            this._module6.Location = new System.Drawing.Point(49, 19);
            this._module6.Name = "_module6";
            this._module6.Size = new System.Drawing.Size(13, 13);
            this._module6.State = BEMN.Forms.LedState.Off;
            this._module6.TabIndex = 17;
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Location = new System.Drawing.Point(68, 19);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(13, 13);
            this.label168.TabIndex = 16;
            this.label168.Text = "6";
            // 
            // _module5
            // 
            this._module5.Location = new System.Drawing.Point(6, 95);
            this._module5.Name = "_module5";
            this._module5.Size = new System.Drawing.Size(13, 13);
            this._module5.State = BEMN.Forms.LedState.Off;
            this._module5.TabIndex = 9;
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Location = new System.Drawing.Point(25, 95);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(13, 13);
            this.label172.TabIndex = 8;
            this.label172.Text = "5";
            // 
            // _module4
            // 
            this._module4.Location = new System.Drawing.Point(6, 76);
            this._module4.Name = "_module4";
            this._module4.Size = new System.Drawing.Size(13, 13);
            this._module4.State = BEMN.Forms.LedState.Off;
            this._module4.TabIndex = 7;
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Location = new System.Drawing.Point(25, 76);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(13, 13);
            this.label173.TabIndex = 6;
            this.label173.Text = "4";
            // 
            // _module3
            // 
            this._module3.Location = new System.Drawing.Point(6, 57);
            this._module3.Name = "_module3";
            this._module3.Size = new System.Drawing.Size(13, 13);
            this._module3.State = BEMN.Forms.LedState.Off;
            this._module3.TabIndex = 5;
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(25, 57);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(13, 13);
            this.label174.TabIndex = 4;
            this.label174.Text = "3";
            // 
            // _module2
            // 
            this._module2.Location = new System.Drawing.Point(6, 38);
            this._module2.Name = "_module2";
            this._module2.Size = new System.Drawing.Size(13, 13);
            this._module2.State = BEMN.Forms.LedState.Off;
            this._module2.TabIndex = 3;
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(25, 38);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(13, 13);
            this.label175.TabIndex = 2;
            this.label175.Text = "2";
            // 
            // _module1
            // 
            this._module1.Location = new System.Drawing.Point(6, 19);
            this._module1.Name = "_module1";
            this._module1.Size = new System.Drawing.Size(13, 13);
            this._module1.State = BEMN.Forms.LedState.Off;
            this._module1.TabIndex = 1;
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Location = new System.Drawing.Point(25, 19);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(13, 13);
            this.label176.TabIndex = 0;
            this.label176.Text = "1";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this._module18);
            this.groupBox16.Controls.Add(this.label161);
            this.groupBox16.Controls.Add(this._module17);
            this.groupBox16.Controls.Add(this.label162);
            this.groupBox16.Controls.Add(this._module16);
            this.groupBox16.Controls.Add(this.label163);
            this.groupBox16.Controls.Add(this._module15);
            this.groupBox16.Controls.Add(this.label169);
            this.groupBox16.Controls.Add(this._module14);
            this.groupBox16.Controls.Add(this.label170);
            this.groupBox16.Controls.Add(this._module13);
            this.groupBox16.Controls.Add(this.label171);
            this.groupBox16.Controls.Add(this._module12);
            this.groupBox16.Controls.Add(this.label177);
            this.groupBox16.Controls.Add(this._module11);
            this.groupBox16.Controls.Add(this.label178);
            this.groupBox16.Location = new System.Drawing.Point(105, 19);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(68, 181);
            this.groupBox16.TabIndex = 9;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Модуль 2";
            // 
            // _module18
            // 
            this._module18.Location = new System.Drawing.Point(6, 152);
            this._module18.Name = "_module18";
            this._module18.Size = new System.Drawing.Size(13, 13);
            this._module18.State = BEMN.Forms.LedState.Off;
            this._module18.TabIndex = 15;
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(25, 152);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(19, 13);
            this.label161.TabIndex = 14;
            this.label161.Text = "18";
            // 
            // _module17
            // 
            this._module17.Location = new System.Drawing.Point(6, 133);
            this._module17.Name = "_module17";
            this._module17.Size = new System.Drawing.Size(13, 13);
            this._module17.State = BEMN.Forms.LedState.Off;
            this._module17.TabIndex = 13;
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Location = new System.Drawing.Point(25, 133);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(19, 13);
            this.label162.TabIndex = 12;
            this.label162.Text = "17";
            // 
            // _module16
            // 
            this._module16.Location = new System.Drawing.Point(6, 114);
            this._module16.Name = "_module16";
            this._module16.Size = new System.Drawing.Size(13, 13);
            this._module16.State = BEMN.Forms.LedState.Off;
            this._module16.TabIndex = 11;
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Location = new System.Drawing.Point(25, 114);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(19, 13);
            this.label163.TabIndex = 10;
            this.label163.Text = "16";
            // 
            // _module15
            // 
            this._module15.Location = new System.Drawing.Point(6, 95);
            this._module15.Name = "_module15";
            this._module15.Size = new System.Drawing.Size(13, 13);
            this._module15.State = BEMN.Forms.LedState.Off;
            this._module15.TabIndex = 9;
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Location = new System.Drawing.Point(25, 95);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(19, 13);
            this.label169.TabIndex = 8;
            this.label169.Text = "15";
            // 
            // _module14
            // 
            this._module14.Location = new System.Drawing.Point(6, 76);
            this._module14.Name = "_module14";
            this._module14.Size = new System.Drawing.Size(13, 13);
            this._module14.State = BEMN.Forms.LedState.Off;
            this._module14.TabIndex = 7;
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(25, 76);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(19, 13);
            this.label170.TabIndex = 6;
            this.label170.Text = "14";
            // 
            // _module13
            // 
            this._module13.Location = new System.Drawing.Point(6, 57);
            this._module13.Name = "_module13";
            this._module13.Size = new System.Drawing.Size(13, 13);
            this._module13.State = BEMN.Forms.LedState.Off;
            this._module13.TabIndex = 5;
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Location = new System.Drawing.Point(25, 57);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(19, 13);
            this.label171.TabIndex = 4;
            this.label171.Text = "13";
            // 
            // _module12
            // 
            this._module12.Location = new System.Drawing.Point(6, 38);
            this._module12.Name = "_module12";
            this._module12.Size = new System.Drawing.Size(13, 13);
            this._module12.State = BEMN.Forms.LedState.Off;
            this._module12.TabIndex = 3;
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Location = new System.Drawing.Point(25, 38);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(19, 13);
            this.label177.TabIndex = 2;
            this.label177.Text = "12";
            // 
            // _module11
            // 
            this._module11.Location = new System.Drawing.Point(6, 19);
            this._module11.Name = "_module11";
            this._module11.Size = new System.Drawing.Size(13, 13);
            this._module11.State = BEMN.Forms.LedState.Off;
            this._module11.TabIndex = 1;
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Location = new System.Drawing.Point(25, 19);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(19, 13);
            this.label178.TabIndex = 0;
            this.label178.Text = "11";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this._urovPO);
            this.groupBox14.Controls.Add(this.label158);
            this.groupBox14.Controls.Add(this._urovSH2);
            this.groupBox14.Controls.Add(this.label159);
            this.groupBox14.Controls.Add(this._urovSH1);
            this.groupBox14.Controls.Add(this.label160);
            this.groupBox14.Controls.Add(this._urovPr6);
            this.groupBox14.Controls.Add(this.label144);
            this.groupBox14.Controls.Add(this._urovPr5);
            this.groupBox14.Controls.Add(this.label145);
            this.groupBox14.Controls.Add(this._urovPr4);
            this.groupBox14.Controls.Add(this.label146);
            this.groupBox14.Controls.Add(this._urovPr3);
            this.groupBox14.Controls.Add(this.label147);
            this.groupBox14.Controls.Add(this._urovPr2);
            this.groupBox14.Controls.Add(this.label148);
            this.groupBox14.Controls.Add(this._urovPr1);
            this.groupBox14.Controls.Add(this.label149);
            this.groupBox14.Location = new System.Drawing.Point(6, 266);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(65, 197);
            this.groupBox14.TabIndex = 7;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "УРОВ";
            // 
            // _urovPO
            // 
            this._urovPO.Location = new System.Drawing.Point(6, 57);
            this._urovPO.Name = "_urovPO";
            this._urovPO.Size = new System.Drawing.Size(13, 13);
            this._urovPO.State = BEMN.Forms.LedState.Off;
            this._urovPO.TabIndex = 37;
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(25, 57);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(23, 13);
            this.label158.TabIndex = 36;
            this.label158.Text = "ПО";
            // 
            // _urovSH2
            // 
            this._urovSH2.Location = new System.Drawing.Point(6, 38);
            this._urovSH2.Name = "_urovSH2";
            this._urovSH2.Size = new System.Drawing.Size(13, 13);
            this._urovSH2.State = BEMN.Forms.LedState.Off;
            this._urovSH2.TabIndex = 35;
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Location = new System.Drawing.Point(25, 38);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(29, 13);
            this.label159.TabIndex = 34;
            this.label159.Text = "СШ2";
            // 
            // _urovSH1
            // 
            this._urovSH1.Location = new System.Drawing.Point(6, 19);
            this._urovSH1.Name = "_urovSH1";
            this._urovSH1.Size = new System.Drawing.Size(13, 13);
            this._urovSH1.State = BEMN.Forms.LedState.Off;
            this._urovSH1.TabIndex = 33;
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(25, 19);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(29, 13);
            this.label160.TabIndex = 32;
            this.label160.Text = "СШ1";
            // 
            // _urovPr6
            // 
            this._urovPr6.Location = new System.Drawing.Point(6, 171);
            this._urovPr6.Name = "_urovPr6";
            this._urovPr6.Size = new System.Drawing.Size(13, 13);
            this._urovPr6.State = BEMN.Forms.LedState.Off;
            this._urovPr6.TabIndex = 11;
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(25, 171);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(32, 13);
            this.label144.TabIndex = 10;
            this.label144.Text = "Пр.ln";
            // 
            // _urovPr5
            // 
            this._urovPr5.Location = new System.Drawing.Point(6, 152);
            this._urovPr5.Name = "_urovPr5";
            this._urovPr5.Size = new System.Drawing.Size(13, 13);
            this._urovPr5.State = BEMN.Forms.LedState.Off;
            this._urovPr5.TabIndex = 9;
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(25, 152);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(30, 13);
            this.label145.TabIndex = 8;
            this.label145.Text = "Пр.5";
            // 
            // _urovPr4
            // 
            this._urovPr4.Location = new System.Drawing.Point(6, 133);
            this._urovPr4.Name = "_urovPr4";
            this._urovPr4.Size = new System.Drawing.Size(13, 13);
            this._urovPr4.State = BEMN.Forms.LedState.Off;
            this._urovPr4.TabIndex = 7;
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(25, 133);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(30, 13);
            this.label146.TabIndex = 6;
            this.label146.Text = "Пр.4";
            // 
            // _urovPr3
            // 
            this._urovPr3.Location = new System.Drawing.Point(6, 114);
            this._urovPr3.Name = "_urovPr3";
            this._urovPr3.Size = new System.Drawing.Size(13, 13);
            this._urovPr3.State = BEMN.Forms.LedState.Off;
            this._urovPr3.TabIndex = 5;
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(25, 114);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(30, 13);
            this.label147.TabIndex = 4;
            this.label147.Text = "Пр.3";
            // 
            // _urovPr2
            // 
            this._urovPr2.Location = new System.Drawing.Point(6, 95);
            this._urovPr2.Name = "_urovPr2";
            this._urovPr2.Size = new System.Drawing.Size(13, 13);
            this._urovPr2.State = BEMN.Forms.LedState.Off;
            this._urovPr2.TabIndex = 3;
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(25, 95);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(30, 13);
            this.label148.TabIndex = 2;
            this.label148.Text = "Пр.2";
            // 
            // _urovPr1
            // 
            this._urovPr1.Location = new System.Drawing.Point(6, 76);
            this._urovPr1.Name = "_urovPr1";
            this._urovPr1.Size = new System.Drawing.Size(13, 13);
            this._urovPr1.State = BEMN.Forms.LedState.Off;
            this._urovPr1.TabIndex = 1;
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(25, 76);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(30, 13);
            this.label149.TabIndex = 0;
            this.label149.Text = "Пр.1";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._vls16);
            this.groupBox10.Controls.Add(this.label63);
            this.groupBox10.Controls.Add(this._vls15);
            this.groupBox10.Controls.Add(this.label64);
            this.groupBox10.Controls.Add(this._vls14);
            this.groupBox10.Controls.Add(this.label65);
            this.groupBox10.Controls.Add(this._vls13);
            this.groupBox10.Controls.Add(this.label66);
            this.groupBox10.Controls.Add(this._vls12);
            this.groupBox10.Controls.Add(this.label67);
            this.groupBox10.Controls.Add(this._vls11);
            this.groupBox10.Controls.Add(this.label68);
            this.groupBox10.Controls.Add(this._vls10);
            this.groupBox10.Controls.Add(this.label69);
            this.groupBox10.Controls.Add(this._vls9);
            this.groupBox10.Controls.Add(this.label70);
            this.groupBox10.Controls.Add(this._vls8);
            this.groupBox10.Controls.Add(this.label71);
            this.groupBox10.Controls.Add(this._vls7);
            this.groupBox10.Controls.Add(this.label72);
            this.groupBox10.Controls.Add(this._vls6);
            this.groupBox10.Controls.Add(this.label73);
            this.groupBox10.Controls.Add(this._vls5);
            this.groupBox10.Controls.Add(this.label74);
            this.groupBox10.Controls.Add(this._vls4);
            this.groupBox10.Controls.Add(this.label75);
            this.groupBox10.Controls.Add(this._vls3);
            this.groupBox10.Controls.Add(this.label76);
            this.groupBox10.Controls.Add(this._vls2);
            this.groupBox10.Controls.Add(this.label77);
            this.groupBox10.Controls.Add(this._vls1);
            this.groupBox10.Controls.Add(this.label78);
            this.groupBox10.Location = new System.Drawing.Point(77, 218);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(141, 191);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Выходные логические сигналы ВЛС";
            // 
            // _vls16
            // 
            this._vls16.Location = new System.Drawing.Point(71, 166);
            this._vls16.Name = "_vls16";
            this._vls16.Size = new System.Drawing.Size(13, 13);
            this._vls16.State = BEMN.Forms.LedState.Off;
            this._vls16.TabIndex = 31;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(90, 166);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(41, 13);
            this.label63.TabIndex = 30;
            this.label63.Text = "ВЛС16";
            // 
            // _vls15
            // 
            this._vls15.Location = new System.Drawing.Point(71, 147);
            this._vls15.Name = "_vls15";
            this._vls15.Size = new System.Drawing.Size(13, 13);
            this._vls15.State = BEMN.Forms.LedState.Off;
            this._vls15.TabIndex = 29;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(90, 147);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(41, 13);
            this.label64.TabIndex = 28;
            this.label64.Text = "ВЛС15";
            // 
            // _vls14
            // 
            this._vls14.Location = new System.Drawing.Point(71, 128);
            this._vls14.Name = "_vls14";
            this._vls14.Size = new System.Drawing.Size(13, 13);
            this._vls14.State = BEMN.Forms.LedState.Off;
            this._vls14.TabIndex = 27;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(90, 128);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(41, 13);
            this.label65.TabIndex = 26;
            this.label65.Text = "ВЛС14";
            // 
            // _vls13
            // 
            this._vls13.Location = new System.Drawing.Point(71, 109);
            this._vls13.Name = "_vls13";
            this._vls13.Size = new System.Drawing.Size(13, 13);
            this._vls13.State = BEMN.Forms.LedState.Off;
            this._vls13.TabIndex = 25;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(90, 109);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(41, 13);
            this.label66.TabIndex = 24;
            this.label66.Text = "ВЛС13";
            // 
            // _vls12
            // 
            this._vls12.Location = new System.Drawing.Point(71, 90);
            this._vls12.Name = "_vls12";
            this._vls12.Size = new System.Drawing.Size(13, 13);
            this._vls12.State = BEMN.Forms.LedState.Off;
            this._vls12.TabIndex = 23;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(90, 90);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(41, 13);
            this.label67.TabIndex = 22;
            this.label67.Text = "ВЛС12";
            // 
            // _vls11
            // 
            this._vls11.Location = new System.Drawing.Point(71, 71);
            this._vls11.Name = "_vls11";
            this._vls11.Size = new System.Drawing.Size(13, 13);
            this._vls11.State = BEMN.Forms.LedState.Off;
            this._vls11.TabIndex = 21;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(90, 71);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(41, 13);
            this.label68.TabIndex = 20;
            this.label68.Text = "ВЛС11";
            // 
            // _vls10
            // 
            this._vls10.Location = new System.Drawing.Point(71, 52);
            this._vls10.Name = "_vls10";
            this._vls10.Size = new System.Drawing.Size(13, 13);
            this._vls10.State = BEMN.Forms.LedState.Off;
            this._vls10.TabIndex = 19;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(90, 52);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(41, 13);
            this.label69.TabIndex = 18;
            this.label69.Text = "ВЛС10";
            // 
            // _vls9
            // 
            this._vls9.Location = new System.Drawing.Point(71, 33);
            this._vls9.Name = "_vls9";
            this._vls9.Size = new System.Drawing.Size(13, 13);
            this._vls9.State = BEMN.Forms.LedState.Off;
            this._vls9.TabIndex = 17;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(90, 33);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(35, 13);
            this.label70.TabIndex = 16;
            this.label70.Text = "ВЛС9";
            // 
            // _vls8
            // 
            this._vls8.Location = new System.Drawing.Point(6, 166);
            this._vls8.Name = "_vls8";
            this._vls8.Size = new System.Drawing.Size(13, 13);
            this._vls8.State = BEMN.Forms.LedState.Off;
            this._vls8.TabIndex = 15;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(25, 166);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(35, 13);
            this.label71.TabIndex = 14;
            this.label71.Text = "ВЛС8";
            // 
            // _vls7
            // 
            this._vls7.Location = new System.Drawing.Point(6, 147);
            this._vls7.Name = "_vls7";
            this._vls7.Size = new System.Drawing.Size(13, 13);
            this._vls7.State = BEMN.Forms.LedState.Off;
            this._vls7.TabIndex = 13;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(25, 147);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(35, 13);
            this.label72.TabIndex = 12;
            this.label72.Text = "ВЛС7";
            // 
            // _vls6
            // 
            this._vls6.Location = new System.Drawing.Point(6, 128);
            this._vls6.Name = "_vls6";
            this._vls6.Size = new System.Drawing.Size(13, 13);
            this._vls6.State = BEMN.Forms.LedState.Off;
            this._vls6.TabIndex = 11;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(25, 128);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(35, 13);
            this.label73.TabIndex = 10;
            this.label73.Text = "ВЛС6";
            // 
            // _vls5
            // 
            this._vls5.Location = new System.Drawing.Point(6, 109);
            this._vls5.Name = "_vls5";
            this._vls5.Size = new System.Drawing.Size(13, 13);
            this._vls5.State = BEMN.Forms.LedState.Off;
            this._vls5.TabIndex = 9;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(25, 109);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(35, 13);
            this.label74.TabIndex = 8;
            this.label74.Text = "ВЛС5";
            // 
            // _vls4
            // 
            this._vls4.Location = new System.Drawing.Point(6, 90);
            this._vls4.Name = "_vls4";
            this._vls4.Size = new System.Drawing.Size(13, 13);
            this._vls4.State = BEMN.Forms.LedState.Off;
            this._vls4.TabIndex = 7;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(25, 90);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(35, 13);
            this.label75.TabIndex = 6;
            this.label75.Text = "ВЛС4";
            // 
            // _vls3
            // 
            this._vls3.Location = new System.Drawing.Point(6, 71);
            this._vls3.Name = "_vls3";
            this._vls3.Size = new System.Drawing.Size(13, 13);
            this._vls3.State = BEMN.Forms.LedState.Off;
            this._vls3.TabIndex = 5;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(25, 71);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(35, 13);
            this.label76.TabIndex = 4;
            this.label76.Text = "ВЛС3";
            // 
            // _vls2
            // 
            this._vls2.Location = new System.Drawing.Point(6, 52);
            this._vls2.Name = "_vls2";
            this._vls2.Size = new System.Drawing.Size(13, 13);
            this._vls2.State = BEMN.Forms.LedState.Off;
            this._vls2.TabIndex = 3;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(25, 52);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(35, 13);
            this.label77.TabIndex = 2;
            this.label77.Text = "ВЛС2";
            // 
            // _vls1
            // 
            this._vls1.Location = new System.Drawing.Point(6, 33);
            this._vls1.Name = "_vls1";
            this._vls1.Size = new System.Drawing.Size(13, 13);
            this._vls1.State = BEMN.Forms.LedState.Off;
            this._vls1.TabIndex = 1;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(25, 33);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(35, 13);
            this.label78.TabIndex = 0;
            this.label78.Text = "ВЛС1";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._ls16);
            this.groupBox9.Controls.Add(this.label47);
            this.groupBox9.Controls.Add(this._ls15);
            this.groupBox9.Controls.Add(this.label48);
            this.groupBox9.Controls.Add(this._ls14);
            this.groupBox9.Controls.Add(this.label49);
            this.groupBox9.Controls.Add(this._ls13);
            this.groupBox9.Controls.Add(this.label50);
            this.groupBox9.Controls.Add(this._ls12);
            this.groupBox9.Controls.Add(this.label51);
            this.groupBox9.Controls.Add(this._ls11);
            this.groupBox9.Controls.Add(this.label52);
            this.groupBox9.Controls.Add(this._ls10);
            this.groupBox9.Controls.Add(this.label53);
            this.groupBox9.Controls.Add(this._ls9);
            this.groupBox9.Controls.Add(this.label54);
            this.groupBox9.Controls.Add(this._ls8);
            this.groupBox9.Controls.Add(this.label55);
            this.groupBox9.Controls.Add(this._ls7);
            this.groupBox9.Controls.Add(this.label56);
            this.groupBox9.Controls.Add(this._ls6);
            this.groupBox9.Controls.Add(this.label57);
            this.groupBox9.Controls.Add(this._ls5);
            this.groupBox9.Controls.Add(this.label58);
            this.groupBox9.Controls.Add(this._ls4);
            this.groupBox9.Controls.Add(this.label59);
            this.groupBox9.Controls.Add(this._ls3);
            this.groupBox9.Controls.Add(this.label60);
            this.groupBox9.Controls.Add(this._ls2);
            this.groupBox9.Controls.Add(this.label61);
            this.groupBox9.Controls.Add(this._ls1);
            this.groupBox9.Controls.Add(this.label62);
            this.groupBox9.Location = new System.Drawing.Point(483, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(141, 206);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Входные логические сигналы ЛС";
            // 
            // _ls16
            // 
            this._ls16.Location = new System.Drawing.Point(71, 173);
            this._ls16.Name = "_ls16";
            this._ls16.Size = new System.Drawing.Size(13, 13);
            this._ls16.State = BEMN.Forms.LedState.Off;
            this._ls16.TabIndex = 31;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(90, 173);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(34, 13);
            this.label47.TabIndex = 30;
            this.label47.Text = "ЛС16";
            // 
            // _ls15
            // 
            this._ls15.Location = new System.Drawing.Point(71, 154);
            this._ls15.Name = "_ls15";
            this._ls15.Size = new System.Drawing.Size(13, 13);
            this._ls15.State = BEMN.Forms.LedState.Off;
            this._ls15.TabIndex = 29;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(90, 154);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(34, 13);
            this.label48.TabIndex = 28;
            this.label48.Text = "ЛС15";
            // 
            // _ls14
            // 
            this._ls14.Location = new System.Drawing.Point(71, 135);
            this._ls14.Name = "_ls14";
            this._ls14.Size = new System.Drawing.Size(13, 13);
            this._ls14.State = BEMN.Forms.LedState.Off;
            this._ls14.TabIndex = 27;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(90, 135);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(34, 13);
            this.label49.TabIndex = 26;
            this.label49.Text = "ЛС14";
            // 
            // _ls13
            // 
            this._ls13.Location = new System.Drawing.Point(71, 116);
            this._ls13.Name = "_ls13";
            this._ls13.Size = new System.Drawing.Size(13, 13);
            this._ls13.State = BEMN.Forms.LedState.Off;
            this._ls13.TabIndex = 25;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(90, 116);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(34, 13);
            this.label50.TabIndex = 24;
            this.label50.Text = "ЛС13";
            // 
            // _ls12
            // 
            this._ls12.Location = new System.Drawing.Point(71, 97);
            this._ls12.Name = "_ls12";
            this._ls12.Size = new System.Drawing.Size(13, 13);
            this._ls12.State = BEMN.Forms.LedState.Off;
            this._ls12.TabIndex = 23;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(90, 97);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(34, 13);
            this.label51.TabIndex = 22;
            this.label51.Text = "ЛС12";
            // 
            // _ls11
            // 
            this._ls11.Location = new System.Drawing.Point(71, 78);
            this._ls11.Name = "_ls11";
            this._ls11.Size = new System.Drawing.Size(13, 13);
            this._ls11.State = BEMN.Forms.LedState.Off;
            this._ls11.TabIndex = 21;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(90, 78);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(34, 13);
            this.label52.TabIndex = 20;
            this.label52.Text = "ЛС11";
            // 
            // _ls10
            // 
            this._ls10.Location = new System.Drawing.Point(71, 59);
            this._ls10.Name = "_ls10";
            this._ls10.Size = new System.Drawing.Size(13, 13);
            this._ls10.State = BEMN.Forms.LedState.Off;
            this._ls10.TabIndex = 19;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(90, 59);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(34, 13);
            this.label53.TabIndex = 18;
            this.label53.Text = "ЛС10";
            // 
            // _ls9
            // 
            this._ls9.Location = new System.Drawing.Point(71, 40);
            this._ls9.Name = "_ls9";
            this._ls9.Size = new System.Drawing.Size(13, 13);
            this._ls9.State = BEMN.Forms.LedState.Off;
            this._ls9.TabIndex = 17;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(90, 40);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(28, 13);
            this.label54.TabIndex = 16;
            this.label54.Text = "ЛС9";
            // 
            // _ls8
            // 
            this._ls8.Location = new System.Drawing.Point(6, 173);
            this._ls8.Name = "_ls8";
            this._ls8.Size = new System.Drawing.Size(13, 13);
            this._ls8.State = BEMN.Forms.LedState.Off;
            this._ls8.TabIndex = 15;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(25, 173);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(28, 13);
            this.label55.TabIndex = 14;
            this.label55.Text = "ЛС8";
            // 
            // _ls7
            // 
            this._ls7.Location = new System.Drawing.Point(6, 154);
            this._ls7.Name = "_ls7";
            this._ls7.Size = new System.Drawing.Size(13, 13);
            this._ls7.State = BEMN.Forms.LedState.Off;
            this._ls7.TabIndex = 13;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(25, 154);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(28, 13);
            this.label56.TabIndex = 12;
            this.label56.Text = "ЛС7";
            // 
            // _ls6
            // 
            this._ls6.Location = new System.Drawing.Point(6, 135);
            this._ls6.Name = "_ls6";
            this._ls6.Size = new System.Drawing.Size(13, 13);
            this._ls6.State = BEMN.Forms.LedState.Off;
            this._ls6.TabIndex = 11;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(25, 135);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(28, 13);
            this.label57.TabIndex = 10;
            this.label57.Text = "ЛС6";
            // 
            // _ls5
            // 
            this._ls5.Location = new System.Drawing.Point(6, 116);
            this._ls5.Name = "_ls5";
            this._ls5.Size = new System.Drawing.Size(13, 13);
            this._ls5.State = BEMN.Forms.LedState.Off;
            this._ls5.TabIndex = 9;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(25, 116);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(28, 13);
            this.label58.TabIndex = 8;
            this.label58.Text = "ЛС5";
            // 
            // _ls4
            // 
            this._ls4.Location = new System.Drawing.Point(6, 97);
            this._ls4.Name = "_ls4";
            this._ls4.Size = new System.Drawing.Size(13, 13);
            this._ls4.State = BEMN.Forms.LedState.Off;
            this._ls4.TabIndex = 7;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(25, 97);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(28, 13);
            this.label59.TabIndex = 6;
            this.label59.Text = "ЛС4";
            // 
            // _ls3
            // 
            this._ls3.Location = new System.Drawing.Point(6, 78);
            this._ls3.Name = "_ls3";
            this._ls3.Size = new System.Drawing.Size(13, 13);
            this._ls3.State = BEMN.Forms.LedState.Off;
            this._ls3.TabIndex = 5;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(25, 78);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(28, 13);
            this.label60.TabIndex = 4;
            this.label60.Text = "ЛС3";
            // 
            // _ls2
            // 
            this._ls2.Location = new System.Drawing.Point(6, 59);
            this._ls2.Name = "_ls2";
            this._ls2.Size = new System.Drawing.Size(13, 13);
            this._ls2.State = BEMN.Forms.LedState.Off;
            this._ls2.TabIndex = 3;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(25, 59);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(28, 13);
            this.label61.TabIndex = 2;
            this.label61.Text = "ЛС2";
            // 
            // _ls1
            // 
            this._ls1.Location = new System.Drawing.Point(6, 40);
            this._ls1.Name = "_ls1";
            this._ls1.Size = new System.Drawing.Size(13, 13);
            this._ls1.State = BEMN.Forms.LedState.Off;
            this._ls1.TabIndex = 1;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(25, 40);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(28, 13);
            this.label62.TabIndex = 0;
            this.label62.Text = "ЛС1";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox8);
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Location = new System.Drawing.Point(267, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(210, 206);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Дискретные входы";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._d24);
            this.groupBox8.Controls.Add(this.label39);
            this.groupBox8.Controls.Add(this._d23);
            this.groupBox8.Controls.Add(this.label40);
            this.groupBox8.Controls.Add(this._d22);
            this.groupBox8.Controls.Add(this.label41);
            this.groupBox8.Controls.Add(this._d21);
            this.groupBox8.Controls.Add(this.label42);
            this.groupBox8.Controls.Add(this._d20);
            this.groupBox8.Controls.Add(this.label43);
            this.groupBox8.Controls.Add(this._d19);
            this.groupBox8.Controls.Add(this.label44);
            this.groupBox8.Controls.Add(this._d18);
            this.groupBox8.Controls.Add(this.label45);
            this.groupBox8.Controls.Add(this._d17);
            this.groupBox8.Controls.Add(this.label46);
            this.groupBox8.Controls.Add(this._d16);
            this.groupBox8.Controls.Add(this.label31);
            this.groupBox8.Controls.Add(this._d15);
            this.groupBox8.Controls.Add(this.label32);
            this.groupBox8.Controls.Add(this._d14);
            this.groupBox8.Controls.Add(this.label33);
            this.groupBox8.Controls.Add(this._d13);
            this.groupBox8.Controls.Add(this.label34);
            this.groupBox8.Controls.Add(this._d12);
            this.groupBox8.Controls.Add(this.label35);
            this.groupBox8.Controls.Add(this._d11);
            this.groupBox8.Controls.Add(this.label36);
            this.groupBox8.Controls.Add(this._d10);
            this.groupBox8.Controls.Add(this.label37);
            this.groupBox8.Controls.Add(this._d9);
            this.groupBox8.Controls.Add(this.label38);
            this.groupBox8.Location = new System.Drawing.Point(86, 19);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(118, 181);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Модуль 3";
            // 
            // _d24
            // 
            this._d24.Location = new System.Drawing.Point(63, 152);
            this._d24.Name = "_d24";
            this._d24.Size = new System.Drawing.Size(13, 13);
            this._d24.State = BEMN.Forms.LedState.Off;
            this._d24.TabIndex = 31;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(82, 152);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(28, 13);
            this.label39.TabIndex = 30;
            this.label39.Text = "Д24";
            // 
            // _d23
            // 
            this._d23.Location = new System.Drawing.Point(63, 133);
            this._d23.Name = "_d23";
            this._d23.Size = new System.Drawing.Size(13, 13);
            this._d23.State = BEMN.Forms.LedState.Off;
            this._d23.TabIndex = 29;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(82, 133);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(28, 13);
            this.label40.TabIndex = 28;
            this.label40.Text = "Д23";
            // 
            // _d22
            // 
            this._d22.Location = new System.Drawing.Point(63, 114);
            this._d22.Name = "_d22";
            this._d22.Size = new System.Drawing.Size(13, 13);
            this._d22.State = BEMN.Forms.LedState.Off;
            this._d22.TabIndex = 27;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(82, 114);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(28, 13);
            this.label41.TabIndex = 26;
            this.label41.Text = "Д22";
            // 
            // _d21
            // 
            this._d21.Location = new System.Drawing.Point(63, 95);
            this._d21.Name = "_d21";
            this._d21.Size = new System.Drawing.Size(13, 13);
            this._d21.State = BEMN.Forms.LedState.Off;
            this._d21.TabIndex = 25;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(82, 95);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(28, 13);
            this.label42.TabIndex = 24;
            this.label42.Text = "Д21";
            // 
            // _d20
            // 
            this._d20.Location = new System.Drawing.Point(63, 76);
            this._d20.Name = "_d20";
            this._d20.Size = new System.Drawing.Size(13, 13);
            this._d20.State = BEMN.Forms.LedState.Off;
            this._d20.TabIndex = 23;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(82, 76);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(28, 13);
            this.label43.TabIndex = 22;
            this.label43.Text = "Д20";
            // 
            // _d19
            // 
            this._d19.Location = new System.Drawing.Point(63, 57);
            this._d19.Name = "_d19";
            this._d19.Size = new System.Drawing.Size(13, 13);
            this._d19.State = BEMN.Forms.LedState.Off;
            this._d19.TabIndex = 21;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(82, 57);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(28, 13);
            this.label44.TabIndex = 20;
            this.label44.Text = "Д19";
            // 
            // _d18
            // 
            this._d18.Location = new System.Drawing.Point(63, 38);
            this._d18.Name = "_d18";
            this._d18.Size = new System.Drawing.Size(13, 13);
            this._d18.State = BEMN.Forms.LedState.Off;
            this._d18.TabIndex = 19;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(82, 38);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(28, 13);
            this.label45.TabIndex = 18;
            this.label45.Text = "Д18";
            // 
            // _d17
            // 
            this._d17.Location = new System.Drawing.Point(63, 19);
            this._d17.Name = "_d17";
            this._d17.Size = new System.Drawing.Size(13, 13);
            this._d17.State = BEMN.Forms.LedState.Off;
            this._d17.TabIndex = 17;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(82, 19);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(28, 13);
            this.label46.TabIndex = 16;
            this.label46.Text = "Д17";
            // 
            // _d16
            // 
            this._d16.Location = new System.Drawing.Point(6, 152);
            this._d16.Name = "_d16";
            this._d16.Size = new System.Drawing.Size(13, 13);
            this._d16.State = BEMN.Forms.LedState.Off;
            this._d16.TabIndex = 15;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(25, 152);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(28, 13);
            this.label31.TabIndex = 14;
            this.label31.Text = "Д16";
            // 
            // _d15
            // 
            this._d15.Location = new System.Drawing.Point(6, 133);
            this._d15.Name = "_d15";
            this._d15.Size = new System.Drawing.Size(13, 13);
            this._d15.State = BEMN.Forms.LedState.Off;
            this._d15.TabIndex = 13;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(25, 133);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(28, 13);
            this.label32.TabIndex = 12;
            this.label32.Text = "Д15";
            // 
            // _d14
            // 
            this._d14.Location = new System.Drawing.Point(6, 114);
            this._d14.Name = "_d14";
            this._d14.Size = new System.Drawing.Size(13, 13);
            this._d14.State = BEMN.Forms.LedState.Off;
            this._d14.TabIndex = 11;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(25, 114);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(28, 13);
            this.label33.TabIndex = 10;
            this.label33.Text = "Д14";
            // 
            // _d13
            // 
            this._d13.Location = new System.Drawing.Point(6, 95);
            this._d13.Name = "_d13";
            this._d13.Size = new System.Drawing.Size(13, 13);
            this._d13.State = BEMN.Forms.LedState.Off;
            this._d13.TabIndex = 9;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(25, 95);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(28, 13);
            this.label34.TabIndex = 8;
            this.label34.Text = "Д13";
            // 
            // _d12
            // 
            this._d12.Location = new System.Drawing.Point(6, 76);
            this._d12.Name = "_d12";
            this._d12.Size = new System.Drawing.Size(13, 13);
            this._d12.State = BEMN.Forms.LedState.Off;
            this._d12.TabIndex = 7;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(25, 76);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(28, 13);
            this.label35.TabIndex = 6;
            this.label35.Text = "Д12";
            // 
            // _d11
            // 
            this._d11.Location = new System.Drawing.Point(6, 57);
            this._d11.Name = "_d11";
            this._d11.Size = new System.Drawing.Size(13, 13);
            this._d11.State = BEMN.Forms.LedState.Off;
            this._d11.TabIndex = 5;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(25, 57);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(28, 13);
            this.label36.TabIndex = 4;
            this.label36.Text = "Д11";
            // 
            // _d10
            // 
            this._d10.Location = new System.Drawing.Point(6, 38);
            this._d10.Name = "_d10";
            this._d10.Size = new System.Drawing.Size(13, 13);
            this._d10.State = BEMN.Forms.LedState.Off;
            this._d10.TabIndex = 3;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(25, 38);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(28, 13);
            this.label37.TabIndex = 2;
            this.label37.Text = "Д10";
            // 
            // _d9
            // 
            this._d9.Location = new System.Drawing.Point(6, 19);
            this._d9.Name = "_d9";
            this._d9.Size = new System.Drawing.Size(13, 13);
            this._d9.State = BEMN.Forms.LedState.Off;
            this._d9.TabIndex = 1;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(25, 19);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(22, 13);
            this.label38.TabIndex = 0;
            this.label38.Text = "Д9";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._d8);
            this.groupBox7.Controls.Add(this.label30);
            this.groupBox7.Controls.Add(this._d7);
            this.groupBox7.Controls.Add(this.label29);
            this.groupBox7.Controls.Add(this._d6);
            this.groupBox7.Controls.Add(this.label28);
            this.groupBox7.Controls.Add(this._d5);
            this.groupBox7.Controls.Add(this.label27);
            this.groupBox7.Controls.Add(this._d4);
            this.groupBox7.Controls.Add(this.label26);
            this.groupBox7.Controls.Add(this._d3);
            this.groupBox7.Controls.Add(this.label25);
            this.groupBox7.Controls.Add(this._d2);
            this.groupBox7.Controls.Add(this.label24);
            this.groupBox7.Controls.Add(this._d1);
            this.groupBox7.Controls.Add(this.label23);
            this.groupBox7.Location = new System.Drawing.Point(6, 19);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(72, 181);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Модуль 2";
            // 
            // _d8
            // 
            this._d8.Location = new System.Drawing.Point(6, 152);
            this._d8.Name = "_d8";
            this._d8.Size = new System.Drawing.Size(13, 13);
            this._d8.State = BEMN.Forms.LedState.Off;
            this._d8.TabIndex = 15;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(25, 152);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(22, 13);
            this.label30.TabIndex = 14;
            this.label30.Text = "Д8";
            // 
            // _d7
            // 
            this._d7.Location = new System.Drawing.Point(6, 133);
            this._d7.Name = "_d7";
            this._d7.Size = new System.Drawing.Size(13, 13);
            this._d7.State = BEMN.Forms.LedState.Off;
            this._d7.TabIndex = 13;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(25, 133);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(22, 13);
            this.label29.TabIndex = 12;
            this.label29.Text = "Д7";
            // 
            // _d6
            // 
            this._d6.Location = new System.Drawing.Point(6, 114);
            this._d6.Name = "_d6";
            this._d6.Size = new System.Drawing.Size(13, 13);
            this._d6.State = BEMN.Forms.LedState.Off;
            this._d6.TabIndex = 11;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(25, 114);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(22, 13);
            this.label28.TabIndex = 10;
            this.label28.Text = "Д6";
            // 
            // _d5
            // 
            this._d5.Location = new System.Drawing.Point(6, 95);
            this._d5.Name = "_d5";
            this._d5.Size = new System.Drawing.Size(13, 13);
            this._d5.State = BEMN.Forms.LedState.Off;
            this._d5.TabIndex = 9;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(25, 95);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(22, 13);
            this.label27.TabIndex = 8;
            this.label27.Text = "Д5";
            // 
            // _d4
            // 
            this._d4.Location = new System.Drawing.Point(6, 76);
            this._d4.Name = "_d4";
            this._d4.Size = new System.Drawing.Size(13, 13);
            this._d4.State = BEMN.Forms.LedState.Off;
            this._d4.TabIndex = 7;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(25, 76);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(22, 13);
            this.label26.TabIndex = 6;
            this.label26.Text = "Д4";
            // 
            // _d3
            // 
            this._d3.Location = new System.Drawing.Point(6, 57);
            this._d3.Name = "_d3";
            this._d3.Size = new System.Drawing.Size(13, 13);
            this._d3.State = BEMN.Forms.LedState.Off;
            this._d3.TabIndex = 5;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(25, 57);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 13);
            this.label25.TabIndex = 4;
            this.label25.Text = "Д3";
            // 
            // _d2
            // 
            this._d2.Location = new System.Drawing.Point(6, 38);
            this._d2.Name = "_d2";
            this._d2.Size = new System.Drawing.Size(13, 13);
            this._d2.State = BEMN.Forms.LedState.Off;
            this._d2.TabIndex = 3;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(25, 38);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(22, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Д2";
            // 
            // _d1
            // 
            this._d1.Location = new System.Drawing.Point(6, 19);
            this._d1.Name = "_d1";
            this._d1.Size = new System.Drawing.Size(13, 13);
            this._d1.State = BEMN.Forms.LedState.Off;
            this._d1.TabIndex = 1;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(25, 19);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(22, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Д1";
            // 
            // _controlSignalsTabPage
            // 
            this._controlSignalsTabPage.Controls.Add(this.groupBox28);
            this._controlSignalsTabPage.Controls.Add(this.groupBox27);
            this._controlSignalsTabPage.Location = new System.Drawing.Point(4, 22);
            this._controlSignalsTabPage.Name = "_controlSignalsTabPage";
            this._controlSignalsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._controlSignalsTabPage.Size = new System.Drawing.Size(827, 659);
            this._controlSignalsTabPage.TabIndex = 2;
            this._controlSignalsTabPage.Text = "Управляющие сигналы";
            this._controlSignalsTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this._reservedControl);
            this.groupBox28.Controls.Add(this._reserveGroupButton);
            this.groupBox28.Controls.Add(this._mainGroupButton);
            this.groupBox28.Controls.Add(this.label224);
            this.groupBox28.Controls.Add(this.label228);
            this.groupBox28.Controls.Add(this._reservedGroupOfSetpointsFromInterface);
            this.groupBox28.Controls.Add(this._mainGroupOfSetpointsFromInterface);
            this.groupBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox28.Location = new System.Drawing.Point(328, 6);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(191, 83);
            this.groupBox28.TabIndex = 24;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Группа уставок";
            // 
            // _reservedControl
            // 
            this._reservedControl.Location = new System.Drawing.Point(89, 19);
            this._reservedControl.Name = "_reservedControl";
            this._reservedControl.Size = new System.Drawing.Size(13, 13);
            this._reservedControl.State = BEMN.Forms.LedState.Off;
            this._reservedControl.TabIndex = 31;
            this._reservedControl.Visible = false;
            // 
            // _reserveGroupButton
            // 
            this._reserveGroupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._reserveGroupButton.Location = new System.Drawing.Point(99, 56);
            this._reserveGroupButton.Name = "_reserveGroupButton";
            this._reserveGroupButton.Size = new System.Drawing.Size(86, 23);
            this._reserveGroupButton.TabIndex = 30;
            this._reserveGroupButton.Text = "Переключить";
            this._reserveGroupButton.UseVisualStyleBackColor = true;
            this._reserveGroupButton.Click += new System.EventHandler(this._reserveGroupButton_Click);
            // 
            // _mainGroupButton
            // 
            this._mainGroupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._mainGroupButton.Location = new System.Drawing.Point(6, 56);
            this._mainGroupButton.Name = "_mainGroupButton";
            this._mainGroupButton.Size = new System.Drawing.Size(86, 23);
            this._mainGroupButton.TabIndex = 29;
            this._mainGroupButton.Text = "Переключить";
            this._mainGroupButton.UseVisualStyleBackColor = true;
            this._mainGroupButton.Click += new System.EventHandler(this._mainGroupButton_Click);
            // 
            // label224
            // 
            this.label224.AutoSize = true;
            this.label224.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label224.Location = new System.Drawing.Point(110, 39);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(62, 13);
            this.label224.TabIndex = 28;
            this.label224.Text = "Резервная";
            // 
            // label228
            // 
            this.label228.AutoSize = true;
            this.label228.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label228.Location = new System.Drawing.Point(21, 39);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(57, 13);
            this.label228.TabIndex = 27;
            this.label228.Text = "Основная";
            // 
            // _reservedGroupOfSetpointsFromInterface
            // 
            this._reservedGroupOfSetpointsFromInterface.Location = new System.Drawing.Point(136, 19);
            this._reservedGroupOfSetpointsFromInterface.Name = "_reservedGroupOfSetpointsFromInterface";
            this._reservedGroupOfSetpointsFromInterface.Size = new System.Drawing.Size(13, 13);
            this._reservedGroupOfSetpointsFromInterface.State = BEMN.Forms.LedState.Off;
            this._reservedGroupOfSetpointsFromInterface.TabIndex = 24;
            // 
            // _mainGroupOfSetpointsFromInterface
            // 
            this._mainGroupOfSetpointsFromInterface.Location = new System.Drawing.Point(41, 19);
            this._mainGroupOfSetpointsFromInterface.Name = "_mainGroupOfSetpointsFromInterface";
            this._mainGroupOfSetpointsFromInterface.Size = new System.Drawing.Size(14, 13);
            this._mainGroupOfSetpointsFromInterface.State = BEMN.Forms.LedState.Off;
            this._mainGroupOfSetpointsFromInterface.TabIndex = 23;
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.groupBox40);
            this.groupBox27.Controls.Add(this._availabilityFaultSystemJournal);
            this.groupBox27.Controls.Add(this._newRecordOscJournal);
            this.groupBox27.Controls.Add(this._oscStart);
            this.groupBox27.Controls.Add(this._resetTt);
            this.groupBox27.Controls.Add(this._newRecordAlarmJournal);
            this.groupBox27.Controls.Add(this._newRecordSystemJournal);
            this.groupBox27.Controls.Add(this._resetAnButton);
            this.groupBox27.Controls.Add(this._resetAvailabilityFaultSystemJournalButton);
            this.groupBox27.Controls.Add(this._resetOscJournalButton);
            this.groupBox27.Controls.Add(this._resetAlarmJournalButton);
            this.groupBox27.Controls.Add(this._resetSystemJournalButton);
            this.groupBox27.Controls.Add(this.label227);
            this.groupBox27.Controls.Add(this.label225);
            this.groupBox27.Controls.Add(this.label226);
            this.groupBox27.Controls.Add(this.label231);
            this.groupBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox27.Location = new System.Drawing.Point(6, 6);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(316, 251);
            this.groupBox27.TabIndex = 23;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Управляющие сигналы";
            // 
            // groupBox40
            // 
            this.groupBox40.Controls.Add(this._logicState);
            this.groupBox40.Controls.Add(this.stopLogic);
            this.groupBox40.Controls.Add(this.startLogic);
            this.groupBox40.Controls.Add(this.label314);
            this.groupBox40.Location = new System.Drawing.Point(6, 112);
            this.groupBox40.Name = "groupBox40";
            this.groupBox40.Size = new System.Drawing.Size(304, 60);
            this.groupBox40.TabIndex = 56;
            this.groupBox40.TabStop = false;
            this.groupBox40.Text = "Свободно программируемая логика";
            // 
            // _logicState
            // 
            this._logicState.Location = new System.Drawing.Point(6, 25);
            this._logicState.Name = "_logicState";
            this._logicState.Size = new System.Drawing.Size(13, 13);
            this._logicState.State = BEMN.Forms.LedState.Off;
            this._logicState.TabIndex = 17;
            // 
            // stopLogic
            // 
            this.stopLogic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stopLogic.Location = new System.Drawing.Point(223, 32);
            this.stopLogic.Name = "stopLogic";
            this.stopLogic.Size = new System.Drawing.Size(75, 23);
            this.stopLogic.TabIndex = 15;
            this.stopLogic.Text = "Остановить";
            this.stopLogic.UseVisualStyleBackColor = true;
            this.stopLogic.Click += new System.EventHandler(this.stopLogic_Click);
            // 
            // startLogic
            // 
            this.startLogic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.startLogic.Location = new System.Drawing.Point(223, 10);
            this.startLogic.Name = "startLogic";
            this.startLogic.Size = new System.Drawing.Size(75, 23);
            this.startLogic.TabIndex = 16;
            this.startLogic.Text = "Запустить";
            this.startLogic.UseVisualStyleBackColor = true;
            this.startLogic.Click += new System.EventHandler(this.startLogic_Click);
            // 
            // label314
            // 
            this.label314.AutoSize = true;
            this.label314.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label314.Location = new System.Drawing.Point(30, 25);
            this.label314.Name = "label314";
            this.label314.Size = new System.Drawing.Size(137, 13);
            this.label314.TabIndex = 14;
            this.label314.Text = "Состояние задачи логики";
            // 
            // _availabilityFaultSystemJournal
            // 
            this._availabilityFaultSystemJournal.Location = new System.Drawing.Point(6, 88);
            this._availabilityFaultSystemJournal.Name = "_availabilityFaultSystemJournal";
            this._availabilityFaultSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._availabilityFaultSystemJournal.State = BEMN.Forms.LedState.Off;
            this._availabilityFaultSystemJournal.TabIndex = 13;
            // 
            // _newRecordOscJournal
            // 
            this._newRecordOscJournal.Location = new System.Drawing.Point(6, 65);
            this._newRecordOscJournal.Name = "_newRecordOscJournal";
            this._newRecordOscJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordOscJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordOscJournal.TabIndex = 12;
            // 
            // _oscStart
            // 
            this._oscStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._oscStart.Location = new System.Drawing.Point(6, 222);
            this._oscStart.Name = "_oscStart";
            this._oscStart.Size = new System.Drawing.Size(304, 23);
            this._oscStart.TabIndex = 9;
            this._oscStart.Text = "Пуск осциллографа";
            this._oscStart.UseVisualStyleBackColor = true;
            this._oscStart.Click += new System.EventHandler(this.StartOscClick);
            // 
            // _resetTt
            // 
            this._resetTt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetTt.Location = new System.Drawing.Point(6, 199);
            this._resetTt.Name = "_resetTt";
            this._resetTt.Size = new System.Drawing.Size(304, 23);
            this._resetTt.TabIndex = 9;
            this._resetTt.Text = "Сброс неисправности ТТ";
            this._resetTt.UseVisualStyleBackColor = true;
            this._resetTt.Click += new System.EventHandler(this.ResetTtClick);
            // 
            // _newRecordAlarmJournal
            // 
            this._newRecordAlarmJournal.Location = new System.Drawing.Point(6, 42);
            this._newRecordAlarmJournal.Name = "_newRecordAlarmJournal";
            this._newRecordAlarmJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordAlarmJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordAlarmJournal.TabIndex = 11;
            // 
            // _newRecordSystemJournal
            // 
            this._newRecordSystemJournal.Location = new System.Drawing.Point(6, 19);
            this._newRecordSystemJournal.Name = "_newRecordSystemJournal";
            this._newRecordSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordSystemJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordSystemJournal.TabIndex = 10;
            // 
            // _resetAnButton
            // 
            this._resetAnButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAnButton.Location = new System.Drawing.Point(6, 176);
            this._resetAnButton.Name = "_resetAnButton";
            this._resetAnButton.Size = new System.Drawing.Size(304, 23);
            this._resetAnButton.TabIndex = 9;
            this._resetAnButton.Text = "Сброс индикации";
            this._resetAnButton.UseVisualStyleBackColor = true;
            this._resetAnButton.Click += new System.EventHandler(this._resetAnButton_Click);
            // 
            // _resetAvailabilityFaultSystemJournalButton
            // 
            this._resetAvailabilityFaultSystemJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAvailabilityFaultSystemJournalButton.Location = new System.Drawing.Point(235, 83);
            this._resetAvailabilityFaultSystemJournalButton.Name = "_resetAvailabilityFaultSystemJournalButton";
            this._resetAvailabilityFaultSystemJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetAvailabilityFaultSystemJournalButton.TabIndex = 8;
            this._resetAvailabilityFaultSystemJournalButton.Text = "Сбросить";
            this._resetAvailabilityFaultSystemJournalButton.UseVisualStyleBackColor = true;
            this._resetAvailabilityFaultSystemJournalButton.Click += new System.EventHandler(this._resetAvailabilityFaultSystemJournalButton_Click);
            // 
            // _resetOscJournalButton
            // 
            this._resetOscJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetOscJournalButton.Location = new System.Drawing.Point(235, 60);
            this._resetOscJournalButton.Name = "_resetOscJournalButton";
            this._resetOscJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetOscJournalButton.TabIndex = 7;
            this._resetOscJournalButton.Text = "Сбросить";
            this._resetOscJournalButton.UseVisualStyleBackColor = true;
            this._resetOscJournalButton.Click += new System.EventHandler(this._resetOscJournalButton_Click);
            // 
            // _resetAlarmJournalButton
            // 
            this._resetAlarmJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAlarmJournalButton.Location = new System.Drawing.Point(235, 37);
            this._resetAlarmJournalButton.Name = "_resetAlarmJournalButton";
            this._resetAlarmJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetAlarmJournalButton.TabIndex = 6;
            this._resetAlarmJournalButton.Text = "Сбросить";
            this._resetAlarmJournalButton.UseVisualStyleBackColor = true;
            this._resetAlarmJournalButton.Click += new System.EventHandler(this._resetAlarmJournalButton_Click);
            // 
            // _resetSystemJournalButton
            // 
            this._resetSystemJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetSystemJournalButton.Location = new System.Drawing.Point(235, 14);
            this._resetSystemJournalButton.Name = "_resetSystemJournalButton";
            this._resetSystemJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetSystemJournalButton.TabIndex = 5;
            this._resetSystemJournalButton.Text = "Сбросить";
            this._resetSystemJournalButton.UseVisualStyleBackColor = true;
            this._resetSystemJournalButton.Click += new System.EventHandler(this._resetSystemJournalButton_Click);
            // 
            // label227
            // 
            this.label227.AutoSize = true;
            this.label227.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label227.Location = new System.Drawing.Point(30, 88);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(119, 13);
            this.label227.TabIndex = 3;
            this.label227.Text = "Новая неисправность";
            // 
            // label225
            // 
            this.label225.AutoSize = true;
            this.label225.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label225.Location = new System.Drawing.Point(30, 65);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(123, 13);
            this.label225.TabIndex = 2;
            this.label225.Text = "Новая осциллограмма";
            // 
            // label226
            // 
            this.label226.AutoSize = true;
            this.label226.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label226.Location = new System.Drawing.Point(30, 42);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(172, 13);
            this.label226.TabIndex = 1;
            this.label226.Text = "Новая запись в журнале аварий";
            // 
            // label231
            // 
            this.label231.AutoSize = true;
            this.label231.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label231.Location = new System.Drawing.Point(30, 19);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(181, 13);
            this.label231.TabIndex = 0;
            this.label231.Text = "Новая запись в журнале системы";
            // 
            // Mr902MeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(833, 686);
            this.Controls.Add(this._dataBaseTabControl);
            this.Name = "Mr902MeasuringForm";
            this.Text = "Mr902MeasuringForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Mr901MeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.Mr902MeasuringForm_Load);
            this._dataBaseTabControl.ResumeLayout(false);
            this._analogTabPage.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this._discretTabPage.ResumeLayout(false);
            this.groupBox22.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this._controlSignalsTabPage.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox40.ResumeLayout(false);
            this.groupBox40.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl _dataBaseTabControl;
        private System.Windows.Forms.TabPage _analogTabPage;
        private BEMN.Forms.DateTimeControl _dateTimeControl;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox _idb1TB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _ida1TB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage _discretTabPage;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.Label label223;
        private BEMN.Forms.LedControl _difDefenceActualIoPo;
        private BEMN.Forms.LedControl _difDefenceActualIoSh2;
        private BEMN.Forms.LedControl _difDefenceActualIoSh1;
        private System.Windows.Forms.Label label216;
        private System.Windows.Forms.Label label217;
        private System.Windows.Forms.Label label218;
        private BEMN.Forms.LedControl _difDefenceActualChtoPo;
        private BEMN.Forms.LedControl _difDefenceActualChtoSh2;
        private BEMN.Forms.LedControl _difDefenceActualChtoSh1;
        private BEMN.Forms.LedControl _difDefenceActualSrabPo;
        private BEMN.Forms.LedControl _difDefenceActualSrabSh2;
        private BEMN.Forms.LedControl _difDefenceActualSrabSh1;
        private System.Windows.Forms.Label label219;
        private System.Windows.Forms.Label label220;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.Label label215;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.Label label213;
        private BEMN.Forms.LedControl _difDefenceInstantaneousChtoPo;
        private BEMN.Forms.LedControl _difDefenceInstantaneousChtoSh2;
        private BEMN.Forms.LedControl _difDefenceInstantaneousChtoSh1;
        private BEMN.Forms.LedControl _difDefenceInstantaneousSrabPo;
        private BEMN.Forms.LedControl _difDefenceInstantaneousSrabSh2;
        private BEMN.Forms.LedControl _difDefenceInstantaneousSrabSh1;
        private System.Windows.Forms.Label label212;
        private System.Windows.Forms.Label label211;
        private System.Windows.Forms.GroupBox groupBox20;
        private BEMN.Forms.LedControl _reservedGroupOfSetpoints;
        private System.Windows.Forms.Label label208;
        private BEMN.Forms.LedControl _mainGroupOfSetpoints;
        private System.Windows.Forms.Label label209;
        private BEMN.Forms.LedControl _fault;
        private System.Windows.Forms.Label label210;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.Label label199;
        private BEMN.Forms.LedControl _i32Io;
        private System.Windows.Forms.Label label221;
        private BEMN.Forms.LedControl _i31Io;
        private System.Windows.Forms.Label label222;
        private BEMN.Forms.LedControl _i30Io;
        private System.Windows.Forms.Label label229;
        private BEMN.Forms.LedControl _i29Io;
        private System.Windows.Forms.Label label230;
        private BEMN.Forms.LedControl _i28Io;
        private System.Windows.Forms.Label label270;
        private BEMN.Forms.LedControl _i27Io;
        private System.Windows.Forms.Label label269;
        private BEMN.Forms.LedControl _i26Io;
        private BEMN.Forms.LedControl _i32;
        private BEMN.Forms.LedControl _i25Io;
        private System.Windows.Forms.Label label95;
        private BEMN.Forms.LedControl _i31;
        private System.Windows.Forms.Label label96;
        private BEMN.Forms.LedControl _i30;
        private System.Windows.Forms.Label label97;
        private BEMN.Forms.LedControl _i29;
        private System.Windows.Forms.Label label98;
        private BEMN.Forms.LedControl _i28;
        private System.Windows.Forms.Label label99;
        private BEMN.Forms.LedControl _i27;
        private System.Windows.Forms.Label label100;
        private BEMN.Forms.LedControl _i24Io;
        private BEMN.Forms.LedControl _i26;
        private System.Windows.Forms.Label label101;
        private BEMN.Forms.LedControl _i23Io;
        private BEMN.Forms.LedControl _i25;
        private System.Windows.Forms.Label label102;
        private BEMN.Forms.LedControl _i22Io;
        private BEMN.Forms.LedControl _i24;
        private System.Windows.Forms.Label label103;
        private BEMN.Forms.LedControl _i21Io;
        private BEMN.Forms.LedControl _i23;
        private System.Windows.Forms.Label label104;
        private BEMN.Forms.LedControl _i20Io;
        private BEMN.Forms.LedControl _i22;
        private System.Windows.Forms.Label label105;
        private BEMN.Forms.LedControl _i19Io;
        private BEMN.Forms.LedControl _i21;
        private System.Windows.Forms.Label label106;
        private BEMN.Forms.LedControl _i18Io;
        private BEMN.Forms.LedControl _i20;
        private System.Windows.Forms.Label label107;
        private BEMN.Forms.LedControl _i17Io;
        private BEMN.Forms.LedControl _i19;
        private System.Windows.Forms.Label label108;
        private BEMN.Forms.LedControl _i18;
        private System.Windows.Forms.Label label109;
        private BEMN.Forms.LedControl _i16Io;
        private BEMN.Forms.LedControl _i17;
        private System.Windows.Forms.Label label110;
        private BEMN.Forms.LedControl _i15Io;
        private BEMN.Forms.LedControl _i16;
        private System.Windows.Forms.Label label79;
        private BEMN.Forms.LedControl _i14Io;
        private BEMN.Forms.LedControl _i15;
        private System.Windows.Forms.Label label80;
        private BEMN.Forms.LedControl _i13Io;
        private BEMN.Forms.LedControl _i14;
        private System.Windows.Forms.Label label81;
        private BEMN.Forms.LedControl _i12Io;
        private BEMN.Forms.LedControl _i13;
        private System.Windows.Forms.Label label82;
        private BEMN.Forms.LedControl _i11Io;
        private BEMN.Forms.LedControl _i12;
        private System.Windows.Forms.Label label83;
        private BEMN.Forms.LedControl _i10Io;
        private BEMN.Forms.LedControl _i11;
        private System.Windows.Forms.Label label84;
        private BEMN.Forms.LedControl _i9Io;
        private BEMN.Forms.LedControl _i10;
        private System.Windows.Forms.Label label85;
        private BEMN.Forms.LedControl _i8Io;
        private BEMN.Forms.LedControl _i9;
        private System.Windows.Forms.Label label86;
        private BEMN.Forms.LedControl _i7Io;
        private BEMN.Forms.LedControl _i8;
        private System.Windows.Forms.Label label87;
        private BEMN.Forms.LedControl _i6Io;
        private BEMN.Forms.LedControl _i7;
        private System.Windows.Forms.Label label88;
        private BEMN.Forms.LedControl _i5Io;
        private BEMN.Forms.LedControl _i6;
        private System.Windows.Forms.Label label89;
        private BEMN.Forms.LedControl _i4Io;
        private BEMN.Forms.LedControl _i5;
        private System.Windows.Forms.Label label90;
        private BEMN.Forms.LedControl _i3Io;
        private BEMN.Forms.LedControl _i4;
        private System.Windows.Forms.Label label91;
        private BEMN.Forms.LedControl _i2Io;
        private BEMN.Forms.LedControl _i3;
        private System.Windows.Forms.Label label92;
        private BEMN.Forms.LedControl _i1Io;
        private BEMN.Forms.LedControl _i2;
        private System.Windows.Forms.Label label93;
        private BEMN.Forms.LedControl _i1;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.GroupBox groupBox21;
        private BEMN.Forms.LedControl _ssl32;
        private System.Windows.Forms.Label label237;
        private BEMN.Forms.LedControl _ssl31;
        private System.Windows.Forms.Label label238;
        private BEMN.Forms.LedControl _ssl30;
        private System.Windows.Forms.Label label239;
        private BEMN.Forms.LedControl _ssl29;
        private System.Windows.Forms.Label label240;
        private BEMN.Forms.LedControl _ssl28;
        private System.Windows.Forms.Label label241;
        private BEMN.Forms.LedControl _ssl27;
        private System.Windows.Forms.Label label242;
        private BEMN.Forms.LedControl _ssl26;
        private System.Windows.Forms.Label label243;
        private BEMN.Forms.LedControl _ssl25;
        private System.Windows.Forms.Label label244;
        private BEMN.Forms.LedControl _ssl24;
        private System.Windows.Forms.Label label245;
        private BEMN.Forms.LedControl _ssl23;
        private System.Windows.Forms.Label label246;
        private BEMN.Forms.LedControl _ssl22;
        private System.Windows.Forms.Label label247;
        private BEMN.Forms.LedControl _ssl21;
        private System.Windows.Forms.Label label248;
        private BEMN.Forms.LedControl _ssl20;
        private System.Windows.Forms.Label label249;
        private BEMN.Forms.LedControl _ssl19;
        private System.Windows.Forms.Label label250;
        private BEMN.Forms.LedControl _ssl18;
        private System.Windows.Forms.Label label251;
        private BEMN.Forms.LedControl _ssl17;
        private System.Windows.Forms.Label label252;
        private BEMN.Forms.LedControl _ssl16;
        private System.Windows.Forms.Label label253;
        private BEMN.Forms.LedControl _ssl15;
        private System.Windows.Forms.Label label254;
        private BEMN.Forms.LedControl _ssl14;
        private System.Windows.Forms.Label label255;
        private BEMN.Forms.LedControl _ssl13;
        private System.Windows.Forms.Label label256;
        private BEMN.Forms.LedControl _ssl12;
        private System.Windows.Forms.Label label257;
        private BEMN.Forms.LedControl _ssl11;
        private System.Windows.Forms.Label label258;
        private BEMN.Forms.LedControl _ssl10;
        private System.Windows.Forms.Label label259;
        private BEMN.Forms.LedControl _ssl9;
        private System.Windows.Forms.Label label260;
        private BEMN.Forms.LedControl _ssl8;
        private System.Windows.Forms.Label label261;
        private BEMN.Forms.LedControl _ssl7;
        private System.Windows.Forms.Label label262;
        private BEMN.Forms.LedControl _ssl6;
        private System.Windows.Forms.Label label263;
        private BEMN.Forms.LedControl _ssl5;
        private System.Windows.Forms.Label label264;
        private BEMN.Forms.LedControl _ssl4;
        private System.Windows.Forms.Label label265;
        private BEMN.Forms.LedControl _ssl3;
        private System.Windows.Forms.Label label266;
        private BEMN.Forms.LedControl _ssl2;
        private System.Windows.Forms.Label label267;
        private BEMN.Forms.LedControl _ssl1;
        private System.Windows.Forms.Label label268;
        private System.Windows.Forms.GroupBox groupBox12;
        private BEMN.Forms.LedControl _vz16;
        private System.Windows.Forms.Label label111;
        private BEMN.Forms.LedControl _vz15;
        private System.Windows.Forms.Label label112;
        private BEMN.Forms.LedControl _vz14;
        private System.Windows.Forms.Label label113;
        private BEMN.Forms.LedControl _vz13;
        private System.Windows.Forms.Label label114;
        private BEMN.Forms.LedControl _vz12;
        private System.Windows.Forms.Label label115;
        private BEMN.Forms.LedControl _vz11;
        private System.Windows.Forms.Label label116;
        private BEMN.Forms.LedControl _vz10;
        private System.Windows.Forms.Label label117;
        private BEMN.Forms.LedControl _vz9;
        private System.Windows.Forms.Label label118;
        private BEMN.Forms.LedControl _vz8;
        private System.Windows.Forms.Label label119;
        private BEMN.Forms.LedControl _vz7;
        private System.Windows.Forms.Label label120;
        private BEMN.Forms.LedControl _vz6;
        private System.Windows.Forms.Label label121;
        private BEMN.Forms.LedControl _vz5;
        private System.Windows.Forms.Label label122;
        private BEMN.Forms.LedControl _vz4;
        private System.Windows.Forms.Label label123;
        private BEMN.Forms.LedControl _vz3;
        private System.Windows.Forms.Label label124;
        private BEMN.Forms.LedControl _vz2;
        private System.Windows.Forms.Label label125;
        private BEMN.Forms.LedControl _vz1;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.GroupBox groupBox19;
        private BEMN.Forms.LedControl _faultUrov;
        private System.Windows.Forms.Label label207;
        private BEMN.Forms.LedControl _faultAlarmJournal;
        private System.Windows.Forms.Label label192;
        private BEMN.Forms.LedControl _faultOsc;
        private System.Windows.Forms.Label label193;
        private BEMN.Forms.LedControl _faultModule5;
        private System.Windows.Forms.Label label194;
        private BEMN.Forms.LedControl _faultModule4;
        private System.Windows.Forms.Label label195;
        private BEMN.Forms.LedControl _faultModule3;
        private System.Windows.Forms.Label label196;
        private BEMN.Forms.LedControl _faultModule2;
        private System.Windows.Forms.Label label197;
        private BEMN.Forms.LedControl _faultModule1;
        private System.Windows.Forms.Label label198;
        private BEMN.Forms.LedControl _faultSystemJournal;
        private System.Windows.Forms.Label label200;
        private BEMN.Forms.LedControl _faultGroupsOfSetpoints;
        private System.Windows.Forms.Label label201;
        private BEMN.Forms.LedControl _faultSetpoints;
        private System.Windows.Forms.Label label202;
        private BEMN.Forms.LedControl _faultLogic;
        private System.Windows.Forms.Label label203;
        private BEMN.Forms.LedControl _faultSoftware;
        private System.Windows.Forms.Label label205;
        private BEMN.Forms.LedControl _faultHardware;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.GroupBox groupBox18;
        private BEMN.Forms.LedControl _indicator11;
        private System.Windows.Forms.Label label190;
        private BEMN.Forms.LedControl _indicator6;
        private System.Windows.Forms.Label label189;
        private BEMN.Forms.LedControl _indicator10;
        private System.Windows.Forms.Label label179;
        private BEMN.Forms.LedControl _indicator9;
        private System.Windows.Forms.Label label180;
        private BEMN.Forms.LedControl _indicator8;
        private System.Windows.Forms.Label label181;
        private BEMN.Forms.LedControl _indicator7;
        private System.Windows.Forms.Label label182;
        private BEMN.Forms.LedControl _indicator12;
        private System.Windows.Forms.Label label183;
        private BEMN.Forms.LedControl _indicator5;
        private System.Windows.Forms.Label label184;
        private BEMN.Forms.LedControl _indicator4;
        private System.Windows.Forms.Label label185;
        private BEMN.Forms.LedControl _indicator3;
        private System.Windows.Forms.Label label186;
        private BEMN.Forms.LedControl _indicator2;
        private System.Windows.Forms.Label label187;
        private BEMN.Forms.LedControl _indicator1;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.GroupBox groupBox15;
        private BEMN.Forms.LedControl _module10;
        private System.Windows.Forms.Label label164;
        private BEMN.Forms.LedControl _module9;
        private System.Windows.Forms.Label label165;
        private BEMN.Forms.LedControl _module8;
        private System.Windows.Forms.Label label166;
        private BEMN.Forms.LedControl _module7;
        private System.Windows.Forms.Label label167;
        private BEMN.Forms.LedControl _module6;
        private System.Windows.Forms.Label label168;
        private BEMN.Forms.LedControl _module5;
        private System.Windows.Forms.Label label172;
        private BEMN.Forms.LedControl _module4;
        private System.Windows.Forms.Label label173;
        private BEMN.Forms.LedControl _module3;
        private System.Windows.Forms.Label label174;
        private BEMN.Forms.LedControl _module2;
        private System.Windows.Forms.Label label175;
        private BEMN.Forms.LedControl _module1;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.GroupBox groupBox16;
        private BEMN.Forms.LedControl _module18;
        private System.Windows.Forms.Label label161;
        private BEMN.Forms.LedControl _module17;
        private System.Windows.Forms.Label label162;
        private BEMN.Forms.LedControl _module16;
        private System.Windows.Forms.Label label163;
        private BEMN.Forms.LedControl _module15;
        private System.Windows.Forms.Label label169;
        private BEMN.Forms.LedControl _module14;
        private System.Windows.Forms.Label label170;
        private BEMN.Forms.LedControl _module13;
        private System.Windows.Forms.Label label171;
        private BEMN.Forms.LedControl _module12;
        private System.Windows.Forms.Label label177;
        private BEMN.Forms.LedControl _module11;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.GroupBox groupBox14;
        private BEMN.Forms.LedControl _urovPO;
        private System.Windows.Forms.Label label158;
        private BEMN.Forms.LedControl _urovSH2;
        private System.Windows.Forms.Label label159;
        private BEMN.Forms.LedControl _urovSH1;
        private System.Windows.Forms.Label label160;
        private BEMN.Forms.LedControl _urovPr6;
        private System.Windows.Forms.Label label144;
        private BEMN.Forms.LedControl _urovPr5;
        private System.Windows.Forms.Label label145;
        private BEMN.Forms.LedControl _urovPr4;
        private System.Windows.Forms.Label label146;
        private BEMN.Forms.LedControl _urovPr3;
        private System.Windows.Forms.Label label147;
        private BEMN.Forms.LedControl _urovPr2;
        private System.Windows.Forms.Label label148;
        private BEMN.Forms.LedControl _urovPr1;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.GroupBox groupBox10;
        private BEMN.Forms.LedControl _vls16;
        private System.Windows.Forms.Label label63;
        private BEMN.Forms.LedControl _vls15;
        private System.Windows.Forms.Label label64;
        private BEMN.Forms.LedControl _vls14;
        private System.Windows.Forms.Label label65;
        private BEMN.Forms.LedControl _vls13;
        private System.Windows.Forms.Label label66;
        private BEMN.Forms.LedControl _vls12;
        private System.Windows.Forms.Label label67;
        private BEMN.Forms.LedControl _vls11;
        private System.Windows.Forms.Label label68;
        private BEMN.Forms.LedControl _vls10;
        private System.Windows.Forms.Label label69;
        private BEMN.Forms.LedControl _vls9;
        private System.Windows.Forms.Label label70;
        private BEMN.Forms.LedControl _vls8;
        private System.Windows.Forms.Label label71;
        private BEMN.Forms.LedControl _vls7;
        private System.Windows.Forms.Label label72;
        private BEMN.Forms.LedControl _vls6;
        private System.Windows.Forms.Label label73;
        private BEMN.Forms.LedControl _vls5;
        private System.Windows.Forms.Label label74;
        private BEMN.Forms.LedControl _vls4;
        private System.Windows.Forms.Label label75;
        private BEMN.Forms.LedControl _vls3;
        private System.Windows.Forms.Label label76;
        private BEMN.Forms.LedControl _vls2;
        private System.Windows.Forms.Label label77;
        private BEMN.Forms.LedControl _vls1;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.GroupBox groupBox9;
        private BEMN.Forms.LedControl _ls16;
        private System.Windows.Forms.Label label47;
        private BEMN.Forms.LedControl _ls15;
        private System.Windows.Forms.Label label48;
        private BEMN.Forms.LedControl _ls14;
        private System.Windows.Forms.Label label49;
        private BEMN.Forms.LedControl _ls13;
        private System.Windows.Forms.Label label50;
        private BEMN.Forms.LedControl _ls12;
        private System.Windows.Forms.Label label51;
        private BEMN.Forms.LedControl _ls11;
        private System.Windows.Forms.Label label52;
        private BEMN.Forms.LedControl _ls10;
        private System.Windows.Forms.Label label53;
        private BEMN.Forms.LedControl _ls9;
        private System.Windows.Forms.Label label54;
        private BEMN.Forms.LedControl _ls8;
        private System.Windows.Forms.Label label55;
        private BEMN.Forms.LedControl _ls7;
        private System.Windows.Forms.Label label56;
        private BEMN.Forms.LedControl _ls6;
        private System.Windows.Forms.Label label57;
        private BEMN.Forms.LedControl _ls5;
        private System.Windows.Forms.Label label58;
        private BEMN.Forms.LedControl _ls4;
        private System.Windows.Forms.Label label59;
        private BEMN.Forms.LedControl _ls3;
        private System.Windows.Forms.Label label60;
        private BEMN.Forms.LedControl _ls2;
        private System.Windows.Forms.Label label61;
        private BEMN.Forms.LedControl _ls1;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox8;
        private BEMN.Forms.LedControl _d24;
        private System.Windows.Forms.Label label39;
        private BEMN.Forms.LedControl _d23;
        private System.Windows.Forms.Label label40;
        private BEMN.Forms.LedControl _d22;
        private System.Windows.Forms.Label label41;
        private BEMN.Forms.LedControl _d21;
        private System.Windows.Forms.Label label42;
        private BEMN.Forms.LedControl _d20;
        private System.Windows.Forms.Label label43;
        private BEMN.Forms.LedControl _d19;
        private System.Windows.Forms.Label label44;
        private BEMN.Forms.LedControl _d18;
        private System.Windows.Forms.Label label45;
        private BEMN.Forms.LedControl _d17;
        private System.Windows.Forms.Label label46;
        private BEMN.Forms.LedControl _d16;
        private System.Windows.Forms.Label label31;
        private BEMN.Forms.LedControl _d15;
        private System.Windows.Forms.Label label32;
        private BEMN.Forms.LedControl _d14;
        private System.Windows.Forms.Label label33;
        private BEMN.Forms.LedControl _d13;
        private System.Windows.Forms.Label label34;
        private BEMN.Forms.LedControl _d12;
        private System.Windows.Forms.Label label35;
        private BEMN.Forms.LedControl _d11;
        private System.Windows.Forms.Label label36;
        private BEMN.Forms.LedControl _d10;
        private System.Windows.Forms.Label label37;
        private BEMN.Forms.LedControl _d9;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.GroupBox groupBox7;
        private BEMN.Forms.LedControl _d8;
        private System.Windows.Forms.Label label30;
        private BEMN.Forms.LedControl _d7;
        private System.Windows.Forms.Label label29;
        private BEMN.Forms.LedControl _d6;
        private System.Windows.Forms.Label label28;
        private BEMN.Forms.LedControl _d5;
        private System.Windows.Forms.Label label27;
        private BEMN.Forms.LedControl _d4;
        private System.Windows.Forms.Label label26;
        private BEMN.Forms.LedControl _d3;
        private System.Windows.Forms.Label label25;
        private BEMN.Forms.LedControl _d2;
        private System.Windows.Forms.Label label24;
        private BEMN.Forms.LedControl _d1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TabPage _controlSignalsTabPage;
        private System.Windows.Forms.GroupBox groupBox28;
        private BEMN.Forms.LedControl _reservedControl;
        private System.Windows.Forms.Button _reserveGroupButton;
        private System.Windows.Forms.Button _mainGroupButton;
        private System.Windows.Forms.Label label224;
        private System.Windows.Forms.Label label228;
        private BEMN.Forms.LedControl _reservedGroupOfSetpointsFromInterface;
        private BEMN.Forms.LedControl _mainGroupOfSetpointsFromInterface;
        private System.Windows.Forms.GroupBox groupBox27;
        private BEMN.Forms.LedControl _availabilityFaultSystemJournal;
        private BEMN.Forms.LedControl _newRecordOscJournal;
        private BEMN.Forms.LedControl _newRecordAlarmJournal;
        private BEMN.Forms.LedControl _newRecordSystemJournal;
        private System.Windows.Forms.Button _resetAnButton;
        private System.Windows.Forms.Button _resetAvailabilityFaultSystemJournalButton;
        private System.Windows.Forms.Button _resetOscJournalButton;
        private System.Windows.Forms.Button _resetAlarmJournalButton;
        private System.Windows.Forms.Button _resetSystemJournalButton;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.Label label225;
        private System.Windows.Forms.Label label226;
        private System.Windows.Forms.Label label231;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox _itc3TB;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.TextBox _itb3TB;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.TextBox _ita3TB;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.TextBox _idc3TB;
        private System.Windows.Forms.Label label234;
        private System.Windows.Forms.TextBox _idb3TB;
        private System.Windows.Forms.Label label235;
        private System.Windows.Forms.TextBox _ida3TB;
        private System.Windows.Forms.Label label236;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox _itc2TB;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.TextBox _itb2TB;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.TextBox _ita2TB;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.TextBox _idc2TB;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.TextBox _idb2TB;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.TextBox _ida2TB;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.TextBox _itc1TB;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.TextBox _itb1TB;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.TextBox _ita1TB;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.TextBox _idc1TB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.TextBox _i16TB;
        private System.Windows.Forms.Label label290;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.TextBox _i15TB;
        private System.Windows.Forms.TextBox _i14TB;
        private System.Windows.Forms.TextBox _i13TB;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.TextBox _i12TB;
        private System.Windows.Forms.TextBox _i11TB;
        private System.Windows.Forms.TextBox _i10TB;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.TextBox _i9TB;
        private System.Windows.Forms.TextBox _i8TB;
        private System.Windows.Forms.TextBox _i7TB;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.TextBox _i6TB;
        private System.Windows.Forms.TextBox _i5TB;
        private System.Windows.Forms.TextBox _i4TB;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox _i3TB;
        private System.Windows.Forms.Label label280;
        private System.Windows.Forms.TextBox _i2TB;
        private System.Windows.Forms.Label label281;
        private System.Windows.Forms.TextBox _i1TB;
        private System.Windows.Forms.Label label282;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label271;
        private System.Windows.Forms.Label label272;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label276;
        private System.Windows.Forms.Label label283;
        private System.Windows.Forms.Label label284;
        private System.Windows.Forms.Button _resetTt;
        private System.Windows.Forms.Button _oscStart;
        private BEMN.Forms.LedControl _faultTt2;
        private System.Windows.Forms.Label fault2Label;
        private BEMN.Forms.LedControl _faultTt1;
        private System.Windows.Forms.Label fault1Label;
        private BEMN.Forms.LedControl _faultTt3;
        private System.Windows.Forms.Label fault3Label;
        private BEMN.Forms.LedControl _faultMeasuring;
        private System.Windows.Forms.Label label204;
        private System.Windows.Forms.GroupBox groupBox40;
        private BEMN.Forms.LedControl _logicState;
        private System.Windows.Forms.Button stopLogic;
        private System.Windows.Forms.Button startLogic;
        private System.Windows.Forms.Label label314;
    }
}