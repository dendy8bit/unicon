﻿namespace BEMN.MR902.Old.Forms
{
    partial class Mr902AlarmJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this._alarmJournalGrid = new System.Windows.Forms.DataGridView();
            this._loadAlarmJournalButton = new System.Windows.Forms.Button();
            this._readAlarmJournalButton = new System.Windows.Forms.Button();
            this._saveAlarmJournalButton = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._openAlarmJournalDialog = new System.Windows.Forms.OpenFileDialog();
            this._saveAlarmJournalDialog = new System.Windows.Forms.SaveFileDialog();
            this._saveToHTMLBtn = new System.Windows.Forms.Button();
            this._saveAlarmHTMLdialog = new System.Windows.Forms.SaveFileDialog();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msg1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._codeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ida1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Idb1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Idc1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ita1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Itb1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Itc1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ida2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Idb2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Idc2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ita2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Itb2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Itc2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ida3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Idb3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Idc3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ita3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Itb3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Itc3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I4Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I5Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I6Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I7Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I8Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I9Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I10Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I11Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I12Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I13Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I14Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I15Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I16Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _alarmJournalGrid
            // 
            this._alarmJournalGrid.AllowUserToAddRows = false;
            this._alarmJournalGrid.AllowUserToDeleteRows = false;
            this._alarmJournalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._alarmJournalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._alarmJournalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._alarmJournalGrid.BackgroundColor = System.Drawing.Color.White;
            this._alarmJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._alarmJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msg1Col,
            this._msgCol,
            this._codeCol,
            this._typeCol,
            this._IaCol,
            this._Ida1Col,
            this._Idb1Col,
            this._Idc1Col,
            this._Ita1Col,
            this._Itb1Col,
            this._Itc1Col,
            this._Ida2Col,
            this._Idb2Col,
            this._Idc2Col,
            this._Ita2Col,
            this._Itb2Col,
            this._Itc2Col,
            this._Ida3Col,
            this._Idb3Col,
            this._Idc3Col,
            this._Ita3Col,
            this._Itb3Col,
            this._Itc3Col,
            this._I1Col,
            this._I2Col,
            this._I3Col,
            this._I4Col,
            this._I5Col,
            this._I6Col,
            this._I7Col,
            this._I8Col,
            this._I9Col,
            this._I10Col,
            this._I11Col,
            this._I12Col,
            this._I13Col,
            this._I14Col,
            this._I15Col,
            this._I16Col,
            this._D0Col,
            this._D1Col,
            this._D2Col});
            this._alarmJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._alarmJournalGrid.Margin = new System.Windows.Forms.Padding(100, 3, 3, 100);
            this._alarmJournalGrid.Name = "_alarmJournalGrid";
            this._alarmJournalGrid.RowHeadersVisible = false;
            this._alarmJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._alarmJournalGrid.Size = new System.Drawing.Size(837, 512);
            this._alarmJournalGrid.TabIndex = 20;
            // 
            // _loadAlarmJournalButton
            // 
            this._loadAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._loadAlarmJournalButton.Location = new System.Drawing.Point(310, 514);
            this._loadAlarmJournalButton.Name = "_loadAlarmJournalButton";
            this._loadAlarmJournalButton.Size = new System.Drawing.Size(143, 23);
            this._loadAlarmJournalButton.TabIndex = 24;
            this._loadAlarmJournalButton.Text = "Загрузить из файла";
            this._loadAlarmJournalButton.UseVisualStyleBackColor = true;
            this._loadAlarmJournalButton.Click += new System.EventHandler(this._loadAlarmJournalButton_Click);
            // 
            // _readAlarmJournalButton
            // 
            this._readAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readAlarmJournalButton.Location = new System.Drawing.Point(12, 514);
            this._readAlarmJournalButton.Name = "_readAlarmJournalButton";
            this._readAlarmJournalButton.Size = new System.Drawing.Size(143, 23);
            this._readAlarmJournalButton.TabIndex = 22;
            this._readAlarmJournalButton.Text = "Прочитать";
            this._readAlarmJournalButton.UseVisualStyleBackColor = true;
            this._readAlarmJournalButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // _saveAlarmJournalButton
            // 
            this._saveAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveAlarmJournalButton.Location = new System.Drawing.Point(161, 514);
            this._saveAlarmJournalButton.Name = "_saveAlarmJournalButton";
            this._saveAlarmJournalButton.Size = new System.Drawing.Size(143, 23);
            this._saveAlarmJournalButton.TabIndex = 23;
            this._saveAlarmJournalButton.Text = "Сохранить в файл";
            this._saveAlarmJournalButton.UseVisualStyleBackColor = true;
            this._saveAlarmJournalButton.Click += new System.EventHandler(this._saveAlarmJournalButton_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 540);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(839, 22);
            this.statusStrip1.TabIndex = 25;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(16, 17);
            this._statusLabel.Text = "...";
            // 
            // _openAlarmJournalDialog
            // 
            this._openAlarmJournalDialog.DefaultExt = "xml";
            this._openAlarmJournalDialog.FileName = "Журнал аварий МР902";
            this._openAlarmJournalDialog.Filter = "ЖА МР902 (.xml)|*.xml|ЖА МР902 (.bin)|*.bin";
            this._openAlarmJournalDialog.RestoreDirectory = true;
            this._openAlarmJournalDialog.Title = "Открыть журнал  аварий для МР902";
            // 
            // _saveAlarmJournalDialog
            // 
            this._saveAlarmJournalDialog.DefaultExt = "xml";
            this._saveAlarmJournalDialog.FileName = "Журнал аварий МР902";
            this._saveAlarmJournalDialog.Filter = "(Журнал аварий МР902) | *.xml";
            this._saveAlarmJournalDialog.Title = "Сохранить  журнал аварий для МР902";
            // 
            // _saveToHTMLBtn
            // 
            this._saveToHTMLBtn.Location = new System.Drawing.Point(459, 514);
            this._saveToHTMLBtn.Name = "_saveToHTMLBtn";
            this._saveToHTMLBtn.Size = new System.Drawing.Size(143, 23);
            this._saveToHTMLBtn.TabIndex = 26;
            this._saveToHTMLBtn.Text = "Сохранить в HTML";
            this._saveToHTMLBtn.UseVisualStyleBackColor = true;
            this._saveToHTMLBtn.Click += new System.EventHandler(this._saveToHTMLBtn_Click);
            // 
            // _saveAlarmHTMLdialog
            // 
            this._saveAlarmHTMLdialog.DefaultExt = "xml";
            this._saveAlarmHTMLdialog.FileName = "Журнал аварий МР902 HTML";
            this._saveAlarmHTMLdialog.Filter = "(Журнал аварий МР902) | *.html";
            this._saveAlarmHTMLdialog.Title = "Сохранить  журнал аварий для МР902";
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "_indexCol";
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 24;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "_timeCol";
            this._timeCol.HeaderText = "Дата/Время";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 77;
            // 
            // _msg1Col
            // 
            this._msg1Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._msg1Col.DataPropertyName = "_msg1Col";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msg1Col.DefaultCellStyle = dataGridViewCellStyle1;
            this._msg1Col.HeaderText = "Сообщение";
            this._msg1Col.Name = "_msg1Col";
            this._msg1Col.ReadOnly = true;
            this._msg1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._msg1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._msgCol.DataPropertyName = "_msgCol";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msgCol.DefaultCellStyle = dataGridViewCellStyle2;
            this._msgCol.HeaderText = "Ступень";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msgCol.Width = 120;
            // 
            // _codeCol
            // 
            this._codeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._codeCol.DataPropertyName = "_codeCol";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._codeCol.DefaultCellStyle = dataGridViewCellStyle3;
            this._codeCol.HeaderText = "Параметр срабатывания";
            this._codeCol.Name = "_codeCol";
            this._codeCol.ReadOnly = true;
            this._codeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._codeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _typeCol
            // 
            this._typeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._typeCol.DataPropertyName = "_typeCol";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._typeCol.DefaultCellStyle = dataGridViewCellStyle4;
            this._typeCol.HeaderText = "Значение параметра срабатывания";
            this._typeCol.Name = "_typeCol";
            this._typeCol.ReadOnly = true;
            this._typeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._typeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _IaCol
            // 
            this._IaCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._IaCol.DataPropertyName = "_IaCol";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IaCol.DefaultCellStyle = dataGridViewCellStyle5;
            this._IaCol.HeaderText = "Группа уставок";
            this._IaCol.Name = "_IaCol";
            this._IaCol.ReadOnly = true;
            this._IaCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _Ida1Col
            // 
            this._Ida1Col.DataPropertyName = "_Ida1Col";
            this._Ida1Col.HeaderText = "Iдa СШ1";
            this._Ida1Col.Name = "_Ida1Col";
            this._Ida1Col.ReadOnly = true;
            this._Ida1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Ida1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ida1Col.Width = 48;
            // 
            // _Idb1Col
            // 
            this._Idb1Col.DataPropertyName = "_Idb1Col";
            this._Idb1Col.HeaderText = "Iдb СШ1";
            this._Idb1Col.Name = "_Idb1Col";
            this._Idb1Col.ReadOnly = true;
            this._Idb1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Idb1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Idb1Col.Width = 48;
            // 
            // _Idc1Col
            // 
            this._Idc1Col.DataPropertyName = "_Idc1Col";
            this._Idc1Col.HeaderText = "Iдc СШ1";
            this._Idc1Col.Name = "_Idc1Col";
            this._Idc1Col.ReadOnly = true;
            this._Idc1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Idc1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Idc1Col.Width = 48;
            // 
            // _Ita1Col
            // 
            this._Ita1Col.DataPropertyName = "_Ita1Col";
            this._Ita1Col.HeaderText = "Iтa СШ1";
            this._Ita1Col.Name = "_Ita1Col";
            this._Ita1Col.ReadOnly = true;
            this._Ita1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Ita1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ita1Col.Width = 47;
            // 
            // _Itb1Col
            // 
            this._Itb1Col.DataPropertyName = "_Itb1Col";
            this._Itb1Col.HeaderText = "Iтb СШ1";
            this._Itb1Col.Name = "_Itb1Col";
            this._Itb1Col.ReadOnly = true;
            this._Itb1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Itb1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Itb1Col.Width = 47;
            // 
            // _Itc1Col
            // 
            this._Itc1Col.DataPropertyName = "_Itc1Col";
            this._Itc1Col.HeaderText = "Iтc СШ1";
            this._Itc1Col.Name = "_Itc1Col";
            this._Itc1Col.ReadOnly = true;
            this._Itc1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Itc1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Itc1Col.Width = 47;
            // 
            // _Ida2Col
            // 
            this._Ida2Col.DataPropertyName = "_Ida2Col";
            this._Ida2Col.HeaderText = "Iдa СШ2";
            this._Ida2Col.Name = "_Ida2Col";
            this._Ida2Col.ReadOnly = true;
            this._Ida2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Ida2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ida2Col.Width = 48;
            // 
            // _Idb2Col
            // 
            this._Idb2Col.DataPropertyName = "_Idb2Col";
            this._Idb2Col.HeaderText = "Iдb СШ2";
            this._Idb2Col.Name = "_Idb2Col";
            this._Idb2Col.ReadOnly = true;
            this._Idb2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Idb2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Idb2Col.Width = 48;
            // 
            // _Idc2Col
            // 
            this._Idc2Col.DataPropertyName = "_Idc2Col";
            this._Idc2Col.HeaderText = "Iдc СШ2";
            this._Idc2Col.Name = "_Idc2Col";
            this._Idc2Col.ReadOnly = true;
            this._Idc2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Idc2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Idc2Col.Width = 48;
            // 
            // _Ita2Col
            // 
            this._Ita2Col.DataPropertyName = "_Ita2Col";
            this._Ita2Col.HeaderText = "Iтa СШ2";
            this._Ita2Col.Name = "_Ita2Col";
            this._Ita2Col.ReadOnly = true;
            this._Ita2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Ita2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ita2Col.Width = 47;
            // 
            // _Itb2Col
            // 
            this._Itb2Col.DataPropertyName = "_Itb2Col";
            this._Itb2Col.HeaderText = "Iтb СШ2";
            this._Itb2Col.Name = "_Itb2Col";
            this._Itb2Col.ReadOnly = true;
            this._Itb2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Itb2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Itb2Col.Width = 47;
            // 
            // _Itc2Col
            // 
            this._Itc2Col.DataPropertyName = "_Itc2Col";
            this._Itc2Col.HeaderText = "Iтc СШ2";
            this._Itc2Col.Name = "_Itc2Col";
            this._Itc2Col.ReadOnly = true;
            this._Itc2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Itc2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Itc2Col.Width = 47;
            // 
            // _Ida3Col
            // 
            this._Ida3Col.DataPropertyName = "_Ida3Col";
            this._Ida3Col.HeaderText = "Iдa ПО";
            this._Ida3Col.Name = "_Ida3Col";
            this._Ida3Col.ReadOnly = true;
            this._Ida3Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Ida3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ida3Col.Width = 42;
            // 
            // _Idb3Col
            // 
            this._Idb3Col.DataPropertyName = "_Idb3Col";
            this._Idb3Col.HeaderText = "Iдb ПО";
            this._Idb3Col.Name = "_Idb3Col";
            this._Idb3Col.ReadOnly = true;
            this._Idb3Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Idb3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Idb3Col.Width = 42;
            // 
            // _Idc3Col
            // 
            this._Idc3Col.DataPropertyName = "_Idc3Col";
            this._Idc3Col.HeaderText = "Iдc ПО";
            this._Idc3Col.Name = "_Idc3Col";
            this._Idc3Col.ReadOnly = true;
            this._Idc3Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Idc3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Idc3Col.Width = 42;
            // 
            // _Ita3Col
            // 
            this._Ita3Col.DataPropertyName = "_Ita3Col";
            this._Ita3Col.HeaderText = "Iтa ПО";
            this._Ita3Col.Name = "_Ita3Col";
            this._Ita3Col.ReadOnly = true;
            this._Ita3Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Ita3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ita3Col.Width = 41;
            // 
            // _Itb3Col
            // 
            this._Itb3Col.DataPropertyName = "_Itb3Col";
            this._Itb3Col.HeaderText = "Iтb ПО";
            this._Itb3Col.Name = "_Itb3Col";
            this._Itb3Col.ReadOnly = true;
            this._Itb3Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Itb3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Itb3Col.Width = 41;
            // 
            // _Itc3Col
            // 
            this._Itc3Col.DataPropertyName = "_Itc3Col";
            this._Itc3Col.HeaderText = "Iтc ПО";
            this._Itc3Col.Name = "_Itc3Col";
            this._Itc3Col.ReadOnly = true;
            this._Itc3Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Itc3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Itc3Col.Width = 41;
            // 
            // _I1Col
            // 
            this._I1Col.DataPropertyName = "_I1Col";
            this._I1Col.HeaderText = "Ia Прис.1";
            this._I1Col.Name = "_I1Col";
            this._I1Col.ReadOnly = true;
            this._I1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._I1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I1Col.Width = 54;
            // 
            // _I2Col
            // 
            this._I2Col.DataPropertyName = "_I2Col";
            this._I2Col.HeaderText = "Ib Прис.1";
            this._I2Col.Name = "_I2Col";
            this._I2Col.ReadOnly = true;
            this._I2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._I2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I2Col.Width = 54;
            // 
            // _I3Col
            // 
            this._I3Col.DataPropertyName = "_I3Col";
            this._I3Col.HeaderText = "Ic Прис.1";
            this._I3Col.Name = "_I3Col";
            this._I3Col.ReadOnly = true;
            this._I3Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._I3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I3Col.Width = 54;
            // 
            // _I4Col
            // 
            this._I4Col.DataPropertyName = "_I4Col";
            this._I4Col.HeaderText = "Ia Прис.2";
            this._I4Col.Name = "_I4Col";
            this._I4Col.ReadOnly = true;
            this._I4Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._I4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I4Col.Width = 54;
            // 
            // _I5Col
            // 
            this._I5Col.DataPropertyName = "_I5Col";
            this._I5Col.HeaderText = "Ib Прис.2";
            this._I5Col.Name = "_I5Col";
            this._I5Col.ReadOnly = true;
            this._I5Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._I5Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I5Col.Width = 54;
            // 
            // _I6Col
            // 
            this._I6Col.DataPropertyName = "_I6Col";
            this._I6Col.HeaderText = "Ic Прис.2";
            this._I6Col.Name = "_I6Col";
            this._I6Col.ReadOnly = true;
            this._I6Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._I6Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I6Col.Width = 54;
            // 
            // _I7Col
            // 
            this._I7Col.DataPropertyName = "_I7Col";
            this._I7Col.HeaderText = "Ia Прис.3";
            this._I7Col.Name = "_I7Col";
            this._I7Col.ReadOnly = true;
            this._I7Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._I7Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I7Col.Width = 54;
            // 
            // _I8Col
            // 
            this._I8Col.DataPropertyName = "_I8Col";
            this._I8Col.HeaderText = "Ib Прис.3";
            this._I8Col.Name = "_I8Col";
            this._I8Col.ReadOnly = true;
            this._I8Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._I8Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I8Col.Width = 54;
            // 
            // _I9Col
            // 
            this._I9Col.DataPropertyName = "_I9Col";
            this._I9Col.HeaderText = "Ic Прис.3";
            this._I9Col.Name = "_I9Col";
            this._I9Col.ReadOnly = true;
            this._I9Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._I9Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I9Col.Width = 54;
            // 
            // _I10Col
            // 
            this._I10Col.DataPropertyName = "_I10Col";
            this._I10Col.HeaderText = "Ia Прис.4";
            this._I10Col.Name = "_I10Col";
            this._I10Col.ReadOnly = true;
            this._I10Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._I10Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I10Col.Width = 54;
            // 
            // _I11Col
            // 
            this._I11Col.DataPropertyName = "_I11Col";
            this._I11Col.HeaderText = "Ib Прис.4";
            this._I11Col.Name = "_I11Col";
            this._I11Col.ReadOnly = true;
            this._I11Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._I11Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I11Col.Width = 54;
            // 
            // _I12Col
            // 
            this._I12Col.DataPropertyName = "_I12Col";
            this._I12Col.HeaderText = "Ic Прис.4";
            this._I12Col.Name = "_I12Col";
            this._I12Col.ReadOnly = true;
            this._I12Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._I12Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I12Col.Width = 54;
            // 
            // _I13Col
            // 
            this._I13Col.DataPropertyName = "_I13Col";
            this._I13Col.HeaderText = "Ia Прис.5";
            this._I13Col.Name = "_I13Col";
            this._I13Col.ReadOnly = true;
            this._I13Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._I13Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I13Col.Width = 54;
            // 
            // _I14Col
            // 
            this._I14Col.DataPropertyName = "_I14Col";
            this._I14Col.HeaderText = "Ib Прис.5";
            this._I14Col.Name = "_I14Col";
            this._I14Col.ReadOnly = true;
            this._I14Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._I14Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I14Col.Width = 54;
            // 
            // _I15Col
            // 
            this._I15Col.DataPropertyName = "_I15Col";
            this._I15Col.HeaderText = "Ic Прис.5";
            this._I15Col.Name = "_I15Col";
            this._I15Col.ReadOnly = true;
            this._I15Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._I15Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I15Col.Width = 54;
            // 
            // _I16Col
            // 
            this._I16Col.DataPropertyName = "_I16Col";
            this._I16Col.HeaderText = "In";
            this._I16Col.Name = "_I16Col";
            this._I16Col.ReadOnly = true;
            this._I16Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._I16Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I16Col.Width = 22;
            // 
            // _D0Col
            // 
            this._D0Col.DataPropertyName = "_D0Col";
            this._D0Col.HeaderText = "D[1-8]";
            this._D0Col.Name = "_D0Col";
            this._D0Col.ReadOnly = true;
            this._D0Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._D0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D0Col.Width = 42;
            // 
            // _D1Col
            // 
            this._D1Col.DataPropertyName = "_D1Col";
            this._D1Col.HeaderText = "D [9-16]";
            this._D1Col.Name = "_D1Col";
            this._D1Col.ReadOnly = true;
            this._D1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._D1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D1Col.Width = 46;
            // 
            // _D2Col
            // 
            this._D2Col.DataPropertyName = "_D2Col";
            this._D2Col.HeaderText = "D [17-24]";
            this._D2Col.Name = "_D2Col";
            this._D2Col.ReadOnly = true;
            this._D2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._D2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D2Col.Width = 51;
            // 
            // Mr902AlarmJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 562);
            this.Controls.Add(this._saveToHTMLBtn);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._loadAlarmJournalButton);
            this.Controls.Add(this._readAlarmJournalButton);
            this.Controls.Add(this._saveAlarmJournalButton);
            this.Controls.Add(this._alarmJournalGrid);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(855, 600);
            this.Name = "Mr902AlarmJournalForm";
            this.Text = "Mr902AlarmJournalForm";
            this.Load += new System.EventHandler(this.Mr902AlarmJournalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView _alarmJournalGrid;
        private System.Windows.Forms.Button _loadAlarmJournalButton;
        private System.Windows.Forms.Button _readAlarmJournalButton;
        private System.Windows.Forms.Button _saveAlarmJournalButton;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.OpenFileDialog _openAlarmJournalDialog;
        private System.Windows.Forms.SaveFileDialog _saveAlarmJournalDialog;
        private System.Windows.Forms.Button _saveToHTMLBtn;
        private System.Windows.Forms.SaveFileDialog _saveAlarmHTMLdialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msg1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _codeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _typeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ida1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Idb1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Idc1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ita1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Itb1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Itc1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ida2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Idb2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Idc2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ita2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Itb2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Itc2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ida3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Idb3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Idc3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ita3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Itb3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Itc3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I4Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I5Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I6Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I7Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I8Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I9Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I10Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I11Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I12Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I13Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I14Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I15Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I16Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D2Col;
    }
}