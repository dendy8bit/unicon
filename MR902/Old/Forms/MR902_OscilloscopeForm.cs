using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.Interfaces;
using BEMN.MR902.Old.Configuration.Structures.Primary;
using BEMN.MR902.Old.HelpClasses;
using BEMN.MR902.Old.HelpClasses.Oscilloscope;
using BEMN.MR902.Old.Structures.JournalStructures;
using BEMN.MR902.Properties;

namespace BEMN.MR902.Old.Forms
{
    public partial class OscilloscopeForm : Form, IFormView
    {
        #region [Constants]
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string RECORDS_IN_JOURNAL = "������������ � ������� - {0}";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string READ_OSC_STOPPED = "������ ������������� ����������";
        private const string JOURNAL_IS_EMPTY = "������ ������������ ����";

        #endregion [Constants]

        #region [Private fields]
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscPageLoader _pageLoader;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscJournalLoader _oscJournalLoader;
        /// <summary>
        /// ��������� ������� �����
        /// </summary>
        private readonly CurrentOptionsLoader _currentOptionsLoader;

        private readonly MemoryEntity<OscilloscopeSettingsStruct> _oscilloscopeSettings;

        /// <summary>
        /// ������ ���
        /// </summary>
        private CountingList _countingList;

        private OscJournalStruct _journalStruct;
        private Mr902 _device;
        /// <summary>
        /// ��� ������
        /// </summary>
        private readonly DataTable _table; 
        #endregion [Private fields]
        
        #region [Ctor's]
        public OscilloscopeForm()
        {
            this.InitializeComponent();
        }

        public OscilloscopeForm(Mr902 device)
        {
            this.InitializeComponent();
            this._table = this.GetJournalDataTable();
            this._device = device;
            //��������� �������
            this._pageLoader = new OscPageLoader(device.Keeper.SetOscStartPage, device.Keeper.OscPage);
            this._pageLoader.PageRead += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadStopped += HandlerHelper.CreateActionHandler(this, this.ReadStop);
          
            //��������� �������
            this._oscJournalLoader = new OscJournalLoader(device.Keeper.OscJournal, device.Keeper.RefreshOscJournal, device.Keeper.OscOptions);
            this._oscJournalLoader.ReadRecordOk += HandlerHelper.CreateActionHandler(this, this.ReadRecord);
            this._oscJournalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.OnAllJournalReadOk);

            //������� ������� �����������
            this._oscilloscopeSettings = device.Keeper.OscilloscopeSettings;
            this._oscilloscopeSettings.AllReadOk += a => this._oscJournalLoader.StartReadJournal();
            this._oscilloscopeSettings.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);
            //��������� ������� �����
            this._currentOptionsLoader = device.Keeper.CurrentOptionsLoader;
            this._currentOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, this._oscilloscopeSettings.LoadStruct);
            this._currentOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
        }
        #endregion [Ctor's]

        /// <summary>
        /// �������� ���� ������
        /// </summary>
        private void OnAllJournalReadOk()
        {
            if (this._oscJournalLoader.OscRecords.Count == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
            }
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
        }


        private void ReadStop()
        {
            this._statusLabel.Text = READ_OSC_STOPPED;
            this._stopReadOsc.Enabled = false;
            this._oscProgressBar.Value = 0;
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
        }
       
        #region [Help Classes Events Handlers]
        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
        }


        /// <summary>
        /// ��������� ���� ������ �������
        /// </summary>
        private void ReadRecord()
        {
            this._oscJournalDataGrid.Refresh();
            var number = this._oscJournalLoader.RecordNumber;
            this._oscilloscopeCountCb.Items.Add(number);
            this._table.Rows.Add(this._oscJournalLoader.Record);
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, number);
            this._oscLoadButton.Enabled = true;
            this._oscJournalReadButton.Enabled = true;
        }

        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            this.CountingList = this._pageLoader.CountingList;
            this._stopReadOsc.Enabled = false;
            this._oscSaveButton.Enabled = true;
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
            this._oscReadButton.Enabled = true;
        }

        #endregion [Help Classes Events Handlers]


        #region [Properties]
        /// <summary>
        /// ���������� ����������� ������� ������������ ��� ������
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// ������ ���
        /// </summary>
        public CountingList CountingList
        {
            get { return this._countingList; }
            set
            {
                this._countingList = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]


        #region [Help members]
        private DataTable GetJournalDataTable()
        {
            var table = new DataTable("��901_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }
        #endregion [Help members]


        #region [Event Handlers]
        /// <summary>
        /// �������� �����
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            this.StartRead();
        }
        /// <summary>
        /// �������� �������������
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (this.CountingList == null)
            {
                this.CountingList = new CountingList(new ushort[12000], new OscJournalStruct());
            }
            if (Validator.GetVersionFromRegistry())
            {
                string fileName;
                if (this._countingList.IsLoad)
                {
                    fileName = this._countingList.FilePath;
                }
                else
                {
                    fileName = Validator.CreateOscFileNameCfg($"��902 v{this._device.DeviceVersion} �������������");
                    this._countingList.Save(fileName, this._oscilloscopeSettings.Value);
                }
                System.Diagnostics.Process.Start(
                    Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"),
                    fileName);
            }
            else
            {
                var resForm = new Mr902OscilloscopeResultForm(this.CountingList, this._oscilloscopeSettings.Value);
                resForm.Show();
            }
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._table.Clear();
            this._oscilloscopeCountCb.Items.Clear();
            this._oscJournalLoader.Reset();
            this._oscJournalDataGrid.Refresh();
            this.CanSelectOsc = false;
            this._currentOptionsLoader.StartRead();
            this._oscJournalReadButton.Enabled = false;
            this._oscReadButton.Enabled = false;
            this._oscLoadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
        }
      

        /// <summary>
        /// ��������� �������������
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStruct = this._oscJournalLoader.OscRecords[selectedOsc];
            this._pageLoader.StartRead(this._journalStruct, this._oscJournalLoader.OscSizeOptions);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoader.PagesCount;
            //�������� ����������� ���������� ������ ������������
            this._stopReadOsc.Enabled = true;
            this._oscJournalReadButton.Enabled = false;
            this._oscReadButton.Enabled = false;
            this._oscLoadButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
        }

        /// <summary>
        /// ��������� ������������� � ����
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        { 
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
               // this._pageLoader.CountingList.SaveArray(this._saveOscilloscopeDlg.FileName);
                  this._pageLoader.CountingList.Save(this._saveOscilloscopeDlg.FileName,this._oscilloscopeSettings.Value);
            }
         
        }

        /// <summary>
        /// ��������� ������������� �� �����
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;

            try
            {
                var settingsStruct = new OscilloscopeSettingsStruct();
                this.CountingList = CountingList.Load(this._openOscilloscopeDlg.FileName, out settingsStruct);
                this._oscilloscopeSettings.Value = settingsStruct;
                this._statusLabel.Text = string.Format("������������ ��������� �� ����� {0}",
                                                       this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
                this._stopReadOsc.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = "���������� ��������� ������������";
            }

        }
        
        /// <summary>
        /// ���������� ������ ������������
        /// </summary>
        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._pageLoader.StopRead();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {   
            this._oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscBeginColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this.checkBox1.Checked;
        }

        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }
        #endregion [Event Handlers]


        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Mr902); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(OscilloscopeForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "�������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}