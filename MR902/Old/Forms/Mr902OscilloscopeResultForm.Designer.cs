﻿namespace BEMN.MR902.Old.Forms
{
    partial class Mr902OscilloscopeResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MAINTABLE = new System.Windows.Forms.TableLayoutPanel();
            this.hScrollBar4 = new System.Windows.Forms.HScrollBar();
            this.MAINPANEL = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._channelsChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._channel8Button = new System.Windows.Forms.Button();
            this._channel7Button = new System.Windows.Forms.Button();
            this._channel6Button = new System.Windows.Forms.Button();
            this._channel5Button = new System.Windows.Forms.Button();
            this._channel4Button = new System.Windows.Forms.Button();
            this._channel3Button = new System.Windows.Forms.Button();
            this._channel2Button = new System.Windows.Forms.Button();
            this._channel1Button = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this._discrestsChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this._discrete5Button = new System.Windows.Forms.Button();
            this._discrete24Button = new System.Windows.Forms.Button();
            this._discrete23Button = new System.Windows.Forms.Button();
            this._discrete22Button = new System.Windows.Forms.Button();
            this._discrete21Button = new System.Windows.Forms.Button();
            this._discrete20Button = new System.Windows.Forms.Button();
            this._discrete19Button = new System.Windows.Forms.Button();
            this._discrete18Button = new System.Windows.Forms.Button();
            this._discrete17Button = new System.Windows.Forms.Button();
            this._discrete16Button = new System.Windows.Forms.Button();
            this._discrete15Button = new System.Windows.Forms.Button();
            this._discrete14Button = new System.Windows.Forms.Button();
            this._discrete13Button = new System.Windows.Forms.Button();
            this._discrete12Button = new System.Windows.Forms.Button();
            this._discrete11Button = new System.Windows.Forms.Button();
            this._discrete10Button = new System.Windows.Forms.Button();
            this._discrete9Button = new System.Windows.Forms.Button();
            this._discrete8Button = new System.Windows.Forms.Button();
            this._discrete7Button = new System.Windows.Forms.Button();
            this._discrete6Button = new System.Windows.Forms.Button();
            this._discrete2Button = new System.Windows.Forms.Button();
            this._discrete3Button = new System.Windows.Forms.Button();
            this._discrete4Button = new System.Windows.Forms.Button();
            this._discrete1Button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this._s1Scroll = new System.Windows.Forms.VScrollBar();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this._i1Button = new System.Windows.Forms.Button();
            this._i3Button = new System.Windows.Forms.Button();
            this._i2Button = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this._i4Button = new System.Windows.Forms.Button();
            this._i5Button = new System.Windows.Forms.Button();
            this._i6Button = new System.Windows.Forms.Button();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this._i7Button = new System.Windows.Forms.Button();
            this._i8Button = new System.Windows.Forms.Button();
            this._i9Button = new System.Windows.Forms.Button();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this._i10Button = new System.Windows.Forms.Button();
            this._i11Button = new System.Windows.Forms.Button();
            this._i12Button = new System.Windows.Forms.Button();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this._i13Button = new System.Windows.Forms.Button();
            this._i14Button = new System.Windows.Forms.Button();
            this._i15Button = new System.Windows.Forms.Button();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this._i16Button = new System.Windows.Forms.Button();
            this._currentСonnectionsChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this._currentСonnectionsChartDecreaseButton = new System.Windows.Forms.Button();
            this._currentСonnectionsChartIncreaseButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.MarkersTable = new System.Windows.Forms.TableLayoutPanel();
            this._marker1TrackBar = new System.Windows.Forms.TrackBar();
            this._marker2TrackBar = new System.Windows.Forms.TrackBar();
            this._label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._markerScrollPanel = new System.Windows.Forms.Panel();
            this._marker2Box = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._marker2K8 = new System.Windows.Forms.Label();
            this._marker2K7 = new System.Windows.Forms.Label();
            this._marker2K6 = new System.Windows.Forms.Label();
            this._marker2K5 = new System.Windows.Forms.Label();
            this._marker2K4 = new System.Windows.Forms.Label();
            this._marker2K3 = new System.Windows.Forms.Label();
            this._marker2K2 = new System.Windows.Forms.Label();
            this._marker2K1 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._marker2D24 = new System.Windows.Forms.Label();
            this._marker2D23 = new System.Windows.Forms.Label();
            this._marker2D22 = new System.Windows.Forms.Label();
            this._marker2D21 = new System.Windows.Forms.Label();
            this._marker2D20 = new System.Windows.Forms.Label();
            this._marker2D11 = new System.Windows.Forms.Label();
            this._marker2D10 = new System.Windows.Forms.Label();
            this._marker2D9 = new System.Windows.Forms.Label();
            this._marker2D19 = new System.Windows.Forms.Label();
            this._marker2D18 = new System.Windows.Forms.Label();
            this._marker2D17 = new System.Windows.Forms.Label();
            this._marker2D16 = new System.Windows.Forms.Label();
            this._marker2D15 = new System.Windows.Forms.Label();
            this._marker2D14 = new System.Windows.Forms.Label();
            this._marker2D13 = new System.Windows.Forms.Label();
            this._marker2D12 = new System.Windows.Forms.Label();
            this._marker2D8 = new System.Windows.Forms.Label();
            this._marker2D7 = new System.Windows.Forms.Label();
            this._marker2D6 = new System.Windows.Forms.Label();
            this._marker2D5 = new System.Windows.Forms.Label();
            this._marker2D4 = new System.Windows.Forms.Label();
            this._marker2D3 = new System.Windows.Forms.Label();
            this._marker2D2 = new System.Windows.Forms.Label();
            this._marker2D1 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._marker2I16 = new System.Windows.Forms.Label();
            this._marker2I15 = new System.Windows.Forms.Label();
            this._marker2I14 = new System.Windows.Forms.Label();
            this._marker2I13 = new System.Windows.Forms.Label();
            this._marker2I12 = new System.Windows.Forms.Label();
            this._marker2I11 = new System.Windows.Forms.Label();
            this._marker2I10 = new System.Windows.Forms.Label();
            this._marker2I9 = new System.Windows.Forms.Label();
            this._marker2I8 = new System.Windows.Forms.Label();
            this._marker2I7 = new System.Windows.Forms.Label();
            this._marker2I6 = new System.Windows.Forms.Label();
            this._marker2I5 = new System.Windows.Forms.Label();
            this._marker2I4 = new System.Windows.Forms.Label();
            this._marker2I3 = new System.Windows.Forms.Label();
            this._marker2I2 = new System.Windows.Forms.Label();
            this._marker2I1 = new System.Windows.Forms.Label();
            this._marker1Box = new System.Windows.Forms.GroupBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this._marker1K8 = new System.Windows.Forms.Label();
            this._marker1K7 = new System.Windows.Forms.Label();
            this._marker1K6 = new System.Windows.Forms.Label();
            this._marker1K5 = new System.Windows.Forms.Label();
            this._marker1K4 = new System.Windows.Forms.Label();
            this._marker1K3 = new System.Windows.Forms.Label();
            this._marker1K2 = new System.Windows.Forms.Label();
            this._marker1K1 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._marker1D24 = new System.Windows.Forms.Label();
            this._marker1D23 = new System.Windows.Forms.Label();
            this._marker1D22 = new System.Windows.Forms.Label();
            this._marker1D21 = new System.Windows.Forms.Label();
            this._marker1D20 = new System.Windows.Forms.Label();
            this._marker1D11 = new System.Windows.Forms.Label();
            this._marker1D10 = new System.Windows.Forms.Label();
            this._marker1D9 = new System.Windows.Forms.Label();
            this._marker1D19 = new System.Windows.Forms.Label();
            this._marker1D18 = new System.Windows.Forms.Label();
            this._marker1D17 = new System.Windows.Forms.Label();
            this._marker1D16 = new System.Windows.Forms.Label();
            this._marker1D15 = new System.Windows.Forms.Label();
            this._marker1D14 = new System.Windows.Forms.Label();
            this._marker1D13 = new System.Windows.Forms.Label();
            this._marker1D12 = new System.Windows.Forms.Label();
            this._marker1D8 = new System.Windows.Forms.Label();
            this._marker1D7 = new System.Windows.Forms.Label();
            this._marker1D6 = new System.Windows.Forms.Label();
            this._marker1D5 = new System.Windows.Forms.Label();
            this._marker1D4 = new System.Windows.Forms.Label();
            this._marker1D3 = new System.Windows.Forms.Label();
            this._marker1D2 = new System.Windows.Forms.Label();
            this._marker1D1 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._marker1I16 = new System.Windows.Forms.Label();
            this._marker1I15 = new System.Windows.Forms.Label();
            this._marker1I14 = new System.Windows.Forms.Label();
            this._marker1I13 = new System.Windows.Forms.Label();
            this._marker1I12 = new System.Windows.Forms.Label();
            this._marker1I11 = new System.Windows.Forms.Label();
            this._marker1I10 = new System.Windows.Forms.Label();
            this._marker1I9 = new System.Windows.Forms.Label();
            this._marker1I8 = new System.Windows.Forms.Label();
            this._marker1I7 = new System.Windows.Forms.Label();
            this._marker1I6 = new System.Windows.Forms.Label();
            this._marker1I5 = new System.Windows.Forms.Label();
            this._marker1I4 = new System.Windows.Forms.Label();
            this._marker1I3 = new System.Windows.Forms.Label();
            this._marker1I2 = new System.Windows.Forms.Label();
            this._marker1I1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._deltaTimeBox = new System.Windows.Forms.GroupBox();
            this._markerTimeDelta = new System.Windows.Forms.Label();
            this._marker2TimeBox = new System.Windows.Forms.GroupBox();
            this._marker2Time = new System.Windows.Forms.Label();
            this._marker1TimeBox = new System.Windows.Forms.GroupBox();
            this._marker1Time = new System.Windows.Forms.Label();
            this._xDecreaseButton = new System.Windows.Forms.Button();
            this._currentСonnectionsСheckBox = new System.Windows.Forms.CheckBox();
            this._discrestsСheckBox = new System.Windows.Forms.CheckBox();
            this._channelsСheckBox = new System.Windows.Forms.CheckBox();
            this._markCheckBox = new System.Windows.Forms.CheckBox();
            this._markerCheckBox = new System.Windows.Forms.CheckBox();
            this._newMarkToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._xIncreaseButton = new System.Windows.Forms.Button();
            this._oscRunСheckBox = new System.Windows.Forms.CheckBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.MAINTABLE.SuspendLayout();
            this.MAINPANEL.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.MarkersTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._marker1TrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._marker2TrackBar)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._markerScrollPanel.SuspendLayout();
            this._marker2Box.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this._marker1Box.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this._deltaTimeBox.SuspendLayout();
            this._marker2TimeBox.SuspendLayout();
            this._marker1TimeBox.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // MAINTABLE
            // 
            this.MAINTABLE.ColumnCount = 2;
            this.MAINTABLE.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MAINTABLE.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.MAINTABLE.Controls.Add(this.hScrollBar4, 0, 2);
            this.MAINTABLE.Controls.Add(this.MAINPANEL, 0, 1);
            this.MAINTABLE.Controls.Add(this.panel3, 1, 1);
            this.MAINTABLE.Controls.Add(this.panel4, 0, 0);
            this.MAINTABLE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MAINTABLE.Location = new System.Drawing.Point(0, 0);
            this.MAINTABLE.Margin = new System.Windows.Forms.Padding(0);
            this.MAINTABLE.Name = "MAINTABLE";
            this.MAINTABLE.RowCount = 2;
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MAINTABLE.Size = new System.Drawing.Size(1384, 753);
            this.MAINTABLE.TabIndex = 1;
            // 
            // hScrollBar4
            // 
            this.hScrollBar4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.hScrollBar4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hScrollBar4.LargeChange = 1;
            this.hScrollBar4.Location = new System.Drawing.Point(0, 733);
            this.hScrollBar4.Maximum = 10;
            this.hScrollBar4.Minimum = 10;
            this.hScrollBar4.Name = "hScrollBar4";
            this.hScrollBar4.Size = new System.Drawing.Size(1084, 20);
            this.hScrollBar4.TabIndex = 20;
            this.hScrollBar4.Value = 10;
            this.hScrollBar4.Visible = false;
            // 
            // MAINPANEL
            // 
            this.MAINPANEL.Controls.Add(this.panel1);
            this.MAINPANEL.Controls.Add(this.MarkersTable);
            this.MAINPANEL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MAINPANEL.Location = new System.Drawing.Point(3, 28);
            this.MAINPANEL.Name = "MAINPANEL";
            this.MAINPANEL.Size = new System.Drawing.Size(1078, 702);
            this.MAINPANEL.TabIndex = 32;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 65);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1078, 637);
            this.panel1.TabIndex = 32;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoSize = true;
            this.panel2.Controls.Add(this.splitter5);
            this.panel2.Controls.Add(this.tableLayoutPanel2);
            this.panel2.Controls.Add(this.splitter2);
            this.panel2.Controls.Add(this.tableLayoutPanel14);
            this.panel2.Controls.Add(this.splitter1);
            this.panel2.Controls.Add(this.tableLayoutPanel13);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1057, 1321);
            this.panel2.TabIndex = 0;
            // 
            // splitter5
            // 
            this.splitter5.BackColor = System.Drawing.Color.Black;
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter5.Location = new System.Drawing.Point(0, 1318);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(1057, 3);
            this.splitter5.TabIndex = 36;
            this.splitter5.TabStop = false;
            this.splitter5.Visible = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this._channelsChart, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label3, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 1109);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1057, 209);
            this.tableLayoutPanel2.TabIndex = 35;
            // 
            // _channelsChart
            // 
            this._channelsChart.BkGradient = false;
            this._channelsChart.BkGradientAngle = 90;
            this._channelsChart.BkGradientColor = System.Drawing.Color.White;
            this._channelsChart.BkGradientRate = 0.5F;
            this._channelsChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._channelsChart.BkRestrictedToChartPanel = false;
            this._channelsChart.BkShinePosition = 1F;
            this._channelsChart.BkTransparency = 0F;
            this._channelsChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._channelsChart.BorderExteriorLength = 0;
            this._channelsChart.BorderGradientAngle = 225;
            this._channelsChart.BorderGradientLightPos1 = 1F;
            this._channelsChart.BorderGradientLightPos2 = -1F;
            this._channelsChart.BorderGradientRate = 0.5F;
            this._channelsChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._channelsChart.BorderLightIntermediateBrightness = 0F;
            this._channelsChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._channelsChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._channelsChart.ChartPanelBkTransparency = 0F;
            this._channelsChart.ControlShadow = false;
            this._channelsChart.CoordinateAxesVisible = true;
            this._channelsChart.CoordinateAxisColor = System.Drawing.Color.Transparent;
            this._channelsChart.CoordinateXOrigin = 10D;
            this._channelsChart.CoordinateYMax = 90D;
            this._channelsChart.CoordinateYMin = 0D;
            this._channelsChart.CoordinateYOrigin = 0D;
            this._channelsChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._channelsChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._channelsChart.FooterColor = System.Drawing.Color.Black;
            this._channelsChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._channelsChart.FooterVisible = false;
            this._channelsChart.GridColor = System.Drawing.Color.MistyRose;
            this._channelsChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._channelsChart.GridVisible = true;
            this._channelsChart.GridXSubTicker = 0;
            this._channelsChart.GridXTicker = 10;
            this._channelsChart.GridYSubTicker = 0;
            this._channelsChart.GridYTicker = 2;
            this._channelsChart.HeaderColor = System.Drawing.Color.Black;
            this._channelsChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._channelsChart.HeaderVisible = false;
            this._channelsChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._channelsChart.InnerBorderLength = 0;
            this._channelsChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._channelsChart.LegendBkColor = System.Drawing.Color.White;
            this._channelsChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._channelsChart.LegendVisible = false;
            this._channelsChart.Location = new System.Drawing.Point(45, 26);
            this._channelsChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._channelsChart.MiddleBorderLength = 0;
            this._channelsChart.Name = "_channelsChart";
            this._channelsChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._channelsChart.OuterBorderLength = 0;
            this._channelsChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._channelsChart.Precision = 0;
            this._channelsChart.RoundRadius = 10;
            this._channelsChart.ShadowColor = System.Drawing.Color.DimGray;
            this._channelsChart.ShadowDepth = 8;
            this._channelsChart.ShadowRate = 0.5F;
            this._channelsChart.Size = new System.Drawing.Size(989, 180);
            this._channelsChart.TabIndex = 11;
            this._channelsChart.Text = "daS_Net_XYChart5";
            this._channelsChart.XMax = 100D;
            this._channelsChart.XMin = 0D;
            this._channelsChart.XScaleColor = System.Drawing.Color.Black;
            this._channelsChart.XScaleVisible = true;
            this._channelsChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Chart_MouseClick);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this._channel8Button, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this._channel7Button, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this._channel6Button, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this._channel5Button, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this._channel4Button, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this._channel3Button, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this._channel2Button, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this._channel1Button, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 26);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(36, 180);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // _channel8Button
            // 
            this._channel8Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel8Button.AutoSize = true;
            this._channel8Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel8Button.ForeColor = System.Drawing.Color.Black;
            this._channel8Button.Location = new System.Drawing.Point(0, 156);
            this._channel8Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel8Button.Name = "_channel8Button";
            this._channel8Button.Size = new System.Drawing.Size(33, 22);
            this._channel8Button.TabIndex = 1;
            this._channel8Button.Text = "К8";
            this._channel8Button.UseVisualStyleBackColor = false;
            this._channel8Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel7Button
            // 
            this._channel7Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel7Button.AutoSize = true;
            this._channel7Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel7Button.ForeColor = System.Drawing.Color.Black;
            this._channel7Button.Location = new System.Drawing.Point(0, 134);
            this._channel7Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel7Button.Name = "_channel7Button";
            this._channel7Button.Size = new System.Drawing.Size(33, 22);
            this._channel7Button.TabIndex = 2;
            this._channel7Button.Text = "К7";
            this._channel7Button.UseVisualStyleBackColor = false;
            this._channel7Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel6Button
            // 
            this._channel6Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel6Button.AutoSize = true;
            this._channel6Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel6Button.ForeColor = System.Drawing.Color.Black;
            this._channel6Button.Location = new System.Drawing.Point(0, 112);
            this._channel6Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel6Button.Name = "_channel6Button";
            this._channel6Button.Size = new System.Drawing.Size(33, 22);
            this._channel6Button.TabIndex = 6;
            this._channel6Button.Text = "К6";
            this._channel6Button.UseVisualStyleBackColor = false;
            this._channel6Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel5Button
            // 
            this._channel5Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel5Button.AutoSize = true;
            this._channel5Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel5Button.ForeColor = System.Drawing.Color.Black;
            this._channel5Button.Location = new System.Drawing.Point(0, 90);
            this._channel5Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel5Button.Name = "_channel5Button";
            this._channel5Button.Size = new System.Drawing.Size(33, 22);
            this._channel5Button.TabIndex = 10;
            this._channel5Button.Text = "К5";
            this._channel5Button.UseVisualStyleBackColor = false;
            this._channel5Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel4Button
            // 
            this._channel4Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel4Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel4Button.ForeColor = System.Drawing.Color.Black;
            this._channel4Button.Location = new System.Drawing.Point(0, 69);
            this._channel4Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel4Button.Name = "_channel4Button";
            this._channel4Button.Size = new System.Drawing.Size(33, 21);
            this._channel4Button.TabIndex = 11;
            this._channel4Button.Text = "К4";
            this._channel4Button.UseVisualStyleBackColor = false;
            this._channel4Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel3Button
            // 
            this._channel3Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel3Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel3Button.ForeColor = System.Drawing.Color.Black;
            this._channel3Button.Location = new System.Drawing.Point(0, 47);
            this._channel3Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel3Button.Name = "_channel3Button";
            this._channel3Button.Size = new System.Drawing.Size(33, 21);
            this._channel3Button.TabIndex = 9;
            this._channel3Button.Text = "К3";
            this._channel3Button.UseVisualStyleBackColor = false;
            this._channel3Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel2Button
            // 
            this._channel2Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel2Button.AutoSize = true;
            this._channel2Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel2Button.ForeColor = System.Drawing.Color.Black;
            this._channel2Button.Location = new System.Drawing.Point(0, 24);
            this._channel2Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel2Button.Name = "_channel2Button";
            this._channel2Button.Size = new System.Drawing.Size(33, 22);
            this._channel2Button.TabIndex = 7;
            this._channel2Button.Text = "К2";
            this._channel2Button.UseVisualStyleBackColor = false;
            this._channel2Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel1Button
            // 
            this._channel1Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel1Button.AutoSize = true;
            this._channel1Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel1Button.ForeColor = System.Drawing.Color.Black;
            this._channel1Button.Location = new System.Drawing.Point(0, 1);
            this._channel1Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel1Button.Name = "_channel1Button";
            this._channel1Button.Size = new System.Drawing.Size(33, 23);
            this._channel1Button.TabIndex = 8;
            this._channel1Button.Text = "К1";
            this._channel1Button.UseVisualStyleBackColor = false;
            this._channel1Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 5);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Каналы  (K1 - K8)";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.Color.Black;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Enabled = false;
            this.splitter2.Location = new System.Drawing.Point(0, 1106);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(1057, 3);
            this.splitter2.TabIndex = 30;
            this.splitter2.TabStop = false;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 3;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.Controls.Add(this._discrestsChart, 1, 1);
            this.tableLayoutPanel14.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(0, 541);
            this.tableLayoutPanel14.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 2;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(1057, 565);
            this.tableLayoutPanel14.TabIndex = 29;
            // 
            // _discrestsChart
            // 
            this._discrestsChart.BkGradient = false;
            this._discrestsChart.BkGradientAngle = 90;
            this._discrestsChart.BkGradientColor = System.Drawing.Color.White;
            this._discrestsChart.BkGradientRate = 0.5F;
            this._discrestsChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._discrestsChart.BkRestrictedToChartPanel = false;
            this._discrestsChart.BkShinePosition = 1F;
            this._discrestsChart.BkTransparency = 0F;
            this._discrestsChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._discrestsChart.BorderExteriorLength = 0;
            this._discrestsChart.BorderGradientAngle = 225;
            this._discrestsChart.BorderGradientLightPos1 = 1F;
            this._discrestsChart.BorderGradientLightPos2 = -1F;
            this._discrestsChart.BorderGradientRate = 0.5F;
            this._discrestsChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._discrestsChart.BorderLightIntermediateBrightness = 0F;
            this._discrestsChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._discrestsChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._discrestsChart.ChartPanelBkTransparency = 0F;
            this._discrestsChart.ControlShadow = false;
            this._discrestsChart.CoordinateAxesVisible = true;
            this._discrestsChart.CoordinateAxisColor = System.Drawing.Color.Transparent;
            this._discrestsChart.CoordinateXOrigin = 0D;
            this._discrestsChart.CoordinateYMax = 100D;
            this._discrestsChart.CoordinateYMin = -100D;
            this._discrestsChart.CoordinateYOrigin = 0D;
            this._discrestsChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._discrestsChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._discrestsChart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._discrestsChart.FooterColor = System.Drawing.Color.Black;
            this._discrestsChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._discrestsChart.FooterVisible = false;
            this._discrestsChart.GridColor = System.Drawing.Color.MistyRose;
            this._discrestsChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._discrestsChart.GridVisible = true;
            this._discrestsChart.GridXSubTicker = 0;
            this._discrestsChart.GridXTicker = 10;
            this._discrestsChart.GridYSubTicker = 0;
            this._discrestsChart.GridYTicker = 2;
            this._discrestsChart.HeaderColor = System.Drawing.Color.Black;
            this._discrestsChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._discrestsChart.HeaderVisible = false;
            this._discrestsChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._discrestsChart.InnerBorderLength = 0;
            this._discrestsChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._discrestsChart.LegendBkColor = System.Drawing.Color.White;
            this._discrestsChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._discrestsChart.LegendVisible = false;
            this._discrestsChart.Location = new System.Drawing.Point(43, 26);
            this._discrestsChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._discrestsChart.MiddleBorderLength = 0;
            this._discrestsChart.Name = "_discrestsChart";
            this._discrestsChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._discrestsChart.OuterBorderLength = 0;
            this._discrestsChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._discrestsChart.Precision = 0;
            this._discrestsChart.RoundRadius = 10;
            this._discrestsChart.ShadowColor = System.Drawing.Color.DimGray;
            this._discrestsChart.ShadowDepth = 8;
            this._discrestsChart.ShadowRate = 0.5F;
            this._discrestsChart.Size = new System.Drawing.Size(991, 536);
            this._discrestsChart.TabIndex = 29;
            this._discrestsChart.Text = "daS_Net_XYChart2";
            this._discrestsChart.XMax = 100D;
            this._discrestsChart.XMin = 0D;
            this._discrestsChart.XScaleColor = System.Drawing.Color.White;
            this._discrestsChart.XScaleVisible = false;
            this._discrestsChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Chart_MouseClick);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.Controls.Add(this._discrete5Button, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this._discrete24Button, 0, 23);
            this.tableLayoutPanel5.Controls.Add(this._discrete23Button, 0, 22);
            this.tableLayoutPanel5.Controls.Add(this._discrete22Button, 0, 21);
            this.tableLayoutPanel5.Controls.Add(this._discrete21Button, 0, 20);
            this.tableLayoutPanel5.Controls.Add(this._discrete20Button, 0, 19);
            this.tableLayoutPanel5.Controls.Add(this._discrete19Button, 0, 18);
            this.tableLayoutPanel5.Controls.Add(this._discrete18Button, 0, 17);
            this.tableLayoutPanel5.Controls.Add(this._discrete17Button, 0, 16);
            this.tableLayoutPanel5.Controls.Add(this._discrete16Button, 0, 15);
            this.tableLayoutPanel5.Controls.Add(this._discrete15Button, 0, 14);
            this.tableLayoutPanel5.Controls.Add(this._discrete14Button, 0, 13);
            this.tableLayoutPanel5.Controls.Add(this._discrete13Button, 0, 12);
            this.tableLayoutPanel5.Controls.Add(this._discrete12Button, 0, 11);
            this.tableLayoutPanel5.Controls.Add(this._discrete11Button, 0, 10);
            this.tableLayoutPanel5.Controls.Add(this._discrete10Button, 0, 9);
            this.tableLayoutPanel5.Controls.Add(this._discrete9Button, 0, 8);
            this.tableLayoutPanel5.Controls.Add(this._discrete8Button, 0, 7);
            this.tableLayoutPanel5.Controls.Add(this._discrete7Button, 0, 6);
            this.tableLayoutPanel5.Controls.Add(this._discrete6Button, 0, 5);
            this.tableLayoutPanel5.Controls.Add(this._discrete2Button, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this._discrete3Button, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this._discrete4Button, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this._discrete1Button, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 26);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 25;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(34, 536);
            this.tableLayoutPanel5.TabIndex = 30;
            // 
            // _discrete5Button
            // 
            this._discrete5Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete5Button.AutoSize = true;
            this._discrete5Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete5Button.ForeColor = System.Drawing.Color.Black;
            this._discrete5Button.Location = new System.Drawing.Point(0, 88);
            this._discrete5Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete5Button.Name = "_discrete5Button";
            this._discrete5Button.Size = new System.Drawing.Size(41, 22);
            this._discrete5Button.TabIndex = 16;
            this._discrete5Button.Text = "Д5";
            this._discrete5Button.UseVisualStyleBackColor = false;
            this._discrete5Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete24Button
            // 
            this._discrete24Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete24Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete24Button.ForeColor = System.Drawing.Color.Black;
            this._discrete24Button.Location = new System.Drawing.Point(0, 506);
            this._discrete24Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete24Button.Name = "_discrete24Button";
            this._discrete24Button.Size = new System.Drawing.Size(41, 22);
            this._discrete24Button.TabIndex = 35;
            this._discrete24Button.Text = "Д24";
            this._discrete24Button.UseVisualStyleBackColor = false;
            this._discrete24Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete23Button
            // 
            this._discrete23Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete23Button.AutoSize = true;
            this._discrete23Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete23Button.ForeColor = System.Drawing.Color.Black;
            this._discrete23Button.Location = new System.Drawing.Point(0, 484);
            this._discrete23Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete23Button.Name = "_discrete23Button";
            this._discrete23Button.Size = new System.Drawing.Size(41, 22);
            this._discrete23Button.TabIndex = 34;
            this._discrete23Button.Text = "Д23";
            this._discrete23Button.UseVisualStyleBackColor = false;
            this._discrete23Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete22Button
            // 
            this._discrete22Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete22Button.AutoSize = true;
            this._discrete22Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete22Button.ForeColor = System.Drawing.Color.Black;
            this._discrete22Button.Location = new System.Drawing.Point(0, 462);
            this._discrete22Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete22Button.Name = "_discrete22Button";
            this._discrete22Button.Size = new System.Drawing.Size(41, 22);
            this._discrete22Button.TabIndex = 33;
            this._discrete22Button.Text = "Д22";
            this._discrete22Button.UseVisualStyleBackColor = false;
            this._discrete22Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete21Button
            // 
            this._discrete21Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete21Button.AutoSize = true;
            this._discrete21Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete21Button.ForeColor = System.Drawing.Color.Black;
            this._discrete21Button.Location = new System.Drawing.Point(0, 440);
            this._discrete21Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete21Button.Name = "_discrete21Button";
            this._discrete21Button.Size = new System.Drawing.Size(41, 22);
            this._discrete21Button.TabIndex = 32;
            this._discrete21Button.Text = "Д21";
            this._discrete21Button.UseVisualStyleBackColor = false;
            this._discrete21Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete20Button
            // 
            this._discrete20Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete20Button.AutoSize = true;
            this._discrete20Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete20Button.ForeColor = System.Drawing.Color.Black;
            this._discrete20Button.Location = new System.Drawing.Point(0, 418);
            this._discrete20Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete20Button.Name = "_discrete20Button";
            this._discrete20Button.Size = new System.Drawing.Size(41, 22);
            this._discrete20Button.TabIndex = 31;
            this._discrete20Button.Text = "Д20";
            this._discrete20Button.UseVisualStyleBackColor = false;
            this._discrete20Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete19Button
            // 
            this._discrete19Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete19Button.AutoSize = true;
            this._discrete19Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete19Button.ForeColor = System.Drawing.Color.Black;
            this._discrete19Button.Location = new System.Drawing.Point(0, 396);
            this._discrete19Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete19Button.Name = "_discrete19Button";
            this._discrete19Button.Size = new System.Drawing.Size(41, 22);
            this._discrete19Button.TabIndex = 30;
            this._discrete19Button.Text = "Д19";
            this._discrete19Button.UseVisualStyleBackColor = false;
            this._discrete19Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete18Button
            // 
            this._discrete18Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete18Button.AutoSize = true;
            this._discrete18Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete18Button.ForeColor = System.Drawing.Color.Black;
            this._discrete18Button.Location = new System.Drawing.Point(0, 374);
            this._discrete18Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete18Button.Name = "_discrete18Button";
            this._discrete18Button.Size = new System.Drawing.Size(41, 22);
            this._discrete18Button.TabIndex = 29;
            this._discrete18Button.Text = "Д18";
            this._discrete18Button.UseVisualStyleBackColor = false;
            this._discrete18Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete17Button
            // 
            this._discrete17Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete17Button.AutoSize = true;
            this._discrete17Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete17Button.ForeColor = System.Drawing.Color.Black;
            this._discrete17Button.Location = new System.Drawing.Point(0, 352);
            this._discrete17Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete17Button.Name = "_discrete17Button";
            this._discrete17Button.Size = new System.Drawing.Size(41, 22);
            this._discrete17Button.TabIndex = 28;
            this._discrete17Button.Text = "Д17";
            this._discrete17Button.UseVisualStyleBackColor = false;
            this._discrete17Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete16Button
            // 
            this._discrete16Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete16Button.AutoSize = true;
            this._discrete16Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete16Button.ForeColor = System.Drawing.Color.Black;
            this._discrete16Button.Location = new System.Drawing.Point(0, 330);
            this._discrete16Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete16Button.Name = "_discrete16Button";
            this._discrete16Button.Size = new System.Drawing.Size(41, 22);
            this._discrete16Button.TabIndex = 27;
            this._discrete16Button.Text = "Д16";
            this._discrete16Button.UseVisualStyleBackColor = false;
            this._discrete16Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete15Button
            // 
            this._discrete15Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete15Button.AutoSize = true;
            this._discrete15Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete15Button.ForeColor = System.Drawing.Color.Black;
            this._discrete15Button.Location = new System.Drawing.Point(0, 308);
            this._discrete15Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete15Button.Name = "_discrete15Button";
            this._discrete15Button.Size = new System.Drawing.Size(41, 22);
            this._discrete15Button.TabIndex = 26;
            this._discrete15Button.Text = "Д15";
            this._discrete15Button.UseVisualStyleBackColor = false;
            this._discrete15Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete14Button
            // 
            this._discrete14Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete14Button.AutoSize = true;
            this._discrete14Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete14Button.ForeColor = System.Drawing.Color.Black;
            this._discrete14Button.Location = new System.Drawing.Point(0, 286);
            this._discrete14Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete14Button.Name = "_discrete14Button";
            this._discrete14Button.Size = new System.Drawing.Size(41, 22);
            this._discrete14Button.TabIndex = 25;
            this._discrete14Button.Text = "Д14";
            this._discrete14Button.UseVisualStyleBackColor = false;
            this._discrete14Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete13Button
            // 
            this._discrete13Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete13Button.AutoSize = true;
            this._discrete13Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete13Button.ForeColor = System.Drawing.Color.Black;
            this._discrete13Button.Location = new System.Drawing.Point(0, 264);
            this._discrete13Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete13Button.Name = "_discrete13Button";
            this._discrete13Button.Size = new System.Drawing.Size(41, 22);
            this._discrete13Button.TabIndex = 24;
            this._discrete13Button.Text = "Д13";
            this._discrete13Button.UseVisualStyleBackColor = false;
            this._discrete13Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete12Button
            // 
            this._discrete12Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete12Button.AutoSize = true;
            this._discrete12Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete12Button.ForeColor = System.Drawing.Color.Black;
            this._discrete12Button.Location = new System.Drawing.Point(0, 242);
            this._discrete12Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete12Button.Name = "_discrete12Button";
            this._discrete12Button.Size = new System.Drawing.Size(41, 22);
            this._discrete12Button.TabIndex = 23;
            this._discrete12Button.Text = "Д12";
            this._discrete12Button.UseVisualStyleBackColor = false;
            this._discrete12Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete11Button
            // 
            this._discrete11Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete11Button.AutoSize = true;
            this._discrete11Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete11Button.ForeColor = System.Drawing.Color.Black;
            this._discrete11Button.Location = new System.Drawing.Point(0, 220);
            this._discrete11Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete11Button.Name = "_discrete11Button";
            this._discrete11Button.Size = new System.Drawing.Size(41, 22);
            this._discrete11Button.TabIndex = 22;
            this._discrete11Button.Text = "Д11";
            this._discrete11Button.UseVisualStyleBackColor = false;
            this._discrete11Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete10Button
            // 
            this._discrete10Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete10Button.AutoSize = true;
            this._discrete10Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete10Button.ForeColor = System.Drawing.Color.Black;
            this._discrete10Button.Location = new System.Drawing.Point(0, 198);
            this._discrete10Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete10Button.Name = "_discrete10Button";
            this._discrete10Button.Size = new System.Drawing.Size(41, 22);
            this._discrete10Button.TabIndex = 21;
            this._discrete10Button.Text = "Д10";
            this._discrete10Button.UseVisualStyleBackColor = false;
            this._discrete10Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete9Button
            // 
            this._discrete9Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete9Button.AutoSize = true;
            this._discrete9Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete9Button.ForeColor = System.Drawing.Color.Black;
            this._discrete9Button.Location = new System.Drawing.Point(0, 176);
            this._discrete9Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete9Button.Name = "_discrete9Button";
            this._discrete9Button.Size = new System.Drawing.Size(41, 22);
            this._discrete9Button.TabIndex = 20;
            this._discrete9Button.Text = "Д9";
            this._discrete9Button.UseVisualStyleBackColor = false;
            this._discrete9Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete8Button
            // 
            this._discrete8Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete8Button.AutoSize = true;
            this._discrete8Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete8Button.ForeColor = System.Drawing.Color.Black;
            this._discrete8Button.Location = new System.Drawing.Point(0, 154);
            this._discrete8Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete8Button.Name = "_discrete8Button";
            this._discrete8Button.Size = new System.Drawing.Size(41, 22);
            this._discrete8Button.TabIndex = 19;
            this._discrete8Button.Text = "Д8";
            this._discrete8Button.UseVisualStyleBackColor = false;
            this._discrete8Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete7Button
            // 
            this._discrete7Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete7Button.AutoSize = true;
            this._discrete7Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete7Button.ForeColor = System.Drawing.Color.Black;
            this._discrete7Button.Location = new System.Drawing.Point(0, 132);
            this._discrete7Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete7Button.Name = "_discrete7Button";
            this._discrete7Button.Size = new System.Drawing.Size(41, 22);
            this._discrete7Button.TabIndex = 18;
            this._discrete7Button.Text = "Д7";
            this._discrete7Button.UseVisualStyleBackColor = false;
            this._discrete7Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete6Button
            // 
            this._discrete6Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete6Button.AutoSize = true;
            this._discrete6Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete6Button.ForeColor = System.Drawing.Color.Black;
            this._discrete6Button.Location = new System.Drawing.Point(0, 110);
            this._discrete6Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete6Button.Name = "_discrete6Button";
            this._discrete6Button.Size = new System.Drawing.Size(41, 22);
            this._discrete6Button.TabIndex = 17;
            this._discrete6Button.Text = "Д6";
            this._discrete6Button.UseVisualStyleBackColor = false;
            this._discrete6Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete2Button
            // 
            this._discrete2Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete2Button.AutoSize = true;
            this._discrete2Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete2Button.ForeColor = System.Drawing.Color.Black;
            this._discrete2Button.Location = new System.Drawing.Point(0, 22);
            this._discrete2Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete2Button.Name = "_discrete2Button";
            this._discrete2Button.Size = new System.Drawing.Size(41, 22);
            this._discrete2Button.TabIndex = 37;
            this._discrete2Button.Text = "Д2";
            this._discrete2Button.UseVisualStyleBackColor = false;
            this._discrete2Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete3Button
            // 
            this._discrete3Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete3Button.AutoSize = true;
            this._discrete3Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete3Button.ForeColor = System.Drawing.Color.Black;
            this._discrete3Button.Location = new System.Drawing.Point(0, 44);
            this._discrete3Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete3Button.Name = "_discrete3Button";
            this._discrete3Button.Size = new System.Drawing.Size(41, 22);
            this._discrete3Button.TabIndex = 38;
            this._discrete3Button.Text = "Д3";
            this._discrete3Button.UseVisualStyleBackColor = false;
            this._discrete3Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete4Button
            // 
            this._discrete4Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete4Button.AutoSize = true;
            this._discrete4Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete4Button.ForeColor = System.Drawing.Color.Black;
            this._discrete4Button.Location = new System.Drawing.Point(0, 66);
            this._discrete4Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete4Button.Name = "_discrete4Button";
            this._discrete4Button.Size = new System.Drawing.Size(41, 22);
            this._discrete4Button.TabIndex = 39;
            this._discrete4Button.Text = "Д4";
            this._discrete4Button.UseVisualStyleBackColor = false;
            this._discrete4Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete1Button
            // 
            this._discrete1Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete1Button.AutoSize = true;
            this._discrete1Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete1Button.ForeColor = System.Drawing.Color.Black;
            this._discrete1Button.Location = new System.Drawing.Point(0, 0);
            this._discrete1Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete1Button.Name = "_discrete1Button";
            this._discrete1Button.Size = new System.Drawing.Size(41, 22);
            this._discrete1Button.TabIndex = 36;
            this._discrete1Button.Text = "Д1";
            this._discrete1Button.UseVisualStyleBackColor = false;
            this._discrete1Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 5);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Дискреты";
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Black;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Enabled = false;
            this.splitter1.Location = new System.Drawing.Point(0, 538);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1057, 3);
            this.splitter1.TabIndex = 28;
            this.splitter1.TabStop = false;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 3;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.Controls.Add(this._s1Scroll, 2, 1);
            this.tableLayoutPanel13.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this._currentСonnectionsChart, 1, 1);
            this.tableLayoutPanel13.Controls.Add(this.tableLayoutPanel8, 0, 1);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel13.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 2;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(1057, 538);
            this.tableLayoutPanel13.TabIndex = 27;
            // 
            // _s1Scroll
            // 
            this._s1Scroll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this._s1Scroll.Cursor = System.Windows.Forms.Cursors.Hand;
            this._s1Scroll.LargeChange = 1;
            this._s1Scroll.Location = new System.Drawing.Point(1039, 105);
            this._s1Scroll.Minimum = 100;
            this._s1Scroll.Name = "_s1Scroll";
            this._s1Scroll.Size = new System.Drawing.Size(15, 433);
            this._s1Scroll.TabIndex = 32;
            this._s1Scroll.Value = 100;
            this._s1Scroll.Visible = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 14;
            this.tableLayoutPanel13.SetColumnSpan(this.tableLayoutPanel3, 3);
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 107F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel6, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel7, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel9, 5, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel10, 7, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel11, 9, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel12, 11, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(987, 99);
            this.tableLayoutPanel3.TabIndex = 23;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this._i1Button, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this._i3Button, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this._i2Button, 0, 1);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(103, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 3;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(94, 93);
            this.tableLayoutPanel6.TabIndex = 16;
            // 
            // _i1Button
            // 
            this._i1Button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._i1Button.BackColor = System.Drawing.Color.Yellow;
            this._i1Button.Location = new System.Drawing.Point(3, 3);
            this._i1Button.Name = "_i1Button";
            this._i1Button.Size = new System.Drawing.Size(88, 25);
            this._i1Button.TabIndex = 10;
            this._i1Button.Text = "Ia Пр.1";
            this._i1Button.UseVisualStyleBackColor = false;
            this._i1Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // _i3Button
            // 
            this._i3Button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._i3Button.BackColor = System.Drawing.Color.Red;
            this._i3Button.Location = new System.Drawing.Point(3, 65);
            this._i3Button.Name = "_i3Button";
            this._i3Button.Size = new System.Drawing.Size(88, 25);
            this._i3Button.TabIndex = 13;
            this._i3Button.Text = "Ic Пр.1";
            this._i3Button.UseVisualStyleBackColor = false;
            this._i3Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // _i2Button
            // 
            this._i2Button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._i2Button.BackColor = System.Drawing.Color.Green;
            this._i2Button.Location = new System.Drawing.Point(3, 34);
            this._i2Button.Name = "_i2Button";
            this._i2Button.Size = new System.Drawing.Size(88, 25);
            this._i2Button.TabIndex = 9;
            this._i2Button.Text = "Ib Пр.1";
            this._i2Button.UseVisualStyleBackColor = false;
            this._i2Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Токи присоед.";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this._i4Button, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this._i5Button, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this._i6Button, 0, 2);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(233, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(94, 93);
            this.tableLayoutPanel7.TabIndex = 27;
            // 
            // _i4Button
            // 
            this._i4Button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._i4Button.BackColor = System.Drawing.Color.Indigo;
            this._i4Button.Location = new System.Drawing.Point(3, 3);
            this._i4Button.Name = "_i4Button";
            this._i4Button.Size = new System.Drawing.Size(88, 25);
            this._i4Button.TabIndex = 11;
            this._i4Button.Text = "Ia Пр.2";
            this._i4Button.UseVisualStyleBackColor = false;
            this._i4Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // _i5Button
            // 
            this._i5Button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._i5Button.BackColor = System.Drawing.Color.DarkOliveGreen;
            this._i5Button.Location = new System.Drawing.Point(3, 34);
            this._i5Button.Name = "_i5Button";
            this._i5Button.Size = new System.Drawing.Size(88, 25);
            this._i5Button.TabIndex = 15;
            this._i5Button.Text = "Ib Пр.2";
            this._i5Button.UseVisualStyleBackColor = false;
            this._i5Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // _i6Button
            // 
            this._i6Button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._i6Button.BackColor = System.Drawing.Color.CadetBlue;
            this._i6Button.Location = new System.Drawing.Point(3, 65);
            this._i6Button.Name = "_i6Button";
            this._i6Button.Size = new System.Drawing.Size(88, 25);
            this._i6Button.TabIndex = 16;
            this._i6Button.Text = "Ic Пр.2";
            this._i6Button.UseVisualStyleBackColor = false;
            this._i6Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this._i7Button, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this._i8Button, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this._i9Button, 0, 2);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(363, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 3;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(94, 93);
            this.tableLayoutPanel9.TabIndex = 28;
            // 
            // _i7Button
            // 
            this._i7Button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._i7Button.BackColor = System.Drawing.Color.LimeGreen;
            this._i7Button.Location = new System.Drawing.Point(3, 3);
            this._i7Button.Name = "_i7Button";
            this._i7Button.Size = new System.Drawing.Size(88, 25);
            this._i7Button.TabIndex = 17;
            this._i7Button.Text = "Ia Пр.3";
            this._i7Button.UseVisualStyleBackColor = false;
            this._i7Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // _i8Button
            // 
            this._i8Button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._i8Button.BackColor = System.Drawing.Color.Gold;
            this._i8Button.Location = new System.Drawing.Point(3, 34);
            this._i8Button.Name = "_i8Button";
            this._i8Button.Size = new System.Drawing.Size(88, 25);
            this._i8Button.TabIndex = 18;
            this._i8Button.Text = "Ib Пр.3";
            this._i8Button.UseVisualStyleBackColor = false;
            this._i8Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // _i9Button
            // 
            this._i9Button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._i9Button.BackColor = System.Drawing.Color.SaddleBrown;
            this._i9Button.Location = new System.Drawing.Point(3, 65);
            this._i9Button.Name = "_i9Button";
            this._i9Button.Size = new System.Drawing.Size(88, 25);
            this._i9Button.TabIndex = 19;
            this._i9Button.Text = "Ic Пр.3";
            this._i9Button.UseVisualStyleBackColor = false;
            this._i9Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this._i10Button, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this._i11Button, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this._i12Button, 0, 2);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(493, 3);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 3;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(94, 93);
            this.tableLayoutPanel10.TabIndex = 29;
            // 
            // _i10Button
            // 
            this._i10Button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._i10Button.BackColor = System.Drawing.Color.Tomato;
            this._i10Button.Location = new System.Drawing.Point(3, 3);
            this._i10Button.Name = "_i10Button";
            this._i10Button.Size = new System.Drawing.Size(88, 25);
            this._i10Button.TabIndex = 20;
            this._i10Button.Text = "Ia Пр.4";
            this._i10Button.UseVisualStyleBackColor = false;
            this._i10Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // _i11Button
            // 
            this._i11Button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._i11Button.BackColor = System.Drawing.Color.Brown;
            this._i11Button.Location = new System.Drawing.Point(3, 34);
            this._i11Button.Name = "_i11Button";
            this._i11Button.Size = new System.Drawing.Size(88, 25);
            this._i11Button.TabIndex = 21;
            this._i11Button.Text = "Ib Пр.4";
            this._i11Button.UseVisualStyleBackColor = false;
            this._i11Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // _i12Button
            // 
            this._i12Button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._i12Button.BackColor = System.Drawing.Color.SteelBlue;
            this._i12Button.Location = new System.Drawing.Point(3, 65);
            this._i12Button.Name = "_i12Button";
            this._i12Button.Size = new System.Drawing.Size(88, 25);
            this._i12Button.TabIndex = 22;
            this._i12Button.Text = "Ic Пр.4";
            this._i12Button.UseVisualStyleBackColor = false;
            this._i12Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Controls.Add(this._i13Button, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this._i14Button, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this._i15Button, 0, 2);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(623, 3);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 3;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(94, 93);
            this.tableLayoutPanel11.TabIndex = 30;
            // 
            // _i13Button
            // 
            this._i13Button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._i13Button.BackColor = System.Drawing.Color.Turquoise;
            this._i13Button.ForeColor = System.Drawing.Color.Black;
            this._i13Button.Location = new System.Drawing.Point(3, 3);
            this._i13Button.Name = "_i13Button";
            this._i13Button.Size = new System.Drawing.Size(88, 25);
            this._i13Button.TabIndex = 23;
            this._i13Button.Text = "Ia Пр.5";
            this._i13Button.UseVisualStyleBackColor = false;
            this._i13Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // _i14Button
            // 
            this._i14Button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._i14Button.BackColor = System.Drawing.Color.OrangeRed;
            this._i14Button.Location = new System.Drawing.Point(3, 34);
            this._i14Button.Name = "_i14Button";
            this._i14Button.Size = new System.Drawing.Size(88, 25);
            this._i14Button.TabIndex = 24;
            this._i14Button.Text = "Ib Пр.5";
            this._i14Button.UseVisualStyleBackColor = false;
            this._i14Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // _i15Button
            // 
            this._i15Button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._i15Button.BackColor = System.Drawing.Color.MidnightBlue;
            this._i15Button.ForeColor = System.Drawing.Color.White;
            this._i15Button.Location = new System.Drawing.Point(3, 65);
            this._i15Button.Name = "_i15Button";
            this._i15Button.Size = new System.Drawing.Size(88, 25);
            this._i15Button.TabIndex = 25;
            this._i15Button.Text = "Ic Пр.5";
            this._i15Button.UseVisualStyleBackColor = false;
            this._i15Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel12.ColumnCount = 1;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Controls.Add(this._i16Button, 0, 1);
            this.tableLayoutPanel12.Location = new System.Drawing.Point(753, 3);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 3;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(94, 93);
            this.tableLayoutPanel12.TabIndex = 31;
            // 
            // _i16Button
            // 
            this._i16Button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._i16Button.BackColor = System.Drawing.Color.Magenta;
            this._i16Button.Location = new System.Drawing.Point(3, 34);
            this._i16Button.Name = "_i16Button";
            this._i16Button.Size = new System.Drawing.Size(88, 25);
            this._i16Button.TabIndex = 26;
            this._i16Button.Text = "In";
            this._i16Button.UseVisualStyleBackColor = false;
            this._i16Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // _currentСonnectionsChart
            // 
            this._currentСonnectionsChart.BkGradient = false;
            this._currentСonnectionsChart.BkGradientAngle = 90;
            this._currentСonnectionsChart.BkGradientColor = System.Drawing.Color.White;
            this._currentСonnectionsChart.BkGradientRate = 0.5F;
            this._currentСonnectionsChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._currentСonnectionsChart.BkRestrictedToChartPanel = false;
            this._currentСonnectionsChart.BkShinePosition = 1F;
            this._currentСonnectionsChart.BkTransparency = 0F;
            this._currentСonnectionsChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._currentСonnectionsChart.BorderExteriorLength = 0;
            this._currentСonnectionsChart.BorderGradientAngle = 225;
            this._currentСonnectionsChart.BorderGradientLightPos1 = 1F;
            this._currentСonnectionsChart.BorderGradientLightPos2 = -1F;
            this._currentСonnectionsChart.BorderGradientRate = 0.5F;
            this._currentСonnectionsChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._currentСonnectionsChart.BorderLightIntermediateBrightness = 0F;
            this._currentСonnectionsChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._currentСonnectionsChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._currentСonnectionsChart.ChartPanelBkTransparency = 0F;
            this._currentСonnectionsChart.ControlShadow = false;
            this._currentСonnectionsChart.CoordinateAxesVisible = true;
            this._currentСonnectionsChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._currentСonnectionsChart.CoordinateXOrigin = 0D;
            this._currentСonnectionsChart.CoordinateYMax = 100D;
            this._currentСonnectionsChart.CoordinateYMin = -100D;
            this._currentСonnectionsChart.CoordinateYOrigin = 0D;
            this._currentСonnectionsChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._currentСonnectionsChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._currentСonnectionsChart.FooterColor = System.Drawing.Color.Black;
            this._currentСonnectionsChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._currentСonnectionsChart.FooterVisible = false;
            this._currentСonnectionsChart.GridColor = System.Drawing.Color.MistyRose;
            this._currentСonnectionsChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._currentСonnectionsChart.GridVisible = true;
            this._currentСonnectionsChart.GridXSubTicker = 0;
            this._currentСonnectionsChart.GridXTicker = 10;
            this._currentСonnectionsChart.GridYSubTicker = 0;
            this._currentСonnectionsChart.GridYTicker = 10;
            this._currentСonnectionsChart.HeaderColor = System.Drawing.Color.Black;
            this._currentСonnectionsChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._currentСonnectionsChart.HeaderVisible = false;
            this._currentСonnectionsChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._currentСonnectionsChart.InnerBorderLength = 0;
            this._currentСonnectionsChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._currentСonnectionsChart.LegendBkColor = System.Drawing.Color.White;
            this._currentСonnectionsChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._currentСonnectionsChart.LegendVisible = false;
            this._currentСonnectionsChart.Location = new System.Drawing.Point(43, 108);
            this._currentСonnectionsChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._currentСonnectionsChart.MiddleBorderLength = 0;
            this._currentСonnectionsChart.Name = "_currentСonnectionsChart";
            this._currentСonnectionsChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._currentСonnectionsChart.OuterBorderLength = 0;
            this._currentСonnectionsChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._currentСonnectionsChart.Precision = 0;
            this._currentСonnectionsChart.RoundRadius = 10;
            this._currentСonnectionsChart.ShadowColor = System.Drawing.Color.DimGray;
            this._currentСonnectionsChart.ShadowDepth = 8;
            this._currentСonnectionsChart.ShadowRate = 0.5F;
            this._currentСonnectionsChart.Size = new System.Drawing.Size(991, 427);
            this._currentСonnectionsChart.TabIndex = 33;
            this._currentСonnectionsChart.Text = "daS_Net_XYChart1";
            this._currentСonnectionsChart.XMax = 100D;
            this._currentСonnectionsChart.XMin = 0D;
            this._currentСonnectionsChart.XScaleColor = System.Drawing.Color.Black;
            this._currentСonnectionsChart.XScaleVisible = true;
            this._currentСonnectionsChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Chart_MouseClick);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this._currentСonnectionsChartDecreaseButton, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this._currentСonnectionsChartIncreaseButton, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 108);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 4;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(34, 427);
            this.tableLayoutPanel8.TabIndex = 31;
            // 
            // _currentСonnectionsChartDecreaseButton
            // 
            this._currentСonnectionsChartDecreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._currentСonnectionsChartDecreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._currentСonnectionsChartDecreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._currentСonnectionsChartDecreaseButton.Enabled = false;
            this._currentСonnectionsChartDecreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._currentСonnectionsChartDecreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._currentСonnectionsChartDecreaseButton.Location = new System.Drawing.Point(3, 216);
            this._currentСonnectionsChartDecreaseButton.Name = "_currentСonnectionsChartDecreaseButton";
            this._currentСonnectionsChartDecreaseButton.Size = new System.Drawing.Size(28, 24);
            this._currentСonnectionsChartDecreaseButton.TabIndex = 1;
            this._currentСonnectionsChartDecreaseButton.Text = "-";
            this._currentСonnectionsChartDecreaseButton.UseVisualStyleBackColor = false;
            // 
            // _currentСonnectionsChartIncreaseButton
            // 
            this._currentСonnectionsChartIncreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._currentСonnectionsChartIncreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._currentСonnectionsChartIncreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._currentСonnectionsChartIncreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._currentСonnectionsChartIncreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._currentСonnectionsChartIncreaseButton.Location = new System.Drawing.Point(3, 186);
            this._currentСonnectionsChartIncreaseButton.Name = "_currentСonnectionsChartIncreaseButton";
            this._currentСonnectionsChartIncreaseButton.Size = new System.Drawing.Size(28, 24);
            this._currentСonnectionsChartIncreaseButton.TabIndex = 0;
            this._currentСonnectionsChartIncreaseButton.Text = "+";
            this._currentСonnectionsChartIncreaseButton.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.Location = new System.Drawing.Point(9, 168);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Y";
            // 
            // MarkersTable
            // 
            this.MarkersTable.BackColor = System.Drawing.Color.LightGray;
            this.MarkersTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.MarkersTable.ColumnCount = 3;
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53F));
            this.MarkersTable.Controls.Add(this._marker1TrackBar, 1, 0);
            this.MarkersTable.Controls.Add(this._marker2TrackBar, 1, 1);
            this.MarkersTable.Controls.Add(this._label4, 0, 0);
            this.MarkersTable.Controls.Add(this.label6, 0, 1);
            this.MarkersTable.Dock = System.Windows.Forms.DockStyle.Top;
            this.MarkersTable.Location = new System.Drawing.Point(0, 0);
            this.MarkersTable.Margin = new System.Windows.Forms.Padding(0);
            this.MarkersTable.Name = "MarkersTable";
            this.MarkersTable.RowCount = 2;
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.MarkersTable.Size = new System.Drawing.Size(1078, 65);
            this.MarkersTable.TabIndex = 31;
            this.MarkersTable.Visible = false;
            // 
            // _marker1TrackBar
            // 
            this._marker1TrackBar.BackColor = System.Drawing.Color.LightSkyBlue;
            this._marker1TrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this._marker1TrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this._marker1TrackBar.Location = new System.Drawing.Point(75, 4);
            this._marker1TrackBar.Name = "_marker1TrackBar";
            this._marker1TrackBar.Size = new System.Drawing.Size(945, 24);
            this._marker1TrackBar.TabIndex = 2;
            this._marker1TrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this._marker1TrackBar.Scroll += new System.EventHandler(this._marker1TrackBar_Scroll);
            // 
            // _marker2TrackBar
            // 
            this._marker2TrackBar.BackColor = System.Drawing.Color.Violet;
            this._marker2TrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this._marker2TrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this._marker2TrackBar.LargeChange = 0;
            this._marker2TrackBar.Location = new System.Drawing.Point(75, 35);
            this._marker2TrackBar.Maximum = 3400;
            this._marker2TrackBar.Name = "_marker2TrackBar";
            this._marker2TrackBar.Size = new System.Drawing.Size(945, 26);
            this._marker2TrackBar.TabIndex = 3;
            this._marker2TrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this._marker2TrackBar.Scroll += new System.EventHandler(this._marker2TrackBar_Scroll);
            // 
            // _label4
            // 
            this._label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._label4.AutoSize = true;
            this._label4.Location = new System.Drawing.Point(4, 1);
            this._label4.Name = "_label4";
            this._label4.Size = new System.Drawing.Size(64, 30);
            this._label4.TabIndex = 5;
            this._label4.Text = "Маркер 1";
            this._label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 32);
            this.label6.TabIndex = 6;
            this.label6.Text = "Маркер 2";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(1087, 28);
            this.panel3.Name = "panel3";
            this.MAINTABLE.SetRowSpan(this.panel3, 2);
            this.panel3.Size = new System.Drawing.Size(294, 722);
            this.panel3.TabIndex = 33;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.LightGray;
            this.groupBox1.Controls.Add(this._markerScrollPanel);
            this.groupBox1.Location = new System.Drawing.Point(3, -1);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox1.Size = new System.Drawing.Size(293, 720);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Мгновенные значения";
            // 
            // _markerScrollPanel
            // 
            this._markerScrollPanel.AutoScroll = true;
            this._markerScrollPanel.Controls.Add(this._marker2Box);
            this._markerScrollPanel.Controls.Add(this._marker1Box);
            this._markerScrollPanel.Controls.Add(this.groupBox4);
            this._markerScrollPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._markerScrollPanel.Location = new System.Drawing.Point(0, 13);
            this._markerScrollPanel.Name = "_markerScrollPanel";
            this._markerScrollPanel.Size = new System.Drawing.Size(293, 707);
            this._markerScrollPanel.TabIndex = 3;
            // 
            // _marker2Box
            // 
            this._marker2Box.Controls.Add(this.groupBox9);
            this._marker2Box.Controls.Add(this.groupBox10);
            this._marker2Box.Controls.Add(this.groupBox12);
            this._marker2Box.Location = new System.Drawing.Point(142, 3);
            this._marker2Box.Name = "_marker2Box";
            this._marker2Box.Size = new System.Drawing.Size(133, 735);
            this._marker2Box.TabIndex = 3;
            this._marker2Box.TabStop = false;
            this._marker2Box.Text = "Маркер 2";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._marker2K8);
            this.groupBox9.Controls.Add(this._marker2K7);
            this.groupBox9.Controls.Add(this._marker2K6);
            this.groupBox9.Controls.Add(this._marker2K5);
            this.groupBox9.Controls.Add(this._marker2K4);
            this.groupBox9.Controls.Add(this._marker2K3);
            this.groupBox9.Controls.Add(this._marker2K2);
            this.groupBox9.Controls.Add(this._marker2K1);
            this.groupBox9.Location = new System.Drawing.Point(5, 626);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(119, 103);
            this.groupBox9.TabIndex = 44;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Каналы";
            // 
            // _marker2K8
            // 
            this._marker2K8.AutoSize = true;
            this._marker2K8.Location = new System.Drawing.Point(62, 76);
            this._marker2K8.Name = "_marker2K8";
            this._marker2K8.Size = new System.Drawing.Size(32, 13);
            this._marker2K8.TabIndex = 36;
            this._marker2K8.Text = "К8 = ";
            // 
            // _marker2K7
            // 
            this._marker2K7.AutoSize = true;
            this._marker2K7.Location = new System.Drawing.Point(62, 56);
            this._marker2K7.Name = "_marker2K7";
            this._marker2K7.Size = new System.Drawing.Size(32, 13);
            this._marker2K7.TabIndex = 35;
            this._marker2K7.Text = "К7 = ";
            // 
            // _marker2K6
            // 
            this._marker2K6.AutoSize = true;
            this._marker2K6.Location = new System.Drawing.Point(62, 36);
            this._marker2K6.Name = "_marker2K6";
            this._marker2K6.Size = new System.Drawing.Size(32, 13);
            this._marker2K6.TabIndex = 34;
            this._marker2K6.Text = "К6 = ";
            // 
            // _marker2K5
            // 
            this._marker2K5.AutoSize = true;
            this._marker2K5.Location = new System.Drawing.Point(62, 16);
            this._marker2K5.Name = "_marker2K5";
            this._marker2K5.Size = new System.Drawing.Size(32, 13);
            this._marker2K5.TabIndex = 33;
            this._marker2K5.Text = "К5 = ";
            // 
            // _marker2K4
            // 
            this._marker2K4.AutoSize = true;
            this._marker2K4.Location = new System.Drawing.Point(6, 76);
            this._marker2K4.Name = "_marker2K4";
            this._marker2K4.Size = new System.Drawing.Size(32, 13);
            this._marker2K4.TabIndex = 32;
            this._marker2K4.Text = "К4 = ";
            // 
            // _marker2K3
            // 
            this._marker2K3.AutoSize = true;
            this._marker2K3.Location = new System.Drawing.Point(6, 56);
            this._marker2K3.Name = "_marker2K3";
            this._marker2K3.Size = new System.Drawing.Size(32, 13);
            this._marker2K3.TabIndex = 31;
            this._marker2K3.Text = "К3 = ";
            // 
            // _marker2K2
            // 
            this._marker2K2.AutoSize = true;
            this._marker2K2.Location = new System.Drawing.Point(6, 36);
            this._marker2K2.Name = "_marker2K2";
            this._marker2K2.Size = new System.Drawing.Size(32, 13);
            this._marker2K2.TabIndex = 30;
            this._marker2K2.Text = "К2 = ";
            // 
            // _marker2K1
            // 
            this._marker2K1.AutoSize = true;
            this._marker2K1.Location = new System.Drawing.Point(6, 16);
            this._marker2K1.Name = "_marker2K1";
            this._marker2K1.Size = new System.Drawing.Size(32, 13);
            this._marker2K1.TabIndex = 29;
            this._marker2K1.Text = "К1 = ";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._marker2D24);
            this.groupBox10.Controls.Add(this._marker2D23);
            this.groupBox10.Controls.Add(this._marker2D22);
            this.groupBox10.Controls.Add(this._marker2D21);
            this.groupBox10.Controls.Add(this._marker2D20);
            this.groupBox10.Controls.Add(this._marker2D11);
            this.groupBox10.Controls.Add(this._marker2D10);
            this.groupBox10.Controls.Add(this._marker2D9);
            this.groupBox10.Controls.Add(this._marker2D19);
            this.groupBox10.Controls.Add(this._marker2D18);
            this.groupBox10.Controls.Add(this._marker2D17);
            this.groupBox10.Controls.Add(this._marker2D16);
            this.groupBox10.Controls.Add(this._marker2D15);
            this.groupBox10.Controls.Add(this._marker2D14);
            this.groupBox10.Controls.Add(this._marker2D13);
            this.groupBox10.Controls.Add(this._marker2D12);
            this.groupBox10.Controls.Add(this._marker2D8);
            this.groupBox10.Controls.Add(this._marker2D7);
            this.groupBox10.Controls.Add(this._marker2D6);
            this.groupBox10.Controls.Add(this._marker2D5);
            this.groupBox10.Controls.Add(this._marker2D4);
            this.groupBox10.Controls.Add(this._marker2D3);
            this.groupBox10.Controls.Add(this._marker2D2);
            this.groupBox10.Controls.Add(this._marker2D1);
            this.groupBox10.Location = new System.Drawing.Point(5, 358);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(119, 262);
            this.groupBox10.TabIndex = 43;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Дискреты";
            // 
            // _marker2D24
            // 
            this._marker2D24.AutoSize = true;
            this._marker2D24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D24.Location = new System.Drawing.Point(62, 236);
            this._marker2D24.Name = "_marker2D24";
            this._marker2D24.Size = new System.Drawing.Size(40, 13);
            this._marker2D24.TabIndex = 39;
            this._marker2D24.Text = "Д24 = ";
            // 
            // _marker2D23
            // 
            this._marker2D23.AutoSize = true;
            this._marker2D23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D23.Location = new System.Drawing.Point(62, 216);
            this._marker2D23.Name = "_marker2D23";
            this._marker2D23.Size = new System.Drawing.Size(40, 13);
            this._marker2D23.TabIndex = 38;
            this._marker2D23.Text = "Д23 = ";
            // 
            // _marker2D22
            // 
            this._marker2D22.AutoSize = true;
            this._marker2D22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D22.Location = new System.Drawing.Point(61, 196);
            this._marker2D22.Name = "_marker2D22";
            this._marker2D22.Size = new System.Drawing.Size(40, 13);
            this._marker2D22.TabIndex = 37;
            this._marker2D22.Text = "Д22 = ";
            // 
            // _marker2D21
            // 
            this._marker2D21.AutoSize = true;
            this._marker2D21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D21.Location = new System.Drawing.Point(61, 176);
            this._marker2D21.Name = "_marker2D21";
            this._marker2D21.Size = new System.Drawing.Size(40, 13);
            this._marker2D21.TabIndex = 36;
            this._marker2D21.Text = "Д21 = ";
            // 
            // _marker2D20
            // 
            this._marker2D20.AutoSize = true;
            this._marker2D20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D20.Location = new System.Drawing.Point(61, 156);
            this._marker2D20.Name = "_marker2D20";
            this._marker2D20.Size = new System.Drawing.Size(40, 13);
            this._marker2D20.TabIndex = 35;
            this._marker2D20.Text = "Д20 = ";
            // 
            // _marker2D11
            // 
            this._marker2D11.AutoSize = true;
            this._marker2D11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D11.Location = new System.Drawing.Point(6, 216);
            this._marker2D11.Name = "_marker2D11";
            this._marker2D11.Size = new System.Drawing.Size(40, 13);
            this._marker2D11.TabIndex = 34;
            this._marker2D11.Text = "Д11 = ";
            // 
            // _marker2D10
            // 
            this._marker2D10.AutoSize = true;
            this._marker2D10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D10.Location = new System.Drawing.Point(6, 196);
            this._marker2D10.Name = "_marker2D10";
            this._marker2D10.Size = new System.Drawing.Size(40, 13);
            this._marker2D10.TabIndex = 33;
            this._marker2D10.Text = "Д10 = ";
            // 
            // _marker2D9
            // 
            this._marker2D9.AutoSize = true;
            this._marker2D9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D9.Location = new System.Drawing.Point(6, 176);
            this._marker2D9.Name = "_marker2D9";
            this._marker2D9.Size = new System.Drawing.Size(34, 13);
            this._marker2D9.TabIndex = 32;
            this._marker2D9.Text = "Д9 = ";
            // 
            // _marker2D19
            // 
            this._marker2D19.AutoSize = true;
            this._marker2D19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D19.Location = new System.Drawing.Point(61, 136);
            this._marker2D19.Name = "_marker2D19";
            this._marker2D19.Size = new System.Drawing.Size(40, 13);
            this._marker2D19.TabIndex = 31;
            this._marker2D19.Text = "Д19 = ";
            // 
            // _marker2D18
            // 
            this._marker2D18.AutoSize = true;
            this._marker2D18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D18.Location = new System.Drawing.Point(61, 116);
            this._marker2D18.Name = "_marker2D18";
            this._marker2D18.Size = new System.Drawing.Size(40, 13);
            this._marker2D18.TabIndex = 30;
            this._marker2D18.Text = "Д18 = ";
            // 
            // _marker2D17
            // 
            this._marker2D17.AutoSize = true;
            this._marker2D17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D17.Location = new System.Drawing.Point(61, 96);
            this._marker2D17.Name = "_marker2D17";
            this._marker2D17.Size = new System.Drawing.Size(40, 13);
            this._marker2D17.TabIndex = 29;
            this._marker2D17.Text = "Д17 = ";
            // 
            // _marker2D16
            // 
            this._marker2D16.AutoSize = true;
            this._marker2D16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D16.Location = new System.Drawing.Point(61, 76);
            this._marker2D16.Name = "_marker2D16";
            this._marker2D16.Size = new System.Drawing.Size(40, 13);
            this._marker2D16.TabIndex = 28;
            this._marker2D16.Text = "Д16 = ";
            // 
            // _marker2D15
            // 
            this._marker2D15.AutoSize = true;
            this._marker2D15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D15.Location = new System.Drawing.Point(61, 56);
            this._marker2D15.Name = "_marker2D15";
            this._marker2D15.Size = new System.Drawing.Size(40, 13);
            this._marker2D15.TabIndex = 27;
            this._marker2D15.Text = "Д15 = ";
            // 
            // _marker2D14
            // 
            this._marker2D14.AutoSize = true;
            this._marker2D14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D14.Location = new System.Drawing.Point(61, 36);
            this._marker2D14.Name = "_marker2D14";
            this._marker2D14.Size = new System.Drawing.Size(40, 13);
            this._marker2D14.TabIndex = 26;
            this._marker2D14.Text = "Д14 = ";
            // 
            // _marker2D13
            // 
            this._marker2D13.AutoSize = true;
            this._marker2D13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D13.Location = new System.Drawing.Point(61, 16);
            this._marker2D13.Name = "_marker2D13";
            this._marker2D13.Size = new System.Drawing.Size(40, 13);
            this._marker2D13.TabIndex = 25;
            this._marker2D13.Text = "Д13 = ";
            // 
            // _marker2D12
            // 
            this._marker2D12.AutoSize = true;
            this._marker2D12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D12.Location = new System.Drawing.Point(5, 236);
            this._marker2D12.Name = "_marker2D12";
            this._marker2D12.Size = new System.Drawing.Size(40, 13);
            this._marker2D12.TabIndex = 24;
            this._marker2D12.Text = "Д12 = ";
            // 
            // _marker2D8
            // 
            this._marker2D8.AutoSize = true;
            this._marker2D8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D8.Location = new System.Drawing.Point(6, 156);
            this._marker2D8.Name = "_marker2D8";
            this._marker2D8.Size = new System.Drawing.Size(34, 13);
            this._marker2D8.TabIndex = 23;
            this._marker2D8.Text = "Д8 = ";
            // 
            // _marker2D7
            // 
            this._marker2D7.AutoSize = true;
            this._marker2D7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D7.Location = new System.Drawing.Point(6, 136);
            this._marker2D7.Name = "_marker2D7";
            this._marker2D7.Size = new System.Drawing.Size(34, 13);
            this._marker2D7.TabIndex = 22;
            this._marker2D7.Text = "Д7 = ";
            // 
            // _marker2D6
            // 
            this._marker2D6.AutoSize = true;
            this._marker2D6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D6.Location = new System.Drawing.Point(6, 116);
            this._marker2D6.Name = "_marker2D6";
            this._marker2D6.Size = new System.Drawing.Size(34, 13);
            this._marker2D6.TabIndex = 21;
            this._marker2D6.Text = "Д6 = ";
            // 
            // _marker2D5
            // 
            this._marker2D5.AutoSize = true;
            this._marker2D5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D5.Location = new System.Drawing.Point(6, 96);
            this._marker2D5.Name = "_marker2D5";
            this._marker2D5.Size = new System.Drawing.Size(34, 13);
            this._marker2D5.TabIndex = 20;
            this._marker2D5.Text = "Д5 = ";
            // 
            // _marker2D4
            // 
            this._marker2D4.AutoSize = true;
            this._marker2D4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D4.Location = new System.Drawing.Point(6, 76);
            this._marker2D4.Name = "_marker2D4";
            this._marker2D4.Size = new System.Drawing.Size(34, 13);
            this._marker2D4.TabIndex = 19;
            this._marker2D4.Text = "Д4 = ";
            // 
            // _marker2D3
            // 
            this._marker2D3.AutoSize = true;
            this._marker2D3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D3.Location = new System.Drawing.Point(6, 56);
            this._marker2D3.Name = "_marker2D3";
            this._marker2D3.Size = new System.Drawing.Size(34, 13);
            this._marker2D3.TabIndex = 18;
            this._marker2D3.Text = "Д3 = ";
            // 
            // _marker2D2
            // 
            this._marker2D2.AutoSize = true;
            this._marker2D2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D2.Location = new System.Drawing.Point(6, 36);
            this._marker2D2.Name = "_marker2D2";
            this._marker2D2.Size = new System.Drawing.Size(34, 13);
            this._marker2D2.TabIndex = 17;
            this._marker2D2.Text = "Д2 = ";
            // 
            // _marker2D1
            // 
            this._marker2D1.AutoSize = true;
            this._marker2D1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D1.Location = new System.Drawing.Point(6, 16);
            this._marker2D1.Name = "_marker2D1";
            this._marker2D1.Size = new System.Drawing.Size(34, 13);
            this._marker2D1.TabIndex = 16;
            this._marker2D1.Text = "Д1 = ";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._marker2I16);
            this.groupBox12.Controls.Add(this._marker2I15);
            this.groupBox12.Controls.Add(this._marker2I14);
            this.groupBox12.Controls.Add(this._marker2I13);
            this.groupBox12.Controls.Add(this._marker2I12);
            this.groupBox12.Controls.Add(this._marker2I11);
            this.groupBox12.Controls.Add(this._marker2I10);
            this.groupBox12.Controls.Add(this._marker2I9);
            this.groupBox12.Controls.Add(this._marker2I8);
            this.groupBox12.Controls.Add(this._marker2I7);
            this.groupBox12.Controls.Add(this._marker2I6);
            this.groupBox12.Controls.Add(this._marker2I5);
            this.groupBox12.Controls.Add(this._marker2I4);
            this.groupBox12.Controls.Add(this._marker2I3);
            this.groupBox12.Controls.Add(this._marker2I2);
            this.groupBox12.Controls.Add(this._marker2I1);
            this.groupBox12.Location = new System.Drawing.Point(5, 12);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(119, 340);
            this.groupBox12.TabIndex = 40;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Токи присоед.";
            // 
            // _marker2I16
            // 
            this._marker2I16.AutoSize = true;
            this._marker2I16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I16.Location = new System.Drawing.Point(6, 318);
            this._marker2I16.Name = "_marker2I16";
            this._marker2I16.Size = new System.Drawing.Size(25, 13);
            this._marker2I16.TabIndex = 15;
            this._marker2I16.Text = "In =";
            // 
            // _marker2I15
            // 
            this._marker2I15.AutoSize = true;
            this._marker2I15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I15.Location = new System.Drawing.Point(6, 298);
            this._marker2I15.Name = "_marker2I15";
            this._marker2I15.Size = new System.Drawing.Size(54, 13);
            this._marker2I15.TabIndex = 14;
            this._marker2I15.Text = "Ic Пр.5 = ";
            // 
            // _marker2I14
            // 
            this._marker2I14.AutoSize = true;
            this._marker2I14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I14.Location = new System.Drawing.Point(6, 278);
            this._marker2I14.Name = "_marker2I14";
            this._marker2I14.Size = new System.Drawing.Size(54, 13);
            this._marker2I14.TabIndex = 13;
            this._marker2I14.Text = "Ib Пр.5 = ";
            // 
            // _marker2I13
            // 
            this._marker2I13.AutoSize = true;
            this._marker2I13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I13.Location = new System.Drawing.Point(6, 258);
            this._marker2I13.Name = "_marker2I13";
            this._marker2I13.Size = new System.Drawing.Size(54, 13);
            this._marker2I13.TabIndex = 12;
            this._marker2I13.Text = "Ia Пр.5 = ";
            // 
            // _marker2I12
            // 
            this._marker2I12.AutoSize = true;
            this._marker2I12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I12.Location = new System.Drawing.Point(6, 238);
            this._marker2I12.Name = "_marker2I12";
            this._marker2I12.Size = new System.Drawing.Size(54, 13);
            this._marker2I12.TabIndex = 11;
            this._marker2I12.Text = "Ic Пр.4 = ";
            // 
            // _marker2I11
            // 
            this._marker2I11.AutoSize = true;
            this._marker2I11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I11.Location = new System.Drawing.Point(6, 218);
            this._marker2I11.Name = "_marker2I11";
            this._marker2I11.Size = new System.Drawing.Size(54, 13);
            this._marker2I11.TabIndex = 10;
            this._marker2I11.Text = "Ib Пр.4 = ";
            // 
            // _marker2I10
            // 
            this._marker2I10.AutoSize = true;
            this._marker2I10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I10.Location = new System.Drawing.Point(6, 198);
            this._marker2I10.Name = "_marker2I10";
            this._marker2I10.Size = new System.Drawing.Size(54, 13);
            this._marker2I10.TabIndex = 9;
            this._marker2I10.Text = "Ia Пр.4 = ";
            // 
            // _marker2I9
            // 
            this._marker2I9.AutoSize = true;
            this._marker2I9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I9.Location = new System.Drawing.Point(6, 178);
            this._marker2I9.Name = "_marker2I9";
            this._marker2I9.Size = new System.Drawing.Size(54, 13);
            this._marker2I9.TabIndex = 8;
            this._marker2I9.Text = "Ic Пр.3 = ";
            // 
            // _marker2I8
            // 
            this._marker2I8.AutoSize = true;
            this._marker2I8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I8.Location = new System.Drawing.Point(6, 158);
            this._marker2I8.Name = "_marker2I8";
            this._marker2I8.Size = new System.Drawing.Size(54, 13);
            this._marker2I8.TabIndex = 7;
            this._marker2I8.Text = "Ib Пр.3 = ";
            // 
            // _marker2I7
            // 
            this._marker2I7.AutoSize = true;
            this._marker2I7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I7.Location = new System.Drawing.Point(6, 138);
            this._marker2I7.Name = "_marker2I7";
            this._marker2I7.Size = new System.Drawing.Size(54, 13);
            this._marker2I7.TabIndex = 6;
            this._marker2I7.Text = "Ia Пр.3 = ";
            // 
            // _marker2I6
            // 
            this._marker2I6.AutoSize = true;
            this._marker2I6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I6.Location = new System.Drawing.Point(6, 118);
            this._marker2I6.Name = "_marker2I6";
            this._marker2I6.Size = new System.Drawing.Size(54, 13);
            this._marker2I6.TabIndex = 5;
            this._marker2I6.Text = "Ic Пр.2 = ";
            // 
            // _marker2I5
            // 
            this._marker2I5.AutoSize = true;
            this._marker2I5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I5.Location = new System.Drawing.Point(6, 98);
            this._marker2I5.Name = "_marker2I5";
            this._marker2I5.Size = new System.Drawing.Size(54, 13);
            this._marker2I5.TabIndex = 4;
            this._marker2I5.Text = "Ib Пр.2 = ";
            // 
            // _marker2I4
            // 
            this._marker2I4.AutoSize = true;
            this._marker2I4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I4.Location = new System.Drawing.Point(6, 78);
            this._marker2I4.Name = "_marker2I4";
            this._marker2I4.Size = new System.Drawing.Size(54, 13);
            this._marker2I4.TabIndex = 3;
            this._marker2I4.Text = "Ia Пр.2 = ";
            // 
            // _marker2I3
            // 
            this._marker2I3.AutoSize = true;
            this._marker2I3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I3.Location = new System.Drawing.Point(6, 58);
            this._marker2I3.Name = "_marker2I3";
            this._marker2I3.Size = new System.Drawing.Size(54, 13);
            this._marker2I3.TabIndex = 2;
            this._marker2I3.Text = "Ic Пр.1 = ";
            // 
            // _marker2I2
            // 
            this._marker2I2.AutoSize = true;
            this._marker2I2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I2.Location = new System.Drawing.Point(6, 38);
            this._marker2I2.Name = "_marker2I2";
            this._marker2I2.Size = new System.Drawing.Size(54, 13);
            this._marker2I2.TabIndex = 1;
            this._marker2I2.Text = "Ib Пр.1 = ";
            // 
            // _marker2I1
            // 
            this._marker2I1.AutoSize = true;
            this._marker2I1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I1.Location = new System.Drawing.Point(6, 18);
            this._marker2I1.Name = "_marker2I1";
            this._marker2I1.Size = new System.Drawing.Size(54, 13);
            this._marker2I1.TabIndex = 0;
            this._marker2I1.Text = "Ia Пр.1 = ";
            // 
            // _marker1Box
            // 
            this._marker1Box.Controls.Add(this.groupBox16);
            this._marker1Box.Controls.Add(this.groupBox11);
            this._marker1Box.Controls.Add(this.groupBox8);
            this._marker1Box.Location = new System.Drawing.Point(3, 3);
            this._marker1Box.Name = "_marker1Box";
            this._marker1Box.Size = new System.Drawing.Size(133, 735);
            this._marker1Box.TabIndex = 0;
            this._marker1Box.TabStop = false;
            this._marker1Box.Text = "Маркер 1";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this._marker1K8);
            this.groupBox16.Controls.Add(this._marker1K7);
            this.groupBox16.Controls.Add(this._marker1K6);
            this.groupBox16.Controls.Add(this._marker1K5);
            this.groupBox16.Controls.Add(this._marker1K4);
            this.groupBox16.Controls.Add(this._marker1K3);
            this.groupBox16.Controls.Add(this._marker1K2);
            this.groupBox16.Controls.Add(this._marker1K1);
            this.groupBox16.Location = new System.Drawing.Point(5, 626);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(119, 103);
            this.groupBox16.TabIndex = 44;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Каналы";
            // 
            // _marker1K8
            // 
            this._marker1K8.AutoSize = true;
            this._marker1K8.Location = new System.Drawing.Point(62, 76);
            this._marker1K8.Name = "_marker1K8";
            this._marker1K8.Size = new System.Drawing.Size(32, 13);
            this._marker1K8.TabIndex = 36;
            this._marker1K8.Text = "К8 = ";
            // 
            // _marker1K7
            // 
            this._marker1K7.AutoSize = true;
            this._marker1K7.Location = new System.Drawing.Point(62, 56);
            this._marker1K7.Name = "_marker1K7";
            this._marker1K7.Size = new System.Drawing.Size(32, 13);
            this._marker1K7.TabIndex = 35;
            this._marker1K7.Text = "К7 = ";
            // 
            // _marker1K6
            // 
            this._marker1K6.AutoSize = true;
            this._marker1K6.Location = new System.Drawing.Point(62, 36);
            this._marker1K6.Name = "_marker1K6";
            this._marker1K6.Size = new System.Drawing.Size(32, 13);
            this._marker1K6.TabIndex = 34;
            this._marker1K6.Text = "К6 = ";
            // 
            // _marker1K5
            // 
            this._marker1K5.AutoSize = true;
            this._marker1K5.Location = new System.Drawing.Point(62, 16);
            this._marker1K5.Name = "_marker1K5";
            this._marker1K5.Size = new System.Drawing.Size(32, 13);
            this._marker1K5.TabIndex = 33;
            this._marker1K5.Text = "К5 = ";
            // 
            // _marker1K4
            // 
            this._marker1K4.AutoSize = true;
            this._marker1K4.Location = new System.Drawing.Point(6, 76);
            this._marker1K4.Name = "_marker1K4";
            this._marker1K4.Size = new System.Drawing.Size(32, 13);
            this._marker1K4.TabIndex = 32;
            this._marker1K4.Text = "К4 = ";
            // 
            // _marker1K3
            // 
            this._marker1K3.AutoSize = true;
            this._marker1K3.Location = new System.Drawing.Point(6, 56);
            this._marker1K3.Name = "_marker1K3";
            this._marker1K3.Size = new System.Drawing.Size(32, 13);
            this._marker1K3.TabIndex = 31;
            this._marker1K3.Text = "К3 = ";
            // 
            // _marker1K2
            // 
            this._marker1K2.AutoSize = true;
            this._marker1K2.Location = new System.Drawing.Point(6, 36);
            this._marker1K2.Name = "_marker1K2";
            this._marker1K2.Size = new System.Drawing.Size(32, 13);
            this._marker1K2.TabIndex = 30;
            this._marker1K2.Text = "К2 = ";
            // 
            // _marker1K1
            // 
            this._marker1K1.AutoSize = true;
            this._marker1K1.Location = new System.Drawing.Point(6, 16);
            this._marker1K1.Name = "_marker1K1";
            this._marker1K1.Size = new System.Drawing.Size(32, 13);
            this._marker1K1.TabIndex = 29;
            this._marker1K1.Text = "К1 = ";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._marker1D24);
            this.groupBox11.Controls.Add(this._marker1D23);
            this.groupBox11.Controls.Add(this._marker1D22);
            this.groupBox11.Controls.Add(this._marker1D21);
            this.groupBox11.Controls.Add(this._marker1D20);
            this.groupBox11.Controls.Add(this._marker1D11);
            this.groupBox11.Controls.Add(this._marker1D10);
            this.groupBox11.Controls.Add(this._marker1D9);
            this.groupBox11.Controls.Add(this._marker1D19);
            this.groupBox11.Controls.Add(this._marker1D18);
            this.groupBox11.Controls.Add(this._marker1D17);
            this.groupBox11.Controls.Add(this._marker1D16);
            this.groupBox11.Controls.Add(this._marker1D15);
            this.groupBox11.Controls.Add(this._marker1D14);
            this.groupBox11.Controls.Add(this._marker1D13);
            this.groupBox11.Controls.Add(this._marker1D12);
            this.groupBox11.Controls.Add(this._marker1D8);
            this.groupBox11.Controls.Add(this._marker1D7);
            this.groupBox11.Controls.Add(this._marker1D6);
            this.groupBox11.Controls.Add(this._marker1D5);
            this.groupBox11.Controls.Add(this._marker1D4);
            this.groupBox11.Controls.Add(this._marker1D3);
            this.groupBox11.Controls.Add(this._marker1D2);
            this.groupBox11.Controls.Add(this._marker1D1);
            this.groupBox11.Location = new System.Drawing.Point(5, 358);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(119, 262);
            this.groupBox11.TabIndex = 43;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Дискреты";
            // 
            // _marker1D24
            // 
            this._marker1D24.AutoSize = true;
            this._marker1D24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D24.Location = new System.Drawing.Point(62, 236);
            this._marker1D24.Name = "_marker1D24";
            this._marker1D24.Size = new System.Drawing.Size(40, 13);
            this._marker1D24.TabIndex = 39;
            this._marker1D24.Text = "Д24 = ";
            // 
            // _marker1D23
            // 
            this._marker1D23.AutoSize = true;
            this._marker1D23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D23.Location = new System.Drawing.Point(62, 216);
            this._marker1D23.Name = "_marker1D23";
            this._marker1D23.Size = new System.Drawing.Size(40, 13);
            this._marker1D23.TabIndex = 38;
            this._marker1D23.Text = "Д23 = ";
            // 
            // _marker1D22
            // 
            this._marker1D22.AutoSize = true;
            this._marker1D22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D22.Location = new System.Drawing.Point(61, 196);
            this._marker1D22.Name = "_marker1D22";
            this._marker1D22.Size = new System.Drawing.Size(40, 13);
            this._marker1D22.TabIndex = 37;
            this._marker1D22.Text = "Д22 = ";
            // 
            // _marker1D21
            // 
            this._marker1D21.AutoSize = true;
            this._marker1D21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D21.Location = new System.Drawing.Point(61, 176);
            this._marker1D21.Name = "_marker1D21";
            this._marker1D21.Size = new System.Drawing.Size(40, 13);
            this._marker1D21.TabIndex = 36;
            this._marker1D21.Text = "Д21 = ";
            // 
            // _marker1D20
            // 
            this._marker1D20.AutoSize = true;
            this._marker1D20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D20.Location = new System.Drawing.Point(61, 156);
            this._marker1D20.Name = "_marker1D20";
            this._marker1D20.Size = new System.Drawing.Size(40, 13);
            this._marker1D20.TabIndex = 35;
            this._marker1D20.Text = "Д20 = ";
            // 
            // _marker1D11
            // 
            this._marker1D11.AutoSize = true;
            this._marker1D11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D11.Location = new System.Drawing.Point(6, 216);
            this._marker1D11.Name = "_marker1D11";
            this._marker1D11.Size = new System.Drawing.Size(40, 13);
            this._marker1D11.TabIndex = 34;
            this._marker1D11.Text = "Д11 = ";
            // 
            // _marker1D10
            // 
            this._marker1D10.AutoSize = true;
            this._marker1D10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D10.Location = new System.Drawing.Point(6, 196);
            this._marker1D10.Name = "_marker1D10";
            this._marker1D10.Size = new System.Drawing.Size(40, 13);
            this._marker1D10.TabIndex = 33;
            this._marker1D10.Text = "Д10 = ";
            // 
            // _marker1D9
            // 
            this._marker1D9.AutoSize = true;
            this._marker1D9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D9.Location = new System.Drawing.Point(6, 176);
            this._marker1D9.Name = "_marker1D9";
            this._marker1D9.Size = new System.Drawing.Size(34, 13);
            this._marker1D9.TabIndex = 32;
            this._marker1D9.Text = "Д9 = ";
            // 
            // _marker1D19
            // 
            this._marker1D19.AutoSize = true;
            this._marker1D19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D19.Location = new System.Drawing.Point(61, 136);
            this._marker1D19.Name = "_marker1D19";
            this._marker1D19.Size = new System.Drawing.Size(40, 13);
            this._marker1D19.TabIndex = 31;
            this._marker1D19.Text = "Д19 = ";
            // 
            // _marker1D18
            // 
            this._marker1D18.AutoSize = true;
            this._marker1D18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D18.Location = new System.Drawing.Point(61, 116);
            this._marker1D18.Name = "_marker1D18";
            this._marker1D18.Size = new System.Drawing.Size(40, 13);
            this._marker1D18.TabIndex = 30;
            this._marker1D18.Text = "Д18 = ";
            // 
            // _marker1D17
            // 
            this._marker1D17.AutoSize = true;
            this._marker1D17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D17.Location = new System.Drawing.Point(61, 96);
            this._marker1D17.Name = "_marker1D17";
            this._marker1D17.Size = new System.Drawing.Size(40, 13);
            this._marker1D17.TabIndex = 29;
            this._marker1D17.Text = "Д17 = ";
            // 
            // _marker1D16
            // 
            this._marker1D16.AutoSize = true;
            this._marker1D16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D16.Location = new System.Drawing.Point(61, 76);
            this._marker1D16.Name = "_marker1D16";
            this._marker1D16.Size = new System.Drawing.Size(40, 13);
            this._marker1D16.TabIndex = 28;
            this._marker1D16.Text = "Д16 = ";
            // 
            // _marker1D15
            // 
            this._marker1D15.AutoSize = true;
            this._marker1D15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D15.Location = new System.Drawing.Point(61, 56);
            this._marker1D15.Name = "_marker1D15";
            this._marker1D15.Size = new System.Drawing.Size(40, 13);
            this._marker1D15.TabIndex = 27;
            this._marker1D15.Text = "Д15 = ";
            // 
            // _marker1D14
            // 
            this._marker1D14.AutoSize = true;
            this._marker1D14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D14.Location = new System.Drawing.Point(61, 36);
            this._marker1D14.Name = "_marker1D14";
            this._marker1D14.Size = new System.Drawing.Size(40, 13);
            this._marker1D14.TabIndex = 26;
            this._marker1D14.Text = "Д14 = ";
            // 
            // _marker1D13
            // 
            this._marker1D13.AutoSize = true;
            this._marker1D13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D13.Location = new System.Drawing.Point(61, 16);
            this._marker1D13.Name = "_marker1D13";
            this._marker1D13.Size = new System.Drawing.Size(40, 13);
            this._marker1D13.TabIndex = 25;
            this._marker1D13.Text = "Д13 = ";
            // 
            // _marker1D12
            // 
            this._marker1D12.AutoSize = true;
            this._marker1D12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D12.Location = new System.Drawing.Point(5, 236);
            this._marker1D12.Name = "_marker1D12";
            this._marker1D12.Size = new System.Drawing.Size(40, 13);
            this._marker1D12.TabIndex = 24;
            this._marker1D12.Text = "Д12 = ";
            // 
            // _marker1D8
            // 
            this._marker1D8.AutoSize = true;
            this._marker1D8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D8.Location = new System.Drawing.Point(6, 156);
            this._marker1D8.Name = "_marker1D8";
            this._marker1D8.Size = new System.Drawing.Size(34, 13);
            this._marker1D8.TabIndex = 23;
            this._marker1D8.Text = "Д8 = ";
            // 
            // _marker1D7
            // 
            this._marker1D7.AutoSize = true;
            this._marker1D7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D7.Location = new System.Drawing.Point(6, 136);
            this._marker1D7.Name = "_marker1D7";
            this._marker1D7.Size = new System.Drawing.Size(34, 13);
            this._marker1D7.TabIndex = 22;
            this._marker1D7.Text = "Д7 = ";
            // 
            // _marker1D6
            // 
            this._marker1D6.AutoSize = true;
            this._marker1D6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D6.Location = new System.Drawing.Point(6, 116);
            this._marker1D6.Name = "_marker1D6";
            this._marker1D6.Size = new System.Drawing.Size(34, 13);
            this._marker1D6.TabIndex = 21;
            this._marker1D6.Text = "Д6 = ";
            // 
            // _marker1D5
            // 
            this._marker1D5.AutoSize = true;
            this._marker1D5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D5.Location = new System.Drawing.Point(6, 96);
            this._marker1D5.Name = "_marker1D5";
            this._marker1D5.Size = new System.Drawing.Size(34, 13);
            this._marker1D5.TabIndex = 20;
            this._marker1D5.Text = "Д5 = ";
            // 
            // _marker1D4
            // 
            this._marker1D4.AutoSize = true;
            this._marker1D4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D4.Location = new System.Drawing.Point(6, 76);
            this._marker1D4.Name = "_marker1D4";
            this._marker1D4.Size = new System.Drawing.Size(34, 13);
            this._marker1D4.TabIndex = 19;
            this._marker1D4.Text = "Д4 = ";
            // 
            // _marker1D3
            // 
            this._marker1D3.AutoSize = true;
            this._marker1D3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D3.Location = new System.Drawing.Point(6, 56);
            this._marker1D3.Name = "_marker1D3";
            this._marker1D3.Size = new System.Drawing.Size(34, 13);
            this._marker1D3.TabIndex = 18;
            this._marker1D3.Text = "Д3 = ";
            // 
            // _marker1D2
            // 
            this._marker1D2.AutoSize = true;
            this._marker1D2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D2.Location = new System.Drawing.Point(6, 36);
            this._marker1D2.Name = "_marker1D2";
            this._marker1D2.Size = new System.Drawing.Size(34, 13);
            this._marker1D2.TabIndex = 17;
            this._marker1D2.Text = "Д2 = ";
            // 
            // _marker1D1
            // 
            this._marker1D1.AutoSize = true;
            this._marker1D1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D1.Location = new System.Drawing.Point(6, 16);
            this._marker1D1.Name = "_marker1D1";
            this._marker1D1.Size = new System.Drawing.Size(34, 13);
            this._marker1D1.TabIndex = 16;
            this._marker1D1.Text = "Д1 = ";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._marker1I16);
            this.groupBox8.Controls.Add(this._marker1I15);
            this.groupBox8.Controls.Add(this._marker1I14);
            this.groupBox8.Controls.Add(this._marker1I13);
            this.groupBox8.Controls.Add(this._marker1I12);
            this.groupBox8.Controls.Add(this._marker1I11);
            this.groupBox8.Controls.Add(this._marker1I10);
            this.groupBox8.Controls.Add(this._marker1I9);
            this.groupBox8.Controls.Add(this._marker1I8);
            this.groupBox8.Controls.Add(this._marker1I7);
            this.groupBox8.Controls.Add(this._marker1I6);
            this.groupBox8.Controls.Add(this._marker1I5);
            this.groupBox8.Controls.Add(this._marker1I4);
            this.groupBox8.Controls.Add(this._marker1I3);
            this.groupBox8.Controls.Add(this._marker1I2);
            this.groupBox8.Controls.Add(this._marker1I1);
            this.groupBox8.Location = new System.Drawing.Point(5, 12);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(119, 340);
            this.groupBox8.TabIndex = 40;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Токи присоед.";
            // 
            // _marker1I16
            // 
            this._marker1I16.AutoSize = true;
            this._marker1I16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I16.Location = new System.Drawing.Point(6, 318);
            this._marker1I16.Name = "_marker1I16";
            this._marker1I16.Size = new System.Drawing.Size(28, 13);
            this._marker1I16.TabIndex = 15;
            this._marker1I16.Text = "In = ";
            // 
            // _marker1I15
            // 
            this._marker1I15.AutoSize = true;
            this._marker1I15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I15.Location = new System.Drawing.Point(6, 298);
            this._marker1I15.Name = "_marker1I15";
            this._marker1I15.Size = new System.Drawing.Size(54, 13);
            this._marker1I15.TabIndex = 14;
            this._marker1I15.Text = "Ic Пр.5 = ";
            // 
            // _marker1I14
            // 
            this._marker1I14.AutoSize = true;
            this._marker1I14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I14.Location = new System.Drawing.Point(6, 278);
            this._marker1I14.Name = "_marker1I14";
            this._marker1I14.Size = new System.Drawing.Size(54, 13);
            this._marker1I14.TabIndex = 13;
            this._marker1I14.Text = "Ib Пр.5 = ";
            // 
            // _marker1I13
            // 
            this._marker1I13.AutoSize = true;
            this._marker1I13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I13.Location = new System.Drawing.Point(6, 258);
            this._marker1I13.Name = "_marker1I13";
            this._marker1I13.Size = new System.Drawing.Size(54, 13);
            this._marker1I13.TabIndex = 12;
            this._marker1I13.Text = "Ia Пр.5 = ";
            // 
            // _marker1I12
            // 
            this._marker1I12.AutoSize = true;
            this._marker1I12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I12.Location = new System.Drawing.Point(6, 238);
            this._marker1I12.Name = "_marker1I12";
            this._marker1I12.Size = new System.Drawing.Size(54, 13);
            this._marker1I12.TabIndex = 11;
            this._marker1I12.Text = "Ic Пр.4 = ";
            // 
            // _marker1I11
            // 
            this._marker1I11.AutoSize = true;
            this._marker1I11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I11.Location = new System.Drawing.Point(6, 218);
            this._marker1I11.Name = "_marker1I11";
            this._marker1I11.Size = new System.Drawing.Size(54, 13);
            this._marker1I11.TabIndex = 10;
            this._marker1I11.Text = "Ib Пр.4 = ";
            // 
            // _marker1I10
            // 
            this._marker1I10.AutoSize = true;
            this._marker1I10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I10.Location = new System.Drawing.Point(6, 198);
            this._marker1I10.Name = "_marker1I10";
            this._marker1I10.Size = new System.Drawing.Size(54, 13);
            this._marker1I10.TabIndex = 9;
            this._marker1I10.Text = "Ia Пр.4 = ";
            // 
            // _marker1I9
            // 
            this._marker1I9.AutoSize = true;
            this._marker1I9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I9.Location = new System.Drawing.Point(6, 178);
            this._marker1I9.Name = "_marker1I9";
            this._marker1I9.Size = new System.Drawing.Size(54, 13);
            this._marker1I9.TabIndex = 8;
            this._marker1I9.Text = "Ic Пр.3 = ";
            // 
            // _marker1I8
            // 
            this._marker1I8.AutoSize = true;
            this._marker1I8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I8.Location = new System.Drawing.Point(6, 158);
            this._marker1I8.Name = "_marker1I8";
            this._marker1I8.Size = new System.Drawing.Size(54, 13);
            this._marker1I8.TabIndex = 7;
            this._marker1I8.Text = "Ib Пр.3 = ";
            // 
            // _marker1I7
            // 
            this._marker1I7.AutoSize = true;
            this._marker1I7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I7.Location = new System.Drawing.Point(6, 138);
            this._marker1I7.Name = "_marker1I7";
            this._marker1I7.Size = new System.Drawing.Size(54, 13);
            this._marker1I7.TabIndex = 6;
            this._marker1I7.Text = "Ia Пр.3 = ";
            // 
            // _marker1I6
            // 
            this._marker1I6.AutoSize = true;
            this._marker1I6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I6.Location = new System.Drawing.Point(6, 118);
            this._marker1I6.Name = "_marker1I6";
            this._marker1I6.Size = new System.Drawing.Size(54, 13);
            this._marker1I6.TabIndex = 5;
            this._marker1I6.Text = "Ic Пр.2 = ";
            // 
            // _marker1I5
            // 
            this._marker1I5.AutoSize = true;
            this._marker1I5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I5.Location = new System.Drawing.Point(6, 98);
            this._marker1I5.Name = "_marker1I5";
            this._marker1I5.Size = new System.Drawing.Size(54, 13);
            this._marker1I5.TabIndex = 4;
            this._marker1I5.Text = "Ib Пр.2 = ";
            // 
            // _marker1I4
            // 
            this._marker1I4.AutoSize = true;
            this._marker1I4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I4.Location = new System.Drawing.Point(6, 78);
            this._marker1I4.Name = "_marker1I4";
            this._marker1I4.Size = new System.Drawing.Size(54, 13);
            this._marker1I4.TabIndex = 3;
            this._marker1I4.Text = "Ia Пр.2 = ";
            // 
            // _marker1I3
            // 
            this._marker1I3.AutoSize = true;
            this._marker1I3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I3.Location = new System.Drawing.Point(6, 58);
            this._marker1I3.Name = "_marker1I3";
            this._marker1I3.Size = new System.Drawing.Size(54, 13);
            this._marker1I3.TabIndex = 2;
            this._marker1I3.Text = "Ic Пр.1 = ";
            // 
            // _marker1I2
            // 
            this._marker1I2.AutoSize = true;
            this._marker1I2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I2.Location = new System.Drawing.Point(6, 38);
            this._marker1I2.Name = "_marker1I2";
            this._marker1I2.Size = new System.Drawing.Size(54, 13);
            this._marker1I2.TabIndex = 1;
            this._marker1I2.Text = "Ib Пр.1 = ";
            // 
            // _marker1I1
            // 
            this._marker1I1.AutoSize = true;
            this._marker1I1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I1.Location = new System.Drawing.Point(6, 18);
            this._marker1I1.Name = "_marker1I1";
            this._marker1I1.Size = new System.Drawing.Size(54, 13);
            this._marker1I1.TabIndex = 0;
            this._marker1I1.Text = "Ia Пр.1 = ";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this._deltaTimeBox);
            this.groupBox4.Controls.Add(this._marker2TimeBox);
            this.groupBox4.Controls.Add(this._marker1TimeBox);
            this.groupBox4.Location = new System.Drawing.Point(3, 744);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(814, 85);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Время";
            // 
            // _deltaTimeBox
            // 
            this._deltaTimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._deltaTimeBox.Controls.Add(this._markerTimeDelta);
            this._deltaTimeBox.Location = new System.Drawing.Point(6, 46);
            this._deltaTimeBox.Name = "_deltaTimeBox";
            this._deltaTimeBox.Size = new System.Drawing.Size(266, 33);
            this._deltaTimeBox.TabIndex = 2;
            this._deltaTimeBox.TabStop = false;
            this._deltaTimeBox.Text = "Дельта";
            // 
            // _markerTimeDelta
            // 
            this._markerTimeDelta.AutoSize = true;
            this._markerTimeDelta.Location = new System.Drawing.Point(118, 16);
            this._markerTimeDelta.Name = "_markerTimeDelta";
            this._markerTimeDelta.Size = new System.Drawing.Size(30, 13);
            this._markerTimeDelta.TabIndex = 2;
            this._markerTimeDelta.Text = "0 мс";
            // 
            // _marker2TimeBox
            // 
            this._marker2TimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._marker2TimeBox.Controls.Add(this._marker2Time);
            this._marker2TimeBox.Location = new System.Drawing.Point(144, 13);
            this._marker2TimeBox.Name = "_marker2TimeBox";
            this._marker2TimeBox.Size = new System.Drawing.Size(119, 33);
            this._marker2TimeBox.TabIndex = 1;
            this._marker2TimeBox.TabStop = false;
            this._marker2TimeBox.Text = "Маркер 2";
            // 
            // _marker2Time
            // 
            this._marker2Time.AutoSize = true;
            this._marker2Time.Location = new System.Drawing.Point(41, 16);
            this._marker2Time.Name = "_marker2Time";
            this._marker2Time.Size = new System.Drawing.Size(30, 13);
            this._marker2Time.TabIndex = 1;
            this._marker2Time.Text = "0 мс";
            // 
            // _marker1TimeBox
            // 
            this._marker1TimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._marker1TimeBox.Controls.Add(this._marker1Time);
            this._marker1TimeBox.Location = new System.Drawing.Point(6, 13);
            this._marker1TimeBox.Name = "_marker1TimeBox";
            this._marker1TimeBox.Size = new System.Drawing.Size(118, 33);
            this._marker1TimeBox.TabIndex = 0;
            this._marker1TimeBox.TabStop = false;
            this._marker1TimeBox.Text = "Маркер 1";
            // 
            // _marker1Time
            // 
            this._marker1Time.AutoSize = true;
            this._marker1Time.Location = new System.Drawing.Point(39, 16);
            this._marker1Time.Name = "_marker1Time";
            this._marker1Time.Size = new System.Drawing.Size(30, 13);
            this._marker1Time.TabIndex = 0;
            this._marker1Time.Text = "0 мс";
            // 
            // _xDecreaseButton
            // 
            this._xDecreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._xDecreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._xDecreaseButton.Location = new System.Drawing.Point(115, -1);
            this._xDecreaseButton.Name = "_xDecreaseButton";
            this._xDecreaseButton.Size = new System.Drawing.Size(33, 20);
            this._xDecreaseButton.TabIndex = 3;
            this._xDecreaseButton.Text = "X -";
            this._xDecreaseButton.UseVisualStyleBackColor = false;
            // 
            // _currentСonnectionsСheckBox
            // 
            this._currentСonnectionsСheckBox.AutoSize = true;
            this._currentСonnectionsСheckBox.BackColor = System.Drawing.Color.Silver;
            this._currentСonnectionsСheckBox.Checked = true;
            this._currentСonnectionsСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._currentСonnectionsСheckBox.Location = new System.Drawing.Point(154, 2);
            this._currentСonnectionsСheckBox.Name = "_currentСonnectionsСheckBox";
            this._currentСonnectionsСheckBox.Size = new System.Drawing.Size(99, 17);
            this._currentСonnectionsСheckBox.TabIndex = 4;
            this._currentСonnectionsСheckBox.Text = "Токи присоед.";
            this._currentСonnectionsСheckBox.UseVisualStyleBackColor = false;
            this._currentСonnectionsСheckBox.CheckedChanged += new System.EventHandler(this._currentСonnectionsСheckBox_CheckedChanged);
            // 
            // _discrestsСheckBox
            // 
            this._discrestsСheckBox.AutoSize = true;
            this._discrestsСheckBox.BackColor = System.Drawing.Color.Silver;
            this._discrestsСheckBox.Checked = true;
            this._discrestsСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._discrestsСheckBox.Location = new System.Drawing.Point(259, 2);
            this._discrestsСheckBox.Name = "_discrestsСheckBox";
            this._discrestsСheckBox.Size = new System.Drawing.Size(78, 17);
            this._discrestsСheckBox.TabIndex = 5;
            this._discrestsСheckBox.Text = "Дискреты";
            this._discrestsСheckBox.UseVisualStyleBackColor = false;
            this._discrestsСheckBox.CheckedChanged += new System.EventHandler(this._discrestsСheckBox_CheckedChanged);
            // 
            // _channelsСheckBox
            // 
            this._channelsСheckBox.AutoSize = true;
            this._channelsСheckBox.BackColor = System.Drawing.Color.Silver;
            this._channelsСheckBox.Checked = true;
            this._channelsСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._channelsСheckBox.Location = new System.Drawing.Point(343, 2);
            this._channelsСheckBox.Name = "_channelsСheckBox";
            this._channelsСheckBox.Size = new System.Drawing.Size(65, 17);
            this._channelsСheckBox.TabIndex = 6;
            this._channelsСheckBox.Text = "Каналы";
            this._channelsСheckBox.UseVisualStyleBackColor = false;
            this._channelsСheckBox.CheckedChanged += new System.EventHandler(this._channelsСheckBox_CheckedChanged);
            // 
            // _markCheckBox
            // 
            this._markCheckBox.AutoSize = true;
            this._markCheckBox.BackColor = System.Drawing.Color.Silver;
            this._markCheckBox.Location = new System.Drawing.Point(547, 2);
            this._markCheckBox.Name = "_markCheckBox";
            this._markCheckBox.Size = new System.Drawing.Size(58, 17);
            this._markCheckBox.TabIndex = 7;
            this._markCheckBox.Text = "Метки";
            this._markCheckBox.UseVisualStyleBackColor = false;
            // 
            // _markerCheckBox
            // 
            this._markerCheckBox.AutoSize = true;
            this._markerCheckBox.BackColor = System.Drawing.Color.Silver;
            this._markerCheckBox.Location = new System.Drawing.Point(611, 2);
            this._markerCheckBox.Name = "_markerCheckBox";
            this._markerCheckBox.Size = new System.Drawing.Size(73, 17);
            this._markerCheckBox.TabIndex = 8;
            this._markerCheckBox.Text = "Маркеры";
            this._markerCheckBox.UseVisualStyleBackColor = false;
            this._markerCheckBox.CheckedChanged += new System.EventHandler(this._markerCheckBox_CheckedChanged);
            // 
            // _xIncreaseButton
            // 
            this._xIncreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._xIncreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._xIncreaseButton.Location = new System.Drawing.Point(75, -1);
            this._xIncreaseButton.Name = "_xIncreaseButton";
            this._xIncreaseButton.Size = new System.Drawing.Size(33, 20);
            this._xIncreaseButton.TabIndex = 2;
            this._xIncreaseButton.Text = "X +";
            this._xIncreaseButton.UseVisualStyleBackColor = false;
            // 
            // _oscRunСheckBox
            // 
            this._oscRunСheckBox.AutoSize = true;
            this._oscRunСheckBox.BackColor = System.Drawing.Color.Silver;
            this._oscRunСheckBox.Location = new System.Drawing.Point(414, 2);
            this._oscRunСheckBox.Name = "_oscRunСheckBox";
            this._oscRunСheckBox.Size = new System.Drawing.Size(127, 17);
            this._oscRunСheckBox.TabIndex = 9;
            this._oscRunСheckBox.Text = "Пуск осциллографа";
            this._oscRunСheckBox.UseVisualStyleBackColor = false;
            this._oscRunСheckBox.CheckedChanged += new System.EventHandler(this._oscRunСheckBox_CheckedChanged);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.LightGray;
            this.panel4.Controls.Add(this._oscRunСheckBox);
            this.panel4.Controls.Add(this._xIncreaseButton);
            this.panel4.Controls.Add(this._markerCheckBox);
            this.panel4.Controls.Add(this._xDecreaseButton);
            this.panel4.Controls.Add(this._markCheckBox);
            this.panel4.Controls.Add(this._currentСonnectionsСheckBox);
            this.panel4.Controls.Add(this._channelsСheckBox);
            this.panel4.Controls.Add(this._discrestsСheckBox);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1078, 19);
            this.panel4.TabIndex = 34;
            // 
            // Mr902OscilloscopeResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 753);
            this.Controls.Add(this.MAINTABLE);
            this.Name = "Mr902OscilloscopeResultForm";
            this.Text = "Mr901OscilloscopeResultForm";
            this.MAINTABLE.ResumeLayout(false);
            this.MAINPANEL.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.MarkersTable.ResumeLayout(false);
            this.MarkersTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._marker1TrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._marker2TrackBar)).EndInit();
            this.panel3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this._markerScrollPanel.ResumeLayout(false);
            this._marker2Box.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this._marker1Box.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this._deltaTimeBox.ResumeLayout(false);
            this._deltaTimeBox.PerformLayout();
            this._marker2TimeBox.ResumeLayout(false);
            this._marker2TimeBox.PerformLayout();
            this._marker1TimeBox.ResumeLayout(false);
            this._marker1TimeBox.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel MAINTABLE;
        private System.Windows.Forms.HScrollBar hScrollBar4;
        private System.Windows.Forms.Panel MAINPANEL;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button _channel1Button;
        private System.Windows.Forms.Button _channel2Button;
        private System.Windows.Forms.Button _channel3Button;
        private System.Windows.Forms.Button _channel4Button;
        private System.Windows.Forms.Button _channel5Button;
        private System.Windows.Forms.Button _channel6Button;
        private System.Windows.Forms.Button _channel7Button;
        private System.Windows.Forms.Button _channel8Button;
        private BEMN_XY_Chart.DAS_Net_XYChart _channelsChart;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private BEMN_XY_Chart.DAS_Net_XYChart _discrestsChart;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.VScrollBar _s1Scroll;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button _currentСonnectionsChartDecreaseButton;
        private System.Windows.Forms.Button _currentСonnectionsChartIncreaseButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button _i3Button;
        private System.Windows.Forms.Button _i4Button;
        private System.Windows.Forms.Button _i1Button;
        private System.Windows.Forms.Button _i2Button;
        private System.Windows.Forms.Label label5;
        private BEMN_XY_Chart.DAS_Net_XYChart _currentСonnectionsChart;
        private System.Windows.Forms.TableLayoutPanel MarkersTable;
        private System.Windows.Forms.TrackBar _marker1TrackBar;
        private System.Windows.Forms.TrackBar _marker2TrackBar;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox _deltaTimeBox;
        private System.Windows.Forms.GroupBox _marker2TimeBox;
        private System.Windows.Forms.GroupBox _marker1TimeBox;
        private System.Windows.Forms.GroupBox _marker1Box;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button _discrete5Button;
        private System.Windows.Forms.Button _discrete6Button;
        private System.Windows.Forms.Button _discrete7Button;
        private System.Windows.Forms.Button _discrete8Button;
        private System.Windows.Forms.Button _discrete9Button;
        private System.Windows.Forms.Button _discrete10Button;
        private System.Windows.Forms.Button _discrete11Button;
        private System.Windows.Forms.Button _discrete12Button;
        private System.Windows.Forms.Button _discrete13Button;
        private System.Windows.Forms.Button _discrete14Button;
        private System.Windows.Forms.Button _discrete15Button;
        private System.Windows.Forms.Button _discrete16Button;
        private System.Windows.Forms.Button _discrete17Button;
        private System.Windows.Forms.Button _discrete18Button;
        private System.Windows.Forms.Button _discrete19Button;
        private System.Windows.Forms.Button _discrete20Button;
        private System.Windows.Forms.Button _discrete21Button;
        private System.Windows.Forms.Button _discrete22Button;
        private System.Windows.Forms.Button _discrete23Button;
        private System.Windows.Forms.Button _discrete24Button;
        private System.Windows.Forms.Button _discrete1Button;
        private System.Windows.Forms.Button _discrete2Button;
        private System.Windows.Forms.Button _discrete3Button;
        private System.Windows.Forms.Button _discrete4Button;
        private System.Windows.Forms.Button _i5Button;
        private System.Windows.Forms.Button _i6Button;
        private System.Windows.Forms.Button _i7Button;
        private System.Windows.Forms.Button _i8Button;
        private System.Windows.Forms.Button _i9Button;
        private System.Windows.Forms.Button _i10Button;
        private System.Windows.Forms.Button _i11Button;
        private System.Windows.Forms.Button _i12Button;
        private System.Windows.Forms.Button _i13Button;
        private System.Windows.Forms.Button _i14Button;
        private System.Windows.Forms.Button _i15Button;
        private System.Windows.Forms.Button _i16Button;
        private System.Windows.Forms.Button _xDecreaseButton;
        private System.Windows.Forms.CheckBox _currentСonnectionsСheckBox;
        private System.Windows.Forms.CheckBox _discrestsСheckBox;
        private System.Windows.Forms.CheckBox _channelsСheckBox;
        private System.Windows.Forms.CheckBox _markCheckBox;
        private System.Windows.Forms.CheckBox _markerCheckBox;
        private System.Windows.Forms.ToolTip _newMarkToolTip;
        private System.Windows.Forms.Label _marker1I16;
        private System.Windows.Forms.Label _marker1I15;
        private System.Windows.Forms.Label _marker1I14;
        private System.Windows.Forms.Label _marker1I13;
        private System.Windows.Forms.Label _marker1I12;
        private System.Windows.Forms.Label _marker1I11;
        private System.Windows.Forms.Label _marker1I10;
        private System.Windows.Forms.Label _marker1I9;
        private System.Windows.Forms.Label _marker1I8;
        private System.Windows.Forms.Label _marker1I7;
        private System.Windows.Forms.Label _marker1I6;
        private System.Windows.Forms.Label _marker1I5;
        private System.Windows.Forms.Label _marker1I4;
        private System.Windows.Forms.Label _marker1I3;
        private System.Windows.Forms.Label _marker1I2;
        private System.Windows.Forms.Label _marker1I1;
        private System.Windows.Forms.Label _marker1D22;
        private System.Windows.Forms.Label _marker1D21;
        private System.Windows.Forms.Label _marker1D20;
        private System.Windows.Forms.Label _marker1D11;
        private System.Windows.Forms.Label _marker1D10;
        private System.Windows.Forms.Label _marker1D9;
        private System.Windows.Forms.Label _marker1D19;
        private System.Windows.Forms.Label _marker1D18;
        private System.Windows.Forms.Label _marker1D17;
        private System.Windows.Forms.Label _marker1D16;
        private System.Windows.Forms.Label _marker1D15;
        private System.Windows.Forms.Label _marker1D14;
        private System.Windows.Forms.Label _marker1D13;
        private System.Windows.Forms.Label _marker1D12;
        private System.Windows.Forms.Label _marker1D8;
        private System.Windows.Forms.Label _marker1D7;
        private System.Windows.Forms.Label _marker1D6;
        private System.Windows.Forms.Label _marker1D5;
        private System.Windows.Forms.Label _marker1D4;
        private System.Windows.Forms.Label _marker1D3;
        private System.Windows.Forms.Label _marker1D2;
        private System.Windows.Forms.Label _marker1D1;
        private System.Windows.Forms.Label _marker1D24;
        private System.Windows.Forms.Label _marker1D23;
        private System.Windows.Forms.Label _marker1K8;
        private System.Windows.Forms.Label _marker1K7;
        private System.Windows.Forms.Label _marker1K6;
        private System.Windows.Forms.Label _marker1K5;
        private System.Windows.Forms.Label _marker1K4;
        private System.Windows.Forms.Label _marker1K3;
        private System.Windows.Forms.Label _marker1K2;
        private System.Windows.Forms.Label _marker1K1;
        private System.Windows.Forms.Panel _markerScrollPanel;
        private System.Windows.Forms.GroupBox _marker2Box;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label _marker2K8;
        private System.Windows.Forms.Label _marker2K7;
        private System.Windows.Forms.Label _marker2K6;
        private System.Windows.Forms.Label _marker2K5;
        private System.Windows.Forms.Label _marker2K4;
        private System.Windows.Forms.Label _marker2K3;
        private System.Windows.Forms.Label _marker2K2;
        private System.Windows.Forms.Label _marker2K1;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label _marker2D24;
        private System.Windows.Forms.Label _marker2D23;
        private System.Windows.Forms.Label _marker2D22;
        private System.Windows.Forms.Label _marker2D21;
        private System.Windows.Forms.Label _marker2D20;
        private System.Windows.Forms.Label _marker2D11;
        private System.Windows.Forms.Label _marker2D10;
        private System.Windows.Forms.Label _marker2D9;
        private System.Windows.Forms.Label _marker2D19;
        private System.Windows.Forms.Label _marker2D18;
        private System.Windows.Forms.Label _marker2D17;
        private System.Windows.Forms.Label _marker2D16;
        private System.Windows.Forms.Label _marker2D15;
        private System.Windows.Forms.Label _marker2D14;
        private System.Windows.Forms.Label _marker2D13;
        private System.Windows.Forms.Label _marker2D12;
        private System.Windows.Forms.Label _marker2D8;
        private System.Windows.Forms.Label _marker2D7;
        private System.Windows.Forms.Label _marker2D6;
        private System.Windows.Forms.Label _marker2D5;
        private System.Windows.Forms.Label _marker2D4;
        private System.Windows.Forms.Label _marker2D3;
        private System.Windows.Forms.Label _marker2D2;
        private System.Windows.Forms.Label _marker2D1;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label _marker2I16;
        private System.Windows.Forms.Label _marker2I15;
        private System.Windows.Forms.Label _marker2I14;
        private System.Windows.Forms.Label _marker2I13;
        private System.Windows.Forms.Label _marker2I12;
        private System.Windows.Forms.Label _marker2I11;
        private System.Windows.Forms.Label _marker2I10;
        private System.Windows.Forms.Label _marker2I9;
        private System.Windows.Forms.Label _marker2I8;
        private System.Windows.Forms.Label _marker2I7;
        private System.Windows.Forms.Label _marker2I6;
        private System.Windows.Forms.Label _marker2I5;
        private System.Windows.Forms.Label _marker2I4;
        private System.Windows.Forms.Label _marker2I3;
        private System.Windows.Forms.Label _marker2I2;
        private System.Windows.Forms.Label _marker2I1;
        private System.Windows.Forms.Button _xIncreaseButton;
        private System.Windows.Forms.Label _markerTimeDelta;
        private System.Windows.Forms.Label _marker2Time;
        private System.Windows.Forms.Label _marker1Time;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox _oscRunСheckBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Label _label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel4;



    }
}