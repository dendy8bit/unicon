﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR902.Old.Configuration.Structures.Primary;
using BEMN.MR902.Old.HelpClasses;
using BEMN.MR902.Old.Structures.JournalStructures;
using BEMN.MR902.Properties;

namespace BEMN.MR902.Old.Forms
{
    public partial class Mr902AlarmJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Аварий в журнале - {0}";
        private const string READ_AJ_FAIL = "Невозможно прочитать журнал аварий";
        private const string READ_AJ = "Чтение журнала аварий";
        private const string ALARM_JOURNAL = "Журнал аварий";
        private const string TABLE_NAME = "МР902_журнал_аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";

        #endregion [Constants]


        #region [Private fields]
        private readonly MemoryEntity<JournalRefreshStruct> _refreshAlarmJournal;
        private readonly MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        private readonly CurrentOptionsLoader _currentOptionsLoader;
        private DataTable _table;
        private int _recordNumber;
        private Mr902 _device;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr902AlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public Mr902AlarmJournalForm(Mr902 device)
        {
            this.InitializeComponent();
            this._device = device;
            
            this._alarmJournal = device.Keeper.AlarmJournal;
            this._refreshAlarmJournal = device.Keeper.RefreshAlarmJournal;
            this._currentOptionsLoader = device.Keeper.CurrentOptionsLoader;
            this._currentOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, this._refreshAlarmJournal.SaveStruct);
            this._currentOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadJournal);

            this._refreshAlarmJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.StartReadJournal);
            this._refreshAlarmJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
            this._alarmJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);
            this._alarmJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
        } 
        #endregion [Ctor's]


        #region [Help members]
        private void FailReadJournal()
        {
            this._statusLabel.Text = READ_AJ_FAIL;
        }

        private void StartReadJournal()
        {
            this._recordNumber = 0;
            this._table.Clear();
            this._alarmJournalGrid.DataSource = this._table;
            this._alarmJournal.LoadStructCycleWhile(a => a.IsEmpty);
        }

        private void ReadRecord()
        {
            try
            {
                if (!this._alarmJournal.Value.IsEmpty)
                {
                    this._recordNumber++;
                    AlarmJournalRecordStruct.Number = this._recordNumber;
                    this._table.Rows.Add(this._alarmJournal.Value.GetRecord);
                    this._alarmJournalGrid.DataSource = this._table;
                    this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                }
            }
            catch
            {
            }
            if (this._recordNumber == 0)
            {
                this._statusLabel.Text = "Журнал пуст";
            }
        } 

         private DataTable GetJournalDataTable()
         {
             var table = new DataTable(TABLE_NAME);
             for (int j = 0; j < this._alarmJournalGrid.Columns.Count; j++)
             {
                 table.Columns.Add(this._alarmJournalGrid.Columns[j].Name);
             }
             return table;
         }
        #endregion [Help members]


        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(Mr902); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr902AlarmJournalForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return ALARM_JOURNAL; }
        }
        #endregion [IFormView Members]


        #region [Event Handlers]
        private void Mr902AlarmJournalForm_Load(object sender, EventArgs e)
        {
            this._table = this.GetJournalDataTable();
            this._alarmJournalGrid.DataSource = this._table;
            this.StartRead();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._table.Rows.Clear();
            this._statusLabel.Text = READ_AJ;
            this._currentOptionsLoader.StartRead();
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this._openAlarmJournalDialog.ShowDialog() == DialogResult.OK)
                {
                    this._table.Clear();
                    if (Path.GetExtension(this._openAlarmJournalDialog.FileName).ToLower().Contains("bin"))
                    {
                        byte[] file = File.ReadAllBytes(this._openAlarmJournalDialog.FileName);
                        int connectionSize = AllConnectionsStruct.CONNECTIONS_MR902 * 2;
                        CurrentConverter.Factors = Common.TOWORDS(file.Skip(file.Length - connectionSize).ToArray(), true);
                        AlarmJournalRecordStruct journal = new AlarmJournalRecordStruct();
                        int size = journal.GetSize();
                        int index = 0;
                        this._recordNumber = 0;
                        while (index < file.Length - connectionSize)
                        {
                            List<byte> journalBytes = new List<byte>();
                            journalBytes.AddRange(Common.SwapListItems(file.Skip(index).Take(size)));
                            journal.InitStruct(journalBytes.ToArray());
                            this._alarmJournal.Value = journal;
                            this.ReadRecord();
                            index += size;
                        }
                    }
                    else
                    {
                        string s = File.ReadAllText(this._openAlarmJournalDialog.FileName); // для поддержки старых версий файлов
                        s = s.Replace("МР901", "МР902");                                    // заглушка
                        this._table.Clear();
                        this._table.ReadXml(new StringReader(s));
                        this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._table.Rows.Count);
                        this._alarmJournalGrid.Refresh();
                    }
                }
            }
            catch
            {
                MessageBox.Show("Невозможно открыть файл", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
            }
        }

        private void _saveToHTMLBtn_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            if (this._saveAlarmHTMLdialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    HtmlExport.Export(this._table, this._saveAlarmHTMLdialog.FileName, Resources.MR902AJ);
                    this._statusLabel.Text = "Журнал успешно сохранен.";

                    /*
                    string xml;

                    using (StringWriter writer = new StringWriter())
                    {
                        this._table.WriteXml(writer);
                        xml = writer.ToString();
                    }

                    HtmlExport.Export(this._saveAlarmHTMLdialog.FileName, "МР902. Журнал аварий", xml);
                    this._statusLabel.Text = "Журнал успешно сохранен.";
                    */
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }
        #endregion [Event Handlers]
    }
}
