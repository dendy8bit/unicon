﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR902.Old.Structures.MeasuringStructures
{
    /// <summary>
    /// МР 902 Аналоговая база данных
    /// </summary>
    public class AnalogDataBaseStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _i1; // ток 1
        [Layout(1)] private ushort _i2; // ток 2
        [Layout(2)] private ushort _i3; // ток 3
        [Layout(3)] private ushort _i4; // ток 4
        [Layout(4)] private ushort _i5; // ток 5
        [Layout(5)] private ushort _i6; // ток 6
        [Layout(6)] private ushort _i7; // ток 7
        [Layout(7)] private ushort _i8; // ток 8
        [Layout(8)] private ushort _i9; // ток 9
        [Layout(9)] private ushort _i10; // ток 10
        [Layout(10)] private ushort _i11; // ток 11
        [Layout(11)] private ushort _i12; // ток 12
        [Layout(12)] private ushort _i13; // ток 13
        [Layout(13)] private ushort _i14; // ток 14
        [Layout(14)] private ushort _i15; // ток 15
        [Layout(15)] private ushort _i16; // ток 16

        [Layout(16)] private ushort _ida1; // ток СШ1
        [Layout(17)] private ushort _ida2; // ток СШ2
        [Layout(18)] private ushort _ida3; // ток ПО
        
        [Layout(19)] private ushort _ita1; // ток СШ1
        [Layout(20)] private ushort _ita2; // ток СШ2
        [Layout(21)] private ushort _ita3; // ток ПО

        [Layout(22)] private ushort _idb1; // ток СШ1
        [Layout(23)] private ushort _idb2; // ток СШ2
        [Layout(24)] private ushort _idb3; // ток ПО

        [Layout(25)] private ushort _itb1; // ток СШ1
        [Layout(26)] private ushort _itb2; // ток СШ2  
        [Layout(27)] private ushort _itb3; // ток ПО

        [Layout(28)] private ushort _idc1; // ток СШ1
        [Layout(29)] private ushort _idc2; // ток СШ2
        [Layout(30)] private ushort _idc3; // ток ПО
        
        [Layout(31)] private ushort _itc1; // ток СШ1
        [Layout(32)] private ushort _itc2; // ток СШ2
        [Layout(33)] private ushort _itc3; // ток ПО


        #endregion [Private fields]


        #region [Properties]

        #region Тормозные и дифференциальные токи
        public string GetIda1(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._ida1);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIdb1(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._idb1);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIdc1(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._idc1);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIta1(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._ita1);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetItb1(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._itb1);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetItc1(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._itc1);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIda2(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._ida2);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIdb2(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._idb2);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIdc2(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._idc2);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIta2(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._ita2);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetItb2(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._itb2);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetItc2(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._itc2);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIda3(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._ida3);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIdb3(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._idb3);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIdc3(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._idc3);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIta3(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._ita3);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetItb3(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._itb3);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetItc3(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._itc3);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }
        #endregion

        #region Токи присоединений
        public string GetIa1(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._i1);
            return ValuesConverterCommon.Analog.GetI(value, factors[1]);
        }

        public string GetIb1(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._i2);
            return ValuesConverterCommon.Analog.GetI(value, factors[1]);
        }

        public string GetIc1(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._i3);
            return ValuesConverterCommon.Analog.GetI(value, factors[1]);
        }

        public string GetIa2(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._i4);
            return ValuesConverterCommon.Analog.GetI(value, factors[2]);
        }

        public string GetIb2(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._i5);
            return ValuesConverterCommon.Analog.GetI(value, factors[2]);
        }

        public string GetIc2(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._i6);
            return ValuesConverterCommon.Analog.GetI(value, factors[2]);
        }

        public string GetIa3(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._i7);
            return ValuesConverterCommon.Analog.GetI(value, factors[3]);
        }

        public string GetIb3(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._i8);
            return ValuesConverterCommon.Analog.GetI(value, factors[3]);
        }

        public string GetIc3(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._i9);
            return ValuesConverterCommon.Analog.GetI(value, factors[3]);
        }

        public string GetIa4(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._i10);
            return ValuesConverterCommon.Analog.GetI(value, factors[4]);
        }

        public string GetIb4(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._i11);
            return ValuesConverterCommon.Analog.GetI(value, factors[4]);
        }

        public string GetIc4(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._i12);
            return ValuesConverterCommon.Analog.GetI(value, factors[4]);
        }

        public string GetIa5(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._i13);
            return ValuesConverterCommon.Analog.GetI(value, factors[5]);
        }

        public string GetIb5(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._i14);
            return ValuesConverterCommon.Analog.GetI(value, factors[5]);
        }

        public string GetIc5(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._i15);
            return ValuesConverterCommon.Analog.GetI(value, factors[5]);
        }

        public string GetIn(List<AnalogDataBaseStruct> list, int[] factors)
        {
            var value = this.GetMean(list, o => o._i16);
            return ValuesConverterCommon.Analog.GetI(value, factors[6]);
        }
        #endregion

        private ushort GetMean(List<AnalogDataBaseStruct> list, Func<AnalogDataBaseStruct, ushort> func)
        {
            var count = list.Count;

            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (ushort)(sum / (double)count);
        }

        #endregion [Properties]
    }
}
