﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR902.Old.Structures.MeasuringStructures
{
    /// <summary>
    /// МР 901 Дискретная база данных
    /// </summary>
    public class DiscretDataBaseStruct : StructBase
    {
        #region [Constants]

        private const int BASE_SIZE = 18;
        private const int ALARM_SIZE = 16;
        private const int PARAM_SIZE = 16;

        #endregion [Constants]


        #region [Private fields]

        [Layout(0, Count = BASE_SIZE)] private ushort[] _base; //бд общаяя
        [Layout(1, Count = ALARM_SIZE)] private ushort[] _alarm; //БД неисправностей
        [Layout(2, Count = PARAM_SIZE)] private ushort[] _param; //БД параметров

        #endregion [Private fields]

        #region [Properties]

        /// <summary>
        /// Дискретные входы
        /// </summary>
        public bool[] DiscretInputs
        {
            get { return Common.GetBitsArray(this._base, 0, 23); }
        }

        /// <summary>
        /// Входные ЛС
        /// </summary>
        public bool[] InputsLogicSignals
        {
            get { return Common.GetBitsArray(this._base, 24, 39); }
        }

        /// <summary>
        /// Выходные ЛС
        /// </summary>
        public bool[] OutputLogicSignals
        {
            get { return Common.GetBitsArray(this._base, 40, 55); }
        }

        /// <summary>
        /// Дествующий ток, диф. защиты
        /// </summary>
        public bool[] OperatingCurrent
        {
            get { return Common.GetBitsArray(this._base, 56, 70); }
        }

        /// <summary>
        /// Максимальный ток
        /// </summary>
        public bool[] MaximumCurrent
        {
            get { return Common.GetBitsArray(this._base, 71, 134); }
        }

        /// <summary>
        /// Внешние защиты
        /// </summary>
        public bool[] ExternalDefenses
        {
            get { return Common.GetBitsArray(this._base, 135, 150); }
        }

        /// <summary>
        /// Свободная логика
        /// </summary>
        public bool[] FreeLogic
        {
            get { return Common.GetBitsArray(this._base, 151, 182); }
        }

        /// <summary>
        /// УРОВ
        /// </summary>
        public bool[] Urov
        {
            get { return Common.GetBitsArray(this._base, 186, 204); }
        }

        /// <summary>
        /// Состояния
        /// </summary>
        public bool[] State
        {
            get { return Common.GetBitsArray(this._base, 221, 223); }
        }

        /// <summary>
        /// Реле
        /// </summary>
        public bool[] Relays
        {
            get { return Common.GetBitsArray(this._base, 227, 244); }
        }

        /// <summary>
        /// Индикаторы
        /// </summary>
        public bool[] Indicators
        {
            get { return Common.GetBitsArray(this._base, 245, 256); }
        }

        /// <summary>
        /// Индикаторы
        /// </summary>
        public bool[] ControlSignals
        {
            get { return Common.GetBitsArray(this._base, 259, 266); }
        }

        /// <summary>
        /// Реле
        /// </summary>
        public bool[] Relaysv203
        {
            get { return Common.GetBitsArray(this._base, 229, 246); }
        }

        /// <summary>
        /// Индикаторы
        /// </summary>
        public bool[] IndicatorsV203
        {
            get { return Common.GetBitsArray(this._base, 247, 258); }
        }

        /// <summary>
        /// Индикаторы
        /// </summary>
        public bool[] ControlSignalsV203
        {
            get { return Common.GetBitsArray(this._base, 261, 268); }
        }

        /// <summary>
        /// Неисправности
        /// </summary>
        public bool[] Faults
        {
            get
            {
                return new[]
                {
                    Common.GetBit(this._alarm[0], 0),  //аппаратная
                    Common.GetBit(this._alarm[0], 1),  //программная
                    Common.GetBit(this._alarm[0], 2),  //измерения ТТ
                    Common.GetBit(this._alarm[0], 3),  //уров
                    Common.GetBit(this._alarm[0], 4),  //модуль 1
                    Common.GetBit(this._alarm[0], 5),  //модуль 2
                    Common.GetBit(this._alarm[0], 6),  //модуль 3
                    Common.GetBit(this._alarm[0], 7),  //модуль 4

                    Common.GetBit(this._alarm[0], 8),  //модуль 5
                    Common.GetBit(this._alarm[0], 9),  //уставок
                    Common.GetBit(this._alarm[0], 10), //группы уставок
                    Common.GetBit(this._alarm[0], 12), //журнала системы
                    Common.GetBit(this._alarm[0], 13), //журнала аварий
                    Common.GetBit(this._alarm[0], 14), //осциллографа
                    this.FaultLogic,
                    Common.GetBit(this._alarm[1], 4),  //неиспр. СШ1
                    Common.GetBit(this._alarm[1], 5),  //неиспр. СШ2
                    Common.GetBit(this._alarm[1], 6)   //неиспр. ПО
                };
            }
        }

        /// <summary>
        /// Неисправность логики
        /// </summary>
        public bool FaultLogic
        {
            get
            {
                return Common.GetBit(this._alarm[0], 15) | //ошибка CRC констант программы логики
                       Common.GetBit(this._alarm[1], 0) | //ошибка CRC разрешения программы логики
                       Common.GetBit(this._alarm[1], 1) | //ошибка CRC программы логики
                       Common.GetBit(this._alarm[1], 2) | //ошибка CRC меню логики
                       Common.GetBit(this._alarm[1], 3); //ошибка в ходе выполнения программы логики
            }
        }


        #endregion [Properties]
    }
}
