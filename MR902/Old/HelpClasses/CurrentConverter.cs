﻿using System.Linq;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR902.Old.HelpClasses
{
    public static class CurrentConverter
    {
        /// <summary>
        /// Коэффициенты привидения
        /// </summary>
        private static ushort[] _factor;
        private static int[] _factorsInt;
        private static int _maxFactor;

        public static ushort[] Factors
        {
            set
            {
                _factor = value;
                _factorsInt = new int[_factor.Length];
                for (int i = 0; i < _factor.Length; i++)
                {
                    _factorsInt[i] = 40*_factor[i];       // коэффициент b
                }
                _maxFactor = _factorsInt.Max();
            }
            get { return _factor; }
        }
        /// <summary>
        /// Рассчитывает ток
        /// </summary>
        /// <param name="current">Полученное значение</param>
        /// <param name="number">Номер(0 для общих)</param>
        /// <returns></returns>
        public static string GetAmperage(ushort current, ushort number)
        {
            int factor = number == 0 ? _maxFactor : _factorsInt[number - 1];
            return ValuesConverterCommon.Analog.GetI(current, factor);
        }
    }
}
