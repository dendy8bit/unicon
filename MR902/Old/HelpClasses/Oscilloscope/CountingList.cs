using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BEMN.MBServer;
using BEMN.MR902.Old.Configuration.Structures.Primary;
using BEMN.MR902.Old.Structures.JournalStructures;

namespace BEMN.MR902.Old.HelpClasses.Oscilloscope
{
    /// <summary>
    /// ��������� �������� ������������
    /// </summary>
    public class CountingList
    {
        #region [Constants]
        /// <summary>
        /// ������ ������ �������(� ������)
        /// </summary>
        private const int COUNTING_SIZE = 18;

        private const int CURRENTS_COUNT = 16;
        private const int DISCRETS_COUNT = 24;
        private const int CHANNELS_COUNT = 8;

        #endregion [Constants]
        
        #region [Private fields]
        /// <summary>
        /// ������ �������� �������� �� �����
        /// </summary>
        private readonly ushort[][] _countingArray;
        /// <summary>
        /// ������ ������� 0-15
        /// </summary>
        private ushort[][] _discrets0to15;
        /// <summary>
        /// ������ ������� 0-7,������� 16-23
        /// </summary>
        private ushort[][] _discrets16to23;
        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        private int _count;

        /// <summary>
        /// ����
        /// </summary>
        private double[][] _currents;
        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        private int _alarm;
        /// <summary>
        /// ����������� ���
        /// </summary>
        private double _minI=0;
        /// <summary>
        /// ������������ ���
        /// </summary>
        private double _maxI = 0;

        private OscJournalStruct _oscJournalStruct;

        private ushort[] _ittn;

        private short[][] _baseCurrents;
        #endregion [Private fields]

        /// <summary>
        /// ������
        /// </summary>
        public ushort[][] Channels
        {
            get
            {
                return new[]
                    {
                        this._discrets0to15[15],
                        this._discrets0to15[14],
                        this._discrets0to15[13],
                        this._discrets0to15[12],
                        this._discrets0to15[11],
                        this._discrets0to15[10],
                        this._discrets0to15[9],
                        this._discrets0to15[8]
                    };
            }
            set
            {
                this._discrets0to15[15] = value[0];
                this._discrets0to15[14] = value[1];
                this._discrets0to15[13] = value[2];
                this._discrets0to15[12] = value[3];
                this._discrets0to15[11] = value[4];
                this._discrets0to15[10] = value[5];
                this._discrets0to15[9] = value[6];
                this._discrets0to15[8] = value[7];
            }
        }

        /// <summary>
        /// ��������
        /// </summary>
        public ushort[][] Discrets
        {
            get
            {
                return new[]
                    {
                        this._discrets0to15[0],
                        this._discrets0to15[1],
                        this._discrets0to15[2],
                        this._discrets0to15[3],
                        this._discrets0to15[4],
                        this._discrets0to15[5],
                        this._discrets0to15[6],
                        this._discrets0to15[7],
                        this._discrets16to23[0],
                        this._discrets16to23[1],
                        this._discrets16to23[2],
                        this._discrets16to23[3],
                        this._discrets16to23[4],
                        this._discrets16to23[5],
                        this._discrets16to23[6],
                        this._discrets16to23[7],
                        this._discrets16to23[8],
                        this._discrets16to23[9],
                        this._discrets16to23[10],
                        this._discrets16to23[11],
                        this._discrets16to23[12],
                        this._discrets16to23[13],
                        this._discrets16to23[14],
                        this._discrets16to23[15]

                    };
            }
            set
            {
                this._discrets0to15[0] = value[0];
                this._discrets0to15[1] = value[1];
                this._discrets0to15[2] = value[2];
                this._discrets0to15[3] = value[3];
                this._discrets0to15[4] = value[4];
                this._discrets0to15[5] = value[5];
                this._discrets0to15[6] = value[6];
                this._discrets0to15[7] = value[7];
                this._discrets16to23[0] = value[8];
                this._discrets16to23[1] = value[9];
                this._discrets16to23[2] = value[10];
                this._discrets16to23[3] = value[11];
                this._discrets16to23[4] = value[12];
                this._discrets16to23[5] = value[13];
                this._discrets16to23[6] = value[14];
                this._discrets16to23[7] = value[15];
                this._discrets16to23[8] = value[16];
                this._discrets16to23[9] = value[17];
                this._discrets16to23[10] = value[18];
                this._discrets16to23[11] = value[19];
                this._discrets16to23[12] = value[20];
                this._discrets16to23[13] = value[21];
                this._discrets16to23[14] = value[22];
                this._discrets16to23[15] = value[23];
            }
        }

        /// <summary>
        /// ������������
        /// </summary>
        public double[][] Currents
        {
            get { return this._currents; }
            set { this._currents = value; }
        }

        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        public int Count
        {
            get { return this._count; }
        }

        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        public int Alarm
        {
            get { return this._alarm; }
        }

        /// <summary>
        /// ����������� ���
        /// </summary>
        public double MinI
        {
            get { return this._minI; }
        }

        /// <summary>
        /// ������������ ���
        /// </summary>
        public double MaxI
        {
            get { return this._maxI; }
        }

        public ushort[] Ittn
        {
            get { return this._ittn; }
            set { this._ittn = value; }
        }

        private string _dateTime;
        public string DateAndTime
        {
            get { return this._dateTime; }
        }
        public bool IsLoad { get; private set; }
        public string FilePath { get; private set; }

        private string _stage;
        public string Stage
        {
            get { return this._stage; }
        }
        #region [Ctor's]
        public CountingList(ushort[] pageValue, OscJournalStruct oscJournalStruct)
        {
            this._oscJournalStruct = oscJournalStruct;
            this._dateTime = oscJournalStruct.GetFormattedDateTime;
            this._alarm = this._oscJournalStruct.Len - this._oscJournalStruct.After;
            this._stage = oscJournalStruct.Stage;
            this._countingArray = new ushort[CountingList.COUNTING_SIZE][];
            //����� ���������� ��������
            this._count = pageValue.Length/CountingList.COUNTING_SIZE;
            //������������� �������
            for (int i = 0; i < CountingList.COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this.Count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n == CountingList.COUNTING_SIZE)
                {
                    m++;
                    n = 0;
                }
            }
            this._ittn = CurrentConverter.Factors;
            this._discrets16to23 = this.DiscretArrayInit(this._countingArray[16]);
            this._discrets0to15 = this.DiscretArrayInit(this._countingArray[17]);

            this._currents = new double[16][];
            this._baseCurrents = new short[16][]; 
            for (int i = 0; i < 16; i++)
            {
                this._baseCurrents[i] = this._countingArray[i].Select(a => (short) a).ToArray();
                double factor = Math.Sqrt(2) * 40 * this._ittn[i/3] / (double)32767;
                this._currents[i] = this._baseCurrents[i].Select(a => factor * a).ToArray();
                this._maxI = Math.Max(this.MaxI, this._currents[i].Max());
                this._minI = Math.Min(this.MinI, this._currents[i].Min());
            }
        }

        #endregion [Ctor's]

        #region [Help members]
        /// <summary>
        /// ���������� ������ ����� � ��������������� ������ ���(�������� 0/1) 
        /// </summary>
        /// <param name="sourseArray">������ �������� ���</param>
        /// <returns></returns>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {

            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        } 
        #endregion [Help members]

        public void SaveArray(string filePath)
        {
            string path = Path.ChangeExtension(filePath, "osc");
            

            using (StreamWriter hdrFile = new StreamWriter(path))
            {
                
                for (int i = 0; i < this._count; i++)
                {
                    string str = this._countingArray.Aggregate(string.Empty, (current, t) => current + (((short) t[i]).ToString(CultureInfo.InvariantCulture) + ", "));
                    hdrFile.WriteLine(str);
                }
            }
        }

        public void Save(string filePath, OscilloscopeSettingsStruct oscSettings)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine("�� 902 {0} {1} ������� - {2}", this._oscJournalStruct.GetDate,
                    this._oscJournalStruct.GetTime, this._oscJournalStruct.Stage);
                hdrFile.WriteLine("Size, ms = {0}", this._oscJournalStruct.Len);
                hdrFile.WriteLine("Alarm = {0}", this._alarm);
                for (int i = 0; i < oscSettings.Kanal.Length; i++)
                {
                    hdrFile.WriteLine("K{0} = {1}", i + 1, oscSettings.Kanal[i]);
                }
                hdrFile.WriteLine(this._stage);
                hdrFile.WriteLine(1251);
            }
            string cgfPath = Path.ChangeExtension(filePath, "cfg");

            using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
            {
                cgfFile.WriteLine("MP902,1");
                cgfFile.WriteLine("48,16A,32D");
                int index = 1;
                for (int i =0; i < this.Currents.Length; i++)
                {
                    NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                    double factor = Math.Sqrt(2) *40*this._ittn[i/3]/(double) 32767;
                    cgfFile.WriteLine("{0},{1},,,A,{2},0,0,-32768,32767,1,1,P", index, StringData.AddingCurrentsNames[i], factor.ToString(format));
                    index++;
                }

                for (int i = 0; i < this.Discrets.Length; i++)
                {
                    cgfFile.WriteLine("{0},D{1},0", index, i+1);
                    index++;
                }

                for (int i = 0; i < this.Channels.Length; i++)
                {
                    cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, StringData.SignalSrab[oscSettings.Kanal[i]]);
                    index++;
                }
                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this._oscJournalStruct.Len);

                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);
                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeAlarm(this.Alarm));
                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < this._count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}",i,i*1000);
                    foreach (short[] current  in this._baseCurrents)
                    {
                        datFile.Write(",{0}", current[i]);      
                    }
                    foreach (ushort[] discret in this.Discrets)
                    {
                        datFile.Write(",{0}", discret[i]);   
                    }
                    foreach (ushort[] chanel in this.Channels)
                    {
                        datFile.Write(",{0}", chanel[i]);
                    }
                    datFile.WriteLine();
                }
            }
        }

        public static CountingList Load(string filePath, out OscilloscopeSettingsStruct oscSettings)
        {
            OscilloscopeSettingsStruct loadOscSettings = new OscilloscopeSettingsStruct();
            loadOscSettings.Kanal = new ushort[CHANNELS_COUNT];
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string timeString;
            if (hdrStrings[0].StartsWith("Fault date :"))
            {
                timeString = hdrStrings[0].Replace("Fault date : ", string.Empty);
            }
            else
            {
                string[] buff = hdrStrings[0].Split(' ');
                timeString = string.Format("{0} {1}", buff[2], buff[3]);
            }
            int alarm = int.Parse(hdrStrings[2].Replace("Alarm = ",string.Empty));
            for (int i = 0; i < CHANNELS_COUNT; i++)
            {
                loadOscSettings.Kanal[i] = ushort.Parse(hdrStrings[3 + i].Replace(string.Format("K{0} = ", i + 1), string.Empty));
            }
            string stage = hdrStrings[11];
            oscSettings = loadOscSettings;

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);
            double[] factors = new double[CURRENTS_COUNT];
            Regex factorRegex = new Regex(@"\d+\,I[\dPrabcn\s]+\,\,\,A\,(?<value>[0-9\.]+)");
            for (int i = 2; i < 2+CURRENTS_COUNT; i+=3)
            {
                string res = factorRegex.Match(cfgStrings[i]).Groups["value"].Value;
                NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                factors[(i - 2)/3] = double.Parse(res, format);
            }
            int counts = int.Parse(cfgStrings[52].Replace("1000,", string.Empty));

            CountingList result = new CountingList(counts);
            result._alarm = alarm;
            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);

            double[][] currents = new double[CURRENTS_COUNT][];
            ushort[][] discrets = new ushort[DISCRETS_COUNT][];
            ushort[][] channels = new ushort[CHANNELS_COUNT][];

            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new double[counts];         
            }

            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[counts];
            }

            for (int i = 0; i < channels.Length; i++)
            {
                channels[i] = new ushort[counts];
            }

            for (int i = 0; i < datStrings.Length; i++)
            {
                string[] line = datStrings[i].Split(',');
                int index = 2;

                for (int j = 0; j < CURRENTS_COUNT; j++)
                {
                    currents[j][i] = double.Parse(line[index]) * factors[j];
                    index++;
                }

                for (int j = 0; j < DISCRETS_COUNT; j++)
                {
                    discrets[j][i] = ushort.Parse(line[index]);
                    index++;
                }

                for (int j = 0; j < CHANNELS_COUNT; j++)
                {
                    channels[j][i] = ushort.Parse(line[index]);
                    index++;
                }      
            }


            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                result._maxI = Math.Max(result.MaxI, currents[i].Max());
                result._minI = Math.Min(result.MinI, currents[i].Min());                
            }
            result.Currents = currents;
            result.Channels = channels;
            result.Discrets = discrets;
            result.FilePath = filePath;
            result.IsLoad = true;
            result._dateTime = timeString;
            result._stage = stage;
            return result;
        }







        private CountingList(int count)
        {
            this._discrets0to15 = new ushort[16][];
            this._discrets16to23 = new ushort[16][];
            this._currents = new double[16][];
            this._baseCurrents = new short[16][];
            this._count = count;
        }

    }
}
