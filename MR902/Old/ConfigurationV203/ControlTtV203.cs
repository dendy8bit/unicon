﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR902.Old.ConfigurationV203
{
    /// <summary>
    /// конфигурация цепей ТТ
    /// </summary>
    public class ControlTtV203 : StructBase
    {
        [Layout(0)] private ushort _config; //выведена, неисправность, неисправность + блокировка
        [Layout(1)] private ushort _ust;    //уставка минимального дифференциального тока
        [Layout(2)] private ushort _time;   //выдержка времени
        [Layout(3)] private ushort _inpRes; //вход сброса неисправностей ТТ (только в структуре СШ1)

        /// <summary>
        /// Неисправность
        /// </summary>
        public string Fault
        {
            get { return Validator.Get(this._config, StringData.TtFault, 0, 1); }
            set { this._config = Validator.Set(value, StringData.TtFault, this._config, 0, 1); }
        }
        /// <summary>
        /// Iдmin
        /// </summary>
        public double Idmin
        {
            get { return ValuesConverterCommon.GetIn(this._ust); }
            set { this._ust = ValuesConverterCommon.SetIn(value); }
        }
        /// <summary>
        /// Tср
        /// </summary>
        public int Tsrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Сброс
        /// </summary>
        public string Reset
        {
            get { return Validator.Get(this._config, StringData.ResetTT, 2); }
            set { this._config = Validator.Set(value, StringData.ResetTT, this._config, 2); }
        }
        /// <summary>
        /// Вход сброса неисправностей ТТ
        /// </summary>
        public string InpResetTT
        {
            get { return Validator.Get(this._inpRes, StringData.SignalSrab); }
            set { this._inpRes = Validator.Set(value, StringData.SignalSrab); }
        }
    }
}
