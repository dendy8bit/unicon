﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR902.Old.ConfigurationV203.Structures.Defenses.Mtz
{
    class AllMtzStruct : StructBase, IDgvRowsContainer<MtzStruct>
    {
        public const int COUNT = 32;

        #region [Private fields]

        [Layout(0, Count = COUNT)] private MtzStruct[] _mtzstr;

        #endregion [Private fields]

        public MtzStruct[] Rows
        {
            get { return this._mtzstr; }
            set { this._mtzstr = value; }
        }
    }
}