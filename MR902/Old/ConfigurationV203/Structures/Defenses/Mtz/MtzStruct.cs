﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR902.Old.ConfigurationV203.Structures.Defenses.Mtz
{
    public class MtzStruct : StructBase
    {
        [Layout(0)] private ushort config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort config1;//конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort block; //вход блокировки
        [Layout(3)] private ushort ust; //уставка срабатывания_
        [Layout(4)] private ushort time; //время срабатывания_ тср
        [Layout(5)] private ushort k; //коэфиц. зависимой хар-ки
        [Layout(6)] private ushort u; //уставка пуска по напряжению Упуск
        [Layout(7)] private ushort tu; //время ускорения_

        #region Защиты I

        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string MtzMode
        {
            get { return Validator.Get(this.config, StringData.ModesLightMode, 0, 1); }
            set { this.config = Validator.Set(value, StringData.ModesLightMode, this.config, 0, 1); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string MtzBlock
        {
            get { return Validator.Get(this.block, StringData.InputSignals); }
            set { this.block = Validator.Set(value, StringData.InputSignals); }
        }
        [BindingProperty(2)]
        [XmlElement(ElementName = "Логика")]
        public string Logic
        {
            get
            {
                return StringData.TokParameter.Count > Common.GetBits(this.config, 5) >> 5
                    ? StringData.TokParameter[Common.GetBits(this.config, 5) >> 5]
                    : "XXXXX";
            }
            set
            {
                var input = StringData.TokParameter.IndexOf(value);
                this.config = Common.SetBits(this.config, (ushort)input, 5);
            }
        }
        [BindingProperty(3)]
        [XmlElement(ElementName = "Измерение")]
        public string MtzMeasure
        {
            get { return Validator.Get(this.config, StringData.MTZJoin, 6, 7, 8, 9); }
            set { this.config = Validator.Set(value, StringData.MTZJoin, this.config, 6, 7, 8, 9); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Уставка")]
        public double MtzIcp
        {
            get { return ValuesConverterCommon.GetIn(this.ust); }
            set { this.ust = ValuesConverterCommon.SetIn(value); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Характеристика")]
        public string MtzChar
        {
            get { return Validator.Get(this.config, StringData.Characteristic, 3); }
            set { this.config = Validator.Set(value, StringData.Characteristic, this.config, 3); }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "tср")]
        public int MtzT
        {
            get { return ValuesConverterCommon.GetWaitTime(this.time); }
            set { this.time = ValuesConverterCommon.SetWaitTime(value); }

        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "К")]
        public ushort MtzK
        {
            get { return this.k; }
            set { this.k = value; }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Осц")]
        public string MtzOsc
        {
            get { return Validator.Get(this.config, StringData.ModesLightOsc, 14, 15); }
            set { this.config = Validator.Set(value, StringData.ModesLightOsc, this.config, 14, 15); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Уров")]
        public string MtzUrov
        {
            get { return Validator.Get(this.config, StringData.ModesLight, 2); }
            set { this.config = Validator.Set(value, StringData.ModesLight, this.config, 2); }
        }

        #endregion


    }
}
