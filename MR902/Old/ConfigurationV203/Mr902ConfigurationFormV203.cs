﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR902.Old.Configuration.Structures.Primary;
using BEMN.MR902.Old.Configuration.Structures.Second;
using BEMN.MR902.Old.ConfigurationV203.Structures.Defenses.Differential;
using BEMN.MR902.Old.ConfigurationV203.Structures.Defenses.External;
using BEMN.MR902.Old.ConfigurationV203.Structures.Defenses.Mtz;
using BEMN.MR902.Properties;

namespace BEMN.MR902.Old.ConfigurationV203
{
    public partial class Mr902ConfigurationFormV203 : Form, IFormView
    {
        #region [Const]

        private const string READ_OK = "Конфигурация успешно прочитана";
        private const string READ_FAIL = "Не удалось прочитать конфигурацию";
        private const string WRITE_OK = "Конфигурация успешно записана";
        private const string WRITE_FAIL = "Невозможно записать конфигурацию";

        #endregion

        #region Поля

        private readonly MemoryEntity<ConfigurationStructV203> _configuration;
        private ConfigurationStructV203 _currentConfig;
        private readonly Mr902 _device;

        private ComboBox[] _OSCCombos;
        private CheckedListBox[] _VLSChectboxlistArray;
        private DataGridView[] _inpSignalsArray;
        
        #region [Защиты]
        // нужны только для валидации и вывода подсказки о неверном значении
        private NewDgwValidatior<AllExternalDefenseStruct, ExternalDefenseStruct> _externalDefenseValidator;
        private NewDgwValidatior<AllDifferentialCurrentStruct, DifferentialCurrentStruct> _differentialCurrentValidator;
        private NewDgwValidatior<AllMtzStruct, MtzStruct> _mtzValidator;

        #endregion


        #endregion

        #region Конструкторы

        public Mr902ConfigurationFormV203()
        {
            this.InitializeComponent();
        }

        public Mr902ConfigurationFormV203(Mr902 device)
        {
            this.InitializeComponent();
            this._device = device;
            StringData.Version = Common.VersionConverter(this._device.DeviceVersion);
            this._device.ConfigWriteOk += HandlerHelper.CreateActionHandler(this, this.AllWriteEnd);
            this._device.ConfigWriteFail += HandlerHelper.CreateActionHandler(this, this.AllWriteEndFail);
            this._configuration = device.ConfigurationV203;
            this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._device.ConfirmConstraint);
            this._configuration.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._configuration.RemoveStructQueries();
                this.AllWriteEndFail();
            });
            this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.AllReadEnd);
            this._configuration.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._configuration.RemoveStructQueries();
                this.AllReadEndFail();
            });
            this._configuration.ReadOk += HandlerHelper.CreateHandler(this, this._configProgressBar.PerformStep);
            this._configuration.WriteOk += HandlerHelper.CreateHandler(this, this._configProgressBar.PerformStep);

            this._configProgressBar.Maximum = this._configuration.Slots.Count;
            this._currentConfig = new ConfigurationStructV203();
            this.Init();
            this.PrepareTt();
            this.PrepareDZH();
            this.PrepareOSC();
            this.PrepareOscchannels();
            this.PrepareUrovJoinGrid();
            this.PrepareJoinGrid();
            this.PrepareParamautomat();
            this.PrepareElssygnal();
            this.PrepareINPSIGNAL();
            this._grUstComboBox.DataSource = StringData.InputSignals;
            this._indComboBox.DataSource = StringData.InputSignals;
            
            this.ShowConfiguration();
        }

        #endregion

        public void Init()
        {
            this._differentialCurrentValidator = new NewDgwValidatior
               <AllDifferentialCurrentStruct, DifferentialCurrentStruct>
               (
               new[] { this._difDDataGrid, this._difMDataGrid },
               new[] { 3, 3 },
               this._toolTip,
               new ColumnInfoCombo(StringData.DefNames, ColumnsType.NAME),
               new ColumnInfoCombo(StringData.ModesLightMode),
               new ColumnInfoCombo(StringData.InputSignals),
               new ColumnInfoText(RulesContainer.Ustavka40),
               new ColumnInfoText(RulesContainer.Ustavka40),
               new ColumnInfoText(RulesContainer.TimeRule),
               new ColumnInfoText(RulesContainer.Ustavka40),
               new ColumnInfoText(new CustomUshortRule(0, 45)),
               new ColumnInfoCombo(StringData.YesNo),
               new ColumnInfoText(RulesContainer.UshortTo100),
               new ColumnInfoCombo(StringData.YesNo),
               new ColumnInfoText(RulesContainer.UshortTo100),
               new ColumnInfoCombo(StringData.YesNo),
               new ColumnInfoCombo(StringData.YesNo),
               new ColumnInfoText(RulesContainer.Ustavka40),
               new ColumnInfoText(RulesContainer.TimeRule),
               new ColumnInfoCombo(StringData.InputSignals),
               new ColumnInfoCombo(StringData.ModesLightOsc),
               new ColumnInfoCombo(StringData.ModesLight)
               )
            {
                TurnOff = new[]
                   {
                        new TurnOffDgv
                            (
                            this._difDDataGrid,
                            new TurnOffRule(1, StringData.ModesLightMode[0], true,
                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18)
                            ),
                        new TurnOffDgv
                            (
                            this._difMDataGrid,
                            new TurnOffRule(1, StringData.ModesLightMode[0], true,
                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18)
                            )
                    }
            };

            this._mtzValidator = new NewDgwValidatior<AllMtzStruct, MtzStruct>
                (
                this._MTZDifensesDataGrid,
                AllMtzStruct.COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringData.MtzNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringData.ModesLightMode),
                new ColumnInfoCombo(StringData.InputSignals),
                new ColumnInfoCombo(StringData.TokParameter),
                new ColumnInfoCombo(StringData.MTZJoin),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoCombo(StringData.Characteristic),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ushort100To4000),
                new ColumnInfoCombo(StringData.ModesLightOsc),
                new ColumnInfoCombo(StringData.ModesLight)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._MTZDifensesDataGrid,
                        new TurnOffRule(1, StringData.ModesLightMode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10)
                        )
                }
            };
            this._externalDefenseValidator = new NewDgwValidatior<AllExternalDefenseStruct, ExternalDefenseStruct>
                (
                this._externalDifensesDataGrid,
                AllExternalDefenseStruct.COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringData.ExternalnNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringData.ModesLightMode),
                new ColumnInfoCombo(StringData.Otkl),
                new ColumnInfoCombo(StringData.SignalSrabExternal),
                new ColumnInfoCombo(StringData.SignalSrabExternal),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCombo(StringData.SignalSrabExternal),
                new ColumnInfoCombo(StringData.YesNo),
                new ColumnInfoCombo(StringData.ModesLightOsc),
                new ColumnInfoCombo(StringData.ModesLight)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._externalDifensesDataGrid,
                        new TurnOffRule(1, StringData.ModesLightMode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10)
                        )
                }
            };


        }
        
        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Mr902); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(Mr902ConfigurationFormV203); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        #region УРОВ ДЗШ

        public void PrepareDZH()
        {
            this._DZHTUrov1.Text = "0";
            this._DZHTUrov2.Text = "0";
            this._DZHTUrov3.Text = "0";
            this._DZHModes.DataSource = StringData.ModesLight;
            this._DZHKontr.DataSource = StringData.KONTR;
            this._DZHDiff.DataSource = StringData.Forbidden;
            this._DZHSelf.DataSource = StringData.Forbidden;
            this._DZHSign.DataSource = StringData.Forbidden;
            this._DZHSH1.DataSource = StringData.InputSignals;
            this._DZHSH2.DataSource = StringData.InputSignals;
            this._DZHPO.DataSource = StringData.InputSignals;
            //SubscriptCombos(_DZHCombos);
        }

        public void ReadDZH()
        {

            UrovStruct dhz = this._currentConfig.UrovStruct;
            this._DZHModes.SelectedItem = dhz.DzhModes;
            this._DZHKontr.SelectedItem = dhz.DzhControl;
            this._DZHDiff.SelectedItem = dhz.DzhDiff;
            this._DZHSelf.SelectedItem = dhz.DzhSelf;
            this._DZHTUrov1.Text = dhz.DzhT1.ToString();
            this._DZHTUrov2.Text = dhz.DzhT2.ToString();
            this._DZHTUrov3.Text = dhz.DzhT3.ToString();
            this._DZHSign.SelectedItem = dhz.DzhSign;
            this._DZHSH1.SelectedItem = dhz.DzhSh1;
            this._DZHSH2.SelectedItem = dhz.DzhSh2;
            this._DZHPO.SelectedItem = dhz.DzhPo;
        }

        public void WriteDZH()
        {
            var dhz = new UrovStruct();
            dhz.DzhModes = this._DZHModes.SelectedItem.ToString();
            dhz.DzhControl = this._DZHKontr.SelectedItem.ToString();
            dhz.DzhDiff = this._DZHDiff.SelectedItem.ToString();
            dhz.DzhSelf = this._DZHSelf.SelectedItem.ToString();
            dhz.DzhT1 = Convert.ToInt32(this._DZHTUrov1.Text);
            dhz.DzhT2 = Convert.ToInt32(this._DZHTUrov2.Text);
            dhz.DzhT3 = Convert.ToInt32(this._DZHTUrov3.Text);
            dhz.DzhSign = this._DZHSign.SelectedItem.ToString();
            dhz.DzhSh1 = this._DZHSH1.SelectedItem.ToString();
            dhz.DzhSh2 = this._DZHSH2.SelectedItem.ToString();
            dhz.DzhPo = this._DZHPO.SelectedItem.ToString();
            this._currentConfig.UrovStruct = dhz;
        }

        #endregion

        #region Осциллограф

        public void PrepareOSC()
        {
            this._OSCCombos = new ComboBox[]
            {
                this._oscLength,
                this._oscFix
            };


            this.ClearCombos(this._OSCCombos);

            this._oscWriteLength.Text = "0";
            this._oscLength.DataSource = StringData.OscLength;
            this._oscFix.DataSource = StringData.OscFix;

            this.SubscriptCombos(this._OSCCombos);
        }

        public void ReadOsc()
        {
            this._oscLength.SelectedItem = this._currentConfig.Osc.OSC_LENGTH;
            this._oscWriteLength.Text = this._currentConfig.Osc.OSC_W_LENGTH.ToString();
            this._oscFix.SelectedItem = this._currentConfig.Osc.OSC_FIX;
        }


        #endregion

        #region Осциллограф Каналы

        public void PrepareOscchannels()
        {
            this._oscSignal.DataSource = StringData.SignalSrab;
            this.oscStartCmb.Items.AddRange(StringData.SignalSrab.ToArray());
            if (this._oscChannels.Rows.Count == 0)
            {
                string[] channels = this._currentConfig.Osc.Channels;
                for (int i = 0; i < channels.Length; i++)
                {
                    this._oscChannels.Rows.Add(new object[]
                    {
                        (i + 1).ToString(),
                        StringData.SignalSrab[0]
                    });
                }
            }
        }

        public void ReadOSCCHANNELS()
        {
            string[] channels = this._currentConfig.Osc.Channels;
            for (int i = 0; i < channels.Length; i++)
            {
                this._oscChannels.Rows[i].Cells["_oscSignal"].Value = channels[i];
            }
            this.oscStartCmb.SelectedItem = this._currentConfig.Osc.InpStart;
        }

        public void WriteALLOSC()
        {
            OscilloscopeSettingsStructV203 config = new OscilloscopeSettingsStructV203
            {
                OSC_LENGTH = this._oscLength.SelectedItem.ToString(),
                OSC_W_LENGTH = Convert.ToUInt16(this._oscWriteLength.Text),
                OSC_FIX = this._oscFix.SelectedItem.ToString()
            };

            string[] channels = new string[8];
            for (int i = 0; i < channels.Length; i++)
            {
                channels[i] = this._oscChannels.Rows[i].Cells["_oscSignal"].Value.ToString();
            }
            config.Channels = channels;
            config.InpStart = this.oscStartCmb.SelectedItem.ToString();
            this._currentConfig.Osc = config;
        }

        #endregion

        #region УРОВ Присоединения

        public void PrepareUrovJoinGrid()
        {
            if (this._UROVJoinData.Rows.Count == 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    this._UROVJoinData.Rows.Add(new object[]
                    {
                        "Присоединение " + (i + 1).ToString(),
                        0,
                        0
                    });
                }
                this._UROVJoinData.Rows.Add(new object[]
                {
                    "In ",
                    0,
                    0
                });
            }
        }

        public void ReadUrovJoinGrid()
        {
            for (int i = 0; i < 6; i++)
            {
                this._UROVJoinData.Rows[i].Cells["_JoinIUROV"].Value = this._currentConfig.UrovConn.Ust[i].UrovJoinI;
                this._UROVJoinData.Rows[i].Cells["_JoinTUROV"].Value = this._currentConfig.UrovConn.Ust[i].UrovJoinT;
            }
        }

        public void WriteUrovJoinGrid()
        {

            for (int i = 0; i < 6; i++)
            {
                this._currentConfig.UrovConn.Ust[i].UrovJoinI =
                    Convert.ToDouble(this._UROVJoinData.Rows[i].Cells["_JoinIUROV"].Value);
                this._currentConfig.UrovConn.Ust[i].UrovJoinT =
                    Convert.ToInt32(this._UROVJoinData.Rows[i].Cells["_JoinTUROV"].Value);
            }
        }

        #endregion

        #region Присоединения

        public void PrepareJoinGrid()
        {
            this._joinSwitchOFF.DataSource = StringData.InputSignals;
            this._joinSwitchOn.DataSource = StringData.InputSignals;
            this._joinJoin.DataSource = StringData.Join;
            this._joinEnter.DataSource = StringData.InputSignals;


            if (this._joinData.Rows.Count == 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    this._joinData.Rows.Add(new object[]
                    {
                        "Присоединение " + (i + 1).ToString(),
                        0,
                        StringData.InputSignals[0],
                        StringData.InputSignals[0],
                        StringData.Join[0],
                        StringData.InputSignals[0],
                        false,
                        0

                    });
                }
                this._joinData.Rows.Add(new object[]
                {
                    "In",
                    0,
                    StringData.InputSignals[0],
                    StringData.InputSignals[0],
                    StringData.Join[0],
                    StringData.InputSignals[0],
                    false,
                    0

                });
                var emptyValues = new List<string> {"-"};

                this._joinData.Rows[5].Cells[4].ReadOnly = true;
                this._joinData.Rows[5].Cells[4].Style.BackColor = SystemColors.Control;

                (this._joinData.Rows[5].Cells[4] as DataGridViewComboBoxCell).DataSource = emptyValues;
                this._joinData.Rows[5].Cells[4].Value = "-";
                this._joinData.Rows[5].Cells[5].ReadOnly = true;
                this._joinData.Rows[5].Cells[5].Style.BackColor = SystemColors.Control;
                (this._joinData.Rows[5].Cells[5] as DataGridViewComboBoxCell).DataSource = emptyValues;
                this._joinData.Rows[5].Cells[5].Value = "-";
                this._joinData.Rows[5].Cells[6].ReadOnly = true;
                this._joinData.Rows[5].Cells[6].Style.BackColor = SystemColors.Control;
                this._joinData.Rows[5].Cells[7].ReadOnly = true;
                this._joinData.Rows[5].Cells[7].Style.BackColor = SystemColors.Control;
            }
        }

        public void ReadJoinGrid()
        {
            for (int i = 0; i < 6; i++)
            {
                var connection = this._currentConfig.Conn.ConnMain[i];

                this._joinData.Rows[i].Cells["_joinITT"].Value = connection.Inom;
                this._joinData.Rows[i].Cells["_joinSwitchOFF"].Value = connection.JoinSwitchoff;
                this._joinData.Rows[i].Cells["_joinSwitchOn"].Value = connection.JoinSwitchon;

                if (i != 5)
                {
                    this._joinData.Rows[i].Cells["_joinJoin"].Value = connection.JoinJoin;
                    this._joinData.Rows[i].Cells["_joinEnter"].Value = connection.JoinEnter;
                    this._joinData.Rows[i].Cells["_joinResetColumn"].Value = connection.ResetJoin;
                    this._joinData.Rows[i].Cells["_timeResetJoinColumn"].Value = connection.ResetDeley;
                }
            }
        }

        public void WriteJoinGrid()
        {
            var allConnections = new AllConnectionsStruct();
            for (int i = 0; i < 6; i++)
            {
                var connection = new ConnectionStruct();
                connection.Inom = Convert.ToUInt16(this._joinData.Rows[i].Cells["_joinITT"].Value);
                connection.JoinSwitchoff = this._joinData.Rows[i].Cells["_joinSwitchOFF"].Value.ToString();
                connection.JoinSwitchon = this._joinData.Rows[i].Cells["_joinSwitchOn"].Value.ToString();



                if (i != 5)
                {
                    connection.JoinJoin = this._joinData.Rows[i].Cells["_joinJoin"].Value.ToString();
                    connection.JoinEnter = this._joinData.Rows[i].Cells["_joinEnter"].Value.ToString();
                    connection.ResetJoin = Convert.ToBoolean(this._joinData.Rows[i].Cells["_joinResetColumn"].Value);
                    connection.ResetDeley =
                        int.Parse(this._joinData.Rows[i].Cells["_timeResetJoinColumn"].Value.ToString());


                }
                allConnections.ConnMain[i] = connection;
            }
            this._currentConfig.Conn = allConnections;
        }

        #endregion

        #region Реле и индикаторы

        private void PrepareParamautomat()
        {
            this._outputReleGrid.Rows.Clear();
            this._outputIndicatorsGrid.Rows.Clear();
            this._releSignalCol.Items.Clear();
            this._releTypeCol.Items.Clear();
            this._outIndSignalCol.Items.Clear();
            this._outIndTypeCol.Items.Clear();

            this._releSignalCol.DataSource = StringData.SignalSrab;
            this._releTypeCol.DataSource = StringData.SignalType;
            this._outIndSignalCol.DataSource = StringData.SignalSrab;
            this._outIndTypeCol.DataSource = StringData.SignalType;

            for (int i = 0; i < Mr902.RELE_COUNT; i++)
            {
                this._outputReleGrid.Rows.Add(new object[]
                {
                    i + 1,
                    "Повторитель",
                    "Нет",
                    0
                });
            }
            this._outputIndicatorsGrid.EditingControlShowing +=
                new DataGridViewEditingControlShowingEventHandler(this.DataGrid_EditingControlShowing);
            this._outputReleGrid.EditingControlShowing +=
                new DataGridViewEditingControlShowingEventHandler(this.DataGrid_EditingControlShowing);
            this._outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < Mr902.INDICATOR_COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(new object[]
                {
                    i + 1,
                    "Повторитель",
                    "Нет",
                    "",
                });
                this._outputIndicatorsGrid.Rows[i].Cells["_outIndColorCol"].Style.BackColor = Color.Red;
            }

            this._neispr1CB.SelectedIndex = 0;
            this._neispr2CB.SelectedIndex = 0;
            this._neispr3CB.SelectedIndex = 0;
            this._neispr4CB.SelectedIndex = 0;
            this._impTB.Text = "0";
        }

        private void ReadPARAMAUTOMAT()
        {
            for (int i = 0; i < Mr902.RELE_COUNT; i++)
            {
                this._outputReleGrid[2, i].Value = this._currentConfig.ParamAutomat.RelayStruct[i].Signal;
                this._outputReleGrid[1, i].Value = this._currentConfig.ParamAutomat.RelayStruct[i].Type;
                this._outputReleGrid[3, i].Value = this._currentConfig.ParamAutomat.RelayStruct[i].Wait;
            }
            
            for (int i = 0; i < Mr902.INDICATOR_COUNT; i++)
            {
                this._outputIndicatorsGrid[1, i].Value = this._currentConfig.ParamAutomat.IndicatorStruct[i].Type;
                this._outputIndicatorsGrid[2, i].Value = this._currentConfig.ParamAutomat.IndicatorStruct[i].Signal;
                this._outputIndicatorsGrid[3, i].Style.BackColor = this._currentConfig.ParamAutomat.IndicatorStruct[i].Color
                    ? Color.Green
                    : Color.Red;
                this._outputIndicatorsGrid[3, i].Style.SelectionBackColor = this._outputIndicatorsGrid[3, i].Style.BackColor;
            }

            this._neispr1CB.SelectedItem = this._currentConfig.ParamAutomat.OutputN1;
            this._neispr2CB.SelectedItem = this._currentConfig.ParamAutomat.OutputN2;
            this._neispr3CB.SelectedItem = this._currentConfig.ParamAutomat.OutputN3;
            this._neispr4CB.SelectedItem = this._currentConfig.ParamAutomat.OutputN4;
            this._impTB.Text = this._currentConfig.ParamAutomat.OutputImp.ToString();
        }

        private void WriteParamAutomat()
        {
            ParamAutomatStruct str = new ParamAutomatStruct
            {
                OutputN1 = this._neispr1CB.SelectedItem.ToString(),
                OutputN2 = this._neispr2CB.SelectedItem.ToString(),
                OutputN3 = this._neispr3CB.SelectedItem.ToString(),
                OutputN4 = this._neispr4CB.SelectedItem.ToString(),
                OutputImp = Convert.ToInt32(this._impTB.Text)
            };

            RelayStruct[] relays = new RelayStruct[Mr902.RELE_COUNT];
            for (int i = 0; i < Mr902.RELE_COUNT; i++)
            {
                relays[i] = new RelayStruct
                {
                    Signal = this._outputReleGrid[2, i].Value.ToString(),
                    Type = this._outputReleGrid[1, i].Value.ToString(),
                    Wait = Convert.ToInt32(this._outputReleGrid[3, i].Value)
                };
            }
            str.RelayStruct = relays;
            IndicatorStruct[] indicators = new IndicatorStruct[Mr902.INDICATOR_COUNT];
            for (int i = 0; i < Mr902.INDICATOR_COUNT; i++)
            {
                indicators[i] = new IndicatorStruct
                {
                    Signal = this._outputIndicatorsGrid[2, i].Value.ToString(),
                    Type = this._outputIndicatorsGrid[1, i].Value.ToString(),
                    Color = this._outputIndicatorsGrid[3, i].Style.BackColor == Color.Green
                };
            }
            str.IndicatorStruct = indicators;

            this._currentConfig.ParamAutomat = str;
        }

        void _outputIndicatorsGrid_Click(object sender, EventArgs e)
        {
            if (this._outputIndicatorsGrid.CurrentCell.ColumnIndex == 3)
            {
                if (this._outputIndicatorsGrid.CurrentCell.Style.BackColor == Color.Red)
                {
                    this._outputIndicatorsGrid.CurrentCell.Style.BackColor = Color.Green;
                    this._outputIndicatorsGrid.CurrentCell.Style.SelectionBackColor =
                        this._outputIndicatorsGrid.CurrentCell.Style.BackColor;
                }
                else
                {
                    this._outputIndicatorsGrid.CurrentCell.Style.BackColor = Color.Red;
                    this._outputIndicatorsGrid.CurrentCell.Style.SelectionBackColor =
                        this._outputIndicatorsGrid.CurrentCell.Style.BackColor;
                }
            }
        }

        #endregion

        #region ВЛС

        public void PrepareElssygnal()
        {
            this._VLSChectboxlistArray = new CheckedListBox[]
            {
                this.VLScheckedListBox1, this.VLScheckedListBox2, this.VLScheckedListBox3, this.VLScheckedListBox4,
                this.VLScheckedListBox5, this.VLScheckedListBox6, this.VLScheckedListBox7, this.VLScheckedListBox8,
                this.VLScheckedListBox9, this.VLScheckedListBox10, this.VLScheckedListBox11, this.VLScheckedListBox12,
                this.VLScheckedListBox13, this.VLScheckedListBox14, this.VLScheckedListBox15, this.VLScheckedListBox16
            };

            for (int i = 0; i < this._VLSChectboxlistArray.Length; i++)
            {

                this._VLSChectboxlistArray[i].DataSource = StringData.VLSSignals;

            }
        }

        public void WriteElssygnal()
        {

            ushort[] _vls = new ushort[AllOutputSignalsStruct.COUNT*OutputSignalStruct.COUNT];

            int byteIndex = 0;

            for (int i = 0; i < AllOutputSignalsStruct.COUNT; i++) //Счетчик закладок ВЛС 1-16
            {
                byteIndex = OutputSignalStruct.COUNT*i; //индекс массива 1ВЛС = 16 байт

                for (int j = 0; j < StringData.VLSSignals.Count; j += AllOutputSignalsStruct.COUNT)
                    //Счетчик элементов одного ВЛС 1-125
                {
                    int itemIndex = j; //индекс элемента списка
                    for (int bitIndex = 0; bitIndex < 16; bitIndex++) //перебираем биты слова
                    {
                        if (itemIndex < (StringData.VLSSignals.Count /*- 1*/))
                            //Чтоб индекс элемента списка не зашкалил за длинну списка
                        {
                            _vls[byteIndex] = BEMN.MBServer.Common.SetBit(_vls[byteIndex], bitIndex,
                                this._VLSChectboxlistArray[i].GetItemChecked(itemIndex));
                            itemIndex++; //переходим к сл. элементу списка
                        }
                    }
                    byteIndex++; //переходим к следующему байту массива данного ВЛС
                }
            }

            var signals = new AllOutputSignalsStruct();
            signals.VLS = _vls;
            this._currentConfig.Els = signals;
        }

        public void ReadElssygnal()
        {
            ushort[] _vls = this._currentConfig.Els.VLS;

            int byteIndex = 0;

            for (int i = 0; i < AllOutputSignalsStruct.COUNT; i++) //Счетчик закладок ВЛС 1-16
            {
                byteIndex = 18*i; //индекс массива 1ВЛС = 16 байт

                for (int j = 0; j < StringData.VLSSignals.Count; j += AllOutputSignalsStruct.COUNT)
                    //Счетчик элементов одного ВЛС 1-125
                {
                    int itemIndex = j; //индекс элемента списка
                    for (int bitIndex = 0; bitIndex < 16; bitIndex++) //перебираем биты слова
                    {
                        if (itemIndex < (StringData.VLSSignals.Count /*- 1*/))
                            //Чтоб индекс элемента списка не зашкалил за длинну списка
                        {
                            this._VLSChectboxlistArray[i].SetItemChecked(itemIndex, Common.GetBit(_vls[byteIndex], bitIndex));
                            itemIndex++; //переходим к сл. элементу списка
                        }
                    }
                    byteIndex++; //переходим к следующему байту массива данного ВЛС
                }
            }
        }

        #endregion

        #region И Или

        public void PrepareINPSIGNAL()
        {
            this._inpSignalsArray = new DataGridView[]
            {
                this._inputSignals1, this._inputSignals2, this._inputSignals3, this._inputSignals4,
                this._inputSignals5, this._inputSignals6, this._inputSignals7, this._inputSignals8,
                this._inputSignals9, this._inputSignals10, this._inputSignals11, this._inputSignals12,
                this._inputSignals13, this._inputSignals14, this._inputSignals15, this._inputSignals16
            };

            for (int j = 0; j < this._inpSignalsArray.Length; j++)
            {
                if (this._inpSignalsArray[j].Rows.Count == 0)
                {
                    for (int i = 0; i < StringData.LogicSignals.Count; i++)
                    {
                        this._signalValueCol.DataSource = StringData.LogicValues;
                        this._inpSignalsArray[j].Rows.Add("Д" + (i + 1).ToString(), StringData.LogicValues[0]);
                    }
                }
            }
        }

        public void ReadInpsignal()
        {
            int _inpSignalsIndex = 0;


            ushort[] _inpSignals = this._currentConfig.Inp.InpSignals;
            for (int j = 0; j < this._inpSignalsArray.Length; j++)
            {
                _inpSignalsIndex = j*4;
                int indexLs = 0;
                do
                {
                    for (int i = 0; i < this._inpSignalsArray.Length; i += 2)
                    {
                        int IND = Common.GetBits(_inpSignals[_inpSignalsIndex], i, i + 1) >> i;
                        this._inpSignalsArray[j][1, indexLs].Value = StringData.LogicValues[IND];
                        indexLs++;
                    }
                    _inpSignalsIndex++;
                } while (indexLs < StringData.LogicSignals.Count);
            }
        }

        public void WriteInpsignal()
        {
            int _inpSignalsIndex = 0;
            ushort[] _inpSignals = new ushort[12*16/2];
            for (int j = 0; j < this._inpSignalsArray.Length; j++)
            {
                _inpSignalsIndex = j*4;
                int indexLs = 0;
                do
                {
                    for (int i = 0; i < this._inpSignalsArray.Length; i += 2)
                    {
                        _inpSignals[_inpSignalsIndex] = Common.SetBits(_inpSignals[_inpSignalsIndex],
                            (ushort) StringData.LogicValues.IndexOf(this._inpSignalsArray[j][1, indexLs].Value.ToString()), i,
                            i + 1);
                        indexLs++;
                    }
                    _inpSignalsIndex++;
                } while (indexLs < StringData.LogicSignals.Count);
            }
            var allInputSignals = new AllInputSignalsStruct {InpSignals = _inpSignals};
            this._currentConfig.Inp = allInputSignals;
        }

        #endregion

        #region Диф защиты действующие
        
        public void ReadDifDGrid(DefenseStruct defense)
        {

            for (int i = 0; i < defense.DiffDstv.Length; i++)
            {

                var def = defense.DiffDstv[i];

                this._difDDataGrid.Rows[i].Cells["_difDModesColumn"].Value = def.DIFD_MODE;
                this._difDDataGrid.Rows[i].Cells["_difDBlockColumn"].Value = def.DIFD_BLOCK;
                this._difDDataGrid.Rows[i].Cells["_difDIcpColumn"].Value = def.DIFD_ICP;
                this._difDDataGrid.Rows[i].Cells["_difDIdoColumn"].Value = def.DIFD_IDO;
                this._difDDataGrid.Rows[i].Cells["_difDtcpColumn"].Value = def.DIFD_TCP;
                this._difDDataGrid.Rows[i].Cells["_difDIbColumn"].Value = def.DIFD_IB;
                this._difDDataGrid.Rows[i].Cells["_difDfColumn"].Value = def.DIFD_F;
                this._difDDataGrid.Rows[i].Cells["_difDBlockGColumn"].Value = def.DIFD_BLOCK_G;
                this._difDDataGrid.Rows[i].Cells["_difDI2gColumn"].Value = def.DIFD_I2G;

                this._difDDataGrid.Rows[i].Cells["_difDBlock5GColumn"].Value = def.DIFD_BLOCK_5G;
                this._difDDataGrid.Rows[i].Cells["_difDI5gColumn"].Value = def.DIFD_I5G;
                this._difDDataGrid.Rows[i].Cells["_difDOprNasColumn"].Value = def.DIFD_BLOCK_OPR_NAS;

                this._difDDataGrid.Rows[i].Cells["_difDOchColumn"].Value = def.DIFD_OCH;
                this._difDDataGrid.Rows[i].Cells["_difDtochColumn"].Value = def.DIFD_T_OCH;
                this._difDDataGrid.Rows[i].Cells["_difDEnterOchColumn"].Value = def.DIFD_ENTER_OCH;
                this._difDDataGrid.Rows[i].Cells["_difDIochColumn"].Value = def.DIFD_I_OCH;
                this._difDDataGrid.Rows[i].Cells["_difDOscColumn"].Value = def.DIFD_OSC;
                this._difDDataGrid.Rows[i].Cells["_difDUrovColumn"].Value = def.DIFD_UROV;
                //Пуск от По
                if (i == 0)
                    this._actualRunForPoId1.Checked = def.DefenceActualRunForPo;
                if (i == 1)
                    this._actualRunForPoId2.Checked = def.DefenceActualRunForPo;
            }
        }

        #endregion

        #region Диф защиты мгновенные
        
        public void ReadDifMGrid(DefenseStruct defense)
        {
            for (int i = 0; i < defense.DiffMgn.Length; i++)
            {
                var def = defense.DiffMgn[i];
                this._difMDataGrid.Rows[i].Cells["_difMModesColumn"].Value = def.DIFM_MODE;
                this._difMDataGrid.Rows[i].Cells["_difMBlockColumn"].Value = def.DIFM_BLOCK;
                this._difMDataGrid.Rows[i].Cells["_difMIcpColumn"].Value = def.DIFM_ICP;
                this._difMDataGrid.Rows[i].Cells["_difMIdoColumn"].Value = def.DIFM_IDO;
                this._difMDataGrid.Rows[i].Cells["_difMIbColumn"].Value = def.DIFM_IB;
                this._difMDataGrid.Rows[i].Cells["_difMfColumn"].Value = def.DIFM_F;
                this._difMDataGrid.Rows[i].Cells["_difMOchColumn"].Value = def.DIFM_OCH;
                this._difMDataGrid.Rows[i].Cells["_difMtochColumn"].Value = def.DIFM_T_OCH;
                this._difMDataGrid.Rows[i].Cells["_difMEnterOchColumn"].Value = def.DIFM_ENTER_OCH;
                this._difMDataGrid.Rows[i].Cells["_difMIochColumn"].Value = def.DIFM_I_OCH;
                this._difMDataGrid.Rows[i].Cells["_difMOscColumn"].Value = def.DIFM_OSC;
                this._difMDataGrid.Rows[i].Cells["_difMUrovColumn"].Value = def.DIFM_UROV;
                //Пуск от По
                if (i == 0)
                    this._instantaneousRunForPoId1.Checked = def.DefenceInstantaneousRunForPo;
                if (i == 1)
                    this._instantaneousRunForPoId2.Checked = def.DefenceInstantaneousRunForPo;
            }
        }

        #endregion

        #region МТЗ
        
        public void ReadMtzGrid(DefenseStruct defense)
        {
            for (int i = 0; i < defense.MtzDefStruct.Length; i++)
            {
                var def = defense.MtzDefStruct[i];
                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzModesColumn"].Value = def.MTZ_MODE;
                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzBlockingColumn"].Value = def.MTZ_BLOCK;
                this._MTZDifensesDataGrid.Rows[i].Cells["_logicImaxColumn"].Value = def.Logic;

                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzMeasureColumn"].Value = def.MTZ_MEASURE;
                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzICPColumn"].Value = def.MTZ_ICP;
                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzCharColumn"].Value = def.MTZ_CHAR;
                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzTColumn"].Value = def.MTZ_T;
                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzkColumn"].Value = def.MTZ_K;
                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzOscColumn"].Value = def.MTZ_OSC;
                this._MTZDifensesDataGrid.Rows[i].Cells["_mtzUROVColumn"].Value = def.MTZ_UROV;
            }
        }
        
        #endregion

        #region Внешние
        
        public void ReadExternalGrid(DefenseStruct defense)
        {
            for (int i = 0; i < defense.MtzExt.Length; i++)
            {
                var def = defense.MtzExt[i];
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifModesColumn"].Value = def.EXTERNAL_MODE;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifOtklColumn"].Value = def.EXTERNAL_OTKL;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifBlockingColumn"].Value = def.EXTERNAL_BLOCK;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifSrabColumn"].Value = def.EXTERNAL_SRAB;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifTsrColumn"].Value = def.EXTERNAL_TSR;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifTvzColumn"].Value = def.EXTERNAL_TVZ;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifVozvrColumn"].Value = def.EXTERNAL_VOZVR;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifVozvrYNColumn"].Value = def.EXTERNAL_VOZVR_YN;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifOscColumn"].Value = def.EXTERNAL_OSC;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifUROVColumn"].Value = def.EXTERNAL_UROV;
            }
        }
        #endregion

        #region [Контроль цепей ТТ]

        public void PrepareTt()
        {
            this._resetTtCmb.Items.AddRange(StringData.SignalSrab.ToArray());
            this._configTtDgv.Rows.Clear();
            this._faultTtColumn.DataSource = StringData.TtFault;
            this._resetCol.DataSource = StringData.ResetTT;
            for (int i = 0; i < 3; i++)
            {
                this._configTtDgv.Rows.Add
                    (
                        StringData.TtNames[i],
                        0,
                        0,
                        StringData.TtFault[0],
                        StringData.ResetTT[0]
                    );
            }
        }

        public void ReadTt()
        {
            this._configTtDgv.Rows.Clear();
            for (int i = 0; i < 3; i++)
            {
                if (i == 0)
                {
                    this._resetTtCmb.SelectedItem = this._currentConfig.Ctt.TtChannels[i].InpResetTT;
                }
                this._configTtDgv.Rows.Add
                    (
                        StringData.TtNames[i],
                        this._currentConfig.Ctt.TtChannels[i].Idmin,
                        this._currentConfig.Ctt.TtChannels[i].Tsrab,
                        this._currentConfig.Ctt.TtChannels[i].Fault,
                        this._currentConfig.Ctt.TtChannels[i].Reset
                    );
            }
        }

        public void WriteTt()
        {
            AllConfigTtStructV203 str = new AllConfigTtStructV203();
            for (int i = 0; i < 3; i++)
            {
                if (i == 0)
                {
                    str.TtChannels[i].InpResetTT = this._resetTtCmb.SelectedItem.ToString();
                }
                str.TtChannels[i].Idmin = Convert.ToDouble(this._configTtDgv[1, i].Value);
                str.TtChannels[i].Tsrab = Convert.ToInt32(this._configTtDgv[2, i].Value);
                str.TtChannels[i].Fault = this._configTtDgv[3, i].Value.ToString();
                str.TtChannels[i].Reset = this._configTtDgv[4, i].Value.ToString();
            }
            this._currentConfig.Ctt = str;
        }

        #endregion [Контроль цепей ТТ]

        #region [Подготовка, чтение и запись всех защит]
        
        public void ReadCurrentProtMain(DefenseStruct defense)
        {
            this.ReadDifDGrid(defense);
            this.ReadDifMGrid(defense);
            this.ReadMtzGrid(defense);
            this.ReadExternalGrid(defense);
        }


        public DefenseStruct WriteCurrentProtMain()
        {
            DefenseStruct defense = new DefenseStruct();

            #region [Действующие]

            try
            {
                for (int i = 0; i < defense.DiffDstv.Length; i++)
                {
                    var def = new DifDefStruct();
                    def.DIFD_MODE = this._difDDataGrid.Rows[i].Cells["_difDModesColumn"].Value.ToString();
                    def.DIFD_BLOCK = this._difDDataGrid.Rows[i].Cells["_difDBlockColumn"].Value.ToString();
                    def.DIFD_ICP = Convert.ToDouble(this._difDDataGrid.Rows[i].Cells["_difDIcpColumn"].Value);
                    def.DIFD_IDO = Convert.ToDouble(this._difDDataGrid.Rows[i].Cells["_difDIdoColumn"].Value);
                    def.DIFD_TCP = Convert.ToInt32(this._difDDataGrid.Rows[i].Cells["_difDtcpColumn"].Value);
                    def.DIFD_IB = Convert.ToDouble(this._difDDataGrid.Rows[i].Cells["_difDIbColumn"].Value);
                    def.DIFD_F = Convert.ToUInt16(this._difDDataGrid.Rows[i].Cells["_difDfColumn"].Value);
                    def.DIFD_BLOCK_G = this._difDDataGrid.Rows[i].Cells["_difDBlockGColumn"].Value.ToString();
                    def.DIFD_I2G = Convert.ToUInt16(this._difDDataGrid.Rows[i].Cells["_difDI2gColumn"].Value);

                    def.DIFD_BLOCK_5G = this._difDDataGrid.Rows[i].Cells["_difDBlock5GColumn"].Value.ToString();
                    def.DIFD_I5G = Convert.ToUInt16(this._difDDataGrid.Rows[i].Cells["_difDI5gColumn"].Value);
                    def.DIFD_BLOCK_OPR_NAS = this._difDDataGrid.Rows[i].Cells["_difDOprNasColumn"].Value.ToString();

                    def.DIFD_OCH = this._difDDataGrid.Rows[i].Cells["_difDOchColumn"].Value.ToString();
                    def.DIFD_T_OCH = Convert.ToInt32(this._difDDataGrid.Rows[i].Cells["_difDtochColumn"].Value);
                    def.DIFD_ENTER_OCH = this._difDDataGrid.Rows[i].Cells["_difDEnterOchColumn"].Value.ToString();
                    def.DIFD_I_OCH = Convert.ToDouble(this._difDDataGrid.Rows[i].Cells["_difDIochColumn"].Value);
                    def.DIFD_OSC = this._difDDataGrid.Rows[i].Cells["_difDOscColumn"].Value.ToString();
                    def.DIFD_UROV = this._difDDataGrid.Rows[i].Cells["_difDUrovColumn"].Value.ToString();

                    //Пуск от По
                    if (i == 0)
                        def.DefenceActualRunForPo = this._actualRunForPoId1.Checked;
                    if (i == 1)
                        def.DefenceActualRunForPo = this._actualRunForPoId2.Checked;

                    defense.DiffDstv[i] = def;
                }
            }
            catch
            {
                throw new InvalidOperationException("Найдены некорректные уставки в защитах по действующим значениям");
            }
            #endregion [Действующие]

            #region [Мгновенные]

            try
            {
                for (int i = 0; i < defense.DiffMgn.Length; i++)
                {
                    var def = new DifDefStruct();
                    def.DIFM_MODE = this._difMDataGrid.Rows[i].Cells["_difMModesColumn"].Value.ToString();
                    def.DIFM_BLOCK = this._difMDataGrid.Rows[i].Cells["_difMBlockColumn"].Value.ToString();
                    def.DIFM_ICP = Convert.ToDouble(this._difMDataGrid.Rows[i].Cells["_difMIcpColumn"].Value);
                    def.DIFM_IDO = Convert.ToDouble(this._difMDataGrid.Rows[i].Cells["_difMIdoColumn"].Value);
                    def.DIFM_IB = Convert.ToDouble(this._difMDataGrid.Rows[i].Cells["_difMIbColumn"].Value);
                    def.DIFM_F = Convert.ToUInt16(this._difMDataGrid.Rows[i].Cells["_difMfColumn"].Value);
                    def.DIFM_OCH = this._difMDataGrid.Rows[i].Cells["_difMOchColumn"].Value.ToString();
                    def.DIFM_T_OCH = Convert.ToInt32(this._difMDataGrid.Rows[i].Cells["_difMtochColumn"].Value);
                    def.DIFM_ENTER_OCH = this._difMDataGrid.Rows[i].Cells["_difMEnterOchColumn"].Value.ToString();
                    def.DIFM_I_OCH = Convert.ToDouble(this._difMDataGrid.Rows[i].Cells["_difMIochColumn"].Value);
                    def.DIFM_OSC = this._difMDataGrid.Rows[i].Cells["_difMOscColumn"].Value.ToString();
                    def.DIFM_UROV = this._difMDataGrid.Rows[i].Cells["_difMUrovColumn"].Value.ToString();
                    //Пуск от По
                    if (i == 0)
                        def.DefenceInstantaneousRunForPo = this._instantaneousRunForPoId1.Checked;
                    if (i == 1)
                        def.DefenceInstantaneousRunForPo = this._instantaneousRunForPoId2.Checked;
                    defense.DiffMgn[i] = def;
                }
            }
            catch
            {
                throw new InvalidOperationException("Найдены некорректные уставки в защитах по мгновенным значениям");
            }

            #endregion [Мгновенные]

            #region [МТЗ]

            try
            {
                for (int i = 0; i < defense.MtzDefStruct.Length; i++)
                {
                    var def = new MtzDefStruct();
                    def.MTZ_MODE = this._MTZDifensesDataGrid.Rows[i].Cells["_mtzModesColumn"].Value.ToString();
                    def.MTZ_BLOCK = this._MTZDifensesDataGrid.Rows[i].Cells["_mtzBlockingColumn"].Value.ToString();
                    def.MTZ_MEASURE = this._MTZDifensesDataGrid.Rows[i].Cells["_mtzMeasureColumn"].Value.ToString();
                    def.MTZ_ICP = Convert.ToDouble(this._MTZDifensesDataGrid.Rows[i].Cells["_mtzICPColumn"].Value);
                    def.Logic = this._MTZDifensesDataGrid.Rows[i].Cells["_logicImaxColumn"].Value.ToString();
                    def.MTZ_CHAR = this._MTZDifensesDataGrid.Rows[i].Cells["_mtzCharColumn"].Value.ToString();
                    def.MTZ_T = Convert.ToInt32(this._MTZDifensesDataGrid.Rows[i].Cells["_mtzTColumn"].Value);
                    def.MTZ_K = Convert.ToUInt16(this._MTZDifensesDataGrid.Rows[i].Cells["_mtzkColumn"].Value);
                    def.MTZ_OSC = this._MTZDifensesDataGrid.Rows[i].Cells["_mtzOscColumn"].Value.ToString();
                    def.MTZ_UROV = this._MTZDifensesDataGrid.Rows[i].Cells["_mtzUROVColumn"].Value.ToString();
                    defense.MtzDefStruct[i] = def;
                }
            }
            catch
            {
                throw new InvalidOperationException("Найдены некорректные уставки в защитах I");
            }

            #endregion [МТЗ]

            #region [Внешние]

            try
            {
                for (int i = 0; i < defense.MtzExt.Length; i++)
                {
                    var def = new ExtDefStruct();
                    def.EXTERNAL_MODE =
                        this._externalDifensesDataGrid.Rows[i].Cells["_externalDifModesColumn"].Value.ToString();
                    def.EXTERNAL_SRAB =
                        this._externalDifensesDataGrid.Rows[i].Cells["_externalDifSrabColumn"].Value.ToString();
                    def.EXTERNAL_OTKL =
                        this._externalDifensesDataGrid.Rows[i].Cells["_externalDifOtklColumn"].Value.ToString();
                    def.EXTERNAL_BLOCK =
                        this._externalDifensesDataGrid.Rows[i].Cells["_externalDifBlockingColumn"].Value.ToString();
                    def.EXTERNAL_TSR =
                        Convert.ToInt32(this._externalDifensesDataGrid.Rows[i].Cells["_externalDifTsrColumn"].Value);
                    def.EXTERNAL_TVZ =
                        Convert.ToInt32(this._externalDifensesDataGrid.Rows[i].Cells["_externalDifTvzColumn"].Value);
                    def.EXTERNAL_VOZVR =
                        this._externalDifensesDataGrid.Rows[i].Cells["_externalDifVozvrColumn"].Value.ToString();
                    def.EXTERNAL_VOZVR_YN =
                        this._externalDifensesDataGrid.Rows[i].Cells["_externalDifVozvrYNColumn"].Value.ToString();
                    def.EXTERNAL_OSC = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifOscColumn"].Value.ToString();
                    def.EXTERNAL_UROV =
                        this._externalDifensesDataGrid.Rows[i].Cells["_externalDifUROVColumn"].Value.ToString();

                    defense.MtzExt[i] = def;
                }
            }
            catch
            {
                throw new InvalidOperationException("Найдены некорректные уставки во внешних защитах");
            }

            #endregion

            return defense;
        }

        #endregion

        #region Группа уставок и Сброс индикации

        public void ReadInputsygnal()
        {
            this._grUstComboBox.Text = this._currentConfig.Impsg.GrUst;
            this._indComboBox.Text = this._currentConfig.Impsg.SbInd;
        }

        public void WriteInputsygnal()
        {
            InputSignalsConfigStruct input = new InputSignalsConfigStruct
            {
                GrUst = this._grUstComboBox.SelectedItem.ToString(),
                SbInd = this._indComboBox.SelectedItem.ToString()
            };
            this._currentConfig.Impsg = input;

        }

        #endregion

        #region Обработчики событий

        private void Configuration_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig)
                this.LoadConfigurationBlocks();
        }

        private void AllWriteEndFail()
        {
            this._configProgressBar.Value = this._configProgressBar.Maximum;
            this._processLabel.Text = WRITE_FAIL;
            MessageBox.Show(WRITE_FAIL);
            this.IsProcess = false;
        }

        private void AllWriteEnd()
        {
            this._processLabel.Text = WRITE_OK;
            MessageBox.Show(WRITE_OK);
            this.IsProcess = false;
        }

        private void AllReadEndFail()
        {
            this._configProgressBar.Value = this._configProgressBar.Maximum;
            this._processLabel.Text = READ_FAIL;
            MessageBox.Show(READ_FAIL);
            this.IsProcess = false;
        }

        private void AllReadEnd()
        {
            this._processLabel.Text = READ_OK;
            this._currentConfig = this._configuration.Value;
            this.ShowConfiguration();
            MessageBox.Show(READ_OK);
            this.IsProcess = false;
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.LoadConfigurationBlocks();
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void DataGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                DataGridViewComboBoxEditingControl combo = e.Control as DataGridViewComboBoxEditingControl;
                if (combo.Items[combo.Items.Count - 1].ToString() == "ХХХХХ")
                {
                    combo.Items.RemoveAt(combo.Items.Count - 1);
                }
            }
        }

        private void _mainRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (this._mainRadioButton.Checked)
            {
                this._currentConfig.AllDefences.ReserveGroup = this.WriteCurrentProtMain();
                this.ReadCurrentProtMain(this._currentConfig.AllDefences.MainGroup);
                this._groupChangeButton.Text = "Основная -> Резервная";
            }
        }

        private void _reserveRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (this._reserveRadioButton.Checked)
            {
                this._currentConfig.AllDefences.MainGroup = this.WriteCurrentProtMain();
                this.ReadCurrentProtMain(this._currentConfig.AllDefences.ReserveGroup);
                this._groupChangeButton.Text = "Резервная -> Основная";
            }
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveinFile();
        }

        private void _joinData_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex != 7)
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        private void _MTZDifensesDataGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex != 7)
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        private void _externalDifensesDataGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if ((e.ColumnIndex != 6) & (e.ColumnIndex != 5))
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        private void _outputReleGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if ((e.ColumnIndex != 3))
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        private void _DZHTUrov1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var tb = sender as MaskedTextBox;
            if (tb == null)
                return;

            string message;
            if (this.TimeValidate(tb.Text, out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        private void _difDDataGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if ((e.ColumnIndex != 5) & (e.ColumnIndex != 15))
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        private void _difMDataGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if ((e.ColumnIndex != 9))
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        private void _UROVJoinData_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if ((e.ColumnIndex != 2))
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        private void _oscLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            int osclen = Common.VersionConverter(this._device.DeviceVersion) < 2.0 ? 26168*2 : 55751*2;
            int index = this._oscLength.SelectedIndex;
            this._oscSizeTextBox.Text = ((int) (osclen/(index + 2))).ToString();
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtml();
        }

        private void SaveToHtml()
        {
            try
            {
                if (this.WriteConfiguration())
                {
                    this._currentConfig.DeviceVersion = Common.VersionConverter(this._device.DeviceVersion);
                    this._currentConfig.DeviceNumber = this._device.DeviceNumber.ToString();
                    this._processLabel.Text = HtmlExport.Export(Resources.MR902MainV203,
                        Resources.MR902ResV203, this._currentConfig, this._currentConfig.DeviceType,
                        this._device.DeviceVersion);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения");
            }
        }

        private void Combo_DropDown(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox) sender;
            combo.DropDown -= new EventHandler(this.Combo_DropDown);
            if (combo.Items.Contains("ХХХХХ"))
            {
                combo.Items.Remove("ХХХХХ");
            }
        }

        private void Combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox) sender;
            combo.SelectedIndexChanged -= new EventHandler(this.Combo_SelectedIndexChanged);
            if (combo.Items.Contains("ХХХХХ"))
            {
                combo.Items.Remove("ХХХХХ");
            }
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void Mr902ConfigurationFormV203_Activated(object sender, EventArgs e)
        {
            StringData.Version = Common.VersionConverter(this._device.DeviceVersion);
        }

        private void Mr902ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.LoadConfigurationBlocks();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.LoadConfigurationBlocks();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtml();
            }
        }

        #endregion

        #region Дополнительные функции
        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }

        private void LoadConfigurationBlocks()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.IsProcess = true;
            this._configProgressBar.Value = 0;
            this._processLabel.Text = "Идёт чтение";
            this._configuration.LoadStruct();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this.WriteConfiguration())
            {
                if (DialogResult.Yes ==
                    MessageBox.Show("Записать конфигурацию МР902 №" + this._device.DeviceNumber + " ?", "Внимание",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                {
                    this._configProgressBar.Value = 0;
                    this.IsProcess = true;
                    this._processLabel.Text = "Идет запись";
                    this._configuration.SaveStruct();
                }
            }
            else
            {
                MessageBox.Show("Имеются некорректные уставки. Проверьте конфигурацию", "Внимание", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
        }

        private bool WriteConfiguration()
        {
            try
            {
                this.WriteDZH();
                this.WriteALLOSC();
                this.WriteUrovJoinGrid();
                this.WriteJoinGrid();
                this.WriteTt();
                this.WriteParamAutomat();
                this.WriteElssygnal();
                this.WriteInpsignal();
                this.WriteInputsygnal();
                if (this._mainRadioButton.Checked)
                {
                    this._currentConfig.AllDefences.MainGroup = this.WriteCurrentProtMain();
                }
                else
                {
                    this._currentConfig.AllDefences.ReserveGroup = this.WriteCurrentProtMain();
                }
                this._configuration.Value = this._currentConfig;
            }
            catch (InvalidOperationException e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
            catch (Exception)
            {
                MessageBox.Show("Найдены неккоректные уставки");
                return false;
            }
            return true;
        }

        private void ClearCombos(ComboBox[] combos)
        {
            foreach (ComboBox c in combos)
            {
                c.Items.Clear();
            }
        }

        private void SubscriptCombos(ComboBox[] combos)
        {
            foreach (ComboBox combo in combos)
            {
                combo.DropDown += new EventHandler(this.Combo_DropDown);
                combo.SelectedIndexChanged += new EventHandler(this.Combo_SelectedIndexChanged);
                combo.SelectedIndex = 0;
            }
        }
        
        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName);
            }
        }

        private void SaveinFile()
        {
            this._saveConfigurationDlg.FileName = string.Format("МР902_Уставки_версия {0:F1}.bin", this._device.DeviceVersion);
            if (this.WriteConfiguration())
            {
                if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
                {

                    this.Serialize(this._saveConfigurationDlg.FileName);
                }
            }
        }

        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MR902"));
                ushort[] values = this._currentConfig.GetValues();
                XmlElement element = doc.CreateElement("MR902_SET_POINTS");
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                MessageBox.Show(string.Format("Файл {0} успешно сохранён", binFileName));
            }
            catch
            {
                MessageBox.Show("Невозможно сохранить файл");
            }
        }

        private void ShowConfiguration()
        {
            this.ReadOsc();
            this.ReadOSCCHANNELS();
            this.ReadDZH();
            this.ReadUrovJoinGrid();
            this.ReadJoinGrid();
            this.ReadInputsygnal();
            this.ReadInpsignal();
            this.ReadElssygnal();
            this.ReadTt();
            if (this._mainRadioButton.Checked)
            {
                this.ReadCurrentProtMain(this._currentConfig.AllDefences.MainGroup);
            }
            if (this._reserveRadioButton.Checked)
            {
                this.ReadCurrentProtMain(this._currentConfig.AllDefences.ReserveGroup);
            }
            this.ReadPARAMAUTOMAT();
        }

        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);

                XmlNode a = doc.FirstChild.SelectSingleNode("MR902_SET_POINTS");
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);
                if (this._currentConfig.GetSize() - this._currentConfig.NetConfSize < values.Length)
                {
                    List<byte> list = new List<byte>();
                    list.AddRange(values.Take(this._currentConfig.AboveNetSize));
                    list.AddRange(values.Skip(this._currentConfig.AboveNetSize + this._currentConfig.NetConfSize));
                    values = list.ToArray();
                }
                this._currentConfig.InitStruct(values);
                this.ShowConfiguration();
                MessageBox.Show(string.Format("Файл {0} успешно загружен", binFileName));
            }
            catch
            {
                MessageBox.Show("Невозможно загрузить файл");
            }
        }

        private bool TimeValidate(string time, out string message)
        {
            message = string.Empty;
            uint value;

            bool flag = uint.TryParse(time, out value);

            if ((value > 3276700) | (!flag))
            {
                message = "Поле должно содержать положительное целое число <= 3.276.700";
                return false;
            }

            if (value == 0)
            {
                return true;
            }

            if (((value >= 1000000) & (value%100 != 0)) | ((value < 1000000) & (value%10 != 0)))
            {
                message = "Для значений меньше 1.000.000 значение должно быть кратно 10, для больших кратно 100";
                return false;
            }

            return true;
        }

        #endregion

        private void _groupChangeButton_Click(object sender, EventArgs e)
        {
            var res = MessageBox.Show("Вы уверены, что хотите копировать уставки?", _groupChangeButton.Text, MessageBoxButtons.YesNo);
            if (res == DialogResult.Yes)
            {
                if (this._mainRadioButton.Checked)
                {
                    this._currentConfig.AllDefences.ReserveGroup = this.WriteCurrentProtMain();
                }
                else
                {
                    this._currentConfig.AllDefences.MainGroup = this.WriteCurrentProtMain();
                }
            }
        }
    }
}
