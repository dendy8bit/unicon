﻿using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR902.Old.ConfigurationV203
{
    /// <summary>
    /// Уставки осциллографа
    /// </summary>
    public class OscilloscopeSettingsStructV203 : StructBase
    {
        [Layout(0)] private ushort config;				        //0 - фиксация по первой аварии 1 - фиксация по последней аварии
        [Layout(1)] private ushort size;				            //размер осциллограмы
        [Layout(2)] private ushort percent;			            //процент от размера осциллограммы
        [Layout(3, Count = 8)] public ushort[] kanal;	        //конфигурация канала осциллографирования
        [Layout(4)] private ushort inpStart;                     //вход пуска
        [Layout(5, Count = 4)] private ushort[] rez;

        #region Конфигурация Осциллографа
        public string OSC_LENGTH
        {
            get
            {
                if (this.size == 0)
                {
                  return  StringData.OscLength[0];
                }
                return StringData.OscLength[this.size - 1];
            }
            set
            {
                this.size = (ushort)(StringData.OscLength.IndexOf(value) + 1);
            }
        }

        public ushort OSC_W_LENGTH
        {
            get
            {
                return (ushort)(this.percent);
            }
            set
            {
                this.percent = (ushort)(value);
            }
        }

        public string OSC_FIX
        {
            get
            {
                return StringData.OscFix[this.config];
            }
            set
            {
                this.config = (ushort)StringData.OscFix.IndexOf(value);
            }
        }
        #endregion

        public string[] Channels
        {
            get { return this.kanal.Select(o => StringData.SignalSrab[o]).ToArray(); }
            set
            {
                for (int i = 0; i < value.Length; i++)
                {

                    this.kanal[i] = (ushort) StringData.SignalSrab.IndexOf(value[i]);
                }
            }
        }

        public string InpStart
        {
            get { return Validator.Get(this.inpStart, StringData.SignalSrab); }
            set { this.inpStart = Validator.Set(value, StringData.SignalSrab); }
        }
    }
}