﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR902.Old.ConfigurationV203
{
    /// <summary>
    /// конфигурация цепей ТТ
    /// </summary>
    public class AllConfigTtStructV203 : StructBase
    {
        public const int TT_COUNT = 3;
        [Layout(0, Count = TT_COUNT)] private ControlTtV203[] _ttChannels;

        public ControlTtV203[] TtChannels
        {
            get { return this._ttChannels; }
            set { this._ttChannels = value; }
        }
    }
}
