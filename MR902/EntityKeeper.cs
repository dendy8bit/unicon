﻿using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MR902.Old.Configuration.Structures.Primary;
using BEMN.MR902.Old.HelpClasses;
using BEMN.MR902.Old.Structures.JournalStructures;
using BEMN.MR902.Old.Structures.MeasuringStructures;
using BEMN.MR902.Old.Structures.OscillogramStruct;
using BEMN.MR902.Old.SystemJournal;

namespace BEMN.MR902
{
    public class EntityKeeper
    {
        private const int DISCRET_DATABASE_START_ADRESS = 0xD00;
        private const int ANALOG_DATABASE_START_ADRESS = 0x0E00;
        private const int DATE_TIME_START_ADRESS = 0x200;
        private const int SYSTEM_JOURNAL_START_ADRESS = 0x600;
        private const int ALARM_JOURNAL_START_ADRESS = 0x700;
        private const int CURRENT_CONNECTIONS_START_ADRESS = 0x1028;
        private const int OSC_JOURNAL_START_ADRESS = 0x800;
        private const int OSC_OPTIONS_START_ADRESS = 0x5a0;

        private MemoryEntity<AnalogDataBaseStruct> _analogDataBase;
        private MemoryEntity<DiscretDataBaseStruct> _discretDataBase;
        private MemoryEntity<DateTimeStruct> _dateTime;
        private MemoryEntity<OneWordStruct> _refreshSystemJournal;
        private MemoryEntity<SystemJournalStruct> _systemJournal;
        private MemoryEntity<JournalRefreshStruct> _refreshAlarmJournal;
        private MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        private MemoryEntity<AllConnectionsStruct> _connectionsMeasuring;
        private MemoryEntity<JournalRefreshStruct> _refreshOscJournal;
        private MemoryEntity<OscJournalStruct> _oscJournal;
        private MemoryEntity<OscOptionsStruct> _oscOptions;
        private MemoryEntity<SetOscStartPageStruct> _setOscStartPage;
        private MemoryEntity<OscPage> _oscPage;
        private MemoryEntity<OscilloscopeSettingsStruct> _oscilloscopeSettings;
        /// <summary>
        /// Загрузчик уставок токов
        /// </summary>
        private CurrentOptionsLoader _currentOptionsLoader;


        public EntityKeeper(Mr902 device)
        {
            this._analogDataBase = new MemoryEntity<AnalogDataBaseStruct>("Аналоговая БД", device, ANALOG_DATABASE_START_ADRESS);
            this._discretDataBase = new MemoryEntity<DiscretDataBaseStruct>("Дискретная БД", device, DISCRET_DATABASE_START_ADRESS);
            this._dateTime = new MemoryEntity<DateTimeStruct>("Дата и время", device, DATE_TIME_START_ADRESS);
            this._systemJournal = new MemoryEntity<SystemJournalStruct>("Журнал системы", device, SYSTEM_JOURNAL_START_ADRESS);
            this._refreshSystemJournal = new MemoryEntity<OneWordStruct>("Обновление журнала системы", device, SYSTEM_JOURNAL_START_ADRESS);
            this._alarmJournal = new MemoryEntity<AlarmJournalRecordStruct>("Журнал аварий", device, ALARM_JOURNAL_START_ADRESS);
            this._refreshAlarmJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала аварий", device, ALARM_JOURNAL_START_ADRESS);
            this._connectionsMeasuring = new MemoryEntity<AllConnectionsStruct>("Токи присоединений для измерений", device, CURRENT_CONNECTIONS_START_ADRESS);

            this._refreshOscJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала осциллографа", device, OSC_JOURNAL_START_ADRESS);
            this._oscJournal = new MemoryEntity<OscJournalStruct>("Журнал осциллографа", device, OSC_JOURNAL_START_ADRESS);
            this._oscOptions = new MemoryEntity<OscOptionsStruct>("Настройки осциллографа", device, OSC_OPTIONS_START_ADRESS);
            this._setOscStartPage = new MemoryEntity<SetOscStartPageStruct>("Установка стартовой страницы осциллограммы", device, 0x900);
            this._oscPage = new MemoryEntity<OscPage>("Страница осциллографа", device, 0x900);
            this._oscilloscopeSettings = new MemoryEntity<OscilloscopeSettingsStruct>("Уставки осциллографа", device, 0x108c);

            this._currentOptionsLoader = new CurrentOptionsLoader(device);
        }

        public MemoryEntity<AnalogDataBaseStruct> AnalogDataBase
        {
            get { return _analogDataBase; }
        }

        public MemoryEntity<DiscretDataBaseStruct> DiscretDataBase
        {
            get { return _discretDataBase; }
        }

        public MemoryEntity<DateTimeStruct> DateTime
        {
            get { return _dateTime; }
        }

        public MemoryEntity<OneWordStruct> RefreshSystemJournal
        {
            get { return _refreshSystemJournal; }
        }

        public MemoryEntity<SystemJournalStruct> SystemJournal
        {
            get { return _systemJournal; }
        }

        public MemoryEntity<JournalRefreshStruct> RefreshAlarmJournal
        {
            get { return _refreshAlarmJournal; }
        }

        public MemoryEntity<AlarmJournalRecordStruct> AlarmJournal
        {
            get { return _alarmJournal; }
        }

        /// <summary>
        /// Загрузчик уставок токов
        /// </summary>
        public CurrentOptionsLoader CurrentOptionsLoader
        {
            get { return _currentOptionsLoader; }
        }

        public MemoryEntity<AllConnectionsStruct> ConnectionsMeasuring
        {
            get { return _connectionsMeasuring; }
        }

        public MemoryEntity<JournalRefreshStruct> RefreshOscJournal
        {
            get { return _refreshOscJournal; }
        }

        public MemoryEntity<OscJournalStruct> OscJournal
        {
            get { return _oscJournal; }
        }

        public MemoryEntity<OscOptionsStruct> OscOptions
        {
            get { return _oscOptions; }
        }

        public MemoryEntity<SetOscStartPageStruct> SetOscStartPage
        {
            get { return _setOscStartPage; }
        }

        public MemoryEntity<OscPage> OscPage
        {
            get { return _oscPage; }
        }

        public MemoryEntity<OscilloscopeSettingsStruct> OscilloscopeSettings
        {
            get { return _oscilloscopeSettings; }
        }
    }
}
