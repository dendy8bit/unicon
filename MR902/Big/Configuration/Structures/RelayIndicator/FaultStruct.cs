using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;

namespace BEMN.MR902.Big.Configuration.Structures.RelayIndicator
{
    /// <summary>
    /// ���� �������������
    /// </summary>
    public class FaultStruct : StructBase
    {
        /// <summary>
        /// ���� �������������
        /// </summary>
        [Layout(0)] private ushort _disrepair;
        /// <summary>
        /// ������� ���� �������������
        /// </summary>
        [Layout(1)] private ushort _disrepairImp;

        #region [Properties]

        /// <summary>
        /// ������������� 1
        /// </summary>
        [BindingProperty(0)]
        [XmlElement("�������������_1")]
        public bool Fault1
        {
            get { return Common.GetBit(this._disrepair, 0); }
            set { this._disrepair = Common.SetBit(this._disrepair, 0, value); }
        }

        /// <summary>
        /// ������������� 2
        /// </summary>
        
        [BindingProperty(1)]
        [XmlElement("�������������_2")]
        public bool Fault2
        {
            get { return Common.GetBit(this._disrepair, 1); }
            set { this._disrepair = Common.SetBit(this._disrepair, 1, value); }
        }

        /// <summary>
        /// ������������� 3
        /// </summary>
        [BindingProperty(2)]
        [XmlElement("�������������_3")]
        public bool Fault3
        {
            get { return Common.GetBit(this._disrepair, 2); }
            set { this._disrepair = Common.SetBit(this._disrepair, 2, value); }
        }

        /// <summary>
        /// ������������� 4
        /// </summary>
        [BindingProperty(3)]
        [XmlElement("�������������_4")]
        public bool Fault4
        {
            get { return Common.GetBit(this._disrepair, 3); }
            set { this._disrepair = Common.SetBit(this._disrepair, 3, value); }
        }

        /// <summary>
        /// ������� ���� �������������
        /// </summary>
        [XmlElement("�������_����_�������������")]
        [BindingProperty(5)]
        public int FaultTime
        {
            get { return ValuesConverterCommon.GetWaitTime(this._disrepairImp); }
            set { this._disrepairImp = ValuesConverterCommon.SetWaitTime(value); }
        }

        #endregion [Properties]
    }
}