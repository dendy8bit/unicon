using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR902.Big.Configuration.Structures.RelayIndicator
{
    /// <summary>
    /// ��� ����
    /// </summary>
    public class AllReleBigStruct : StructBase, IDgvRowsContainer<ReleOutputBigStruct>
    {
        private const int RELAY_MEMORY_COUNT = 96;
        private static int _currentCount;
        public static int CurrentCount => _currentCount;
        /// <summary>
        /// ����
        /// </summary>
        [Layout(0, Count = RELAY_MEMORY_COUNT)]
        private ReleOutputBigStruct[] _relays;

        /// <summary>
        /// ����
        /// </summary>
        [XmlArray(ElementName = "���_����")]
        public ReleOutputBigStruct[] Rows
        {
            get { return this._relays; }
            set { this._relays = value; }
        }

        public static void SetDeviceRelaysType(string type)
        {
            switch (type)
            {
                case "A1":
                case "A4":
                    _currentCount = 42;
                    break;
                case "A2":
                    _currentCount = 34;
                    break;
                case "A3":
                    _currentCount = 50;
                    break;
                default:
                    _currentCount = 18;
                    break;
            }
        }
    }
}