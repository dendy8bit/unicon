﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR902.Big.Configuration.Structures.ConfigSystem
{
    public class ConfigIgSsh : StructBase
    {
        [Layout(0)] private ushort _id2ssh2; //конфигурация индикатора Iд2СШ2
        [Layout(1)] private ushort _config;  //конфигурация UART-C: 0-Ethernet, 1-rs485

        [BindingProperty(0)]
        public string IndConf
        {
            get { return Validator.Get(this._id2ssh2, StringData.ConfigIgssh, 0, 1); }
            set { this._id2ssh2 = Validator.Set(value, StringData.ConfigIgssh, this._id2ssh2, 0, 1); }
        }
    }
}