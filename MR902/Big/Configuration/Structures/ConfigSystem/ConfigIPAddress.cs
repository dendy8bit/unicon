﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR902.Big.Configuration.Structures.ConfigSystem
{
    public class ConfigIPAddress : StructBase
    {
        [Layout(0)] private ushort _ipLo;
        [Layout(1)] private ushort _ipHi;

        [BindingProperty(0)]
        public ushort IpLo1
        {
            get { return Common.GetBits(this._ipLo, 0, 1, 2, 3, 4, 5, 6, 7); }
            set { this._ipLo = Common.SetBits(this._ipLo, value, 0, 1, 2, 3, 4, 5, 6, 7); }
        }
        [BindingProperty(1)]
        public ushort IpLo2
        {
            get { return (ushort)(Common.GetBits(this._ipLo, 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this._ipLo = Common.SetBits(this._ipLo, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        [BindingProperty(2)]
        public ushort IpHi1
        {
            get { return Common.GetBits(this._ipHi, 0, 1, 2, 3, 4, 5, 6, 7); }
            set { this._ipHi = Common.SetBits(this._ipHi, value, 0, 1, 2, 3, 4, 5, 6, 7); }
        }

        [BindingProperty(3)]
        public ushort IpHi2
        {
            get { return (ushort)(Common.GetBits(this._ipHi, 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this._ipHi = Common.SetBits(this._ipHi, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }
    }
}
