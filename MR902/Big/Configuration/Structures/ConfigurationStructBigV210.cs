﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR902.Big.Configuration.Structures.ConfigSystem;
using BEMN.MR902.Big.Configuration.Structures.Connections;
using BEMN.MR902.Big.Configuration.Structures.Defenses;
using BEMN.MR902.Big.Configuration.Structures.Ls;
using BEMN.MR902.Big.Configuration.Structures.Osc;
using BEMN.MR902.Big.Configuration.Structures.RelayIndicator;
using BEMN.MR902.Big.Configuration.Structures.Tt;
using BEMN.MR902.Big.Configuration.Structures.Urov;
using BEMN.MR902.Big.Configuration.Structures.Vls;

namespace BEMN.MR902.Big.Configuration.Structures
{
    [XmlRoot(ElementName = "МР902")]
    public class ConfigurationStructBigV210 : StructBase
    {
        [XmlElement(ElementName = "Версия")]
        public double DeviceVersion { get; set; }

        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }

        [XmlElement(ElementName = "Тип_устройства")]
        public string Device { get { return "МР902"; } set { } }

        [XmlElement(ElementName = "Тип_конфигурации_устройства")]
        public string DeviceType { get; set; }

        [XmlElement(ElementName = "Количество_присоединений")]
        public int CountConnection
        {
            get { return AllConnectionBigStruct.ConnectionsCount; }
            set { }
        }

        [XmlElement(ElementName = "Количество_реле")]
        public int CountRelay
        {
            get { return AllReleBigStruct.CurrentCount; }
            set { }
        }

        [XmlElement(ElementName = "Количество_осц_мс")]
        public string OscCount { get; set; }


        private static string _deviceModelType = string.Empty;
        public static string DeviceModelType
        {
            get { return _deviceModelType; }
            set
            {
                _deviceModelType = value;
                InputLogicBigStruct.SetDeviceDiscretsType(value);
                AllReleBigStruct.SetDeviceRelaysType(value);
                AllUrovConnectionBigStruct.SetDeviceConnectionsType(value);
                AllConnectionBigStruct.SetDeviceConnectionsType(value);
            }
        }
        
        [Layout(0)] private UrovNewStruct _urov;
        [Layout(1)] private AllUrovConnectionBigStruct _allUrovConnection;
        [Layout(2)] private AllConnectionBigStruct _allConnection;
        [Layout(3)] private InputSignalBigStruct _inputSignal;
        [Layout(4)] private OscilloscopeSettingsBigStruct _oscilloscopeSettings;
        [Layout(5)] private ControlTtUnionStruct _controlTt;
        [Layout(6)] private AllInputLogicSignalBigStruct _allInputLogicSignal;
        [Layout(7)] private AllOutputLogicSignalBigStruct _allOutputLogicSignal;
        [Layout(8)] private AllDefensesSetpointsNewStruct _allDefensesSetpoints;
        [Layout(9)] private AllReleBigStruct _allRele;
        [Layout(10)] private AllIndicatorsBigStruct _allIndicators;
        [Layout(11)] private FaultStruct _fault;
        [Layout(12, Ignore = true, Count = 52)] private ushort[] rez;
        [Layout(13)] private ConfigIPAddress _ipAddress;

        [XmlElement(ElementName = "Уров")]
        [BindingProperty(0)]
        public UrovNewStruct Urov
        {
            get { return this._urov; }
            set { this._urov = value; }
        }
        [XmlElement(ElementName = "Уров_Присоединения")]
        [BindingProperty(1)]
        public AllUrovConnectionBigStruct AllUrovConnection
        {
            get { return this._allUrovConnection; }
            set { this._allUrovConnection = value; }
        }
        [XmlElement(ElementName = "Присоединения")]
        [BindingProperty(2)]
        public AllConnectionBigStruct AllConnection
        {
            get { return this._allConnection; }
            set { this._allConnection = value; }
        }
        [XmlElement(ElementName = "Входные_сигналы")]
        [BindingProperty(3)]
        public InputSignalBigStruct InputSignal
        {
            get { return this._inputSignal; }
            set { this._inputSignal = value; }
        }
        [XmlElement(ElementName = "Конфигурация_осциллографа")]
        [BindingProperty(4)]
        public OscilloscopeSettingsBigStruct OscilloscopeSettings
        {
            get { return this._oscilloscopeSettings; }
            set { this._oscilloscopeSettings = value; }
        }
        [XmlElement(ElementName = "Цепи_ТТ")]
        [BindingProperty(5)]
        public ControlTtUnionStruct ControlTt
        {
            get { return this._controlTt; }
            set { this._controlTt = value; }
        }
        [XmlElement(ElementName = "Конфигурация_входных_сигналов")]
        [BindingProperty(6)]
        public AllInputLogicSignalBigStruct AllInputLogicSignal
        {
            get { return this._allInputLogicSignal; }
            set { this._allInputLogicSignal = value; }
        }
        [XmlElement(ElementName = "Все_ВЛС")]
        [BindingProperty(7)]
        public AllOutputLogicSignalBigStruct AllOutputLogicSignal
        {
            get { return this._allOutputLogicSignal; }
            set { this._allOutputLogicSignal = value; }
        }
        [XmlElement(ElementName = "Защиты")]
        [BindingProperty(8)]
        public AllDefensesSetpointsNewStruct AllDefensesSetpoints
        {
            get { return this._allDefensesSetpoints; }
            set { this._allDefensesSetpoints = value; }
        }
        [XmlElement(ElementName = "Реле")]
        [BindingProperty(9)]
        public AllReleBigStruct AllRele
        {
            get { return this._allRele; }
            set { this._allRele = value; }
        }
        [XmlElement(ElementName = "Индикаторы")]
        [BindingProperty(10)]
        public AllIndicatorsBigStruct AllIndicators
        {
            get { return this._allIndicators; }
            set { this._allIndicators = value; }
        }
        [XmlElement(ElementName = "Реле_неисправности")]
        [BindingProperty(11)]
        public FaultStruct Fault
        {
            get { return this._fault; }
            set { this._fault = value; }
        }
        [XmlElement(ElementName = "IP")]
        [BindingProperty(12)]
        public ConfigIPAddress IP
        {
            get { return this._ipAddress; }
            set { this._ipAddress = value; }
        }
    }
}
