﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR902.Big.Configuration.Structures.Ls
{
    /// <summary>
    /// Конфигурациия входных логических сигналов
    /// </summary>
    [XmlRoot(ElementName = "Конфигурация_одного_ЛС")]
    public class InputLogicBigStruct : StructBase, IXmlSerializable
    {
        private static int _discretsCount;
        public static int DiscrestCount => _discretsCount;

        #region [Private fields]

        [Layout(0)] ushort _a1;
        [Layout(1)] ushort _a2;
        [Layout(2)] ushort _a3;
        [Layout(3)] ushort _a4;
        [Layout(4)] ushort _a5;
        [Layout(5)] ushort _a6;
        [Layout(6)] ushort _a7;
        [Layout(7)] ushort _a8;
        [Layout(8)] ushort _a9;
        [Layout(9)] ushort _a10;
        [Layout(10)] ushort _a11;
        [Layout(11)] ushort _a12;

        #endregion [Private fields]

        [XmlIgnore]
        private ushort[] Mass
        {
            get
            {
                return new[]
                {
                    this._a1,
                    this._a2,
                    this._a3,
                    this._a4,
                    this._a5,
                    this._a6,
                    this._a7,
                    this._a8,
                    this._a9,
                    this._a10,
                    this._a11,
                    this._a12
                };
            }
            set
            {
                this._a1 = value[0];
                this._a2 = value[1];
                this._a3 = value[2];
                this._a4 = value[3];
                this._a5 = value[4];
                this._a6 = value[5];
                this._a7 = value[6];
                this._a8 = value[7];
                this._a9 = value[8];
                this._a10 = value[9];
                this._a11 = value[10];
                this._a12 = value[11];
            }
        }

        [XmlIgnore]
        public ushort[] LogicSignal
        {
            get
            {
                ushort[] result = new ushort[_discretsCount];

                ushort[] sourse = this.Mass;
                IEnumerator enumerator = sourse.GetEnumerator();
                int j = 0;
                while (j < _discretsCount)
                {
                    enumerator.MoveNext();
                    ushort temp = (ushort) enumerator.Current;

                    for (int i = 0; i < 16; i += 2)
                    {
                        result[j] = (ushort) (Common.GetBits(temp, i, i + 1) >> i);
                        j++;
                    }
                }
                return result;
            }

            set
            {
                int j = 0;
                List<ushort> result = new List<ushort>();
                while (j < _discretsCount)
                {
                    ushort newValue = 0;

                    for (int i = 0; i < 16; i += 2)
                    {
                        ushort bits = value[j];
                        newValue = Common.SetBits(newValue, bits, i, i + 1);
                        j++;
                    }
                    result.Add(newValue);
                }
                this.Mass = result.ToArray();
            }
        }

        [BindingProperty(0)]
        [XmlIgnore]
        public string this[int ls]
        {
            get
            {
                ushort sourse = this.Mass[ls/8];
                int pos = (ls*2)%16;
                return Validator.Get(sourse, StringData.LogicValues, pos, pos + 1);
            }

            set
            {
                ushort sourse = this.Mass[ls/8];
                int pos = (ls*2)%16;
                sourse = Validator.Set(value, StringData.LogicValues, sourse, pos, pos + 1);
                ushort[] mass = this.Mass;
                mass[ls/8] = sourse;
                this.Mass = mass;
            }
        }

        public static void SetDeviceDiscretsType(string type)
        {
            switch (type)
            {
                case "A1":
                    _discretsCount = 64;
                    break;
                case "A2":
                    _discretsCount = 40;
                    break;
                case "A3":
                    _discretsCount = 24;
                    break;
                case "A4":
                    _discretsCount = 32;
                    break;
                default:
                    _discretsCount = 24;
                    break;
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {

        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {

            for (int i = 0; i < _discretsCount; i++)
            {
                writer.WriteElementString("Дискрет", this[i]);
            }
        }
    }
}
