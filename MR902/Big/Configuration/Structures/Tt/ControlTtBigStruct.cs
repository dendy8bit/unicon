﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR902.Big.Configuration.Structures.Tt
{
    /// <summary>
    /// конфигурация цепей ТТ
    /// </summary>
    public class ControlTtBigStruct : StructBase
    {
        [Layout(0)] private ushort _config; //	выведена, неисправность, неисправность + блокировка
        [Layout(1)] private ushort _ust; //	уставка минимального дифференциального тока
        [Layout(2)] private ushort _time; //	выдержка времени
        [Layout(3)] private ushort _inpReset; // для  СШ1 - вход Сброс неисправности ТТ

        /// <summary>
        /// Iдmin
        /// </summary>
        [XmlElement(ElementName = "Iдmin")]
        [BindingProperty(0)]
        public double Idmin
        {
            get { return ValuesConverterCommon.GetIn(this._ust); }
            set { this._ust = ValuesConverterCommon.SetIn(value); }
        }
        /// <summary>
        /// Tср
        /// </summary>
        [XmlElement(ElementName = "Tср")]
        [BindingProperty(1)]
        public int Tsrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// Неисправность
        /// </summary>
        [XmlElement(ElementName = "Неисправность")]
        [BindingProperty(2)]
        public string Fault
        {
            get { return Validator.Get(this._config, StringData.TtFault, 0, 1); }
            set { this._config = Validator.Set(value, StringData.TtFault, this._config, 0, 1); }
        }
        /// <summary>
        /// Неисправность
        /// </summary>
        [XmlElement(ElementName = "Сброс")]
        [BindingProperty(3)]
        public string Reset
        {
            get { return Validator.Get(this._config, StringData.ResetTT, 2); }
            set { this._config = Validator.Set(value, StringData.ResetTT, this._config, 2); }
        }
        /// <summary>
        /// Вход сброса неисправностей ТТ
        /// </summary>
        public string InpResertFaultTt
        {
            get { return Validator.Get(this._inpReset, StringData.InputSignalsBig); }
            set { this._inpReset = Validator.Set(value, StringData.InputSignalsBig); }
        }
    }
}
