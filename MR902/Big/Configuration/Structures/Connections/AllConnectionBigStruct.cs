﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR902.Big.Configuration.Structures.Connections
{
    /// <summary>
    /// Структура. Все присоединения
    /// </summary>
    public class AllConnectionBigStruct : StructBase, IDgvRowsContainer<ConnectionBigStruct>
    {
        #region [Constants]

        private const int CONNECTIONS_MEMORY_COUNT = 24;
        public static int ConnectionsCount { get; private set; }

        #endregion [Constants]


        #region [Private fields]

        [Layout(0, Count = CONNECTIONS_MEMORY_COUNT)] private ConnectionBigStruct[] _connectionStructs;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Токи присоединений
        /// </summary>
        [XmlIgnore]
        public int[] GetAllItt
        {
            get
            {
                List<ushort> result = ConnectionsCount == 6 
                    ? new List<ushort> { this._connectionStructs.Take(ConnectionsCount-1).Max(o => o.Inom) } 
                    : new List<ushort> { this._connectionStructs.Take(ConnectionsCount).Max(o => o.Inom) };
                result.AddRange(this._connectionStructs.Take(ConnectionsCount).Select(oneStruct => oneStruct.Inom));
                return result.Select(r => r * 40).ToArray();
            }
        }

        #endregion [Properties]

        public ConnectionBigStruct[] Rows
        {
            get { return this._connectionStructs; }
            set { this._connectionStructs = value; }
        }

        public static void SetDeviceConnectionsType(string type)
        {
            switch (type)
            {
                case "A1":
                    ConnectionsCount = 6;
                    break;
                case "A2":
                    ConnectionsCount = 8;
                    break;
                case "A3":
                    ConnectionsCount = 8;
                    break;
                case "A4":
                    ConnectionsCount = 8;
                    break;
                default:
                    ConnectionsCount = 6;
                    break;
            }
        }
    }
}