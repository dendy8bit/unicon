﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR902.Big.Configuration.Structures.Urov
{
    public class UrovNewStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort config;      //конфигурация
        [Layout(1)] private ushort ust;         //уставка срабатывания
        [Layout(2)] private ushort time1;       //время срабатывания
        [Layout(3)] private ushort time2;       //время срабатывания
        [Layout(4)] private ushort time3;       //время срабатывания
        [Layout(5)] private ushort urovSH1;
        [Layout(6)] private ushort urovSH2;
        [Layout(7)] private ushort urovSH12;
        [Layout(8)] private ushort _block;      //блокировка УРОВ
        [Layout(9)] private ushort _res;
        #endregion [Private fields]

        #region ДЗШ

        [XmlElement(ElementName = "Режим")]
        [BindingProperty(0)]
        public string DzhModes
        {
            get { return Validator.Get(this.config, StringData.ModesLight, 0); }
            set { this.config = Validator.Set(value, StringData.ModesLight, this.config, 0); }
        }

        [XmlElement(ElementName = "Контр")]
        [BindingProperty(1)]
        public string DzhControl
        {
            get { return Validator.Get(this.config, StringData.KONTR, 1); }
            set { this.config = Validator.Set(value, StringData.KONTR, this.config, 1); }
        }

        [XmlElement(ElementName = "От_защит")]
        [BindingProperty(2)]
        public string DzhDiff
        {
            get { return Validator.Get(this.config, StringData.Forbidden, 2); }
            set { this.config = Validator.Set(value, StringData.Forbidden, this.config, 2); }
        }

        [XmlElement(ElementName = "На_себя")]
        [BindingProperty(3)]
        public string DzhSelf
        {
            get { return Validator.Get(this.config, StringData.Forbidden, 3); }
            set { this.config = Validator.Set(value, StringData.Forbidden, this.config, 3); }
        }

        [XmlElement(ElementName = "От_сигн")]
        [BindingProperty(4)]
        public string DzhSign
        {
            get { return Validator.Get(this.config, StringData.Forbidden, 4); }
            set { this.config = Validator.Set(value, StringData.Forbidden, this.config, 4); }
        }

        [XmlElement(ElementName = "Т1")]
        [BindingProperty(5)]
        public int DzhT1
        {
            get { return ValuesConverterCommon.GetWaitTime(this.time1); }
            set { this.time1 = ValuesConverterCommon.SetWaitTime(value); }

        }

        [XmlElement(ElementName = "Т2")]
        [BindingProperty(6)]
        public int DzhT2
        {
            get { return ValuesConverterCommon.GetWaitTime(this.time2); }
            set { this.time2 = ValuesConverterCommon.SetWaitTime(value); }
        }

        [XmlElement(ElementName = "Т3")]
        [BindingProperty(7)]
        public int DzhT3
        {
            get { return ValuesConverterCommon.GetWaitTime(this.time3); }
            set { this.time3 = ValuesConverterCommon.SetWaitTime(value); }
        }

        [XmlElement(ElementName = "СШ1")]
        [BindingProperty(8)]
        public string DzhSh1
        {
            get { return Validator.Get(this.urovSH1, StringData.InputSignalsBig); }
            set { this.urovSH1 = Validator.Set(value, StringData.InputSignalsBig); }
        }

        [XmlElement(ElementName = "СШ2")]
        [BindingProperty(9)]
        public string DzhSh2
        {
            get { return Validator.Get(this.urovSH2, StringData.InputSignalsBig); }
            set { this.urovSH2 = Validator.Set(value, StringData.InputSignalsBig); }
        }

        [XmlElement(ElementName = "ПО")]
        [BindingProperty(10)]
        public string DzhPo
        {
            get { return Validator.Get(this.urovSH12, StringData.InputSignalsBig); }
            set { this.urovSH12 = Validator.Set(value, StringData.InputSignalsBig); }
        }

        #endregion
    }
}