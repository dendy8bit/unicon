﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR902.Big.Configuration.Structures.Urov
{
    /// <summary>
    /// конфигурация уров присоединения
    /// </summary>
    public class UrovConnectionNewStruct : StructBase
    {
        [Layout(0)] private ushort _ust;		//ток уров
        [Layout(1)] private ushort _time;		//время уров
        [Layout(2)] private ushort _pusk;
        [Layout(3)] private ushort _res;

        #region Уров Присоединения

        [BindingProperty(0)]
        public double UrovJoinI
        {
            get { return ValuesConverterCommon.GetIn(this._ust); }
            set { this._ust = ValuesConverterCommon.SetIn(value); }
        }
        [BindingProperty(1)]
        public int UrovJoinT
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }

        #endregion
    }
}