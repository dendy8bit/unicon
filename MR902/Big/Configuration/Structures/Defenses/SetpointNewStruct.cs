﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR902.Big.Configuration.Structures.Defenses.Differential;
using BEMN.MR902.Big.Configuration.Structures.Defenses.External;
using BEMN.MR902.Big.Configuration.Structures.Defenses.Mtz;

namespace BEMN.MR902.Big.Configuration.Structures.Defenses
{
    public class SetpointNewStruct : StructBase
    {
        [Layout(0)] private AllDifferentialCurrentBigStruct _allDifferentialCurrent;
        [Layout(1)] private AllMtzBigStruct _allMtz;
        [Layout(2)] private AllExternalDefenseBigStruct _allExternalDefense;

        [BindingProperty(0)]
        [XmlElement(ElementName = "Диф")]
        public AllDifferentialCurrentBigStruct AllDifferentialCurrent
        {
            get { return this._allDifferentialCurrent; }
            set { this._allDifferentialCurrent = value; }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "МТЗ")]
        public AllMtzBigStruct AllMtz
        {
            get { return this._allMtz; }
            set { this._allMtz = value; }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Внешние")]
        public AllExternalDefenseBigStruct AllExternalDefense
        {
            get { return this._allExternalDefense; }
            set { this._allExternalDefense = value; }
        }
    }
}
