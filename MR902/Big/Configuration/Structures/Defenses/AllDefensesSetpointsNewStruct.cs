﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR902.Big.Configuration.Structures.Defenses
{
    /// <summary>
    /// защиты по двум группам уставок
    /// </summary>
    [Serializable]
    public class AllDefensesSetpointsNewStruct : StructBase, ISetpointContainer<SetpointNewStruct>
    {
        #region [Public fields]
        [Layout(0)] private SetpointNewStruct _mainSetpoints;
        [Layout(1)] private SetpointNewStruct _reserveSetpoints;
        #endregion [Public fields]

        [XmlIgnore]
        public SetpointNewStruct[] Setpoints
        {
            get
            {
                return new[]
                {
                    this._mainSetpoints.Clone<SetpointNewStruct>(),
                    this._reserveSetpoints.Clone<SetpointNewStruct>()
                };
            }
            set
            {
                this._mainSetpoints = value[0].Clone<SetpointNewStruct>();
                this._reserveSetpoints = value[1].Clone<SetpointNewStruct>();
            }
        }

        /// <summary>
        /// Основная группа уставок
        /// </summary>
        [XmlElement(ElementName = "Основная_группа_уставок")]
        public SetpointNewStruct MainSetpoints
        {
            get { return this._mainSetpoints; }
            set { this._mainSetpoints = value; }
        }

        /// <summary>
        /// Резервная группа уставок
        /// </summary>
        [XmlElement(ElementName = "Резервная_группа_уставок")]
        public SetpointNewStruct ReserveSetpoints
        {
            get { return this._reserveSetpoints; }
            set { this._reserveSetpoints = value; }
        }
    }
}
