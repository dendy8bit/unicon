﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR902.Big.Configuration.Structures.Defenses.Mtz
{
  public  class AllMtzBigStruct:StructBase, IDgvRowsContainer<MtzBigStruct>
  {
      public const int COUNT = 32;
      #region [Private fields]

      [Layout(0, Count = COUNT)] private MtzBigStruct[] _mtz;

      #endregion [Private fields]

      public MtzBigStruct[] Rows
      {
          get { return this._mtz; }
          set { this._mtz = value; }
      }
    }
}
