﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR902.Big.Configuration.Structures.Defenses.External
{
    public class AllExternalDefenseBigStruct : StructBase, IDgvRowsContainer<ExternalDefenseBigStruct>
    {
        public const int COUNT = 24;
        [Layout(0, Count = COUNT)] private ExternalDefenseBigStruct[] _externalDefenses;

        public ExternalDefenseBigStruct[] Rows
        {
            get { return this._externalDefenses; }
            set { this._externalDefenses = value; }
        }
    }
}
