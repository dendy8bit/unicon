﻿using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR902.Big.Configuration.Structures.Osc
{
    public class AllChannelsStruct : StructBase, IDgvRowsContainer<ChannelBigStruct>
    {
        public const int COUNT = 96;
        [Layout(0, Count = COUNT)] private ChannelBigStruct[] _channel;

        /// <summary>
        /// Каналы
        /// </summary>
        [XmlArray(ElementName = "Все_каналы")]
        public ChannelBigStruct[] Rows
        {
            get { return this._channel; }
            set { this._channel = value; }
        }

        [XmlIgnore]
        public ushort[] Words
        {
            get { return this._channel.Select(c=>c.Channel).ToArray(); }
            set
            {
                for (int i = 0; i < value.Length; i++)
                {
                    this._channel[i].Channel = value[i];
                }
            }
        }
    }

    [XmlType(TypeName = "Один_канал")]
    public class ChannelBigStruct : StructBase
    {
        [Layout(0)] private ushort _channel;

        [BindingProperty(0)]
        [XmlElement("Канал")]
        public string ChannelStr
        {
            get { return Validator.Get(this._channel, StringData.RelaySignalsBig); }
            set { this._channel = Validator.Set(value, StringData.RelaySignalsBig); }
        }

        public ushort Channel
        {
            get { return this._channel; }
            set { this._channel = value; }
        }
    }

}
