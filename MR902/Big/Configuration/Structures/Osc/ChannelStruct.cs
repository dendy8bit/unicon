﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR902.Big.Configuration.Structures.Osc
{
    //[XmlType(TypeName = "Один_канал")]
    public class ChannelStruct : StructBase
    {
        [Layout(0)] private ushort _channel;

        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Канал")]
        public string ChannelStr
        {
            get { return Validator.Get(this._channel, StringData.RelaySignalsBig); }
            set { this._channel = Validator.Set(value, StringData.RelaySignalsBig); }
        }

        public ushort ChannelUshort
        {
            get { return this._channel; }
        }
    }
}
