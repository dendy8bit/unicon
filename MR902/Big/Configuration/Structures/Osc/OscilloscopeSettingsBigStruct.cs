﻿using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR902.Big.Configuration.Structures.Osc
{
    /// <summary>
    /// Уставки осциллографа
    /// </summary>
    public class OscilloscopeSettingsBigStruct : StructBase
    {
        [Layout(0)] private ushort config; //0 - фиксация по первой аварии 1 - фиксация по последней аварии
        [Layout(1)] private ushort size; //размер осциллограмы
        [Layout(2)] private ushort percent; //процент от размера осциллограммы
        [Layout(3)] private AllChannelsStruct _channels; //конфигурация канала осциллографирования
        [Layout(4)] private ushort _inputOsc;
        [Layout(5, Count = 4)] private ushort[] rez;

        [XmlIgnore]
        public ushort[] ChannelsWord
        {
            get { return this._channels.Words; }
            set { this._channels.Words = value; }
        }

        #region Конфигурация Осциллографа
        [BindingProperty(0)]
        [XmlElement(ElementName = "Количество_осциллограмм")]
        public string OscConfig
        {
            get { return Validator.Get((ushort) (this.size - 1), StringData.OscLength); }
            set { this.size = (ushort) (Validator.Set(value, StringData.OscLength) + 1); }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "Предзапись")]
        public ushort OscWLength
        {
            get { return this.percent; }
            set { this.percent = value; }
        }
        [XmlElement(ElementName = "Фиксация")]
        [BindingProperty(2)]
        public string OscFix
        {
            get { return Validator.Get(this.config, StringData.OscFix); }
            set { this.config = Validator.Set(value, StringData.OscFix); }
        }
        [XmlElement(ElementName = "Конфигурация_каналов")]
        [BindingProperty(3)]
        public AllChannelsStruct Channels
        {
            get { return this._channels; }
            set { this._channels = value; }
        }

        [XmlElement(ElementName = "Вход_пуска_осциллографа")]
        [BindingProperty(4)]
        public string InputOsc
        {
            get { return Validator.Get(this._inputOsc, StringData.RelaySignalsBig); }
            set { this._inputOsc = Validator.Set(value, StringData.RelaySignalsBig); }
        }

        [XmlElement(ElementName = "Длительность_осциллограммы")]
        public int OscLength
        {
            get { return 31360*2/(this.size - 1 + 2); }
            set {  }
        }
        #endregion
    }
}