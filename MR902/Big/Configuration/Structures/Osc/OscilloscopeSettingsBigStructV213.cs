﻿using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR902.Big.Configuration.Structures.Osc
{
    public class OscilloscopeSettingsBigStructV213 : StructBase
    {
        [Layout(0)] private ushort config; //0 - фиксация по первой аварии 1 - фиксация по последней аварии
        [Layout(1)] private ushort size; //размер осциллограмы
        [Layout(2)] private ushort percent; //процент от размера осциллограммы
        [Layout(3, Count = 96)] private ushort[] _kanals; //конфигурация канала осциллографирования
        [Layout(4)] private ushort _inputOsc;
        [Layout(5, Count = 4)] private ushort[] rez;

        [XmlIgnore]
        public ushort[] Kanal
        {
            get { return this._kanals; }
            set { this._kanals = value; }
        }

        #region Конфигурация Осциллографа
        [BindingProperty(0)]
        [XmlElement(ElementName = "Количество_осциллограм")]
        public string OscLength
        {
            get
            {
                if (this.size == 0)
                {
                    this.size = 1;
                }
                return Validator.Get((ushort)(this.size - 1), StringData.OscLength);
            }
            set { this.size = (ushort)(Validator.Set(value, StringData.OscLength) + 1); }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "Предзапись")]
        public ushort OscWLength
        {
            get { return this.percent; }
            set { this.percent = value; }
        }
        [XmlElement(ElementName = "Фиксация")]
        [BindingProperty(2)]
        public string OscFix
        {
            get { return Validator.Get(this.config, StringData.OscFix); }
            set { this.config = Validator.Set(value, StringData.OscFix); }
        }

        [XmlElement(ElementName = "Вход_пуска_осциллографа")]
        [BindingProperty(3)]
        public string InputOsc
        {
            get { return Validator.Get(this._inputOsc, StringData.RelaySignalsBig); }
            set { this._inputOsc = Validator.Set(value, StringData.RelaySignalsBig); }
        }

        [XmlElement(ElementName = "Конфигурация_каналов_база")]
        [BindingProperty(4)]
        public AllChannelsWithBase Channel
        {
            get
            {
               var str = new AllChannelsWithBase();
               str.OscChannels = this._kanals.Take(AllChannelsWithBase.KANAL_COUNT).ToArray();
               str.CfgOscChannels = this.rez;
               return str;
            }
            set
            {
                for (int i = 0; i < AllChannelsWithBase.KANAL_COUNT; i++)
                {
                    this._kanals[i] = value.OscChannels[i];
                }
                //зачем ты создавал структуру, когда надо использовать ключевое слово value???
                this.rez = value.CfgOscChannels;
                value.CfgOscChannels = null;
            }
        }

        [XmlElement(ElementName = "Все_каналы")]
        [BindingProperty(5)]
        public ChannelBigStructV213 ChannelStruct
        {
            get
            {
                var str = new ChannelBigStructV213();
                //сначала надо скипнуь все значения, что пошли в каналы с базами
                str.Kanal = this._kanals.Skip(AllChannelsWithBase.KANAL_COUNT).Take(ChannelBigStructV213.COUNT).ToArray();
                return str;
            }
            set
            {
                //никогда не используй Magic значения, если есть воля или константы, которые за это отвечают. их и надо использовать
                for (int i = AllChannelsWithBase.KANAL_COUNT; i < AllChannelsWithBase.KANAL_COUNT+ChannelBigStructV213.COUNT; i++)
                { 
                    this._kanals[i] = value.Kanal[i - AllChannelsWithBase.KANAL_COUNT];
                }
            }
        }

        #endregion
    }
}
