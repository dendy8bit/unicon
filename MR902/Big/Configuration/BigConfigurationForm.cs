﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Forms.TreeView;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR902.Big.Configuration.Structures;
using BEMN.MR902.Big.Configuration.Structures.ConfigSystem;
using BEMN.MR902.Big.Configuration.Structures.Connections;
using BEMN.MR902.Big.Configuration.Structures.Defenses;
using BEMN.MR902.Big.Configuration.Structures.Defenses.Differential;
using BEMN.MR902.Big.Configuration.Structures.Defenses.External;
using BEMN.MR902.Big.Configuration.Structures.Defenses.Mtz;
using BEMN.MR902.Big.Configuration.Structures.Ls;
using BEMN.MR902.Big.Configuration.Structures.Osc;
using BEMN.MR902.Big.Configuration.Structures.RelayIndicator;
using BEMN.MR902.Big.Configuration.Structures.Tt;
using BEMN.MR902.Big.Configuration.Structures.Urov;
using BEMN.MR902.Big.Configuration.Structures.Vls;

namespace BEMN.MR902.Big.Configuration
{
    public partial class BigConfigurationForm : Form, IFormView
    {
        #region [Constants]
        private const string FILE_SAVE_FAIL = "Невозможно сохранить файл";
        private const string FILE_LOAD_FAIL = "Невозможно загрузить файл";
        private const string XML_HEAD = "MR902_SET_POINTS";
        private const string ERROR_SETPOINTS_VALUE = "В конфигурации заданы некорректные значения. Проверьте конфигурацию.";
        private const string INVALID_PORT = "Порт недоступен.";
        private const string READ_OK = "Конфигурация успешно прочитана";
        private const string READ_FAIL = "Не удалось прочитать конфигурацию";
        private const string WRITE_OK = "Конфигурация успешно записана";
        private const string WRITE_FAIL = "Невозможно записать конфигурацию";

        #endregion [Constants]
        
        #region Поля
        
        private Mr902 _device;
        private MemoryEntity<ConfigurationStructBigV210> _configuration;
        private ConfigurationStructBigV210 _currentSetpointsStruct;
        private StructUnion<ConfigurationStructBigV210> _configurationValidator;

        private MemoryEntity<ConfigurationStructBigV213> _configurationV213;
        private ConfigurationStructBigV213 _currentSetpointsStructV213;
        private StructUnion<ConfigurationStructBigV213> _configurationValidatorV213;

        private NewDgwValidatior<AllChannelsWithBase, ChannelWithBase> allChannelsWithBaseV213;
        private NewDgwValidatior<ChannelBigStructV213, ChannelStruct> allChannelsV213;
        private NewStructValidator<OscilloscopeSettingsBigStructV213> _oscValidatorV213;

        private List<CheckedListBox> allVlsCheckedListBoxs = new List<CheckedListBox>();

        #region [Реле и индикаторы]
        private NewDgwValidatior<AllReleBigStruct, ReleOutputBigStruct> _relayValidator;
        private NewDgwValidatior<AllIndicatorsBigStruct, IndicatorsBigStruct> _indicatorValidator;
        private NewStructValidator<FaultStruct> _faultValidator;
        #endregion [Реле и индикаторы]

        #region [ВЛС]
        private NewCheckedListBoxValidator<OutputLogicBigStruct>[] _vlsValidator;
        private StructUnion<AllOutputLogicSignalBigStruct> _vlsUnion;
        /// <summary>
        /// Массив контролов для ВЛС
        /// </summary>
        private CheckedListBox[] _vlsBoxes;
        #endregion [ВЛС]

        #region [Входные сигналы]
        /// <summary>
        /// Массив контролов для ЛС
        /// </summary>
        private DataGridView[] _lsBoxes;
        private NewDgwValidatior<InputLogicBigStruct>[] _inputLogicValidator;
        private StructUnion<AllInputLogicSignalBigStruct> _inputLogicUnion;
        #endregion [Входные сигналы]

        private NewDgwValidatior<AllControlTtBigStruct, ControlTtBigStruct> _ttValidator;
        private NewStructValidator<ControlTtUnionStruct> _newTtValidator;
        private NewStructValidator<UrovNewStruct> _urovValidator;
        private NewStructValidator<InputSignalBigStruct> _inputSignalValidator;
        private NewStructValidator<OscilloscopeSettingsBigStruct> _oscValidator;
        private NewDgwValidatior<AllUrovConnectionBigStruct, UrovConnectionNewStruct> _urovConnectionValidator;
        private NewDgwValidatior<AllConnectionBigStruct, ConnectionBigStruct> _connectionValidator;

        #region [Защиты]
        private NewDgwValidatior<AllDifferentialCurrentBigStruct, DifferentialCurrentBigStruct> _differentialCurrentValidator;
        private NewDgwValidatior<AllMtzBigStruct, MtzBigStruct> _mtzValidator;
        private NewDgwValidatior<AllExternalDefenseBigStruct, ExternalDefenseBigStruct> _externalDefenseValidator;
        private StructUnion<SetpointNewStruct> _setpointValidator;
        private SetpointsValidator<AllDefensesSetpointsNewStruct, SetpointNewStruct> _defensesValidator; 
        #endregion [Защиты]

        
        

        #endregion

        #region Конструкторы

        public BigConfigurationForm()
        {
            this.InitializeComponent();
        }

        public BigConfigurationForm(Mr902 device)
        {
            this.InitializeComponent();
            this._device = device;
            StringData.Version = Common.VersionConverter(this._device.DeviceVersion);

            if (StringData.Version < 2.13)
            {
                ConfigurationStructBigV210.DeviceModelType = this._device.DevicePlant;
                groupBox27.Visible = false; // Конфигурация->Осцилографф->Программируемые дискретные каналы
                _confIndGB.Visible = false; // Конфигурация->Реле и индикаторы->Конфигурация индикатора
            }
            else
            {
                ConfigurationStructBigV213.DeviceModelType = this._device.DevicePlant;
            }


            // общее
            this._device.ConfigWriteOk += HandlerHelper.CreateActionHandler(this, this.ConfigurationWriteOk);
            this._device.ConfigWriteFail += HandlerHelper.CreateActionHandler(this, this.ConfigurationWriteFail);

            // для версии < 2.13
            this._configuration = device.ConfigurationBigV210;
            this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
            this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._device.ConfirmConstraint);
            this._configuration.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._configuration.RemoveStructQueries();
                this.ConfigurationWriteFail();
            });
            this._configuration.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._configuration.RemoveStructQueries();
                this.ConfigurationReadFail();
            });
            this._configuration.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
            this._configuration.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);


            // для версии 2.13
            this._configurationV213 = device.ConfigurationBigV213;
            this._configurationV213.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
            this._configurationV213.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._device.ConfirmConstraint);
            this._configurationV213.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._configurationV213.RemoveStructQueries();
                this.ConfigurationWriteFail();
            });
            this._configurationV213.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._configurationV213.RemoveStructQueries();
                this.ConfigurationReadFail();
            });
            this._configurationV213.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
            this._configurationV213.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);

            this.Init();
        }

        private void Init()
        {
            if (StringData.Version < 2.13) this._progressBar.Maximum = this._configuration.Slots.Count;
            else                           this._progressBar.Maximum = this._configurationV213.Slots.Count;
            this._currentSetpointsStruct = new ConfigurationStructBigV210();
            this._currentSetpointsStructV213 = new ConfigurationStructBigV213();

            #region [Реле и Индикаторы]

            this._relayValidator = new NewDgwValidatior<AllReleBigStruct, ReleOutputBigStruct>
                (
                this._outputReleGrid,
                AllReleBigStruct.CurrentCount,
                this._toolTip,
                new ColumnInfoCombo(StringData.RelayNamesNew, ColumnsType.NAME),
                new ColumnInfoCombo(StringData.SignalType),
                new ColumnInfoCombo(StringData.RelaySignalsBig),
                new ColumnInfoText(RulesContainer.TimeRule)
                );

            this._indicatorValidator = new NewDgwValidatior<AllIndicatorsBigStruct, IndicatorsBigStruct>
                (
                this._outputIndicatorsGrid,
                AllIndicatorsBigStruct.COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringData.IndNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringData.SignalType),
                new ColumnInfoCombo(StringData.RelaySignalsBig),
                new ColumnInfoColor()
                );

            this._faultValidator = new NewStructValidator<FaultStruct>
                (
                this._toolTip,
                new ControlInfoCheck(this._fault1CheckBox),
                new ControlInfoCheck(this._fault2CheckBox),
                new ControlInfoCheck(this._fault3CheckBox),
                new ControlInfoCheck(this._fault4CheckBox),
                new ControlInfoText(this._impTB, RulesContainer.TimeRule)
                );
            #endregion [Реле и Индикаторы]

            #region [ВЛС]

            allVlsCheckedListBoxs.Add(this.VLScheckedListBox1);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox2);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox3);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox4);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox5);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox6);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox7);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox8);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox9);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox10);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox11);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox12);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox13);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox14);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox15);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox16);

            this._vlsBoxes = new[]
            {
                this.VLScheckedListBox1,
                this.VLScheckedListBox2,
                this.VLScheckedListBox3,
                this.VLScheckedListBox4,
                this.VLScheckedListBox5,
                this.VLScheckedListBox6,
                this.VLScheckedListBox7,
                this.VLScheckedListBox8,
                this.VLScheckedListBox9,
                this.VLScheckedListBox10,
                this.VLScheckedListBox11,
                this.VLScheckedListBox12,
                this.VLScheckedListBox13,
                this.VLScheckedListBox14,
                this.VLScheckedListBox15,
                this.VLScheckedListBox16
            };
            this._vlsValidator = new NewCheckedListBoxValidator<OutputLogicBigStruct>[AllOutputLogicSignalBigStruct.LOGIC_COUNT];
            for (int i = 0; i < AllOutputLogicSignalBigStruct.LOGIC_COUNT; i++)
            {
                this._vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicBigStruct>(this._vlsBoxes[i],
                    StringData.VlsSignalsBig);
            }
            this._vlsUnion = new StructUnion<AllOutputLogicSignalBigStruct>(this._vlsValidator);

            #endregion [ВЛС]

            #region [ЛС]

            this._lsBoxes = new[]
            {
                this._inputSignals1, this._inputSignals2, this._inputSignals3, this._inputSignals4,
                this._inputSignals5,this._inputSignals6,this._inputSignals7,
                this._inputSignals8,
                this._inputSignals9,
                this._inputSignals10,
                this._inputSignals11,
                this._inputSignals12,
                this._inputSignals13,
                this._inputSignals14,
                this._inputSignals15,
                this._inputSignals16
            };

            foreach (DataGridView gridView in this._lsBoxes)
            {
                gridView.CellBeginEdit += this.GridOnCellBeginEdit;
            }

            this._inputLogicValidator = new NewDgwValidatior<InputLogicBigStruct>[AllInputLogicSignalBigStruct.LOGIC_COUNT];
            for (int i = 0; i < AllInputLogicSignalBigStruct.LOGIC_COUNT; i++)
            {
                this._inputLogicValidator[i] = new NewDgwValidatior<InputLogicBigStruct>
                    (
                    this._lsBoxes[i],
                    InputLogicBigStruct.DiscrestCount,
                    this._toolTip,
                    new ColumnInfoCombo(StringData.LogicSignalNames, ColumnsType.NAME),
                    new ColumnInfoCombo(StringData.LogicValues)
                    );
            }
            this._inputLogicUnion = new StructUnion<AllInputLogicSignalBigStruct>(this._inputLogicValidator);

            #endregion [ЛС]

            this._ttValidator = new NewDgwValidatior<AllControlTtBigStruct, ControlTtBigStruct>
                (
                this._configTtDgv,
                AllControlTtBigStruct.TT_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringData.TtNames, ColumnsType.NAME),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCombo(StringData.TtFault),
                new ColumnInfoCombo(StringData.ResetTT)
                );

            this._newTtValidator = new NewStructValidator<ControlTtUnionStruct>
                (
                this._toolTip,
                new ControlInfoValidator(this._ttValidator),
                new ControlInfoCombo(this._inpResetTtcomboBox, StringData.InputSignalsBig)
                );

            this._urovValidator = new NewStructValidator<UrovNewStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._DZHModes, StringData.ModesLight),
                new ControlInfoCombo(this._DZHKontr, StringData.KONTR),
                new ControlInfoCombo(this._DZHDiff, StringData.Forbidden),
                new ControlInfoCombo(this._DZHSelf, StringData.Forbidden),
                new ControlInfoCombo(this._DZHSign, StringData.Forbidden),
                new ControlInfoText(this._DZHTUrov1, RulesContainer.TimeRule),
                new ControlInfoText(this._DZHTUrov2, RulesContainer.TimeRule),
                new ControlInfoText(this._DZHTUrov3, RulesContainer.TimeRule),
                new ControlInfoCombo(this._DZHSH1, StringData.InputSignalsBig),
                new ControlInfoCombo(this._DZHSH2, StringData.InputSignalsBig),
                new ControlInfoCombo(this._DZHPO, StringData.InputSignalsBig)
                );

            this._inputSignalValidator = new NewStructValidator<InputSignalBigStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._grUstComboBox, StringData.InputSignalsBig),
                new ControlInfoCombo(this._indComboBox, StringData.InputSignalsBig)
                );


            // для версии < 2.13
            if (StringData.Version < 2.13)
            {
                NewDgwValidatior<AllChannelsStruct, ChannelBigStruct> channelValidator =
                    new NewDgwValidatior<AllChannelsStruct, ChannelBigStruct>
                    (
                        this._oscChannels,
                        AllChannelsStruct.COUNT,
                        this._toolTip,
                        new ColumnInfoCombo(StringData.ChannelsNamesBig, ColumnsType.NAME),
                        new ColumnInfoCombo(StringData.RelaySignalsBig)
                    );

                this._oscValidator = new NewStructValidator<OscilloscopeSettingsBigStruct>
                (
                    this._toolTip,
                    new ControlInfoCombo(this._oscLength, StringData.OscLength),
                    new ControlInfoText(this._oscWriteLength, RulesContainer.UshortTo100),
                    new ControlInfoCombo(this._oscFix, StringData.OscFix),
                    new ControlInfoValidator(channelValidator),
                    new ControlInfoCombo(this._inpOscComboBox, StringData.RelaySignalsBig)
                );
            }

            // для версии 2.13
            else
            {
                Func<string, Dictionary<ushort, string>> func = str =>
                {
                    if (string.IsNullOrEmpty(str)) return new Dictionary<ushort, string>();
                    int index = StringData.OscBases.IndexOf(str);
                    return index != -1 ? StringData.OscChannelSignals[(ushort)index] : new Dictionary<ushort, string>();
                };


                NewDgwValidatior<AllChannelsWithBase, ChannelWithBase> allChannelsWithBase =
                    new DgvValidatorWithDepend<AllChannelsWithBase, ChannelWithBase>
                    (
                        this._oscChannelsWithBaseGrid,
                        AllChannelsWithBase.KANAL_COUNT,
                        this._toolTip,
                        new ColumnInfoCombo(StringData.OscChannelWithBaseNames, ColumnsType.NAME),
                        new ColumnInfoComboControl(StringData.OscBases, 2),
                        new ColumnInfoDictionaryComboDependent(func, 1)
                    );

                NewDgwValidatior<ChannelBigStructV213, ChannelStruct> channelValidatior213 =
                    new DgvValidatorWithDepend<ChannelBigStructV213, ChannelStruct>
                    (
                        this._oscChannels,
                        ChannelBigStructV213.COUNT,
                        this._toolTip,
                        new ColumnInfoCombo(StringData.OscChannelNames, ColumnsType.NAME),
                        new ColumnInfoCombo(StringData.RelaySignalsBig)
                    );


                this._oscValidatorV213 = new NewStructValidator<OscilloscopeSettingsBigStructV213>
                (
                    this._toolTip,
                    new ControlInfoCombo(this._oscLength, StringData.OscLength),
                    new ControlInfoText(this._oscWriteLength, RulesContainer.UshortTo100),
                    new ControlInfoCombo(this._oscFix, StringData.OscFix),
                    new ControlInfoCombo(this._inpOscComboBox, StringData.RelaySignalsBig),
                    new ControlInfoValidator(allChannelsWithBase),
                    new ControlInfoValidator(channelValidatior213)
                );
            }

            
            // далее общее
            this._oscChannels.CellBeginEdit += this.GridOnCellBeginEdit;


            this._urovConnectionValidator = new NewDgwValidatior<AllUrovConnectionBigStruct, UrovConnectionNewStruct>
                (
                this._UROVJoinData,
                AllUrovConnectionBigStruct.CurrentUrovConnectionCount,
                this._toolTip,
                new ColumnInfoCombo(StringData.ConnectionNamesBig, ColumnsType.NAME),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(RulesContainer.TimeRule)
                );

            this._connectionValidator = new NewDgwValidatior<AllConnectionBigStruct, ConnectionBigStruct>
                (
                this._joinData,
                AllConnectionBigStruct.ConnectionsCount,
                this._toolTip,
                new ColumnInfoCombo(StringData.ConnectionNamesBig, ColumnsType.NAME),
                new ColumnInfoText(RulesContainer.UshortTo65534),
                new ColumnInfoCombo(StringData.InputSignalsBig),
                new ColumnInfoCombo(StringData.InputSignalsBig),
                new ColumnInfoCombo(StringData.Join),
                new ColumnInfoCombo(StringData.InputSignalsBig),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.TimeRule)
                );
            if (AllConnectionBigStruct.ConnectionsCount == 6)
            {
                this._connectionValidator.Disabled = new[]
                {
                    new Point(4, 5), new Point(5, 5), new Point(6, 5), new Point(7, 5)
                };
            }

            this._differentialCurrentValidator = new NewDgwValidatior<AllDifferentialCurrentBigStruct, DifferentialCurrentBigStruct>
                (
                new[] {this._difDDataGrid, this._difMDataGrid},
                new[] {3, 3},
                this._toolTip,
                new ColumnInfoCombo(StringData.DefNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringData.ModesLightMode),
                new ColumnInfoCombo(StringData.InputSignalsBig),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(RulesContainer.TimeRule, true, false),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(new CustomUshortRule(0, 45)),
                new ColumnInfoCheck(true, false),
                new ColumnInfoText(RulesContainer.UshortTo100, true, false),
                new ColumnInfoCheck(true, false),
                new ColumnInfoText(RulesContainer.UshortTo100, true, false),
                new ColumnInfoCheck(true, false),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCombo(StringData.InputSignalsBig),
                new ColumnInfoCombo(StringData.ModesLightOsc),
                new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difDDataGrid,
                        new TurnOffRule(1, StringData.ModesLightMode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19)
                        ),
                    new TurnOffDgv
                        (
                        this._difMDataGrid,
                        new TurnOffRule(1, StringData.ModesLightMode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19)
                        )
                },
                Disabled = new[]
                {
                    new Point(3, 2)
                }
            };
            this._mtzValidator = new NewDgwValidatior<AllMtzBigStruct, MtzBigStruct>
                (
                this._MTZDifensesDataGrid,
                AllMtzBigStruct.COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringData.MtzNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringData.ModesLightMode),
                new ColumnInfoCombo(StringData.InputSignalsBig),
                new ColumnInfoCombo(StringData.TokParameter),
                new ColumnInfoCombo(StringData.MTZJoinBig),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoCombo(StringData.Characteristic),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ushort100To4000),
                new ColumnInfoCombo(StringData.ModesLightOsc),
                new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._MTZDifensesDataGrid,
                        new TurnOffRule(1, StringData.ModesLightMode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10)
                        )
                }
            };
            this._externalDefenseValidator = new NewDgwValidatior<AllExternalDefenseBigStruct, ExternalDefenseBigStruct>
                (
                this._externalDifensesDataGrid,
                AllExternalDefenseBigStruct.COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringData.ExternalnNamesBig, ColumnsType.NAME),
                new ColumnInfoCombo(StringData.ModesLightMode),
                new ColumnInfoCombo(StringData.OtklBig),
                new ColumnInfoCombo(StringData.ExtDefSignalsBig),
                new ColumnInfoCombo(StringData.ExtDefSignalsBig),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCombo(StringData.ExtDefSignalsBig),
                new ColumnInfoCombo(StringData.YesNo),
                new ColumnInfoCombo(StringData.ModesLightOsc),
                new ColumnInfoCombo(StringData.ModesLight)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._externalDifensesDataGrid,
                        new TurnOffRule(1, StringData.ModesLightMode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10)
                        )
                }
            };

            NewStructValidator<ConfigIgSsh> igSshValidator = new NewStructValidator<ConfigIgSsh>(
                this._toolTip,
                new ControlInfoCombo(this._configIgSshComboBox, StringData.ConfigIgssh));

            NewStructValidator<ConfigIPAddress> ethernetValidator = new NewStructValidator<ConfigIPAddress>(
                this._toolTip,
                new ControlInfoText(this._ipLo1, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipLo2, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipHi1, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipHi2, new CustomUshortRule(0, 255)));

            this._setpointValidator = new StructUnion<SetpointNewStruct>
                (
                this._differentialCurrentValidator,
                this._mtzValidator,
                this._externalDefenseValidator
                );


            RadioButtonSelector groupSelector = new RadioButtonSelector(this._mainRadioButton, this._reserveRadioButton, this._groupChangeButton);
            this._defensesValidator = new SetpointsValidator<AllDefensesSetpointsNewStruct, SetpointNewStruct>(groupSelector,
                this._setpointValidator);

            if (StringData.Version < 2.13)
            {
                this._configurationValidator = new StructUnion<ConfigurationStructBigV210>
                (
                    this._urovValidator,
                    this._urovConnectionValidator,
                    this._connectionValidator,
                    this._inputSignalValidator,
                    this._oscValidator,
                    this._newTtValidator,
                    this._inputLogicUnion,
                    this._vlsUnion,
                    this._defensesValidator,
                    this._relayValidator,
                    this._indicatorValidator,
                    this._faultValidator,
                    ethernetValidator
                );
            }
            else
            {
                this._configurationValidatorV213 = new StructUnion<ConfigurationStructBigV213>
                (
                    this._urovValidator,
                    this._urovConnectionValidator,
                    this._connectionValidator,
                    this._inputSignalValidator,
                    this._oscValidatorV213, // 
                    this._newTtValidator,
                    this._inputLogicUnion,
                    this._vlsUnion,
                    this._defensesValidator,
                    this._relayValidator,
                    this._indicatorValidator,
                    this._faultValidator,
                    ethernetValidator,
                    igSshValidator
                );
            }           
        }

        #endregion

        #region Дополнительные функции

        private void GridOnCellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this.contextMenu.Tag = sender;
        }

        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._resetSetpointsButton.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }

        public void ConfigurationReadOk()
        {
            this._statusLabel.Text = READ_OK;
            if (StringData.Version < 2.13) this._currentSetpointsStruct = this._configuration.Value;
            else                           this._currentSetpointsStructV213 = this._configurationV213.Value;
            this.ShowConfiguration();
            MessageBox.Show(READ_OK);
            this.IsProcess = false;
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
        }

        /// <summary>
        /// Конфигурация успешно записана
        /// </summary>
        private void ConfigurationWriteOk()
        {
            this._statusLabel.Text = WRITE_OK;
            MessageBox.Show(WRITE_OK);
            this.IsProcess = false;
        }

        /// <summary>
        /// Ошибка чтения конфигурации
        /// </summary>
        private void ConfigurationReadFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = READ_FAIL;
            MessageBox.Show(READ_FAIL);
        }

        /// <summary>
        /// Ошибка записи конфигурации
        /// </summary>
        private void ConfigurationWriteFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = WRITE_FAIL;
            MessageBox.Show(WRITE_FAIL);
        }

        /// <summary>
        /// Выводит все данные на экран
        /// </summary>
        private void ShowConfiguration()
        {
            if (StringData.Version < 2.13) this._configurationValidator.Set(this._currentSetpointsStruct);
            else                           this._configurationValidatorV213.Set(this._currentSetpointsStructV213);
        }

        /// <summary>
        /// Запуск чтения конфигурации
        /// </summary>
        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.IsProcess = true;
            this._progressBar.Value = 0;
            if (StringData.Version < 2.13) this._configuration.LoadStruct();
            else                           this._configurationV213.LoadStruct();
            this._statusLabel.Text = @"Идет чтение";
        }

        /// <summary>
        /// Читает все данные с экрана
        /// </summary>
        private bool WriteConfiguration()
        {
            string message;
            if (StringData.Version < 2.13)
            {
                if (this._configurationValidator.Check(out message, true))
                {
                    this._currentSetpointsStruct = this._configurationValidator.Get();
                    return true;
                }
            }
            else
            {
                if (this._configurationValidatorV213.Check(out message, true))
                {
                    this._currentSetpointsStructV213 = this._configurationValidatorV213.Get();
                    return true;
                }                
            }
            MessageBox.Show(@"Невозможно записать конфигурацию", @"Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            return false;
        }
        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);
                XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                byte[] values =  Convert.FromBase64String(a.InnerText);
                if (StringData.Version < 2.13) this._currentSetpointsStruct?.InitStruct(values);
                else                           this._currentSetpointsStructV213?.InitStruct(values);
                this.ShowConfiguration();
                this._statusLabel.Text = string.Format("Файл {0} успешно загружен", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }

        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName);
            }
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            //TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
            //TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
        }

        private void SaveinFile()
        {
            this._saveConfigurationDlg.FileName = string.Format("МР902_Уставки_версия {0:F1}.bin", this._device.DeviceVersion);
            if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
            {
                this.WriteConfiguration();
                this.Serialize(this._saveConfigurationDlg.FileName);
            }
        }

        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MR902"));

                ushort[] values;
                if (StringData.Version < 2.13) values = this._currentSetpointsStruct.GetValues();
                else                           values = this._currentSetpointsStructV213.GetValues();

                XmlElement element = doc.CreateElement(XML_HEAD);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("Файл {0} успешно сохранён", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }
        #endregion

        #region Обработчики событий
        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.contextMenu.Items.Clear();
            this.contextMenu.Items.AddRange(new ToolStripItem[]
            {
                this.readFromDeviceItem,
                this.writeToDeviceItem,
                this.clearSetpointsItem,
                this.readFromFileItem,
                this.writeToFileItem,
                this.writeToHtmlItem
            });
            this.readFromDeviceItem.Enabled = this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }

        private void Configuration_Load(object sender, EventArgs e)
        {
            if (StringData.Version < 2.13) this._configurationValidator.Reset();
            else                           this._configurationValidatorV213.Reset();

            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this.StartRead();
            }
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
        }
        
        private void _oscLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            int osclen = 31360*2;
            int index = this._oscLength.SelectedIndex;
            this._oscSizeTextBox.Text = $"{osclen/(index + 2)} мс";
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            //if (this._device.MB.IsPortInvalid)
            //{
            //    MessageBox.Show(INVALID_PORT);
            //    return;
            //}
            DialogResult result = MessageBox.Show(string.Format("Записать конфигурацию МР 902 №{0}? " +
                                                                "\nВ устройство будет записан IP-адрес: {1}.{2}.{3}.{4}!",
                    this._device.DeviceNumber, this._ipHi2.Text, this._ipHi1.Text, this._ipLo2.Text, this._ipLo1.Text),
                "Запись", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                if (this.WriteConfiguration())
                {
                    this._statusLabel.Text = "Идёт запись конфигурации";
                    this.IsProcess = true;
                    this._progressBar.Value = 0;
                    if (StringData.Version < 2.13)
                    {
                        this._configuration.Value = this._currentSetpointsStruct;
                        this._configuration.SaveStruct();
                    }
                    else
                    {
                        this._configurationV213.Value = this._currentSetpointsStructV213;
                        this._configurationV213.SaveStruct();
                    }                       
                }
                else
                {
                    MessageBox.Show(ERROR_SETPOINTS_VALUE);
                }
            }
        }
        private void _resetSetpointsButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Обнулить уставки?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;
            if (StringData.Version < 2.13) this._configurationValidator.Reset();
            else                           this._configurationValidatorV213.Reset();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            //TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
            //TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            //TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
            //TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveinFile();
        }
        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtml();
        }

        private void SaveToHtml()
        {
            try
            {
                if (this.WriteConfiguration())
                {
                    if (StringData.Version < 2.13)
                    {
                        this._currentSetpointsStruct.DeviceVersion =
                            Common.VersionConverter(this._device.DeviceVersion);
                        this._currentSetpointsStruct.DeviceNumber = this._device.DeviceNumber.ToString();
                        this._currentSetpointsStruct.DeviceType = this._device.DevicePlant;
                        this._currentSetpointsStruct.OscCount = this._oscSizeTextBox.Text;
                        this._statusLabel.Text = HtmlExport.Export(Resources.MR902MainV210, Resources.MR902ResV210,
                            this._currentSetpointsStruct,
                            this._currentSetpointsStruct.Device, this._device.DeviceVersion);
                    }
                    else
                    {
                        this._currentSetpointsStructV213.DeviceVersion =
                            Common.VersionConverter(this._device.DeviceVersion);
                        this._currentSetpointsStructV213.DeviceNumber = this._device.DeviceNumber.ToString();
                        this._currentSetpointsStructV213.DeviceType = this._device.DevicePlant;
                        this._currentSetpointsStructV213.OscCount = this._oscSizeTextBox.Text;
                        this._statusLabel.Text = HtmlExport.Export(Resources.MR902MainV213, Resources.MR902ResV213,
                            this._currentSetpointsStructV213,
                            this._currentSetpointsStructV213.Device, this._device.DeviceVersion);
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения");
            }
        }
        private void ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ContextMenuStrip menu = (ContextMenuStrip)sender;
            if (menu == null) return;
            DataGridView dgv = menu.Tag as DataGridView;
            dgv?.EndEdit();
            menu.Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                if (StringData.Version < 2.13) this._configurationValidator.Reset();
                else                           this._configurationValidatorV213.Reset();
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                //TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
                //TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                //TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
                //TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtml();
            }
        }

        private void ConfigurationForm_Activated(object sender, EventArgs e)
        {
            StringData.Version = Common.VersionConverter(this._device.DeviceVersion);
            if (StringData.Version < 2.13) ConfigurationStructBigV210.DeviceModelType = this._device.DevicePlant;
            else                           ConfigurationStructBigV213.DeviceModelType = this._device.DevicePlant;
        }
        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Mr902); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(BigConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        #region [Events CreateTree]
        private void VLScheckedListBox_SelectedValueChanged(object sender, EventArgs e)
        {
            TreeViewVLS.StateNodes(this.treeViewForVLS);
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandCurrentTreeNode(this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
        }

        private void treeViewForVLS_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewVLS.DeleteNode(sender, e, this.contextMenu, this.allVlsCheckedListBoxs);
            TreeViewVLS.StateNodes(this.treeViewForVLS);
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
        }

        private void _wrapBtn_Click(object sender, EventArgs e)
        {
            if (this._wrapBtn.Text == "Свернуть")
            {
                this.treeViewForVLS.CollapseAll();
                this._wrapBtn.Text = "Развернуть";
            }
            else
            {
                this.treeViewForVLS.ExpandAll();
                this._wrapBtn.Text = "Свернуть";
            }
        }
        #endregion [Events CreateTree]       
    }
}
