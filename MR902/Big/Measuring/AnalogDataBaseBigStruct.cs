﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR902.Big.Measuring
{
    /// <summary>
    /// МР 901 Аналоговая база данных
    /// </summary>
    public class AnalogDataBaseBigStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _i1; // ток 1
        [Layout(1)] private ushort _i2; // ток 2
        [Layout(2)] private ushort _i3; // ток 3
        [Layout(3)] private ushort _i4; // ток 4
        [Layout(4)] private ushort _i5; // ток 5
        [Layout(5)] private ushort _i6; // ток 6
        [Layout(6)] private ushort _i7; // ток 7
        [Layout(7)] private ushort _i8; // ток 8
        [Layout(8)] private ushort _i9; // ток 9
        [Layout(9)] private ushort _i10; // ток 10
        [Layout(10)] private ushort _i11; // ток 11
        [Layout(11)] private ushort _i12; // ток 12
        [Layout(12)] private ushort _i13; // ток 13
        [Layout(13)] private ushort _i14; // ток 14
        [Layout(14)] private ushort _i15; // ток 15
        [Layout(15)] private ushort _i16; // ток 16
        [Layout(16)] private ushort _i17; // ток 9
        [Layout(17)] private ushort _i18; // ток 10
        [Layout(18)] private ushort _i19; // ток 11
        [Layout(19)] private ushort _i20; // ток 12
        [Layout(20)] private ushort _i21; // ток 13
        [Layout(21)] private ushort _i22; // ток 14
        [Layout(22)] private ushort _i23; // ток 15
        [Layout(23)] private ushort _i24; // ток 16

        [Layout(24)] private ushort _ida1; // ток СШ1
        [Layout(25)] private ushort _ida2; // ток СШ2
        [Layout(26)] private ushort _ida3; // ток ПО
        
        [Layout(27)] private ushort _ita1; // ток СШ1
        [Layout(28)] private ushort _ita2; // ток СШ2
        [Layout(29)] private ushort _ita3; // ток ПО

        [Layout(30)] private ushort _idb1; // ток СШ1
        [Layout(31)] private ushort _idb2; // ток СШ2
        [Layout(32)] private ushort _idb3; // ток ПО

        [Layout(33)] private ushort _itb1; // ток СШ1
        [Layout(34)] private ushort _itb2; // ток СШ2  
        [Layout(35)] private ushort _itb3; // ток ПО

        [Layout(36)] private ushort _idc1; // ток СШ1
        [Layout(37)] private ushort _idc2; // ток СШ2
        [Layout(38)] private ushort _idc3; // ток ПО
        
        [Layout(39)] private ushort _itc1; // ток СШ1
        [Layout(40)] private ushort _itc2; // ток СШ2
        [Layout(41)] private ushort _itc3; // ток ПО
        //СФЛ свободно программируеммой логики 
        [Layout(42)] private ushort _sfl1;
        [Layout(43)] private ushort _sfl2;
        [Layout(44)] private ushort _sfl3;
        [Layout(45)] private ushort _sfl4;
        [Layout(46)] private ushort _sfl5;
        [Layout(47)] private ushort _sfl6;
        [Layout(48)] private ushort _sfl7;
        [Layout(49)] private ushort _sfl8;
        #endregion [Private fields]


        #region Тормозные и дифференциальные токи
        public string GetIda1(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._ida1);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIdb1(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._idb1);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIdc1(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._idc1);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIta1(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._ita1);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetItb1(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._itb1);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetItc1(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._itc1);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIda2(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._ida2);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIdb2(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._idb2);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIdc2(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._idc2);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIta2(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._ita2);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetItb2(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._itb2);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetItc2(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._itc2);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIda3(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._ida3);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIdb3(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._idb3);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIdc3(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._idc3);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIta3(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._ita3);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetItb3(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._itb3);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetItc3(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._itc3);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }
        #endregion

        #region Токи присоединений
        public string GetIa1(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i1);
            return ValuesConverterCommon.Analog.GetI(value, factors[1]);
        }

        public string GetIb1(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i2);
            return ValuesConverterCommon.Analog.GetI(value, factors[1]);
        }

        public string GetIc1(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i3);
            return ValuesConverterCommon.Analog.GetI(value, factors[1]);
        }

        public string GetIa2(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i4);
            return ValuesConverterCommon.Analog.GetI(value, factors[2]);
        }

        public string GetIb2(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i5);
            return ValuesConverterCommon.Analog.GetI(value, factors[2]);
        }

        public string GetIc2(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i6);
            return ValuesConverterCommon.Analog.GetI(value, factors[2]);
        }

        public string GetIa3(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i7);
            return ValuesConverterCommon.Analog.GetI(value, factors[3]);
        }

        public string GetIb3(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i8);
            return ValuesConverterCommon.Analog.GetI(value, factors[3]);
        }

        public string GetIc3(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i9);
            return ValuesConverterCommon.Analog.GetI(value, factors[3]);
        }

        public string GetIa4(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i10);
            return ValuesConverterCommon.Analog.GetI(value, factors[4]);
        }

        public string GetIb4(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i11);
            return ValuesConverterCommon.Analog.GetI(value, factors[4]);
        }

        public string GetIc4(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i12);
            return ValuesConverterCommon.Analog.GetI(value, factors[4]);
        }

        public string GetIa5(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i13);
            return ValuesConverterCommon.Analog.GetI(value, factors[5]);
        }

        public string GetIb5(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i14);
            return ValuesConverterCommon.Analog.GetI(value, factors[5]);
        }

        public string GetIc5(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i15);
            return ValuesConverterCommon.Analog.GetI(value, factors[5]);
        }

        public string GetIa6(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i16);
            return ValuesConverterCommon.Analog.GetI(value, factors[6]);
        }

        public string GetIb6(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i17);
            return ValuesConverterCommon.Analog.GetI(value, factors[6]);
        }

        public string GetIc6(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i18);
            return ValuesConverterCommon.Analog.GetI(value, factors[6]);
        }

        public string GetIa7(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i19);
            return ValuesConverterCommon.Analog.GetI(value, factors[7]);
        }

        public string GetIb7(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i20);
            return ValuesConverterCommon.Analog.GetI(value, factors[7]);
        }

        public string GetIc7(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i21);
            return ValuesConverterCommon.Analog.GetI(value, factors[7]);
        }

        public string GetIa8(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i22);
            return ValuesConverterCommon.Analog.GetI(value, factors[8]);
        }

        public string GetIb8(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i23);
            return ValuesConverterCommon.Analog.GetI(value, factors[8]);
        }

        public string GetIc8(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i24);
            return ValuesConverterCommon.Analog.GetI(value, factors[8]);
        }

        public string GetIn(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i16);
            return ValuesConverterCommon.Analog.GetI(value, factors[6]);
        }
        #endregion

        private ushort GetMean(List<AnalogDataBaseBigStruct> list, Func<AnalogDataBaseBigStruct, ushort> func)
        {
            int count = list.Count;

            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (ushort)(sum / (double)count);
        }
    }
}
