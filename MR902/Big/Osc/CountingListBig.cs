using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BEMN.MBServer;
using BEMN.MR902.Big.Configuration.Structures.Osc;
using BEMN.MR902.Big.Osc.Structures;

namespace BEMN.MR902.Big.Osc
{
    /// <summary>
    /// ��������� �������� ������������
    /// </summary>
    public class CountingListBig
    {
        #region [Constants]
        /// <summary>
        /// ������ ������ �������(� ������)
        /// </summary>
        private const int COUNTING_SIZE = 32;

        private const int CURRENTS_MAX = 24;
        public int CurrentsCount { get; private set; }
        public int DiscretsCount { get; }
        public int ChannelsCount { get; }

        #endregion [Constants]

        #region [Private fields]

        private string _devicePlant;
        private OscJournalStructNew _oscJournalStruct;
        private OscilloscopeSettingsBigStruct _oscConfig;
        /// <summary>
        /// ������ �������� �������� �� �����
        /// </summary>
        private readonly ushort[][] _countingArray;
        /// <summary>
        /// ������ �������
        /// </summary>
        private ushort[][] _discrets;
        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        private int _count;
        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        private int _alarm;
        /// <summary>
        /// ����������� ���
        /// </summary>
        private double _minI;
        /// <summary>
        /// ������������ ���
        /// </summary>
        private double _maxI;
        /// <summary>
        /// ������������
        /// </summary>
        private int[] _factors;
        /// <summary>
        /// ���� ������������� �� �������������
        /// </summary>
        private double[][] _currents;
        private short[][] _baseCurrents;

        private string _stage;
        private string _dateTime;

        private ChannelWithBase[] _channelsWithBase;
        #endregion [Private fields]

        /// <summary>
        /// ������
        /// </summary>
        private ushort[][] _channels;
        
        public bool IsLoad { get; private set; }
        public string FilePath { get; private set; }

        private Mr902 _device;

        #region [Ctor's]
        public CountingListBig(string devicePlant)
        {
            this.DiscretsCount  = 16;
            this.ChannelsCount = 96;
            this.GetConst(devicePlant);
        }

        private static List<string> _currentsNames; 
        private void GetConst(string devicePlant)
        {
            this._devicePlant = devicePlant;
            switch (devicePlant)
            {
                case "A1":
                    _currentsNames = new List<string>
                    {
                        "Ia ��.1",
                        "Ib ��.1",
                        "Ic ��.1",
                        "Ia ��.2",
                        "Ib ��.2",
                        "Ic ��.2",
                        "Ia ��.3",
                        "Ib ��.3",
                        "Ic ��.3",
                        "Ia ��.4",
                        "Ib ��.4",
                        "Ic ��.4",
                        "Ia ��.5",
                        "Ib ��.5",
                        "Ic ��.5",
                        "In"
                    };
                    this.CurrentsCount = 16;
                    break;
                case "A2":
                case "A3":
                case "A4":
                    _currentsNames = new List<string>
                    {
                        "Ia ��.1",
                        "Ib ��.1",
                        "Ic ��.1",
                        "Ia ��.2",
                        "Ib ��.2",
                        "Ic ��.2",
                        "Ia ��.3",
                        "Ib ��.3",
                        "Ic ��.3",
                        "Ia ��.4",
                        "Ib ��.4",
                        "Ic ��.4",
                        "Ia ��.5",
                        "Ib ��.5",
                        "Ic ��.5",
                        "Ia ��.6",
                        "Ib ��.6",
                        "Ic ��.6",
                        "Ia ��.7",
                        "Ib ��.7",
                        "Ic ��.7",
                        "Ia ��.8",
                        "Ib ��.8",
                        "Ic ��.8"
                    };
                    this.CurrentsCount = 24;
                    break;
                default:
                    _currentsNames = new List<string>
                    {
                        "Ia ��.1",
                        "Ib ��.1",
                        "Ic ��.1",
                        "Ia ��.2",
                        "Ib ��.2",
                        "Ic ��.2",
                        "Ia ��.3",
                        "Ib ��.3",
                        "Ic ��.3",
                        "Ia ��.4",
                        "Ib ��.4",
                        "Ic ��.4",
                        "Ia ��.5",
                        "Ib ��.5",
                        "Ic ��.5",
                        "In"
                    };
                    this.CurrentsCount = 16;
                    break;
            }
        }

        public CountingListBig(Mr902 device, ushort[] pageValue, OscJournalStructNew oscJournalStruct, OscilloscopeSettingsBigStruct oscConfig, int[] factors, string devicePlant):this(devicePlant)
        {
            this._oscJournalStruct = oscJournalStruct;
            this._oscConfig = oscConfig;

            _device = device;

            if (StringData.Version >= 2.13) _channelsWithBase = _device.AllChannels.Value.Channel.ForOsc;

            this._dateTime = this._oscJournalStruct.GetDate + " " + this._oscJournalStruct.GetTime;
            this._alarm = this._oscJournalStruct.Len - this._oscJournalStruct.After;
            this._stage = oscJournalStruct.Stage;
            this._countingArray = new ushort[COUNTING_SIZE][];
            //����� ���������� ��������
            this._count = pageValue.Length/ COUNTING_SIZE;
            //������������� �������
            for (int i = 0; i < COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this._count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n == COUNTING_SIZE)
                {
                    m++;
                    n = 0;
                }
            }

            List<ushort[]> discrets = new List<ushort[]>();
            discrets.AddRange(this.DiscretArrayInit(this._countingArray[CURRENTS_MAX]));
            this._discrets = discrets.ToArray();

            List<ushort[]> channels = new List<ushort[]>();
            channels.AddRange(this.DiscretArrayInit(this._countingArray[CURRENTS_MAX+1]));
            channels.AddRange(this.DiscretArrayInit(this._countingArray[CURRENTS_MAX+2]));
            channels.AddRange(this.DiscretArrayInit(this._countingArray[CURRENTS_MAX+3]));
            channels.AddRange(this.DiscretArrayInit(this._countingArray[CURRENTS_MAX+4]));
            channels.AddRange(this.DiscretArrayInit(this._countingArray[CURRENTS_MAX+5]));
            channels.AddRange(this.DiscretArrayInit(this._countingArray[CURRENTS_MAX+6]));
            this._channels = channels.ToArray();

            this._factors = factors;
            this._currents = new double[this.CurrentsCount][];
            this._baseCurrents = new short[this.CurrentsCount][];
            this._maxI = double.MinValue;
            this._minI = double.MaxValue;
            int f = 0;
            for (int i = 0; i < this.CurrentsCount; i++)
            {
                if (i%3 == 0 && i!=0) f++;
                this._baseCurrents[i] = this._countingArray[i].Select(a => (short) a).ToArray();
                double factor = Math.Sqrt(2) * this._factors[f] / 32767; //��� �������� �� 40
                this._currents[i] = this._baseCurrents[i].Select(a => factor * a).ToArray();
                this._maxI = Math.Max(this._maxI, this._currents[i].Max());
                this._minI = Math.Min(this._minI, this._currents[i].Min());
            }
        }

        #endregion [Ctor's]

        #region [Help members]
        /// <summary>
        /// ���������� ������ ����� � ��������������� ������ ���(�������� 0/1) 
        /// </summary>
        /// <param name="sourseArray">������ �������� ���</param>
        /// <returns></returns>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {

            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        } 
        #endregion [Help members]
        
        public void Save(string filePath, OscilloscopeSettingsBigStruct oscSettings)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine("�� 902 {0} ������� - {1}", this._dateTime, this._stage);
                hdrFile.WriteLine("Size, ms = {0}", this._oscJournalStruct.Len);
                hdrFile.WriteLine("Alarm = {0}", this._alarm);
                hdrFile.WriteLine("DevicePlant = {0}", this._devicePlant);
                for (int i = 0; i < this.ChannelsCount; i++)
                {
                    hdrFile.WriteLine("K{0} = {1}", i + 1, oscSettings.Channels.Words[i]);
                }
                hdrFile.WriteLine(this._stage);
                hdrFile.WriteLine(1251);
            }
            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
            {
                cgfFile.WriteLine("MP902,1");
                cgfFile.WriteLine("{0},{1}A,{2}D", this.CurrentsCount+this.DiscretsCount+this.ChannelsCount, this.CurrentsCount, this.DiscretsCount+this.ChannelsCount);
                int index = 1;
                int f = 0;
                for (int i = 0; i < this._currents.Length; i++)
                {
                    NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                    if (i % 3 == 0 && i != 0) f++;
                    double factor = Math.Sqrt(2)*this._factors[f]/32767;
                    cgfFile.WriteLine("{0},{1},,,A,{2},0,0,-32768,32767,1,1,P", index, _currentsNames[i], factor.ToString(format));
                    index++;
                }

                for (int i = 0; i < this._discrets.Length; i++)
                {
                    cgfFile.WriteLine("{0},D{1},0", index, i + 1);
                    index++;
                }

                if (StringData.Version >= 2.13)
                {
                    for (int i = 0; i < this._channels.Length; i++)
                    {

                        if (i < 32)
                        {
                            cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, this._channelsWithBase[i].ChannelStr);
                            index++;
                        }
                        else
                        {
                            cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, StringData.RelaySignalsBig[oscSettings.ChannelsWord[i]]);
                            index++;
                        }
                    }
                }
                else
                {
                    for(int i = 0; i < this._channels.Length; i++)
                    {
                        cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, StringData.RelaySignalsBig[oscSettings.Channels.Words[i]]);
                        index++;
                    }
                }

                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this._oscJournalStruct.Len);

                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);
                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeAlarm(this._alarm));
                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < this._count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}", i, i*1000);
                    foreach (short[] current  in this._baseCurrents)
                    {
                        datFile.Write(",{0}", current[i]);
                    }
                    foreach (ushort[] discret in this._discrets)
                    {
                        datFile.Write(",{0}", discret[i]);
                    }
                    foreach (ushort[] chanel in this._channels)
                    {
                        datFile.Write(",{0}", chanel[i]);
                    }
                    datFile.WriteLine();
                }
            }
        }

        public CountingListBig Load(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);

            string devicePlant = hdrStrings[3].Replace("DevisePlant = ", string.Empty);
            string[] channelsCount = cfgStrings[1].Split(',');
            CountingListBig result = new CountingListBig(devicePlant);
            result.CurrentsCount = int.Parse(channelsCount[1].Replace("A", string.Empty));
            string[] buff = hdrStrings[0].Split(' ');
            result._dateTime = $"{buff[2]} {buff[3]}";
            result._alarm = int.Parse(hdrStrings[2].Replace("Alarm = ", string.Empty));
            
            this._oscConfig = new OscilloscopeSettingsBigStruct();
            this._oscConfig.ChannelsWord = new ushort[this.ChannelsCount];
            for (int i = 0; i < this.ChannelsCount; i++)
            {
                this._oscConfig.ChannelsWord[i] = ushort.Parse(hdrStrings[4 + i].Replace(string.Format("K{0} = ", i + 1), string.Empty));
            }
            result._stage = hdrStrings[3+this.ChannelsCount];
            double[] factors = new double[this.CurrentsCount];
            Regex factorRegex = new Regex(@"\d+\,I\d+\,\,\,A\,(?<value>[0-9\.]+)");
            for (int i = 2; i < 2+ this.CurrentsCount; i++)
            {
                string res = factorRegex.Match(cfgStrings[i]).Groups["value"].Value;
                NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                factors[i - 2] = double.Parse(res, format);
            }
            result._count =int.Parse(cfgStrings[2+CURRENTS_MAX+ this.DiscretsCount+ this.ChannelsCount+2].Replace("1000,", string.Empty));
            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);
            double[][] currents = new double[this.CurrentsCount][];
            ushort[][] discrets = new ushort[this.DiscretsCount][];
            ushort[][] channels = new ushort[this.ChannelsCount][];
            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new double[result._count];         
            }

            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[result._count];
            }

            for (int i = 0; i < channels.Length; i++)
            {
                channels[i] = new ushort[result._count];
            }

            for (int i = 0; i < datStrings.Length; i++)
            {
                string[] line = datStrings[i].Split(',');
                int index = 2;

                for (int j = 0; j < this.CurrentsCount; j++)
                {
                    currents[j][i] = double.Parse(line[index]) * factors[j];
                    index++;
                }

                for (int j = 0; j < this.DiscretsCount; j++)
                {
                    discrets[j][i] = ushort.Parse(line[index]);
                    index++;
                }

                for (int j = 0; j < this.ChannelsCount; j++)
                {
                    channels[j][i] = ushort.Parse(line[index]);
                    index++;
                }      
            }
            result._maxI = double.MinValue;
            result._minI = double.MaxValue;
            for (int i = 0; i < this.CurrentsCount; i++)
            {
                result._maxI = Math.Max(result._maxI, currents[i].Max());
                result._minI = Math.Min(result._minI, currents[i].Min());                
            }
            result._currents = currents;
            result._channels = channels;
            result._discrets = discrets;
            result.FilePath = filePath;
            result.IsLoad = true;
            return result;
        }
    }
}
