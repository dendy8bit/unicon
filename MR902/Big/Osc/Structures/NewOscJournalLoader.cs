﻿using System;
using System.Collections.Generic;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MR902.Old.Structures.OscillogramStruct;

namespace BEMN.MR902.Big.Osc.Structures
{
    /// <summary>
    /// Загружает журнал осцилограммы
    /// </summary>
    public class NewOscJournalLoader
    {
        #region [Private fields]
        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<OscJournalStructNew> _oscJournal;
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private readonly MemoryEntity<JournalRefreshStruct> _refreshOscJournal;
        /// <summary>
        /// Длинна осцилограммы
        /// </summary>
        private readonly MemoryEntity<OscOptionsStruct> _oscOptions;
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        private readonly List<OscJournalStructNew> _oscRecords;
        /// <summary>
        /// Текущий номер записи журнала осциллографа
        /// </summary>
        private int _recordNumber;

        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Успешно прочитана одна запись журнала осциллографа
        /// </summary>
        public event Action ReadRecordOk;
        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action ReadJournalFail;
        /// <summary>
        /// Успешно прочитаны все записи
        /// </summary>
        public event Action AllJournalReadOk;
        #endregion [Events]


        #region [Ctor's]
        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        /// <param name="device">Устройство</param>
        public NewOscJournalLoader(Mr902 device)
        {
            this._oscRecords = new List<OscJournalStructNew>();
            //Длинна осцилограммы
            this._oscOptions = new MemoryEntity<OscOptionsStruct>("Параметры осциллографа (big)", device, 0x5A0);
            //Если длинна прочитана - сбрасываем журнал
            this._oscOptions.AllReadOk += o => this._refreshOscJournal.SaveStruct();
            this._oscOptions.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);

            //Сброс журнала на нулевую запись
            this._refreshOscJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала осциллографа (big)", device, 0x800);
            this._refreshOscJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this.StartReadOscJournal);
            this._refreshOscJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);

            //Записи журнала
            this._oscJournal = new MemoryEntity<OscJournalStructNew>("Запись ЖО", device, 0x800);
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);

    
        } 
        #endregion [Ctor's]


        #region [Properties]
        /// <summary>
        /// Номер текущей записи журнала осциллографа
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
        }
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<OscJournalStructNew> OscRecords
        {
            get { return this._oscRecords; }
        }
        /// <summary>
        /// Длинна осцилограммы
        /// </summary>
        public OscOptionsStruct OscSizeOptions
        {
            get { return this._oscOptions.Value; }
        }

        #endregion [Properties]


        #region [Private MemoryEntity Events Handlers]
        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            if (this.ReadJournalFail != null)
                this.ReadJournalFail.Invoke();
        }

        /// <summary>
        /// Журнал сброшен. Запуск чтения записей
        /// </summary>
        private void StartReadOscJournal()
        {
            this._recordNumber = 0;
            this._oscJournal.LoadStructCycleWhile(a => a.IsEmpty);
        }

        public object[] GetRecord
        {
            get { return this._oscJournal.Value.GetRecord; }
        }

        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            if (!this._oscJournal.Value.IsEmpty)
            {
                OscJournalStructNew.RecordIndex = this.RecordNumber;
                
                this.OscRecords.Add(this._oscJournal.Value.Clone<OscJournalStructNew>());
                this._recordNumber = this.RecordNumber + 1;
                if (this.ReadRecordOk != null)
                    this.ReadRecordOk.Invoke();
            }
            else
            {
                if (this.AllJournalReadOk == null) return;
                this.AllJournalReadOk.Invoke();
            }
        } 
        #endregion [Private MemoryEntity Events Handlers]


        #region [Public members]
        /// <summary>
        /// Запуск чтения журнала осциллографа
        /// </summary>
        public void StartReadJournal()
        {          
            this._oscOptions.LoadStruct();
        } 
        #endregion [Public members]

        internal void Reset()
        {
            this._oscRecords.Clear();
        }
    }
}
