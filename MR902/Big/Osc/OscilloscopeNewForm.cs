using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR902.Big.Configuration.Structures;
using BEMN.MR902.Big.Configuration.Structures.Connections;
using BEMN.MR902.Big.Configuration.Structures.Osc;
using BEMN.MR902.Big.Osc.Structures;
using BEMN.MR902.Properties;

namespace BEMN.MR902.Big.Osc
{
    public partial class OscilloscopeNewForm : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "�������������";
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string RECORDS_IN_JOURNAL = "������������ � ������� - {0}";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string READ_OSC_STOPPED = "������ ������������� ����������";
        private const string JOURNAL_IS_EMPTY = "������ ������������ ����";

        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly NewOscPageLoader _pageLoader;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly NewOscJournalLoader _oscJournalLoader;
        /// <summary>
        /// ��������� ������� �����
        /// </summary>
        private readonly MemoryEntity<AllConnectionBigStruct> _connections;

        private readonly MemoryEntity<OscilloscopeSettingsBigStruct> _oscilloscopeSettings;

        private MemoryEntity<OscilloscopeSettingsBigStructV213> _oscopeAllChannel;

        /// <summary>
        /// ������ ���
        /// </summary>
        private CountingListBig _countingList;

        private Mr902 _device;
        private OscJournalStructNew _journalStruct;
        private readonly DataTable _table;
        #endregion [Private fields]


        #region [Ctor's]
        public OscilloscopeNewForm()
        {
            this.InitializeComponent();
        }

        public OscilloscopeNewForm(Mr902 device)
        {
            this.InitializeComponent();
            this._device = device;
            MemoryEntity<SetOscStartPageStruct> setOscStartPage = new MemoryEntity<SetOscStartPageStruct>("����� �������� ������������� (big)", device, 0x900);
            MemoryEntity<OscPage> oscPage = new MemoryEntity<OscPage>("�������� ������������ (big)", device, 0x900);
            //��������� �������
            this._pageLoader = new NewOscPageLoader(setOscStartPage, oscPage);
            this._pageLoader.PageRead +=  HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadStopped += HandlerHelper.CreateActionHandler(this, this.ReadStop);
          
            //��������� �������
            this._oscJournalLoader = new NewOscJournalLoader(device);
            this._oscJournalLoader.ReadRecordOk += HandlerHelper.CreateActionHandler(this, this.ReadRecord);
            this._oscJournalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.OnAllJournalReadOk);

            //������� ����������� (������)
            this._oscilloscopeSettings = new MemoryEntity<OscilloscopeSettingsBigStruct>("������� ������������ (big)", device, 0x10FE);
            this._oscilloscopeSettings.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._oscJournalLoader.StartReadJournal);
            this._oscilloscopeSettings.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);

            //������ ������������
            this._oscopeAllChannel = this._device.AllChannels;
            this._oscopeAllChannel.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._oscilloscopeSettings.LoadStruct();
            });
            this._oscopeAllChannel.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);

            //��������� ������� �����
            this._connections = new MemoryEntity<AllConnectionBigStruct>("������� ������������� (��)", device, 0x106A);
            this._connections.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                if (StringData.Version >= 2.13)
                {
                    _oscopeAllChannel.LoadStruct();
                }
                else
                {
                    this._oscilloscopeSettings.LoadStruct();
                }
            });
            this._connections.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);
            

            this._table = this.GetJournalDataTable();
        }
        #endregion [Ctor's]

        /// <summary>
        /// �������� ���� ������
        /// </summary>
        private void OnAllJournalReadOk()
        {
            if (this._oscJournalLoader.OscRecords.Count == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
            }
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
            this._oscReadButton.Enabled = true;
        }

        private void ReadStop()
        {
            this._statusLabel.Text = READ_OSC_STOPPED;
            this._stopReadOsc.Enabled = false;
            this._oscProgressBar.Value = 0;
        }
        
        #region [Help Classes Events Handlers]
        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
        }


        /// <summary>
        /// ��������� ���� ������ �������
        /// </summary>
        private void ReadRecord()
        {
            var number = this._oscJournalLoader.RecordNumber;
            this._oscilloscopeCountCb.Items.Add(number);
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, number);
            this._table.Rows.Add(this._oscJournalLoader.GetRecord);
            this._oscJournalDataGrid.Refresh();
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
        }

        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            StringData.Version = Common.VersionConverter(this._device.DeviceVersion);
            ConfigurationStructBigV210.DeviceModelType = this._device.DevicePlant;
            try
            {
                this.CountingList = new CountingListBig(_device, this._pageLoader.ReadWords, this._journalStruct, this._oscilloscopeSettings.Value,
                    this._connections.Value.GetAllItt.Skip(1).ToArray(), this._device.DevicePlant);
            }
            catch (Exception e)
            {
                MessageBox.Show("������ ������ ������������", "��������!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            this._stopReadOsc.Enabled = false;
            this._oscSaveButton.Enabled = true;
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
            this._oscReadButton.Enabled = true;
            this._oscShowButton.Enabled = true;
        }

        #endregion [Help Classes Events Handlers]


        #region [Properties]
        /// <summary>
        /// ���������� ����������� ������� ������������ ��� ������
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// ������ ���
        /// </summary>
        public CountingListBig CountingList
        {
            get { return this._countingList??new CountingListBig(this._device.DevicePlant); }
            set
            {
                this._countingList = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]


        #region [Help members]
        private DataTable GetJournalDataTable()
        {
            var table = new DataTable("��902_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }
        #endregion [Help members]


        #region [Event Handlers]
        /// <summary>
        /// �������� �����
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            this.StartRead();
        
        }
        /// <summary>
        /// �������� �������������
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (this.CountingList == null) return;
            
            if (Validator.GetVersionFromRegistry())
            {
                string fileName;
                if (this._countingList.IsLoad)
                {
                    fileName = this._countingList.FilePath;
                }
                else
                {
                    fileName = Validator.CreateOscFileNameCfg($"��902 v{this._device.DeviceVersion} �������������");
                    this._countingList.Save(fileName,this._oscilloscopeSettings.Value);
                }
                System.Diagnostics.Process.Start(Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"),
                    fileName);
            }
            else
            {
                MessageBox.Show("���������� ������ �������������. ����������, ����������, Framework .net 4.0!",
                    "�������������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;            
            this._oscilloscopeCountCb.Items.Clear();
            this._oscilloscopeCountCb.SelectedIndex = -1;
            this._table.Clear();
            this._oscJournalLoader.Reset();
            this._oscJournalDataGrid.Refresh();
            this.CanSelectOsc = false;
            this._connections.LoadStruct();
            this._oscJournalReadButton.Enabled = false;
            this._oscReadButton.Enabled = false;
            this._oscLoadButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
        }

        /// <summary>
        /// ��������� �������������
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStruct = this._oscJournalLoader.OscRecords[selectedOsc];
            this._pageLoader.StartRead(this._journalStruct, this._oscJournalLoader.OscSizeOptions);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoader.PagesCount;
            //�������� ����������� ���������� ������ ������������
            this._stopReadOsc.Enabled = true;
            this._oscJournalReadButton.Enabled = false;
            this._oscLoadButton.Enabled = false;
            this._oscReadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
        }

        /// <summary>
        /// ��������� ������������� � ����
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        { 
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                this.CountingList.Save(this._saveOscilloscopeDlg.FileName,this._oscilloscopeSettings.Value);
            }
        }

        /// <summary>
        /// ��������� ������������� �� �����
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;

            try
            {
                this.CountingList = this.CountingList.Load(this._openOscilloscopeDlg.FileName);
                this._statusLabel.Text = string.Format("������������ ��������� �� ����� {0}", this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
                this._stopReadOsc.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = "���������� ��������� ������������";
            }

        }

        #endregion [Event Handlers]

        /// <summary>
        /// ���������� ������ ������������
        /// </summary>
        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._pageLoader.StopRead();
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {   
            this._oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscBeginColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this.checkBox1.Checked;
        }

        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }


        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(Mr902); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(OscilloscopeNewForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return OSC; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }
        #endregion [IFormView Members]
    }
}