﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR902.Big.AlarmJournal.Structures;
using BEMN.MR902.Big.Configuration.Structures;
using BEMN.MR902.Big.Configuration.Structures.Connections;

namespace BEMN.MR902.Big.AlarmJournal
{
    public partial class AlarmJournalBigForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Аварий в журнале - {0}";
        private const string READ_AJ_FAIL = "Невозможно прочитать журнал аварий";
        private const string READ_AJ = "Чтение журнала аварий";
        private const string ALARM_JOURNAL = "Журнал аварий";
        private const string TABLE_NAME = "МР902_журнал_аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";

        #endregion [Constants]


        #region [Private fields]
        private readonly MemoryEntity<OneWordStruct> _refreshAlarmJournal;
        private readonly MemoryEntity<AlarmJournalRecordBigStruct> _alarmJournal;
        private readonly MemoryEntity<AllConnectionBigStruct> _currentConnections;
        private DataTable _table;
        private int _recordNumber;
        private Mr902 _device;
        private int[] _factors;
        #endregion [Private fields]


        #region [Ctor's]
        public AlarmJournalBigForm()
        {
            this.InitializeComponent();
        }

        public AlarmJournalBigForm(Mr902 device)
        {
            this.InitializeComponent();
            this._device = device;

            this._alarmJournal = new MemoryEntity<AlarmJournalRecordBigStruct>("Журнал аварий МР902", device, 0x700);
            this._refreshAlarmJournal = new MemoryEntity<OneWordStruct>("Номер записи ЖА", device, 0x700);
            this._currentConnections = new MemoryEntity<AllConnectionBigStruct>("Присоединения (ЖА)", device, 0x106A);
            this._currentConnections.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                StringData.Version = Common.VersionConverter(this._device.DeviceVersion);
                ConfigurationStructBigV210.DeviceModelType = this._device.DevicePlant;
                this._factors = this._currentConnections.Value.GetAllItt;
                this._refreshAlarmJournal.SaveStruct();
            });
            this._currentConnections.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);

            this._refreshAlarmJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.StartReadJournal);
            this._refreshAlarmJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
            this._alarmJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);
            this._alarmJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);

            this._table = this.GetJournalDataTable();
            this._alarmJournalGrid.DataSource = this._table;
        } 
        #endregion [Ctor's]


        #region [Help members]
        private void FailReadJournal()
        {
            this._statusLabel.Text = READ_AJ_FAIL;
            if(this._recordNumber == 0)
                this._alarmJournal.RemoveStructQueries();
        }

        private void StartReadJournal()
        {
            this._recordNumber = 0;
            this._alarmJournal.LoadStructCycleWhile(a => a.IsEmpty);
        }

        private void ReadRecord()
        {
            if (!this._alarmJournal.Value.IsEmpty)
            {
                this._recordNumber++;
                this._table.Rows.Add(this._alarmJournal.Value.GetRecord(this._recordNumber, this._factors, this._device.DevicePlant));
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
            }
            else
            {
                if (this._recordNumber == 0)
                {
                    this._statusLabel.Text = "Журнал пуст";
                }
            }
        }

        private DataTable GetJournalDataTable()
        {
            switch (this._device.DevicePlant)
            {
                case "A1":
                    this._alarmJournalGrid.Columns["_IaPr6Col"].HeaderText = "Прис.In";
                    this._alarmJournalGrid.Columns.Remove(this._IbPr6Col);
                    this._alarmJournalGrid.Columns.Remove(this._IcPr6Col);
                    this._alarmJournalGrid.Columns.Remove(this._IaPr7Col);
                    this._alarmJournalGrid.Columns.Remove(this._IbPr7Col);
                    this._alarmJournalGrid.Columns.Remove(this._IcPr7Col);
                    this._alarmJournalGrid.Columns.Remove(this._IaPr8Col);
                    this._alarmJournalGrid.Columns.Remove(this._IbPr8Col);
                    this._alarmJournalGrid.Columns.Remove(this._IcPr8Col);
                    break;
                case "A2":
                    this._alarmJournalGrid.Columns.Remove(this._D5Col);
                    this._alarmJournalGrid.Columns.Remove(this._D6Col);
                    this._alarmJournalGrid.Columns.Remove(this._D7Col);
                    break;
                case "A3":
                    this._alarmJournalGrid.Columns.Remove(this._D3Col);
                    this._alarmJournalGrid.Columns.Remove(this._D4Col);
                    this._alarmJournalGrid.Columns.Remove(this._D5Col);
                    this._alarmJournalGrid.Columns.Remove(this._D6Col);
                    this._alarmJournalGrid.Columns.Remove(this._D7Col);
                    break;
                case "A4":
                    this._alarmJournalGrid.Columns.Remove(this._D4Col);
                    this._alarmJournalGrid.Columns.Remove(this._D5Col);
                    this._alarmJournalGrid.Columns.Remove(this._D6Col);
                    this._alarmJournalGrid.Columns.Remove(this._D7Col);
                    break;
                default:
                    this._alarmJournalGrid.Columns["_IaPr6Col"].HeaderText = "Прис.In";
                    this._alarmJournalGrid.Columns.Remove(this._IbPr6Col);
                    this._alarmJournalGrid.Columns.Remove(this._IcPr6Col);
                    this._alarmJournalGrid.Columns.Remove(this._IaPr7Col);
                    this._alarmJournalGrid.Columns.Remove(this._IbPr7Col);
                    this._alarmJournalGrid.Columns.Remove(this._IcPr7Col);
                    this._alarmJournalGrid.Columns.Remove(this._IaPr8Col);
                    this._alarmJournalGrid.Columns.Remove(this._IbPr8Col);
                    this._alarmJournalGrid.Columns.Remove(this._IcPr8Col);
                    this._alarmJournalGrid.Columns.Remove(this._D3Col);
                    this._alarmJournalGrid.Columns.Remove(this._D4Col);
                    this._alarmJournalGrid.Columns.Remove(this._D5Col);
                    this._alarmJournalGrid.Columns.Remove(this._D6Col);
                    this._alarmJournalGrid.Columns.Remove(this._D7Col);
                    break;
            }

            DataTable table = new DataTable(TABLE_NAME);
            for (int j = 0; j < this._alarmJournalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._alarmJournalGrid.Columns[j].Name);
            }
            return table;
        }

        #endregion [Help members]

        #region [Event Handlers]
        private void AlarmJournalForm_Load(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._table.Rows.Clear();
            this._statusLabel.Text = READ_AJ;
            this._currentConnections.LoadStruct();
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this._openAlarmJournalDialog.ShowDialog() == DialogResult.OK)
                {
                    this._table.Clear();
                    if (Path.GetExtension(this._openAlarmJournalDialog.FileName).ToLower().Contains("bin"))
                    {
                        byte[] file = File.ReadAllBytes(this._openAlarmJournalDialog.FileName);
                        AlarmJournalRecordBigStruct journal = new AlarmJournalRecordBigStruct();
                        int size = journal.GetSize();
                        int connectionSize = (file.Length - 16)%size == 0 ? 16 : 24;
                        ushort[] buff = Common.TOWORDS(file.Skip(file.Length - connectionSize).ToArray(), true);
                        this._factors = new int[buff.Length];
                        for(int i = 0; i < buff.Length; i++)
                        {
                            this._factors[i] = buff[i];
                        }
                        int index = 0;
                        this._recordNumber = 0;
                        while (index < file.Length - connectionSize)
                        {
                            List<byte> journalBytes = new List<byte>();
                            journalBytes.AddRange(Common.SwapListItems(file.Skip(index).Take(size)));
                            journal.InitStruct(journalBytes.ToArray());
                            this._alarmJournal.Value = journal;
                            this.ReadRecord();
                            index += size;
                        }
                    }
                    else
                    {
                        this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                        this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._table.Rows.Count);
                        this._alarmJournalGrid.Refresh();
                    }
                }
            }
            catch
            {
                MessageBox.Show("Невозможно открыть файл", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
            }
        }

        private void _saveJournaloToHTML(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            if (this._saveAlarmHTMLdialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string xml;

                    using (StringWriter writer = new StringWriter())
                    {
                        this._table.WriteXml(writer);
                        xml = writer.ToString();
                    }

                    HtmlExport.Export(this._saveAlarmHTMLdialog.FileName, "МР902. Журнал аварий", xml);
                    this._statusLabel.Text = "Журнал успешно сохранен.";

                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }

        #endregion [Event Handlers]



        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(Mr902); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(AlarmJournalBigForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return ALARM_JOURNAL; }
        }
        #endregion [IFormView Members]
    }
}
