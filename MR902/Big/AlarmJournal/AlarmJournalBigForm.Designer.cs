﻿namespace BEMN.MR902.Big.AlarmJournal
{
    partial class AlarmJournalBigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this._readAlarmJournalButton = new System.Windows.Forms.Button();
            this._saveAlarmJournalButton = new System.Windows.Forms.Button();
            this._loadAlarmJournalButton = new System.Windows.Forms.Button();
            this._openAlarmJournalDialog = new System.Windows.Forms.OpenFileDialog();
            this._saveAlarmJournalDialog = new System.Windows.Forms.SaveFileDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this._alarmJournalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msg1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._codeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ida1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Idb1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Idc1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ita1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Itb1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Itc1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ida2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Idb2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Idc2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ita2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Itb2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Itc2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ida3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Idb3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Idc3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ita3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Itb3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Itc3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaPr1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbPr1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcPr1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaPr2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbPr2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcPr2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaPr3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbPr3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcPr3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaPr4Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbPr4Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcPr4Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaPr5Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbPr5Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcPr5Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaPr6Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbPr6Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcPr6Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaPr7Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbPr7Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcPr7Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaPr8Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbPr8Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcPr8Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D4Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D5Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D6Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D7Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._saveToHTMLBtn = new System.Windows.Forms.Button();
            this._saveAlarmHTMLdialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _readAlarmJournalButton
            // 
            this._readAlarmJournalButton.Location = new System.Drawing.Point(3, 3);
            this._readAlarmJournalButton.Name = "_readAlarmJournalButton";
            this._readAlarmJournalButton.Size = new System.Drawing.Size(143, 23);
            this._readAlarmJournalButton.TabIndex = 1;
            this._readAlarmJournalButton.Text = "Прочитать";
            this._readAlarmJournalButton.UseVisualStyleBackColor = true;
            this._readAlarmJournalButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // _saveAlarmJournalButton
            // 
            this._saveAlarmJournalButton.Location = new System.Drawing.Point(152, 3);
            this._saveAlarmJournalButton.Name = "_saveAlarmJournalButton";
            this._saveAlarmJournalButton.Size = new System.Drawing.Size(143, 23);
            this._saveAlarmJournalButton.TabIndex = 20;
            this._saveAlarmJournalButton.Text = "Сохранить в файл";
            this._saveAlarmJournalButton.UseVisualStyleBackColor = true;
            this._saveAlarmJournalButton.Click += new System.EventHandler(this._saveAlarmJournalButton_Click);
            // 
            // _loadAlarmJournalButton
            // 
            this._loadAlarmJournalButton.Location = new System.Drawing.Point(301, 3);
            this._loadAlarmJournalButton.Name = "_loadAlarmJournalButton";
            this._loadAlarmJournalButton.Size = new System.Drawing.Size(143, 23);
            this._loadAlarmJournalButton.TabIndex = 21;
            this._loadAlarmJournalButton.Text = "Загрузить из файла";
            this._loadAlarmJournalButton.UseVisualStyleBackColor = true;
            this._loadAlarmJournalButton.Click += new System.EventHandler(this._loadAlarmJournalButton_Click);
            // 
            // _openAlarmJournalDialog
            // 
            this._openAlarmJournalDialog.DefaultExt = "xml";
            this._openAlarmJournalDialog.FileName = "Журнал аварий МР901";
            this._openAlarmJournalDialog.Filter = "ЖА МР901 (.xml)|*.xml|ЖА МР901 (.bin)|*.bin";
            this._openAlarmJournalDialog.RestoreDirectory = true;
            this._openAlarmJournalDialog.Title = "Открыть журнал  аварий для МР901";
            // 
            // _saveAlarmJournalDialog
            // 
            this._saveAlarmJournalDialog.DefaultExt = "xml";
            this._saveAlarmJournalDialog.FileName = "Журнал аварий МР901";
            this._saveAlarmJournalDialog.Filter = "(Журнал аварий МР901) | *.xml";
            this._saveAlarmJournalDialog.Title = "Сохранить  журнал аварий для МР901";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this._alarmJournalGrid);
            this.splitContainer1.Panel1MinSize = 100;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this._saveToHTMLBtn);
            this.splitContainer1.Panel2.Controls.Add(this.statusStrip1);
            this.splitContainer1.Panel2.Controls.Add(this._loadAlarmJournalButton);
            this.splitContainer1.Panel2.Controls.Add(this._readAlarmJournalButton);
            this.splitContainer1.Panel2.Controls.Add(this._saveAlarmJournalButton);
            this.splitContainer1.Panel2MinSize = 50;
            this.splitContainer1.Size = new System.Drawing.Size(849, 551);
            this.splitContainer1.SplitterDistance = 497;
            this.splitContainer1.TabIndex = 23;
            // 
            // _alarmJournalGrid
            // 
            this._alarmJournalGrid.AllowUserToAddRows = false;
            this._alarmJournalGrid.AllowUserToDeleteRows = false;
            this._alarmJournalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._alarmJournalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._alarmJournalGrid.BackgroundColor = System.Drawing.Color.White;
            this._alarmJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._alarmJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msg1Col,
            this._msgCol,
            this._codeCol,
            this._typeCol,
            this._IaCol,
            this._Ida1Col,
            this._Idb1Col,
            this._Idc1Col,
            this._Ita1Col,
            this._Itb1Col,
            this._Itc1Col,
            this._Ida2Col,
            this._Idb2Col,
            this._Idc2Col,
            this._Ita2Col,
            this._Itb2Col,
            this._Itc2Col,
            this._Ida3Col,
            this._Idb3Col,
            this._Idc3Col,
            this._Ita3Col,
            this._Itb3Col,
            this._Itc3Col,
            this._IaPr1Col,
            this._IbPr1Col,
            this._IcPr1Col,
            this._IaPr2Col,
            this._IbPr2Col,
            this._IcPr2Col,
            this._IaPr3Col,
            this._IbPr3Col,
            this._IcPr3Col,
            this._IaPr4Col,
            this._IbPr4Col,
            this._IcPr4Col,
            this._IaPr5Col,
            this._IbPr5Col,
            this._IcPr5Col,
            this._IaPr6Col,
            this._IbPr6Col,
            this._IcPr6Col,
            this._IaPr7Col,
            this._IbPr7Col,
            this._IcPr7Col,
            this._IaPr8Col,
            this._IbPr8Col,
            this._IcPr8Col,
            this._D0Col,
            this._D1Col,
            this._D2Col,
            this._D3Col,
            this._D4Col,
            this._D5Col,
            this._D6Col,
            this._D7Col});
            this._alarmJournalGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._alarmJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._alarmJournalGrid.Margin = new System.Windows.Forms.Padding(100, 3, 3, 100);
            this._alarmJournalGrid.Name = "_alarmJournalGrid";
            this._alarmJournalGrid.RowHeadersVisible = false;
            this._alarmJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._alarmJournalGrid.Size = new System.Drawing.Size(849, 497);
            this._alarmJournalGrid.TabIndex = 21;
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "_indexCol";
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 24;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "_timeCol";
            this._timeCol.HeaderText = "Дата/Время";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 77;
            // 
            // _msg1Col
            // 
            this._msg1Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._msg1Col.DataPropertyName = "_msg1Col";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msg1Col.DefaultCellStyle = dataGridViewCellStyle11;
            this._msg1Col.HeaderText = "Сообщение";
            this._msg1Col.Name = "_msg1Col";
            this._msg1Col.ReadOnly = true;
            this._msg1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._msg1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._msgCol.DataPropertyName = "_msgCol";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msgCol.DefaultCellStyle = dataGridViewCellStyle12;
            this._msgCol.HeaderText = "Ступень";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msgCol.Width = 120;
            // 
            // _codeCol
            // 
            this._codeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._codeCol.DataPropertyName = "_codeCol";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._codeCol.DefaultCellStyle = dataGridViewCellStyle13;
            this._codeCol.HeaderText = "Параметр срабатывания";
            this._codeCol.Name = "_codeCol";
            this._codeCol.ReadOnly = true;
            this._codeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._codeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _typeCol
            // 
            this._typeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._typeCol.DataPropertyName = "_typeCol";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._typeCol.DefaultCellStyle = dataGridViewCellStyle14;
            this._typeCol.HeaderText = "Значение параметра срабатывания";
            this._typeCol.Name = "_typeCol";
            this._typeCol.ReadOnly = true;
            this._typeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._typeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _IaCol
            // 
            this._IaCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._IaCol.DataPropertyName = "_IaCol";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IaCol.DefaultCellStyle = dataGridViewCellStyle15;
            this._IaCol.HeaderText = "Группа уставок";
            this._IaCol.Name = "_IaCol";
            this._IaCol.ReadOnly = true;
            this._IaCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _Ida1Col
            // 
            this._Ida1Col.DataPropertyName = "_Ida1Col";
            this._Ida1Col.HeaderText = "Iдa СШ1";
            this._Ida1Col.Name = "_Ida1Col";
            this._Ida1Col.ReadOnly = true;
            this._Ida1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Ida1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ida1Col.Width = 48;
            // 
            // _Idb1Col
            // 
            this._Idb1Col.DataPropertyName = "_Idb1Col";
            this._Idb1Col.HeaderText = "Iдb СШ1";
            this._Idb1Col.Name = "_Idb1Col";
            this._Idb1Col.ReadOnly = true;
            this._Idb1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Idb1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Idb1Col.Width = 48;
            // 
            // _Idc1Col
            // 
            this._Idc1Col.DataPropertyName = "_Idc1Col";
            this._Idc1Col.HeaderText = "Iдc СШ1";
            this._Idc1Col.Name = "_Idc1Col";
            this._Idc1Col.ReadOnly = true;
            this._Idc1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Idc1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Idc1Col.Width = 48;
            // 
            // _Ita1Col
            // 
            this._Ita1Col.DataPropertyName = "_Ita1Col";
            this._Ita1Col.HeaderText = "Iтa СШ1";
            this._Ita1Col.Name = "_Ita1Col";
            this._Ita1Col.ReadOnly = true;
            this._Ita1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Ita1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ita1Col.Width = 47;
            // 
            // _Itb1Col
            // 
            this._Itb1Col.DataPropertyName = "_Itb1Col";
            this._Itb1Col.HeaderText = "Iтb СШ1";
            this._Itb1Col.Name = "_Itb1Col";
            this._Itb1Col.ReadOnly = true;
            this._Itb1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Itb1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Itb1Col.Width = 47;
            // 
            // _Itc1Col
            // 
            this._Itc1Col.DataPropertyName = "_Itc1Col";
            this._Itc1Col.HeaderText = "Iтc СШ1";
            this._Itc1Col.Name = "_Itc1Col";
            this._Itc1Col.ReadOnly = true;
            this._Itc1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Itc1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Itc1Col.Width = 47;
            // 
            // _Ida2Col
            // 
            this._Ida2Col.DataPropertyName = "_Ida2Col";
            this._Ida2Col.HeaderText = "Iдa СШ2";
            this._Ida2Col.Name = "_Ida2Col";
            this._Ida2Col.ReadOnly = true;
            this._Ida2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Ida2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ida2Col.Width = 48;
            // 
            // _Idb2Col
            // 
            this._Idb2Col.DataPropertyName = "_Idb2Col";
            this._Idb2Col.HeaderText = "Iдb СШ2";
            this._Idb2Col.Name = "_Idb2Col";
            this._Idb2Col.ReadOnly = true;
            this._Idb2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Idb2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Idb2Col.Width = 48;
            // 
            // _Idc2Col
            // 
            this._Idc2Col.DataPropertyName = "_Idc2Col";
            this._Idc2Col.HeaderText = "Iдc СШ2";
            this._Idc2Col.Name = "_Idc2Col";
            this._Idc2Col.ReadOnly = true;
            this._Idc2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Idc2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Idc2Col.Width = 48;
            // 
            // _Ita2Col
            // 
            this._Ita2Col.DataPropertyName = "_Ita2Col";
            this._Ita2Col.HeaderText = "Iтa СШ2";
            this._Ita2Col.Name = "_Ita2Col";
            this._Ita2Col.ReadOnly = true;
            this._Ita2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Ita2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ita2Col.Width = 47;
            // 
            // _Itb2Col
            // 
            this._Itb2Col.DataPropertyName = "_Itb2Col";
            this._Itb2Col.HeaderText = "Iтb СШ2";
            this._Itb2Col.Name = "_Itb2Col";
            this._Itb2Col.ReadOnly = true;
            this._Itb2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Itb2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Itb2Col.Width = 47;
            // 
            // _Itc2Col
            // 
            this._Itc2Col.DataPropertyName = "_Itc2Col";
            this._Itc2Col.HeaderText = "Iтc СШ2";
            this._Itc2Col.Name = "_Itc2Col";
            this._Itc2Col.ReadOnly = true;
            this._Itc2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Itc2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Itc2Col.Width = 47;
            // 
            // _Ida3Col
            // 
            this._Ida3Col.DataPropertyName = "_Ida3Col";
            this._Ida3Col.HeaderText = "Iдa ПО";
            this._Ida3Col.Name = "_Ida3Col";
            this._Ida3Col.ReadOnly = true;
            this._Ida3Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Ida3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ida3Col.Width = 42;
            // 
            // _Idb3Col
            // 
            this._Idb3Col.DataPropertyName = "_Idb3Col";
            this._Idb3Col.HeaderText = "Iдb ПО";
            this._Idb3Col.Name = "_Idb3Col";
            this._Idb3Col.ReadOnly = true;
            this._Idb3Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Idb3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Idb3Col.Width = 42;
            // 
            // _Idc3Col
            // 
            this._Idc3Col.DataPropertyName = "_Idc3Col";
            this._Idc3Col.HeaderText = "Iдc ПО";
            this._Idc3Col.Name = "_Idc3Col";
            this._Idc3Col.ReadOnly = true;
            this._Idc3Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Idc3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Idc3Col.Width = 42;
            // 
            // _Ita3Col
            // 
            this._Ita3Col.DataPropertyName = "_Ita3Col";
            this._Ita3Col.HeaderText = "Iтa ПО";
            this._Ita3Col.Name = "_Ita3Col";
            this._Ita3Col.ReadOnly = true;
            this._Ita3Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Ita3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ita3Col.Width = 41;
            // 
            // _Itb3Col
            // 
            this._Itb3Col.DataPropertyName = "_Itb3Col";
            this._Itb3Col.HeaderText = "Iтb ПО";
            this._Itb3Col.Name = "_Itb3Col";
            this._Itb3Col.ReadOnly = true;
            this._Itb3Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Itb3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Itb3Col.Width = 41;
            // 
            // _Itc3Col
            // 
            this._Itc3Col.DataPropertyName = "_Itc3Col";
            this._Itc3Col.HeaderText = "Iтc ПО";
            this._Itc3Col.Name = "_Itc3Col";
            this._Itc3Col.ReadOnly = true;
            this._Itc3Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._Itc3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Itc3Col.Width = 41;
            // 
            // _IaPr1Col
            // 
            this._IaPr1Col.DataPropertyName = "_IaPr1Col";
            this._IaPr1Col.HeaderText = "Ia Прис.1";
            this._IaPr1Col.Name = "_IaPr1Col";
            this._IaPr1Col.ReadOnly = true;
            this._IaPr1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IaPr1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaPr1Col.Width = 54;
            // 
            // _IbPr1Col
            // 
            this._IbPr1Col.DataPropertyName = "_IbPr1Col";
            this._IbPr1Col.HeaderText = "Ib Прис.1";
            this._IbPr1Col.Name = "_IbPr1Col";
            this._IbPr1Col.ReadOnly = true;
            this._IbPr1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IbPr1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbPr1Col.Width = 54;
            // 
            // _IcPr1Col
            // 
            this._IcPr1Col.DataPropertyName = "_IcPr1Col";
            this._IcPr1Col.HeaderText = "Ic Прис.1";
            this._IcPr1Col.Name = "_IcPr1Col";
            this._IcPr1Col.ReadOnly = true;
            this._IcPr1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IcPr1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcPr1Col.Width = 54;
            // 
            // _IaPr2Col
            // 
            this._IaPr2Col.DataPropertyName = "_IaPr2Col";
            this._IaPr2Col.HeaderText = "Ia Прис.2";
            this._IaPr2Col.Name = "_IaPr2Col";
            this._IaPr2Col.ReadOnly = true;
            this._IaPr2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IaPr2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaPr2Col.Width = 54;
            // 
            // _IbPr2Col
            // 
            this._IbPr2Col.DataPropertyName = "_IbPr2Col";
            this._IbPr2Col.HeaderText = "Ib Прис.2";
            this._IbPr2Col.Name = "_IbPr2Col";
            this._IbPr2Col.ReadOnly = true;
            this._IbPr2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IbPr2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbPr2Col.Width = 54;
            // 
            // _IcPr2Col
            // 
            this._IcPr2Col.DataPropertyName = "_IcPr2Col";
            this._IcPr2Col.HeaderText = "Ic Прис.2";
            this._IcPr2Col.Name = "_IcPr2Col";
            this._IcPr2Col.ReadOnly = true;
            this._IcPr2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IcPr2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcPr2Col.Width = 54;
            // 
            // _IaPr3Col
            // 
            this._IaPr3Col.DataPropertyName = "_IaPr3Col";
            this._IaPr3Col.HeaderText = "Ia Прис.3";
            this._IaPr3Col.Name = "_IaPr3Col";
            this._IaPr3Col.ReadOnly = true;
            this._IaPr3Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IaPr3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaPr3Col.Width = 54;
            // 
            // _IbPr3Col
            // 
            this._IbPr3Col.DataPropertyName = "_IbPr3Col";
            this._IbPr3Col.HeaderText = "Ib Прис.3";
            this._IbPr3Col.Name = "_IbPr3Col";
            this._IbPr3Col.ReadOnly = true;
            this._IbPr3Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IbPr3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbPr3Col.Width = 54;
            // 
            // _IcPr3Col
            // 
            this._IcPr3Col.DataPropertyName = "_IcPr3Col";
            this._IcPr3Col.HeaderText = "Ic Прис.3";
            this._IcPr3Col.Name = "_IcPr3Col";
            this._IcPr3Col.ReadOnly = true;
            this._IcPr3Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IcPr3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcPr3Col.Width = 54;
            // 
            // _IaPr4Col
            // 
            this._IaPr4Col.DataPropertyName = "_IaPr4Col";
            this._IaPr4Col.HeaderText = "Ia Прис.4";
            this._IaPr4Col.Name = "_IaPr4Col";
            this._IaPr4Col.ReadOnly = true;
            this._IaPr4Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IaPr4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaPr4Col.Width = 54;
            // 
            // _IbPr4Col
            // 
            this._IbPr4Col.DataPropertyName = "_IbPr4Col";
            this._IbPr4Col.HeaderText = "Ib Прис.4";
            this._IbPr4Col.Name = "_IbPr4Col";
            this._IbPr4Col.ReadOnly = true;
            this._IbPr4Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IbPr4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbPr4Col.Width = 54;
            // 
            // _IcPr4Col
            // 
            this._IcPr4Col.DataPropertyName = "_IcPr4Col";
            this._IcPr4Col.HeaderText = "Ic Прис.4";
            this._IcPr4Col.Name = "_IcPr4Col";
            this._IcPr4Col.ReadOnly = true;
            this._IcPr4Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IcPr4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcPr4Col.Width = 54;
            // 
            // _IaPr5Col
            // 
            this._IaPr5Col.DataPropertyName = "_IaPr5Col";
            this._IaPr5Col.HeaderText = "Ia Прис.5";
            this._IaPr5Col.Name = "_IaPr5Col";
            this._IaPr5Col.ReadOnly = true;
            this._IaPr5Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IaPr5Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaPr5Col.Width = 54;
            // 
            // _IbPr5Col
            // 
            this._IbPr5Col.DataPropertyName = "_IbPr5Col";
            this._IbPr5Col.HeaderText = "Ib Прис.5";
            this._IbPr5Col.Name = "_IbPr5Col";
            this._IbPr5Col.ReadOnly = true;
            this._IbPr5Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IbPr5Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbPr5Col.Width = 54;
            // 
            // _IcPr5Col
            // 
            this._IcPr5Col.DataPropertyName = "_IcPr5Col";
            this._IcPr5Col.HeaderText = "Ic Прис.5";
            this._IcPr5Col.Name = "_IcPr5Col";
            this._IcPr5Col.ReadOnly = true;
            this._IcPr5Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IcPr5Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcPr5Col.Width = 54;
            // 
            // _IaPr6Col
            // 
            this._IaPr6Col.DataPropertyName = "_IaPr6Col";
            this._IaPr6Col.HeaderText = "Ia Прис.6";
            this._IaPr6Col.Name = "_IaPr6Col";
            this._IaPr6Col.ReadOnly = true;
            this._IaPr6Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IaPr6Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaPr6Col.Width = 54;
            // 
            // _IbPr6Col
            // 
            this._IbPr6Col.DataPropertyName = "_IbPr6Col";
            this._IbPr6Col.HeaderText = "Ib Прис.6";
            this._IbPr6Col.Name = "_IbPr6Col";
            this._IbPr6Col.ReadOnly = true;
            this._IbPr6Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IbPr6Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbPr6Col.Width = 54;
            // 
            // _IcPr6Col
            // 
            this._IcPr6Col.DataPropertyName = "_IcPr6Col";
            this._IcPr6Col.HeaderText = "Ic Прис.6";
            this._IcPr6Col.Name = "_IcPr6Col";
            this._IcPr6Col.ReadOnly = true;
            this._IcPr6Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IcPr6Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcPr6Col.Width = 54;
            // 
            // _IaPr7Col
            // 
            this._IaPr7Col.DataPropertyName = "_IaPr7Col";
            this._IaPr7Col.HeaderText = "Ia Прис.7";
            this._IaPr7Col.Name = "_IaPr7Col";
            this._IaPr7Col.ReadOnly = true;
            this._IaPr7Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IaPr7Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaPr7Col.Width = 54;
            // 
            // _IbPr7Col
            // 
            this._IbPr7Col.DataPropertyName = "_IbPr7Col";
            this._IbPr7Col.HeaderText = "Ib Прис.7";
            this._IbPr7Col.Name = "_IbPr7Col";
            this._IbPr7Col.ReadOnly = true;
            this._IbPr7Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IbPr7Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbPr7Col.Width = 54;
            // 
            // _IcPr7Col
            // 
            this._IcPr7Col.DataPropertyName = "_IcPr7Col";
            this._IcPr7Col.HeaderText = "Ic Прис.7";
            this._IcPr7Col.Name = "_IcPr7Col";
            this._IcPr7Col.ReadOnly = true;
            this._IcPr7Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IcPr7Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcPr7Col.Width = 54;
            // 
            // _IaPr8Col
            // 
            this._IaPr8Col.DataPropertyName = "_IaPr8Col";
            this._IaPr8Col.HeaderText = "Ia Прис.8";
            this._IaPr8Col.Name = "_IaPr8Col";
            this._IaPr8Col.ReadOnly = true;
            this._IaPr8Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IaPr8Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaPr8Col.Width = 54;
            // 
            // _IbPr8Col
            // 
            this._IbPr8Col.DataPropertyName = "_IbPr8Col";
            this._IbPr8Col.HeaderText = "Ib Прис.8";
            this._IbPr8Col.Name = "_IbPr8Col";
            this._IbPr8Col.ReadOnly = true;
            this._IbPr8Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IbPr8Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbPr8Col.Width = 54;
            // 
            // _IcPr8Col
            // 
            this._IcPr8Col.DataPropertyName = "_IcPr8Col";
            this._IcPr8Col.HeaderText = "Ic Прис.8";
            this._IcPr8Col.Name = "_IcPr8Col";
            this._IcPr8Col.ReadOnly = true;
            this._IcPr8Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._IcPr8Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcPr8Col.Width = 54;
            // 
            // _D0Col
            // 
            this._D0Col.DataPropertyName = "_D0Col";
            this._D0Col.HeaderText = "D[1-8]";
            this._D0Col.Name = "_D0Col";
            this._D0Col.ReadOnly = true;
            this._D0Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._D0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D0Col.Width = 42;
            // 
            // _D1Col
            // 
            this._D1Col.DataPropertyName = "_D1Col";
            this._D1Col.HeaderText = "D[9-16]";
            this._D1Col.Name = "_D1Col";
            this._D1Col.ReadOnly = true;
            this._D1Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._D1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D1Col.Width = 48;
            // 
            // _D2Col
            // 
            this._D2Col.DataPropertyName = "_D2Col";
            this._D2Col.HeaderText = "D[17-24]";
            this._D2Col.Name = "_D2Col";
            this._D2Col.ReadOnly = true;
            this._D2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._D2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D2Col.Width = 54;
            // 
            // _D3Col
            // 
            this._D3Col.DataPropertyName = "_D3Col";
            this._D3Col.HeaderText = "D[25-32]";
            this._D3Col.Name = "_D3Col";
            this._D3Col.ReadOnly = true;
            this._D3Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._D3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D3Col.Width = 54;
            // 
            // _D4Col
            // 
            this._D4Col.DataPropertyName = "_D4Col";
            this._D4Col.HeaderText = "D[33-40]";
            this._D4Col.Name = "_D4Col";
            this._D4Col.ReadOnly = true;
            this._D4Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._D4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D4Col.Width = 54;
            // 
            // _D5Col
            // 
            this._D5Col.DataPropertyName = "_D5Col";
            this._D5Col.HeaderText = "D[41-48]";
            this._D5Col.Name = "_D5Col";
            this._D5Col.ReadOnly = true;
            this._D5Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._D5Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D5Col.Width = 54;
            // 
            // _D6Col
            // 
            this._D6Col.DataPropertyName = "_D6Col";
            this._D6Col.HeaderText = "D[49-56]";
            this._D6Col.Name = "_D6Col";
            this._D6Col.ReadOnly = true;
            this._D6Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._D6Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D6Col.Width = 54;
            // 
            // _D7Col
            // 
            this._D7Col.DataPropertyName = "_D7Col";
            this._D7Col.HeaderText = "D[57-64]";
            this._D7Col.Name = "_D7Col";
            this._D7Col.ReadOnly = true;
            this._D7Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._D7Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D7Col.Width = 54;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 28);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(849, 22);
            this.statusStrip1.TabIndex = 22;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _saveToHTMLBtn
            // 
            this._saveToHTMLBtn.Location = new System.Drawing.Point(450, 3);
            this._saveToHTMLBtn.Name = "_saveToHTMLBtn";
            this._saveToHTMLBtn.Size = new System.Drawing.Size(143, 23);
            this._saveToHTMLBtn.TabIndex = 23;
            this._saveToHTMLBtn.Text = "Сохранить в HTML";
            this._saveToHTMLBtn.UseVisualStyleBackColor = true;
            this._saveToHTMLBtn.Click += new System.EventHandler(this._saveJournaloToHTML);
            // 
            // _saveAlarmHTMLdialog
            // 
            this._saveAlarmHTMLdialog.DefaultExt = "xml";
            this._saveAlarmHTMLdialog.FileName = "Журнал аварий МР902 HTML";
            this._saveAlarmHTMLdialog.Filter = "(Журнал аварий МР902) | *.html";
            this._saveAlarmHTMLdialog.Title = "Сохранить  журнал аварий для МР902";
            // 
            // AlarmJournalBigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 551);
            this.Controls.Add(this.splitContainer1);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "AlarmJournalBigForm";
            this.Text = "Mr901AlarmJournalForm";
            this.Load += new System.EventHandler(this.AlarmJournalForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button _readAlarmJournalButton;
        private System.Windows.Forms.Button _saveAlarmJournalButton;
        private System.Windows.Forms.Button _loadAlarmJournalButton;
        private System.Windows.Forms.OpenFileDialog _openAlarmJournalDialog;
        private System.Windows.Forms.SaveFileDialog _saveAlarmJournalDialog;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView _alarmJournalGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msg1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _codeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _typeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ida1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Idb1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Idc1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ita1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Itb1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Itc1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ida2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Idb2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Idc2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ita2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Itb2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Itc2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ida3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Idb3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Idc3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ita3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Itb3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Itc3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaPr1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbPr1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcPr1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaPr2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbPr2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcPr2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaPr3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbPr3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcPr3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaPr4Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbPr4Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcPr4Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaPr5Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbPr5Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcPr5Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaPr6Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbPr6Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcPr6Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaPr7Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbPr7Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcPr7Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaPr8Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbPr8Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcPr8Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D4Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D5Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D6Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D7Col;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.Button _saveToHTMLBtn;
        private System.Windows.Forms.SaveFileDialog _saveAlarmHTMLdialog;
    }
}