<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy® -->
<xsl:stylesheet version="1.1"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head/>
      <body>
        <xsl:variable name="type" select="��902/���_������������_����������"/>
        <xsl:variable name="countCon" select="��902/����������_�������������"/>
        <xsl:variable name="countRelay" select="��902/����������_����"/>

        <h3>���������� <xsl:value-of select="��902/���_����������"/> . ������ �� <xsl:value-of select="��902/������"/> . ����� <xsl:value-of select="��902/�����_����������"/></h3>
        <p></p>
        
        <b>�������������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="32CD32">
            <th>�������</th>
            <th>I��, �</th>
            <th>������.</th>
            <th>�����.</th>
            <th>��������</th>
            <th>����</th>
            <th>���������</th>
            <th>�������� ���������, ��</th>
          </tr>
          <xsl:for-each select="��902/�������������/Rows/ConnectionBigStruct">
            <tr align="center">
              <xsl:if test="position() &lt; ($countCon+1)">
                <xsl:choose>
                  <xsl:when test="$type = 'A1' or $type = ''">
                    <xsl:if test="position() &lt; 6">
                      <td>������������� <xsl:value-of select="position()"/></td>
                      <td>
                        <xsl:value-of select="I��"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:value-of select="���������"/>
                      </td>
                      <td>
                        <xsl:value-of select="��������"/>
                      </td>
                      <td>
                        <xsl:value-of select="����"/>
                      </td>
                      <td>
                        <xsl:for-each select="���������">
                          <xsl:if test="current() ='false'">���</xsl:if>
                          <xsl:if test="current() ='true'">��</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="��������_���������"/>
                      </td>
                    </xsl:if>
                    <xsl:if test="position()=6">
                      <td>In</td>
                        <td><xsl:value-of select="I��"/></td>
                        <td><xsl:value-of select="����������"/></td>
                        <td><xsl:value-of select="���������"/></td>
                        <td><xsl:value-of select="��������"/></td>
                        <td><xsl:value-of select="����"/></td>
                        <td><xsl:for-each select="���������">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td><xsl:value-of select="��������_���������"/></td>
                    </xsl:if>
                  </xsl:when>
                  <xsl:otherwise>
                      <td>������������� <xsl:value-of select="position()"/></td>
                      <td><xsl:value-of select="I��"/></td>
                      <td><xsl:value-of select="����������"/></td>
                      <td><xsl:value-of select="���������"/></td>
                      <td><xsl:value-of select="��������"/></td>
                      <td><xsl:value-of select="����"/></td>
                      <td>
                        <xsl:for-each select="���������">
                          <xsl:if test="current() ='false'">���</xsl:if>
                          <xsl:if test="current() ='true'">��</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td><xsl:value-of select="��������_���������"/></td>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:if>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>
        
        <h3>������� �������</h3>
        <b>������� ���������� �������</b>
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������� �</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="FFFFCC">
                <th>����� ��</th>
                <th>������������</th>
              </tr>

              <xsl:for-each select="��902/������������_�������_��������/��">
                <xsl:if test="position() &lt; 9 ">
                  <tr>
                    <td>
                      <xsl:value-of  select="position()"/>
                    </td>

                    <td>
                      <xsl:for-each select="�������">
                        <xsl:if test="current() !='���'">
                          <xsl:if test="current() ='��'">
                            �<xsl:value-of select="position()"/>
                          </xsl:if>
                          <xsl:if test="current() ='������'">
                            ^�<xsl:value-of select="position()"/>
                          </xsl:if>
                        </xsl:if>
                      </xsl:for-each>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:for-each>
            </table>

            <b>���������� ������� ���</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="FFFFCC">
                <th>����� ��</th>
                <th>������������</th>
              </tr>

              <xsl:for-each select="��902/������������_�������_��������/��">
                <xsl:if test="position() &gt; 8 ">
                  <tr>
                    <td>
                      <xsl:value-of  select="position()"/>
                    </td>

                    <td>
                      <xsl:for-each select="�������">
                        <xsl:if test="current() !='���'">
                          <xsl:if test="current() ='��'">
                            �<xsl:value-of select="position()"/>
                          </xsl:if>
                          <xsl:if test="current() ='������'">
                            ^�<xsl:value-of select="position()"/>
                          </xsl:if>
                        </xsl:if>
                      </xsl:for-each>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:for-each>
            </table>
          </td>
        </table>
        <p></p>
        
        <table border="1" cellspacing="0">
          <tr bgcolor="FFFFCC">
            <th>��������� ������ �������</th>
            <th>����� ���������</th>
            <th>����� ������������� ��</th>
          </tr>
          <tr align="center">
            <td><xsl:value-of select="��902/�������_�������/���������_������"/></td>
            <td><xsl:value-of select="��902/�������_�������/�����_���������"/></td>
            <td><xsl:value-of select="��902/����_��/�����_�������������_��"/></td>
          </tr>
        </table>
        <p></p>

        <h3>�������� �������</h3>
        
        <b>�������� ����</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="4682B4">
            <th>�����</th>
            <th>���</th>
            <th>������</th>
            <th>������, ��</th>
          </tr>
          <xsl:for-each select="��902/����/���_����/����_����">
            <xsl:if test="position() &lt; ($countRelay+1)">
              <tr align="center">
                <td><xsl:value-of select="position()"/></td>
                <td><xsl:value-of select= "@���"/></td>
                <td><xsl:value-of select= "@������"/></td>
                <td><xsl:value-of select= "@�����"/></td>
              </tr></xsl:if>
          </xsl:for-each>
        </table>
        <p></p>
      
        <b>����������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="4682B4">
            <th>�����</th>
            <th>���</th>
            <th>������</th>
            <th>����</th>
          </tr>
          <xsl:for-each select="��902/����������/���_����������/����_���������">
            <tr align="center">
              <td><xsl:value-of select="position()"/></td>
              <td><xsl:value-of select= "@���" /></td>
              <td><xsl:value-of select= "@������" /></td>
              <td><xsl:value-of select= "@����_����������" /></td>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>
        
        <b>���� �������������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="4682B4">
            <th>���������� �������������</th>
            <th>����������� �������������</th>
            <th>������������� ����� ��</th>
            <th>����� ����������� (����)</th>
            <th>������, ��</th>
          </tr>
          <tr align="center">
            <td>
              <xsl:for-each select="��902/����_�������������/�������������_1">
                <xsl:if test="current() ='false'">���</xsl:if>
                <xsl:if test="current() ='true'">��</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="��902/����_�������������/�������������_2">
                <xsl:if test="current() ='false'">���</xsl:if>
                <xsl:if test="current() ='true'">��</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="��902/����_�������������/�������������_3">
                <xsl:if test="current() ='false'">���</xsl:if>
                <xsl:if test="current() ='true'">��</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="��902/����_�������������/�������������_4">
                <xsl:if test="current() ='false'">���</xsl:if>
                <xsl:if test="current() ='true'">��</xsl:if>
              </xsl:for-each>
            </td>
            <td><xsl:value-of select= "��902/����_�������������/�������_����_�������������" /></td>
          </tr>
        </table>
        <p></p>
        
        <b>���</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="c1ced5">
            <th>�����</th>
            <th>������������</th>
          </tr>
          <xsl:for-each select="��902/���_���/���">
            <xsl:if test="position() &lt; 17">
              <tr>
                <td><xsl:value-of select="position()"/></td>
                <td>
                  <xsl:for-each select="�������">
                    <xsl:value-of select="current()"/>|
                  </xsl:for-each>
                </td>
              </tr>
            </xsl:if>
          </xsl:for-each>
        </table>
        <p></p>
        
        <p></p>
        <h3>�����������</h3>
      
        <table border="1" cellspacing="0">
          <td>
            <table border="1" cellspacing="0">
              <tr bgcolor="98FB98">
                <th>����������</th>
                <th>����. ����������, %</th>
                <th>������. ��</th>
                <th>���� �����</th>
              </tr>
              <tr align="center">
                <td><xsl:value-of select="��902/������������_������������/����������_�����������"/> - <xsl:value-of select="��902/����������_���_��"/></td>
                <td><xsl:value-of select="��902/������������_������������/����������"/></td>
                <td><xsl:value-of select="��902/������������_������������/��������"/></td>
                <td><xsl:value-of select="��902/������������_������������/����_�����_������������"/></td>
              </tr>

              <p></p><tr>��������� �����������</tr>
              
            </table>
            <table border="1" cellspacing="0">
              <tr bgcolor="98FB98">
                <th>�����</th>
                <th>������</th>
              </tr>
              <xsl:for-each select="��902/������������_������������/���_������/������">
                <tr align="center">
                  <td><xsl:value-of select="position()+32"/></td>
                  <td><xsl:value-of select="@�����"/></td>
                </tr>
              </xsl:for-each>

              <p></p>
              <tr>������ �����������</tr>
            </table>
          </td>
        </table>

        <p></p><tr>��������������� ���������� ������</tr>
        
        <table border="1" cellpadding="4" cellspacing="0">
          <tr bgcolor="FF69B4">
            <th>�����</th>
            <th>����</th>
            <th>�����</th>
          </tr>
          <xsl:for-each select="��902/������������_������������/������������_�������_����/���_������/ChannelWithBase">
            <tr align="center">
              <td><xsl:value-of select="position()"/></td>
              <td><xsl:value-of select="@����"/></td>
              <td><xsl:value-of select="@�����"/></td>
            </tr>
          </xsl:for-each>
        </table>
        
        <h3>����</h3>
        <b>���</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FFFFE0">
            <th>�����</th>
            <th>�����</th>
            <th>�� �����</th>
            <th>�� ����</th>
            <th>t����1, ��</th>
            <th>t����2, ��</th>
            <th>t����3, ��</th>
            <th>�� ����</th>
            <th>��1</th>
            <th>��2</th>
            <th>��</th>
          </tr>
          <tr align="center">
            <td><xsl:value-of select="��902/����/�����"/></td>
            <td><xsl:value-of select="��902/����/�����"/></td>
            <td><xsl:value-of select="��902/����/��_�����"/></td>
            <td><xsl:value-of select="��902/����/��_����"/></td>
            <td><xsl:value-of select="��902/����/�1"/></td>
            <td><xsl:value-of select="��902/����/�2"/></td>
            <td><xsl:value-of select="��902/����/�3"/></td>
            <td><xsl:value-of select="��902/����/��_����"/></td>
            <td><xsl:value-of select="��902/����/��1"/></td>
            <td><xsl:value-of select="��902/����/��2"/></td>
            <td><xsl:value-of select="��902/����/��"/></td>
          </tr>
        </table>
        
        <p></p>
        <b>�������������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="4682B4">
            <th>�������</th>
            <th>l����, l�</th>
            <th>t����, ��</th>
          </tr>
          <xsl:for-each select="��902/����_�������������/Rows/UrovConnectionNewStruct">
            <tr align="center">
              <xsl:if test="position() &lt; ($countCon+1)">
                <xsl:choose>
                  <xsl:when test="$type = 'A1' or $type = ''">
                    <xsl:if test="position() &lt; 6">
                      <td>������������� <xsl:value-of select="position()"/></td>
                    </xsl:if>
                    <xsl:if test="position()=6">
                      <td>In</td>
                    </xsl:if>
                  </xsl:when>
                  <xsl:otherwise>
                    <td>������������� <xsl:value-of select="position()"/></td>
                  </xsl:otherwise>
                </xsl:choose>
                <td><xsl:value-of select="UrovJoinI"/></td>
                <td><xsl:value-of select="UrovJoinT"/></td>
              </xsl:if>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>

        <h3>�������� ����� TT</h3>
        <table border="1" cellspacing="0">
          <tr bgcolor="FF69B4">
            <th>�������� ����� ��</th>
            <th>l�min, l�</th>
            <th>T��, ��</th>
            <th>�����</th>
            <th>�����</th>
          </tr>
          <xsl:for-each select="��902/����_��/����_��_���/Rows/ControlTtBigStruct">
            <tr align="center">
              <td>
                <xsl:if test="position() =1">��1</xsl:if>
                <xsl:if test="position() =2">��2</xsl:if>
                <xsl:if test="position() =3">��</xsl:if>
              </td>
              <td><xsl:value-of select="I�min"/></td>
              <td><xsl:value-of select="T��"/></td>
              <td><xsl:value-of select="�������������"/></td>
              <td><xsl:value-of select="�����"/></td>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>
        
        <h3>������. �������� ������</h3>
        
        <table border="1" cellspacing="0">
          <td>
            <b>���������������� ������ �� ����������� ���������</b>
            <table border="1" cellspacing="0">
              <tr bgcolor="B0C4DE">
                <th>�������</th>
                <th>�����</th>
                <th>����������</th>
                <th>���� �� I�3 ��</th>
                <th>l�>, l�</th>
                <th>l�>>, l�</th>
                <th>t��, ��</th>
                <th>l�, l�</th>
                <th>f</th>
                <th>����.������.2</th>
                <th>l2�, %</th>
                <th>����.������.5</th>
                <th>l5�, %</th>
                <th>�����.�����.</th>
                <th>������������</th>
                <th>l�*,l�</th>
                <th>t��, ��</th>
                <th>���� �������.</th>
                <th>���.</th>
                <th>����</th>
              </tr>
              <xsl:for-each select="��902/������/��������_������_�������/���/Rows/DifferentialCurrentBigStruct">
                <xsl:if test="position() &lt; 4 ">
                  <tr align="center">
                    <td>
                      <xsl:if test="position() =1">l�1 ��1</xsl:if>
                      <xsl:if test="position() =2">l�2 ��2</xsl:if>
                      <xsl:if test="position() =3">l�3 ��</xsl:if>
                    </td>
                    <td><xsl:value-of select="�����"/></td>
                    <td><xsl:value-of select="����������"/></td>
                    <td>
                      <xsl:for-each select="��">
                        <xsl:if test="current() ='false'">���</xsl:if>
                        <xsl:if test="current() ='true'">��</xsl:if>
                      </xsl:for-each>
                    </td>
                    <td><xsl:value-of select="�������"/></td>
                    <td><xsl:value-of select="�������2"/></td>
                    <td><xsl:value-of select="t��"/></td>
                    <td><xsl:value-of select="I�"/></td>
                    <td><xsl:value-of select="f"/></td>
                    <td>
                      <xsl:for-each select="����2">
                        <xsl:if test="current() ='false'">���</xsl:if>
                        <xsl:if test="current() ='true'">��</xsl:if>
                      </xsl:for-each>
                    </td>
                    <td><xsl:value-of select="����2�������"/></td>
                    <td>
                      <xsl:for-each select="����5">
                        <xsl:if test="current() ='false'">���</xsl:if>
                        <xsl:if test="current() ='true'">��</xsl:if>
                      </xsl:for-each>
                    </td>
                    <td><xsl:value-of select="����5�������"/></td>
                    <td>
                      <xsl:for-each select="�����">
                        <xsl:if test="current() ='false'">���</xsl:if>
                        <xsl:if test="current() ='true'">��</xsl:if>
                      </xsl:for-each>
                    </td>
                    <td>
                      <xsl:for-each select="�����������">
                        <xsl:if test="current() ='false'">���</xsl:if>
                        <xsl:if test="current() ='true'">��</xsl:if>
                      </xsl:for-each>
                    </td>
                    <td><xsl:value-of select="I�"/></td>
                    <td><xsl:value-of select="T��"/></td>
                    <td><xsl:value-of select="����_��"/></td>
                    <td><xsl:value-of select="���"/></td>
                    <td>
                      <xsl:for-each select="����">
                        <xsl:if test="current() ='false'">���</xsl:if>
                        <xsl:if test="current() ='true'">��</xsl:if>
                      </xsl:for-each>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:for-each>
            </table>

            <b>���������������� ������ �� ���������� ���������</b>
            <table border="1" cellspacing="0">
              <tr bgcolor="B0C4DE">
                <th>�������</th>
                <th>�����</th>
                <th>����������</th>
                <th>���� �� I�3 ��</th>
                <th>l�>, l�</th>
                <th>l�>>, l�</th>
                <th>l�, l�</th>
                <th>f</th>
                <th>������������</th>
                <th>l�*,l�</th>
                <th>t��, ��</th>
                <th>���� �������.</th>
                <th>���.</th>
                <th>����</th>
              </tr>
              <xsl:for-each select="��902/������/��������_������_�������/���/Rows/DifferentialCurrentBigStruct">
                <xsl:if test="position() &gt; 3 ">
                  <tr align="center">
                    <td>
                      <xsl:if test="position() =4">l�1 ��1</xsl:if>
                      <xsl:if test="position() =5">l�2 ��2</xsl:if>
                      <xsl:if test="position() =6">l�3 ��</xsl:if>
                    </td>
                    <td><xsl:value-of select="�����"/></td>
                    <td><xsl:value-of select="����������"/></td>
                    <td>
                      <xsl:for-each select="��">
                        <xsl:if test="current() ='false'">���</xsl:if>
                        <xsl:if test="current() ='true'">��</xsl:if>
                      </xsl:for-each>
                    </td>
                    <td><xsl:value-of select="�������"/></td>
                    <td><xsl:value-of select="�������2"/></td>
                    <td><xsl:value-of select="I�"/></td>
                    <td><xsl:value-of select="f"/></td>
                    <td>
                      <xsl:for-each select="�����������">
                        <xsl:if test="current() ='false'">���</xsl:if>
                        <xsl:if test="current() ='true'">��</xsl:if>
                      </xsl:for-each>
                    </td>
                    <td><xsl:value-of select="I�"/></td>
                    <td><xsl:value-of select="T��"/></td>
                    <td><xsl:value-of select="����_��"/></td>
                    <td><xsl:value-of select="���"/></td>
                    <td>
                      <xsl:for-each select="����">
                        <xsl:if test="current() ='false'">���</xsl:if>
                        <xsl:if test="current() ='true'">��</xsl:if>
                      </xsl:for-each>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:for-each>
            </table>
          </td>
        </table>

        <p></p>
        <b>������ I</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="B0C4DE">
            <th>�������</th>
            <th>�����</th>
            <th>����������</th>
            <th>������</th>
            <th>���������</th>
            <th>l��, l�</th>
            <th>��������������</th>
            <th>t, ��</th>
            <th>k �����. ���-��</th>
            <th>�����������</th>
            <th>����</th>
          </tr>
          <xsl:for-each select="��902/������/��������_������_�������/���/Rows/MtzBigStruct">
            <tr align="center">
              <td>������� I> <xsl:value-of select="position()"/></td>
              <td><xsl:value-of select="�����"/></td>
              <td><xsl:value-of select="����������"/></td>
              <td><xsl:value-of select="������"/></td>
              <td><xsl:value-of select="���������"/></td>
              <td><xsl:value-of select="�������"/></td>
              <td><xsl:value-of select="��������������"/></td>
              <td><xsl:value-of select="t��"/></td>
              <td><xsl:value-of select="�"/></td>
              <td><xsl:value-of select="���"/></td>
              <td>
                <xsl:for-each select="����">
                  <xsl:if test="current() ='false'">���</xsl:if>
                  <xsl:if test="current() ='true'">��</xsl:if>
                </xsl:for-each>
              </td>
            </tr>
          </xsl:for-each>
        </table>

        <p></p>
        <b>�������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="B0C4DE">
            <th>�������</th>
            <th>�����</th>
            <th>����������</th>
            <th>����������</th>
            <th>������������</th>
            <th>t��, ��</th>
            <th>t��, ��</th>
            <th>������ ��������</th>
            <th>�������</th>
            <th>�����������</th>
            <th>����</th>
          </tr>
          <xsl:for-each select="��902/������/��������_������_�������/�������/Rows/ExternalDefenseBigStruct">
            <tr align="center">
              <td>������� <xsl:value-of select="position()"/></td>
              <td><xsl:value-of select="�����"/></td>
              <td><xsl:value-of select="����"/></td>
              <td><xsl:value-of select="����������"/></td>
              <td><xsl:value-of select="����"/></td>
              <td><xsl:value-of select="t��"/></td>
              <td><xsl:value-of select="t��"/></td>
              <td><xsl:value-of select="����_�������"/></td>
              <td><xsl:value-of select="�������"/></td>
              <td><xsl:value-of select="���"/></td>
              <td><xsl:value-of select="����"/></td>
            </tr>
          </xsl:for-each>
        </table>

        <p></p>
        <b>������������ Ethernet</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="B0C4DE">
            <th>IP-�����</th>
          </tr>
          <xsl:for-each select="��902/IP">
            <tr align="center">
              <td>
                <xsl:value-of select="IpHi2"/>.
                <xsl:value-of select="IpHi1"/>.
                <xsl:value-of select="IpLo2"/>.
                <xsl:value-of select="IpLo1"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>

      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>