﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Forms;
using BEMN.Forms.Structures;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MLK10.Properties;

namespace BEMN.MLK10.Mlk10Querys
{
    public partial class Mlk10Querys : Form, IFormView
    {
        #region Feilds
        private const int WORD_BITS = 16;
        private int _wordCnt;

        private MLK10 _device;
        private MemoryEntity<SomeStruct> _query;
        private HexTextbox _currentHex;
        private Button _currentButtton;
        private TextBox _currentAdduction;
        private LedControl[][] _words;
        private AdductionSettings[] _adduction;
        private Label[] _addrLabels;
        private HexTextbox[] _hexLabels;
        private HexTextbox[] _decLabels;
        private TextBox[] _adductionBoxes;

        private bool _loop = false;
        #endregion

        #region

        private const string SET_BIT_PATTERN = "SetBit_{0}_{1}";
        #endregion

        #region Constructors
        public Mlk10Querys()
        {
            InitializeComponent();
            _countOfWord.SelectedIndex = 0;
            _wordCnt = Convert.ToInt32(_countOfWord.SelectedItem);
            _words = new LedControl[_wordCnt][];
            _adduction = new AdductionSettings[_wordCnt];
            _addrLabels = new Label[_wordCnt];
            _hexLabels = new HexTextbox[_wordCnt];
            _decLabels = new HexTextbox[_wordCnt];
            _adductionBoxes = new TextBox[_wordCnt];
        }

        public Mlk10Querys(MLK10 device)
        {
            InitializeComponent();
            _countOfWord.SelectedIndex = 0;
            _countOfWord.SelectedIndexChanged += new System.EventHandler(this._countOfWord_SelectedIndexChanged);
            _addrHex.TextChanged += new EventHandler(_addrHex_TextChanged);
            _device = device;
            _device.CaptionChanged += new CaptionPropertyChanged(OnDeviceCaptionChanged);
            _device.DeviceNumberChanged += DeviceOnDeviceNumberChanged;
            InitNewQuery();
        }
        #endregion

        #region Init
        private void InitNewQuery()
        {
            string queryName = string.Format("Обмены{0}", _device.Counter);
            _query = new MemoryEntity<SomeStruct>(queryName, _device.MB, 0x0000)
            {
                DeviceNum = _device.DeviceNumber,
                Caption = _device.Caption
            };
            _query.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, MLKQueryReadComplete);
            _query.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, MLKQueryReadFail);
            _device.Counter++;
        }

        private void MLK10_Querys_Load(object sender, EventArgs e)
        {
            InitControls();
            SetAddrLabelText();

            _addrHex.BorderStyle = BorderStyle.Fixed3D;
            _addrHex.Text = _device.Adress.ToString("X");
            _addrHex.Correct();

            _bitfRadio.Checked = _device.IsBit;
            _queryEnableCheckBox.Checked = true;
        }

        private void InitControls()
        {
            _addrHex.HexBase = true;
            _addrHex.Correct();

            _ledsGroup.Controls.Clear();
            _addrGroup.Controls.Clear();
            _decGroup.Controls.Clear();
            _hexGroup.Controls.Clear();
            _adductionGroup.Controls.Clear();

            _wordCnt = _countOfWord.SelectedIndex + 8;

            InitLedsControl();

            _adduction = new AdductionSettings[_wordCnt];
            _addrLabels = new Label[_wordCnt];
            _hexLabels = new HexTextbox[_wordCnt];
            _decLabels = new HexTextbox[_wordCnt];
            _adductionBoxes = new TextBox[_wordCnt];
            for (int i = 0; i < _wordCnt; i++)
            {
                CreateAddrLabel(i);
                CreateDecLabel(i);
                CreateHexLabel(i);
                CreateAdductionBox(i);

                _ledsGroup.Controls.AddRange(_words[i]);
                _wordIndexCombo.Items.Add(i + 1);
                _adduction[i] = new AdductionSettings(100.0, 65535, 3);
            }

            for (int j = 0; j < 4; j++)
            {
                _symbolsCombo.Items.Add(j);
            }

            _addrGroup.Controls.AddRange(_addrLabels);
            _decGroup.Controls.AddRange(_decLabels);
            _hexGroup.Controls.AddRange(_hexLabels);
            _adductionGroup.Controls.AddRange(_adductionBoxes);

            _wordIndexCombo.SelectedIndex = 0;
            _symbolsCombo.SelectedIndex = 3;
        }

        private void InitLedsControl()
        {
            _words = new LedControl[_wordCnt][];
            for (int i = 0; i < _wordCnt; i++)
            {
                _words[i] = new LedControl[WORD_BITS];
                for (int j = 0; j < WORD_BITS; j++)
                {
                    _words[i][j] = new LedControl();
                    _words[i][j].Location = j < 8
                        ? new Point(198 - j * 12, 14 + i * 18)
                        : new Point(198 - j * 12 - 6, 14 + i * 18);
                    _words[i][j].State = LedState.Off;
                    _words[i][j].pictureBox.MouseClick += new MouseEventHandler(Led_MouseClick);
                    _ledsGroup.Controls.Add(_words[i][j]);
                }
            }
        }
        #endregion

        #region ReadStruct

        public void MLKQueryReadFail()
        {
            if (false == Disposing && false == IsDisposed)
            {
                TurnOff();
            }
        }

        public void MLKQueryReadComplete() 
        {
            if (false == Disposing && false == IsDisposed)
            {
                byte[] buffer = Common.TOBYTES(_query.Values, true);

                for (int i = 0; i < buffer.Length; i += 2)
                {
                    LedManager.SetLeds(_words[i / 2], new BitArray(new byte[] { buffer[i + 1], buffer[i] }));
                    if ((bool)_hexLabels[i / 2].Tag)
                    {
                        _hexLabels[i / 2].Text = (Common.TOWORD(buffer[i], buffer[i + 1])).ToString("X4");
                    }
                    if ((bool)_decLabels[i / 2].Tag)
                    {
                        _decLabels[i / 2].Text = (Common.TOWORD(buffer[i], buffer[i + 1])).ToString("");
                    }

                    if ((bool)_adductionBoxes[i / 2].Tag)
                    {
                        ushort val = Common.TOWORD(buffer[i], buffer[i + 1]);
                        double adduct = val * _adduction[i / 2].Limit / _adduction[i / 2].Max;

                        _adductionBoxes[i / 2].Text = adduct.ToString("F" + _adduction[i / 2].Symbols);
                    }
                }
            }
        }
        #endregion

        #region Functions
        private void TurnOff()
        {
            if (LedManager.IsControlsCreated(_decLabels))
            {
                for (int i = 0; i < _wordCnt; i++)
                {
                    LedManager.TurnOffLeds(_words[i]);
                    _hexLabels[i].Text = "";
                    _decLabels[i].Text = "";
                    _adductionBoxes[i].Text = "";
                }
            }
        }

        /// <summary>
        /// Отобразить адреса
        /// </summary>
        private void SetAddrLabelText()
        {
            //Если адресация словная
            if (_wordfRadio.Checked)
            {
                for (int i = 0; i < _addrLabels.Length; i++)
                {
                    //Адреса больше 0xFFFF не рисуем
                    if (_addrHex.Number + i <= ushort.MaxValue)
                    {
                        _addrLabels[i].Text = (_addrHex.Number + i).ToString("X4");
                    }
                    else
                    {
                        _addrLabels[i].Text = "";
                    }

                    //Строка слишком длинная, метку надо отодвинуть
                    if (50 > _addrLabels[i].Location.X)
                    {
                        _addrLabels[i].Location = new Point(_addrLabels[i].Location.X + 35, _addrLabels[i].Location.Y);
                    }
                }
            }
            else
            {
                //Если адресация битная
                for (int i = 0; i < _addrLabels.Length; i++)
                {
                    if (_addrHex.Number + i + 0xF <= ushort.MaxValue)
                    {
                        _addrLabels[i].Text = (_addrHex.Number + 0x10 * i + 0xF).ToString("X4") + "-" +
                                              (_addrHex.Number + 0x10 * i).ToString("X4");
                    }
                    else
                    {
                        _addrLabels[i].Text = "";
                    }

                    if (50 == _addrLabels[i].Location.X)
                    {
                        _addrLabels[i].Location = new Point(_addrLabels[i].Location.X - 35, _addrLabels[i].Location.Y);
                    }
                }
            }
        }

        private void LoadWithCurrentSettings()
        {
            if (_bitfRadio.Checked)
            {
                if ((ushort)_addrHex.Number < (ushort)(_addrHex.Number + _wordCnt * 0x10))
                {
                    _query.Clear();
                    ushort[] values = new ushort[_wordCnt];
                    _query.Values = values;
                    _query.Slots = _device.SetSlots(values, _device.Adress);
                    _query.LoadBitsCicle();
                }
            }
            else
            {
                _query.Clear();
                ushort[] values = new ushort[_wordCnt];
                _query.Values = values;
                _query.Slots = _device.SetSlots(values, _device.Adress);
                _query.LoadStructCycle();
            }
        }

        private void CreateAddrLabel(int i)
        {
            _addrLabels[i] = new Label();
            _addrLabels[i].AutoSize = true;
            _addrLabels[i].Font = new Font("Microsoft Sans Serif", 8F);
            _addrLabels[i].Location = new Point(50, 11 + i * 18);
            _addrLabels[i].Size = new Size(34, 13);
            _addrLabels[i].TextAlign = ContentAlignment.MiddleRight;
        }

        private void CreateDecLabel(int i)
        {
            _decLabels[i] = new HexTextbox();
            _decLabels[i].AutoSize = true;
            _decLabels[i].HexBase = false;
            _decLabels[i].Font = new Font("Microsoft Sans Serif", 8F);
            _decLabels[i].Location = new Point(3, 11 + i * 18);
            _decLabels[i].Size = new Size(49, 13);
            _decLabels[i].TextAlign = HorizontalAlignment.Center;
            _decLabels[i].Text = i.ToString("D4");
            _decLabels[i].Limit = ushort.MaxValue + 1;
            _decLabels[i].Tag = true;
            _decLabels[i].GotFocus += new EventHandler(MLK_Query_GotFocus);
            _decLabels[i].LostFocus += new EventHandler(MLK_Query_LostFocus);
        }

        private void CreateHexLabel(int i)
        {
            _hexLabels[i] = new HexTextbox();
            _hexLabels[i].HexBase = true;
            _hexLabels[i].AutoSize = true;
            _hexLabels[i].Font = new Font("Microsoft Sans Serif", 8F);
            _hexLabels[i].Location = new Point(3, 11 + i * 18);
            _hexLabels[i].Size = new Size(49, 13);
            _hexLabels[i].TextAlign = HorizontalAlignment.Center;
            _hexLabels[i].Text = i.ToString("X4");
            _hexLabels[i].Limit = ushort.MaxValue + 1;
            _hexLabels[i].Tag = true;
            _hexLabels[i].GotFocus += new EventHandler(MLK_Query_GotFocus);
            _hexLabels[i].LostFocus += new EventHandler(MLK_Query_LostFocus);
        }

        private void CreateAdductionBox(int i)
        {
            _adductionBoxes[i] = new TextBox();
            _adductionBoxes[i].Size = new Size(49, 13);
            _adductionBoxes[i].Location = new Point(3, 11 + i * 18);
            _adductionBoxes[i].Tag = true;
            _adductionBoxes[i].GotFocus += new EventHandler(MLK_Query_GotFocus);
            _adductionBoxes[i].LostFocus += new EventHandler(MLK_Query_LostFocus);
        }

        private void CreateSetButton(object sender)
        {
            GroupBox parent = (sender as Control).Parent as GroupBox;
            Point hexLocation = (sender as Control).Location;
            Size hexSize = (sender as Control).Size;
            Point buttonPoint =
                new Point(parent.Location.X + hexLocation.X + hexSize.Width, parent.Location.Y + hexLocation.Y);
            _currentButtton = new Button();
            _currentButtton.Location = buttonPoint;
            _currentButtton.Size = new Size(30, hexSize.Height - 2);
            _currentButtton.Text = "Уст.";
            _currentButtton.TextAlign = ContentAlignment.MiddleLeft;
            _currentButtton.Show();

            _currentButtton.Click += new EventHandler(_currentButtton_Click);
            Controls.Add(_currentButtton);
            Controls.SetChildIndex(_currentButtton, 0);
        }

        private ushort GetBitIndex(Point ledLocation)
        {
            int row;
            int column;

            if (0 == ledLocation.X % 12)
            {
                row = ledLocation.X / 12;
            }
            else
            {
                row = (ledLocation.X - 6) / 12;
            }

            column = (ledLocation.Y - 14) / 18;

            row = 16 - row;
            return (ushort)(column * 16 + row + _addrHex.Number);
        }
        
        private void OnHexSet()
        {
            SomeStruct ss = new SomeStruct(_wordCnt);
            for (int i = 0; i < _wordCnt; i++)
            {
                _hexLabels[i].Correct();
                ss.Values[i] = (ushort)(_hexLabels[i].Number);
            }
            _query.Value = ss;
            _query.Slots = _device.SetSlots(ss.Values, _device.Adress);
        }

        private void OnDecSet()
        {
            SomeStruct ss = new SomeStruct(_wordCnt);
            for (int i = 0; i < _wordCnt; i++)
            {
                _decLabels[i].Correct();
                ss.Values[i] = (ushort)(_decLabels[i].Number);
            }
            _query.Value = ss;
            _query.Slots = _device.SetSlots(ss.Values, _device.Adress);
        }

        private void OnAdductionSet()
        {
            try
            {
                for (int i = 0; i < 8; i++)
                {
                    _query.Values[i] = FromAdductionToUshort(i);
                }
            }
            catch (FormatException)
            {
            }
        }

        private ushort FromAdductionToUshort(int i)
        {
            return (ushort)(_adduction[i].Max * Convert.ToDouble(_adductionBoxes[i].Text) / _adduction[i].Limit);
        }

        private void EnableValueBoxes(bool bEnable)
        {
            for (int i = 0; i < _wordCnt; i++)
            {
                _hexLabels[i].Enabled = bEnable;
                _decLabels[i].Enabled = bEnable;
                _adductionBoxes[i].Enabled = bEnable;
            }
        }

        private void ShowAdduction(int wordIndex)
        {
            if (wordIndex <= _wordCnt && wordIndex >= 0)
            {
                _maxHex.Text = _adduction[wordIndex].Max.ToString();
                _limitBox.Text = _adduction[wordIndex].Limit.ToString();
                _symbolsCombo.SelectedIndex = _adduction[wordIndex].Symbols;
            }
        }

        /// <summary>
        /// Выбранный номер слова
        /// </summary>
        public int CurrentWordIndex
        {
            get
            {
                try
                {
                    int ret = Convert.ToInt32(_wordIndexCombo.Text);
                    if (ret < 1 || ret > 16)
                    {
                        _maxHex.Text = "";
                        _limitBox.Text = "";
                        _symbolsCombo.Text = "";
                    }
                    ret -= 1;

                    return ret;
                }
                catch (FormatException e)
                {
                    _maxHex.Text = "";
                    _limitBox.Text = "";
                    _symbolsCombo.Text = "";
                    throw e;
                }
            }
        }

        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MLK10); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(Mlk10Querys); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.Calibrate; }
        }

        public string NodeName
        {
            get { return "Обмены"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        #region Events

        private void MLK_Query_LostFocus(object sender, EventArgs e)
        {
            if (false == _currentButtton.Focused)
            {
                Controls.Remove(_currentButtton);

                (sender as Control).Tag = true;
            }
        }

        private void _addrHex_TextChanged(object sender, EventArgs e)
        {
            _addrHex.Correct();
            _device.Adress = (ushort)_addrHex.Number;
            LoadWithCurrentSettings();
            SetAddrLabelText();
        }

        /// <summary>
        /// Поле ввода получило фокус
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MLK_Query_GotFocus(object sender, EventArgs e)
        {
            //Создаем кнопку 
            CreateSetButton(sender);

            if (sender is HexTextbox)
            {
                //Теперь поле ввода обновлять не требуется
                _currentHex = (sender as HexTextbox);
                _currentHex.Tag = false;
            }
            if (sender is TextBox)
            {
                //Теперь поле ввода обновлять не требуется
                _currentAdduction = sender as TextBox;
                _currentAdduction.Tag = false;
            }
        }

        /// <summary>
        /// Клацнули мышой по лампочке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Led_MouseClick(object sender, MouseEventArgs e)
        {
            //Только если битная адресация
            if (_bitfRadio.Checked)
            {
                LedControl led = (sender as PictureBox).Parent as LedControl;

                ushort bitInd = GetBitIndex(led.Location);

                switch (e.Button)
                {
                    case MouseButtons.Left:
                        //По левой кнопке инвертируем бит
                        if (LedState.Signaled == led.State)
                        {
                            _device.SetBit(_device.DeviceNumber, bitInd, true, "setBit" + bitInd + _device.DeviceNumber, _device);
                        }
                        else if (LedState.NoSignaled == led.State)
                        {
                            _device.SetBit(_device.DeviceNumber, bitInd, false, "setBit" + bitInd + _device.DeviceNumber, _device);
                        }
                        break;
                    //По правой показываем меню
                    case MouseButtons.Right:
                        _contextMenu.Items.Clear();
                        string set0 = "Сбросить     бит № - " + bitInd.ToString("X4") + "(" + bitInd + ")";
                        string set1 = "Установить бит № - " + bitInd.ToString("X4") + "(" + bitInd + ")";
                        _contextMenu.Items.Add(set0);
                        _contextMenu.Items.Add(set1);
                        _contextMenu.Items[0].Tag = _contextMenu.Items[1].Tag = bitInd;
                        _contextMenu.ItemClicked += new ToolStripItemClickedEventHandler(_contextMenu_ItemClicked);
                        _contextMenu.Show(MousePosition.X, MousePosition.Y);
                        break;
                }
            }
        }

        private void _symbolsCombo_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                _adduction[CurrentWordIndex].Symbols = _symbolsCombo.SelectedIndex;
            }

            catch (IndexOutOfRangeException)
            {
                return;
            }
        }

        private void _contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ushort bitInd = (ushort)e.ClickedItem.Tag;
            try
            {
                if (e.ClickedItem == _contextMenu.Items[0])
                {
                    _device.SetBit(_device.DeviceNumber, bitInd, false,
                        string.Format(SET_BIT_PATTERN, bitInd, _device.DeviceNumber), _device);
                }
                if (e.ClickedItem == _contextMenu.Items[1])
                {
                    _device.SetBit(_device.DeviceNumber, bitInd, true,
                        string.Format(SET_BIT_PATTERN, bitInd, _device.DeviceNumber), _device);
                }
            }
            catch(Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void _bitfRadio_CheckedChanged(object sender, EventArgs e)
        {
            _device.IsBit = _bitfRadio.Checked;
            EnableValueBoxes(_wordfRadio.Checked);
            SetAddrLabelText();
            LoadWithCurrentSettings();
        }

        private void _wordIndexCombo_TextUpdate(object sender, EventArgs e)
        {
            try
            {
                ShowAdduction(CurrentWordIndex);
            }
            catch (FormatException)
            {
                _maxHex.Text = "";
                _limitBox.Text = "";
                _symbolsCombo.Text = "";
            }
        }

        private void _wordIndexCombo_SelectedValueChanged(object sender, EventArgs e)
        {
            ShowAdduction(CurrentWordIndex);
        }

        private void _limitBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                _adduction[CurrentWordIndex].Limit = Convert.ToDouble(_limitBox.Text);
            }
            catch (FormatException)
            {
                return;
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }
        }

        private void _maxHex_TextChanged(object sender, EventArgs e)
        {
            _maxHex.Correct();
            try
            {
                _adduction[CurrentWordIndex].Max = (ushort)_maxHex.Number;
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }
        }

        private void _loopButton_Click(object sender, EventArgs e)
        {
            if (_loop)
            {
                this._loopButton.Text = ">";
                this.Width = 550;
                this._optionsGB.Location = new Point(560, 1);
                _loop = false;
            }
            else
            {
                this._loopButton.Text = "<";
                this.Width = 820;
                this._optionsGB.Location = new Point(525, 1);
                _loop = true;
            }
        }

        private void _currentButtton_Click(object sender, EventArgs e)
        {
            Controls.Remove(sender as Button);
            if (null != _currentHex)
            {
                _currentHex.Correct();
                _currentHex.Tag = true;
                if (_currentHex.HexBase)
                {
                    OnHexSet();
                }
                else
                {
                    OnDecSet();
                }
            }
            else
            {
                OnAdductionSet();
                _currentAdduction.Tag = true;
            }

            _currentHex = null;
            _currentAdduction = null;

            _query.SaveStruct();
        }

        private void MLK_Querys_FormClosing(object sender, FormClosingEventArgs e)
        {
            _query.RemoveStructQuerys();
        }
        private void _countOfWord_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (_countOfWord.SelectedIndex == -1)
                {
                    this.Height = 220;
                }
                else
                {
                    int i = _countOfWord.SelectedIndex;
                    this.Height = 220 + i * 18;
                }
                _wordCnt = _countOfWord.SelectedIndex + 8;
                InitControls();
                SetAddrLabelText();
                if (_queryEnableCheckBox.Checked)
                {
                    LoadWithCurrentSettings();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void _queryEnableCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (_queryEnableCheckBox.Checked)
            {
                LoadWithCurrentSettings();
            }
            else
            {
                _query.Clear();
            }
        }

        private void OnDeviceCaptionChanged()
        {
            _query.Caption = _device.Caption;
        }
        private void DeviceOnDeviceNumberChanged(object sender, byte oldNumber, byte newNumber)
        {
            _query.DeviceNum = newNumber;
        }
        #endregion
    }
}
