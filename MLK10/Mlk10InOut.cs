﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MLK10.HelpClasses;
using BEMN.MLK10.Properties;
using BEMN.MLK10.Structures;

namespace BEMN.MLK10
{
    public partial class Mlk10InOut : Form, IFormView
    {
        #region Поля
        private MLK10 _device;
        private LedControl[] _discretLeds;
        private LedControl[] _relayLeds;
        private MemoryEntity<UIORam> _uioRam;
        private MemoryEntity<Clock> _clock;
        private MemoryEntity<SysErr> _sysErr;

        Timer _timer = new Timer();
        private bool _start;
        #endregion

        #region Константы
        private const double _limit = 300;

        private const string CANAL_ERR = "Неисправен канал";
        private const string LOWER_LIMIT = "Нижний предел";
        private const string UPPER_LIMIT = "Верхний предел";
        private const string ERROR_WRITING_DATE = "Невозможно записать дату/врумя";
        private const string ERROR_WRITING = "Ошибка записи";
        private const string CONNECTION_OK = "Ошибок соединения нет";
        #endregion

        #region Конструкторы
        public Mlk10InOut()
        {
            InitializeComponent();
        }

        public Mlk10InOut(MLK10 device)
        {
            InitializeComponent();
            _device = device;
            
            _clock = _device.Clock;
            _clock.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ClockAstrReadComplete);
            _clock.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, ClockAstrReadFail);

            _uioRam = _device.UioRam;
            _uioRam.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, UioReadComplete);
            _uioRam.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, UioReadFail);
            _uioRam.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show("Невозможно записать структуру ввода-вывода", "Внимание!");
            });

            _sysErr = _device.SysErr;
            _sysErr.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, SysErrorReadCompleat);
            _sysErr.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, SysErrorReadFail);

            _timer.Interval = 100;
            _timer.Tick += new EventHandler(_timer_Tick);
            _start = true;
        }
        
        #endregion

        #region Аналоги, дискреты, реле и светодиоды
        private void UioReadComplete()
        {
            #region Analog

            if (_uioRam.Value.Analog == (ushort) 0xffff)
            {
                _analogU.Text = CANAL_ERR;
            }
            else if (_uioRam.Value.Analog == (ushort) 0x8000)
            {
                _analogU.Text = LOWER_LIMIT;
            }
            else if (_uioRam.Value.Analog == (ushort) 0x7fff)
            {
                _analogU.Text = UPPER_LIMIT;
            }
            else
            {
                _analogU.Text = Math.Round(_limit / 0x7ffe * (double)_uioRam.Value.Analog, 5).ToString();
            }

            #endregion

            #region Discrets
            LedManager.SetLeds(this._discretLeds, this._uioRam.Value.Discrets);
            #endregion

            #region Relay
            LedManager.SetLeds(this._relayLeds, this._uioRam.Value.Relay);
            #endregion

            #region Светодиоды
            //_sv0Led.BackColor = _uioRam.Value.Leds.Leds_L1_1;
            //_sv1Led.BackColor = _uioRam.Value.Leds.Leds_L1_2;
            //_sv2Led.BackColor = _uioRam.Value.Leds.Leds_L2;
            //_sv3Led.BackColor = _uioRam.Value.Leds.Leds_L3;
            #endregion
        }

        private void UioReadFail()
        {
            _analogU.Text = string.Empty;

            LedManager.TurnOffLeds(_discretLeds);
            LedManager.TurnOffLeds(_relayLeds);
        }
        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MLK10); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(Mlk10InOut); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Ввод - вывод"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        #region Астраномические дата/время

        public void ClockAstrReadComplete()
        {
            _astrDateClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            _astrDateClockTB.Text = PrepareGridParam(_clock.Value.Day.ToString(), 2, 0) +
                                    PrepareGridParam(_clock.Value.Month.ToString(), 2, 0) +
                                    PrepareGridParam(_clock.Value.Year.ToString(), 2, 0);
            _astrTimeClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            _astrTimeClockTB.Text = PrepareGridParam(_clock.Value.Hour.ToString(), 2, 0) +
                                    PrepareGridParam(_clock.Value.Minutes.ToString(), 2, 0) +
                                    PrepareGridParam(_clock.Value.Seconds.ToString(), 2, 0);
        }

        public void ClockAstrReadFail()
        {
            _astrDateClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            _astrDateClockTB.Text = "00" + "00" + "00";
            _astrTimeClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            _astrTimeClockTB.Text = "00" + "00" + "00";
        }

        #endregion

        #region Системные ошибки
        private void SysErrorReadCompleat() 
        {
            if (_errorsDG.Rows.Count == 0)
            {
                _errorsDG.Rows.Clear();
                for (int i = 0; i < _sysErr.Value.StatisticsRequest.Length; i++)
                {
                    if (_sysErr.Value.StatisticsRequest[i].SendMessage == 0) continue;
                    byte buf = _sysErr.Value.StatisticsRequest[i].Status;
                    string stat = string.Empty;
                    if (buf == 0)
                    {
                        stat = CONNECTION_OK;
                    }
                    else
                    {
                        BitArray bitStat = new BitArray(new byte[] { buf });
                        for (int j = 0; j < bitStat.Length; j++)
                        {
                            if (bitStat[j])
                                stat += Strings.SystemStatusErrors[j] + "\n";
                        }
                    }

                    _errorsDG.Rows.Add((_errorsDG.Rows.Count + 1).ToString(),
                        _sysErr.Value.StatisticsRequest[i].AcceptMessage.ToString(),
                        _sysErr.Value.StatisticsRequest[i].SendMessage.ToString(),
                        _sysErr.Value.StatisticsRequest[i].CommunicationQuality.ToString(),
                        stat);

                    SetCellColor(_sysErr.Value.StatisticsRequest[i].CommunicationQuality, i);

                    _errorsDG.Rows[i].Height = 20;
                }
            }
            else
            {
                for (int i = 0; i < _sysErr.Value.StatisticsRequest.Length; i++)
                {
                    if (_sysErr.Value.StatisticsRequest[i].SendMessage == 0) continue;
                    byte buf = _sysErr.Value.StatisticsRequest[i].Status;
                    string stat = string.Empty;
                    if (buf == 0)
                    {
                        stat = CONNECTION_OK;
                    }
                    else
                    {
                        BitArray bitStat = new BitArray(new byte[] { buf });
                        for (int j = 0; j < bitStat.Length; j++)
                        {
                            if (bitStat[j])
                                stat += Strings.SystemStatusErrors[j] + "/n";
                        }
                    }
                    _errorsDG.Rows[i].Cells[1].Value = _sysErr.Value.StatisticsRequest[i].AcceptMessage.ToString();
                    _errorsDG.Rows[i].Cells[2].Value = _sysErr.Value.StatisticsRequest[i].SendMessage.ToString();
                    _errorsDG.Rows[i].Cells[3].Value =
                        _sysErr.Value.StatisticsRequest[i].CommunicationQuality.ToString();
                    _errorsDG.Rows[i].Cells[4].Value = stat;

                    SetCellColor(_sysErr.Value.StatisticsRequest[i].CommunicationQuality, i);
                }
            }
        }

        private void SysErrorReadFail()
        {
            _errorsDG.Rows.Clear();
        }
        #endregion

        #region Дополнительные функции

        private void RemoveAllQuerys()
        {
            _uioRam.RemoveStructQueries();
            _clock.RemoveStructQueries();
            _sysErr.RemoveStructQueries();
        }

        private string PrepareGridParam(string param, int count, int index)
        {
            if (param.Length < count)
            {
                param = "0" + param;
            }
            if (param.Length > count)
            {
                string temp = string.Empty;
                for (int i = index; i < param.Length; i++)
                {
                    temp += param[i];
                }
                param = temp;
            }
            return param;
        }

        private void SetCellColor(ushort communicationQuality, int i)
        {
            if (communicationQuality < 25)
            {
                _errorsDG.Rows[i].Cells[3].Style.BackColor = Color.Red;
            }
            else if (communicationQuality >= 25 && communicationQuality < 50)
            {
                _errorsDG.Rows[i].Cells[3].Style.BackColor = Color.LightCoral;
            }
            else if (communicationQuality >= 50 && communicationQuality < 75)
            {
                _errorsDG.Rows[i].Cells[3].Style.BackColor = Color.LightGreen;
            }
            else if (communicationQuality >= 75 && communicationQuality <= 100)
            {
                _errorsDG.Rows[i].Cells[3].Style.BackColor = Color.Green;
            }
        }


        private void _timer_Tick(object sender, EventArgs e)
        {
            _astrDateClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            _astrDateClockTB.Text = PrepareGridParam(DateTime.Now.Day.ToString(), 2, 0) +
                                PrepareGridParam(DateTime.Now.Month.ToString(), 2, 0) +
                                PrepareGridParam(DateTime.Now.Year.ToString(), 2, 2);
            _astrTimeClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            _astrTimeClockTB.Text = PrepareGridParam(DateTime.Now.Hour.ToString(), 2, 0) +
                                PrepareGridParam(DateTime.Now.Minute.ToString(), 2, 0) +
                                PrepareGridParam(DateTime.Now.Second.ToString(), 2, 0);
        }

        private void SetRelay(LedControl led, BitArray tempReleLed, int ledInd)
        {
            bool ledState = (led.State == LedState.NoSignaled) ? false : true;
            tempReleLed[ledInd] = ledState;
            UIORam set = _uioRam.Value;
            set.Relay = tempReleLed;
            _uioRam.Value = set;
            _uioRam.SaveStruct();
        }
        #endregion

        #region Обработчики событий

        private void MLK_InOut_Load(object sender, EventArgs e)
        {
            #region Инициализация LedControls

            _discretLeds = new LedControl[]
            {
                _diskr1,
                _diskr2,
                _diskr3,
                _diskr4,
                _diskr5,
                _diskr6,
                _diskr7,
                _diskr8,
                _diskr9,
                _diskr10,
                _diskr11,
                _diskr12,
                _diskr13,
                _diskr14,
                _diskr15,
                _diskr16
            };
            _relayLeds = new LedControl[]
            {
                _rele1,
                _rele2,
                _rele3,
                _rele4,
                _rele5,
                _rele6,
                _rele7,
                _rele8,
                _rele9,
                _rele10,
                _rele11,
                _rele12,
                _rele13,
                _rele14,
                _rele15,
                _rele16
            };
            #endregion

            this._uioRam.LoadStructCycle();
            this._clock.LoadStructCycle();
            this._sysErr.LoadStructCycle();
        }

        private void _astrStopCB_CheckedChanged(object sender, EventArgs e)
        {
            if (_astrStopCB.Checked)
            {
                _clock.RemoveStructQueries();
                _dateTimeNowButt.Enabled = true;
                _writeDateTimeButt.Enabled = true;
            }
            else
            {
                _clock.LoadStructCycle();
                _dateTimeNowButt.Enabled = false;
                _writeDateTimeButt.Enabled = false;
                if (!_start)
                {
                    _start = true;
                    _timer.Stop();
                }
            }
        }

        private void MLK_InOut_FormClosing(object sender, FormClosingEventArgs e)
        {
            RemoveAllQuerys();
        }
        
        private void _rele_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                LedControl led = sender as LedControl;
                List<LedControl> tempArray = new List<LedControl>();
                tempArray.AddRange(_relayLeds);
                int ledInd = tempArray.IndexOf(led);
                BitArray tempReleLed = _uioRam.Value.Relay;

                switch (e.Button)
                {
                    //По левой кнопке инвертируем бит
                    case MouseButtons.Left:
                        SetRelay(led, tempReleLed, ledInd);
                        break;
                    //По правой показываем меню
                    case MouseButtons.Right:
                        _contextMenu.Items.Clear();
                        string contextItem;
                        if (tempReleLed[ledInd])
                        {
                            contextItem = string.Format("Выключить реле № - {0}", ledInd + 1);
                        }
                        else
                        {
                            contextItem = string.Format("Включить реле № - {0}", ledInd + 1);
                        }
                        _contextMenu.Items.Add(contextItem);
                        ArrayList array = new ArrayList();
                        array.Add(led);
                        array.Add(tempReleLed);
                        array.Add(ledInd);
                        _contextMenu.Items[0].Tag = array;
                        _contextMenu.ItemClicked += new ToolStripItemClickedEventHandler(_contextMenu_ItemClicked);
                        _contextMenu.Show(MousePosition.X, MousePosition.Y);
                        break;
                }
            }
            catch
            {
                MessageBox.Show("Невозможно изменить состояние реле", "Ошибка");
            }
        }

        private void _contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ArrayList array = (ArrayList)e.ClickedItem.Tag;
            SetRelay((LedControl)array[0], (BitArray)array[1], (int)array[2]);
        }
        
        private void _dateTimeNowButt_Click(object sender, EventArgs e)
        {
            if (_start)
            {
                _start = false;
                _timer.Start();
            }
            else
            {
                _start = true;
                _timer.Stop();
            }
        }

        private void _writeDateTimeButt_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> dateTime = new List<string>();
                dateTime.AddRange(_astrDateClockTB.Text.Split('.'));
                dateTime.AddRange(_astrTimeClockTB.Text.Split(':', '.', ','));
                Clock clock = new Clock();
                clock.InitStruct(new byte[_clock.Values.Length * 2]);
                clock.Day = Convert.ToUInt16(dateTime[0]);
                clock.Month = Convert.ToUInt16(dateTime[1]);
                clock.Year = Convert.ToUInt16(dateTime[2]);
                clock.Hour = Convert.ToUInt16(dateTime[3]);
                clock.Minutes = Convert.ToUInt16(dateTime[4]);
                clock.Seconds = Convert.ToUInt16(dateTime[5]);
                this._clock.Value = clock;
                this._clock.SaveStruct();
                this._astrStopCB.Checked = true;
            }
            catch
            {
                MessageBox.Show(ERROR_WRITING_DATE, ERROR_WRITING, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        #endregion
    }
}