﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MLK10.HelpClasses;
using BEMN.MLK10.Properties;
using BEMN.MLK10.Structures;

namespace BEMN.MLK10
{
    public partial class Mlk10Configuration : Form, IFormView
    {
        #region Поля
        private MLK10 _device;
        private ComboBox[]  _RS232Combos,
                            _RS485Combos;

        private MaskedTextBox[] _ulongrs232MaskedTextBoxes,
                                _ulongrs485MaskedTextBoxes,
                                _ulongPereferyMaskedTextBoxes,
                                _doublePercentMaskedTextBox,
                                _ulongRequestCountMaskedTextBox;

        private string _oldValue;
        private string _oldCellValue;

        private bool _validatingOk;
        private bool _readAll;
        private bool _readAnalog;
        private byte _step;
        private double _temp;
        private int _counter;

        private MemoryEntity<MemConfigRS485> _configRS485; 
        private MemoryEntity<MemConfigRS232> _configRS232;
        private MemoryEntity<MemConfigRequest> _configRequest;
        private MemoryEntity<AnalogExtConfig> _periphery;
        private MemoryEntity<UIORam> _analog; 
        #endregion

        #region Константы

        private const string BAD_DATA = "проверьте правильность введенных данных";
        private const string ERROR_WRITE = "Ошибка записи";
        private const string ERROR_READ_RS485 = "Не удалось загрузить параметры RS-485";
        private const string READ_RS485_OK = "Конфигурация RS-485 загружена успешно";
        private const string ERROR_READ_RS232 = "Не удалось загрузить параметры RS-232";
        private const string READ_RS232_OK = "Конфигурация RS-232 загружена успешно";
        private const string ERROR_READ_REQUESTS = "Не удалось загрузить запросы к модулям";
        private const string READ_REQUESTS_OK = "Кконфигурация запросов заружена успешно";
        private const string ERROR_READ_PERIPHERY = "Не удалось загрузить коэфициенты калибровки";
        private const string READ_PERIPHERY_OK = "Коэффициенты загружены успешно";
        private const string ERROR_WRITE_REQUESTS = "Проверьте введенные данные для запросов";
        private const string WRITE_RS232_OK = "Конфигурация RS-232 записана успешно";
        private const string WRITE_RS232_FAIL = "Не удалось записать конфигурацию RS-232";
        private const string WRITE_RS485_OK = "Конфигурация RS-485 записана успешно";
        private const string WRITE_RS485_FAIL = "Не удалось записать конфигурацию RS-485";
        private const string WRITE_REQUEST_OK = "Конфигурация запросов записана успешно";
        private const string WRITE_REQUEST_FAIL = "Не удалось записать конфигурацию запросов";
        private const string WRITE_PERIPHERY_OK = "Конфигурация периферии записана успешно";
        private const string WRITE_PERIPHERY_FAIL = "Не удалось записать конфигурацию периферии";

        private const string READ_ANALOG_FAIL = "Не удалось прочитать аналоговый канал";
        private const string CANAL_ERR = "Неиспр. канал";
        private const string LOWER_LIMIT = "Нижний предел";
        private const string UPPER_LIMIT = "Верхний предел";
        private const string STEP = "Шаг {0}";
        private const string DefaultLimit = "100";
        private const string DefaultPercent = "0,0";
        private const string Info1 =
            "   Для проведения процедуры калибровки выберите номер аналогового измерительного органа (АОИ) и нажмите кнопку \"Продолжить\".";

        private const string Info2 =
            "   Для получения \"коэффициента В\" убедитесь, что ко входу калибруемого АИО нет физических подключений и нажмите кнопку \"Продолжить\".";

        private const string Info3 = "   Для получения \"коэффициента А\":\n" +
                                     "   а) установите величину верхнего предела шкалы измерения (поле \"Предел шкалы\").\n" +
                                     "   б) установите величину эталонного сигнала, который будет подан на вход (поле \"Сигнал на входе\").\n" +
                                     "   в) создайте физическое подключение калибруемого АИО с источником эталонного сигнала и перейдите к следующему шагу.";
        private const string Info4 = "При необходимости коэффициенты можно подправить и сохранить в устройство";

        private const string B_SAVED = "Коэффициент В сохранен";
        private const string B_SABING = "Расчет и запись коэффициента В";
        private const string A_MEASSURING = "Идет расчет коеффициента А";
        private const string BAD_LIMIT_PERCENT = "Неверно введен лимит или процент";
        private const string NO_POWER_SUPPLY = "Нет подключения с источником сигнала!";
        private const string COMPLETE = "Калибровка завершена успешно";

        private const string CONTINUE = "Продолжить";
        private const string REPEAT = "Завершить";

        #endregion

        #region Конструкторы
        public Mlk10Configuration()
        {
            InitializeComponent();
        }

        public Mlk10Configuration(MLK10 device)
        {
            InitializeComponent();
            _device = device;

            #region RS-232
            _configRS232 = _device.ConfigRs232;
            _configRS232.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadRS232Ok);
            _configRS232.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ERROR_READ_RS232;
                _statusLabel.BackColor = Color.Red;
            });
            _configRS232.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = WRITE_RS232_OK;
                _statusLabel.BackColor = Color.Green;
            });
            _configRS232.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = WRITE_RS232_FAIL;
                _statusLabel.BackColor = Color.Red;
            });
            #endregion
            #region RS-485
            _configRS485 = _device.ConfigRs485;
            _configRS485.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadRS485Ok);
            _configRS485.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ERROR_READ_RS485;
                _statusLabel.BackColor = Color.Red;
            });
            _configRS485.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = WRITE_RS485_OK;
                _statusLabel.BackColor = Color.Green;
            });
            _configRS485.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = WRITE_RS485_FAIL;
                _statusLabel.BackColor = Color.Red;
            });
            #endregion
            #region Request
            _configRequest = _device.ConfigRequest;
            _configRequest.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadRequestOk);
            _configRequest.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ERROR_READ_REQUESTS;
                _statusLabel.BackColor = Color.Red;
            });
            _configRequest.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = WRITE_REQUEST_OK;
                _statusLabel.BackColor = Color.Green;
            });
            _configRequest.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = WRITE_REQUEST_FAIL;
                _statusLabel.BackColor = Color.Red;
            });
            #endregion
            #region Periphery
            _periphery = _device.Periphery;
            _periphery.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadPeriferyOk);
            _periphery.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ERROR_READ_PERIPHERY;
                _statusLabel.BackColor = Color.Red;
            });
            _periphery.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = WRITE_PERIPHERY_OK;
                _statusLabel.BackColor = Color.Green;
                if (_step == 2) { _statusStep.Text = B_SAVED; }
            });
            _periphery.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = WRITE_PERIPHERY_FAIL;
                _statusLabel.BackColor = Color.Red;
            });

            _analog = _device.UioRam;
            _analog.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, AnalogReadOk);
            _analog.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = READ_ANALOG_FAIL;
                _statusLabel.BackColor = Color.Red;
            });

            _readAnalog = false;
            #endregion
        }
        
        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MLK10); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(Mlk10Configuration); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        #region RS232
        public void PrepareRS232()
        {
            _RS232Combos = new ComboBox[]
            {
                _rs232speedsCombo,
                _rs232dataBitsCombo,
                _rs232stopBitsCombo,
                _rs232paritetCHETCombo,
                _rs232paritetYNCombo,
                _rs232doubleSpeedCombo
            };

            _ulongrs232MaskedTextBoxes = new MaskedTextBox[]
            {_rs232AddressTB, _rs232ToSendBeforeTB, _rs232ToSendAfterTB};

            ClearCombos(_RS232Combos);

            FillRS232Combo();

            SubscriptCombos(_RS232Combos);

            PrepareMaskedBoxes(_ulongrs232MaskedTextBoxes, typeof(ulong));

        }

        public void FillRS232Combo()
        {
            if (_rs232speedsCombo.Items.Count == 0)
            {
                _rs232speedsCombo.Items.AddRange(Strings.RS_SPEEDS.ToArray());
            }
            if (_rs232dataBitsCombo.Items.Count == 0)
            {
                _rs232dataBitsCombo.Items.AddRange(Strings.RS_DATA_BITS.ToArray());
            }
            if (_rs232stopBitsCombo.Items.Count == 0)
            {
                _rs232stopBitsCombo.Items.AddRange(Strings.RS_STOPBITS.ToArray());
            }
            if (_rs232paritetYNCombo.Items.Count == 0)
            {
                _rs232paritetYNCombo.Items.AddRange(Strings.RS_PARITET_YN.ToArray());
            }
            if (_rs232paritetCHETCombo.Items.Count == 0)
            {
                _rs232paritetCHETCombo.Items.AddRange(Strings.RS_PARITET_CHET.ToArray());
            }
            if (_rs232doubleSpeedCombo.Items.Count == 0)
            {
                _rs232doubleSpeedCombo.Items.AddRange(Strings.RS_DOUBLESPEED.ToArray());
            }
        }

        public void ReadRS232Ok()
        {
            _rs232speedsCombo.SelectedItem = _configRS232.Value.RS232Speed;
            _rs232dataBitsCombo.SelectedItem = _configRS232.Value.RS232DataBits;
            _rs232stopBitsCombo.SelectedItem = _configRS232.Value.RS232StopBits;
            _rs232paritetYNCombo.SelectedItem = _configRS232.Value.RS232ParitetOnOff;
            _rs232paritetCHETCombo.SelectedItem = _configRS232.Value.RS232ParitetChet;
            _rs232doubleSpeedCombo.SelectedItem = _configRS232.Value.Rs232DoubleSpeed;
            _rs232AddressTB.Text = _configRS232.Value.RS232Address.ToString();
            _rs232ToSendBeforeTB.Text = _configRS232.Value.RS232ToSendBefore.ToString();
            _rs232ToSendAfterTB.Text = _configRS232.Value.RS232ToSendAfter.ToString();
            if (_rs232paritetYNCombo.SelectedIndex > 0)
            {
                _rs232paritetCHETCombo.Enabled = true;
            }
            _statusLabel.Text = READ_RS232_OK;
            _statusLabel.BackColor = Color.Green;
        }

        public bool WriteRS232() 
        {
            bool res = true;
            try
            {
                MemConfigRS232 config = new MemConfigRS232();
                config.InitStruct(new byte[_configRS232.Values.Length*2]);
                config.RS232Speed = _rs232speedsCombo.SelectedItem.ToString();
                config.RS232DataBits = _rs232dataBitsCombo.SelectedItem.ToString();
                config.RS232StopBits = _rs232stopBitsCombo.SelectedItem.ToString();
                config.RS232ParitetOnOff = _rs232paritetYNCombo.SelectedItem.ToString();
                config.RS232ParitetChet = _rs232paritetCHETCombo.SelectedItem.ToString();
                config.Rs232DoubleSpeed = _rs232doubleSpeedCombo.SelectedItem.ToString();
                config.RS232Address = Convert.ToByte(_rs232AddressTB.Text);
                config.RS232ToSendBefore = Convert.ToByte(_rs232ToSendBeforeTB.Text);
                config.RS232ToSendAfter = Convert.ToByte(_rs232ToSendAfterTB.Text);
                _configRS232.Value = config;
            }
            catch 
            {
                res = false;
            }
            return res;
        }
        #endregion

        #region RS485
        public void PrepareRS485()
        {
            _RS485Combos = new ComboBox[]
            {
                _rs485dataBitsCombo,
                _rs485speedsCombo,
                _rs485stopBitsCombo,
                _rs485paritetCHETCombo,
                _rs485paritetYNCombo,
                _rs485doubleSpeedCombo
            };

            _ulongrs485MaskedTextBoxes = new MaskedTextBox[]
            {_rs485AddressTB, _rs485ToSendTB, _rs485ToSendBeforeTB, _rs485ToSendAfterTB, _rs485AnswerTB};

            ClearCombos(_RS485Combos);

            FillRS485Combo();

            SubscriptCombos(_RS485Combos);

            PrepareMaskedBoxes(_ulongrs485MaskedTextBoxes, typeof(ulong));

        }

        public void FillRS485Combo()
        {
            if (_rs485dataBitsCombo.Items.Count == 0)
            {
                _rs485dataBitsCombo.Items.AddRange(Strings.RS_DATA_BITS.ToArray());
            }
            if (_rs485speedsCombo.Items.Count == 0)
            {
                _rs485speedsCombo.Items.AddRange(Strings.RS_SPEEDS.ToArray());
            }
            if (_rs485stopBitsCombo.Items.Count == 0)
            {
                _rs485stopBitsCombo.Items.AddRange(Strings.RS_STOPBITS.ToArray());
            }
            if (_rs485paritetYNCombo.Items.Count == 0)
            {
                _rs485paritetYNCombo.Items.AddRange(Strings.RS_PARITET_YN.ToArray());
            }
            if (_rs485paritetCHETCombo.Items.Count == 0)
            {
                _rs485paritetCHETCombo.Items.AddRange(Strings.RS_PARITET_CHET.ToArray());
            }
            if (_rs485doubleSpeedCombo.Items.Count == 0)
            {
                _rs485doubleSpeedCombo.Items.AddRange(Strings.RS_DOUBLESPEED.ToArray());
            }
        }

        public void ReadRS485Ok()
        {
            try
            {
                _rs485modeCheck.Checked = _configRS485.Value.RS485Mode;
                _rs485speedsCombo.SelectedItem = _configRS485.Value.RS485Speed;
                _rs485dataBitsCombo.SelectedItem = _configRS485.Value.RS485DataBits;
                _rs485stopBitsCombo.SelectedItem = _configRS485.Value.RS485StopBits;
                _rs485paritetYNCombo.SelectedItem = _configRS485.Value.RS485ParitetOnOff;
                _rs485paritetCHETCombo.SelectedItem = _configRS485.Value.RS485ParitetChet;
                _rs485doubleSpeedCombo.SelectedItem = _configRS485.Value.RS485DoubleSpeed;
                _rs485AddressTB.Text = _configRS485.Value.RS485Address.ToString();
                _rs485ToSendTB.Text = _configRS485.Value.RS485ToSend.ToString();
                _rs485ToSendBeforeTB.Text = _configRS485.Value.RS485ToSendBefore.ToString();
                _rs485ToSendAfterTB.Text = _configRS485.Value.RS485ToSendAfter.ToString();
                _rs485AnswerTB.Text = _configRS485.Value.RS485Answer.ToString();
                if (_rs485paritetYNCombo.SelectedIndex > 0)
                {
                    _rs485paritetCHETCombo.Enabled = true;
                }
                _statusLabel.Text = READ_RS485_OK;
                _statusLabel.BackColor = Color.Green;
            }
            catch (Exception)
            {}
        }

        public bool WriteRS485()
        {
            bool res = true;
            try
            {
                MemConfigRS485 config = new MemConfigRS485();
                config.InitStruct(new byte[_configRS485.Values.Length*2]);
                config.RS485Mode = _rs485modeCheck.Checked;
                config.RS485Speed = _rs485speedsCombo.SelectedItem.ToString();
                config.RS485DataBits = _rs485dataBitsCombo.SelectedItem.ToString();
                config.RS485StopBits = _rs485stopBitsCombo.SelectedItem.ToString();
                config.RS485ParitetOnOff = _rs485paritetYNCombo.SelectedItem.ToString();
                config.RS485ParitetChet = _rs485paritetCHETCombo.SelectedItem.ToString();
                config.RS485DoubleSpeed = _rs485doubleSpeedCombo.SelectedItem.ToString();
                config.RS485Address = Convert.ToByte(_rs485AddressTB.Text);
                config.RS485ToSend = Convert.ToByte(_rs485ToSendTB.Text);
                config.RS485ToSendBefore = Convert.ToByte(_rs485ToSendBeforeTB.Text);
                config.RS485ToSendAfter = Convert.ToByte(_rs485ToSendAfterTB.Text);
                config.RS485Answer = Convert.ToUInt16(_rs485AnswerTB.Text);
                _configRS485.Value = config;
            }
            catch
            {
                res = false;
            }
            return res;
        }
        #endregion

        #region Запросы

        #region Количество запросов

        public void PrepareRequestCount()
        {
            _ulongRequestCountMaskedTextBox = new MaskedTextBox[] { _querysCount };

            PrepareMaskedBoxes(_ulongRequestCountMaskedTextBox, typeof(ulong));
        }

        public void ReadRequestCount()
        {
            _querysCount.Text = _configRequest.Value.Config.NumberRequest.ToString();
        }

        #endregion

        public void PrepareRequest()
        {
            _commandCol.Items.AddRange(Strings.Command.ToArray());
            if (_querys.Rows.Count == 0)
            {
                for (int i = 0; i < (int)_configRequest.Value.Config.NumberRequest; i++)
                {
                    _querys.Rows.Add(new object[]{(i + 1).ToString(),
                                                              0,
                                                              0,
                                                              0,
                                                              0,
                                                              0,
                                                              0
                    });
                }
            }
        }

        public void ReadRequestOk()
        {
            if (_readAll)
            {
                ReadRequestCount();
                _readAll = false;
            }
            _querys.Rows.Clear();
            if (_configRequest.Value.Config.NumberRequest > Convert.ToInt32(_querysCount.Text))
            {
                for (int i = 0; i < Convert.ToInt32(_querysCount.Text); i++)
                {
                    _querys.Rows.Add((i + 1).ToString(),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysPhase.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysDevNum.ToString("X")),
                        Strings.Command[_configRequest.Value.Request[i].QuerysCommand],
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysSlaveHi.ToString("X")) +
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysSlaveLow.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysMasterHi.ToString("X")) +
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysMasterLow.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysParamNum.ToString("X")));
                }
            }
            if (_configRequest.Value.Config.NumberRequest < Convert.ToInt32(_querysCount.Text))
            {
                for (int i = 0; i < _configRequest.Value.Config.NumberRequest; i++)
                {
                    _querys.Rows.Add((i + 1).ToString(),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysPhase.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysDevNum.ToString("X")),
                        Strings.Command[_configRequest.Value.Request[i].QuerysCommand],
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysSlaveHi.ToString("X")) +
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysSlaveLow.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysMasterHi.ToString("X")) +
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysMasterLow.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysParamNum.ToString("X")));
                }
                for (int i = _configRequest.Value.Config.NumberRequest; i < Convert.ToInt32(_querysCount.Text); i++)
                {
                    _querys.Rows.Add((i + 1).ToString(), "00", "00", PrepareGridCombo(0), "0000", "0000", "00");
                }
            }
            if (_configRequest.Value.Config.NumberRequest == Convert.ToInt32(_querysCount.Text))
            {
                for (int i = 0; i < Convert.ToInt32(_querysCount.Text); i++)
                {
                    _querys.Rows.Add((i + 1).ToString(),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysPhase.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysDevNum.ToString("X")),
                        Strings.Command[_configRequest.Value.Request[i].QuerysCommand],
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysSlaveHi.ToString("X")) +
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysSlaveLow.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysMasterHi.ToString("X")) +
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysMasterLow.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysParamNum.ToString("X")));
                }
            }
            _statusLabel.Text = READ_REQUESTS_OK;
            _statusLabel.BackColor = Color.Green;
        }

        public bool WriteConfigRequest()
        {
            bool res = true;
            MemConfigRequest config = new MemConfigRequest();
            config.InitStruct(new byte[_configRequest.Values.Length*2]);
            try
            {
                config.Config.NumberRequest = ushort.Parse(_querysCount.Text);
                for (int i = 0; i < ushort.Parse(_querysCount.Text); i++)
                {
                    config.Request[i].QuerysPhase =
                        byte.Parse(_querys.Rows[i].Cells["_timeCol"].Value.ToString(),
                            System.Globalization.NumberStyles.HexNumber);
                    config.Request[i].QuerysDevNum =
                        byte.Parse(_querys.Rows[i].Cells["_msgCol"].Value.ToString(),
                            System.Globalization.NumberStyles.HexNumber);
                    config.Request[i].QuerysCommand =
                        (byte) Strings.Command.IndexOf(_querys.Rows[i].Cells["_commandCol"].Value.ToString());
                    config.Request[i].QuerysSlaveHi =
                        Common.HIBYTE(ushort.Parse(_querys.Rows[i].Cells["_masterAddrCol"].Value.ToString(),
                            System.Globalization.NumberStyles.HexNumber));
                    config.Request[i].QuerysSlaveLow =
                        Common.LOBYTE(ushort.Parse(_querys.Rows[i].Cells["_masterAddrCol"].Value.ToString(),
                            System.Globalization.NumberStyles.HexNumber));
                    config.Request[i].QuerysMasterHi =
                        Common.HIBYTE(ushort.Parse(_querys.Rows[i].Cells["_slaveAddrCol"].Value.ToString(),
                            System.Globalization.NumberStyles.HexNumber));
                    config.Request[i].QuerysMasterLow =
                        Common.LOBYTE(ushort.Parse(_querys.Rows[i].Cells["_slaveAddrCol"].Value.ToString(),
                            System.Globalization.NumberStyles.HexNumber));
                    config.Request[i].QuerysParamNum =
                        byte.Parse(_querys.Rows[i].Cells["_numCol"].Value.ToString(),
                            System.Globalization.NumberStyles.HexNumber);
                }
                _configRequest.Value = config;
            }
            catch
            {
                res = false;
                MessageBox.Show(ERROR_WRITE_REQUESTS, ERROR_WRITE, MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
            return res;
        }
        #endregion

        #region Периферия
        public void PreparePeriphery()
        {
            _ulongPereferyMaskedTextBoxes = new MaskedTextBox[] { _koeffB, _koeffA, _koeffpA, _limit };
            _doublePercentMaskedTextBox = new MaskedTextBox[] { _Uin };

            PrepareMaskedBoxes(_ulongPereferyMaskedTextBoxes, typeof(ulong));
            PrepareMaskedBoxes(_doublePercentMaskedTextBox, typeof(double));
        }

        public void ReadPeriferyOk()
        {
            _koeffB.Text = _periphery.Value.B.ToString();
            _koeffA.Text = _periphery.Value.A.ToString();
            _koeffpA.Text = _periphery.Value.Pa.ToString();
            _statusLabel.Text = READ_PERIPHERY_OK;
            _statusLabel.BackColor = Color.Green;
        }

        public bool WritePerefery()
        {
            bool res = true;
            try
            {
                AnalogExtConfig config = new AnalogExtConfig();
                config.InitStruct(new byte[_periphery.Values.Length * 2]);
                config.B = Convert.ToUInt16(_koeffB.Text);
                config.A = Convert.ToUInt16(_koeffA.Text);
                config.Pa = Convert.ToUInt16(_koeffpA.Text);
                _periphery.Value = config;
            }
            catch
            {
                res = false;
            }
            return res;
        }

        private void AnalogReadOk()
        {
            _currentADC.Text = _analog.Value.Analog.ToString();
            if (ValidateLimit() && ValidatePercent())
            {
                switch (_analog.Value.Analog)
                {
                    case (ushort)0xffff:
                        _processed.Text = CANAL_ERR;
                        break;
                    case (ushort)0x8000:
                        _processed.Text = LOWER_LIMIT;
                        break;
                    case (ushort)0x7fff:
                        _processed.Text = UPPER_LIMIT;
                        break;
                    default:
                        _processed.Text =
                            Math.Round(double.Parse(_limit.Text) / 0x7ffe * (double)_analog.Value.Analog, 5).ToString();
                        if (_readAnalog)
                        {
                            MeasuringKoeffB();
                        }
                        break;
                }
            }
        }
        //подсчет среднего значения для коэффициента В
        //и переход к соответствующему шагу
        private void MeasuringKoeffB()
        {
            if (_counter != 10)
            {
                _temp += _analog.Value.Analog;
                _counter++;
            }
            else
            {
                _temp = _temp / 10;
                _readAnalog = false;
                switch (_step)
                {
                    case 2:
                        StepTwo();
                        break;
                    case 3:
                        StepThree();
                        break;
                }
            }
        }

        private void StepOne()
        {
            AnalogExtConfig config = new AnalogExtConfig();
            config.InitStruct(new byte[_periphery.Values.Length * 2]);
            config.B = 0x0000;
            config.A = 0x7FFF;
            config.Pa = 0x0000;
            _periphery.Value = config;
            _periphery.SaveStruct();
            _periphery.LoadStruct();

            _step++;
        }

        private void StepTwo()
        {
            AnalogExtConfig conf = _periphery.Value;
            conf.B = (ushort)(_temp * 2);
            _periphery.Value = conf;
            _periphery.SaveStruct();
            _periphery.LoadStruct();

            _continue.Enabled = true;
            _step++;
        }

        private void StepThree()
        {
            _temp = (ulong)(double.Parse(_Uin.Text) * 0x8000 * 0x8000 / double.Parse(_limit.Text) / (double)_temp);
            if (0 == _temp)
            {
                _temp = 0x8000;
            }
            ushort _pA = 0;
            while (_temp > 0xFFFF)
            {
                _temp /= 2;
                _pA++;
            }

            AnalogExtConfig conf = _periphery.Value;
            conf.A = (ushort)_temp;
            conf.Pa = _pA;
            _periphery.Value = conf;
            _periphery.SaveStruct();
            _statusStep.Text = COMPLETE;
            _periphery.LoadStruct();

            _continue.Enabled = true;
            _step++;
        }

        private void _continue_Click(object sender, EventArgs e)
        {
            try
            {
                switch (_step)
                {
                    case 1:
                        {
                            _descriptionGroup.Text = string.Format(STEP, _step);
                            _descriptionBox.Text = Info2;

                            StepOne();

                            _limit.Text = DefaultLimit;
                            _Uin.Text = DefaultPercent;
                        }
                        break;
                    case 2:
                        {
                            _descriptionBox.Text = Info3;
                            _descriptionGroup.Text = string.Format(STEP, _step);
                            _statusStep.Text = B_SABING;

                            _continue.Enabled = false;
                            _readAnalog = true;
                            _temp = 0;
                            _counter = 0;

                            _limit.Enabled = true;
                            _Uin.Enabled = true;
                        }
                        break;
                    case 3:
                        {
                            if (!(ValidateLimit() && ValidatePercent()))
                            {
                                _statusStep.Text = BAD_LIMIT_PERCENT;
                                return;
                            }
                            if (ushort.Parse(_currentADC.Text) == 0)
                            {
                                _statusStep.Text = NO_POWER_SUPPLY;
                                return;
                            }
                            _descriptionGroup.Text = string.Format(STEP, _step);
                            _descriptionBox.Text = Info4;
                            _statusStep.Text = A_MEASSURING;

                            _continue.Enabled = false;
                            _readAnalog = true;
                            _temp = 0;
                            _counter = 0;

                            _continue.Text = REPEAT;
                            _koeffA.Enabled = _koeffB.Enabled = _koeffpA.Enabled = true;
                        }
                        break;
                    case 4:
                        {
                            _chanelCombo.SelectedIndex = -1;
                            _continue.Text = CONTINUE;
                            _continue.Enabled = false;
                            _step = 1;
                            _descriptionBox.Text = Info1;
                            _descriptionGroup.Text = string.Format(STEP, _step);
                            _koeffA.Enabled = _koeffB.Enabled = _koeffpA.Enabled = _limit.Enabled = _Uin.Enabled = false;
                        }
                        break;
                }
            }
            catch (Exception)
            { }
        }
        #endregion

        #region Дополнительные функции
        private void LoadConfigurationBlocks()
        {
            _configRS232.LoadStruct();
            _configRS485.LoadStruct();
            _configRequest.LoadStruct();
            _periphery.LoadStruct();
        }

        private string PrepareGridParam(string param)
        {
            if (param.Length < 2)
            {
                param = "0" + param;
            }
            return param;
        }

        private string PrepareGridCombo(byte command)
        {
            return Strings.Command[command];
        }

        private void ClearCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].Items.Clear();
            }
        }

        private void SubscriptCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].DropDown += new EventHandler(Combo_DropDown);
                combos[i].SelectedIndex = 0;
            }
        }

        private void PrepareMaskedBoxes(MaskedTextBox[] boxes, Type validateType)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].TypeValidationCompleted += new TypeValidationEventHandler(TypeValidation);
                boxes[i].ValidatingType = validateType;
            }
        }
        
        private void TypeValidation(object sender, TypeValidationEventArgs e)
        {
            if (sender is MaskedTextBox)
            {
                MaskedTextBox box = sender as MaskedTextBox;

                if (!e.IsValidInput && box.ValidatingType == typeof(ulong))
                {
                    ShowToolTip(box);
                    return;
                }
                if (!e.IsValidInput && box.ValidatingType == typeof(double))
                {
                    ShowToolTipDouble(box);
                    return;
                }

                if (box.ValidatingType == typeof (ulong) && ulong.Parse(box.Text) > ulong.Parse(box.Tag.ToString()))
                {
                    ShowToolTip(box);
                }
                else if (box.ValidatingType == typeof (double) &&
                         (double.Parse(box.Text) > double.Parse(box.Tag.ToString())) || (double.Parse(box.Text) < 0))
                {
                    ShowToolTipDouble(box);
                }
            }
        }

        private void ShowToolTip(MaskedTextBox box)
        {
            //if (_validatingOk)
            //{
                if (box.Parent.Parent is TabPage)
                {
                    _configurationTabControl.SelectedTab = box.Parent.Parent as TabPage;
                }

                _toolTip.Show("Введите целое число в диапазоне [0-" + box.Tag + "]", box, 2000);
                box.Focus();
                box.SelectAll();
                _validatingOk = false;
            //}
        }

        private void ShowToolTipDouble(MaskedTextBox box)
        {
            //if (_validatingOk)
            //{
                if (box.Parent.Parent is TabPage)
                {
                    _configurationTabControl.SelectedTab = box.Parent.Parent as TabPage;
                }
                _toolTip.Show("Введите целое число в диапазоне [0,0-" + box.Tag + ".0]", box, 2000);
                box.Focus();
                box.SelectAll();
                _validatingOk = false;
            //}
        }

        private void ValidateAll()
        {
            _validatingOk = true;

            ValidateMaskedBoxes(_ulongrs232MaskedTextBoxes);
            ValidateMaskedBoxes(_ulongrs485MaskedTextBoxes);
            ValidateMaskedBoxes(_ulongRequestCountMaskedTextBox);
            ValidateMaskedBoxes(_ulongPereferyMaskedTextBoxes);
        }

        private void ValidateMaskedBoxes(MaskedTextBox[] boxes)
        {
            foreach (MaskedTextBox box in boxes)
            {
                box.ValidateText();
            }
        }

        private bool ValidateLimit()
        {
            _validatingOk = true;
            _limit.ValidateText();
            return _validatingOk;
        }

        private bool ValidatePercent()
        {
            _validatingOk = true;
            _Uin.ValidateText();
            return _validatingOk;
        }
        #endregion

        #region Обработчики событий

        private void MLK10_Configuration_Load(object sender, EventArgs e)
        {
            _statusLabel.Text = "";
            _statusStep.Text = "";
            _validatingOk = _readAll = true;
            _descriptionGroup.Text = string.Format(STEP, _step = 1);
            _descriptionBox.Text = Info1;

            PrepareRS232();
            PrepareRS485();
            PrepareRequestCount();
            PrepareRequest();
            PreparePeriphery();

            LoadConfigurationBlocks();
        }

        private void _readButton_Click(object sender, EventArgs e)
        {
            _readAll = true;
            LoadConfigurationBlocks();
        }

        private void Combo_DropDown(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            combo.DropDown -= new EventHandler(Combo_DropDown);
            if (combo.Items.Contains("ХХХХХ"))
            {
                combo.Items.Remove("ХХХХХ");
            }
        }

        private void _rs232Write_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes ==
                MessageBox.Show("Записать конфигурацию RS232 MLK10 №" + _device.DeviceNumber + " ?", "Внимание",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                ValidateMaskedBoxes(_ulongrs232MaskedTextBoxes);
                if (_validatingOk && WriteRS232())
                {
                    _statusLabel.Text = string.Empty;
                    _configRS232.SaveStruct();
                }
                else
                {
                    MessageBox.Show(BAD_DATA, ERROR_WRITE, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void _rs485Write_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes ==
                MessageBox.Show("Записать конфигурацию RS485 MLK10 №" + _device.DeviceNumber + " ?", "Внимание",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                ValidateMaskedBoxes(_ulongrs485MaskedTextBoxes);
                if (_validatingOk && WriteRS485())
                {
                    _statusLabel.Text = string.Empty;
                    _configRS485.SaveStruct();
                }
                else
                {
                    MessageBox.Show(BAD_DATA, ERROR_WRITE, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        
        private void _querysWrite_Click(object sender, EventArgs e)
        {

            if (DialogResult.Yes ==
                MessageBox.Show("Записать конфигурацию Запросов MLK10 №" + _device.DeviceNumber + " ?", "Внимание",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                ValidateMaskedBoxes(_ulongRequestCountMaskedTextBox);
                if (_validatingOk && WriteConfigRequest())
                {
                    _statusLabel.Text = string.Empty;
                    _configRequest.SaveStruct();
                }
                else
                {
                    MessageBox.Show(BAD_DATA, ERROR_WRITE, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void _peripheryWrite_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes ==
                MessageBox.Show("Записать конфигурацию Перефирии MLK10 №" + _device.DeviceNumber + " ?", "Внимание",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                ValidateMaskedBoxes(_ulongPereferyMaskedTextBoxes);
                if (_validatingOk && WritePerefery())
                {
                    _statusLabel.Text = string.Empty;
                    _periphery.SaveStruct();
                }
                else
                {
                    MessageBox.Show(BAD_DATA, ERROR_WRITE, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void _writeAllButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes ==
                MessageBox.Show("Записать конфигурацию MLK10 №" + _device.DeviceNumber + " ?", "Внимание",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                ValidateAll();
                if (_validatingOk && WriteRS232() && WriteRS485() && WriteConfigRequest() && WritePerefery())
                {
                    _statusLabel.Text = string.Empty;
                    _configRS232.SaveStruct();
                    _configRS485.SaveStruct();
                    _configRequest.SaveStruct();
                    _periphery.SaveStruct();
                }
                else
                {
                    MessageBox.Show(BAD_DATA, ERROR_WRITE, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void _querysCount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void _querysCount_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                int temp = Convert.ToInt32(_querysCount.Text);
                if (temp > 64 || temp < 0 || temp.ToString().Length > 2)
                {
                    _querysCount.Text = _oldValue;
                }
                else
                {
                    ReadRequestOk();
                }
            }
            catch (Exception)
            {
                int temp = 0;
                _querysCount.Text = temp.ToString();
            }
        }
        
        private void _querysCount_KeyDown(object sender, KeyEventArgs e)
        {
            _oldValue = _querysCount.Text;
        }
       
        private void _querys_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            var value = dgv.CurrentCell.Value;
            if (value != null)
                _oldCellValue = value.ToString();
            dgv.CurrentCell.Value = "";
        }

        private void _querys_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView) sender;
            if (dgv.CurrentCell.Value.ToString() == "")
            {
                dgv.CurrentCell.Value = _oldCellValue;
            }
        }
        
        private void _rs232paritetYNCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox) sender;
            if (combo.SelectedIndex == 1)
            {
                _rs232paritetCHETCombo.Enabled = true;
            }
            else
            {
                _rs232paritetCHETCombo.Enabled = false;
            }
        }
        
        private void _rs485paritetYNCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            if (combo.SelectedIndex == 1)
            {
                _rs485paritetCHETCombo.Enabled = true;
            }
            else
            {
                _rs485paritetCHETCombo.Enabled = false;
            }
        }

        private void _rs485modeCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (_rs485modeCheck.Checked)
            {
                _rs485modeCheck.Text = "Да";
                _rs485ToSendTB.Enabled = true;
                _rs485AnswerTB.Enabled = true;
            }
            else
            {
                _rs485modeCheck.Text = "Нет";
                _rs485ToSendTB.Enabled = false;
                _rs485AnswerTB.Enabled = false;
            }
        }

        private void MLK10_Configuration_FormClosing(object sender, FormClosingEventArgs e)
        {
            _analog.RemoveStructQueries();
        }
        
        private void _readFromDevice_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Прочитать значения коэффициентов MLK10 №" + _device.DeviceNumber + " ?", "Внимание",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                _periphery.LoadStruct();
            }
        }
        
        private void _limit_TextChanged(object sender, EventArgs e)
        {
            _Uin.Tag = _limit.Text;
        }

        private void _configurationTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabControl control = (TabControl)sender;
            TabPage page = control.SelectedTab;

            if (page.Name == _analogPage.Name)
            {
                _analog.LoadStructCycle();
            }
            else
            {
                _analog.RemoveStructQueries();
            }
        }

        private void _chanelCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            _continue.Enabled = true;
        }

        #endregion
    }
}