﻿using System.Collections.Generic;

namespace BEMN.MLK10.HelpClasses
{
    public class Strings
    {
        public static double DevVersion = 0;

        public static List<string> RS_SPEEDS
        {
            get
            {
                return new List<string>(new string[]
                {
                    "600",
                    "1200",
                    "2400",
                    "4800",
                    "9600",
                    "19200",
                    "38400",
                    "76800",
                    "900",
                    "1800",
                    "3600",
                    "7200",
                    "14400",
                    "28800",
                    "57600",
                    "115200"
                });
            }
        }

        public static List<string> RS_DATA_BITS
        {
            get
            {
                return new List<string>(new string[]
                {
                    "5 бит",
                    "6 бит",
                    "7 бит",
                    "8 бит",
                    "9 бит"
                });
            }
        }

        public static List<string> RS_STOPBITS
        {
            get
            {
                return new List<string>(new string[]
                {
                    "1 бит",
                    "2 бита"
                });
            }
        }

        public static List<string> RS_PARITET_YN
        {
            get
            {
                return new List<string>(new string[]
                {
                    "Нет",
                    "Есть"
                });
            }
        }

        public static List<string> RS_PARITET_CHET
        {
            get
            {
                return new List<string>(new string[]
                {
                    "Четный",
                    "Нечетный"
                });
            }
        }

        public static List<string> RS_DOUBLESPEED
        {
            get
            {
                return new List<string>(new string[]
                {
                    "Без удвоения",
                    "С удвоением"
                });
            }
        }

        public static List<string> SystemStatusErrors
        {
            get
            {
                return new List<string>(new string[]
                {
                    "Ошибка CRC",
                    "Ошибка таймаута",
                    "Плохой ответ",
                    "Плохой запрос",
                    "",
                    "",
                    "",
                    ""
                });
            }
        }

        public static List<string> Date
        {
            get
            {
                return new List<string>(new string[]
                {
                    "Понедельник",
                    "Вторник",
                    "Среда",
                    "Четверг",
                    "Пятница",
                    "Суббота",
                    "Воскресенье"
                });
            }
        }

        public static Dictionary<ushort, string> SysMessages
        {
            get
            {
                return new Dictionary<ushort, string>
                {
                    {0x0000, "SYSTEM_ERROR_OK"},
                    {0x0010, "SYSTEM_ERROR_TWI"},
                    {0x0020, "SYSTEM_ERROR_SPI"},
                    {0x0030, "SYSTEM_ERROR_EXEEPROM"},
                    {0x0040, "SYSTEM_ERROR__INEEPROM"},
                    {0x0050, "SYSTEM_ERROR_RTC"},
                    {0x0060, "SYSTEM_ERROR_FLASH"},
                    {0x0070, "SYSTEM_ERROR_USART0"},
                    {0x0080, "SYSTEM_ERROR_USART1"},
                    {0x0090, "SYSTEM_ERROR_ADC"}
                };
            }
        }

        public static Dictionary<ushort, string> LogicMessages
        {
            get
            {
                return new Dictionary<ushort, string>
                {
                    {0x1000, "LOGIC_ERROR"},
                    {0x1001, "LOGIC_ERROR_NULL_CMD"},
                    {0x1002, "LOGIC_ERROR_PC_OVER_SIZE"},
                    {0x1003, "LOGIC_ERROR_OVER_CMD"},
                    {0x1004, "LOGIC_ERROR_ACCSESS"},
                    {0x1005, "LOGIC_ERROR_ACCSESS_ADRESS"},
                    {0x1006, "LOGIC_ERROR_DATA_CONST"},
                    {0x1007, "LOGIC_ERROR_STACK"},
                    {0x1008, "LOGIC_ERROR_DEBUG_CMD"},
                    {0x1009, "LOGIC_ERROR_SPL_CMD"},
                    {0x100A, "LOGIC_ERROR_BAD_BP"},
                    {0x100B, "LOGIC_ERROR_TASK_TIME"},
                    {0x100C, "LOGIC_ERROR_DIV_ZERO"}
                };
            }
        }

        public static Dictionary<ushort, string> PowerMessages
        {
            get
            {
                return new Dictionary<ushort, string>
                {
                    {0xFE00, "Включение устройства"},
                    {0xFF00, "Выключение устройства"}
                };
            }
        }

        public static List<string> Command
        {
            get
            {
                return new List<string>
                {
                    "Чтение слов",
                    "Запись слов",
                    "Чтение бит",
                    "Запись бит"
                };
            }
        }
    }
}
