﻿using System;
using System.ComponentModel;
using System.Drawing;
using BEMN.Forms.Queries;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.MLK10
{
    public class Mlk10QueriesForm : QueriesForm, IFormView
    {
        #region Constructors
        public Mlk10QueriesForm() : base()
        {
            Multishow = true;
        }

        public Mlk10QueriesForm(MLK10 device) : base(device)
        {
            Multishow = true;
        }

        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MLK10); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(Mlk10QueriesForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.Calibrate; }
        }

        public string NodeName
        {
            get { return "Обмены"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}
