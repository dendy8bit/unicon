﻿using System;
using System.Runtime.InteropServices;
using BEMN.Devices;

namespace BEMN.MLK.Structures
{
    /*20 РЕЗЕРВ*/
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SysJournalReserve : IStruct
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3068)]
        public UInt16[] reserved;

        public StructInfo GetStructInfo()
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(SysJournalReserve)) / 2);
            sInfo.SlotsArray = (sInfo.FullSize > 64) ? true : false;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray, out int arrayLength)
        {
            object result = null;
            arrayLength = 0;
            return result;
        }
    }

}
