﻿using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.MLK10.Structures.IntermediateStructures
{
    public struct UsartConfig : IStruct, IStructInit
    {
        /*    15 _ _ _ _ _ _ 8|7 _ _ _ _ _ _ 0
             |_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|
               | | | | | | | ||| | | | | | | |______00   скорость передачи фикс. разр.0|	(от 0 до 15)
               | | | | | | | ||| | | | | | |________01	скорость передачи фикс. разр.1|	0-600, 1-1200, 2-2400, 3-4800, 4-9600, 5-19200,
               | | | | | | | ||| | | | | |__________02	скорость передачи фикс. разр.2|	6-38400, 7-76800, 8-900, 9-1800, 10-3600, 11-7200,
               | | | | | | | ||| | | | |____________03	скорость передачи фикс. разр.3|	12-14400, 13-28800, 14-57600, 15-115200
               | | | | | | | ||| | | |______________04	количество бит в байте разр.0	|	0 - 5 бит, 1 - 6 бит
               | | | | | | | ||| | |________________05	количество бит в байте разр.1 |	2 - 7 бит, 3 - 8 бит
               | | | | | | | ||| |__________________06	количество бит в байте разр.2 |	7 - 9 бит
               | | | | | | | |||____________________07	количество стоп бит 				|	0 - 1 бит, 1 - 2 бита
               |_|_|_|_|_|_|_|_ _ _ _ _ _ _ _ _ _ _
               | | | | | | | |______________________08   паритет разр.0						|	0 - чётный, 1 - нечётный
               | | | | | | |________________________09	паритет разр.1						|	0 - паритета нет, 1 - паритет есть
               | | | | | |__________________________10	удвоение скорости передачи		|	0 - без удвоения, 1 - с удвоением
               | | | | |____________________________11	режим передачи 					|	0 - асинхронный, 1 - синхронный
               | | | |______________________________12	полярность синхронизации		|	//	0 - по возрастанию, 1 - по убыванию
               | | |________________________________13	ведомый, ведущий					|	//	0 - ведомый, 1 - ведущий опрос
               | |__________________________________14	интерфейс							|	//	0 - USART, 1 - SPI
               |____________________________________15	режим работы						|	//	0 - обычный, 1 - мультипроцессорный
 */
        private ushort _hip1;				// разбирается только младшая половина ()
        private ushort _hip2;
        private byte _address;			//	адрес устройства
        private byte _toSend;			//	таймаут передачи
        private byte _toSendAfter;	//	таймаут после выдачи данных
        private byte _toSendBefore;	//	таймаут до выдачи данных
        private ushort _toWaitAnswer;	//	ожидание ответа

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            _hip1 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            _hip2 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            _address = array[index];
            index += sizeof(byte);

            _toSend = array[index];
            index += sizeof(byte);

            _toSendAfter = array[index];
            index += sizeof(byte);

            _toSendBefore = array[index];
            index += sizeof(byte);

            _toWaitAnswer = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(_hip1);
            result.Add(_hip2);
            result.Add(Common.TOWORD(_toSend, _address));
            result.Add(Common.TOWORD(_toSendBefore, _toSendAfter));
            result.Add(_toWaitAnswer);
            return result.ToArray();
        }
        #endregion

        #region Properties

        public ushort Hip1
        {
            get { return _hip1; }
            set { _hip1 = value; }
        }

        public ushort Hip2
        {
            get { return _hip2; }
            set { _hip2 = value; }
        }

        public byte Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public byte ToSend
        {
            get { return _toSend; }
            set { _toSend = value; }
        }

        public byte ToSendAfter
        {
            get { return _toSendAfter; }
            set { _toSendAfter = value; }
        }

        public byte ToSendBefore
        {
            get { return _toSendBefore; }
            set { _toSendBefore = value; }
        }

        public ushort ToWaitAnswer
        {
            get { return _toWaitAnswer; }
            set { _toWaitAnswer = value; }
        }

        #endregion
    }
}
