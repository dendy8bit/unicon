﻿using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.MLK10.Structures.IntermediateStructures
{
    public struct QueryStatusChanel : IStruct, IStructInit
    {
        private byte _acceptMessage;				//	принято правильных сообщений
        private byte _sendMessage;					//	послано сообщений
        private byte _communicationQuality;		    //	процент по связи
        private byte _status;						//	возникающие ошибки в реальном времени
                                                    // uInt8 crc:	    1;	ошибка CRC
                                                    // uInt8 timeout:	1;  ошибка таймаута
                                                    // uInt8 answerBad:	1;	плохой ответ
				                                    // uInt8 requestBad:1;	плохой запрос ???
                                                    // 4 бита резерва
        public byte AcceptMessage
        {
            get { return _acceptMessage; }
        }

        public byte SendMessage
        {
            get { return _sendMessage; }
        }

        public byte CommunicationQuality
        {
            get { return _communicationQuality; }
        }

        public byte Status
        {
            get { return _status; }
        }
        
        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            _acceptMessage = array[0];
            _sendMessage = array[1];
            _communicationQuality = array[2];
            _status = array[3];
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(Common.TOWORD(SendMessage, AcceptMessage));
            result.Add(Common.TOWORD(Status, CommunicationQuality));
            return result.ToArray();
        }
        #endregion
    }
}
