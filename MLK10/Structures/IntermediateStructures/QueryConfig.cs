﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.MLK10.Structures.IntermediateStructures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct QueryConfig : IStruct, IStructInit
    {
        private ushort _numberRequest;		//	количество запросов
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private ushort[] _reserve;

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            _numberRequest = Common.TOWORD(array[1], array[0]);

            _reserve = new ushort[3];
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(_numberRequest);
            result.AddRange(new ushort[3]);
            return result.ToArray();
        }
        #endregion

        #region Property
        public ushort NumberRequest
        {
            get { return _numberRequest; }
            set { _numberRequest = value; }
        }
        #endregion
    }
}
