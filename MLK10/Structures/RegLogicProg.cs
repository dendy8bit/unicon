﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.MLK10.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct RegLogicProg : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        private byte[] _AX;					//	аккумулятор
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        private byte[] _BX;					//	аккумулятор
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        private byte[] _CX;				    //	аккумулятор-индексный
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        private byte[] _DX;					//	аккумулятор-индексный
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        private byte[] _SP;					//	указатель стека
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        private byte[] _DP;					//	указатель данных
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        private ushort[] _reserve1;					//	резерв
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        private ushort[] _reserve2;					//	резерв

        private byte _LP;                             // упарвляющий
        private byte _SC;                             // управляющий

        private ushort _SREG;                         // статуса
        private ushort _DEBUG;                        // отладки

        private ushort _IP;                           // выборки команд
        private ushort _CS;                           // выборки команд

        private ushort _COMMAND;                      // команда
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        private ushort[] _reserve;                    // резерв

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region [IStructInit]

        public void InitStruct(byte[] array)
        {
            int index = 0;

            _AX = new byte[8];
            for (int i = index; i < index + _AX.Length; i++)
            {
                _AX[i - index] = array[i];
            }
            index += sizeof (byte)*8;

            _BX = new byte[8];
            for (int i = index; i < index + _BX.Length; i++)
            {
                _BX[i - index] = array[i];
            }
            index += sizeof(byte)*8;

            _CX = new byte[8];
            for (int i = index; i < index + _CX.Length; i++)
            {
                _CX[i - index] = array[i];
            }
            index += sizeof(byte) * 8;

            _DX = new byte[8];
            for (int i = index; i < index + _DX.Length; i++)
            {
                _DX[i - index] = array[i];
            }
            index += sizeof(byte) * 8;

            _SP = new byte[8];
            for (int i = index; i < index + _SP.Length; i++)
            {
                _SP[i - index] = array[i];
            }
            index += sizeof(byte) * 8;

            _DP = new byte[8];
            for (int i = index; i < index + _DP.Length; i++)
            {
                _DP[i - index] = array[i];
            }
            index += sizeof(byte) * 8;

            _reserve1 = new ushort[4];
            _reserve2 = new ushort[4];
            index += sizeof (ushort)*8;

            _LP = array[index];
            index += sizeof(byte);

            _SC = array[index];
            index += sizeof (byte);

            _SREG = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            _DEBUG = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            _IP = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            _CS = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            _COMMAND = Common.TOWORD(array[index + 1], array[index]);

            _reserve = new ushort[2];
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            for (int i = 0; i < _AX.Length; i += 2)
            {
                result.Add(Common.TOWORD(_AX[i+1], _AX[i]));
            }
            for (int i = 0; i < _BX.Length; i += 2)
            {
                result.Add(Common.TOWORD(_BX[i + 1], _BX[i]));
            }
            for (int i = 0; i < _CX.Length; i += 2)
            {
                result.Add(Common.TOWORD(_CX[i + 1], _CX[i]));
            }
            for (int i = 0; i < _DX.Length; i += 2)
            {
                result.Add(Common.TOWORD(_DX[i + 1], _DX[i]));
            }
            for (int i = 0; i < _SP.Length; i += 2)
            {
                result.Add(Common.TOWORD(_SP[i + 1], _SP[i]));
            } 
            for (int i = 0; i < _DP.Length; i += 2)
            {
                result.Add(Common.TOWORD(_DP[i + 1], _DP[i]));
            }
            result.AddRange(_reserve1);
            result.AddRange(_reserve2);
            result.Add(Common.TOWORD(_SC,_LP));
            result.Add(_SREG);
            result.Add(_DEBUG);
            result.Add(_IP);
            result.Add(_CS);
            result.Add(_COMMAND);
            result.AddRange(_reserve);
            return result.ToArray();
        }
        #endregion

        #region Properties

        #region AX
        public string Alow1
        {
            get { return Convert.ToString(_AX[6], 16).ToUpper().PadLeft(2, '0'); }
            set { _AX[6] = Convert.ToByte(value, 16); }
        }
        public string Ahigh1
        {
            get { return Convert.ToString(_AX[7], 16).ToUpper().PadLeft(2, '0'); }
            set { _AX[7] = Convert.ToByte(value, 16); }
        }
        public string AXlow1
        {
            get { return Ahigh1 + Alow1; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                Ahigh1 = setter[0];
                Alow1 = setter[1];
            }
        }

        public string Alow2
        {
            get { return Convert.ToString(_AX[4], 16).ToUpper().PadLeft(2, '0'); }
            set { _AX[4] = Convert.ToByte(value, 16); }
        }
        public string Ahigh2
        {
            get { return Convert.ToString(_AX[5], 16).ToUpper().PadLeft(2, '0'); }
            set { _AX[5] = Convert.ToByte(value, 16); }
        }
        public string AXhigh1
        {
            get { return Ahigh2 + Alow2; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                Ahigh2 = setter[0];
                Alow2 = setter[1];
            }
        }

        public string EAXlow
        {
            get { return AXhigh1 + AXlow1; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                AXhigh1 = setter[0];
                AXlow1 = setter[1];
            }
        }

        public string Alow3
        {
            get { return Convert.ToString(_AX[2], 16).ToUpper().PadLeft(2, '0'); }
            set { _AX[2] = Convert.ToByte(value, 16); }
        }
        public string Ahigh3
        {
            get { return Convert.ToString(_AX[3], 16).ToUpper().PadLeft(2, '0'); }
            set { _AX[3] = Convert.ToByte(value, 16); }
        }
        public string AXlow2
        {
            get { return Ahigh3 + Alow3; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                Ahigh3 = setter[0];
                Alow3 = setter[1];
            }
        }

        public string Alow4
        {
            get { return Convert.ToString(_AX[0], 16).ToUpper().PadLeft(2, '0'); }
            set { _AX[0] = Convert.ToByte(value, 16); }
        }
        public string Ahigh4
        {
            get { return Convert.ToString(_AX[1], 16).ToUpper().PadLeft(2, '0'); }
            set { _AX[1] = Convert.ToByte(value, 16); }
        }
        public string AXhigh2
        {
            get { return Ahigh4 + Alow4; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                Ahigh4 = setter[0];
                Alow4 = setter[1];
            }
        }

        public string EAXhigh
        {
            get { return AXhigh2 + AXlow2; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                AXhigh2 = setter[0];
                AXlow2 = setter[1];
            }
        }

        public string RAX
        {
            get { return EAXhigh + EAXlow; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                EAXhigh = setter[0];
                EAXlow = setter[1];
            }
        }
        #endregion

        #region BX
        public string Blow1
        {
            get { return Convert.ToString(_BX[6], 16).ToUpper().PadLeft(2,'0'); }
            set { _BX[6] = Convert.ToByte(value, 16); }
        }
        public string Bhigh1
        {
            get { return Convert.ToString(_BX[7], 16).ToUpper().PadLeft(2, '0'); }
            set { _BX[7] = Convert.ToByte(value, 16); }
        }
        public string BXlow1
        {
            get { return Bhigh1 + Blow1; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                Bhigh1 = setter[0];
                Blow1 = setter[1];
            }
        }

        public string Blow2
        {
            get { return Convert.ToString(_BX[4], 16).ToUpper().PadLeft(2, '0'); }
            set { _BX[4] = Convert.ToByte(value, 16); }
        }
        public string Bhigh2
        {
            get { return Convert.ToString(_BX[5], 16).ToUpper().PadLeft(2, '0'); }
            set { _BX[5] = Convert.ToByte(value, 16); }
        }
        public string BXhigh1
        {
            get { return Bhigh2 + Blow2; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                Bhigh2 = setter[0];
                Blow2 = setter[1];
            }
        }

        public string EBXlow
        {
            get { return BXhigh1 + BXlow1; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                BXhigh1 = setter[0];
                BXlow1 = setter[1];
            }
        }

        public string Blow3
        {
            get { return Convert.ToString(_BX[2], 16).ToUpper().PadLeft(2, '0'); }
            set { _BX[2] = Convert.ToByte(value, 16); }
        }
        public string Bhigh3
        {
            get { return Convert.ToString(_BX[3], 16).ToUpper().PadLeft(2, '0'); }
            set { _BX[3] = Convert.ToByte(value, 16); }
        }
        public string BXlow2
        {
            get { return Bhigh3 + Blow3; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                Bhigh3 = setter[0];
                Blow3 = setter[1];
            }
        }

        public string Blow4
        {
            get { return Convert.ToString(_BX[0], 16).ToUpper().PadLeft(2, '0'); }
            set { _BX[0] = Convert.ToByte(value, 16); }
        }
        public string Bhigh4
        {
            get { return Convert.ToString(_BX[1], 16).ToUpper().PadLeft(2, '0'); }
            set { _BX[1] = Convert.ToByte(value, 16); }
        }
        public string BXhigh2
        {
            get { return Bhigh4 + Blow4; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                Bhigh4 = setter[0];
                Blow4 = setter[1];
            }
        }

        public string EBXhigh
        {
            get { return BXhigh2 + BXlow2; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                BXhigh2 = setter[0];
                BXlow2 = setter[1];
            }
        }

        public string RBX
        {
            get { return EBXhigh + EBXlow; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                EBXhigh = setter[0];
                EBXlow = setter[1];
            }
        }
        #endregion

        #region CX
        public string Clow1
        {
            get { return Convert.ToString(_CX[6], 16).ToUpper().PadLeft(2, '0'); }
            set { _CX[6] = Convert.ToByte(value, 16); }
        }
        public string Chigh1
        {
            get { return Convert.ToString(_CX[7], 16).ToUpper().PadLeft(2, '0'); }
            set { _CX[7] = Convert.ToByte(value, 16); }
        }
        public string CXlow1
        {
            get { return Chigh1 + Clow1; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                Chigh1 = setter[0];
                Clow1 = setter[1];
            }
        }

        public string Clow2
        {
            get { return Convert.ToString(_CX[4], 16).ToUpper().PadLeft(2, '0'); }
            set { _CX[4] = Convert.ToByte(value, 16); }
        }
        public string Chigh2
        {
            get { return Convert.ToString(_CX[5], 16).ToUpper().PadLeft(2, '0'); }
            set { _CX[5] = Convert.ToByte(value, 16); }
        }
        public string CXhigh1
        {
            get { return Chigh2 + Clow2; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                Chigh2 = setter[0];
                Clow2 = setter[1];
            }
        }

        public string ECXlow
        {
            get { return CXhigh1 + CXlow1; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                CXhigh1 = setter[0];
                CXlow1 = setter[1];
            }
        }

        public string Clow3
        {
            get { return Convert.ToString(_CX[2], 16).ToUpper().PadLeft(2, '0'); }
            set { _CX[2] = Convert.ToByte(value, 16); }
        }
        public string Chigh3
        {
            get { return Convert.ToString(_CX[3], 16).ToUpper().PadLeft(2, '0'); }
            set { _CX[3] = Convert.ToByte(value, 16); }
        }
        public string CXlow2
        {
            get { return Chigh3 + Clow3; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                Chigh3 = setter[0];
                Clow3 = setter[1];
            }
        }

        public string Clow4
        {
            get { return Convert.ToString(_CX[0], 16).ToUpper().PadLeft(2, '0'); }
            set { _CX[0] = Convert.ToByte(value, 16); }
        }
        public string Chigh4
        {
            get { return Convert.ToString(_CX[1], 16).ToUpper().PadLeft(2, '0'); }
            set { _CX[1] = Convert.ToByte(value, 16); }
        }
        public string CXhigh2
        {
            get { return Chigh4 + Clow4; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                Chigh4 = setter[0];
                Clow4 = setter[1];
            }
        }

        public string ECXhigh
        {
            get { return CXhigh2 + CXlow2; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                CXhigh2 = setter[0];
                CXlow2 = setter[1];
            }
        }

        public string RCX
        {
            get { return ECXhigh + ECXlow; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                ECXhigh = setter[0];
                ECXlow = setter[1];
            }
        }
        #endregion

        #region DX
        public string Dlow1
        {
            get { return Convert.ToString(_DX[6], 16).ToUpper().PadLeft(2, '0'); }
            set { _DX[6] = Convert.ToByte(value, 16); }
        }
        public string Dhigh1
        {
            get { return Convert.ToString(_DX[7], 16).ToUpper().PadLeft(2, '0'); }
            set { _DX[7] = Convert.ToByte(value, 16); }
        }
        public string DXlow1
        {
            get { return Dhigh1 + Dlow1; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                Dhigh1 = setter[0];
                Dlow1 = setter[1];
            }
        }

        public string Dlow2
        {
            get { return Convert.ToString(_DX[4], 16).ToUpper().PadLeft(2, '0'); }
            set { _DX[4] = Convert.ToByte(value, 16); }
        }
        public string Dhigh2
        {
            get { return Convert.ToString(_DX[5], 16).ToUpper().PadLeft(2, '0'); }
            set { _DX[5] = Convert.ToByte(value, 16); }
        }
        public string DXhigh1
        {
            get { return Dhigh2 + Dlow2; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                Dhigh2 = setter[0];
                Dlow2 = setter[1];
            }
        }

        public string EDXlow
        {
            get { return DXhigh1 + DXlow1; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                DXhigh1 = setter[0];
                DXlow1 = setter[1];
            }
        }

        public string Dlow3
        {
            get { return Convert.ToString(_DX[2], 16).ToUpper().PadLeft(2, '0'); }
            set { _DX[2] = Convert.ToByte(value, 16); }
        }
        public string Dhigh3
        {
            get { return Convert.ToString(_DX[3], 16).ToUpper().PadLeft(2, '0'); }
            set { _DX[3] = Convert.ToByte(value, 16); }
        }
        public string DXlow2
        {
            get { return Dhigh3 + Dlow3; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                Dhigh3 = setter[0];
                Dlow3 = setter[1];
            }
        }

        public string Dlow4
        {
            get { return Convert.ToString(_DX[0], 16).ToUpper().PadLeft(2, '0'); }
            set { _DX[0] = Convert.ToByte(value, 16); }
        }
        public string Dhigh4
        {
            get { return Convert.ToString(_DX[1], 16).ToUpper().PadLeft(2, '0'); }
            set { _DX[1] = Convert.ToByte(value, 16); }
        }
        public string DXhigh2
        {
            get { return Dhigh4 + Dlow4; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                Dhigh4 = setter[0];
                Dlow4 = setter[1];
            }
        }

        public string EDXhigh
        {
            get { return DXhigh2 + DXlow2; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                DXhigh2 = setter[0];
                DXlow2 = setter[1];
            }
        }

        public string RDX
        {
            get { return EDXhigh + EDXlow; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                EDXhigh = setter[0];
                EDXlow = setter[1];
            }
        }
        #endregion

        #region SP
        public string SP
        {
            get
            {
                string ret = string.Empty;
                for(int i = 0; i < _SP.Length; i+=2)
                {
                    ret += Convert.ToString(_SP[i+1], 16).ToUpper().PadLeft(2, '0');
                    ret += Convert.ToString(_SP[i], 16).ToUpper().PadLeft(2, '0');
                }
                return ret;
            }
            set
            {
                string[] setter = new string[8];
                int ind = 0;
                int length = value.Length/8;
                for (int i = 0; i < setter.Length; i++)
                {
                    setter[i] = value.Substring(ind, length);
                    ind += length;
                }
                for(int i = 0; i<_SP.Length; i+=2)
                {
                    _SP[i+1] = Convert.ToByte(setter[i], 16);
                    _SP[i] = Convert.ToByte(setter[i+1], 16);
                }
            }
        }
        #endregion

        #region DP
        public string DP
        {
            get
            {
                string ret = string.Empty;
                for (int i = 0; i < _DP.Length; i+=2)
                {
                    ret += Convert.ToString(_DP[i + 1], 16).ToUpper().PadLeft(2, '0');
                    ret += Convert.ToString(_DP[i], 16).ToUpper().PadLeft(2, '0');
                }
                return ret;
            }
            set
            {
                string[] setter = new string[8];
                int ind = 0;
                int length = value.Length / 4;
                for (int i = 0; i < setter.Length; i++)
                {
                    setter[i] = value.Substring(ind, length);
                    ind += length;
                }
                for (int i = 0; i < _DP.Length; i++)
                {
                    _DP[i + 1] = Convert.ToByte(setter[i], 16);
                    _DP[i] = Convert.ToByte(setter[i + 1], 16);
                }
            }
        }
        #endregion

        #region SCLP
        public string LP
        {
            get { return Convert.ToString(_LP, 16).ToUpper().PadLeft(2, '0'); }
            set { _LP = Convert.ToByte(value, 16); }
        }
        public string SC
        {
            get { return Convert.ToString(_SC, 16).ToUpper().PadLeft(2, '0'); }
            set { _SC = Convert.ToByte(value, 16); }
        }
        public string SCLP
        {
            get { return SC + LP; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length/2);
                setter[1] = value.Substring(value.Length/2, value.Length / 2);
                SC = setter[0];
                LP = setter[1];
            }
        }
        #endregion

        public string SREG
        {
            get { return Convert.ToString(_SREG, 16).ToUpper().PadLeft(4, '0'); }
            set { _SREG = Convert.ToUInt16(value, 16); }
        }

        public string DEBUG
        {
            get { return Convert.ToString(_DEBUG, 16).ToUpper().PadLeft(4,'0'); }
            set { _DEBUG = Convert.ToUInt16(value, 16); }
        }

        #region CSIP
        public string IP
        {
            get { return Convert.ToString(_IP, 16).ToUpper().PadLeft(4, '0'); }
            set { _IP = Convert.ToUInt16(value, 16); }
        }

        public string CS
        {
            get { return Convert.ToString(_CS, 16).ToUpper().PadLeft(4, '0'); }
            set { _CS = Convert.ToUInt16(value, 16); }
        }

        public string CSIP
        {
            get { return CS + IP; }
            set
            {
                string[] setter = new string[2];
                setter[0] = value.Substring(0, value.Length / 2);
                setter[1] = value.Substring(value.Length / 2, value.Length / 2);
                CS = setter[0];
                IP = setter[1];
            }
        }
        #endregion

        public string COMMAND
        {
            get { return Convert.ToString(_COMMAND, 16).ToUpper().PadLeft(4, '0'); }
            set { _COMMAND = Convert.ToUInt16(value, 16); }
        }
        
        #endregion
    }

}
