﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.MBServer;
using BEMN.MLK.Data;
using BEMN.MLK.Structures.IntermediateStructures;

namespace BEMN.MLK.Structures
{
    /*8*/
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MemConfigRS232 : IStruct, IStructInit
    {
        public UsartConfig configRS232;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private UInt16[] _reserved;

        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, slotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            configRS232 = new UsartConfig();

            var oneStruct = new byte[Marshal.SizeOf(typeof(UsartConfig))];
            Array.ConstrainedCopy(array, 0, oneStruct, 0, oneStruct.Length);
            configRS232.InitStruct(oneStruct);

            _reserved = new ushort[3];
            for (int i = 0; i < _reserved.Length; i++)
            {
                _reserved[i] = 0;
            }
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.AddRange(configRS232.GetValues());
            result.AddRange(_reserved);
            return result.ToArray();
        }
        #endregion

        #region Properties
        public string RS232Speed
        {
            get
            {
                return Strings.RS_SPEEDS[(int)Common.GetBits(configRS232.Hip1, 0, 1, 2, 3) >> 0];
            }
            set
            {
                configRS232.Hip1 = Common.SetBits(configRS232.Hip1, (ushort)Strings.RS_SPEEDS.IndexOf(value), 0, 1, 2, 3);
            }
        }

        public string RS232StopBits
        {
            get
            {
                return Strings.RS_STOPBITS[(int)Common.GetBits(configRS232.Hip1, 7) >> 7];
            }
            set
            {
                configRS232.Hip1 = Common.SetBits(configRS232.Hip1, (ushort)Strings.RS_STOPBITS.IndexOf(value), 7);
            }
        }

        public string RS232ParitetChet
        {
            get
            {
                return Strings.RS_PARITET_CHET[(int)Common.GetBits(configRS232.Hip1, 8) >> 8];
            }
            set
            {
                configRS232.Hip1 = Common.SetBits(configRS232.Hip1, (ushort)Strings.RS_PARITET_CHET.IndexOf(value), 8);
            }
        }

        public string RS232ParitetOnOff
        {
            get
            {
                return Strings.RS_PARITET_YN[(int)Common.GetBits(configRS232.Hip1, 9) >> 9];
            }
            set
            {
                configRS232.Hip1 = Common.SetBits(configRS232.Hip1, (ushort)Strings.RS_PARITET_YN.IndexOf(value), 9);
            }
        }

        public string Rs232DoubleSpeed
        {
            get
            {
                return Strings.RS_DOUBLESPEED[(int)Common.GetBits(configRS232.Hip1, 10) >> 10];
            }
            set
            {
                configRS232.Hip1 = Common.SetBits(configRS232.Hip1, (ushort)Strings.RS_DOUBLESPEED.IndexOf(value), 10);
            }
        }

        public byte RS232Address
        {
            get
            {
                return configRS232.Address;
            }
            set
            {
                configRS232.Address = value;
            }
        }

        public byte RS232ToSend
        {
            get
            {
                return configRS232.ToSend;
            }
            set
            {
                configRS232.ToSend = value;
            }
        }

        public byte RS232ToSendAfter
        {
            get
            {
                return configRS232.ToSendAfter;
            }
            set
            {
                configRS232.ToSendAfter = value;
            }
        }

        public byte RS232ToSendBefore
        {
            get
            {
                return configRS232.ToSendBefore;
            }
            set
            {
                configRS232.ToSendBefore = value;
            }
        }
        #endregion
    }
}
