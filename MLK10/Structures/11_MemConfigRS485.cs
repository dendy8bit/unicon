﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic; 
using BEMN.Devices;
using BEMN.MBServer;
using BEMN.MLK.Data;
using BEMN.MLK.Structures.IntermediateStructures;

namespace BEMN.MLK.Structures
{
    /*9*/
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MemConfigRS485 : IStruct, IStructInit
    {
        public UsartConfig configRS485;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public UInt16[] reserved;

        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, slotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            configRS485 = new UsartConfig();

            byte[] oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(UsartConfig))];
            Array.ConstrainedCopy(array, 0, oneStruct, 0, oneStruct.Length);
            configRS485.InitStruct(oneStruct);

            reserved = new ushort[3];
            for (int i = 0; i < reserved.Length; i++)
            { reserved[i] = 0; }
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.AddRange(configRS485.GetValues());
            result.AddRange(new ushort[3]);
            return result.ToArray();
        }
        #endregion

        #region Properties
        public string RS485Mode
        {
            get
            {
                return Strings.RS_MODE[(int)Common.GetBits(configRS485.Hip1, 13) >> 13];
            }
            set
            {
                configRS485.Hip1 = Common.SetBits(configRS485.Hip1, (ushort)Strings.RS_MODE.IndexOf(value), 13);
            }
        }

        public string RS485Speed
        {
            get
            {
                return Strings.RS_SPEEDS[(int)Common.GetBits(configRS485.Hip1, 0, 1, 2, 3) >> 0];
            }
            set
            {
                configRS485.Hip1 = Common.SetBits(configRS485.Hip1, (ushort)Strings.RS_SPEEDS.IndexOf(value), 0, 1, 2, 3);
            }
        }

        public string RS485StopBits
        {
            get
            {
                return Strings.RS_STOPBITS[(int)Common.GetBits(configRS485.Hip1, 7) >> 7];
            }
            set
            {
                configRS485.Hip1 = Common.SetBits(configRS485.Hip1, (ushort)Strings.RS_STOPBITS.IndexOf(value), 7);
            }
        }

        public string RS485ParitetChet
        {
            get
            {
                return Strings.RS_PARITET_CHET[(int)Common.GetBits(configRS485.Hip1, 8) >> 8];
            }
            set
            {
                configRS485.Hip1 = Common.SetBits(configRS485.Hip1, (ushort)Strings.RS_PARITET_CHET.IndexOf(value), 8);
            }
        }

        public string RS485ParitetOnOff
        {
            get
            {
                return Strings.RS_PARITET_YN[(int)Common.GetBits(configRS485.Hip1, 9) >> 9];
            }
            set
            {
                configRS485.Hip1 = Common.SetBits(configRS485.Hip1, (ushort)Strings.RS_PARITET_YN.IndexOf(value), 9);
            }
        }

        public string RS485DoubleSpeed
        {
            get
            {
                return Strings.RS_DOUBLESPEED[(int)Common.GetBits(configRS485.Hip1, 10) >> 10];
            }
            set
            {
                configRS485.Hip1 = Common.SetBits(configRS485.Hip1, (ushort)Strings.RS_DOUBLESPEED.IndexOf(value), 10);
            }
        }

        public byte RS485Address
        {
            get
            {
                return configRS485.Address;
            }
            set
            {
                configRS485.Address = value;
            }
        }

        public byte RS485ToSend
        {
            get
            {
                return configRS485.ToSend;
            }
            set
            {
                configRS485.ToSend = value;
            }
        }

        public byte RS485ToSendAfter
        {
            get
            {
                return configRS485.ToSendAfter;
            }
            set
            {
                configRS485.ToSendAfter = value;
            }
        }

        public byte RS485ToSendBefore
        {
            get
            {
                return configRS485.ToSendBefore;
            }
            set
            {
                configRS485.ToSendBefore = value;
            }
        }

        public ushort RS485Answer
        {
            get
            {
                return configRS485.ToWaitAnswer;
            }
            set
            {
                configRS485.ToWaitAnswer = value;
            }
        }
        #endregion
    }
}
