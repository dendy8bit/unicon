﻿using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.MLK10.Structures
{
    public struct AnalogExtConfig : IStruct, IStructInit
    {
        private ushort _b;				//	коэффициент
        private ushort _a;				//	коэффициент
        private ushort _pA;			    //	степень
        private ushort _reserved;

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region IStructInit Members
        public void InitStruct(byte[] array)
        {
            int index = 0;
            _b = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            _a = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            _pA = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof (ushort);

            _reserved = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(_b);
            result.Add(_a);
            result.Add(_pA);
            result.Add(_reserved);
            return result.ToArray();
        }
        #endregion

        #region Properties
        public ushort B
        {
            get { return _b; }
            set { _b = value; }
        }

        public ushort A
        {
            get { return _a; }
            set { _a = value; }
        }

        public ushort Pa
        {
            get { return _pA; }
            set { _pA = value; }
        }
        #endregion
    }
}
