﻿using System.Runtime.InteropServices;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MLK10.Structures.IntermediateStructures;

namespace BEMN.MLK10.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SysErr : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public QueryStatusChanel[] StatisticsRequest;	//	статистика по запросам

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            StatisticsRequest = new QueryStatusChanel[16];

            int index = 0;
            StructHelper.GetArrayStruct(array, StatisticsRequest, ref index);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            for (int j = 0; j < StatisticsRequest.Length; j++)
            {
                result.AddRange(StatisticsRequest[j].GetValues());
            }
            return result.ToArray();
        }
        #endregion
    }
}
