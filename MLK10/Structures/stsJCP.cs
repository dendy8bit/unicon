﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.MBServer;

namespace BEMN.MLK.Structures
{
    /*18*/
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct stsJCP : IStruct, IStructInit
    {
        public UInt16 currentMessage;
        public UInt16 counter;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public UInt16[] reserved;

        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, slotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            int index = 0;
            currentMessage = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            counter = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(currentMessage);
            result.Add(counter);
            result.AddRange(reserved);
            return result.ToArray();
        }
        #endregion
    }
}
