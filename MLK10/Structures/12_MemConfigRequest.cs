﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.MLK.Structures.IntermediateStructures;
using BEMN.MBServer;

namespace BEMN.MLK.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MemConfigRequest : IStruct, IStructInit
    {
        public MasterQueryConfig Config;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public MasterQueryElem[] Request;

        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, slotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]

        #region [IStructInit]

        public void InitStruct(byte[] array)
        {
            int ind = 0;
            var oneStruct = new byte[Marshal.SizeOf(typeof (MasterQueryConfig))];
            Array.ConstrainedCopy(array, ind, oneStruct, 0, oneStruct.Length);
            Config.InitStruct(oneStruct);

            Request = new MasterQueryElem[32];
            ind += Marshal.SizeOf(typeof (MasterQueryConfig));
            for (int i = 0; i < Request.Length; i++)
            {
                oneStruct = new byte[Marshal.SizeOf(typeof (MasterQueryElem))];
                Array.ConstrainedCopy(array, ind, oneStruct, 0, oneStruct.Length);
                Request[i].InitStruct(oneStruct);
                ind += Marshal.SizeOf(typeof (MasterQueryElem));
            }
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.AddRange(Config.GetValues());
            foreach (var r in Request)
            {
                result.AddRange(r.GetValues());
            }
            return result.ToArray();
        }

        #endregion
    }
}
