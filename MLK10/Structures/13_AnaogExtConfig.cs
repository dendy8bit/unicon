﻿using System;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.MBServer;

namespace BEMN.MLK.Structures.IntermediateStructures
{
    public struct AnalogExtConfig : IStruct, IStructInit
    {
        private UInt16 _b;				//	коэффициент
        private UInt16 _a;				//	коэффициент
        private UInt16 _degree;			//	степень
        private UInt16 _reserved;

        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, slotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]

        #region IStructInit Members
        public void InitStruct(byte[] array)
        {
            int index = 0;
            _b = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            _a = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            _degree = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof (UInt16);

            _reserved = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(_b);
            result.Add(_a);
            result.Add(_degree);
            result.Add(_reserved);
            return result.ToArray();
        }
        #endregion

        #region Properties
        public ushort B
        {
            get { return _b; }
            set { _b = value; }
        }

        public ushort A
        {
            get { return _a; }
            set { _a = value; }
        }

        public ushort Degree
        {
            get { return _degree; }
            set { _degree = value; }
        }
        #endregion
    }
}
