﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.MLK10.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct UIORam : IStruct, IStructInit
    {
        private ushort _analog;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
        private ushort[] _reserve1;
        private ushort _discret;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private ushort[] _reserve2;
        private ushort _relay;

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            int index = 0;
            _analog = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof (ushort);

            _reserve1 = new ushort[7];
            index += sizeof (ushort)*7;

            _discret = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof (ushort);
            
            _reserve2 = new ushort[3];
            index += sizeof (ushort)*3;

            _relay = Common.TOWORD(array[index + 1], array[index]);           
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(_analog);
            result.AddRange(_reserve1);
            result.Add(_discret);
            result.AddRange(_reserve2);
            result.Add(_relay);
            return result.ToArray();
        }
        #endregion

        public ushort Analog
        {
            get { return _analog; }
        }
        /// <summary>
        /// Дискреты, начиная с младшего бита
        /// </summary>
        public BitArray Discrets
        {
            get
            {
                byte[] buf = Common.TOBYTE(_discret).Reverse().ToArray();
                BitArray ret = new BitArray(buf);
                return ret;
            }
        }

        public BitArray Relay
        {
            get
            {
                byte[] buf = Common.TOBYTE(_relay).Reverse().ToArray();
                BitArray ret = new BitArray(buf);
                return ret;
            }
            set
            {
                _relay = Common.BitsToUshort(value);
            }
        }
    }
}
