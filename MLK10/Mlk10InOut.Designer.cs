﻿using BEMN.Forms;
namespace BEMN.MLK10
{
    partial class Mlk10InOut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._writeDateTimeButt = new System.Windows.Forms.Button();
            this._dateTimeNowButt = new System.Windows.Forms.Button();
            this._astrStopCB = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._astrTimeClockTB = new System.Windows.Forms.MaskedTextBox();
            this._astrDateClockTB = new System.Windows.Forms.MaskedTextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this._rele16 = new BEMN.Forms.LedControl();
            this._rele15 = new BEMN.Forms.LedControl();
            this.label6 = new System.Windows.Forms.Label();
            this._rele14 = new BEMN.Forms.LedControl();
            this._rele13 = new BEMN.Forms.LedControl();
            this._rele7 = new BEMN.Forms.LedControl();
            this.label5 = new System.Windows.Forms.Label();
            this._rele12 = new BEMN.Forms.LedControl();
            this._rele11 = new BEMN.Forms.LedControl();
            this._rele6 = new BEMN.Forms.LedControl();
            this.label4 = new System.Windows.Forms.Label();
            this._rele10 = new BEMN.Forms.LedControl();
            this._rele9 = new BEMN.Forms.LedControl();
            this._rele5 = new BEMN.Forms.LedControl();
            this._rele1 = new BEMN.Forms.LedControl();
            this._rele8 = new BEMN.Forms.LedControl();
            this.label39 = new System.Windows.Forms.Label();
            this._rele4 = new BEMN.Forms.LedControl();
            this._rele2 = new BEMN.Forms.LedControl();
            this._rele3 = new BEMN.Forms.LedControl();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._diskr16 = new BEMN.Forms.LedControl();
            this._diskr15 = new BEMN.Forms.LedControl();
            this._diskr14 = new BEMN.Forms.LedControl();
            this._diskr13 = new BEMN.Forms.LedControl();
            this._diskr8 = new BEMN.Forms.LedControl();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this._diskr1 = new BEMN.Forms.LedControl();
            this._diskr3 = new BEMN.Forms.LedControl();
            this._diskr7 = new BEMN.Forms.LedControl();
            this.label36 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this._diskr2 = new BEMN.Forms.LedControl();
            this._diskr6 = new BEMN.Forms.LedControl();
            this._diskr5 = new BEMN.Forms.LedControl();
            this.label35 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this._diskr12 = new BEMN.Forms.LedControl();
            this._diskr11 = new BEMN.Forms.LedControl();
            this._diskr4 = new BEMN.Forms.LedControl();
            this.label33 = new System.Windows.Forms.Label();
            this._diskr10 = new BEMN.Forms.LedControl();
            this._diskr9 = new BEMN.Forms.LedControl();
            this.label34 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._analogU = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._errorsDG = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acceptMessage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendMessage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.communicationQuality = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._errorsDG)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(392, 434);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox8);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(384, 408);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Ввод - вывод";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._writeDateTimeButt);
            this.groupBox1.Controls.Add(this._dateTimeNowButt);
            this.groupBox1.Controls.Add(this._astrStopCB);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._astrTimeClockTB);
            this.groupBox1.Controls.Add(this._astrDateClockTB);
            this.groupBox1.Location = new System.Drawing.Point(28, 290);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(329, 85);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Астрономическое время";
            // 
            // _writeDateTimeButt
            // 
            this._writeDateTimeButt.Enabled = false;
            this._writeDateTimeButt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._writeDateTimeButt.Location = new System.Drawing.Point(165, 56);
            this._writeDateTimeButt.Name = "_writeDateTimeButt";
            this._writeDateTimeButt.Size = new System.Drawing.Size(156, 23);
            this._writeDateTimeButt.TabIndex = 6;
            this._writeDateTimeButt.Text = "Установить";
            this._writeDateTimeButt.UseVisualStyleBackColor = true;
            this._writeDateTimeButt.Click += new System.EventHandler(this._writeDateTimeButt_Click);
            // 
            // _dateTimeNowButt
            // 
            this._dateTimeNowButt.Enabled = false;
            this._dateTimeNowButt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._dateTimeNowButt.Location = new System.Drawing.Point(165, 19);
            this._dateTimeNowButt.Name = "_dateTimeNowButt";
            this._dateTimeNowButt.Size = new System.Drawing.Size(156, 23);
            this._dateTimeNowButt.TabIndex = 5;
            this._dateTimeNowButt.Text = "Системные дата и время";
            this._dateTimeNowButt.UseVisualStyleBackColor = true;
            this._dateTimeNowButt.Click += new System.EventHandler(this._dateTimeNowButt_Click);
            // 
            // _astrStopCB
            // 
            this._astrStopCB.AutoSize = true;
            this._astrStopCB.Location = new System.Drawing.Point(12, 62);
            this._astrStopCB.Name = "_astrStopCB";
            this._astrStopCB.Size = new System.Drawing.Size(112, 17);
            this._astrStopCB.TabIndex = 4;
            this._astrStopCB.Text = "Изменить время";
            this._astrStopCB.UseVisualStyleBackColor = true;
            this._astrStopCB.CheckedChanged += new System.EventHandler(this._astrStopCB_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(68, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Время";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Дата";
            // 
            // _astrTimeClockTB
            // 
            this._astrTimeClockTB.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this._astrTimeClockTB.Location = new System.Drawing.Point(71, 34);
            this._astrTimeClockTB.Mask = "90:00:00";
            this._astrTimeClockTB.Name = "_astrTimeClockTB";
            this._astrTimeClockTB.Size = new System.Drawing.Size(51, 20);
            this._astrTimeClockTB.TabIndex = 1;
            this._astrTimeClockTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _astrDateClockTB
            // 
            this._astrDateClockTB.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this._astrDateClockTB.Location = new System.Drawing.Point(12, 34);
            this._astrDateClockTB.Mask = "00/00/00";
            this._astrDateClockTB.Name = "_astrDateClockTB";
            this._astrDateClockTB.Size = new System.Drawing.Size(49, 20);
            this._astrDateClockTB.TabIndex = 0;
            this._astrDateClockTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label7);
            this.groupBox8.Controls.Add(this._rele16);
            this.groupBox8.Controls.Add(this._rele15);
            this.groupBox8.Controls.Add(this.label6);
            this.groupBox8.Controls.Add(this._rele14);
            this.groupBox8.Controls.Add(this._rele13);
            this.groupBox8.Controls.Add(this._rele7);
            this.groupBox8.Controls.Add(this.label5);
            this.groupBox8.Controls.Add(this._rele12);
            this.groupBox8.Controls.Add(this._rele11);
            this.groupBox8.Controls.Add(this._rele6);
            this.groupBox8.Controls.Add(this.label4);
            this.groupBox8.Controls.Add(this._rele10);
            this.groupBox8.Controls.Add(this._rele9);
            this.groupBox8.Controls.Add(this._rele5);
            this.groupBox8.Controls.Add(this._rele1);
            this.groupBox8.Controls.Add(this._rele8);
            this.groupBox8.Controls.Add(this.label39);
            this.groupBox8.Controls.Add(this._rele4);
            this.groupBox8.Controls.Add(this._rele2);
            this.groupBox8.Controls.Add(this._rele3);
            this.groupBox8.Controls.Add(this.label37);
            this.groupBox8.Controls.Add(this.label38);
            this.groupBox8.Location = new System.Drawing.Point(6, 124);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(174, 53);
            this.groupBox8.TabIndex = 3;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Реле";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(152, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 13);
            this.label7.TabIndex = 142;
            this.label7.Text = "Р7";
            // 
            // _rele16
            // 
            this._rele16.BackColor = System.Drawing.Color.Transparent;
            this._rele16.Location = new System.Drawing.Point(180, 19);
            this._rele16.Name = "_rele16";
            this._rele16.Size = new System.Drawing.Size(13, 13);
            this._rele16.State = BEMN.Forms.LedState.Off;
            this._rele16.TabIndex = 138;
            this._rele16.Visible = false;
            // 
            // _rele15
            // 
            this._rele15.BackColor = System.Drawing.Color.Transparent;
            this._rele15.Location = new System.Drawing.Point(180, 19);
            this._rele15.Name = "_rele15";
            this._rele15.Size = new System.Drawing.Size(13, 13);
            this._rele15.State = BEMN.Forms.LedState.Off;
            this._rele15.TabIndex = 137;
            this._rele15.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(128, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 13);
            this.label6.TabIndex = 141;
            this.label6.Text = "Р6";
            // 
            // _rele14
            // 
            this._rele14.BackColor = System.Drawing.Color.Transparent;
            this._rele14.Location = new System.Drawing.Point(180, 19);
            this._rele14.Name = "_rele14";
            this._rele14.Size = new System.Drawing.Size(13, 13);
            this._rele14.State = BEMN.Forms.LedState.Off;
            this._rele14.TabIndex = 136;
            this._rele14.Visible = false;
            // 
            // _rele13
            // 
            this._rele13.BackColor = System.Drawing.Color.Transparent;
            this._rele13.Location = new System.Drawing.Point(180, 19);
            this._rele13.Name = "_rele13";
            this._rele13.Size = new System.Drawing.Size(13, 13);
            this._rele13.State = BEMN.Forms.LedState.Off;
            this._rele13.TabIndex = 135;
            this._rele13.Visible = false;
            // 
            // _rele7
            // 
            this._rele7.BackColor = System.Drawing.Color.Transparent;
            this._rele7.Location = new System.Drawing.Point(155, 19);
            this._rele7.Name = "_rele7";
            this._rele7.Size = new System.Drawing.Size(13, 13);
            this._rele7.State = BEMN.Forms.LedState.Off;
            this._rele7.TabIndex = 93;
            this._rele7.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._rele_MouseClick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(102, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 140;
            this.label5.Text = "Р5";
            // 
            // _rele12
            // 
            this._rele12.BackColor = System.Drawing.Color.Transparent;
            this._rele12.Location = new System.Drawing.Point(180, 19);
            this._rele12.Name = "_rele12";
            this._rele12.Size = new System.Drawing.Size(13, 13);
            this._rele12.State = BEMN.Forms.LedState.Off;
            this._rele12.TabIndex = 134;
            this._rele12.Visible = false;
            // 
            // _rele11
            // 
            this._rele11.BackColor = System.Drawing.Color.Transparent;
            this._rele11.Location = new System.Drawing.Point(180, 19);
            this._rele11.Name = "_rele11";
            this._rele11.Size = new System.Drawing.Size(13, 13);
            this._rele11.State = BEMN.Forms.LedState.Off;
            this._rele11.TabIndex = 93;
            this._rele11.Visible = false;
            // 
            // _rele6
            // 
            this._rele6.BackColor = System.Drawing.Color.Transparent;
            this._rele6.Location = new System.Drawing.Point(130, 19);
            this._rele6.Name = "_rele6";
            this._rele6.Size = new System.Drawing.Size(13, 13);
            this._rele6.State = BEMN.Forms.LedState.Off;
            this._rele6.TabIndex = 94;
            this._rele6.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._rele_MouseClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(77, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 139;
            this.label4.Text = "Р4";
            // 
            // _rele10
            // 
            this._rele10.BackColor = System.Drawing.Color.Transparent;
            this._rele10.Location = new System.Drawing.Point(180, 19);
            this._rele10.Name = "_rele10";
            this._rele10.Size = new System.Drawing.Size(13, 13);
            this._rele10.State = BEMN.Forms.LedState.Off;
            this._rele10.TabIndex = 133;
            this._rele10.Visible = false;
            // 
            // _rele9
            // 
            this._rele9.BackColor = System.Drawing.Color.Transparent;
            this._rele9.Location = new System.Drawing.Point(180, 19);
            this._rele9.Name = "_rele9";
            this._rele9.Size = new System.Drawing.Size(13, 13);
            this._rele9.State = BEMN.Forms.LedState.Off;
            this._rele9.TabIndex = 132;
            this._rele9.Visible = false;
            // 
            // _rele5
            // 
            this._rele5.BackColor = System.Drawing.Color.Transparent;
            this._rele5.Location = new System.Drawing.Point(105, 19);
            this._rele5.Name = "_rele5";
            this._rele5.Size = new System.Drawing.Size(13, 13);
            this._rele5.State = BEMN.Forms.LedState.Off;
            this._rele5.TabIndex = 95;
            this._rele5.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._rele_MouseClick);
            // 
            // _rele1
            // 
            this._rele1.BackColor = System.Drawing.Color.Transparent;
            this._rele1.Location = new System.Drawing.Point(6, 19);
            this._rele1.Name = "_rele1";
            this._rele1.Size = new System.Drawing.Size(13, 13);
            this._rele1.State = BEMN.Forms.LedState.Off;
            this._rele1.TabIndex = 97;
            this._rele1.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._rele_MouseClick);
            // 
            // _rele8
            // 
            this._rele8.BackColor = System.Drawing.Color.Transparent;
            this._rele8.Location = new System.Drawing.Point(180, 19);
            this._rele8.Name = "_rele8";
            this._rele8.Size = new System.Drawing.Size(13, 13);
            this._rele8.State = BEMN.Forms.LedState.Off;
            this._rele8.TabIndex = 92;
            this._rele8.Visible = false;
            this._rele8.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._rele_MouseClick);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(3, 35);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(20, 13);
            this.label39.TabIndex = 129;
            this.label39.Text = "Р1";
            // 
            // _rele4
            // 
            this._rele4.BackColor = System.Drawing.Color.Transparent;
            this._rele4.Location = new System.Drawing.Point(80, 19);
            this._rele4.Name = "_rele4";
            this._rele4.Size = new System.Drawing.Size(13, 13);
            this._rele4.State = BEMN.Forms.LedState.Off;
            this._rele4.TabIndex = 96;
            this._rele4.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._rele_MouseClick);
            // 
            // _rele2
            // 
            this._rele2.BackColor = System.Drawing.Color.Transparent;
            this._rele2.Location = new System.Drawing.Point(30, 19);
            this._rele2.Name = "_rele2";
            this._rele2.Size = new System.Drawing.Size(13, 13);
            this._rele2.State = BEMN.Forms.LedState.Off;
            this._rele2.TabIndex = 98;
            this._rele2.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._rele_MouseClick);
            // 
            // _rele3
            // 
            this._rele3.BackColor = System.Drawing.Color.Transparent;
            this._rele3.Location = new System.Drawing.Point(55, 19);
            this._rele3.Name = "_rele3";
            this._rele3.Size = new System.Drawing.Size(13, 13);
            this._rele3.State = BEMN.Forms.LedState.Off;
            this._rele3.TabIndex = 99;
            this._rele3.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._rele_MouseClick);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(52, 35);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(20, 13);
            this.label37.TabIndex = 131;
            this.label37.Text = "Р3";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(28, 35);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(20, 13);
            this.label38.TabIndex = 130;
            this.label38.Text = "Р2";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._diskr16);
            this.groupBox6.Controls.Add(this._diskr15);
            this.groupBox6.Controls.Add(this._diskr14);
            this.groupBox6.Controls.Add(this._diskr13);
            this.groupBox6.Controls.Add(this._diskr8);
            this.groupBox6.Controls.Add(this.label29);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Controls.Add(this._diskr1);
            this.groupBox6.Controls.Add(this._diskr3);
            this.groupBox6.Controls.Add(this._diskr7);
            this.groupBox6.Controls.Add(this.label36);
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this._diskr2);
            this.groupBox6.Controls.Add(this._diskr6);
            this.groupBox6.Controls.Add(this._diskr5);
            this.groupBox6.Controls.Add(this.label35);
            this.groupBox6.Controls.Add(this.label32);
            this.groupBox6.Controls.Add(this._diskr12);
            this.groupBox6.Controls.Add(this._diskr11);
            this.groupBox6.Controls.Add(this._diskr4);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Controls.Add(this._diskr10);
            this.groupBox6.Controls.Add(this._diskr9);
            this.groupBox6.Controls.Add(this.label34);
            this.groupBox6.Location = new System.Drawing.Point(6, 65);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(200, 53);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Дискреты";
            // 
            // _diskr16
            // 
            this._diskr16.BackColor = System.Drawing.Color.Transparent;
            this._diskr16.Location = new System.Drawing.Point(206, 19);
            this._diskr16.Name = "_diskr16";
            this._diskr16.Size = new System.Drawing.Size(13, 13);
            this._diskr16.State = BEMN.Forms.LedState.Off;
            this._diskr16.TabIndex = 131;
            this._diskr16.Visible = false;
            // 
            // _diskr15
            // 
            this._diskr15.BackColor = System.Drawing.Color.Transparent;
            this._diskr15.Location = new System.Drawing.Point(206, 19);
            this._diskr15.Name = "_diskr15";
            this._diskr15.Size = new System.Drawing.Size(13, 13);
            this._diskr15.State = BEMN.Forms.LedState.Off;
            this._diskr15.TabIndex = 130;
            this._diskr15.Visible = false;
            // 
            // _diskr14
            // 
            this._diskr14.BackColor = System.Drawing.Color.Transparent;
            this._diskr14.Location = new System.Drawing.Point(206, 19);
            this._diskr14.Name = "_diskr14";
            this._diskr14.Size = new System.Drawing.Size(13, 13);
            this._diskr14.State = BEMN.Forms.LedState.Off;
            this._diskr14.TabIndex = 129;
            this._diskr14.Visible = false;
            // 
            // _diskr13
            // 
            this._diskr13.BackColor = System.Drawing.Color.Transparent;
            this._diskr13.Location = new System.Drawing.Point(206, 19);
            this._diskr13.Name = "_diskr13";
            this._diskr13.Size = new System.Drawing.Size(13, 13);
            this._diskr13.State = BEMN.Forms.LedState.Off;
            this._diskr13.TabIndex = 128;
            this._diskr13.Visible = false;
            // 
            // _diskr8
            // 
            this._diskr8.BackColor = System.Drawing.Color.Transparent;
            this._diskr8.Location = new System.Drawing.Point(180, 19);
            this._diskr8.Name = "_diskr8";
            this._diskr8.Size = new System.Drawing.Size(13, 13);
            this._diskr8.State = BEMN.Forms.LedState.Off;
            this._diskr8.TabIndex = 91;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(178, 35);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(22, 13);
            this.label29.TabIndex = 123;
            this.label29.Text = "Д8";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(152, 35);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(22, 13);
            this.label30.TabIndex = 122;
            this.label30.Text = "Д7";
            // 
            // _diskr1
            // 
            this._diskr1.BackColor = System.Drawing.Color.Transparent;
            this._diskr1.Location = new System.Drawing.Point(6, 19);
            this._diskr1.Name = "_diskr1";
            this._diskr1.Size = new System.Drawing.Size(13, 13);
            this._diskr1.State = BEMN.Forms.LedState.Off;
            this._diskr1.TabIndex = 84;
            // 
            // _diskr3
            // 
            this._diskr3.BackColor = System.Drawing.Color.Transparent;
            this._diskr3.Location = new System.Drawing.Point(55, 19);
            this._diskr3.Name = "_diskr3";
            this._diskr3.Size = new System.Drawing.Size(13, 13);
            this._diskr3.State = BEMN.Forms.LedState.Off;
            this._diskr3.TabIndex = 86;
            // 
            // _diskr7
            // 
            this._diskr7.BackColor = System.Drawing.Color.Transparent;
            this._diskr7.Location = new System.Drawing.Point(155, 19);
            this._diskr7.Name = "_diskr7";
            this._diskr7.Size = new System.Drawing.Size(13, 13);
            this._diskr7.State = BEMN.Forms.LedState.Off;
            this._diskr7.TabIndex = 90;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(3, 35);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(22, 13);
            this.label36.TabIndex = 116;
            this.label36.Text = "Д1";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(127, 35);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(22, 13);
            this.label31.TabIndex = 121;
            this.label31.Text = "Д6";
            // 
            // _diskr2
            // 
            this._diskr2.BackColor = System.Drawing.Color.Transparent;
            this._diskr2.Location = new System.Drawing.Point(30, 19);
            this._diskr2.Name = "_diskr2";
            this._diskr2.Size = new System.Drawing.Size(13, 13);
            this._diskr2.State = BEMN.Forms.LedState.Off;
            this._diskr2.TabIndex = 85;
            // 
            // _diskr6
            // 
            this._diskr6.BackColor = System.Drawing.Color.Transparent;
            this._diskr6.Location = new System.Drawing.Point(130, 19);
            this._diskr6.Name = "_diskr6";
            this._diskr6.Size = new System.Drawing.Size(13, 13);
            this._diskr6.State = BEMN.Forms.LedState.Off;
            this._diskr6.TabIndex = 89;
            // 
            // _diskr5
            // 
            this._diskr5.BackColor = System.Drawing.Color.Transparent;
            this._diskr5.Location = new System.Drawing.Point(105, 19);
            this._diskr5.Name = "_diskr5";
            this._diskr5.Size = new System.Drawing.Size(13, 13);
            this._diskr5.State = BEMN.Forms.LedState.Off;
            this._diskr5.TabIndex = 88;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(26, 35);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(22, 13);
            this.label35.TabIndex = 117;
            this.label35.Text = "Д2";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(102, 35);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(22, 13);
            this.label32.TabIndex = 120;
            this.label32.Text = "Д5";
            // 
            // _diskr12
            // 
            this._diskr12.BackColor = System.Drawing.Color.Transparent;
            this._diskr12.Location = new System.Drawing.Point(206, 19);
            this._diskr12.Name = "_diskr12";
            this._diskr12.Size = new System.Drawing.Size(13, 13);
            this._diskr12.State = BEMN.Forms.LedState.Off;
            this._diskr12.TabIndex = 95;
            this._diskr12.Visible = false;
            // 
            // _diskr11
            // 
            this._diskr11.BackColor = System.Drawing.Color.Transparent;
            this._diskr11.Location = new System.Drawing.Point(206, 19);
            this._diskr11.Name = "_diskr11";
            this._diskr11.Size = new System.Drawing.Size(13, 13);
            this._diskr11.State = BEMN.Forms.LedState.Off;
            this._diskr11.TabIndex = 94;
            this._diskr11.Visible = false;
            // 
            // _diskr4
            // 
            this._diskr4.BackColor = System.Drawing.Color.Transparent;
            this._diskr4.Location = new System.Drawing.Point(80, 19);
            this._diskr4.Name = "_diskr4";
            this._diskr4.Size = new System.Drawing.Size(13, 13);
            this._diskr4.State = BEMN.Forms.LedState.Off;
            this._diskr4.TabIndex = 87;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(77, 35);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(22, 13);
            this.label33.TabIndex = 119;
            this.label33.Text = "Д4";
            // 
            // _diskr10
            // 
            this._diskr10.BackColor = System.Drawing.Color.Transparent;
            this._diskr10.Location = new System.Drawing.Point(206, 19);
            this._diskr10.Name = "_diskr10";
            this._diskr10.Size = new System.Drawing.Size(13, 13);
            this._diskr10.State = BEMN.Forms.LedState.Off;
            this._diskr10.TabIndex = 93;
            this._diskr10.Visible = false;
            // 
            // _diskr9
            // 
            this._diskr9.BackColor = System.Drawing.Color.Transparent;
            this._diskr9.Location = new System.Drawing.Point(206, 19);
            this._diskr9.Name = "_diskr9";
            this._diskr9.Size = new System.Drawing.Size(13, 13);
            this._diskr9.State = BEMN.Forms.LedState.Off;
            this._diskr9.TabIndex = 92;
            this._diskr9.Visible = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(52, 35);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(22, 13);
            this.label34.TabIndex = 118;
            this.label34.Text = "Д3";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._analogU);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Location = new System.Drawing.Point(6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(299, 53);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Аналоги";
            // 
            // _analogU
            // 
            this._analogU.Enabled = false;
            this._analogU.Location = new System.Drawing.Point(115, 17);
            this._analogU.Name = "_analogU";
            this._analogU.Size = new System.Drawing.Size(178, 20);
            this._analogU.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Текущее значение";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(384, 408);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Cтатистика";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._errorsDG);
            this.groupBox4.Location = new System.Drawing.Point(8, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(364, 397);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Регистры логической программы";
            // 
            // _errorsDG
            // 
            this._errorsDG.AllowUserToAddRows = false;
            this._errorsDG.AllowUserToDeleteRows = false;
            this._errorsDG.BackgroundColor = System.Drawing.Color.White;
            this._errorsDG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._errorsDG.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.acceptMessage,
            this.sendMessage,
            this.communicationQuality,
            this.status});
            this._errorsDG.Dock = System.Windows.Forms.DockStyle.Fill;
            this._errorsDG.Location = new System.Drawing.Point(3, 16);
            this._errorsDG.Name = "_errorsDG";
            this._errorsDG.RowHeadersVisible = false;
            this._errorsDG.Size = new System.Drawing.Size(358, 378);
            this._errorsDG.TabIndex = 16;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "№";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 30;
            // 
            // acceptMessage
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.acceptMessage.DefaultCellStyle = dataGridViewCellStyle2;
            this.acceptMessage.HeaderText = "Принято правильных сообщений";
            this.acceptMessage.Name = "acceptMessage";
            this.acceptMessage.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.acceptMessage.Width = 80;
            // 
            // sendMessage
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sendMessage.DefaultCellStyle = dataGridViewCellStyle3;
            this.sendMessage.HeaderText = "Послано сообщений";
            this.sendMessage.Name = "sendMessage";
            this.sendMessage.Width = 80;
            // 
            // communicationQuality
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.communicationQuality.DefaultCellStyle = dataGridViewCellStyle4;
            this.communicationQuality.HeaderText = "Процент по связи";
            this.communicationQuality.Name = "communicationQuality";
            this.communicationQuality.Width = 80;
            // 
            // status
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.status.DefaultCellStyle = dataGridViewCellStyle5;
            this.status.HeaderText = "Ошибки в реальном времени";
            this.status.Name = "status";
            this.status.Width = 180;
            // 
            // _contextMenu
            // 
            this._contextMenu.Name = "_contextMenu";
            this._contextMenu.Size = new System.Drawing.Size(153, 26);
            // 
            // Mlk10InOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 434);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Mlk10InOut";
            this.Text = "Ввод-вывод";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MLK_InOut_FormClosing);
            this.Load += new System.EventHandler(this.MLK_InOut_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._errorsDG)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView _errorsDG;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private BEMN.Forms.LedControl _diskr12;
        private BEMN.Forms.LedControl _diskr11;
        private BEMN.Forms.LedControl _diskr10;
        private BEMN.Forms.LedControl _diskr9;
        private BEMN.Forms.LedControl _diskr8;
        private BEMN.Forms.LedControl _diskr7;
        private BEMN.Forms.LedControl _diskr6;
        private BEMN.Forms.LedControl _diskr5;
        private BEMN.Forms.LedControl _diskr4;
        private BEMN.Forms.LedControl _diskr3;
        private BEMN.Forms.LedControl _diskr2;
        private BEMN.Forms.LedControl _diskr1;
        private System.Windows.Forms.GroupBox groupBox8;
        private BEMN.Forms.LedControl _rele3;
        private BEMN.Forms.LedControl _rele2;
        private BEMN.Forms.LedControl _rele1;
        private BEMN.Forms.LedControl _rele5;
        private BEMN.Forms.LedControl _rele6;
        private BEMN.Forms.LedControl _rele7;
        private BEMN.Forms.LedControl _rele8;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button _writeDateTimeButt;
        private System.Windows.Forms.Button _dateTimeNowButt;
        private System.Windows.Forms.CheckBox _astrStopCB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox _astrTimeClockTB;
        private System.Windows.Forms.MaskedTextBox _astrDateClockTB;
        private LedControl _diskr16;
        private LedControl _diskr15;
        private LedControl _diskr14;
        private LedControl _diskr13;
        private System.Windows.Forms.TextBox _analogU;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn acceptMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn communicationQuality;
        private System.Windows.Forms.DataGridViewTextBoxColumn status;
        private LedControl _rele4;
        private LedControl _rele16;
        private LedControl _rele15;
        private LedControl _rele14;
        private LedControl _rele13;
        private LedControl _rele12;
        private LedControl _rele11;
        private LedControl _rele10;
        private LedControl _rele9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ContextMenuStrip _contextMenu;


    }
}