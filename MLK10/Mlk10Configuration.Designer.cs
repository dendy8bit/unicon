﻿using BEMN.MLK10;

namespace BEMN.MLK10
{
    partial class Mlk10Configuration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this._readButton = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._writeAllButton = new System.Windows.Forms.Button();
            this._analogPage = new System.Windows.Forms.TabPage();
            this._descriptionGroup = new System.Windows.Forms.GroupBox();
            this._descriptionBox = new BEMN.Forms.RichTextBox.AdvRichTextBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._chanelCombo = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this._Uin = new System.Windows.Forms.MaskedTextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._readFromDevice = new System.Windows.Forms.Button();
            this._koeffpA = new System.Windows.Forms.MaskedTextBox();
            this._koeffA = new System.Windows.Forms.MaskedTextBox();
            this._koeffB = new System.Windows.Forms.MaskedTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this._processed = new System.Windows.Forms.TextBox();
            this._limit = new System.Windows.Forms.MaskedTextBox();
            this._currentADC = new System.Windows.Forms.MaskedTextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this._peripheryWrite = new System.Windows.Forms.Button();
            this._continue = new System.Windows.Forms.Button();
            this._statusStep = new System.Windows.Forms.Label();
            this._querysPage = new System.Windows.Forms.TabPage();
            this.label29 = new System.Windows.Forms.Label();
            this._querysCount = new System.Windows.Forms.MaskedTextBox();
            this.label23 = new System.Windows.Forms.Label();
            this._querysWrite = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._querys = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._commandCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._masterAddrCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._slaveAddrCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._numCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._RS485Page = new System.Windows.Forms.TabPage();
            this._rs485Write = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._rs485modeCheck = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this._rs485dataBitsCombo = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this._rs485AnswerTB = new System.Windows.Forms.MaskedTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this._rs485ToSendAfterTB = new System.Windows.Forms.MaskedTextBox();
            this._rs485ToSendBeforeTB = new System.Windows.Forms.MaskedTextBox();
            this._rs485ToSendTB = new System.Windows.Forms.MaskedTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this._rs485AddressTB = new System.Windows.Forms.MaskedTextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this._rs485doubleSpeedCombo = new System.Windows.Forms.ComboBox();
            this._rs485paritetCHETCombo = new System.Windows.Forms.ComboBox();
            this._rs485paritetYNCombo = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this._rs485stopBitsCombo = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this._rs485speedsCombo = new System.Windows.Forms.ComboBox();
            this._RS232Page = new System.Windows.Forms.TabPage();
            this._rs232Write = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._rs232dataBitsCombo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this._rs232ToSendAfterTB = new System.Windows.Forms.MaskedTextBox();
            this._rs232ToSendBeforeTB = new System.Windows.Forms.MaskedTextBox();
            this._rs232AddressTB = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this._rs232doubleSpeedCombo = new System.Windows.Forms.ComboBox();
            this._rs232paritetCHETCombo = new System.Windows.Forms.ComboBox();
            this._rs232paritetYNCombo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this._rs232stopBitsCombo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this._rs232speedsCombo = new System.Windows.Forms.ComboBox();
            this._configurationTabControl = new System.Windows.Forms.TabControl();
            this.statusStrip1.SuspendLayout();
            this._analogPage.SuspendLayout();
            this._descriptionGroup.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this._querysPage.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._querys)).BeginInit();
            this._RS485Page.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this._RS232Page.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this._configurationTabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // _readButton
            // 
            this._readButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._readButton.Location = new System.Drawing.Point(5, 344);
            this._readButton.Name = "_readButton";
            this._readButton.Size = new System.Drawing.Size(75, 23);
            this._readButton.TabIndex = 0;
            this._readButton.Text = "Прочитать";
            this._readButton.UseVisualStyleBackColor = true;
            this._readButton.Click += new System.EventHandler(this._readButton_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 370);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(429, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(118, 17);
            this._statusLabel.Text = "toolStripStatusLabel1";
            // 
            // _toolTip
            // 
            this._toolTip.ShowAlways = true;
            this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // _writeAllButton
            // 
            this._writeAllButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._writeAllButton.Location = new System.Drawing.Point(86, 344);
            this._writeAllButton.Name = "_writeAllButton";
            this._writeAllButton.Size = new System.Drawing.Size(88, 23);
            this._writeAllButton.TabIndex = 14;
            this._writeAllButton.Text = "Записать все";
            this._writeAllButton.UseVisualStyleBackColor = true;
            this._writeAllButton.Click += new System.EventHandler(this._writeAllButton_Click);
            // 
            // _analogPage
            // 
            this._analogPage.Controls.Add(this._descriptionGroup);
            this._analogPage.Controls.Add(this.groupBox12);
            this._analogPage.Controls.Add(this.groupBox6);
            this._analogPage.Controls.Add(this.groupBox13);
            this._analogPage.Controls.Add(this._peripheryWrite);
            this._analogPage.Controls.Add(this._continue);
            this._analogPage.Controls.Add(this._statusStep);
            this._analogPage.Location = new System.Drawing.Point(4, 22);
            this._analogPage.Name = "_analogPage";
            this._analogPage.Size = new System.Drawing.Size(423, 312);
            this._analogPage.TabIndex = 3;
            this._analogPage.Text = "Периферия";
            this._analogPage.UseVisualStyleBackColor = true;
            // 
            // _descriptionGroup
            // 
            this._descriptionGroup.Controls.Add(this._descriptionBox);
            this._descriptionGroup.Location = new System.Drawing.Point(188, 92);
            this._descriptionGroup.Name = "_descriptionGroup";
            this._descriptionGroup.Size = new System.Drawing.Size(224, 181);
            this._descriptionGroup.TabIndex = 24;
            this._descriptionGroup.TabStop = false;
            this._descriptionGroup.Text = "groupBox14";
            // 
            // _descriptionBox
            // 
            this._descriptionBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._descriptionBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._descriptionBox.Location = new System.Drawing.Point(3, 16);
            this._descriptionBox.Name = "_descriptionBox";
            this._descriptionBox.ReadOnly = true;
            this._descriptionBox.SelectionAlignment = BEMN.Forms.RichTextBox.TextAlign.Justify;
            this._descriptionBox.Size = new System.Drawing.Size(218, 162);
            this._descriptionBox.TabIndex = 11;
            this._descriptionBox.Text = "";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._chanelCombo);
            this.groupBox12.Controls.Add(this.label31);
            this.groupBox12.Controls.Add(this.label30);
            this.groupBox12.Controls.Add(this._Uin);
            this.groupBox12.Location = new System.Drawing.Point(7, 3);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(175, 69);
            this.groupBox12.TabIndex = 21;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Калибровка";
            // 
            // _chanelCombo
            // 
            this._chanelCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._chanelCombo.FormattingEnabled = true;
            this._chanelCombo.Items.AddRange(new object[] {
            "1"});
            this._chanelCombo.Location = new System.Drawing.Point(127, 13);
            this._chanelCombo.Name = "_chanelCombo";
            this._chanelCombo.Size = new System.Drawing.Size(42, 21);
            this._chanelCombo.TabIndex = 0;
            this._chanelCombo.SelectedIndexChanged += new System.EventHandler(this._chanelCombo_SelectedIndexChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(3, 16);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(30, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "АИО";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(3, 40);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(90, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = "Сигнал на входе";
            // 
            // _Uin
            // 
            this._Uin.Enabled = false;
            this._Uin.Location = new System.Drawing.Point(127, 36);
            this._Uin.Name = "_Uin";
            this._Uin.Size = new System.Drawing.Size(42, 20);
            this._Uin.TabIndex = 0;
            this._Uin.Tag = "300";
            this._Uin.Text = "0";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._readFromDevice);
            this.groupBox6.Controls.Add(this._koeffpA);
            this.groupBox6.Controls.Add(this._koeffA);
            this.groupBox6.Controls.Add(this._koeffB);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Location = new System.Drawing.Point(7, 78);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(175, 112);
            this.groupBox6.TabIndex = 18;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Коэффициенты";
            // 
            // _readFromDevice
            // 
            this._readFromDevice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._readFromDevice.Location = new System.Drawing.Point(6, 83);
            this._readFromDevice.Name = "_readFromDevice";
            this._readFromDevice.Size = new System.Drawing.Size(160, 23);
            this._readFromDevice.TabIndex = 19;
            this._readFromDevice.Text = "Прочитать из устройства";
            this._readFromDevice.UseVisualStyleBackColor = true;
            this._readFromDevice.Click += new System.EventHandler(this._readFromDevice_Click);
            // 
            // _koeffpA
            // 
            this._koeffpA.Enabled = false;
            this._koeffpA.Location = new System.Drawing.Point(49, 57);
            this._koeffpA.Name = "_koeffpA";
            this._koeffpA.Size = new System.Drawing.Size(81, 20);
            this._koeffpA.TabIndex = 7;
            this._koeffpA.Tag = "65535";
            this._koeffpA.Text = "0";
            // 
            // _koeffA
            // 
            this._koeffA.Enabled = false;
            this._koeffA.Location = new System.Drawing.Point(49, 38);
            this._koeffA.Name = "_koeffA";
            this._koeffA.Size = new System.Drawing.Size(81, 20);
            this._koeffA.TabIndex = 6;
            this._koeffA.Tag = "65535";
            this._koeffA.Text = "0";
            // 
            // _koeffB
            // 
            this._koeffB.Enabled = false;
            this._koeffB.Location = new System.Drawing.Point(49, 19);
            this._koeffB.Name = "_koeffB";
            this._koeffB.Size = new System.Drawing.Size(81, 20);
            this._koeffB.TabIndex = 5;
            this._koeffB.Tag = "65535";
            this._koeffB.Text = "0";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 60);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(20, 13);
            this.label26.TabIndex = 4;
            this.label26.Text = "pA";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 41);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(14, 13);
            this.label25.TabIndex = 3;
            this.label25.Text = "А";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(14, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "B";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this._processed);
            this.groupBox13.Controls.Add(this._limit);
            this.groupBox13.Controls.Add(this._currentADC);
            this.groupBox13.Controls.Add(this.label42);
            this.groupBox13.Controls.Add(this.label40);
            this.groupBox13.Controls.Add(this.label41);
            this.groupBox13.Location = new System.Drawing.Point(188, 3);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(224, 83);
            this.groupBox13.TabIndex = 22;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Измереное значение";
            // 
            // _processed
            // 
            this._processed.Enabled = false;
            this._processed.Location = new System.Drawing.Point(87, 33);
            this._processed.Name = "_processed";
            this._processed.Size = new System.Drawing.Size(100, 20);
            this._processed.TabIndex = 17;
            // 
            // _limit
            // 
            this._limit.Enabled = false;
            this._limit.Location = new System.Drawing.Point(87, 53);
            this._limit.Name = "_limit";
            this._limit.Size = new System.Drawing.Size(100, 20);
            this._limit.TabIndex = 1;
            this._limit.Tag = "300";
            this._limit.Text = "0";
            this._limit.TextChanged += new System.EventHandler(this._limit_TextChanged);
            // 
            // _currentADC
            // 
            this._currentADC.Enabled = false;
            this._currentADC.Location = new System.Drawing.Point(87, 13);
            this._currentADC.Name = "_currentADC";
            this._currentADC.Size = new System.Drawing.Size(100, 20);
            this._currentADC.TabIndex = 1;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(6, 16);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(52, 13);
            this.label42.TabIndex = 0;
            this.label42.Text = "Текущее";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(6, 36);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(75, 13);
            this.label40.TabIndex = 0;
            this.label40.Text = "Приведенное";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(6, 56);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(82, 13);
            this.label41.TabIndex = 0;
            this.label41.Text = "Предел шкалы";
            // 
            // _peripheryWrite
            // 
            this._peripheryWrite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._peripheryWrite.Location = new System.Drawing.Point(7, 286);
            this._peripheryWrite.Name = "_peripheryWrite";
            this._peripheryWrite.Size = new System.Drawing.Size(75, 23);
            this._peripheryWrite.TabIndex = 20;
            this._peripheryWrite.Text = "Записать";
            this._peripheryWrite.UseVisualStyleBackColor = true;
            this._peripheryWrite.Click += new System.EventHandler(this._peripheryWrite_Click);
            // 
            // _continue
            // 
            this._continue.Enabled = false;
            this._continue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._continue.Location = new System.Drawing.Point(324, 286);
            this._continue.Name = "_continue";
            this._continue.Size = new System.Drawing.Size(88, 23);
            this._continue.TabIndex = 19;
            this._continue.Text = "Продолжить";
            this._continue.UseVisualStyleBackColor = true;
            this._continue.Click += new System.EventHandler(this._continue_Click);
            // 
            // _statusStep
            // 
            this._statusStep.AutoSize = true;
            this._statusStep.Location = new System.Drawing.Point(88, 291);
            this._statusStep.Name = "_statusStep";
            this._statusStep.Size = new System.Drawing.Size(41, 13);
            this._statusStep.TabIndex = 23;
            this._statusStep.Text = "label43";
            // 
            // _querysPage
            // 
            this._querysPage.Controls.Add(this.label29);
            this._querysPage.Controls.Add(this._querysCount);
            this._querysPage.Controls.Add(this.label23);
            this._querysPage.Controls.Add(this._querysWrite);
            this._querysPage.Controls.Add(this.groupBox5);
            this._querysPage.Location = new System.Drawing.Point(4, 22);
            this._querysPage.Name = "_querysPage";
            this._querysPage.Size = new System.Drawing.Size(423, 312);
            this._querysPage.TabIndex = 2;
            this._querysPage.Text = "Запросы";
            this._querysPage.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label29.Location = new System.Drawing.Point(168, 14);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(40, 13);
            this.label29.TabIndex = 15;
            this.label29.Text = "[0 - 64]";
            // 
            // _querysCount
            // 
            this._querysCount.Location = new System.Drawing.Point(131, 11);
            this._querysCount.Name = "_querysCount";
            this._querysCount.Size = new System.Drawing.Size(37, 20);
            this._querysCount.TabIndex = 14;
            this._querysCount.Tag = "64";
            this._querysCount.Text = "0";
            this._querysCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._querysCount.KeyDown += new System.Windows.Forms.KeyEventHandler(this._querysCount_KeyDown);
            this._querysCount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._querysCount_KeyPress);
            this._querysCount.KeyUp += new System.Windows.Forms.KeyEventHandler(this._querysCount_KeyUp);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 14);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(117, 13);
            this.label23.TabIndex = 13;
            this.label23.Text = "Количество запросов";
            // 
            // _querysWrite
            // 
            this._querysWrite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._querysWrite.Location = new System.Drawing.Point(7, 283);
            this._querysWrite.Name = "_querysWrite";
            this._querysWrite.Size = new System.Drawing.Size(75, 23);
            this._querysWrite.TabIndex = 12;
            this._querysWrite.Text = "Записать";
            this._querysWrite.UseVisualStyleBackColor = true;
            this._querysWrite.Click += new System.EventHandler(this._querysWrite_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._querys);
            this.groupBox5.Location = new System.Drawing.Point(4, 34);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(413, 246);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Запросы";
            // 
            // _querys
            // 
            this._querys.AllowUserToAddRows = false;
            this._querys.AllowUserToDeleteRows = false;
            this._querys.BackgroundColor = System.Drawing.Color.White;
            this._querys.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._querys.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msgCol,
            this._commandCol,
            this._masterAddrCol,
            this._slaveAddrCol,
            this._numCol});
            this._querys.Location = new System.Drawing.Point(6, 14);
            this._querys.Name = "_querys";
            this._querys.RowHeadersVisible = false;
            this._querys.Size = new System.Drawing.Size(401, 226);
            this._querys.TabIndex = 13;
            this._querys.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this._querys_CellBeginEdit);
            this._querys.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._querys_CellEndEdit);
            // 
            // _indexCol
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._indexCol.DefaultCellStyle = dataGridViewCellStyle8;
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.Width = 30;
            // 
            // _timeCol
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._timeCol.DefaultCellStyle = dataGridViewCellStyle9;
            this._timeCol.HeaderText = "Фаза";
            this._timeCol.Name = "_timeCol";
            this._timeCol.Width = 40;
            // 
            // _msgCol
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msgCol.DefaultCellStyle = dataGridViewCellStyle10;
            this._msgCol.HeaderText = "Адрес";
            this._msgCol.Name = "_msgCol";
            this._msgCol.Width = 40;
            // 
            // _commandCol
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._commandCol.DefaultCellStyle = dataGridViewCellStyle11;
            this._commandCol.HeaderText = "Команда";
            this._commandCol.Name = "_commandCol";
            this._commandCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._commandCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // _masterAddrCol
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._masterAddrCol.DefaultCellStyle = dataGridViewCellStyle12;
            this._masterAddrCol.HeaderText = "Адрес ведомого";
            this._masterAddrCol.Name = "_masterAddrCol";
            this._masterAddrCol.Width = 57;
            // 
            // _slaveAddrCol
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._slaveAddrCol.DefaultCellStyle = dataGridViewCellStyle13;
            this._slaveAddrCol.HeaderText = "Адрес ведущего";
            this._slaveAddrCol.Name = "_slaveAddrCol";
            this._slaveAddrCol.Width = 57;
            // 
            // _numCol
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._numCol.DefaultCellStyle = dataGridViewCellStyle14;
            this._numCol.HeaderText = "Количество параметров";
            this._numCol.Name = "_numCol";
            this._numCol.Width = 70;
            // 
            // _RS485Page
            // 
            this._RS485Page.Controls.Add(this._rs485Write);
            this._RS485Page.Controls.Add(this.groupBox4);
            this._RS485Page.Location = new System.Drawing.Point(4, 22);
            this._RS485Page.Name = "_RS485Page";
            this._RS485Page.Padding = new System.Windows.Forms.Padding(3);
            this._RS485Page.Size = new System.Drawing.Size(423, 312);
            this._RS485Page.TabIndex = 1;
            this._RS485Page.Text = "RS485";
            this._RS485Page.UseVisualStyleBackColor = true;
            // 
            // _rs485Write
            // 
            this._rs485Write.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._rs485Write.Location = new System.Drawing.Point(7, 283);
            this._rs485Write.Name = "_rs485Write";
            this._rs485Write.Size = new System.Drawing.Size(75, 23);
            this._rs485Write.TabIndex = 11;
            this._rs485Write.Text = "Записать";
            this._rs485Write.UseVisualStyleBackColor = true;
            this._rs485Write.Click += new System.EventHandler(this._rs485Write_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._rs485modeCheck);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this._rs485dataBitsCombo);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this._rs485AnswerTB);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this._rs485ToSendAfterTB);
            this.groupBox4.Controls.Add(this._rs485ToSendBeforeTB);
            this.groupBox4.Controls.Add(this._rs485ToSendTB);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this._rs485AddressTB);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this._rs485doubleSpeedCombo);
            this.groupBox4.Controls.Add(this._rs485paritetCHETCombo);
            this.groupBox4.Controls.Add(this._rs485paritetYNCombo);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this._rs485stopBitsCombo);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this._rs485speedsCombo);
            this.groupBox4.Location = new System.Drawing.Point(7, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(405, 271);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Конфигурация RS485";
            // 
            // _rs485modeCheck
            // 
            this._rs485modeCheck.AutoSize = true;
            this._rs485modeCheck.Location = new System.Drawing.Point(330, 18);
            this._rs485modeCheck.Name = "_rs485modeCheck";
            this._rs485modeCheck.Size = new System.Drawing.Size(45, 17);
            this._rs485modeCheck.TabIndex = 34;
            this._rs485modeCheck.Text = "Нет";
            this._rs485modeCheck.UseVisualStyleBackColor = true;
            this._rs485modeCheck.CheckedChanged += new System.EventHandler(this._rs485modeCheck_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 13);
            this.label5.TabIndex = 33;
            this.label5.Text = "Количество бит данных";
            // 
            // _rs485dataBitsCombo
            // 
            this._rs485dataBitsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs485dataBitsCombo.FormattingEnabled = true;
            this._rs485dataBitsCombo.Items.AddRange(new object[] {
            "1 бит",
            "2 бита"});
            this._rs485dataBitsCombo.Location = new System.Drawing.Point(181, 64);
            this._rs485dataBitsCombo.Name = "_rs485dataBitsCombo";
            this._rs485dataBitsCombo.Size = new System.Drawing.Size(97, 21);
            this._rs485dataBitsCombo.TabIndex = 32;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 19);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(91, 13);
            this.label28.TabIndex = 31;
            this.label28.Text = "Режим главного";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 245);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(96, 13);
            this.label27.TabIndex = 29;
            this.label27.Text = "Ожидание ответа";
            // 
            // _rs485AnswerTB
            // 
            this._rs485AnswerTB.Enabled = false;
            this._rs485AnswerTB.Location = new System.Drawing.Point(278, 242);
            this._rs485AnswerTB.Name = "_rs485AnswerTB";
            this._rs485AnswerTB.Size = new System.Drawing.Size(97, 20);
            this._rs485AnswerTB.TabIndex = 28;
            this._rs485AnswerTB.Tag = "65535";
            this._rs485AnswerTB.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 205);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(163, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "Таймаут после выдачи данных";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 186);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(145, 13);
            this.label16.TabIndex = 24;
            this.label16.Text = "Таймаут до выдачи данных";
            // 
            // _rs485ToSendAfterTB
            // 
            this._rs485ToSendAfterTB.Location = new System.Drawing.Point(181, 202);
            this._rs485ToSendAfterTB.Name = "_rs485ToSendAfterTB";
            this._rs485ToSendAfterTB.Size = new System.Drawing.Size(97, 20);
            this._rs485ToSendAfterTB.TabIndex = 23;
            this._rs485ToSendAfterTB.Tag = "255";
            this._rs485ToSendAfterTB.Text = "0";
            // 
            // _rs485ToSendBeforeTB
            // 
            this._rs485ToSendBeforeTB.Location = new System.Drawing.Point(181, 183);
            this._rs485ToSendBeforeTB.Name = "_rs485ToSendBeforeTB";
            this._rs485ToSendBeforeTB.Size = new System.Drawing.Size(97, 20);
            this._rs485ToSendBeforeTB.TabIndex = 22;
            this._rs485ToSendBeforeTB.Tag = "255";
            this._rs485ToSendBeforeTB.Text = "0";
            // 
            // _rs485ToSendTB
            // 
            this._rs485ToSendTB.Enabled = false;
            this._rs485ToSendTB.Location = new System.Drawing.Point(278, 223);
            this._rs485ToSendTB.Name = "_rs485ToSendTB";
            this._rs485ToSendTB.Size = new System.Drawing.Size(97, 20);
            this._rs485ToSendTB.TabIndex = 21;
            this._rs485ToSendTB.Tag = "255";
            this._rs485ToSendTB.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 226);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(100, 13);
            this.label17.TabIndex = 20;
            this.label17.Text = "Таймаут передачи";
            // 
            // _rs485AddressTB
            // 
            this._rs485AddressTB.Location = new System.Drawing.Point(181, 164);
            this._rs485AddressTB.Name = "_rs485AddressTB";
            this._rs485AddressTB.Size = new System.Drawing.Size(97, 20);
            this._rs485AddressTB.TabIndex = 19;
            this._rs485AddressTB.Tag = "255";
            this._rs485AddressTB.Text = "1";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 167);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(98, 13);
            this.label18.TabIndex = 18;
            this.label18.Text = "Адрес устройства";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 147);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(107, 13);
            this.label19.TabIndex = 17;
            this.label19.Text = "Удвоение скорости";
            // 
            // _rs485doubleSpeedCombo
            // 
            this._rs485doubleSpeedCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs485doubleSpeedCombo.FormattingEnabled = true;
            this._rs485doubleSpeedCombo.Items.AddRange(new object[] {
            "Без удвоения",
            "С удвоением"});
            this._rs485doubleSpeedCombo.Location = new System.Drawing.Point(181, 144);
            this._rs485doubleSpeedCombo.Name = "_rs485doubleSpeedCombo";
            this._rs485doubleSpeedCombo.Size = new System.Drawing.Size(97, 21);
            this._rs485doubleSpeedCombo.TabIndex = 16;
            // 
            // _rs485paritetCHETCombo
            // 
            this._rs485paritetCHETCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs485paritetCHETCombo.FormattingEnabled = true;
            this._rs485paritetCHETCombo.Items.AddRange(new object[] {
            "Четный",
            "Нечетный"});
            this._rs485paritetCHETCombo.Location = new System.Drawing.Point(181, 124);
            this._rs485paritetCHETCombo.Name = "_rs485paritetCHETCombo";
            this._rs485paritetCHETCombo.Size = new System.Drawing.Size(97, 21);
            this._rs485paritetCHETCombo.TabIndex = 15;
            // 
            // _rs485paritetYNCombo
            // 
            this._rs485paritetYNCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs485paritetYNCombo.FormattingEnabled = true;
            this._rs485paritetYNCombo.Items.AddRange(new object[] {
            "Нет",
            "Есть"});
            this._rs485paritetYNCombo.Location = new System.Drawing.Point(181, 104);
            this._rs485paritetYNCombo.Name = "_rs485paritetYNCombo";
            this._rs485paritetYNCombo.Size = new System.Drawing.Size(97, 21);
            this._rs485paritetYNCombo.TabIndex = 14;
            this._rs485paritetYNCombo.SelectedIndexChanged += new System.EventHandler(this._rs485paritetYNCombo_SelectedIndexChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 107);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(49, 13);
            this.label20.TabIndex = 13;
            this.label20.Text = "Паритет";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 87);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(112, 13);
            this.label21.TabIndex = 12;
            this.label21.Text = "Количество стоп бит";
            // 
            // _rs485stopBitsCombo
            // 
            this._rs485stopBitsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs485stopBitsCombo.FormattingEnabled = true;
            this._rs485stopBitsCombo.Items.AddRange(new object[] {
            "1 бит",
            "2 бита"});
            this._rs485stopBitsCombo.Location = new System.Drawing.Point(181, 84);
            this._rs485stopBitsCombo.Name = "_rs485stopBitsCombo";
            this._rs485stopBitsCombo.Size = new System.Drawing.Size(97, 21);
            this._rs485stopBitsCombo.TabIndex = 11;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 47);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(105, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "Скорость передачи";
            // 
            // _rs485speedsCombo
            // 
            this._rs485speedsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs485speedsCombo.FormattingEnabled = true;
            this._rs485speedsCombo.Items.AddRange(new object[] {
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "76800",
            "900",
            "1800",
            "3600",
            "7200",
            "14400",
            "28800",
            "57600",
            "115200"});
            this._rs485speedsCombo.Location = new System.Drawing.Point(181, 44);
            this._rs485speedsCombo.Name = "_rs485speedsCombo";
            this._rs485speedsCombo.Size = new System.Drawing.Size(97, 21);
            this._rs485speedsCombo.TabIndex = 9;
            // 
            // _RS232Page
            // 
            this._RS232Page.Controls.Add(this._rs232Write);
            this._RS232Page.Controls.Add(this.groupBox3);
            this._RS232Page.Location = new System.Drawing.Point(4, 22);
            this._RS232Page.Name = "_RS232Page";
            this._RS232Page.Padding = new System.Windows.Forms.Padding(3);
            this._RS232Page.Size = new System.Drawing.Size(423, 312);
            this._RS232Page.TabIndex = 0;
            this._RS232Page.Text = "RS232";
            this._RS232Page.UseVisualStyleBackColor = true;
            // 
            // _rs232Write
            // 
            this._rs232Write.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._rs232Write.Location = new System.Drawing.Point(7, 283);
            this._rs232Write.Name = "_rs232Write";
            this._rs232Write.Size = new System.Drawing.Size(75, 23);
            this._rs232Write.TabIndex = 10;
            this._rs232Write.Text = "Записать";
            this._rs232Write.UseVisualStyleBackColor = true;
            this._rs232Write.Click += new System.EventHandler(this._rs232Write_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._rs232dataBitsCombo);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this._rs232ToSendAfterTB);
            this.groupBox3.Controls.Add(this._rs232ToSendBeforeTB);
            this.groupBox3.Controls.Add(this._rs232AddressTB);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this._rs232doubleSpeedCombo);
            this.groupBox3.Controls.Add(this._rs232paritetCHETCombo);
            this.groupBox3.Controls.Add(this._rs232paritetYNCombo);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this._rs232stopBitsCombo);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this._rs232speedsCombo);
            this.groupBox3.Location = new System.Drawing.Point(7, 28);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(287, 210);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Конфигурация RS232";
            // 
            // _rs232dataBitsCombo
            // 
            this._rs232dataBitsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs232dataBitsCombo.FormattingEnabled = true;
            this._rs232dataBitsCombo.Items.AddRange(new object[] {
            "1 бит",
            "2 бита"});
            this._rs232dataBitsCombo.Location = new System.Drawing.Point(181, 42);
            this._rs232dataBitsCombo.Name = "_rs232dataBitsCombo";
            this._rs232dataBitsCombo.Size = new System.Drawing.Size(97, 21);
            this._rs232dataBitsCombo.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Количество бит данных";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 183);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(163, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "Таймаут после выдачи данных";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 164);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(145, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Таймаут до выдачи данных";
            // 
            // _rs232ToSendAfterTB
            // 
            this._rs232ToSendAfterTB.Location = new System.Drawing.Point(181, 180);
            this._rs232ToSendAfterTB.Name = "_rs232ToSendAfterTB";
            this._rs232ToSendAfterTB.Size = new System.Drawing.Size(97, 20);
            this._rs232ToSendAfterTB.TabIndex = 23;
            this._rs232ToSendAfterTB.Tag = "255";
            this._rs232ToSendAfterTB.Text = "0";
            // 
            // _rs232ToSendBeforeTB
            // 
            this._rs232ToSendBeforeTB.Location = new System.Drawing.Point(181, 161);
            this._rs232ToSendBeforeTB.Name = "_rs232ToSendBeforeTB";
            this._rs232ToSendBeforeTB.Size = new System.Drawing.Size(97, 20);
            this._rs232ToSendBeforeTB.TabIndex = 22;
            this._rs232ToSendBeforeTB.Tag = "255";
            this._rs232ToSendBeforeTB.Text = "0";
            // 
            // _rs232AddressTB
            // 
            this._rs232AddressTB.Location = new System.Drawing.Point(181, 142);
            this._rs232AddressTB.Name = "_rs232AddressTB";
            this._rs232AddressTB.Size = new System.Drawing.Size(97, 20);
            this._rs232AddressTB.TabIndex = 19;
            this._rs232AddressTB.Tag = "255";
            this._rs232AddressTB.Text = "1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 145);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Адрес устройства";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 125);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Удвоение скорости";
            // 
            // _rs232doubleSpeedCombo
            // 
            this._rs232doubleSpeedCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs232doubleSpeedCombo.FormattingEnabled = true;
            this._rs232doubleSpeedCombo.Items.AddRange(new object[] {
            "Без удвоения",
            "С удвоением"});
            this._rs232doubleSpeedCombo.Location = new System.Drawing.Point(181, 122);
            this._rs232doubleSpeedCombo.Name = "_rs232doubleSpeedCombo";
            this._rs232doubleSpeedCombo.Size = new System.Drawing.Size(97, 21);
            this._rs232doubleSpeedCombo.TabIndex = 16;
            // 
            // _rs232paritetCHETCombo
            // 
            this._rs232paritetCHETCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs232paritetCHETCombo.FormattingEnabled = true;
            this._rs232paritetCHETCombo.Items.AddRange(new object[] {
            "Четный",
            "Нечетный"});
            this._rs232paritetCHETCombo.Location = new System.Drawing.Point(181, 102);
            this._rs232paritetCHETCombo.Name = "_rs232paritetCHETCombo";
            this._rs232paritetCHETCombo.Size = new System.Drawing.Size(97, 21);
            this._rs232paritetCHETCombo.TabIndex = 15;
            // 
            // _rs232paritetYNCombo
            // 
            this._rs232paritetYNCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs232paritetYNCombo.FormattingEnabled = true;
            this._rs232paritetYNCombo.Items.AddRange(new object[] {
            "Нет",
            "Есть"});
            this._rs232paritetYNCombo.Location = new System.Drawing.Point(181, 82);
            this._rs232paritetYNCombo.Name = "_rs232paritetYNCombo";
            this._rs232paritetYNCombo.Size = new System.Drawing.Size(97, 21);
            this._rs232paritetYNCombo.TabIndex = 14;
            this._rs232paritetYNCombo.SelectedIndexChanged += new System.EventHandler(this._rs232paritetYNCombo_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 85);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Паритет";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Количество стоп бит";
            // 
            // _rs232stopBitsCombo
            // 
            this._rs232stopBitsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs232stopBitsCombo.FormattingEnabled = true;
            this._rs232stopBitsCombo.Items.AddRange(new object[] {
            "1 бит",
            "2 бита"});
            this._rs232stopBitsCombo.Location = new System.Drawing.Point(181, 62);
            this._rs232stopBitsCombo.Name = "_rs232stopBitsCombo";
            this._rs232stopBitsCombo.Size = new System.Drawing.Size(97, 21);
            this._rs232stopBitsCombo.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Скорость передачи";
            // 
            // _rs232speedsCombo
            // 
            this._rs232speedsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs232speedsCombo.FormattingEnabled = true;
            this._rs232speedsCombo.Items.AddRange(new object[] {
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "76800",
            "900",
            "1800",
            "3600",
            "7200",
            "14400",
            "28800",
            "57600",
            "115200"});
            this._rs232speedsCombo.Location = new System.Drawing.Point(181, 22);
            this._rs232speedsCombo.Name = "_rs232speedsCombo";
            this._rs232speedsCombo.Size = new System.Drawing.Size(97, 21);
            this._rs232speedsCombo.TabIndex = 9;
            // 
            // _configurationTabControl
            // 
            this._configurationTabControl.Controls.Add(this._RS232Page);
            this._configurationTabControl.Controls.Add(this._RS485Page);
            this._configurationTabControl.Controls.Add(this._querysPage);
            this._configurationTabControl.Controls.Add(this._analogPage);
            this._configurationTabControl.Location = new System.Drawing.Point(1, 0);
            this._configurationTabControl.Name = "_configurationTabControl";
            this._configurationTabControl.SelectedIndex = 0;
            this._configurationTabControl.Size = new System.Drawing.Size(431, 338);
            this._configurationTabControl.TabIndex = 1;
            this._configurationTabControl.SelectedIndexChanged += new System.EventHandler(this._configurationTabControl_SelectedIndexChanged);
            // 
            // Mlk10Configuration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 392);
            this.Controls.Add(this._writeAllButton);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._configurationTabControl);
            this.Controls.Add(this._readButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Mlk10Configuration";
            this.Text = "Конфигурация";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MLK10_Configuration_FormClosing);
            this.Load += new System.EventHandler(this.MLK10_Configuration_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this._analogPage.ResumeLayout(false);
            this._analogPage.PerformLayout();
            this._descriptionGroup.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this._querysPage.ResumeLayout(false);
            this._querysPage.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._querys)).EndInit();
            this._RS485Page.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this._RS232Page.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this._configurationTabControl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _readButton;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.Button _writeAllButton;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.TabPage _analogPage;
        private System.Windows.Forms.TabPage _querysPage;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.MaskedTextBox _querysCount;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button _querysWrite;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView _querys;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _commandCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _masterAddrCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _slaveAddrCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _numCol;
        private System.Windows.Forms.TabPage _RS485Page;
        private System.Windows.Forms.Button _rs485Write;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox _rs485modeCheck;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox _rs485dataBitsCombo;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.MaskedTextBox _rs485AnswerTB;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.MaskedTextBox _rs485ToSendAfterTB;
        private System.Windows.Forms.MaskedTextBox _rs485ToSendBeforeTB;
        private System.Windows.Forms.MaskedTextBox _rs485ToSendTB;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.MaskedTextBox _rs485AddressTB;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox _rs485doubleSpeedCombo;
        private System.Windows.Forms.ComboBox _rs485paritetCHETCombo;
        private System.Windows.Forms.ComboBox _rs485paritetYNCombo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox _rs485stopBitsCombo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox _rs485speedsCombo;
        private System.Windows.Forms.TabPage _RS232Page;
        private System.Windows.Forms.Button _rs232Write;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox _rs232dataBitsCombo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox _rs232ToSendAfterTB;
        private System.Windows.Forms.MaskedTextBox _rs232ToSendBeforeTB;
        private System.Windows.Forms.MaskedTextBox _rs232AddressTB;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox _rs232doubleSpeedCombo;
        private System.Windows.Forms.ComboBox _rs232paritetCHETCombo;
        private System.Windows.Forms.ComboBox _rs232paritetYNCombo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox _rs232stopBitsCombo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox _rs232speedsCombo;
        private System.Windows.Forms.TabControl _configurationTabControl;
        private System.Windows.Forms.GroupBox _descriptionGroup;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.ComboBox _chanelCombo;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.MaskedTextBox _Uin;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button _readFromDevice;
        private System.Windows.Forms.MaskedTextBox _koeffpA;
        private System.Windows.Forms.MaskedTextBox _koeffA;
        private System.Windows.Forms.MaskedTextBox _koeffB;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox _processed;
        private System.Windows.Forms.MaskedTextBox _limit;
        private System.Windows.Forms.MaskedTextBox _currentADC;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Button _peripheryWrite;
        private System.Windows.Forms.Button _continue;
        private System.Windows.Forms.Label _statusStep;
        private Forms.RichTextBox.AdvRichTextBox _descriptionBox;
    }
}