﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using BEMN.Forms.Queries;
using BEMN.MLK10.Properties;
using BEMN.MLK10.Structures;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.Framework;

namespace BEMN.MLK10
{
    public class MLK10 : Device, IDeviceView
    {
        #region Private fields
        private MemoryEntity<UIORam> _uioRam;
        private MemoryEntity<Clock> _clock;
        private MemoryEntity<SysErr> _sysErr;
        private MemoryEntity<MemConfigRS232> _configRS232;
        private MemoryEntity<MemConfigRS485> _configRS485;
        private MemoryEntity<MemConfigRequest> _configRequest;
        private MemoryEntity<AnalogExtConfig> _periphery;
        private MemoryEntity<SomeStruct> _querys;
        #endregion

        #region Constructors
        public MLK10()
        {
            HaveVersion = true;
        }

        public MLK10(Modbus mb)
        {
            HaveVersion = true;
            MB = mb;
            this.InitStructures();
        }

        [XmlIgnore]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        public override Modbus MB
        {
            get { return mb; }
            set
            {
                if (value == null) return;
                mb = value;
                mb.CompleteExchange += mb_CompleteExchange;
            }
        }

        private void InitStructures()
        {
            this._uioRam = new MemoryEntity<UIORam>("RAM ввода-вывода", this, 0x01F0);
            this._clock = new MemoryEntity<Clock>("Часы реального времени", this, 0x0260);
            this._sysErr = new MemoryEntity<SysErr>("Системные ошибки", this, 0x0270);

            this._configRS232 = new MemoryEntity<MemConfigRS232>("RS-232", this, 0x1000);
            this._configRS485 = new MemoryEntity<MemConfigRS485>("RS-485", this, 0x1008);
            this._configRequest = new MemoryEntity<MemConfigRequest>("Запросы к модулям", this, 0x1010);
            this._periphery = new MemoryEntity<AnalogExtConfig>("Конфигурация аналогового канала", this, 0x1500);

            this._querys = new MemoryEntity<SomeStruct>("Обмены", this, 0x0000);
        }
        #endregion

        #region IDeviceViewMembers
        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(MLK10); }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mlk; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "МЛК10"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }
        #endregion

        #region Property

        public MemoryEntity<UIORam> UioRam
        {
            get { return _uioRam; }
        }

        public MemoryEntity<Clock> Clock
        {
            get { return _clock; }
        }

        public MemoryEntity<SysErr> SysErr
        {
            get { return _sysErr; }
        }

        public MemoryEntity<MemConfigRS232> ConfigRs232
        {
            get { return _configRS232; }
        }

        public MemoryEntity<MemConfigRS485> ConfigRs485
        {
            get { return _configRS485; }
        }

        public MemoryEntity<MemConfigRequest> ConfigRequest
        {
            get { return _configRequest; }
        }

        public MemoryEntity<AnalogExtConfig> Periphery
        {
            get { return _periphery; }
        }
        #endregion

        #region Read version
        public override void LoadVersion(object deviceObj)
        {
            System.Threading.Thread.Sleep(500);
            LoadSlot(DeviceNumber, new slot((ushort)0x1F00, (ushort)0x1F18), "version" + DeviceNumber, this);
        }
        #endregion

        #region Function
        /// <summary>
        /// Получает массив слов из массива слотов
        /// </summary>
        /// <returns></returns>
        public ushort[] GetValues(List<slot> slots)
        {
            List<ushort> ret = new List<ushort>();
            foreach (var slot in slots)
            {
                ret.AddRange(slot.Value);
            }
            return ret.ToArray();
        }
        /// <summary>
        /// Возвращает структуру журнала для считывания с указанного адреа и нужной длины
        /// </summary>
        /// <param name="name">Название считываемой структуры</param>
        /// <param name="length">Количество записей в журнале</param>
        /// <param name="startAddress">Начальный адрес</param>
        /// <returns></returns>
        public MemoryEntity<Journal> GetJournal(string name, int length, ushort startAddress)
        {
            Journal journalStruct = new Journal(length);
            journalStruct.InitStruct(new byte[Marshal.SizeOf(typeof(JournalRep)) * length]);

            MemoryEntity<Journal> retJournal = new MemoryEntity<Journal>(name, this, startAddress);
            retJournal.Slots = QueriesForm.SetSlots(journalStruct.GetValues(), startAddress);
            retJournal.Value = journalStruct;
            retJournal.Values = journalStruct.GetValues();
            return retJournal;
        }
        #endregion

    }
}
