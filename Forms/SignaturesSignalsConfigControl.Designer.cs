﻿namespace BEMN.Forms
{
    partial class SignaturesSignalsConfigControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this._writeSignalsInDeviceButton = new System.Windows.Forms.Button();
            this._signaturesSignalsDGV = new System.Windows.Forms.DataGridView();
            this._acceptButton = new System.Windows.Forms.Button();
            this._baseSignalsColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signalsInDeviceColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._changeSignalsColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this._signaturesSignalsDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // _writeSignalsInDeviceButton
            // 
            this._writeSignalsInDeviceButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._writeSignalsInDeviceButton.Location = new System.Drawing.Point(0, 528);
            this._writeSignalsInDeviceButton.Name = "_writeSignalsInDeviceButton";
            this._writeSignalsInDeviceButton.Size = new System.Drawing.Size(324, 23);
            this._writeSignalsInDeviceButton.TabIndex = 5;
            this._writeSignalsInDeviceButton.Text = "Восстановить базовые сигналы";
            this._writeSignalsInDeviceButton.UseVisualStyleBackColor = true;
            this._writeSignalsInDeviceButton.Click += new System.EventHandler(this._writeSignalsInDeviceButton_Click);
            // 
            // _signaturesSignalsDGV
            // 
            this._signaturesSignalsDGV.AllowUserToAddRows = false;
            this._signaturesSignalsDGV.AllowUserToDeleteRows = false;
            this._signaturesSignalsDGV.AllowUserToResizeColumns = false;
            this._signaturesSignalsDGV.AllowUserToResizeRows = false;
            this._signaturesSignalsDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._signaturesSignalsDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._signaturesSignalsDGV.BackgroundColor = System.Drawing.Color.White;
            this._signaturesSignalsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._signaturesSignalsDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._baseSignalsColumn,
            this._signalsInDeviceColumn,
            this._changeSignalsColumn});
            this._signaturesSignalsDGV.Dock = System.Windows.Forms.DockStyle.Top;
            this._signaturesSignalsDGV.Location = new System.Drawing.Point(0, 0);
            this._signaturesSignalsDGV.Name = "_signaturesSignalsDGV";
            this._signaturesSignalsDGV.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._signaturesSignalsDGV.RowHeadersVisible = false;
            this._signaturesSignalsDGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._signaturesSignalsDGV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._signaturesSignalsDGV.ShowCellErrors = false;
            this._signaturesSignalsDGV.ShowRowErrors = false;
            this._signaturesSignalsDGV.Size = new System.Drawing.Size(324, 506);
            this._signaturesSignalsDGV.TabIndex = 3;
            this._signaturesSignalsDGV.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._signaturesSignalsDGV_DataError);
            // 
            // _acceptButton
            // 
            this._acceptButton.Dock = System.Windows.Forms.DockStyle.Top;
            this._acceptButton.Location = new System.Drawing.Point(0, 506);
            this._acceptButton.Name = "_acceptButton";
            this._acceptButton.Size = new System.Drawing.Size(324, 23);
            this._acceptButton.TabIndex = 6;
            this._acceptButton.Text = "Применить сигналы";
            this._acceptButton.UseVisualStyleBackColor = true;
            this._acceptButton.Click += new System.EventHandler(this._acceptButton_Click);
            // 
            // _baseSignalsColumn
            // 
            this._baseSignalsColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.NullValue = " ";
            this._baseSignalsColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this._baseSignalsColumn.HeaderText = "Базовые сигналы";
            this._baseSignalsColumn.Name = "_baseSignalsColumn";
            this._baseSignalsColumn.ReadOnly = true;
            this._baseSignalsColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._baseSignalsColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._baseSignalsColumn.Width = 94;
            // 
            // _signalsInDeviceColumn
            // 
            this._signalsInDeviceColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Gainsboro;
            this._signalsInDeviceColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this._signalsInDeviceColumn.HeaderText = "Текущий список на устройстве";
            this._signalsInDeviceColumn.Name = "_signalsInDeviceColumn";
            this._signalsInDeviceColumn.ReadOnly = true;
            this._signalsInDeviceColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._signalsInDeviceColumn.Width = 104;
            // 
            // _changeSignalsColumn
            // 
            this._changeSignalsColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle3.NullValue = " ";
            this._changeSignalsColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this._changeSignalsColumn.HeaderText = "Сигналы для записи в устройство";
            this._changeSignalsColumn.MaxInputLength = 9;
            this._changeSignalsColumn.Name = "_changeSignalsColumn";
            this._changeSignalsColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // SignaturesSignalsConfigControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._acceptButton);
            this.Controls.Add(this._writeSignalsInDeviceButton);
            this.Controls.Add(this._signaturesSignalsDGV);
            this.Name = "SignaturesSignalsConfigControl";
            this.Size = new System.Drawing.Size(324, 551);
            ((System.ComponentModel.ISupportInitialize)(this._signaturesSignalsDGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _writeSignalsInDeviceButton;
        private System.Windows.Forms.DataGridView _signaturesSignalsDGV;
        private System.Windows.Forms.Button _acceptButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn _baseSignalsColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _signalsInDeviceColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _changeSignalsColumn;
    }
}
