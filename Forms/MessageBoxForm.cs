﻿using System;
using System.Windows.Forms;

namespace BEMN.Forms
{
    public partial class MessageBoxForm : Form
    {
        public delegate void ButtonClick(DialogResult result);
        public delegate void UpProgressBar();
        public event ButtonClick OkButtonClick;
        private int _exitClick;
        /// <summary>
        /// Указывает, была ли ошибка в операциях чтения/записи
        /// </summary>
        public bool Fail { get; set; }

        public string Caption
        {
            get { return Text; }
            set { Text = value; }
        }

        private bool _okClicked;
        
        public MessageBoxForm()
        {
            this.InitializeComponent();
            this.Fail = false;
            this._okClicked = false;
            this._exitClick = 0;
        }

        public bool OkBtnVisibility 
        {
            get { return this._finishOK.Visible; }
            set { this._finishOK.Visible = value; }
        }
        
        private void _finishOK_Click(object sender, EventArgs e)
        {
            if (!this.Fail && this.OkButtonClick != null)
            {
                this.OkButtonClick(DialogResult.OK);
            }
            this._okClicked = true;
            this.Fail = false;
            Close();
        }

        public void ShowDialog(string message)
        {
            SetCursorStyle(true, false);
            this._textLabel.Text = message;
            this._finishOK.Enabled = false;
            this.ShowDialog();
			this.OkButtonClick?.Invoke(DialogResult.Retry);
		}

	    public void ShowDialogOffline(string message)
	    {
		    this._textLabel.Text = message;
		    ShowDialog();
		}

        public void IncrementProgressBar(int count)
	    {
			progressBar1.Increment(count);
	    }

	    public void FinishProgressBar()
	    {
		    progressBar1.Value = 100;
	    }

		public void ShowResultMessage(string message)
        {
            SetCursorStyle(false, true);

            this._textLabel.Text = message;
            SetProgressBarStyle(ProgressBarStyle.Blocks);
            this.progressBar1.Value = this.progressBar1.Maximum;
            this._finishOK.Visible = true;
            this._finishOK.Enabled = true;
        }

        private void SetCursorStyle(bool useWaitCursor, bool isEnd)
        {
            this.UseWaitCursor = useWaitCursor;
            Cursor.Current = isEnd ? Cursors.Default : Cursors.WaitCursor;
        }

        private void MessageBoxForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !this._okClicked;
            if (this._okClicked) return;

            e.Cancel = this._exitClick < 3;
            this._exitClick++;
        }
        public void SetMaxProgramBar(int pbProgram)
        {
            this.progressBar1.Maximum = pbProgram;
            this.progressBar1.Value = 0;
        }

        public void ShowMessage(string message)
        {
            this._textLabel.Text = message;
        }
        public void ProgramExchangeOk()
        {
            this.progressBar1.PerformStep();
        }

        public void SetProgressBarStyle(ProgressBarStyle style)
        {
            this.progressBar1.Style = style;
        }
        /// <summary>
        /// При успешном выполнеии запрошенной операции закрываем окно
        /// </summary>
        public void OperationComplete()
        {
            this._okClicked = true;
            Close();
        }
    }
}
