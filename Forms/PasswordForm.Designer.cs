﻿namespace BEMN.Forms
{
    partial class PasswordForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._confirmPasswordButton = new System.Windows.Forms.Button();
            this.passwordTextBox = new System.Windows.Forms.MaskedTextBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // _confirmPasswordButton
            // 
            this._confirmPasswordButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._confirmPasswordButton.Enabled = false;
            this._confirmPasswordButton.Location = new System.Drawing.Point(12, 38);
            this._confirmPasswordButton.Name = "_confirmPasswordButton";
            this._confirmPasswordButton.Size = new System.Drawing.Size(75, 23);
            this._confirmPasswordButton.TabIndex = 0;
            this._confirmPasswordButton.Text = "Принять";
            this._confirmPasswordButton.UseVisualStyleBackColor = true;
            this._confirmPasswordButton.Click += new System.EventHandler(this._confirmPasswordButton_Click);
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Location = new System.Drawing.Point(12, 12);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(75, 20);
            this.passwordTextBox.TabIndex = 1;
            this.passwordTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.passwordTextBox.ValidatingType = typeof(int);
            this.passwordTextBox.TextChanged += new System.EventHandler(this.passwordTextBox_TextChanged);
            this.passwordTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.passwordTextBox_KeyPress);
            // 
            // PasswordForm
            // 
            this.AcceptButton = this._confirmPasswordButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(92, 63);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this._confirmPasswordButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(108, 102);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(108, 102);
            this.Name = "PasswordForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Пароль";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _confirmPasswordButton;
        private System.Windows.Forms.MaskedTextBox passwordTextBox;
        private System.Windows.Forms.ToolTip toolTip;
    }
}