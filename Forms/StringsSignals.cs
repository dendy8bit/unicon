﻿using System;
using System.Collections.Generic;
using BEMN.Devices;

namespace BEMN.Forms
{
    public class StringsSignals
    {
        public static Dictionary<string, string> userData;

        private static Dictionary<Device, List<string>> _signalsDictionary = new Dictionary<Device, List<string>>();
        public static Dictionary<Device, List<string>> SignalsDictionary
        {
            get { return _signalsDictionary; }
        }
        public static List<string> GetNewSignalsList()
        {
            return new List<string>
            {
                
            };
        }

        public static void RemoveSign(Device device)
        {
            if (!SignalsDictionary.ContainsKey(device)) return;
            int ind = Convert.ToInt32(userData["inOutType"]);
            SignalsDictionary[device].RemoveAt(ind);
            string num = ind + 1 < 10 ? " " + (ind + 1) : (ind + 1).ToString();
            SignalsDictionary[device].Insert(ind, num);
        }

        public static void UpdateLists(Device device)
        {
            if (!SignalsDictionary.ContainsKey(device) || !userData.ContainsKey("sign")
                                                        || userData["sign"] == string.Empty) return;
            int ind = Convert.ToInt32(userData["inOutType"]);
            SignalsDictionary[device].RemoveAt(ind);
            SignalsDictionary[device].Insert(ind, userData["sign"]);
            
        }
    }
}
