﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace BEMN.Forms
{
    public partial class ScenesForm : Form
    {
        private bool _isValid;

        public string NameScene { get; private set; }
        public int Time { get; private set; }

        public ScenesForm()
        {
            InitializeComponent();

            StartPosition = FormStartPosition.CenterParent;
            _timeTB.MaxLength = 15;
        }

        private void _addSceneButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_nameSceneMTB.Text))
            {
                MessageBox.Show("Название сценария не должно быть пустым!", "Внимание!", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return;
            }

            NameScene = _nameSceneMTB.Text;
            Time = Convert.ToInt32(_timeTB.Text);

            DialogResult = DialogResult.OK;
            Close();
        }

        private void _timeTB_TextChanged(object sender, EventArgs e)
        {
            try
            {
                RemoveSymbol(_isValid);
            }
            catch (Exception ex)
            {
            }
        }

        private void RemoveSymbol(bool isValid)
        {
            if (!_timeTB.Text.Any(c => char.IsLetter(c)))
            {
                _timeTB.SelectionStart = _timeTB.Text.Length;
                _addSceneButton.Enabled = _timeTB.Text.Length >= 2;
                return;
            }

            if (!isValid)
            {
                _timeTB.Text = _timeTB.Text.Remove(_timeTB.Text.Length - 1);
            }
        }

        private void _timeTB_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if ((e.KeyChar == '\b') && _timeTB.Text.Length <= 2) return;

                if (char.IsControl(e.KeyChar) || !char.IsDigit(e.KeyChar) || (e.KeyChar == '.'))
                {
                    _isValid = false;
                }
                else
                {
                    _isValid = true;
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
