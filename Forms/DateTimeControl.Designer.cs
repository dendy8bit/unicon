﻿namespace BEMN.Forms
{
    partial class DateTimeControl
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this._dateTimeBox = new System.Windows.Forms.GroupBox();
            this._writeDateTimeButt = new System.Windows.Forms.Button();
            this._dateTimeNowButt = new System.Windows.Forms.Button();
            this._stopCB = new System.Windows.Forms.CheckBox();
            this._timeClockTB = new System.Windows.Forms.MaskedTextBox();
            this.label127 = new System.Windows.Forms.Label();
            this._dateClockTB = new System.Windows.Forms.MaskedTextBox();
            this.label128 = new System.Windows.Forms.Label();
            this._dateTimeBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _dateTimeBox
            // 
            this._dateTimeBox.Controls.Add(this._writeDateTimeButt);
            this._dateTimeBox.Controls.Add(this._dateTimeNowButt);
            this._dateTimeBox.Controls.Add(this._stopCB);
            this._dateTimeBox.Controls.Add(this._timeClockTB);
            this._dateTimeBox.Controls.Add(this.label127);
            this._dateTimeBox.Controls.Add(this._dateClockTB);
            this._dateTimeBox.Controls.Add(this.label128);
            this._dateTimeBox.Location = new System.Drawing.Point(3, 3);
            this._dateTimeBox.Name = "_dateTimeBox";
            this._dateTimeBox.Size = new System.Drawing.Size(195, 162);
            this._dateTimeBox.TabIndex = 43;
            this._dateTimeBox.TabStop = false;
            this._dateTimeBox.Text = "Дата-Время";
            // 
            // _writeDateTimeButt
            // 
            this._writeDateTimeButt.Enabled = false;
            this._writeDateTimeButt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._writeDateTimeButt.Location = new System.Drawing.Point(21, 131);
            this._writeDateTimeButt.Name = "_writeDateTimeButt";
            this._writeDateTimeButt.Size = new System.Drawing.Size(156, 23);
            this._writeDateTimeButt.TabIndex = 43;
            this._writeDateTimeButt.Text = "Установить";
            this._writeDateTimeButt.UseVisualStyleBackColor = true;
            this._writeDateTimeButt.Click += new System.EventHandler(this._writeDateTimeButt_Click);
            // 
            // _dateTimeNowButt
            // 
            this._dateTimeNowButt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._dateTimeNowButt.Location = new System.Drawing.Point(21, 102);
            this._dateTimeNowButt.Name = "_dateTimeNowButt";
            this._dateTimeNowButt.Size = new System.Drawing.Size(156, 23);
            this._dateTimeNowButt.TabIndex = 42;
            this._dateTimeNowButt.Text = "Системные дата и время";
            this._dateTimeNowButt.UseVisualStyleBackColor = true;
            this._dateTimeNowButt.Click += new System.EventHandler(this._dateTimeNowButt_Click);
            // 
            // _stopCB
            // 
            this._stopCB.AutoSize = true;
            this._stopCB.Location = new System.Drawing.Point(47, 79);
            this._stopCB.Name = "_stopCB";
            this._stopCB.Size = new System.Drawing.Size(112, 17);
            this._stopCB.TabIndex = 41;
            this._stopCB.Text = "Изменить время";
            this._stopCB.UseVisualStyleBackColor = true;
            this._stopCB.CheckedChanged += new System.EventHandler(this._stopCB_CheckedChanged);
            // 
            // _timeClockTB
            // 
            this._timeClockTB.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this._timeClockTB.Location = new System.Drawing.Point(106, 45);
            this._timeClockTB.Mask = "90:00:00.00";
            this._timeClockTB.Name = "_timeClockTB";
            this._timeClockTB.ReadOnly = true;
            this._timeClockTB.Size = new System.Drawing.Size(83, 20);
            this._timeClockTB.TabIndex = 38;
            this._timeClockTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(103, 27);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(40, 13);
            this.label127.TabIndex = 40;
            this.label127.Text = "Время";
            // 
            // _dateClockTB
            // 
            this._dateClockTB.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this._dateClockTB.Location = new System.Drawing.Point(21, 45);
            this._dateClockTB.Mask = "00.00.00";
            this._dateClockTB.Name = "_dateClockTB";
            this._dateClockTB.ReadOnly = true;
            this._dateClockTB.Size = new System.Drawing.Size(79, 20);
            this._dateClockTB.TabIndex = 37;
            this._dateClockTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(18, 27);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(33, 13);
            this.label128.TabIndex = 39;
            this.label128.Text = "Дата";
            // 
            // DateTimeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._dateTimeBox);
            this.Name = "DateTimeControl";
            this.Size = new System.Drawing.Size(201, 166);
            this._dateTimeBox.ResumeLayout(false);
            this._dateTimeBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox _dateTimeBox;
        private System.Windows.Forms.Button _writeDateTimeButt;
        private System.Windows.Forms.Button _dateTimeNowButt;
        private System.Windows.Forms.CheckBox _stopCB;
        private System.Windows.Forms.MaskedTextBox _timeClockTB;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.MaskedTextBox _dateClockTB;
        private System.Windows.Forms.Label label128;
    }
}
