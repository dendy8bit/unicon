﻿namespace BEMN.Forms
{
    partial class SignaturesOscControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this._oscSignatures = new System.Windows.Forms.DataGridView();
            this._resetSignalsButton = new System.Windows.Forms.Button();
            this._copySignaturesButtons = new System.Windows.Forms.Button();
            this._standartSignalsTextCell = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._deviceSignalsCell = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signaturesCell = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this._oscSignatures)).BeginInit();
            this.SuspendLayout();
            // 
            // _oscSignatures
            // 
            this._oscSignatures.AllowUserToAddRows = false;
            this._oscSignatures.AllowUserToDeleteRows = false;
            this._oscSignatures.AllowUserToResizeColumns = false;
            this._oscSignatures.AllowUserToResizeRows = false;
            this._oscSignatures.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._oscSignatures.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._oscSignatures.BackgroundColor = System.Drawing.Color.White;
            this._oscSignatures.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._oscSignatures.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._standartSignalsTextCell,
            this._deviceSignalsCell,
            this._signaturesCell});
            this._oscSignatures.Dock = System.Windows.Forms.DockStyle.Top;
            this._oscSignatures.Location = new System.Drawing.Point(0, 0);
            this._oscSignatures.Name = "_oscSignatures";
            this._oscSignatures.RowHeadersVisible = false;
            this._oscSignatures.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._oscSignatures.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._oscSignatures.ShowCellErrors = false;
            this._oscSignatures.ShowRowErrors = false;
            this._oscSignatures.Size = new System.Drawing.Size(366, 482);
            this._oscSignatures.TabIndex = 0;
            this._oscSignatures.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._oscSignatures_DataError);
            // 
            // _resetSignalsButton
            // 
            this._resetSignalsButton.Dock = System.Windows.Forms.DockStyle.Top;
            this._resetSignalsButton.Location = new System.Drawing.Point(0, 482);
            this._resetSignalsButton.Name = "_resetSignalsButton";
            this._resetSignalsButton.Size = new System.Drawing.Size(366, 23);
            this._resetSignalsButton.TabIndex = 1;
            this._resetSignalsButton.Text = "Восстановить базовые сигналы";
            this._resetSignalsButton.UseVisualStyleBackColor = true;
            this._resetSignalsButton.Click += new System.EventHandler(this._resetSignalsButton_Click);
            // 
            // _copySignaturesButtons
            // 
            this._copySignaturesButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._copySignaturesButtons.Location = new System.Drawing.Point(0, 505);
            this._copySignaturesButtons.Name = "_copySignaturesButtons";
            this._copySignaturesButtons.Size = new System.Drawing.Size(366, 23);
            this._copySignaturesButtons.TabIndex = 2;
            this._copySignaturesButtons.Text = "Копировать подписи из списка сигналов";
            this._copySignaturesButtons.UseVisualStyleBackColor = true;
            this._copySignaturesButtons.Click += new System.EventHandler(this._copySignaturesButtons_Click);
            // 
            // _standartSignalsTextCell
            // 
            this._standartSignalsTextCell.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this._standartSignalsTextCell.DefaultCellStyle = dataGridViewCellStyle1;
            this._standartSignalsTextCell.HeaderText = "Базовый сигнал";
            this._standartSignalsTextCell.Name = "_standartSignalsTextCell";
            this._standartSignalsTextCell.ReadOnly = true;
            this._standartSignalsTextCell.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._standartSignalsTextCell.Width = 80;
            // 
            // _deviceSignalsCell
            // 
            this._deviceSignalsCell.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Gainsboro;
            this._deviceSignalsCell.DefaultCellStyle = dataGridViewCellStyle2;
            this._deviceSignalsCell.HeaderText = "Текущий список на устройстве";
            this._deviceSignalsCell.Name = "_deviceSignalsCell";
            this._deviceSignalsCell.ReadOnly = true;
            this._deviceSignalsCell.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._deviceSignalsCell.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._deviceSignalsCell.Width = 135;
            // 
            // _signaturesCell
            // 
            this._signaturesCell.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._signaturesCell.HeaderText = "Сигналы для записи в устройство";
            this._signaturesCell.MaxInputLength = 20;
            this._signaturesCell.Name = "_signaturesCell";
            this._signaturesCell.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._signaturesCell.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._signaturesCell.Width = 135;
            // 
            // SignaturesOscControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._copySignaturesButtons);
            this.Controls.Add(this._resetSignalsButton);
            this.Controls.Add(this._oscSignatures);
            this.Name = "SignaturesOscControl";
            this.Size = new System.Drawing.Size(366, 528);
            ((System.ComponentModel.ISupportInitialize)(this._oscSignatures)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView _oscSignatures;
        private System.Windows.Forms.Button _resetSignalsButton;
        private System.Windows.Forms.Button _copySignaturesButtons;
        private System.Windows.Forms.DataGridViewTextBoxColumn _standartSignalsTextCell;
        private System.Windows.Forms.DataGridViewTextBoxColumn _deviceSignalsCell;
        private System.Windows.Forms.DataGridViewTextBoxColumn _signaturesCell;
    }
}
