using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Modules;
using System.Reflection;

namespace BEMN.Forms
{
	/// <summary>
	/// Summary description for UserControl1.
	/// </summary>
	public class MemoryMap : System.Windows.Forms.UserControl
	{
		private System.ComponentModel.IContainer components;
		private MemoryManager _memoryManager;
		private System.Windows.Forms.ToolTip butTip;
		private System.Windows.Forms.MainMenu viewMenu;
		private System.Windows.Forms.MenuItem viewItem;
		private System.Windows.Forms.MenuItem byteItem;
		private System.Windows.Forms.MenuItem commonItem;
		private bool _byteView = true;

		private ArrayList _clasterButtons = new ArrayList();
		private ArrayList _blockPanels = new ArrayList();
		private BEMN.Modules.Module[] _modules;
		
		public MemoryManager Memory
		{
			get
			{
				return _memoryManager;
			}
			set
			{
				_memoryManager = value;
				
			}
		}
		public BEMN.Modules.Module[] Modules
		{
			get
			{
				return _modules;
			}
			set
			{
				_modules = value;
			}
		}

		private int GetModuleIndex(MemoryManager.Block block)
		{
			int ret = -1;

			if (null != _modules)
			{
				for (int i = 0; i < _modules.Length; i++)
				{
					if (_modules[i].MemoryBlocks.Contains(block))
					{
						ret = i;
						break;
					}
				}
			}
			
			return ret;
		}

		public MemoryMap()
		{
			
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			//this.MinimizeBox = false;
			//this.MaximizeBox = false;

//			MemoryManager m = new MemoryManager(500);
//			m.CreateBlock(100);
//			m.CreateBlock(200);
//			m.CreateBlock(50);
//			m.CreateBlock(50);
//			m.CreateBlock(100);
//			m.CreateBlock(50,100);
//			m.CreateBlock(75,25);
//			m.CreateBlock(200,20);
//
//			Memory = m;
//			
			///this.Size = new Size(258,227);
			//DrawByteMap();	
		}

		public void DrawCommonMap()
		{
			if (null != Memory)
			{
				Controls.Clear();
				_blockPanels.Clear();
				int panelHeight = CalculatePanelsHeight();
				int y = 10;
				for (int i = 0; i < _memoryManager.Blocks.Length; i++)
				{
					CreatePanel(i,y);
					y += panelHeight + 10;
				
				}
				SetPanelsText();
				Controls.AddRange((Panel[])_blockPanels.ToArray(typeof(Panel)));
			}
			
		}
		private void CreatePanel(int blockIndex,int yPos)
		{	PropertyInfo[] colors = typeof(Color).GetProperties();

			Panel panel = new Panel();
			panel.Location = new Point(CalculatePanelWidth(_memoryManager.Blocks[blockIndex].Start + 10),yPos);
			panel.Size = new Size(CalculatePanelWidth(_memoryManager.Blocks[blockIndex].Size),CalculatePanelsHeight());
			Label label = new Label();
			panel.Controls.Add(label);
			panel.MouseEnter +=new EventHandler(panel_MouseEnter);
			panel.BorderStyle = BorderStyle.FixedSingle;
			panel.BackColor = (Color)colors[blockIndex + 33].GetValue(null,null);
			_blockPanels.Add(panel);
			
		}

		private void SetPanelsText()
		{
			for(int i = 0; i < _blockPanels.Count; i++)
			{
				(_blockPanels[i] as Panel).Tag = "���� " + (i + 1) + "\n" + 
												 "������ " + _memoryManager.Blocks[i].Start + "\n" + 
												 "����� " + _memoryManager.Blocks[i].End + "\n" + 
									 		     "������ " + _memoryManager.Blocks[i].Size;
																									
																											
			}

		}
	
		private int CalculatePanelWidth(int blockSize)
		{
			int maxBlockLength = 0;
			
			foreach(MemoryManager.Block bl in _memoryManager.Blocks)
			{
				maxBlockLength += bl.Size;
			}

			return this.Width * blockSize / maxBlockLength ;
		}
		
		private int CalculatePanelsHeight()
		{
			return (this.Height -  _memoryManager.Blocks.Length * 18) / _memoryManager.Blocks.Length ;
		}



		public void DrawByteMap()
		{
			if (null != Memory)
			{
				Controls.Clear();
				_blockPanels.Clear();
				_clasterButtons.Clear();
				DrawButtons(CalculateButtonSize(this.Size,_memoryManager.Capacacity));
				PaintBlocks(_memoryManager);
				SetButttonsText((Button[])_clasterButtons.ToArray(typeof(Button)),_memoryManager.Blocks);
			}
			
		}
		
		private void PaintBlocks(MemoryManager manager)	
		{
				
			for (int i = 0; i < _clasterButtons.Count; i++)
			{
				int blockNumber;
				int overlapCnt = _memoryManager.GetOverlapCount(i,out blockNumber);
				if (0 != overlapCnt)
				{
					if ( 1 == overlapCnt)
					{				
						
						(_clasterButtons[i] as Button).BackColor = GetBlockColor(_memoryManager.Blocks[blockNumber]);
					}
					else
					{
						(_clasterButtons[i] as Button).BackColor = Color.Red;	
					}
				}
				
				
			}
		}

		private Color GetBlockColor(MemoryManager.Block block)
		{	
			PropertyInfo[] colors = typeof(Color).GetProperties();
			int moduleIndex = GetModuleIndex(block);
			return (Color)colors[moduleIndex + 33].GetValue(null,null);
						
		}
		
		private int CalculateButtonSize(Size formSize,int clustersCnt)
		{			 
			return (int)Math.Sqrt(formSize.Height * formSize.Width / clustersCnt) ;
		}
		private void SetButtonColor(int start,int end,Color color)
		{
			for (int i = start; i < end ; i++)
			{
				(_clasterButtons[i] as Button).BackColor = color;
			}
		}

		private void DrawButtons(int butSize)
		{
			
			_clasterButtons.Clear();


			for (int i = 0; i < Memory.Capacacity;i++)
			{
				Button but = new Button();
				but.Location = CalculateButtonLocation(i,butSize);
				but.BackColor = Color.White;
				but.Size = new Size(butSize,butSize);
				_clasterButtons.Add(but);
			}
			foreach(MemoryManager.Block bl in Memory.Blocks)
			{
				for (int i  = bl.Start; i < bl.End;i++)
				{
					(_clasterButtons[i] as Button).MouseEnter +=new EventHandler(but_MouseEnter);
					
				}
			}
			Controls.AddRange((Button[])_clasterButtons.ToArray(typeof(Button)));
		}


		
		
		private Point CalculateButtonLocation(int buttonNumber,int buttonSize)
		{
			buttonSize -= 1;
			int butCntinColumn = (Width - buttonSize) / buttonSize;
			int butCntinRow = (Height - buttonSize) / buttonSize;
			
			int butNumRow = buttonNumber / butCntinColumn;
			int butNumColumn;
			Math.DivRem(buttonNumber,butCntinColumn,out butNumColumn);
					
			return new Point(butNumColumn * buttonSize,butNumRow * buttonSize);

		}
		
		protected void SetButttonsText(Button[] buttons,MemoryManager.Block[] blocks)
		{
			for (int i = 0; i < Memory.Blocks.Length; i++)
			{
				SetButttonsText(i,buttons,Memory.Blocks[i].Start,Memory.Blocks[i].Size);
			}
			
			
			
		}
		protected void SetButttonsText(int blockIndex,Button[] buttons,int start,int cnt)
		{
						
            for (int i = start; i < start + cnt; i++)
			{
				if (null == buttons[i].Tag)
				{
					buttons[i].Tag += "����� ����� � ���������� " + i + "\n";
				}
				if (null == _modules)
				{
					buttons[i].Tag += "���� " + (blockIndex + 1) + " \n" + 
									 "����� ����� � ����� " + (i - _memoryManager.Blocks[blockIndex].Start ) + "\n";
				}else
				{
					int moduleIndex = GetModuleIndex(_memoryManager.Blocks[blockIndex]) + 1 ;
					if (0 == moduleIndex)
					{
						buttons[i].Tag += "������ �� ��������. \n" + 
										   "����� ����� � ������ " + (i - _memoryManager.Blocks[blockIndex].Start ) + "\n";
					}
					else
					{
						buttons[i].Tag += "������ " + moduleIndex + " \n" + 
							"����� ����� � ������ " + (i - _memoryManager.Blocks[blockIndex].Start ) + "\n";
					}
					
				}
				
				
			}	
		}
		
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( components != null )
					components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.butTip = new System.Windows.Forms.ToolTip(this.components);
			this.viewMenu = new System.Windows.Forms.MainMenu();
			this.viewItem = new System.Windows.Forms.MenuItem();
			this.byteItem = new System.Windows.Forms.MenuItem();
			this.commonItem = new System.Windows.Forms.MenuItem();
			// 
			// butTip
			// 
			this.butTip.AutomaticDelay = 100;
			this.butTip.AutoPopDelay = 5000;
			this.butTip.InitialDelay = 100;
			this.butTip.ReshowDelay = 20;
			// 
			// viewMenu
			// 
			this.viewMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.viewItem});
			// 
			// viewItem
			// 
			this.viewItem.Index = 0;
			this.viewItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.byteItem,
																					 this.commonItem});
			this.viewItem.Text = "��� �����";
			// 
			// byteItem
			// 
			this.byteItem.Checked = true;
			this.byteItem.Index = 0;
			this.byteItem.Text = "��������";
			this.byteItem.Click += new System.EventHandler(this.byteItem_Click);
			// 
			// commonItem
			// 
			this.commonItem.Index = 1;
			this.commonItem.Text = "����� �����";
			this.commonItem.Click += new System.EventHandler(this.commonItem_Click);
			// 
			// MemoryMap
			// 
//			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(504, 349);
//			this.Menu = this.viewMenu;
			this.Name = "MemoryMap";
			this.SizeChanged += new System.EventHandler(this.MemoryMap_SizeChanged);

		}
		#endregion

		protected void MemoryMap_SizeChanged(object sender, System.EventArgs e)
		{
			if (_byteView)
			{
				DrawByteMap();
			}else
			{
				DrawCommonMap();
			}
			
		}

		private int GetButtonIndex(Button but)
		{
			int ret = -1;
			for (int i = 0 ;  i < _clasterButtons.Count; i++)
			{
				if (but.Text == (_clasterButtons[i] as Button).Text)
				{
					ret = i;
					break;
				}	
			}
			
			return ret;
		}

		private int GetBlockIndex(int start,int end)
		{
			int ret = -1;
			for (int i = 0; i < _memoryManager.Blocks.Length; i++)
			{
				if (_memoryManager.Blocks[i].Start == start && _memoryManager.Blocks[i].End == end)
				{
					ret = i;
					break;
				}
			}
			return ret;
		}

		protected void but_MouseEnter(object sender, EventArgs e)
		{
			butTip.SetToolTip(sender as Button,(sender as Button).Tag.ToString());
		}

		private void byteItem_Click(object sender, System.EventArgs e)
		{
			commonItem.Checked = !commonItem.Checked;
            byteItem.Checked = !byteItem.Checked;
			_byteView = true;
			DrawByteMap();
		}

		private void commonItem_Click(object sender, System.EventArgs e)
		{
			commonItem.Checked = !commonItem.Checked;
			byteItem.Checked = !byteItem.Checked;
			_byteView = false;
			DrawCommonMap();
			
		
		}

		private void panel_MouseEnter(object sender, EventArgs e)
		{
			butTip.SetToolTip(sender as Panel,(sender as Panel).Tag.ToString());
		}

		
	}
}
