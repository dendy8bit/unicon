﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Utils;
using Crownwood.Magic.Common;
using Crownwood.Magic.Docking;
using ICSharpCode.TextEditor;
using ICSharpCode.TextEditor.Document;
using TabPage = Crownwood.Magic.Controls.TabPage;
using BEMN.Interfaces;
using System.ComponentModel;

namespace BEMN.Forms.IDE
{

    public abstract partial class GraphDevelopmentForm : Form, ISourceRedactor
    {
        DockingManager _manager;

        protected ListBox _outputList = new ListBox();
        private Panel _headerPanel, _footerPanel, _mainPanel;
        private ComboBox _labelsCombo;
        private Device _device;

        private DocumentNavigator _navigator;
        private CompilerResults compilerResults;

        public TextEditorControl SelectedRichBox
        {
            get
            {
                return _sourcesTabControl.SelectedTab.Control as TextEditorControl;
            }
        }
        public DockingManager StyleManager
        {
            get
            {
                return _manager;
            }
        }



        public GraphDevelopmentForm()
        {
            InitializeComponent();
            _manager = new DockingManager(this, VisualStyle.IDE);
            Load += new EventHandler(BasicDevelopmentForm_Load);
        }

        public GraphDevelopmentForm(Device device)
        {
            InitializeComponent();
            _manager = new DockingManager(this, VisualStyle.IDE);
            Load += new EventHandler(BasicDevelopmentForm_Load);
            _device = device;
        }

        protected void _outputList_DoubleClick(object sender, EventArgs e)
        {
            if (null != _outputList.SelectedItem)
            {
                Match m =
                    Regex.Match(_outputList.SelectedItem as string, "Error on Line - (?<line>\\d+)",
                                RegexOptions.Compiled);
                if (m.Length != 0)
                {
                    int lineNum = Convert.ToInt32(m.Result("${line}"));


                    SelectedRichBox.ActiveTextAreaControl.Caret.Line = lineNum - 1;
                    SelectedRichBox.ActiveTextAreaControl.JumpTo(lineNum - 1, 0);


                    LineSegment selSegment = SelectedRichBox.ActiveTextAreaControl.Document.GetLineSegment(lineNum - 1);
                    Point startSel = SelectedRichBox.ActiveTextAreaControl.Caret.Position;
                    Point endSel =
                        SelectedRichBox.ActiveTextAreaControl.Document.OffsetToPosition(selSegment.Offset +
                                                                                        selSegment.Length);

                    SelectedRichBox.ActiveTextAreaControl.SelectionManager.SetSelection(startSel, endSel);
                }
            }
        }

        void BasicDevelopmentForm_Load(object sender, EventArgs e)
        {
            Controls.Remove(_sourcesTabControl);
            CreateLabelCombo();

            CreateComboFooterPanel();
            CreateFooterPanel();
            CreateMainPanel();

            Controls.AddRange(new Control[] { _headerPanel, _mainPanel });


            Content c = StyleManager.Contents.Add(_footerPanel, "Сообщения компилятора");
            c.CloseButton = c.HideButton = c.CaptionBar = true;
            c.Docked = false;
            c.DisplaySize = _footerPanel.Size;

            StyleManager.AddContentWithState(c, State.DockBottom);

            _navigator = new DocumentNavigator(this);
            _outputList.DoubleClick += new EventHandler(_outputList_DoubleClick);
        }

        //void TextArea_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.Control && 70 == e.KeyValue)
        //    {

        //    }   
        //}

        private void CreateMainPanel()
        {
            _mainPanel = new Panel();
            _mainPanel.Dock = DockStyle.Fill;
            _mainPanel.Size = new Size(_mainPanel.Width, _mainPanel.Height - _footerPanel.Height * 2);
            _mainPanel.Top = _headerPanel.Height;
            _mainPanel.Left = 0;

            _mainPanel.Controls.Add(_sourcesTabControl);
        }

        private void CreateFooterPanel()
        {
            _footerPanel = new Panel();
            _footerPanel.Dock = DockStyle.Bottom;
            _footerPanel.Size = new Size(Width, 100);
            _outputList.Dock = DockStyle.Fill;
            _footerPanel.Controls.Add(_outputList);
        }

        private void CreateComboFooterPanel()
        {
            _headerPanel = new Panel();
            _headerPanel.Dock = DockStyle.Bottom;
            _headerPanel.Size = new Size(Width, 25);
            _headerPanel.Controls.Add(_labelsCombo);
        }

        private void CreateLabelCombo()
        {
            _labelsCombo = new ComboBox();
            _labelsCombo.Top = 2;
            _labelsCombo.Size = new Size(150, 30);
            _labelsCombo.Dock = DockStyle.Left;
            _labelsCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            _labelsCombo.Sorted = true;
            _labelsCombo.DropDown += new EventHandler(_labelsCombo_DropDown);
            _labelsCombo.SelectedIndexChanged += new EventHandler(_labelsCombo_SelectedIndexChanged);
        }

        void _labelsCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (LabelNavigator)
            {
                LineSegment segment = DocumentNavigator.GetSegmentByWord(_navigator.GetLabels().GetEnumerator(), _labelsCombo.SelectedItem.ToString());
                if (null != segment)
                {
                    _navigator.GotoSegment(segment);
                }
            }
        }

        void _labelsCombo_DropDown(object sender, EventArgs e)
        {
            if (LabelNavigator)
            {
                _labelsCombo.Items.Clear();
                List<LineSegment> labels = _navigator.GetLabels();
                foreach (LineSegment label in labels)
                {
                    _labelsCombo.Items.Add(label.Words[0].Word);
                }
            }
        }

        public int IsSourceAdded(object src)
        {
            int ret = -1;
            for (int i = 0; i < _sourcesTabControl.TabPages.Count; i++)
            {
                TabPage page = _sourcesTabControl.TabPages[i];
                if (page.Tag == src)
                {
                    ret = i;
                    break;
                }
            }
            return ret;
        }

        void Document_DocumentChanged(object sender, DocumentEventArgs e)
        {
            Source src = _navigator.GetSourceByDocument(e.Document);
            if (null != src)
            {
                src.IsSaved = false;
                src.Value = e.Document.TextContent;
            }
        }

        void back_DoWork(object sender, DoWorkEventArgs e)
        {
            Source source = (Source)e.Argument;
            source.SourceClass.SetCompilerOptions(false);
            compilerResults = source.SourceClass.Compile(source);
        }

        void back_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            FillOutputList(compilerResults);
            HighlightErrors();
        }

        protected void FillOutputList(CompilerResults results)
        {
            _outputList.Items.Add("Компиляция завершилась");

            foreach (string msg in results.Output)
            {
                _outputList.Items.Add(msg);
            }
        }


        protected void HighlightErrors()
        {
            foreach (object o in _outputList.Items)
            {
                Match m =
                  Regex.Match((string)o, "Error on Line - (?<line>\\d+)",
                              RegexOptions.Compiled);

                if (m.Length != 0)
                {
                    int lineNum = Convert.ToInt32(m.Result("${line}"));

                    LineSegment selSegment = SelectedRichBox.ActiveTextAreaControl.Document.GetLineSegment(lineNum - 1);

                    foreach (TextWord w in selSegment.Words)
                    {
                        if (!w.IsWhiteSpace)
                        {
                            w.SyntaxColor = new HighlightColor(Color.Red, false, true);
                        }
                    }
                }
            }
        }


        #region ISourceRedactor Members

        public void ShowSource(object src)
        {
            int ind = IsSourceAdded(src);
            if (ind >= 0)
            {
                _sourcesTabControl.SelectedTab = _sourcesTabControl.TabPages[ind];
            }
            if (false == Visible)
            {
                Show();
            }
        }

        public virtual bool AddSource(object src)
        {
            bool ret = false;

            if (IsSourceAdded(src) < 0)
            {
                Source source = (Source)src;
                TextEditorControl codeBox = new TextEditorControl();
                codeBox.ShowEOLMarkers = codeBox.ShowInvalidLines = codeBox.ShowSpaces =
                codeBox.ShowTabs = codeBox.ShowVRuler = codeBox.ShowHRuler = false;
                codeBox.Dock = DockStyle.Fill;

                codeBox.LoadFile(source.FileName, true, true);
                codeBox.Document.DocumentChanged += new DocumentEventHandler(Document_DocumentChanged);
                //codeBox.ActiveTextAreaControl.TextArea.KeyDown +=new System.Windows.Forms.KeyEventHandler(TextArea_KeyDown);
                Bitmap btmp = new Bitmap(source.SourceClass.NodeImage);
                Icon icon = Icon.FromHandle(btmp.GetHicon());

                TabPage page = new TabPage(Path.GetFileName(source.FileName), codeBox, icon);
                _sourcesTabControl.TabPages.Add(page);
                page.Tag = source;

                source.Value = codeBox.ActiveTextAreaControl.Document.TextContent;
                ret = true;

            }


            return ret;
        }

        public void SaveSources(List<string> fileNames)
        {
            for (int i = 0; i < _sourcesTabControl.TabPages.Count; i++)
            {
                TabPage page = _sourcesTabControl.TabPages[i];
                Source source = (Source)page.Tag;
                if (fileNames.Contains(source.FileName))
                {
                    source.SourceClass.SaveSource(source);
                }
            }
        }

        public List<string> NotSavedSourceNames
        {
            get
            {
                List<string> ret = new List<string>(_sourcesTabControl.TabPages.Count);
                for (int i = 0; i < _sourcesTabControl.TabPages.Count; i++)
                {
                    TabPage page = _sourcesTabControl.TabPages[i];
                    if (false == ((Source)page.Tag).IsSaved)
                    {
                        ret.Add(((Source)page.Tag).FileName);
                    }
                }
                return ret;
            }
        }

        public void RemoveSource(object src)
        {
            int removed = -1;
            for (int i = 0; i < _sourcesTabControl.TabPages.Count; i++)
            {
                TabPage page = _sourcesTabControl.TabPages[i];
                if ((Source)page.Tag == (Source)src)
                {
                    removed = i;
                    break;
                }
            }
            if (removed >= 0)
            {
                _sourcesTabControl.TabPages.RemoveAt(removed);
            }
            if (0 == _sourcesTabControl.TabPages.Count)
            {
                Close();
            }
        }

        public ArrayList Sources
        {
            get
            {
                ArrayList ret = new ArrayList(_sourcesTabControl.TabPages.Count);
                for (int i = 0; i < _sourcesTabControl.TabPages.Count; i++)
                {
                    TabPage page = _sourcesTabControl.TabPages[i];
                    ret.Add(page.Tag);
                }
                return ret;
            }
        }

        public bool LabelNavigator
        {
            get
            {
                return _labelsCombo.Visible;
            }
            set
            {
                _labelsCombo.Visible = value;
            }


        }
        public virtual void Compile(List<object> srcs)
        {
            //List<string> res = Search(null, "Nasos", "", null);
            BackgroundWorker back = new BackgroundWorker();
            back.DoWork += new DoWorkEventHandler(back_DoWork);
            back.RunWorkerCompleted += new RunWorkerCompletedEventHandler(back_RunWorkerCompleted);
            if (srcs.Count > 1)
            {
                throw new NotSupportSourceCountException();
            }
            Source source = (Source)srcs[0];
            source.SourceClass.SaveSource(source);
            back.RunWorkerAsync(source);

            _outputList.Items.Clear();
            _outputList.Items.Add("Компиляция началась...");
        }

        public void ReopenSource(object src)
        {
            _navigator.GetCodeBoxByFileName(((Source)src).FileName).LoadFile(((Source)src).FileName, true, true);
            _navigator.GetPageBySource((Source)src).Title = Path.GetFileName(((Source)src).FileName);
        }

        public void Copy()
        {
            SelectedRichBox.ActiveTextAreaControl.TextArea.ClipboardHandler.Copy(null, new EventArgs());
        }

        public void Paste()
        {
            SelectedRichBox.ActiveTextAreaControl.TextArea.ClipboardHandler.Paste(null, new EventArgs());
        }

        public void Undo()
        {
            SelectedRichBox.ActiveTextAreaControl.Document.UndoStack.Undo();
        }

        public void Cut()
        {
            SelectedRichBox.ActiveTextAreaControl.TextArea.ClipboardHandler.Cut(null, new EventArgs());
        }

        public void Delete()
        {
            SelectedRichBox.ActiveTextAreaControl.TextArea.ClipboardHandler.Delete(null, new EventArgs());
        }

        public List<string> Search(string filename, string whatStr, string replaceStr, object[] options)
        {
            List<string> results = new List<string>();
            List<string> searchStrings = new List<string>(Regex.Split(whatStr, " "));

            List<IDocument> idocs = _navigator.GetDocumentsByFile(filename);
            foreach (IDocument idoc in idocs)
            {
                foreach (LineSegment segment in idoc.LineSegmentCollection)
                {
                    List<string> temporarySearch = new List<string>(searchStrings);

                    foreach (TextWord word in segment.Words)
                    {
                        if (temporarySearch.Contains(word.Word))
                        {
                            temporarySearch.Remove(word.Word);
                        }
                    }
                    if (0 == temporarySearch.Count)
                    {
                        results.Add(_navigator.GetSourceByDocument(idoc).FileName + "(" + idoc.GetLineNumberForOffset(segment.Offset) + "): " + SegmentToString(segment));
                    }
                }
            }
            return results;

        }
        #endregion

        public string SegmentToString(LineSegment segment)
        {
            string ret = "";
            foreach (TextWord word in segment.Words)
            {
                ret += word.Word;
            }
            return ret;
        }

        private bool showSaveDlg = true;
        private void BasicDevelopmentForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.MdiFormClosing)
            {
                showSaveDlg = false;
            }
            if (showSaveDlg)
            {
                SaveSourceDialog form = new SaveSourceDialog();
                List<string> notSaved = NotSavedSourceNames;
                if (0 != notSaved.Count)
                {
                    form.FilesList = notSaved;
                    form.ShowInTaskbar = false;
                    form.StartPosition = FormStartPosition.CenterParent;
                    DialogResult dlgResult = form.ShowDialog();

                    if (DialogResult.Cancel == dlgResult)
                    {
                        e.Cancel = true;
                    }

                    if (DialogResult.Yes == dlgResult)
                    {
                        for (int i = 0; i < _sourcesTabControl.TabPages.Count; i++)
                        {
                            TabPage page = _sourcesTabControl.TabPages[i];
                            ((Source)page.Tag).IsSaved = true;
                        }
                        List<String> toSave = form.FilesList;

                        for (int i = 0; i < toSave.Count; i++)
                        {
                            String text = _navigator.GetCodeBoxByFileName(toSave[i]).Text;
                            File.WriteAllText(toSave[i], text);
                        }
                    }
                }
            }
        }

        private void _sourcesTabControl_ClosePressed(object sender, EventArgs e)
        {
            Source src = (Source)_sourcesTabControl.SelectedTab.Tag;

            if (src.IsSaved == false)
            {
                if (DialogResult.Yes == MessageBox.Show("Сохранить изменения в файле " + src.FileName + " ?", "Уникон", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    File.WriteAllText(src.FileName, _navigator.GetCodeBoxByFileName(src.FileName).Document.TextContent);
                    src.IsSaved = true;
                }
            }
            _sourcesTabControl.TabPages.Remove(_sourcesTabControl.SelectedTab);
        }


    }
}