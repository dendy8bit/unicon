using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace BEMN.Forms
{
	/// <summary>
	/// Summary description for MemoryView.
	/// </summary>
	public class MemoryView : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.ListBox valueList;
		private System.Windows.Forms.ListBox addrList;
		private System.Windows.Forms.ListBox stringList;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private BEMN.Forms.HexTextbox addrBox;
		private System.Windows.Forms.Label addrLabel;

		private byte[] _memory;
		
		public byte[] Memory
		{
			get
			{
				return _memory;
			}
			set
			{
				_memory = value;
				if (this.Created)
				{
					InitLists();
				}
			}

		}
		
		
		public MemoryView()
		{
			
			Image im =  Image.FromStream(System.Reflection.Assembly.LoadFrom("Forms.dll").GetManifestResourceStream("Forms.Res.memoryview.bmp"));
			this.Icon = Icon.FromHandle(new Bitmap(im).GetHicon());
			InitializeComponent();
		}

		private void InitLists()
		{
			for (int i = 0; i < _memory.Length; i+= 8)
			{
				addrList.Items.Add("0x" + i.ToString("x"));
				string values = null;
				string str = null;
				for (int j = i; j < i + 8; j++)
				{
					if (j >= _memory.Length)
					{
						break;
					}
					values += "0x" + _memory[j].ToString("x") + " ";
					str += _memory[j].ToString("d");

				}
				valueList.Items.Add(values);
				stringList.Items.Add(str);
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.addrLabel = new System.Windows.Forms.Label();
			this.addrBox = new BEMN.Forms.HexTextbox();
			this.stringList = new System.Windows.Forms.ListBox();
			this.addrList = new System.Windows.Forms.ListBox();
			this.valueList = new System.Windows.Forms.ListBox();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.addrLabel);
			this.panel1.Controls.Add(this.addrBox);
			this.panel1.Controls.Add(this.stringList);
			this.panel1.Controls.Add(this.addrList);
			this.panel1.Controls.Add(this.valueList);
			this.panel1.Location = new System.Drawing.Point(4, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(392, 320);
			this.panel1.TabIndex = 0;
			// 
			// addrLabel
			// 
			this.addrLabel.Location = new System.Drawing.Point(8, 8);
			this.addrLabel.Name = "addrLabel";
			this.addrLabel.Size = new System.Drawing.Size(104, 16);
			this.addrLabel.TabIndex = 4;
			this.addrLabel.Text = "������� � ������:";
			// 
			// addrBox
			// 
			this.addrBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.addrBox.HexBase = true;
			this.addrBox.Limit = 65536;
			this.addrBox.Location = new System.Drawing.Point(120, 8);
			this.addrBox.Name = "addrBox";
			this.addrBox.Size = new System.Drawing.Size(104, 20);
			this.addrBox.TabIndex = 3;
			this.addrBox.Text = "";
			this.addrBox.TextChanged += new System.EventHandler(this.addrBox_TextChanged);
			// 
			// stringList
			// 
			this.stringList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.stringList.Location = new System.Drawing.Point(260, 32);
			this.stringList.Name = "stringList";
			this.stringList.Size = new System.Drawing.Size(132, 288);
			this.stringList.TabIndex = 2;
			this.stringList.SelectedIndexChanged += new System.EventHandler(this.stringList_SelectedIndexChanged);
			// 
			// addrList
			// 
			this.addrList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.addrList.Location = new System.Drawing.Point(0, 32);
			this.addrList.Name = "addrList";
			this.addrList.Size = new System.Drawing.Size(76, 288);
			this.addrList.TabIndex = 1;
			this.addrList.SelectedIndexChanged += new System.EventHandler(this.addrList_SelectedIndexChanged);
			// 
			// valueList
			// 
			this.valueList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.valueList.Location = new System.Drawing.Point(72, 32);
			this.valueList.Name = "valueList";
			this.valueList.Size = new System.Drawing.Size(192, 288);
			this.valueList.TabIndex = 0;
			this.valueList.SelectedIndexChanged += new System.EventHandler(this.valueList_SelectedIndexChanged);
			// 
			// MemoryView
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(392, 317);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Name = "MemoryView";
			this.Text = "�������� ������";
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void addrList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			stringList.SelectedIndex = valueList.SelectedIndex = addrList.SelectedIndex;
		}

		private void stringList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			valueList.SelectedIndex = addrList.SelectedIndex = stringList.SelectedIndex;
		}

		private void valueList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			 addrList.SelectedIndex = stringList.SelectedIndex = valueList.SelectedIndex ;
		}


		private int RoundIndex(int addr)
		{
			int ret = _memory.Length;

			if (_memory.Length >= addr)
			{
				int result;
				Math.DivRem(addr,8,out result);

				if (0 ==  result)
				{
					ret = addr;
				}else
				{
					ret = addr +  8 -  result;
				}
			
			}

			return ret;
		}

		private void addrBox_TextChanged(object sender, System.EventArgs e)
		{
			string str ="0x";
			try
			{
				str +=   RoundIndex(Convert.ToInt32(addrBox.Text,16)).ToString("x");
			}
			catch
			{
				return;
			}
			 
			if (addrList.Items.Contains(str))
			{
				addrList.SelectedItem = str;
			}	
		}
	}
}
