namespace BEMN.Forms
{
    public enum SheetFormat
    {
        A4_P,
        A4_L,
        A3_P,
        A3_L,
        A2_P,
        A2_L,
        A1_P,
        A1_L,
        A0_P,
        A0_L
    }
}