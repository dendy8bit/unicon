﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices;

namespace BEMN.Forms
{
    public partial class SignaturesSignalsConfigControl : UserControl
    {
        public event Action AcceptedSignals;
        public List<string> SignalsList { get; set; }
        public List<string> DefaultList { get; set; }
        public string DeviceType;
        public int DiskretsCount;

        public SignaturesSignalsConfigControl()
        {
            InitializeComponent();
        }
        
        public ListsOfConfigurations ListConfiguration(string deviceType, List<string> list, double deviceVersion)
        {
            ListsOfConfigurations listConfig = new ListsOfConfigurations(deviceType);
            listConfig.SignalsList.MessagesList = list;
            listConfig.SignalsList.Count = SignalsList.Count;
            listConfig.Version = deviceVersion;

            return listConfig;
        }

        private static string AddSpaceInString(string str)
        {
            if (str.Length < 9)
            {
                int count = 9 - str.Length;
                for (int i = 0; i < count; i++)
                {
                    str += " ";
                }
            }

            return str;
        }

        public void ClearDgv()
        {
            for (int i = 0; i < _signaturesSignalsDGV.RowCount; i++)
            {
                _signaturesSignalsDGV.Rows[i].Cells[2].Value = "";
            }
        }

        public void FillSignaturesSignalsDgv(List<string> messages, List<string> signals, bool isLoaded = false, List<string> signalsInDevice = null, List<string> signalsUser = null)
        {
            _signaturesSignalsDGV.Rows.Clear();

            int j = 0;
            int count = 88 - DiskretsCount;
            try
            {
                _signaturesSignalsDGV.Rows.Add(signals.Count - count - 1);
                
                for (int i = 0; i < signals.Count; i++)
                {
                    if (i > DiskretsCount && i <= DiskretsCount + count || i == 0)
                        continue;
                    _signaturesSignalsDGV.Rows[j].Cells[0].Value = signals[i];
                    j++;
                }

                j = 0;
                for (int i = 0; i < messages.Count; i++)
                {
                    if (i > DiskretsCount && i <= DiskretsCount + count || i == 0)
                        continue;
                    _signaturesSignalsDGV.Rows[j].Cells[1].Value = messages[i];
                    j++;
                }

                j = 0;
                for (int i = 0; i < signals.Count; i++)
                {
                    if (isLoaded)
                    {
                        if (signalsInDevice != null)
                        {
                            if (i > DiskretsCount && i <= DiskretsCount + count || i == 0)
                                continue;
                            _signaturesSignalsDGV.Rows[j].Cells[1].Value = signalsInDevice[i];
                            j++;
                        }
                        else
                        {
                            if (i > DiskretsCount && i <= DiskretsCount + count || i == 0)
                                continue;
                            _signaturesSignalsDGV.Rows[j].Cells[1].Value = DefaultList[i];
                            j++;
                        }

                        if (signalsUser != null)
                        {
                            if (i > DiskretsCount && i <= DiskretsCount + count || i == 0)
                                continue;
                            _signaturesSignalsDGV.Rows[j].Cells[2].Value = signalsUser[i];
                            j++;
                        }
                        else
                        {
                            if (i > DiskretsCount && i <= DiskretsCount + count || i == 0)
                                continue;
                            _signaturesSignalsDGV.Rows[j].Cells[2].Value = DefaultList[i];
                            j++;
                        }
                    }
                    else
                    {
                        if (i > DiskretsCount && i <= DiskretsCount + count || i == 0)
                            continue;
                        _signaturesSignalsDGV.Rows[j].Cells[2].Value = "";
                        j++;
                    }
                }

            }
            catch (Exception ex)
            {
                j = 0;
                for (int i = 0; i < signals.Count; i++)
                {
                    if (i > DiskretsCount && i <= DiskretsCount + count || i == 0)
                        continue;
                    _signaturesSignalsDGV.Rows.Add(signals[i], "");
                    j++;
                }
            }
        }
        
        public List<string> SaveToFile(out List<string> value, int cell)
        {
            value = new List<string>();

            for (int i = 0; i < _signaturesSignalsDGV.RowCount; i++)
            {
                value.Add((string)_signaturesSignalsDGV.Rows[i].Cells[cell].Value);
            }

            return value;
        }

        public void SetToolTipForTabPages(List<TabPage> tabPages, int indexStart)
        {
            List<string> list = new List<string>();
            list.AddRange(SignalsList.GetRange(indexStart, 16));
            for (int i = 0; i < list.Count; i++)
            {
                var trim = list[i].Trim();
                list[i] = trim;
            }

            for (int i = 0; i < tabPages.Count; i++)
            {
                tabPages[i].ToolTipText = list[i];
            }
        }

        public void AddedItemInList(List<string> list = null, Dictionary<ushort, string> dictionary = null, bool isDictionary = false)
        {
            list?.Clear();
            dictionary?.Clear();

            if (isDictionary)
            {
                for (ushort i = 0; i < SignalsList.Count * 2 - 1; i++)
                {
                    try
                    {
                        if ((i % 2) == 0 && i != 0)
                        {
                            dictionary.Add(i, SignalsList[i / 2].Trim() + " Инв.");
                        }
                        else
                        {
                            dictionary.Add(i,  SignalsList[(i + 1) / 2].Trim());
                        }
                    }
                    catch (Exception exception)
                    {

                    }
                }
            }
            else
            {
                list?.Add("НЕТ");

                for (int i = 0; i < SignalsList.Count * 2; i++)
                {
                    try
                    {
                        if ((i % 2) != 0)
                        {
                            list.Add(SignalsList[(i - 1) / 2].Trim() + " Инв.");
                        }
                        else
                        {
                            list.Add(SignalsList[i/ 2].Trim());
                        }
                    }
                    catch (Exception exception)
                    {

                    }
                }
            }
            
        }

        public void AddedItemInListInvSignals(string endSignal, string startSignal = "", List<string> list = null, Dictionary<ushort, string> dictionary = null, bool isDictionary = false)
        {
            list?.Clear();
            dictionary?.Clear();

            if (isDictionary)
            {
                for (ushort i = 0; i < SignalsList.Count * 2 - 1; i++)
                {
                   
                    var index = DefaultList.IndexOf(startSignal);

                    if (DefaultList[(i + 1) / 2].Trim().StartsWith(endSignal))
                    {
                        break;
                    }

                    if (i < index && startSignal != "" && i != 0) continue;

                    try
                    {
                        if ((i % 2) == 0 && i != 0)
                        {
                            dictionary.Add(i, SignalsList[i / 2].Trim() + " Инв.");
                        }
                        else
                        {
                            dictionary.Add(i, SignalsList[(i + 1) / 2].Trim());
                        }
                    }
                    catch (Exception exception)
                    {

                    }

                    
                }
            }
            else
            {
                list?.Add("НЕТ");
                for (int i = 0; i < SignalsList.Count * 2; i++)
                {
                    if (SignalsList[i / 2].Trim().StartsWith(endSignal))
                    {
                        break;
                    }

                    var index = SignalsList.IndexOf(startSignal);

                    if (i < index && startSignal != "" && i != 0) continue;

                    try
                    {
                        if ((i % 2) != 0)
                        {
                            list?.Add(SignalsList[(i - 1) / 2].Trim() + " Инв.");
                        }
                        else
                        {
                            list?.Add(SignalsList[i / 2].Trim());
                        }

                    }
                    catch (Exception exception)
                    {

                    }
                }
            }
        }

        public void AddedItemInListVLS(string startIndex, string endIndex, string endSignal = "", List<string> list = null, Dictionary<int, string> dictionary = null, bool isDictionary = false)
        {
            list?.Clear();
            dictionary?.Clear();

            if (isDictionary)
            {
                ushort i = 0;

                if (startIndex == "НЕТ")
                {
                    dictionary?.Add(0, startIndex);
                    i++;
                }

                for (; i < SignalsList.Count - 1; i++)
                {
                    if (SignalsList[i].Trim().StartsWith(endSignal) && endSignal != "")
                    {
                        break;
                    }

                    var sIndex = DefaultList.IndexOf(startIndex);
                    var eIndex = DefaultList.IndexOf(endIndex);

                    if (i > sIndex - 1 && i < eIndex + 1) continue;
                    
                    if (startIndex == "НЕТ")
                    {
                        dictionary?.Add(i, SignalsList[i - 1].Trim());
                    }
                    else
                    {
                        dictionary?.Add(i, SignalsList[i + 1].Trim());
                    }
                }
            }
            else
            {
                if (startIndex == "НЕТ") list.Insert(0, startIndex);

                for (int i = 0; i < SignalsList.Count; i++)
                {
                    if (DefaultList[i].Trim().StartsWith(endSignal) && endSignal != "")
                    {
                        break;
                    }
                    
                    var sIndex = DefaultList.IndexOf(startIndex);
                    var eIndex = DefaultList.IndexOf(endIndex);

                    if (i > sIndex - 1 && i < eIndex + 1) continue;

                    try
                    {
                        list.Add(SignalsList[i].Trim());
                    }
                    catch (Exception exception)
                    {

                    }
                }
            }
            
        }

        public void PreparationListSignatures()
        {
            int j = 0;
            int count = 88 - DiskretsCount;

            for (int i = 0; i < DefaultList.Count; i++)
            {
                SignalsList[i] = AddSpaceInString(SignalsList[i]);
            }

            for (int i = 0; i < SignalsList.Count; i++)
            {
                try
                {
                    if (i > DiskretsCount && i <= DiskretsCount + count || i == 0)
                        continue;

                    if (_signaturesSignalsDGV.Rows[j].Cells[2].Value.ToString() != "")
                    {
                        if (_signaturesSignalsDGV.Rows[j].Cells[2].Value.ToString().Length < 10)
                        {
                            SignalsList[i] = AddSpaceInString(_signaturesSignalsDGV.Rows[j].Cells[2].Value.ToString());
                        }
                        else
                        {
                            SignalsList[i] = _signaturesSignalsDGV.Rows[j].Cells[2].Value.ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                }

                j++;
            }
        }

        private void _writeSignalsInDeviceButton_Click(object sender, System.EventArgs e)
        {
            if (MessageBox.Show("Восстановить базовые сигналы?", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                int j = 0;
                int count = 88 - DiskretsCount;
                try
                {
                    for (int i = 0; i < DefaultList.Count; i++)
                    {
                        if (i > DiskretsCount && i <= DiskretsCount + count || i == 0)
                            continue;
                        try
                        {
                            _signaturesSignalsDGV.Rows[j].Cells[2].Value = DefaultList[i];
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }

                        j++;
                    }
                }
                catch (Exception exc){}
            }
        }

        public void AcceptSignals()
        {
            for (int i = 0; i < _signaturesSignalsDGV.RowCount; i++)
            {
                if ((string) _signaturesSignalsDGV.Rows[i].Cells[2].Value != "")
                {
                    SignalsList[i] = (string) _signaturesSignalsDGV.Rows[i].Cells[2].Value;
                    _signaturesSignalsDGV.Rows[i].Cells[1].Value = SignalsList[i];
                    _signaturesSignalsDGV.Rows[i].Cells[2].Value = "";
                }
            }
            AcceptedSignals?.Invoke();
        }

        private void _acceptButton_Click(object sender, EventArgs e)
        {
            AcceptSignals();
        }

        private void _signaturesSignalsDGV_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
    }
}
