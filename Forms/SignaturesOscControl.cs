﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BEMN.Forms
{
    public partial class SignaturesOscControl : UserControl
    {
        public event Action CopySignalsAction;

        public List<string> Messages { get; set; }
        private List<string> _defaultList;

        public List<string> DefaultList => _defaultList;

        public SignaturesOscControl()
        {
            InitializeComponent();

            //_oscSignatures.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            //_oscSignatures.AutoResizeColumns();

            //_oscSignatures.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            //_oscSignatures.AutoResizeRows(DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders);
        }

        public SignaturesForOsc SettingsSignaturesForOsc(string deviceType, List<string> list, double deviceVersion)
        {
            SignaturesForOsc signatures = new SignaturesForOsc(deviceType);
            signatures.Signatures.MessagesList = list;
            signatures.Signatures.Count = list.Count;
            signatures.Version = deviceVersion;
            return signatures;
        }

        public void ClearDgv()
        {
            for (int i = 0; i < _oscSignatures.RowCount; i++)
            {
                _oscSignatures.Rows[i].Cells[2].Value = "";
            }
        }

        public void FillDefaultList(int dCount, int chCount)
        {
            _defaultList = new List<string>();

            for (int i = 0; i < dCount; i++)
            {
                _defaultList.Add($"Д{i + 1}");
            }

            for (int i = 0; i < chCount; i++)
            {
                _defaultList.Add($"K{i + 1}");
            }
        }

        public void PreparationListSignatures()
        {
            Messages = new List<string>();
            
            for (int i = 0; i < _oscSignatures.RowCount; i++)
            {
                try
                {
                    if (_oscSignatures.Rows[i].Cells[2].Value.ToString() != "")
                    {
                        Messages.Add(_oscSignatures.Rows[i].Cells[2].Value.ToString());
                    }
                    else
                    {
                        Messages.Add(_oscSignatures.Rows[i].Cells[1].Value.ToString());
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }

        public void FillSignaturesSignalsDgv(List<string> messages, int dCount, List<string> oscDataGridMessages = null, bool isLoaded = false, List<string> signalsInDevice = null, List<string> signalsUser = null)
        {
            _oscSignatures.Rows.Clear();

            try
            {
                _oscSignatures.Rows.Add(_defaultList.Count);

                for (int i = 0; i < _defaultList.Count; i++)
                {
                    _oscSignatures.Rows[i].Cells[0].Value = _defaultList[i];
                }

                if (messages != null)
                {
                    for (int i = 0; i < messages.Count; i++)
                    {
                        if (i < dCount)
                        {
                            _oscSignatures.Rows[i].Cells[1].Value = messages[i];
                            continue;
                        }

                        if (oscDataGridMessages != null)
                        {
                            if (oscDataGridMessages[i - dCount] != "")
                            {
                                _oscSignatures.Rows[i].Cells[1].Value = oscDataGridMessages[i - dCount];
                            }
                            else if (oscDataGridMessages[i - dCount] != "НЕТ")
                            {
                                _oscSignatures.Rows[i].Cells[1].Value = messages[i];
                            }
                        }
                        else
                        {
                            _oscSignatures.Rows[i].Cells[1].Value = messages[i];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < _defaultList.Count; i++)
                    {
                        if (i < dCount)
                        {
                            _oscSignatures.Rows[i].Cells[1].Value = _defaultList[i];
                            continue;
                        }

                        if (oscDataGridMessages != null)
                        {
                            if (oscDataGridMessages[i - dCount] != "")
                            {
                                _oscSignatures.Rows[i].Cells[1].Value = oscDataGridMessages[i - dCount];
                            }
                            else if (oscDataGridMessages[i - dCount] != "НЕТ")
                            {
                                _oscSignatures.Rows[i].Cells[1].Value = _defaultList[i];
                            }
                        }
                        else
                        {
                            _oscSignatures.Rows[i].Cells[1].Value = _defaultList[i];
                        }
                    }
                }
                
                for (int i = 0; i < _defaultList.Count; i++)
                {
                    if (isLoaded)
                    {
                        if (signalsInDevice != null)
                        {
                            _oscSignatures.Rows[i].Cells[1].Value = signalsInDevice[i];
                        }
                        else
                        {
                            _oscSignatures.Rows[i].Cells[1].Value = _defaultList[i];
                        }

                        if (signalsUser != null)
                        {
                            _oscSignatures.Rows[i].Cells[2].Value = signalsUser[i];
                        }
                        else
                        {
                            _oscSignatures.Rows[i].Cells[2].Value = _defaultList[i];
                        }
                    }
                    else
                    {
                        _oscSignatures.Rows[i].Cells[2].Value = "";
                    }
                }

            }
            catch (Exception ex)
            {
                for (int i = 0; i < _defaultList.Count; i++)
                {
                    _oscSignatures.Rows.Add(_defaultList[i], "");
                }
            }
        }

        public List<string> SaveToFile(out List<string> value, int cell)
        {
            value = new List<string>();

            for (int i = 0; i < _oscSignatures.RowCount; i++)
            {
                value.Add((string)_oscSignatures.Rows[i].Cells[cell].Value);
            }

            return value;
        }

        private void _oscSignatures_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void _resetSignalsButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Восстановить базовые сигналы?", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                for (int i = 0; i < _oscSignatures.RowCount; i++)
                {
                    try
                    {
                        _oscSignatures.Rows[i].Cells[2].Value = DefaultList[i];
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void _copySignaturesButtons_Click(object sender, EventArgs e)
        {
            CopySignalsAction.Invoke();;
        }

        public void CopySignalsInDataGrid(List<string> diskretsSignals, List<string> oscSignals)
        {
            for (int i = 0; i < diskretsSignals.Count; i++)
            {
                _oscSignatures.Rows[i].Cells[2].Value = diskretsSignals[i];
            }

            for (int i = 0; i < oscSignals.Count; i++)
            {
                _oscSignatures.Rows[i + diskretsSignals.Count].Cells[2].Value = oscSignals[i];
            }
        }
    }
}
