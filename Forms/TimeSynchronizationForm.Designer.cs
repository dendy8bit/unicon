﻿namespace BEMN.Forms
{
    partial class TimeSynchronizationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this._portBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this._periodBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._currentTime = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._lastSinchronization = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this._state = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this._dataTimeOldSync = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Порт";
            // 
            // _portBox
            // 
            this._portBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._portBox.FormattingEnabled = true;
            this._portBox.Location = new System.Drawing.Point(178, 18);
            this._portBox.Name = "_portBox";
            this._portBox.Size = new System.Drawing.Size(100, 21);
            this._portBox.TabIndex = 1;
            this._portBox.DropDown += new System.EventHandler(this._portBox_DropDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Период, с";
            // 
            // _periodBox
            // 
            this._periodBox.Location = new System.Drawing.Point(178, 45);
            this._periodBox.Name = "_periodBox";
            this._periodBox.Size = new System.Drawing.Size(100, 20);
            this._periodBox.TabIndex = 3;
            this._periodBox.Text = "60";
            this._periodBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Текущее время";
            // 
            // _currentTime
            // 
            this._currentTime.AutoSize = true;
            this._currentTime.Location = new System.Drawing.Point(177, 75);
            this._currentTime.Name = "_currentTime";
            this._currentTime.Size = new System.Drawing.Size(0, 13);
            this._currentTime.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(12, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(148, 32);
            this.label5.TabIndex = 6;
            this.label5.Text = "Последняя синхронизация производилась в";
            // 
            // _lastSinchronization
            // 
            this._lastSinchronization.AutoSize = true;
            this._lastSinchronization.Location = new System.Drawing.Point(177, 117);
            this._lastSinchronization.Name = "_lastSinchronization";
            this._lastSinchronization.Size = new System.Drawing.Size(0, 13);
            this._lastSinchronization.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 183);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(266, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Запуск синхронизации";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // _state
            // 
            this._state.AutoSize = true;
            this._state.Location = new System.Drawing.Point(177, 142);
            this._state.Name = "_state";
            this._state.Size = new System.Drawing.Size(0, 13);
            this._state.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 142);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Состояние";
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(12, 212);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(266, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Остановка синхронизации";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // _dataTimeOldSync
            // 
            this._dataTimeOldSync.AutoSize = true;
            this._dataTimeOldSync.Location = new System.Drawing.Point(13, 160);
            this._dataTimeOldSync.Name = "_dataTimeOldSync";
            this._dataTimeOldSync.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._dataTimeOldSync.Size = new System.Drawing.Size(212, 17);
            this._dataTimeOldSync.TabIndex = 12;
            this._dataTimeOldSync.Text = "Синх. для МР600 версий 2.10 и ниже";
            this._dataTimeOldSync.UseVisualStyleBackColor = true;
            // 
            // TimeSynchronizationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 241);
            this.Controls.Add(this._dataTimeOldSync);
            this.Controls.Add(this.button2);
            this.Controls.Add(this._state);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button1);
            this.Controls.Add(this._lastSinchronization);
            this.Controls.Add(this.label5);
            this.Controls.Add(this._currentTime);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._periodBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._portBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TimeSynchronizationForm";
            this.Text = "Синхронизация времени";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox _portBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _periodBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label _currentTime;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label _lastSinchronization;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label _state;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox _dataTimeOldSync;
    }
}