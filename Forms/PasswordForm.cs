﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Exception = System.Exception;

namespace BEMN.Forms
{
    public partial class PasswordForm : Form
    {
        private string _password; 
        public string Password => _password;
        private bool isEngLetter;

        public PasswordForm()
        {
            InitializeComponent();
            passwordTextBox.MaxLength = 4;
        }

        private void _confirmPasswordButton_Click(object sender, EventArgs e)
        {
            _password = passwordTextBox.Text;
            Close();
        }

        private void passwordTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (!((e.KeyChar >= 'А' && e.KeyChar <= 'Я')
            //      || (e.KeyChar >= '0' && e.KeyChar <= '9')
            //      || e.KeyChar == 'ё'
            //      || e.KeyChar == 'Ё'
            //      || e.KeyChar == '\b'))
            //{
            //    try
            //    {
            //        if ((e.KeyChar >= 'A' && e.KeyChar <= 'z'))
            //        {
            //            toolTip.Show("Текст должен быть на русском языке", passwordTextBox, -20, -passwordTextBox.Height);
            //            isEngLetter = true;
            //            return;
            //        }

            //        string c = e.KeyChar.ToString();
            //        e.KeyChar = Convert.ToChar(c.ToUpper());
            //    }

            //    catch (Exception exception)
            //    {
            //    }
                
                
            //}
        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (passwordTextBox.Text.Length > 4 || isEngLetter)
                {
                    isEngLetter = false;
                    passwordTextBox.Text = passwordTextBox.Text.Remove(passwordTextBox.Text.Length - 1);
                    passwordTextBox.SelectionStart = passwordTextBox.Text.Length;
                }

                _confirmPasswordButton.Enabled = passwordTextBox.Text.Length >= 4;
            }
            catch (Exception ex)
            {
            }
            
        }
    }
}
