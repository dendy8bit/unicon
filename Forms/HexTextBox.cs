using System;
using System.Windows.Forms;


namespace BEMN.Forms
{         
	/// <summary>
	/// Summary description for HexTextBox.
	/// </summary>
	public class HexTextbox : TextBox
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		
		protected string oldDecText = "0";
		protected string oldHexText = "00";
		protected int limit = 65536;
		protected bool hexBase = true;
		
		public int Limit
		{
			get => this.limit;
			set => limit = value;		
		}

		public bool HexBase
	    {
	        get => hexBase;
			set 
			{
				OnHexBaseChange(hexBase, value);
				hexBase = value;					
			}
		}

		
		public HexTextbox()
		{
			this.Size = new System.Drawing.Size(104, 20);
		
            this.Leave += new EventHandler(HexTextbox_Leave);
            this.KeyDown += new KeyEventHandler(HexTextbox_KeyDown);
		}

        void HexTextbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Correct();
            }
        }

        void HexTextbox_Leave(object sender, EventArgs e)
        {
            Correct();
        }

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		public bool Convert(out int number)
		{
			bool bRet = true;
			try
			{
				if (HexBase)
				{
					number = System.Convert.ToInt32(Text,16);
				}
				else
				{
					number = System.Convert.ToInt32(Text);
				}
			}
			catch(Exception)
			{
				number = 0;
				if (HexBase)
				{
					Text = oldHexText;
				}
				else
				{
					Text = oldDecText;
				}
				bRet = false;
			}
			
			return bRet;
		}

        private int number;
        public int Number => number;

		public void Correct()
		{
		    if (Text.Length == 0)
		    {
		        this.number = 0;
		        this.Text = "0";
		        return;
		    }

		    if (Convert(out number))
		    {
		        if (limit < number | number < 0)
		        {
		            if (hexBase)
		            {
		                Text = oldHexText;
		            }
		            else
		            {
		                Text = oldDecText;
		            }
		        }
		        else
		        {
		            if (hexBase)
		            {
		                oldHexText = Text;
		            }
		            else
		            {
		                oldDecText = Text;
		            }
		        }
		    }
		}
		

		protected void OnHexBaseChange(bool bOld,bool bNew)
		{
            if (this.Text != null && this.Text.Length != 0)
            {
                if (bOld != bNew && Text.Length != 0)
                {
                    //DEC->HEX
                    Correct();

                    if (bNew)
                    {
                        Text = System.Convert.ToInt32(Text).ToString("X");
                    }
                    else
                    {
                        Text = System.Convert.ToInt32(Text, 16).ToString("");
                    }
                }    
            }	
		}
	}
}
