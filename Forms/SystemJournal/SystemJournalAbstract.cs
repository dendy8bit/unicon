﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using System.Collections.Generic;

namespace BEMN.Forms.SystemJournal
{
    /// <summary>
    /// Общий класс для всех Журналов Системы
    /// </summary>
    public abstract class SystemJournalAbstract : StructBase
    {
        #region [Private fields]

        [Layout(0)] protected ushort _year;
        [Layout(1)] protected ushort _month;
        [Layout(2)] protected ushort _date;
        [Layout(3)] protected ushort _hour;
        [Layout(4)] protected ushort _minute;
        [Layout(5)] protected ushort _second;
        [Layout(6)] protected ushort _millisecond;
        [Layout(7)] protected ushort _moduleErrorCode;
        [Layout(8)] protected ushort _message;

        #endregion [Private fields]

        #region [Properties]
        public List<string> MessagesList { get; set; }
        /// <summary>
        /// true если во всех полях 0, условие конца ЖС
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                var sum = this.Year +
                          this.Month +
                          this.Date +
                          this.Hour +
                          this.Minute +
                          this.Second +
                          this.Millisecond +
                          this.ModuleErrorCode +
                          this.Message;
                return sum == 0;
            }
        }

        public ushort Year
        {
            get { return this._year; }
            set { this._year = value; }
        }

        public ushort Month
        {
            get { return this._month; }
            set { this._month = value; }
        }

        public ushort Date
        {
            get { return this._date; }
            set { this._date = value; }
        }

        public ushort Hour
        {
            get { return this._hour; }
            set { this._hour = value; }
        }

        public ushort Minute
        {
            get { return this._minute; }
            set { this._minute = value; }
        }

        public ushort Second
        {
            get { return this._second; }
            set { this._second = value; }
        }

        public ushort Millisecond
        {
            get { return this._millisecond; }
            set { this._millisecond = value; }
        }

        public ushort ModuleErrorCode
        {
            get { return this._moduleErrorCode; }
            set { this._moduleErrorCode = value; }
        }

        public ushort Message
        {
            get { return this._message; }
            set { this._message = value; }
        }

        #region [Abstract Properties]

        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public abstract string GetRecordTime { get; }
        
        /// <summary>
        /// Текст сообщения
        /// </summary>
        public abstract string GetRecordMessage { get; }

        #endregion [Abstract Property]

        #endregion [Properties]
    }
}
