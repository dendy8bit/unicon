﻿namespace BEMN.Forms.SystemJournal
{
    partial class Mr900SystemJournalBaseForm<T>
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._systemJournalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._readJournalButton = new System.Windows.Forms.Button();
            this._loadJournalButton = new System.Windows.Forms.Button();
            this._saveJournalButton = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._saveXmlDialog = new System.Windows.Forms.SaveFileDialog();
            this._openJournalDialog = new System.Windows.Forms.OpenFileDialog();
            this._saveToHtmlBtn = new System.Windows.Forms.Button();
            this._saveHtmlDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this._systemJournalGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _systemJournalGrid
            // 
            this._systemJournalGrid.AllowUserToAddRows = false;
            this._systemJournalGrid.AllowUserToDeleteRows = false;
            this._systemJournalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._systemJournalGrid.BackgroundColor = System.Drawing.Color.White;
            this._systemJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._systemJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msgCol});
            this._systemJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._systemJournalGrid.Name = "_systemJournalGrid";
            this._systemJournalGrid.RowHeadersVisible = false;
            this._systemJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._systemJournalGrid.Size = new System.Drawing.Size(573, 458);
            this._systemJournalGrid.TabIndex = 14;
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "Номер";
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 30;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "Время";
            this._timeCol.HeaderText = "Время";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 46;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._msgCol.DataPropertyName = "Сообщение";
            this._msgCol.HeaderText = "Сообщение";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _readJournalButton
            // 
            this._readJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readJournalButton.Location = new System.Drawing.Point(3, 464);
            this._readJournalButton.Name = "_readJournalButton";
            this._readJournalButton.Size = new System.Drawing.Size(137, 23);
            this._readJournalButton.TabIndex = 21;
            this._readJournalButton.Text = "Прочитать журнал";
            this._readJournalButton.UseVisualStyleBackColor = true;
            this._readJournalButton.Click += new System.EventHandler(this._readJournalButton_Click);
            // 
            // _loadJournalButton
            // 
            this._loadJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._loadJournalButton.Location = new System.Drawing.Point(289, 464);
            this._loadJournalButton.Name = "_loadJournalButton";
            this._loadJournalButton.Size = new System.Drawing.Size(137, 23);
            this._loadJournalButton.TabIndex = 23;
            this._loadJournalButton.Text = "Загрузить";
            this._loadJournalButton.UseVisualStyleBackColor = true;
            this._loadJournalButton.Click += new System.EventHandler(this._loadJournalButton_Click);
            // 
            // _saveJournalButton
            // 
            this._saveJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveJournalButton.Location = new System.Drawing.Point(146, 464);
            this._saveJournalButton.Name = "_saveJournalButton";
            this._saveJournalButton.Size = new System.Drawing.Size(137, 23);
            this._saveJournalButton.TabIndex = 22;
            this._saveJournalButton.Text = "Сохранить";
            this._saveJournalButton.UseVisualStyleBackColor = true;
            this._saveJournalButton.Click += new System.EventHandler(this._saveJournalButton_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 490);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(575, 22);
            this.statusStrip1.TabIndex = 24;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(16, 17);
            this._statusLabel.Text = "...";
            // 
            // _saveXmlDialog
            // 
            this._saveXmlDialog.DefaultExt = "xml";
            this._saveXmlDialog.FileName = "Журнал Системы";
            this._saveXmlDialog.Filter = "Журнал Системы | *.xml";
            this._saveXmlDialog.Title = "Сохранить  журнал системы для МР90x";
            // 
            // _openJournalDialog
            // 
            this._openJournalDialog.DefaultExt = "xml";
            this._openJournalDialog.RestoreDirectory = true;
            // 
            // _saveToHtmlBtn
            // 
            this._saveToHtmlBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveToHtmlBtn.Location = new System.Drawing.Point(432, 464);
            this._saveToHtmlBtn.Name = "_saveToHtmlBtn";
            this._saveToHtmlBtn.Size = new System.Drawing.Size(137, 23);
            this._saveToHtmlBtn.TabIndex = 23;
            this._saveToHtmlBtn.Text = "Сохранить в HTML";
            this._saveToHtmlBtn.UseVisualStyleBackColor = true;
            this._saveToHtmlBtn.Visible = false;
            // 
            // _saveHtmlDialog
            // 
            this._saveHtmlDialog.DefaultExt = "xml";
            this._saveHtmlDialog.FileName = "Журнал Системы HTML";
            this._saveHtmlDialog.Filter = "Журнал Системы | *.html";
            this._saveHtmlDialog.Title = "Сохранить  журнал системы для МР90x";
            // 
            // Mr900SystemJournalBaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 512);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._readJournalButton);
            this.Controls.Add(this._saveToHtmlBtn);
            this.Controls.Add(this._loadJournalButton);
            this.Controls.Add(this._saveJournalButton);
            this.Controls.Add(this._systemJournalGrid);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(450, 550);
            this.Name = "Mr900SystemJournalBaseForm";
            this.Text = "Mr902SystemJournalForm";
            this.Load += new System.EventHandler(this.Mr902SystemJournalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._systemJournalGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView _systemJournalGrid;
        private System.Windows.Forms.Button _readJournalButton;
        private System.Windows.Forms.Button _loadJournalButton;
        private System.Windows.Forms.Button _saveJournalButton;
        private System.Windows.Forms.StatusStrip statusStrip1;
        protected System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.SaveFileDialog _saveXmlDialog;
        private System.Windows.Forms.OpenFileDialog _openJournalDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        public System.Windows.Forms.Button _saveToHtmlBtn;
        protected System.Windows.Forms.SaveFileDialog _saveHtmlDialog;
    }
}