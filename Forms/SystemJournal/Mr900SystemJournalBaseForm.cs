﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Properties;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.Forms.SystemJournal
{
    public abstract partial class Mr900SystemJournalBaseForm<T> : Form, IFormView where T: SystemJournalAbstract, new()
    {
        #region [Constants]
        protected const string RECORDS_IN_JOURNAL = "Записей в журнале - {0}";
        protected const string JOURNAL_SAVED = "Журнал сохранён";
        protected const string READ_FAIL = "Невозможно прочитать журнал";
        protected const string TABLE_NAME_SYS = "МР901_журнал_системы";
        protected const string NUMBER_SYS = "Номер";
        protected const string TIME_SYS = "Время";
        protected const string MESSAGE_SYS = "Сообщение";
        protected const string SYSTEM_JOURNAL = "Журнал системы";
        protected const string JOURNAL_IS_EMPTY = "Журнал пуст";
        #endregion [Constants]


        #region [Private fields]
        protected Device _device;
        private readonly MemoryEntity<OneWordStruct> _refreshSystemJournal;
        private readonly MemoryEntity<T> _systemJournal;
        protected DataTable _dataTable;
        private int _recordNumber;
        #endregion [Private fields]


        #region [Ctor's]
        protected Mr900SystemJournalBaseForm()
        {
            this.InitializeComponent();
        }

        protected Mr900SystemJournalBaseForm(Device device, MemoryEntity<OneWordStruct> refreshSystemJournal, MemoryEntity<T> systemJournal)
        {
            this.InitializeComponent();
            this._device = device;

            this._systemJournal = systemJournal;
            this._refreshSystemJournal = refreshSystemJournal;
        
            this._refreshSystemJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.StartReadJournal);
            this._refreshSystemJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
            this._systemJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);

            this._saveXmlDialog.FileName = string.Format("{0} Журнал Системы", device.DeviceType);
            this._saveXmlDialog.Filter = string.Format("{0} Журнал Системы|*.xml", device.DeviceType);
            this._saveXmlDialog.Title = string.Format("Сохранить  журнал системы для {0}", device.DeviceType);

            this._openJournalDialog.FileName = string.Format("{0} Журнал Системы", device.DeviceType);
            this._openJournalDialog.Filter = string.Format("{0} ЖС (*.xml)|*.xml|{0} ЖС (*.bin)|*.bin", device.DeviceType);
            this._openJournalDialog.Title = string.Format("Открыть журнал системы для {0}", device.DeviceType);

            this._saveHtmlDialog.FileName = string.Format("{0} Журнал Системы", device.DeviceType);
            this._saveHtmlDialog.Filter = string.Format("{0} Журнал Системы|*.html", device.DeviceType);
            this._saveHtmlDialog.Title = string.Format("Сохранить  журнал системы для {0}", device.DeviceType);

            this._dataTable = this.GetJournalDataTable();
            this._systemJournalGrid.DataSource = this._dataTable;
        }
        #endregion [Ctor's]


        #region [IFormView Members]
        
        public abstract Type FormDevice { get; }
        public bool Multishow { get; private set; }


        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public abstract Type ClassType { get; }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return  Resources.js; }
        }

        public string NodeName
        {
            get { return SYSTEM_JOURNAL; }
        }
        #endregion [IFormView Members]


        #region [Properties]
        /// <summary>
        /// Счётчик сообщений
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
            set
            {
                this._recordNumber = value;
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
            }
        }
        #endregion [Properties]

        
        #region [Help members]
        private void FailReadJournal()
        {
            this.RecordNumber = 0;
            this._statusLabel.Text = READ_FAIL;
        }

        private void StartReadJournal()
        {
            this.RecordNumber = 0;
            this._systemJournal.LoadStructCycleWhile(a => a.IsEmpty);
        }

        private void ReadRecord()
        {  
            if (!this._systemJournal.Value.IsEmpty)
            {
                this.RecordNumber++;
                this._dataTable.Rows.Add(
                    this.RecordNumber.ToString(CultureInfo.InvariantCulture),
                    this._systemJournal.Value.GetRecordTime, 
                    this._systemJournal.Value.GetRecordMessage);
            }     
        }

        private DataTable GetJournalDataTable()
        {
            var table = new DataTable(TABLE_NAME_SYS);
            table.Columns.Add(NUMBER_SYS);
            table.Columns.Add(TIME_SYS);
            table.Columns.Add(MESSAGE_SYS);
            return table;
        }

        private void SaveJournalToFile()
        {
            if (DialogResult.OK == this._saveXmlDialog.ShowDialog())
            {
                this._dataTable.WriteXml(this._saveXmlDialog.FileName);
                this._statusLabel.Text = JOURNAL_SAVED;

            }
        }

        private void LoadJournalFromFile()
        { 
            if (DialogResult.OK == this._openJournalDialog.ShowDialog())
            {
                this._dataTable.Clear();
                if (Path.GetExtension(this._openJournalDialog.FileName).ToLower().Contains("bin"))
                {
                    byte[] file = File.ReadAllBytes(this._openJournalDialog.FileName);
                    T journal = new T();
                    int size = journal.GetSize();
                    int index = 0;
                    this.RecordNumber = 0;
                    while (index < file.Length)
                    {
                        List<byte> journalBytes = new List<byte>();
                        journalBytes.AddRange(Common.SwapListItems(file.Skip(index).Take(size)));
                        journal.InitStruct(journalBytes.ToArray());
                        this._systemJournal.Value = journal;
                        this.ReadRecord();
                        index += size;
                    }
                }
                else
                {
                    this._dataTable.ReadXml(this._openJournalDialog.FileName);
                    this.RecordNumber = this._dataTable.Rows.Count;
                    this._systemJournalGrid.Refresh();
                }
            }
        }
        #endregion [Help members]


        #region [Events Handlers]
        private void Mr902SystemJournalForm_Load(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._refreshSystemJournal.SaveStruct();
        }

        private void _readJournalButton_Click(object sender, EventArgs e)
        {
            this._dataTable.Clear();
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._refreshSystemJournal.SaveStruct();
        }

        private void _saveJournalButton_Click(object sender, EventArgs e)
        {
            this.SaveJournalToFile();
        }

        private void _loadJournalButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.LoadJournalFromFile();
            }
            catch
            {
                MessageBox.Show("Невозможно открыть файл.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion [Events Handlers]
    }
}
