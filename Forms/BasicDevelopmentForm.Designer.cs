namespace BEMN.Forms.IDE
{
    partial class BasicDevelopmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary >
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {                
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
            catch (System.ObjectDisposedException e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);
            }
            
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._sourcesTabControl = new Crownwood.Magic.Controls.TabControl();
            this.inertButton1 = new Crownwood.Magic.Controls.InertButton();
            this._sourcesTabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // _sourcesTabControl
            // 
            this._sourcesTabControl.Appearance = Crownwood.Magic.Controls.TabControl.VisualAppearance.MultiDocument;
            this._sourcesTabControl.Controls.Add(this.inertButton1);
            this._sourcesTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._sourcesTabControl.HideTabsMode = Crownwood.Magic.Controls.TabControl.HideTabsModes.ShowAlways;
            this._sourcesTabControl.IDEPixelBorder = false;
            this._sourcesTabControl.Location = new System.Drawing.Point(0, 0);
            this._sourcesTabControl.Name = "_sourcesTabControl";
            this._sourcesTabControl.Size = new System.Drawing.Size(629, 435);
            this._sourcesTabControl.TabIndex = 0;
            this._sourcesTabControl.ClosePressed += new System.EventHandler(this._sourcesTabControl_ClosePressed);
            // 
            // inertButton1
            // 
            this.inertButton1.Location = new System.Drawing.Point(230, 181);
            this.inertButton1.Name = "inertButton1";
            this.inertButton1.Size = new System.Drawing.Size(75, 23);
            this.inertButton1.TabIndex = 4;
            this.inertButton1.Text = "inertButton1";
            // 
            // BasicDevelopmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 435);
            this.Controls.Add(this._sourcesTabControl);
            this.Name = "BasicDevelopmentForm";
            this.Text = "Разработка";
            
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BasicDevelopmentForm_FormClosing);
            this._sourcesTabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected  Crownwood.Magic.Controls.TabControl _sourcesTabControl;
        private Crownwood.Magic.Controls.InertButton inertButton1;

        public Crownwood.Magic.Controls.TabControl SourcesTabControl
        {
            get
            {
                return _sourcesTabControl;
            }
        }

    }
}