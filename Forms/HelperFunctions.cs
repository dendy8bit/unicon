﻿using System.Collections.Generic;
using System.Linq;
using BEMN.Devices;
using BEMN.Devices.Structures;

namespace BEMN.Forms
{
    public static class HelperFunctions
    {
        public static List<Device.slot> SetSlots(ushort[] values, ushort startAdress, int slotLen = StructBase.MAX_SLOT_LENGHT_DEFAULT)
        {
            List<Device.slot> slotsRet = new List<Device.slot>();
            bool isSlotsArr = values.Length > slotLen;
            if (isSlotsArr)
            {
                int arrayLength = values.Length/slotLen;
                int lastSlotLength = values.Length%slotLen;
                Device.slot[] slots;
                ushort startAddr = startAdress;
                if (lastSlotLength != 0)
                {
                    arrayLength++;
                    slots = new Device.slot[arrayLength];

                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort) (startAddr + slotLen));
                        startAddr += (ushort) slotLen;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort) (startAddr + lastSlotLength));
                    slotsRet.AddRange(slots.ToArray());
                }
                else
                {
                    slots = new Device.slot[arrayLength];
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort) (startAddr + slotLen));
                        startAddr += (ushort) slotLen;
                    }
                    slotsRet.AddRange(slots.ToArray());
                }
            }
            else
            {
                slotsRet.Add(new Device.slot(startAdress, (ushort) (startAdress + values.Length)));
            }
            return slotsRet;
        }
    }
}
