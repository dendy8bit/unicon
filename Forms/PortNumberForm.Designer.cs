﻿namespace BEMN.Forms
{
    partial class PortNumberForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this._portNumberComboBox = new System.Windows.Forms.ComboBox();
            this._createProcessButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Выберите порт";
            // 
            // _portNumberComboBox
            // 
            this._portNumberComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._portNumberComboBox.FormattingEnabled = true;
            this._portNumberComboBox.Location = new System.Drawing.Point(134, 12);
            this._portNumberComboBox.Name = "_portNumberComboBox";
            this._portNumberComboBox.Size = new System.Drawing.Size(82, 21);
            this._portNumberComboBox.TabIndex = 1;
            // 
            // _createProcessButton
            // 
            this._createProcessButton.Location = new System.Drawing.Point(29, 44);
            this._createProcessButton.Name = "_createProcessButton";
            this._createProcessButton.Size = new System.Drawing.Size(168, 23);
            this._createProcessButton.TabIndex = 2;
            this._createProcessButton.Text = "Выполнить команду";
            this._createProcessButton.UseVisualStyleBackColor = true;
            this._createProcessButton.Click += new System.EventHandler(this._createProcessButton_Click);
            // 
            // PortNumberForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(228, 79);
            this.Controls.Add(this._createProcessButton);
            this.Controls.Add(this._portNumberComboBox);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(244, 118);
            this.MinimumSize = new System.Drawing.Size(244, 118);
            this.Name = "PortNumberForm";
            this.Text = "Номер порта";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox _portNumberComboBox;
        private System.Windows.Forms.Button _createProcessButton;
    }
}