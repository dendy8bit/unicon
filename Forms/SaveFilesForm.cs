using System.Collections.Generic;
using System.Windows.Forms;

namespace BEMN.Forms
{
    public partial class SaveSourceDialog : Form
    {
        public SaveSourceDialog()
        {
            InitializeComponent();
        }

        public List<string> FilesList
        {
            get
            {
                List<string> ret = new List<string>(_fileList.Items.Count);
                for (int i = 0; i < _fileList.Items.Count; i++)
                {

                    ret.Add(_fileList.Items[i] as string);

                }
                return ret;
            }
            set
            {
                for (int i = 0; i < value.Count; i++)
                {
                    _fileList.Items.Add(value[i]);
                }
            }
        }
    }

}