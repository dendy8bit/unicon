﻿using System;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MBServer;
using SerialServer;

namespace BEMN.Forms
{
    public partial class TimeSynchronizationForm : Form
    {
        private readonly Timer _currentTimeTimer;
        private readonly Timer _synTimer;
        private Modbus _modbus;
        private MemoryEntity<DateTimeStruct> _memoryEntity;
        private MemoryEntity<DateTimeStructOld> _memoryEntityOld;
        private DateTimeStruct _dateTime = new DateTimeStruct();
        private DateTimeStructOld _dateTimeOld = new DateTimeStructOld();

        public TimeSynchronizationForm()
        {
            this.InitializeComponent();
            this._currentTimeTimer = new Timer {Enabled = false,Interval = 1000};
            this._synTimer = new Timer{Enabled = false};
            this._synTimer.Tick += this._synTimer_Tick;
            this._currentTimeTimer.Tick += this._currentTimeTimer_Tick;
            this._memoryEntityOld = new MemoryEntity<DateTimeStructOld>("Синхронизация времени", null, 0x200);
            this._memoryEntity = new MemoryEntity<DateTimeStruct>("Синхронизация времени", null, 0x200);
            VisibleChanged += this.TimeSynchronizationForm_VisibleChanged;
            FormClosing += this.TimeSynchronizationForm_FormClosing;
        }

        private void _synTimer_Tick(object sender, EventArgs e)
        {
            this.Sync();
        }

        private void Sync()
        {
            try
            {
                if (this._dataTimeOldSync.Checked)
                {
                    DateTime currentTimeOld = this._dateTimeOld.SetDateTimeNow();
                    this._lastSinchronization.Text = currentTimeOld.ToString();
                    this._memoryEntityOld.Value = this._dateTimeOld;
                    this._memoryEntityOld.SaveStruct();
                }
                else
                {
                    DateTime currentTime = this._dateTime.SetDateTimeNow();
                    this._lastSinchronization.Text = currentTime.ToString();
                    this._memoryEntity.Value = this._dateTime;
                    this._memoryEntity.SaveStruct();
                }
                
            }
            catch (Exception)
            {
                MessageBox.Show("Что-то пошло не так!", "OOOps!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        void _currentTimeTimer_Tick(object sender, EventArgs e)
        {
            this._currentTime.Text = DateTime.Now.ToString();
        }

        private void TimeSynchronizationForm_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                this._currentTimeTimer.Start();
                try
                {
                    this._portBox.DataSource = Modbus.SerialServer.ExistPorts;
                    this._portBox.SelectedIndex = 0;
                }
                catch (Exception)
                {
                    MessageBox.Show("Порты не найдены");
                }

            }
            else
            {
                this._currentTimeTimer.Stop();
                this._synTimer.Stop();
                this._state.Text = "Синхронизация остановлена";
                //if (this._modbus != null)
                //{
                //    this._modbus.Dispose();
                //    this._modbus = null;
                //}
                this.button1.Enabled = true;
                this.button2.Enabled = false;
            }
        }

        private void TimeSynchronizationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
            e.Cancel = true;
        }

        private void _portBox_DropDown(object sender, EventArgs e)
        {
            try
            {
                this._portBox.DataSource = Modbus.SerialServer.ExistPorts;
            }
            catch (Exception){ }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int period = Convert.ToInt32(this._periodBox.Text);
                this._synTimer.Interval = period*1000;
            }
            catch (Exception)
            {
                this._state.Text = "Неверный период";
                return;
            }
            try
            {
                byte portNum = byte.Parse(this._portBox.SelectedItem.ToString());
                CPort port = Modbus.SerialServer.GetPort(portNum);
                this._modbus = new Modbus(port);
            }
            catch (Exception)
            {
                this._state.Text = "Неверный порт";
                return;
            }
            if (this._dataTimeOldSync.Checked)
            {
                this._memoryEntityOld.DeviceObj = new Device(this._modbus);
                this._state.Text = "Идёт синхронизация";

                this._synTimer.Start();
                this._dataTimeOldSync.Enabled = false;
                this.button1.Enabled = false;
                this.button2.Enabled = true;
                this.Sync();
            }
            else
            {
                this._memoryEntity.DeviceObj = new Device(this._modbus);
                this._state.Text = "Идёт синхронизация";

                this._synTimer.Start();
                this._dataTimeOldSync.Enabled = false;
                this.button1.Enabled = false;
                this.button2.Enabled = true;
                this.Sync();
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this._synTimer.Stop();
            this._state.Text = "Остановлено";
            //if (this._modbus != null)
            //{
            //    this._modbus.Dispose();
            //    this._modbus = null;
            //}
            this._dataTimeOldSync.Enabled = true;
            this.button1.Enabled = true;
            this.button2.Enabled = false;
        }
    }
}
