﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Forms.Properties;

namespace BEMN.Forms
{
    public partial class AboutDevice : Form
    {
        public AboutDevice()
        {
            InitializeComponent();
        }

        public AboutDevice(Device device)
        {
            InitializeComponent();

            var deviceName = device.DeviceType;

            switch (deviceName)
            {
                case "MR761":
                case "MR762":
                case "MR763":
                case "MR771":
                case "MR801":
                case "MR901":
                case "MR902":
                    pictureBox.Image = Resources.MRBIG;
                    _aboutDeviceLabel.Text = $"Устройство: {device.DeviceType}" +
                                             Environment.NewLine + Environment.NewLine +
                                             $"Версия: {device.DeviceVersion}" +
                                             Environment.NewLine + Environment.NewLine +
                                             $"Аппаратная часть: {device.DevicePlant}";
                    break;
            }
        }
    }
}
