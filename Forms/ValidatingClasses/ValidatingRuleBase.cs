﻿using System;
using System.Threading;

namespace BEMN.Forms.ValidatingClasses
{
    public abstract class ValidatingRuleBase<T> : IValidatingRule<T> where T : struct , IComparable<T>
    {
        public bool IsValid(string value)
        {
            try
            {
                return IsValid(Parse(value));
            }
            catch (Exception)
            {

                return false;
            }
        }
        public virtual bool IsValid(T value)
        {
            return (value.CompareTo(this.Min) >= 0) && (value.CompareTo(this.Max) <= 0);
        }

        protected virtual string PrepareString(string value)
        {
            var separator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            return value.Replace(",", separator).Replace(".", separator);
        }

        public abstract T Parse(string value);
        public abstract T Min { get; }
        public abstract T Max { get; }


        public abstract bool IsValidKey(char key);

        object IValidatingRule.Parse(string value)
        {
            return this.Parse(value);
        }

        object IValidatingRule.Min
        {
            get { return this.Min; }
        }

        object IValidatingRule.Max
        {
            get { return this.Max; }
        }

        public virtual string ErrorMessage
        {
            get { return string.Format("Диапазон {0} .. {1}", Min, Max); }
        }
    }
}
