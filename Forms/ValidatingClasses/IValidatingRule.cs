﻿using System;

namespace BEMN.Forms.ValidatingClasses
{
    public interface IValidatingRule<T> : IValidatingRule where T : struct, IComparable<T>
    {
        // bool IsValid(string value);
        bool IsValid(T value);
        new T Parse(string value);
        new T Min { get; }
        new T Max { get; }
    }

    public interface IValidatingRule
    {
        bool IsValid(string value);
        bool IsValidKey(char key);
        object Parse(string value);
        object Min { get; }
        object Max { get; }
        string ErrorMessage { get; }
    }
}
