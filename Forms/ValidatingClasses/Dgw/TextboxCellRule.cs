﻿namespace BEMN.Forms.ValidatingClasses.Dgw
{
    public class TextboxCellRule
    {
        private int _row;
        private int _column;
        private IValidatingRule _rule;

        public TextboxCellRule(int row, int column, IValidatingRule rule)
        {
            this._row = row;
            this._column = column;
            this.Rule = rule;
        }

        public int Row
        {
            get { return _row; }
        }

        public int Column
        {
            get { return _column; }
        }

        public IValidatingRule Rule
        {
            get { return _rule; }
            set { _rule = value; }
        }
    }
}
