﻿using System.Collections.Generic;
using System.Reflection;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Forms.ValidatingClasses.New
{
    public class InitData
    {
        private readonly SortedDictionary<LayoutAttribute, FieldInfo> _fields;
        private SortedDictionary<BindingPropertyAttribute, PropertyInfo> _properties;
        private List<int> _ignored;
        private readonly int _size = -1;

        public InitData(int size, SortedDictionary<LayoutAttribute, FieldInfo> fields,
            SortedDictionary<BindingPropertyAttribute, PropertyInfo> properties, List<int> ignored)
        {
            this._size = size;
            this._fields = fields;
            this._properties = properties;
            this._ignored = ignored;
        }

        public int Size
        {
            get { return this._size; }
        }

        public SortedDictionary<LayoutAttribute, FieldInfo> Fields
        {
            get { return this._fields; }
        }

        public SortedDictionary<BindingPropertyAttribute, PropertyInfo> Properties
        {
            get { return _properties; }
        }

        public List<int> Ignored
        {
            get { return this._ignored; }
        }
    }
}
