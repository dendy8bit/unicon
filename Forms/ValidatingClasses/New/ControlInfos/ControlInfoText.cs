﻿using System.Windows.Forms;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;

namespace BEMN.Forms.ValidatingClasses.New.ControlInfos
{
    public class ControlInfoText : ColumnInfoText, IControlInfo
    {
        private Control _control;
        public ControlInfoText(MaskedTextBox control, IValidatingRule rule):base(rule)
        {
            this._control = control;
        }

        public object CurrentControl
        {
            get { return this._control; }
        }
    }

    public class ControlInfoUpDownText :  IControlInfo
    {
        private Control _control;
        IValidatingRule _rule;
        public ControlInfoUpDownText(NumericUpDown control, IValidatingRule rule)
        {
            this._control = control;
            _rule = rule;
        }

        public object CurrentControl
        {
            get { return this._control; }
        }

        public ColumnsType ColumnType
        {
            get { return ColumnsType.UPDOWNTEXT; }
        }

        public System.Collections.Generic.List<string> Items
        {
            get { return null; }
        }

        public IValidatingRule Rule
        {
            get { return _rule ; }
        }
    }
}