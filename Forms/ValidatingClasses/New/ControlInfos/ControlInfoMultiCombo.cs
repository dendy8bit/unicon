﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;

namespace BEMN.Forms.ValidatingClasses.New.ControlInfos
{
    public class ControlInfoMultiCombo : ColumnInfoMultiCombo, IControlInfo
    {
        private ComboBox[] _controls;

        public ControlInfoMultiCombo(ComboBox[] controls, List<string> items) : base(items)
        {
            this._controls = controls;
        }

        public object CurrentControl
        {
            get { return this._controls; }
        }
    }
}
