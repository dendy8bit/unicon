﻿namespace BEMN.Forms.ValidatingClasses.New.ControlInfos
{
    public interface IControlInfo:IDataInfo
    {
        object CurrentControl { get; }
    }
}