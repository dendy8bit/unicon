﻿using System.Windows.Forms;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;

namespace BEMN.Forms.ValidatingClasses.New.ControlInfos
{
    public class ControlInfoCheck : ColumnInfoCheck,IControlInfo
    {
        private Control _control;
        public ControlInfoCheck(CheckBox control)
        {
            this._control = control;
        }

        public object CurrentControl
        {
            get {return this._control; }
        }
    }
}