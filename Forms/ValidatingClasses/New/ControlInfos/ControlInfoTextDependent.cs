﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;

namespace BEMN.Forms.ValidatingClasses.New.ControlInfos
{
    public class ControlInfoTextDependent : IControlInfo
    {
        private MaskedTextBox _control;
        private Func<IValidatingRule> _getRule;
        public ControlInfoTextDependent(MaskedTextBox textBox, Func<IValidatingRule> getRule)
        {
            this._control = textBox;
            this._getRule = getRule;
        }

        public object CurrentControl
        {
            get { return this._control; }
        }

        public ColumnsType ColumnType
        {
            get { return ColumnsType.TEXT;}
        }

        public List<string> Items
        {
            get { return null; }
        }

        public IValidatingRule Rule
        {
            get { return this._getRule.Invoke(); }
        }
    }
}
