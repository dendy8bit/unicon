﻿using System.Collections.Generic;
using System.Windows.Forms;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;

namespace BEMN.Forms.ValidatingClasses.New.ControlInfos
{
    public class ControlInfoCombo : ColumnInfoCombo,IControlInfo
    {
        private Control _control;
        public ControlInfoCombo(ComboBox control, List<string> items) : base(items)
        {
            this._control = control;
        }

        public ControlInfoCombo(ComboBox control, Dictionary<ushort, string> items)
            : base(items)
        {
            this._control = control;
        }

        public object CurrentControl
        {
            get { return this._control; }
        }
    }
}