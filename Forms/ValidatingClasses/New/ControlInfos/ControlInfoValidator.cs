﻿using System.Collections.Generic;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;

namespace BEMN.Forms.ValidatingClasses.New.ControlInfos
{
    public class ControlInfoValidator :  IControlInfo
    {
        private readonly IValidator _validator;
        public ControlInfoValidator(IValidator validator)
        {
            this._validator = validator;

            
        }

        public object CurrentControl
        {
            get {return this._validator; }
        }

        public ColumnsType ColumnType
        {
            get { return ColumnsType.VALIDATOR;}
        }

        public List<string> Items
        {
            get { return null; }
        }

        public IValidatingRule Rule
        {
            get { return null; }
        }
    }
}
