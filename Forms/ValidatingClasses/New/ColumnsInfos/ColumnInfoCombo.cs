﻿using BEMN.Forms.ValidatingClasses.Rules.ComboBox;
using System.Collections.Generic;

namespace BEMN.Forms.ValidatingClasses.New.ColumnsInfos
{
    public class ColumnInfoCombo : IColumnInfo
    {
        private readonly bool[] _enabled;
        private readonly List<string> _items;
        private IValidatingRule _rule;

        public ColumnInfoCombo(List<string> items, ColumnsType columnsType = ColumnsType.COMBO, params bool[] enabled)
        {
            _enabled = enabled;
            this._items = items;
            this.ColumnType = columnsType;
            _rule = new ComboBoxValidateRule();
        }

        public ColumnInfoCombo(Dictionary<ushort, string> items, params bool[] enabled)
        {
            _rule = new ComboBoxValidateRule();
            _enabled = enabled;
            this._items = new List<string>();
            foreach (var item in items)
            {
                this._items.Add(item.Value);
            }
            this.ColumnType = ColumnsType.COMBO;
        }
        
        public bool[] Enabled
        {
            get { return this._enabled; }
        }
        
        public ColumnsType ColumnType { get; private set; }

        public List<string> Items
        {
            get { return this._items; }
        }

        public IValidatingRule Rule
        {
            get { return _rule; }
        }
    }
}