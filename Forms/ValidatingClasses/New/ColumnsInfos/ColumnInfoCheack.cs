﻿using System.Collections.Generic;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;

namespace BEMN.Forms.ValidatingClasses.New
{
    public class ColumnInfoCheack : IColumnInfo
    {
        private readonly bool[] _enabled;

        public ColumnInfoCheack(params bool[] enabled)
        {
            this._enabled = enabled;
        }
  public bool[] Enabled
        {
            get { return this._enabled; }
        }
        public ColumnsType ColumnType
        {
            get { return ColumnsType.CHEACK; }
        }

        public List<string> Items
        {
            get { return null; }
        }

        public IValidatingRule Rule
        {
            get { return null; }
        }


      
    }
}