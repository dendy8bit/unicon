﻿using System;
using System.Collections.Generic;
using BEMN.Forms.ValidatingClasses.Rules.ComboBox;

namespace BEMN.Forms.ValidatingClasses.New.ColumnsInfos
{
    public class ColumnInfoComboDependent: IColumnInfo
    {
        private readonly Func<string, List<string>> _items;

        public ColumnInfoComboDependent(Func<string, List<string>> items, int masterCol, params bool[] enabled)
        {
            this.Enabled = enabled;
            this._items = items;
            this.MasterColumn = masterCol;
            this.ColumnType = ColumnsType.DEPENDENT_COMBO;
            this.Rule = new ComboBoxValidateRule();
        }

        public bool[] Enabled { get; }

        public int MasterColumn { get; private set; }
        public string SelectedMasterString { get; set; }

        public ColumnsType ColumnType { get; private set; }

        public List<string> Items
        {
            get { return this._items.Invoke(this.SelectedMasterString); }
        }

        public IValidatingRule Rule { get; private set; }
    }

    public class ColumnInfoDictionaryComboDependent : ColumnInfoComboDependent
    {
        private readonly Func<string, Dictionary<ushort, string>> _items;

        public ColumnInfoDictionaryComboDependent(Func<string, Dictionary<ushort, string>> items, int masterCol, params bool[] enabled):base(null, masterCol, enabled)
        {
            this._items = items;
        }

        public new Dictionary<ushort, string> Items
        {
            get { return this._items.Invoke(this.SelectedMasterString); }
        }
    }
}
