﻿using System.Collections.Generic;
using BEMN.Forms.ValidatingClasses.Rules.ComboBox;

namespace BEMN.Forms.ValidatingClasses.New.ColumnsInfos
{
    public class ColumnInfoMultiCombo : IColumnInfo
    {
        public ColumnInfoMultiCombo(List<string> items,  params bool[] enabled)
        {
            this.Items = items;
            this.Enabled = enabled;
            this.ColumnType = ColumnsType.MULTI_COMBO;
            this.Rule = new ComboBoxValidateRule();
        }

        public bool[] Enabled { get; }

        public ColumnsType ColumnType { get; }

        public List<string> Items { get; }

        public IValidatingRule Rule { get; }
    }
}
