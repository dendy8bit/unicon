﻿using System.Collections.Generic;

namespace BEMN.Forms.ValidatingClasses.New.ColumnsInfos
{
    public class ColumnInfoText : IColumnInfo 
    {
        private readonly bool[] _enabled;
        public bool[] Enabled
        {
            get { return this._enabled; }
        }

        private readonly IValidatingRule _rule;
  

        public ColumnInfoText(IValidatingRule rule, params bool[] enabled)
        {
            this._rule = rule;
            _enabled = enabled;
        }
    
        public List<string> Items
        {
            get { return null; }
        }

        public IValidatingRule Rule
        {
            get { return this._rule; }
        }

        public ColumnsType ColumnType
        {
            get { return ColumnsType.TEXT;}
        }
    }
}