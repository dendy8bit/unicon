﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BEMN.Forms.ValidatingClasses.New.ColumnsInfos
{
    public class ColumnInfoTextDependent : IColumnInfo
    {
        private readonly bool[] _enabled;
        private Func<IValidatingRule> _getRule;
        
        public ColumnInfoTextDependent(Func<IValidatingRule> getRule, params bool[] enabled)
        {
            _enabled = enabled;
            this._getRule = getRule;
        }

        public bool[] Enabled
        {
            get { return this._enabled; }
        }

        public List<string> Items
        {
            get { return null; }
        }

        public IValidatingRule Rule
        {
            get { return this._getRule.Invoke(); }
        }

        public ColumnsType ColumnType
        {
            get { return ColumnsType.TEXT; }
        }
    }

    public class ColumnInfoTextDgvDependent : IColumnInfo
    {
        private readonly bool[] _enabled;
        private Func<DataGridView, IValidatingRule> _getRule;
        private DataGridView _dgv;

        public ColumnInfoTextDgvDependent(Func<DataGridView, IValidatingRule> getRule, DataGridView dgv, params bool[] enabled)
        {
            _enabled = enabled;
            this._getRule = getRule;
            this._dgv = dgv;
        }

        public bool[] Enabled
        {
            get { return this._enabled; }
        }

        public List<string> Items
        {
            get { return null; }
        }

        public IValidatingRule Rule
        {
            get { return this._getRule.Invoke(this._dgv); }
        }

        public ColumnsType ColumnType
        {
            get { return ColumnsType.TEXT; }
        }
    }
}
