﻿using System.Collections.Generic;

namespace BEMN.Forms.ValidatingClasses.New.ColumnsInfos
{
    public class ColumnInfoColor : IColumnInfo
    {
          private readonly bool[] _enabled;

          public ColumnInfoColor(params bool[] enabled)
        {
            this._enabled = enabled;
        }
  public bool[] Enabled
        {
            get { return this._enabled; }
        }
        public ColumnsType ColumnType
        {
            get { return ColumnsType.COLOR;}
        }

        public List<string> Items
        {
            get { return null; }
        }

        public IValidatingRule Rule
        {
            get { return null; }
        }
    }
}