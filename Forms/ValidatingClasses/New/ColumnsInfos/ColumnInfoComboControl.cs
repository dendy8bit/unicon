﻿using System.Collections.Generic;
using BEMN.Forms.ValidatingClasses.Rules.ComboBox;

namespace BEMN.Forms.ValidatingClasses.New.ColumnsInfos
{
    public class ColumnInfoComboControl : IColumnInfo
    {
        private readonly bool[] _enabled;
        private readonly List<string> _items;

        public ColumnInfoComboControl(List<string> items, int slaveCol, params bool[] enabled)
        {
            this._enabled = enabled;
            this._items = items;
            this.ColumnType = ColumnsType.CONTROL_COMBO;
            this.SlaveColumn = slaveCol;
            this.Rule = new ComboBoxValidateRule();
        }

        public bool[] Enabled
        {
            get { return this._enabled; }
        }

        public int SlaveColumn { get; private set; }

        public ColumnsType ColumnType { get; private set; }

        public List<string> Items
        {
            get { return this._items; }
        }

        public IValidatingRule Rule { get; private set; }
    }
}