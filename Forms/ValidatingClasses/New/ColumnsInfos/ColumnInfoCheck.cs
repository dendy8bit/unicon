﻿using System.Collections.Generic;

namespace BEMN.Forms.ValidatingClasses.New.ColumnsInfos
{
    public class ColumnInfoCheck : IColumnInfo
    {
        private readonly bool[] _enabled;

        public ColumnInfoCheck(params bool[] enabled)
        {
            this._enabled = enabled;
        }
  public bool[] Enabled
        {
            get { return this._enabled; }
        }
        public ColumnsType ColumnType
        {
            get { return ColumnsType.CHEACK; }
        }

        public List<string> Items
        {
            get { return null; }
        }

        public IValidatingRule Rule
        {
            get { return null; }
        }


      
    }
}