﻿namespace BEMN.Forms.ValidatingClasses.New.ColumnsInfos
{
    public interface IColumnInfo : IDataInfo
    {
        bool[] Enabled { get; }
    }
}
