﻿namespace BEMN.Forms.ValidatingClasses.New.ColumnsInfos
{
    public enum ColumnsType
    {
        NAME,
        TEXT,
        COMBO,
        COLOR,
        CHEACK,
        VALIDATOR,
        MULTI_COMBO,
        MULTI_TEXT,
        CONTROL_COMBO,
        DEPENDENT_COMBO,
        UPDOWNTEXT,
        IP
    }
}