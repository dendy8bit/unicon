﻿using System;
using System.Windows.Forms;
using System.Linq;
using System.Reflection;
using BEMN.Devices.Structures;

namespace BEMN.Forms.ValidatingClasses.New.Validators
{
    public class StructUnion<T>: IValidator where T : StructBase
    {
        private IValidator[] _validators;
        private T _currentStruct;

        public StructUnion(params IValidator[] validators)
        {
            this._validators = validators;
            this._currentStruct = Activator.CreateInstance<T>();
        }

        public T Get()
        {
            try
            {
                int i = 0;

                if (this._currentStruct.Properties.First().Value.GetIndexParameters().Length != 0)
                {
                    var prop = this._currentStruct.Properties.First().Value;

                    for (i = 0; i < this._validators.Length; i++)
                    {
                        prop.SetValue(this._currentStruct, _validators[i].Get(), new object[]{i});
                    }
                    return this._currentStruct;
                }

                for (; i < this._validators.Length; i++)
                {
                    this._currentStruct.Properties[i].SetValue(this._currentStruct, _validators[i].Get(), null);
                }
                return this._currentStruct;
            }
            catch (Exception e)
            {
                if (Validator.DEBUG)
                {
                      MessageBox.Show($"Ошибка в Get() StructUnion, {typeof (T)}\n{e.Message}");
                }
              
            }
            return this._currentStruct;
        }


        public void Set(T data)
        {
            try
            {
                int i = 0;
                this._currentStruct = data;

                if (this._currentStruct.Properties.First().Value.GetIndexParameters().Length != 0)
                {
                    var prop = this._currentStruct.Properties.First().Value;

                    for (i = 0; i < this._validators.Length; i++)
                    {
                        _validators[i].Set(prop.GetValue(data, new object[]{i}));
                    }
                    return;
                }

                for (; i < this._validators.Length; i++)
                {
                    this._validators[i].Set(data.Properties[i].GetValue(data, null));
                }
            }
            catch (Exception e)
            {
                if (Validator.DEBUG)
                {
                    MessageBox.Show(string.Format("Ошибка в Set() StructUnion, {0}\n{1}",this.GetType(), e.Message)); 
                }
            }

        }

        object IValidator.Get()
        {
            return this.Get();
        }

        public void Set(object data)
        {
          this.Set((T)data);
        }

        public void Reset(string deviceType)
        {
            try
            {
                for (int i = 0; i < this._validators.Length; i++)
                {
                    _validators[i].Reset();
                }
                if (deviceType == "MR761" || deviceType == "MR762" || deviceType == "MR763" || deviceType == "MR771" || deviceType == "MR7" || deviceType == "Universal")
                {
                    _validators[0].Reset(); // для дополнительного обнуления
                }
            }
            catch (Exception)
            {
                if (Validator.DEBUG)
                {
                    MessageBox.Show(string.Format("Ошибка в StructUnion.Reset() для класса {0}", typeof(T)));
                }
            }
        }


        public void Reset()
        {
            try
            {
                for (int i = 0; i < this._validators.Length; i++)
                {
                    _validators[i].Reset();
                }
            }
            catch (Exception)
            {
                if (Validator.DEBUG)
                {
                    MessageBox.Show(string.Format("Ошибка в StructUnion.Reset() для класса {0}", typeof(T)));
                }
            }
        }
        
        public bool Check(out string message, bool write)
        {
            message = null;
            try
            {
                bool res = true;
                this._currentStruct = this.Get();
                foreach (var validator in _validators)
                {
                    res = res & validator.Check(out message, write);
                    if (!res)
                    {
                        if (Validator.DEBUG)
                        {
                            MessageBox.Show(string.Format("StructUnion.Check() \r\n Ошибка в {0}", validator));
                        }
                        return false;
                        //break;
                    }
                }
                return res;
            }
            catch (Exception)
            {
                if (Validator.DEBUG)
                {
                    MessageBox.Show("Ошибка в StructUnion.Check()");
                }
                return false;
            }
        }

        public bool CheckStruct(object data, bool write, bool visible)
        {
            try
            {
                bool res = true;
                int i = 0;
                
                if (this._currentStruct.Properties.First().Value.GetIndexParameters().Length != 0)
                {
                    var prop = this._currentStruct.Properties.First().Value;

                    for (i = 0; i < this._validators.Length; i++)
                    {
                        res = res & _validators[i].CheckStruct(prop.GetValue(data, new object[] { i }), write, visible);
                    }
                    return res;
                }
                
                for (; i < this._validators.Length; i++)
                {
                    PropertyInfo prop = this._currentStruct.Properties[i];
                    object value = prop.GetValue(data, null);
                    res = res & _validators[i].CheckStruct(value, write, visible);
                    //i++;     //пропускало через один
                }

                return res;
            }
            catch (Exception e)
            {
                if (Validator.DEBUG)
                {
                    MessageBox.Show("Ошибка в StructUnion.CheckStruct()\n"+e.Message);
                }
                return false;
            }
        }
        /// <summary>
        /// Возвращает значения валидируемой структуры
        /// </summary>
        /// <returns></returns>
        public ushort[] GetValues()
        {
            return this.Get().GetValues();
        }
    }
}
