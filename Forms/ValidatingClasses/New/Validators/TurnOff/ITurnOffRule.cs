﻿namespace BEMN.Forms.ValidatingClasses.New.Validators.TurnOff
{
  public  interface ITurnOffRule
  {
      bool IsNeedTurnOff(object value);
      int Column { get; }
      int[] Indexes { get; }
      bool Main { get; }
  }
}
