﻿namespace BEMN.Forms.ValidatingClasses.New.Validators.TurnOff
{
   public class TurnOffRule : ITurnOffRule
    {
       private object _offValue;
       public TurnOffRule(int column, object offValue, bool main = false, params int[] indexes)
       {
           this.Column = column;
           this._offValue = offValue;
           this.Indexes = indexes;
           this.Main = main;
       }


        public bool IsNeedTurnOff(object value)
        {
            return  this._offValue.ToString() == value.ToString();
        }

        public int Column { get; private set; }
        public int[] Indexes { get; private set; }
        public bool Main { get; private set; }
    }
}
