﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices.Structures;

namespace BEMN.Forms.ValidatingClasses.New.Validators.TurnOff
{
   public class TurnOffDgv
    {
       private DataGridView _dataGridView;
       private ITurnOffRule[] _rules;
       public TurnOffDgv(DataGridView dataGridView, params ITurnOffRule[] rules)
       {
           this._dataGridView = dataGridView;
           this._rules = rules;
       }

       public DataGridView DataGridView
       {
           get { return _dataGridView; }
       }

       public bool IsEnabledCell<T>(int column, int row, T data) where T: StructBase
       {
           var rules = _rules.Where(o => o.Indexes.Any( o1 => o1 ==column));
           bool enabled = true;
           foreach (var rule in rules)
           {
               var properties = data.Properties;
               var value = properties.ElementAt(rule.Column-1).Value.GetValue(data, null);
               enabled = enabled & !rule.IsNeedTurnOff(value);
           }
           return enabled;
       }

       public void Validating(int column, int row, object value)
       {
           ITurnOffRule rule = this._rules.FirstOrDefault(o => o.Column == column);
           if (rule == null)
           {
               return;
           }
           this.AsseptRow(rule, row, !rule.IsNeedTurnOff(value));
       }


       private void AsseptRow(ITurnOffRule rule,int row,bool flag)
       {
           //Перечисление всех правил кроме входного
           var otherRules = this._rules.Where(o => o != rule);
           for (int i = 0; i < rule.Indexes.Length; i++)
           {
               if (rule.Main)
               {
                   if (flag)
                   {
                       if (otherRules.Any(o => o.Indexes.Contains(rule.Indexes[i])))
                       {
                           continue;
                       }
                       var newRule = this._rules.FirstOrDefault(o => o.Column == rule.Indexes[i]);
                       if (newRule != null)
                       {
                           var newFlag = !newRule.IsNeedTurnOff(this.DataGridView[rule.Indexes[i], row].Value);
                           this.AsseptRow(newRule, row, newFlag);
                       }
                   }
               }
               Validator.CellEnabled(DataGridView[rule.Indexes[i], row], flag);
           }
       }

       public void Prepare()
       {
           IEnumerable<ITurnOffRule> main = this._rules.Where(o => o.Main);
           IEnumerable<ITurnOffRule> notMain = this._rules.Where(o => !o.Main);
           foreach (ITurnOffRule rule in notMain)
           {
               for (int i = 0; i < this.DataGridView.Rows.Count; i++)
               {
                   bool flag = !rule.IsNeedTurnOff(this.DataGridView[rule.Column, i].Value);
                   this.AsseptRow(rule, i, flag);
               }
           }
           foreach (ITurnOffRule rule in main)
           {
               for (int i = 0; i < this.DataGridView.Rows.Count; i++)
               {
                   bool flag = !rule.IsNeedTurnOff(this.DataGridView[rule.Column, i].Value);
                   this.AsseptRow(rule, i, flag);
               }
           }
       }
    }
}
