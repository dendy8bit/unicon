﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices.Structures;

namespace BEMN.Forms.ValidatingClasses.New.Validators
{
    public class NewCheckedListBoxValidator<T> : IValidator where T : StructBase, IBitField
    {
        private readonly CheckedListBox _checkedListBox;
        private readonly bool _dictionary;
        private T _currentStruct;
        private Dictionary<int, string> _items;
        public NewCheckedListBoxValidator(CheckedListBox checkedListBox)
        {
            this._checkedListBox = checkedListBox;
            this._currentStruct = Activator.CreateInstance<T>();
        }
        public NewCheckedListBoxValidator(CheckedListBox checkedListBox, List<string> items )
        {
            this._currentStruct = Activator.CreateInstance<T>();
            this._checkedListBox = checkedListBox;
            if (items != null)
            {
                this._checkedListBox.Items.Clear();
                this._checkedListBox.Items.AddRange(items.ToArray());
            }
        }

        public NewCheckedListBoxValidator(CheckedListBox checkedListBox, Dictionary<int,string> items)
        {
            this._currentStruct = Activator.CreateInstance<T>();
            _items = items;
            _dictionary = true;
            this._checkedListBox = checkedListBox;
            if (items != null)
            {
                this._checkedListBox.Items.Clear();
                this._checkedListBox.Items.AddRange(items.Values.ToArray());
            }
        }

        public object Get()
        {
            bool[] bits;
            if (_dictionary)
            {
                var count = _items.Keys.Max() + 1;
                bits = new bool[count];
                for (int i = 0; i < _checkedListBox.Items.Count; i++)
                {
                    var value = _checkedListBox.Items[i].ToString();
                    KeyValuePair<int, string> pair = _items.FirstOrDefault(o => o.Value == value);

                    if (pair.Value != null)
                    {
                        bits[pair.Key] = _checkedListBox.GetItemChecked(i);
                    }
                }
            }
            else
            {
                var count = _checkedListBox.Items.Count;
                bits = new bool[count];
                for (int i = 0; i < count; i++)
                {
                    bits[i] = _checkedListBox.GetItemChecked(i);
                }
            }
            this._currentStruct.Bits = new BitArray(bits);
            return this._currentStruct;

        }

        public void Set(object data)
        {
            this._currentStruct = (T) data;
            var value = ((IBitField) data).Bits;
            if (_dictionary)
            {
                for (int i = 0; i < _checkedListBox.Items.Count; i++)
                {
                    var strValue = _checkedListBox.Items[i].ToString();
                    KeyValuePair<int, string> pair = _items.FirstOrDefault(o => o.Value == strValue);
                    _checkedListBox.SetItemChecked(i, value[pair.Key]);
                }
            }
            else
            {
                for (int i = 0; i < _checkedListBox.Items.Count; i++)
                {
                    _checkedListBox.SetItemChecked(i, value[i]);
                }
            }
        }


        public void Reset()
        {
            for (int i = 0; i < _checkedListBox.Items.Count; i++)
            {
                _checkedListBox.SetItemChecked(i, false);
            }
        }


        public bool Check(out string message, bool write)
        {
            message = null;
            return true;
        }


        public bool CheckStruct(object data, bool write, bool visible)
        {
            return true;
        }


    }
}
