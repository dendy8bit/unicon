﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses.Dgw;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;

namespace BEMN.Forms.ValidatingClasses.New.Validators
{
    public class NewDgwValidatior<T, T1> : IValidator
        where T : StructBase, IDgvRowsContainer<T1>
        where T1 : StructBase
    {
        #region [Private fields]

        protected int _rows;
        protected T _currentStruct;
        protected IColumnInfo[] _columnInfos;
        //protected SortedDictionary<BindingPropertyAttribute, PropertyInfo> _properties;
        protected SortedDictionary<int, PropertyInfo> _properties;
        protected ToolTip _toolTip;
        protected DataGridView[] _dgvM;
        protected int[] _rowsM;
        private bool _isCurrentCellChanged;
        private TurnOffDgv[] _turnOffRule;
        #endregion [Private fields]


        #region [Properties]
        public TextboxCellRule[] AddRules { get; set; }
        public Point[] Disabled { get; set; }

        public TurnOffDgv[] TurnOff
        {
            get { return this._turnOffRule; }
            set
            {
                if(value == null) return;
                this._turnOffRule = value;
                foreach (TurnOffDgv turnOffDgv in this._turnOffRule)
                {
                    turnOffDgv.Prepare();
                }
            }
        }

        #endregion [Properties]

        #region [Ctor's]

        public NewDgwValidatior()
        {
        }

        /// <summary>
        /// Создаёт новый объект NewDgwValidatior для одного DataGridView
        /// </summary>
        /// <param name="dgv">целевой DataGridView</param>
        /// <param name="rows">Количество строк</param>
        /// <param name="toolTip">ToolTip для вывода подсказок</param>
        /// <param name="columnInfos">Массив описаний столбцов целевого DataGridView</param>
        public NewDgwValidatior(DataGridView dgv, int rows, ToolTip toolTip, params IColumnInfo[] columnInfos)
            : this(new[] { dgv }, new[] { rows }, toolTip, columnInfos)
        { }


        /// <summary>
        /// Создаёт новый объект NewDgwValidatior для неснольких DataGridView
        /// </summary>
        /// <param name="dgvs">массив целевых DataGridView</param>
        /// <param name="rows">Массив задающий количество строк в каждом целевом DataGridView</param>
        /// <param name="toolTip">ToolTip для вывода подсказок</param>
        /// <param name="columnInfos">Массив описаний столбцов целевых DataGridView</param>
        public NewDgwValidatior(DataGridView[] dgvs, int[] rows, ToolTip toolTip, params IColumnInfo[] columnInfos)
        {
            this._toolTip = toolTip;
            this._columnInfos = columnInfos;
            this._dgvM = dgvs;
            this._rowsM = rows;
            this._rows = rows.Sum();
            this._currentStruct = Activator.CreateInstance<T>();
            this._properties = this._currentStruct.Rows[0].Properties;

            if (this._dgvM.All(o => o.Columns.Count != this._dgvM[0].Columns.Count) &
                (this._dgvM[0].Columns.Count != this._columnInfos.Length))
            {
                throw new ArgumentException("Количество столбцов не соответствует количеству описаний");
            }

            for (int j = 0; j < this._dgvM.Length; j++)
            {
                for (int i = 0; i < columnInfos.Length; i++)
                {
                    IColumnInfo info;
                    if (columnInfos[i].Enabled.Length == 0)
                    {
                        info = columnInfos[i];
                    }
                    else
                    {
                        info = columnInfos[i].Enabled[j] ? columnInfos[i] : null;
                    }

                    if (info == null)
                    {
                        continue;
                    }
                    if (info.ColumnType == ColumnsType.COMBO)
                    {
                        DataGridViewComboBoxColumn col = (DataGridViewComboBoxColumn)this._dgvM[j].Columns[i];
                        col.Items.Clear();
                        foreach (string item in columnInfos[i].Items)
                        {
                            col.Items.Add(item);
                        }
                    }

                    if (info.ColumnType == ColumnsType.MULTI_COMBO)
                    {
                        DataGridViewComboBoxColumn col = (DataGridViewComboBoxColumn)this._dgvM[j].Columns[i];
                        col.Items.Clear();
                        ColumnInfoMultiCombo multi = (ColumnInfoMultiCombo)columnInfos[i];
                        foreach (char item in multi.Items[j])
                        {
                            col.Items.Add(item);
                        }
                    }
                }
                this._dgvM[j].Rows.Clear();
                this._dgvM[j].Rows.Add(rows[j]);
                this._dgvM[j].CellValidating += this._dgv_CellValidating;
                this._dgvM[j].EditingControlShowing += this.dgv_EditingControlShowing;
                this._dgvM[j].CellClick += this._dgv_CellClick;
            }
            
            this.Reset();
            foreach (DataGridView dgv in this._dgvM)
            {
                dgv.CurrentCellDirtyStateChanged += this.NewDgwValidatior_CurrentCellDirtyStateChanged;
                dgv.CellValueChanged += this.NewDgwValidatior_CellValueChanged;
            }
        }

        #endregion [Ctor's]
        
        #region [Public members]

        object IValidator.Get()
        {
            return this.Get();
        }

        /// <summary>
        /// Выводит значения в DataGridView
        /// </summary>
        /// <param name="data">Объект данных</param>
        public void Set(object data)
        {
            try
            {
                this.Set((T)data);
            }
            catch (Exception)
            {
                if (Validator.DEBUG)
                {
                   MessageBox.Show("Ошибка в NewDgwValidatior.Set(object data)"); 
                }
            }
        }

        /// <summary>
        /// Выводит значения в DataGridView
        /// </summary>
        /// <param name="data">Объект данных</param>
        public void Set(T data)
        {
            int structInd = 0;
            for (int gridIndex = 0; gridIndex < this._rowsM.Length; gridIndex++) // по таблицам и их массивам строк
            {
                for (int rowInd = 0; rowInd < this._rowsM[gridIndex]; rowInd++)    // по строкам таблицы
                {
                    for (int colInd = 0; colInd < this._columnInfos.Length; colInd++) // по столбцам
                    {
                        this.SetValueToDgv(this._dgvM[gridIndex], structInd, colInd, rowInd, data);
                    }
                    structInd++;
                }
                this._isCurrentCellChanged = true;
                this._dgvM[gridIndex].ClearSelection();
                this._dgvM[gridIndex].CurrentCell = this._dgvM[gridIndex][1, 0];
            }

            if (this.TurnOff != null)
            {
                foreach (TurnOffDgv turnOffDgv in this.TurnOff)
                {
                    turnOffDgv.Prepare();
                }
            }
            if (this.Disabled != null)
            {
                foreach (Point point in this.Disabled)
                {
                    int index = this.GetIndex(point.Y);
                    int dgwRow = index == 0 ? point.Y : this.GetRow(this._rowsM, index, point.Y);
                    Validator.CellEnabled(this._dgvM[index][point.X, dgwRow], false);
                }
            }
        }

        /// <summary>
        /// Считывает данные из DataGridView в объект данных
        /// </summary>
        /// <returns>Объект данных</returns>
        public T Get()
        {
            foreach (DataGridView dgv in this._dgvM)
            {
                dgv.EndEdit();
            }
            int structInd = 0;
            for (int gridIndex = 0; gridIndex < this._rowsM.Length; gridIndex++) // по таблицам и их массивам строк
            {
                for (int rowInd = 0; rowInd < this._rowsM[gridIndex]; rowInd++) // по строкам таблицы
                {
                    for (int colInd = 0; colInd < this._columnInfos.Length; colInd++) // по столбцам
                    {
                        try
                        {
                            this.GetValueFromDgv(this._dgvM[gridIndex], structInd, colInd, rowInd);
                        }
                        catch (Exception)
                        {
                            if (Validator.DEBUG)
                            {
                                MessageBox.Show(string.Format(
                                        "Ошибка в GgwValidator<T,T1>.Get() для класса {0} \r\nstructInd={1}\r\ncolInd={2}",
                                        typeof (T), structInd, colInd));
                            }
                        }
                    }
                    structInd++;
                }
            }
            return this._currentStruct;
        }

        #endregion [Public members]
        
        protected void CellEditEnd(DataGridView sender, int column, int row, object value)
        {
            if (this.TurnOff != null)
            {
                foreach (TurnOffDgv turnOffDgv in this.TurnOff)
                {
                    if(turnOffDgv.DataGridView != sender || sender[column, row].ReadOnly)
                        continue;
                    turnOffDgv.Validating(column, row, value);
                }
            }

            if (this.Disabled != null)
            {
                foreach (Point point in this.Disabled)
                {
                    Validator.CellEnabled(sender[point.X, point.Y], false);
                }
            }
        }

        #region [Event handlers]

        protected void NewDgwValidatior_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCell curCell = ((DataGridView)sender).CurrentCell;
            if (curCell == null) return;
            object value = curCell.Value;
            this.CellEditEnd((DataGridView)sender, e.ColumnIndex, e.RowIndex, value);
        }

        protected void NewDgwValidatior_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            dgv.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        protected void _dgv_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            this.CellEditEnd((DataGridView) sender, e.ColumnIndex, e.RowIndex, e.FormattedValue);
            if (dgv == null || dgv[e.ColumnIndex,e.RowIndex].ReadOnly)
            {
                return;
            }

            IColumnInfo columnInfo = null;

            try
            {
                int index = Array.IndexOf(this._dgvM, sender);
                if ((this._columnInfos[e.ColumnIndex].Enabled.Length == 0) ||
                    this._columnInfos[e.ColumnIndex].Enabled[index])
                {
                    columnInfo = this._columnInfos[e.ColumnIndex];
                }
            }
            catch (Exception)
            { }

            if (columnInfo == null || columnInfo.ColumnType != ColumnsType.TEXT || this._isCurrentCellChanged)
            {
                this._isCurrentCellChanged = false;
                return;
            }

            TextboxCellRule rule = this.AddRules?.FirstOrDefault(o => (o.Column == e.ColumnIndex) & (o.Row == e.RowIndex));
            if (rule != null)
            {
                e.Cancel = !rule.Rule.IsValid(e.FormattedValue.ToString());
                return;
            }

            dgv.Tag = dgv[e.ColumnIndex, e.RowIndex];
            
            if (!columnInfo.Rule.IsValid(e.FormattedValue.ToString()))
            {
                e.Cancel = true;
                dgv.BeginEdit(true);
            }
            this.ShowHideToolTip(dgv, (TextBox)dgv.EditingControl);
        }

       
        /// <summary>
        /// Обработка нажатия на ячейку
        /// </summary>
        protected void _dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex == -1) return;

            DataGridView dgv = (DataGridView)sender;
            ColumnsType columnType = this._columnInfos[e.ColumnIndex].ColumnType;
            if (columnType == ColumnsType.COMBO || columnType == ColumnsType.DEPENDENT_COMBO 
                || columnType == ColumnsType.CONTROL_COMBO)
            {
                dgv.BeginEdit(true);                
                ComboBox comboBox = dgv.EditingControl as ComboBox;
                if (comboBox == null) return; //когда задизейблена ячейка,редактируемого контрола нет и выпадает исключение
                comboBox.DroppedDown = true;
            }
            if (columnType == ColumnsType.COLOR)
            {

                if (dgv.CurrentCell.Style.BackColor == Color.Red)
                {
                    dgv.CurrentCell.Style.BackColor = Color.Green;
                    dgv.CurrentCell.Style.SelectionBackColor = dgv.CurrentCell.Style.BackColor;
                }
                else
                {
                    dgv.CurrentCell.Style.BackColor = Color.Red;
                    dgv.CurrentCell.Style.SelectionBackColor = dgv.CurrentCell.Style.BackColor;
                }
            }
        }

        /// <summary>
        /// Подпись на события TextBox
        /// </summary>
        protected void dgv_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            if (e.Control is DataGridViewTextBoxEditingControl) // Только для текстовых ячеек
            {
                dgv.EditingControl.KeyPress += this.EditingControl_KeyPress;
                dgv.EditingControl.TextChanged += this.EditingControl_TextChanged;
            }
            if (e.Control is DataGridViewComboBoxEditingControl) // только для выпадающих списков
            {
                DataGridViewComboBoxEditingControl combo = e.Control as DataGridViewComboBoxEditingControl;
                combo.DropDownStyle = ComboBoxStyle.DropDown;                
                combo.AutoCompleteSource = AutoCompleteSource.ListItems;
                combo.AutoCompleteMode = AutoCompleteMode.Suggest;
                combo.KeyDown += this.ComboOnKeyDown;
                
                if (combo.Items.Contains(Validator.ERROR_VALUE))
                {
                    combo.EditingControlFormattedValue = combo.Items[0];
                    combo.Items.Remove(Validator.ERROR_VALUE);
                }
            }
        }

        protected void ComboOnKeyDown(object sender, KeyEventArgs keyEventArgs)
        {
            ComboBox combo = sender as ComboBox;
            combo.DroppedDown = false;
            combo.KeyDown -= this.ComboOnKeyDown;
        }
        
        /// <summary>
        /// Обработка изменения текста
        /// </summary>
        protected void EditingControl_TextChanged(object sender, EventArgs e)
        {
            DataGridView dgv = (DataGridView)((Control)sender).Parent.Parent;
            dgv.Tag = dgv.CurrentCell;
            TextBox tb = (TextBox)sender;

            this.ShowHideToolTip(dgv, tb);
        }

        private void ShowHideToolTip(DataGridView dgv, TextBox tb)
        {
            if (tb == null) return;
            IColumnInfo columnInfo = this._columnInfos[dgv.CurrentCell.ColumnIndex];
            IValidatingRule rule = columnInfo.Rule;
            if (this.AddRules != null)
            {
                TextboxCellRule addRule = this.AddRules.FirstOrDefault(o => (o.Column == dgv.CurrentCell.ColumnIndex) & (o.Row == dgv.CurrentCell.RowIndex));
                if (addRule != null)
                {
                    rule = addRule.Rule;
                }
            }
            Panel panel = (Panel)tb.Parent;
            if (rule.IsValid(tb.Text))
            {
                dgv.CurrentCell.Style.SelectionBackColor = SystemColors.Highlight;
                panel.BackColor = Color.White;

                dgv.CurrentCell.Style.BackColor = Color.White;
                tb.BackColor = Color.White;
                this._toolTip.Hide(tb);
            }
            else
            {
                dgv.CurrentCell.Style.SelectionBackColor = Color.Red;
                panel.BackColor = Color.Red;

                dgv.CurrentCell.Style.BackColor = Color.Red;
                tb.BackColor = Color.Red;
                this._toolTip.Show(rule.ErrorMessage, tb, tb.Width, 0);
            }
        }

        /// <summary>
        /// Обработка нажатия клавиши
        /// </summary>
        protected void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            Control control = sender as Control;
            if (control == null)
            {
                e.Handled = false;
                return;
            }
            if (control.Parent == null || control.Parent.Parent == null)
            {
                control.KeyPress -= this.EditingControl_KeyPress;
                control.TextChanged -= this.EditingControl_TextChanged;
                e.Handled = false;
                return;
            }
            DataGridView dgv = (DataGridView) ((Control) sender).Parent.Parent;
            dgv.Tag = dgv.CurrentCell;
            IColumnInfo columnInfo = this._columnInfos[dgv.CurrentCell.ColumnIndex];
            e.Handled = columnInfo.Rule.IsValidKey(e.KeyChar);
        }

        #endregion [Event handlers]


        #region [Help members]
        protected int GetRow(int[] rows, int index, int i)
        {
            for (; index > 0; index--)
            {
                i = i - rows[index - 1];
            }
            return i;
        }

        /// <summary>
        /// Извлекает инфу о столбце для массива DataGridView по координатам ячейки
        /// </summary>
        /// <param name="row">Строка ячейки</param>
        /// <param name="column">Колонка ячейки</param>
        /// <returns></returns>
        protected IColumnInfo GetInfo(int row, int column)
        {
            int index = this.GetIndex(row);
            if ((this._columnInfos[column].Enabled.Length == 0) || this._columnInfos[column].Enabled[index])
            {
                return this._columnInfos[column];
            }
            return null;
        }

        /// <summary>
        /// Получает индекс DataGridView в массиве по номеру строки
        /// </summary>
        /// <param name="row">Номер строки</param>
        /// <returns>индекс Dgw</returns>
        protected int GetIndex(int row)
        {
            int temp = 0;
            for (int i = 0; i < this._rowsM.Length; i++)
            {
                if (i == 0)
                {
                    temp = this._rowsM[i];
                }
                else
                {
                    temp = this._rowsM[i] + temp;
                }

                if (row < temp)
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Записывает одно значение из DataGridView в структуру
        /// </summary>
        /// <param name="dgv">DataGridView</param>
        /// <param name="structRow">позиция в структуре</param>
        /// <param name="column">колонка в DataGridView</param>
        /// <param name="row">строка в DataGridView</param>
        protected void GetValueFromDgv(DataGridView dgv, int structRow, int column, int row)
        {
            IColumnInfo info = this.GetInfo(structRow, column);
            if (info == null)
            {
                return;
            }

            if (this._columnInfos[0].ColumnType == ColumnsType.NAME)
            {
                if (column == 0)
                {
                    return;
                }
                if (info.ColumnType == ColumnsType.COMBO ||
                    info.ColumnType == ColumnsType.CHEACK ||
                    info.ColumnType == ColumnsType.MULTI_COMBO)
                {
                    this._properties.ElementAt(column - 1)
                        .Value.SetValue(this._currentStruct.Rows[structRow], dgv[column, row].Value, null);
                }
                if (info.ColumnType == ColumnsType.TEXT)
                {
                    this._properties.ElementAt(column - 1).Value.SetValue(this._currentStruct.Rows[structRow],
                        info.Rule.Parse(dgv[column, row].Value.ToString()), null);
                }

                if (info.ColumnType == ColumnsType.COLOR)
                {
                    this._properties.ElementAt(column - 1)
                        .Value.SetValue(this._currentStruct.Rows[structRow], dgv[column, row].Style.BackColor, null);
                }
            }
            else
            {
                this._properties.ElementAt(column)
                    .Value.SetValue(this._currentStruct.Rows[structRow], dgv[column, row].Value, null);
            }
        }

        /// <summary>
        /// Записывает одно значение из структуры  в DataGridView
        /// </summary>
        /// <param name="dgv">DataGridView</param>
        /// <param name="structRow">суммарная позиция по строкам таблиц</param>
        /// <param name="column">колонка в текущей таблице</param>
        /// <param name="row">строка в текущей таблице</param>
        /// <param name="data">структура</param>
        /// <param name="reset"></param>
        protected void SetValueToDgv(DataGridView dgv, int structRow, int column, int row, T data, bool reset = false)
        {
            try
            {
                IColumnInfo info = this.GetInfo(structRow, column);
                if (info == null)
                {
                    return;
                }
                if (reset && column != 0 || data == null)
                {
                    this.ResetCell(dgv[column, row], info, structRow);
                    return;
                }
                if (this._columnInfos[0].ColumnType == ColumnsType.NAME)
                {
                    if (column == 0)
                    {
                        dgv[column, row].Value = this._columnInfos[0].Items[structRow];
                    }
                    else
                    {
                        object value = this._properties.ElementAt(column - 1).Value.GetValue(data.Rows[structRow], null);
                        if (info.ColumnType == ColumnsType.COLOR)
                        {
                            dgv[column, row].Style.BackColor = (Color)value;
                            dgv[column, row].Style.SelectionBackColor = (Color)value;
                            return;
                        }
                        if (info.ColumnType == ColumnsType.COMBO || info.ColumnType == ColumnsType.MULTI_COMBO
                            || info.ColumnType == ColumnsType.CONTROL_COMBO || info.ColumnType == ColumnsType.DEPENDENT_COMBO)
                        {
                            if (value.ToString() == Validator.ERROR_VALUE)
                            {
                                DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)dgv[column, row];
                                if (!cell.Items.Contains(Validator.ERROR_VALUE))
                                {
                                    cell.Items.Add(Validator.ERROR_VALUE);
                                    cell.Value = Validator.ERROR_VALUE;
                                }
                            }
                        }
                        dgv[column, row].Value = value;
                        if (dgv[column, row].Visible)
                        {
                            dgv.Tag = dgv[column, row];
                        }
                    }
                }
                else
                {
                    dgv[column, row].Value = this._properties.ElementAt(column).Value.GetValue(data.Rows[structRow], null).ToString();
                }
            }
            catch (Exception e)
            {
                if (Validator.DEBUG)
                {
                      MessageBox.Show("NewDgwValidatior.Set\r\n" + e.Message);
                }
            }
        }
        
        protected void ResetCell(DataGridViewCell dataGridViewCell, IColumnInfo info, int structRow = 0)
        {
            switch (info.ColumnType)
            {
                case ColumnsType.NAME:
                {
                    dataGridViewCell.Value = info.Items[structRow];
                    break;
                }
                case ColumnsType.CHEACK:
                {
                    dataGridViewCell.Value = false;
                    break;
                }
                case ColumnsType.COLOR:
                {
                    dataGridViewCell.Style.BackColor = Color.Green;
                    dataGridViewCell.Style.SelectionBackColor = Color.Green;
                    break;
                }
                case ColumnsType.COMBO:
                case ColumnsType.MULTI_COMBO:
                case ColumnsType.CONTROL_COMBO:
                case ColumnsType.DEPENDENT_COMBO:
                {
                    dataGridViewCell.Value = ((DataGridViewComboBoxCell) dataGridViewCell).Items[0];
                    break;
                }
                case ColumnsType.TEXT:
                {
                    dataGridViewCell.Value = info.Rule.Min;
                    break;
                }
            }
        }

        #endregion [Help members]

        public void Reset()
        {
            int nameInd = 0;
            for (int gridIndex = 0; gridIndex < this._rowsM.Length; gridIndex++) // по таблицам и их массивам строк
            {
                for (int rowInd = 0; rowInd < this._rowsM[gridIndex]; rowInd++)    // по строкам таблицы
                {
                    for (int colInd = 0; colInd < this._columnInfos.Length; colInd++) // по столбцам
                    {
                        this.SetValueToDgv(this._dgvM[gridIndex], nameInd, colInd, rowInd, null, true);

                        if (this.TurnOff != null)
                        {
                            foreach (TurnOffDgv turnOffDgv in this.TurnOff)
                            {
                                if (turnOffDgv.DataGridView != this._dgvM[gridIndex] ||
                                    this._dgvM[gridIndex][colInd, rowInd].ReadOnly)
                                    continue;
                                turnOffDgv.Validating(colInd, rowInd, this._dgvM[gridIndex][colInd, rowInd].Value);
                            }
                        }
                    }
                    nameInd++;
                }
                this._isCurrentCellChanged = true;
                this._dgvM[gridIndex].ClearSelection();
            }
        }
        
        public bool Check(out string message, bool write)
        {
            message = null;
            return this.CheckStruct(this.Get(), write, true);
        }

        protected bool IsEnabledCell(int column, int row, DataGridView dgv, T1 data)
        {
            if (this.Disabled != null)
            {
                Point res = this.Disabled.FirstOrDefault(o => o == new Point(column, row));
                if (res != default(Point))
                {
                    return false;
                }
            }

            if (this.TurnOff == null)
            {
                return true;
            }
            TurnOffDgv turnOff = this.TurnOff.FirstOrDefault(o => o.DataGridView == dgv);
            if (turnOff == null)
            {
                return true;
            }
            return turnOff.IsEnabledCell(column, row, data);
        }

        public bool CheckStruct(object data, bool write, bool visible)
        {
            try
            {
                int structInd = 0;
                for (int gridIndex = 0; gridIndex < this._rowsM.Length; gridIndex++) // по таблицам и их массивам строк
                {
                    for (int rowInd = 0; rowInd < this._rowsM[gridIndex]; rowInd++) // по строкам таблицы
                    {
                        for (int colInd = 0; colInd < this._columnInfos.Length; colInd++) // по столбцам
                        {
                            IColumnInfo info = this.GetInfo(structInd, colInd);
                            IValidatingRule validatingRule = null;

                            if (this.AddRules != null)
                            {
                                TextboxCellRule addRule =
                                    this.AddRules.FirstOrDefault(o => o.Column == colInd & o.Row == rowInd);
                                if (addRule != null)
                                {
                                    validatingRule = addRule.Rule;
                                }
                            }
                            if (!this._dgvM[gridIndex][colInd, rowInd].Visible)
                            {
                                continue;
                            }
                            this._dgvM[gridIndex].Tag = this._dgvM[gridIndex][colInd, rowInd];
                            bool result = this.CheckValue(structInd, colInd, (T) data, info, validatingRule);
                            if (write)
                            {
                                //Проверяем включена ли ячейка 
                                if (!this.IsEnabledCell(colInd, rowInd, this._dgvM[gridIndex],((T) data).Rows[structInd]) && !result)
                                {
                                    if (visible)
                                        this.ResetCell(this._dgvM[gridIndex][colInd, rowInd], this._columnInfos[colInd]);
                                    result = true;
                                }
                                if(!result)
                                {
                                    Validator.ShowControl(this._dgvM[gridIndex]);
                                    if (info.ColumnType == ColumnsType.TEXT)
                                    {
                                        this._dgvM[gridIndex][colInd, rowInd].Style.BackColor = Color.Red;
                                    }
                                    return false;
                                }
                            }
                        }
                        structInd++;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                if (Validator.DEBUG)
                {
                    MessageBox.Show(string.Format("{0}.\n{1}", e.Message, data));
                }
                return false;
            }
        }

        protected bool CheckValue(int structRow, int column, T data, IColumnInfo info, IValidatingRule rule)
        {
            try
            {
                if (info == null)
                {
                    return true;
                }
                if (this._columnInfos[0].ColumnType == ColumnsType.NAME)
                {
                    if (column == 0)
                    {
                        return true;
                    }
                    object value = this._properties.ElementAt(column - 1).Value.GetValue(data.Rows[structRow], null);
                    if (info.ColumnType == ColumnsType.COMBO || info.ColumnType == ColumnsType.MULTI_COMBO 
                        || info.ColumnType == ColumnsType.CONTROL_COMBO || info.ColumnType == ColumnsType.DEPENDENT_COMBO)
                    {
                        if (value.ToString() == Validator.ERROR_VALUE)
                        {
                            return false;
                        }
                    }
                    if (info.ColumnType == ColumnsType.COLOR)
                    {
                        return true;
                    }
                    if (info.ColumnType == ColumnsType.TEXT)
                    { 
                        return rule == null ? info.Rule.IsValid(value.ToString()) : rule.IsValid(value.ToString());
                    }
                }

            }
            catch (Exception e)
            {
                if (Validator.DEBUG)
                {
                    MessageBox.Show("NewDgwValidatior.Set\r\n" + e);
                }
            }
            return true;
        }
    }
}
