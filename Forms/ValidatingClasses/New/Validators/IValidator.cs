﻿namespace BEMN.Forms.ValidatingClasses.New.Validators
{
   public interface IValidator
   {
       object Get();
       void Set(object data);
       void Reset();
       bool Check(out string message,bool write);
       bool CheckStruct(object data, bool write, bool visible);
   }
}
