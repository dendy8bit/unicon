﻿using System;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;

namespace BEMN.Forms.ValidatingClasses.New.Validators
{
    public class DgvValidatorWithDepend<T, T1> : NewDgwValidatior<T, T1>, IValidator where T : StructBase, IDgvRowsContainer<T1>
        where T1 : StructBase
    {
        public DgvValidatorWithDepend(DataGridView dgv, int rows, ToolTip toolTip, params IColumnInfo[] columnInfos) :
            this(new[] { dgv }, new[] { rows }, toolTip, columnInfos)
        {
        }

        public DgvValidatorWithDepend(DataGridView[] dgvs, int[] rows, ToolTip toolTip, params IColumnInfo[] columnInfos)
        {
            _toolTip = toolTip;
            _columnInfos = columnInfos;
            _dgvM = dgvs;
            _rowsM = rows;
            _rows = rows.Sum();
            _currentStruct = Activator.CreateInstance<T>();
            _properties = _currentStruct.Rows[0].Properties;

            if (_dgvM.All(o => o.Columns.Count != _dgvM[0].Columns.Count) &
                (_dgvM[0].Columns.Count != _columnInfos.Length))
            {
                throw new ArgumentException("Количество столбцов не соответствует количеству описаний");
            }

            for (int i = 0; i < _dgvM.Length; i++) // DataGrigView
            {
                _dgvM[i].Rows.Clear();

                for (int j = 0; j < _rowsM[i]; j++) // rows in current DGV
                {
                    DataGridViewRow row = new DataGridViewRow();
                    foreach (IColumnInfo info in columnInfos)
                    {
                        if (info.Enabled.Length != 0 && !info.Enabled[i])
                        {
                            continue;
                        }
                        if (info.ColumnType == ColumnsType.NAME)
                        {
                            DataGridViewTextBoxCell cell = new DataGridViewTextBoxCell();
                            cell.Value = info.Items[j];
                            row.Cells.Add(cell);
                        }
                        if (info.ColumnType == ColumnsType.COMBO || info.ColumnType == ColumnsType.CONTROL_COMBO)
                        {
                            DataGridViewComboBoxCell cell = new DataGridViewComboBoxCell();
                            cell.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox;
                            foreach (string item in info.Items)
                            {
                                cell.Items.Add(item);
                            }
                            cell.Value = cell.Items[0];
                            row.Cells.Add(cell);
                        }
                        if (info.ColumnType == ColumnsType.DEPENDENT_COMBO)
                        {
                            DataGridViewComboBoxCell cell = new DataGridViewComboBoxCell();
                            cell.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox;

                            if (info is ColumnInfoDictionaryComboDependent)
                            {
                                ColumnInfoDictionaryComboDependent depend = (ColumnInfoDictionaryComboDependent) info;
                                ColumnInfoComboControl master = (ColumnInfoComboControl) _columnInfos[depend.MasterColumn];
                                depend.SelectedMasterString = master.Items[0];
                                cell.Tag = depend.Items;
                                foreach (string item in depend.Items.Select(pair=>pair.Value))
                                {
                                    cell.Items.Add(item);
                                }
                            }
                            else
                            {
                                ColumnInfoComboDependent depend = (ColumnInfoComboDependent) info;
                                ColumnInfoComboControl master = (ColumnInfoComboControl) _columnInfos[depend.MasterColumn];
                                depend.SelectedMasterString = master.Items[0];
                                foreach (string item in depend.Items)
                                {
                                    cell.Items.Add(item);
                                }
                            }

                            cell.Value = cell.Items[0];
                            row.Cells.Add(cell);
                        }
                    }
                    _dgvM[i].Rows.Add(row);
                }

                _dgvM[i].CellValidating += _dgv_CellValidating;
                _dgvM[i].EditingControlShowing += dgv_EditingControlShowing;
                _dgvM[i].CellClick += _dgv_CellClick;
                _dgvM[i].CurrentCellDirtyStateChanged += NewDgwValidatior_CurrentCellDirtyStateChanged;
                _dgvM[i].CellValueChanged += this.NewDgwValidatior_CellValueChanged;
            }
        }

        protected new void NewDgwValidatior_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCell cell = ((DataGridView) sender)[e.ColumnIndex, e.RowIndex];
            this.CellEditEnd((DataGridView)sender, e.ColumnIndex, e.RowIndex, cell.Value );
        }

        protected new void CellEditEnd(DataGridView sender, int column, int row, object value)
        {
            base.CellEditEnd(sender, column, row, value);
            if (_columnInfos[column].ColumnType == ColumnsType.CONTROL_COMBO)
            {
                ColumnInfoComboControl master = (ColumnInfoComboControl)_columnInfos[column];
                ColumnInfoComboDependent depend = (ColumnInfoComboDependent)_columnInfos[master.SlaveColumn];
                depend.SelectedMasterString = Convert.ToString(sender[column, row].Value);
                DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)sender[master.SlaveColumn, row];
                cell.Items.Clear();
                if (cell.Tag != null)
                {
                    ColumnInfoDictionaryComboDependent dictionaryDepend = (ColumnInfoDictionaryComboDependent) depend;
                    cell.Tag = dictionaryDepend.Items;
                    foreach (string item in dictionaryDepend.Items.Select(pair => pair.Value))
                    {
                        cell.Items.Add(item);
                    }
                }
                else
                {
                    foreach (string item in depend.Items)
                    {
                        cell.Items.Add(item);
                    }
                }
                cell.Value = cell.Items[0];
            }
        }

        object IValidator.Get()
        {
            return this.Get();
        }

        /// <summary>
        /// Считывает данные из DataGridView в объект данных
        /// </summary>
        /// <returns>Объект данных</returns>
        public new T Get()
        {
            foreach (DataGridView dgv in _dgvM)
            {
                dgv.EndEdit();
            }

            T1[] setterValue = (T1[])_currentStruct.Rows.Clone();
            int structInd = 0;
            for (int gridIndex = 0; gridIndex < this._rowsM.Length; gridIndex++) // по таблицам и их массивам строк
            {
                for (int rowInd = 0; rowInd < this._rowsM[gridIndex]; rowInd++) // по строкам таблицы
                {
                    for (int colInd = 0; colInd < this._columnInfos.Length; colInd++) // по столбцам
                    {
                        try
                        {
                            this.GetValueFromDgv(this._dgvM[gridIndex], structInd, colInd, rowInd, setterValue);
                        }
                        catch (Exception)
                        {
                            if (Validator.DEBUG)
                            {
                                MessageBox.Show(string.Format(
                                        "Ошибка в GgwValidator<T,T1>.Get() для класса {0} \r\nstructInd={1}\r\ncolInd={2}",
                                        typeof(T), structInd, colInd));
                            }
                        }
                    }
                    structInd++;
                }
            }
            _currentStruct.Rows = setterValue;
            return _currentStruct;
        }

        /// <summary>
        /// Записывает одно значение из DataGridView в структуру
        /// </summary>
        /// <param name="dgv">DataGridView</param>
        /// <param name="structRow">позиция в структуре</param>
        /// <param name="column">колонка</param>
        /// <param name="row">строка</param>
        /// <param name="setterValue">Устанавливыемые значения</param>
        protected void GetValueFromDgv(DataGridView dgv, int structRow, int column, int row, T1[] setterValue)
        {
            try
            {
                IColumnInfo info = GetInfo(row, column);
                if (info == null || info.ColumnType == ColumnsType.NAME)
                {
                    return;
                }
                
                if (info.ColumnType == ColumnsType.DEPENDENT_COMBO ||
                    info.ColumnType == ColumnsType.CONTROL_COMBO ||
                    info.ColumnType == ColumnsType.COMBO ||
                    info.ColumnType == ColumnsType.CHEACK ||
                    info.ColumnType == ColumnsType.MULTI_COMBO)
                {
                    _properties.ElementAt(column - 1)
                        .Value.SetValue(setterValue[structRow], dgv[column, row].Value, null);
                }
                if (info.ColumnType == ColumnsType.TEXT)
                {
                    _properties.ElementAt(column - 1)
                        .Value.SetValue(setterValue[structRow], info.Rule.Parse(dgv[column, row].Value.ToString()), null);
                }

                if (info.ColumnType == ColumnsType.COLOR)
                {
                    _properties.ElementAt(column - 1)
                        .Value.SetValue(setterValue[structRow], dgv[column, row].Style.BackColor, null);
                }
            }
            catch (Exception)
            {
                if (Validator.DEBUG)
                {
                    MessageBox.Show(string.Format("Ошибка в NewDgwValidatior<{0},{1}>.GetValueFromDgv() \r\nстрока-{2} \r\nстолбец{3}",
                            typeof (T), typeof (T1), row, column));
                }
            }
        }

        public new bool Check(out string message, bool write)
        {
            message = null;
            return CheckStruct(this.Get(), write, true);
        }
    }
}
