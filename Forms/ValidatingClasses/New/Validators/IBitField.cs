﻿using System.Collections;

namespace BEMN.Forms.ValidatingClasses.New.Validators
{
   public interface IBitField
    {
       BitArray Bits { get; set; }
    }
}
