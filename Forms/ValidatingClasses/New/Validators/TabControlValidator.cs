﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BEMN.Devices.Structures;

namespace BEMN.Forms.ValidatingClasses.New.Validators
{
    public class TabControlValidator<T> : IValidator where T : StructBase
    {
        private T _currenStruct;

        public TabControlValidator(TabControl tabControl, int rows, ToolTip toolTip, params IDataInfo[] columnInfos)
        {

        }

        #region Implementation of IValidator

        public object Get()
        {
            throw new NotImplementedException();
        }

        public void Set(object data)
        {
            throw new NotImplementedException();
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }

        public bool Check(out string message, bool write)
        {
            throw new NotImplementedException();
        }

        public bool CheckStruct(object data, bool write, bool visible)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
