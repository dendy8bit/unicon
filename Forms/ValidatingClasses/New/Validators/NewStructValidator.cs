﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses.ControlsSupportClasses;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;

namespace BEMN.Forms.ValidatingClasses.New.Validators
{
    public class NewStructValidator<T> : IValidator where T : StructBase
    {
        private IControlInfo[] _controlInfos;
        private ToolTip _toolTip;
        private T _currentStruct;
        private List<ComboboxSupport> _comboboxSupports;
        private List<TextboxSupport> _textboxSupports;
        private List<NumericUpDownSupport> _numericUpDownSupports;

        public NewStructValidator(ToolTip toolTip, params IControlInfo[] controlInfos)
        {
            _currentStruct = Activator.CreateInstance<T>();
            this._comboboxSupports = new List<ComboboxSupport>();
            this._textboxSupports = new List<TextboxSupport>();
            this._numericUpDownSupports =new List<NumericUpDownSupport>();
            this._controlInfos = controlInfos;
            this._toolTip = toolTip;
            foreach (var controlInfo in _controlInfos)
            {
                switch (controlInfo.ColumnType)
                {
                    case ColumnsType.CHEACK:
                    {
                        break;
                    }
                    case ColumnsType.COMBO:
                    {
                        ComboBox control = (ComboBox) controlInfo.CurrentControl;
                        control.Items.Clear();
                        control.Items.AddRange(controlInfo.Items.ToArray());
                        this._comboboxSupports.Add(new ComboboxSupport(control, controlInfo, toolTip));
                        break;
                    }
                    case ColumnsType.MULTI_COMBO:
                        {
                            ComboBox[] control = (ComboBox[]) controlInfo.CurrentControl;
                            foreach (ComboBox comboBox in control)
                            {
                                comboBox.Items.Clear();
                                comboBox.Items.AddRange(controlInfo.Items.ToArray());
                                this._comboboxSupports.Add(new ComboboxSupport(comboBox, controlInfo, toolTip));
                            }
                            break;
                        }
                    case ColumnsType.TEXT:
                    {
                        MaskedTextBox control = (MaskedTextBox) controlInfo.CurrentControl;
                        this._textboxSupports.Add(new TextboxSupport(control, controlInfo, toolTip));
                        break;
                    }

                    case ColumnsType.UPDOWNTEXT:
                    {
                        NumericUpDown control = (NumericUpDown)controlInfo.CurrentControl;
                        _numericUpDownSupports.Add(new NumericUpDownSupport(control,controlInfo,toolTip));
                        break;
                    }
                }
            }
            this.Reset();
        }

        public T Get()
        {
            int i = 0;
            foreach (var prop in this._currentStruct.Properties)
            {
                if (_controlInfos.Length <= i)
                {
                    break;
                }
                switch (_controlInfos[i].ColumnType)
                {
                    case ColumnsType.CHEACK:
                    {
                        var control = (CheckBox) _controlInfos[i].CurrentControl;
                        prop.Value.SetValue(this._currentStruct, control.Checked, null);
                        break;
                    }
                    case ColumnsType.TEXT:
                    {
                        var control = (MaskedTextBox) _controlInfos[i].CurrentControl;
                        prop.Value.SetValue(this._currentStruct, _controlInfos[i].Rule.Parse(control.Text), null);
                        break;
                    }
                    case ColumnsType.COMBO:
                    {
                        var control = (ComboBox) _controlInfos[i].CurrentControl;
                        prop.Value.SetValue(this._currentStruct, control.SelectedItem, null);
                        break;
                    }
                    case ColumnsType.MULTI_COMBO:
                    {
                        ComboBox[] combos = (ComboBox[]) this._controlInfos[i].CurrentControl;
                        prop.Value.SetValue(this._currentStruct, combos.Select(combo => (string) combo.SelectedItem).ToArray(), null);
                        break;
                    }
                    case ColumnsType.VALIDATOR:
                    {
                        var validator = (IValidator) _controlInfos[i].CurrentControl;
                        prop.Value.SetValue(this._currentStruct, validator.Get(), null);
                        break;
                    }
                    case  ColumnsType.UPDOWNTEXT:
                    {
                        var control = (NumericUpDown)_controlInfos[i].CurrentControl;
                        prop.Value.SetValue(this._currentStruct, control.Value, null);
                        break;
                    }
                }
                i++;
            }
            return this._currentStruct;
        }

        public void Set(T data)
        {
            int i = 0;
            this._currentStruct = data;
            foreach (var prop in data.Properties)
            {
                if (_controlInfos.Length <= i)
                {
                    break;
                }
                switch (_controlInfos[i].ColumnType)
                {
                    case ColumnsType.CHEACK:
                    {
                        var control = (CheckBox) _controlInfos[i].CurrentControl;
                        control.Checked = (bool) prop.Value.GetValue(data, null);
                        break;
                    }
                    case ColumnsType.TEXT:
                    {
                        var control = (MaskedTextBox) _controlInfos[i].CurrentControl;
                        control.Text = prop.Value.GetValue(data, null).ToString();
                        break;
                    }
                    case ColumnsType.COMBO:
                    {
                        var control = (ComboBox) _controlInfos[i].CurrentControl;
                        string value = (string) prop.Value.GetValue(data, null);
                        if (value == Validator.ERROR_VALUE)
                        {
                            control.Items.Add(value);

                        }
                        control.SelectedItem = value;
                        break;
                    }
                    case ColumnsType.MULTI_COMBO:
                    {
                        ComboBox[] combos = (ComboBox[]) this._controlInfos[i].CurrentControl;
                        string[] values = (string[]) prop.Value.GetValue(data, null);
                        for (int j = 0; j < combos.Length; j++)
                        {
                            if (values[j] == Validator.ERROR_VALUE)
                            {
                                combos[j].Items.Add(values[j]);
                            }
                            combos[j].SelectedItem = values[j];
                        }
                        break;
                    }
                    case ColumnsType.VALIDATOR:
                    {
                        var validator = (IValidator) _controlInfos[i].CurrentControl;
                        validator.Set(prop.Value.GetValue(this._currentStruct, null));

                        break;
                    }
                    case ColumnsType.UPDOWNTEXT:
                    {
                        var control = (NumericUpDown)_controlInfos[i].CurrentControl;
                        control.Value = Convert.ToDecimal(prop.Value.GetValue(data, null));
                        break;
                    }
                }
                i++;
            }

        }

        object IValidator.Get()
        {
            return this.Get();
        }

        public void Set(object data)
        {
            this.Set((T) data);
        }
        
        public void Reset()
        {
            try
            {
                foreach (var controlInfo in _controlInfos)
                {
                    switch (controlInfo.ColumnType)
                    {
                        case ColumnsType.CHEACK:
                        {
                            var control = (CheckBox) controlInfo.CurrentControl;
                            control.Checked = false;
                            break;
                        }

                        case ColumnsType.TEXT:
                        {
                            var control = (MaskedTextBox) controlInfo.CurrentControl;
                            control.Text = controlInfo.Rule.Min.ToString();
                            break;
                        }
                        case ColumnsType.COMBO:
                        {
                            var control = (ComboBox) controlInfo.CurrentControl;
                            control.SelectedItem = controlInfo.Items[0];
                            break;
                        }
                        case ColumnsType.MULTI_COMBO:
                            {
                                ComboBox[] combos = (ComboBox[])controlInfo.CurrentControl;
                                foreach (ComboBox c in combos)
                                {
                                    c.SelectedItem = controlInfo.Items[0];
                                }
                                break;
                            }
                        case ColumnsType.VALIDATOR:
                        {
                            IValidator validator = (IValidator) controlInfo.CurrentControl;
                            validator.Reset();
                            break;
                        }
                        case ColumnsType.UPDOWNTEXT:
                        {
                            var control = (NumericUpDown)controlInfo.CurrentControl;
                            control.Value = Convert.ToDecimal(controlInfo.Rule.Min);
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            {
                if (Validator.DEBUG)
                {
                    MessageBox.Show(string.Format("Ошибка в StructValidator.Reset() для класса {0}", typeof (T)));
                }
            }
        }

        public bool Check(bool write = true)
        {
            string str;
            return this.Check(out str, write);
        }

        public bool Check(out string message, bool write)
        {
            message = null;
            try
            {
                foreach (var controlInfo in _controlInfos)
                {

                    switch (controlInfo.ColumnType)
                    {
                        case ColumnsType.CHEACK:
                        {
                            break;
                        }
                        case ColumnsType.TEXT:
                        {
                            if (controlInfo.Rule.IsValid(((MaskedTextBox) controlInfo.CurrentControl).Text))
                            {
                                break;
                            }
                            else
                            {
                                var control = (Control) controlInfo.CurrentControl;
                                control.BackColor = Color.Red;
                                Validator.ShowControl(control);
                                _toolTip.Show(string.Empty, control, 0);
                                _toolTip.Show(
                                    String.Format("Диапазон {0} .. {1}", controlInfo.Rule.Min, controlInfo.Rule.Max),
                                    control, 0, control.Height);
                                return false;
                            }

                        }
                        case ColumnsType.COMBO:
                        {
                            string value = ((ComboBox) controlInfo.CurrentControl).SelectedItem.ToString();
                            if (value == Validator.ERROR_VALUE)
                            {
                                Validator.ShowControl((Control) controlInfo.CurrentControl);
                                return false;

                            }
                            else
                            {
                                break;
                            }
                        }
                        case ColumnsType.MULTI_COMBO:
                            {
                                ComboBox[] combos = (ComboBox[])controlInfo.CurrentControl;
                                string[] values = combos.Select(combo => (string)combo.SelectedItem).ToArray();
                                for (int i = 0; i < values.Length; i++)
                                {
                                    if (values[i] == Validator.ERROR_VALUE)
                                    {
                                        Validator.ShowControl(combos[i]);
                                        return false;
                                    }
                                }
                                break;
                            }
                        case ColumnsType.VALIDATOR:
                        {
                            var validator = (IValidator) controlInfo.CurrentControl;
                            var res = validator.Check(out message, write);
                            if (res)
                            {
                                break;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }

                }
                return true;
            }
            catch (Exception)
            {
                if (Validator.DEBUG)
                {
                    MessageBox.Show("Ошибка в NewStructValidator.CheckStruct(object data)");
                }

                return false;
            }
            //return this.CheckStruct(this.Get(),  write,true);
        }

        public bool CheckStruct(object data, bool write, bool visible)
        {
            try
            {
                int i = 0;
                var structData = (T) data;
                foreach (var prop in structData.Properties)
                {
                    if (i >= _controlInfos.Length)
                    {
                        break;
                    }
                    switch (_controlInfos[i].ColumnType)
                    {
                        case ColumnsType.CHEACK:
                        {
                            break;
                        }
                            
                        case ColumnsType.TEXT:
                        {

                            if (_controlInfos[i].Rule.IsValid(prop.Value.GetValue(data, null).ToString()))
                            {
                                break;

                            }
                            else
                            {
                                var control = (Control) _controlInfos[i].CurrentControl;
                                Validator.ShowControl(control);
                                control.BackColor = Color.Red;
                                _toolTip.Show(string.Empty, control, 0);
                                _toolTip.Show(
                                    String.Format("Диапазон {0} .. {1}", _controlInfos[i].Rule.Min,_controlInfos[i].Rule.Max), control, 0, control.Height);
                                return false;
                            }

                        }
                        case ColumnsType.COMBO:
                        {

                            string value = (string) prop.Value.GetValue(data, null);
                            if (value == Validator.ERROR_VALUE)
                            {
                                Validator.ShowControl((Control) _controlInfos[i].CurrentControl);
                                return false;

                            }
                            else
                            {
                                break;
                            }
                        }
                        case ColumnsType.MULTI_COMBO:
                        {
                            ComboBox[] combos = (ComboBox[]) this._controlInfos[i].CurrentControl;
                            string[] values = combos.Select(combo => (string) combo.SelectedItem).ToArray();
                            for (int j = 0; j < values.Length; j++)
                            {
                                if (values[j] == Validator.ERROR_VALUE)
                                {
                                    Validator.ShowControl(combos[j]);
                                    return false;
                                }
                            }
                            break;
                        }
                        case ColumnsType.VALIDATOR:
                        {
                            var validator = (IValidator) _controlInfos[i].CurrentControl;
                            var res = validator.CheckStruct(prop.Value.GetValue(this._currentStruct, null), write,
                                visible);
                            if (res)
                            {
                                break;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    i++;
                }
                return true;
            }
            catch (Exception)
            {
                if (Validator.DEBUG)
                {
                    MessageBox.Show("Ошибка в NewStructValidator.CheckStruct(object data)");
                }

                return false;
            }
        }
    }
}
