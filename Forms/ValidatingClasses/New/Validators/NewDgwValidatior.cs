﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;
using System.Reflection;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;

namespace BEMN.Forms.ValidatingClasses.New.Validators
{
    public class NewDgwValidatior<T> : IValidator where T : StructBase
    {
        private int _rows;
        private DataGridView _dgv;
        private T _currentStruct;
        private readonly IDataInfo[] _columnInfos;
        //private SortedDictionary<BindingPropertyAttribute, PropertyInfo> _properties;
        private SortedDictionary<int, PropertyInfo> _properties;
        private ToolTip _toolTip;

        object IValidator.Get()
        {
            return this.Get();
        }

        public void Set(object data)
        {
            try
            {
                this.Set((T) data);
            }
            catch (Exception)
            {
                if (Validator.DEBUG)
                {
                    MessageBox.Show("Ошибка в NewDgwValidatior.Set(object data)");
                }
            }
        }

        public NewDgwValidatior(DataGridView dgv, int rows, ToolTip toolTip, params IDataInfo[] columnInfos)
        {
            this._toolTip = toolTip;
            this._columnInfos = columnInfos;
            this._dgv = dgv;
            this._rows = rows;
            this._currentStruct = Activator.CreateInstance<T>();
            this._properties = this._currentStruct.Properties;

            if (dgv.Columns.Count != this._columnInfos.Length)
            {
                throw new ArgumentException("Количество столбцов не соответствует количеству описаний");
            }

            for (int i = 0; i < columnInfos.Length; i++)
            {
                if (columnInfos[i].ColumnType == ColumnsType.COMBO)
                {
                    DataGridViewComboBoxColumn col = (DataGridViewComboBoxColumn) dgv.Columns[i];
                    col.Items.Clear();
                    foreach (string item in columnInfos[i].Items)
                    {
                        col.Items.Add(item);
                    }
                }
            }

            this._dgv.Rows.Clear();
            this._dgv.Rows.Add(rows);

            this._dgv.EditingControlShowing += this.dgv_EditingControlShowing;
            this._dgv.CellClick += this._dgv_CellClick;
            this.Reset();
        }

        private void _dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ColumnsType columnType = this._columnInfos[e.ColumnIndex].ColumnType;

            if (columnType == ColumnsType.COMBO || columnType == ColumnsType.DEPENDENT_COMBO
                || columnType == ColumnsType.CONTROL_COMBO)
            {
                this._dgv.BeginEdit(true);
                ComboBox comboBox = (ComboBox) this._dgv.EditingControl;
                if (comboBox == null) return; //когда задизейблена ячейка,редактируемого контрола нет и выпадает исключение
                comboBox.DroppedDown = true;
            }
            if (columnType == ColumnsType.COLOR)
            {
                if (this._dgv.CurrentCell.Style.BackColor == Color.Red)
                {
                    this._dgv.CurrentCell.Style.BackColor = Color.Green;
                    this._dgv.CurrentCell.Style.SelectionBackColor = this._dgv.CurrentCell.Style.BackColor;
                }
                else
                {
                    this._dgv.CurrentCell.Style.BackColor = Color.Red;
                    this._dgv.CurrentCell.Style.SelectionBackColor = this._dgv.CurrentCell.Style.BackColor;
                }
            }
        }

        private void dgv_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl) // Только для текстовых ячеек
            {
                this._dgv.EditingControl.KeyPress += this.EditingControl_KeyPress;
                this._dgv.EditingControl.TextChanged += this.EditingControl_TextChanged;
                this._dgv.EditingControlShowing -= this.dgv_EditingControlShowing;
            }
            else if (e.Control is DataGridViewComboBoxEditingControl) // только для выпадающих списков
            {
                DataGridViewComboBoxEditingControl combo = (DataGridViewComboBoxEditingControl) e.Control;
                combo.DropDownStyle = ComboBoxStyle.DropDown;
                combo.AutoCompleteSource = AutoCompleteSource.ListItems;
                combo.AutoCompleteMode = AutoCompleteMode.Suggest;
                combo.KeyDown += this.ComboOnKeyDown;
                
                if (combo.Items.Contains(Validator.ERROR_VALUE))
                {
                    combo.EditingControlFormattedValue = combo.Items[0];
                    combo.Items.Remove(Validator.ERROR_VALUE);
                }
            }
        }

        private void ComboOnKeyDown(object sender, KeyEventArgs keyEventArgs)
        {
            ComboBox combo = sender as ComboBox;
            combo.DroppedDown = false;
            combo.KeyDown -= this.ComboOnKeyDown;
        }

        private void EditingControl_TextChanged(object sender, EventArgs e)
        {
            IDataInfo columnInfo = this._columnInfos[this._dgv.CurrentCell.ColumnIndex];

            if (columnInfo.ColumnType != ColumnsType.TEXT)
            {
                return;
            }

            TextBox tb = (TextBox) sender;
            bool flag = columnInfo.Rule.IsValid(tb.Text);
            DataGridView dgv = (DataGridView) tb.Parent.Parent;
            Panel panel = (Panel) tb.Parent;

            if (flag)
            {
                dgv.CurrentCell.Style.SelectionBackColor = SystemColors.Highlight;
                panel.BackColor = Color.White;

                dgv.CurrentCell.Style.BackColor = Color.White;
                tb.BackColor = Color.White;
                this._toolTip.Hide(tb);
            }
            else
            {
                dgv.CurrentCell.Style.SelectionBackColor = Color.Red;
                panel.BackColor = Color.Red;

                dgv.CurrentCell.Style.BackColor = Color.Red;
                tb.BackColor = Color.Red;
                this._toolTip.Show(columnInfo.Rule.ErrorMessage, tb, tb.Width, 0);
            }
        }

        private void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsDigit(e.KeyChar) | (e.KeyChar == '\b'));
        }

        public T Get()
        {
            this._dgv.EndEdit();
            for (int i = 0; i < this._rows; i++)
            {
                for (int j = 0; j < this._dgv.Columns.Count; j++)
                {
                    try
                    {
                        if (this._columnInfos[0].ColumnType == ColumnsType.NAME)
                        {
                            if (j == 0)
                            {
                                continue;
                            }
                            if (this._columnInfos[j].ColumnType == ColumnsType.COMBO)
                            {
                                this._properties.ElementAt(j - 1)
                                    .Value.SetValue(this._currentStruct, this._dgv[j, i].Value, new object[] {i});
                            }
                            if (this._columnInfos[j].ColumnType == ColumnsType.TEXT)
                            {
                                this._properties.ElementAt(j - 1)
                                    .Value.SetValue(this._currentStruct, this._columnInfos[j].Rule.Parse(this._dgv[j, i].Value.ToString()), new object[] {i});
                            }

                            if (this._columnInfos[j].ColumnType == ColumnsType.COLOR)
                            {
                                this._properties.ElementAt(j - 1)
                                    .Value.SetValue(this._currentStruct, this._dgv[j, i].Style.BackColor, new object[] {i});
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        if (Validator.DEBUG)
                        {
                            MessageBox.Show("NewDgwValidatior.Get\r\n" + e.ToString());
                        }
                    }
                }
            }
            return this._currentStruct;
        }

        public void Set(T data)
        {
            for (int i = 0; i < this._rows; i++)
            {
                for (int j = 0; j < this._dgv.Columns.Count; j++)
                {
                    try
                    {
                        if (this._columnInfos[0].ColumnType == ColumnsType.NAME)
                        {
                            if (j == 0)
                            {
                                this._dgv[j, i].Value = this._columnInfos[0].Items[i];
                            }
                            else
                            {
                                ParameterInfo[] f = this._properties.ElementAt(j - 1).Value.GetIndexParameters();

                                object value = this._properties.ElementAt(j - 1).Value.GetValue(data, f.Length > 0 ? new object[] {i} : null);

                                if (this._columnInfos[j].ColumnType == ColumnsType.COLOR)
                                {
                                    this._dgv[j, i].Style.BackColor = (Color) value;
                                    this._dgv[j, i].Style.SelectionBackColor = (Color) value;
                                    continue;
                                }
                                if (this._columnInfos[j].ColumnType == ColumnsType.TEXT)
                                {
                                    bool flag = this._columnInfos[j].Rule.IsValid(value.ToString());
                                    if (flag)
                                    {
                                        this._dgv[j, i].Style.BackColor = Color.White;
                                        this._dgv[j, i].Style.SelectionBackColor = Color.White;
                                        this._dgv[j, i].Value = value;
                                    }
                                    else
                                    {
                                        this._dgv[j, i].Style.BackColor = Color.Red;
                                        this._dgv[j, i].Style.SelectionBackColor = Color.Red;
                                        this._dgv[j, i].Value = Validator.ERROR_VALUE;
                                    }

                                    continue;
                                }
                                this._dgv[j, i].Value = value;
                            }
                        }
                        else
                        {
                            this._dgv[j, i].Value = this._properties.ElementAt(j).Value.GetValue(data, new object[] {i}).ToString();
                        }

                    }
                    catch (Exception e)
                    {
                        if (Validator.DEBUG)
                        {
                            MessageBox.Show("NewDgwValidatior.Set\r\n" + e.Message);
                        }
                    }
                }
            }
        }

        private void ResetCell(DataGridViewCell dataGridViewCell, IDataInfo info)
        {
            switch (info.ColumnType)
            {
                case ColumnsType.CHEACK:
                {
                    dataGridViewCell.Value = false;
                    break;
                }
                case ColumnsType.COLOR:
                {
                    dataGridViewCell.Style.BackColor = Color.Green;
                    dataGridViewCell.Style.SelectionBackColor = Color.Green;
                    break;
                }
                case ColumnsType.COMBO:
                {
                    dataGridViewCell.Value = info.Items[0];
                    break;
                }
                case ColumnsType.TEXT:
                {
                    dataGridViewCell.Value = info.Rule.Min;
                    break;
                }
            }
        }

        public void Reset()
        {
            for (int i = 0; i < this._rows; i++)
            {
                for (int j = 0; j < this._dgv.Columns.Count; j++)
                {
                    try
                    {
                        if (this._columnInfos[0].ColumnType == ColumnsType.NAME)
                        {
                            if (j == 0)
                            {
                                this._dgv[j, i].Value = this._columnInfos[0].Items[i];

                            }
                            else
                            {
                                this.ResetCell(this._dgv[j, i], this._columnInfos[j]);
                            }
                        }
                        else
                        {
                            this.ResetCell(this._dgv[j, i], this._columnInfos[j]);
                        }
                    }
                    catch (Exception e)
                    {
                        if (Validator.DEBUG)
                        {
                            MessageBox.Show("NewDgwValidatior.Set\r\n" + e.ToString());
                        }
                    }

                }
            }
        }

        public bool Check(out string message, bool write)
        {
            message = null;
            for (int i = 0; i < this._rows; i++)
            {
                for (int j = 0; j < this._dgv.Columns.Count; j++)
                {
                    try
                    {
                        if (this._columnInfos[0].ColumnType == ColumnsType.NAME)
                        {
                            if (j != 0)
                            {
                                if (this._columnInfos[j].ColumnType == ColumnsType.COMBO)
                                {
                                    if (this._dgv[j, i].Value.ToString() == Validator.ERROR_VALUE)
                                    {
                                        Validator.ShowControl(this._dgv);
                                        return false;
                                    }
                                }
                                if (this._columnInfos[j].ColumnType == ColumnsType.TEXT)
                                {
                                    if (!this._columnInfos[j].Rule.IsValid(this._dgv[j, i].Value.ToString()))
                                    {
                                        Validator.ShowControl(this._dgv);
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        if (Validator.DEBUG)
                        {
                            MessageBox.Show("NewDgwValidatior.Chech\r\n" + e.ToString());
                        }

                        return false;
                    }

                }
            }
            return true;
        }

        public bool CheckStruct(object data, bool write, bool visible)
        {
            for (int i = 0; i < this._rows; i++)
            {
                for (int j = 0; j < this._dgv.Columns.Count; j++)
                {
                    try
                    {
                        if (this._columnInfos[0].ColumnType == ColumnsType.NAME)
                        {
                            if (j != 0)
                            {
                                ParameterInfo[] f = this._properties.ElementAt(j - 1).Value.GetIndexParameters();
                                object value = this._properties.ElementAt(j - 1).Value.GetValue(data, f.Length > 0 ? new object[] {i} : null);
                                if (this._columnInfos[j].ColumnType == ColumnsType.COLOR)
                                {
                                    continue;
                                }
                                if (this._columnInfos[j].ColumnType == ColumnsType.TEXT)
                                {

                                    bool flag = this._columnInfos[j].Rule.IsValid(value.ToString());
                                    if (!flag)
                                    {
                                        Validator.ShowControl(this._dgv);
                                        return false;
                                    }
                                    continue;
                                }

                                if (this._columnInfos[j].ColumnType == ColumnsType.COMBO)
                                {
                                    if (value.ToString() == Validator.ERROR_VALUE)
                                    {
                                        Validator.ShowControl(this._dgv);
                                        return false;
                                    }
                                }
                            }
                        }
                        else
                        {
                            this._dgv[j, i].Value = this._properties.ElementAt(j).Value.GetValue(data, new object[] {i}).ToString();
                        }

                    }
                    catch (Exception e)
                    {
                        if (Validator.DEBUG)
                        {
                            MessageBox.Show("NewDgwValidatior.Set\r\n" + e.ToString());
                        }

                        return false;
                    }
                }
            }
            return true;
        }
    }
}
