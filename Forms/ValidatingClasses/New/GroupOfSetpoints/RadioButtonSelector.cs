﻿using System;
using System.Windows.Forms;

namespace BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints
{
    public class RadioButtonSelector : ISetpointsSelector
    {
        public event Action OnSelect;
        private readonly RadioButton _reserve;        
        private readonly RadioButton _main;
        private readonly Button _copyButton;
        public event Action NeedCopyGroup;
        private bool _checkForMR5;
        public RadioButtonSelector(RadioButton main, RadioButton reserve)
        {
            this._main = main;
            this._reserve = reserve;
            main.CheckedChanged +=main_CheckedChanged;
        }

        public RadioButtonSelector(RadioButton main, RadioButton reserve, Button copyButton, bool check = false)
        {
            this._main = main;
            this._reserve = reserve;
            _copyButton = copyButton;
            this._checkForMR5 = check;
            
            main.CheckedChanged += main_CheckedChanged;
            reserve.CheckedChanged += reserve_CheckedChanged;
            _copyButton.Click += _copyButton_Click;
        }
        

        void reserve_CheckedChanged(object sender, EventArgs e)
        {
            if (this._reserve.Checked)
            {
                if (_copyButton != null)
                {
                    this._copyButton.Text = "Резервные -> Основные";
                }
            }
        }

        void _copyButton_Click(object sender, EventArgs e)
        {
            if (this._checkForMR5)
            {
                if (NeedCopyGroup != null)
                {
                    NeedCopyGroup.Invoke();
                }
                return;
            }

            var res = MessageBox.Show("Вы уверены, что хотите копировать уставки?", _copyButton.Text, MessageBoxButtons.YesNo);
            if (res == DialogResult.Yes)
            {
                if (NeedCopyGroup != null)
                {
                    NeedCopyGroup.Invoke();
                }
            }
        }

        private void main_CheckedChanged(object sender, EventArgs e)
        {
            if (this._main.Checked)
            {
                if (_copyButton != null)
                {
                    this._copyButton.Text = "Основные -> Резервные";
                }

            }
            if (this.OnSelect != null)
            {
                this.OnSelect.Invoke();
            }
        }



        public int SelectedGroup
        {
            get
            {
                if (this._main.Checked)
                {
                    return 0;
                }
                return 1;
            }
            set
            {
                this._main.Checked = value == 0;
                this._reserve.Checked = value == 1;
            }
        }

        public int GroupCount
        {
            get { return 2; }
        }

        public string GetErrorMeessage(int number)
        {
            if (number == 0)
            {
                return "Обнаружены ошибки в основной группе уставок.";
            }
            return "Обнаружены ошибки в резервной группе уставок.";
        }
    }
}
