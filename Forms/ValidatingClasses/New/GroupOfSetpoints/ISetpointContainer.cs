﻿using BEMN.Devices.Structures;

namespace BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints
{
    public interface ISetpointContainer<T> where T:StructBase
    {
        T[] Setpoints { get; set; }
    }
}
