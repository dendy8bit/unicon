﻿using System;
using System.Windows.Forms;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses.New.Validators;

namespace BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints
{
    public class SetpointsValidator<T, T1> : IValidator where T : StructBase, ISetpointContainer<T1>
        where T1 : StructBase
    {
        private ISetpointsSelector _selector;
        private T _data;
        private IValidator _validator;
        private int _oldGroup;

        public SetpointsValidator(ISetpointsSelector selector, IValidator validator)
        {
            this._selector = selector;
            this._data = Activator.CreateInstance<T>();
            this._validator = validator;
            this._oldGroup = this._selector.SelectedGroup;
            this._selector.OnSelect += _selector_OnSelect;
            selector.NeedCopyGroup += selector_NeedCopyGroup;
        }

        void selector_NeedCopyGroup()
        {
            this.CopySetpoint();
        }

        void _selector_OnSelect()
        {
            int setpointIndex = this._selector.SelectedGroup;
            //Копия всех уставок
            T1[] allSetpoints = this._data.Setpoints;
            //текущие уставки на эране
            object currentSetpoints = this._validator.Get();
            allSetpoints[this._oldGroup] = (T1) currentSetpoints;
            this._data.Setpoints = allSetpoints;
            this.ShowSetpoint();
            this._oldGroup = setpointIndex;
        }

        private void ShowSetpoint()
        {
            int setpointIndex = this._selector.SelectedGroup;
            T1[] allSetpoints = this._data.Setpoints;
            this._validator.Set(allSetpoints[setpointIndex]);
        }

        /// <summary>
        /// Копирует группу уставок на основании значения в селекторе
        /// </summary>
        public void CopySetpoint()
        {
            try
            {
                this.Get();
                T1[] allSetpoints = this._data.Setpoints;
                allSetpoints[_selector.GroupCount - 1 - _selector.SelectedGroup] =
                    allSetpoints[_selector.SelectedGroup].Clone<T1>();
                this._data.Setpoints = allSetpoints;
            }
            catch (Exception)
            {

                this.ShowErrorMessage("CopySetpoint");
            }
        }

        public object Get()
        {
            int setpointIndex = this._selector.SelectedGroup;
            //Копия всех уставок
            T1[] allSetpoints = this._data.Setpoints;
            //текущие уставки на эране
            object currentSetpoints = this._validator.Get();


            allSetpoints[setpointIndex] = (T1) currentSetpoints;

            this._data.Setpoints = allSetpoints;
            return this._data;
        }

        public void Set(object data)
        {
            try
            {
                this._data = (T) data;
                this.ShowSetpoint();
            }
            catch (Exception)
            {
                this.ShowErrorMessage("Get");
            }
        }

        private void ShowErrorMessage(string methodName)
        {
            MessageBox.Show(string.Format("Ошибка в SetpointsValidator<T,T1>.{0}() для класса {1}", methodName,
                this.GetType().GetGenericArguments()[0]));
        }

        public void Reset()
        {
            try
            {
                this._validator.Reset();
                T1 defaultValue = (T1) this._validator.Get();
                T1[] setpoints = this._data.Setpoints;
                for (int i = 0; i < this._selector.GroupCount; i++)
                {
                    setpoints[i] = defaultValue.Clone<T1>();
                }
                this._data.Setpoints = setpoints;
            }
            catch (Exception)
            {
                this.ShowErrorMessage("Reset");
            }
        }


        public bool Check(out string message, bool write)
        {
            message = null;
            try
            {

                for (int i = 0; i < this._selector.GroupCount; i++)
                {
                    if (!this._validator.CheckStruct(this._data.Setpoints[i], write, this._selector.SelectedGroup == i))
                    {

                        message = this._selector.GetErrorMeessage(i);
                        this._selector.SelectedGroup = i;
                        this.ShowSetpoint();
                        return false;
                    }

                }
                return true;
            }
            catch (Exception)
            {
                this.ShowErrorMessage("Check");
                return false;
            }
        }

        //Не должно вызываться
        public bool CheckStruct(object data, bool write, bool visible)
        {
            throw new NotImplementedException();
        }
    }
}
