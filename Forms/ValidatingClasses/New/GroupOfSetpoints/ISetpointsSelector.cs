﻿using System;

namespace BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints
{
   public interface ISetpointsSelector
   {
       event Action OnSelect;
       int SelectedGroup { get; set; }
       int GroupCount { get; }
       string GetErrorMeessage(int number);
       event Action NeedCopyGroup;
   }
}
