﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints
{
    public class ComboboxSelector : ISetpointsSelector
    {
        public event Action OnSelect;
        public event Action OnRefreshInfoTable;

        private readonly ComboBox _comboBox;

        public ComboboxSelector(ComboBox comboBox, List<string> groupsNames)
        {
            this._comboBox = comboBox;
            this._comboBox.Items.Clear();
            this._comboBox.Items.AddRange(groupsNames.ToArray());
            this.GroupCount = groupsNames.Count;
            this._comboBox.SelectedIndexChanged += _comboBox_SelectedIndexChanged;
            this._comboBox.SelectedIndex = 0;
        }

        void _comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnSelect?.Invoke();
            OnRefreshInfoTable?.Invoke();
        }

        public int SelectedGroup
        {
            get
            {
                if (this._comboBox.SelectedIndex < 0)
                {
                    return 0;
                }
                else
                {
                    return this._comboBox.SelectedIndex;
                }
            }
            set { this._comboBox.SelectedIndex = value; }
        }


        public int GroupCount { get; private set; }
        

        public string GetErrorMeessage(int number)
        {
        return    string.Format("Обнаружены ошибки в уставках \"{0}\"", this._comboBox.Items[number]);
        }

        public event Action NeedCopyGroup;
    }
}
