﻿using BEMN.Devices.Structures;

namespace BEMN.Forms.ValidatingClasses.New
{
   public interface IDgvRowsContainer<T> where T:StructBase 
    {
       T[] Rows { get; set; }
    }
}
