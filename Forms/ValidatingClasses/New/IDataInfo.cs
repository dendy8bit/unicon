﻿using System.Collections.Generic;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;

namespace BEMN.Forms.ValidatingClasses.New
{
    public interface IDataInfo
    {
        ColumnsType ColumnType { get; }
        List<string> Items { get; }
        IValidatingRule Rule { get; }

    }
}