﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using BEMN.MBServer;
using Microsoft.Win32;

namespace BEMN.Forms.ValidatingClasses
{
    public static class Validator
    {
        public const string ERROR_VALUE = "XXXXX";

        public static bool DEBUG
        {
            get { return false; }
        }

        public static string CreateOscFileNameCfg(string fileName)
        {
            string directory = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "TempOsc");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            
            return Path.Combine(directory, fileName) + ".cfg";
        }

        /// <summary>
        /// Метод, который проверяет версию .Net Framework, установленную на компьютере.
        /// Информация об установленных в системе версиях .Net хранится в реестре.
        /// Для ее получения надо открыть РЕДАКТОР РЕЕСТРА и перейти в раздел
        /// HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\NET Framework Setup\NDP,
        /// в котором перечислены все установленные в системе версии.
        /// Возвращает true - если есть версия, которая начинается с букв "v4", false - в остальных случаях".
        /// </summary>
        public static bool GetVersionFromRegistry()
        {
            try
            {
                using (RegistryKey ndpKey = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, "")
                    .OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP\"))
                {
                    foreach (string versionKeyName in ndpKey.GetSubKeyNames())
                    {
                        if (versionKeyName.StartsWith("v4"))
                        {
                            return true;
                        }
                    }
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void CellEnabled(DataGridViewCell cell, bool enabled)
        {
            var comboCell = cell as DataGridViewComboBoxCell;
            if (comboCell != null)
            {
                comboCell.DisplayStyle = enabled
                    ? DataGridViewComboBoxDisplayStyle.ComboBox
                    : DataGridViewComboBoxDisplayStyle.DropDownButton;
            }
            if (enabled)
            {
                cell.ReadOnly = false;
                if (cell.Style.BackColor != Color.Red)
                {
                    cell.Style.BackColor = Color.White;
                }

            }
            else
            {
                cell.ReadOnly = true;
                cell.Style.BackColor = SystemColors.Control;

            }
        }

        public static string GetJornal(ushort value, List<string> items, params int[] indexes)
        {
            if (indexes.Length != 0)
            {
                value = (ushort) (Common.GetBits(value, indexes) >> indexes[0]);
            }

            if (value >= items.Count)
            {
                return string.Format("Код - {0}", value);
            }
            return items[value];
        }

        public static string GetJornal<T>(T value, Dictionary<T, string> items)
        {
            if (items.ContainsKey(value))
            {
                return items[value];
            }
            else
            {
                return string.Format("Код - {0}", value);
            }
        }

        public static string Get(ushort value, List<string> items, params int[] indexes)
        {
            value = (ushort) (Common.GetBits(value, indexes) >> indexes[0]);
            if (value >= items.Count)
            {
                return ERROR_VALUE;
            }
            return items[value];
        }


        public static string GetForItkzV6(ushort value, List<string> items, params int[] indexes)
        {
            value = (ushort)(Common.GetBits(value, indexes) >> indexes[0]);
            if (value >= 4) return items[value - 1];
            return items[value];
        }


        public static string Get(ushort value, List<string> items)
        {
            if (value >= items.Count)
            {
                return ERROR_VALUE;
            }
            return items[value];
        }

        public static string Get(ushort value, Dictionary<ushort, string> items, params int[] indexes)
        {
            value = (ushort) (Common.GetBits(value, indexes) >> indexes[0]);
            if (!items.ContainsKey(value))
            {
                return ERROR_VALUE;
            }
            return items[value];
        }

        public static string Get(ushort value, Dictionary<ushort, string> items)
        {
            if (!items.ContainsKey(value))
            {
                return ERROR_VALUE;
            }
            return items[value];
        }

        public static ushort Set(string value, List<string> items)
        {
            if (value == ERROR_VALUE)
            {
                return ushort.MaxValue;
            }
            return (ushort) items.IndexOf(value);
        }

        public static ushort Set(string value, Dictionary<ushort, string> items)
        {
            ushort keyValue = 0;

            if (value == ERROR_VALUE)
            {
                return ushort.MaxValue;
            }
            try
            {
                keyValue = items.First(v => v.Value == value).Key;
            }
            catch (Exception e)
            {

            }
            return keyValue;
        }

        public static ushort Set(string value, List<string> items, ushort startValue, params int[] indexes)
        {
            if (indexes.Length == 0)
            {
                if (Validator.DEBUG)
                {
                    MessageBox.Show("ошибка Validator.Set, массив индексов пуст");
                }
            }
            if (value == ERROR_VALUE)
            {
                return Common.SetBits(startValue, ushort.MaxValue, indexes);

            }
            var newValue = (ushort) items.IndexOf(value);
            return Common.SetBits(startValue, newValue, indexes);
        }

        public static ushort Set(string value, Dictionary<ushort, string> items, ushort startValue, params int[] indexes)
        {
            if (indexes.Length == 0)
            {
                if (Validator.DEBUG)
                {
                    MessageBox.Show("ошибка Validator.Set, массив индексов пуст");
                }
            }
            if (value == ERROR_VALUE)
            {
                return Common.SetBits(startValue, ushort.MaxValue, indexes);
            }
            var newValue = items.First(v => v.Value == value).Key;
            return Common.SetBits(startValue, newValue, indexes);
        }

        class ShowControlData
        {
            private TabControl _tabControl;
            private TabPage _tabPage;

            public ShowControlData(TabControl tabControl, TabPage tabPage)
            {
                this._tabControl = tabControl;
                this._tabPage = tabPage;
            }

            public TabPage TabPage
            {
                get { return _tabPage; }
            }

            public TabControl TabControl
            {
                get { return _tabControl; }
            }
        }

        public static bool ShowControl(Control control, ToolTip toolTip, string message)
        {
            Stack<ShowControlData> actions = new Stack<ShowControlData>();
            if (!control.Visible)
            {
                Control parent = control.Parent;
                while (parent != null)
                {
                    if (parent is TabPage)
                    {
                        //    () => ((TabControl)parent.Parent).SelectTab(((TabPage)parent))
                        actions.Push(new ShowControlData((TabControl) parent.Parent, (TabPage) parent));

                        // break;
                    }
                    parent = parent.Parent;
                }

                while (actions.Count > 0)
                {
                    try
                    {
                        var a = actions.Pop();
                        a.TabControl.SelectTab(a.TabPage);
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            toolTip.Show(message, control);
            return control.Focus();
        }

        public static bool ShowControl(Control control)
        {
            Stack<ShowControlData> actions = new Stack<ShowControlData>();
            if (!control.Visible)
            {
                Control parent = control.Parent;
                while (parent != null)
                {
                    if (parent is TabPage)
                    {
                        actions.Push(new ShowControlData((TabControl) parent.Parent, (TabPage) parent));
                    }
                    parent = parent.Parent;
                }

                while (actions.Count > 0)
                {
                    try
                    {
                        var a = actions.Pop();
                        a.TabControl.SelectTab(a.TabPage);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            return control.Focus();
        }
    }
}