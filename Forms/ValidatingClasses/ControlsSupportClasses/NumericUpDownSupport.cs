﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;

namespace BEMN.Forms.ValidatingClasses.ControlsSupportClasses
{
    public class NumericUpDownSupport
    {
        private NumericUpDown _numericUpDown;
        private IControlInfo _controlInfo;
        private ToolTip _toolTip;
        public NumericUpDownSupport(NumericUpDown numericUpDown, IControlInfo controlInfo, ToolTip toolTip)
        {
            _numericUpDown = numericUpDown;
            _controlInfo = controlInfo;
            _toolTip = toolTip;
            _numericUpDown.PreviewKeyDown += numericUpDown_PreviewKeyDown;
            _numericUpDown.KeyPress += numericUpDown_KeyPress;
            _numericUpDown.TextChanged += numericUpDown_TextChanged;
            _numericUpDown.LostFocus += numericUpDown_LostFocus;
            this._numericUpDown.Value = Decimal.Parse(controlInfo.Rule.Min.ToString());
        }


        private void numericUpDown_LostFocus(object sender, EventArgs e)
        {
            _toolTip.Hide(_numericUpDown);
        }

        private void numericUpDown_TextChanged(object sender, EventArgs e)
        {
            if (this._controlInfo.Rule.IsValid(_numericUpDown.Value.ToString()))
            {
                _numericUpDown.BackColor = Color.White;
                _toolTip.Hide(_numericUpDown);
            }
            else
            {
                _numericUpDown.BackColor = Color.Red;
                _toolTip.Show(string.Empty, _numericUpDown, 0);
                //_toolTip.Show(String.Format("Диапазон {0} .. {1}", _controlInfo.Rule.Min, _controlInfo.Rule.Max), this._textBox, 0, _textBox.Height);
                _toolTip.Show(this._controlInfo.Rule.ErrorMessage, _numericUpDown, 0, _numericUpDown.Height);
            }
        }

        void numericUpDown_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Control)
            {
                if ((e.KeyCode == Keys.Z) || (e.KeyCode == Keys.X) || (e.KeyCode == Keys.C) || (e.KeyCode == Keys.V))
                {
                    _comandFlag = true;
                    return;
                }
            }
            _comandFlag = false;
        }

        private bool _comandFlag;

        void numericUpDown_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (_comandFlag)
            {
                _comandFlag = false;
                return;
            }
            e.Handled = _controlInfo.Rule.IsValidKey(e.KeyChar);
        }
    }
}
