﻿using BEMN.Forms.ValidatingClasses.New;
using BEMN.Forms.ValidatingClasses.Rules.ComboBox;
using System;
using System.Windows.Forms;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;

namespace BEMN.Forms.ValidatingClasses.ControlsSupportClasses
{
    public class ComboboxSupport
    {
        private ComboBox _comboBox;
        private ToolTip _toolTip;
        private IControlInfo _controlInfo;
        private string _oldValue;
        private bool _comandFlag;

        public ComboboxSupport(ComboBox comboBox, IControlInfo controlinfo, ToolTip toolTip)
        {
            this._toolTip = toolTip;
            this._comboBox = comboBox;
            this._controlInfo = controlinfo;
            this._comboBox.DropDown += this._comboBox_DropDown;
            this._comboBox.KeyPress += this.combobox_KeyPress;
            this._comboBox.PreviewKeyDown += this.combobox_PreviewKeyDown;
            this._comboBox.GotFocus += this._combobox_GotFocus;
            //this._comboBox.LostFocus += this._combobox_LostFocus;
            this._comboBox.KeyUp += this.combobox_KeyUp;
        }

        private void _combobox_LostFocus(object sender, EventArgs e)
        {
            ComboBoxValidateRule rule = this._controlInfo.Rule as ComboBoxValidateRule;
            int i = rule.IsValid(this._comboBox.Text, this._controlInfo.Items);
            try
            {
                if (i == -1)
                {
                    this._toolTip.Show(this._controlInfo.Rule.ErrorMessage, this._comboBox, 0, this._comboBox.Height, 1000);
                    this._comboBox.Text = this._oldValue;
                }
                else
                {
                    this._comboBox.Text = this._controlInfo.Items[i];
                }
            }
            catch
            {
                this._comboBox.Text = this._oldValue;
            }
        }

        private void _combobox_GotFocus(object sender, EventArgs e)
        {
            this._oldValue = this._comboBox.Text;
        }

        private void _comboBox_DropDown(object sender, EventArgs e)
        {
            this._toolTip.Hide(this._comboBox);
            if (this._comboBox.Items.Contains(Validator.ERROR_VALUE))
            {
                this._comboBox.Items.Remove(Validator.ERROR_VALUE);
                this._comboBox.SelectedIndex = 0;
                this._comboBox.KeyPress += this.combobox_KeyPress; 
            }
        }

        private void combobox_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Control)
            {
                if ((e.KeyCode == Keys.Z) || (e.KeyCode == Keys.X) || (e.KeyCode == Keys.C) || (e.KeyCode == Keys.V))
                {
                    this._comandFlag = true;
                    return;
                }
            }
            this._comandFlag = false;
        }

        private void combobox_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (this._comandFlag)
            {
                this._comandFlag = false;
                return;
            }
            e.Handled = this._controlInfo.Rule.IsValidKey(e.KeyChar);
            this._comboBox.DroppedDown = false;
        }

        private void combobox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //this._combobox_LostFocus(sender, e);
            }
        }

    }
}