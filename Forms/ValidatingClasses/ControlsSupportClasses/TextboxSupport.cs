﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;

namespace BEMN.Forms.ValidatingClasses.ControlsSupportClasses
{
    public class TextboxSupport
    {
        private MaskedTextBox _textBox;
        private IControlInfo _controlInfo;
        private ToolTip _toolTip;
        private bool _comandFlag;

        /// <summary>
        /// Статическое свойство класса TextboxSupport, false - когда поле типа MaskedTextBox не прошло валидацию - горит красным цветом.  
        /// </summary>
        public static bool PassedValidation { get; set; }
        public TextboxSupport(MaskedTextBox textBox, IControlInfo controlInfo, ToolTip toolTip)
        {
            this._textBox = textBox;
            this._controlInfo = controlInfo;
            this._toolTip = toolTip;
            this._textBox.PreviewKeyDown += this.textBox_PreviewKeyDown;
            this._textBox.KeyPress += this.textBox_KeyPress;
            this._textBox.TextChanged += this.textBox_TextChanged;
            this._textBox.LostFocus += this.textBox_LostFocus;
            this._textBox.Text = controlInfo.Rule.Min.ToString();
        }

        private void textBox_LostFocus(object sender, EventArgs e)
        {
            this._toolTip.Hide(this._textBox);
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            if (this._controlInfo.Rule.IsValid(this._textBox.Text))
            {
                this._textBox.BackColor = Color.White;
                this._toolTip.Hide(this._textBox);
                PassedValidation = true;
            }
            else
            {
                this._textBox.BackColor = Color.Red;
                this._toolTip.Show(string.Empty, this._textBox, 0);
                //_toolTip.Show(String.Format("Диапазон {0} .. {1}", _controlInfo.Rule.Min, _controlInfo.Rule.Max), this._textBox, 0, _textBox.Height);
                this._toolTip.Show(this._controlInfo.Rule.ErrorMessage, this._textBox, 0, this._textBox.Height);
                PassedValidation = false;
            }
        }

        void textBox_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {         
            if (e.Control)
            {
                if ((e.KeyCode == Keys.Z)|| (e.KeyCode == Keys.X)|| (e.KeyCode == Keys.C)|| (e.KeyCode == Keys.V))
                {
                    this._comandFlag = true;
                    return;
                }
            }
            this._comandFlag = false;
        }

        void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (this._comandFlag)
            {
                this._comandFlag = false;
                return;
            }
            e.Handled = this._controlInfo.Rule.IsValidKey(e.KeyChar);
        }
    }   
}