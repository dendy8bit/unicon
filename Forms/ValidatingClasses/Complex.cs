﻿using System;
using System.Globalization;

namespace BEMN.Forms.ValidatingClasses
{
    public struct Complex
    {
        private double _real;
        private double _imaginary;

        public Complex(double real, double imaginary) //конструктор
        {
            this._real = real; // вещественное число
            this._imaginary = imaginary; // мнимое число
        }

        // Объявление  оператора  (+)
        public static Complex operator +(Complex c1, Complex c2)
        {
            return new Complex(c1._real + c2._real, c1._imaginary + c2._imaginary);
        }

        // Объявление  оператора  (-)  
        public static Complex operator -(Complex c1, Complex c2)
        {
            return new Complex(c1._real - c2._real, c1._imaginary - c2._imaginary);
        }

        // Объявление  оператора  (*)
        public static Complex operator *(Complex c1, Complex c2)
        {
            return new Complex(c1._real*c2._real - c1._imaginary*c2._imaginary,
                c1._real*c2._imaginary + c1._imaginary*c2._real);
        }

        public static Complex operator *(double d, Complex c)
        {
            return new Complex(c._real*d, c._imaginary*d);
        }

        public static Complex operator *(Complex c, double d)
        {
            return new Complex(c._real * d, c._imaginary * d);
        }

        // Объявление  оператора  (/)
        public static Complex operator / (Complex c1, Complex c2)
        {
            double denominator = c2._real*c2._real + c2._imaginary*c2._imaginary;

            return new Complex((c1._real*c2._real + c1._imaginary*c2._imaginary)/denominator,
                (c2._real*c1._imaginary - c2._imaginary*c1._real)/denominator);
        }
        
        //комплексное число в традиционном формате
        public override string ToString()
        {
            return this._imaginary < 0
                ? string.Format("{0:F4} - j{1:F4}", this._real, -1*this._imaginary)
                : string.Format("{0:F4} + j{1:F4}", this._real, this._imaginary);
        }

        public string Mod
        {
            get
            {
                if (double.IsNaN(this._real) || double.IsNaN(this._imaginary))
                    return "Не определено";
                return Math.Sqrt(this._real*this._real + this._imaginary*this._imaginary).ToString("F2", CultureInfo.CurrentCulture);
            }
        }

        public string Argument
        {
            get
            {
                if (double.IsNaN(this._real) || double.IsNaN(this._imaginary))
                    return "Не определено";
                return (Math.Atan2(this._imaginary, this._real) / Math.PI * 180).ToString("F2", CultureInfo.CurrentCulture);
            }
        }
    }
}
