﻿using System;
using System.Linq;
using System.Threading;

namespace BEMN.Forms.ValidatingClasses.Rules
{
    public class CustomFloatRule : ValidatingRuleBase<float>
    {
        private float _min;
        private float _max;
        private int _digits;

        public CustomFloatRule(float min, float max):this(min, max, 5){ }

        public CustomFloatRule(float min, float max, int digits)
        {
            this._min = min;
            this._max = max;
            this._digits = digits;
        }

        public override float Parse(string value)
        {
            return (float )Math.Round(float.Parse(value), this._digits);
        }

        public override float Min
        {
            get { return this._min; }
        }

        public override float Max
        {
            get { return this._max; }
        }

        public override bool IsValidKey(char key)
        {
            return !(char.IsDigit(key) | (key == '\b') | Thread.CurrentThread.CurrentUICulture.NumberFormat.NumberDecimalSeparator.Contains(key));
        }

        public override string ErrorMessage
        {
            get { return string.Format("Диапазон {0:F2} .. {1:F2}", Min, Max); }
        }
    }
}
