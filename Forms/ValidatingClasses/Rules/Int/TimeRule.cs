﻿namespace BEMN.Forms.ValidatingClasses.Rules.Int
{
    public class TimeRule : ValidatingRuleBase<int>
    {
        public override int Parse(string value)
        {
            return int.Parse(value);
        }

        public override int Min
        {
            get { return 0; }
        }

        public override int Max
        {
            get { return 3276700; }
        }

        public override bool IsValidKey(char key)
        {
            return !(char.IsDigit(key) | (key == '\b'));
        }
    }
}
