﻿namespace BEMN.Forms.ValidatingClasses.Rules.Int
{
    public class CustomIntRule : ValidatingRuleBase<int>
    {
        private int _min;
        private int _max;

        public CustomIntRule(int min, int max)
        {
            _min = min;
            _max = max;
        }

        public override bool IsValidKey(char key)
        {
            return !(char.IsDigit(key) | (key == '\b'));
        }

        public override int Parse(string value)
        {
            return int.Parse(value);
        }

        public override int Min
        {
            get { return _min; }
        }

        public override int Max
        {
            get { return _max; }
        }
    }
}
