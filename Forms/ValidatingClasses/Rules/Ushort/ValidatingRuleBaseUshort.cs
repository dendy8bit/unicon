﻿namespace BEMN.Forms.ValidatingClasses.Rules.Ushort
{
  public abstract class ValidatingRuleBaseUshort : ValidatingRuleBase<ushort>
    {
        public override bool IsValidKey(char key)
        {
            return !(char.IsDigit(key) | (key == '\b'));
        }
    }
}
