﻿namespace BEMN.Forms.ValidatingClasses.Rules.Ushort
{
    public class Ushort100To4000Rule : ValidatingRuleBaseUshort
    {
        public override ushort Parse(string value)
        {
            return ushort.Parse(value);
        }

        public override ushort Min
        {
            get { return 100; }
        }

        public override ushort Max
        {
            get { return 4000; }
        }
    }
}
