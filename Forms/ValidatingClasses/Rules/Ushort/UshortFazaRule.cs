﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BEMN.Forms.ValidatingClasses.Rules.Ushort
{
    public class UshortFazaRule : ValidatingRuleBaseUshort
    {
        public override ushort Parse(string value)
        {
            try
            {
                return ushort.Parse(value);
            }
            catch (Exception)
            {

                return 0;
            }
        }

        public override ushort Min
        {
            get { return 0; }
        }

        public override ushort Max
        {
            get { return 359; }
        }
    }
}
