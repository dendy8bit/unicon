﻿namespace BEMN.Forms.ValidatingClasses.Rules.Ushort
{
    /// <summary>
    /// Целое 0..65535
    /// </summary>
    public class UshortRule : ValidatingRuleBaseUshort
    {
        public override ushort Parse(string value)
        {
            return ushort.Parse(value);
        }

        public override ushort Min
        {
            get { return 0x0; }
        }

        public override ushort Max
        {
            get { return 0xffff; }
        }
    }
}
