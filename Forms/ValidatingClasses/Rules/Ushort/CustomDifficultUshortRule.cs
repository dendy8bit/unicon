﻿using System;

namespace BEMN.Forms.ValidatingClasses.Rules.Ushort
{
    // Пример: диапазон от 100 до 10000 и 0 - вне диапазона
    // new CustomDifficultUshortRule(0, 100, 10000)
    public class CustomDifficultUshortRule : ValidatingRuleBaseUshort
    {
        public override ushort Min { get; }
        public override ushort Max { get; }
        public ushort UnicValue { get; } // уникальное значение вне диапазона
         
        public CustomDifficultUshortRule(ushort unicValue, ushort min, ushort max)
        {
            this.Min = min;
            this.Max = max;
            this.UnicValue = unicValue;
        }
        public override ushort Parse(string value) => ushort.Parse(value);
        
        public override bool IsValid(ushort value)
        {
            return (value.CompareTo(this.Min) >= 0 && value.CompareTo(this.Max) <= 0 || value.CompareTo(this.UnicValue) == 0);
        }

        public override string ErrorMessage => $"Число должно быть {this.UnicValue} или в диапазоне {this.Min}..{this.Max}.";
    }
}
