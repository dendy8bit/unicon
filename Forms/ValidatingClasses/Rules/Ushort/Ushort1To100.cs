﻿namespace BEMN.Forms.ValidatingClasses.Rules.Ushort
{
    public class Ushort1To100Rule : ValidatingRuleBaseUshort
    {
        public override ushort Parse(string value)
        {
            return ushort.Parse(value);
        }

        public override ushort Min
        {
            get { return 1; }
        }

        public override ushort Max
        {
            get { return 100; }
        }
    }
}
