﻿namespace BEMN.Forms.ValidatingClasses.Rules.Ushort
{
    public class CustomUshortRule : ValidatingRuleBaseUshort
    {
        private ushort _min;
        private ushort _max;
        public CustomUshortRule(ushort min, ushort max)
        {
            this._min = min;
            this._max = max;
        }
        public override ushort Parse(string value)
        {
            return ushort.Parse(value);
        }

        public override ushort Min
        {
            get { return this._min; }
        }

        public override ushort Max
        {
            get { return this._max; }
        }
    }
}
