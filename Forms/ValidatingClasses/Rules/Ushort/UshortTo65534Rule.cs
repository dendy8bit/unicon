﻿namespace BEMN.Forms.ValidatingClasses.Rules.Ushort
{
    public class UshortTo65534Rule : ValidatingRuleBaseUshort
    {
        public override ushort Parse(string value)
        {
            return ushort.Parse(value);
        }

        public override ushort Min
        {
            get { return 0; }
        }

        public override ushort Max
        {
            get { return 65534; }
        }
    }
}
