﻿namespace BEMN.Forms.ValidatingClasses.Rules.Byte
{
    public class CustomByteRule : ValidatingRuleBase<byte>
    {
        private byte _min;
        private byte _max;

        public CustomByteRule(byte min, byte max)
        {
            _min = min;
            _max = max;
        }
        public override byte Parse(string value)
        {
            return byte.Parse(value);
        }

        public override byte Min
        {
            get { return _min; }
        }

        public override byte Max
        {
            get { return _max; }
        }

        public override bool IsValidKey(char key)
        {
            return !(char.IsDigit(key) | (key == '\b'));
        }
    }
}
