﻿using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Int;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;

namespace BEMN.Forms.ValidatingClasses.Rules
{
    public static class RulesContainer
    {
        public static readonly DoubleTo01Rule DoubleTo01 = new DoubleTo01Rule();
        public static readonly DoubleTo04Rule DoubleTo04 = new DoubleTo04Rule();
        public static readonly DoubleTo1Rule DoubleTo1 = new DoubleTo1Rule();

        public static readonly Ustavka5Rule Ustavka5 = new Ustavka5Rule();
        public static readonly Ustavka40Rule Ustavka40 = new Ustavka40Rule();
        public static readonly Ustavka40To60Rule Ustavka40To60 = new Ustavka40To60Rule();
        public static readonly Ustavka100Rule Ustavka100 = new Ustavka100Rule();
        public static readonly Ustavka128Rule Ustavka128 = new Ustavka128Rule();
        public static readonly Ustavka150Rule Ustavka150 = new Ustavka150Rule();
        public static readonly Ustavka256Rule Ustavka256 = new Ustavka256Rule();
        public static readonly Ustavka400Rule Ustavka400 = new Ustavka400Rule();
        public static readonly Ustavka128000Rule Ustavka128000 = new Ustavka128000Rule();

        public static readonly UshortTo10Rule UshortTo10 = new UshortTo10Rule();
        public static readonly UshortTo15Rule UshortTo15 = new UshortTo15Rule();
        public static readonly UshortTo1500Rule UshortTo1500 = new UshortTo1500Rule();
        public static readonly UshortTo1000Rule UshortTo1000 = new UshortTo1000Rule();
        public static readonly UshortTo100Rule UshortTo100 = new UshortTo100Rule();
        public static readonly UshortTo298Rule UshortTo298 = new UshortTo298Rule();
        public static readonly UshortTo360Rule UshortTo360 = new UshortTo360Rule();
        public static readonly UshortTo600Rule UshortTo600 = new UshortTo600Rule();
        public static readonly Ushort100To4000Rule Ushort100To4000 = new Ushort100To4000Rule();
        public static readonly Ushort1To100Rule Ushort1To100 = new Ushort1To100Rule();
        public static readonly UshortTo65534Rule UshortTo65534 = new UshortTo65534Rule();
        public static readonly UshortTo10KRule UshortTo10K = new UshortTo10KRule();
        public static readonly UshortRule UshortRule = new UshortRule();
        public static readonly UshortTo200 UshortRuleto200 = new UshortTo200();
        public static readonly UshortFazaRule UshortFazaRule = new UshortFazaRule();


        public static readonly TimeRule TimeRule = new TimeRule();
        public static readonly IntTo3MRule IntTo3M = new IntTo3MRule();
    }
}
