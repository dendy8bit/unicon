﻿using System;
using System.Globalization;

namespace BEMN.Forms.ValidatingClasses.Rules.Double
{
    public class CustomDoubleRule : ValidatingRuleBaseDouble
    {
        private double _min;
        private double _max;
        private int _digits;

        public CustomDoubleRule(double min, double max):this(min, max, 5){ }

        public CustomDoubleRule(double min, double max, int digits)
        {
            this._min = min;
            this._max = max;
            this._digits = digits;
        }

        public override double Parse(string value)
        {
            if (value == "")
            {
                return  0;
            }
            return Math.Round(double.Parse(value, CultureInfo.CurrentCulture), this._digits);
        }

        public override double Min
        {
            get { return this._min; }
        }

        public override double Max
        {
            get { return this._max; }
        }
    }
}
