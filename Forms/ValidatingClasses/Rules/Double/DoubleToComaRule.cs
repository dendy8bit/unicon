﻿using System;
using System.Globalization;

namespace BEMN.Forms.ValidatingClasses.Rules.Double
{
    public class DoubleToComaRule : ValidatingRuleBaseDouble
    {
        private double _min;
        private double _max;
        private double _count;
        private string _message;

        public DoubleToComaRule(double min, double max, int count)
        {
            this._min = min;
            this._max = max;
            this._count = count;
        }

        public override double Parse(string value)
        {
            this._message = string.Empty;
            string[] values = value.Split(new[] {',', '.'}, StringSplitOptions.RemoveEmptyEntries);
            if (values.Length > 1 && values[1].Length > this._count)
            {
                this._message = string.Format("После запятой должно быть {0} знаков", this._count);
                throw new Exception();
            }
            return Math.Round(double.Parse(value, CultureInfo.CurrentCulture), 5);
        }

        public override double Min
        {
            get { return this._min; }
        }

        public override double Max
        {
            get { return this._max; }
        }

        public override string ErrorMessage
        {
            get
            {
                if (this._message == string.Empty)
                {
                    string pattern = "Диапазон {0:F" + this._count + "} .. {1:F" + this._count + "}";
                    return string.Format(pattern, this._min, this._max);
                }
                return this._message;
            }
        }
    }
}
