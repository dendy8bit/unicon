﻿using System;
using System.Globalization;
using System.Linq;

namespace BEMN.Forms.ValidatingClasses.Rules.Double
{
    public abstract class ValidatingRuleBaseDouble : ValidatingRuleBase<double>
    {
        public override bool IsValidKey(char key)
        {
            bool ret = Max < 0 || Min < 0
                ? !(char.IsDigit(key) | (key == '\b') | (key == '-') |
                    CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.Contains(key))
                : !(char.IsDigit(key) | (key == '\b') |
                    CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.Contains(key));

            return ret;

        }

        public override string ErrorMessage
        {
            get
            {
                if(Math.Abs(Min-(int)Min)>0 || Math.Abs(Max - (int)Max) > 0)
                {
                     return string.Format("Диапазон {0} .. {1}", Min, Max);
                }
                return string.Format("Диапазон {0}{1}0 .. {2}{3}0", Min,
                    CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator, Max,
                    CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
            }
        }
    }
}
