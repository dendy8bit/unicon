﻿using System.Globalization;

namespace BEMN.Forms.ValidatingClasses.Rules.Double
{
    public class Ustavka40To60Rule : ValidatingRuleBaseDouble
    {
        public override double Parse(string value)
        {
            return double.Parse(value, CultureInfo.CurrentCulture);
        }

        public override double Min
        {
            get { return 40; }
        }

        public override double Max
        {
            get { return 60; }
        }
    }
}
