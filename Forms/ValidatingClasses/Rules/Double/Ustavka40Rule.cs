﻿using System.Globalization;

namespace BEMN.Forms.ValidatingClasses.Rules.Double
{
    /// <summary>
    /// Дробное 0..40
    /// </summary>
    public class Ustavka40Rule : ValidatingRuleBaseDouble
    {
        public override double Parse(string value)
        {
          return  double.Parse(value, CultureInfo.CurrentCulture);
        }

        public override double Min
        {
            get { return 0; }
        }

        public override double Max
        {
            get { return 40; }
        }
    }
}
