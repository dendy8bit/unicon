﻿using System;
using System.Collections.Generic;

namespace BEMN.Forms.ValidatingClasses.Rules.ComboBox
{
    public class ComboBoxValidateRule : IValidatingRule
    {
        public string ErrorMessage
        {
            get
            {
                return "Введено неверное значение";
            }
        }
        
        public int IsValid(string value, List<string> lst)
        {
            List<string> upList = new List<string>(0);
            value = value.ToUpper();
            int i = 0;
            foreach (string str in lst)
            {
                string s = "";
                s = str.ToUpper();
                upList.Add(s);
            }
            foreach (string str in upList)
            {
                bool equal = value.Equals(str);
                if (equal) return i;
                i++;
            }
            return -1;
        }

        public bool IsValidKey(char key)
        {
            bool res = true;
            if ((key >= 'А' && key <= 'я') || (key >= 'A' && key <= 'z') || key == '\b' || key == '.' || (key >= '0' && key <= '9') || key == ' ')
            {

                return false;
            }

            return res;
        }

        #region [Method`s interface]

        public object Parse(string value)
        {
            throw new NotImplementedException();
        }
        public object Max
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public object Min
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool IsValid(string value)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
