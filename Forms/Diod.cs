﻿using System.Drawing;
using System.Windows.Forms;

namespace BEMN.Forms
{
    public enum DiodState
    {
        GREEN,
        RED,
        OFF,
        NOT_SET,
        ORANGE
    }

    public partial class Diod : UserControl
    {
        private DiodState _state;
        public Diod()
        {
            InitializeComponent();
            _state = DiodState.NOT_SET;
        }

        public DiodState State
        {
            get { return this._state; }
            set
            {
                if (this._state == value)
                {
                    return;
                }
                this._state = value;
                Invalidate();
            }
        }
        
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.FillEllipse(this.GetBrush(), 0, 0, 13, 13);
            e.Graphics.DrawEllipse(new Pen(Color.SlateGray), 0, 0, 13, 13);
        }

        private Brush GetBrush()
        {
            switch (_state)
            {
                case DiodState.NOT_SET: return Brushes.LightGray;
                case DiodState.OFF: return Brushes.DarkSlateGray;
                case DiodState.GREEN: return Brushes.LightGreen;
                case DiodState.RED: return Brushes.Red;
                case DiodState.ORANGE: return Brushes.Orange;
                default: return Brushes.LightGray;
            }
        }

        /// <summary>
        /// Устанавливает состояние светодиода в зависимости от значений цветов
        /// </summary>
        /// <param name="diodValues">Значения зелетого и красного цветов</param>
        public void SetState(bool[] diodValues)
        {
            if (diodValues[0] && !diodValues[1])
            {
                this.State = DiodState.GREEN;
            }
            if (!diodValues[0] && diodValues[1])
            {
                this.State = DiodState.RED;
            }
            if (!diodValues[0] && !diodValues[1])
            {
                this.State = DiodState.OFF;
            }
        }

        public void SetStateForSwitch(bool diodValues)
        {
            if (diodValues)
            {
                this.State = DiodState.GREEN;
            }
            if (!diodValues)
            {
                this.State = DiodState.OFF;
            }
        }

        public void RedDiod(bool diodValue)
        {
            if (diodValue)
            {
                this.State = DiodState.RED;
            }
            if (!diodValue)
            {
                this.State = DiodState.OFF;
            }
        }

        public void OrangeDiod(bool diodValue)
        {
            if (diodValue)
            {
                this.State = DiodState.ORANGE;
            }
            if (!diodValue)
            {
                this.State = DiodState.OFF;
            }
        }


        /// <summary>
        /// Нет связи, диод серый
        /// </summary>
        public void TurnOff()
        {
            this.State = DiodState.NOT_SET;
        }
    }
}
