﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace BEMN.Forms
{
    public partial class PortNumberForm : Form
    {
        private Process shlupic = new Process();
        
        public PortNumberForm(byte[] portNumbers)
        {
            InitializeComponent();
            // Подписка на события, когда процесс завершится 
            shlupic.Exited += ShlOnExited;

            if (null != portNumbers)
            {
                _portNumberComboBox.Items.AddRange(Array.ConvertAll(portNumbers, new Converter<byte, object>(ByteToObject)));
                _portNumberComboBox.SelectedIndex = 0;
            }
        }

        public static object ByteToObject(byte b)
        {
            return (object)b;
        }

        private void ShlOnExited(object sender, EventArgs eventArgs)
        {
            MessageBox.Show("Процесс завершен");
           _createProcessButton.Invoke((MethodInvoker)delegate { this._createProcessButton.Enabled = true; });
        }

        private void CreateProcess()
        {
            _createProcessButton.Enabled = false;

            //OpenFileDialog fileProcessDialog = new OpenFileDialog();

            //if (DialogResult.OK == fileProcessDialog.ShowDialog())
           // {
                try
                {
                    KillProces();

                    //string fileName = fileProcessDialog.FileName;
                    string finalPath = @"FILEOPERATION";

                    shlupic.StartInfo.FileName = "shlpicr.exe";
                    shlupic.StartInfo.CreateNoWindow = true;
                    shlupic.StartInfo.UseShellExecute = true;
                    shlupic.StartInfo.Arguments = finalPath;
                    shlupic.EnableRaisingEvents = true;
                    shlupic.Start();
                }
                catch (Exception e)
                {
                    _createProcessButton.Enabled = true;
                    MessageBox.Show(e.Message);
                }

          //  }

        }

        private void KillProces()
        {
            try
            {
                foreach (var process in Process.GetProcesses())
                {
                    if (process.ProcessName == "shlpicr")
                    {
                        process.Kill();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void _createProcessButton_Click(object sender, EventArgs e)
        {
            CreateProcess();
        }
    }
}
