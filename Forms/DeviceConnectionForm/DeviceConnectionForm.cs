using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.MBServer;
using IPAddressControlLib;

namespace BEMN.Forms.DeviceConnectionForm
{
	/// <summary>
	/// Summary description for DeviceNumberDlg.
	/// </summary>
	public class DeviceConnectionForm : Form
    {
        #region Private fields and controls
        private GroupBox _deviceGroup;
		private Button ok;
        private Button cancel;
		private ToolTip tip;
        private System.ComponentModel.IContainer components;
        private ComboBox _portCombo;
        private bool _close = true;
        private ComboBox _deviceNumberCombo;
        private GroupBox portNumGroup;
        private GroupBox TcpGroup;
        private GroupBox groupBox3;
        private RadioButton radioButton1;
        private RadioButton radioButton2;
        private RadioButton radioButton3;
        private RadioButton[] rButtons;
        private Label label4;
        private GroupBox groupBox4;
        private Label label6;
        private MaskedTextBox _timeoutBox;
        private Label label5;
        private MaskedTextBox _tcpPortBox;
	    private DeviceDlgInfo _currentInfo;
        private IPAddressControl ipAddress;
        private GroupBox groupBox1;
        private RadioButton isOfflineRadioBtn;
        private RadioButton isConnectionRadioBtn;
        private GroupBox connGroup;
        private Button _getDevNumBtn;
        
        #endregion

	    public DeviceConnectionForm(string devName, byte[] portsNumbers, List<byte> notShown, bool enablePortNumber, byte parentPort)
	    {
	        this.InitializeComponent();
	        this.rButtons = new[] {this.radioButton1, this.radioButton2, this.radioButton3};
            this._currentInfo = new DeviceDlgInfo();
	        Text = "���������� " + devName;
	        this.ok.MouseEnter += this.button_MouseEnter;
	        this.ok.MouseLeave += this.button_MouseLeave;
	        this.cancel.MouseEnter += this.button_MouseEnter;
	        this.cancel.MouseLeave += this.button_MouseLeave;

	        if (null != portsNumbers)
	        {
	            this._portCombo.Items.AddRange(Array.ConvertAll(portsNumbers, ByteToObject));
	        }

	        for (byte i = 1; i < 248; i++)
	        {
	            if (!notShown.Contains(i))
	            {
	                this._deviceNumberCombo.Items.Add(i);
	            }
	        }
	        this._deviceNumberCombo.SelectedIndex = 0;
	        if (null != portsNumbers && 0 != portsNumbers.Length)
	        {
	            if (this._portCombo.Items.Contains(parentPort))
	            {
	                this._portCombo.SelectedItem = parentPort;
	                this._portCombo.Enabled = enablePortNumber;
	            }
	            else
	            {
	                this._portCombo.SelectedIndex = 0;
	            }
	        }
	        this.isConnectionRadioBtn.Checked = this._portCombo.Items.Count != 0;
	        this.tip.SetToolTip(this._deviceNumberCombo, "����� ������������� ����������. ������ ������ � ��������� {1 - 247}");
	        this.ok.Focus();
	    }


        /// <summary>
        /// ��� ��������������� ����������� ����������
        /// </summary>
        public DeviceConnectionForm(byte[] portsNumbers, List<byte> notShown, bool enablePortNumber, byte parentPort)
        {
            this.InitializeComponent();
            this.rButtons = new[] { this.radioButton1, this.radioButton2, this.radioButton3 };
            this._currentInfo = new DeviceDlgInfo();
            Text = "�������������� ����������� ����������";
            this.ok.MouseEnter += this.button_MouseEnter;
            this.ok.MouseLeave += this.button_MouseLeave;
            this.cancel.MouseEnter += this.button_MouseEnter;
            this.cancel.MouseLeave += this.button_MouseLeave;

            if (null != portsNumbers)
            {
                this._portCombo.Items.AddRange(Array.ConvertAll(portsNumbers, ByteToObject));
            }

            for (byte i = 1; i < 248; i++)
            {
                if (!notShown.Contains(i))
                {
                    this._deviceNumberCombo.Items.Add(i);
                }
            }
            this._deviceNumberCombo.SelectedIndex = 0;
            if (null != portsNumbers && 0 != portsNumbers.Length)
            {
                if (this._portCombo.Items.Contains(parentPort))
                {
                    this._portCombo.SelectedItem = parentPort;
                    this._portCombo.Enabled = enablePortNumber;
                }
                else
                {
                    this._portCombo.SelectedIndex = 0;
                }
            }
            this.isConnectionRadioBtn.Checked = this._portCombo.Items.Count != 0;
            this.tip.SetToolTip(this._deviceNumberCombo, "����� ������������� ����������. ������ ������ � ��������� {1 - 247}");
            this.ok.Focus();
        }

        public static object ByteToObject(byte b) => b;
        
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
            if (disposing)
            {
                this.components?.Dispose();
            }
            base.Dispose(disposing);
        }

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this._deviceGroup = new GroupBox();
            this._getDevNumBtn = new Button();
            this._deviceNumberCombo = new ComboBox();
            this.ok = new Button();
            this.cancel = new Button();
            this.tip = new ToolTip(this.components);
            this._portCombo = new ComboBox();
            this.portNumGroup = new GroupBox();
            this.radioButton1 = new RadioButton();
            this.label4 = new Label();
            this.TcpGroup = new GroupBox();
            this.groupBox4 = new GroupBox();
            this.label6 = new Label();
            this._timeoutBox = new MaskedTextBox();
            this.label5 = new Label();
            this._tcpPortBox = new MaskedTextBox();
            this.radioButton3 = new RadioButton();
            this.groupBox3 = new GroupBox();
            this.ipAddress = new IPAddressControl();
            this.radioButton2 = new RadioButton();
            this.groupBox1 = new GroupBox();
            this.isOfflineRadioBtn = new RadioButton();
            this.isConnectionRadioBtn = new RadioButton();
            this.connGroup = new GroupBox();
            this._deviceGroup.SuspendLayout();
            this.portNumGroup.SuspendLayout();
            this.TcpGroup.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.connGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // _deviceGroup
            // 
            this._deviceGroup.BackColor = Color.LightGray;
            this._deviceGroup.Controls.Add(this._getDevNumBtn);
            this._deviceGroup.Controls.Add(this._deviceNumberCombo);
            this._deviceGroup.Location = new Point(6, 19);
            this._deviceGroup.Name = "_deviceGroup";
            this._deviceGroup.Size = new Size(220, 49);
            this._deviceGroup.TabIndex = 0;
            this._deviceGroup.TabStop = false;
            this._deviceGroup.Text = "����� ����������";
            // 
            // _getDevNumBtn
            // 
            this._getDevNumBtn.Location = new Point(91, 20);
            this._getDevNumBtn.Name = "_getDevNumBtn";
            this._getDevNumBtn.Size = new Size(123, 23);
            this._getDevNumBtn.TabIndex = 2;
            this._getDevNumBtn.Text = "�������� �����";
            this._getDevNumBtn.UseVisualStyleBackColor = true;
            this._getDevNumBtn.Click += new EventHandler(this.GetNumBtnOnClick);
            // 
            // _deviceNumberCombo
            // 
            this._deviceNumberCombo.FormattingEnabled = true;
            this._deviceNumberCombo.Location = new Point(12, 22);
            this._deviceNumberCombo.Name = "_deviceNumberCombo";
            this._deviceNumberCombo.Size = new Size(64, 21);
            this._deviceNumberCombo.TabIndex = 1;
            // 
            // ok
            // 
            this.ok.Anchor = AnchorStyles.Bottom;
            this.ok.BackColor = Color.LightGray;
            this.ok.DialogResult = DialogResult.OK;
            this.ok.Location = new Point(21, 378);
            this.ok.Name = "ok";
            this.ok.Size = new Size(90, 23);
            this.ok.TabIndex = 1;
            this.ok.Text = "��";
            this.ok.UseVisualStyleBackColor = false;
            this.ok.Click += new EventHandler(this.ok_Click);
            // 
            // cancel
            // 
            this.cancel.Anchor = AnchorStyles.Bottom;
            this.cancel.BackColor = Color.LightGray;
            this.cancel.DialogResult = DialogResult.Cancel;
            this.cancel.ImageAlign = ContentAlignment.MiddleLeft;
            this.cancel.Location = new Point(143, 378);
            this.cancel.Name = "cancel";
            this.cancel.Size = new Size(90, 23);
            this.cancel.TabIndex = 2;
            this.cancel.Text = "������";
            this.cancel.UseVisualStyleBackColor = false;
            this.cancel.Click += new EventHandler(this.cancel_Click);
            // 
            // tip
            // 
            this.tip.AutoPopDelay = 1000;
            this.tip.InitialDelay = 500;
            this.tip.ReshowDelay = 100;
            // 
            // _portCombo
            // 
            this._portCombo.DropDownStyle = ComboBoxStyle.DropDownList;
            this._portCombo.FormattingEnabled = true;
            this._portCombo.Location = new Point(102, 16);
            this._portCombo.Name = "_portCombo";
            this._portCombo.Size = new Size(64, 21);
            this._portCombo.TabIndex = 0;
            // 
            // portNumGroup
            // 
            this.portNumGroup.Controls.Add(this._portCombo);
            this.portNumGroup.Controls.Add(this.radioButton1);
            this.portNumGroup.Controls.Add(this.label4);
            this.portNumGroup.Location = new Point(6, 76);
            this.portNumGroup.Name = "portNumGroup";
            this.portNumGroup.Size = new Size(220, 45);
            this.portNumGroup.TabIndex = 3;
            this.portNumGroup.TabStop = false;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new Point(6, 0);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new Size(89, 17);
            this.radioButton1.TabIndex = 5;
            this.radioButton1.TabStop = true;
            this.radioButton1.Tag = "1";
            this.radioButton1.Text = "Modbus-RTU";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new EventHandler(this.radioButton_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new Point(6, 19);
            this.label4.Name = "label4";
            this.label4.Size = new Size(73, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "����� �����";
            // 
            // TcpGroup
            // 
            this.TcpGroup.Controls.Add(this.groupBox4);
            this.TcpGroup.Controls.Add(this.radioButton3);
            this.TcpGroup.Controls.Add(this.groupBox3);
            this.TcpGroup.Controls.Add(this.radioButton2);
            this.TcpGroup.Location = new Point(6, 127);
            this.TcpGroup.Name = "TcpGroup";
            this.TcpGroup.Size = new Size(220, 181);
            this.TcpGroup.TabIndex = 4;
            this.TcpGroup.TabStop = false;
            this.TcpGroup.Text = "TCP";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this._timeoutBox);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this._tcpPortBox);
            this.groupBox4.Enabled = false;
            this.groupBox4.Location = new Point(6, 103);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new Size(205, 66);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new Point(6, 43);
            this.label6.Name = "label6";
            this.label6.Size = new Size(70, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "�������, ��";
            // 
            // _timeoutBox
            // 
            this._timeoutBox.Location = new Point(99, 40);
            this._timeoutBox.Name = "_timeoutBox";
            this._timeoutBox.Size = new Size(100, 20);
            this._timeoutBox.TabIndex = 2;
            this._timeoutBox.Text = "3000";
            this._timeoutBox.TextChanged += new EventHandler(this._timeoutBox_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new Point(6, 17);
            this.label5.Name = "label5";
            this.label5.Size = new Size(73, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "����� �����";
            // 
            // _tcpPortBox
            // 
            this._tcpPortBox.Location = new Point(99, 14);
            this._tcpPortBox.Name = "_tcpPortBox";
            this._tcpPortBox.Size = new Size(100, 20);
            this._tcpPortBox.TabIndex = 3;
            this._tcpPortBox.Text = "502";
            this._tcpPortBox.TextChanged += new EventHandler(this._tcpPortBox_TextChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new Point(6, 35);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new Size(128, 17);
            this.radioButton3.TabIndex = 7;
            this.radioButton3.TabStop = true;
            this.radioButton3.Tag = "3";
            this.radioButton3.Text = "Modbus-RTU �� TCP";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new EventHandler(this.radioButton_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = Color.LightGray;
            this.groupBox3.Controls.Add(this.ipAddress);
            this.groupBox3.Enabled = false;
            this.groupBox3.Location = new Point(6, 54);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new Size(206, 49);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "IP-�����";
            // 
            // ipAddress
            // 
            this.ipAddress.AllowInternalTab = false;
            this.ipAddress.AutoHeight = true;
            this.ipAddress.BackColor = SystemColors.Window;
            this.ipAddress.BorderStyle = BorderStyle.Fixed3D;
            this.ipAddress.Cursor = Cursors.IBeam;
            this.ipAddress.Location = new Point(9, 19);
            this.ipAddress.MinimumSize = new Size(87, 20);
            this.ipAddress.Name = "ipAddress";
            this.ipAddress.ReadOnly = false;
            this.ipAddress.Size = new Size(190, 20);
            this.ipAddress.TabIndex = 7;
            this.ipAddress.Text = "...";
            this.ipAddress.TextChanged += new EventHandler(this.ipAddress_TextChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new Point(6, 17);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new Size(87, 17);
            this.radioButton2.TabIndex = 6;
            this.radioButton2.TabStop = true;
            this.radioButton2.Tag = "2";
            this.radioButton2.Text = "Modbus-TCP";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new EventHandler(this.radioButton_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = Color.LightGray;
            this.groupBox1.Controls.Add(this.isOfflineRadioBtn);
            this.groupBox1.Controls.Add(this.isConnectionRadioBtn);
            this.groupBox1.Location = new Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new Size(233, 39);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "����� ������";
            // 
            // isOfflineRadioBtn
            // 
            this.isOfflineRadioBtn.AutoSize = true;
            this.isOfflineRadioBtn.Location = new Point(106, 15);
            this.isOfflineRadioBtn.Name = "isOfflineRadioBtn";
            this.isOfflineRadioBtn.Size = new Size(73, 17);
            this.isOfflineRadioBtn.TabIndex = 5;
            this.isOfflineRadioBtn.Tag = "1";
            this.isOfflineRadioBtn.Text = "�������";
            this.isOfflineRadioBtn.UseVisualStyleBackColor = true;
            // 
            // isConnectionRadioBtn
            // 
            this.isConnectionRadioBtn.AutoSize = true;
            this.isConnectionRadioBtn.Checked = true;
            this.isConnectionRadioBtn.Location = new Point(6, 15);
            this.isConnectionRadioBtn.Name = "isConnectionRadioBtn";
            this.isConnectionRadioBtn.Size = new Size(94, 17);
            this.isConnectionRadioBtn.TabIndex = 5;
            this.isConnectionRadioBtn.TabStop = true;
            this.isConnectionRadioBtn.Tag = "1";
            this.isConnectionRadioBtn.Text = "�����������";
            this.isConnectionRadioBtn.UseVisualStyleBackColor = true;
            this.isConnectionRadioBtn.CheckedChanged += new EventHandler(this.ModeRadioButtonCheckedChanged);
            // 
            // connGroup
            // 
            this.connGroup.Controls.Add(this._deviceGroup);
            this.connGroup.Controls.Add(this.portNumGroup);
            this.connGroup.Controls.Add(this.TcpGroup);
            this.connGroup.Location = new Point(12, 57);
            this.connGroup.Name = "connGroup";
            this.connGroup.Size = new Size(233, 315);
            this.connGroup.TabIndex = 6;
            this.connGroup.TabStop = false;
            // 
            // DeviceConnectionForm
            // 
            this.AcceptButton = this.ok;
            this.AutoScaleBaseSize = new Size(5, 13);
            this.BackColor = Color.LightGray;
            this.CancelButton = this.cancel;
            this.ClientSize = new Size(258, 404);
            this.Controls.Add(this.connGroup);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.ok);
            this.FormBorderStyle = FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DeviceConnectionForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = FormStartPosition.CenterParent;
            this.Text = "����������";
            this.FormClosing += new FormClosingEventHandler(this.DeviceNumberDlg_FormClosing);
            this._deviceGroup.ResumeLayout(false);
            this.portNumGroup.ResumeLayout(false);
            this.portNumGroup.PerformLayout();
            this.TcpGroup.ResumeLayout(false);
            this.TcpGroup.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.connGroup.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

        private bool Validate(MaskedTextBox tb, int min, int max)
        {
            try
            {
                int value = int.Parse(tb.Text);
                if (value < min || value > max)
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                this.tip.IsBalloon = true;
                this.tip.Show($"�������� ������ ������ � ��������� {min}-{max}", tb, 2000);
                return false;
            }
            return true;
        }

	    private bool ValidateIp(IPAddressControl ctrl, string ipStr)
	    {
	        string[] ip = ipStr.Split(new []{'.'}, StringSplitOptions.RemoveEmptyEntries);
	        if (ip.Length < 4)
	        {
	            this.tip.IsBalloon = true;
	            this.tip.Show("�������� ������ ������ ���� � ��������� 0-255 � �� ���� ������", ctrl, 2000);
	            return false;
	        }
	        this._close = true;
	        return true;
	    }

	    private void ok_Click(object sender, EventArgs e)
	    {
            this.GetCurrentInfo();
            this._currentInfo.IsOkPressed = this._close;
	    }
        
	    private void GetCurrentInfo()
	    {
            if (!this.ValidateDeviceNumbeerCombo()) return;
            if (this.radioButton1.Checked)
            {
                this._currentInfo.ModbusType = ModbusType.ModbusRtu;
                this._currentInfo.IsConnectionMode = _portCombo.Items.Count != 0 && this.isConnectionRadioBtn.Checked;
            }
            else
            {
                this._currentInfo.ModbusType = this.radioButton2.Checked ? ModbusType.ModbusTcp : ModbusType.ModbusOnTcp;
                if (!this.ValidateIp(this.ipAddress, this.ipAddress.Text) || !this.Validate(this._tcpPortBox, 0, 65535) || !this.Validate(this._timeoutBox, 0, 20000))
                {
                    //this._close = false;
                    return;
                }
                this._currentInfo.IsConnectionMode = this.isConnectionRadioBtn.Checked;
            }
	        this._currentInfo.Ip = this.ipAddress.Text;
	        this._currentInfo.TcpPort = int.Parse(this._tcpPortBox.Text);
	        this._currentInfo.ReceiveTimeout = int.Parse(this._timeoutBox.Text);
            this._currentInfo.DeviceNumber = Convert.ToByte(this._deviceNumberCombo.Text);
	        this._currentInfo.PortNumber = (byte?) this._portCombo.SelectedItem ?? (byte)0;	        
	    }

	    private bool ValidateDeviceNumbeerCombo()
	    {
	        try
	        {
                this._close = this._deviceNumberCombo.Items.Contains(byte.Parse(this._deviceNumberCombo.Text));
	            return true;
	        }
	        catch (FormatException)
	        {
	            this.tip.IsBalloon = true;
	            this.tip.Show("������� ����� ���������� � ���������� �������", this._deviceNumberCombo, 2000);
	            this._close = false;
	            return false;
	        }
	        catch (OverflowException)
	        {
	            this.tip.IsBalloon = true;
	            this.tip.Show("����� ���������� ������ ������ � ��������� [1-247]", this._deviceNumberCombo, 2000);
	            this._close = false;
	            return false;
	        }
	    }

		private void cancel_Click(object sender, EventArgs e)
		{
		    this.GetCurrentInfo();
            //this._close = true;
			Close();
		}

		private void button_MouseEnter(object sender, EventArgs e)
		{
			((Button)sender).BackColor = Color.Black;
			((Button)sender).ForeColor = Color.White;
			
		}

		private void button_MouseLeave(object sender, EventArgs e)
		{
			((Button)sender).BackColor = Color.LightGray;
			((Button)sender).ForeColor = Color.Black;
		}

        private void DeviceNumberDlg_FormClosing(object sender, FormClosingEventArgs e)
        {
            GetCurrentInfo();
            e.Cancel = !this._close;
        }
        
        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            int num = Convert.ToInt32((sender as RadioButton).Tag);
            bool check = (sender as RadioButton).Checked;
            if (!check) return;

            switch (num)
            {
                case 1:
                    this.ok.Enabled = true;
                    this.SetRadioButtons(num);
                    this.SetEnable(false);
                    break;
                case 2:
                    this.ok.Enabled = !this.ValidateIp(this.ipAddress, this.ipAddress.Text) ? false : true;
                    this.SetRadioButtons(num);
                    this.SetEnable(true);
                    break;
                case 3:
                    this.ok.Enabled = !this.ValidateIp(this.ipAddress, this.ipAddress.Text) ? false : true;
                    this.SetRadioButtons(num);
                    this.SetEnable(true);
                    break;
                default:
                    this.ok.Enabled = true;
                    this.SetRadioButtons(1);
                    this.SetEnable(false);
                    break;
            }
        }

	    private void ModeRadioButtonCheckedChanged(object sender, EventArgs e)
	    {
	        this._currentInfo.IsConnectionMode = this.isConnectionRadioBtn.Checked;
	        this.isOfflineRadioBtn.Checked = !this.isConnectionRadioBtn.Checked;
            this.connGroup.Enabled = this.isConnectionRadioBtn.Checked;
	    }

        private void SetRadioButtons(int num)
        {
            foreach (var r in this.rButtons)
            {
                r.Checked = Convert.ToInt32(r.Tag) == num;
            }
        }

        private void SetEnable(bool p)
        {
            this.groupBox3.Enabled = p;
            this.groupBox4.Enabled = p;
        }

	    public DeviceDlgInfo ShowDialogRes(DeviceDlgInfo info)
	    {
	        string[] oldIp = info.Ip.Split('.');
	        this.ipAddress.Text = oldIp.Length != 4 ? string.Empty : info.Ip;
	        this._tcpPortBox.Text = info.TcpPort.ToString();
	        this._timeoutBox.Text = info.ReceiveTimeout.ToString();
            ShowDialog();
	        return this._currentInfo;
	    }

	    public DeviceDlgInfo ShowDialog(DeviceDlgInfo oldInfo)
	    {
	        this.SetOldInfo(oldInfo);
            ShowDialog();
	        return this._currentInfo;
	    }

	    private void SetOldInfo(DeviceDlgInfo oldInfo)
	    {
	        if (oldInfo == null) return;
	        
            string[] oldIp = oldInfo.Ip.Split('.');
            this.ipAddress.Text = oldIp.Length != 4 ? string.Empty : oldInfo.Ip;
            int ind = this._deviceNumberCombo.Items.IndexOf(oldInfo.DeviceNumber);
	        if (ind == -1 && this._deviceNumberCombo.Items.Count != 0)
	        {
	            ind = 0;
	        }
	        this._deviceNumberCombo.SelectedIndex = ind;

            ind = this._portCombo.Items.IndexOf(oldInfo.PortNumber);
	        if (ind != -1)
	        {
	            this._portCombo.SelectedIndex = ind;
	        }
	        else if (this._portCombo.Items.Count != 0)
	        {
	            this._portCombo.SelectedIndex = 0;
	        }
	        this.isConnectionRadioBtn.Checked = this._portCombo.Items.Count != 0 && oldInfo.IsConnectionMode;
	        
	        this._tcpPortBox.Text = oldInfo.TcpPort.ToString();
            this._timeoutBox.Text = oldInfo.ReceiveTimeout.ToString();
            this.SetModbuseType(oldInfo.ModbusType);
            oldInfo.IsOkPressed = false;
	    }

        private void SetModbuseType(ModbusType mbType)
        {
            switch (mbType)
            {
                case ModbusType.ModbusRtu:
                    this.radioButton1.Checked = true;
                    break;
                case ModbusType.ModbusTcp:
                    this.radioButton2.Checked = true;
                    break;
                case ModbusType.ModbusOnTcp:
                    this.radioButton3.Checked = true;
                    break;
                default:
                    this.radioButton1.Checked = true;
                    break;
            }
        }

        private void GetNumBtnOnClick(object sender, EventArgs eventArgs)
        {
            GetDeviceNumForm form = null;
            try
            {
                this.GetCurrentInfo();
                form = new GetDeviceNumForm(this._currentInfo);
                form.ShowDialog();
                this._currentInfo.DeviceNumber = form.DeviceNumber;
                if (form.DeviceNumber != 0) this._deviceNumberCombo.SelectedItem = this._currentInfo.DeviceNumber;
            }
            catch(Exception)
            {
                form?.Close();
                MessageBox.Show("���������� �������� ����� ����������", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void _tcpPortBox_TextChanged(object sender, EventArgs e)
        {
            this.ok.Enabled = this.Validate(this._tcpPortBox, 1, 65535);
            this._tcpPortBox.BackColor = !this.Validate(this._tcpPortBox, 1, 65535) ? Color.Red : Color.White;
        }

        private void _timeoutBox_TextChanged(object sender, EventArgs e)
        {
            this.ok.Enabled = this.Validate(this._timeoutBox, 0, 20000);
            this._timeoutBox.BackColor = !this.Validate(this._timeoutBox, 0, 20000) ? Color.Red : Color.White;
        }

        private void ipAddress_TextChanged(object sender, EventArgs e)
        {
            this.ok.Enabled = this.ValidateIp(this.ipAddress, this.ipAddress.Text);
            this.ipAddress.BackColor = !this.ValidateIp(this.ipAddress, this.ipAddress.Text) ? Color.Red : Color.White;
        }
    }
}
