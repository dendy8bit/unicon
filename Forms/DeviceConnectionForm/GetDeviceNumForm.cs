﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.MBServer;
using BEMN.MBServer.Queries;

namespace BEMN.Forms.DeviceConnectionForm
{
    public partial class GetDeviceNumForm : Form
    {
        private Modbus _mb;
        private int _index;
        private string _ip;
        private int _receiveTimeout;
        private int _port;
        private ModbusType _mbt;
        private Device.slot _loadSlot;

        private const string QUERY_NAME = "LoadDevNum";

        public byte DeviceNumber
        { get; private set; }
        public bool Complete
        { get; private set; }
        public GetDeviceNumForm()
        {
            this.InitializeComponent();
        }
        public GetDeviceNumForm(DeviceDlgInfo info)
        {
            this.InitializeComponent();
            this._index = 0;
            this.Complete = false;
            this._mb = this.CreateModbus(info);
            this.DeviceNumber = 1;
            this._loadSlot = new Device.slot(0x500,0x501);
        }

        private Modbus CreateModbus(DeviceDlgInfo deviceInfo)
        {
            //Создаем Modbus -->
            Modbus modbus = new Modbus();

            try
            {
                if (deviceInfo.ModbusType == ModbusType.ModbusRtu)
                {
                    modbus =
                        new Modbus(Modbus.SerialServer.GetNamedPort(deviceInfo.PortNumber, (UInt32) Modbus.ProcessID));
                }
                else
                {
                    this._ip =  deviceInfo.Ip;
                    this._port = deviceInfo.TcpPort;
                    this._receiveTimeout = deviceInfo.ReceiveTimeout;
                    this._mbt = deviceInfo.ModbusType;
                    this.TcpConnect();
                }
            }
            catch
            {}
            modbus.CompleteExchange += this.ModbusCompleteExchange;
            return modbus;
        }

        private void TcpConnect()
        {
            if (this._index == 1)
            {
                this._statusLabel.Text = "Попытка повторного подключения";
            }
            if (this._index > 1)
            {
                this._statusLabel.Text = "Невозможно подключиться";
                return;
            }

            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.ReceiveTimeout = this._receiveTimeout;
            IPAddress addr = IPAddress.Parse(this._ip);
            socket.BeginConnect(addr, this._port, this.Callback, socket);
            this._index++;
        }

        private void Callback(IAsyncResult ar)
        {
            try
            {
                Socket socket = (Socket) ar.AsyncState;
                socket.EndConnect(ar);
                this._mb.SetNetworkConnection(socket, this._mbt);
            }
            catch (Exception)
            {
                Invoke(new Action(this.TcpConnect));
            }
        }

        private Query CreateQuery()
        {
            Query query = new Query();
            query.name = QUERY_NAME+ this.DeviceNumber;
            query.device = this.DeviceNumber;
            query.deviceObj = this;
            query.func = 0x04;
            query.priority = 10;
            query.countOfMaxRequest = 1;
            query.dataStart = this._loadSlot.Start;
            this._loadSlot.Loaded = false;
            query.writeBuffer = this._loadSlot.Value;
            query.globalSize = this._loadSlot.Size;
            
            return query;
        }

        private void GetDeviceNumForm_Load(object sender, EventArgs e)
        {
            this._mb.AddQuery(this.CreateQuery());
        }

        private void OkBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ModbusCompleteExchange(object sender, Query query)
        {
            if (query.name != QUERY_NAME + this.DeviceNumber) return;
            try
            {
                if (query.fail != 0)
                {
                    if (this.DeviceNumber < 248)
                    {
                        this.DeviceNumber++;
                        Invoke(new Action(() => this._progressBar.PerformStep()));
                        this._mb.AddQuery(this.CreateQuery());
                    }
                    else
                    {
                        Invoke(new Action(() =>
                        {
                            this.OkBtn.Enabled = true;
                            this._statusLabel.Text = "Невозможно получить\nномер устройства";
                        }));
                        this.DeviceNumber = 0;
                    }
                }
                else
                {
                    Invoke(new Action(() =>
                    {
                        this._progressBar.Value = this._progressBar.Maximum;
                        this.OkBtn.Enabled = true;
                        this._statusLabel.Text = "Номер подключенного\nустройства - " + this.DeviceNumber;
                    }));
                    this.Complete = true;
                }
            }
            catch (Exception)
            {
            }
        }
    }
}
