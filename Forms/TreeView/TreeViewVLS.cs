﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace BEMN.Forms.TreeView
{
    // Класс используется на форме Конфигурация, вкладка Уставки -> ВЛС. Нужен при отображении дерева.
    // 1.Добавляем новый компонент на форму treeView класса System.Windows.Forms.TreeView на вкладке ВЛС. Назвать его лучше treeViewForVLS.
    // 2.Объявить глобально private List<CheckedListBox> allVlsCheckedListBoxs = new List<CheckedListBox>();
    // и добавить в конструкторе в него все используемые компоненты типа CheckedListBox.
    // allVlsCheckedListBoxs.Add(this."Название компонента");
    // 3.Посмотреть в МР771 в region [Event CreateTree] как реализовано, подписать компоненты на события, сделать аналогично.
    // Обратить внимание на количество ссылок над методами!
    // 4.Добавить на каждую кнопку, связанную с изменением состояний ВЛС необходимые методы, смотрим всё как в МР771.
    // 5.Важно!!! Добавить в метод по событию компонента contextNenu_Opening следующие строки: (Это в МР771, в других МР могут быть другие,
    // посмотреть можно нажав на компонент contextMenu свойство Items -> ...).
    // this.contextMenu.Items.Clear();
    // this.contextMenu.Items.AddRange(new ToolStripItem[]
    // {
    //    this.readFromDeviceItem,
    //    this.writeToDeviceItem,
    //    this.resetSetpointsItem,
    //    this.clearSetpointsItem,
    //    this.readFromFileItem,
    //    this.writeToFileItem,
    //    this.writeToHtmlItem
    // });
    // При удалении узла дерева, контекстное меню очищается, для повторного вызова меню нужен этот код.


    // Для защит с несколькими группами уставок: добавить в метод OnRefreshInfoTable() 
    // TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.tabControl2, this.treeViewForVLS);  Пример взят из МР771.
    public static class TreeViewVLS
    {       
        /// <summary>
        /// Список развернутых узлов дерева (treeViewForVLS)
        /// </summary>
        public static List<string> expandedNodes = new List<string>();

        /// <summary>
        /// Метод проверяет состояние узла, если развернут заносит в List string expandedNodes.
        /// </summary>
        public static void StateNodes(System.Windows.Forms.TreeView treeViewForVLS)
        {
            expandedNodes.Clear();
            for (int i = 0; i < treeViewForVLS.Nodes.Count; i++)
            {
                if (treeViewForVLS.Nodes[i].IsExpanded)
                {
                    if (!expandedNodes.Contains(treeViewForVLS.Nodes[i].Text))
                    {
                        expandedNodes.Add(treeViewForVLS.Nodes[i].Text);
                    }
                }
                else
                {
                    if (treeViewForVLS.Nodes.Count > 0)
                    {
                        if (expandedNodes.Contains(treeViewForVLS.Nodes[i].Text))
                        {
                            expandedNodes.Remove(treeViewForVLS.Nodes[i].Text);
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Метод рисует дерево (treeViewForVLS), которое отображает выбранные чекбоксы вкладок ВЛС1 - ВЛС16. Уставки -> Вкладка ВЛС.
        /// </summary>
        public static void CreateTree(List<CheckedListBox> allVlsCheckedListBoxs, TabControl tabControl, System.Windows.Forms.TreeView treeViewForVLS)
        {
            treeViewForVLS.Nodes.Clear();
            TabControl.TabPageCollection allTabPages = tabControl.TabPages;
            CheckedListBox.CheckedItemCollection checkedItemsInList;
            for (int i = 0; i < allVlsCheckedListBoxs.Count; i++)
            {
                int countChildren = 0;
                if (allVlsCheckedListBoxs[i].CheckedItems.Count > 0)
                {
                    countChildren = allVlsCheckedListBoxs[i].CheckedItems.Count;
                    checkedItemsInList = allVlsCheckedListBoxs[i].CheckedItems;
                    var withoutSpaces = allTabPages[i].Text.Replace(" ", "").Replace(" ", "").Replace(" ", "");
                    TreeNode parentNode = new TreeNode(withoutSpaces);
                    treeViewForVLS.Nodes.Add(parentNode);
                    for (int j = 0; j < countChildren; j++)
                    {
                        TreeNode childrenNode = new TreeNode(checkedItemsInList[j].ToString());
                        parentNode.Nodes.Add(childrenNode);
                    }
                }
            }
        }


        /// <summary>
        /// Метод разворачивает узлы дерева (treeViewForVLS), имена которых находятся в листе expandedNodes.
        /// </summary>
        public static void ExpandTreeNodes(System.Windows.Forms.TreeView treeViewForVLS)
        {
            for (int k = 0; k < treeViewForVLS.Nodes.Count; k++)
            {
                if (expandedNodes.Count > 0)
                {
                    if (expandedNodes.Contains(treeViewForVLS.Nodes[k].Text))
                    {
                        treeViewForVLS.Nodes[k].Expand();
                    }
                }
            }
        }


        /// <summary>
        /// Метод разворачивает узел дерева (treeViewForVLS), находящийся на этапе редактирования.
        /// </summary>
        public static void ExpandCurrentTreeNode(TabControl tabControl, System.Windows.Forms.TreeView treeViewForVLS)
        {
            TabPage currentTabPage = new TabPage();
            currentTabPage = tabControl.SelectedTab;
            for (int k = 0; k < treeViewForVLS.Nodes.Count; k++)
            {
                if (currentTabPage.Text.Replace(" ", "").Replace(" ","").Replace(" ","") == treeViewForVLS.Nodes[k].Text)
                {
                    treeViewForVLS.Nodes[k].Expand();
                }
            }
        }


        /// <summary>
        /// Метод позволяет удалять узлы дерева (treeViewForVLS) по нажатию ПКМ. Уставки -> Вкладка ВЛС.
        /// </summary>
        public static void DeleteNode(object sender, TreeNodeMouseClickEventArgs e, ContextMenuStrip contextMenu, List<CheckedListBox> allVlsCheckedListBoxs)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenu.Items.Clear();
                string nodeName = e.Node.Text;
                if (nodeName.StartsWith("ВЛС"))
                {
                    DialogResult result = MessageBox.Show($"Удалить узел {nodeName}?", "Подтверждение действия",
                        MessageBoxButtons.YesNo);
                    if (result != DialogResult.Yes) return;
                }
                else
                {
                    DialogResult result = MessageBox.Show($"Удалить сигнал {nodeName}?", "Подтверждение действия",
                        MessageBoxButtons.YesNo);
                    if (result != DialogResult.Yes) return;
                }
                for (int i = 0; i < allVlsCheckedListBoxs.Count; i++)
                {
                    if (nodeName == allVlsCheckedListBoxs[i].Parent.Text.Replace(" ", "").Replace(" ", "").Replace(" ", "")) // при удалении корневого узла
                    {
                        for (int k = 0; k < allVlsCheckedListBoxs[i].Items.Count; k++)
                        {
                            allVlsCheckedListBoxs[i].SetItemChecked(k, false);
                        }
                        break;
                    }
                    for (int j = 0; j < allVlsCheckedListBoxs[i].Items.Count; j++)
                    {
                        if (nodeName == allVlsCheckedListBoxs[i].Items[j].ToString()) // при удалении дочернего узла
                        {
                            allVlsCheckedListBoxs[i].SetItemChecked(j, false);
                            break;
                        }
                    }
                }
            }
        }
    }
}