﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace BEMN.Forms.TreeView
{
    // Класс используется на форме Конфигурация, вкладка Уставки -> ЛС. Нужен при отображении дерева.
    // 1.Добавляем новый компонент на форму treeView класса System.Windows.Forms.TreeView на вкладке ЛС. Назвать его лучше treeViewForLsAND и treeViewForLsOR.
    // 2.Объявить глобально private List<DataGridView> dataGridsViewLsAND = new List<DataGridView>(); 
    //                      private List<DataGridView> dataGridsViewLsOR = new List<DataGridView>();
    // и добавить в конструкторе в них все используемые компоненты типа DataGridView соответственно.
    // dataGridsViewLsAND.Add(this."Название компонента");
    // dataGridsViewLsOR.Add(this."Название компонента");
    // 3.Посмотреть в МР771 в region [Event CreateTree] как реализовано, подписать компоненты на события, сделать аналогично.
    // Обратить внимание на количество ссылок над методами!
    // 4.Добавить на каждую кнопку, связанную с изменением состояний ВЛС необходимые методы, смотрим всё как в МР771.
    // 5.Важно!!! Добавить в метод по событию компонента contextNenu_Opening следующие строки: (Это в МР771, в других МР могут быть другие,
    // посмотреть можно нажав на компонент contextMenu свойство Items -> ...).
    // this.contextMenu.Items.Clear();
    // this.contextMenu.Items.AddRange(new ToolStripItem[]
    // {
    //    this.readFromDeviceItem,
    //    this.writeToDeviceItem,
    //    this.resetSetpointsItem,
    //    this.clearSetpointsItem,
    //    this.readFromFileItem,
    //    this.writeToFileItem,
    //    this.writeToHtmlItem
    // });
    // При удалении узла дерева, контекстное меню очищается, для повторного вызова меню нужен этот код.


    // Для защит с несколькими группами уставок: добавить в метод OnRefreshInfoTable() 
    // TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
    // TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);   Пример взят из МР771.
    public static class TreeViewLS
    {
        /// <summary>
        /// Список развернутых узлов дерева (treeViewForLsOR)
        /// </summary>
        public static List<string> expandedNodesOR = new List<string>();

        /// <summary>
        /// Список развернутых узлов дерева (treeViewForLsAND)
        /// </summary>
        public static List<string> expandedNodesAND = new List<string>();

        /// <summary>
        /// Метод рисует дерево, которое отображает сообщения "Да" и "Инверс" вкладок ЛС1 - ЛС8 или ЛС9 - ЛС16. Уставки -> Вкладка ЛС.
        /// </summary>
        public static void CreateTree(List<DataGridView> dataGridsView, TabControl tabControl, System.Windows.Forms.TreeView treeView)
        {
            treeView.Nodes.Clear();
            TabControl.TabPageCollection allTabPages = tabControl.TabPages;
            for (int i = 0; i < dataGridsView.Count; i++)
            {
                int countYesAndInvers = 0; // количество выбранных элементов "Да", "Инверс"
                TreeNode parentNode = new TreeNode(allTabPages[i].Text);
                for (int j = 0; j < dataGridsView[i].RowCount; j++)
                {
                    if (dataGridsView[i].Rows[j].Cells[1].Value.ToString() != "Нет" && dataGridsView[i].Rows[j].Cells[1].Value.ToString() != "НЕТ")
                    {
                        countYesAndInvers++;
                        if (countYesAndInvers == 1)
                        {
                            treeView.Nodes.Add(parentNode);
                        }
                        TreeNode childrenNode = new TreeNode(dataGridsView[i].Rows[j].Cells[0].Value + " (" + dataGridsView[i].Rows[j].Cells[1].Value + ")");
                        childrenNode.Name = dataGridsView[i].Rows[j].Cells[0].Value.ToString();
                        parentNode.Nodes.Add(childrenNode);
                    }
                }
            }
        }


        /// <summary>
        /// Метод разворачивает узел дерева, находящийся на этапе редактирования.
        /// </summary>
        public static void ExpandCurrentTreeNode(TabControl tabControl, System.Windows.Forms.TreeView treeView)
        {
            TabPage currentTabPage = new TabPage();
            currentTabPage = tabControl.SelectedTab;
            for (int k = 0; k < treeView.Nodes.Count; k++)
            {
                if (currentTabPage.Text == treeView.Nodes[k].Text)
                {
                    treeView.Nodes[k].Expand();
                }
            }
        }


        /// <summary>
        /// Метод разворачивает узлы дерева (treeViewForLsOR), имена которых находятся в листе expandedNodesOR.
        /// </summary>
        public static void ExpandTreeNodesOR(System.Windows.Forms.TreeView treeView)
        {
            for (int k = 0; k < treeView.Nodes.Count; k++)
            {
                if (expandedNodesOR.Count > 0)
                {
                    if (expandedNodesOR.Contains(treeView.Nodes[k].Text))
                    {
                        treeView.Nodes[k].Expand();
                    }
                }
            }
        }


        /// <summary>
        /// Метод разворачивает узлы дерева (treeViewForLsAND), имена которых находятся в листе expandedNodesAND.
        /// </summary>
        public static void ExpandTreeNodesAND(System.Windows.Forms.TreeView treeView)
        {
            for (int k = 0; k < treeView.Nodes.Count; k++)
            {
                if (expandedNodesAND.Count > 0)
                {
                    if (expandedNodesAND.Contains(treeView.Nodes[k].Text))
                    {
                        treeView.Nodes[k].Expand();
                    }
                }
            }
        }


        /// <summary>
        /// Метод проверяет состояние узла, если развернут заносит в List string expandedNodes. ДЛЯ ЛС ИЛИ.
        /// </summary>
        public static void StateNodesOR(System.Windows.Forms.TreeView treeView)
        {
            expandedNodesOR.Clear();
            for (int i = 0; i < treeView.Nodes.Count; i++)
            {
                if (treeView.Nodes[i].IsExpanded)
                {
                    if (!expandedNodesOR.Contains(treeView.Nodes[i].Text))
                    {
                        expandedNodesOR.Add(treeView.Nodes[i].Text);
                    }
                }
                else
                {
                    if (treeView.Nodes.Count > 0)
                    {
                        if (expandedNodesOR.Contains(treeView.Nodes[i].Text))
                        {
                            expandedNodesOR.Remove(treeView.Nodes[i].Text);
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Метод проверяет состояние узла, если развернут заносит в List string expandedNodes. ДЛЯ ЛС И.
        /// </summary>
        public static void StateNodesAND(System.Windows.Forms.TreeView treeView)
        {
            expandedNodesAND.Clear();
            for (int i = 0; i < treeView.Nodes.Count; i++)
            {
                if (treeView.Nodes[i].IsExpanded)
                {
                    if (!expandedNodesAND.Contains(treeView.Nodes[i].Text))
                    {
                        expandedNodesAND.Add(treeView.Nodes[i].Text);
                    }
                }
                else
                {
                    if (treeView.Nodes.Count > 0)
                    {
                        if (expandedNodesAND.Contains(treeView.Nodes[i].Text))
                        {
                            expandedNodesAND.Remove(treeView.Nodes[i].Text);
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Метод позволяет удалять узлы дерева по нажатию ПКМ. Уставки -> Вкладка ЛС.
        /// </summary>
        public static void DeleteNode(object sender, TreeNodeMouseClickEventArgs e, ContextMenuStrip contextMenu, List<DataGridView> dataGridsView)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenu.Items.Clear();
                string nodeText = e.Node.Text;
                string nodeName = e.Node.Name;
                if (nodeText.StartsWith("ЛС"))
                {
                    DialogResult result = MessageBox.Show($"Удалить узел {nodeText}?", "Подтверждение действия",
                        MessageBoxButtons.YesNo);
                    if (result != DialogResult.Yes) return;
                }
                else
                {
                    DialogResult result = MessageBox.Show($"Удалить сигнал {nodeText}?", "Подтверждение действия",
                        MessageBoxButtons.YesNo);
                    if (result != DialogResult.Yes) return;
                }
                for (int i = 0; i < dataGridsView.Count; i++)
                {
                    for (int j = 0; j < dataGridsView[i].RowCount; j++)
                    {
                        if (nodeText == dataGridsView[i].Parent.Text) // при удалении корневого узла
                        {
                            dataGridsView[i].Rows[j].Cells[1].Value = "Нет";
                        }
                        if (nodeName == dataGridsView[i].Rows[j].Cells[0].Value.ToString()) // при удалении дочернего узла
                        {
                            dataGridsView[i].Rows[j].Cells[1].Value = "Нет";
                        }
                    }
                }
            }
        }
    }
}