﻿namespace BEMN.Forms
{
    partial class ScenesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this._addSceneButton = new System.Windows.Forms.Button();
            this._nameSceneMTB = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._timeTB = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(70, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Время";
            // 
            // _addSceneButton
            // 
            this._addSceneButton.Location = new System.Drawing.Point(53, 67);
            this._addSceneButton.Name = "_addSceneButton";
            this._addSceneButton.Size = new System.Drawing.Size(163, 23);
            this._addSceneButton.TabIndex = 8;
            this._addSceneButton.Text = "Добавить";
            this._addSceneButton.UseVisualStyleBackColor = true;
            this._addSceneButton.Click += new System.EventHandler(this._addSceneButton_Click);
            // 
            // _nameSceneMTB
            // 
            this._nameSceneMTB.Location = new System.Drawing.Point(116, 10);
            this._nameSceneMTB.Name = "_nameSceneMTB";
            this._nameSceneMTB.Size = new System.Drawing.Size(145, 20);
            this._nameSceneMTB.TabIndex = 9;
            this._nameSceneMTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Название сценария";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(237, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "мс.";
            // 
            // _timeTB
            // 
            this._timeTB.Location = new System.Drawing.Point(116, 41);
            this._timeTB.Name = "_timeTB";
            this._timeTB.Size = new System.Drawing.Size(115, 20);
            this._timeTB.TabIndex = 12;
            this._timeTB.Text = "10";
            this._timeTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._timeTB.TextChanged += new System.EventHandler(this._timeTB_TextChanged);
            this._timeTB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._timeTB_KeyPress);
            // 
            // ScenesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(268, 94);
            this.Controls.Add(this._timeTB);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this._nameSceneMTB);
            this.Controls.Add(this._addSceneButton);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ScenesForm";
            this.Text = "Добавление сценария";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _addSceneButton;
        private System.Windows.Forms.MaskedTextBox _nameSceneMTB;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _timeTB;
    }
}