namespace BEMN.Forms
{
    partial class SaveSourceDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._cancelBut = new System.Windows.Forms.Button();
            this._acceptBut = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._fileList = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _cancelBut
            // 
            this._cancelBut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancelBut.Location = new System.Drawing.Point(281, 169);
            this._cancelBut.Name = "_cancelBut";
            this._cancelBut.Size = new System.Drawing.Size(87, 23);
            this._cancelBut.TabIndex = 3;
            this._cancelBut.Text = "������";
            this._cancelBut.UseVisualStyleBackColor = true;
            // 
            // _acceptBut
            // 
            this._acceptBut.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this._acceptBut.Location = new System.Drawing.Point(9, 169);
            this._acceptBut.Name = "_acceptBut";
            this._acceptBut.Size = new System.Drawing.Size(87, 23);
            this._acceptBut.TabIndex = 1;
            this._acceptBut.Text = "���������";
            this._acceptBut.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.No;
            this.button1.Location = new System.Drawing.Point(156, 169);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "�� ���������";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._fileList);
            this.groupBox1.Location = new System.Drawing.Point(9, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(359, 160);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "�������� ����������� �����";
            // 
            // _fileList
            // 
            this._fileList.FormattingEnabled = true;
            this._fileList.Location = new System.Drawing.Point(6, 19);
            this._fileList.Name = "_fileList";
            this._fileList.Size = new System.Drawing.Size(347, 134);
            this._fileList.TabIndex = 0;
            // 
            // SaveSourceDialog
            // 
            this.AcceptButton = this._acceptBut;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._cancelBut;
            this.ClientSize = new System.Drawing.Size(380, 201);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this._cancelBut);
            this.Controls.Add(this._acceptBut);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SaveSourceDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "���������� ������";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _cancelBut;
        private System.Windows.Forms.Button _acceptBut;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox _fileList;

    }
}