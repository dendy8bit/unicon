﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MBServer;

namespace BEMN.Forms.Queries
{
    public partial class QueriesForm : Form
    {
        #region Fields
        private const int WORD_BITS = 16;
        private int _wordCnt;
        private ushort _currentBoxIndex;
        private Device _device;
        private MemoryEntity<SomeStruct> _query;
        private HexTextbox _currentBox;
        private Button _currentButton;
        private LedControl[][] _words;
        private AdductionSettings[] _adduction;
        private Label[] _addrLabels;
        private HexTextbox[] _hexBoxes;
        private HexTextbox[] _decBoxes;
        private TextBox[] _adductionBoxes;
        private bool _loop;
        private static int Counter;
        #endregion

        #region Const

        private const string SET_BIT_PATTERN = "SetBit_{0}_{1}";
        #endregion

        #region Constructors
        public QueriesForm()
        {
            this.InitializeComponent();
            this._countOfWord.SelectedIndex = 0;
            this._wordCnt = Convert.ToInt32(this._countOfWord.SelectedItem);
            this._words = new LedControl[this._wordCnt][];
            this._adduction = new AdductionSettings[this._wordCnt];
            this._addrLabels = new Label[this._wordCnt];
            this._hexBoxes = new HexTextbox[this._wordCnt];
            this._decBoxes = new HexTextbox[this._wordCnt];
            this._adductionBoxes = new TextBox[this._wordCnt];
        }

        public QueriesForm(Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._countOfWord.SelectedIndex = 0;
            this._countOfWord.SelectedIndexChanged += new System.EventHandler(this._countOfWord_SelectedIndexChanged);
            this._addrHex.TextChanged += new EventHandler(this._addrHex_TextChanged);

            this.InitNewQuery();
        }
        
        #endregion

        #region Init
        private void InitNewQuery()
        {
            string queryName = string.Format("Обмены{0}",Counter);
            this._query = new MemoryEntity<SomeStruct>(queryName, this._device, 0x0000);
            this._query.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MlkQueryReadComplete);
            this._query.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MlkQueryReadFail);
            Counter++;
        }

        private void InitControls()
        {
            this._addrHex.HexBase = true;
            this._addrHex.Correct();

            this._ledsGroup.Controls.Clear();
            this._addrGroup.Controls.Clear();
            this._decGroup.Controls.Clear();
            this._hexGroup.Controls.Clear();
            this._adductionGroup.Controls.Clear();

            this._wordCnt = this._countOfWord.SelectedIndex + 8;

            this.InitLedsControl();

            this._adduction = new AdductionSettings[this._wordCnt];
            this._addrLabels = new Label[this._wordCnt];
            this._hexBoxes = new HexTextbox[this._wordCnt];
            this._decBoxes = new HexTextbox[this._wordCnt];
            this._adductionBoxes = new TextBox[this._wordCnt];
            this._wordIndexCombo.Items.Clear();
            for (int i = 0; i < this._wordCnt; i++)
            {
                this.CreateAddrLabel(i);
                this.CreateDecLabel(i);
                this.CreateHexLabel(i);
                this.CreateAdductionBox(i);

                this._ledsGroup.Controls.AddRange(this._words[i]);
                this._wordIndexCombo.Items.Add(i + 1);
                this._adduction[i] = new AdductionSettings(100.0, 65535, 3);
            }
            this._symbolsCombo.Items.Clear();
            for (int j = 0; j < 4; j++)
            {
                this._symbolsCombo.Items.Add(j);
            }

            this._addrGroup.Controls.AddRange(this._addrLabels);
            this._decGroup.Controls.AddRange(this._decBoxes);
            this._hexGroup.Controls.AddRange(this._hexBoxes);
            this._adductionGroup.Controls.AddRange(this._adductionBoxes);

            this._wordIndexCombo.SelectedIndex = 0;
            this._symbolsCombo.SelectedIndex = 3;
        }

        private void InitLedsControl()
        {
            this._words = new LedControl[this._wordCnt][];
            for (int i = 0; i < this._wordCnt; i++)
            {
                this._words[i] = new LedControl[WORD_BITS];
                for (int j = 0; j < WORD_BITS; j++)
                {
                    this._words[i][j] = new LedControl();
                    this._words[i][j].Location = j < 8
                        ? new Point(198 - j * 12, 14 + i * 18)
                        : new Point(198 - j * 12 - 6, 14 + i * 18);
                    this._words[i][j].State = LedState.Off;
                    this._words[i][j].pictureBox.MouseClick += new MouseEventHandler(this.Led_MouseClick);
                    this._ledsGroup.Controls.Add(this._words[i][j]);
                }
            }
        }
        #endregion

        #region ReadStruct

        public void MlkQueryReadFail()
        {
            if (false == Disposing && false == IsDisposed)
            {
                this.TurnOff();
            }
        }

        public void MlkQueryReadComplete() 
        {
            if (false == Disposing && false == IsDisposed)
            {
                byte[] buffer = Common.TOBYTES(this._query.Values, true);

                for (int i = 0; i < buffer.Length; i += 2)
                {
                    LedManager.SetLeds(this._words[i / 2], new BitArray(new[] { buffer[i + 1], buffer[i] }));
                    if ((bool) this._hexBoxes[i / 2].Tag)
                    {
                        this._hexBoxes[i / 2].Text = Common.TOWORD(buffer[i], buffer[i + 1]).ToString("X4");
                    }
                    if ((bool) this._decBoxes[i / 2].Tag)
                    {
                        this._decBoxes[i / 2].Text = Common.TOWORD(buffer[i], buffer[i + 1]).ToString("");
                    }

                    if ((bool) this._adductionBoxes[i / 2].Tag)
                    {
                        ushort val = Common.TOWORD(buffer[i], buffer[i + 1]);
                        double adduct = val *this._adduction[i / 2].Limit /this._adduction[i / 2].Max;

                        this._adductionBoxes[i / 2].Text = adduct.ToString("F" + this._adduction[i / 2].Symbols);
                    }
                }
            }
        }
        #endregion

        #region Functions
        private void TurnOff()
        {
            if (LedManager.IsControlsCreated(this._decBoxes))
            {
                for (int i = 0; i < this._wordCnt; i++)
                {
                    LedManager.TurnOffLeds(this._words[i]);
                    this._hexBoxes[i].Text = "";
                    this._decBoxes[i].Text = "";
                    this._adductionBoxes[i].Text = "";
                }
            }
        }

        /// <summary>
        /// Отобразить адреса
        /// </summary>
        private void SetAddrLabelText()
        {
            //Если адресация словная
            if (this._wordfRadio.Checked)
            {
                for (int i = 0; i < this._addrLabels.Length; i++)
                {
                    //Адреса больше 0xFFFF не рисуем
                    if (this._addrHex.Number + i <= ushort.MaxValue)
                    {
                        this._addrLabels[i].Text = (this._addrHex.Number + i).ToString("X4");
                    }
                    else
                    {
                        this._addrLabels[i].Text = "";
                    }

                    //Строка слишком длинная, метку надо отодвинуть
                    if (50 > this._addrLabels[i].Location.X)
                    {
                        this._addrLabels[i].Location = new Point(this._addrLabels[i].Location.X + 35, this._addrLabels[i].Location.Y);
                    }
                }
            }
            else
            {
                //Если адресация битная
                for (int i = 0; i < this._addrLabels.Length; i++)
                {
                    if (this._addrHex.Number + i + 0xF <= ushort.MaxValue)
                    {
                        this._addrLabels[i].Text = (this._addrHex.Number + 0x10 * i + 0xF).ToString("X4") + "-" +
                                              (this._addrHex.Number + 0x10 * i).ToString("X4");
                    }
                    else
                    {
                        this._addrLabels[i].Text = "";
                    }

                    if (50 == this._addrLabels[i].Location.X)
                    {
                        this._addrLabels[i].Location = new Point(this._addrLabels[i].Location.X - 35, this._addrLabels[i].Location.Y);
                    }
                }
            }
        }

        private void LoadWithCurrentSettings()
        {
            if (!this._queryEnableCheckBox.Checked) return;
            if (this._bitfRadio.Checked)
            {
                if ((ushort) this._addrHex.Number < (ushort)(this._addrHex.Number + this._wordCnt * 0x10))
                {
                    this._query.Clear();
                    ushort[] values = new ushort[this._wordCnt];
                    this._query.Values = values;
                    this._query.Slots = SetSlots(values, this._query.StartAddress);
                    this._query.LoadBitsCycle();
                }
            }
            else
            {
                this._query.Clear();
                ushort[] values = new ushort[this._wordCnt];
                this._query.Values = values;
                this._query.Slots = SetSlots(values, this._query.StartAddress);
                this._query.LoadStructCycle();
            }
        }

        private void CreateAddrLabel(int i)
        {
            this._addrLabels[i] = new Label();
            this._addrLabels[i].AutoSize = true;
            this._addrLabels[i].Font = new Font("Microsoft Sans Serif", 8F);
            this._addrLabels[i].Location = new Point(50, 11 + i * 18);
            this._addrLabels[i].Size = new Size(34, 13);
            this._addrLabels[i].TextAlign = ContentAlignment.MiddleRight;
        }

        private void CreateDecLabel(int i)
        {
            this._decBoxes[i] = new HexTextbox();
            this._decBoxes[i].AutoSize = true;
            this._decBoxes[i].HexBase = false;
            this._decBoxes[i].Font = new Font("Microsoft Sans Serif", 8F);
            this._decBoxes[i].Location = new Point(3, 11 + i * 18);
            this._decBoxes[i].Size = new Size(49, 13);
            this._decBoxes[i].TextAlign = HorizontalAlignment.Center;
            this._decBoxes[i].Text = i.ToString("D4");
            this._decBoxes[i].Limit = ushort.MaxValue + 1;
            this._decBoxes[i].Tag = true;
            this._decBoxes[i].GotFocus += new EventHandler(this.TextBoxGotFocus);
            this._decBoxes[i].LostFocus += new EventHandler(this.TextBoxLostFocus);
        }

        private void CreateHexLabel(int i)
        {
            this._hexBoxes[i] = new HexTextbox();
            this._hexBoxes[i].HexBase = true;
            this._hexBoxes[i].AutoSize = true;
            this._hexBoxes[i].Font = new Font("Microsoft Sans Serif", 8F);
            this._hexBoxes[i].Location = new Point(3, 11 + i * 18);
            this._hexBoxes[i].Size = new Size(49, 13);
            this._hexBoxes[i].TextAlign = HorizontalAlignment.Center;
            this._hexBoxes[i].Text = i.ToString("X4");
            this._hexBoxes[i].Limit = ushort.MaxValue + 1;
            this._hexBoxes[i].Tag = true;
            this._hexBoxes[i].GotFocus += new EventHandler(this.TextBoxGotFocus);
            this._hexBoxes[i].LostFocus += new EventHandler(this.TextBoxLostFocus);
        }

        private void CreateAdductionBox(int i)
        {
            this._adductionBoxes[i] = new TextBox();
            this._adductionBoxes[i].Size = new Size(49, 13);
            this._adductionBoxes[i].Location = new Point(3, 11 + i * 18);
            this._adductionBoxes[i].Tag = true;
        }

        private void CreateSetButton()
        {
            this._currentButton = new Button();
            this._currentButton.Size = new Size(30, 20 - 2);
            this._currentButton.Text = "Уст.";
            this._currentButton.TextAlign = ContentAlignment.MiddleLeft;
            this._currentButton.MouseDown += new MouseEventHandler(this._currentButtton_Click);
            Controls.Add(this._currentButton);
            Controls.SetChildIndex(this._currentButton, 0);
        }

        private ushort GetBitIndex(Point ledLocation)
        {
            int row;
            int column;

            if (0 == ledLocation.X % 12)
            {
                row = ledLocation.X / 12;
            }
            else
            {
                row = (ledLocation.X - 6) / 12;
            }

            column = (ledLocation.Y - 14) / 18;

            row = 16 - row;
            return (ushort)(column * 16 + row + this._addrHex.Number);
        }

        private void OnHexSet()
        {
            ushort newValue = (ushort) this._hexBoxes[this._currentBoxIndex].Number;
            this._query.SaveOneWord(newValue, this._currentBoxIndex);
        }

        private void OnDecSet()
        {
            ushort newValue = (ushort)this._decBoxes[this._currentBoxIndex].Number;
            this._query.SaveOneWord(newValue, this._currentBoxIndex);
        }

        private void EnableValueBoxes(bool bEnable)
        {
            for (int i = 0; i < this._wordCnt; i++)
            {
                this._hexBoxes[i].Enabled = bEnable;
                this._decBoxes[i].Enabled = bEnable;
                this._adductionBoxes[i].Enabled = bEnable;
            }
        }

        private void ShowAdduction(int wordIndex)
        {
            if (wordIndex <= this._wordCnt && wordIndex >= 0)
            {
                this._maxHex.Text = this._adduction[wordIndex].Max.ToString();
                this._limitBox.Text = this._adduction[wordIndex].Limit.ToString();
                this._symbolsCombo.SelectedIndex = this._adduction[wordIndex].Symbols;
            }
        }
        #endregion

        #region Event handlers
        private void KlQueriesFormLoad(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            MdiParent.Refresh();

            this.InitControls();
            this.SetAddrLabelText();

            this._addrHex.BorderStyle = BorderStyle.Fixed3D;
            this._addrHex.Text = this._query.StartAddress.ToString("X");
            this._addrHex.Correct();

            this._bitfRadio.Checked = false;
        }

        private void MLK_Querys_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._query.RemoveStructQueries();
            this._query.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this, this.MlkQueryReadComplete);
            this._query.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this, this.MlkQueryReadFail);
        }

        private void _addrHex_TextChanged(object sender, EventArgs e)
        {
            this._addrHex.Correct();
            this._query.StartAddress = (ushort) this._addrHex.Number;
            this.LoadWithCurrentSettings();
            this.SetAddrLabelText();
        }
        /// <summary>
        /// Клик по всплывающей кнопке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _currentButtton_Click(object sender, EventArgs e)
        {
            this._currentButton.Hide();
            if (null != this._currentBox)
            {
                this._currentBox.Correct();
                this._currentBox.Tag = true;
                if (this._currentBox.HexBase)
                {
                    this.OnHexSet();
                }
                else
                {
                    this.OnDecSet();
                }
            }

            this._currentBox = null; ;
        }
        /// <summary>
        /// Поле ввода получило фокус
        /// </summary>
        /// <param name="sender">Выбранное поле ввода</param>
        /// <param name="e">Параметры ивента</param>
        private void TextBoxGotFocus(object sender, EventArgs e)
        {
            //Создаем кнопку, если ранее не была создана
            if (this._currentButton == null)
            {
                this.CreateSetButton();
            }
            //Передвигаем кнопку к нужному полю ввода
            //CreateSetButton(sender);
            this.BindSetButton(sender);
            //Теперь поле ввода обновлять не требуется
            this._currentBox = sender as HexTextbox;
            if (this._currentBox == null) return;
            this._currentBox.Tag = false;
            this._currentBoxIndex = this._currentBox.HexBase
                ? (ushort) this._hexBoxes.ToList().IndexOf(this._currentBox)
                : (ushort) this._decBoxes.ToList().IndexOf(this._currentBox);
        }

        private void TextBoxLostFocus(object sender, EventArgs e)
        {
            if (this._currentButton.Focused) return;
            this._currentButton.Hide();
            (sender as Control).Tag = true;
        }

        private void BindSetButton(object sender)
        {
            GroupBox parent = (sender as Control).Parent as GroupBox;
            Point hexLocation = (sender as Control).Location;
            Size hexSize = (sender as Control).Size;
            Point buttonPoint =
                new Point(parent.Location.X + hexLocation.X + hexSize.Width, parent.Location.Y + hexLocation.Y);
            this._currentButton.Location = buttonPoint;
            this._currentButton.Show();
        }
        /// <summary>
        /// Клацнули мышой по лампочке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Led_MouseClick(object sender, MouseEventArgs e)
        {
            //Только если битная адресация
            if (this._bitfRadio.Checked)
            {
                LedControl led = (sender as PictureBox).Parent as LedControl;

                ushort bitInd = this.GetBitIndex(led.Location);

                switch (e.Button)
                {
                    case MouseButtons.Left:
                        //По левой кнопке инвертируем бит
                        if (LedState.Signaled == led.State)
                        {
                            this._device.SetBit(this._device.DeviceNumber, bitInd, true,
                                string.Format(SET_BIT_PATTERN, bitInd, this._device.DeviceNumber), this._device);
                        }
                        else if (LedState.NoSignaled == led.State)
                        {
                            this._device.SetBit(this._device.DeviceNumber, bitInd, false,
                                string.Format(SET_BIT_PATTERN, bitInd, this._device.DeviceNumber), this._device);
                        }
                        break;
                    //По правой показываем меню
                    case MouseButtons.Right:
                        this._contextMenu.Items.Clear();
                        string set0 = "Сбросить     бит № - " + bitInd.ToString("X4") + "(" + bitInd + ")";
                        string set1 = "Установить бит № - " + bitInd.ToString("X4") + "(" + bitInd + ")";
                        this._contextMenu.Items.Add(set0);
                        this._contextMenu.Items.Add(set1);
                        this._contextMenu.Items[0].Tag = this._contextMenu.Items[1].Tag = bitInd;
                        this._contextMenu.ItemClicked += new ToolStripItemClickedEventHandler(this._contextMenu_ItemClicked);
                        this._contextMenu.Show(MousePosition.X, MousePosition.Y);
                        break;
                }
            }
        }

        private void _symbolsCombo_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                this._adduction[this.CurrentWordIndex].Symbols = this._symbolsCombo.SelectedIndex;
            }
            catch (IndexOutOfRangeException)
            {
            }
        }

        /// <summary>
        /// Выбранный номер слова
        /// </summary>
        public int CurrentWordIndex
        {
            get
            {
                try
                {
                    int ret = Convert.ToInt32(this._wordIndexCombo.Text);
                    if (ret < 1 || ret > 16)
                    {
                        this._maxHex.Text = "";
                        this._limitBox.Text = "";
                        this._symbolsCombo.Text = "";
                    }
                    ret -= 1;
                    return ret;
                }
                catch (FormatException)
                {
                    this._maxHex.Text = "";
                    this._limitBox.Text = "";
                    this._symbolsCombo.Text = "";
                    return 0;
                }
            }
        }

        private void _contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ushort bitInd = (ushort)e.ClickedItem.Tag;
            try
            {
                if (e.ClickedItem == this._contextMenu.Items[0])
                {
                    this._device.SetBit(this._device.DeviceNumber, bitInd, false,
                        string.Format(SET_BIT_PATTERN, bitInd, this._device.DeviceNumber), this._device);
                }
                if (e.ClickedItem == this._contextMenu.Items[1])
                {
                    this._device.SetBit(this._device.DeviceNumber, bitInd, true,
                        string.Format(SET_BIT_PATTERN, bitInd, this._device.DeviceNumber), this._device);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void _bitfRadio_CheckedChanged(object sender, EventArgs e)
        {
            this.EnableValueBoxes(this._wordfRadio.Checked);
            this.SetAddrLabelText();
            if (this._queryEnableCheckBox.Checked)
            {
                this.LoadWithCurrentSettings();
            }
        }

        private void _wordIndexCombo_TextUpdate(object sender, EventArgs e)
        {
            try
            {
                this.ShowAdduction(this.CurrentWordIndex);
            }
            catch (FormatException)
            {
                this._maxHex.Text = "";
                this._limitBox.Text = "";
                this._symbolsCombo.Text = "";
            }
        }

        private void _wordIndexCombo_SelectedValueChanged(object sender, EventArgs e)
        {
            this.ShowAdduction(this.CurrentWordIndex);
        }

        private void _limitBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this._adduction[this.CurrentWordIndex].Limit = Convert.ToDouble(this._limitBox.Text);
            }
            catch (FormatException)
            {
                return;
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }
        }

        private void _maxHex_TextChanged(object sender, EventArgs e)
        {
            this._maxHex.Correct();
            try
            {
                this._adduction[this.CurrentWordIndex].Max = (ushort) this._maxHex.Number;
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }
        }

        private void _loopButton_Click(object sender, EventArgs e)
        {
            if (this._loop)
            {
                this._loopButton.Text = ">";
                Width = 550;
                this._optionsGB.Location = new Point(560, 1);
                this._loop = false;
            }
            else
            {
                this._loopButton.Text = "<";
                Width = 820;
                this._optionsGB.Location = new Point(525, 1);
                this._loop = true;
            }
        }
        
        private void _countOfWord_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (this._countOfWord.SelectedIndex == -1)
                {
                    Height = 200;
                }
                else
                {
                    int i = this._countOfWord.SelectedIndex;
                    Height = 200 + i * 18;
                }
                this._wordCnt = this._countOfWord.SelectedIndex + 8;
                this.InitControls();
                this.SetAddrLabelText();
                for (int i = 0; i < this._adduction.Length; i++)
                {
                    this._adduction[i].Limit = double.Parse(this._limitBox.Text);
                    this._adduction[i].Max = ushort.Parse(this._maxHex.Text);
                    this._adduction[i].Symbols = this._symbolsCombo.SelectedIndex;
                }
                this.LoadWithCurrentSettings();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void _queryEnableCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (this._queryEnableCheckBox.Checked)
            {
                this.LoadWithCurrentSettings();
            }
            else
            {
                this._query.RemoveStructQueries();
            }
        }
        
        #endregion
        /// <summary>
        /// Получает слот или массив слотов в зависимости от размера массива
        /// записываемых слов в устройство
        /// </summary>
        /// <param name="values">Массив слов</param>
        /// <param name="startAdress">Стартовый адрес</param>
        /// <returns>Лист слотов</returns>
        public static List<Device.slot> SetSlots(ushort[] values, ushort startAdress)
        {
            List<Device.slot> slotsRet = new List<Device.slot>();
            const int maxLength = 64;
            bool isSlotsArr = values.Length > maxLength;
            if (!isSlotsArr)
            {
                slotsRet.Add(new Device.slot(startAdress, (ushort)(startAdress + values.Length)));
            }
            else
            {
                int arrayLength = values.Length / maxLength;
                int lastSlotLength = values.Length % maxLength;
                Device.slot[] slots;
                ushort startAddr = startAdress;
                if (lastSlotLength != 0)
                {
                    arrayLength++;
                    slots = new Device.slot[arrayLength];

                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + maxLength));
                        startAddr += (ushort)maxLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                    slotsRet.AddRange(slots.ToArray());
                }
                else
                {
                    slots = new Device.slot[arrayLength];
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + maxLength));
                        startAddr += (ushort)maxLength;
                    }
                    slotsRet.AddRange(slots.ToArray());
                }
            }
            return slotsRet;
        }
    }
}
