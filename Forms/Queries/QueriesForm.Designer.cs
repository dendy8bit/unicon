﻿namespace BEMN.Forms.Queries
{
    partial class QueriesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._loopButton = new System.Windows.Forms.Button();
            this._contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._addrGroup = new System.Windows.Forms.GroupBox();
            this._decGroup = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._wordIndexCombo = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this._maxHex = new BEMN.Forms.HexTextbox();
            this._symbolsCombo = new System.Windows.Forms.ComboBox();
            this._limitBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._addrHex = new BEMN.Forms.HexTextbox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._bitfRadio = new System.Windows.Forms.RadioButton();
            this._wordfRadio = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._countOfWord = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this._optionsGB = new System.Windows.Forms.GroupBox();
            this._queryEnableCheckBox = new System.Windows.Forms.CheckBox();
            this._hexGroup = new System.Windows.Forms.GroupBox();
            this._adductionGroup = new System.Windows.Forms.GroupBox();
            this._ledsGroup = new System.Windows.Forms.GroupBox();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._optionsGB.SuspendLayout();
            this.SuspendLayout();
            // 
            // _loopButton
            // 
            this._loopButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._loopButton.BackColor = System.Drawing.Color.LightGray;
            this._loopButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._loopButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._loopButton.Location = new System.Drawing.Point(519, 1);
            this._loopButton.Name = "_loopButton";
            this._loopButton.Size = new System.Drawing.Size(14, 165);
            this._loopButton.TabIndex = 3;
            this._loopButton.Text = ">";
            this._loopButton.UseVisualStyleBackColor = false;
            this._loopButton.Click += new System.EventHandler(this._loopButton_Click);
            // 
            // _contextMenu
            // 
            this._contextMenu.Name = "_contextMenu";
            this._contextMenu.Size = new System.Drawing.Size(61, 4);
            // 
            // _addrGroup
            // 
            this._addrGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._addrGroup.Location = new System.Drawing.Point(2, 0);
            this._addrGroup.Name = "_addrGroup";
            this._addrGroup.Size = new System.Drawing.Size(84, 165);
            this._addrGroup.TabIndex = 0;
            this._addrGroup.TabStop = false;
            this._addrGroup.Text = "Адрес";
            // 
            // _decGroup
            // 
            this._decGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._decGroup.Location = new System.Drawing.Point(386, 1);
            this._decGroup.Name = "_decGroup";
            this._decGroup.Size = new System.Drawing.Size(57, 164);
            this._decGroup.TabIndex = 2;
            this._decGroup.TabStop = false;
            this._decGroup.Text = "DEC";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._wordIndexCombo);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this._maxHex);
            this.groupBox5.Controls.Add(this._symbolsCombo);
            this.groupBox5.Controls.Add(this._limitBox);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Location = new System.Drawing.Point(115, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(141, 126);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Приведение(dec)";
            // 
            // _wordIndexCombo
            // 
            this._wordIndexCombo.FormattingEnabled = true;
            this._wordIndexCombo.Location = new System.Drawing.Point(76, 19);
            this._wordIndexCombo.Name = "_wordIndexCombo";
            this._wordIndexCombo.Size = new System.Drawing.Size(59, 21);
            this._wordIndexCombo.TabIndex = 12;
            this._wordIndexCombo.TextUpdate += new System.EventHandler(this._wordIndexCombo_TextUpdate);
            this._wordIndexCombo.SelectedValueChanged += new System.EventHandler(this._wordIndexCombo_SelectedValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(2, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 13);
            this.label12.TabIndex = 13;
            this.label12.Text = "Номер слова";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(25, 102);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(44, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "Знаков";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(25, 50);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Предел";
            // 
            // _maxHex
            // 
            this._maxHex.HexBase = false;
            this._maxHex.Limit = 65535;
            this._maxHex.Location = new System.Drawing.Point(76, 73);
            this._maxHex.Name = "_maxHex";
            this._maxHex.Size = new System.Drawing.Size(59, 20);
            this._maxHex.TabIndex = 9;
            this._maxHex.Text = "65535";
            this._maxHex.TextChanged += new System.EventHandler(this._maxHex_TextChanged);
            // 
            // _symbolsCombo
            // 
            this._symbolsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._symbolsCombo.FormattingEnabled = true;
            this._symbolsCombo.Location = new System.Drawing.Point(76, 99);
            this._symbolsCombo.Name = "_symbolsCombo";
            this._symbolsCombo.Size = new System.Drawing.Size(59, 21);
            this._symbolsCombo.TabIndex = 5;
            this._symbolsCombo.SelectedIndexChanged += new System.EventHandler(this._symbolsCombo_SelectedValueChanged);
            // 
            // _limitBox
            // 
            this._limitBox.Location = new System.Drawing.Point(76, 47);
            this._limitBox.Name = "_limitBox";
            this._limitBox.Size = new System.Drawing.Size(59, 20);
            this._limitBox.TabIndex = 10;
            this._limitBox.Text = "100.0";
            this._limitBox.TextChanged += new System.EventHandler(this._limitBox_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 76);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Максимум";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._addrHex);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Location = new System.Drawing.Point(6, 13);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(103, 41);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            // 
            // _addrHex
            // 
            this._addrHex.HexBase = true;
            this._addrHex.Limit = 65536;
            this._addrHex.Location = new System.Drawing.Point(6, 14);
            this._addrHex.Name = "_addrHex";
            this._addrHex.Size = new System.Drawing.Size(91, 20);
            this._addrHex.TabIndex = 4;
            this._addrHex.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(0, -1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Начальный адрес";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._bitfRadio);
            this.groupBox7.Controls.Add(this._wordfRadio);
            this.groupBox7.Location = new System.Drawing.Point(6, 105);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(103, 52);
            this.groupBox7.TabIndex = 8;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Адресация";
            // 
            // _bitfRadio
            // 
            this._bitfRadio.AutoSize = true;
            this._bitfRadio.Location = new System.Drawing.Point(6, 29);
            this._bitfRadio.Name = "_bitfRadio";
            this._bitfRadio.Size = new System.Drawing.Size(61, 17);
            this._bitfRadio.TabIndex = 6;
            this._bitfRadio.Text = "Битная";
            this._bitfRadio.UseVisualStyleBackColor = true;
            this._bitfRadio.CheckedChanged += new System.EventHandler(this._bitfRadio_CheckedChanged);
            // 
            // _wordfRadio
            // 
            this._wordfRadio.AutoSize = true;
            this._wordfRadio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this._wordfRadio.Checked = true;
            this._wordfRadio.Location = new System.Drawing.Point(6, 14);
            this._wordfRadio.Name = "_wordfRadio";
            this._wordfRadio.Size = new System.Drawing.Size(68, 17);
            this._wordfRadio.TabIndex = 5;
            this._wordfRadio.TabStop = true;
            this._wordfRadio.Text = "Словная";
            this._wordfRadio.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._countOfWord);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(6, 53);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(103, 45);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            // 
            // _countOfWord
            // 
            this._countOfWord.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._countOfWord.Items.AddRange(new object[] {
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32"});
            this._countOfWord.Location = new System.Drawing.Point(6, 19);
            this._countOfWord.Name = "_countOfWord";
            this._countOfWord.Size = new System.Drawing.Size(91, 21);
            this._countOfWord.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Количество слов";
            // 
            // _optionsGB
            // 
            this._optionsGB.Controls.Add(this.groupBox1);
            this._optionsGB.Controls.Add(this._queryEnableCheckBox);
            this._optionsGB.Controls.Add(this.groupBox7);
            this._optionsGB.Controls.Add(this.groupBox6);
            this._optionsGB.Controls.Add(this.groupBox5);
            this._optionsGB.Location = new System.Drawing.Point(560, 2);
            this._optionsGB.Name = "_optionsGB";
            this._optionsGB.Size = new System.Drawing.Size(262, 163);
            this._optionsGB.TabIndex = 4;
            this._optionsGB.TabStop = false;
            this._optionsGB.Text = "Настройка";
            // 
            // _queryEnableCheckBox
            // 
            this._queryEnableCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._queryEnableCheckBox.AutoSize = true;
            this._queryEnableCheckBox.Location = new System.Drawing.Point(127, 140);
            this._queryEnableCheckBox.Name = "_queryEnableCheckBox";
            this._queryEnableCheckBox.Size = new System.Drawing.Size(68, 17);
            this._queryEnableCheckBox.TabIndex = 8;
            this._queryEnableCheckBox.Text = "Обмены";
            this._queryEnableCheckBox.UseVisualStyleBackColor = true;
            this._queryEnableCheckBox.CheckedChanged += new System.EventHandler(this._queryEnableCheckBox_CheckedChanged);
            // 
            // _hexGroup
            // 
            this._hexGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._hexGroup.Location = new System.Drawing.Point(323, 1);
            this._hexGroup.Name = "_hexGroup";
            this._hexGroup.Size = new System.Drawing.Size(57, 164);
            this._hexGroup.TabIndex = 5;
            this._hexGroup.TabStop = false;
            this._hexGroup.Text = "HEX";
            // 
            // _adductionGroup
            // 
            this._adductionGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._adductionGroup.Location = new System.Drawing.Point(449, 1);
            this._adductionGroup.Name = "_adductionGroup";
            this._adductionGroup.Size = new System.Drawing.Size(63, 164);
            this._adductionGroup.TabIndex = 7;
            this._adductionGroup.TabStop = false;
            this._adductionGroup.Text = "Прив.";
            // 
            // _ledsGroup
            // 
            this._ledsGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._ledsGroup.Location = new System.Drawing.Point(92, 1);
            this._ledsGroup.Name = "_ledsGroup";
            this._ledsGroup.Size = new System.Drawing.Size(225, 164);
            this._ledsGroup.TabIndex = 1;
            this._ledsGroup.TabStop = false;
            // 
            // QueriesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 167);
            this.Controls.Add(this._ledsGroup);
            this.Controls.Add(this._adductionGroup);
            this.Controls.Add(this._hexGroup);
            this.Controls.Add(this._optionsGB);
            this.Controls.Add(this._loopButton);
            this.Controls.Add(this._decGroup);
            this.Controls.Add(this._addrGroup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "QueriesForm";
            this.Text = "Обмены";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MLK_Querys_FormClosing);
            this.Load += new System.EventHandler(this.KlQueriesFormLoad);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._optionsGB.ResumeLayout(false);
            this._optionsGB.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _loopButton;
        private System.Windows.Forms.ContextMenuStrip _contextMenu;
        private System.Windows.Forms.GroupBox _addrGroup;
        private System.Windows.Forms.GroupBox _decGroup;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox _wordIndexCombo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label11;
        private Forms.HexTextbox _maxHex;
        private System.Windows.Forms.ComboBox _symbolsCombo;
        private System.Windows.Forms.TextBox _limitBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox6;
        private Forms.HexTextbox _addrHex;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton _bitfRadio;
        private System.Windows.Forms.RadioButton _wordfRadio;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox _countOfWord;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox _optionsGB;
        private System.Windows.Forms.GroupBox _hexGroup;
        private System.Windows.Forms.GroupBox _adductionGroup;
        private System.Windows.Forms.GroupBox _ledsGroup;
        private System.Windows.Forms.CheckBox _queryEnableCheckBox;


    }
}