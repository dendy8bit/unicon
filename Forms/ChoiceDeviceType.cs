﻿using System;
using System.Windows.Forms;

namespace BEMN.Forms
{
    public partial class ChoiceDeviceType : Form
    {
        private string[] outStrings;
        public ChoiceDeviceType(string[] list, string[] res)
        {
            this.InitializeComponent();
            this.comboBox1.DataSource = list;
            this.comboBox1.SelectedIndex = 0;

            this.outStrings = res;
        }

        public string DeviceType => this.outStrings[this.comboBox1.SelectedIndex];

        private void OK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
