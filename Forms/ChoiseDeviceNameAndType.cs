﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BEMN.MBServer;

namespace BEMN.Forms
{
    public partial class ChoiseDeviceNameAndType : Form
    {
        private readonly Type _deviceType;


        public string DeviceName => this._deviceNameCB.SelectedItem?.ToString();
        public string DeviceVersion => this._deviceVersionCB.SelectedItem?.ToString();
        public string DeviceConfig
        {
            get => this._deviceConfigCB.SelectedItem?.ToString();
            set => this._deviceConfigCB.Text = value;
        }


        public ChoiseDeviceNameAndType()
        {
            this.InitializeComponent();

            this._deviceType = null;

            this._deviceNameCB.Items.AddRange(DevicesName.ToArray());
            this._deviceVersionCB.Items.AddRange(Mr100Versions.ToArray());

            this.SelectedDefaultValue();
        }
        public ChoiseDeviceNameAndType(Type deviceType, string deviceName)
        {
            this.InitializeComponent();

            this._deviceType = deviceType;

            this._deviceNameCB.Items.Add(deviceName);
            this._deviceNameCB.SelectedIndex = 0;
        }
        

        private void SelectedDefaultValue()
        {
            this._deviceNameCB.SelectedIndex = 0;
            this._deviceVersionCB.SelectedIndex = 0;
            this._deviceConfigCB.Visible = false;
        }
        
        private void UpdateCB(ComboBox cb, List<string> list)
        {
            cb.Items.AddRange(list.ToArray());
        }

        private void Transform()
        {
            if (this.DeviceName == "MR902")
            {
                if (this.DeviceVersion.StartsWith("2"))
                {
                    if (this.DeviceVersion == "2.00 - 2.02")
                    {
                        return;
                    }
                    if (Common.VersionConverter(this.DeviceVersion) > 2.10 && Common.VersionConverter(this.DeviceVersion) < 3)
                    {
                        if (this.DeviceConfig == "T16N0D24R19(M3)")
                        {
                            this._deviceConfigCB.Items.Add(string.Empty);
                            this.DeviceConfig = string.Empty;
                        }
                        if (this.DeviceConfig == "T24N0D40R35(A2)")
                        {
                            this._deviceConfigCB.Items.Add("A2");
                            this.DeviceConfig = "A2";
                        }
                    }
                }

                if (this.DeviceVersion.StartsWith("3"))
                {
                    if (this.DeviceConfig == "T16N0D64R43")
                    {
                        this._deviceConfigCB.Items.Add("A1");
                        this.DeviceConfig = "A1";
                    }
                    if (this.DeviceConfig == "T24N0D40R35")
                    {
                        this._deviceConfigCB.Items.Add("A2");
                        this.DeviceConfig = "A2";
                    }
                    if (this.DeviceConfig == "T24N0D24R51")
                    {
                        this._deviceConfigCB.Items.Add("A3");
                        this.DeviceConfig = "A3";
                    }
                    if (this.DeviceConfig == "T24N0D32R43")
                    {
                        this._deviceConfigCB.Items.Add("A4");
                        this.DeviceConfig = "A4";
                    }
                    if (this.DeviceConfig == "T16N0D24R19")
                    {
                        this._deviceConfigCB.Items.Add(string.Empty);
                        this.DeviceConfig = string.Empty;
                    }
                }
            }
        }

        private void _okButton_Click(object sender, EventArgs e)
        {
            this.Transform();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void _deviceNameCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.InitVersion(this._deviceType);   // получаем версии по названию устройства / получаем версии из сборки (типа устройства)

            this.VisibleVersionAndApparatConfig();   // скрываем версии и аппаратную часть для некоторых устройств

            // если у устройства есть версия
            if (this._deviceVersionCB.Items.Count != 0)
            {
                this._deviceVersionCB.SelectedIndex = 0; // по умолчанию первая запись
            }
        }

        private void _deviceVersionCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.InitApparatConfig();                // получаем аппаратную часть 
            this.VisibleVersionAndApparatConfig();   // скрываем версии и аппаратную часть для некоторых устройств
            
            // если у устройства есть аппаратная часть
            if (this._deviceConfigCB.Items.Count != 0)
            {
                this._deviceConfigCB.SelectedIndex = 0; // по умолчанию первая запись
            }
        }

        private void VisibleVersionAndApparatConfig()
        {
            if (this._deviceVersionCB.Items.Count == 0)
            {
                this._versionLabel.Visible = false;
                this._deviceVersionCB.Visible = false;
                this._configLabel.Visible = false;
                this._deviceConfigCB.Visible = false;
                return;
            }

            if (this._deviceConfigCB.Items.Count == 0)
            {
                this._versionLabel.Visible = true;
                this._deviceVersionCB.Visible = true;
                this._configLabel.Visible = false;
                this._deviceConfigCB.Visible = false;
                return;
            }

            this._versionLabel.Visible = true;
            this._deviceVersionCB.Visible = true;
            this._configLabel.Visible = true;
            this._deviceConfigCB.Visible = true;
        }
        
        /// <summary>
        /// Заполняет комбобокс "Версия", очищает "Аппаратная часть" 
        /// </summary>
        private void InitVersion(Type deviceType)
        {
            // При изменении названия устройства будут заново заполнятся списки, поэтому очищаем их
            this._deviceVersionCB.Items.Clear();
            this._deviceConfigCB.Items.Clear();

            List<string> versions = new List<string>();

            // Получаем все версии выбранного устройства 
            switch (this._deviceNameCB.SelectedItem.ToString().ToUpper())
            {
                case "MR100":
                    versions.AddRange(this.Mr100Versions);
                    break;
                case "MR300":
                    break;
                case "MR301":
                    versions.AddRange(this.Mr301Versions);
                    break;
                case "MR5":
                    versions.AddRange(this.Mr5Versions);
                    break;
                case "MR500":
                    versions.AddRange(this.Mr500Versions);
                    break;
                case "MR550":
                    versions.AddRange(this.Mr550Versions);
                    break;
                case "MR600":
                    versions.AddRange(this.Mr600Versions);
                    break;
                case "MR700":
                    versions.AddRange(this.Mr700Versions);
                    break;
                case "MR730":
                case "MR731":
                    break;
                case "MR741":
                    versions.AddRange(this.Mr741Versions(deviceType));
                    break;
                case "MR750":
                    versions.AddRange(this.Mr750Versions);
                    break;
                case "MR761":
                    versions.AddRange(this.Mr761Versions(deviceType));
                    break;
                case "MR761OBR":
                    versions.AddRange(this.Mr761obrVersions);
                    break;
                case "MR762":
                    versions.AddRange(this.Mr762Versions);
                    break;
                case "MR763":
                    versions.AddRange(this.Mr763Versions);
                    break;
                case "MR771":
                    versions.AddRange(this.Mr771Versions(deviceType));
                    break;
                case "MR801":
                    versions.AddRange(this.Mr801Versions(deviceType));
                    break;
                case "MR801DVG":
                    versions.AddRange(this.Mr801dvgVersions);
                    break;
                case "MR851":
                    versions.AddRange(this.Mr851Versions);
                    break;
                case "MR901":
                    versions.AddRange(this.Mr901Versions(deviceType));
                    break;
                case "MR902":
                    versions.AddRange(this.Mr902Versions(deviceType));
                    break;
            }

            this.UpdateCB(this._deviceVersionCB, versions);
        }

        /// <summary>
        /// Заполняет комбобокс "Аппаратная часть" 
        /// </summary>
        private void InitApparatConfig()
        {
            this._deviceConfigCB.Items.Clear();

            string version = this._deviceVersionCB.Text;

            List<string> listConfig = new List<string>();

            switch (this._deviceNameCB.SelectedItem.ToString())
            {
                // не имеют аппаратной части
                case "MR100":
                case "MR300":
                case "MR301":
                case "MR5":
                case "MR500":
                case "MR550":
                case "MR600":
                case "MR700":
                case "MR730":
                case "MR731":
                case "MR750":
                case "MR763":
                case "MR801DGV":
                case "MR851":
                    break;

                // имеют аппаратную часть
                case "MR741":
                    listConfig.AddRange(this.Mr741ApparatConfig(version));
                    break;
                case "MR761":
                    listConfig.AddRange(this.Mr761ApparatConfig(version));
                    break;
                case "MR761OBR":
                    listConfig.AddRange(this.Mr761obrApparatConfig(version));
                    break;
                case "MR762":
                    listConfig.AddRange(this.Mr762ApparatConfig(version));
                    break;
                case "MR771":
                    listConfig.AddRange(this.Mr771ApparatConfig(version));
                    break;
                case "MR801":
                    listConfig.AddRange(this.Mr801ApparatConfig(version));
                    break;
                case "MR901":
                    listConfig.AddRange(this.Mr901ApparatConfig(version));
                    break;
                case "MR902":
                    listConfig.AddRange(this.Mr902ApparatConfig(version));
                    break;
            }

            this.UpdateCB(this._deviceConfigCB, listConfig);
        }


        List<string> DevicesName
        {
            get
            {
                return new List<string>
                {
                    "MR100",
                    "MR301",
                    "MR5",
                    "MR500",
                    "MR550",
                    "MR600",
                    "MR700",
                    "MR730",
                    "MR731",
                    "MR741",
                    "MR750",
                    "MR761",
                    "MR761OBR",
                    "MR762",
                    "MR763",
                    "MR771",
                    "MR801",
                    "MR801DVG",
                    "MR851",
                    "MR901",
                    "MR902"
                };
            }
        }


        #region [All Versions]
        public List<string> Mr100Versions
        {
            get
            {
                return new List<string>
                {
                    "5.20 и ниже",
                    "5.40 - 6.10",
                    "7.00"
                };
            }
        }


        public List<string> Mr301Versions
        {
            get
            {
                return new List<string>
                {
                    "До версии 2.15",
                    "2.16"
                };
            }
        }


        public List<string> Mr5Versions
        {
            get
            {
                return new List<string>
                {
                    "50.00",
                    "50.01",
                    "50.02",
                    "50.03",
                    "50.04",
                    "50.05",
                    "50.06",
                    "55.00",
                    "60.00",
                    "60.01",
                    "60.02",
                    "60.03",
                    "60.04",
                    "60.05",
                    "70.00",
                    "70.01",
                    "70.02",
                    "70.03",
                    "70.04",
                    "70.05",
                    "70.06",
                    "70.07",
                    "75.00",
                    "75.01",
                    "75.02",
                    "75.03",
                    "75.04"
                };
            }
        }


        public List<string> Mr500Versions
        {
            get
            {
                return new List<string>
                {
                    "1.00 - 2.03",
                    "2.04 - 2.08",
                    "3.00 - 3.07"
                };
            }
        }


        public List<string> Mr550Versions
        {
            get
            {
                return new List<string>
                {
                    "1.0",
                    "1.01",
                    "1.02",
                    "1.03"
                };
            }
        }


        public List<string> Mr600Versions
        {
            get
            {
                return new List<string>
                {
                    "до 2.03",
                    "с 2.03 по 2.11",
                    "3.00",
                    "3.01",
                    "3.02",
                    "3.03",
                    "3.04",
                    "3.05"
                };
            }
        }


        public List<string> Mr700Versions
        {
            get
            {
                return new List<string>
                {
                    "1.00",
                    "1.10",
                    "1.11",
                    "1.12",
                    "1.13",
                    "1.14",
                    "1.15",
                    "1.16",
                    "1.17",
                    "1.18",
                    "2.00",
                    "2.01",
                    "2.02",
                    "2.03",
                    "2.04",
                    "2.05",
                    "2.06",
                    "2.07"
                };
            }
        }


        public List<string> Mr741Versions(Type deviceType)
        {
            if (deviceType == null)
            {
                return new List<string>
                {
                    "1.00",
                    "1.10",
                    "1.11",
                    "1.12",
                    "1.13",
                    "1.14",
                    "1.15",
                    "1.16",
                    "1.17 - 1.24",
                    "2.00 - 2.10",
                    "2.00S - 2.10S",
                    "3.00",
                    "3.00S",
                    "3.01S",
                    "3.02S",
                    "3.03S",
                    "3.04S",
                    "3.05S",
                    "3.06S",
                    "3.07",
                    "3.09"
                };
            }

            switch (deviceType.Name)
            {
                case "TZL":
                    return new List<string>
                    {
                        "1.00",
                        "1.10",
                        "1.11",
                        "1.12",
                        "1.13",
                        "1.14",
                        "1.15",
                        "1.16",
                        "1.17 - 1.24",
                        "2.00 - 2.10",
                        "2.00S - 2.10S",
                        "3.00",
                        "3.00S",
                        "3.01S",
                        "3.02S",
                        "3.03S",
                        "3.04S",
                        "3.05S",
                        "3.06S",
                        "3.07"
                    };
                case "MRUniversal":
                    return new List<string>
                    {
                        "3.09"
                    };
            }

            return new List<string>();
        }


        public List<string> Mr750Versions
        {
            get
            {
                return new List<string>
                {
                    "1.00",
                    "1.01",
                    "1.02"
                };
            }
        }


        public List<string> Mr761Versions(Type deviceType)
        {
            if (deviceType == null)
            {
                return new List<string>
                {
                    "1.00 - 1.01",
                    "1.02 - 1.05",
                    "2.00",
                    "2.01",
                    "2.02",
                    "2.03",
                    "2.04",
                    "2.05",
                    "2.06",
                    "3.00",
                    "3.01",
                    "3.02",
                    "3.03",
                    "3.04",
                    "3.05",
                    "3.06",
                    "3.07",
                    "3.08",
                    "3.09"
                };
            }

            switch (deviceType.Name)
            {
                case "MR761":
                    return new List<string>
                    {
                        "1.00 - 1.01",
                        "1.02 - 1.05",
                        "2.00",
                        "2.01",
                        "2.02",
                        "2.03",
                        "2.04",
                        "2.05",
                        "2.06",
                        "3.00",
                        "3.01",
                        "3.02",
                        "3.03",
                        "3.04",
                        "3.05",
                        "3.06",
                        "3.07",
                        "3.08",
                    };
                case "MRUniversal":
                    return new List<string>
                    {
                        "3.09"
                    };
            }

            return new List<string>();
        }


        public List<string> Mr761obrVersions
        {
            get
            {
                return new List<string>
                {
                    "1.00",
                    "1.01",
                    "3.00"
                };
            }
        }


        public List<string> Mr762Versions
        {
            get
            {
                return new List<string>
                {
                    "1.00 - 1.01",
                    "1.02 - 1.05",
                    "2.00",
                    "2.01",
                    "2.02",
                    "2.03",
                    "2.04",
                    "2.05",
                    "2.06",
                    "3.00",
                    "3.01",
                    "3.02",
                    "3.03",
                    "3.04",
                    "3.05",
                    "3.06",
                    "3.07",
                    "3.08"
                };
            }
        }


        public List<string> Mr763Versions
        {
            get
            {
                return new List<string>
                {
                    "1.00 - 1.01",
                    "1.02 - 1.05",
                    "2.00",
                    "2.01",
                    "2.02",
                    "2.03",
                    "2.04",
                    "2.05",
                    "2.06",
                    "3.00",
                    "3.01",
                    "3.02",
                    "3.03",
                    "3.04",
                    "3.05",
                    "3.06",
                    "3.07",
                    "3.08"
                };
            }
        }


        public List<string> Mr771Versions(Type deviceType)
        {
            if (deviceType == null)
            {
                return new List<string>
                {
                    "1.02",
                    "1.03",
                    "1.04",
                    "1.05",
                    "1.06",
                    "1.07",
                    "1.08",
                    "1.09",
                    "1.10",
                    "1.11",
                    "1.12",
                    "1.14"
                };
            }

            switch (deviceType.Name)
            {
                case "Mr771":
                    return new List<string>
                    {
                        "1.02",
                        "1.03",
                        "1.04",
                        "1.05",
                        "1.06",
                        "1.07",
                        "1.08",
                        "1.09",
                        "1.10",
                        "1.11",
                        "1.12",
                    };
                case "MRUniversal":
                    return new List<string>
                    {
                        "1.14"
                    };
            }

            return new List<string>();
        }


        public List<string> Mr801Versions(Type deviceType)
        {
            if (deviceType == null)
            {
                return new List<string>
                {
                    "1.00",
                    "1.01",
                    "1.10",
                    "1.11",
                    "1.15",
                    "1.16",
                    "1.17",
                    "1.18",
                    "1.19",
                    "1.20",
                    "1.21",
                    "1.22",
                    "1.23",
                    "2.00",
                    "2.01",
                    "2.02",
                    "2.03",
                    "2.04",
                    "2.05",
                    "2.06",
                    "2.07",
                    "2.08",
                    "3.00",
                    "3.01",
                    "3.02"
                };
            }

            switch (deviceType.Name)
            {
                case "UDZT":
                    return new List<string>
                    {
                        "1.00",
                        "1.01",
                        "1.10",
                        "1.11",
                        "1.15",
                        "1.16",
                        "1.17",
                        "1.18",
                        "1.19",
                        "1.20",
                        "1.21",
                        "1.22",
                        "1.23",
                        "2.00",
                        "2.01",
                        "2.02",
                        "2.03",
                        "2.04",
                        "2.05",
                        "2.06",
                        "2.07",
                        "2.08"
                    };
                case "Mr7":
                    return new List<string>
                    {
                        "3.00",
                        "3.01",
                        "3.02"
                    };
            }

            return new List<string>();
        }


        public List<string> Mr801dvgVersions
        {
            get
            {
                return new List<string>
                {
                    "1.00",
                    "1.01"
                };
            }
        }


        public List<string> Mr851Versions
        {
            get
            {
                return new List<string>
                {
                    "1.00",
                    "1.01",
                    "1.02",
                    "1.03",
                    "1.04",
                    "1.05",
                    "1.06",
                    "1.08",
                    "2.00",
                    "2.01",
                    "2.02",
                    "2.03",
                    "2.04",
                    "2.05",
                    "2.06",
                    "2.07"
                };
            }
        }


        public List<string> Mr901Versions(Type deviceType)
        {
            if (deviceType == null)
            {
                return new List<string>
                {
                    "1.00 - 1.03",
                    "2.00 - 2.02",
                    "2.03",
                    "2.04",
                    "2.05",
                    "2.06",
                    "2.10",
                    "2.11",
                    "2.12",
                    "2.13",
                    "3.00"
                };
            }

            switch (deviceType.Name)
            {
                case "Mr901Device":
                    return new List<string>
                    {
                        "1.00 - 1.03",
                        "2.00 - 2.02",
                        "2.03",
                        "2.04",
                        "2.05",
                        "2.06",
                        "2.10",
                        "2.11",
                        "2.12",
                        "2.13",
                    };
                case "MR901New":
                    return new List<string>
                    {
                        "3.00"
                    };
            }

            return new List<string>();
        }


        public List<string> Mr902Versions(Type deviceType)
        {
            if (deviceType == null)
            {
                return new List<string>
                {
                    "1.00 - 1.03",
                    "2.00 - 2.02",
                    "2.03",
                    "2.04",
                    "2.05",
                    "2.06",
                    "2.10",
                    "2.13",
                    "3.00"
                };
            }

            switch (deviceType.Name)
            {
                case "Mr902":
                    return new List<string>
                    {
                        "1.00 - 1.03",
                        "2.00 - 2.02",
                        "2.03",
                        "2.04",
                        "2.05",
                        "2.06",
                        "2.10",
                        "2.13",
                    };
                case "Mr902New":
                    return new List<string>
                    {
                        "3.00"
                    };
            }

            return new List<string>();
        }
        #endregion [All Versions]


        #region [All ApparatConfig]
        public List<string> Mr801ApparatConfig(string version)
        {
            if (Common.VersionConverter(version) >= 3)
            {
                return new List<string>
                {
                    "T12N5D58R51",
                    "T12N6D58R51",
                    "T9N8D58R51",
                    "T6N3D42R35",
                    "T12N4D26R19"
                };
            }

            return new List<string>();
        }


        public List<string> Mr741ApparatConfig(string version)
        {
            if (version.StartsWith("3"))
            {
                if (Common.VersionConverter(version) >= 3.09)
                {
                    return new List<string>
                    {
                        "T4N4D18R16"
                    };
                }               
            }

            return new List<string>();
        }


        public List<string> Mr761ApparatConfig(string version)
        {
            if (version.StartsWith("3"))
            {
                if (Common.VersionConverter(version) >= 3.03 && Common.VersionConverter(version) <= 3.08)
                {
                    return new List<string>
                    {
                        "T4N5D42R35",
                        "T4N4D42R35"
                    };
                }
                if (Common.VersionConverter(version) >= 3.09)
                {
                    return new List<string>
                    {
                        "T4N4D42R35",
                        "T4N4D74R67",
                        "T4N5D42R35",
                        "T4N5D74R67"
                    };
                }
            }

            return new List<string>();
        }


        public List<string> Mr761obrApparatConfig(string version)
        {
            if (Common.VersionConverter(this._deviceVersionCB.Text) >= 3.00)
            {
                return new List<string>
                {
                    "T0N0D74R35",
                    "T0N0D106R67",
                    "T0N0D114R59"
                };
            }

            return new List<string>();
        }


        public List<string> Mr762ApparatConfig(string version)
        {
            if (version.StartsWith("3"))
            {
                if (Common.VersionConverter(version) >= 3.07 && Common.VersionConverter(version) <= 3.08)
                {
                    return new List<string>
                    {
                        "T5N3D42R35",
                        "T5N4D42R35"
                    };
                }
                if (Common.VersionConverter(version) >= 3.09)
                {
                    return new List<string>
                    {
                        "T4N4D42R35",
                        "T4N4D74R67",
                        "T4N5D42R35",
                        "T4N5D74R67"
                    };
                }
            }

            return new List<string>();
        }


        public List<string> Mr771ApparatConfig(string version)
        {
            if (Common.VersionConverter(version) >= 1.14)
            {
                return new List<string>
                {
                    "T4N5D42R35",
                    "T4N5D74R67",
                };
            }

            return new List<string>();
        }


        public List<string> Mr901ApparatConfig(string version)
        {
            if (version.StartsWith("2"))
            {
                if (version == "2.00 - 2.02")
                {
                    return new List<string>();
                } 
                if (Common.VersionConverter(version) >= 2.10)
                {
                    return new List<string>
                    {
                        "T16N0D64R43",
                        "T24N0D40R35",
                        "T24N0D24R51",
                        "T24N0D32R43",
                        "T20N4D40R35",
                        "T16N0D24R19"
                    };
                }
            }
            if (version.StartsWith("3"))
            {
                return new List<string>
                {
                    "T16N0D64R43",
                    "T24N0D40R35",
                    "T24N0D24R51",
                    "T24N0D32R43",
                    "T20N4D40R35",
                    "T20N4D32R43",
                    "T16N0D24R19"
                };
            }

            return new List<string>();
        }


        public List<string> Mr902ApparatConfig(string version)
        {
            if (version.StartsWith("2"))
            {
                if (version == "2.00 - 2.02")
                {
                    return new List<string>();
                }
                if (Common.VersionConverter(version) >= 2.10)
                {
                    return new List<string>
                    {
                        "T24N0D40R35(A2)",
                        "T16N0D24R19(М3)"
                    };
                }
            }
            if (version.StartsWith("3"))
            {
                return new List<string>
                {
                    "T16N0D64R43",
                    "T24N0D40R35",
                    "T24N0D24R51",
                    "T24N0D32R43",
                    "T20N4D40R35",
                    "T20N4D32R43",
                    "T16N0D24R19"
                };
            }

            return new List<string>();
        }
        #endregion [All ApparatConfig]
    }
}
