﻿using System;
using BEMN.Devices;
using BEMN.MBServer;

namespace BEMN.Forms.DisplayInfoDictionary
{
    public class DisplayInfoDictionary
    {
        public event Handler DisplayInfoLoadOk;
        public event Handler DisplayInfoLoadFail;

        public string DisplayInfo { get; private set; }

        private bool _isAz;
        public DisplayInfoDictionary(bool isAz)
        {
            this._isAz = isAz;
        }

        public void GetDisplayInfo(ushort[] buffer)
        {
            byte[] byteBuf = Common.TOBYTES(buffer, true);
            char[] charBuf = new char[byteBuf.Length];
            char[] chars = new char[byteBuf.Length];

            try
            {
                for (int i = 0; i < charBuf.Length; i++)
                {
                    charBuf[i] = DecodeSymbols(byteBuf[i]);
                }
                chars = Common.SwapArrayItems(charBuf);
            }
            catch (ArgumentNullException)
            {
                throw new ApplicationException("Передан нулевой буфер");
            }

            try
            {
                DisplayInfo = new string(chars);
                DisplayInfoLoadOk?.Invoke(this);
            }
            catch (ArgumentOutOfRangeException)
            {
                DisplayInfoLoadFail?.Invoke(this);
            }
        }
        
        private char DecodeSymbols(byte lit)
        {
            if (this._isAz)
            {
                switch (lit)
                {
                    case 0x00: return 'Ç';
                    case 0x01: return 'Ş';
                    case 0x02: return 'Ə';
                    case 0x03: return 'Ö';
                    case 0x04: return 'Ü';
                    case 0x05: return 'Ğ';
                    case 0x06: return 'İ';
                    case 0x07: return 'ş';
                    case 0x0B: return 'ö';
                    case 0x0C: return 'ü';
                    case 0x0D: return 'ğ';
                    case 0xC5: return 'ə';
                    case 0xD1: return 'ı';
                    case 0xEB: return 'ç';
                    case 0x021: return '!';
                    case 0x022: return '"'; //	'"'
                    case 0x023: return '#'; //	'#'
                    case 0x024: return '$'; //	'$'
                    case 0x025: return '%'; //	'%'
                    case 0x026: return '&'; //	'&'
                    case 0x027: return '\'';    //	'''
                    case 0x028: return '('; //	'('
                    case 0x029: return ')'; //	')'
                    case 0x02A: return '*'; //	'*'
                    case 0x02B: return '+'; //	'+'
                    case 0x02C: return ','; //	','
                    case 0x02D: return '-'; //	'-'
                    case 0x02E: return '.'; //	'.'
                    case 0x02F: return '/'; //	'/'
                    case 0x030: return '0'; //	'0'
                    case 0x031: return '1'; //	'1'
                    case 0x032: return '2'; //	'2'
                    case 0x033: return '3'; //	'3'
                    case 0x034: return '4'; //	'4'
                    case 0x035: return '5'; //	'5'
                    case 0x036: return '6'; //	'6'
                    case 0x037: return '7'; //	'7'
                    case 0x038: return '8'; //	'8'
                    case 0x039: return '9'; //	'9'
                    case 0x03A: return ':'; //	':'
                    case 0x03B: return ';'; //	';'
                    case 0x03C: return '<'; //	'<'
                    case 0x03D: return '='; //	'='
                    case 0x03E: return '>'; //	'>'
                    case 0x03F: return '?'; //	'?'
                    case 0x040: return '@'; //	'@'
                    case 0x041: return 'A'; //	'A'
                    case 0x042: return 'B'; //	'B'
                    case 0x043: return 'C'; //	'C'
                    case 0x044: return 'D'; //	'D'
                    case 0x045: return 'E'; //	'E'
                    case 0x046: return 'F'; //	'F'
                    case 0x047: return 'G'; //	'G'
                    case 0x048: return 'H'; //	'H'
                    case 0x049: return 'I'; //	'I'
                    case 0x04A: return 'J'; //	'J'
                    case 0x04B: return 'K'; //	'K'
                    case 0x04C: return 'L'; //	'L'
                    case 0x04D: return 'M'; //	'M'
                    case 0x04E: return 'N'; //	'N'
                    case 0x04F: return 'O'; //	'O'
                    case 0x050: return 'P'; //	'P'
                    case 0x051: return 'Q'; //	'Q'
                    case 0x052: return 'R'; //	'R'
                    case 0x053: return 'S'; //	'S'
                    case 0x054: return 'T'; //	'T'
                    case 0x055: return 'U'; //	'U'
                    case 0x056: return 'V'; //	'V'
                    case 0x057: return 'W'; //	'W'
                    case 0x058: return 'X'; //	'X'
                    case 0x059: return 'Y'; //	'Y'
                    case 0x05A: return 'Z'; //	'Z'
                    case 0x05B: return '['; //	'['
                    case 0x05C: return ' '; //	'\'
                    case 0x05D: return ']'; //	']'
                    case 0x05E: return '^'; //	'^'
                    case 0x05F: return '_'; //	'_'
                    case 0x060: return '`'; //	'`'
                    case 0x061: return 'a'; //	'a'
                    case 0x062: return 'b'; //	'b'
                    case 0x063: return 'c'; //	'c'
                    case 0x064: return 'd'; //	'd'
                    case 0x065: return 'e'; //	'e'
                    case 0x066: return 'f'; //	'f'
                    case 0x067: return 'g'; //	'g'
                    case 0x068: return 'h'; //	'h'
                    case 0x069: return 'i'; //	'i'
                    case 0x06A: return 'j'; //	'j'
                    case 0x06B: return 'k'; //	'k'
                    case 0x06C: return 'l'; //	'l'
                    case 0x06D: return 'm'; //	'm'
                    case 0x06E: return 'n'; //	'n'
                    case 0x06F: return 'o'; //	'o'
                    case 0x070: return 'p'; //	'p'
                    case 0x071: return 'q'; //	'q'
                    case 0x072: return 'r'; //	'r'
                    case 0x073: return 's'; //	's'
                    case 0x074: return 't'; //	't'
                    case 0x075: return 'u'; //	'u'
                    case 0x076: return 'v'; //	'v'
                    case 0x077: return 'w'; //	'w'
                    case 0x078: return 'x'; //	'x'
                    case 0x079: return 'y'; //	'y'
                    case 0x07A: return 'z'; //	'z'
                    //////////
                    //	case 0x0c5: return 'э';	//	'U'
                    default: return ' ';
                }
            }
            switch (lit)
            {
                case 0x021: return '!';
                case 0x022: return '"'; //	'"'
                case 0x023: return '#'; //	'#'
                case 0x024: return '$'; //	'$'
                case 0x025: return '%'; //	'%'
                case 0x026: return '&'; //	'&'
                case 0x027: return (char)0x27;    //	'''
                case 0x028: return '('; //	'('
                case 0x029: return ')'; //	')'
                case 0x02A: return '*'; //	'*'
                case 0x02B: return '+'; //	'+'
                case 0x02C: return ','; //	','
                case 0x02D: return '-'; //	'-'
                case 0x02E: return '.'; //	'.'
                case 0x02F: return '/'; //	'/'
                case 0x030: return '0'; //	'0'
                case 0x031: return '1'; //	'1'
                case 0x032: return '2'; //	'2'
                case 0x033: return '3'; //	'3'
                case 0x034: return '4'; //	'4'
                case 0x035: return '5'; //	'5'
                case 0x036: return '6'; //	'6'
                case 0x037: return '7'; //	'7'
                case 0x038: return '8'; //	'8'
                case 0x039: return '9'; //	'9'
                case 0x03A: return ':'; //	':'
                case 0x03B: return ';'; //	';'
                case 0x03C: return '<'; //	'<'
                case 0x03D: return '='; //	'='
                case 0x03E: return '>'; //	'>'
                case 0x03F: return '?'; //	'?'
                case 0x040: return '@'; //	'@'
                case 0x041: return 'A'; //	'A'
                case 0x042: return 'B'; //	'B'
                case 0x043: return 'C'; //	'C'
                case 0x044: return 'D'; //	'D'
                case 0x045: return 'E'; //	'E'
                case 0x046: return 'F'; //	'F'
                case 0x047: return 'G'; //	'G'
                case 0x048: return 'H'; //	'H'
                case 0x049: return 'I'; //	'I'
                case 0x04A: return 'J'; //	'J'
                case 0x04B: return 'K'; //	'K'
                case 0x04C: return 'L'; //	'L'
                case 0x04D: return 'M'; //	'M'
                case 0x04E: return 'N'; //	'N'
                case 0x04F: return 'O'; //	'O'
                case 0x050: return 'P'; //	'P'
                case 0x051: return 'Q'; //	'Q'
                case 0x052: return 'R'; //	'R'
                case 0x053: return 'S'; //	'S'
                case 0x054: return 'T'; //	'T'
                case 0x055: return 'U'; //	'U'
                case 0x056: return 'V'; //	'V'
                case 0x057: return 'W'; //	'W'
                case 0x058: return 'X'; //	'X'
                case 0x059: return 'Y'; //	'Y'
                case 0x05A: return 'Z'; //	'Z'
                case 0x05B: return '['; //	'['
                case 0x05C: return ' '; //	'\'
                case 0x05D: return ']'; //	']'
                case 0x05E: return '^'; //	'^'
                case 0x05F: return '_'; //	'_'
                case 0x060: return '`'; //	'`'
                case 0x061: return 'a'; //	'a'
                case 0x062: return 'b'; //	'b'
                case 0x063: return 'c'; //	'c'
                case 0x064: return 'd'; //	'd'
                case 0x065: return 'e'; //	'e'
                case 0x066: return 'f'; //	'f'
                case 0x067: return 'g'; //	'g'
                case 0x068: return 'h'; //	'h'
                case 0x069: return 'i'; //	'i'
                case 0x06A: return 'j'; //	'j'
                case 0x06B: return 'k'; //	'k'
                case 0x06C: return 'l'; //	'l'
                case 0x06D: return 'm'; //	'm'
                case 0x06E: return 'n'; //	'n'
                case 0x06F: return 'o'; //	'o'
                case 0x070: return 'p'; //	'p'
                case 0x071: return 'q'; //	'q'
                case 0x072: return 'r'; //	'r'
                case 0x073: return 's'; //	's'
                case 0x074: return 't'; //	't'
                case 0x075: return 'u'; //	'u'
                case 0x076: return 'v'; //	'v'
                case 0x077: return 'w'; //	'w'
                case 0x078: return 'x'; //	'x'
                case 0x079: return 'y'; //	'y'
                case 0x07A: return 'z'; //	'z'
                //////////
                case 0x0a0: return 'Б'; //	'0'
                case 0x0a1: return 'Г'; //	'1'
                case 0x0a2: return 'ё'; //	'2'
                case 0x0a3: return 'Ж'; //	'3'
                case 0x0a4: return 'З'; //	'4'
                case 0x0a5: return 'И'; //	'5'
                case 0x0a6: return 'Й'; //	'6'
                case 0x0a7: return 'Л'; //	'7'
                case 0x0a8: return 'П'; //	'8'
                case 0x0a9: return 'У'; //	'9'
                case 0x0aA: return 'Ф'; //	':'
                case 0x0aB: return 'Ч'; //	';'
                case 0x0aC: return 'Ш'; //	'<'
                case 0x0aD: return 'Ъ'; //	'='
                case 0x0aE: return 'Ы'; //	'>'
                case 0x0aF: return 'Э'; //	'?'
                case 0x0b0: return 'Ю'; //	'@'
                case 0x0b1: return 'Я'; //	'A'
                case 0x0b2: return 'б'; //	'B'
                case 0x0b3: return 'в'; //	'C'
                case 0x0b4: return 'г'; //	'D'
                case 0x0b5: return 'Ё'; //	'E'
                case 0x0b6: return 'ж'; //	'F'
                case 0x0b7: return 'з'; //	'G'
                case 0x0b8: return 'и'; //	'H'
                case 0x0b9: return 'й'; //	'I'
                case 0x0bA: return 'к'; //	'J'
                case 0x0bB: return 'л'; //	'K'
                case 0x0bC: return 'м'; //	'L'
                case 0x0bD: return 'н'; //	'M'
                case 0x0bE: return 'п'; //	'N'
                case 0x0bF: return 'т'; //	'O'
                case 0x0c0: return 'ч'; //	'P'
                case 0x0c1: return 'ш'; //	'Q'
                case 0x0c2: return 'ъ'; //	'R'
                case 0x0c3: return 'ы'; //	'S'
                case 0x0c4: return 'ь'; //	'T'
                case 0x0c5: return 'э'; //	'U'
                case 0x0c6: return 'ю'; //	'V'
                case 0x0c7: return 'я'; //	'W'
                case 0x0e0: return 'Д'; //	'P'
                case 0x0e1: return 'Ц'; //	'Q'
                case 0x0e2: return 'Щ'; //	'R'
                case 0x0e3: return 'д'; //	'S'
                case 0x0e4: return 'ф'; //	'T'
                case 0x0e5: return 'ц'; //	'U'
                case 0x0e6: return 'щ'; //	'V'
                default: return ' ';
            }
        }
    }
}
