﻿using System;
using BEMN.MBServer;

namespace BEMN.Forms.MeasuringClasses
{
    public static class ValuesConverterCommon
    {
        /// <summary>
        /// Методы для рассчёта значений аналоговых сигналов
        /// </summary>
        public static class Analog
        {

            public static string ShowPercent(int value)
            {
                string param = "%Iб";
                if (value < 33)
                {
                    return 0 + " " + param;
                }

                double percent = value * 100 * 40.0 / 65536.0;
                //var percent1 = percent.ToString("#.###");
                //var persent2 = percent1.Remove(percent1.Length - 1);
                //return persent2 + " " + param;
                return Math.Round(percent, 2) + " " + param;
            }

            public static string GetQt(ushort value)
            {
                return DoubleToString3((short) value/256.0)+ "%";
            }

            public static string GetQ(ushort value)
            {
                return DoubleToString3(value / 256.0) + "%";
            }

            public static string GetCosF(ushort value)
            {
                return DoubleToString3((short) value/256.0) ;
            }

            public static string GetCosF801(int value)
            {
                return DoubleToString3((short) value / 256.0);
            }

            private static string GetPower(double value, double koef)
            {
                return DoubleToString3(value * 40 * 256 * koef / 0x20000000);
            }

            public static string GetQ(int value, double koef)
            {
                return GetPower(value, koef) + "вар";
            }

            public static string GetP(int value, double koef)
            {
                return GetPower(value, koef) + "Вт";
            }

            public static string GetFullPower(short p, short q, double ktn, double ittl)
            {
                double P = p * ktn * ittl * 10 / 256;
                double Q = q * ktn * ittl * 10 / 256;

                if (p == 32767 && q == 32767)
                {
                    return "------ , ------";
                }

                if (p == 32767)
                {
                    return $"------ , { DoubleToString3(Q) }Вар";
                }

                if (q == 32767)
                {
                    return $"{ DoubleToString3(P) } Вт, ------";
                }
                
                return $"{ DoubleToString3(P) } Вт, { DoubleToString3(Q) }Вар";
            }

            public static string GetP(double value, double koef)
            {
                return DoubleToString3(value * 40 * 256 * koef / 0x20000000) +"Вт";
            }

            public static string GetQMr750(int value, double koef)
            {
                return GetPowerMr750(value, koef) + "вар";

            }
            public static string GetQMr575(int value, double koef)
            {
                return GetPowerMr575(value, koef) + "вар";

            }
            public static string GetPMr750(int value, double koef)
            {
                return GetPowerMr750(value, koef) + "Вт";
            }
            public static string GetPAJMr575(int value, double koef)
            {
                return GetPowerMr750(value, koef) + "Рн";
            }
            public static string GetPMr575(int value, double koef)
            {
                return GetPowerMr575(value, koef) + "Вт";
            }

            public static string GetF(ushort value)
            {
                return DoubleToString4(value/256.0) + "Гц";
            }


            public static string GetF801(int value)
            {
                return DoubleToString4(value / 256.0) + "Гц";
            }

            public static string GetSignF(short value)
            {
                return DoubleToString3(value / 256.0) + "Гц";
            }

            public static string GetF(short value)
            {
                return DoubleToString3(value / 256.0) + "Гц";
            }

            public static string GetI(ushort value, double koef)
            {
                double res = Math.Floor(value * koef / 256.0);
                return DoubleToString3(res / 256) + "А";
            }

            public static string GetIBig(int value, double koef)
            {
                double res = Math.Floor(value * koef / 256.0);
                return DoubleToString3(res / 256) + "А";
            }

            public static string GetI901(ushort value, double koef)
            {
                double res = Math.Floor(value * koef / 256.0);
                return DoubleToString3(res / 128) + "А";
            }

            public static string GetI801(int value, double koef)
            {
                double res = Math.Floor(value * koef / 256.0);
                return DoubleToString3(res / 256) + "А";
            }

            public static string GetI801Diff(int value, double s, double u)
            {
                if (value < 29 && u == 0)
                {
                    return 0 + "A";
                }
                double valueOne = 40.0 * value / 65536;
                double valueTwo = s / (Math.Sqrt(3) * u) * 1000;
                double res = valueOne * valueTwo;
                return DoubleToString3(res) + "А";
            }

            public static string GetISmall(ushort value, double koef)
            {
                double res = Math.Ceiling(value * koef / 256);
                res = res / 256;
                return DoubleToString3(res) + "A";
            }


            public static string GetU(ushort value, double koef)
            {
                double res = Math.Floor(value * koef);
                return DoubleToString3(res / 256) + "В";
            }

            public static string GetUBig(int value, double koef)
            {
                double res = Math.Floor(value * koef);
                return DoubleToString3(res / 256) + "В";
            }

            public static string GetU801(int value, double koef)
            {
                double res = Math.Floor(value * koef);
                return DoubleToString3(res / 256) + "В";
            }

            public static string GetUdouble(double value)
            {
                return DoubleToString3(value / 256.0) + "В";
            }

            
            private static string GetPowerMr750(int value, double koef)
            {
                return DoubleToString3(value * koef / 0x10000);
            }

            private static string GetPowerMr575(int value, double koef)
            {
                return DoubleToString3(value * koef);
            }

            public static string DoubleToString3(double value)
            {
                if (Math.Abs(value )> 1000000)
                {
                    return string.Format("{0} М",Common.NumberToThreeChar(value/1000000)) ;
                }
                if (Math.Abs(value )> 1000)
                {
                    return string.Format("{0} к", Common.NumberToThreeChar(value / 1000));
                }
                return Common.NumberToThreeChar(value) + " ";
            }

            public static string DoubleToString4(double value)
            {
                if (value > 1000000)
                {
                    return string.Format("{0} М", Common.NumberToFourChar(value / 1000000));
                }
                if (value > 1000)
                {
                    return string.Format("{0} к", Common.NumberToFourChar(value / 1000));
                }
                return Common.NumberToFourChar(value) + " ";
            }

            public static string GetIrzt(ushort value, double tt, double koefNom)
            {
                double res = Math.Floor((short)value * tt * koefNom / 256);
                return DoubleToString3(res /256) + "А";
            }

            public static string GetUrzt(ushort value, double koef)
            {
                return DoubleToString3((uint)(value * koef) * 400.0 / 65536.0) + "В";
            }

            public static string GetZ(double r, double x, double ktn, double ittl)
            {
                double zR = Math.Floor(256.0 * r / 5 * ktn / ittl)/256;
                double zX = Math.Floor(256.0 * x / 5 * ktn / ittl)/256;
                
                string znak = zX >= 0 ? "+" : "-";
                zX = Math.Abs(zX);
                if (zR > 1000000 || zX > 1000000)
                {
                    return string.Format("{0} {1}j{2} МОм", Common.NumberToThreeChar(zR/1000000), znak,
                        Common.NumberToThreeChar(zX/1000000));
                }
                if (zR > 1000 || zX > 1000)
                {
                    return string.Format("{0} {1}j{2} кОм", Common.NumberToThreeChar(zR/1000), znak,
                        Common.NumberToThreeChar(zX/1000));
                }
                return string.Format("{0} {1}j{2} Ом", Common.NumberToThreeChar(zR), znak,
                        Common.NumberToThreeChar(zX));
            }

            public static string GetZint(double r, double x, double ktn, double ittl)
            {
                double zR = Math.Floor(256.0 * r / 10240 * ktn / ittl) / 256;
                double zX = Math.Floor(256.0 * x / 10240 * ktn / ittl) / 256;

                string znak = zX >= 0 ? "+" : "-";
                zX = Math.Abs(zX);
                if (zR > 1000000 || zX > 1000000)
                {
                    return string.Format("{0} {1}j{2} МОм", Common.NumberToThreeChar(zR / 1000000), znak,
                        Common.NumberToThreeChar(zX / 1000000));
                }
                if (zR > 1000 || zX > 1000)
                {
                    return string.Format("{0} {1}j{2} кОм", Common.NumberToThreeChar(zR / 1000), znak,
                        Common.NumberToThreeChar(zX / 1000));
                }
                return string.Format("{0} {1}j{2} Ом", Common.NumberToThreeChar(zR), znak,
                    Common.NumberToThreeChar(zX));
            }

            public static string GetZ801(double r, double x, double ktn, double ittl)
            {
                double zR = Math.Floor(256.0 * r / 10240 * ktn / ittl) / 256;
                double zX = Math.Floor(256.0 * x / 10240 * ktn / ittl) / 256;

                string znak = zX >= 0 ? "+" : "-";
                zX = Math.Abs(zX);
                if (zR > 1000000 || zX > 1000000)
                {
                    return string.Format("{0} {1}j{2} МОм", Common.NumberToThreeChar(zR / 1000000), znak,
                        Common.NumberToThreeChar(zX / 1000000));
                }
                if (zR > 1000 || zX > 1000)
                {
                    return string.Format("{0} {1}j{2} кОм", Common.NumberToThreeChar(zR / 1000), znak,
                        Common.NumberToThreeChar(zX / 1000));
                }
                return string.Format("{0} {1}j{2} Ом", Common.NumberToThreeChar(zR), znak,
                    Common.NumberToThreeChar(zX));
            }

            public static string GetResist(short param, double koeftt, double koeftn)
            {
                if (param == short.MaxValue || param == short.MinValue) return "-----";

                double res = Math.Floor(256.0 * param / 5 * koeftn / koeftt);
                return DoubleToString3(res/256)+"Ом";
            }

            public static string GetOmp(ushort value)
            {
                return Math.Round(value / 256.0, 2) + " км";
            }
        }

        public static double GetUstavka2(ushort value)
        {
            double result = value / (double)0xffff * 2.0;
            return Math.Round(result, 4);
        }

        public static ushort SetUstavka2(double value)
        {
            if (Math.Abs(value - 2) < 0.00001)
            {
                return ushort.MaxValue;
            }

            return (ushort)(value / 2.0 * 0xffff);
        }

        public static double GetPersent(ushort value)
        {
            return Math.Round(value / 256.0, 2);
        }

        public static ushort SetPersent(double value)
        {
            if (Math.Abs(value - 100) < 0.01)
            {
                return ushort.MaxValue;
            }
            return (ushort)(value * 256);
        }
 
        public static double GetU(ushort value)
        {
            return Math.Round(value / 256.0, 2);
        }

        public static ushort SetU(double value)
        {
            if (Math.Abs(value - 256) < 0.01)
            {
                return ushort.MaxValue;
            }
            return (ushort)(value * 256);
        }

    
        public static double GetUstavka256(ushort value)
        {
            return Math.Round(value/256.0, 2);
        }

        public static double GetUstavka256MR801(ushort value)
        {
            if (value == 12)
            {
                value = 13;
            }
            return Math.Round(value / 256.0, 2);
        }

        public static ushort SetUstavka256(double value)
        {
            if (Math.Abs(value - 256) < 0.0001)
            {
                return ushort.MaxValue;
            }
            return (ushort) (value*256);
        }

        public static ushort SetUstavka256FromDfDf(double value)
        {
            if (value == 0.05)
            {
                value = 0.05078125;
            }
            if (Math.Abs(value - 256) < 0.0001)
            {
                return ushort.MaxValue;
            }
            return (ushort)(value * 256);
        }

        public static ushort SetAplituda(double value)
        {
            return (ushort)Math.Round(value *327.675);
        }
        public static double GetAplituda(ushort value)
        {
            return Math.Round(value / 327.675,2);
        }

        public static double GetKth(ushort value)
        {
            value = Common.SetBit(value, 15, false);
            return Math.Round(value / 256.0, 2);
        }

        public static ushort SetKth(double value)
        {
            if (Math.Abs(value - 128) < 0.0001)
            {
                return 32767;
            }
            return (ushort)(value * 256);
        }

        public static ushort SetKthRound(double value)
        {
            if (Math.Abs(value - 128) < 0.0001)
            {
                return 32767;
            }
            return (ushort)Math.Round(value * 256);
        }

        public static double GetUstavka100(ushort value)
        {
            double result = value/(double) 0xffff*100.0;
            return Math.Round(result, 2);
        }

        public static ushort SetUstavka100(double value)
        {
            if (Math.Abs(value - 100) < 0.0001)
            {
                return ushort.MaxValue;
            }

            return (ushort) (value/100.0*0xffff);
        }

        public static double GetUstavka5(ushort value)
        {
            double result = value / (double)0xffff * 5.0;
            return Math.Round(result, 2);
        }

        public static ushort SetUstavka5(double value)
        {
            if (Math.Abs(value - 5) < 0.0001)
            {
                return ushort.MaxValue;
            }

            return (ushort)(value / 5.0 * 0xffff);
        }

        /// <summary>
        /// Преобразует задержку времени из формата устройства в обычный
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static int GetWaitTime(ushort time)
        {
            int factor = Common.GetBit(time, 15) ? 100 : 10;
            ushort res = Common.SetBit(time, 15, false);
            return factor * res;
        }
        /// <summary>
        /// Преобразует задержку времени из обычного в формат устройства 
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static ushort SetWaitTime(int time)
        {
            ushort result;
            bool factor = time >= 300000;
            if (factor)
            {
                result = (ushort)(time / (double)100);
                result = Common.SetBit(result, 15, true);
            }
            else
            {
                result = (ushort)(time / (double)10);
            }
            return result;
        }

        public static double GetIn(ushort i)
        {
            uint temp = (uint) (i*0x2800);
            if (temp>= 0x8000)
            {
                temp += 0x10000;
            }
            temp = temp >> 16;
            double result = temp/256.0;
            return Math.Truncate(result*100)/100;
        }
        
        public static ushort SetIn(double i)
        {
            if (Math.Abs(i - 40.0) < 0.0001)
            {
                return ushort.MaxValue;
            }
            int temp = (int) (i*256);
            temp *= 0x33333;
            if (temp >= 0x4000)
            {
                temp += 0x8000;
            }
            temp = temp >> 15;
            if (temp >0xffff)
            {
                temp = 0xffff;
            }
            return (ushort) temp;
        }

        public static double GetFactor(ushort factor)
        {
            int value = Common.SetBit(factor, 15, false);
            if (Common.GetBit(factor, 15))
            {
                int k = 1000;
                return Math.Round(value  / (double)256,2)* k;
            }
            else
            {
                return Math.Round(value  / (double)256, 2);
            }
        }

        public static ushort SetFactor(double factor)
        {
            if (factor == 128000)
            {
                return ushort.MaxValue;
            }
            if (factor > 128)
            {
                ushort result = (ushort)(factor * 256 / (double)1000);

                return Common.SetBit(result, 15, true);
            }
            else
            {
                return (ushort)(factor * 256);
            }
        }

        public static double GetUstavka40(ushort value)
        {
            double result = value / (double)0xffff * 40.0;
            return Math.Round(result, 2);
        }
        public static ushort SetUstavka40(double value)
        {
            if (Math.Abs(value - 40) < 0.01)
            {
                return ushort.MaxValue;
            }
            return (ushort)(value / 40 * 0xffff);
        }
        public static double GetUstavka8(ushort value)
        {
            double result = value / (double)0xffff * 8.0;
            return Math.Round(result, 2);
        }
        public static ushort SetUstavka8(double value)
        {
            if (Math.Abs(value - 8) < 0.01)
            {
                return ushort.MaxValue;
            }
            return (ushort)(value / 8 * 0xffff);
        }

        public static double GetKthFull(ushort value)
        {
            int koef = Common.GetBit(value, 15) ? 1000 : 1;
            value = Common.SetBit(value, 15, false);
            return Math.Round(value  / 256.0, 2)* koef;
        }

        public static ushort SetKthFull(double value)
        {
            if (Math.Abs(value - 128000) < 0.01)
            {
                return ushort.MaxValue;
            }
            if (value <= 128)
            {
                return (ushort)(value * 256);
            }
            else
            {
                value = value / 1000;
                ushort res = (ushort)(value * 256);
                return Common.SetBit(res, 15, true);
            }
        }
  
    }
}
