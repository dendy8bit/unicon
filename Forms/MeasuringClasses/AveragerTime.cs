﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

namespace BEMN.Forms.MeasuringClasses
{
    public class AveragerTime<T>
    {
        private const string ERROR_MESSAGE = "Ошибка усреднения измеренных величин\n";

        private const string INFO_ERR =
            "Невозможно расситать усредненные значения измеряемых величин. Проверьте правильность подключенного устройства. Текущее окно измерений будет закрыто.";
        private int MAX_COUNT_ERR = 3;

        private readonly List<T> _list;
        private readonly Stopwatch _stopwatch;
        private readonly int _timeout;
        private bool _start;
        private Form _form;
        private int _counter;
        
        public event Action Tick;

        public List<T> ValueList
        {
            get { return this._list; }
        }

        public AveragerTime(int timeout)
        {
            this._timeout = timeout;
            this._stopwatch = new Stopwatch();
            this._list = new List<T>();
        }

        public AveragerTime(Form form, int timeout):this(timeout)
        {
            this._form = form;
            this._counter = 0;
        } 

        public void Add(T item)
        {
            try
            {
                if (!this._start)
                {
                    this._start = true;
                    this._stopwatch.Start();
                }
                this._list.Add(item);

                if (this._stopwatch.ElapsedMilliseconds < this._timeout)
                {
                    return;
                }

                if (this.Tick != null)
                {
                    this.Tick.Invoke();
                }

                this._list.Clear();
                this._stopwatch.Reset();
                this._counter = 0;
                this._stopwatch.Start();
            }
            catch (Exception e)
            {
                if (this._counter < this.MAX_COUNT_ERR)
                {
                    MessageBox.Show(ERROR_MESSAGE + e.Message);
                    if (e.InnerException != null)
                    {
                        MessageBox.Show(e.InnerException.Message);
                    }
                    this._counter++;
                }
                else
                {
                    MessageBox.Show(INFO_ERR, "Ошибка усреднения", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this._form?.Close();
                }
            }

        }
    }
}
