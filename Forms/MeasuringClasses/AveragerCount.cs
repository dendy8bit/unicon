﻿using System;
using System.Collections.Generic;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;

namespace BEMN.Forms.MeasuringClasses
{
    public class AveragerCount<T> where T: IStruct, IStructInit,new()
    {
        private List<T> _list; // список прочитанных значений
        public event Action Tick; // событие, возникающее при прочтении структуры заданное количество раз
        public event Action IncIndex; // событие, возникающее при прочтении структуры 1 раз
        private int _count; // количество чтений структуры
        private MemoryEntity<T> _memoryEntity;
        private int _index; 

        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }

        public List<T> ValueList
        {
            get { return this._list; }
        }

        public AveragerCount(MemoryEntity<T> memoryEntity)
        {
            _memoryEntity = memoryEntity;
            this._list = new List<T>();
            _memoryEntity.AllReadOk += _memoryEntity_AllReadOk;
        }

        void _memoryEntity_AllReadOk(object sender) // добавляет прочитанные значения в список
        {
            this._list.Add(_memoryEntity.Value);
            _index++;
            this.IncIndex?.Invoke();
            if (_index == _count)
            {
                _memoryEntity.RemoveStructQueries();
                this.Tick?.Invoke();
            }          
        }

        public void MemoryEntityRemove()
        {
            _memoryEntity.RemoveStructQueries();
        }

        public void Start(int count)
        {
            _count = count;
            this._list = new List<T>();
            _index = 0;
            _memoryEntity.LoadStructCycle(new TimeSpan(0,0,0,0,500)); // структура читается каждые 0.5 секунды, вызывает метод _memoryEntity_AllReadOk(object sender) 
        }
    }
}