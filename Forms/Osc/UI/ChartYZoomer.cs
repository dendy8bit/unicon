﻿using System;
using System.Windows.Forms;
using BEMN_XY_Chart;

namespace BEMN.Forms.Osc.UI
{
    /// <summary>
    /// Управляет масштабированием графика по У
    /// </summary>
    public class ChartYZoomer
    {
        #region [Constants]
        /// <summary>
        /// Минимальное увеличение
        /// </summary>
        private const int MIN_ZOOM = 1;
        /// <summary>
        /// Максимальное увеличение
        /// </summary>
        private const int MAX_ZOOM = 64;
        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// Целевой график
        /// </summary>
        private readonly DAS_Net_XYChart _chart;
        /// <summary>
        /// Кнопка увеличения
        /// </summary>
        private readonly Button _increaseButton;
        /// <summary>
        /// Кнопка уменьшения
        /// </summary>
        private readonly Button _decreaseButton;
        /// <summary>
        /// Полоса прокрутки
        /// </summary>
        private readonly VScrollBar _scrollBar;
        /// <summary>
        /// Максимум по У
        /// </summary>
        private readonly double _maxY;
        /// <summary>
        /// Минимум по У
        /// </summary>
        private readonly double _minY;
        /// <summary>
        /// Общая начальная высота графика
        /// </summary>
        private readonly double _height;
        /// <summary>
        /// Середина графика
        /// </summary>
        private readonly double _middle;
        /// <summary>
        /// Увеличение
        /// </summary>
        private int _zoom = 1;
        #endregion [Private fields]


        #region [Ctor's]

        /// <summary>
        /// Создаёт ChartYZoomer
        /// </summary>
        /// <param name="chart">Целевой график</param>
        /// <param name="increaseButton">Кнопка увеличения</param>
        /// <param name="decreaseButton">Кнопка уменьшения</param>
        /// <param name="scrollBar">Полоса прокрутки</param>
        public ChartYZoomer(DAS_Net_XYChart chart, Button increaseButton, Button decreaseButton, VScrollBar scrollBar)
        {
            this._chart = chart;
            this._increaseButton = increaseButton;
            this._decreaseButton = decreaseButton;
            this._scrollBar = scrollBar;

            this._maxY =  this._chart.CoordinateYMax;
            this._minY =  this._chart.CoordinateYMin;
            this._height = Math.Abs(this._maxY) + Math.Abs(this._minY);
            this._middle = (this._maxY + this._minY)/2;

            this._scrollBar.Scroll += _scrollBar_Scroll;
            this._increaseButton.Click += _increaseButton_Click;
            this._decreaseButton.Click += _decreaseButton_Click;
            this._decreaseButton.Enabled = false;
        }


        #endregion [Ctor's]


        #region [Event Handlers]
        /// <summary>
        /// Прокрутка
        /// </summary>
        void _scrollBar_Scroll(object sender, ScrollEventArgs e)
        {
           this.ApplyChanges();
        }

        /// <summary>
        /// Увеличение
        /// </summary>
        private void _increaseButton_Click(object sender, EventArgs e)
        {
            this._decreaseButton.Enabled = true;
            this._scrollBar.Visible = true;
            this._zoom *= 2;
            this.SetScrollBarValues();
            this.ApplyChanges();
            if (this._zoom == ChartYZoomer.MAX_ZOOM)
            {
                this._increaseButton.Enabled = false;
            }
        }

        /// <summary>
        /// Уменьшение
        /// </summary>
        private void _decreaseButton_Click(object sender, EventArgs e)
        {

            this._increaseButton.Enabled = true;
            this._zoom /= 2;
            this.SetScrollBarValues();
            this.ApplyChanges();
            if (this._zoom == ChartYZoomer.MIN_ZOOM)
            {
                this._decreaseButton.Enabled = false;
                this._scrollBar.Visible = false;
            }
        }

        #endregion [Event Handlers]


        #region [Help members]
        /// <summary>
        /// Устанавливает значения полосы прокрутки
        /// </summary>
        private void SetScrollBarValues()
        {
            this._scrollBar.Minimum = -1* (this._zoom-1);
            this._scrollBar.Maximum = this._zoom-1;
            this._scrollBar.Value = 0;
        }
        /// <summary>
        /// Применить изменения
        /// </summary>
        private void ApplyChanges()
        {
            var halfCurrentHeight = this._height/(this._zoom*2);
            var currentMiddle = this._middle  + -1 * this._scrollBar.Value * halfCurrentHeight;
            this._chart.CoordinateYMax = currentMiddle + halfCurrentHeight;
            this._chart.CoordinateYMin = currentMiddle - halfCurrentHeight;
            this._chart.CoordinateYOrigin = currentMiddle;
            this._chart.Update();
        } 
        #endregion [Help members]        
    }
}
