using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using BEMN.MBServer;

namespace BEMN.Forms
{
	/// <summary>
	/// Summary description for DeviceNumberDlg.
	/// </summary>
	public class DeviceConnectionForm : Form
	{
		private GroupBox _deviceGroup;
		private Button ok;
        private Button cancel;

	
        private byte _portNumber;
		private ToolTip tip;
        private System.ComponentModel.IContainer components;
        private ComboBox _portCombo;
        private bool _close = true;

        private bool _okPressed = false;
        private ComboBox _deviceNumberCombo;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private GroupBox groupBox3;
        private Label label3;
        private Label label2;
        private Label label1;
        private MaskedTextBox _ip4;
        private MaskedTextBox _ip3;
        private MaskedTextBox _ip2;
        private MaskedTextBox _ip1;
        private MaskedTextBox maskedTextBox5;
        private RadioButton radioButton1;
        private RadioButton radioButton2;
        private MaskedTextBox maskedTextBox1;
        private Label label4;
        private Label label6;
        private Label label5;
        private CheckBox rtuOnTcp;
        private byte _deviceNumber;
     

		public DeviceConnectionForm(string devType, byte[] portsNumbers,List<byte> notShown,bool enablePortNumber,byte parentPort)
		{			
			InitializeComponent();
			_deviceNumber = 0;
		    this.Text = "���������� " + devType;
			ok.MouseEnter +=new EventHandler(button_MouseEnter);
			ok.MouseLeave +=new EventHandler(button_MouseLeave);
			cancel.MouseEnter +=new EventHandler(button_MouseEnter);
			cancel.MouseLeave +=new EventHandler(button_MouseLeave);

            if (null != portsNumbers)
            {
                _portCombo.Items.AddRange(Array.ConvertAll(portsNumbers, new Converter<byte, object>(ByteToObject)));    
            }

            for (byte i = 1; i < 248; i++)
            {
                if (false == notShown.Contains(i))
                {
                    _deviceNumberCombo.Items.Add(i);
                }                
            }
            _deviceNumberCombo.SelectedIndex = 0;
            if (null != portsNumbers && 0 != portsNumbers.Length)
            {
                if (_portCombo.Items.Contains(parentPort))
                {                    
                    _portCombo.SelectedItem = parentPort;
                    _portCombo.Enabled = enablePortNumber;
                }
                else
                {
                    _portCombo.SelectedIndex = 0;
                }
            }

            tip.SetToolTip(_deviceNumberCombo, "����� ������������� ����������. ������ ������ � ��������� {1 - 247}");
            ok.Focus();
		}


        public static object ByteToObject(byte b)
        {
            return (object)b;
        }


        
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this._deviceGroup = new System.Windows.Forms.GroupBox();
            this._deviceNumberCombo = new System.Windows.Forms.ComboBox();
            this.ok = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.tip = new System.Windows.Forms.ToolTip(this.components);
            this._portCombo = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rtuOnTcp = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._ip4 = new System.Windows.Forms.MaskedTextBox();
            this._ip3 = new System.Windows.Forms.MaskedTextBox();
            this._ip2 = new System.Windows.Forms.MaskedTextBox();
            this._ip1 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.maskedTextBox5 = new System.Windows.Forms.MaskedTextBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this._deviceGroup.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // _deviceGroup
            // 
            this._deviceGroup.BackColor = System.Drawing.Color.LightGray;
            this._deviceGroup.Controls.Add(this._deviceNumberCombo);
            this._deviceGroup.Location = new System.Drawing.Point(12, 12);
            this._deviceGroup.Name = "_deviceGroup";
            this._deviceGroup.Size = new System.Drawing.Size(212, 49);
            this._deviceGroup.TabIndex = 0;
            this._deviceGroup.TabStop = false;
            this._deviceGroup.Text = "����� ����������";
            // 
            // _deviceNumberCombo
            // 
            this._deviceNumberCombo.FormattingEnabled = true;
            this._deviceNumberCombo.Location = new System.Drawing.Point(76, 22);
            this._deviceNumberCombo.Name = "_deviceNumberCombo";
            this._deviceNumberCombo.Size = new System.Drawing.Size(64, 21);
            this._deviceNumberCombo.TabIndex = 1;
            // 
            // ok
            // 
            this.ok.BackColor = System.Drawing.Color.LightGray;
            this.ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ok.Location = new System.Drawing.Point(12, 287);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(90, 23);
            this.ok.TabIndex = 1;
            this.ok.Text = "��";
            this.ok.UseVisualStyleBackColor = false;
            this.ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // cancel
            // 
            this.cancel.BackColor = System.Drawing.Color.LightGray;
            this.cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cancel.Location = new System.Drawing.Point(134, 287);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(90, 23);
            this.cancel.TabIndex = 2;
            this.cancel.Text = "������";
            this.cancel.UseVisualStyleBackColor = false;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // tip
            // 
            this.tip.AutoPopDelay = 1000;
            this.tip.InitialDelay = 500;
            this.tip.ReshowDelay = 100;
            // 
            // _portCombo
            // 
            this._portCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._portCombo.FormattingEnabled = true;
            this._portCombo.Location = new System.Drawing.Point(102, 25);
            this._portCombo.Name = "_portCombo";
            this._portCombo.Size = new System.Drawing.Size(64, 21);
            this._portCombo.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._portCombo);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 69);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(212, 58);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "    RTU";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "����� �����";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rtuOnTcp);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.maskedTextBox1);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.maskedTextBox5);
            this.groupBox2.Enabled = false;
            this.groupBox2.Location = new System.Drawing.Point(12, 133);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(212, 148);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "    TCP";
            // 
            // rtuOnTcp
            // 
            this.rtuOnTcp.AutoSize = true;
            this.rtuOnTcp.Checked = true;
            this.rtuOnTcp.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rtuOnTcp.Location = new System.Drawing.Point(12, 125);
            this.rtuOnTcp.Name = "rtuOnTcp";
            this.rtuOnTcp.Size = new System.Drawing.Size(193, 17);
            this.rtuOnTcp.TabIndex = 3;
            this.rtuOnTcp.Text = "��������������� � Modbus TCP";
            this.rtuOnTcp.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "�������, ��";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.LightGray;
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this._ip4);
            this.groupBox3.Controls.Add(this._ip3);
            this.groupBox3.Controls.Add(this._ip2);
            this.groupBox3.Controls.Add(this._ip1);
            this.groupBox3.Location = new System.Drawing.Point(6, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(196, 49);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "IP-�����";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(90, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(10, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = ".";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = ".";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(140, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(10, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = ".";
            // 
            // _ip4
            // 
            this._ip4.Location = new System.Drawing.Point(156, 19);
            this._ip4.Name = "_ip4";
            this._ip4.Size = new System.Drawing.Size(28, 20);
            this._ip4.TabIndex = 3;
            this._ip4.Text = "122";
            this._ip4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._ip1_KeyPress);
            // 
            // _ip3
            // 
            this._ip3.Location = new System.Drawing.Point(106, 19);
            this._ip3.Name = "_ip3";
            this._ip3.Size = new System.Drawing.Size(28, 20);
            this._ip3.TabIndex = 2;
            this._ip3.Text = "89";
            this._ip3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._ip1_KeyPress);
            // 
            // _ip2
            // 
            this._ip2.Location = new System.Drawing.Point(56, 19);
            this._ip2.Name = "_ip2";
            this._ip2.PromptChar = ' ';
            this._ip2.Size = new System.Drawing.Size(28, 20);
            this._ip2.TabIndex = 1;
            this._ip2.Text = "12";
            this._ip2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._ip1_KeyPress);
            // 
            // _ip1
            // 
            this._ip1.Location = new System.Drawing.Point(6, 19);
            this._ip1.Name = "_ip1";
            this._ip1.Size = new System.Drawing.Size(28, 20);
            this._ip1.TabIndex = 0;
            this._ip1.Text = "10";
            this._ip1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._ip1_KeyPress);
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(102, 100);
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(100, 20);
            this.maskedTextBox1.TabIndex = 0;
            this.maskedTextBox1.Text = "3000";
            this.maskedTextBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._ip1_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "����� �����";
            // 
            // maskedTextBox5
            // 
            this.maskedTextBox5.Location = new System.Drawing.Point(102, 74);
            this.maskedTextBox5.Name = "maskedTextBox5";
            this.maskedTextBox5.Size = new System.Drawing.Size(100, 20);
            this.maskedTextBox5.TabIndex = 0;
            this.maskedTextBox5.Text = "4444";
            this.maskedTextBox5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._ip1_KeyPress);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(18, 69);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(14, 13);
            this.radioButton1.TabIndex = 5;
            this.radioButton1.TabStop = true;
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(17, 132);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(14, 13);
            this.radioButton2.TabIndex = 6;
            this.radioButton2.TabStop = true;
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // DeviceConnectionForm
            // 
            this.AcceptButton = this.ok;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.LightGray;
            this.CancelButton = this.cancel;
            this.ClientSize = new System.Drawing.Size(238, 316);
            this.Controls.Add(this._deviceGroup);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.ok);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DeviceConnectionForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "����������";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DeviceNumberDlg_FormClosing);
            this.Load += new System.EventHandler(this.DeviceNumberDlg_Load);
            this._deviceGroup.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        public ModbusType ModbusType { get; private set; }
        public string Ip { get; private set; }
        public int Port { get; private set; }
        public int ReceiveTimeout { get; private set; }
        public bool RtuToTcp { get; private set; }

        private bool Validate(MaskedTextBox tb, int min, int max)
        {
            try
            {
                var value = int.Parse(tb.Text);
                if ((value< min)||(value> max))
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                tip.IsBalloon = true;
                tip.Show(string.Format("�������� ������ ������ � ��������� {0}-{1}",min,max), tb, 2000);
                return false;
            }
            return true;
        }

		private void ok_Click(object sender, System.EventArgs e)
		{
            bool res = false;
            try
            {
                res = _deviceNumberCombo.Items.Contains(Byte.Parse(_deviceNumberCombo.Text));
            }
            catch(System.FormatException)
            {
              tip.IsBalloon = true;
              tip.Show("������� ����� ���������� � ���������� �������", _deviceNumberCombo,2000);
              _close = false;
              return;
            }
            catch (System.OverflowException)
            {
                tip.IsBalloon = true;
                tip.Show("����� ���������� ������ ������ � ��������� [1-247]", _deviceNumberCombo, 2000);
                _close = false;
                return;
            }


		    if (radioButton1.Checked)
		    {
		        this.ModbusType = ModbusType.RTU;
		    }
		    else
		    {
		        this.ModbusType = ModbusType.TCP;

		        var flag = Validate(_ip1, 0, 255) & Validate(_ip2, 0, 255) & Validate(_ip3, 0, 255) & Validate(_ip4, 0, 255) &
		                   Validate(maskedTextBox5, 0, 65535) & Validate(maskedTextBox1, 0, 20000);
		        _close = flag;
		        if (!flag)
		        {
		            return;
		        }
		        Ip = string.Format("{0}.{1}.{2}.{3}", _ip1.Text, _ip2.Text, _ip3.Text, _ip4.Text);
		        Port = int.Parse(maskedTextBox5.Text);
		        ReceiveTimeout = int.Parse(maskedTextBox1.Text);
		        RtuToTcp = rtuOnTcp.Checked;
		        _okPressed = true;
		    }
            
		    if (false == res)
	        {
               tip.IsBalloon = true;
               tip.Show("���������� � ���� ������� ���������", _deviceNumberCombo,2000);
               _close = false;
               return;
        	}
            else
            {
                _deviceNumber = System.Convert.ToByte(_deviceNumberCombo.Text);
                
                _portNumber = (null == _portCombo.SelectedItem) ? (byte)0 : (byte)(_portCombo.SelectedItem);
                _okPressed = true;
                _close = true;
            }
		}

		private void cancel_Click(object sender, System.EventArgs e)
		{
			_deviceNumber = 0;
            _portNumber = 0;
            _close = true;
			this.Close();
		
		}

		private void button_MouseEnter(object sender, EventArgs e)
		{
			((Button)sender).BackColor = Color.Black;
			((Button)sender).ForeColor = Color.White;
			
		}

		private void button_MouseLeave(object sender, EventArgs e)
		{
			((Button)sender).BackColor = Color.LightGray;
			((Button)sender).ForeColor = Color.Black;
		}

		

		public byte PortNumber
        {
            get
            {
                return _portNumber;
            }
        }

		public byte DeviceNumber
		{
			get
			{
				return _deviceNumber;
			}
		}

        public bool OkPressed
        {
            get
            {
                return _okPressed;
            }
        }

        private void DeviceNumberDlg_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !_close;
        }

        private void DeviceNumberDlg_Load(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioButton1.Checked)
            {
                this.groupBox1.Enabled = true;
                this.groupBox2.Enabled = false;
            }
            else
            {
                this.groupBox1.Enabled = false;
                this.groupBox2.Enabled = true;                
            }
        }

        private void _ip1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsDigit(e.KeyChar) | (e.KeyChar == '\b') );
        }		
	}
}
