﻿using System;
using System.Windows.Forms;

namespace BEMN.Forms
{
    public partial class ChoiceDeviceTypeForMr902 : Form
    {
        private string[] outStrings;
        public ChoiceDeviceTypeForMr902(string[] list, string[] res)
        {
            this.InitializeComponent();

            // Цикл for нужен только для МР902, поэтому сделана форма отдельно для Мр902 для экономии времени загрузки форм
            for (int i = 0; i < list.Length; i++)
            {
                var index1 = list[i].IndexOf('(');
                var index2 = list[i].IndexOf(')');
                if (index1 < 0 || index2 < 0) continue;
                list[i] = list[i].Remove(index1, index2 - index1 + 1);
            }

            this.comboBox1.DataSource = list;
            this.comboBox1.SelectedIndex = 0;

            this.outStrings = res;
        }

        public string DevicePlant => this.outStrings[this.comboBox1.SelectedIndex];

        private void OK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
