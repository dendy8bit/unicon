using System;
using System.Collections;
using System.Windows.Forms;

namespace BEMN.Forms.Old
{
    public partial class LedEditorForm : Form
    {
        public LedEditorForm()
        {
            this.InitializeComponent();
        }

        LedControl[] leds;
        public LedEditorForm(BitArray bits)
        {
            this.InitializeComponent();
            this.leds = new LedControl[]{this.ledControl1,this.ledControl2,this.ledControl3,this.ledControl4,
                                           this.ledControl5,this.ledControl6,this.ledControl7,this.ledControl8,
                                           this.ledControl9,this.ledControl10,this.ledControl11,this.ledControl12,
                                           this.ledControl13,this.ledControl14,this.ledControl15,this.ledControl16};
            for (int i = 0; i < bits.Count; i++)
            {
                if (i >= this.leds.Length)
                {
                    break;
                }
                this.leds[i].State =   bits[i] ? LedState.NoSignaled : LedState.Signaled;
            }
            for (int i = bits.Count; i < this.leds.Length; i++)
            {
                this.leds[i].Visible = false;
            }

        }

        private void _closeBut_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}