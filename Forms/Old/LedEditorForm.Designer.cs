namespace BEMN.Forms.Old
{
    partial class LedEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ledControl16 = new BEMN.Forms.LedControl();
            this.ledControl15 = new BEMN.Forms.LedControl();
            this.ledControl14 = new BEMN.Forms.LedControl();
            this.ledControl13 = new BEMN.Forms.LedControl();
            this.ledControl12 = new BEMN.Forms.LedControl();
            this.ledControl11 = new BEMN.Forms.LedControl();
            this.ledControl10 = new BEMN.Forms.LedControl();
            this.ledControl9 = new BEMN.Forms.LedControl();
            this.ledControl8 = new BEMN.Forms.LedControl();
            this.ledControl7 = new BEMN.Forms.LedControl();
            this.ledControl6 = new BEMN.Forms.LedControl();
            this.ledControl5 = new BEMN.Forms.LedControl();
            this.ledControl4 = new BEMN.Forms.LedControl();
            this.ledControl3 = new BEMN.Forms.LedControl();
            this.ledControl2 = new BEMN.Forms.LedControl();
            this.ledControl1 = new BEMN.Forms.LedControl();
            this._closeBut = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ledControl16);
            this.groupBox1.Controls.Add(this.ledControl15);
            this.groupBox1.Controls.Add(this.ledControl14);
            this.groupBox1.Controls.Add(this.ledControl13);
            this.groupBox1.Controls.Add(this.ledControl12);
            this.groupBox1.Controls.Add(this.ledControl11);
            this.groupBox1.Controls.Add(this.ledControl10);
            this.groupBox1.Controls.Add(this.ledControl9);
            this.groupBox1.Controls.Add(this.ledControl8);
            this.groupBox1.Controls.Add(this.ledControl7);
            this.groupBox1.Controls.Add(this.ledControl6);
            this.groupBox1.Controls.Add(this.ledControl5);
            this.groupBox1.Controls.Add(this.ledControl4);
            this.groupBox1.Controls.Add(this.ledControl3);
            this.groupBox1.Controls.Add(this.ledControl2);
            this.groupBox1.Controls.Add(this.ledControl1);
            this.groupBox1.Location = new System.Drawing.Point(3, -2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(117, 48);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // ledControl16
            // 
            this.ledControl16.Location = new System.Drawing.Point(95, 28);
            this.ledControl16.Name = "ledControl16";
            this.ledControl16.Size = new System.Drawing.Size(13, 13);
            this.ledControl16.State = BEMN.Forms.LedState.Off;
            this.ledControl16.TabIndex = 15;
            // 
            // ledControl15
            // 
            this.ledControl15.Location = new System.Drawing.Point(83, 28);
            this.ledControl15.Name = "ledControl15";
            this.ledControl15.Size = new System.Drawing.Size(13, 13);
            this.ledControl15.State = BEMN.Forms.LedState.Off;
            this.ledControl15.TabIndex = 14;
            // 
            // ledControl14
            // 
            this.ledControl14.Location = new System.Drawing.Point(71, 28);
            this.ledControl14.Name = "ledControl14";
            this.ledControl14.Size = new System.Drawing.Size(13, 13);
            this.ledControl14.State = BEMN.Forms.LedState.Off;
            this.ledControl14.TabIndex = 13;
            // 
            // ledControl13
            // 
            this.ledControl13.Location = new System.Drawing.Point(59, 28);
            this.ledControl13.Name = "ledControl13";
            this.ledControl13.Size = new System.Drawing.Size(13, 13);
            this.ledControl13.State = BEMN.Forms.LedState.Off;
            this.ledControl13.TabIndex = 12;
            // 
            // ledControl12
            // 
            this.ledControl12.Location = new System.Drawing.Point(47, 28);
            this.ledControl12.Name = "ledControl12";
            this.ledControl12.Size = new System.Drawing.Size(13, 13);
            this.ledControl12.State = BEMN.Forms.LedState.Off;
            this.ledControl12.TabIndex = 11;
            // 
            // ledControl11
            // 
            this.ledControl11.Location = new System.Drawing.Point(35, 28);
            this.ledControl11.Name = "ledControl11";
            this.ledControl11.Size = new System.Drawing.Size(13, 13);
            this.ledControl11.State = BEMN.Forms.LedState.Off;
            this.ledControl11.TabIndex = 10;
            // 
            // ledControl10
            // 
            this.ledControl10.Location = new System.Drawing.Point(23, 28);
            this.ledControl10.Name = "ledControl10";
            this.ledControl10.Size = new System.Drawing.Size(13, 13);
            this.ledControl10.State = BEMN.Forms.LedState.Off;
            this.ledControl10.TabIndex = 9;
            // 
            // ledControl9
            // 
            this.ledControl9.Location = new System.Drawing.Point(11, 28);
            this.ledControl9.Name = "ledControl9";
            this.ledControl9.Size = new System.Drawing.Size(13, 13);
            this.ledControl9.State = BEMN.Forms.LedState.Off;
            this.ledControl9.TabIndex = 8;
            // 
            // ledControl8
            // 
            this.ledControl8.Location = new System.Drawing.Point(95, 13);
            this.ledControl8.Name = "ledControl8";
            this.ledControl8.Size = new System.Drawing.Size(13, 13);
            this.ledControl8.State = BEMN.Forms.LedState.Off;
            this.ledControl8.TabIndex = 7;
            // 
            // ledControl7
            // 
            this.ledControl7.Location = new System.Drawing.Point(83, 13);
            this.ledControl7.Name = "ledControl7";
            this.ledControl7.Size = new System.Drawing.Size(13, 13);
            this.ledControl7.State = BEMN.Forms.LedState.Off;
            this.ledControl7.TabIndex = 6;
            // 
            // ledControl6
            // 
            this.ledControl6.Location = new System.Drawing.Point(71, 13);
            this.ledControl6.Name = "ledControl6";
            this.ledControl6.Size = new System.Drawing.Size(13, 13);
            this.ledControl6.State = BEMN.Forms.LedState.Off;
            this.ledControl6.TabIndex = 5;
            // 
            // ledControl5
            // 
            this.ledControl5.Location = new System.Drawing.Point(59, 13);
            this.ledControl5.Name = "ledControl5";
            this.ledControl5.Size = new System.Drawing.Size(13, 13);
            this.ledControl5.State = BEMN.Forms.LedState.Off;
            this.ledControl5.TabIndex = 4;
            // 
            // ledControl4
            // 
            this.ledControl4.Location = new System.Drawing.Point(47, 13);
            this.ledControl4.Name = "ledControl4";
            this.ledControl4.Size = new System.Drawing.Size(13, 13);
            this.ledControl4.State = BEMN.Forms.LedState.Off;
            this.ledControl4.TabIndex = 3;
            // 
            // ledControl3
            // 
            this.ledControl3.Location = new System.Drawing.Point(35, 13);
            this.ledControl3.Name = "ledControl3";
            this.ledControl3.Size = new System.Drawing.Size(13, 13);
            this.ledControl3.State = BEMN.Forms.LedState.Off;
            this.ledControl3.TabIndex = 2;
            // 
            // ledControl2
            // 
            this.ledControl2.Location = new System.Drawing.Point(23, 13);
            this.ledControl2.Name = "ledControl2";
            this.ledControl2.Size = new System.Drawing.Size(13, 13);
            this.ledControl2.State = BEMN.Forms.LedState.Off;
            this.ledControl2.TabIndex = 1;
            // 
            // ledControl1
            // 
            this.ledControl1.Location = new System.Drawing.Point(11, 13);
            this.ledControl1.Name = "ledControl1";
            this.ledControl1.Size = new System.Drawing.Size(13, 13);
            this.ledControl1.State = BEMN.Forms.LedState.Off;
            this.ledControl1.TabIndex = 0;
            // 
            // _closeBut
            // 
            this._closeBut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._closeBut.Location = new System.Drawing.Point(24, 52);
            this._closeBut.Name = "_closeBut";
            this._closeBut.Size = new System.Drawing.Size(75, 23);
            this._closeBut.TabIndex = 1;
            this._closeBut.Text = "�������";
            this._closeBut.UseVisualStyleBackColor = true;
            this._closeBut.Click += new System.EventHandler(this._closeBut_Click);
            // 
            // LedEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._closeBut;
            this.ClientSize = new System.Drawing.Size(123, 78);
            this.Controls.Add(this._closeBut);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LedEditorForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "�������";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private LedControl ledControl16;
        private LedControl ledControl15;
        private LedControl ledControl14;
        private LedControl ledControl13;
        private LedControl ledControl12;
        private LedControl ledControl11;
        private LedControl ledControl10;
        private LedControl ledControl9;
        private LedControl ledControl8;
        private LedControl ledControl7;
        private LedControl ledControl6;
        private LedControl ledControl5;
        private LedControl ledControl4;
        private LedControl ledControl3;
        private LedControl ledControl2;
        private LedControl ledControl1;
        private System.Windows.Forms.Button _closeBut;
    }
}