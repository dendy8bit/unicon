using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace BEMN.Forms.Old
{
    public class BitsEditor : UITypeEditor
    {
        /// <summary>
        /// ���������� ������ ��������������
        /// </summary>
        public override object EditValue(ITypeDescriptorContext context,IServiceProvider provider,object value)
        {
            if ((context != null) && (provider != null))
            {
                IWindowsFormsEditorService svc =
                   (IWindowsFormsEditorService)
                   provider.GetService(typeof(IWindowsFormsEditorService));

                if (svc != null)
                {
                    using (BitsEditorForm ipfrm =
                       new BitsEditorForm((BitArray)value))
                    {
                        if (svc.ShowDialog(ipfrm) == DialogResult.OK)
                        {
                            value = ipfrm.Bits;
                        }
                    }
                }
            }
            return base.EditValue(context, provider, value);
        }

        /// <summary>
        /// ���������� ����� ��������� - ��������� ����
        /// </summary>
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            if (context != null)
                return UITypeEditorEditStyle.Modal;
            else
                return base.GetEditStyle(context);
        }

    }
     public class LedEditor : UITypeEditor
     {
        /// <summary>
        /// ���������� ������ ��������������
        /// </summary>
        public override object EditValue(ITypeDescriptorContext context,IServiceProvider provider,object value)
        {
            if ((context != null) && (provider != null))
            {
                IWindowsFormsEditorService svc =
                   (IWindowsFormsEditorService)
                   provider.GetService(typeof(IWindowsFormsEditorService));

                if (svc != null)
                {
                    using (LedEditorForm frm =
                       new LedEditorForm((BitArray)value))
                    {
                        svc.ShowDialog(frm);
                    }
                }
            }
            return base.EditValue(context, provider, value);
        }

        /// <summary>
        /// ���������� ����� ��������� - ��������� ����
        /// </summary>
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            if (context != null)
                return UITypeEditorEditStyle.Modal;
            else
                return base.GetEditStyle(context);
        }
    }

    public class RussianCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// �����������
        /// </summary>
        public RussianCollectionEditor(Type t): base(t)
        {
            }

        /// <summary>
        /// ���������� ����� �������� ����� ��������� - ��� ����������/��������������
        /// �������� � ��������� ����
        /// </summary>
        protected override CollectionForm CreateCollectionForm()
        {
            CollectionForm collform = base.CreateCollectionForm();
            // ���������� ���� ���������� �������� � � ��� 
            // ��������������� ��������� � ������ ����
            collform.Load += delegate(object sender, EventArgs e)
            {

                collform.HelpButton = false;
                collform.Text = "����� ���������";


                #region ����������� � ������������ �������� �� �����

                Label BlaBlaProperties = new Label();
                string MembersText = "&��������:";
                string PropertiesText = "&�������� ���������:";

                // ���������� ��� �������� �� ����� � ��������
                // ������������ �������
                foreach (Control ctrl in collform.Controls)
                {

                    foreach (Control ctrl1 in ctrl.Controls)
                    {
                        if (ctrl1.GetType().ToString() ==
                            "System.Windows.Forms.Label" &&
                            (ctrl1.Text == "&Members:" || ctrl1.Text == "&�����:"))
                        {
                            ctrl1.Text = MembersText;
                        }

                        if (ctrl1.GetType().ToString() ==
                            "System.Windows.Forms.Label" &&
                            (ctrl1.Text.Contains("&properties") || ctrl1.Text.Contains("&C�������")))
                        {
                            BlaBlaProperties = (Label) ctrl1;
                            BlaBlaProperties.Text = PropertiesText;
                        }

                        if (ctrl1.GetType().ToString() ==
                            "System.ComponentModel.Design.CollectionEditor+FilterListBox")
                        {
                            // ��� ����� ���������� ����������, �� ����� ����
                            // ����������� ���������� ����� � ������ ������� �� ����
                            //((ListBox)ctrl1).SelectedValueChanged +=
                            //   delegate(object sndr, EventArgs ea)
                            //   {
                            //      Trace.WriteLine("SelectedValueChanged()");
                            //      BlaBlaProperties.Text = PropertiesText ;
                            //   };

                            // ������ ������ ����������� - ��� �������� - 
                            // �� ����� ������� � ���������
                            ((ListBox) ctrl1).SelectedIndexChanged +=
                                delegate(object sndr, EventArgs ea)
                                {
                                    BlaBlaProperties.Text = PropertiesText;
                                };
                        }

                        if (ctrl1.GetType().ToString() ==
                            "System.Windows.Forms.Design.VsPropertyGrid")
                        {
                            // � �� �������������� � PropertyGrid
                            ((PropertyGrid) ctrl1).SelectedGridItemChanged +=
                                delegate(object sndr, SelectedGridItemChangedEventArgs segichd)
                                {
                                    BlaBlaProperties.Text = PropertiesText;
                                };

                            // ����� ������� ��������� ���� � ����������� �� ���������� � ������ ����� 
                            ((PropertyGrid) ctrl1).HelpVisible = true;
                            ((PropertyGrid) ctrl1).HelpBackColor = System.Drawing.SystemColors.Info;
                        }

                    }
                }

                #endregion
            };
            return collform;
        }
    }
}
