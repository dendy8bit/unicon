using System.Collections;
using System.Windows.Forms;

namespace BEMN.Forms.Old
{
    public partial class BitsEditorForm : Form
    {       
        public BitArray Bits
        {
            get
            {
                int cnt = this._checkList.Items.Count;
                BitArray ret = new BitArray(cnt);
                for (int i = 0; i < cnt; i++)
                {
                    ret[i] = this._checkList.GetItemChecked(i);
                }
                return ret;
            }
            set
            {                
                for (int i = 0; i < value.Count; i++)
                {
                    this._checkList.Items.Add("��� �" + i, value[i]);
                }
            }
        }
        
        public BitsEditorForm()
        {
            this.InitializeComponent();
        }
        public BitsEditorForm(BitArray bits)
        {
            this.InitializeComponent();
            this.Bits = bits;
        }
        


    }
}