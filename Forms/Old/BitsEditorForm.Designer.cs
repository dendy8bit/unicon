namespace BEMN.Forms.Old
{
    partial class BitsEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._okButton = new System.Windows.Forms.Button();
            this._cancelBut = new System.Windows.Forms.Button();
            this._checkList = new System.Windows.Forms.CheckedListBox();
            this.SuspendLayout();
            // 
            // _okButton
            // 
            this._okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._okButton.Location = new System.Drawing.Point(2, 145);
            this._okButton.Name = "_okButton";
            this._okButton.Size = new System.Drawing.Size(75, 23);
            this._okButton.TabIndex = 0;
            this._okButton.Text = "�������";
            this._okButton.UseVisualStyleBackColor = true;
            // 
            // _cancelBut
            // 
            this._cancelBut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancelBut.Location = new System.Drawing.Point(81, 145);
            this._cancelBut.Name = "_cancelBut";
            this._cancelBut.Size = new System.Drawing.Size(75, 23);
            this._cancelBut.TabIndex = 1;
            this._cancelBut.Text = "��������";
            this._cancelBut.UseVisualStyleBackColor = true;
            // 
            // _checkList
            // 
            this._checkList.CheckOnClick = true;
            this._checkList.FormattingEnabled = true;
            this._checkList.Location = new System.Drawing.Point(2, 0);
            this._checkList.Name = "_checkList";
            this._checkList.Size = new System.Drawing.Size(154, 139);
            this._checkList.TabIndex = 2;
            // 
            // BitsEditorForm
            // 
            this.AcceptButton = this._okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._cancelBut;
            this.ClientSize = new System.Drawing.Size(157, 170);
            this.Controls.Add(this._checkList);
            this.Controls.Add(this._cancelBut);
            this.Controls.Add(this._okButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BitsEditorForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "�������� �����";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _okButton;
        private System.Windows.Forms.Button _cancelBut;
        private System.Windows.Forms.CheckedListBox _checkList;
    }
}