using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BEMN.Forms.Old
{
    public class DateTimeBox : MaskedTextBox
    {
        private byte trash = 0;
        private byte trash2 = 0;
        byte[] seven = new byte[7];
        public DateTimeBox()
        {
            this.PromptChar = ' ';
            this.Mask = "00-00-00 00:00:00:00";
        }

        private bool _piconmicroFlag;

        public bool PiconMicroFlag
        {
            get { return this._piconmicroFlag; }
            set { this._piconmicroFlag = value;
                   if (this.PiconMicroFlag)
                   {
                       this.MR700Flag = false;
                   }
                }
        }

        private bool _mr700Flag;

        public bool MR700Flag
        {
            get { return this._mr700Flag; }
            set { 
                   this._mr700Flag = value;
                   if (this.MR700Flag)
                   {
                       this.PiconMicroFlag = false;
                   }
                }
        }

        private bool _tzlFlag;

        public bool TZLFlag
        {
            get { return this._tzlFlag; }
            set
            {
                this._tzlFlag = value;
                if (this.TZLFlag)
                {
                    this.PiconMicroFlag = false;
                }
            }
        }
              
        public byte[] DateTime
        {
            get
             {
                 try
                 {
                     this.seven = this.MaskedTextToBytes(Text);
                     byte[] buffer = new byte[16];

                     if (this.PiconMicroFlag)
                     {
                         if (this.seven.Length >= 6)
                         {
                             buffer[1] = this.seven[2];
                             buffer[3] = this.seven[1];
                             buffer[5] = this.seven[0];
                             buffer[7] = this.seven[3];
                             buffer[9] = this.seven[4];
                             buffer[11] = this.seven[5];
                             buffer[13] = this.seven[6];
                         }

                         return buffer;
                     }
                     else if (this.MR700Flag)
                     {
                         if (this.seven.Length >= 5)
                         {
                             buffer[1] = this.seven[2];
                             buffer[3] = this.seven[1];
                             buffer[5] = this.seven[0];
                             buffer[7] = this.seven[3];
                             buffer[9] = this.seven[4];
                             buffer[11] = this.seven[5];
                         }

                         return buffer;
                     }
                     else
                     {
                         if (this.TZLFlag)
                         {
                             if (this.seven.Length >= 5)
                             {
                                 buffer[1] = this.seven[2];
                                 buffer[3] = this.seven[1];
                                 buffer[5] = this.seven[0];
                                 buffer[7] = this.seven[3];
                                 buffer[9] = this.seven[4];
                                 buffer[11] = this.seven[5];
                             }

                             return buffer;
                         }
                         else
                         {
                             byte[] eight = new byte[this.seven.Length + 1];

                             this.trash = this.seven[0];
                             this.trash2 = this.seven[2];
                             this.seven[0] = this.trash2;
                             this.seven[2] = this.trash;

                             Array.ConstrainedCopy(this.seven, 0, eight, 1, eight.Length - 1);


                             return eight;


                             //!!!!!
                             /*  buffer[1] = seven[2];
                               buffer[3] = seven[1];
                               buffer[5] = seven[0];
                               buffer[7] = seven[3];
                               buffer[9] = seven[4];
                               buffer[11] = seven[5];
                               return buffer;*/
                         }

                         //byte[] eight = new byte[seven.Length + 1];
                         //Array.ConstrainedCopy(seven, 0, eight, 1, eight.Length - 1);
                         //return eight;
                     }
                 }
                 catch (Exception)
                 {

                     return new byte[16];
                 }
            
                

             }
            set
            {
                string str = "";

                if (this.PiconMicroFlag)
                {
                    str = String.Concat(value[9].ToString("D2"), value[11].ToString("d2"), value[13].ToString("d2"),
                                        value[1].ToString("d2"), value[3].ToString("d2"), value[5].ToString("d2"),"00");
                }
                else if(this.MR700Flag)
                {
                    str = String.Concat(value[5].ToString("D2"), value[3].ToString("D2"), value[1].ToString("D2"),
                                        value[7].ToString("D2"), value[9].ToString("D2"), value[11].ToString("D2"), value[13].ToString("D2"));
                }
                else
                {
                    if (this.TZLFlag)
                    {
                        str = String.Concat(value[5].ToString("D2"), value[3].ToString("D2"), value[1].ToString("D2"),
                                            value[7].ToString("D2"), value[9].ToString("D2"), value[11].ToString("D2"), value[13].ToString("D2"));
                    }
                    else
                    {
                        for (int i = 1; i < value.Length; i++)
                        {
                            str += value[i].ToString("X2");
                        }
                    }

                    //for (int i = 1; i < value.Length; i++)
                    //{
                    //    str += value[i].ToString("X2");
                    //}
                }
                
                Text = str;
            }
        }

        public static byte CharToByte(char c)
        {            
            return (byte)((byte)c - 0x30);
        }

        private byte[] MaskedTextToBytes(string text)
        {
            byte[] buffer = Array.ConvertAll(this.Text.ToCharArray(), new Converter<char, byte>(CharToByte));
            List<byte> inBytes = new List<byte>(buffer.Length);
            for (int i = 0; i < buffer.Length; i++)
            {
                if (buffer[i] >= 0 && buffer[i] <= 9)
                {
                    inBytes.Add(buffer[i]);
                }   
            }
            if (inBytes.Count % 2 != 0)
            {
                inBytes.Insert(0, 0);
            }
            byte[] ret = new byte[inBytes.Count / 2];
            for (int i = 0; i < inBytes.Count; i += 2)
            {
                if(this.PiconMicroFlag || this.MR700Flag)
                {
                    ret[i / 2] = (byte)(inBytes[i] * 10 + inBytes[i + 1]);
                }
                else
                {
                    if (this.PiconMicroFlag || this.TZLFlag)
                    {
                        ret[i / 2] = (byte)(inBytes[i] * 10 + inBytes[i + 1]);
                    }
                    else
                    {
                        ret[i / 2] = (byte)(inBytes[i] * 16 + inBytes[i + 1]);
                    }
                    //ret[i / 2] = (byte)(inBytes[i] * 16 + inBytes[i + 1]);     
                }
                
            }
            return ret;
        }
    }
}
