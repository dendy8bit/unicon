using System;
using System.Drawing;
using System.Windows.Forms;

namespace BEMN.Forms
{
    public enum LedState { Signaled, NoSignaled, Off };
    /// <summary>
    /// Summary description for led.
    /// </summary>
    public class LedControl : System.Windows.Forms.UserControl
    {

        public event EventHandler<MouseEventArgs> LedClicked;

        //Bitmap greenBmp = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("BEMN.Forms.Res.green.bmp"));
        //Bitmap redBmp = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("BEMN.Forms.Res.red.bmp"));
        //Bitmap grayBmp = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("BEMN.Forms.Res.gray.bmp"));
        public System.Windows.Forms.PictureBox pictureBox;
        private LedState _state = LedState.Off;
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        public LedControl()
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();
            State = _state;
            // TODO: Add any initialization after the InitializeComponent call

        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components !=   null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        public void SetState(bool state)
        {
            if (state)
            {
                State = LedState.NoSignaled;
            }
            else
            {
                State = LedState.Signaled;
            }
        }

        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(13, 13);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            this.pictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseClick);
            // 
            // LedControl
            // 
            this.Controls.Add(this.pictureBox);
            this.Name = "LedControl";
            this.Size = new System.Drawing.Size(13, 13);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
        public LedState State
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;

                if (LedState.Off == value)
                {
                    //this.pictureBox.Image = grayBmp;
                    this.pictureBox.BackColor = Color.LightGray;

                }
                else if (LedState.Signaled == value)
                {
                    //this.pictureBox.Image = greenBmp;
                    pictureBox.BackColor = Color.LightGreen;
                }
                else if (LedState.NoSignaled == value)
                {
                    //this.pictureBox.Image = redBmp;
                    pictureBox.BackColor = Color.Red;
                }
            }
        }

        private void pictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (this.LedClicked != null)
                this.LedClicked(this, e);
        }
    }

}