﻿using System;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;


namespace BEMN.Forms.Export
{
    /// <summary>
    /// Вспомогательный класс экспорта в Html
    /// </summary>
    public static class HtmlExport
    {
        public static SaveFileDialog _saveDialog = new SaveFileDialog()
            {
                Filter = @"Html (*.html)|*.html|Xml (*.xml)|*.xml"
            };

        /// <summary>
        /// Экспортирует данные из DataTable в файл Html
        /// </summary>
        /// <param name="dataTable">Исходный DataTable</param>
        /// <param name="fileName">Имя итогового html файла</param>
        /// <param name="shema">xsl схема в виде строки</param>
        public static void Export(DataTable dataTable, string fileName, string shema)
        {
            try
            {
                using (MemoryStream stream = new MemoryStream(1000))
                {
                    dataTable.WriteXml(stream);
                    //dataTable.WriteXml(@"D:\1.xml"); // Для отладки
                    stream.Seek(0, SeekOrigin.Begin);
                    using (XmlReader reader = XmlReader.Create(stream))
                    {
                        XslCompiledTransform xmlt = new XslCompiledTransform();
                        xmlt.Load(new XmlTextReader(new StringReader(shema)));
                        xmlt.Transform(reader, new XmlTextWriter(fileName, Encoding.GetEncoding(1251)));
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public static void Export(string fileName,string header, string xml)
        {
            try
            {
                using (XmlReader reader = new XmlTextReader(new StringReader(xml)))
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load(reader);
                    XmlElement xRoot = xDoc.DocumentElement;
                    if(xRoot == null) throw new Exception("Invalid xml document!");

                    StringBuilder builder = new StringBuilder();
                    builder.AppendLine("<?xml version=\"1.0\" encoding=\"WINDOWS-1251\"?>");
                    builder.AppendLine("<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">");
                    builder.AppendLine("<xsl:template match=\"/\">");
                    builder.AppendLine("<html>\n<body>");
                    builder.AppendLine($"<h3>{header}</h3>");
                    builder.AppendLine("<p></p>");

                    builder.AppendLine("<table border=\"1\">");
                    //названия столбцов таблицы
                    builder.AppendLine("<tr bgcolor=\"#c1ced5\">");
                    foreach (XmlNode node in xRoot.FirstChild.ChildNodes)
                    {
                        string name = ReplaceSpecialSymbols(node.Name);
                        builder.AppendLine($"<th>{name}</th>");
                    }
                    builder.AppendLine("</tr>");
                    //выборка значений из XML
                    builder.AppendLine($"<xsl:for-each select=\"{xRoot.Name}/{xRoot.FirstChild.Name}\">");
                    builder.AppendLine("<tr align=\"center\">");
                    foreach (XmlNode node in xRoot.FirstChild.ChildNodes)
                    {
                        builder.AppendLine($"<td><xsl:value-of select=\"{node.Name}\"/></td>");
                    }
                    builder.AppendLine("</tr>");
                    builder.AppendLine("</xsl:for-each>");
                    builder.AppendLine("</table>");

                    builder.AppendLine("</body>\n</html>");
                    builder.AppendLine("</xsl:template>");
                    builder.AppendLine("</xsl:stylesheet>");
                    string str = builder.ToString();
                    using (XmlReader r = XmlReader.Create(new StringReader(xml)))
                    {
                        XslCompiledTransform xmlt = new XslCompiledTransform();
                        xmlt.Load(new XmlTextReader(new StringReader(str)));
                        xmlt.Transform(r, new XmlTextWriter(fileName, Encoding.GetEncoding(1251)));
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private static string ReplaceSpecialSymbols(string input)
        {
            if (input.Contains("_x"))
            {
                string[] parts = input.Split(new [] {"_x", "_"}, StringSplitOptions.None);
                string newStr = parts[0];
                for (int i = 1; i < parts.Length; i += 2)
                {
                    ushort ch = ushort.Parse(parts[i], NumberStyles.AllowHexSpecifier);
                    newStr += (char) ch + parts[i+1];
                }
                return newStr;
            }

            return input;
        }

        /// <summary>
        /// Для защит с группами уставок
        /// </summary>
        /// <param name="mainShema">XSL-схема основной группы уставок</param>
        /// <param name="reserveShema">XSL-схема резервной группы уставок</param>
        /// <param name="config">Сохраняемая (серализируемая) конфигурация</param>
        /// <param name="deviceType">Название устройства</param>
        /// <param name="version">Версия устройства</param>
        /// <returns></returns>
        public static string Export(string mainShema, string reserveShema, object config, string deviceType,
            string version)
        {
            try
            {
                ExportSelectForm exportSelect = new ExportSelectForm();
                string shema = string.Empty;
                _saveDialog.FileName = string.Format("{0} Уставки версия {1}", deviceType, version);
                if (exportSelect.ShowDialog() == DialogResult.OK)
                {
                    switch (exportSelect.Group)
                    {
                        case ExportGroup.MAIN:
                        {
                            shema = mainShema;
                            break;
                        }
                        case ExportGroup.MAIN_RESERVE:
                        {
                            shema = reserveShema;
                            break;
                        }
                    }
                }
                else
                {
                    return string.Empty;
                }
                bool res = false;
                if (_saveDialog.ShowDialog() == DialogResult.OK)
                {
                    string extension = Path.GetExtension(_saveDialog.FileName);
                    if (string.Compare(extension, ".xml", true) == 0)
                    {
                        res = ExportXml(_saveDialog.FileName, config);
                    }
                    if (string.Compare(extension, ".html", true) == 0)
                    {
                        res = ExportHtml(_saveDialog.FileName, shema, config);
                    }
                }
                if (res)
                {
                    return string.Format("Файл {0} успешно сохранён", _saveDialog.FileName);
                }
                return string.Empty;
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка сохранения. "+e.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "Ошибка сохранения";
            }
        }

        public static string ExportGroupMain(string group1, string group2, string group3, string group4, string group5, string group6, object device, string deviceType, string version)
        {
            try
            {
                ExportGroupForm _exportGroup = new ExportGroupForm();
                string shema = string.Empty;
                _saveDialog.FileName = string.Format("{0} Уставки версия {1}", deviceType, version);
                if (_exportGroup.ShowDialog() == DialogResult.OK)
                {
                    switch (_exportGroup.SelectedGroup)
                    {
                        case ExportStruct.GROUP1:
                            {
                                shema = group1;
                                break;
                            }
                        case ExportStruct.GROUP2:
                            {
                                shema = group2;
                                break;
                            }
                        case ExportStruct.GROUP3:
                        {
                            shema = group3;
                            break;
                        }
                        case ExportStruct.GROUP4:
                        {
                            shema = group4;
                            break;
                        }
                        case ExportStruct.GROUP5:
                        {
                            shema = group5;
                            break;
                        }
                        case ExportStruct.GROUP6:
                        {
                            shema = group6;
                            break;
                        }
                    }
                }
                else
                {
                    return string.Empty;
                }

                bool res = false;
                if (_saveDialog.ShowDialog() == DialogResult.OK)
                {
                    var extension = Path.GetExtension(_saveDialog.FileName);
                    if (string.Compare(extension, ".xml", true) == 0)
                    {
                        res = ExportXml(_saveDialog.FileName, device);
                    }
                    if (string.Compare(extension, ".html", true) == 0)
                    {
                        res = ExportHtml(_saveDialog.FileName, shema, device);
                    }
                }

                if (res)
                {
                    return string.Format("Файл {0} успешно сохранён", _saveDialog.FileName);
                }
                return string.Empty;
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "Ошибка сохранения";
            }
        }

        public static string ExportGroupMain(string groupSchema, string fileName, object structure)
        {
            try
            {
                bool res = false;
                _saveDialog.FileName = fileName;
                if (_saveDialog.ShowDialog() == DialogResult.OK)
                {
                    string extension = Path.GetExtension(_saveDialog.FileName);
                    if (string.Compare(extension, ".xml", StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        res = ExportXml(_saveDialog.FileName, structure);
                    }
                    if (string.Compare(extension, ".html", StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        res = ExportHtml(_saveDialog.FileName, groupSchema, structure);
                    }
                }
                if (res)
                {
                    return string.Format("Файл {0} успешно сохранён", _saveDialog.FileName);
                }
                return string.Empty;
            }
            catch (Exception exc)
            {
                MessageBox.Show("Ошибка сохранения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Debug.WriteLine(exc.Message);
                return "Ошибка сохранения";
            }
        }

        /// <summary>
        /// без выбора группы уставок
        /// </summary>
        /// <param name="mainShema"></param>
        /// <param name="device"></param>
        /// <param name="deviceType"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static string ExportHtml(string mainShema,  object device, string deviceType, string version)
        {
            try
            {
                _saveDialog.FileName = string.Format("{0} Уставки версия {1}", deviceType, version);

                bool res = false;
                if (_saveDialog.ShowDialog() == DialogResult.OK)
                {
                    var extension = Path.GetExtension(_saveDialog.FileName);
                    if (string.Compare(extension, ".xml", true) == 0)
                    {
                        res = ExportXml(_saveDialog.FileName, device);
                    }
                    if (string.Compare(extension, ".html", true) == 0)
                    {
                        res = ExportHtml(_saveDialog.FileName, mainShema, device);
                    }
                }

                if (res)
                {
                    return string.Format("Файл {0} успешно сохранён", _saveDialog.FileName);
                }
                return string.Empty;
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "Ошибка сохранения";
            }
        }

        public static bool ExportHtml(string fileName, string shema, object device)
        {
            XmlDocument faultDocument = new XmlDocument();
            XPathNavigator nav = faultDocument.CreateNavigator();

            using (XmlWriter writer = nav.AppendChild())
            {
                XmlSerializer ser = new XmlSerializer(device.GetType());
                ser.Serialize(writer, device);
            }

            XslCompiledTransform xmlt = new XslCompiledTransform();
            xmlt.Load(new XmlTextReader(new StringReader(shema)), new XsltSettings(true, true), new XmlUrlResolver());
            using (XmlTextWriter writter = new XmlTextWriter(fileName, Encoding.GetEncoding(1251)))
            {
                xmlt.Transform(faultDocument, writter);
            }
            return true;
        }


        public static bool ExportXml(string fileName,  object device)
        {
            try
            {
                var faultDocument = new XmlDocument();
                var nav = faultDocument.CreateNavigator();

                using (var writer = nav.AppendChild())
                {
                    var ser = new XmlSerializer(device.GetType());
                    ser.Serialize(writer, device);
                }
                faultDocument.Save(fileName);
                return true;
            }
            catch (InvalidOperationException e)
            {
                Debug.Write(e.Message);
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void ExportJournal(DataTable dataTable, string fileName, string shema, string DeviceVersion, string DeviceType, string DeviceNumber, string DevicePlant)
        {
            try
            {
                XmlDocument xDocument = new XmlDocument();
                XmlElement dataTableNode = xDocument.CreateElement("JOURNAL_TABLE");
                XslCompiledTransform xmlt = new XslCompiledTransform();
                xmlt.Load(new XmlTextReader(new StringReader(shema)), new XsltSettings(true, true), new XmlUrlResolver());
                using (XmlTextWriter writter = new XmlTextWriter(fileName, Encoding.GetEncoding(1251)))
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        XmlElement signalsCount = xDocument.CreateElement("ЗАПИСЬ_ЖУРНАЛА");

                        for (int j = 0; j < dataTable.Columns.Count; j++)
                        {
                            XmlElement nameCaption = xDocument.CreateElement(dataTable.Columns[j].Caption);
                            XmlText signalsText = xDocument.CreateTextNode(dataTable.Rows[i].ItemArray[j].ToString());

                            nameCaption.AppendChild(signalsText);
                            signalsCount.AppendChild(nameCaption);
                        }

                        dataTableNode.AppendChild(signalsCount);
                    }

                    XmlElement deviceInfo = xDocument.CreateElement("deviceInfo");
                    XmlElement deviceVersion = xDocument.CreateElement("deviceVersion");
                    XmlElement deviceType = xDocument.CreateElement("deviceType");
                    XmlElement deviceNumber = xDocument.CreateElement("deviceNumber");
                    XmlElement devicePlant = xDocument.CreateElement("devicePlant");
                    XmlText deviceVersionText = xDocument.CreateTextNode(DeviceVersion);
                    XmlText deviceTypeText = xDocument.CreateTextNode(DeviceType);
                    XmlText deviceNumberText = xDocument.CreateTextNode(Convert.ToString(DeviceNumber));
                    XmlText devicePlantText = xDocument.CreateTextNode(DevicePlant);
                    deviceVersion.AppendChild(deviceVersionText);
                    deviceType.AppendChild(deviceTypeText);
                    deviceNumber.AppendChild(deviceNumberText);
                    devicePlant.AppendChild(devicePlantText);
                    deviceInfo.AppendChild(deviceVersion);
                    deviceInfo.AppendChild(deviceType);
                    deviceInfo.AppendChild(deviceNumber);
                    deviceInfo.AppendChild(devicePlant);
                    dataTableNode.AppendChild(deviceInfo);
                    xDocument.AppendChild(dataTableNode);
                    xmlt.Transform(xDocument, writter);
                }
                return;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }
    }
}
