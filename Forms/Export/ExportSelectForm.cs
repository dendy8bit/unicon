﻿using System;
using System.Windows.Forms;

namespace BEMN.Forms.Export
{
    public partial class ExportSelectForm : Form
    {
        public ExportGroup Group { get; private set; }

        public ExportSelectForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Group = ExportGroup.MAIN;
            DialogResult = DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Group = ExportGroup.MAIN_RESERVE;
            DialogResult = DialogResult.OK;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
