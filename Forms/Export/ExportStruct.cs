﻿namespace BEMN.Forms.Export
{
    public enum ExportStruct
    {
        ALL = 0,
        GROUP1 = 1,
        GROUP2 = 2,
        GROUP3 = 3,
        GROUP4 = 4, 
        GROUP5 = 5,
        GROUP6 = 6
    }
}
