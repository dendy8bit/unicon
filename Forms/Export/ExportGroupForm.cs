﻿using System;
using System.Drawing;
using System.Net.Mime;
using System.Windows.Forms;

namespace BEMN.Forms.Export
{
    public partial class ExportGroupForm : Form
    {
        public ExportStruct SelectedGroup { get; private set; }
        private Button[] _buttons;

        private bool _isExport;
        private Point _button2Point;
        private Point _button7Point;
        private Point _button8Point;
        private Size _button7Size;
        private Size _button8Size;
        private Size _mainSize;

        public ExportGroupForm()
        {
            this.InitializeComponent();
        }

        public ExportGroupForm(int groupCount)
        {
            this.InitializeComponent();
            this._buttons = new[] { this.button1, this.button2, this.button3, this.button4, this.button5, this.button6 };
            for (int i = groupCount; i < this._buttons.Length; i++)
            {
                this._buttons[i].Visible = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int index = Convert.ToInt32((sender as Button).Tag);
            switch (index)
            {
                case 0:
                    this.SelectedGroup = ExportStruct.ALL;
                    break;
                case 1:
                    this.SelectedGroup = ExportStruct.GROUP1;
                    break;
                case 2:
                    this.SelectedGroup = ExportStruct.GROUP2;
                    break;
                case 3: 
                    this.SelectedGroup = ExportStruct.GROUP3;
                    break;
                case 4: 
                    this.SelectedGroup = ExportStruct.GROUP4;
                    break;
                case 5: 
                    this.SelectedGroup = ExportStruct.GROUP5;
                    break;
                case 6: 
                    this.SelectedGroup = ExportStruct.GROUP6;
                    break;
                default :
                    this.SelectedGroup = ExportStruct.ALL;
                    break;
            }
            DialogResult = DialogResult.OK;
        }

        public bool IsExport
        {
            get => _isExport;
            set
            {
                if (_isExport == value) return;

                this._isExport = value;

                this.button3.Visible = !this._isExport;
                this.button4.Visible = !this._isExport;
                this.button5.Visible = !this._isExport;
                this.button6.Visible = !this._isExport;

                if (_isExport)
                {
                    this.button2.Location = new Point(83, 12);

                    this.button8.Size = new Size(136, 23);
                    this.button8.Location = new Point(12, 41);

                    this.button7.Size = new Size(136, 23);
                    this.button7.Location = new Point(12, 70);

                    this.Size = new Size(166, 133);
                    this.Text = "Сохранение гр. уст.";

                }
                else
                {
                    this.button2.Location = this._button2Point;

                    this.button8.Size = this._button8Size;
                    this.button8.Location = this._button8Point;

                    this.button7.Size = this._button7Size;
                    this.button7.Location = this._button7Point;

                    this.Size = this._mainSize;
                    this.Text = "Сохранение групп уставок";
                }

            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }

}
