﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using BEMN.Devices.Structures;
using BEMN.MBServer;

namespace BEMN.Forms
{
    public partial class DateTimeControl : UserControl
    {
        private const string INVALID_DATE_TIME = "Некорректное значение даты или времени";
        private bool _timeChange;

        private DateTimeStruct _dateTime;

        public event Action TimeChanged;

        public DateTimeControl()
        {
            InitializeComponent();
            this.DateTime = new DateTimeStruct();
        }

        [Browsable(false)]
        public DateTimeStruct DateTime
        {
            get { return _dateTime; }
            set
            {
                if (this._timeChange || value == null) return;
                _dateTime = value;
                this._dateClockTB.Text = this._dateTime.Date;
                this._timeClockTB.Text = this._dateTime.Time;
            }
        }
        
        private void _stopCB_CheckedChanged(object sender, EventArgs e)
        {
            this._writeDateTimeButt.Enabled = this._timeChange = _stopCB.Checked;
            this._dateClockTB.ReadOnly = this._timeClockTB.ReadOnly = !_stopCB.Checked;
        }

        private void _dateTimeNowButt_Click(object sender, EventArgs e)
        {
            var dateTimeNow = new DateTimeStruct();
            dateTimeNow.SetDateTimeNow();
            this._dateTime = dateTimeNow;
            this.OnRaiseTimeChanged();
        }

        private void _writeDateTimeButt_Click(object sender, EventArgs e)
        {
            try
            {
                var dateElements = this._dateClockTB.Text.Split(',','.');
                var timeElements = this._timeClockTB.Text.Split(',', '.',':');
                var day = ushort.Parse(dateElements[0]);
                var month = ushort.Parse(dateElements[1]);
                var year = ushort.Parse(dateElements[2]);
                var hour = ushort.Parse(timeElements[0]);
                var minute = ushort.Parse(timeElements[1]);
                var second = ushort.Parse(timeElements[2]);
                var millisecond = ushort.Parse(timeElements[3]);
                if ((day > 31) |
                    (month > 12) |
                    (year > 99) |
                    (hour > 23) |
                    (minute > 59) |
                    (second > 59) |
                    (millisecond > 99))
                    throw new ArgumentException();

                var dateTime = new DateTimeStruct();
                var timeArray = new[] { year, month, day, hour, minute, second, millisecond };
                dateTime.InitStruct(Common.TOBYTES(timeArray, false));
                this._dateTime = dateTime;
                this.OnRaiseTimeChanged();
            }
            catch
            {
                MessageBox.Show(INVALID_DATE_TIME);
            }
            
        }

        private void OnRaiseTimeChanged()
        {
            if(TimeChanged != null)
                TimeChanged.Invoke();
        }
    }
}
