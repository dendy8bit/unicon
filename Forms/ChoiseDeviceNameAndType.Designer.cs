﻿namespace BEMN.Forms
{
    partial class ChoiseDeviceNameAndType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._deviceLabel = new System.Windows.Forms.Label();
            this._configLabel = new System.Windows.Forms.Label();
            this._deviceNameCB = new System.Windows.Forms.ComboBox();
            this._deviceConfigCB = new System.Windows.Forms.ComboBox();
            this._okButton = new System.Windows.Forms.Button();
            this._versionLabel = new System.Windows.Forms.Label();
            this._deviceVersionCB = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // _deviceLabel
            // 
            this._deviceLabel.AutoSize = true;
            this._deviceLabel.Location = new System.Drawing.Point(12, 21);
            this._deviceLabel.Name = "_deviceLabel";
            this._deviceLabel.Size = new System.Drawing.Size(67, 13);
            this._deviceLabel.TabIndex = 0;
            this._deviceLabel.Text = "Устройство";
            // 
            // _configLabel
            // 
            this._configLabel.AutoSize = true;
            this._configLabel.Location = new System.Drawing.Point(12, 75);
            this._configLabel.Name = "_configLabel";
            this._configLabel.Size = new System.Drawing.Size(98, 13);
            this._configLabel.TabIndex = 1;
            this._configLabel.Text = "Аппаратная часть";
            // 
            // _deviceNameCB
            // 
            this._deviceNameCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._deviceNameCB.FormattingEnabled = true;
            this._deviceNameCB.Location = new System.Drawing.Point(142, 18);
            this._deviceNameCB.Name = "_deviceNameCB";
            this._deviceNameCB.Size = new System.Drawing.Size(96, 21);
            this._deviceNameCB.TabIndex = 2;
            this._deviceNameCB.SelectedIndexChanged += new System.EventHandler(this._deviceNameCB_SelectedIndexChanged);
            // 
            // _deviceConfigCB
            // 
            this._deviceConfigCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._deviceConfigCB.FormattingEnabled = true;
            this._deviceConfigCB.Location = new System.Drawing.Point(142, 72);
            this._deviceConfigCB.Name = "_deviceConfigCB";
            this._deviceConfigCB.Size = new System.Drawing.Size(176, 21);
            this._deviceConfigCB.TabIndex = 3;
            // 
            // _okButton
            // 
            this._okButton.Location = new System.Drawing.Point(126, 104);
            this._okButton.Name = "_okButton";
            this._okButton.Size = new System.Drawing.Size(75, 23);
            this._okButton.TabIndex = 4;
            this._okButton.Text = "Принять";
            this._okButton.UseVisualStyleBackColor = true;
            this._okButton.Click += new System.EventHandler(this._okButton_Click);
            // 
            // _versionLabel
            // 
            this._versionLabel.AutoSize = true;
            this._versionLabel.Location = new System.Drawing.Point(12, 48);
            this._versionLabel.Name = "_versionLabel";
            this._versionLabel.Size = new System.Drawing.Size(44, 13);
            this._versionLabel.TabIndex = 5;
            this._versionLabel.Text = "Версия";
            // 
            // _deviceVersionCB
            // 
            this._deviceVersionCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._deviceVersionCB.FormattingEnabled = true;
            this._deviceVersionCB.Location = new System.Drawing.Point(142, 45);
            this._deviceVersionCB.Name = "_deviceVersionCB";
            this._deviceVersionCB.Size = new System.Drawing.Size(131, 21);
            this._deviceVersionCB.TabIndex = 6;
            this._deviceVersionCB.SelectedIndexChanged += new System.EventHandler(this._deviceVersionCB_SelectedIndexChanged);
            // 
            // ChoiseDeviceNameAndType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(330, 139);
            this.ControlBox = false;
            this.Controls.Add(this._deviceVersionCB);
            this.Controls.Add(this._versionLabel);
            this.Controls.Add(this._okButton);
            this.Controls.Add(this._deviceConfigCB);
            this.Controls.Add(this._deviceNameCB);
            this.Controls.Add(this._configLabel);
            this.Controls.Add(this._deviceLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChoiseDeviceNameAndType";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Выбор устройства";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _deviceLabel;
        private System.Windows.Forms.Label _configLabel;
        private System.Windows.Forms.ComboBox _deviceNameCB;
        private System.Windows.Forms.ComboBox _deviceConfigCB;
        private System.Windows.Forms.Button _okButton;
        private System.Windows.Forms.Label _versionLabel;
        private System.Windows.Forms.ComboBox _deviceVersionCB;
    }
}