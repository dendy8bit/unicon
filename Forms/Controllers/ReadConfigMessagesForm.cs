﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace BEMN.Forms.Controllers
{
    public partial class ReadConfigMessagesForm : Form
    {
        private int _itemIndex;

        private const string Success = "Загружено успешно";
        private const string Failure = "Невозможно загрузить";

        public ReadConfigMessagesForm(int max)
        {
            InitializeComponent();
            SetMaxProgressBar(max);
            messagesListView.View = View.Details;

            messagesListView.Columns.Add("Название запроса", -2, HorizontalAlignment.Left);
            messagesListView.Columns.Add("Статус", -2, HorizontalAlignment.Left);
        }

        public void ShowDialog(List<string> queryNames)
        {
            foreach (ListViewItem item in queryNames.Select(name => new ListViewItem(name)))
            {
                item.UseItemStyleForSubItems = false;
                messagesListView.Items.Add(item);
            }
            _itemIndex = 0;
            ShowDialog();
        }

        public void SetMaxProgressBar(int max)
        {
            progressBar.Maximum = max;
        }

        public void OperationCommplete(bool success)
        {
            try
            {
                if (success)
                {
                    messagesListView.Items[_itemIndex].SubItems.Add(Success, DefaultForeColor, Color.LightGreen,
                        DefaultFont);
                }
                else
                {
                    messagesListView.Items[_itemIndex].SubItems.Add(Failure, DefaultForeColor, Color.IndianRed,
                        DefaultFont);
                }
                _itemIndex++;
                progressBar.PerformStep();
                okBtn.Enabled = progressBar.Value == progressBar.Maximum;
            }
            catch{}
        }

        private void okBtn_Click(object sender, System.EventArgs e)
        {
            messagesListView.Items.Clear();
            Close();
            Dispose();
        }
    }
}
