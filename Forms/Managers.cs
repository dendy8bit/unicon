using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;

namespace BEMN.Forms
{
    public class GridManager
    {
        public static bool ChangeCellDisabling(DataGridView grid, int rowIndex, object disableObject, int compareColumnIndex, params int[] enablingColumnIndexes)
        {
            bool equal = false;

            if (disableObject is string)
            {
                equal = disableObject.ToString() == grid[compareColumnIndex, rowIndex].Value.ToString();
            }
            if (disableObject is bool)
            {
                equal = (bool)disableObject == (bool)grid[compareColumnIndex, rowIndex].Value;
            }
            
            for (int i = 0; i < enablingColumnIndexes.Length; i++)
            {
                int columnIndex = enablingColumnIndexes[i];
                if (equal)
                {
                    grid[columnIndex, rowIndex].ReadOnly = true;
                    grid[columnIndex, rowIndex].Style.BackColor = SystemColors.Control;
                }
                else
                {
                    grid[columnIndex, rowIndex].ReadOnly = false;
                    grid[columnIndex, rowIndex].Style.BackColor = Color.White;
                }
            }
            return equal;
        }
    }

    public class CheckedListBoxManager
    {
        public CheckedListBoxManager() { }
        public CheckedListBoxManager(CheckedListBox list)
        {
            this._checkList = list;
        }
        private CheckedListBox _checkList;

        public CheckedListBox CheckList
        {
            get { return this._checkList; }
            set { this._checkList = value; }
        }

        public BitArray ToBitArray()
        {
            BitArray ret = new BitArray(this._checkList.Items.Count);
            for (int i = 0; i < this._checkList.Items.Count; i++)
            {
                ret[i] = this._checkList.GetItemChecked(i);
            }
            return ret;
        }

        public List<bool> ToBoolList()
        {
            List<bool> ret = new List<bool>();
            for (int i = 0; i < this._checkList.Items.Count; i++)
            {
                ret.Add(this._checkList.GetItemChecked(i));
            }
            return ret;
        }
    }

    public class LedManager
    {
        public static void TurnOffLeds(LedControl[] leds)
        {
            for (int i = 0; i < leds.Length; i++)
            {
                leds[i].State = LedState.Off;
            }
        }

        public static void TurnOffLed(LedControl led)
        {
            led.State = LedState.Off;
        }


        public static bool IsControlsCreated(Control[] ctrls)
        {
            bool ret = true;
            for (int i = 0; i < ctrls.Length; i++)
            {
                if (false == ctrls[i].Created)
                {
                    ret = false;
                    break;
                }
            }
            return ret;
        }

        public static void SetLeds(LedControl[] leds, BitArray bits)
        {
            if (null == bits || null == leds)
            {
                return;
            }

            for (int i = 0; i < leds.Length; i++)
            {
                leds[i].State = bits[i] ? LedState.NoSignaled : LedState.Signaled;
            }
        }

        public static void SetLeds(LedControl[] leds, bool[] bits)
        {
            if (bits == null || leds == null)
            {
                return;
            }
            for (int i = 0; i < leds.Length; i++)
            {
                leds[i].State = bits[i] ? LedState.NoSignaled : LedState.Signaled;
            }
        }

        public static void SetLed(LedControl led, bool bit)
        {
            if (null == led) return;
            led.State = bit ? LedState.NoSignaled : LedState.Signaled;
        }

        public static void SetLed(LedControl led, bool? bit)
        {
            if (null == led) return;
            switch (bit)
            {
                case true:
                    led.State = LedState.NoSignaled;
                    break;
                case false:
                    led.State = LedState.Signaled;
                    break;
                default:
                    led.State = LedState.Off;
                    break;
            }
        }

        public static void SetLedsReverse(LedControl[] leds, BitArray bits)
        {
            if (null == bits || null == leds)
            {
                return;
            }

            for (int i = 0; i < leds.Length; i++)
            {
                leds[i].State = bits[leds.Length - i - 1] ? LedState.NoSignaled : LedState.Signaled;
            }
        }

    }
}
