using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Framework.Properties;
using BEMN.Interfaces;

namespace BEMN.MR730
{    
    public partial class AlarmJournalForm : Form , IFormView
    {
        private MR730 _device;

        public AlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public AlarmJournalForm(MR730 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._journalProgress.Maximum = MR730.ALARMJOURNAL_RECORD_CNT;
            this._device.AlarmJournalLoadOk += HandlerHelper.CreateHandler(this, this.OnAlarmJournalLoadOk);
            this._device.AlarmJournalRecordLoadOk += new IndexHandler(this._device_AlarmJournalRecordLoadOk);
            this._device.AlarmJournalRecordLoadFail += new IndexHandler(this._device_AlarmJournalRecordLoadFail);
        }
        
        private void OnAlarmJournalLoadOk()
        {
            this._readBut.Enabled = true;
            if (this._journalGrid.Rows.Count == 0) 
            {
                MessageBox.Show("������ ������ ����");
            }
        }
        
        void _device_AlarmJournalRecordLoadFail(object sender, int index)
        {
            MessageBox.Show("���������� ��������� ������ ������. ��������� �����.", "������ - ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        void _device_AlarmJournalRecordLoadOk(object sender, int index)
        {
            try
            {
                Invoke(new OnDeviceIndexEventHandler(this.OnAlarmJournalRecordLoadOk), new object[] { index });
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnAlarmJournalRecordLoadOk(int i)
        {
            this._journalProgress.Increment(1);
            this._journalCntLabel.Text = (i + 1).ToString();
            this._journalGrid.Rows.Add(i + 1,
                this._device.AlarmJournal[i].time, 
                this._device.AlarmJournal[i].msg,
                this._device.AlarmJournal[i].code,
                this._device.AlarmJournal[i].type_value,
                this._device.AlarmJournal[i].Ia,
                this._device.AlarmJournal[i].Ib,
                this._device.AlarmJournal[i].Ic,
                this._device.AlarmJournal[i].I0,
                this._device.AlarmJournal[i].I1,
                this._device.AlarmJournal[i].I2,
                this._device.AlarmJournal[i].In,
                this._device.AlarmJournal[i].Ig,
                this._device.AlarmJournal[i].F,
                this._device.AlarmJournal[i].Uab,
                this._device.AlarmJournal[i].Ubc,
                this._device.AlarmJournal[i].Uca,
                this._device.AlarmJournal[i].U0,
                this._device.AlarmJournal[i].U1,
                this._device.AlarmJournal[i].U2,
                this._device.AlarmJournal[i].Un,
                this._device.AlarmJournal[i].inSignals1,
                this._device.AlarmJournal[i].inSignals2);
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR730); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(AlarmJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get 
            {
                return Resources.ja; 
            }
        }

        public string NodeName
        {
            get { return "������ ������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _readBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;

            this._device.AlarmJournal = new MR730.CAlarmJournal(this._device);
            this._device.LoadInputSignals();
            this._readBut.Enabled = false;
            this._device.RemoveAlarmJournal();
            this._journalProgress.Value = 0;
            this._journalGrid.Rows.Clear();
            this._device.LoadAlarmJournal();
        }

        private void AlarmJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
           this._device.RemoveAlarmJournal();
        }

        private void AlarmJournalForm_Activated(object sender, EventArgs e)
        {
            this._device.SuspendAlarmJournal(false);
        }

        private void AlarmJournalForm_Deactivate(object sender, EventArgs e)
        {
            this._device.SuspendAlarmJournal(true);
        }

        private void _serializeBut_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable("��73X_������_������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");
            table.Columns.Add("��� �����������");
            table.Columns.Add("��� �����������");
            table.Columns.Add("Ia");
            table.Columns.Add("Ib");
            table.Columns.Add("Ic");
            table.Columns.Add("I0");
            table.Columns.Add("I1");
            table.Columns.Add("I2");
            table.Columns.Add("Ig");
            table.Columns.Add("In");
            table.Columns.Add("F");
            table.Columns.Add("Uab");
            table.Columns.Add("Ubc");
            table.Columns.Add("Uca");
            table.Columns.Add("U0");
            table.Columns.Add("U1");
            table.Columns.Add("U2");
            table.Columns.Add("Un");
            table.Columns.Add("��.������� 8-1");
            table.Columns.Add("��.������� 9-16");
               
            List<object> row = new List<object>();

            for (int i = 0; i < this._journalGrid.Rows.Count; i++)
            {
                row.Clear();
                for (int j = 0; j < this._journalGrid.Columns.Count; j++)
                {
                    row.Add(this._journalGrid[j,i].Value);
                }   
                table.Rows.Add(row.ToArray());
            }
            if (DialogResult.OK == this._saveSysJournalDlg.ShowDialog())
            {
                table.WriteXml(this._saveSysJournalDlg.FileName);
            }
        }

        private void _deserializeBut_Click(object sender, EventArgs e)
        {            
            DataTable table = new DataTable("��73X_������_������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");
            table.Columns.Add("��� �����������");
            table.Columns.Add("��� �����������");
            table.Columns.Add("Ia");
            table.Columns.Add("Ib");
            table.Columns.Add("Ic");
            table.Columns.Add("I0");
            table.Columns.Add("I1");
            table.Columns.Add("I2");
            table.Columns.Add("Ig"); 
            table.Columns.Add("In");
            table.Columns.Add("F");
            table.Columns.Add("Uab");
            table.Columns.Add("Ubc");
            table.Columns.Add("Uca");
            table.Columns.Add("U0");
            table.Columns.Add("U1");
            table.Columns.Add("U2");
            table.Columns.Add("Un");
            table.Columns.Add("��.������� 8-1");
            table.Columns.Add("��.������� 9-16");

            if (DialogResult.OK == this._openSysJounralDlg.ShowDialog())
            {
                this._journalGrid.Rows.Clear();
                table.ReadXml(this._openSysJounralDlg.FileName);
            }

            List<object> row = new List<object>();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                row.Clear();
                row.AddRange(table.Rows[i].ItemArray);
                this._journalGrid.Rows.Add(row.ToArray());
            }
        }
    }
}