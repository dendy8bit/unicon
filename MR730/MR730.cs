using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Forms.Old;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MBServer.Queries;
using CategoryAttribute=System.ComponentModel.CategoryAttribute;
using DescriptionAttribute=System.ComponentModel.DescriptionAttribute;

namespace BEMN.MR730
{
    public class MR730 : Device, IDeviceView
    {
        public const ulong TIMELIMIT = 3000000;
        public const ulong KZLIMIT = 4000;
        public const string TIMELIMIT_ERROR_MSG = "������� ����� � ��������� [0 - 3000000]";
        public const string KZLIMIT_ERROR_MSG = "������� ����� � ��������� [0 - 4000]";


        #region ��������� ������

        public const int SYSTEMJOURNAL_RECORD_CNT = 128;

        [Browsable(false)]
        [XmlIgnore]
        public CSystemJournal SystemJournal
        {
            get { return this._systemJournalRecords; }
            set { this._systemJournalRecords = value; }
        }

        public struct SystemRecord
        {
            public string msg;
            public string time;
        } ;

        public class CSystemJournal : ICollection
        {
            private List<SystemRecord> _messages = new List<SystemRecord>(SYSTEMJOURNAL_RECORD_CNT);

            public int Count
            {
                get { return this._messages.Count; }
            }

            public SystemRecord this[int i]
            {
                get { return this._messages[i]; }
            }

            public bool IsMessageEmpty(int i)
            {
                return "������ ����" == this._messages[i].msg;
            }

            public bool AddMessage(ushort[] value)
            {
                SystemRecord msg = CreateMessage(value);
                if ("������ ����" == msg.msg)
                {
                    return false;
                }
                else
                {
                    this._messages.Add(msg);
                    return true;
                }
            }

            private static SystemRecord CreateMessage(ushort[] value)
            {
                SystemRecord msg = new SystemRecord();

                switch (value[0])
                {
                    case 0:
                        msg.msg = "������ ����";
                        break;
                    case 1:
                    case 2:
                        msg.msg = "������ �������� ������";
                        break;
                    case 3:
                        msg.msg = "������������� ��. ����";
                        break;
                    case 4:
                        msg.msg = "��. ���� ��������";
                        break;
                    case 5:
                        msg.msg = "����������� ���� �����";
                        break;
                    case 6:
                        msg.msg = "����������� � �����";
                        break;
                    case 7:
                        msg.msg = "��� 2 ���������� (����)";
                        break;
                    case 8:
                        msg.msg = "��� 2 �������� (����)";
                        break;
                    case 9:
                        msg.msg = "��� 1 ���������� (����)";
                        break;
                    case 10:
                        msg.msg = "��� 1 �������� (����)";
                        break;
                    case 11:
                        msg.msg = "��� ����������";
                        break;
                    case 12:
                        msg.msg = "��� ��������";
                        break;
                    case 13:
                        msg.msg = "���1 ����������";
                        break;
                    case 14:
                        msg.msg = "���1 ��������";
                        break;
                    case 15:
                        msg.msg = "���2 ����������";
                        break;
                    case 16:
                        msg.msg = "���2 ��������";
                        break;
                    case 17:
                        msg.msg = "������ ����������� ����� �������";
                        break;
                    case 18:
                    case 19:
                        msg.msg = "������ ����������� ����� ������";
                        break;
                    case 20:
                        msg.msg = "������ ������� �������";
                        break;
                    case 21:
                        msg.msg = "������ ������� ������";
                        break;
                    case 22:
                        msg.msg = "��������� �����";
                        break;
                    case 25:
                        msg.msg = "���� - ������� ��������";
                        break;
                    case 26:
                        msg.msg = "������ �������";
                        break;
                    case 27:
                        msg.msg = "����� ������� �������";
                        break;
                    case 28:
                        msg.msg = "����� ������� ������";
                        break;
                    case 29:
                        msg.msg = "����� ������� �����������";
                        break;
                    case 32:
                        msg.msg = "���� � ������� ��������";
                        break;
                    case 33:
                        msg.msg = "������ ��������� ����������";
                        break;
                    case 34:
                        msg.msg = "������� ����������";
                        break;
                    case 35:
                        msg.msg = "���������� ���������";
                        break;
                    case 36:
                        msg.msg = "���������� ��������";
                        break;
                    case 38:
                        msg.msg = "���� ����� ������������ ";
                        break;
                    case 39:
                        msg.msg = "���� � ����� ������������ ";
                        break;
                    case 46:
                        msg.msg = "�������� ���  Iabc";
                        break;
                    case 47:
                        msg.msg = "������ ���  Iabc";
                        break;
                    case 48:
                        msg.msg = "�����������  Iabc";
                        break;
                    case 49:
                        msg.msg = "���������  Iabc";
                        break;
                    case 50:
                        msg.msg = "�� ����. �������������";
                        break;
                    case 51:
                        msg.msg = "�� ��������";
                        break;
                    case 56:
                        msg.msg = "U��� < 5�";
                        break;
                    case 57:
                        msg.msg = "U��� > 5�";
                        break;
                    case 58:
                        msg.msg = "���� ����. �������������";
                        break;
                    case 59:
                        msg.msg = "���� ��������";
                        break;
                    case 60:
                        msg.msg = "������� ��� ���������";
                        break;
                    case 61:
                        msg.msg = "������� � �����";
                        break;
                    case 62:
                        msg.msg = "����������� ��������";
                        break;
                    case 63:
                        msg.msg = "����������� �������";
                        break;
                    case 64:
                        msg.msg = "���������� �����������";
                        break;
                    case 65:
                        msg.msg = "����� �����������";
                        break;
                    case 66:
                        msg.msg = "������������� �����������";
                        break;
                    case 67:
                        msg.msg = "����.������. �����������";
                        break;
                    case 68:
                        msg.msg = "������.�����. �����������";
                        break;
                    case 69:
                        msg.msg = "������ ����";
                        break;
                    case 71:
                        msg.msg = "������ ���������";
                        break;
                    case 72:
                        msg.msg = "��� �����������";
                        break;
                    case 73:
                        msg.msg = "��� ��.����������";
                        break;
                    case 74:
                        msg.msg = "������ ��� 1 ����";
                        break;
                    case 75:
                        msg.msg = "������ ��� 2 ����";
                        break;
                    case 76:
                        msg.msg = "������ ��� 3 ����";
                        break;
                    case 77:
                        msg.msg = "������ ��� 4 ����";
                        break;
                    case 78:
                        msg.msg = "��� ��������";
                        break;
                    case 85:
                        msg.msg = "A�� ����������";
                        break;
                    case 86:
                        msg.msg = "��� ����. ����������";
                        break;
                    case 87:
                        msg.msg = "��� ����������";
                        break;
                    case 88:
                        msg.msg = "��� ���������";
                        break;
                    case 89:
                        msg.msg = "��� ��������";
                        break;
                    case 90:
                        msg.msg = "��� ���. ������";
                        break;
                    case 91:
                        msg.msg = "��� ����. ������";
                        break;
                    case 92:
                        msg.msg = "��� ������ �� ������";
                        break;
                    case 93:
                        msg.msg = "��� ������ ������� ����.";
                        break;
                    case 94:
                        msg.msg = "��� ������ �� �������";
                        break;
                    case 95:
                        msg.msg = "��� ������ ��������.";
                        break;
                    case 96:
                        msg.msg = "������ ���������";
                        break;
                    case 97:
                        msg.msg = "������ ��������";
                        break;
                    case 98:
                        msg.msg = "���� ���������";
                        break;
                    case 99:
                        msg.msg = "���� ��������";
                        break;
                    case 100:
                        msg.msg = "������� ���������";
                        break;
                    case 101:
                        msg.msg = "������� ��������";
                        break;
                    case 102:
                        msg.msg = "���� ���������";
                        break;
                    case 103:
                        msg.msg = "���� ��������";
                        break;
                    case 104:
                        msg.msg = "�������� �������";
                        break;
                    case 105:
                        msg.msg = "��������� �������";
                        break;
                    case 106:
                        msg.msg = "����.������. �������";
                        break;
                    case 108:
                        msg.msg = "����-�������� �������";
                        break;
                    case 109:
                        msg.msg = "����-��������� �������";
                        break;
                    case 110:
                        msg.msg = "����-�������� �������";
                        break;
                    case 111:
                        msg.msg = "����-��������� �������";
                        break;
                    case 112:
                        msg.msg = "��� �������";
                        break;
                    case 113:
                        msg.msg = "��� ������� F>";
                        break;
                    case 114:
                        msg.msg = "��� ������� F>>";
                        break;
                    case 115:
                        msg.msg = "��� ������� F<";
                        break;
                    case 116:
                        msg.msg = "��� ������� F<<";
                        break;
                    case 117:
                        msg.msg = "��� ������� U>";
                        break;
                    case 118:
                        msg.msg = "��� ������� U>>";
                        break;
                    case 119:
                    case 120:
                        msg.msg = "��� ������� U<<";
                        break;
                    case 121:
                        msg.msg = "��� ������� U2>";
                        break;
                    case 122:
                        msg.msg = "��� ������� U2>>";
                        break;
                    case 123:
                        msg.msg = "��� ������� Uo>";
                        break;
                    case 124:
                        msg.msg = "��� ������� Uo>";
                        break;
                    case 125:
                        msg.msg = "��� ������� ��-1";
                        break;
                    case 126:
                        msg.msg = "��� ������� ��-2";
                        break;
                    case 127:
                        msg.msg = "��� ������� ��-3";
                        break;
                    case 128:
                        msg.msg = "��� ������� ��-4";
                        break;
                    case 129:
                        msg.msg = "��� ������� ��-5";
                        break;
                    case 130:
                        msg.msg = "��� ������� ��-6";
                        break;
                    case 131:
                        msg.msg = "��� ������� ��-7";
                        break;
                    case 132:
                        msg.msg = "��� ������� ��-8";
                        break;
                    case 133:
                        msg.msg = "U<10B ������� ������������";
                        break;
                    case 134:
                        msg.msg = "U>10B ������� ����������";
                        break;
                    case 135:
                        msg.msg = "��� ���� ����������";
                        break;
                    case 136:
                        msg.msg = "��� ���� ����������";
                        break;
                    case 137:
                        msg.msg = "���������� �� ���������";
                        break;
                    case 138:
                        msg.msg = "���������� �� ����� ������";
                        break;
                    case 139:
                        msg.msg = "��. � ����� ��������� ���������";
                        break;
                    case 140:
                        msg.msg = "��. � ����� ����� ������";
                        break;
                    case 141:
                        msg.msg = "���� � ����� ��������� ���������";
                        break;
                    case 142:
                        msg.msg = "���� � ����� ����� ������";
                        break;
                    case 143:
                        msg.msg = "���� � ����� ��������� ���������";
                        break;
                    case 144:
                        msg.msg = "���� � ����� ����� ������";
                        break;
                }
                msg.time = value[3].ToString("d2") + "-" + value[2].ToString("d2") + "-" + value[1].ToString("d2") + " " +
                           value[4].ToString("d2") + ":" + value[5].ToString("d2") + ":" + value[6].ToString("d2") + ":" +
                           value[7].ToString("d2");

                return msg;
            }

            #region ICollection Members

            public void CopyTo(Array array, int index)
            {
            }

            public bool IsSynchronized
            {
                get { return false; }
            }

            public object SyncRoot
            {
                get { return this._messages; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._messages.GetEnumerator();
            }

            #endregion
        }

        #endregion

        #region ������ ������

        
        public const int ALARMJOURNAL_RECORD_CNT = 32;

        [Browsable(false)]
        public CAlarmJournal AlarmJournal
        {
            get { return this._alarmJournalRecords; }
            set { this._alarmJournalRecords = value; }
        }

        public struct AlarmRecord
        {
            public string time;
            public string msg;
            public string code;
            public string type_value;
            public string Ia;
            public string Ib;
            public string Ic;
            public string I0;
            public string I1;
            public string I2;
            public string In;
            public string Ig;
            public string F;
            public string Uab;
            public string Ubc;
            public string Uca;
            public string U0;
            public string U1;
            public string U2;
            public string Un;
            public string inSignals1;
            public string inSignals2;
        } ;

        public class CAlarmJournal
        {
            private Dictionary<ushort, string> _msgDictionary;
            private Dictionary<byte, string> _codeDictionary;
            private Dictionary<byte, string> _typeDictionary;
            private MR730 _device;

            public CAlarmJournal()
            {
 
            }

            public CAlarmJournal(MR730 device)
            {
                this._device = device;
                this._msgDictionary = new Dictionary<ushort, string>(6);
                this._codeDictionary = new Dictionary<byte, string>(31);
                this._typeDictionary = new Dictionary<byte, string>(26);

                this._msgDictionary.Add(0, "������ ����");
                this._msgDictionary.Add(1, "������������");
                this._msgDictionary.Add(2, "����������");
                this._msgDictionary.Add(3, "������");
                this._msgDictionary.Add(4, "���������� ���");
                this._msgDictionary.Add(5, "�������");
                this._msgDictionary.Add(6, "���������");
                this._msgDictionary.Add(7, "���");

                this._codeDictionary.Add(0, "");
                this._codeDictionary.Add(1, "I>");
                this._codeDictionary.Add(2, "I>>");
                this._codeDictionary.Add(3, "I>>>");
                this._codeDictionary.Add(4, "I>>>>");
                this._codeDictionary.Add(5, "I2>");
                this._codeDictionary.Add(6, "I2>>");
                this._codeDictionary.Add(7, "I0>");
                this._codeDictionary.Add(8, "I0>>");
                this._codeDictionary.Add(9, "In>");
                this._codeDictionary.Add(10, "In>>");
                this._codeDictionary.Add(11, "I�>");
                this._codeDictionary.Add(12, "I2/I1");
                this._codeDictionary.Add(13, "F>");
                this._codeDictionary.Add(14, "F>>");
                this._codeDictionary.Add(15, "F<");
                this._codeDictionary.Add(16, "F<<");
                this._codeDictionary.Add(17, "U>");
                this._codeDictionary.Add(18, "U>>");
                this._codeDictionary.Add(19, "U<");
                this._codeDictionary.Add(20, "U<<");
                this._codeDictionary.Add(21, "U2>");
                this._codeDictionary.Add(22, "U2>>");
                this._codeDictionary.Add(23, "U0>");
                this._codeDictionary.Add(24, "U0>>");
                this._codeDictionary.Add(25, "Q>");
                this._codeDictionary.Add(26, "Q>>");
                this._codeDictionary.Add(27, "Q���");
                this._codeDictionary.Add(28, "Q����");
                this._codeDictionary.Add(29, "������");
                this._codeDictionary.Add(30, "������");
                this._codeDictionary.Add(31, "������");
                this._codeDictionary.Add(32, "������");
                this._codeDictionary.Add(33, "��-1");
                this._codeDictionary.Add(34, "��-2");
                this._codeDictionary.Add(35, "��-3");
                this._codeDictionary.Add(36, "��-4");
                this._codeDictionary.Add(37, "��-5");
                this._codeDictionary.Add(38, "��-6");
                this._codeDictionary.Add(39, "��-7");
                this._codeDictionary.Add(40, "��-8");

                this._typeDictionary.Add(0, "");
                this._typeDictionary.Add(1, "I�");
                this._typeDictionary.Add(2, "In");
                this._typeDictionary.Add(3, "Ia");
                this._typeDictionary.Add(4, "Ib");
                this._typeDictionary.Add(5, "Ic");
                this._typeDictionary.Add(6, "I0");
                this._typeDictionary.Add(7, "I1");
                this._typeDictionary.Add(8, "I2");
                this._typeDictionary.Add(9, "Pn");
                this._typeDictionary.Add(10, "Pa");
                this._typeDictionary.Add(11, "Pb");
                this._typeDictionary.Add(12, "Pc");
                this._typeDictionary.Add(13, "P0");
                this._typeDictionary.Add(14, "P1");
                this._typeDictionary.Add(15, "P2");
                this._typeDictionary.Add(16, "F");
                this._typeDictionary.Add(17, "Un");
                this._typeDictionary.Add(18, "Ua");
                this._typeDictionary.Add(19, "Ub");
                this._typeDictionary.Add(20, "Uc");
                this._typeDictionary.Add(21, "U0");
                this._typeDictionary.Add(22, "U1");
                this._typeDictionary.Add(23, "U2");
                this._typeDictionary.Add(24, "Uab");
                this._typeDictionary.Add(25, "Ubc");
                this._typeDictionary.Add(26, "Uca");
                this._typeDictionary.Add(27, "����� �������");
                this._typeDictionary.Add(28, "Q");
            }

            private List<AlarmRecord> _messages = new List<AlarmRecord>(ALARMJOURNAL_RECORD_CNT);

            public int Count
            {
                get { return this._messages.Count; }
            }

            public AlarmRecord this[int i]
            {
                get { return this._messages[i]; }
            }

            public bool IsMessageEmpty(int i)
            {
                if (this._messages.Count != i)
                { 
                    return "������ ����" == this._messages[i].msg; 
                }
                else 
                {
                    return true;
                }
            }

            public bool AddMessage(byte[] value)
            {
                bool ret;
                Common.SwapArrayItems(ref value);
                AlarmRecord msg = this.CreateMessage(value);
                if ("������ ����" == msg.msg)
                {
                    ret = false;
                }
                else
                {
                    ret = true;
                    this._messages.Add(msg);
                }
                return ret;
            }

            public string GetMessage(byte b)
            {
                if (b<8)
                {
                    return this._msgDictionary[b];
                }else
                {
                    return this._msgDictionary[7];
                }
                
            }

            public string GetDateTime(byte[] datetime)
            {
                return datetime[4].ToString("d2") + "-" + datetime[2].ToString("d2") + "-" + datetime[0].ToString("d2") +
                           " " +
                           datetime[6].ToString("d2") + ":" + datetime[8].ToString("d2") + ":" + datetime[10].ToString("d2") +
                           ":" + datetime[12].ToString("d2");
            }

            public string GetCode(byte codeByte,byte phaseByte)
            {
                
                string group = (0 == (codeByte & 0x80)) ? "��������" : "������.";
                string phase = "";
                phase += (0 == (phaseByte & 0x08)) ? " " : "_";
                if (phaseByte == 12)
                {
                    phase += (0 == (phaseByte & 0x01)) ? " " : "A";
                    phase += (0 == (phaseByte & 0x02)) ? " " : "B";
                    phase += (0 == (phaseByte & 0x04)) ? " " : "C";
                }
                else if (phaseByte == 14)
                {
                        phase += (0 == (phaseByte & 0x01)) ? " " : "A";
                        phase += (0 == (phaseByte & 0x02)) ? " " : "B";
                        phase += (0 == (phaseByte & 0x04)) ? " " : "C";
                }
                else
                {
                    phase += (0 == (phaseByte & 0x04)) ? " " : "A";
                    phase += (0 == (phaseByte & 0x02)) ? " " : "B";
                    phase += (0 == (phaseByte & 0x01)) ? " " : "C";
                }

                byte code = (byte)(codeByte & 0x7F);
                try
                {
                    return this._codeDictionary[code] + " " + group + " " + phase;    
                }
                catch(KeyNotFoundException)
                {
                    return "";
                }
                
            }

            public string GetTypeValue(ushort damageWord,byte typeByte, string code)
            {
                bool OB = false;
                double damageValue = 0;
                switch (code)
                {
                    case "Ia": 
                    case "Ib":
                    case "Ic":
                    case "I0":
                    case "I1":
                    case "I2": damageValue = Measuring.GetI(damageWord, this._device.TT, true);
                        break;
                    case "Pn": damageValue = Measuring.GetP(damageWord, (ushort)(this._device.TTNP *this._device.TNNP),false);
                        break;
                    case "Pa":
                    case "Pb":
                    case "Pc":
                    case "P0":
                    case "P1":
                    case "P2": damageValue = Measuring.GetP(damageWord, (ushort)(this._device.TT *this._device.TN), true);
                        break;
                    case "In":
                    case "I�": damageValue = Measuring.GetI(damageWord, this._device.TTNP, false);
                        break;
                    case "I2/I1": damageValue = Measuring.GetU(damageWord, 65536);
                        break;
                    case "F":
                    case "Q": damageValue = Measuring.GetConstraintOnly(damageWord, ConstraintKoefficient.K_25600);
                        break;
                    case "Un":
                    case "Ua":
                    case "Ub":
                    case "Uc":
                    case "U0":
                    case "U1":
                    case "U2":
                    case "Uab":
                    case "Ubc":
                    case "Uca": damageValue = Math.Round(Measuring.GetU(damageWord, this._device.TN),1);
                        break;
                    case "����� �������": OB = true;
                        break;
                    default: break;
                }

                try
                {
                    if (OB)
                    {
                        return this._typeDictionary[typeByte];
                    }
                    else
                    {
                        return this._typeDictionary[typeByte] + string.Format(" = {0:F2}", damageValue);
                    }
                }
                catch (KeyNotFoundException)
                {
                    return "";
                }
            }


            private AlarmRecord CreateMessage(byte[] buffer)
            {
                Measuring.SetConsGA = true;
                AlarmRecord rec = new AlarmRecord();
                rec.msg = this.GetMessage(buffer[0]);
                byte[] datetime = new byte[14];
                Array.ConstrainedCopy(buffer,2,datetime,0,14);
                rec.time = this.GetDateTime(datetime);

                string group = (0 == (buffer[16] & 0x80)) ? "��������" : "������.";
                string phase = "";
                phase += (0 == (buffer[18] & 0x08)) ? " " : "_";
                phase += (0 == (buffer[18] & 0x01)) ? " " : "A";
                phase += (0 == (buffer[18] & 0x02)) ? " " : "B";
                phase += (0 == (buffer[18] & 0x04)) ? " " : "C";
                
                byte codeByte = (byte)(buffer[16] & 0x7F);
                if (buffer[0] == 0)
                {
                    rec.code = "";
                }
                else 
                {
                    rec.code = this._codeDictionary[codeByte] + " " + group + " " + phase;
                }
                
                double damageValue =
                Measuring.GetConstraintOnly(Common.TOWORD(buffer[21], buffer[20]), ConstraintKoefficient.K_25600);
                if (buffer[19] == 0)
                {
                    rec.type_value = "";
                }
                else
                {
                    rec.type_value = this.GetTypeValue(Common.TOWORD(buffer[21], buffer[20]), buffer[19], this._typeDictionary[buffer[19]]);
                }

                
                double Ia = Measuring.GetI(Common.TOWORD(buffer[23], buffer[22]), this._device.TT, true);
                double Ib = Measuring.GetI(Common.TOWORD(buffer[25], buffer[24]), this._device.TT, true);
                double Ic = Measuring.GetI(Common.TOWORD(buffer[27], buffer[26]), this._device.TT, true);
                double I0 = Measuring.GetI(Common.TOWORD(buffer[29], buffer[28]), this._device.TT, true);
                double I1 = Measuring.GetI(Common.TOWORD(buffer[31], buffer[30]), this._device.TT, true);
                double I2 = Measuring.GetI(Common.TOWORD(buffer[33], buffer[32]), this._device.TT, true);
                double In = Measuring.GetI(Common.TOWORD(buffer[35], buffer[34]), this._device.TTNP, false);
                double Ig = Measuring.GetI(Common.TOWORD(buffer[37], buffer[36]), this._device.TTNP, false);
                double F = Measuring.GetF(Common.TOWORD(buffer[39], buffer[38]));
                double Uab = Measuring.GetU(Common.TOWORD(buffer[41], buffer[40]), this._device.TN);
                double Ubc = Measuring.GetU(Common.TOWORD(buffer[43], buffer[42]), this._device.TN);
                double Uca = Measuring.GetU(Common.TOWORD(buffer[45], buffer[44]), this._device.TN);
                double U0 = Measuring.GetU(Common.TOWORD(buffer[47], buffer[46]), this._device.TN);
                double U1 = Measuring.GetU(Common.TOWORD(buffer[49], buffer[48]), this._device.TN);
                double U2 = Measuring.GetU(Common.TOWORD(buffer[51], buffer[50]), this._device.TN);
                double Un = Measuring.GetU(Common.TOWORD(buffer[53], buffer[52]), this._device.TNNP);
                Measuring.SetConsGA = false;

                rec.Ia = string.Format("{0:F2}", Ia);
                rec.Ib = string.Format("{0:F2}", Ib);
                rec.Ic = string.Format("{0:F2}", Ic);
                rec.I0 = string.Format("{0:F2}", I0);
                rec.I1 = string.Format("{0:F2}", I1);
                rec.I2 = string.Format("{0:F2}", I2);
                rec.In = string.Format("{0:F2}", In);
                rec.Ig = string.Format("{0:F2}", Ig);
                rec.F = string.Format("{0:F2}", F);
                rec.Uab = string.Format("{0:F2}", Uab);
                rec.Ubc = string.Format("{0:F2}", Ubc);
                rec.Uca = string.Format("{0:F2}", Uca);
                rec.U0 = string.Format("{0:F2}", U0);
                rec.U1 = string.Format("{0:F2}", U1);
                rec.U2 = string.Format("{0:F2}", U2);
                rec.Un = string.Format("{0:F2}", Un);
                byte[] b1 = new byte[] {buffer[54]};
                byte[] b2 = new byte[] {buffer[55]};
                rec.inSignals1 = Common.BitsToString(new BitArray(b1));
                rec.inSignals2 = Common.BitsToString(new BitArray(b2));
                return rec;
            }
        }

        #endregion

        #region ����

        private slot _diagnostic = new slot(0x1800, 0x181C);
        private slot _inputSignals = new slot(0x1000, 0x103C);
        private slot _outputSignals = new slot(0x1200, 0x1270);
        private slot _externalDefenses = new slot(0x1050, 0x1080);

        private slot[] _systemJournal = new slot[SYSTEMJOURNAL_RECORD_CNT];
        private slot[] _alarmJournal = new slot[ALARMJOURNAL_RECORD_CNT];
        private bool _stopAlarmJournal = false;
        private bool _stopSystemJournal = false;
        private CSystemJournal _systemJournalRecords = new CSystemJournal();
        private CAlarmJournal _alarmJournalRecords;
        private COutputRele _outputRele = new COutputRele();

        private slot _konfCount = new slot(0x1274, 0x1275);
        private slot _oscCrush = new slot(0x3800, 0x3801);
        #endregion

        #region ������� 

        public event Handler DiagnosticLoadOk;
        public event Handler DiagnosticLoadFail;
        public event Handler DateTimeLoadOk;
        public event Handler DateTimeLoadFail;
        public event Handler InputSignalsLoadOK;
        public event Handler InputSignalsLoadFail;
        public event Handler InputSignalsSaveOK;
        public event Handler InputSignalsSaveFail;
        public event Handler KonfCountLoadOK;
        public event Handler KonfCountLoadFail;
        public event Handler KonfCountSaveOK;
        public event Handler KonfCountSaveFail;
        public event Handler ExternalDefensesLoadOK;
        public event Handler ExternalDefensesLoadFail;
        public event Handler ExternalDefensesSaveOK;
        public event Handler ExternalDefensesSaveFail;
        public event Handler TokDefensesLoadOK;
        public event Handler TokDefensesLoadFail;
        public event Handler TokDefensesSaveOK;
        public event Handler TokDefensesSaveFail;

        public event Handler AnalogSignalsLoadOK;
        public event Handler AnalogSignalsLoadFail;
        public event Handler SystemJournalLoadOk;
        public event IndexHandler SystemJournalRecordLoadOk;
        public event IndexHandler SystemJournalRecordLoadFail;
        public event Handler AlarmJournalLoadOk;
        public event IndexHandler AlarmJournalRecordLoadOk;
        public event IndexHandler AlarmJournalRecordLoadFail;
        public event Handler OutputSignalsLoadOK;
        public event Handler OutputSignalsLoadFail;
        public event Handler OutputSignalsSaveOK;
        public event Handler OutputSignalsSaveFail;

        #endregion

        #region ������������ �������������

        public MR730()
        {
            this.Init();
        }

        public MR730(Modbus mb)
        {
            this.MB = mb;
            this.Init();
        }

        [XmlIgnore]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        public override Modbus MB
        {
            get { return mb; }
            set
            {
                mb = value;
                if (null != mb)
                {
                    mb.CompleteExchange += this.mb_CompleteExchange;
                }
            }
        }


        private void Init()
        {
            this._oscilloscope = new Oscilloscope(this);
            this._oscilloscope.Initialize();
            HaveVersion = true;
            this._alarmJournalRecords = new CAlarmJournal(this);
            ushort start = 0x2000;
            ushort size = 0x8;
            for (int i = 0; i < SYSTEMJOURNAL_RECORD_CNT; i++)
            {
                this._systemJournal[i] = new slot(start, (ushort) (start + size));
                start += 0x10;
            }
            start = 0x2800;
            size = 0x1C;
            for (int i = 0; i < ALARMJOURNAL_RECORD_CNT; i++)
            {
                this._alarmJournal[i] = new slot(start, (ushort) (start + size));
                start += 0x40;
            }
        }


        #endregion

        #region �������� ����

        public class COutputRele : ICollection
        {
            public const int LENGTH = 16;
            public const int COUNT = 8;

            public COutputRele()
            {
                this.SetBuffer(new ushort[LENGTH]);
            }

            private List<OutputReleItem> _releList = new List<OutputReleItem>(COUNT);

            [DisplayName("����������")]
            public int Count
            {
                get { return this._releList.Count; }
            }

            public void SetBuffer(ushort[] values)
            {
                this._releList.Clear();
                if (values.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Rele values", LENGTH, "Output rele length must be 16");
                }
                for (int i = 0; i < LENGTH; i += 2)
                {
                    this._releList.Add(new OutputReleItem(values[i], values[i + 1]));
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] ret = new ushort[LENGTH];
                for (int i = 0; i < COUNT; i++)
                {
                    ret[i*2] = this._releList[i].Value[0];
                    ret[i*2 + 1] = this._releList[i].Value[1];
                }
                return ret;
            }

            public OutputReleItem this[int i]
            {
                get { return this._releList[i]; }
                set { this._releList[i] = value; }
            }

            #region ICollection Members

            public void Add(OutputReleItem item)
            {
                this._releList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._releList.GetEnumerator();
            }

            #endregion
        } ;

        public class OutputReleItem
        {
            private ushort _hiWord;
            private ushort _loWord;

            public override string ToString()
            {
                return "�������� ����";
            }

            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [CategoryAttribute("������� ���")]
            [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            public ushort[] Value
            {
                get { return new ushort[] {this._hiWord, this._loWord}; }
                set
                {
                    this._hiWord = value[0];
                    this._loWord = value[1];
                }
            }

            public OutputReleItem()
            {
            }

            public OutputReleItem(ushort hiWord, ushort loWord)
            {
                this.SetItem(hiWord, loWord);
            }

            public void SetItem(ushort hiWord, ushort loWord)
            {
                this._hiWord = hiWord;
                this._loWord = loWord;
            }

            [XmlIgnore]
            [Browsable(false)]
            public bool IsBlinker
            {
                get
                {
                    return 0 != Common.HIBYTE(this._hiWord);
                }
                set
                {

                    if (value)
                    {
                        this._hiWord = Common.TOWORD(0xff, Common.LOBYTE(this._hiWord));
                    }
                    else
                    {
                        this._hiWord = Common.TOWORD(0x0, Common.LOBYTE(this._hiWord));
                    }
                }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Description("��� ������� ����")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BlinkerTypeConverter))]
            public string Type
            {
                get
                {
                    string ret = this.IsBlinker ? "�������" : "�����������";
                    return ret;
                }
                set
                {
                    if ("�������" == value)
                    {
                        this.IsBlinker = true;
                    }
                    else if ("�����������" == value)
                    {
                        this.IsBlinker = false;
                    }
                    else
                    {
                        throw new ArgumentException("Rele type undefined", "Type");
                    }
                }
            }

            [XmlIgnore]
            [Browsable(false)]
            public byte SignalByte
            {
                get { return Common.LOBYTE(this._hiWord); }
                set { this._hiWord = Common.TOWORD(Common.HIBYTE(this._hiWord), value); }
            }

            [XmlAttribute("������")]
            [DisplayName("���")]
            [Description("��� ������� ����")]
            [Category("��������� ���")]
            [TypeConverter(typeof (AllSignalsTypeConverter))]
            public string SignalString
            {
                get
                {
                    string ret = Strings.All[Strings.All.Count - 1];
                    if (Common.LOBYTE(this._hiWord) <= Strings.All.Count)
                    {
                        ret = Strings.All[Common.LOBYTE(this._hiWord)];
                    }
                    return ret;
                }
                set
                {
                    byte index = (byte) Strings.All.IndexOf(value);
                    this._hiWord = Common.TOWORD(Common.HIBYTE(this._hiWord), index);
                }
            }

            [XmlAttribute("�������")]
            [DisplayName("�������")]
            [Description("������������ �������� ����")]
            [Category("��������� ���")]
            public ulong ImpulseUlong
            {
                get { return Measuring.GetTime(this._loWord); }
                set { this._loWord = Measuring.SetTime(value); }
            }

            [XmlIgnore]
            [Browsable(false)]
            public ushort ImpulseUshort
            {
                get { return this._loWord; }
                set { this._loWord = value; }
            }
        } ;



        [DisplayName("�������� ����")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("�������� �������")]
        [XmlElement("��������_����")]
        public COutputRele OutputRele
        {
            get { return this._outputRele; }
        }

        #endregion

        #region �������� ����������

        public class COutputIndicator : ICollection
        {
            #region ICollection Members

            public void Add(OutputIndicatorItem item)
            {
                this._indList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._indList.GetEnumerator();
            }

            #endregion

            [DisplayName("����������")]
            public int Count
            {
                get { return this._indList.Count; }
            }


            public const int LENGTH = 16;
            public const int COUNT = 8;

            private List<OutputIndicatorItem> _indList = new List<OutputIndicatorItem>(COUNT);

            public COutputIndicator()
            {
                this.SetBuffer(new ushort[LENGTH]);
            }

            public void SetBuffer(ushort[] values)
            {
                this._indList.Clear();
                if (values.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Output Indicator values",  LENGTH,
                                                          "Output indicator length must be 16");
                }
                for (int i = 0; i < LENGTH; i += 2)
                {
                    this._indList.Add(new OutputIndicatorItem(values[i], values[i + 1]));
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] ret = new ushort[LENGTH];
                for (int i = 0; i < COUNT; i++)
                {
                    ret[i*2] = this._indList[i].Value[0];
                    ret[i*2 + 1] = this._indList[i].Value[1];
                }
                return ret;
            }

            public OutputIndicatorItem this[int i]
            {
                get { return this._indList[i]; }
                set { this._indList[i] = value; }
            }
        } ;

        public class OutputIndicatorItem
        {
            private ushort _hiWord;
            private ushort _loWord;

            public override string ToString()
            {
                return "�������� ���������";
            }

            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [Category("������� ���")]
            [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            public ushort[] Value
            {
                get { return new ushort[] {this._hiWord, this._loWord}; }
            }

            public OutputIndicatorItem()
            {
            }

            public OutputIndicatorItem(ushort hiWord, ushort loWord)
            {
                this.SetItem(hiWord, loWord);
            }

            public void SetItem(ushort hiWord, ushort loWord)
            {
                this._hiWord = hiWord;
                this._loWord = loWord;
            }

            [XmlIgnore]
            [Browsable(false)]
            public bool IsBlinker
            {
                get 
                {
                    return 0 != Common.HIBYTE(this._hiWord); 
                }
                set
                {
                    
                    if (value)
                    {
                        this._hiWord = Common.TOWORD(0xff, Common.LOBYTE(this._hiWord));
                    }
                    else
                    {
                        this._hiWord = Common.TOWORD(0x0, Common.LOBYTE(this._hiWord));
                    }
                }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Description("��� ������� ����������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BlinkerTypeConverter))]
            public string Type
            {
                get
                {
                    string ret = this.IsBlinker ? "�������" : "�����������";
                    return ret;
                }
                set
                {
                    if ("�������" == value)
                    {
                        this.IsBlinker = true;
                    }
                    else if ("�����������" == value)
                    {
                        this.IsBlinker = false;
                    }
                    else
                    {
                        throw new ArgumentException("Rele type undefined", "Type");
                    }
                }
            }

            [XmlIgnore]
            [Browsable(false)]
            public byte SignalByte
            {
                get { return Common.LOBYTE(this._hiWord); }
                set { this._hiWord = Common.TOWORD(Common.HIBYTE(this._hiWord), value); }
            }

            [XmlAttribute("������")]
            [DisplayName("���")]
            [Description("��� ������� ����������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (AllSignalsTypeConverter))]
            public string SignalString
            {
                get
                {
                    string ret = Strings.All[Strings.All.Count - 1];
                    if (Common.LOBYTE(this._hiWord) <= Strings.All.Count)
                    {
                        ret = Strings.All[Common.LOBYTE(this._hiWord)];
                    }
                    return ret;
                }
                set
                {
                    byte index = (byte) Strings.All.IndexOf(value);
                    this._hiWord = Common.TOWORD(Common.HIBYTE(this._hiWord), index);
                }
            }

            [XmlAttribute("���������")]
            [DisplayName("������ ���������")]
            [Description("������ '����� ���������'")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool Indication
            {
                get { return 0 != (this._loWord & 0x1); }
                set { this._loWord = value ? (ushort) (this._loWord | 0x1) : (ushort) (this._loWord & ~0x1); }
            }

            [XmlAttribute("������")]
            [DisplayName("�. ������")]
            [Description("������ '����� ������� ������'")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool Alarm
            {
                get { return 0 != (this._loWord & 0x2); }
                set { this._loWord = value ? (ushort) (this._loWord | 0x2) : (ushort) (this._loWord & ~0x2); }
            }

            [XmlAttribute("�������")]
            [DisplayName("�. �������")]
            [Description("������ '����� ������� �������'")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool System
            {
                get { return 0 != (this._loWord & 0x4); }
                set { this._loWord = value ? (ushort) (this._loWord | 0x4) : (ushort) (this._loWord & ~0x4); }
            }
        } ;

        private COutputIndicator _outputIndicator = new COutputIndicator();

        [XmlElement("��������_����������")]
        [DisplayName("�������� ����������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("�������� �������")]
        public COutputIndicator OutputIndicator
        {
            get { return this._outputIndicator; }
        }

        #endregion

        #region ���������� �������

        private slot _analog = new slot(0x1900, 0x1918);

        [Category("���������� �������")]
        public double In
        {
            get { return Measuring.GetI(this._analog.Value[0], this.TTNP, false); }
        }

        [Category("���������� �������")]
        public double Ia
        {
            get { return Measuring.GetI(this._analog.Value[1], this.TT, true); }
        }

        [Category("���������� �������")]
        public double Ib
        {
            get { return Measuring.GetI(this._analog.Value[2], this.TT, true); }
        }

        [Category("���������� �������")]
        public double Ic
        {
            get { return Measuring.GetI(this._analog.Value[3], this.TT, true); }
        }

        [Category("���������� �������")]
        public double I0
        {
            get { return Measuring.GetI(this._analog.Value[4], this.TT, true); }
        }

        [Category("���������� �������")]
        public double I1
        {
            get { return Measuring.GetI(this._analog.Value[5], this.TT, true); }
        }

        [Category("���������� �������")]
        public double I2
        {
            get { return Measuring.GetI(this._analog.Value[6], this.TT, true); }
        }

        [Category("���������� �������")]
        public double Ig
        {
            get { return Measuring.GetI(this._analog.Value[7], this.TTNP, false); }
        }

        [Category("���������� �������")]
        public double Un
        {
            get { return Measuring.GetU(this._analog.Value[8], this.TNNP); }
        }

        [Category("���������� �������")]
        public double Ua
        {
            get { return Measuring.GetU(this._analog.Value[9], this.TN); }
        }

        [Category("���������� �������")]
        public double Ub
        {
            get { return Measuring.GetU(this._analog.Value[10], this.TN); }
        }

        [Category("���������� �������")]
        public double Uc
        {
            get { return Measuring.GetU(this._analog.Value[11], this.TN); }
        }

        [Category("���������� �������")]
        public double Uab
        {
            get { return Measuring.GetU(this._analog.Value[12], this.TN); }
        }

        [Category("���������� �������")]
        public double Ubc
        {
            get { return Measuring.GetU(this._analog.Value[13], this.TN); }
        }

        [Category("���������� �������")]
        public double Uca
        {
            get { return Measuring.GetU(this._analog.Value[14], this.TN); }
        }

        [Category("���������� �������")]
        public double U0
        {
            get { return Measuring.GetU(this._analog.Value[15], this.TN); }
        }

        [Category("���������� �������")]
        public double U1
        {
            get { return Measuring.GetU(this._analog.Value[16], this.TN); }
        }

        [Category("���������� �������")]
        public double U2
        {
            get { return Measuring.GetU(this._analog.Value[17], this.TN); }
        }

        [Category("���������� �������")]
        public double F
        {
            get { return Measuring.GetF(this._analog.Value[18]); }
        }

        [Category("���������� �������")]
        public double Q
        {
            get { return Measuring.GetConstraint(this._analog.Value[19], ConstraintKoefficient.K_25600); }
        }

        [Category("���������� �������")]
        public int Npusk
        {
            get { return this._analog.Value[20]; }
        }

        [Category("���������� �������")]
        public int Ngor
        {
            get { return this._analog.Value[21]; }
        }

        #endregion

        #region ���������� �������

        public string LoadZnaki(ushort[] masInputSignals)
        {
            byte[] buffer1 = Common.TOBYTES(masInputSignals, false);
            byte[] b1 = new byte[] { buffer1[54] };
            byte[] b2 = new byte[] { buffer1[55] };
            string r1 = Common.BitsToString(new BitArray(b1));
            string r2 = Common.BitsToString(new BitArray(b2));
            string res = r1 + r2;
            return res;
        }

        public string BdZnaki
        {
            get
            {
                return this.LoadZnaki(this._diagnostic.Value);
            }
        }

        [XmlIgnore]
        [DisplayName("����������� �������")]
        [Description("����������� ������� ����")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray ManageSignals
        {
            get
            {
                return
                    new BitArray(new byte[] {Common.LOBYTE(this._diagnostic.Value[0]), Common.HIBYTE(this._diagnostic.Value[0])});
            }
        }

        [XmlIgnore]
        [DisplayName("�������������� �������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray AdditionalSignals
        {
            get
            {
                BitArray temp = new BitArray(new byte[] {Common.LOBYTE(this._diagnostic.Value[2])});
                BitArray ret = new BitArray(4);
                for (int i = 0; i < 4; i++)
                {
                    ret[i] = temp[i + 4];
                }
                return ret;
            }
        }

        [XmlIgnore]
        [DisplayName("����������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray Indicators
        {
            get { return new BitArray(new byte[] {Common.HIBYTE(this._diagnostic.Value[2])}); }
        }

        [XmlIgnore]
        [DisplayName("������� �������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray InputSignals
        {
            get
            {
                return new BitArray(new byte[]
                                        {
                                            Common.LOBYTE(this._diagnostic.Value[0x9]),
                                            Common.HIBYTE(this._diagnostic.Value[0x9]),
                                            Common.LOBYTE(this._diagnostic.Value[0xA])
                                        });
            }
        }

        [XmlIgnore]
        [DisplayName("����")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray Rele
        {
            get { return new BitArray(new byte[] {Common.LOBYTE(this._diagnostic.Value[3])}); }
        }

        [XmlIgnore]
        [DisplayName("�������� �������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray OutputSignals
        {
            get { return new BitArray(new byte[] { Common.HIBYTE(this._diagnostic.Value[0xE]) }); }
        }

        [XmlIgnore]
        [DisplayName("������� ��������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray LimitSignals
        {
            get
            {
                return new BitArray(new byte[]
                                        {      Common.HIBYTE(this._diagnostic.Value[0xA]),
                                               Common.LOBYTE(this._diagnostic.Value[0xB]),
                                               Common.HIBYTE(this._diagnostic.Value[0xB]),
                                               Common.LOBYTE(this._diagnostic.Value[0xC]),
                                               Common.HIBYTE(this._diagnostic.Value[0xC]),
                                               Common.LOBYTE(this._diagnostic.Value[0xD]), 
                                               Common.LOBYTE(this._diagnostic.Value[0xE])});
            }
        }

        [XmlIgnore]
        [DisplayName("��������� �������������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray FaultState
        {
            get { return new BitArray(new byte[] {Common.LOBYTE(this._diagnostic.Value[4])}); }
        }

        [XmlIgnore]
        [DisplayName("������� �������������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray FaultSignals
        {
            get
            {
                return
                    new BitArray(new byte[] {Common.LOBYTE(this._diagnostic.Value[5]), Common.HIBYTE(this._diagnostic.Value[5])});
            }
        }

        [XmlIgnore]
        [DisplayName("����������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray Automation
        {
            get { return new BitArray(new byte[] {Common.HIBYTE(this._diagnostic.Value[8])}); }
        }

        #endregion

        #region ��������� ���� � ����������

        [XmlElement("��")]
        [DisplayName("��")]
        [Description("��������� ��� ��������������")]
        [Category("��������� ���� � ����������")]
        public ushort TT
        {
            get { return this._inputSignals.Value[1]; }
            set { this._inputSignals.Value[1] = value; }
        }

        [XmlElement("����")]
        [DisplayName("����")]
        [Description("��������� ��� �������������� ������� ������������������")]
        [Category("��������� ���� � ����������")]
        public ushort TTNP
        {
            get { return this._inputSignals.Value[2]; }
            set { this._inputSignals.Value[2] = value; }
        }

        [XmlElement("���_��")]
        [DisplayName("��� ��")]
        [Category("��������� ���� � ����������")]
        [TypeConverter(typeof (TN_TypeConverter))]
        public string TN_Type
        {
            get { return Strings.TnType[this._inputSignals.Value[8]]; }
            set { this._inputSignals.Value[8] = (ushort) Strings.TnType.IndexOf(value); }
        }

        [XmlElement("��")]
        [DisplayName("��")]
        [Description("����������� ��")]
        [Category("��������� ���� � ����������")]
        public double TN
        {
            get { return Measuring.GetConstraint(this._inputSignals.Value[9], ConstraintKoefficient.K_25600); }
            set { this._inputSignals.Value[9] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
        }

        [XmlElement("�������������_��")]
        [DisplayName("������������� ��")]
        [Description("������� ������������� ��")]
        [Category("��������� ���� � ����������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string TN_Dispepair
        {
            get
            {
                try
                {
                    return Strings.Logic[this._inputSignals.Value[10]];
                }
                catch (ArgumentOutOfRangeException)
                {
                    return Strings.Logic[Strings.Logic.Count - 1];
                }
            }
            set { this._inputSignals.Value[10] = (ushort) Strings.Logic.IndexOf(value); }
        }

        [XmlElement("�������������_����")]
        [DisplayName("������������� ����")]
        [Description("������� ������������� ����")]
        [Category("��������� ���� � ����������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string TNNP_Dispepair
        {
            get
            {
                try
                {
                    return Strings.Logic[this._inputSignals.Value[12]];
                }
                catch (ArgumentOutOfRangeException)
                {
                    return Strings.Logic[Strings.Logic.Count - 1];
                }
            }
            set { this._inputSignals.Value[12] = (ushort) Strings.Logic.IndexOf(value); }
        }

        [XmlElement("����")]
        [DisplayName("����")]
        [Description("����������� ����")]
        [Category("��������� ���� � ����������")]
        public double TNNP
        {
            get { return Measuring.GetConstraint(this._inputSignals.Value[0xB], ConstraintKoefficient.K_25600); }
            set { this._inputSignals.Value[0xB] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
        }

        [XmlElement("������������_���")]
        [DisplayName("������������ ���")]
        [Description("������������ ��� ��������")]
        [Category("��������� ���� � ����������")]
        public double MaxTok
        {
            get { return Measuring.GetConstraint(this._inputSignals.Value[3], ConstraintKoefficient.K_4000); }
            set { this._inputSignals.Value[3] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000); }
        }

        #endregion

        #region ������� �������
        public virtual double POscilloscopeCrush
        {
            set
            {
                this._oscCrush.Value[0] = (ushort)value;
            }
        }
        public virtual double OscilloscopeKonfCount
        {
            get
            {
                return this._konfCount.Value[0];
            }
            set
            {
                this._konfCount.Value[0] = (ushort)value;
            }
        }

        private List<bool> _disrepairs = new List<bool>();
        [XmlElement("�������_�������������")]
        [DisplayName("������� �������������")]
        [Category("������� �������")]
        [Editor(typeof (BitsEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public List<bool> DisrepairSignal
        {
            get
            {
                byte disrepair = Common.LOBYTE(this._inputSignals.Value[0x1D]);
                this._disrepairs.Clear();
                for (int i = 0; i < 8; i++)
                {
                    int b = disrepair & (byte)Math.Pow(2, i);
                    this._disrepairs.Add(b > 0);
                }
                return this._disrepairs;
            }
            set
            {
                this._inputSignals.Value[0x1D] = Common.BitsToUshort(value);
            }
        }

        [XmlElement("�������_��")]
        [DisplayName("�������, ��")]
        [Category("������� �������")]
        [Editor(typeof (BitsEditor), typeof (UITypeEditor))]
        public ulong DispepairImpulse
        {
            get { return Measuring.GetTime(this._inputSignals.Value[0x1E]); }
            set { this._inputSignals.Value[0x1E] = Measuring.SetTime(value); }
        }


        [XmlElement("����_��������")]
        [DisplayName("���� ��������")]
        [Description("����� ����� ����� ��������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string KeyOn
        {
            get
            {
                ushort index = this._inputSignals.Value[0x10];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[ index];
            }
            set { this._inputSignals.Value[0x10] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("����_���������")]
        [DisplayName("���� ���������")]
        [Description("����� ����� ����� ���������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string KeyOff
        {
            get
            {
                ushort index = this._inputSignals.Value[0x11];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { this._inputSignals.Value[0x11] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�������_��������")]
        [DisplayName("������� ��������")]
        [Description("����� ����� ������� ��������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string ExternalOn
        {
            get
            {
                ushort index = this._inputSignals.Value[0x12];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { this._inputSignals.Value[0x12] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�������_���������")]
        [DisplayName("������� ���������")]
        [Description("����� ����� ������� ���������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string ExternalOff
        {
            get
            {
                ushort index = this._inputSignals.Value[0x13];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { this._inputSignals.Value[0x13] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("������������_�������")]
        [DisplayName("������������ �������")]
        [Description("������� ������ ������ �������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string ConstraintGroup
        {
            get
            {
                ushort index = this._inputSignals.Value[0x15];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { this._inputSignals.Value[0x15] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("����_������������")]
        [DisplayName("���� ������������")]
        [Description("������� ������ ���� ������������")]
        [Category("������� �������")]
        public string OscilloscopeStart
        {
            get
            {
                ushort index = this._inputSignals.Value[0x16];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { this._inputSignals.Value[0x16] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�����_������������")]
        [DisplayName("����� ������������")]
        [Description("������� ������ ����� ������������")]
        [Category("������� �������")]
        public string SignalizationReset
        {
            get
            {
                ushort index = this._inputSignals.Value[0x14];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { this._inputSignals.Value[0x14] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        #endregion

        #region ������� ����������

        [XmlElement("������_��_������")]
        [DisplayName("������ �� ������")]
        [Category("������� �������")]
        [Description("����������� ������ �� ������")]
        [TypeConverter(typeof (ForbiddenTypeConverter))]
        public string ManageSignalButton
        {
            get
            {
                string ret = Common.GetBit(this._inputSignals.Value[0x3B], 0) ? Strings.Forbidden[0] : Strings.Forbidden[1];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Forbidden[0]) ? true : false;
                this._inputSignals.Value[0x3B] = Common.SetBit(this._inputSignals.Value[0x3B], 0, bit);
            }
        }

        [XmlElement("������_��_�����")]
        [DisplayName("������ �� �����")]
        [Category("������� �������")]
        [Description("����������� ������ �� �����")]
        [TypeConverter(typeof (ControlTypeConverter))]
        public string ManageSignalKey
        {
            get
            {
                string ret = Common.GetBit(this._inputSignals.Value[0x3B], 1) ? Strings.Control[0] : Strings.Control[1];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Control[0]) ? true : false;
                this._inputSignals.Value[0x3B] = Common.SetBit(this._inputSignals.Value[0x3B], 1, bit);
            }
        }

        [XmlElement("������_��_��������")]
        [DisplayName("������ �� ��������")]
        [Category("������� �������")]
        [Description("����������� ������ �� �������� ����������")]
        [TypeConverter(typeof (ControlTypeConverter))]
        public string ManageSignalExternal
        {
            get
            {
                string ret = Common.GetBit(this._inputSignals.Value[0x3B], 2) ? Strings.Control[0] : Strings.Control[1];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Control[0]) ? true : false;
                this._inputSignals.Value[0x3B] = Common.SetBit(this._inputSignals.Value[0x3B], 2, bit);
            }
        }

        [XmlElement("������_��_����")]
        [DisplayName("������ �� ����")]
        [Category("������� �������")]
        [Description("����������� ������ �� ����")]
        [TypeConverter(typeof (ForbiddenTypeConverter))]
        public string ManageSignalSDTU
        {
            get
            {
                string ret = Common.GetBit(this._inputSignals.Value[0x3B], 3) ? Strings.Forbidden[0] : Strings.Forbidden[1];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Forbidden[0]) ? true : false;
                this._inputSignals.Value[0x3B] = Common.SetBit(this._inputSignals.Value[0x3B], 3, bit);
            }
        }

        #endregion

        #region �����������

        [XmlElement("�����������_���������")]
        [DisplayName("����������� - ����������")]
        [Description("����������� - ��������� ����������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string SwitcherOff
        {
            get
            {
                ushort index = this._inputSignals.Value[50];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { this._inputSignals.Value[50] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�����������_��������")]
        [DisplayName("����������� - ���������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string SwitcherOn
        {
            get
            {
                ushort index = this._inputSignals.Value[51];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { this._inputSignals.Value[51] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�����������_������")]
        [DisplayName("����������� - �������������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string SwitcherError
        {
            get
            {
                ushort index = this._inputSignals.Value[52];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { this._inputSignals.Value[52] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�����������_����������")]
        [DisplayName("����������� - ����������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string SwitcherBlock
        {
            get
            {
                ushort index = this._inputSignals.Value[53];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { this._inputSignals.Value[53] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�����������_�����_����")]
        [DisplayName("����������� - ����� ����")]
        [Category("������� �������")]
        public ulong SwitcherTimeUROV
        {
            get { return Measuring.GetTime(this._inputSignals.Value[54]); }
            set { this._inputSignals.Value[54] = Measuring.SetTime(value); }
        }

        [XmlElement("�����������_���_����")]
        [DisplayName("����������� - ��� ����")]
        [Category("������� �������")]
        public double SwitcherTokUROV
        {
            get { return Measuring.GetConstraint(this._inputSignals.Value[55], ConstraintKoefficient.K_4001); }
            set { this._inputSignals.Value[55] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4001); }
        }

        [XmlElement("�����������_�������")]
        [DisplayName("����������� - �������")]
        [Category("������� �������")]
        public ulong SwitcherImpulse
        {
            get { return Measuring.GetTime(this._inputSignals.Value[56]); }
            set { this._inputSignals.Value[56] = Measuring.SetTime(value); }
        }

        [XmlElement("�����������_������������")]
        [DisplayName("����������� - ������������")]
        [Description("����������� -   ������������ ��������� ������")]
        [Category("������� �������")]
        public ulong SwitcherDuration
        {
            get { return Measuring.GetTime(this._inputSignals.Value[57]); }
            set { this._inputSignals.Value[57] = Measuring.SetTime(value); }
        }

        #endregion

        #region �������� ���������� �������
        public class OutputLogicSignalDataTransfer
        {
            [XmlElement("���1")]
            public LogicSignalUnitDataTransfer Vls1Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���2")]
            public LogicSignalUnitDataTransfer Vls2Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���3")]
            public LogicSignalUnitDataTransfer Vls3Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���4")]
            public LogicSignalUnitDataTransfer Vls4Signal = new LogicSignalUnitDataTransfer();


            [XmlElement("���5")]
            public LogicSignalUnitDataTransfer Vls5Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���6")]
            public LogicSignalUnitDataTransfer Vls6Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���7")]
            public LogicSignalUnitDataTransfer Vls7Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���8")]
            public LogicSignalUnitDataTransfer Vls8Signal = new LogicSignalUnitDataTransfer();

        }


        [XmlElement("��������_����������_�������")]
        public OutputLogicSignalDataTransfer OutputLogicSignalStruct
        {
            get
            {
                OutputLogicSignalDataTransfer toRet = new OutputLogicSignalDataTransfer();
                toRet.Vls1Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(0));
                toRet.Vls2Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(1));
                toRet.Vls3Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(2));
                toRet.Vls4Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(3));
                toRet.Vls5Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(4));
                toRet.Vls6Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(5));
                toRet.Vls7Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(6));
                toRet.Vls8Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(7));
                return toRet;

            }
            set { }
        }





        [XmlIgnore]
        [TypeConverter(typeof(RussianCollectionEditor))]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        public List<BitArray> OutputLogicSignals
        {
            //get
            //{
            //    List<List<string>> ret = new List<List<string>>();
            //    for (int i = 0; i < 8; i++)
            //    {
            //        BitArray array = GetOutputLogicSignals(i);
            //        List<string> strList = new List<string>();
            //        for (int j = 0; j < array.Count; j++)
            //        {
            //            strList.Add(string.Format("{0} | {1}", Strings.OutputSignals[i], array[i] ? "��" : "���"));
            //        }
            //        ret.Add(strList);
            //    }
            //    return ret;
            //}
            //set
            //{

            //}
                get
                    {
                    List<BitArray> _outputLogicSignals = new List<BitArray>(8);
                    for (int i = 0; i < 8; i++)
                    {
                        _outputLogicSignals.Add(this.GetOutputLogicSignals(i));
                    }
                    return _outputLogicSignals;
                }
                set
                    {
                    for (int i = 0; i < 8; i++)
                    {
                        this.SetOutputLogicSignals(i, value[i]);
                    }
                }
        }


        public BitArray GetOutputLogicSignals(int channel)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }

            ushort[] logicBuffer = new ushort[8];
            Array.ConstrainedCopy(this._outputSignals.Value, channel*8, logicBuffer, 0, 8);
            BitArray buffer = new BitArray(Common.TOBYTES(logicBuffer, false));
            BitArray ret = new BitArray(104);
            for (int i = 0; i < ret.Count; i++)
            {
                ret[i] = buffer[i];
            }
            return ret;
        }

        public void SetOutputLogicSignals(int channel, BitArray values)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            if (104 != values.Count)
            {
                throw new ArgumentOutOfRangeException("OutputLogic bits count");
            }

            ushort[] logicBuffer = new ushort[8];
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    if (i*16 + j < 104)
                    {
                        if (values[i*16 + j])
                        {
                            logicBuffer[i] += (ushort) Math.Pow(2, j);
                        }
                    }
                }
            }
            Array.ConstrainedCopy(logicBuffer, 0, this._outputSignals.Value, channel*8, 8);
        }

        #endregion

        #region ������� ���������� �������

        public class LogicSignalUnitDataTransfer
        {
            [XmlElement("�������")] public List<string> Value = new List<string>();

            public void FillValues(LogicState[] logicStates)
            {
                this.Value.Clear();
                foreach (var logicState in logicStates)
                {
                    this.Value.Add(logicState.ToString());
                }
            }

            public void FillOutValuesFromBitArray(BitArray bitArray)
            {
                this.Value.Clear();
                for (int bitNum = 0; bitNum < bitArray.Count; bitNum++)
                {
                    if (bitArray[bitNum])
                    {
                        this.Value.Add(Strings.OutputSignals[bitNum]);
                    }
                }
            }

        }

        public class InputLogicSignalDataTransfer
        {
            [XmlElement("�1")]
            public LogicSignalUnitDataTransfer And1Signal=new LogicSignalUnitDataTransfer();
            [XmlElement("�2")]
            public LogicSignalUnitDataTransfer And2Signal=new LogicSignalUnitDataTransfer();
            [XmlElement("�3")]
            public LogicSignalUnitDataTransfer And3Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("�4")]
            public LogicSignalUnitDataTransfer And4Signal = new LogicSignalUnitDataTransfer();


            [XmlElement("���1")]
            public LogicSignalUnitDataTransfer Or1Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���2")]
            public LogicSignalUnitDataTransfer Or2Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���3")]
            public LogicSignalUnitDataTransfer Or3Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���4")]
            public LogicSignalUnitDataTransfer Or4Signal = new LogicSignalUnitDataTransfer();

        }


        [XmlElement("�������_����������_�������")]
        public InputLogicSignalDataTransfer InputLogicSignalStruct
        {
            get
            {
                InputLogicSignalDataTransfer toRet = new InputLogicSignalDataTransfer();
                toRet.And1Signal.FillValues(this.GetInputLogicSignals(0));
                toRet.And2Signal.FillValues(this.GetInputLogicSignals(1));
                toRet.And3Signal.FillValues(this.GetInputLogicSignals(2));
                toRet.And4Signal.FillValues(this.GetInputLogicSignals(3));
                toRet.Or1Signal.FillValues(this.GetInputLogicSignals(4));
                toRet.Or2Signal.FillValues(this.GetInputLogicSignals(5));
                toRet.Or3Signal.FillValues(this.GetInputLogicSignals(6));
                toRet.Or4Signal.FillValues(this.GetInputLogicSignals(7));
                return toRet;

            }
            set { }
        }


        public LogicState[] GetInputLogicSignals(int channel)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            LogicState[] ret = new LogicState[16];
            SetLogicStates(this._inputSignals.Value[0x22 + channel*2], true, ref ret);
            SetLogicStates(this._inputSignals.Value[0x22 + channel*2 + 1], false, ref ret);
            return ret;
        }

        public void SetInputLogicSignals(int channel, LogicState[] logicSignals)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            ushort firstWord = 0;
            ushort secondWord = 0;

            for (int i = 0; i < logicSignals.Length; i++)
            {
                if (i < 8)
                {
                    if (LogicState.�� == logicSignals[i])
                    {
                        firstWord += (ushort) Math.Pow(2, i);
                    }
                    if (LogicState.������ == logicSignals[i])
                    {
                        firstWord += (ushort) Math.Pow(2, i);
                        firstWord += (ushort) Math.Pow(2, i + 8);
                    }
                }
                else
                {
                    if (LogicState.�� == logicSignals[i])
                    {
                        secondWord += (ushort) Math.Pow(2, i - 8);
                    }
                    if (LogicState.������ == logicSignals[i])
                    {
                        secondWord += (ushort) Math.Pow(2, i - 8);
                        secondWord += (ushort) Math.Pow(2, i);
                    }
                }
            }
            this._inputSignals.Value[0x22 + channel*2] = firstWord;
            this._inputSignals.Value[0x22 + channel*2 + 1] = secondWord;
        }

        private static void SetLogicStates(ushort value, bool firstWord, ref LogicState[] logicArray)
        {
            byte lowByte = Common.LOBYTE(value);
            byte hiByte = Common.HIBYTE(value);

            int start = firstWord ? 0 : 8;
            int end = firstWord ? 8 : 16;

            for (int i = start; i < end; i++)
            {
                int pow = firstWord ? i : i - 8;
                logicArray[i] = (byte) (Math.Pow(2, pow)) != ((lowByte) & (byte) (Math.Pow(2, pow)))
                                    ? logicArray[i]
                                    : LogicState.��;
                //logicArray[i] = (byte)(Math.Pow(2, pow)) != ((hiByte) & (byte)(Math.Pow(2, pow))) ? logicArray[i] : LogicState.������;
                logicArray[i] = IsLogicEquals(hiByte, pow) && (logicArray[i] == LogicState.��)
                                    ? LogicState.������
                                    : logicArray[i];
            }
        }

        private static bool IsLogicEquals(byte hiByte, int pow)
        {
            return (byte) (Math.Pow(2, pow)) == ((hiByte) & (byte) (Math.Pow(2, pow)));
        }

        #endregion

        #region �������� ����������

        //���� ����������
        private slot _automaticsPage = new slot(0x103c, 0x1050);
        private BitArray _bits = new BitArray(8);
        //������� �������� ����������
        public event Handler AutomaticsPageLoadOK;
        public event Handler AutomaticsPageLoadFail;
        public event Handler AutomaticsPageSaveOK;
        public event Handler AutomaticsPageSaveFail;

        public void LoadAutomaticsPage()
        {
            LoadSlot(DeviceNumber, this._automaticsPage, "LoadAutomaticsPage" + DeviceNumber, this);
        }

        public void SaveAutomaticsPage()
        {
            SaveSlot(DeviceNumber, this._automaticsPage, "SaveAutomaticsPage" + DeviceNumber, this);
        }

        [XmlElement("������������_���")]
        [DisplayName("������������ ���")]
        [Category("����������")]
        [TypeConverter(typeof (ModeTypeConverter))]
        public string APV_Cnf
        {
            get
            {
                byte index =Common.LOBYTE(this._automaticsPage.Value[0]);
                if (index >= Strings.Crat.Count)
                {
                    index = (byte) (Strings.Crat.Count - 1);
                }
                return Strings.Crat[index];
            }
            set
            {
                this._automaticsPage.Value[0] =
                    Common.TOWORD(Common.HIBYTE(this._automaticsPage.Value[0]), (byte) Strings.Crat.IndexOf(value));
            }
        }

        [XmlElement("����������_���")]
        [DisplayName("���������� ���")]
        [Description("����� ����� ���������� ���")]
        [Category("����������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string APV_Blocking
        {
            get
            {
                byte index = Common.LOBYTE(this._automaticsPage.Value[1]);
                if (index >= Strings.Logic.Count)
                {
                    index = (byte) (Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set { this._automaticsPage.Value[1] = (ushort) Strings.Logic.IndexOf(value); }
        }

        [XmlElement("�����_����������_���")]
        [DisplayName("����� ���")]
        [Description("����� ���������� ���")]
        [Category("����������")]
        public ulong APV_Time_Blocking
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[2]); }
            set { this._automaticsPage.Value[2] = Measuring.SetTime(value); }
        }

        [XmlElement("�����_����������_���")]
        [DisplayName("���������� ���")]
        [Description("����� ���������� ���")]
        [Category("����������")]
        public ulong APV_Time_Ready
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[3]); }
            set { this._automaticsPage.Value[3] = Measuring.SetTime(value); }
        }

        [XmlElement("�����_1_�����_���")]
        [DisplayName("���� 1 ���")]
        [Description("����� 1����� ���")]
        [Category("����������")]
        public ulong APV_Time_1Krat
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[4]); }
            set { this._automaticsPage.Value[4] = Measuring.SetTime(value); }
        }

        [XmlElement("�����_2_�����_���")]
        [DisplayName("���� 2 ���")]
        [Description("����� 2����� ���")]
        [Category("����������")]
        public ulong APV_Time_2Krat
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[5]); }
            set { this._automaticsPage.Value[5] = Measuring.SetTime(value); }
        }

        [XmlElement("�����_3_�����_���")]
        [DisplayName("���� 3 ���")]
        [Description("����� 3����� ���")]
        [Category("����������")]
        public ulong APV_Time_3Krat
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[6]); }
            set { this._automaticsPage.Value[6] = Measuring.SetTime(value); }
        }

        [XmlElement("�����_4_�����_���")]
        [DisplayName("���� 4 ���")]
        [Description("����� 4����� ���")]
        [Category("����������")]
        public ulong APV_Time_4Krat
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[7]); }
            set { this._automaticsPage.Value[7] = Measuring.SetTime(value); }
        }

        [XmlElement("������_���_��_��������������")]
        [DisplayName("�������������� ���")]
        [Description("������ ��� �� ��������������")]
        [TypeConverter(typeof (YesNoTypeConverter))]
        [Category("����������")]
        public string APV_Start
        {
            get
            {
                byte index = Common.HIBYTE(this._automaticsPage.Value[0]);
                if (index >= Strings.YesNo.Count)
                {
                    index = (byte) (Strings.YesNo.Count - 1);
                }
                return Strings.YesNo[index];
            }
            set
            {
                this._automaticsPage.Value[0] =
                    Common.TOWORD((byte) Strings.YesNo.IndexOf(value), Common.LOBYTE(this._automaticsPage.Value[0]));
            }
        }

        [XmlElement("����������_�������_���")]
        [DisplayName("������� ���")]
        [Description("���������� ������� ���")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        [Category("����������")]

        public bool AVR_Supply_Off
        {
            get
            {
                return this.AVR_Add_Signals[0];
            }
            set
            {
                this.SetCurrentBit(8, 0, value);
            }
        }

        private void SetCurrentBit(byte num_word, byte num_bit, bool val)
        {
            if (val)
            {
                if (((this._automaticsPage.Value[num_word] >> num_bit) & 1) == 0)
                {
                    this._automaticsPage.Value[num_word] += (ushort) Math.Pow(2, num_bit);
                }
            }
            else
            {
                if (((this._automaticsPage.Value[num_word] >> num_bit) & 1) == 1)
                {
                    this._automaticsPage.Value[num_word] -= (ushort) Math.Pow(2, num_bit);
                }
            }
            this._bits[num_bit] = val;
        }

        [XmlElement("�������_����������_�����������_���")]
        [DisplayName("���������� ���")]
        [Description("������� ���������� ����������� ���")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        [Category("����������")]
        public bool AVR_Switch_Off
        {
            get
            {
                return this.AVR_Add_Signals[2];
            }
            set
            {
                this.SetCurrentBit(8, 2, value);
            }

        }

        [XmlElement("��������������_���")]
        [DisplayName("�������������� ���")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        [Category("����������")]
        public bool AVR_Self_Off
        {
            get
            {
                return this.AVR_Add_Signals[1];
            }
            set
            {
                this.SetCurrentBit(8, 1, value);
            }
        }

        [XmlElement("������������_������_���")]
        [DisplayName("������ ���")]
        [Description("������������ ������ ���")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        [Category("����������")]
        public bool AVR_Abrasion_Switch
        {
            get
            {
                return this.AVR_Add_Signals[3];
            }
            set
            {
                this.SetCurrentBit(8, 3, value);
            }
        }

        [XmlElement("����������_������_���_��_���������_�����������")]
        [DisplayName("����� ���")]
        [Description("���������� ������ ��� �� ��������� �����������")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        [Category("����������")]

        public bool AVR_Reset_Switch
        {
            get
            {
                return this.AVR_Add_Signals[7];
            }
            set
            {
                this.SetCurrentBit(8, 7, value);
            }
        }

        [XmlIgnore]
        [DisplayName("������� ���")]
        [Editor(typeof (BitsEditor), typeof (UITypeEditor))]
        [Category("����������")]
        private BitArray AVR_Add_Signals
        {
            get { return this._bits; }
            set { this._bits = value; }
        }

        [XmlElement("����������_���")]
        [DisplayName("���������� ���")]
        [Description("����� ����� ���������� ���")]
        [Category("����������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string AVR_Blocking
        {
            get
            {
                ushort index = this._automaticsPage.Value[9];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte) (Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set { this._automaticsPage.Value[9] = (ushort) Strings.Logic.IndexOf(value); }
        }

        [XmlElement("�����_����������_���")]
        [DisplayName("����� ���")]
        [Description("����� ���������� ���")]
        [Category("����������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string AVR_Reset
        {
            get
            {
                ushort index = this._automaticsPage.Value[10];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte) (Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set { this._automaticsPage.Value[10] = (ushort) Strings.Logic.IndexOf(value); }
        }

        [XmlElement("������_���")]
        [DisplayName("������ ���")]
        [Category("����������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string AVR_Start
        {
            get
            {
                ushort index = this._automaticsPage.Value[11];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte) (Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set { this._automaticsPage.Value[11] = (ushort) Strings.Logic.IndexOf(value); }
        }

        [XmlElement("������������_���")]
        [DisplayName("������������ ���")]
        [Category("����������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string AVR_Abrasion
        {
            get
            {
                ushort index = this._automaticsPage.Value[12];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte) (Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set { this._automaticsPage.Value[12] = (ushort) Strings.Logic.IndexOf(value); }
        }

        [XmlElement("�����_������������_���")]
        [DisplayName("����� c����������� ���")]
        [Category("����������")]
        public ulong AVR_Time_Abrasion
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[13]); }
            set { this._automaticsPage.Value[13] = Measuring.SetTime(value); }
        }

        [XmlElement("�������_���")]
        [DisplayName("������� ���")]
        [Category("����������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string AVR_Return
        {
            get
            {
                ushort index = this._automaticsPage.Value[14];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte) (Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set { this._automaticsPage.Value[14] = (ushort) Strings.Logic.IndexOf(value); }
        }

        [XmlElement("�����_��������_���")]
        [DisplayName("����� �������� ���")]
        [Category("����������")]
        public ulong AVR_Time_Return
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[15]); }
            set { this._automaticsPage.Value[15] = Measuring.SetTime(value); }
        }

        [XmlElement("�����_����������_���")]
        [DisplayName("����� ���������� ���")]
        [Category("����������")]
        public ulong AVR_Time_Off
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[16]); }
            set { this._automaticsPage.Value[16] = Measuring.SetTime(value); }
        }

        [XmlElement("������������_���")]
        [DisplayName("������������ ���")]
        [Category("����������")]
        [TypeConverter(typeof (ModeLightTypeConverter))]
        public string LZSH_Cnf
        {
            get
            {
                ushort index = this._automaticsPage.Value[18];
                if (index >= Strings.ModesLight.Count)
                {
                    index = (byte) (Strings.ModesLight.Count - 1);
                }
                return Strings.ModesLight[index];
            }
            set { this._automaticsPage.Value[18] = (ushort) Strings.ModesLight.IndexOf(value); }
        }

        [XmlElement("�������_���")]
        [DisplayName("������� ���")]
        [Category("����������")]
        public double LZSH_Constraint
        {
            get { return Measuring.GetConstraint(this._automaticsPage.Value[19], ConstraintKoefficient.K_4001); }
            set { this._automaticsPage.Value[19] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4001); }
        }

        #endregion

        #region ������� ������

        public class CExternalDefenses : ICollection
        {
            public const int LENGTH = 48;
            public const int COUNT = 8;

            #region ICollection Members

            public void Add(ExternalDefenseItem item)
            {
                this._defenseList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._defenseList.GetEnumerator();
            }

            #endregion

            public CExternalDefenses()
            {
                try 
                {
                    this.SetBuffer(new ushort[LENGTH]);
                }
                catch (Exception) 
                {

                }
            }

            private List<ExternalDefenseItem> _defenseList = new List<ExternalDefenseItem>(COUNT);

            public void SetBuffer(ushort[] buffer)
            {
                if (buffer.Length == LENGTH)
                {
                    this._defenseList.Clear();
                    if (buffer.Length != LENGTH)
                    {
                        throw new ArgumentOutOfRangeException("External defense values", LENGTH, "External defense length must be 48");
                    }
                    for (int i = 0; i < LENGTH; i += ExternalDefenseItem.LENGTH)
                    {
                        ushort[] temp = new ushort[ExternalDefenseItem.LENGTH];
                        Array.ConstrainedCopy(buffer, i, temp, 0, ExternalDefenseItem.LENGTH);
                        this._defenseList.Add(new ExternalDefenseItem(temp));
                    }
                }
            }

            [DisplayName("����������")]
            public int Count
            {
                get { return this._defenseList.Count; }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                for (int i = 0; i < this.Count; i++)
                {
                    Array.ConstrainedCopy(this._defenseList[i].Values, 0, buffer, i*ExternalDefenseItem.LENGTH,
                                          ExternalDefenseItem.LENGTH);
                }
                return buffer;
            }

            public ExternalDefenseItem this[int i]
            {
                get { return this._defenseList[i]; }
                set { this._defenseList[i] = value; }
            }
        } ;

        public class ExternalDefenseItem
        {
            public const int LENGTH = 6;
            private ushort[] _values = new ushort[LENGTH];

            public ExternalDefenseItem()
            {
            }

            public ExternalDefenseItem(ushort[] buffer)
            {
                this.SetItem(buffer);
            }

            public override string ToString()
            {
                return "������� ������";
            }

            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [Category("������� ���")]
            [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            public ushort[] Values
            {
                get { return this._values; }
                set { this.SetItem(value); }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("External defense item value",  LENGTH,
                                                          "External defense item length must be 6");
                }
                this._values = buffer;
            }

            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [Description("����� ������ ������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    ushort index = Common.GetBits(this._values[0], 0, 1, 2);
                    if (index >= Strings.ModesExternal.Count)
                    {
                        index = (byte)(Strings.ModesExternal.Count - 1);
                    }
                    return Strings.ModesExternal[index];
                }
                set 
                {
                    this._values[0] = Common.SetBits(this._values[0], (ushort) Strings.ModesExternal.IndexOf(value), 0, 1, 2); 
                }
            }


            [XmlAttribute("���")]
            [DisplayName("���")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool AVR
            {
                get { return 0 != (this._values[0] & 32); }
                set { this._values[0] = value ? this._values[0] |= 32 : (ushort) (this._values[0] & ~32); }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool APV
            {
                get { return 0 != (this._values[0] & 64); }
                set { this._values[0] = value ? this._values[0] |= 64 : (ushort) (this._values[0] & ~64); }
            }

            [XmlAttribute("����")]
            [DisplayName("����")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool UROV
            {
                get { return 0 != (this._values[0] & 128); }
                set { this._values[0] = value ? this._values[0] |= 128 : (ushort) (this._values[0] & ~128); }
            }

            [XmlAttribute("�������_���")]
            [DisplayName("��� ��������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool APV_Return
            {
                get { return 0 != (this._values[0] & 0x8000); }
                set { this._values[0] = value ? this._values[0] |= 0x8000 : (ushort) (this._values[0] & ~0x8000); }
            }

            [XmlAttribute("�������")]
            [DisplayName("�������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool Return
            {
                get { return 0 != (this._values[0] & 0x4000); }
                set { this._values[0] = value ? this._values[0] |= 0x4000 : (ushort) (this._values[0] & ~0x4000); }
            }


            [XmlAttribute("����������_�����")]
            [DisplayName("����� ����������")]
            [Description("����� ����� ���������� ��")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ExternalDefensesTypeConverter))]
            public string BlockingNumber
            {
                get
                {
                    ushort value = this._values[1];
                    if (value >= Strings.ExternalDefense.Count)
                    {
                        value = (ushort) (Strings.ExternalDefense.Count - 1);
                    }
                    return Strings.ExternalDefense[value];
                }
                set { this._values[1] = (ushort) Strings.ExternalDefense.IndexOf(value); }
            }

            [XmlAttribute("������������_�����")]
            [DisplayName("����� ������������")]
            [Description("����� ����� ������������ ��")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ExternalDefensesTypeConverter))]
            public string WorkingNumber
            {
                get
                {
                    ushort value = this._values[2];
                    if (value >= Strings.ExternalDefense.Count)
                    {
                        value = (ushort) (Strings.ExternalDefense.Count - 1);
                    }
                    return Strings.ExternalDefense[value];
                }
                set 
                {
                    this._values[2] = (ushort) Strings.ExternalDefense.IndexOf(value); 
                }
            }

            [XmlAttribute("������������_�����")]
            [DisplayName("����� ������������")]
            [Description("�������� ������� ������������ ��")]
            [Category("��������� ���")]
            public ulong WorkingTime
            {
                get { return Measuring.GetTime(this._values[3]); }
                set { this._values[3] = Measuring.SetTime(value); }
            }

            [XmlAttribute("�������_�����")]
            [DisplayName("����� ��������")]
            [Description("����� ����� �������� ��")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ExternalDefensesTypeConverter))]
            public string ReturnNumber
            {
                get
                {
                    ushort value = this._values[4];
                    if (value >= Strings.ExternalDefense.Count)
                    {
                        value = (ushort) (Strings.ExternalDefense.Count - 1);
                    }
                    return Strings.ExternalDefense[value];
                }
                set { this._values[4] = (ushort) Strings.ExternalDefense.IndexOf(value); }
            }

            [XmlAttribute("�������_�����")]
            [DisplayName("����� ��������")]
            [Description("�������� ������� �������� ��")]
            [Category("��������� ���")]
            public ulong ReturnTime
            {
                get { return Measuring.GetTime(this._values[5]); }
                set { this._values[5] = Measuring.SetTime(value); }
            }
        }



        private CExternalDefenses _cexternalDefenses = new CExternalDefenses();

        [XmlElement("�������_������")]
        [DisplayName("������� ������")]
        [Description("������� ������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("������� ������")]
        public CExternalDefenses ExternalDefenses
        {
            get { return this._cexternalDefenses; }
            set { this._cexternalDefenses = value; }
        }

        #endregion

        #region ������� ������

        private slot _tokDefensesMain = new slot(0x1080, 0x10C0);
        private slot _tokDefenses2Main = new slot(0x1100, 0x1110);
        private slot _tokDefensesReserve = new slot(0x10C0, 0x1100);
        private slot _tokDefenses2Reserve = new slot(0x1110, 0x1120);

        public void LoadTokDefenses()
        {
            LoadSlot(DeviceNumber, this._tokDefenses2Reserve, "LoadTokDefensesReserve2" + DeviceNumber, this);
            LoadSlot(DeviceNumber, this._tokDefensesReserve, "LoadTokDefensesReserve" + DeviceNumber, this);
            LoadSlot(DeviceNumber, this._tokDefenses2Main, "LoadTokDefensesMain2" + DeviceNumber, this);
            LoadSlot(DeviceNumber, this._tokDefensesMain, "LoadTokDefensesMain" + DeviceNumber, this);
        }

        public void SaveTokDefenses()
        {
            Array.ConstrainedCopy(this.TokDefensesMain.ToUshort1(), 0, this._tokDefensesMain.Value, 0, CTokDefenses.LENGTH1);
            Array.ConstrainedCopy(this.TokDefensesMain.ToUshort2(), 0, this._tokDefenses2Main.Value, 0, CTokDefenses.LENGTH2);
            Array.ConstrainedCopy(this.TokDefensesReserve.ToUshort1(), 0, this._tokDefensesReserve.Value, 0, CTokDefenses.LENGTH1);
            Array.ConstrainedCopy(this.TokDefensesReserve.ToUshort2(), 0, this._tokDefenses2Reserve.Value, 0, CTokDefenses.LENGTH2);

            SaveSlot(DeviceNumber, this._tokDefenses2Main, "SaveTokDefensesMain2" + DeviceNumber, this);
            SaveSlot(DeviceNumber, this._tokDefenses2Reserve, "SaveTokDefensesReserve2" + DeviceNumber, this);
            SaveSlot(DeviceNumber, this._tokDefensesReserve, "SaveTokDefensesReserve" + DeviceNumber, this);
            SaveSlot(DeviceNumber, this._tokDefensesMain, "SaveTokDefensesMain" + DeviceNumber, this);
        }

        private CTokDefenses _ctokDefensesMain = new CTokDefenses();
        private CTokDefenses _ctokDefensesReserve = new CTokDefenses();

        [XmlElement("�������_������_��������")]
        [DisplayName("�������� ������")]
        [Description("������� ������ - �������� ������ �������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("������� ������")]
        public CTokDefenses TokDefensesMain
        {
            get { return this._ctokDefensesMain; }
            set { this._ctokDefensesMain = value; }
        }
        

        [XmlElement("����_����_����������������_��������")]
        [Browsable(false)]
        public MaxSensitivity MaxSensitivityMain
        {
            get
            {
                return new MaxSensitivity()
                {
                    I = this._ctokDefensesMain.I,
                    I0 = this._ctokDefensesMain.I0,
                    I2 = this._ctokDefensesMain.I2,
                    In = this._ctokDefensesMain.In
                };
            }
            set { }
        }


        [XmlElement("�������_������_���������")]
        [DisplayName("��������� ������")]
        [Description("������� ������ - ��������� ������ �������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("������� ������")]
        public CTokDefenses TokDefensesReserve
        {
            get { return this._ctokDefensesReserve; }
            set { this._ctokDefensesReserve = value; }
        }


        [XmlElement("����_����_����������������_���������")]
        [Browsable(false)]
        public MaxSensitivity MaxSensitivityReserve
        {
            get
            {
                return new MaxSensitivity()
                {
                    I = this._ctokDefensesReserve.I,
                    I0 = this._ctokDefensesReserve.I0,
                    I2 = this._ctokDefensesReserve.I2,
                    In = this._ctokDefensesReserve.In
                };
            }
            set { }
        }

        public class CTokDefenses : ICollection
        {
            public const int LENGTH1 = 64;
            public const int COUNT1 = 10;
            public const int LENGTH2 = 16;
            public const int COUNT2 = 2;
            private ushort[] _buffer;

            #region ICollection Members

            [XmlElement]
            public ushort TestI
            {
                get { return this._buffer[0]; }
                set { this._buffer[0] = value; }
            }
            [XmlElement]
            public ushort TestI0
            {
                get { return this._buffer[1]; }
                set { this._buffer[0] = value; }
            }
            [XmlElement]
            public ushort TestIn {
                get { return this._buffer[2]; }
                set { this._buffer[0] = value; }
            }
            [XmlElement]
            public ushort TestI2 {
                get { return this._buffer[3]; }
                set { this._buffer[0] = value; }
            }

            [DisplayName("����������")]
            public int Count
            {
                get { return this._items.Count; }
            }

            public void Add(TokDefenseItem item)
            {
                this._items.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._items.GetEnumerator();
            }

            #endregion
            [XmlElement(nameof(I))]
            public ushort I
            {
                get { return this._buffer[0]; }
                set { this._buffer[0] = value; }
            }
            [XmlElement(nameof(I0))]
            public ushort I0
            {
                get { return this._buffer[1]; }
                set { this._buffer[1] = value; }
            }
            [XmlElement(nameof(In))]
            public ushort In
            {
                get { return this._buffer[2]; }
                set { this._buffer[2] = value; }
            }
            [XmlElement(nameof(I2))]
            public ushort I2
            {
                get { return this._buffer[3]; }
                set { this._buffer[3] = value; }
            }

            private List<TokDefenseItem> _items = new List<TokDefenseItem>(13);

            public CTokDefenses()
            {
                this.SetBuffer(new ushort[160]);
            }

            public TokDefenseItem this[int i]
            {
                get { return this._items[i]; }
                set { this._items[i] = value; }
            }

            public void SetBuffer(ushort[] buffer)
            {
                this._buffer = buffer;
                this._items.Clear();
                for (int i = 0; i < TokDefenseItem.LENGTH*12; i += TokDefenseItem.LENGTH)
                {
                    if (i == TokDefenseItem.LENGTH*11)
                    {
                        i += 2;
                    }
                    ushort[] item = new ushort[TokDefenseItem.LENGTH];
                    Array.ConstrainedCopy(this._buffer, i + 4, item, 0, TokDefenseItem.LENGTH);
                    this._items.Add(new TokDefenseItem(item));
                }

                for (int i = 0; i < 12; i++)
                {
                    this._items[i].IsIncrease = i < 4 ? true : false;
                    this._items[i].IsKoeff500 = i < 8 ? false : true;
                    this._items[i].IsI12 = false;
                }


                this._items[11].IsI12 = true;

                this._items[0].Name = "I>";
                this._items[1].Name = "I>>";
                this._items[2].Name = "I>>>";
                this._items[3].Name = "I>>>>";
                this._items[4].Name = "I2>";
                this._items[5].Name = "I2>>";
                this._items[6].Name = "I0>";
                this._items[7].Name = "I0>>";
                this._items[8].Name = "In>";
                this._items[9].Name = "In>>";
                this._items[10].Name = "I�";
                this._items[11].Name = "I2/I1";
            }

            public ushort[] ToUshort1()
            {
                ushort[] buffer = new ushort[LENGTH1];
                buffer[0] = this.I;
                buffer[1] = this.I0;
                buffer[2] = this.In;
                buffer[3] = this.I2;

                for (int i = 0; i < COUNT1; i++)
                {
                    Array.ConstrainedCopy(this._items[i].Values, 0, buffer, 4 + i*TokDefenseItem.LENGTH,
                                          TokDefenseItem.LENGTH);
                }
                return buffer;
            }

            public ushort[] ToUshort2()
            {
                ushort[] buffer = new ushort[LENGTH2];

                Array.ConstrainedCopy(this._items[10].Values, 0, buffer, 0, TokDefenseItem.LENGTH);
                Array.ConstrainedCopy(this._items[11].Values, 0, buffer, 2 + TokDefenseItem.LENGTH, TokDefenseItem.LENGTH);
                return buffer;
            }
        }
        
        public class MaxSensitivity
        {
            [XmlElement(nameof(I))]
            public ushort I { get; set; }

            [XmlElement(nameof(I0))]
            public ushort I0 { get; set; }

            [XmlElement(nameof(In))]
            public ushort In { get; set; }

            [XmlElement(nameof(I2))]
            public ushort I2 { get; set; }
        }


        public class TokDefenseItem
        {
            public const int LENGTH = 6;
            private bool _isKoeff500;

            [XmlIgnore]
            [Browsable(false)]
            public bool IsKoeff500
            {
                get { return this._isKoeff500; }
                set { this._isKoeff500 = value; }
            }

            private bool _isIncrease;
            private string _name;
            private ushort[] _values = new ushort[LENGTH];

            public TokDefenseItem()
            {
            }

            public TokDefenseItem(ushort[] buffer)
            {
                this.SetItem(buffer);
            }

            private bool _isI12 = false;

            public bool IsI12
            {
                get { return this._isI12; }
                set { this._isI12 = value; }
            }


            [XmlIgnore]
            [Browsable(false)]
            public bool IsIncrease
            {
                get { return this._isIncrease; }
                set { this._isIncrease = value; }
            }

            [XmlIgnore]
            [Browsable(false)]
            public string Name
            {
                get { return this._name; }
                set { this._name = value; }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Tok defense increase1 item value",  LENGTH,
                                                          "Tok defense increase1 item length must be 6");
                }
                this._values = buffer;
            }

            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [Category("������� ���")]
            [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            public ushort[] Values
            {
                get { return this._values; }
                set { this.SetItem(value); }
            }

            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [Description("����� ������ ������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    byte index = (byte) (Common.LOBYTE(this._values[0]) & 0x07);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte) (Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set { this._values[0] = Common.SetBits(this._values[0], (ushort) Strings.Modes.IndexOf(value), 0, 1, 2); }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool AVR
            {
                get { return Common.GetBit(this._values[0], 5); }
                set { this._values[0] = Common.SetBit(this._values[0], 5, value); }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool APV
            {
                get { return Common.GetBit(this._values[0], 6); }
                set { this._values[0] = Common.SetBit(this._values[0], 6, value); }
            }

            [XmlAttribute("����")]
            [DisplayName("����")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool UROV
            {
                get { return Common.GetBit(this._values[0], 7); }
                set { this._values[0] = Common.SetBit(this._values[0], 7, value); }
            }

            [XmlAttribute("����������_��������")]
            [DisplayName("���������� ���")]
            [Description("��� ����������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BlockingTypeConverter))]
            public string BlockingExist
            {
                get
                {
                    return  Common.GetBit(this._values[0], 9) ? Strings.Blocking[1] : Strings.Blocking[0];
                }
                set
                {
                    bool b = true;
                    if (Strings.Blocking[0] == value)
                    {
                        b = false;
                    }
                    this._values[0] = Common.SetBit(this._values[0], 9, b);
                }
            }

            [XmlAttribute("����������_�����")]
            [DisplayName("���������� �����")]
            [Description("����� ����� ����������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (LogicTypeConverter))]
            public string BlockingNumber
            {
                get
                {
                    ushort value = this._values[1];
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set { this._values[1] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlAttribute("����_��_U")]
            [DisplayName("���� �� U")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool PuskU
            {
                get { return Common.GetBit(this._values[0], 14); }
                set { this._values[0] = Common.SetBit(this._values[0], 14, value); }
            }

            [XmlAttribute("���������")]
            [DisplayName("���������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool Speedup
            {
                get { return Common.GetBit(this._values[0], 15); }
                set { this._values[0] = Common.SetBit(this._values[0], 15, value); }
            }

            [XmlAttribute("�����������")]
            [DisplayName("�����������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BusDirectionTypeConverter))]
            public string Direction
            {
                get
                {
                    ushort temp = Common.GetBits(this._values[0], 10, 11);
                    byte value = (byte) (Common.HIBYTE(temp) >> 2);
                    value = value >= Strings.BusDirection.Count ? (byte) (Strings.BusDirection.Count - 1) : value;
                    return Strings.BusDirection[value];
                }
                set
                {
                    ushort index = (ushort) Strings.BusDirection.IndexOf(value);
                    this._values[0] = Common.SetBits(this._values[0], index, 10, 11);
                }
            }

            [XmlAttribute("��������")]
            [Browsable(false)]
            public string Parameter
            {
                get
                {
                    string ret;
                    if (this.IsIncrease)
                    {
                        ret = Common.GetBit(this._values[0], 8) ? Strings.TokParameter[1] : Strings.TokParameter[0];
                    }
                    else
                    {
                        ret = Common.GetBit(this._values[0], 8) ? Strings.EngineParameter[1] : Strings.EngineParameter[0];
                    }

                    return ret;
                }
                set
                {
                    bool b = true;
                    if (this.IsIncrease)
                    {
                        if (Strings.TokParameter[0] == value)
                        {
                            b = false;
                        }
                    }
                    else
                    {
                        if (Strings.EngineParameter[0] == value)
                        {
                            b = false;
                        }
                    }

                    this._values[0] = Common.SetBit(this._values[0], 8, b);
                }
            }

            [XmlAttribute("���������")]
            [Browsable(false)]
            public string Engine
            {
                get
                {
                    return  Common.GetBit(this._values[0], 15) ? Strings.EngineMode[1] : Strings.EngineMode[0];
                }
                set
                {
                    bool b = true;
                    if (Strings.EngineMode[0] == value)
                    {
                        b = false;
                    }
                    this._values[0] = Common.SetBit(this._values[0], 15, b);
                }
            }

            [XmlAttribute("��������������")]
            [DisplayName("��������������")]
            [Category("��������� ���")]
            [TypeConverter(typeof(FeatureTypeConverter))]
            public string Feature
            {
                get
                {
                    string ret;
                    if (this.IsIncrease)
                    {
                        ret = Common.GetBit(this._values[0], 12) ? Strings.FeatureLight[1] : Strings.Feature[0];
                    }
                    else
                    {
                        ret = Common.GetBit(this._values[0], 12) ? Strings.FeatureLight[0] : Strings.Feature[0];
                    }

                    return ret;
                }
                set
                {
                    bool b = true;
                    if (this.IsIncrease)
                    {
                        if (Strings.FeatureLight[0] == value)
                        {
                            b = false;
                        }
                    }
                    else
                    {
                        if (Strings.Feature[0] == value)
                        {
                            b = false;
                        }
                    }

                    this._values[0] = Common.SetBit(this._values[0], 12, b);
                }
            }

            [XmlAttribute("������������_�������")]
            [DisplayName("������������ �������")]
            [Description("������� ������������")]
            [Category("��������� ���")]
            public double WorkConstraint
            {
                get
                {
                    double ret;
                    ConstraintKoefficient koeff = ConstraintKoefficient.K_4000;
                    if (this.IsKoeff500)
                    {
                        koeff = ConstraintKoefficient.K_500;
                    }
                    if (this.IsI12)
                    {
                        koeff = ConstraintKoefficient.K_10000;
                    }
                    ret = Measuring.GetConstraint(this._values[2], koeff);
                    return ret;
                }
                set
                {
                    ConstraintKoefficient koeff = ConstraintKoefficient.K_4000;
                    if (this.IsKoeff500)
                    {
                        koeff = ConstraintKoefficient.K_500;
                    }
                    if (this.IsI12)
                    {
                        koeff = ConstraintKoefficient.K_10000;
                    }
                    this._values[2] = Measuring.SetConstraint(value, koeff);
                }
            }

            [XmlAttribute("������������_�����")]
            [DisplayName("������������ �����")]
            [Description("�������� ������� ������������")]
            [Category("��������� ���")]
            public ulong WorkTime
            {
                get { return Measuring.GetTime(this._values[3]); }
                set { this._values[3] = Measuring.SetTime(value); }
            }

            [XmlAttribute("����_��_U_�������")]
            [DisplayName("���� �� U �������")]
            [Description("������� ����� �� U")]
            [Category("��������� ���")]
            public double PuskU_Constraint
            {
                get { return Measuring.GetConstraint(this._values[4], ConstraintKoefficient.K_25600); }
                set { this._values[4] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
            }

            [XmlAttribute("���������_�����")]
            [DisplayName("��������� �����")]
            [Description("�������� ������� ���������")]
            [Category("��������� ���")]
            public ulong SpeedupTime
            {
                get { return Measuring.GetTime(this._values[5]); }
                set { this._values[5] = Measuring.SetTime(value); }
            }
        } ;

        #endregion

        #region ������ �� ����������

        private slot _voltageDefensesMain = new slot(0x1180, 0x11C0);
        private slot _voltageDefensesReserve = new slot(0x11C0, 0x1200);

        public void LoadVoltageDefenses()
        {
            LoadSlot(DeviceNumber, this._voltageDefensesReserve, "LoadVoltageDefensesReserve" + DeviceNumber, this);
            LoadSlot(DeviceNumber, this._voltageDefensesMain, "LoadVoltageDefensesMain" + DeviceNumber, this);
        }

        public void SaveVoltageDefenses()
        {
            Array.ConstrainedCopy(this.VoltageDefensesMain.ToUshort(), 0, this._voltageDefensesMain.Value, 0,
                                  CVoltageDefenses.LENGTH);
            Array.ConstrainedCopy(this.VoltageDefensesReserve.ToUshort(), 0, this._voltageDefensesReserve.Value, 0,
                                  CVoltageDefenses.LENGTH);
            SaveSlot(DeviceNumber, this._voltageDefensesReserve, "SaveVoltageDefensesReserve" + DeviceNumber, this);
            SaveSlot(DeviceNumber, this._voltageDefensesMain, "SaveVoltageDefensesMain" + DeviceNumber, this);
        }

        public event Handler VoltageDefensesLoadOk;
        public event Handler VoltageDefensesLoadFail;
        public event Handler VoltageDefensesSaveOk;
        public event Handler VoltageDefensesSaveFail;

        private CVoltageDefenses _cvoltageDefensesMain = new CVoltageDefenses();
        private CVoltageDefenses _cvoltageDefensesReserve = new CVoltageDefenses();

        [XmlElement("������_����������_��������")]
        [DisplayName("�������� ������")]
        [Description("������ �� ���������� - �������� ������ �������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("������ �� ����������")]
        public CVoltageDefenses VoltageDefensesMain
        {
            get { return this._cvoltageDefensesMain; }
            set { this._cvoltageDefensesMain = value; }
        }

        [XmlElement("������_����������_���������")]
        [DisplayName("��������� ������")]
        [Description("������ �� ���������� - ��������� ������ �������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("������ �� ����������")]
        public CVoltageDefenses VoltageDefensesReserve
        {
            get { return this._cvoltageDefensesReserve; }
            set { this._cvoltageDefensesReserve = value; }
        }

        public enum VoltageDefenseType
        {
            U,
            U2,
            U0
        } ;

        public class CVoltageDefenses : ICollection
        {
            public const int LENGTH = 64;
            public const int COUNT = 8;

            #region ICollection Members

            public void Add(VoltageDefenseItem item)
            {
                this._defenseList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            [XmlIgnore]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            [XmlIgnore]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._defenseList.GetEnumerator();
            }

            #endregion

            private List<VoltageDefenseItem> _defenseList = new List<VoltageDefenseItem>(COUNT);

            public CVoltageDefenses()
            {
                this.SetBuffer(new ushort[LENGTH]);
            }

            public void SetBuffer(ushort[] buffer)
            {
                this._defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("������ �� ����������",LENGTH,
                                                          "����� ��� ����� �� ���������� ������ ���� " + LENGTH);
                }
                for (int i = 0; i < LENGTH; i += VoltageDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[VoltageDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, VoltageDefenseItem.LENGTH);
                    this._defenseList.Add(new VoltageDefenseItem(temp));
                }
                this._defenseList[4].DefenseType = this._defenseList[5].DefenseType = VoltageDefenseType.U2;
                this._defenseList[6].DefenseType = this._defenseList[7].DefenseType = VoltageDefenseType.U0;

                this._defenseList[0].Name = "U>";
                this._defenseList[1].Name = "U>>";
                this._defenseList[2].Name = "U<";
                this._defenseList[3].Name = "U<<";
                this._defenseList[4].Name = "U2>";
                this._defenseList[5].Name = "U2>>";
                this._defenseList[6].Name = "U0>";
                this._defenseList[7].Name = "U0>>";
            }

            [DisplayName("����������")]
            public int Count
            {
                get { return this._defenseList.Count; }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                for (int i = 0; i < this.Count; i++)
                {
                    Array.ConstrainedCopy(this._defenseList[i].Values, 0, buffer, i*VoltageDefenseItem.LENGTH,
                                          VoltageDefenseItem.LENGTH);
                }
                return buffer;
            }

            public VoltageDefenseItem this[int i]
            {
                get { return this._defenseList[i]; }
                set { this._defenseList[i] = value; }
            }
        } ;

        public class VoltageDefenseItem
        {
            public const int LENGTH = 8;
            private ushort[] _values = new ushort[LENGTH];

            public override string ToString()
            {
                return "������ �� ����������";
            }

            public VoltageDefenseItem()
            {
            }

            public VoltageDefenseItem(ushort[] buffer)
            {
                this.SetItem(buffer);
            }

            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [Category("������� ���")]
            [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            public ushort[] Values
            {
                get { return this._values; }
                set { this.SetItem(value); }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("����� ������ �� ����������", LENGTH,
                                                          "����� ����� ������ ���� ������" + LENGTH);
                }
                this._values = buffer;
            }

            private VoltageDefenseType _type = VoltageDefenseType.U;

            [XmlAttribute("���")]
            [Browsable(false)]
            public VoltageDefenseType DefenseType
            {
                get { return this._type; }
                set { this._type = value; }
            }

            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    ushort index = Common.GetBits(this._values[0], 0, 1, 2);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte) (Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set { this._values[0] = Common.SetBits(this._values[0], (ushort) Strings.Modes.IndexOf(value), 0, 1, 2); }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool AVR
            {
                get { return Common.GetBit(this._values[0], 5); }
                set { this._values[0] = Common.SetBit(this._values[0], 5, value); }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool APV
            {
                get { return Common.GetBit(this._values[0], 6); }
                set { this._values[0] = Common.SetBit(this._values[0], 6, value); }
            }

            [XmlAttribute("����")]
            [DisplayName("����")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool UROV
            {
                get { return Common.GetBit(this._values[0], 7); }
                set { this._values[0] = Common.SetBit(this._values[0], 7, value); }
            }

            [XmlAttribute("���_�������")]
            [DisplayName("��� ��������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool APV_Return
            {
                get { return Common.GetBit(this._values[0], 15); }
                set { this._values[0] = Common.SetBit(this._values[0], 15, value); }
            }

            [XmlAttribute("�������")]
            [DisplayName("�������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool Return
            {
                get { return Common.GetBit(this._values[0], 14); }
                set { this._values[0] = Common.SetBit(this._values[0], 14, value); }
            }

            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Category("��������� ���")]
            public string Parameter
            {
                get
                {
                    string ret = "";
                    int index = (Common.GetBits(this._values[0], 8, 9, 10, 11) >> 8);
                    switch (this._type)
                    {
                        case VoltageDefenseType.U:
                            if (index >= Strings.VoltageParameterU.Count)
                            {
                                index = Strings.VoltageParameterU.Count - 1;
                            }
                            ret = Strings.VoltageParameterU[index];
                            break;
                        case VoltageDefenseType.U0:
                            if (index >= Strings.VoltageParameterU0.Count)
                            {
                                index = Strings.VoltageParameterU0.Count - 1;
                            }
                            ret = Strings.VoltageParameterU0[index];
                            break;
                        case VoltageDefenseType.U2:
                            if (index >= Strings.VoltageParameterU2.Count)
                            {
                                index = Strings.VoltageParameterU2.Count - 1;
                            }
                            ret = Strings.VoltageParameterU2[index];
                            break;
                        default:
                            break;
                    }
                    return ret;
                }
                set
                {
                    switch (this._type)
                    {
                        case VoltageDefenseType.U:
                            this._values[0] =
                                Common.SetBits(this._values[0], (ushort)Strings.VoltageParameterU.IndexOf(value), 8, 9, 10, 11);
                            break;
                        case VoltageDefenseType.U2:
                            this._values[0] =
                                Common.SetBits(this._values[0], (ushort)Strings.VoltageParameterU2.IndexOf(value), 8, 9, 10, 11);
                            break;
                        case VoltageDefenseType.U0:
                            this._values[0] =
                                Common.SetBits(this._values[0], (ushort)Strings.VoltageParameterU0.IndexOf(value), 8, 9, 10, 11);
                            break;
                        default:
                            break;
                    }
                }
            }

            [XmlAttribute("����������_�����")]
            [DisplayName("���������� �����")]
            [Description("����� ����� ����������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (LogicTypeConverter))]
            public string BlockingNumber
            {
                get
                {
                    ushort value = (ushort) (this._values[1] & 0xFF);
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set { this._values[1] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlAttribute("������������_�������")]
            [DisplayName("������������ �������")]
            [Description("������� ������������")]
            [Category("��������� ���")]
            public double WorkConstraint
            {
                get
                {
                    double ret;
                    ret = Measuring.GetConstraint(this._values[2], ConstraintKoefficient.K_25600);
                    return ret;
                }
                set { this._values[2] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
            }

            [XmlAttribute("������������_�����")]
            [DisplayName("������������ �����")]
            [Description("�������� ������� ������������")]
            [Category("��������� ���")]
            public ulong WorkTime
            {
                get { return Measuring.GetTime(this._values[3]); }
                set { this._values[3] = Measuring.SetTime(value); }
            }

            [XmlAttribute("�������_�������")]
            [DisplayName("������� �������")]
            [Category("��������� ���")]
            public double ReturnConstraint
            {
                get
                {
                    double ret;
                    ret = Measuring.GetConstraint(this._values[4], ConstraintKoefficient.K_25600);
                    return ret;
                }
                set { this._values[4] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
            }

            [XmlAttribute("�������_�����")]
            [DisplayName("������� �����")]
            [Description("�������� ������� ��������")]
            [Category("��������� ���")]
            public ulong ReturnTime
            {
                get { return Measuring.GetTime(this._values[5]); }
                set { this._values[5] = Measuring.SetTime(value); }
            }

            private string _name;

            [Browsable(false)]
            [XmlIgnore]
            public string Name
            {
                get { return this._name; }
                set { this._name = value; }
            }
        }

        #endregion

        #region �������� ����� �� �������

        private slot _frequenceDefensesMain = new slot(0x1140, 0x1160);
        private slot _frequenceDefensesReserve = new slot(0x1160, 0x1180);

        public void LoadFrequenceDefenses()
        {
            LoadSlot(DeviceNumber, this._frequenceDefensesReserve, "LoadFrequenceDefensesReserve" + DeviceNumber, this);
            LoadSlot(DeviceNumber, this._frequenceDefensesMain, "LoadFrequenceDefensesMain" + DeviceNumber, this);
        }

        public void SaveFrequenceDefenses()
        {
            Array.ConstrainedCopy(this.FrequenceDefensesMain.ToUshort(), 0, this._frequenceDefensesMain.Value, 0,
                                  CFrequenceDefenses.LENGTH);
            Array.ConstrainedCopy(this.FrequenceDefensesReserve.ToUshort(), 0, this._frequenceDefensesReserve.Value, 0,
                                  CFrequenceDefenses.LENGTH);
            SaveSlot(DeviceNumber, this._frequenceDefensesReserve, "SaveFrequenceDefensesReserve" + DeviceNumber, this);
            SaveSlot(DeviceNumber, this._frequenceDefensesMain, "SaveFrequenceDefensesMain" + DeviceNumber, this);
        }

        public event Handler FrequenceDefensesLoadOk;
        public event Handler FrequenceDefensesLoadFail;
        public event Handler FrequenceDefensesSaveOk;
        public event Handler FrequenceDefensesSaveFail;

        private CFrequenceDefenses _cFrequenceDefensesMain = new CFrequenceDefenses();
        private CFrequenceDefenses _cFrequenceDefensesReserve = new CFrequenceDefenses();

        [XmlElement("������_��_�������_��������")]
        [DisplayName("�������� ������")]
        [Description("������ �� ������� - �������� ������ �������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("������ �� �������")]
        public CFrequenceDefenses FrequenceDefensesMain
        {
            get { return this._cFrequenceDefensesMain; }
            set { this._cFrequenceDefensesMain = value; }
        }

        [XmlElement("������_��_�������_���������")]
        [DisplayName("��������� ������")]
        [Description("������ �� ������� - ��������� ������ �������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("������ �� �������")]
        public CFrequenceDefenses FrequenceDefensesReserve
        {
            get { return this._cFrequenceDefensesReserve; }
            set { this._cFrequenceDefensesReserve = value; }
        }

        public class CFrequenceDefenses : ICollection
        {
            public const int LENGTH = 0x20;
            public const int COUNT = 4;

            #region ICollection Members

            public void Add(FrequenceDefenseItem item)
            {
                this._defenseList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            [XmlIgnore]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            [XmlIgnore]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._defenseList.GetEnumerator();
            }

            #endregion

            private List<FrequenceDefenseItem> _defenseList = new List<FrequenceDefenseItem>(COUNT);

            public CFrequenceDefenses()
            {
                this.SetBuffer(new ushort[LENGTH]);
            }

            public void SetBuffer(ushort[] buffer)
            {
                this._defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("������ �� �������", LENGTH,
                                                          "����� ��� ����� �� ������� ������ ���� " + LENGTH);
                }
                for (int i = 0; i < LENGTH; i += FrequenceDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[FrequenceDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, FrequenceDefenseItem.LENGTH);
                    this._defenseList.Add(new FrequenceDefenseItem(temp));
                }
                this._defenseList[0].Name = "F>";
                this._defenseList[1].Name = "F>>";
                this._defenseList[2].Name = "F<";
                this._defenseList[3].Name = "F<<";
            }

            [DisplayName("����������")]
            public int Count
            {
                get { return this._defenseList.Count; }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                for (int i = 0; i < this.Count; i++)
                {
                    Array.ConstrainedCopy(this._defenseList[i].Values, 0, buffer, i*FrequenceDefenseItem.LENGTH,
                                          FrequenceDefenseItem.LENGTH);
                }
                return buffer;
            }

            public FrequenceDefenseItem this[int i]
            {
                get { return this._defenseList[i]; }
                set { this._defenseList[i] = value; }
            }
        } ;

        public class FrequenceDefenseItem
        {
            public const int LENGTH = 8;
            private ushort[] _values = new ushort[LENGTH];

            private string _name;

            [Browsable(false)]
            [XmlIgnore]
            public string Name
            {
                get { return this._name; }
                set { this._name = value; }
            }

            public FrequenceDefenseItem()
            {
            }

            public FrequenceDefenseItem(ushort[] buffer)
            {
                this.SetItem(buffer);
            }

            public override string ToString()
            {
                return "������ �� �������";
            }

            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [Category("������� ���")]
            [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            public ushort[] Values
            {
                get { return this._values; }
                set { this.SetItem(value); }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("����� ������ �� �������", LENGTH,
                                                          "����� ������� ������ ���� ������" + LENGTH);
                }
                this._values = buffer;
            }

            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [Category("��������� ���")]
            [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            [TypeConverter(typeof (ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    ushort index = Common.GetBits(this._values[0], 0, 1, 2);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte) (Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set { this._values[0] = Common.SetBits(this._values[0], (ushort) Strings.Modes.IndexOf(value), 0, 1, 2); }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool AVR
            {
                get { return Common.GetBit(this._values[0], 5); }
                set { this._values[0] = Common.SetBit(this._values[0], 5, value); }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool APV
            {
                get { return Common.GetBit(this._values[0], 6); }
                set { this._values[0] = Common.SetBit(this._values[0], 6, value); }
            }

            [XmlAttribute("����")]
            [DisplayName("����")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool UROV
            {
                get { return Common.GetBit(this._values[0], 7); }
                set { this._values[0] = Common.SetBit(this._values[0], 7, value); }
            }

            [XmlAttribute("�������_���")]
            [DisplayName("��� ��������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool APV_Return
            {
                get { return Common.GetBit(this._values[0], 15); }
                set { this._values[0] = Common.SetBit(this._values[0], 15, value); }
            }

            [XmlAttribute("�������")]
            [DisplayName("�������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool Return
            {
                get { return Common.GetBit(this._values[0], 14); }
                set { this._values[0] = Common.SetBit(this._values[0], 14, value); }
            }

            [XmlAttribute("����������_�����")]
            [DisplayName("���������� �����")]
            [Description("����� ����� ����������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ModeTypeConverter))]
            public string BlockingNumber
            {
                get
                {
                    ushort value = (ushort) (this._values[1] & 0xFF);
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set { this._values[1] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlAttribute("������������_�������")]
            [DisplayName("������� ������������")]
            [Category("��������� ���")]
            public double WorkConstraint
            {
                get
                {
                    double ret;
                    ret = Measuring.GetConstraint(this._values[2], ConstraintKoefficient.K_25600);
                    return ret;
                }
                set { this._values[2] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
            }

            [XmlAttribute("������������_�����")]
            [DisplayName("����� ������������")]
            [Description("�������� ������� ������������")]
            [Category("��������� ���")]
            public ulong WorkTime
            {
                get { return Measuring.GetTime(this._values[3]); }
                set { this._values[3] = Measuring.SetTime(value); }
            }

            [XmlAttribute("������������_���")]
            [DisplayName("������� ���")]
            [Description("������� ������������ ���")]
            [Category("��������� ���")]
            public double ConstraintAPV
            {
                get
                {
                    double ret;
                    ret = Measuring.GetConstraint(this._values[4], ConstraintKoefficient.K_25600);
                    return ret;
                }
                set { this._values[4] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
            }

            [XmlAttribute("�������_�����")]
            [DisplayName("����� ��������")]
            [Description("�������� ������� ��������")]
            [Category("��������� ���")]
            public ulong ReturnTime
            {
                get { return Measuring.GetTime(this._values[5]); }
                set { this._values[5] = Measuring.SetTime(value); }
            }
        }

        #endregion

        #region ������ ���������

        private slot _engineDefenses = new slot(0x1120, 0x1133);

        public void LoadEngineDefenses()
        {
            LoadSlot(DeviceNumber, this._engineDefenses, "LoadEngineDefenses" + DeviceNumber, this);
        }

        public void SaveEngineDefenses()
        {
            Array.ConstrainedCopy(this.EngineDefenses.ToUshort(), 0, this._engineDefenses.Value, 0, CEngingeDefenses.LENGTH);
            SaveSlot(DeviceNumber, this._engineDefenses, "SaveEngineDefenses" + DeviceNumber, this);
        }

        public event Handler EngineDefensesLoadOk;
        public event Handler EngineDefensesLoadFail;
        public event Handler EngineDefensesSaveOk;
        public event Handler EngineDefensesSaveFail;


        private CEngingeDefenses _cengineDefenses = new CEngingeDefenses();

        [XmlElement("������_���������")]
        [DisplayName("������ ���������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("������ ���������")]
        public CEngingeDefenses EngineDefenses
        {
            get { return this._cengineDefenses; }
        }

        [XmlElement("�����_�������_���������")]
        [DisplayName("����� �������")]
        [Description("����� ������� ���������")]
        [Category("������ ���������")]
        public ushort EngineHeatingTime
        {
            get { return this._engineDefenses.Value[0]; }
            set { this._engineDefenses.Value[0] = value; }
        }

        [XmlElement("�����_����������_���������")]
        [DisplayName("����� ����������")]
        [Description("����� ���������� ���������")]
        [Category("������ ���������")]
        public ushort EngineCoolingTime
        {
            get { return this._engineDefenses.Value[1]; }
            set { this._engineDefenses.Value[1] = value; }
        }

        [XmlElement("���_���������")]
        [DisplayName("��� ���������")]
        [Description("��� ���������,In")]
        [Category("������ ���������")]
        public double EngineIn
        {
            get { return Measuring.GetConstraint(this._engineDefenses.Value[2], ConstraintKoefficient.K_4001); }
            set { this._engineDefenses.Value[2] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4001); }
        }

        [XmlElement("�������_������������_���������")]
        [DisplayName("������� ������������")]
        [Description("������� ������������,I�")]
        [Category("������ ���������")]
        public double EngineWorkConstraint
        {
            get { return Measuring.GetConstraint(this._engineDefenses.Value[3], ConstraintKoefficient.K_4001); }
            set { this._engineDefenses.Value[3] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4001); }
        }

        [XmlElement("�����_�����_���������")]
        [DisplayName("����� �����")]
        [Category("������ ���������")]
        public ulong EnginePuskTime
        {
            get { return Measuring.GetTime(this._engineDefenses.Value[4]); }
            set { this._engineDefenses.Value[4] = Measuring.SetTime(value); }
        }

        [XmlElement("���_���������_���������")]
        [DisplayName("��������� ���������")]
        [Description("��������� ���������,%")]
        [Category("������ ���������")]
        public double EngineHeatPuskConstraint
        {
            get { return Measuring.GetConstraint(this._engineDefenses.Value[5], ConstraintKoefficient.K_25600); }
            set { this._engineDefenses.Value[5] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
        }

        [XmlElement("����_Q_�����")]
        [DisplayName("���� Q �����")]
        [TypeConverter(typeof (LogicTypeConverter))]
        [Category("������ ���������")]
        public string EngineResetQ
        {
            get
            {
                ushort index = (ushort) (this._engineDefenses.Value[6] & 0xFF);
                if (index >= Strings.Logic.Count)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set { this._engineDefenses.Value[6] = (ushort) Strings.Logic.IndexOf(value); }
        }

        [XmlElement("����_N_����")]
        [DisplayName("���� N ����")]
        [TypeConverter(typeof (LogicTypeConverter))]
        [Category("������ ���������")]
        public string EnginePuskN
        {
            get
            {
                ushort index = (ushort) (this._engineDefenses.Value[7] & 0xFF);
                if (index >= Strings.Logic.Count)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set { this._engineDefenses.Value[7] = (ushort) Strings.Logic.IndexOf(value); }
        }

        [XmlElement("�����_Q")]
        [DisplayName("����� Q")]
        [TypeConverter(typeof (ModeLightTypeConverter))]
        [Category("������ ���������")]
        public string QMode
        {
            get
            {
                string ret;
                bool bit = Common.GetBit(this._engineDefenses.Value[12], 0);
                ret = bit ? "�������" : "��������";
                return ret;
            }
            set
            {
                int index = Strings.ModesLight.IndexOf(value);
                this._engineDefenses.Value[12] = (0 == index)
                                                ? Common.SetBit(this._engineDefenses.Value[12], 0, false)
                                                : Common.SetBit(this._engineDefenses.Value[12], 0, true);
            }
        }

        [XmlElement("�������_Q")]
        [DisplayName("������� Q")]
        [Category("������ ���������")]
        public double QConstraint
        {
            get { return Measuring.GetConstraint(this._engineDefenses.Value[13], ConstraintKoefficient.K_25600); }
            set { this._engineDefenses.Value[13] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
        }

        [XmlElement("�����_Q")]
        [DisplayName("����� Q")]
        [Category("������ ���������")]
        public ushort QTime
        {
            get { return this._engineDefenses.Value[14]; }
            set { this._engineDefenses.Value[14] = value; }
        }

        [XmlElement("N_����")]
        [DisplayName("����� ������")]
        [Category("������ ���������")]
        public ushort BlockingPuskCount
        {
            get { return this._engineDefenses.Value[16]; }
            set { this._engineDefenses.Value[16] = value; }
        }

        [XmlElement("N_���")]
        [DisplayName("����� ���.������")]
        [Category("������ ���������")]
        public ushort BlockingHeatCount
        {
            get { return this._engineDefenses.Value[15]; }
            set { this._engineDefenses.Value[15] = value; }
        }

        [XmlElement("T_����")]
        [DisplayName("������������ ����������")]
        [Category("������ ���������")]
        public ushort BlockingDuration
        {
            get { return this._engineDefenses.Value[17]; }
            set { this._engineDefenses.Value[17] = value; }
        }

        [XmlElement("T_����")]
        [DisplayName("����� ����������")]
        [Category("������ ���������")]
        public ushort BlockingTime
        {
            get { return this._engineDefenses.Value[18]; }
            set { this._engineDefenses.Value[18] = value; }
        }

        public class CEngingeDefenses : ICollection
        {
            public const int LENGTH = 19;
            public const int COUNT = 2;

            private ushort[] _buffer = new ushort[LENGTH];
            private List<EngineDefenseItem> _defenseList = new List<EngineDefenseItem>(COUNT);

            public CEngingeDefenses()
            {
                this.SetBuffer(new ushort[LENGTH]);
            }

            public void SetBuffer(ushort[] buffer)
            {
                this._defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("������ ���������", LENGTH,
                                                          "����� ��� ����� ��������� ������ ���� " + LENGTH);
                }
                this._buffer = buffer;
                for (int i = 8; i < 12; i += EngineDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[EngineDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, EngineDefenseItem.LENGTH);
                    this._defenseList.Add(new EngineDefenseItem(temp));
                }
                this._defenseList[0].Name = "Q>";
                this._defenseList[1].Name = "Q>>";
            }

            [DisplayName("����������")]
            public int Count
            {
                get { return this._defenseList.Count; }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                Array.ConstrainedCopy(this._buffer, 0, buffer, 0, LENGTH);

                for (int i = 0; i < COUNT; i++)
                {
                    Array.ConstrainedCopy(this._defenseList[i].Values, 0, buffer, 8 + EngineDefenseItem.LENGTH*i,
                                          EngineDefenseItem.LENGTH);
                }
                return buffer;
            }

            public EngineDefenseItem this[int i]
            {
                get { return this._defenseList[i]; }
                set { this._defenseList[i] = value; }
            }

            #region ICollection Members

            public void CopyTo(Array array, int index)
            {
            }

            public void Add(EngineDefenseItem item)
            {
                this._defenseList.Add(item);
            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._defenseList.GetEnumerator();
            }

            #endregion
        }

        public class EngineDefenseItem
        {
            public const int LENGTH = 2;
            private ushort[] _values = new ushort[LENGTH];

            public override string ToString()
            {
                return "������ ���������";
            }

            public EngineDefenseItem()
            {
            }

            public EngineDefenseItem(ushort[] buffer)
            {
                this.SetItem(buffer);
            }

            private string _name;

            [Browsable(false)]
            [XmlIgnore]
            public string Name
            {
                get { return this._name; }
                set { this._name = value; }
            }


            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [Category("������� ���")]
            [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            public ushort[] Values
            {
                get { return this._values; }
                set { this.SetItem(value); }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("����� ������ �� ����������", LENGTH,
                                                          "����� ����� ������ ���� ������" + LENGTH);
                }
                this._values = buffer;
            }

            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    ushort index = Common.GetBits(this._values[0], 0, 1, 2);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte) (Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set { this._values[0] = Common.SetBits(this._values[0], (ushort) Strings.Modes.IndexOf(value), 0, 1, 2); }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool AVR
            {
                get { return Common.GetBit(this._values[0], 5); }
                set { this._values[0] = Common.SetBit(this._values[0], 5, value); }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool APV
            {
                get { return Common.GetBit(this._values[0], 6); }
                set { this._values[0] = Common.SetBit(this._values[0], 6, value); }
            }

            [XmlAttribute("����")]
            [DisplayName("����")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool UROV
            {
                get { return Common.GetBit(this._values[0], 7); }
                set { this._values[0] = Common.SetBit(this._values[0], 7, value); }
            }

            [XmlAttribute("�������")]
            [DisplayName("������� ������������")]
            [Category("��������� ���")]
            public double WorkConstraint
            {
                get
                {

                    return Measuring.GetConstraint(this._values[1], ConstraintKoefficient.K_25600);
                    
                }
                set { this._values[1] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
            }
        }

        #endregion

        #region ���� - �����

        private slot _datetime = new slot(0x200, 0x207);

        public void LoadTimeCycle()
        {
            LoadSlotCycle(DeviceNumber, this._datetime, "LoadDateTime" + DeviceNumber, new TimeSpan(1000), 10, this);
        }

        [Browsable(false)]
        public byte[] DateTime
        {
            get { return Common.TOBYTES(this._datetime.Value, true); }
            set { this._datetime.Value = Common.TOWORDS(value, true); }
        }

        #endregion

        #region ������� ��������/����������
        public void CrushOsc()
        {
            SaveSlot6(DeviceNumber, this._oscCrush, "����� ������������� - ����������� ����", this);
        }
        public void LoadDiagnostic()
        {
            LoadBitSlot(DeviceNumber, this._diagnostic, "LoadDiagnostic" + DeviceNumber, this);
        }

        public void LoadDiagnosticCycle()
        {
            LoadSlotCycle(DeviceNumber, this._diagnostic, "LoadDiagnostic" + DeviceNumber, new TimeSpan(1000), 10, this);
        }

        public void LoadAnalogSignalsCycle()
        {
            LoadSlotCycle(DeviceNumber, this._analog, "LoadAnalogSignals" + DeviceNumber, new TimeSpan(1000), 10, this);
        }

        public void LoadAnalogSignals()
        {
            LoadSlot(DeviceNumber, this._analog, "LoadAnalogSignals" + DeviceNumber, this);
        }

        public void LoadInputSignals()
        {
            LoadSlot(DeviceNumber, this._inputSignals, "LoadInputSignals" + DeviceNumber, this);
            LoadSlot(DeviceNumber, this._konfCount, "�" + DeviceNumber + " ��������� ������������", this);
        }

        public void LoadOutputSignals()
        {
            LoadSlot(DeviceNumber, this._outputSignals, "LoadOutputSignals" + DeviceNumber, this);
        }

        public void LoadExternalDefenses()
        {
            LoadSlot(DeviceNumber, this._externalDefenses, "LoadExternalDefenses" + DeviceNumber, this);
        }

        public void SaveExternalDefenses()
        {
            if (CExternalDefenses.COUNT == this.ExternalDefenses.Count)
            {
                Array.ConstrainedCopy(this.ExternalDefenses.ToUshort(), 0, this._externalDefenses.Value, 0,
                                      CExternalDefenses.LENGTH);
            }
            SaveSlot(DeviceNumber, this._externalDefenses, "SaveExternalDefenses" + DeviceNumber, this);
        }


        public void SaveOutputSignals()
        {
            if (COutputRele.COUNT == this.OutputRele.Count)
            {
                Array.ConstrainedCopy(this.OutputRele.ToUshort(), 0, this._outputSignals.Value, 0x40, COutputRele.LENGTH);
            }
            if (COutputIndicator.COUNT == this.OutputIndicator.Count)
            {
                Array.ConstrainedCopy(this.OutputIndicator.ToUshort(), 0, this._outputSignals.Value, 0x60, COutputIndicator.LENGTH);
            }
            SaveSlot(DeviceNumber, this._outputSignals, "SaveOutputSignals" + DeviceNumber, this);
        }

        public void SaveInputSignals()
        {
            SaveSlot(DeviceNumber, this._inputSignals, "SaveInputSignals" + DeviceNumber, this);
            SaveSlot(DeviceNumber, this._konfCount, "SaveKonfCount" + DeviceNumber, this);
        }

        public void RemoveDiagnostic()
        {
            this.MB.RemoveQuery("LoadDiagnostic" + DeviceNumber);
        }

        public void RemoveAnalogSignals()
        {
            this.MB.RemoveQuery("LoadAnalogSignals" + DeviceNumber);
        }

        public void MakeDiagnosticQuick()
        {
            MakeQueryQuick("LoadDiagnostic" + DeviceNumber);
        }

        public void MakeAnalogSignalsQuick()
        {
            MakeQueryQuick("LoadAnalogSignals" + DeviceNumber);
        }

        public void MakeDiagnosticSlow()
        {
            MakeQuerySlow("LoadDiagnostic" + DeviceNumber);
        }

        public void MakeAnalogSignalsSlow()
        {
            MakeQuerySlow("LoadAnalogSignals");
        }

        public void RemoveDateTime()
        {
            this.MB.RemoveQuery("LoadDateTime" + DeviceNumber);
        }

        public void MakeDateTimeQuick()
        {
            MakeQueryQuick("LoadDateTime" + DeviceNumber);
        }

        public void MakeDateTimeSlow()
        {
            MakeQuerySlow("LoadDateTime" + DeviceNumber);
        }

        public void SuspendDateTime(bool suspend)
        {
            this.MB.SuspendQuery("LoadDateTime" + DeviceNumber, suspend);
        }

        public void SuspendSystemJournal(bool suspend)
        {
            List<Query> q = this.MB.GetQueryByExpression("LoadSJRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                this.MB.SuspendQuery(q[i].name, suspend);
            }
        }

        public void SuspendAlarmJournal(bool suspend)
        {
            List<Query> q = this.MB.GetQueryByExpression("LoadALRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                this.MB.SuspendQuery(q[i].name, suspend);
            }
        }

        public void RemoveAlarmJournal()
        {
            List<Query> q = this.MB.GetQueryByExpression("LoadALRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                this.MB.RemoveQuery(q[i].name);
            }
            this._stopAlarmJournal = true;
        }

        public void RemoveSystemJournal()
        {
            List<Query> q = this.MB.GetQueryByExpression("LoadSJRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                this.MB.RemoveQuery(q[i].name);
            }
            this._stopSystemJournal = true;
        }

        public void SaveDateTime()
        {
            SaveSlot(DeviceNumber, this._datetime, "SaveDateTime" + DeviceNumber, this);
        }

        public void LoadSystemJournal()
        {
            LoadSlot(DeviceNumber, this._systemJournal[0], "LoadSJRecord0_" + DeviceNumber, this);
            this._stopSystemJournal = false;
        }

        public void LoadAlarmJournal()
        {
            if (!this._inputSignals.Loaded)
            {
                this.LoadInputSignals();
            }
            this._stopAlarmJournal = false;
            LoadSlot(DeviceNumber, this._alarmJournal[0], "LoadALRecord0_" + DeviceNumber, this);
        }

        #endregion

        #region ������������

        public void Deserialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(binFileName);
            }
            catch (XmlException)
            {
                throw new FileLoadException("���� ������� ��730 ���������", binFileName);
            }

            try
            {
                DeserializeSlot(doc, "/��73X_�������/������", _infoSlot);
                DeserializeSlot(doc, "/��73X_�������/����_�����", this._datetime);
                DeserializeSlot(doc, "/��73X_�������/�������_�������", this._inputSignals);
                DeserializeSlot(doc, "/��73X_�������/��������_�������", this._outputSignals);
                DeserializeSlot(doc, "/��73X_�������/�����������", this._diagnostic);
                DeserializeSlot(doc, "/��73X_�������/������_���������", this._engineDefenses);
                DeserializeSlot(doc, "/��73X_�������/������_�������", this._externalDefenses);
                DeserializeSlot(doc, "/��73X_�������/������_�������_��������", this._tokDefensesMain);
                DeserializeSlot(doc, "/��73X_�������/������_�������_���������", this._tokDefensesReserve);
                DeserializeSlot(doc, "/��73X_�������/������_�������2_��������", this._tokDefenses2Main);
                DeserializeSlot(doc, "/��73X_�������/������_�������2_���������", this._tokDefenses2Reserve);
                DeserializeSlot(doc, "/��73X_�������/������_����������_��������", this._voltageDefensesMain);
                DeserializeSlot(doc, "/��73X_�������/������_����������_���������", this._voltageDefensesReserve);
                DeserializeSlot(doc, "/��73X_�������/����������", this._automaticsPage);
                this.DeserializeBit(doc, "/��73X_�������/����������_����������_�������", out this._bits);
                DeserializeSlot(doc, "/��73X_�������/������_�������_��������", this._frequenceDefensesMain);
                DeserializeSlot(doc, "/��73X_�������/������_�������_���������", this._frequenceDefensesReserve);
                DeserializeSlot(doc, "/��73X_�������/������������_�����������", this._konfCount);
            }
            catch (NullReferenceException)
            {
                throw new FileLoadException("���� ������� ��730 ���������", binFileName);
            }

                        try
            {
                this.ExternalDefenses.SetBuffer(this._externalDefenses.Value);
            ushort[] buffer = new ushort[this._tokDefensesReserve.Size + this._tokDefenses2Reserve.Size];
            Array.ConstrainedCopy(this._tokDefensesReserve.Value, 0, buffer, 0, this._tokDefensesReserve.Size);
            Array.ConstrainedCopy(this._tokDefenses2Reserve.Value, 0, buffer, this._tokDefensesReserve.Size, this._tokDefenses2Reserve.Size);
                this.TokDefensesReserve.SetBuffer(buffer);
            buffer = new ushort[this._tokDefensesMain.Size + this._tokDefenses2Main.Size];
            Array.ConstrainedCopy(this._tokDefensesMain.Value, 0, buffer, 0, this._tokDefensesMain.Size);
            Array.ConstrainedCopy(this._tokDefenses2Main.Value, 0, buffer, this._tokDefensesMain.Size, this._tokDefenses2Main.Size);
                this.TokDefensesMain.SetBuffer(buffer);
                this.VoltageDefensesMain.SetBuffer(this._voltageDefensesMain.Value);
                this.VoltageDefensesReserve.SetBuffer(this._voltageDefensesReserve.Value);
                this.EngineDefenses.SetBuffer(this._engineDefenses.Value);
            ushort[] releBuffer = new ushort[COutputRele.LENGTH];
            Array.ConstrainedCopy(this._outputSignals.Value, 0x40, releBuffer, 0, COutputRele.LENGTH);
                this.OutputRele.SetBuffer(releBuffer);
            ushort[] outputIndicator = new ushort[COutputIndicator.LENGTH];
            Array.ConstrainedCopy(this._outputSignals.Value, 0x60, outputIndicator, 0, COutputIndicator.LENGTH);
                this.OutputIndicator.SetBuffer(outputIndicator);
                this.FrequenceDefensesMain.SetBuffer(this._frequenceDefensesMain.Value);
                this.FrequenceDefensesReserve.SetBuffer(this._frequenceDefensesReserve.Value);
            }
            catch (NullReferenceException)
            {
                throw new FileLoadException("���� ������� ��730 ���������", binFileName);
            }
        }

        void DeserializeBit(XmlDocument doc, string nodePath, out BitArray Bits)
        {
            Bits = Common.StringToBits(doc.SelectSingleNode(nodePath).InnerText);
        }

        private static void DeserializeSlot(XmlDocument doc, string nodePath, slot slot)
        {
            slot.Value = Common.TOWORDS(Convert.FromBase64String(doc.SelectSingleNode(nodePath).InnerText), true);
        }


        public void Serialize(string binFileName)
        {
            string xmlFileName = Path.ChangeExtension(binFileName, ".xml");

            XmlSerializer ser = new XmlSerializer(GetType());
            TextWriter writer = new StreamWriter(xmlFileName, false, Encoding.UTF8);
            ser.Serialize(writer, this);
            writer.Close();
           
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("��73X_�������"));

            Array.ConstrainedCopy(this.EngineDefenses.ToUshort(), 0, this._engineDefenses.Value, 0, CEngingeDefenses.LENGTH);
            Array.ConstrainedCopy(this.ExternalDefenses.ToUshort(), 0, this._externalDefenses.Value, 0, CExternalDefenses.LENGTH);
            Array.ConstrainedCopy(this.FrequenceDefensesMain.ToUshort(), 0, this._frequenceDefensesMain.Value, 0,
                                  CFrequenceDefenses.LENGTH);
            Array.ConstrainedCopy(this.FrequenceDefensesReserve.ToUshort(), 0, this._frequenceDefensesReserve.Value, 0,
                                  CFrequenceDefenses.LENGTH);
            Array.ConstrainedCopy(this.OutputRele.ToUshort(), 0, this._outputSignals.Value, 0x40, COutputRele.LENGTH);
            Array.ConstrainedCopy(this.OutputIndicator.ToUshort(), 0, this._outputSignals.Value, 0x60, COutputIndicator.LENGTH);
            Array.ConstrainedCopy(this.TokDefensesMain.ToUshort1(), 0, this._tokDefensesMain.Value, 0, CTokDefenses.LENGTH1);
            Array.ConstrainedCopy(this.TokDefensesMain.ToUshort2(), 0, this._tokDefenses2Main.Value, 0, CTokDefenses.LENGTH2);
            Array.ConstrainedCopy(this.TokDefensesReserve.ToUshort1(), 0, this._tokDefensesReserve.Value, 0, CTokDefenses.LENGTH1);
            Array.ConstrainedCopy(this.TokDefensesReserve.ToUshort2(), 0, this._tokDefenses2Reserve.Value, 0, CTokDefenses.LENGTH2);
            Array.ConstrainedCopy(this.VoltageDefensesMain.ToUshort(), 0, this._voltageDefensesMain.Value, 0,
                                  CVoltageDefenses.LENGTH);
            Array.ConstrainedCopy(this.VoltageDefensesReserve.ToUshort(), 0, this._voltageDefensesReserve.Value, 0,
                                  CVoltageDefenses.LENGTH);

            SerializeSlot(doc, "������", _infoSlot);
            SerializeSlot(doc, "����_�����", this._datetime);
            SerializeSlot(doc, "�������_�������", this._inputSignals);
            SerializeSlot(doc, "��������_�������", this._outputSignals);
            SerializeSlot(doc, "�����������", this._diagnostic);
            SerializeSlot(doc, "������_���������", this._engineDefenses);
            SerializeSlot(doc, "������_�������", this._externalDefenses);
            SerializeSlot(doc, "������_�������_��������", this._tokDefensesMain);
            SerializeSlot(doc, "������_�������_���������", this._tokDefensesReserve);
            SerializeSlot(doc, "������_�������2_��������", this._tokDefenses2Main);
            SerializeSlot(doc, "������_�������2_���������", this._tokDefenses2Reserve);
            SerializeSlot(doc, "������_����������_��������", this._voltageDefensesMain);
            SerializeSlot(doc, "������_����������_���������", this._voltageDefensesReserve);
            SerializeSlot(doc, "����������", this._automaticsPage);
            this.SerializeBits(doc, "����������_����������_�������", this._bits);
            SerializeSlot(doc, "������_�������_��������", this._frequenceDefensesMain);
            SerializeSlot(doc, "������_�������_���������", this._frequenceDefensesReserve);
            SerializeSlot(doc, "������������_�����������", this._konfCount);

            doc.Save(binFileName);
        }

        void SerializeBits(XmlDocument doc, string nodeName, BitArray Bits)
        {
            XmlElement element = doc.CreateElement(nodeName);
            element.InnerText = Common.BitsToString(Bits);
            doc.DocumentElement.AppendChild(element);
        }

        private static void SerializeSlot(XmlDocument doc, string nodeName, slot slot)
        {
            XmlElement element = doc.CreateElement(nodeName);
            element.InnerText = Convert.ToBase64String(Common.TOBYTES(slot.Value, true));
            doc.DocumentElement.AppendChild(element);
        }

        #endregion

        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof (MR730); }
        }

        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mr700; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "��730"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion
        
        private Oscilloscope _oscilloscope;

        public Oscilloscope Oscilloscope
        {
            get { return this._oscilloscope; }
        }

        public void ConfirmConstraint()
        {
            SetBit(DeviceNumber, 0, true, "��73X ������ �������������", this);
        }

        private slot _constraintSelector = new slot(0x400, 0x401);

        public void SelectConstraintGroup(bool mainGroup)
        {
            this._constraintSelector.Value[0] = mainGroup ? (ushort) 0 : (ushort) 1;
            SaveSlot(DeviceNumber, this._constraintSelector, "��73X �" + DeviceNumber + " ������������ ������ �������", this);
        }

        private new void mb_CompleteExchange(object sender, Query query)
        {
            if (this._oscilloscope == null) // ��� �������� ������� �� ������ ����� ������������� �������� ������ (��������� ������� � ������������ ��� ����������
                // � ����������� �������������� � �����.
            {
                this.MB.CompleteExchange -= this.mb_CompleteExchange;
                return;
            }
            this._oscilloscope.OnMbCompleteExchange(sender,query);

            #region ������ �� �������

            if ("LoadFrequenceDefensesReserve" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    this._frequenceDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);
                    this.FrequenceDefensesReserve.SetBuffer(this._frequenceDefensesReserve.Value);
                }
            }
            if ("LoadFrequenceDefensesMain" + DeviceNumber == query.name)
            {
                this._frequenceDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
                this.FrequenceDefensesMain.SetBuffer(this._frequenceDefensesMain.Value);
                if (0 == query.fail)
                {
                    if (null != this.FrequenceDefensesLoadOk)
                    {
                        this.FrequenceDefensesLoadOk(this);
                    }
                }
                else
                {
                    if (null != this.FrequenceDefensesLoadFail)
                    {
                        this.FrequenceDefensesLoadFail(this);
                    }
                }
            }
            if ("SaveFrequenceDefensesMain" + DeviceNumber == query.name)
            {
                Raise(query, this.FrequenceDefensesSaveOk, this.FrequenceDefensesSaveFail);
            }

            #endregion

            #region ������ ���������

            if ("LoadEngineDefenses" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    this._engineDefenses.Value = Common.TOWORDS(query.readBuffer, true);
                    this.EngineDefenses.SetBuffer(this._engineDefenses.Value);
                    if (null != this.EngineDefensesLoadOk)
                    {
                        this.EngineDefensesLoadOk(this);
                    }
                    else
                    {
                        if (null != this.EngineDefensesLoadFail)
                        {
                            this.EngineDefensesLoadFail(this);
                        }
                    }
                }
            }
            if ("SaveEngineDefenses" + DeviceNumber == query.name)
            {
                Raise(query, this.EngineDefensesSaveOk, this.EngineDefensesSaveFail);
            }

            #endregion

            #region ������ ����������

            if ("LoadVoltageDefensesReserve" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    this._voltageDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);
                    this.VoltageDefensesReserve.SetBuffer(this._voltageDefensesReserve.Value);
                }
            }
            if ("LoadVoltageDefensesMain" + DeviceNumber == query.name)
            {
                this._voltageDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
                this.VoltageDefensesMain.SetBuffer(this._voltageDefensesMain.Value);
                if (0 == query.fail)
                {
                    if (null != this.VoltageDefensesLoadOk)
                    {
                        this.VoltageDefensesLoadOk(this);
                    }
                }
                else
                {
                    if (null != this.VoltageDefensesLoadFail)
                    {
                        this.VoltageDefensesLoadFail(this);
                    }
                }
            }
            if ("SaveVoltageDefensesMain" + DeviceNumber == query.name)
            {
                Raise(query, this.VoltageDefensesSaveOk, this.VoltageDefensesSaveFail);
            }

            #endregion

            #region ������� ������

            if ("LoadTokDefensesMain2" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    this._tokDefenses2Main.Value = Common.TOWORDS(query.readBuffer, true);
                }
            }
            if ("LoadTokDefensesReserve2" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    this._tokDefenses2Reserve.Value = Common.TOWORDS(query.readBuffer, true);
                }
            }
            if ("LoadTokDefensesReserve" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    ushort[] buffer = new ushort[this._tokDefensesReserve.Size + this._tokDefenses2Reserve.Size];
                    this._tokDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);
                    Array.ConstrainedCopy(this._tokDefensesReserve.Value, 0, buffer, 0, this._tokDefensesReserve.Size);
                    Array.ConstrainedCopy(this._tokDefenses2Reserve.Value, 0, buffer, this._tokDefensesReserve.Size, this._tokDefenses2Reserve.Size);
                    this._ctokDefensesReserve.SetBuffer(buffer);
                }
            }
            if ("LoadTokDefensesMain" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    ushort[] buffer = new ushort[this._tokDefensesMain.Size + this._tokDefenses2Main.Size];
                    this._tokDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
                    Array.ConstrainedCopy(this._tokDefensesMain.Value, 0, buffer, 0, this._tokDefensesMain.Size);
                    Array.ConstrainedCopy(this._tokDefenses2Main.Value, 0, buffer, this._tokDefensesMain.Size, this._tokDefenses2Main.Size);
                    this._ctokDefensesMain.SetBuffer(buffer);
                    if (null != this.TokDefensesLoadOK)
                    {
                        this.TokDefensesLoadOK(this);
                    }
                }
                else
                {
                    if (null != this.TokDefensesLoadFail)
                    {
                        this.TokDefensesLoadFail(this);
                    }
                }
            }
            if ("SaveTokDefensesMain" + DeviceNumber == query.name)
            {
                Raise(query, this.TokDefensesSaveOK, this.TokDefensesSaveFail);
            }

            #endregion

            #region ������ ������

            if (IsIndexQuery(query.name, "LoadALRecord"))
            {
                int index = GetIndex(query.name, "LoadALRecord");
                if (0 == query.fail)
                {
                    this._alarmJournalRecords.AddMessage(query.readBuffer);
                    if (this._alarmJournalRecords.IsMessageEmpty(index))
                    {
                        if (null != this.AlarmJournalLoadOk)
                        {
                            this.AlarmJournalLoadOk(this);
                        }
                    }
                    else
                    {
                        
                        if (null != this.AlarmJournalRecordLoadOk)
                        {
                            this.AlarmJournalRecordLoadOk(this, index);
                        }
                        index += 1;
                        if (index < ALARMJOURNAL_RECORD_CNT && !this._stopAlarmJournal)
                        {
                            LoadSlot(DeviceNumber, this._alarmJournal[index], "LoadALRecord" + index + "_" + DeviceNumber, this);
                        }
                        else
                        {
                            if (null != this.AlarmJournalLoadOk)
                            {
                                this.AlarmJournalLoadOk(this);
                            }
                        }
                    }
                }
                else
                {
                    if (null != this.AlarmJournalRecordLoadFail)
                    {
                        this.AlarmJournalRecordLoadFail(this, index);
                    }
                }
            }

            #endregion

            #region ������ �������

            if (IsIndexQuery(query.name, "LoadSJRecord"))
            {
                int index = GetIndex(query.name, "LoadSJRecord");
                if (0 == query.fail)
                {
                    if (!this._systemJournalRecords.AddMessage(Common.TOWORDS(query.readBuffer, true)))
                    {
                        if (this.SystemJournalLoadOk != null)
                        {
                            this.SystemJournalLoadOk(this);
                        }
                    }
                    else
                    {
                        if (this.SystemJournalRecordLoadOk != null)
                        {
                            this.SystemJournalRecordLoadOk(this, index);
                        }
                        index += 1;
                        if (index < SYSTEMJOURNAL_RECORD_CNT && !this._stopSystemJournal)
                        {
                            LoadSlot(DeviceNumber, this._systemJournal[index], "LoadSJRecord" + index + "_" + DeviceNumber, this);
                        }
                        else
                        {
                            if (this.SystemJournalLoadOk != null)
                            {
                                this.SystemJournalLoadOk(this);
                            }
                        }
                    }
                }
                else
                {
                    if (this.SystemJournalRecordLoadFail != null)
                    {
                        this.SystemJournalRecordLoadFail(this, index);
                    }
                }
            }

            #endregion

            #region �������� �������

            if ("LoadOutputSignals" + DeviceNumber == query.name)
            {
                this._outputSignals.Value = Common.TOWORDS(query.readBuffer, true);
                ushort[] releBuffer = new ushort[COutputRele.LENGTH];
                Array.ConstrainedCopy(this._outputSignals.Value, 0x40, releBuffer, 0, COutputRele.LENGTH);
                this._outputRele.SetBuffer(releBuffer);

                ushort[] outputIndicator = new ushort[COutputIndicator.LENGTH];
                Array.ConstrainedCopy(this._outputSignals.Value, 0x60, outputIndicator, 0, COutputIndicator.LENGTH);
                this._outputIndicator.SetBuffer(outputIndicator);

                if (0 == query.fail)
                {
                    if (null != this.OutputSignalsLoadOK)
                    {
                        this.OutputSignalsLoadOK(this);
                    }
                }
                else
                {
                    if (null != this.OutputSignalsLoadFail)
                    {
                        this.OutputSignalsLoadFail(this);
                    }
                }
            }
            if ("SaveOutputSignals" + DeviceNumber == query.name)
            {
                Raise(query, this.OutputSignalsSaveOK, this.OutputSignalsSaveFail);
            }

            #endregion

            #region ������� ������

            if ("LoadExternalDefenses" + DeviceNumber == query.name)
            {
                this._externalDefenses.Value = Common.TOWORDS(query.readBuffer, true);
                this.ExternalDefenses.SetBuffer(this._externalDefenses.Value);
                if (0 == query.fail)
                {
                    if (null != this.ExternalDefensesLoadOK)
                    {
                        this.ExternalDefensesLoadOK(this);
                    }
                }
                else
                {
                    if (null != this.ExternalDefensesLoadFail)
                    {
                        this.ExternalDefensesLoadFail(this);
                    }
                }
            }
            if ("SaveExternalDefenses" + DeviceNumber == query.name)
            {
                Raise(query, this.ExternalDefensesSaveOK, this.ExternalDefensesSaveFail);
            }

            #endregion

            #region ������� �������

            if ("LoadInputSignals" + DeviceNumber == query.name)
            {
                Raise(query, this.InputSignalsLoadOK, this.InputSignalsLoadFail, ref this._inputSignals);
            }
            if ("SaveInputSignals" + DeviceNumber == query.name)
            {
                Raise(query, this.InputSignalsSaveOK, this.InputSignalsSaveFail);
            }
            if (query.name == "�" + DeviceNumber + " ��������� ������������")
            {
                Raise(query, this.KonfCountLoadOK, this.KonfCountLoadFail, ref this._konfCount);
            }
            if ("SaveKonfCount" + DeviceNumber == query.name)
            {
                Raise(query, this.KonfCountSaveOK, this.KonfCountSaveFail);
            }
            #endregion

            #region ��� �������� ����������

            if ("LoadAutomaticsPage" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    this._automaticsPage.Value = Common.TOWORDS(query.readBuffer, true);
                    this.AVR_Add_Signals = new BitArray(new byte[] {Common.LOBYTE(this._automaticsPage.Value[8])});
                    if (null != this.AutomaticsPageLoadOK)
                    {
                        this.AutomaticsPageLoadOK(this);
                    }
                }
                else
                {
                    if (null != this.AutomaticsPageLoadFail)
                    {
                        this.AutomaticsPageLoadFail(this);
                    }
                }
            }
            if ("SaveAutomaticsPage" + DeviceNumber == query.name)
            {
                Raise(query, this.AutomaticsPageSaveOK, this.AutomaticsPageSaveFail);
            }

            #endregion

            if ("LoadDiagnostic" + DeviceNumber == query.name)
            {
                Raise(query, this.DiagnosticLoadOk, this.DiagnosticLoadFail, ref this._diagnostic);
            }
            if ("LoadDateTime" + DeviceNumber == query.name)
            {
                Raise(query, this.DateTimeLoadOk, this.DateTimeLoadFail, ref this._datetime);
            }


            if ("LoadAnalogSignals" + DeviceNumber == query.name)
            {
                if (!this._inputSignals.Loaded)
                {
                    this.LoadInputSignals();
                }
                else
                {
                    Raise(query, this.AnalogSignalsLoadOK, this.AnalogSignalsLoadFail, ref this._analog);
                }
            }

            base.mb_CompleteExchange(sender, query);
        }
    }
}