<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
<head>
 <script type="text/javascript">
	function translateBoolean(value, elementId){
 		var result = "";
 		if(value.toString().toLowerCase() == "true")
  			result = "��";
 		else if(value.toString().toLowerCase() == "false")
  			result = "���";
		else 
			result = "������������ ��������"
 		document.getElementById(elementId).innerHTML = result; 
}
  function translateDisrepair(value, elementId){
 		var result = "";
 		if(value.toString().toLowerCase() == "true")
  			result = "���������";
 		else if(value.toString().toLowerCase() == "false")
  			result = "���������";
		else 
			result = "������������ ��������"
 		document.getElementById(elementId).innerHTML = result; 
}
</script>
</head>
  <body>
    <h3>
      ���������� <xsl:value-of select="MR730/DeviceType"/> ������ �� <xsl:value-of select="MR730/DeviceVersion"/></h3>

    <p></p>
    <h3>������� �������</h3>
    <h4>��������� ����</h4>
    <table border="1">
      <tr bgcolor="#ced5c1">
        <th>��������� ��� ��, �</th>
        <th>��������� ��� ����, �</th>
        <th>����. ��� ��������, I�</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="MR730/��"/>
        </td>
        <td>
          <xsl:value-of select="MR730/����"/>
        </td>
        <td>
          <xsl:value-of select="MR730/������������_���"/>
        </td>
      </tr>
    </table>
    <p></p>
    <h4>��� ��</h4>
    <table border="1">
      <tr bgcolor="#ced5c1">
        <th>����������� ��</th>
        <th>����������� ����</th>
        <th>������������� ��</th>
        <th>������������� ����</th>
        <th>��� ��</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="MR730/��"/>
        </td>
        <td>
          <xsl:value-of select="MR730/����"/>
        </td>
        <td>
          <xsl:value-of select="MR730/�������������_��"/>
        </td>
        <td>
          <xsl:value-of select="MR730/�������������_����"/>
        </td>
        <td>
          <xsl:value-of select="MR730/���_��"/>
        </td>
      </tr>
    </table>
    <p></p>
    <h4>������������ ������� ��������</h4>
    <table border="1">
      <tr bgcolor="#ced5c1">
        <th>���� ���������</th>
        <th>���� ��������</th>
        <th>������� ���������</th>
        <th>������� ��������</th>
        <th>����� ���������</th>
        <th>������� �� ���.�������</th>
      </tr>
      <tr align="center">
        <td>
          <!--<xsl:value-of select="MR730/����_���������"/>-->
        <xsl:value-of select="MR730/����_��������"/>
        </td>
        <td>
          <!--<xsl:value-of select="MR730/����_��������"/>-->
        <xsl:value-of select="MR730/����_���������"/>
        </td>
        <td>
          <!--<xsl:value-of select="MR730/�������_���������"/>-->
        <xsl:value-of select="MR730/�������_��������"/>
        </td>
        <td>
          <!--<xsl:value-of select="MR730/�������_��������"/>-->
        <xsl:value-of select="MR730/�������_���������"/>
        </td>
        <td>
          <xsl:value-of select="MR730/�����_������������"/>
        </td>
        <td>
          <xsl:value-of select="MR730/������������_�������"/>
        </td>
      </tr>
    </table>
    <p></p>
    <h4>������������ ���������� ����������</h4>
    <table border="1">
      <tr bgcolor="#ced5c1">
        <th>�� ������</th>
        <th>�� �����</th>
        <th>�������</th>
        <th>�� ����</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="MR730/������_��_������"/>
        </td>
        <td>
          <xsl:value-of select="MR730/������_��_�����"/>
        </td>
        <td>
          <xsl:value-of select="MR730/������_��_��������"/>
        </td>
        <td>
          <xsl:value-of select="MR730/������_��_����"/>
        </td>
      </tr>
    </table>
    <p></p>
    <h4>������������ ���������� �����������</h4>
    <table border="1">
      <tr bgcolor="#ced5c1">
        <th>��������</th>
        <th>�������</th>
        <th>�������������</th>
        <th>����-�� ���������</th>
        <th>����� ����, ��</th>
        <th>��� ����, I�</th>
        <th>������� ��, ��</th>
        <th>����. ���������, ��</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="MR730/�����������_���������"/>
        </td>
        <td>
          <xsl:value-of select="MR730/�����������_��������"/>
        </td>
        <td>
          <xsl:value-of select="MR730/�����������_������"/>
        </td>
        <td>
          <xsl:value-of select="MR730/�����������_����������"/>
        </td>
        <td>
          <xsl:value-of select="MR730/�����������_�����_����"/>
        </td>
        <td>
          <xsl:value-of select="MR730/�����������_���_����"/>
        </td>
        <td>
          <xsl:value-of select="MR730/�����������_�������"/>
        </td>
        <td>
          <xsl:value-of select="MR730/�����������_������������"/>
        </td>
      </tr>
    </table>
    <p></p>
    <h4>������������ ������������</h4> 
    <table border="1">    
      <tr bgcolor="#ced5c1">
         <th>���������� ������������</th>
      </tr>
      <tr align="center">
         <td>
           <xsl:for-each select ="MR730/OscilloscopeKonfCount">
                  <xsl:if test="current() ='0'">1 �������������</xsl:if>
                  <xsl:if test="current() ='1'">1 ���������������� ���.</xsl:if>
                  <xsl:if test="current() ='2'">2 ���������������� ���.</xsl:if>
                </xsl:for-each>
         </td>
      </tr>
     </table>
    <p></p>
    <h4>������������ ���� �������������</h4> 
    <table border="1">    
      <tr bgcolor="#ced5c1">
         <th>�������</th>
      </tr>
      <tr>
         <td><xsl:value-of select="MR730/�������_��"/></td>
      </tr>
     </table>
    
    <table border="1">
      <xsl:for-each select="MR730/�������_�������������">
      <tr align="center">
        <th bgcolor="#ced5c1">������������� <xsl:value-of select="position()"/></th>
        <xsl:element name="td">
          <xsl:attribute name="id">disrepair_<xsl:value-of select="position()"/></xsl:attribute>  
          <script>translateDisrepair(<xsl:value-of select="current()"/>,"disrepair_<xsl:value-of select="position()"/>");</script>
        </xsl:element>
      </tr>
      </xsl:for-each>
    </table>
    <p></p>
    
    
    
    <!--������� ���������� �������-->


    <b>������� ���������� �������</b>
    <div id="the whole thing" style="width:100%; overflow: hidden;">
      <div id="col1" style="float: left; width:25%;">
          
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������ �1 (�)</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="ced5c1">
                <th>����</th>
                <th>������������</th>
              </tr>

              <xsl:for-each select="MR730/�������_����������_�������/�1/�������">
                <tr>
                  <td>
                    <xsl:value-of  select="format-number(position(), '�#')"/>
                  </td>

                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>

          </td>
        </table>
      </div>
          
            
      <div id="col2" style="float: left; width:25%;">
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������ �2 (�)</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="ced5c1">
                <th>����</th>
                <th>������������</th>
              </tr>

              <xsl:for-each select="MR730/�������_����������_�������/�2/�������">
                <tr>
                  <td>
                    <xsl:value-of  select="format-number(position(), '�#')"/>
                  </td>

                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>

          </td>
        </table>
      </div>
      <div id="col3" style="float: left; width:25%;">
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������ �3 (�)</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="ced5c1">
                <th>����</th>
                <th>������������</th>
              </tr>

              <xsl:for-each select="MR730/�������_����������_�������/�3/�������">
                <tr>
                  <td>
                    <xsl:value-of  select="format-number(position(), '�#')"/>
                  </td>

                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>

          </td>
        </table>
      </div>
      <div id="col4" style="float: left; width:25%;">    
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������ �4 (�)</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="ced5c1">
                <th>����</th>
                <th>������������</th>
              </tr>

              <xsl:for-each select="MR730/�������_����������_�������/�4/�������">
                <tr>
                  <td>
                    <xsl:value-of  select="format-number(position(), '�#')"/>
                  </td>

                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>

          </td>
        </table></div>

    </div>
        
          
    <div id="the whole thing" style="width:100%; overflow: hidden;">
      <div id="col1" style="float: left; width:25%;">
          
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������ �5 (���)</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="ced5c1">
                <th>����</th>
                <th>������������</th>
              </tr>

              <xsl:for-each select="MR730/�������_����������_�������/���1/�������">
                <tr>
                  <td>
                    <xsl:value-of  select="format-number(position(), '�#')"/>
                  </td>

                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>

          </td>
        </table>
      </div>
          
            
      <div id="col2" style="float: left; width:25%;">
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������ �6 (���)</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="ced5c1">
                <th>����</th>
                <th>������������</th>
              </tr>

              <xsl:for-each select="MR730/�������_����������_�������/���2/�������">
                <tr>
                  <td>
                    <xsl:value-of  select="format-number(position(), '�#')"/>
                  </td>

                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>

          </td>
        </table>
      </div>
      <div id="col3" style="float: left; width:25%;">
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������ �7 (���)</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="ced5c1">
                <th>����</th>
                <th>������������</th>
              </tr>

              <xsl:for-each select="MR730/�������_����������_�������/���3/�������">
                <tr>
                  <td>
                    <xsl:value-of  select="format-number(position(), '�#')"/>
                  </td>

                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>

          </td>
        </table>
      </div>
      <div id="col4" style="float: left; width:25%;">    
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������ �8 (���)</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="ced5c1">
                <th>����</th>
                <th>������������</th>
              </tr>

              <xsl:for-each select="MR730/�������_����������_�������/���4/�������">
                <tr>
                  <td>
                    <xsl:value-of  select="format-number(position(), '�#')"/>
                  </td>

                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>

          </td>
        </table></div>

    </div>


      
        
        
        
        
    <h3>�������� �������</h3>
    <h4>������������ �������� ����</h4>
    <table border="1">
      <tr bgcolor="#c1ced5">
         <th>����� ����</th>
         <th>���</th>
         <th>������</th>
         <th>�������, ��</th>
      </tr>
      <xsl:for-each select="MR730/��������_����">
      <tr align="center">
         <td><xsl:value-of select="position()"/></td>
         <td><xsl:value-of select="@���"/></td>
         <td><xsl:value-of select="@������"/></td>
         <td><xsl:value-of select="@�������"/></td>
      </tr>
    </xsl:for-each>
    </table>
    
    
    
    
    
    
    
            
    <!--�������� ���������� �������-->


    <b>�������� ���������� �������</b>
    <div id="the whole thing" style="width:100%; overflow: hidden;">
      <div id="col1" style="float: left; width:25%;">
          
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������� ���1</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="c1ced5">
                <th>������������</th>
              </tr>

              <xsl:for-each select="MR730/��������_����������_�������/���1/�������">
                <tr>
                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>
          </td>
        </table>
      </div>
      <div id="col2" style="float: left; width:25%;">
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������� ���2</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="c1ced5">
                <th>������������</th>
              </tr>

              <xsl:for-each select="MR730/��������_����������_�������/���2/�������">
                <tr>

                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>

          </td>
        </table>
      </div>
      <div id="col3" style="float: left; width:25%;">
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������� ���3</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="c1ced5">
                <th>������������</th>
              </tr>

              <xsl:for-each select="MR730/��������_����������_�������/���3/�������">
                <tr>
                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>

          </td>
        </table>
      </div>
      <div id="col4" style="float: left; width:25%;">    
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������� ���4</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="c1ced5">
                <th>������������</th>
              </tr>

              <xsl:for-each select="MR730/��������_����������_�������/���4/�������">
                <tr>
                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>

          </td>
        </table></div>

    </div>
            
    <div id="the whole thing" style="width:100%; overflow: hidden;">
      <div id="col1" style="float: left; width:25%;">
          
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������� ���5</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="c1ced5">
                <th>������������</th>
              </tr>

              <xsl:for-each select="MR730/��������_����������_�������/���5/�������">
                <tr>
                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>
          </td>
        </table>
      </div>
      <div id="col2" style="float: left; width:25%;">
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������� ���6</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="c1ced5">
                <th>������������</th>
              </tr>

              <xsl:for-each select="MR730/��������_����������_�������/���6/�������">
                <tr>

                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>

          </td>
        </table>
      </div>
      <div id="col3" style="float: left; width:25%;">
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������� ���7</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="c1ced5">
                <th>������������</th>
              </tr>

              <xsl:for-each select="MR730/��������_����������_�������/���7/�������">
                <tr>
                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>

          </td>
        </table>
      </div>
      <div id="col4" style="float: left; width:25%;">    
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������� ���8</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="c1ced5">
                <th>������������</th>
              </tr>

              <xsl:for-each select="MR730/��������_����������_�������/���8/�������">
                <tr>
                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>

          </td>
        </table></div>

    </div>
            
            
              
                
                
                
                
        
        
    <p></p>
    <h4>������������ ������������ �����������</h4> 
    <table border="1">    
      <tr bgcolor="#c1ced5">
         <th>����� ����������</th>
         <th>���</th>
         <th>������</th>
         <th>����� �����. �� ���. ���.</th>
         <th>����� �����. �� ��</th>
         <th>����� �����. �� ��</th>
      </tr>
     <xsl:for-each select="MR730/��������_����������">
        <tr align="center">
         <td><xsl:value-of select="position()"/></td>
         <td><xsl:value-of select="@���"/></td>
         <td><xsl:value-of select="@������"/></td>
         <td>
          <xsl:for-each select="@���������">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
           <td>
          <xsl:for-each select="@������">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
                     <td>
          <xsl:for-each select="@�������">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
        </tr>
      </xsl:for-each>
     </table>
    <p></p>
    <h3>������� ������</h3>
    <table border="1">
      <tr bgcolor="#bcbcbc">
        <th>����� ��</th>
        <th>�����</th>
        <th>����-��</th>
        <th>���� ����.</th>
        <th>T����, ��</th>
        <th>�������</th>
        <th>��� �� ��������</th>
        <th>���� �����.</th>
        <th>������, ��</th>
        <th>����</th>
        <th>���</th>
        <th>���</th>
      </tr>
      <xsl:for-each select="MR730/�������_������">
        <tr align="center">
          <td>
            <xsl:value-of select="position()"/>
          </td>
          <td>
            <xsl:value-of select="@�����"/>
          </td>
          <td>
            <xsl:value-of select="@����������_�����"/>
          </td>
          <td>
            <xsl:value-of select="@������������_�����"/>
          </td>
          <td>
            <xsl:value-of select="@������������_�����"/>
          </td>
          <td>
          <xsl:for-each select="@�������">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
          <td>
          <xsl:for-each select="@�������_���">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
          <td>
            <xsl:value-of select="@�������_�����"/>
          </td>
          <td>
            <xsl:value-of select="@�������_�����"/>
          </td>
          <td>
                 <xsl:for-each select="@����">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
            <td>
                  <xsl:for-each select="@���">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
            <td>
          <xsl:for-each select="@���">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
              </td>
        </tr>
      </xsl:for-each>
    </table>

    <h3>����������</h3>
    <h4>������������ ���</h4>
    <table border="1">    
      <tr bgcolor="#bcbcbc">
         <th>������������</th>
         <th>����������</th>
         <th>����� ����������, ��</th>
         <th>����� ����������, ��</th>
         <th>����� 1 �����, ��</th>
         <th>����� 2 �����, ��</th>
         <th>����� 3 �����, ��</th>
         <th>����� 4 �����, ��</th>
         <th>������ ��� �� ��������.</th>
      </tr>
      <tr align="center">
         <td><xsl:value-of select="MR730/������������_���"/></td>
         <td><xsl:value-of select="MR730/����������_���"/></td>
         <td><xsl:value-of select="MR730/�����_����������_���"/></td>
         <td><xsl:value-of select="MR730/�����_����������_���"/></td>
         <td><xsl:value-of select="MR730/�����_1_�����_���"/></td>
         <td><xsl:value-of select="MR730/�����_2_�����_���"/></td>
         <td><xsl:value-of select="MR730/�����_3_�����_���"/></td>
         <td><xsl:value-of select="MR730/�����_4_�����_���"/></td>
         <td><xsl:value-of select="MR730/������_���_��_��������������"/></td>
      </tr>
     </table>
    <p></p>
    <h4>������������ ���</h4>
     <table border="1">    
      <tr bgcolor="#bcbcbc">
         <th>���� ��� �� ����.�������</th>
         <th>���� ��� �� ����.���. ����-�</th>
         <th>���� ��� �� ����������. ����-�</th>
         <th>���� ��� �� ������</th>
         <th>������. ������ ����-�� �� ����.���-�</th>
         <th>������ ������� ���</th>
         <th>������ ����-��</th>
         <th>������ ������ ����-��</th>
         <th>������ ������������</th>
         <th>�����, ��</th>
         <th>C����� ��������</th>
         <th>������, ��</th>
         <th>�����, ��</th>
      </tr>
      <tr align="center">
         <td id="td1"><script>translateBoolean(<xsl:value-of select="MR730/����������_�������_���"/>,"td1");</script></td>
         <td id="td2"><script>translateBoolean(<xsl:value-of select="MR730/�������_����������_�����������_���"/>,"td2");</script></td>
         <td id="td3"><script>translateBoolean(<xsl:value-of select="MR730/��������������_���"/>,"td3");</script></td>
         <td id="td4"><script>translateBoolean(<xsl:value-of select="MR730/������������_������_���"/>,"td4");</script></td>
         <td id="td5"><script>translateBoolean(<xsl:value-of select="MR730/����������_������_���_��_���������_�����������"/>,"td5");</script></td>  
         <td><xsl:value-of select="MR730/������_���"/></td>
         <td><xsl:value-of select="MR730/����������_���"/></td>
         <td><xsl:value-of select="MR730/�����_����������_���"/></td>
         <td><xsl:value-of select="MR730/������������_���"/></td>
         <td><xsl:value-of select="MR730/�����_������������_���"/></td>
         <td><xsl:value-of select="MR730/�������_���"/></td>
         <td><xsl:value-of select="MR730/�����_��������_���"/></td>
         <td><xsl:value-of select="MR730/�����_����������_���"/></td>
      </tr>
     </table>
    <p></p>
    <h4>������������ ���</h4>
     <table border="1">    
      <tr bgcolor="#bcbcbc">
         <th>������������</th>
         <th>�������, I�</th>
      </tr>
     <tr align="center">
         <td><xsl:value-of select="MR730/������������_���"/></td>
         <td><xsl:value-of select="MR730/�������_���"/></td>
      </tr>
     </table>
    
    <h3>������ ���������</h3>
    <h4>��������� ������ ���������</h4>
    <table border="1">    
      <tr bgcolor="#bcbcbc">
         <th>� �������, �</th>
         <th>� ����������, �</th>
         <th>I��, I�</th>
         <th>���� Q �����</th>
         <th>I����, I�</th>
         <th>T����, ��</th>
         <th>Q���, %</th>
         <th>���� N �����</th>
      </tr>
     <tr align="center">
         <td><xsl:value-of select="MR730/�����_�������_���������"/></td>
         <td><xsl:value-of select="MR730/�����_����������_���������"/></td>
         <td><xsl:value-of select="MR730/���_���������"/></td>
         <td><xsl:value-of select="MR730/����_Q_�����"/></td>
         <td><xsl:value-of select="MR730/�������_������������_���������"/></td>
         <td><xsl:value-of select="MR730/�����_�����_���������"/></td>
         <td><xsl:value-of select="MR730/���_���������_���������"/></td>
         <td><xsl:value-of select="MR730/����_N_����"/></td>
      </tr>
     </table>
    <h4>���������� �����</h4>
    <table border="1">
      <tr bgcolor="#bcbcbc">
        <th>���������� �� ��������� ���������</th>
        <th>���������� �� ���������� ������</th>
      </tr>
      <tr>
        <td>
          <table border="1">    
            <tr bgcolor="#adadad">
               <th>�����</th>
               <th>Q���, %</th>
               <th>����, �</th>
            </tr>
            <tr align="center">
               <td><xsl:value-of select="MR730/�����_Q"/></td>
               <td><xsl:value-of select="MR730/�������_Q"/></td>
               <td><xsl:value-of select="MR730/�����_Q"/></td>
            </tr>
          </table>
        </td>
        <td>
          <table border="1">    
            <tr bgcolor="#adadad">
               <th>N����</th>
               <th>N���</th>
               <th>T����, �</th>
               <th>����, �</th>
            </tr>
           <tr align="center">
               <td><xsl:value-of select="MR730/N_����"/></td>
               <td><xsl:value-of select="MR730/N_���"/></td>
               <td><xsl:value-of select="MR730/T_����"/></td>
               <td><xsl:value-of select="MR730/T_����"/></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <h4>������ ��������� �� ���������</h4>
    <table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������</th>
         <th>�����</th>
         <th>Q��, %</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
      </tr>
      <xsl:for-each select="MR730/������_���������">
       <tr align="center">
         <td>
           <xsl:if test="position()=1">Q><xsl:value-of select="position()"/></xsl:if>
           <xsl:if test="position()=2">Q><xsl:value-of select="position()"/></xsl:if>
         </td>
         <td><xsl:value-of select="@�����"/></td>
         <td><xsl:value-of select="@�������"/></td>
         <xsl:element name="td">
             <xsl:attribute name="id">UROV_Q_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@����"/>,"UROV_Q_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">apvQ_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"apvQ_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">avrQ_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"avrQ_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
      </tr>
      </xsl:for-each>
      </table>
    <p></p>
    <h3>������</h3>

    <h4>
      <b>���� ��. ��������</b>
    </h4>
    <table border="1">
      <tr bgcolor="d0d0d0">
        <th>I</th>
        <th>I0</th>
        <th>I2</th>
        <th>In</th>
      </tr>

      <tr>
        <td>
          <xsl:value-of select="MR730/����_����_����������������_��������/I"/>
        </td>
        <td>
          <xsl:value-of select="MR730/����_����_����������������_��������/I0"/>
        </td>
        <td>
          <xsl:value-of select="MR730/����_����_����������������_��������/I2"/>
        </td>
        <td>
          <xsl:value-of select="MR730/����_����_����������������_��������/In"/>
        </td>
      </tr>
    </table>
 
    <h4>������������ ������� �����. �������� �������</h4>
    <h4>������� ������ I>, I>></h4>
     <table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>���� �� U</th>
         <th>U��c�, �</th>
         <th>�������.</th>
         <th>��� ������. ���.�������.</th>
         <th>��������</th>
         <th>I����, I�</th>
         <th>���-�� ����.</th>
         <th>T����, ��/����.</th>
         <th>�����.</th>
         <th>T�����, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
      </tr>
      <xsl:for-each select="MR730/�������_������_��������">
        <xsl:if test="position() &lt; 3">
          <tr align="center">
           <td>I><xsl:value-of select="position()"/></td>
           <td><xsl:value-of select="@�����"/></td>
           <td><xsl:value-of select="@����������_�����"/></td>
           <xsl:element name="td">
             <xsl:attribute name="id">puskpoU_<xsl:value-of select="position()"/></xsl:attribute>     
             <script>translateBoolean(<xsl:value-of select="@����_��_U"/>,"puskpoU_<xsl:value-of select="position()"/>");</script>
           </xsl:element>
           <td><xsl:value-of select="@����_��_U_�������"/></td>
           <td><xsl:value-of select="@�����������"/></td>
           <td><xsl:value-of select="@����������_��������"/></td>
           <td><xsl:value-of select="@��������"/></td>
           <td><xsl:value-of select="@������������_�������"/></td>
           <td><xsl:value-of select="@��������������"/></td>
           <td><xsl:value-of select="@������������_�����"/></td>
           <xsl:element name="td">
               <xsl:attribute name="id">Uskorenie_<xsl:value-of select="position()"/></xsl:attribute>            
               <script>translateBoolean(<xsl:value-of select="@���������"/>,"Uskorenie_<xsl:value-of select="position()"/>");</script>
           </xsl:element>
           <td><xsl:value-of select="@���������_�����"/></td>
           <xsl:element name="td">
               <xsl:attribute name="id">UROVI_<xsl:value-of select="position()"/></xsl:attribute>            
               <script>translateBoolean(<xsl:value-of select="@����"/>,"UROVI_<xsl:value-of select="position()"/>");</script>
           </xsl:element>
           <xsl:element name="td">
               <xsl:attribute name="id">apvI_<xsl:value-of select="position()"/></xsl:attribute>            
               <script>translateBoolean(<xsl:value-of select="@���"/>,"apvI_<xsl:value-of select="position()"/>");</script>
           </xsl:element>
           <xsl:element name="td">
               <xsl:attribute name="id">avrI_<xsl:value-of select="position()"/></xsl:attribute>            
               <script>translateBoolean(<xsl:value-of select="@���"/>,"avrI_<xsl:value-of select="position()"/>");</script>
           </xsl:element>
          </tr>
        </xsl:if>
      </xsl:for-each>
      </table>
    <h4>������� ������ I>>>, I>>>></h4>
     <table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>���� �� U</th>
         <th>U��c�, �</th>
         <th>�������.</th>
         <th>��� ������. ���.�������.</th>
         <th>��������</th>
         <th>I����, I�</th>
         <th>���-�� ����.</th>
         <th>T����, ��/����.</th>
         <th>���������</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
      </tr>
      <xsl:for-each select="MR730/�������_������_��������">
        <xsl:if test="position() &lt;5 and position() &gt;2">
          <tr align="center">
             <td>I><xsl:value-of select="position()"/></td>
             <td><xsl:value-of select="@�����"/></td>
             <td><xsl:value-of select="@����������_�����"/></td>
             <xsl:element name="td">
               <xsl:attribute name="id">puskpoU_<xsl:value-of select="position()"/></xsl:attribute>     
               <script>translateBoolean(<xsl:value-of select="@����_��_U"/>,"puskpoU_<xsl:value-of select="position()"/>");</script>
             </xsl:element>
             <td><xsl:value-of select="@����_��_U_�������"/></td>
             <td><xsl:value-of select="@�����������"/></td>
             <td><xsl:value-of select="@����������_��������"/></td>
             <td><xsl:value-of select="@��������"/></td>
             <td><xsl:value-of select="@������������_�������"/></td>
             <td><xsl:value-of select="@��������������"/></td>
             <td><xsl:value-of select="@������������_�����"/></td>
             <td><xsl:value-of select="@���������"/></td>
             <xsl:element name="td">
                 <xsl:attribute name="id">UROVI_<xsl:value-of select="position()"/></xsl:attribute>            
                 <script>translateBoolean(<xsl:value-of select="@����"/>,"UROVI_<xsl:value-of select="position()"/>");</script>
             </xsl:element>
             <xsl:element name="td">
                 <xsl:attribute name="id">apvI_<xsl:value-of select="position()"/></xsl:attribute>            
                 <script>translateBoolean(<xsl:value-of select="@���"/>,"apvI_<xsl:value-of select="position()"/>");</script>
             </xsl:element>
             <xsl:element name="td">
                 <xsl:attribute name="id">avrI_<xsl:value-of select="position()"/></xsl:attribute>            
                 <script>translateBoolean(<xsl:value-of select="@���"/>,"avrI_<xsl:value-of select="position()"/>");</script>
             </xsl:element>
           </tr>
         </xsl:if>
      </xsl:for-each>
      </table>
    <h4>������� ������ I2, I0, In</h4>
     <table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>���� �� U</th>
         <th>U��c�, �</th>
         <th>�������.</th>
         <th>��� ������. ���.�������.</th>
         <th>��������</th>
         <th>I����, I�</th>
         <th>���-�� ����.</th>
         <th>T����, ��/����.</th>
         <th>�����.</th>
         <th>T�����, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
      </tr>
      <xsl:for-each select="MR730/�������_������_��������">
        <xsl:if test="position() &lt;11 and position() &gt;4">
         <tr align="center">
           <td><xsl:if test="position()=5 or position()=6">I2><xsl:value-of select="position()-4"/></xsl:if><xsl:if test="position()=7 or position()=8">I0><xsl:value-of select="position()-6"/></xsl:if><xsl:if test="position()=9 or position()=10">In><xsl:value-of select="position()-8"/></xsl:if></td>
           <td><xsl:value-of select="@�����"/></td>
           <td><xsl:value-of select="@����������_�����"/></td>
           <xsl:element name="td">
             <xsl:attribute name="id">puskpoU_<xsl:value-of select="position()"/></xsl:attribute>     
             <script>translateBoolean(<xsl:value-of select="@����_��_U"/>,"puskpoU_<xsl:value-of select="position()"/>");</script>
           </xsl:element>
           <td><xsl:value-of select="@����_��_U_�������"/></td>
           <td><xsl:value-of select="@�����������"/></td>
           <td><xsl:value-of select="@����������_��������"/></td>
           <td><xsl:value-of select="@��������"/></td>
           <td><xsl:value-of select="@������������_�������"/></td>
           <td><xsl:value-of select="@��������������"/></td>
           <td><xsl:value-of select="@������������_�����"/></td>
           <xsl:element name="td">
               <xsl:attribute name="id">Uskorenie_<xsl:value-of select="position()"/></xsl:attribute>            
               <script>translateBoolean(<xsl:value-of select="@���������"/>,"Uskorenie_<xsl:value-of select="position()"/>");</script>
           </xsl:element>
           <td><xsl:value-of select="@���������_�����"/></td>
           <xsl:element name="td">
               <xsl:attribute name="id">UROVI_<xsl:value-of select="position()"/></xsl:attribute>            
               <script>translateBoolean(<xsl:value-of select="@����"/>,"UROVI_<xsl:value-of select="position()"/>");</script>
           </xsl:element>
           <xsl:element name="td">
               <xsl:attribute name="id">apvI_<xsl:value-of select="position()"/></xsl:attribute>            
               <script>translateBoolean(<xsl:value-of select="@���"/>,"apvI_<xsl:value-of select="position()"/>");</script>
           </xsl:element>
           <xsl:element name="td">
               <xsl:attribute name="id">avrI_<xsl:value-of select="position()"/></xsl:attribute>            
               <script>translateBoolean(<xsl:value-of select="@���"/>,"avrI_<xsl:value-of select="position()"/>");</script>
           </xsl:element>
          </tr>
        </xsl:if>
      </xsl:for-each>
      </table>
    <h4>������� ������ I�, I2/I1</h4>
     <table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>���� �� U</th>
         <th>U��c�, �</th>
         <th>I����, I�</th>
         <th>T����, ��/����.</th>
         <th>�����.</th>
         <th>T�����, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
      </tr>
      <xsl:for-each select="MR730/�������_������_��������">
        <xsl:if test="position() &gt; 10">
          <tr align="center">
            <xsl:if test="position()=11">
              <td>I�</td>
            </xsl:if>
            <xsl:if test="position()=12">
              <td>I2/I1</td>
            </xsl:if>
           <td><xsl:value-of select="@�����"/></td>
           <td><xsl:value-of select="@����������_�����"/></td>
           <xsl:element name="td">
             <xsl:attribute name="id">puskpoU_<xsl:value-of select="position()"/></xsl:attribute>     
             <script>translateBoolean(<xsl:value-of select="@����_��_U"/>,"puskpoU_<xsl:value-of select="position()"/>");</script>
           </xsl:element>
           <td><xsl:value-of select="@����_��_U_�������"/></td>
           <td><xsl:value-of select="@������������_�������"/></td>
           <td><xsl:value-of select="@������������_�����"/></td>
           <xsl:element name="td">
               <xsl:attribute name="id">Uskorenie_<xsl:value-of select="position()"/></xsl:attribute>            
               <script>translateBoolean(<xsl:value-of select="@���������"/>,"Uskorenie_<xsl:value-of select="position()"/>");</script>
           </xsl:element>
           <td><xsl:value-of select="@���������_�����"/></td>
           <xsl:element name="td">
               <xsl:attribute name="id">UROVI_<xsl:value-of select="position()"/></xsl:attribute>            
               <script>translateBoolean(<xsl:value-of select="@����"/>,"UROVI_<xsl:value-of select="position()"/>");</script>
           </xsl:element>
           <xsl:element name="td">
               <xsl:attribute name="id">apvI_<xsl:value-of select="position()"/></xsl:attribute>            
               <script>translateBoolean(<xsl:value-of select="@���"/>,"apvI_<xsl:value-of select="position()"/>");</script>
           </xsl:element>
           <xsl:element name="td">
               <xsl:attribute name="id">avrI_<xsl:value-of select="position()"/></xsl:attribute>            
               <script>translateBoolean(<xsl:value-of select="@���"/>,"avrI_<xsl:value-of select="position()"/>");</script>
           </xsl:element>
          </tr>
        </xsl:if>
      </xsl:for-each>
      </table>
    <p>
      <h4>������������ ����� �� ����������. �������� �������</h4>
      <table border="1"> 
        <tr bgcolor="#d0d0d0">
         <th>C������ ���.������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>��������</th>
         <th>U����, �</th>
         <th>T����, ��</th>
         <th>�������</th>
         <th>��� ��</th>
         <th>U��, �</th>
         <th>T��, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
      </tr>
      <xsl:for-each select="MR730/������_����������_��������">
      <tr align="center">
          <td><xsl:if test="position()=1 or position()=2">
             U><xsl:value-of select="position()"/>
           </xsl:if>
           <xsl:if test="position()=3 or position()=4">
             U&#60;<xsl:value-of select="position()-2"/>
           </xsl:if>
           <xsl:if test="position()=5 or position() =6">
             U2><xsl:value-of select="position()-4"/>
           </xsl:if>
           <xsl:if test="position() =7 or position() =8">
             Un><xsl:value-of select="position()-6"/>
           </xsl:if>
         </td>
          <td><xsl:value-of select="@�����"/></td>
          <td><xsl:value-of select="@����������_�����"/></td>
          <td><xsl:value-of select="@��������"/></td>
          <td><xsl:value-of select="@������������_�������"/></td>
          <td><xsl:value-of select="@������������_�����"/></td>
 	        <xsl:element name="td">
             <xsl:attribute name="id">vozvratU_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@�������"/>,"vozvratU_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
 	       <xsl:element name="td">
             <xsl:attribute name="id">APVvozvratU_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���_�������"/>,"APVvozvratU_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <td><xsl:value-of select="@�������_�������"/></td>
         <td><xsl:value-of select="@�������_�����"/></td>

         <xsl:element name="td">
             <xsl:attribute name="id">UROVU_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@����"/>,"UROVU_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">apvU_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"apvU_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">avrU_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"avrU_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
      </tr>
      </xsl:for-each>
      </table>
    </p>
    <p>
      <h4>������������ ����� �� �������. �������� �������</h4>
      <table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>F����, ��</th>
         <th>T����, ��</th>
         <th>�������</th>
         <th>��� ��</th>
         <th>F��, �</th>
         <th>T��, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
      </tr>
      <xsl:for-each select="MR730/������_��_�������_��������">
       <tr align="center">
          <td><xsl:if test="position()=1 or position()=2">
                F><xsl:value-of select="position()"/>
              </xsl:if>
              <xsl:if test="position()=3 or position()=4">
                F&#60;<xsl:value-of select="position()-2"/>
              </xsl:if>
         </td>
          <td><xsl:value-of select="@�����"/></td>
         <td><xsl:value-of select="@����������_�����"/></td>
         <td><xsl:value-of select="@������������_�������"/></td>
         <td><xsl:value-of select="@������������_�����"/></td>
 	       <xsl:element name="td">
             <xsl:attribute name="id">vozvratF_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@�������"/>,"vozvratF_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
 	       <xsl:element name="td">
             <xsl:attribute name="id">APVvozvratF_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@�������_���"/>,"APVvozvratF_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <td><xsl:value-of select="@������������_���"/></td>
         <td><xsl:value-of select="@�������_�����"/></td>

         <xsl:element name="td">
             <xsl:attribute name="id">UROVF_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@����"/>,"UROVF_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">apvF_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"apvF_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">avrF_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"avrF_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
      </tr>
      </xsl:for-each>
      </table>
    </p>
    <h4>
      <b>���� ��. ���������</b>
    </h4>
    <table border="1">
      <tr bgcolor="d0d0d0">
        <th>I</th>
        <th>I0</th>
        <th>I2</th>
        <th>In</th>
      </tr>

      <tr>
        <td>
          <xsl:value-of select="MR730/����_����_����������������_���������/I"/>
        </td>
        <td>
          <xsl:value-of select="MR730/����_����_����������������_���������/I0"/>
        </td>
        <td>
          <xsl:value-of select="MR730/����_����_����������������_���������/I2"/>
        </td>
        <td>
          <xsl:value-of select="MR730/����_����_����������������_���������/In"/>
        </td>
      </tr>
    </table>

    <p>
      <h4>������������ ������� �����. ��������� �������</h4>
      <h4>������� ������ I>, I>></h4>
     <table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>���� �� U</th>
         <th>U��c�, �</th>
         <th>�������.</th>
         <th>��� ������. ���.�������.</th>
         <th>��������</th>
         <th>I����, I�</th>
         <th>���-�� ����.</th>
         <th>T����, ��/����.</th>
         <th>�����.</th>
         <th>T�����, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
      </tr>
      <xsl:for-each select="MR730/�������_������_���������">
        <xsl:if test="position() &lt;3">
          <tr align="center">
           <td>I><xsl:value-of select="position()"/></td>
           <td><xsl:value-of select="@�����"/></td>
           <td><xsl:value-of select="@����������_�����"/></td>
           <td>
           <xsl:for-each select="@����_��_U">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
           <td><xsl:value-of select="@����_��_U_�������"/></td>
           <td><xsl:value-of select="@�����������"/></td>
           <td><xsl:value-of select="@����������_��������"/></td>
           <td><xsl:value-of select="@��������"/></td>
           <td><xsl:value-of select="@������������_�������"/></td>
           <td><xsl:value-of select="@��������������"/></td>
           <td><xsl:value-of select="@������������_�����"/></td>
            <td>
           <xsl:for-each select="@���������">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
           <td><xsl:value-of select="@���������_�����"/></td>
            <td>
           <xsl:for-each select="@����">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
                    <td>
           <xsl:for-each select="@���">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
          <td>
           <xsl:for-each select="@���">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
          </tr>
        </xsl:if>
      </xsl:for-each>
      </table>
    <h4>������� ������ I>>>, I>>>></h4>
     <table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>���� �� U</th>
         <th>U��c�, �</th>
         <th>�������.</th>
         <th>��� ������. ���.�������.</th>
         <th>��������</th>
         <th>I����, I�</th>
         <th>���-�� ����.</th>
         <th>T����, ��/����.</th>
         <th>���������</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
      </tr>
      <xsl:for-each select="MR730/�������_������_���������">
        <xsl:if test="position() &gt;2 and position() &lt;5">
          <tr align="center">
             <td>I><xsl:value-of select="position()"/></td>
             <td><xsl:value-of select="@�����"/></td>
             <td><xsl:value-of select="@����������_�����"/></td>
             <td>
           <xsl:for-each select="@����_��_U">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
             <td><xsl:value-of select="@����_��_U_�������"/></td>
             <td><xsl:value-of select="@�����������"/></td>
             <td><xsl:value-of select="@����������_��������"/></td>
             <td><xsl:value-of select="@��������"/></td>
             <td><xsl:value-of select="@������������_�������"/></td>
             <td><xsl:value-of select="@��������������"/></td>
             <td><xsl:value-of select="@������������_�����"/></td>
             <td><xsl:value-of select="@���������"/></td>
            <td>
           <xsl:for-each select="@����">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
                    <td>
           <xsl:for-each select="@���">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
          <td>
           <xsl:for-each select="@���">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
           </tr>
         </xsl:if>
      </xsl:for-each>
      </table>
    <h4>������� ������ I2, I0, In</h4>
     <table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>���� �� U</th>
         <th>U��c�, �</th>
         <th>�������.</th>
         <th>��� ������. ���.�������.</th>
         <th>��������</th>
         <th>I����, I�</th>
         <th>���-�� ����.</th>
         <th>T����, ��/����.</th>
         <th>�����.</th>
         <th>T�����, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
      </tr>
      <xsl:for-each select="MR730/�������_������_���������">
        <xsl:if test="position() &gt;4 and position() &lt;11">
          <tr align="center">
           <td><xsl:if test="position()=5 or position()=6">I2><xsl:value-of select="position()-4"/></xsl:if><xsl:if test="position()=7 or position()=8">I0><xsl:value-of select="position()-6"/></xsl:if><xsl:if test="position()=9 or position()=10">In><xsl:value-of select="position()-8"/></xsl:if></td>
           <td><xsl:value-of select="@�����"/></td>
           <td><xsl:value-of select="@����������_�����"/></td>
           <td>
           <xsl:for-each select="@����_��_U">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
           <td><xsl:value-of select="@����_��_U_�������"/></td>
           <td><xsl:value-of select="@�����������"/></td>
           <td><xsl:value-of select="@����������_��������"/></td>
           <td><xsl:value-of select="@��������"/></td>
           <td><xsl:value-of select="@������������_�������"/></td>
           <td><xsl:value-of select="@��������������"/></td>
           <td><xsl:value-of select="@������������_�����"/></td>
           <td>
           <xsl:for-each select="@���������">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
           <td><xsl:value-of select="@���������_�����"/></td>
           <td>
           <xsl:for-each select="@����">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
                    <td>
           <xsl:for-each select="@���">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
          <td>
           <xsl:for-each select="@���">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
          </tr>
        </xsl:if>
      </xsl:for-each>
      </table>
    <h4>������� ������ I�, I2/I1</h4>
     <table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>���� �� U</th>
         <th>U��c�, �</th>
         <th>I����, I�</th>
         <th>T����, ��/����.</th>
         <th>�����.</th>
         <th>T�����, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
      </tr>
      <xsl:for-each select="MR730/�������_������_���������">
        <xsl:if test="position() &gt;10">
          <tr align="center">
            <xsl:if test="position()=11">
              <td>I�</td>
            </xsl:if>
            <xsl:if test="position()=12">
              <td>I2/I1</td>
            </xsl:if>
           <td><xsl:value-of select="@�����"/></td>
           <td><xsl:value-of select="@����������_�����"/></td>
           <td>
           <xsl:for-each select="@����_��_U">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
           <td><xsl:value-of select="@����_��_U_�������"/></td>
           <td><xsl:value-of select="@������������_�������"/></td>
           <td><xsl:value-of select="@������������_�����"/></td>
          <td>
           <xsl:for-each select="@���������">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
           <td><xsl:value-of select="@���������_�����"/></td>
           <td>
           <xsl:for-each select="@����">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
                    <td>
           <xsl:for-each select="@���">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
          <td>
           <xsl:for-each select="@���">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
          </tr>
        </xsl:if>
      </xsl:for-each>
      </table>
    </p>
    <h4>������������ ����� �� ����������. ��������� �������</h4>
    <table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>��������</th>
         <th>U����, �</th>
         <th>T����, ��</th>
         <th>�������</th>
         <th>��� ��</th>
         <th>U��, �</th>
         <th>T��, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
      </tr>
      <xsl:for-each select="MR730/������_����������_���������">
       <tr align="center">
          <td><xsl:if test="position()=1 or position()=2">
             U><xsl:value-of select="position()"/>
           </xsl:if>
           <xsl:if test="position()=3 or position()=4">
             U&#60;<xsl:value-of select="position()-2"/>
           </xsl:if>
           <xsl:if test="position()=5 or position() =6">
             U2><xsl:value-of select="position()-4"/>
           </xsl:if>
           <xsl:if test="position() =7 or position() =8">
             Un><xsl:value-of select="position()-6"/>
           </xsl:if>
         </td>
          <td><xsl:value-of select="@�����"/></td>
          <td><xsl:value-of select="@����������_�����"/></td>
          <td><xsl:value-of select="@��������"/></td>
         <td><xsl:value-of select="@������������_�������"/></td>
         <td><xsl:value-of select="@������������_�����"/></td>
         <td>
           <xsl:for-each select="@�������">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
                    <td>
           <xsl:for-each select="@���_�������">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
         <td><xsl:value-of select="@�������_�������"/></td>
         <td><xsl:value-of select="@�������_�����"/></td>
         <td>
           <xsl:for-each select="@����">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
                    <td>
           <xsl:for-each select="@���">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
          <td>
           <xsl:for-each select="@���">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
      </tr>
      </xsl:for-each>
      </table>
    <h4>������������ ����� �� �������. ��������� �������</h4>
    <table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>F����, ��</th>
         <th>T����, ��</th>
         <th>�������</th>
         <th>��� ��</th>
         <th>F��, �</th>
         <th>T��, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
      </tr>
      <xsl:for-each select="MR730/������_��_�������_���������">
       <tr>
         <td>
           <xsl:if test="position()=1 or position()=2">
             F><xsl:value-of select="position()"/>
           </xsl:if>
           <xsl:if test="position()=3 or position()=4">
             F&#60;<xsl:value-of select="position()-2"/>
           </xsl:if>
         </td>
         <td><xsl:value-of select="@�����"/></td>
         <td><xsl:value-of select="@����������_�����"/></td>
         <td><xsl:value-of select="@������������_�������"/></td>
         <td><xsl:value-of select="@������������_�����"/></td>
 	       <td>
           <xsl:for-each select="@�������">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
         <td>
           <xsl:for-each select="@�������_���">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
         <td><xsl:value-of select="@������������_���"/></td>
         <td><xsl:value-of select="@�������_�����"/></td>
<td>
           <xsl:for-each select="@����">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
                    <td>
           <xsl:for-each select="@���">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
          <td>
           <xsl:for-each select="@���">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
           </td>
      </tr>
      </xsl:for-each>
      </table>
   </body>
  </html>
</xsl:template>
</xsl:stylesheet>
