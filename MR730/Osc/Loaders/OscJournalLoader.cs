﻿using System;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MR730.Osc.Structures;

namespace BEMN.MR730.Osc.Loaders
{
    /// <summary>
    /// Загружает журнал осцилограммы(СТАРЫЙ)
    /// </summary>
    public class OldOscJournalLoader
    {
        #region [Private fields]
        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<OscJournalStruct> _oscJournal;
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _refreshOscJournal;
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        private readonly List<OscJournalStruct> _oscRecords;
        /// <summary>
        /// Текущий номер записи журнала осциллографа
        /// </summary>
        private int _recordNumber;

        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Успешно прочитана одна запись журнала осциллографа
        /// </summary>
        public event Action ReadRecordOk;
        /// <summary>
        /// Успешно прочитан весь журнал
        /// </summary>
        public event Action AllReadOk;
        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action ReadJournalFail;
        private int _oscCounts;
        #endregion [Events]


        #region [Ctor's]
        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        public OldOscJournalLoader(Device device)
        {
            this._oscRecords = new List<OscJournalStruct>();

            //Сброс журнала на нулевую запись
            this._refreshOscJournal = new MemoryEntity<OneWordStruct>("Установка номера осцилограммы", device, 0x3F02);
            this._refreshOscJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(StartReadOscJournal);
            this._refreshOscJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(FailReadOscJournal);

            //Записи журнала
            this._oscJournal = new MemoryEntity<OscJournalStruct>("Чтение записи журнала осцилограммы", device, 0x3C00);
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(ReadRecord);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(FailReadOscJournal);


        }
        #endregion [Ctor's]


        #region [Properties]
        /// <summary>
        /// Номер текущей записи журнала осциллографа
        /// </summary>
        public int RecordNumber
        {
            get { return _recordNumber; }
        }
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<OscJournalStruct> OscRecords
        {
            get { return _oscRecords; }
        }

        //public bool OscJournalLoaded { get; private set; }
        #endregion [Properties]


        #region [Private MemoryEntity Events Handlers]
        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            if (this.ReadJournalFail != null)
                this.ReadJournalFail.Invoke();
        }


        private void StartReadOscJournal()
        {
            this._oscJournal.LoadStruct();
        }

        public OscJournalStruct RecordStruct
        {
            get { return this._oscJournal.Value; }
        }
        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            OscJournalStruct.RecordIndex = this.RecordNumber;

            this.OscRecords.Add(this._oscJournal.Value.Clone<OscJournalStruct>());
            this._recordNumber = this.RecordNumber + 1;
            if (this.ReadRecordOk != null)
            {
                this.ReadRecordOk.Invoke();
            }
            if (this._recordNumber == this._oscCounts && this.AllReadOk != null)
            {
                this.AllReadOk.Invoke();
                return;
            }
            this.WriteRecordNumber();
        }

        #endregion [Private MemoryEntity Events Handlers]


        #region [Public members]
        /// <summary>
        /// Запуск чтения журнала осциллографа
        /// </summary>
        public void StartReadJournal(int oscCounts)
        {
            this._oscCounts = oscCounts;
            this._recordNumber = 0;
            this.WriteRecordNumber();
        }

        private void WriteRecordNumber()
        {
            this._refreshOscJournal.Value.Word =  (ushort)(this._recordNumber + 1); //new OneWordStruct { Word = (ushort)((ushort)this._recordNumber + 1) };
            this._refreshOscJournal.SaveStruct6();
        }

        #endregion [Public members]

        internal void Reset()
        {
            this._oscRecords.Clear();
        }
    }
}
