﻿using System;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MBServer;
using BEMN.MR730.Osc.Structures;

namespace BEMN.MR730.Osc.Loaders
{
    /// <summary>
    /// Класс загрузки страниц осциллограммы
    /// </summary>
    public class OldOscPageLoader
    {
        #region [Private fields]
        /// <summary>
        /// Страница осциллограммы
        /// </summary>
        private MemoryEntity<ArrayStruct> _oscArray;
        /// <summary>
        /// Структура записи страницы
        /// </summary>
        private MemoryEntity<OneWordStruct> _page;
        /// <summary>
        /// Запись о текущей осцилограмме
        /// </summary>
        private OscJournalStruct _journalStruct;


        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Прочитана один слот
        /// </summary>
        public event Action SlotReadSuccessful;
        /// <summary>
        /// Осцилограмма загружена успешно
        /// </summary>
        public event Action OscReadSuccessful;
        /// <summary>
        /// Осцилограмма не загружена
        /// </summary>
        public event Action OscReadFail;

        private Device _device;
        private int _oscCount;
        #endregion [Events]


        #region [Ctor's]
        public OldOscPageLoader(Device device)
        {
            this._device = device;
            this._page = new MemoryEntity<OneWordStruct>("Установка номера осцилограммы", device, 0x3F02);
            this._page.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this.LoadOscArray);
            this._page.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this.OnRaiseOscReadFail);
        }
        #endregion [Ctor's]


        #region [Public members]

        public int SlotCount
        {
            get { return this._oscArray.Slots.Count; }
        }

        /// <summary>
        /// Запускает чтение указанной осцилограммы
        /// </summary>
        /// <param name="journalStruct">Запись журнала о осцилограмме</param>
        /// <param name="oscCount"></param>
        /// <param name="oscPage"></param>
        public void StartRead(OscJournalStruct journalStruct, int oscCount, int oscPage)
        {
            this._oscCount = oscCount;
            this._journalStruct = journalStruct;
            ArrayStruct.FullSize = journalStruct.Len;
            this._oscArray = new MemoryEntity<ArrayStruct>("Отсчёты", this._device, 0x4000);
            this._oscArray.AllReadOk += HandlerHelper.CreateReadArrayHandler(() =>
            {
                this._oscArray.RemoveStructQueries();
                this.OscReadComplite();
            });
            this._oscArray.AllReadFail += o =>
            {
                this._oscArray.RemoveStructQueries();
                this.OnRaiseOscReadFail();
            };
            this._oscArray.ReadOk += OnRaiseSlotReadSuccessful;

            this._page.Value = new OneWordStruct((ushort)(oscPage + 1));
            this._page.SaveStruct6();
        }

        private void LoadOscArray()
        {
            this._oscArray.LoadStruct();
        }

        #endregion [Public members]


        #region [Event Raise Members]
        private void OnRaiseSlotReadSuccessful(object sender)
        {
            if (this.SlotReadSuccessful != null)
            {
                IAsyncResult result = this.SlotReadSuccessful.BeginInvoke(null, this);
                result.AsyncWaitHandle.WaitOne();
                this.SlotReadSuccessful.EndInvoke(result);
                result.AsyncWaitHandle.Close();
            }
        }
        /// <summary>
        /// Вызывает событие "Осцилограмма загружена успешно"
        /// </summary>
        private void OnRaiseOscReadSuccessful()
        {
            if (this.OscReadSuccessful != null)
            {
                IAsyncResult result = this.OscReadSuccessful.BeginInvoke(null, this);
                result.AsyncWaitHandle.WaitOne();
                this.OscReadSuccessful.EndInvoke(result);
                result.AsyncWaitHandle.Close();
            }
        }

        /// <summary>
        /// Вызывает событие "Осцилограмма не загружена"
        /// </summary>
        private void OnRaiseOscReadFail()
        {
            if (this.OscReadFail != null)
            {
                IAsyncResult result = this.OscReadFail.BeginInvoke(null, this);
                result.AsyncWaitHandle.WaitOne();
                this.OscReadFail.EndInvoke(result);
                result.AsyncWaitHandle.Close();
                //this.OscReadFail.Invoke();
            }
        }

        #endregion [Event Raise Members]


        #region [Help members]



        /// <summary>
        /// Осцилограмма прочитана
        /// </summary>
        private void OscReadComplite()
        {
            int p = this._journalStruct.Len * 2;//р=р´×2 байт
            int a = this._journalStruct.After * 2;     //а - начало аварии (смещение в байтах) = а´×2
            int k = this._journalStruct.AfterFault + this._oscCount; //где  k = k´ + 1 (для одной осциллограммы и для одной перезаписываемой осциллограммы);
                                                                     //k = k´ + 2 (для двух перезаписываемых осциллограмм).     
            int b = a + (k * 18 + p % 18);    //b - начало осциллограммы = а+(k×18+p%18),

            if (b >= p)//При этом: если b>=p, то b=b-p.
            {
                b -= p;
            }
            //Результирующий массив
            byte[] readMassiv = Common.TOBYTES(this._oscArray.Value.Data, false);
            byte[] invertedMass = new byte[p];

            Array.ConstrainedCopy(readMassiv, b, invertedMass, 0, p - b);
            Array.ConstrainedCopy(readMassiv, 0, invertedMass, p - b, b);
            this.ResultArray = Common.TOWORDS(invertedMass, false);

            this.OnRaiseOscReadSuccessful();
        }

        #endregion [Help members]


        #region [Properties]

        /// <summary>
        /// Готовый массив осц
        /// </summary>
        public ushort[] ResultArray { get; private set; }
        #endregion [Properties]
    }
}
