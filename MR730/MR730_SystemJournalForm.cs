using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Framework.Properties;
using BEMN.Interfaces;

namespace BEMN.MR730
{
    public partial class SystemJournalForm : Form , IFormView
    {
        private MR730 _device;

        public SystemJournalForm()
        {
            this.InitializeComponent();
        }

        public SystemJournalForm(MR730 device)
        {
            this.InitializeComponent();
            this._device = device;

            this._device.SystemJournalRecordLoadOk += new IndexHandler(this._device_SystemJournalRecordLoadOk);
            this._device.SystemJournalLoadOk += new Handler(this._device_SystemJournalLoadOk);
        }

        void _device_SystemJournalLoadOk(object sender)
        {
            try
            {
                Invoke(new Action(this.OnSysJournalLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_SystemJournalRecordLoadOk(object sender, int index)
        {
            try
            {
                Invoke(new OnDeviceIndexEventHandler(this.OnSysJournalIndexLoadOk), new object[] { index });
            }
            catch (InvalidOperationException)
            {}
        }

        private void OnSysJournalLoadOk()
        {
            this._readSysJournalBut.Enabled = true;
        }

        private void OnSysJournalIndexLoadOk(int i)
        {            
            this._journalProgress.Increment(1);
            this._journalCntLabel.Text = (i + 1).ToString();
            this._sysJournalGrid.Rows.Add(new object[] { i + 1, this._device.SystemJournal[i].time, this._device.SystemJournal[i].msg });
        }
        
        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR730); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(SystemJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Resources.js;
            }
        }

        public string NodeName
        {
            get { return "������ �������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _readSystemJournalBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._readSysJournalBut.Enabled = false;
            this._device.SystemJournal = new MR730.CSystemJournal();
            this._device.RemoveSystemJournal();
            this._journalProgress.Value = 0;
            this._journalProgress.Maximum = MR730.SYSTEMJOURNAL_RECORD_CNT;
            this._sysJournalGrid.Rows.Clear();
            this._device.LoadSystemJournal();
        }
        
        private void _serializeSysJournalBut_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable("��73X_������_�������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");
            for (int i = 0; i < this._sysJournalGrid.Rows.Count; i++)
            {
                table.Rows.Add(new object[]{this._sysJournalGrid["_indexCol", i].Value,
                                            this._sysJournalGrid["_timeCol", i].Value,
                                            this._sysJournalGrid["_msgCol", i].Value});
            }

            if (DialogResult.OK == this._saveSysJournalDlg.ShowDialog())
            {
                table.WriteXml(this._saveSysJournalDlg.FileName);
            }
        }

        private void _deserializeSysJournalBut_Click(object sender, EventArgs e)
        {            
            DataTable table = new DataTable("��73X_������_�������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");

            if (DialogResult.OK == this._openSysJounralDlg.ShowDialog())
            {
                this._sysJournalGrid.Rows.Clear();
                table.ReadXml(this._openSysJounralDlg.FileName);
            }

            for (int i = 0; i < table.Rows.Count; i++)
            {
                this._sysJournalGrid.Rows.Add(new object[]{table.Rows[i].ItemArray[0],
                                                      table.Rows[i].ItemArray[1],
                                                      table.Rows[i].ItemArray[2]});
            }
        }

        private void SystemJournalForm_Activated(object sender, EventArgs e)
        {
            this._device.SuspendSystemJournal(false);
        }

        private void SystemJournalForm_Deactivate(object sender, EventArgs e)
        {
            this._device.SuspendSystemJournal(true);
        }

        private void SystemJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.RemoveSystemJournal();
        }
    }
}