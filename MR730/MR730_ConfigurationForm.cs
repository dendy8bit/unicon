using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.MR730
{
    public partial class ConfigurationForm : Form, IFormView
    {
        private bool mess = false;
        private MR730 _device;
        private bool _validatingOk = true;
        private bool _connectingErrors = false;

        public ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public ConfigurationForm(MR730 device)
        {
            this._device = device;
            this.InitializeComponent();
            this._groupSelector = new RadioButtonSelector(this._mainGroupRadioButton, this._reserveGroupRadioButton, this._groupChangeButton);
            this._groupSelector.NeedCopyGroup += this._groupSelector_NeedCopyGroup;
            this._device.InputSignalsLoadOK += HandlerHelper.CreateHandler(this, this.ReadInputSignals);
            this._device.InputSignalsLoadFail += HandlerHelper.CreateHandler(this, this.OnLoadFail);
            this._device.KonfCountLoadOK += HandlerHelper.CreateHandler(this, this.ReadKonfCount);
            this._device.KonfCountLoadFail += HandlerHelper.CreateHandler(this, this.OnLoadFail);
            this._device.OutputSignalsLoadOK += HandlerHelper.CreateHandler(this, this.ReadOutputSignals);
            this._device.OutputSignalsLoadFail += HandlerHelper.CreateHandler(this, this.OnLoadFail);
            this._device.ExternalDefensesLoadOK += HandlerHelper.CreateHandler(this, this.ReadExternalDefenses);
            this._device.ExternalDefensesLoadFail += HandlerHelper.CreateHandler(this, this.OnLoadFail);
            this._device.AutomaticsPageLoadOK += new Handler(this._device_AutomaticsPageLoadOK);
            this._device.AutomaticsPageLoadFail += new Handler(this._device_AutomaticsPageLoadFail);
            //this._device.AutomaticsPageLoadOK += HandlerHelper.CreateHandler(this, this.OnAutomaticsPageLoadOk);
            //this._device.AutomaticsPageLoadFail += HandlerHelper.CreateHandler(this, this.OnLoadFail);
            this._device.TokDefensesLoadOK += HandlerHelper.CreateHandler(this, this.OnTokDefenseLoadOk);
            this._device.TokDefensesLoadFail += HandlerHelper.CreateHandler(this, this.OnLoadFail);
            this._device.VoltageDefensesLoadOk += HandlerHelper.CreateHandler(this, this.OnVoltageDefensesLoadOk);
            this._device.VoltageDefensesLoadFail += HandlerHelper.CreateHandler(this, this.OnLoadFail);
            this._device.EngineDefensesLoadOk += HandlerHelper.CreateHandler(this, this.OnEngineDefensesLoadOk);
            this._device.EngineDefensesLoadFail += HandlerHelper.CreateHandler(this, this.OnLoadFail);
            this._device.FrequenceDefensesLoadOk += HandlerHelper.CreateHandler(this, this.FrequenceDefensesLoadOk);
            this._device.FrequenceDefensesLoadFail += HandlerHelper.CreateHandler(this, this.FrequenceDefensesLoadFail);

            this.SubscriptOnSaveHandlers();

            this._oscKonfComb.SelectedIndex = 0;
            this.PrepareInputSignals();
            this.PrepareOutputSignals();
            this.PrepareExternalDefenses();
            this.PrepareAutomaticsSignals();
            this.PrepareTokDefenses();
            this.PrepareVoltageDefenses();
            this.PrepareEngineDefenses();
            this.PrepareFrequenceDefenses();
        }

        //���� 0,��������� ������� � ���������� � ���������
        void _groupSelector_NeedCopyGroup()
        {
            if (this._groupSelector.SelectedGroup == 0)
            {
                this.WriteTokDefenses(this._device.TokDefensesReserve);
                this.WriteVoltageDefenses(this._device.VoltageDefensesReserve);
                this.WriteFrequenceDefenses(this._device.FrequenceDefensesReserve);
            }
            else
            {
                this.WriteTokDefenses(this._device.TokDefensesMain);
                this.WriteVoltageDefenses(this._device.VoltageDefensesMain);
                this.WriteFrequenceDefenses(this._device.FrequenceDefensesMain);
            }
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR730); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(ConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Resources.config.ToBitmap();
            }
        }

        public string NodeName
        {
            get { return "������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            this._exchangeProgressBar.Value = 0;
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                try
                {
                    this._device.Deserialize(this._openConfigurationDlg.FileName);
                }
                catch (System.IO.FileLoadException exc)
                {
                    this._processLabel.Text = "���� " + System.IO.Path.GetFileName(exc.FileName) + " �� �������� ������ ������� ��730 ��� ���������";
                    return;
                }


                if (this._mainGroupRadioButton.Checked)
                {
                    this.ShowTokDefenses(this._device.TokDefensesMain);
                    this.ShowVoltageDefenses(this._device.VoltageDefensesMain);
                    this.ShowFrequenceDefenses(this._device.FrequenceDefensesMain);
                }
                else
                {
                    this.ShowTokDefenses(this._device.TokDefensesReserve);
                    this.ShowVoltageDefenses(this._device.VoltageDefensesReserve);
                    this.ShowFrequenceDefenses(this._device.FrequenceDefensesReserve);
                }
                this.ReadInputSignals();
                this.ReadExternalDefenses();
                this.ReadOutputSignals();
                this.ShowEngineDefenses();
                this.ReadAutomaticsPage();
                this.ReadKonfCount();

                this._exchangeProgressBar.Value = 0;
                this._processLabel.Text = "���� " + this._openConfigurationDlg.FileName + " ��������";
            } 
        }
        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveinFile();
        }

        private void SaveinFile()
        {
            this.ValidateAll();
            if (this._validatingOk)
            {
                this._saveConfigurationDlg.FileName = string.Format("��730_�������_������ {0:F1}.bin", this._device.DeviceVersion);
                if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
                {
                    this._exchangeProgressBar.Value = 0;
                    this._device.Serialize(this._saveConfigurationDlg.FileName);
                    this._processLabel.Text = "���� " + this._openConfigurationDlg.FileName + " ��������";
                }
            } 
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtmlFile();
        }

        private void SaveToHtmlFile()
        {
            this.ValidateAll();
            if (this._validatingOk)
            {
                this._processLabel.Text = HtmlExport.Export(Resources.MR730Main, Resources.MR730Res, this._device, "��730", this._device.DeviceVersion);
            }
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;

            this._connectingErrors = false;
            this._exchangeProgressBar.Value = 0;
            this._validatingOk = true;

            this.ValidateAll();
            if (this._validatingOk)
            {
                if (DialogResult.Yes == MessageBox.Show("�������� ������������ ��730 �" + this._device.DeviceNumber + " ?", "��������", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                {
                    this._processLabel.Text = "���� ������";
                    this._device.SaveOutputSignals();
                    this._device.SaveInputSignals();
                    this._device.SaveExternalDefenses();
                    this._device.SaveTokDefenses();
                    this._device.SaveVoltageDefenses();
                    this._device.SaveEngineDefenses();
                    this._device.SaveAutomaticsPage();
                    this._device.SaveFrequenceDefenses();
                    this._device.ConfirmConstraint();
                }
            }  
        }

        private void ValidateAll()
        {
            this._validatingOk = true;
            this.ValidateInputSignals();
            this.ValidateTokDefenses();
            this.ValidateEngineDefenses();
            this.ValidateAutomatics();
            if (this._validatingOk)
            {
                this._validatingOk = this._validatingOk && this.WriteOutputSignals() 
                    && this.WriteExternalDefenses() 
                    && this.WriteInputSignals() 
                    && this.WriteAutomaticsPage() 
                    && this.WriteEngineDefenses();
                this._validatingOk &= this._mainGroupRadioButton.Checked 
                    ? this.WriteTokDefenses(this._device.TokDefensesMain) 
                    : this.WriteTokDefenses(this._device.TokDefensesReserve);
                this._validatingOk &= this._mainGroupRadioButton.Checked 
                    ? this.WriteVoltageDefenses(this._device.VoltageDefensesMain) 
                    : this.WriteVoltageDefenses(this._device.VoltageDefensesReserve);
                this._validatingOk &= this._mainGroupRadioButton.Checked 
                    ? this.WriteFrequenceDefenses(this._device.FrequenceDefensesMain) 
                    : this.WriteFrequenceDefenses(this._device.FrequenceDefensesReserve);
            }
        }
        
        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void OnSaveFail()
        {
            if (!this._connectingErrors)
            {
                MessageBox.Show("���������� �������� ������������. ��������� �����", "������-��730. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this._connectingErrors = true;
        }

        private void OnLoadFail()
        {
            if (!this._connectingErrors)
            {
                MessageBox.Show("���������� ��������� ������������. ��������� �����", "������-��730. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this._connectingErrors = true;
        }

        private void OnSaveOk()
        {
            this._exchangeProgressBar.PerformStep();
        }

        void OnLoadComplete()
        {
            if (this._connectingErrors)
            {
                this._processLabel.Text = "������ ��������� ���������";                
            }
            else
            {
                this._processLabel.Text = "������ ������� ���������";    
            }
        }

        void OnSaveComplete()
        {            
            if (this._connectingErrors)
            {
                this._processLabel.Text = "������ ��������� ���������";
            }
            else
            {
                this._exchangeProgressBar.PerformStep();
                this._processLabel.Text = "������ ������� ���������";
            }
        }

        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            if(Device.AutoloadConfig)
                this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;

            this._exchangeProgressBar.Value = 0;
            this._processLabel.Text = "���� ������...";
            this._connectingErrors = false;
            this._device.LoadInputSignals();
            this._device.LoadOutputSignals();
            this._device.LoadExternalDefenses();
            this._device.LoadAutomaticsPage();
            this._device.LoadTokDefenses();
            this._device.LoadVoltageDefenses();
            this._device.LoadEngineDefenses();
            this._device.LoadFrequenceDefenses();
        }

        private void SubscriptOnSaveHandlers()
        {
            this._device.OutputSignalsSaveOK += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.OutputSignalsSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.InputSignalsSaveOK += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.InputSignalsSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };

            this._device.KonfCountSaveOK += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.KonfCountSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.ExternalDefensesSaveOK += delegate(object o)
           {
               try
               {
                   Invoke(new OnDeviceEventHandler(this.OnSaveOk));
               }
               catch (InvalidOperationException)
               { }
           };
            this._device.ExternalDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.AutomaticsPageSaveOK += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.AutomaticsPageSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.TokDefensesSaveOK += delegate(object o)
           {
               try
               {
                   Invoke(new OnDeviceEventHandler(this.OnSaveOk));
               }
               catch (InvalidOperationException)
               { }
           };
            this._device.TokDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.VoltageDefensesSaveOk += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.VoltageDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.EngineDefensesSaveOk += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.EngineDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.FrequenceDefensesSaveOk += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveComplete));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.FrequenceDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveComplete));
                }
                catch (InvalidOperationException)
                { }
            };
        }

        void TypeValidation(object sender, TypeValidationEventArgs e)
        {
            if (sender is MaskedTextBox)
            {
                MaskedTextBox box = sender as MaskedTextBox;

                if (!e.IsValidInput)
                {
                    this.ShowToolTip(box);

                }
                else
                {
                    if (box.ValidatingType == typeof(ulong) && ulong.Parse(box.Text) > ulong.Parse(box.Tag.ToString()))
                    {
                        this.ShowToolTip(box);
                    }
                    if (box.ValidatingType == typeof(double) && (double.Parse(box.Text) > double.Parse(box.Tag.ToString())) || (double.Parse(box.Text) < 0))
                    {
                        this.ShowToolTip(box);
                    }
                }

            }
        }
        
        
        private void ShowToolTip(MaskedTextBox box)
        {
            if (this._validatingOk)
            {
                if (box.Parent.Parent is TabPage)
                {
                    this._tabControl.SelectedTab = box.Parent.Parent as TabPage;
                }                
                this._toolTip.Show("������� ����� � ��������� [0-" + box.Tag + "]", box, 3000);
                box.Focus();
                box.SelectAll();
                this._validatingOk = false;
            }
        }

        /// <summary>
        /// ����� ���������� ��������� �� ����� ��� �������� ������������� ��������,
        /// ������ �������� ������� (TabPage) ���������� TabControl, ��� ������� ������.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="cell"></param>
        /// <param name="page"></param>
        private void ShowToolTip(string msg, DataGridViewCell cell, TabPage page)
        {
            cell.OwningColumn.DataGridView.CurrentCell.Selected = false;
            DataGridViewCell currentCell;
            Rectangle cellDisplayRect;
            if (this._validatingOk)
            {
                if (page.Parent.Parent.Parent.Text == "������")
                {
                    var tabPage = page.Parent.Parent.Parent;
                    var tabControl = page.Parent;
                    this._tabControl.SelectedTab = (TabPage)tabPage;
                    TabControl temp = tabControl as TabControl;
                    temp.SelectedTab = page;
                }
                if (page.Text == "������� ������" || page.Text == "������ ���������")
                {
                    this._tabControl.SelectedTab = page;
                }
                if (page.Text == "�������� �������")
                {
                    this._tabControl.SelectedTab = _outputSignalsPage;
                }

                //cell.Selected = true;
                currentCell = cell.OwningColumn.DataGridView.CurrentCell;
                cellDisplayRect = cell.OwningColumn.DataGridView.GetCellDisplayRectangle(cell.ColumnIndex, cell.RowIndex, false);
                this._toolTip.Show(msg + "\n�������� : [" + cell.DataGridView.Columns[cell.ColumnIndex].HeaderText + "]", this,
                    cell.DataGridView.Location.X + cellDisplayRect.X + currentCell.Size.Width / 2,
                    cell.DataGridView.Location.Y + cellDisplayRect.Y + currentCell.Size.Height * 2 + 15, 2500);
                cell.ToolTipText = msg;
                cell.DataGridView.Rows[cell.RowIndex].Cells[cell.ColumnIndex].ErrorText = msg;
                this._validatingOk = false;
            }
        }

        void Combo_DropDown(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            combo.DropDown -= new EventHandler(this.Combo_DropDown);
            if (combo.SelectedIndex == combo.Items.Count - 1)
            {
                combo.SelectedIndex = 0;
            }
            if (combo.Items.Contains("XXXXX"))
            {
                combo.Items.Remove("XXXXX");
            }

        }

        private void ClearCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].Items.Clear();
            }
        }

        private void SubscriptCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].DropDown += new EventHandler(this.Combo_DropDown);
                combos[i].SelectedIndexChanged += new EventHandler(this.Combo_SelectedIndexChanged);
                combos[i].SelectedIndex = 0;
            }
        }

        void Combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            combo.SelectedIndexChanged -= new EventHandler(this.Combo_SelectedIndexChanged);
            if (combo.SelectedIndex == combo.Items.Count - 1)
            {
                combo.SelectedIndex = 0;
            }
            if (combo.Items.Contains("XXXXX"))
            {
                combo.Items.Remove("XXXXX");
            }
        }

        private void PrepareMaskedBoxes(MaskedTextBox[] boxes,Type validateType)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].TypeValidationCompleted += this.TypeValidation;
                boxes[i].ValidatingType = validateType;
            }
        }

        private void ValidateMaskedBoxes(MaskedTextBox[] boxes)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].ValidateText();
            }
        }


        #region �������� �������
        private void PrepareOutputSignals()
        {
            this._releSignalCol.Items.AddRange(Strings.All.ToArray());
            this._outIndSignalCol.Items.AddRange(Strings.All.ToArray());
            this._outputLogicCheckList.Items.AddRange(Strings.OutputSignals.ToArray());
            this._outputLogicCombo.SelectedIndex = 0;
            for (int i = 0; i < MR730.COutputRele.COUNT; i++)
            {
                this._outputReleGrid.Rows.Add(new object[] { i + 1, "�����������","���", 0 });
            }
            this._outputIndicatorsGrid.EditingControlShowing +=new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._outputReleGrid.EditingControlShowing +=new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < MR730.COutputIndicator.COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(new object[]{i + 1,
                                                            "�����������",
                                                            "���",
                                                            false,
                                                            false,
                                                            false});
            }
        }
        
        private void ReadOutputSignals()
        {
            try
            {
                this._exchangeProgressBar.PerformStep();
                this._outputReleGrid.Rows.Clear();
                for (int i = 0; i < MR730.COutputRele.COUNT; i++)
                {
                    this._outputReleGrid.Rows.Add(new object[]
                    {
                        i + 1, this._device.OutputRele[i].Type, this._device.OutputRele[i].SignalString,
                        this._device.OutputRele[i].ImpulseUlong
                    });
                }
                this._outputIndicatorsGrid.Rows.Clear();
                for (int i = 0; i < MR730.COutputIndicator.COUNT; i++)
                {
                    this._outputIndicatorsGrid.Rows.Add(new object[]
                    {
                        i + 1,
                        this._device.OutputIndicator[i].Type,
                        this._device.OutputIndicator[i].SignalString,
                        this._device.OutputIndicator[i].Indication,
                        this._device.OutputIndicator[i].Alarm,
                        this._device.OutputIndicator[i].System
                    });
                }
                this._outputLogicCombo.SelectedIndex = 0;
                this.ShowOutputLogicSignals(0);
            }
            catch (Exception)
            {
                if (!this.mess)
                {
                    MessageBox.Show("������������ �������� � �������� ��������.", "������");
                    this.mess = true;
                }
            }
        }

        private void ShowOutputLogicSignals(int channel)
        {
            BitArray bits = this._device.GetOutputLogicSignals(channel);
            for (int i = 0; i < bits.Count; i++)
            {
                this._outputLogicCheckList.SetItemChecked(i, bits[i]);
            }
        }

        private bool WriteOutputSignals()
        {
            bool ret = true;
            if (this._device.OutputRele.Count == MR730.COutputRele.COUNT && this._outputReleGrid.Rows.Count <= MR730.COutputRele.COUNT)
            {
                for (int i = 0; i < this._outputReleGrid.Rows.Count; i++)
                {
                    this._device.OutputRele[i].Type = this._outputReleGrid["_releTypeCol", i].Value.ToString();
                    this._device.OutputRele[i].SignalString = this._outputReleGrid["_releSignalCol", i].Value.ToString();
                    try
                    {
                        ulong value = ulong.Parse(this._outputReleGrid["_releImpulseCol", i].Value.ToString());
                        if (value > MR730.TIMELIMIT)
                        {
                            this.ShowToolTip(MR730.TIMELIMIT_ERROR_MSG, this._outputReleGrid["_releImpulseCol", i], this._outputSignalsPage);
                            ret = false;
                        }
                        else
                        {
                            this._device.OutputRele[i].ImpulseUlong = value;
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        this.ShowToolTip(MR730.TIMELIMIT_ERROR_MSG, this._outputReleGrid["_releImpulseCol", i], this._outputSignalsPage);
                        ret = false;
                    }
                }
            }

            if (this._device.OutputIndicator.Count == MR730.COutputIndicator.COUNT && this._outputIndicatorsGrid.Rows.Count <= MR730.COutputIndicator.COUNT)
            {
                for (int i = 0; i < this._outputIndicatorsGrid.Rows.Count; i++)
                {
                    this._device.OutputIndicator[i].Type = this._outputIndicatorsGrid["_outIndTypeCol", i].Value.ToString();
                    this._device.OutputIndicator[i].SignalString = this._outputIndicatorsGrid["_outIndSignalCol", i].Value.ToString();
                    this._device.OutputIndicator[i].Indication = (bool)this._outputIndicatorsGrid["_outIndResetCol", i].Value;
                    this._device.OutputIndicator[i].Alarm = (bool)this._outputIndicatorsGrid["_outIndAlarmCol", i].Value;
                    this._device.OutputIndicator[i].System = (bool)this._outputIndicatorsGrid["_outIndSystemCol", i].Value;
                }

            }
            return ret;
        }

        private void _outputLogicCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ShowOutputLogicSignals(this._outputLogicCombo.SelectedIndex);
        }

        private void _outputLogicAcceptBut_Click(object sender, EventArgs e)
        {
            BitArray bits = new BitArray(104);
            for (int i = 0; i < this._outputLogicCheckList.Items.Count; i++)
            {
                bits[i] = this._outputLogicCheckList.GetItemChecked(i);
            }
            this._device.SetOutputLogicSignals(this._outputLogicCombo.SelectedIndex, bits);
        }
        #endregion

        #region ������� ������
        private void PrepareExternalDefenses()
        {            
            this._externalDefenseGrid.EditingControlShowing +=new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._exDefBlockingCol.Items.AddRange(Strings.ExternalDefense.ToArray());
            this._exDefModeCol.Items.AddRange(Strings.ModesExternal.ToArray());
            this._exDefReturnNumberCol.Items.AddRange(Strings.ExternalDefense.ToArray());
            this._exDefWorkingCol.Items.AddRange(Strings.ExternalDefense.ToArray());
            for (int i = 0; i < MR730.CExternalDefenses.COUNT; i++)
            {
                this._externalDefenseGrid.Rows.Add(new object[]{i + 1,
                                                           "��������",
                                                           "���",
                                                           "���",
                                                           0,
                                                           false,
                                                           false,
                                                           "���",
                                                           0,
                                                           false,
                                                           false,
                                                           false});
                this.OnExternalDefenseGridModeChanged(i);
                GridManager.ChangeCellDisabling(this._externalDefenseGrid, i, false, 5, new int[] { 6, 7, 8 });

            }
            this._externalDefenseGrid.Height = this._externalDefenseGrid.ColumnHeadersHeight + this._externalDefenseGrid.Rows.Count * this._externalDefenseGrid.RowTemplate.Height;
            this._externalDefenseGrid.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._externalDefenseGrid_CellStateChanged);
        }

        private bool OnExternalDefenseGridModeChanged(int row)
        {
            int[] columns = new int[this._externalDefenseGrid.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
			{
			    columns[i] = i + 2;
			}
            return GridManager.ChangeCellDisabling(this._externalDefenseGrid, row, "��������", 1, columns);
        }
        
        void _externalDefenseGrid_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            //���������� ����� ��� ������� 
            if (this._exDefModeCol == e.Cell.OwningColumn || this._exDefReturnCol == e.Cell.OwningColumn)
            {
                if (!this.OnExternalDefenseGridModeChanged(e.Cell.RowIndex))
                {
                    GridManager.ChangeCellDisabling(this._externalDefenseGrid, e.Cell.RowIndex, false, 5, new int[] { 6, 7, 8 });
                }
            }
        }
        
        private void ReadExternalDefenses()
        {
            this._exchangeProgressBar.PerformStep();
            this._externalDefenseGrid.Rows.Clear();
            
            for (int i = 0; i < MR730.CExternalDefenses.COUNT; i++)
            {
                this._externalDefenseGrid.Rows.Add(new object[]{i + 1,
                                                           this._device.ExternalDefenses[i].Mode,
                                                           this._device.ExternalDefenses[i].BlockingNumber,
                                                           this._device.ExternalDefenses[i].WorkingNumber,
                                                           this._device.ExternalDefenses[i].WorkingTime,
                                                           this._device.ExternalDefenses[i].Return,
                                                           this._device.ExternalDefenses[i].APV_Return,
                                                           this._device.ExternalDefenses[i].ReturnNumber,
                                                           this._device.ExternalDefenses[i].ReturnTime,
                                                           this._device.ExternalDefenses[i].UROV,
                                                           this._device.ExternalDefenses[i].APV,
                                                           this._device.ExternalDefenses[i].AVR});
                if(!this.OnExternalDefenseGridModeChanged(i))
                {
                    GridManager.ChangeCellDisabling(this._externalDefenseGrid, i, false, 5, new int[] { 6, 7, 8 });
                }
            }
        }

        

        private bool WriteExternalDefenses()
        {
            bool ret = true;
            if (this._device.ExternalDefenses.Count == MR730.CExternalDefenses.COUNT && this._externalDefenseGrid.Rows.Count <= MR730.CExternalDefenses.COUNT)
            {
                for (int i = 0; i < this._externalDefenseGrid.Rows.Count; i++)
                {
                    this._device.ExternalDefenses[i].Mode =this._externalDefenseGrid["_exDefModeCol", i].Value.ToString();
                    this._device.ExternalDefenses[i].BlockingNumber = this._externalDefenseGrid["_exDefBlockingCol", i].Value.ToString();
                    this._device.ExternalDefenses[i].WorkingNumber = this._externalDefenseGrid["_exDefWorkingCol", i].Value.ToString();
                    this._device.ExternalDefenses[i].Return = (bool)this._externalDefenseGrid["_exDefReturnCol", i].Value;
                    this._device.ExternalDefenses[i].APV_Return = (bool)this._externalDefenseGrid["_exDefAPVreturnCol", i].Value;
                    this._device.ExternalDefenses[i].ReturnNumber = this._externalDefenseGrid["_exDefReturnNumberCol", i].Value.ToString();
                    this._device.ExternalDefenses[i].UROV = (bool)this._externalDefenseGrid["_exDefUROVcol", i].Value;
                    this._device.ExternalDefenses[i].APV = (bool)this._externalDefenseGrid["_exDefAPVcol", i].Value;
                    this._device.ExternalDefenses[i].AVR = (bool)this._externalDefenseGrid["_exDefAVRcol", i].Value;
                    try
                    {
                        ulong value = ulong.Parse(this._externalDefenseGrid["_exDefReturnTimeCol", i].Value.ToString());
                        if (value > MR730.TIMELIMIT)
                        {
                            throw new OverflowException(MR730.TIMELIMIT_ERROR_MSG);
                        }
                        else
                        {
                            this._device.ExternalDefenses[i].ReturnTime = value;
                        }
                    
                    }
                    catch (Exception)
                    {
                        this.ShowToolTip(MR730.TIMELIMIT_ERROR_MSG, this._externalDefenseGrid["_exDefReturnTimeCol", i], this._externalDefensePage);
                        ret = false;
                    }
                    try
                    {
                        ulong value = ulong.Parse(this._externalDefenseGrid["_exDefWorkingTimeCol", i].Value.ToString());
                        if (value > MR730.TIMELIMIT)
                        {
                            throw new OverflowException(MR730.TIMELIMIT_ERROR_MSG);
                        }
                        else
                        {
                            this._device.ExternalDefenses[i].WorkingTime = ulong.Parse(this._externalDefenseGrid["_exDefWorkingTimeCol", i].Value.ToString());
                        }
                        this._device.ExternalDefenses[i].WorkingTime = ulong.Parse(this._externalDefenseGrid["_exDefWorkingTimeCol", i].Value.ToString());
                    }
                    catch (Exception)
                    {
                        this.ShowToolTip(MR730.TIMELIMIT_ERROR_MSG, this._externalDefenseGrid["_exDefWorkingTimeCol", i], this._externalDefensePage);
                        ret = false;
                    }
                    
                }
                
            }
            return ret;
        }

        #endregion

        #region ������� �������

        private void ReadInputSignals()
        {
            this.ClearCombos(this._inputSignalsCombos);
            this.FillInputSignalsCombos();
            this.SubscriptCombos(this._inputSignalsCombos);
            this._exchangeProgressBar.PerformStep();
            this._TT_Box.Text = this._device.TT.ToString();
            this._TTNP_Box.Text = this._device.TTNP.ToString();
            this._TN_Box.Text = this._device.TN.ToString();
            this._TNNP_Box.Text = this._device.TNNP.ToString();
            double res;
            if (this._device.MaxTok > 16)
            {
                res = this._device.MaxTok - 0.01;
            }
            else
            {
                res = this._device.MaxTok;
            }
            this._maxTok_Box.Text = res.ToString();
            this._switcherDurationBox.Text = this._device.SwitcherDuration.ToString();
            this._switcherImpulseBox.Text = this._device.SwitcherImpulse.ToString();
            this._switcherTimeBox.Text = this._device.SwitcherTimeUROV.ToString();
            this._switcherTokBox.Text = this._device.SwitcherTokUROV.ToString();
                       
            this._extOffCombo.SelectedItem = this._device.ExternalOff;
            this._extOnCombo.SelectedItem = this._device.ExternalOn;
            this._constraintGroupCombo.SelectedItem = this._device.ConstraintGroup;
            this._keyOffCombo.SelectedItem = this._device.KeyOff;
            this._keyOnCombo.SelectedItem = this._device.KeyOn;
            this._signalizationCombo.SelectedItem = this._device.SignalizationReset;
            this._switcherBlockCombo.SelectedItem = this._device.SwitcherBlock;
            this._switcherErrorCombo.SelectedItem = this._device.SwitcherError;
            this._switcherStateOffCombo.SelectedItem = this._device.SwitcherOff;
            this._switcherStateOnCombo.SelectedItem = this._device.SwitcherOn;

            this._manageSignalsSDTU_Combo.SelectedItem = this._device.ManageSignalSDTU;
            this._manageSignalsKeyCombo.SelectedItem = this._device.ManageSignalKey;
            this._manageSignalsExternalCombo.SelectedItem = this._device.ManageSignalExternal;
            this._manageSignalsButtonCombo.SelectedItem = this._device.ManageSignalButton;
                        
            this._TNNP_dispepairCombo.SelectedItem = this._device.TNNP_Dispepair;
            this._TN_dispepairCombo.SelectedItem = this._device.TN_Dispepair;
            this._TN_typeCombo.SelectedItem = this._device.TN_Type;

            this._releDispepairBox.Text = this._device.DispepairImpulse.ToString();
                          
            for (int i = 0; i < this._device.DisrepairSignal.Count; i++)
            {
                this._dispepairCheckList.SetItemChecked(i, this._device.DisrepairSignal[i]);
            }
        
            this.ShowInputLogicSignals(this._logicChannelsCombo.SelectedIndex);
        }

        private void ReadKonfCount()
        {
            try
            {
                this._oscKonfComb.SelectedIndex = (int)this._device.OscilloscopeKonfCount;
            }
            catch (Exception e)
            {
            }
            
        }
        
        private ComboBox[] _inputSignalsCombos;
        private MaskedTextBox[] _inputSignalsDoubleMaskBoxes;
        private MaskedTextBox[] _inputSignalsUlongMaskBoxes;
                       
        private void PrepareInputSignals()
        {
            this._inputSignalsCombos = new ComboBox[] { this._extOffCombo,this._extOnCombo,this._constraintGroupCombo,
                                                  this._keyOffCombo,this._keyOnCombo,this._signalizationCombo,
                                                  this._switcherBlockCombo,this._switcherErrorCombo,this._switcherStateOffCombo,
                                                  this._switcherStateOnCombo,this._TN_dispepairCombo,this._TN_typeCombo,this._TNNP_dispepairCombo,
                                                  this._manageSignalsButtonCombo,this._manageSignalsExternalCombo,this._manageSignalsSDTU_Combo,this._manageSignalsKeyCombo};
            this._inputSignalsDoubleMaskBoxes = new MaskedTextBox[] { this._maxTok_Box, this._TN_Box, this._TNNP_Box, this._switcherTokBox };
            this._inputSignalsUlongMaskBoxes = new MaskedTextBox[] { this._TTNP_Box, this._TT_Box, this._switcherTimeBox, this._switcherImpulseBox, this._switcherDurationBox,this._releDispepairBox };
            

            this.FillInputSignalsCombos();

            this._logicChannelsCombo.SelectedIndex = 0;
            this.SubscriptCombos(this._inputSignalsCombos);

            this.PrepareMaskedBoxes(this._inputSignalsDoubleMaskBoxes, typeof(double));
            this.PrepareMaskedBoxes(this._inputSignalsUlongMaskBoxes, typeof(ulong));
        }

        private void FillInputSignalsCombos()
        {
            this._extOffCombo.Items.AddRange(Strings.Logic.ToArray());
            this._extOnCombo.Items.AddRange(Strings.Logic.ToArray());
            this._constraintGroupCombo.Items.AddRange(Strings.Logic.ToArray());
            this._keyOffCombo.Items.AddRange(Strings.Logic.ToArray());
            this._keyOnCombo.Items.AddRange(Strings.Logic.ToArray());
            this._signalizationCombo.Items.AddRange(Strings.Logic.ToArray());
            this._switcherBlockCombo.Items.AddRange(Strings.Logic.ToArray());
            this._switcherErrorCombo.Items.AddRange(Strings.Logic.ToArray());
            this._switcherStateOffCombo.Items.AddRange(Strings.Logic.ToArray());
            this._switcherStateOnCombo.Items.AddRange(Strings.Logic.ToArray());
            this._TNNP_dispepairCombo.Items.AddRange(Strings.Logic.ToArray());
            this._TN_dispepairCombo.Items.AddRange(Strings.Logic.ToArray());
            this._TN_typeCombo.Items.AddRange(Strings.TnType.ToArray());
            this._manageSignalsButtonCombo.Items.AddRange(Strings.Forbidden.ToArray());
            this._manageSignalsKeyCombo.Items.AddRange(Strings.Control.ToArray());
            this._manageSignalsExternalCombo.Items.AddRange(Strings.Control.ToArray());
            this._manageSignalsSDTU_Combo.Items.AddRange(Strings.Forbidden.ToArray());
        }
        
        private void _logicChannelsCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ShowInputLogicSignals(this._logicChannelsCombo.SelectedIndex);
        }

        private void ShowInputLogicSignals(int channel)
        {
            this._logicSignalsDataGrid.Rows.Clear();
            LogicState[] logicSignals = this._device.GetInputLogicSignals(channel);
            for (int i = 0; i < 16; i++)
            {               
                this._logicSignalsDataGrid.Rows.Add(new object[] {"�" + (i + 1), logicSignals[i].ToString() });
            }

        }

        private void ValidateInputSignals()
        {
            this.ValidateMaskedBoxes(this._inputSignalsUlongMaskBoxes);
            this.ValidateMaskedBoxes(this._inputSignalsDoubleMaskBoxes);
        }

        private bool WriteInputSignals()
        {
            this._device.TT = UInt16.Parse(this._TT_Box.Text);
            this._device.TTNP = UInt16.Parse(this._TTNP_Box.Text);
            this._device.TNNP = Double.Parse(this._TNNP_Box.Text);
            this._device.TN = Double.Parse(this._TN_Box.Text);
            if (Double.Parse(this._maxTok_Box.Text) > 16)
            {
                this._device.MaxTok = Double.Parse(this._maxTok_Box.Text) + 0.01;
            }else
            {
                this._device.MaxTok = Double.Parse(this._maxTok_Box.Text);
            }
            this._device.SwitcherDuration = UInt64.Parse(this._switcherDurationBox.Text);
            this._device.SwitcherImpulse = UInt64.Parse(this._switcherImpulseBox.Text);
            this._device.SwitcherTimeUROV = UInt64.Parse(this._switcherTimeBox.Text);
            this._device.SwitcherTokUROV = Double.Parse(this._switcherTokBox.Text);
                            
            this._device.ExternalOff = this._extOffCombo.SelectedItem.ToString();
            this._device.ExternalOn = this._extOnCombo.SelectedItem.ToString();
            this._device.ConstraintGroup = this._constraintGroupCombo.SelectedItem.ToString();
            this._device.KeyOff = this._keyOffCombo.SelectedItem.ToString();
            this._device.KeyOn = this._keyOnCombo.SelectedItem.ToString();
            this._device.SignalizationReset = this._signalizationCombo.SelectedItem.ToString();
            this._device.SwitcherBlock = this._switcherBlockCombo.SelectedItem.ToString();
            this._device.SwitcherError = this._switcherErrorCombo.SelectedItem.ToString();
            this._device.SwitcherOff = this._switcherStateOffCombo.SelectedItem.ToString();
            this._device.SwitcherOn = this._switcherStateOnCombo.SelectedItem.ToString();

            CheckedListBoxManager checkManager = new CheckedListBoxManager();
            checkManager.CheckList = this._dispepairCheckList;
            this._device.DisrepairSignal = checkManager.ToBoolList();
            this._device.DispepairImpulse = ulong.Parse(this._releDispepairBox.Text);

            this._device.TNNP_Dispepair = this._TNNP_dispepairCombo.SelectedItem.ToString();
            this._device.TN_Dispepair = this._TN_dispepairCombo.SelectedItem.ToString();
            this._device.TN_Type = this._TN_typeCombo.SelectedItem.ToString();

            this._device.ManageSignalButton = this._manageSignalsButtonCombo.SelectedItem.ToString();
            this._device.ManageSignalExternal = this._manageSignalsExternalCombo.SelectedItem.ToString();
            this._device.ManageSignalKey = this._manageSignalsKeyCombo.SelectedItem.ToString();
            this._device.ManageSignalSDTU = this._manageSignalsSDTU_Combo.SelectedItem.ToString();
            return true;
                         
        }
        
        private void _applyLogicSignalsBut_Click(object sender, EventArgs e)
        {
            LogicState[] logicSignals = new LogicState[16];
            for (int i = 0; i < this._logicSignalsDataGrid.Rows.Count; i++)
            {
                object o = Enum.Parse(typeof(LogicState),this._logicSignalsDataGrid["_diskretValueCol", i].Value.ToString());
                logicSignals[i] = (LogicState)(o);
            }
            this._device.SetInputLogicSignals(this._logicChannelsCombo.SelectedIndex,logicSignals);
        }
        #endregion

        #region �������� ����������

        void _device_AutomaticsPageLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_AutomaticsPageLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnAutomaticsPageLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }


        private ComboBox[] _automaticCombos;
        private MaskedTextBox[] _automaticsMaskedBoxes;

        private void ValidateAutomatics()
        {
            this.ValidateMaskedBoxes(this._automaticsMaskedBoxes);
            this.ValidateMaskedBoxes(new MaskedTextBox[] { this.lzsh_constraint });
        }
        
        private void PrepareAutomaticsSignals()
        {
            this._automaticCombos = new ComboBox[] 
            {
                this.apv_self_off,this.apv_conf,this.apv_blocking,
                this.avr_abrasion,this.avr_blocking,this.avr_reset_blocking,
                this.avr_return,this.avr_start,this.lzsh_conf
            };
            this._automaticsMaskedBoxes = new MaskedTextBox[]
            {
                this.apv_time_1krat,this.apv_time_2krat,this.apv_time_3krat,this.apv_time_4krat,
                this.apv_time_blocking,this.apv_time_ready,this.avr_disconnection,
                this.avr_time_abrasion,this.avr_time_return
            };
            this.PrepareMaskedBoxes(this._automaticsMaskedBoxes, typeof(ulong));
            this.PrepareMaskedBoxes(new MaskedTextBox[] { this.lzsh_constraint }, typeof(double));

            this.FillAutomaticCombo();
            this.SubscriptCombos(this._automaticCombos);
           
        }

        private void FillAutomaticCombo()
        {
            this.apv_blocking.Items.AddRange(Strings.Logic.ToArray());
            this.avr_start.Items.AddRange(Strings.Logic.ToArray());
            this.avr_blocking.Items.AddRange(Strings.Logic.ToArray());
            this.avr_reset_blocking.Items.AddRange(Strings.Logic.ToArray());
            this.avr_abrasion.Items.AddRange(Strings.Logic.ToArray());
            this.avr_return.Items.AddRange(Strings.Logic.ToArray());
            this.apv_conf.Items.AddRange(Strings.Crat.ToArray());
            this.lzsh_conf.Items.AddRange(Strings.ModesLight.ToArray());
            this.apv_self_off.Items.AddRange(Strings.YesNo.ToArray());


        }

        private void ReadAutomaticsPage() 
        {
            this._exchangeProgressBar.PerformStep();
            this.ClearCombos(this._automaticCombos);
            this.FillAutomaticCombo();
            this.SubscriptCombos(this._automaticCombos);
            this.apv_conf.Text = this._device.APV_Cnf;
            this.apv_blocking.Text = this._device.APV_Blocking;
            this.apv_self_off.Text = this._device.APV_Start;
            this.avr_start.Text = this._device.AVR_Start;
            this.avr_blocking.Text = this._device.AVR_Blocking;
            this.avr_reset_blocking.Text = this._device.AVR_Reset;
            this.avr_abrasion.Text = this._device.AVR_Abrasion;
            this.avr_return.Text = this._device.AVR_Return;
            this.lzsh_conf.Text = this._device.LZSH_Cnf;

            this.apv_time_blocking.Text = this._device.APV_Time_Blocking.ToString();
            this.apv_time_ready.Text = this._device.APV_Time_Ready.ToString();
            this.apv_time_1krat.Text = this._device.APV_Time_1Krat.ToString();
            this.apv_time_2krat.Text = this._device.APV_Time_2Krat.ToString();
            this.apv_time_3krat.Text = this._device.APV_Time_3Krat.ToString();
            this.apv_time_4krat.Text = this._device.APV_Time_4Krat.ToString();

            this.avr_supply_off.Checked = this._device.AVR_Supply_Off;
            this.avr_self_off.Checked = this._device.AVR_Self_Off;
            this.avr_switch_off.Checked = this._device.AVR_Switch_Off;
            this.avr_abrasion_switch.Checked = this._device.AVR_Abrasion_Switch;
            this.avr_permit_reset_switch.Checked = this._device.AVR_Reset_Switch;
            this.avr_time_abrasion.Text = this._device.AVR_Time_Abrasion.ToString();
            this.avr_time_return.Text = this._device.AVR_Time_Return.ToString();
            this.avr_disconnection.Text = this._device.AVR_Time_Off.ToString();
            this.lzsh_constraint.Text = this._device.LZSH_Constraint.ToString();

        }

        private void OnAutomaticsPageLoadOk()
        {
            this._exchangeProgressBar.PerformStep();
            //this.ClearCombos(this._automaticCombos);
            //this.FillAutomaticCombo();
            //this.SubscriptCombos(this._automaticCombos);
            this.apv_conf.Text = this._device.APV_Cnf;
            this.apv_blocking.Text = this._device.APV_Blocking;
            this.apv_self_off.Text = this._device.APV_Start;
            this.avr_start.Text = this._device.AVR_Start;
            this.avr_blocking.Text = this._device.AVR_Blocking;
            this.avr_reset_blocking.Text = this._device.AVR_Reset;
            this.avr_abrasion.Text = this._device.AVR_Abrasion;
            this.avr_return.Text = this._device.AVR_Return;
            this.lzsh_conf.Text = this._device.LZSH_Cnf;
                        
            this.apv_time_blocking.Text = this._device.APV_Time_Blocking.ToString();
            this.apv_time_ready.Text = this._device.APV_Time_Ready.ToString();
            this.apv_time_1krat.Text = this._device.APV_Time_1Krat.ToString();
            this.apv_time_2krat.Text = this._device.APV_Time_2Krat.ToString();
            this.apv_time_3krat.Text = this._device.APV_Time_3Krat.ToString();
            this.apv_time_4krat.Text = this._device.APV_Time_4Krat.ToString();
            
            this.avr_supply_off.Checked = this._device.AVR_Supply_Off;
            this.avr_self_off.Checked = this._device.AVR_Self_Off;
            this.avr_switch_off.Checked = this._device.AVR_Switch_Off;
            this.avr_abrasion_switch.Checked = this._device.AVR_Abrasion_Switch;
            this.avr_permit_reset_switch.Checked = this._device.AVR_Reset_Switch;
            this.avr_time_abrasion.Text = this._device.AVR_Time_Abrasion.ToString();
            this.avr_time_return.Text = this._device.AVR_Time_Return.ToString();
            this.avr_disconnection.Text = this._device.AVR_Time_Off.ToString();
            this.lzsh_constraint.Text = this._device.LZSH_Constraint.ToString();

        }

        private bool WriteAutomaticsPage()
        {
            this._device.APV_Cnf = this.apv_conf.Text;
            this._device.APV_Blocking = this.apv_blocking.Text;
            this._device.APV_Time_Blocking = UInt64.Parse(this.apv_time_blocking.Text);
            this._device.APV_Time_Ready = UInt64.Parse(this.apv_time_ready.Text);
            this._device.APV_Time_1Krat = UInt64.Parse(this.apv_time_1krat.Text);
            this._device.APV_Time_2Krat = UInt64.Parse(this.apv_time_2krat.Text);
            this._device.APV_Time_3Krat = UInt64.Parse(this.apv_time_3krat.Text);
            this._device.APV_Time_4Krat = UInt64.Parse(this.apv_time_4krat.Text);
            this._device.APV_Start = this.apv_self_off.Text;

            this._device.AVR_Supply_Off = this.avr_supply_off.Checked;
            this._device.AVR_Self_Off = this.avr_self_off.Checked;
            this._device.AVR_Switch_Off = this.avr_switch_off.Checked;
            this._device.AVR_Abrasion_Switch = this.avr_abrasion_switch.Checked;
            this._device.AVR_Reset_Switch = this.avr_permit_reset_switch.Checked;
            this._device.AVR_Start = this.avr_start.Text;
            this._device.AVR_Blocking = this.avr_blocking.Text;
            this._device.AVR_Reset = this.avr_reset_blocking.Text;
            this._device.AVR_Abrasion = this.avr_abrasion.Text;
            this._device.AVR_Time_Abrasion = UInt64.Parse(this.avr_time_abrasion.Text);
            this._device.AVR_Return = this.avr_return.Text;
            this._device.AVR_Time_Return = UInt64.Parse(this.avr_time_return.Text);
            this._device.AVR_Time_Off = UInt64.Parse(this.avr_disconnection.Text);

            this._device.LZSH_Cnf = this.lzsh_conf.Text;
            this._device.LZSH_Constraint = double.Parse(this.lzsh_constraint.Text);

            return true;
        }
        #endregion

        #region ������� ������

        void ChangeTokDefenseCellDisabling1(DataGridView grid,int row)
        {
            if (!this.OnTokDefenseGridModeChanged(grid, row))
            {
                GridManager.ChangeCellDisabling(grid, row, false, 3, 4);
                GridManager.ChangeCellDisabling(grid, row, "���", 5, 6);
                GridManager.ChangeCellDisabling(grid, row, false, 11, 12);
            }
        }
        void ChangeTokDefenseCellDisabling2(DataGridView grid, int row)
        {
            if (!this.OnTokDefenseGridModeChanged(grid, row))
            {
                GridManager.ChangeCellDisabling(grid, row, false, 3, 4);
                GridManager.ChangeCellDisabling(grid, row, "���", 5, 6);
            }
        }
        void ChangeTokDefenseCellDisabling3(DataGridView grid, int row)
        {
            if (!this.OnTokDefenseGridModeChanged(grid, row))
            {
                if (0 == row)
                {
                    GridManager.ChangeCellDisabling(grid, row, false, 3, 4);
                    GridManager.ChangeCellDisabling(grid, row, false, 7, 8);    
                }
                
            }
        }

        void ShowTokDefenses(MR730.CTokDefenses tokDefenses)
        {
            this._tokDefenseGrid1.Rows.Clear();
            this._tokDefenseGrid3.Rows.Clear();
            this._tokDefenseGrid2.Rows.Clear();
            this._tokDefenseGrid4.Rows.Clear();

            this._tokDefenseIbox.Text = tokDefenses.I.ToString();
            this._tokDefenseInbox.Text = tokDefenses.In.ToString();
            this._tokDefenseI0box.Text = tokDefenses.I0.ToString();
            this._tokDefenseI2box.Text = tokDefenses.I2.ToString();
            
            for (int i = 0; i < 2; i++)
            {
                if (tokDefenses[i].Feature.ToString() == "���������")
                {
                    if (tokDefenses[i].WorkConstraint <= 24)
                    {
                        this._tokDefenseGrid1.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime/10,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                        this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);
                    }
                    else
                    {
                        this._tokDefenseGrid1.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint - 0.01,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime/10,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                        this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);
                    }
                }
                else
                {
                    if (tokDefenses[i].WorkConstraint <= 24)
                    {
                        this._tokDefenseGrid1.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                        this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);
                    }
                    else
                    {
                        this._tokDefenseGrid1.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint - 0.01,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                        this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);
                    }
}

            }
            for (int i = 2; i < 4; i++)
            {
                if (tokDefenses[i].Feature.ToString() == "���������")
                {
                    if (tokDefenses[i].WorkConstraint <= 24)
                    {
                        this._tokDefenseGrid2.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime/10,
                                                       tokDefenses[i].Engine,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                        this.ChangeTokDefenseCellDisabling2(this._tokDefenseGrid2, i - 2);
                    }
                    else
                    {
                        this._tokDefenseGrid2.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint-0.01,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime/10,
                                                       tokDefenses[i].Engine,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                        this.ChangeTokDefenseCellDisabling2(this._tokDefenseGrid2, i - 2);
                    }
                }
                else 
                {
                    if (tokDefenses[i].WorkConstraint <= 24)
                    {
                        this._tokDefenseGrid2.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Engine,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                        this.ChangeTokDefenseCellDisabling2(this._tokDefenseGrid2, i - 2);
                    }
                    else
                    {
                        this._tokDefenseGrid2.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint-0.01,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Engine,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                        this.ChangeTokDefenseCellDisabling2(this._tokDefenseGrid2, i - 2);
                    }

                }
            }
            for (int i = 4; i < 10; i++)
            {
                if (tokDefenses[i].WorkConstraint <= 24)
                {
                    this._tokDefenseGrid3.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                    this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid3, i - 4);
                }
                else
                {
                    this._tokDefenseGrid3.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint-0.01,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                    this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid3, i - 4);
                }

                
            }

            const int Ig_index = 10;
            this._tokDefenseGrid4.Rows.Add(new object[]{ tokDefenses[Ig_index].Name,
                                                       tokDefenses[Ig_index].Mode,     
                                                       tokDefenses[Ig_index].BlockingNumber,
                                                       tokDefenses[Ig_index].PuskU,
                                                       tokDefenses[Ig_index].PuskU_Constraint,
                                                       tokDefenses[Ig_index].WorkConstraint,
                                                       tokDefenses[Ig_index].WorkTime,
                                                       tokDefenses[Ig_index].Speedup,
                                                       tokDefenses[Ig_index].SpeedupTime,
                                                       tokDefenses[Ig_index].UROV,
                                                       tokDefenses[Ig_index].APV,
                                                       tokDefenses[Ig_index].AVR                                                    

            });
            this.ChangeTokDefenseCellDisabling3(this._tokDefenseGrid4, 0);
            const int I12_index = 11;
            this._tokDefenseGrid4.Rows.Add(new object[]{ tokDefenses[I12_index].Name,
                                                       tokDefenses[I12_index].Mode,     
                                                       tokDefenses[I12_index].BlockingNumber,
                                                       false,
                                                       0.0,
                                                       tokDefenses[I12_index].WorkConstraint,
                                                       tokDefenses[I12_index].WorkTime,
                                                       false,
                                                       0.0,
                                                       tokDefenses[I12_index].UROV,
                                                       tokDefenses[I12_index].APV,
                                                       tokDefenses[I12_index].AVR  });
            this.ChangeTokDefenseCellDisabling3(this._tokDefenseGrid4, 1);

            this._tokDefenseGrid4["_tokDefense4UpuskCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4UpuskCol", 1].Style.BackColor = SystemColors.Control;
            this._tokDefenseGrid4["_tokDefense4PuskConstraintCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4PuskConstraintCol", 1].Style.BackColor = SystemColors.Control;
            this._tokDefenseGrid4["_tokDefense4SpeedupCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4SpeedupCol", 1].Style.BackColor = SystemColors.Control;
            this._tokDefenseGrid4["_tokDefense4SpeedupTimeCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4SpeedupTimeCol", 1].Style.BackColor = SystemColors.Control;

            for (int i = 0; i < this._tokDefenseGrid1.Rows.Count; i++)
            {
                this._tokDefenseGrid1[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            for (int i = 0; i < this._tokDefenseGrid2.Rows.Count; i++)
            {
                this._tokDefenseGrid2[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            for (int i = 0; i < this._tokDefenseGrid3.Rows.Count; i++)
            {
                this._tokDefenseGrid3[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            for (int i = 0; i < this._tokDefenseGrid4.Rows.Count; i++)
            {
                this._tokDefenseGrid4[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
        }

        void PrepareTokDefenses()
        {
            this._tokDefenseModeCol.Items.AddRange(Strings.Modes.ToArray());
            this._tokDefenseParameterCol.Items.AddRange(Strings.TokParameter.ToArray());
            this._tokDefenseBlockExistCol.Items.AddRange(Strings.Blocking.ToArray());
            this._tokDefenseDirectionCol.Items.AddRange(Strings.BusDirection.ToArray());
            this._tokDefenseFeatureCol.Items.AddRange(Strings.FeatureLight.ToArray());
            this._tokDefenseBlockNumberCol.Items.AddRange(Strings.Logic.ToArray());

            this._tokDefense2ModeCol.Items.AddRange(Strings.Modes.ToArray());
            this._tokDefense2ParameterCol.Items.AddRange(Strings.TokParameter.ToArray());
            this._tokDefense2BlockExistCol.Items.AddRange(Strings.Blocking.ToArray());
            this._tokDefense2DirectionCol.Items.AddRange(Strings.BusDirection.ToArray());
            this._tokDefense2FeatureCol.Items.AddRange(Strings.FeatureLight.ToArray());
            this._tokDefense2BlockNumberCol.Items.AddRange(Strings.Logic.ToArray());
            this._tokDefense2EngineCol.Items.AddRange(Strings.EngineMode.ToArray());

            this._tokDefense3ModeCol.Items.AddRange(Strings.Modes.ToArray());
            this._tokDefense3ParameterCol.Items.AddRange(Strings.EngineParameter.ToArray());
            this._tokDefense3BlockingExistCol.Items.AddRange(Strings.Blocking.ToArray());
            this._tokDefense3DirectionCol.Items.AddRange(Strings.BusDirection.ToArray());
            this._tokDefense3FeatureCol.Items.AddRange(Strings.Feature.ToArray());
            this._tokDefense3BlockingNumberCol.Items.AddRange(Strings.Logic.ToArray());

            this._tokDefense4ModeCol.Items.AddRange(Strings.Modes.ToArray());
            this._tokDefense4BlockNumberCol.Items.AddRange(Strings.Logic.ToArray());

            this._tokDefenseI0box.ValidatingType = this._tokDefenseI2box.ValidatingType =
            this._tokDefenseInbox.ValidatingType = this._tokDefenseIbox.ValidatingType = typeof(ulong);

            this._tokDefenseIbox.TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
            this._tokDefenseI2box.TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
            this._tokDefenseI0box.TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
            this._tokDefenseInbox.TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
            
                        
            this.ShowTokDefenses(this._device.TokDefensesMain);
                                                
            this._tokDefenseGrid1.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._tokDefenseGrid_CellStateChanged);
            this._tokDefenseGrid2.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._tokDefenseGrid_CellStateChanged);
            this._tokDefenseGrid3.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._tokDefenseGrid_CellStateChanged);
            this._tokDefenseGrid4.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._tokDefenseGrid_CellStateChanged);

            this._tokDefenseGrid1.EditingControlShowing +=new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._tokDefenseGrid2.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._tokDefenseGrid3.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._tokDefenseGrid4.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            
            
        }

        void _tokDefenseGrid_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (this._tokDefenseModeCol == e.Cell.OwningColumn ||
                this._tokDefenseU_PuskCol == e.Cell.OwningColumn ||
                this._tokDefenseSpeedUpCol == e.Cell.OwningColumn ||
                this._tokDefenseDirectionCol == e.Cell.OwningColumn || 
                                
                this._tokDefense3ModeCol == e.Cell.OwningColumn ||
                this._tokDefense3UpuskCol == e.Cell.OwningColumn ||
                this._tokDefense3SpeedupCol == e.Cell.OwningColumn ||
                this._tokDefense3DirectionCol == e.Cell.OwningColumn 
                )
            {
                this.ChangeTokDefenseCellDisabling1(sender as DataGridView, e.Cell.RowIndex);    
            }
            if (this._tokDefense2ModeCol == e.Cell.OwningColumn ||
                this._tokDefense2PuskUCol == e.Cell.OwningColumn ||
                this._tokDefense2DirectionCol == e.Cell.OwningColumn 
                )
            {
                this.ChangeTokDefenseCellDisabling2(sender as DataGridView, e.Cell.RowIndex);    
            }
            if (this._tokDefense4ModeCol == e.Cell.OwningColumn ||
                (this._tokDefense4SpeedupCol == e.Cell.OwningColumn && 1 != e.Cell.RowIndex) ||
                this._tokDefense4UpuskCol == e.Cell.OwningColumn && 1 != e.Cell.RowIndex)
            {
                this.ChangeTokDefenseCellDisabling3(sender as DataGridView, e.Cell.RowIndex);
            }
            
        }
        
        private void OnTokDefenseLoadOk()
        {
            this._exchangeProgressBar.PerformStep();

            if (this._mainGroupRadioButton.Checked)
	        {
		         this.ShowTokDefenses(this._device.TokDefensesMain);
	        }
            else
            {
                this.ShowTokDefenses(this._device.TokDefensesReserve);
            }
        }
        
        private bool OnTokDefenseGridModeChanged(DataGridView grid,int row)
        {
            int[] columns = new int[grid.Columns.Count - 2];
            if (grid == this._tokDefenseGrid4 && 1 == row)
            {
                columns = new int[6] {2,5,6,9,10,11};
            }
            else
            {
                for (int i = 0; i < columns.Length; i++)
                {
                    columns[i] = i + 2;
                }
            }
            return GridManager.ChangeCellDisabling(grid, row, "��������", 1, columns);
        }
                
        private bool WriteTokDefenseItem(MR730.CTokDefenses tokDefenses,DataGridView grid,int rowIndex,int itemIndex)
        {
           bool ret = true;
           bool enabled = false;
           tokDefenses[itemIndex].Mode = grid[1, rowIndex].Value.ToString();
           enabled = tokDefenses[itemIndex].Mode == "��������";
           //if (enabled)
           //{
               tokDefenses[itemIndex].BlockingNumber = grid[2, rowIndex].Value.ToString();
               tokDefenses[itemIndex].PuskU = (bool)grid[3, rowIndex].Value;
               try
               {
                   double value = double.Parse(grid[4, rowIndex].Value.ToString());
                   if ((value < 0 || value > 256) && !enabled)
                   {
                       this.ShowToolTip("������� ����� � ��������� [0 - 256.0]", grid[4, rowIndex], this._tokDefendTabPage);
                       ret = false;
                   }
                   else
                   {
                       tokDefenses[itemIndex].PuskU_Constraint = value;
                   }

               }
               catch (Exception)
               {
                   this.ShowToolTip("������� ����� � ��������� [0 - 256.0]", grid[4, rowIndex], this._tokDefendTabPage);
                   ret = false;
               }

               if (grid != this._tokDefenseGrid4)
               {
                   tokDefenses[itemIndex].Direction = grid[5, rowIndex].Value.ToString();
                   tokDefenses[itemIndex].BlockingExist = grid[6, rowIndex].Value.ToString();
                   tokDefenses[itemIndex].Parameter = grid[7, rowIndex].Value.ToString();
               }

               int workConstraintIndex = (grid == this._tokDefenseGrid4) ? 5 : 8;
               int workTimeIndex = (grid == this._tokDefenseGrid4) ? 6 : 10;
               int speedupIndex = (grid == this._tokDefenseGrid4) ? 7 : 11;
               int speedupTimeIndex = (grid == this._tokDefenseGrid4) ? 8 : 12;
               int UROVIndex = (grid == this._tokDefenseGrid4) ? 9 : 13;
               int APVIndex = (grid == this._tokDefenseGrid4) ? 10 : 14;
               int AVRIndex = (grid == this._tokDefenseGrid4) ? 11 : 15;

               double limit = 40;
               if (grid == this._tokDefenseGrid1)
               {
                   limit = 40;
               }
               if (grid == this._tokDefenseGrid4)
               {
                   if (0 == rowIndex)
                   {
                       limit = 5;
                   }
                   else
                   {
                       limit = 100;
                   }
               }
               if (grid == this._tokDefenseGrid3)
               {
                   if (4 == rowIndex || 5 == rowIndex)
                   {
                       limit = 5;
                   }
               }

               try
               {
                   double value = double.Parse(grid[workConstraintIndex, rowIndex].Value.ToString());
                   if ((value < 0 || value > limit) && !enabled)
                   {
                       this.ShowToolTip("������� �����  � ��������� [0 - " + limit + ".0]", grid[workConstraintIndex, rowIndex], this._tokDefendTabPage);
                       ret = false;
                   }
                   else
                   {
                       if(value > 24)
                       {
                           if (grid == this._tokDefenseGrid4)
                           {
                               tokDefenses[itemIndex].WorkConstraint = value;
                           }
                           else { tokDefenses[itemIndex].WorkConstraint = value + 0.01; }
                           
                       }
                       else{
                           if (grid == this._tokDefenseGrid4)
                           {
                               tokDefenses[itemIndex].WorkConstraint = value;
                           }
                           else { tokDefenses[itemIndex].WorkConstraint = value; }

                       }
                       
                   }

               }
               catch (Exception)
               {
                   this.ShowToolTip("������� �����  � ��������� [0 - " + limit + ".0]", grid[workConstraintIndex, rowIndex], this._tokDefendTabPage);
                   ret = false;
               }
               tokDefenses[itemIndex].Feature = grid[9, rowIndex].Value.ToString();
               try
               {
                   ulong value = ulong.Parse(grid[workTimeIndex, rowIndex].Value.ToString());

                   if ("���������" == grid[9, rowIndex].Value.ToString())
                   {
                       if (value > MR730.KZLIMIT && !enabled)
                       {
                           this.ShowToolTip(MR730.KZLIMIT_ERROR_MSG, grid[workTimeIndex, rowIndex], this._tokDefendTabPage);
                           ret = false;
                       }
                       else
                       {
                           tokDefenses[itemIndex].WorkTime = value * 10;
                       }

                   }else
                   {
                       if (value > MR730.TIMELIMIT && !enabled)
                       {
                           this.ShowToolTip(MR730.TIMELIMIT_ERROR_MSG, grid[workTimeIndex, rowIndex], this._tokDefendTabPage);
                           ret = false;
                       }
                       else
                       {
                           tokDefenses[itemIndex].WorkTime = value;
                       }
                   }


               }
               catch (Exception)
               {
                   this.ShowToolTip(MR730.TIMELIMIT_ERROR_MSG, grid[workTimeIndex, rowIndex], this._tokDefendTabPage);
                   ret = false;
               }
               if (grid == this._tokDefenseGrid2)
               {
                   tokDefenses[itemIndex].Engine = grid[11, rowIndex].Value.ToString();
                   tokDefenses[itemIndex].UROV = (bool)grid[12, rowIndex].Value;
                   tokDefenses[itemIndex].APV = (bool)grid[13, rowIndex].Value;
                   tokDefenses[itemIndex].AVR = (bool)grid[14, rowIndex].Value;
               }
               else
               {
                   tokDefenses[itemIndex].Speedup = (bool)grid[speedupIndex, rowIndex].Value;
                   try
                   {
                       ulong value = ulong.Parse(grid[speedupTimeIndex, rowIndex].Value.ToString());
                       if (value > MR730.TIMELIMIT && !enabled)
                       {
                           this.ShowToolTip(MR730.TIMELIMIT_ERROR_MSG, grid[speedupTimeIndex, rowIndex], this._tokDefendTabPage);
                           ret = false;
                       }
                       else
                       {
                           tokDefenses[itemIndex].SpeedupTime = value;
                       }

                   }
                   catch (Exception)
                   {
                       this.ShowToolTip(MR730.TIMELIMIT_ERROR_MSG, grid[speedupTimeIndex, rowIndex], this._tokDefendTabPage);
                       ret = false;
                   }
                   tokDefenses[itemIndex].UROV = (bool)grid[UROVIndex, rowIndex].Value;
                   tokDefenses[itemIndex].APV = (bool)grid[APVIndex, rowIndex].Value;
                   tokDefenses[itemIndex].AVR = (bool)grid[AVRIndex, rowIndex].Value;
               }
           //}
            return ret;
        }

        private bool WriteTokDefenses(MR730.CTokDefenses tokDefenses)
        {
            bool ret = true;
           tokDefenses.I = ushort.Parse(this._tokDefenseIbox.Text);
           tokDefenses.I0 = ushort.Parse(this._tokDefenseI0box.Text);
           tokDefenses.I2 = ushort.Parse(this._tokDefenseI2box.Text);
           tokDefenses.In = ushort.Parse(this._tokDefenseInbox.Text);
            for(int i = 0; i < 12;i++)
            {
                if (i >= 0 && i < 2)
	            {
                    ret &= this.WriteTokDefenseItem(tokDefenses,this._tokDefenseGrid1, i, i);
	            }
                if (i >= 2 && i < 4)
                {
                    ret &= this.WriteTokDefenseItem(tokDefenses,this._tokDefenseGrid2, i - 2, i);
                }
                if (i >= 4 && i < 10)
                {
                    ret &= this.WriteTokDefenseItem(tokDefenses,this._tokDefenseGrid3, i - 4, i);
                }
                if (i >= 10 && i < 12)
                {
                    ret &= this.WriteTokDefenseItem(tokDefenses,this._tokDefenseGrid4, i - 10, i);
                }
            }
            
            return ret;
        }

        private void ValidateTokDefenses()
        {
            this._tokDefenseIbox.ValidateText();
            this._tokDefenseI2box.ValidateText();
            this._tokDefenseI0box.ValidateText();
            this._tokDefenseInbox.ValidateText();
        }

        private void _tokDefenseMainConstraintRadio_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region ������ �� ����������
        private void OnVoltageDefensesLoadOk()
        {
            this._exchangeProgressBar.PerformStep();
            if (this._mainGroupRadioButton.Checked)
            {
                this.ShowVoltageDefenses(this._device.VoltageDefensesMain);    
            }
            else
            {
                this.ShowVoltageDefenses(this._device.VoltageDefensesReserve);
            }
        }

        private void PrepareVoltageDefenses()
        {
            this._voltageDefensesGrid1.Rows.Add(4);
            this._voltageDefensesGrid1[0, 0].Value = "U>";
            this._voltageDefensesGrid1[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid1[0, 1].Value = "U>>";
            this._voltageDefensesGrid1[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid1[0, 2].Value = "U<";
            this._voltageDefensesGrid1[0, 2].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid1[0, 3].Value = "U<<";
            this._voltageDefensesGrid1[0, 3].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid2.Rows.Add(2);
            this._voltageDefensesGrid2[0, 0].Value = "U2>";
            this._voltageDefensesGrid2[0, 1].Value = "U2>>";
            this._voltageDefensesGrid2[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid2[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid3.Rows.Add(2);
            this._voltageDefensesGrid3[0, 0].Value = "U0>";
            this._voltageDefensesGrid3[0, 1].Value = "U0>>";
            this._voltageDefensesGrid3[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid3[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;

            this._voltageDefensesGrid1.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._voltageDefensesGrid2.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._voltageDefensesGrid3.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);

            this._voltageDefensesModeCol1.Items.AddRange(Strings.Modes.ToArray());
            this._voltageDefensesBlockingNumberCol1.Items.AddRange(Strings.Logic.ToArray());
            this._voltageDefensesParameterCol1.Items.AddRange(Strings.VoltageParameterU.ToArray());

            this._voltageDefensesModeCol2.Items.AddRange(Strings.Modes.ToArray());
            this._voltageDefensesBlockingNumberCol2.Items.AddRange(Strings.Logic.ToArray());
            this._voltageDefensesParameterCol2.Items.AddRange(Strings.VoltageParameterU2.ToArray());

            this._voltageDefensesModeCol3.Items.AddRange(Strings.Modes.ToArray());
            this._voltageDefensesBlockingNumberCol3.Items.AddRange(Strings.Logic.ToArray());
            this._voltageDefensesParameterCol3.Items.AddRange(Strings.VoltageParameterU0.ToArray());

            this._voltageDefensesGrid1.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this.OnVoltageDefenseCellStateChanged);
            this._voltageDefensesGrid2.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this.OnVoltageDefenseCellStateChanged);
            this._voltageDefensesGrid3.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this.OnVoltageDefenseCellStateChanged);

            this.ShowVoltageDefenses(this._device.VoltageDefensesMain);
        }

        void OnVoltageDefenseCellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (e.Cell.OwningColumn == this._voltageDefensesModeCol1 || 
                e.Cell.OwningColumn == this._voltageDefensesModeCol2 ||
                e.Cell.OwningColumn == this._voltageDefensesModeCol3 ||
                e.Cell.OwningColumn == this._voltageDefensesReturnCol1 ||
                e.Cell.OwningColumn == this._voltageDefensesReturnCol2 ||
                e.Cell.OwningColumn == this._voltageDefensesReturnCol3
                )
            {
                DataGridView voltageGrid = sender as DataGridView;
                int rowIndex = e.Cell.RowIndex;

                OnVoltageDefenseModeChange(voltageGrid, rowIndex);
            }
        }

        private static void OnVoltageDefenseModeChange(DataGridView voltageGrid, int rowIndex)
        {
            int[] columns = new int[voltageGrid.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }
            if (!GridManager.ChangeCellDisabling(voltageGrid, rowIndex, "��������", 1, columns))
            {
                GridManager.ChangeCellDisabling(voltageGrid, rowIndex, false, 6, 7, 8, 9);
            }
        }
        
        private void SetVoltageDefenseGridItem(DataGridView grid,MR730.CVoltageDefenses voltageDefenses,int rowIndex,int defenseItemIndex)
        {             
            grid[1, rowIndex].Value = voltageDefenses[defenseItemIndex].Mode;
            grid[2, rowIndex].Value = voltageDefenses[defenseItemIndex].BlockingNumber;
            grid[3, rowIndex].Value = voltageDefenses[defenseItemIndex].Parameter;
            grid[4, rowIndex].Value = voltageDefenses[defenseItemIndex].WorkConstraint;
            grid[5, rowIndex].Value = voltageDefenses[defenseItemIndex].WorkTime;
            grid[6, rowIndex].Value = voltageDefenses[defenseItemIndex].Return;
            grid[7, rowIndex].Value = voltageDefenses[defenseItemIndex].APV_Return;
            grid[8, rowIndex].Value = voltageDefenses[defenseItemIndex].ReturnConstraint;
            grid[9, rowIndex].Value = voltageDefenses[defenseItemIndex].ReturnTime;
            grid[10, rowIndex].Value = voltageDefenses[defenseItemIndex].UROV;
            grid[11, rowIndex].Value = voltageDefenses[defenseItemIndex].APV;
            grid[12, rowIndex].Value = voltageDefenses[defenseItemIndex].AVR;
        }

        private void ShowVoltageDefenses(MR730.CVoltageDefenses voltageDefenses)
        {            
            for(int i = 0; i < 4;i++)
            {
                this.SetVoltageDefenseGridItem(this._voltageDefensesGrid1, voltageDefenses, i, i);
                OnVoltageDefenseModeChange(this._voltageDefensesGrid1, i);
            }
            for (int i = 0; i < 2; i++)
            {
                this.SetVoltageDefenseGridItem(this._voltageDefensesGrid2, voltageDefenses, i, i + 4);
                OnVoltageDefenseModeChange(this._voltageDefensesGrid2, i);
            }
            for (int i = 0; i < 2; i++)
            {
                this.SetVoltageDefenseGridItem(this._voltageDefensesGrid3, voltageDefenses, i, i + 6);
                OnVoltageDefenseModeChange(this._voltageDefensesGrid3, i);
            }
      
        }

        private void _voltageDefenseMainConstraintRadio_CheckedChanged(object sender, EventArgs e)
        {
          
        }

        private bool WriteVoltageDefenseGridItem(DataGridView grid, MR730.CVoltageDefenses voltageDefenses, int rowIndex, int defenseItemIndex)
        {
            bool ret = true;
            bool enabled = false;
            voltageDefenses[defenseItemIndex].Mode = grid[1, rowIndex].Value.ToString();
            enabled = voltageDefenses[defenseItemIndex].Mode == "��������";
            //if (enabled)
            //{


                voltageDefenses[defenseItemIndex].BlockingNumber = grid[2, rowIndex].Value.ToString();
                voltageDefenses[defenseItemIndex].Parameter = grid[3, rowIndex].Value.ToString();
                try
                {
                    double value = double.Parse(grid[4, rowIndex].Value.ToString());
                    if ((value < 0 || value > 256) && !enabled)
                    {
                        this.ShowToolTip("������� ����� � ��������� [0 - 256.0]", grid[4, rowIndex], this._voltageDefendTabPage);
                        ret = false;
                    }
                    else
                    {
                        voltageDefenses[defenseItemIndex].WorkConstraint = double.Parse(grid[4, rowIndex].Value.ToString());
                    }

                }
                catch (Exception)
                {
                    this.ShowToolTip("������� ����� � ��������� [0 - 256.0]", grid[4, rowIndex], this._voltageDefendTabPage);
                    ret = false;
                }
                try
                {
                    ulong value;
                    if (grid[5, rowIndex].Value==null)
                    {value = 0;}else
                    {value = ulong.Parse(grid[5, rowIndex].Value.ToString());}
                    
                    if (value > MR730.TIMELIMIT && !enabled)
                    {
                        this.ShowToolTip(MR730.TIMELIMIT_ERROR_MSG, grid[5, rowIndex], this._voltageDefendTabPage);
                        ret = false;
                    }
                    else
                    {
                        voltageDefenses[defenseItemIndex].WorkTime = value;
                    }

                }
                catch (Exception)
                {
                    this.ShowToolTip(MR730.TIMELIMIT_ERROR_MSG, grid[5, rowIndex], this._voltageDefendTabPage);
                    ret = false;
                }
                voltageDefenses[defenseItemIndex].Return = (bool)grid[6, rowIndex].Value;
                voltageDefenses[defenseItemIndex].APV_Return = (bool)grid[7, rowIndex].Value;

                try
                {
                    double value = double.Parse(grid[8, rowIndex].Value.ToString());
                    if ((value < 0 || value > 256) && !enabled)
                    {
                        this.ShowToolTip("������� ����� � ��������� [0 - 256.0]", grid[8, rowIndex], this._voltageDefendTabPage);
                        ret = false;
                    }
                    else
                    {
                        voltageDefenses[defenseItemIndex].ReturnConstraint = value;
                    }

                }
                catch (Exception)
                {
                    this.ShowToolTip("������� ����� � ��������� [0 - 256.0]", grid[8, rowIndex], this._voltageDefendTabPage);
                    ret = false;
                }

                try
                {
                    ulong value = ulong.Parse(grid[9, rowIndex].Value.ToString());
                    if (value > MR730.TIMELIMIT && !enabled)
                    {
                        this.ShowToolTip(MR730.TIMELIMIT_ERROR_MSG, grid[9, rowIndex], this._voltageDefendTabPage);
                        ret = false;
                    }
                    else
                    {
                        voltageDefenses[defenseItemIndex].ReturnTime = value;
                    }

                }
                catch (Exception)
                {
                    this.ShowToolTip(MR730.TIMELIMIT_ERROR_MSG, grid[9, rowIndex], this._voltageDefendTabPage);
                    ret = false;
                }


                voltageDefenses[defenseItemIndex].UROV = (bool)grid[10, rowIndex].Value;
                voltageDefenses[defenseItemIndex].APV = (bool)grid[11, rowIndex].Value;
                voltageDefenses[defenseItemIndex].AVR = (bool)grid[12, rowIndex].Value;
            //}
            return ret;
        }

        public bool  WriteVoltageDefenses(MR730.CVoltageDefenses voltageDefenses)
        {
            bool ret = true;
            for (int i = 0; i < 4; i++)
            {
                ret &= this.WriteVoltageDefenseGridItem(this._voltageDefensesGrid1, voltageDefenses, i, i);
            }
            for (int i = 0; i < 2; i++)
            {
                ret &= this.WriteVoltageDefenseGridItem(this._voltageDefensesGrid2, voltageDefenses, i, i + 4);
            }
            for (int i = 0; i < 2; i++)
            {
                ret &= this.WriteVoltageDefenseGridItem(this._voltageDefensesGrid3, voltageDefenses, i, i + 6);
            }
            return ret;
        }

        #endregion
        
        #region ������ ���������

        void ValidateEngineDefenses()
        {
            this.ValidateMaskedBoxes(this._engineDoubleMaskedBoxes);
            this.ValidateMaskedBoxes(this._engineUlongMaskedBoxes);
        }
    

        void PrepareEngineDefenses()
        {
            this._engineDefensesGrid.Rows.Add(MR730.CEngingeDefenses.COUNT);
            this._engineDefensesGrid[0, 0].Value = "Q>";
            this._engineDefensesGrid[0, 1].Value = "Q>>";
            this._engineCombos = new ComboBox[] { this._engineQresetCombo, this._engineNpuskCombo, this._engineQmodeCombo };
            this.FillEngineCombos();
            this.SubscriptCombos(this._engineCombos);
            this._engineDefenseModeCol.Items.AddRange(Strings.Modes.ToArray());

            this._engineQresetCombo.SelectedIndex = this._engineNpuskCombo.SelectedIndex =
            this._engineQmodeCombo.SelectedIndex = 0;
            this._engineDefensesGrid.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._engineDefensesGrid_CellStateChanged);
            this._engineDefensesGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);

            this._engineUlongMaskedBoxes = new MaskedTextBox[] { this._engineHeatingTimeBox, this._engineCoolingTimeBox, this._enginePuskTimeBox,
                                                            this._engineQtimeBox,this._engineBlockHeatBox,this._engineBlockDurationBox,
                                                            this._engineBlockTimeBox,this._engineBlockPuskBox};
            this._engineDoubleMaskedBoxes = new MaskedTextBox[] { this._engineInBox, this._engineIpConstraintBox, this._engineHeatPuskConstraintBox,this._engineQconstraintBox};

            this.PrepareMaskedBoxes(this._engineUlongMaskedBoxes, typeof(ulong));
            this.PrepareMaskedBoxes(this._engineDoubleMaskedBoxes, typeof(double));

            this.ShowEngineDefenses();
        }
        private MaskedTextBox[] _engineUlongMaskedBoxes;
        private MaskedTextBox[] _engineDoubleMaskedBoxes;
        private ComboBox[] _engineCombos;
        private RadioButtonSelector _groupSelector;

        private void FillEngineCombos()
        {
            this._engineQresetCombo.Items.AddRange(Strings.Logic.ToArray());
            this._engineNpuskCombo.Items.AddRange(Strings.Logic.ToArray());
            this._engineQmodeCombo.Items.AddRange(Strings.ModesLight.ToArray());
        }

        void Grid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                DataGridViewComboBoxEditingControl combo = e.Control as DataGridViewComboBoxEditingControl;
                //combo.SelectedIndex = 0;
                combo.Items.RemoveAt(combo.Items.Count - 1);
            }
        }

        void _engineDefensesGrid_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (e.Cell.OwningColumn == this._engineDefenseModeCol)
            {
                GridManager.ChangeCellDisabling(this._engineDefensesGrid, 0, "��������", 1, 2, 3, 4, 5);
                GridManager.ChangeCellDisabling(this._engineDefensesGrid, 1, "��������", 1, 2, 3, 4, 5);
            }
        }
        
        bool WriteEngineDefenses()
        {
            bool ret = true;
            this._device.EngineHeatingTime = ushort.Parse(this._engineHeatingTimeBox.Text);
            this._device.EngineCoolingTime = ushort.Parse(this._engineCoolingTimeBox.Text);
            this._device.EngineIn = double.Parse(this._engineInBox.Text);
            this._device.EngineWorkConstraint = double.Parse(this._engineIpConstraintBox.Text);
            this._device.EnginePuskTime = ulong.Parse(this._enginePuskTimeBox.Text);
            this._device.EngineHeatPuskConstraint = double.Parse(this._engineHeatPuskConstraintBox.Text);
            this._device.EnginePuskN = this._engineNpuskCombo.SelectedItem.ToString();
            this._device.EngineResetQ = this._engineQresetCombo.SelectedItem.ToString();
            this._device.QMode = this._engineQmodeCombo.SelectedItem.ToString();
            this._device.QConstraint = double.Parse(this._engineQconstraintBox.Text);
            this._device.QTime = ushort.Parse(this._engineQtimeBox.Text);
            this._device.BlockingPuskCount = ushort.Parse(this._engineBlockPuskBox.Text);
            this._device.BlockingHeatCount = ushort.Parse(this._engineBlockHeatBox.Text);
            this._device.BlockingDuration = ushort.Parse(this._engineBlockDurationBox.Text);
            this._device.BlockingTime = ushort.Parse(this._engineBlockTimeBox.Text);


            for (int i = 0; i < MR730.CEngingeDefenses.COUNT; i++)
            {
                this._device.EngineDefenses[i].Mode = this._engineDefensesGrid[1, i].Value.ToString();
                try
                {
                    double value = double.Parse(this._engineDefensesGrid[2, i].Value.ToString());
                    if (value < 0 || value > 256)
                    {                        
                        this.ShowToolTip("������� ����� � ��������� [0 - 256.0]", this._engineDefensesGrid[2, i], this._engineDefensesPage);
                        ret = false;            
                    }
                    else
                    {
                        this._device.EngineDefenses[i].WorkConstraint = value;
                    }
                    
                }
                catch (Exception)
                {
                    this.ShowToolTip("������� ����� � ��������� [0 - 256.0]", this._engineDefensesGrid[2, i], this._engineDefensesPage);
                    ret = false;
                }
                
                this._device.EngineDefenses[i].UROV = (bool)this._engineDefensesGrid[3, i].Value;
                this._device.EngineDefenses[i].APV = (bool)this._engineDefensesGrid[4, i].Value;
                this._device.EngineDefenses[i].AVR = (bool)this._engineDefensesGrid[5, i].Value;
            }
            return ret;
        }

        private void ShowEngineDefenses()
        {
            this.ClearCombos(this._engineCombos);
            this.FillEngineCombos();
            this.SubscriptCombos(this._engineCombos);
            //��������� ��������� -->
            this._engineHeatingTimeBox.Text = this._device.EngineHeatingTime.ToString();
            this._engineCoolingTimeBox.Text = this._device.EngineCoolingTime.ToString();
            this._engineInBox.Text = this._device.EngineIn.ToString();

            this._engineIpConstraintBox.Text = this._device.EngineWorkConstraint.ToString();
            this._enginePuskTimeBox.Text = this._device.EnginePuskTime.ToString();
            this._engineHeatPuskConstraintBox.Text = this._device.EngineHeatPuskConstraint.ToString();

            this._engineNpuskCombo.SelectedItem = this._device.EnginePuskN;
            this._engineQresetCombo.SelectedItem = this._device.EngineResetQ;
            //<--

            //���������� ����� -->
            this._engineQmodeCombo.SelectedItem = this._device.QMode;
            this._engineQconstraintBox.Text = this._device.QConstraint.ToString();
            this._engineQtimeBox.Text = this._device.QTime.ToString();

            this._engineBlockPuskBox.Text = this._device.BlockingPuskCount.ToString();
            this._engineBlockHeatBox.Text = this._device.BlockingHeatCount.ToString();
            this._engineBlockDurationBox.Text = this._device.BlockingDuration.ToString();
            this._engineBlockTimeBox.Text = this._device.BlockingTime.ToString();
            //<--

            for(int i = 0; i < MR730.CEngingeDefenses.COUNT; i++)
            {
                this._engineDefensesGrid[1, i].Value = this._device.EngineDefenses[i].Mode;
                this._engineDefensesGrid[2, i].Value = this._device.EngineDefenses[i].WorkConstraint;
                this._engineDefensesGrid[3, i].Value = this._device.EngineDefenses[i].UROV;
                this._engineDefensesGrid[4, i].Value = this._device.EngineDefenses[i].APV;
                this._engineDefensesGrid[5, i].Value = this._device.EngineDefenses[i].AVR;
            }
            GridManager.ChangeCellDisabling(this._engineDefensesGrid, 0, "��������", 1, 2, 3, 4, 5);
            GridManager.ChangeCellDisabling(this._engineDefensesGrid, 1, "��������", 1, 2, 3, 4, 5);
        }
        
        private void OnEngineDefensesLoadOk()
        {
            this._exchangeProgressBar.PerformStep();
            this.ShowEngineDefenses();
        }
        
        #endregion

        #region �������� ����� �� �������

        private void FrequenceDefensesLoadFail()
        {
            this.OnLoadFail();
            this.OnLoadComplete();
        }
                
        private void FrequenceDefensesLoadOk()
        {
            this.OnFrequenceDefensesLoadOk();
            this.OnLoadComplete();
        }

        private void OnFrequenceDefensesLoadOk()
        {
            this._exchangeProgressBar.PerformStep();
            if (this._mainGroupRadioButton.Checked)
            {
                this.ShowFrequenceDefenses(this._device.FrequenceDefensesMain);    
            }
            else
            {
                this.ShowFrequenceDefenses(this._device.FrequenceDefensesReserve);
            }
            
        }

        private void PrepareFrequenceDefenses()
        {
            this._frequenceDefensesGrid.Rows.Add(4);

            this._frequenceDefensesMode.Items.AddRange(Strings.Modes.ToArray());
            this._frequenceDefensesBlockingNumber.Items.AddRange(Strings.Logic.ToArray());
            this._frequenceDefensesGrid.EditingControlShowing +=new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._frequenceDefensesGrid.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this.OnFrequenceDefenseCellStateChanged);
            this._frequenceDefensesGrid.Height = this._frequenceDefensesGrid.ColumnHeadersHeight + (this._frequenceDefensesGrid.Rows.Count + 1) * this._frequenceDefensesGrid.RowTemplate.Height;
            this.ShowFrequenceDefenses(this._device.FrequenceDefensesMain);
        }

        void OnFrequenceDefenseCellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (e.Cell.OwningColumn == this._frequenceDefensesMode || e.Cell.OwningColumn == this._frequenceDefensesReturn )
            {
                DataGridView frequenceGrid = sender as DataGridView;
                int rowIndex = e.Cell.RowIndex;

                OnFrequenceDefenseModeChanged(frequenceGrid, rowIndex);
            }
        }

        private static void OnFrequenceDefenseModeChanged(DataGridView frequenceGrid, int rowIndex)
        {
            int[] columns = new int[frequenceGrid.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }
            
            if (!GridManager.ChangeCellDisabling(frequenceGrid, rowIndex, "��������", 1, columns))
            {
                GridManager.ChangeCellDisabling(frequenceGrid, rowIndex, false, 5, 6, 7, 8);
            }
        }

        private void SetFrequenceDefenseGridItem(DataGridView grid, MR730.CFrequenceDefenses frequenceDefenses, int rowIndex, int defenseItemIndex)
        {
            grid[0, rowIndex].Value = frequenceDefenses[defenseItemIndex].Name;
            grid[1, rowIndex].Value = frequenceDefenses[defenseItemIndex].Mode;
            grid[2, rowIndex].Value = frequenceDefenses[defenseItemIndex].BlockingNumber;
            grid[3, rowIndex].Value = frequenceDefenses[defenseItemIndex].WorkConstraint;
            grid[4, rowIndex].Value = frequenceDefenses[defenseItemIndex].WorkTime;
            grid[5, rowIndex].Value = frequenceDefenses[defenseItemIndex].Return;
            grid[6, rowIndex].Value = frequenceDefenses[defenseItemIndex].APV_Return;
            grid[7, rowIndex].Value = frequenceDefenses[defenseItemIndex].ConstraintAPV;
            grid[8, rowIndex].Value = frequenceDefenses[defenseItemIndex].ReturnTime;
            grid[9, rowIndex].Value = frequenceDefenses[defenseItemIndex].UROV;
            grid[10, rowIndex].Value = frequenceDefenses[defenseItemIndex].APV;
            grid[11, rowIndex].Value = frequenceDefenses[defenseItemIndex].AVR;
        }

        private void ShowFrequenceDefenses(MR730.CFrequenceDefenses frequenceDefenses)
        {
            for (int i = 0; i < 4; i++)
            {
                this.SetFrequenceDefenseGridItem(this._frequenceDefensesGrid, frequenceDefenses, i, i);
                OnFrequenceDefenseModeChanged(this._frequenceDefensesGrid, i);
            }
        }

        private void _FrequenceDefenseMainConstraintRadio_CheckedChanged(object sender, EventArgs e)
        {
          
        }

        private bool WriteFrequenceDefenseGridItem(DataGridView grid, MR730.CFrequenceDefenses frequenceDefenses, int rowIndex, int defenseItemIndex)
        {
            bool ret = true;
            bool rowEnabled = false;
            frequenceDefenses[defenseItemIndex].Mode = grid[1, rowIndex].Value.ToString();
            rowEnabled = ("��������" == frequenceDefenses[defenseItemIndex].Mode);
            //if (rowEnabled)
            //{
                frequenceDefenses[defenseItemIndex].BlockingNumber = grid[2, rowIndex].Value.ToString();
                try
                {

                    double value = double.Parse(grid[3, rowIndex].Value.ToString());

                    if ((value < 47 || value > 52) && !rowEnabled)
                    {
                        this.ShowToolTip("������� ����� � ��������� [47 - 52]", grid[3, rowIndex], this._frequencyDefendTabPage);
                        ret = false;
                    }
                    else
                    {
                        frequenceDefenses[defenseItemIndex].WorkConstraint = value;
                    }

                }
                catch (Exception)
                {
                    this.ShowToolTip("������� ����� � ��������� [47 - 52]", grid[3, rowIndex], this._frequencyDefendTabPage);
                    ret = false;
                }
                try
                {
                    ulong value;
                    if (grid[4, rowIndex].Value == null) { value = 0; } else { value = ulong.Parse(grid[4, rowIndex].Value.ToString()); }
                    if (value > MR730.TIMELIMIT && !rowEnabled)
                    {
                        throw new OverflowException(MR730.TIMELIMIT_ERROR_MSG);
                    }
                    else
                    {
                        frequenceDefenses[defenseItemIndex].WorkTime = value;
                    }

                }
                catch (Exception)
                {
                    this.ShowToolTip(MR730.TIMELIMIT_ERROR_MSG, grid[4, rowIndex], this._frequencyDefendTabPage);
                    ret = false;
                }
                frequenceDefenses[defenseItemIndex].Return = (bool)grid[5, rowIndex].Value;
                frequenceDefenses[defenseItemIndex].APV_Return = (bool)grid[6, rowIndex].Value;
                try
                {
                    if (frequenceDefenses[defenseItemIndex].Return)
                    {
                        double value = double.Parse(grid[7, rowIndex].Value.ToString());

                        if ((value < 47 || value > 52) && !rowEnabled)
                        {
                            this.ShowToolTip("������� ����� � ��������� [47 - 52]", grid[7, rowIndex], this._frequencyDefendTabPage);
                            ret = false;
                        }
                        else
                        {
                            frequenceDefenses[defenseItemIndex].ConstraintAPV = value;
                        }
                    }

                }
                catch (Exception)
                {
                    this.ShowToolTip("������� ����� � ��������� [47 - 52]", grid[7, rowIndex], this._frequencyDefendTabPage);
                    ret = false;
                }
                try
                {
                    ulong value = ulong.Parse(grid[8, rowIndex].Value.ToString());
                    if (value > MR730.TIMELIMIT && !rowEnabled)
                    {
                        throw new OverflowException(MR730.TIMELIMIT_ERROR_MSG);
                    }
                    else
                    {
                        frequenceDefenses[defenseItemIndex].ReturnTime = value;
                    }
                }
                catch (Exception)
                {
                    this.ShowToolTip(MR730.TIMELIMIT_ERROR_MSG, grid[8, rowIndex], this._frequencyDefendTabPage);
                    ret = false;
                }
            frequenceDefenses[defenseItemIndex].UROV = (bool)grid[9, rowIndex].Value;
                frequenceDefenses[defenseItemIndex].APV = (bool)grid[10, rowIndex].Value;
                frequenceDefenses[defenseItemIndex].AVR = (bool)grid[11, rowIndex].Value;
            //}
            return ret;
        }

        public bool WriteFrequenceDefenses(MR730.CFrequenceDefenses frequenceDefenses)
        {
            bool ret = true;
            for (int i = 0; i < 4; i++)
            {
                ret &= this.WriteFrequenceDefenseGridItem(this._frequenceDefensesGrid, frequenceDefenses, i, i);
            }

            return ret;
        }

        #endregion
        
        private void _oscKonfComb_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._device.OscilloscopeKonfCount = this._oscKonfComb.SelectedIndex;
        }
        
        private void _tokDefenseGrid1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this._tokDefenseGrid1.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
            this._tokDefenseGrid1.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "";
        }

        private void _tokDefenseGrid2_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this._tokDefenseGrid2.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
            this._tokDefenseGrid2.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "";
        }

        private void _tokDefenseGrid3_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this._tokDefenseGrid3.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
            this._tokDefenseGrid3.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "";
        }

        private void _tokDefenseGrid4_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this._tokDefenseGrid4.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
            this._tokDefenseGrid4.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "";
        }

        private void _voltageDefensesGrid1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this._voltageDefensesGrid1.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
            this._voltageDefensesGrid1.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "";
        }

        private void _voltageDefensesGrid2_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this._voltageDefensesGrid2.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
            this._voltageDefensesGrid2.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "";
        }

        private void _voltageDefensesGrid3_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this._voltageDefensesGrid3.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
            this._voltageDefensesGrid3.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "";
        }

        private void _frequenceDefensesGrid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this._frequenceDefensesGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
            this._frequenceDefensesGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "";
        }

        private void _externalDefenseGrid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this._externalDefenseGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
            this._externalDefenseGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "";
        }

        private void _engineDefensesGrid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this._tokDefenseGrid1.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
            this._tokDefenseGrid1.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "";
        }


        private void _mainGroupRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (this._mainGroupRadioButton.Checked)
            {
                this.WriteTokDefenses(this._device.TokDefensesReserve);
                this.ShowTokDefenses(this._device.TokDefensesMain);

                this.WriteVoltageDefenses(this._device.VoltageDefensesReserve);
                this.ShowVoltageDefenses(this._device.VoltageDefensesMain);

                this.WriteFrequenceDefenses(this._device.FrequenceDefensesReserve);
                this.ShowFrequenceDefenses(this._device.FrequenceDefensesMain);
            }
            else
            {
                this.WriteTokDefenses(this._device.TokDefensesMain);
                this.ShowTokDefenses(this._device.TokDefensesReserve);

                this.WriteVoltageDefenses(this._device.VoltageDefensesMain);
                this.ShowVoltageDefenses(this._device.VoltageDefensesReserve);

                this.WriteFrequenceDefenses(this._device.FrequenceDefensesMain);
                this.ShowFrequenceDefenses(this._device.FrequenceDefensesReserve);
            }
        }

        private void avr_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = sender as CheckBox;
            if (cb == null) return;
            int ind = Convert.ToInt32(cb.Tag);
            switch (ind)
            {
                case 1:
                    this._device.AVR_Supply_Off = cb.Checked;
                    break;
                case 2:
                    this._device.AVR_Switch_Off = cb.Checked;
                    break;
                case 3:
                    this._device.AVR_Self_Off = cb.Checked;
                    break;
                case 4:
                    this._device.AVR_Abrasion_Switch = cb.Checked;
                    break;
                case 5:
                    this._device.AVR_Reset_Switch = cb.Checked;
                    break;
            }
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this._writeConfigBut.Focus();
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.resetSetPointStruct)
            {
                this.ResetSetpoints();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
            }
            if(e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtmlFile();
            }
        }

        private void Mr730ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        #region [����� �������]

        private void _resetSetpointsButton_Click(object sender, EventArgs e)
        {
            this.ResetSetpoints();
        }

        private void ResetSetpoints()
        {
            DialogResult res = MessageBox.Show(@"�������� �������?", @"�������",
            MessageBoxButtons.YesNo,
            MessageBoxIcon.Question);

            if (res == DialogResult.No) return;

            this.ResetInputSignals();
            this.ResetOutputsignals();
            this.ResetExtDef();
            this.ResetAutomat();
            this.ResetEngine();
            this.ResetCorners();
            this.ResetTokDef();
            this.ResetVoltDef();
            this.ResetFreqDef();
        }

        private void ResetInputSignals()
        {
            this._TT_Box.Text = 0.ToString();
            this._TTNP_Box.Text = 0.ToString();
            this._maxTok_Box.Text = 0.ToString();

            this._TN_Box.Text = 0.ToString();
            this._TNNP_Box.Text = 0.ToString();
            this._TN_dispepairCombo.SelectedIndex = 0;
            this._TNNP_dispepairCombo.SelectedIndex = 0;
            this._TN_typeCombo.SelectedIndex = 0;

            this._keyOffCombo.SelectedIndex = 0;
            this._keyOnCombo.SelectedIndex = 0;
            this._extOffCombo.SelectedIndex = 0;
            this._extOnCombo.SelectedIndex = 0;
            this._signalizationCombo.SelectedIndex = 0;
            this._constraintGroupCombo.SelectedIndex = 0;

            this._switcherStateOffCombo.SelectedIndex = 0;
            this._switcherStateOnCombo.SelectedIndex = 0;
            this._switcherErrorCombo.SelectedIndex = 0;
            this._switcherBlockCombo.SelectedIndex = 0;
            this._switcherTimeBox.Text = 0.ToString();
            this._switcherTokBox.Text = 0.ToString();
            this._switcherImpulseBox.Text = 0.ToString();
            this._switcherDurationBox.Text = 0.ToString();

            this._manageSignalsButtonCombo.SelectedIndex = 0;
            this._manageSignalsKeyCombo.SelectedIndex = 0;
            this._manageSignalsExternalCombo.SelectedIndex = 0;
            this._manageSignalsSDTU_Combo.SelectedIndex = 0;

            this._oscKonfComb.SelectedIndex = 0;

            //����� ��
            for (int i = 0; i < this._logicChannelsCombo.Items.Count; i++)
            {
                this._device.SetInputLogicSignals(i, new LogicState[16]);
            }
            this._logicSignalsDataGrid.Rows.Clear();
            for (int i = 0; i < 16; i++)
            {
                this._logicSignalsDataGrid.Rows.Add(new object[] { i + 1, Strings.LogycValues[0] });
            }
            this._logicChannelsCombo.SelectedIndex = 0;

            //����� ��������������
            this._releDispepairBox.Text = 0.ToString();


            for (int i = 0; i < this._device.DisrepairSignal.Count; i++)
            {
                this._dispepairCheckList.SetItemChecked(i, false);
            }
        }

        private void ResetOutputsignals()
        {
            //����� �������� ����
            this._outputReleGrid.Rows.Clear();
            for (int i = 0; i < MR730.COutputRele.COUNT; i++)
            {
                this._outputReleGrid.Rows.Add(new object[] { i + 1, "�����������", "���", 0 });
            }

            //����� �����������
            this._outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < MR730.COutputIndicator.COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(new object[]{i + 1,
                                                            "�����������",
                                                            "���",
                                                            false,
                                                            false,
                                                            false});
            }

            //����� ���
            for (int i = 0; i < this._outputLogicCombo.Items.Count; i++)
            {
                this._device.SetOutputLogicSignals(i, new BitArray(104));
            }
            this._outputLogicCheckList.ClearSelected();
            this._outputLogicCombo.SelectedIndex = 0;

            this._outputLogicCheckList.Items.Clear();
            if (this._outputLogicCheckList.Items.Count == 0)
            {
                this._outputLogicCheckList.Items.AddRange(Strings.OutputSignals.ToArray());
            }
        }

        private void ResetExtDef()
        {
            this._externalDefenseGrid.Rows.Clear();
            for (int i = 0; i < MR730.CExternalDefenses.COUNT; i++)
            {
                this._externalDefenseGrid.Rows.Add(new object[]{i + 1,
                                                           "��������",
                                                           "���",
                                                           "���",
                                                           0,
                                                           false,
                                                           false,
                                                           "���",
                                                           0,
                                                           false,
                                                           false,
                                                           false});
                this.OnExternalDefenseGridModeChanged(i);
                GridManager.ChangeCellDisabling(this._externalDefenseGrid, i, false, 5, new int[] { 6, 7, 8 });

            }
            this._externalDefenseGrid.Height = this._externalDefenseGrid.ColumnHeadersHeight + this._externalDefenseGrid.Rows.Count * this._externalDefenseGrid.RowTemplate.Height;
        }

        private void ResetAutomat()
        {
            this.apv_conf.SelectedIndex = 0;
            this.apv_blocking.SelectedIndex = 0;
            this.apv_time_blocking.Text = 0.ToString();
            this.apv_time_ready.Text = 0.ToString();
            this.apv_time_1krat.Text = 0.ToString();
            this.apv_time_2krat.Text = 0.ToString();
            this.apv_time_3krat.Text = 0.ToString();
            this.apv_time_4krat.Text = 0.ToString();
            this.apv_self_off.SelectedIndex = 0;

            this.lzsh_conf.SelectedIndex = 0;
            this.lzsh_constraint.Text = 0.ToString();

            this.avr_supply_off.Checked = false;
            this.avr_switch_off.Checked = false;
            this.avr_self_off.Checked = false;
            this.avr_abrasion_switch.Checked = false;

            this.avr_permit_reset_switch.Checked = false;

            this.avr_start.SelectedIndex = 0;
            this.avr_blocking.SelectedIndex = 0;
            this.avr_reset_blocking.SelectedIndex = 0;
            this.avr_abrasion.SelectedIndex = 0;
            this.avr_time_abrasion.Text = 0.ToString();
            this.avr_return.SelectedIndex = 0;
            this.avr_time_return.Text = 0.ToString();
            this.avr_disconnection.Text = 0.ToString();

        }

        private void ResetEngine()
        {
            this._engineHeatingTimeBox.Text = 0.ToString();
            this._engineCoolingTimeBox.Text = 0.ToString();
            this._engineInBox.Text = 0.ToString();
            this._engineQresetCombo.SelectedIndex = 0;
            this._engineIpConstraintBox.Text = 0.ToString();
            this._enginePuskTimeBox.Text = 0.ToString();
            this._engineHeatPuskConstraintBox.Text = 0.ToString();
            this._engineNpuskCombo.SelectedIndex = 0;

            this._engineQmodeCombo.SelectedIndex = 0;
            this._engineQconstraintBox.Text = 0.ToString();
            this._engineQtimeBox.Text = 0.ToString();
            this._engineBlockPuskBox.Text = 0.ToString();
            this._engineBlockHeatBox.Text = 0.ToString();
            this._engineBlockDurationBox.Text = 0.ToString();
            this._engineBlockTimeBox.Text = 0.ToString();
            for (int i = 0; i < MR730.CEngingeDefenses.COUNT; i++)
            {
                this._engineDefensesGrid[1, i].Value = "��������";
                this._engineDefensesGrid[2, i].Value = 0;
                this._engineDefensesGrid[3, i].Value = false;
                this._engineDefensesGrid[4, i].Value = false;
                this._engineDefensesGrid[5, i].Value = false;
            }
            GridManager.ChangeCellDisabling(this._engineDefensesGrid, 0, "��������", 1, 2, 3, 4, 5);
            GridManager.ChangeCellDisabling(this._engineDefensesGrid, 1, "��������", 1, 2, 3, 4, 5);
        }

        private void ResetCorners()
        {
            this._tokDefenseIbox.Text = 0.ToString();
            this._tokDefenseInbox.Text = 0.ToString();
            this._tokDefenseI0box.Text = 0.ToString();
            this._tokDefenseI2box.Text = 0.ToString();

            this._device.TokDefensesMain = new BEMN.MR730.MR730.CTokDefenses();

            this._tokDefenseIbox.Text = this._device.TokDefensesMain.I.ToString();
            this._tokDefenseInbox.Text = this._device.TokDefensesMain.In.ToString();
            this._tokDefenseI0box.Text = this._device.TokDefensesMain.I0.ToString();
            this._tokDefenseI2box.Text = this._device.TokDefensesMain.I2.ToString();

            this._device.TokDefensesReserve = new BEMN.MR730.MR730.CTokDefenses();
        }

        private void ResetTokDef()
        {
            this._device.TokDefensesMain = new BEMN.MR730.MR730.CTokDefenses();

            this._tokDefenseGrid1.Rows.Clear();
            this._tokDefenseGrid2.Rows.Clear();
            this._tokDefenseGrid3.Rows.Clear();
            this._tokDefenseGrid4.Rows.Clear();

            for (int i = 0; i < 2; i++)
            {
                if (this._device.TokDefensesMain[i].Feature.ToString() == "���������")
                {
                    if (this._device.TokDefensesMain[i].WorkConstraint <= 24)
                    {
                        this._tokDefenseGrid1.Rows.Add(new object[]{this._device.TokDefensesMain[i].Name,
                                                       this._device.TokDefensesMain[i].Mode,
                                                       this._device.TokDefensesMain[i].BlockingNumber,
                                                       this._device.TokDefensesMain[i].PuskU,
                                                       this._device.TokDefensesMain[i].PuskU_Constraint,
                                                       this._device.TokDefensesMain[i].Direction,
                                                       this._device.TokDefensesMain[i].BlockingExist,
                                                       this._device.TokDefensesMain[i].Parameter,
                                                       this._device.TokDefensesMain[i].WorkConstraint,
                                                       this._device.TokDefensesMain[i].Feature,
                                                       this._device.TokDefensesMain[i].WorkTime/10,
                                                       this._device.TokDefensesMain[i].Speedup,
                                                       this._device.TokDefensesMain[i].SpeedupTime,
                                                       this._device.TokDefensesMain[i].UROV,
                                                       this._device.TokDefensesMain[i].APV,
                                                       this._device.TokDefensesMain[i].AVR});
                        this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);
                    }
                    else
                    {
                        this._tokDefenseGrid1.Rows.Add(new object[]{this._device.TokDefensesMain[i].Name,
                                                       this._device.TokDefensesMain[i].Mode,
                                                       this._device.TokDefensesMain[i].BlockingNumber,
                                                       this._device.TokDefensesMain[i].PuskU,
                                                       this._device.TokDefensesMain[i].PuskU_Constraint,
                                                       this._device.TokDefensesMain[i].Direction,
                                                       this._device.TokDefensesMain[i].BlockingExist,
                                                       this._device.TokDefensesMain[i].Parameter,
                                                       this._device.TokDefensesMain[i].WorkConstraint - 0.01,
                                                       this._device.TokDefensesMain[i].Feature,
                                                       this._device.TokDefensesMain[i].WorkTime/10,
                                                       this._device.TokDefensesMain[i].Speedup,
                                                       this._device.TokDefensesMain[i].SpeedupTime,
                                                       this._device.TokDefensesMain[i].UROV,
                                                       this._device.TokDefensesMain[i].APV,
                                                       this._device.TokDefensesMain[i].AVR});
                        this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);
                    }
                }
                else
                {
                    if (this._device.TokDefensesMain[i].WorkConstraint <= 24)
                    {
                        this._tokDefenseGrid1.Rows.Add(new object[]{this._device.TokDefensesMain[i].Name,
                                                       this._device.TokDefensesMain[i].Mode,
                                                       this._device.TokDefensesMain[i].BlockingNumber,
                                                       this._device.TokDefensesMain[i].PuskU,
                                                       this._device.TokDefensesMain[i].PuskU_Constraint,
                                                       this._device.TokDefensesMain[i].Direction,
                                                       this._device.TokDefensesMain[i].BlockingExist,
                                                       this._device.TokDefensesMain[i].Parameter,
                                                       this._device.TokDefensesMain[i].WorkConstraint,
                                                       this._device.TokDefensesMain[i].Feature,
                                                       this._device.TokDefensesMain[i].WorkTime,
                                                       this._device.TokDefensesMain[i].Speedup,
                                                       this._device.TokDefensesMain[i].SpeedupTime,
                                                       this._device.TokDefensesMain[i].UROV,
                                                       this._device.TokDefensesMain[i].APV,
                                                       this._device.TokDefensesMain[i].AVR});
                        this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);
                    }
                    else
                    {
                        this._tokDefenseGrid1.Rows.Add(new object[]{this._device.TokDefensesMain[i].Name,
                                                       this._device.TokDefensesMain[i].Mode,
                                                       this._device.TokDefensesMain[i].BlockingNumber,
                                                       this._device.TokDefensesMain[i].PuskU,
                                                       this._device.TokDefensesMain[i].PuskU_Constraint,
                                                       this._device.TokDefensesMain[i].Direction,
                                                       this._device.TokDefensesMain[i].BlockingExist,
                                                       this._device.TokDefensesMain[i].Parameter,
                                                       this._device.TokDefensesMain[i].WorkConstraint - 0.01,
                                                       this._device.TokDefensesMain[i].Feature,
                                                       this._device.TokDefensesMain[i].WorkTime,
                                                       this._device.TokDefensesMain[i].Speedup,
                                                       this._device.TokDefensesMain[i].SpeedupTime,
                                                       this._device.TokDefensesMain[i].UROV,
                                                       this._device.TokDefensesMain[i].APV,
                                                       this._device.TokDefensesMain[i].AVR});
                        this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);
                    }
                }
            }

            for (int i = 2; i < 4; i++)
            {
                if (this._device.TokDefensesMain[i].Feature.ToString() == "���������")
                {
                    if (this._device.TokDefensesMain[i].WorkConstraint <= 24)
                    {
                        this._tokDefenseGrid2.Rows.Add(new object[]{ this._device.TokDefensesMain[i].Name,
                                                        this._device.TokDefensesMain[i].Mode,
                                                        this._device.TokDefensesMain[i].BlockingNumber,
                                                        this._device.TokDefensesMain[i].PuskU,
                                                        this._device.TokDefensesMain[i].PuskU_Constraint,
                                                        this._device.TokDefensesMain[i].Direction,
                                                        this._device.TokDefensesMain[i].BlockingExist,
                                                        this._device.TokDefensesMain[i].Parameter,
                                                        this._device.TokDefensesMain[i].WorkConstraint,
                                                        this._device.TokDefensesMain[i].Feature,
                                                        this._device.TokDefensesMain[i].WorkTime/10,
                                                        this._device.TokDefensesMain[i].Engine,
                                                        this._device.TokDefensesMain[i].UROV,
                                                        this._device.TokDefensesMain[i].APV,
                                                        this._device.TokDefensesMain[i].AVR});
                        this.ChangeTokDefenseCellDisabling2(this._tokDefenseGrid2, i - 2);
                    }
                    else
                    {
                        this._tokDefenseGrid2.Rows.Add(new object[]{ this._device.TokDefensesMain[i].Name,
                                                        this._device.TokDefensesMain[i].Mode,
                                                        this._device.TokDefensesMain[i].BlockingNumber,
                                                        this._device.TokDefensesMain[i].PuskU,
                                                        this._device.TokDefensesMain[i].PuskU_Constraint,
                                                        this._device.TokDefensesMain[i].Direction,
                                                        this._device.TokDefensesMain[i].BlockingExist,
                                                        this._device.TokDefensesMain[i].Parameter,
                                                        this._device.TokDefensesMain[i].WorkConstraint-0.01,
                                                        this._device.TokDefensesMain[i].Feature,
                                                        this._device.TokDefensesMain[i].WorkTime/10,
                                                        this._device.TokDefensesMain[i].Engine,
                                                        this._device.TokDefensesMain[i].UROV,
                                                        this._device.TokDefensesMain[i].APV,
                                                        this._device.TokDefensesMain[i].AVR});
                        this.ChangeTokDefenseCellDisabling2(this._tokDefenseGrid2, i - 2);
                    }
                }
                else
                {
                    if (this._device.TokDefensesMain[i].WorkConstraint <= 24)
                    {
                        this._tokDefenseGrid2.Rows.Add(new object[]{ this._device.TokDefensesMain[i].Name,
                                                        this._device.TokDefensesMain[i].Mode,
                                                        this._device.TokDefensesMain[i].BlockingNumber,
                                                        this._device.TokDefensesMain[i].PuskU,
                                                        this._device.TokDefensesMain[i].PuskU_Constraint,
                                                        this._device.TokDefensesMain[i].Direction,
                                                        this._device.TokDefensesMain[i].BlockingExist,
                                                        this._device.TokDefensesMain[i].Parameter,
                                                        this._device.TokDefensesMain[i].WorkConstraint,
                                                        this._device.TokDefensesMain[i].Feature,
                                                        this._device.TokDefensesMain[i].WorkTime,
                                                        this._device.TokDefensesMain[i].Engine,
                                                        this._device.TokDefensesMain[i].UROV,
                                                        this._device.TokDefensesMain[i].APV,
                                                        this._device.TokDefensesMain[i].AVR});
                        this.ChangeTokDefenseCellDisabling2(this._tokDefenseGrid2, i - 2);
                    }
                    else
                    {
                        this._tokDefenseGrid2.Rows.Add(new object[]{ this._device.TokDefensesMain[i].Name,
                                                        this._device.TokDefensesMain[i].Mode,
                                                        this._device.TokDefensesMain[i].BlockingNumber,
                                                        this._device.TokDefensesMain[i].PuskU,
                                                        this._device.TokDefensesMain[i].PuskU_Constraint,
                                                        this._device.TokDefensesMain[i].Direction,
                                                        this._device.TokDefensesMain[i].BlockingExist,
                                                        this._device.TokDefensesMain[i].Parameter,
                                                        this._device.TokDefensesMain[i].WorkConstraint-0.01,
                                                        this._device.TokDefensesMain[i].Feature,
                                                        this._device.TokDefensesMain[i].WorkTime,
                                                        this._device.TokDefensesMain[i].Engine,
                                                        this._device.TokDefensesMain[i].UROV,
                                                        this._device.TokDefensesMain[i].APV,
                                                        this._device.TokDefensesMain[i].AVR});
                        this.ChangeTokDefenseCellDisabling2(this._tokDefenseGrid2, i - 2);
                    }

                }
            }
            for (int i = 4; i < 10; i++)
            {
                if (this._device.TokDefensesMain[i].WorkConstraint <= 24)
                {
                    this._tokDefenseGrid3.Rows.Add(new object[]{ this._device.TokDefensesMain[i].Name,
                                                        this._device.TokDefensesMain[i].Mode,
                                                        this._device.TokDefensesMain[i].BlockingNumber,
                                                        this._device.TokDefensesMain[i].PuskU,
                                                        this._device.TokDefensesMain[i].PuskU_Constraint,
                                                        this._device.TokDefensesMain[i].Direction,
                                                        this._device.TokDefensesMain[i].BlockingExist,
                                                        this._device.TokDefensesMain[i].Parameter,
                                                        this._device.TokDefensesMain[i].WorkConstraint,
                                                        this._device.TokDefensesMain[i].Feature,
                                                        this._device.TokDefensesMain[i].WorkTime,
                                                        this._device.TokDefensesMain[i].Speedup,
                                                        this._device.TokDefensesMain[i].SpeedupTime,
                                                        this._device.TokDefensesMain[i].UROV,
                                                        this._device.TokDefensesMain[i].APV,
                                                        this._device.TokDefensesMain[i].AVR});
                    this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid3, i - 4);
                }
                else
                {
                    this._tokDefenseGrid3.Rows.Add(new object[]{ this._device.TokDefensesMain[i].Name,
                                                        this._device.TokDefensesMain[i].Mode,
                                                        this._device.TokDefensesMain[i].BlockingNumber,
                                                        this._device.TokDefensesMain[i].PuskU,
                                                        this._device.TokDefensesMain[i].PuskU_Constraint,
                                                        this._device.TokDefensesMain[i].Direction,
                                                        this._device.TokDefensesMain[i].BlockingExist,
                                                        this._device.TokDefensesMain[i].Parameter,
                                                        this._device.TokDefensesMain[i].WorkConstraint-0.01,
                                                        this._device.TokDefensesMain[i].Feature,
                                                        this._device.TokDefensesMain[i].WorkTime,
                                                        this._device.TokDefensesMain[i].Speedup,
                                                        this._device.TokDefensesMain[i].SpeedupTime,
                                                        this._device.TokDefensesMain[i].UROV,
                                                        this._device.TokDefensesMain[i].APV,
                                                        this._device.TokDefensesMain[i].AVR});
                    this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid3, i - 4);
                }


            }

            const int Ig_index = 10;
            this._tokDefenseGrid4.Rows.Add(new object[]{  this._device.TokDefensesMain[Ig_index].Name,
                                                        this._device.TokDefensesMain[Ig_index].Mode,
                                                        this._device.TokDefensesMain[Ig_index].BlockingNumber,
                                                        this._device.TokDefensesMain[Ig_index].PuskU,
                                                        this._device.TokDefensesMain[Ig_index].PuskU_Constraint,
                                                        this._device.TokDefensesMain[Ig_index].WorkConstraint,
                                                        this._device.TokDefensesMain[Ig_index].WorkTime,
                                                        this._device.TokDefensesMain[Ig_index].Speedup,
                                                        this._device.TokDefensesMain[Ig_index].SpeedupTime,
                                                        this._device.TokDefensesMain[Ig_index].UROV,
                                                        this._device.TokDefensesMain[Ig_index].APV,
                                                        this._device.TokDefensesMain[Ig_index].AVR

            });
            this.ChangeTokDefenseCellDisabling3(this._tokDefenseGrid4, 0);
            const int I12_index = 11;
            this._tokDefenseGrid4.Rows.Add(new object[]{  this._device.TokDefensesMain[I12_index].Name,
                                                        this._device.TokDefensesMain[I12_index].Mode,
                                                        this._device.TokDefensesMain[I12_index].BlockingNumber,
                                                       false,
                                                       0.0,
                                                        this._device.TokDefensesMain[I12_index].WorkConstraint,
                                                        this._device.TokDefensesMain[I12_index].WorkTime,
                                                       false,
                                                       0.0,
                                                        this._device.TokDefensesMain[I12_index].UROV,
                                                        this._device.TokDefensesMain[I12_index].APV,
                                                        this._device.TokDefensesMain[I12_index].AVR  });
            this.ChangeTokDefenseCellDisabling3(this._tokDefenseGrid4, 1);

            this._tokDefenseGrid4["_tokDefense4UpuskCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4UpuskCol", 1].Style.BackColor = SystemColors.Control;
            this._tokDefenseGrid4["_tokDefense4PuskConstraintCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4PuskConstraintCol", 1].Style.BackColor = SystemColors.Control;
            this._tokDefenseGrid4["_tokDefense4SpeedupCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4SpeedupCol", 1].Style.BackColor = SystemColors.Control;
            this._tokDefenseGrid4["_tokDefense4SpeedupTimeCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4SpeedupTimeCol", 1].Style.BackColor = SystemColors.Control;

            for (int i = 0; i < this._tokDefenseGrid1.Rows.Count; i++)
            {
                this._tokDefenseGrid1[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            for (int i = 0; i < this._tokDefenseGrid2.Rows.Count; i++)
            {
                this._tokDefenseGrid2[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            for (int i = 0; i < this._tokDefenseGrid3.Rows.Count; i++)
            {
                this._tokDefenseGrid3[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            for (int i = 0; i < this._tokDefenseGrid4.Rows.Count; i++)
            {
                this._tokDefenseGrid4[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }

            this._device.TokDefensesReserve = new BEMN.MR730.MR730.CTokDefenses();

        }

        private void ResetVoltDef()
        {
            this._device.VoltageDefensesMain = new BEMN.MR730.MR730.CVoltageDefenses();

            this._voltageDefensesGrid1.Rows.Clear();
            this._voltageDefensesGrid2.Rows.Clear();
            this._voltageDefensesGrid3.Rows.Clear();

            this._voltageDefensesGrid1.Rows.Add(4);
            this._voltageDefensesGrid1[0, 0].Value = "U>";
            this._voltageDefensesGrid1[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid1[0, 1].Value = "U>>";
            this._voltageDefensesGrid1[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid1[0, 2].Value = "U<";
            this._voltageDefensesGrid1[0, 2].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid1[0, 3].Value = "U<<";
            this._voltageDefensesGrid1[0, 3].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid2.Rows.Add(2);
            this._voltageDefensesGrid2[0, 0].Value = "U2>";
            this._voltageDefensesGrid2[0, 1].Value = "U2>>";
            this._voltageDefensesGrid2[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid2[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid3.Rows.Add(2);
            this._voltageDefensesGrid3[0, 0].Value = "U0>";
            this._voltageDefensesGrid3[0, 1].Value = "U0>>";
            this._voltageDefensesGrid3[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid3[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;

            for (int i = 0; i < 4; i++)
            {
                this.SetVoltageDefenseGridItem(this._voltageDefensesGrid1, this._device.VoltageDefensesMain, i, i);
                OnVoltageDefenseModeChange(this._voltageDefensesGrid1, i);
            }
            for (int i = 0; i < 2; i++)
            {
                this.SetVoltageDefenseGridItem(this._voltageDefensesGrid2, this._device.VoltageDefensesMain, i, i + 4);
                OnVoltageDefenseModeChange(this._voltageDefensesGrid2, i);
            }
            for (int i = 0; i < 2; i++)
            {
                this.SetVoltageDefenseGridItem(this._voltageDefensesGrid3, this._device.VoltageDefensesMain, i, i + 6);
                OnVoltageDefenseModeChange(this._voltageDefensesGrid3, i);
            }

            this._device.VoltageDefensesReserve = new BEMN.MR730.MR730.CVoltageDefenses();
        }

        private void ResetFreqDef()
        {
            this._device.FrequenceDefensesMain = new BEMN.MR730.MR730.CFrequenceDefenses();

            this._frequenceDefensesGrid.Rows.Clear();

            this._frequenceDefensesGrid.Rows.Add(4);

            for (int i = 0; i < 4; i++)
            {
                this.SetFrequenceDefenseGridItem(this._frequenceDefensesGrid, this._device.FrequenceDefensesMain, i, i);
                OnFrequenceDefenseModeChanged(this._frequenceDefensesGrid, i);
            }

            this._device.FrequenceDefensesReserve = new BEMN.MR730.MR730.CFrequenceDefenses();

        }
        #endregion

        
    }
}
