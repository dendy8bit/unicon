using System;

namespace BEMN.MR730
{
    public class Measuring
    {
        private static bool _setConsGA = false;

        public static bool SetConsGA
        {
            get { return _setConsGA; }
            set { _setConsGA = value; }
        }

        public static double GetI(ushort value, ushort KoeffNPTT, bool phaseTok)
        {
            int b;
            b = phaseTok ? 40 : 5;
            return ((double) b*(double) value/0x10000*KoeffNPTT);
        }

        public static double GetP(ushort value, ushort KoeffTTTN, bool phaseTok)
        {
            int b;
            b = phaseTok ? 40 : 5;
            return ((double)b * (double)value / 0x10000);
        }

        public static double GetU(ushort value, double Koeff)
        {
            return (double) (value / (double) 0x100 * Koeff);
        }

        public static double GetF(ushort value)
        {
            return (double) value/(double) 0x100;
        }

        public static ulong GetTime(ushort value)
        {
            return value < 32768 ? (ulong) value*10 : ((ulong) value - 32768)*100;
        }

        public static double GetConstraint(ushort value, ConstraintKoefficient koeff)
        {
            double ret = 0.0f;
            if (_setConsGA)
            {
                double temp = (double)value * (int)koeff / 65535;
                return Math.Round(temp, 7) / 100;
            }
            else 
            {
                double temp = (double)value * (int)koeff / 65535;
                ret = Math.Round(temp, 0, MidpointRounding.ToEven) / 100;
            }
            return ret;
        }

        public static double GetConstraintOnly(ushort value, ConstraintKoefficient koeff)
        {
            double temp = ((double)value * (int)koeff / 65536)/100;
            return Math.Round(temp, 2);
            //return Math.Floor(temp + 0.5) / 100;
        }

        public static ushort SetConstraint(double value, ConstraintKoefficient koeff)
        {
            ushort t = (ushort) value;
            return (ushort)(value * 65535 * 100 / (int)koeff);
        }

        public static ushort SetTime(ulong value)
        {
            return value < 327680 ? (ushort) (value/10) : (ushort) (value/100 + 32768);
        }
    }
}