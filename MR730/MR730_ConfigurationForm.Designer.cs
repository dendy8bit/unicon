namespace BEMN.MR730
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            this._tabControl = new System.Windows.Forms.TabControl();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetSetPointStruct = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this._inSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this._oscKonfComb = new System.Windows.Forms.ComboBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._manageSignalsSDTU_Combo = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this._manageSignalsExternalCombo = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this._manageSignalsKeyCombo = new System.Windows.Forms.ComboBox();
            this.label45 = new System.Windows.Forms.Label();
            this._manageSignalsButtonCombo = new System.Windows.Forms.ComboBox();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._dispepairCheckList = new System.Windows.Forms.CheckedListBox();
            this._releDispepairBox = new System.Windows.Forms.MaskedTextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._applyLogicSignalsBut = new System.Windows.Forms.Button();
            this._logicChannelsCombo = new System.Windows.Forms.ComboBox();
            this._logicSignalsDataGrid = new System.Windows.Forms.DataGridView();
            this._diskretChannelCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._diskretValueCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this._switcherDurationBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherTokBox = new System.Windows.Forms.MaskedTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this._switcherTimeBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherImpulseBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherBlockCombo = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this._switcherErrorCombo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this._switcherStateOnCombo = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this._switcherStateOffCombo = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._constraintGroupCombo = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this._signalizationCombo = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this._extOffCombo = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this._extOnCombo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this._keyOffCombo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this._keyOnCombo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._TN_typeCombo = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this._TNNP_dispepairCombo = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this._TN_dispepairCombo = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._TNNP_Box = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this._TN_Box = new System.Windows.Forms.MaskedTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this._maxTok_Box = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._TTNP_Box = new System.Windows.Forms.MaskedTextBox();
            this._TT_Box = new System.Windows.Forms.MaskedTextBox();
            this._outputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._outputLogicCheckList = new System.Windows.Forms.CheckedListBox();
            this._outputLogicAcceptBut = new System.Windows.Forms.Button();
            this._outputLogicCombo = new System.Windows.Forms.ComboBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._outputIndicatorsGrid = new System.Windows.Forms.DataGridView();
            this._outIndNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outIndTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndResetCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndAlarmCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndSystemCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releImpulseCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDefensePage = new System.Windows.Forms.TabPage();
            this._externalDefenseGrid = new System.Windows.Forms.DataGridView();
            this._extDefNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefBlockingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefWorkingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefWorkingTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefReturnCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAPVreturnCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefReturnNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefReturnTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefUROVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAPVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAVRcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._automaticPage = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.avr_disconnection = new System.Windows.Forms.MaskedTextBox();
            this.avr_permit_reset_switch = new System.Windows.Forms.CheckBox();
            this.label61 = new System.Windows.Forms.Label();
            this.avr_time_return = new System.Windows.Forms.MaskedTextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.avr_time_abrasion = new System.Windows.Forms.MaskedTextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.avr_return = new System.Windows.Forms.ComboBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.avr_abrasion = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this.avr_reset_blocking = new System.Windows.Forms.ComboBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.avr_abrasion_switch = new System.Windows.Forms.CheckBox();
            this.avr_supply_off = new System.Windows.Forms.CheckBox();
            this.avr_switch_off = new System.Windows.Forms.CheckBox();
            this.avr_self_off = new System.Windows.Forms.CheckBox();
            this.avr_blocking = new System.Windows.Forms.ComboBox();
            this.avr_start = new System.Windows.Forms.ComboBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label57 = new System.Windows.Forms.Label();
            this.lzsh_constraint = new System.Windows.Forms.MaskedTextBox();
            this.lzsh_conf = new System.Windows.Forms.ComboBox();
            this.label48 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.apv_time_4krat = new System.Windows.Forms.MaskedTextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.apv_time_3krat = new System.Windows.Forms.MaskedTextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.apv_time_2krat = new System.Windows.Forms.MaskedTextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.apv_time_1krat = new System.Windows.Forms.MaskedTextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.apv_time_ready = new System.Windows.Forms.MaskedTextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.apv_time_blocking = new System.Windows.Forms.MaskedTextBox();
            this.apv_self_off = new System.Windows.Forms.ComboBox();
            this.apv_blocking = new System.Windows.Forms.ComboBox();
            this.apv_conf = new System.Windows.Forms.ComboBox();
            this._engineDefensesPage = new System.Windows.Forms.TabPage();
            this._engineDefensesGrid = new System.Windows.Forms.DataGridView();
            this._engineDefenseNameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._engineDefenseModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._engineDefenseConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._engineDefenseUROVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._engineDefenseAVRcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._engineDefenseAPVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this._engineBlockTimeBox = new System.Windows.Forms.MaskedTextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this._engineQmodeCombo = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this._engineQtimeBox = new System.Windows.Forms.MaskedTextBox();
            this._engineBlockPuskBox = new System.Windows.Forms.MaskedTextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this._engineBlockDurationBox = new System.Windows.Forms.MaskedTextBox();
            this._engineQconstraintBox = new System.Windows.Forms.MaskedTextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this._engineBlockHeatBox = new System.Windows.Forms.MaskedTextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this._engineNpuskCombo = new System.Windows.Forms.ComboBox();
            this._engineQresetCombo = new System.Windows.Forms.ComboBox();
            this._engineHeatPuskConstraintBox = new System.Windows.Forms.MaskedTextBox();
            this.label29 = new System.Windows.Forms.Label();
            this._enginePuskTimeBox = new System.Windows.Forms.MaskedTextBox();
            this.label30 = new System.Windows.Forms.Label();
            this._engineIpConstraintBox = new System.Windows.Forms.MaskedTextBox();
            this.label27 = new System.Windows.Forms.Label();
            this._engineInBox = new System.Windows.Forms.MaskedTextBox();
            this.label28 = new System.Windows.Forms.Label();
            this._engineCoolingTimeBox = new System.Windows.Forms.MaskedTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this._engineHeatingTimeBox = new System.Windows.Forms.MaskedTextBox();
            this.label25 = new System.Windows.Forms.Label();
            this._defendTabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this._tokDefenseInbox = new System.Windows.Forms.MaskedTextBox();
            this.label24 = new System.Windows.Forms.Label();
            this._tokDefenseI2box = new System.Windows.Forms.MaskedTextBox();
            this.label23 = new System.Windows.Forms.Label();
            this._tokDefenseI0box = new System.Windows.Forms.MaskedTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this._tokDefenseIbox = new System.Windows.Forms.MaskedTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this._tokDefendTabPage = new System.Windows.Forms.TabPage();
            this._tokDefenseGrid4 = new System.Windows.Forms.DataGridView();
            this._tokDefense4NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4ModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense4BlockNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense4UpuskCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4PuskConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4WorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4WorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4SpeedupCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4SpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4UROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4APVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4AVRCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseGrid3 = new System.Windows.Forms.DataGridView();
            this._tokDefense3NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3ModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3BlockingNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3UpuskCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3PuskConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3DirectionCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3BlockingExistCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3ParameterCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3WorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3FeatureCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3WorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3SpeedupCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3SpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3UROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3APVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3AVRCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseGrid2 = new System.Windows.Forms.DataGridView();
            this._tokDefense2NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense2ModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense2BlockNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense2PuskUCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense2PuskConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense2DirectionCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense2BlockExistCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense2ParameterCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense2WorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense2FeatureCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense2WorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense2EngineCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense2UROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense2APVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense2AVRCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseGrid1 = new System.Windows.Forms.DataGridView();
            this._tokDefenseNameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseBlockNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseU_PuskCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefensePuskConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseDirectionCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseBlockExistCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseParameterCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseWorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseFeatureCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseWorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseSpeedUpCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseSpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseUROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseAPVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseAVRCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefendTabPage = new System.Windows.Forms.TabPage();
            this._voltageDefensesGrid2 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesBlockingNumberCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesParameterCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesWorkConstraintCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesWorkTimeCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnCol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVreturnCol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesReturnConstraintCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnTimeCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesUROVcol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVcol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAVRcol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesGrid1 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesBlockingNumberCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesParameterCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesWorkConstraintCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesWorkTimeCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVReturlCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesReturnConstraintCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnTimeCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesUROVCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAVRCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesGrid3 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesBlockingNumberCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesParameterCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesWorkConstraintCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesTimeConstraintCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnCol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVreturnCol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesReturnConstraintCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnTimeCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesUROVcol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVcol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAVRcol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequencyDefendTabPage = new System.Windows.Forms.TabPage();
            this._frequenceDefensesGrid = new System.Windows.Forms.DataGridView();
            this._frequenceDefensesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesMode = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesBlockingNumber = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesWorkConstraint = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesWorkTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesReturn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesAPVReturn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesConstraintAPV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesReturnTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesUROV = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesAPV = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesAVR = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this._groupChangeButton = new System.Windows.Forms.Button();
            this._reserveGroupRadioButton = new System.Windows.Forms.RadioButton();
            this._mainGroupRadioButton = new System.Windows.Forms.RadioButton();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this._exchangeProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._processLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.button1 = new System.Windows.Forms.Button();
            this._resetSetpointsButton = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.writeToHtmlItem = new System.Windows.Forms.ToolStripMenuItem();
            this._tabControl.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this._inSignalsPage.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._outputSignalsPage.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this._externalDefensePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).BeginInit();
            this._automaticPage.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this._engineDefensesPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._engineDefensesGrid)).BeginInit();
            this.groupBox19.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this._defendTabPage1.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this._tokDefendTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid1)).BeginInit();
            this._voltageDefendTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid3)).BeginInit();
            this._frequencyDefendTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._frequenceDefensesGrid)).BeginInit();
            this.groupBox24.SuspendLayout();
            this._statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tabControl
            // 
            this._tabControl.ContextMenuStrip = this.contextMenu;
            this._tabControl.Controls.Add(this._inSignalsPage);
            this._tabControl.Controls.Add(this._outputSignalsPage);
            this._tabControl.Controls.Add(this._externalDefensePage);
            this._tabControl.Controls.Add(this._automaticPage);
            this._tabControl.Controls.Add(this._engineDefensesPage);
            this._tabControl.Controls.Add(this._defendTabPage1);
            this._tabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._tabControl.Location = new System.Drawing.Point(0, 0);
            this._tabControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(1548, 777);
            this._tabControl.TabIndex = 0;
            // 
            // contextMenu
            // 
            this.contextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.resetSetPointStruct,
            this.readFromFileItem,
            this.writeToFileItem,
            this.writeToHtmlItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(253, 176);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(252, 24);
            this.readFromDeviceItem.Text = "��������� �� ����������";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(252, 24);
            this.writeToDeviceItem.Text = "�������� � ����������";
            // 
            // resetSetPointStruct
            // 
            this.resetSetPointStruct.Name = "resetSetPointStruct";
            this.resetSetPointStruct.Size = new System.Drawing.Size(252, 24);
            this.resetSetPointStruct.Text = "��������� �������";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(252, 24);
            this.readFromFileItem.Text = "��������� �� �����";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(252, 24);
            this.writeToFileItem.Text = "��������� � ����";
            // 
            // _inSignalsPage
            // 
            this._inSignalsPage.Controls.Add(this.groupBox23);
            this._inSignalsPage.Controls.Add(this.groupBox7);
            this._inSignalsPage.Controls.Add(this.groupBox6);
            this._inSignalsPage.Controls.Add(this.groupBox5);
            this._inSignalsPage.Controls.Add(this.groupBox4);
            this._inSignalsPage.Controls.Add(this.groupBox3);
            this._inSignalsPage.Controls.Add(this.groupBox2);
            this._inSignalsPage.Controls.Add(this.groupBox1);
            this._inSignalsPage.Location = new System.Drawing.Point(4, 25);
            this._inSignalsPage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._inSignalsPage.Name = "_inSignalsPage";
            this._inSignalsPage.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._inSignalsPage.Size = new System.Drawing.Size(1540, 748);
            this._inSignalsPage.TabIndex = 0;
            this._inSignalsPage.Text = "������� �������";
            this._inSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this._oscKonfComb);
            this.groupBox23.Location = new System.Drawing.Point(327, 357);
            this.groupBox23.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox23.Size = new System.Drawing.Size(271, 55);
            this.groupBox23.TabIndex = 21;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "���������� ������������";
            // 
            // _oscKonfComb
            // 
            this._oscKonfComb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscKonfComb.FormattingEnabled = true;
            this._oscKonfComb.Items.AddRange(new object[] {
            "1 �������������",
            "1 ���������������� ���.",
            "2 ���������������� ���."});
            this._oscKonfComb.Location = new System.Drawing.Point(20, 21);
            this._oscKonfComb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._oscKonfComb.Name = "_oscKonfComb";
            this._oscKonfComb.Size = new System.Drawing.Size(232, 24);
            this._oscKonfComb.TabIndex = 0;
            this._oscKonfComb.SelectedIndexChanged += new System.EventHandler(this._oscKonfComb_SelectedIndexChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._manageSignalsSDTU_Combo);
            this.groupBox7.Controls.Add(this.label43);
            this.groupBox7.Controls.Add(this._manageSignalsExternalCombo);
            this.groupBox7.Controls.Add(this.label44);
            this.groupBox7.Controls.Add(this._manageSignalsKeyCombo);
            this.groupBox7.Controls.Add(this.label45);
            this.groupBox7.Controls.Add(this._manageSignalsButtonCombo);
            this.groupBox7.Controls.Add(this.label46);
            this.groupBox7.Location = new System.Drawing.Point(327, 222);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox7.Size = new System.Drawing.Size(271, 129);
            this.groupBox7.TabIndex = 7;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "������� ����������";
            // 
            // _manageSignalsSDTU_Combo
            // 
            this._manageSignalsSDTU_Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsSDTU_Combo.FormattingEnabled = true;
            this._manageSignalsSDTU_Combo.Location = new System.Drawing.Point(95, 92);
            this._manageSignalsSDTU_Combo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._manageSignalsSDTU_Combo.Name = "_manageSignalsSDTU_Combo";
            this._manageSignalsSDTU_Combo.Size = new System.Drawing.Size(167, 24);
            this._manageSignalsSDTU_Combo.TabIndex = 23;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(8, 97);
            this.label43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(68, 17);
            this.label43.TabIndex = 22;
            this.label43.Text = "�� ����";
            // 
            // _manageSignalsExternalCombo
            // 
            this._manageSignalsExternalCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsExternalCombo.FormattingEnabled = true;
            this._manageSignalsExternalCombo.Location = new System.Drawing.Point(95, 69);
            this._manageSignalsExternalCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._manageSignalsExternalCombo.Name = "_manageSignalsExternalCombo";
            this._manageSignalsExternalCombo.Size = new System.Drawing.Size(167, 24);
            this._manageSignalsExternalCombo.TabIndex = 21;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(8, 74);
            this.label44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(68, 17);
            this.label44.TabIndex = 20;
            this.label44.Text = "�������";
            // 
            // _manageSignalsKeyCombo
            // 
            this._manageSignalsKeyCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsKeyCombo.FormattingEnabled = true;
            this._manageSignalsKeyCombo.Location = new System.Drawing.Point(95, 46);
            this._manageSignalsKeyCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._manageSignalsKeyCombo.Name = "_manageSignalsKeyCombo";
            this._manageSignalsKeyCombo.Size = new System.Drawing.Size(167, 24);
            this._manageSignalsKeyCombo.TabIndex = 19;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(8, 50);
            this.label45.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(71, 17);
            this.label45.TabIndex = 18;
            this.label45.Text = "�� �����";
            // 
            // _manageSignalsButtonCombo
            // 
            this._manageSignalsButtonCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsButtonCombo.FormattingEnabled = true;
            this._manageSignalsButtonCombo.Location = new System.Drawing.Point(95, 21);
            this._manageSignalsButtonCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._manageSignalsButtonCombo.Name = "_manageSignalsButtonCombo";
            this._manageSignalsButtonCombo.Size = new System.Drawing.Size(167, 24);
            this._manageSignalsButtonCombo.TabIndex = 17;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(8, 27);
            this.label46.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(76, 17);
            this.label46.TabIndex = 16;
            this.label46.Text = "�� ������";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox21);
            this.groupBox6.Controls.Add(this._releDispepairBox);
            this.groupBox6.Controls.Add(this.label67);
            this.groupBox6.Location = new System.Drawing.Point(815, 12);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox6.Size = new System.Drawing.Size(188, 276);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "���� �������������";
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._dispepairCheckList);
            this.groupBox21.Location = new System.Drawing.Point(8, 82);
            this.groupBox21.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox21.Size = new System.Drawing.Size(172, 177);
            this.groupBox21.TabIndex = 8;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "������";
            // 
            // _dispepairCheckList
            // 
            this._dispepairCheckList.CheckOnClick = true;
            this._dispepairCheckList.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dispepairCheckList.FormattingEnabled = true;
            this._dispepairCheckList.Items.AddRange(new object[] {
            "������������� 1",
            "������������� 2",
            "������������� 3",
            "������������� 4",
            "������������� 5",
            "������������� 6",
            "������������� 7",
            "������������� 8"});
            this._dispepairCheckList.Location = new System.Drawing.Point(4, 19);
            this._dispepairCheckList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._dispepairCheckList.Name = "_dispepairCheckList";
            this._dispepairCheckList.Size = new System.Drawing.Size(164, 154);
            this._dispepairCheckList.TabIndex = 5;
            // 
            // _releDispepairBox
            // 
            this._releDispepairBox.Location = new System.Drawing.Point(21, 39);
            this._releDispepairBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._releDispepairBox.Name = "_releDispepairBox";
            this._releDispepairBox.Size = new System.Drawing.Size(153, 22);
            this._releDispepairBox.TabIndex = 9;
            this._releDispepairBox.Tag = "3000000";
            this._releDispepairBox.Text = "0";
            this._releDispepairBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(17, 22);
            this.label67.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(78, 17);
            this.label67.TabIndex = 22;
            this.label67.Text = "������, ��";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._applyLogicSignalsBut);
            this.groupBox5.Controls.Add(this._logicChannelsCombo);
            this.groupBox5.Controls.Add(this._logicSignalsDataGrid);
            this.groupBox5.Location = new System.Drawing.Point(605, 7);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox5.Size = new System.Drawing.Size(201, 518);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "���������� �������";
            // 
            // _applyLogicSignalsBut
            // 
            this._applyLogicSignalsBut.Location = new System.Drawing.Point(61, 482);
            this._applyLogicSignalsBut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._applyLogicSignalsBut.Name = "_applyLogicSignalsBut";
            this._applyLogicSignalsBut.Size = new System.Drawing.Size(83, 28);
            this._applyLogicSignalsBut.TabIndex = 3;
            this._applyLogicSignalsBut.Text = "�������";
            this._applyLogicSignalsBut.UseVisualStyleBackColor = true;
            this._applyLogicSignalsBut.Click += new System.EventHandler(this._applyLogicSignalsBut_Click);
            // 
            // _logicChannelsCombo
            // 
            this._logicChannelsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._logicChannelsCombo.FormattingEnabled = true;
            this._logicChannelsCombo.Items.AddRange(new object[] {
            "�1 �",
            "�2 �",
            "�3 �",
            "�4 �",
            "�5 ���",
            "�6 ���",
            "�7 ���",
            "�8 ���"});
            this._logicChannelsCombo.Location = new System.Drawing.Point(45, 23);
            this._logicChannelsCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._logicChannelsCombo.Name = "_logicChannelsCombo";
            this._logicChannelsCombo.Size = new System.Drawing.Size(84, 24);
            this._logicChannelsCombo.TabIndex = 1;
            this._logicChannelsCombo.SelectedIndexChanged += new System.EventHandler(this._logicChannelsCombo_SelectedIndexChanged);
            // 
            // _logicSignalsDataGrid
            // 
            this._logicSignalsDataGrid.AllowUserToAddRows = false;
            this._logicSignalsDataGrid.AllowUserToDeleteRows = false;
            this._logicSignalsDataGrid.AllowUserToResizeColumns = false;
            this._logicSignalsDataGrid.AllowUserToResizeRows = false;
            this._logicSignalsDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._logicSignalsDataGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._logicSignalsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._logicSignalsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._diskretChannelCol,
            this._diskretValueCol});
            this._logicSignalsDataGrid.Location = new System.Drawing.Point(15, 55);
            this._logicSignalsDataGrid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._logicSignalsDataGrid.MultiSelect = false;
            this._logicSignalsDataGrid.Name = "_logicSignalsDataGrid";
            this._logicSignalsDataGrid.RowHeadersVisible = false;
            this._logicSignalsDataGrid.RowHeadersWidth = 51;
            this._logicSignalsDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._logicSignalsDataGrid.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this._logicSignalsDataGrid.RowTemplate.Height = 20;
            this._logicSignalsDataGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._logicSignalsDataGrid.ShowCellErrors = false;
            this._logicSignalsDataGrid.ShowCellToolTips = false;
            this._logicSignalsDataGrid.ShowEditingIcon = false;
            this._logicSignalsDataGrid.ShowRowErrors = false;
            this._logicSignalsDataGrid.Size = new System.Drawing.Size(176, 418);
            this._logicSignalsDataGrid.TabIndex = 0;
            // 
            // _diskretChannelCol
            // 
            this._diskretChannelCol.HeaderText = "�";
            this._diskretChannelCol.MinimumWidth = 6;
            this._diskretChannelCol.Name = "_diskretChannelCol";
            this._diskretChannelCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._diskretChannelCol.Width = 28;
            // 
            // _diskretValueCol
            // 
            this._diskretValueCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._diskretValueCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._diskretValueCol.HeaderText = "��������";
            this._diskretValueCol.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this._diskretValueCol.MinimumWidth = 6;
            this._diskretValueCol.Name = "_diskretValueCol";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this._switcherDurationBox);
            this.groupBox4.Controls.Add(this._switcherTokBox);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this._switcherTimeBox);
            this.groupBox4.Controls.Add(this._switcherImpulseBox);
            this.groupBox4.Controls.Add(this._switcherBlockCombo);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this._switcherErrorCombo);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this._switcherStateOnCombo);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this._switcherStateOffCombo);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Location = new System.Drawing.Point(327, 7);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Size = new System.Drawing.Size(271, 212);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "�����������";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(7, 182);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(119, 17);
            this.label19.TabIndex = 23;
            this.label19.Text = "����. ���������";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 135);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(93, 17);
            this.label17.TabIndex = 19;
            this.label17.Text = "��� ����, I�";
            // 
            // _switcherDurationBox
            // 
            this._switcherDurationBox.Location = new System.Drawing.Point(167, 181);
            this._switcherDurationBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._switcherDurationBox.Name = "_switcherDurationBox";
            this._switcherDurationBox.Size = new System.Drawing.Size(93, 22);
            this._switcherDurationBox.TabIndex = 22;
            this._switcherDurationBox.Tag = "3000000";
            this._switcherDurationBox.Text = "0";
            this._switcherDurationBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherTokBox
            // 
            this._switcherTokBox.Location = new System.Drawing.Point(167, 132);
            this._switcherTokBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._switcherTokBox.Name = "_switcherTokBox";
            this._switcherTokBox.Size = new System.Drawing.Size(93, 22);
            this._switcherTokBox.TabIndex = 18;
            this._switcherTokBox.Tag = "40";
            this._switcherTokBox.Text = "0";
            this._switcherTokBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 159);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(110, 17);
            this.label20.TabIndex = 21;
            this.label20.Text = "������� ��, ��";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(7, 112);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(116, 17);
            this.label18.TabIndex = 17;
            this.label18.Text = "����� ����, ��";
            // 
            // _switcherTimeBox
            // 
            this._switcherTimeBox.Location = new System.Drawing.Point(167, 107);
            this._switcherTimeBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._switcherTimeBox.Name = "_switcherTimeBox";
            this._switcherTimeBox.Size = new System.Drawing.Size(93, 22);
            this._switcherTimeBox.TabIndex = 16;
            this._switcherTimeBox.Tag = "3000000";
            this._switcherTimeBox.Text = "0";
            this._switcherTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherImpulseBox
            // 
            this._switcherImpulseBox.Location = new System.Drawing.Point(167, 156);
            this._switcherImpulseBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._switcherImpulseBox.Name = "_switcherImpulseBox";
            this._switcherImpulseBox.Size = new System.Drawing.Size(93, 22);
            this._switcherImpulseBox.TabIndex = 20;
            this._switcherImpulseBox.Tag = "3000000";
            this._switcherImpulseBox.Text = "0";
            this._switcherImpulseBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherBlockCombo
            // 
            this._switcherBlockCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherBlockCombo.FormattingEnabled = true;
            this._switcherBlockCombo.Location = new System.Drawing.Point(167, 84);
            this._switcherBlockCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._switcherBlockCombo.Name = "_switcherBlockCombo";
            this._switcherBlockCombo.Size = new System.Drawing.Size(93, 24);
            this._switcherBlockCombo.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 89);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 17);
            this.label10.TabIndex = 14;
            this.label10.Text = "����������";
            // 
            // _switcherErrorCombo
            // 
            this._switcherErrorCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherErrorCombo.FormattingEnabled = true;
            this._switcherErrorCombo.Location = new System.Drawing.Point(167, 60);
            this._switcherErrorCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._switcherErrorCombo.Name = "_switcherErrorCombo";
            this._switcherErrorCombo.Size = new System.Drawing.Size(93, 24);
            this._switcherErrorCombo.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 65);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(109, 17);
            this.label14.TabIndex = 12;
            this.label14.Text = "�������������";
            // 
            // _switcherStateOnCombo
            // 
            this._switcherStateOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherStateOnCombo.FormattingEnabled = true;
            this._switcherStateOnCombo.Location = new System.Drawing.Point(167, 37);
            this._switcherStateOnCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._switcherStateOnCombo.Name = "_switcherStateOnCombo";
            this._switcherStateOnCombo.Size = new System.Drawing.Size(93, 24);
            this._switcherStateOnCombo.TabIndex = 11;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 42);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(147, 17);
            this.label15.TabIndex = 10;
            this.label15.Text = "��������� ��������";
            // 
            // _switcherStateOffCombo
            // 
            this._switcherStateOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherStateOffCombo.FormattingEnabled = true;
            this._switcherStateOffCombo.Location = new System.Drawing.Point(167, 14);
            this._switcherStateOffCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._switcherStateOffCombo.Name = "_switcherStateOffCombo";
            this._switcherStateOffCombo.Size = new System.Drawing.Size(93, 24);
            this._switcherStateOffCombo.TabIndex = 9;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 18);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(155, 17);
            this.label16.TabIndex = 8;
            this.label16.Text = "��������� ���������";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._constraintGroupCombo);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this._signalizationCombo);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this._extOffCombo);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this._extOnCombo);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this._keyOffCombo);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this._keyOnCombo);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(11, 278);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(293, 182);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "������� �������";
            // 
            // _constraintGroupCombo
            // 
            this._constraintGroupCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._constraintGroupCombo.FormattingEnabled = true;
            this._constraintGroupCombo.Location = new System.Drawing.Point(189, 143);
            this._constraintGroupCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._constraintGroupCombo.Name = "_constraintGroupCombo";
            this._constraintGroupCombo.Size = new System.Drawing.Size(93, 24);
            this._constraintGroupCombo.TabIndex = 11;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 146);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(116, 17);
            this.label12.TabIndex = 10;
            this.label12.Text = "������. �������";
            // 
            // _signalizationCombo
            // 
            this._signalizationCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._signalizationCombo.FormattingEnabled = true;
            this._signalizationCombo.Location = new System.Drawing.Point(189, 119);
            this._signalizationCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._signalizationCombo.Name = "_signalizationCombo";
            this._signalizationCombo.Size = new System.Drawing.Size(93, 24);
            this._signalizationCombo.TabIndex = 9;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 123);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(123, 17);
            this.label13.TabIndex = 8;
            this.label13.Text = "����� ���������";
            // 
            // _extOffCombo
            // 
            this._extOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._extOffCombo.FormattingEnabled = true;
            this._extOffCombo.Location = new System.Drawing.Point(189, 95);
            this._extOffCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._extOffCombo.Name = "_extOffCombo";
            this._extOffCombo.Size = new System.Drawing.Size(93, 24);
            this._extOffCombo.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 75);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 17);
            this.label8.TabIndex = 6;
            this.label8.Text = "����. ���������";
            // 
            // _extOnCombo
            // 
            this._extOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._extOnCombo.FormattingEnabled = true;
            this._extOnCombo.Location = new System.Drawing.Point(189, 71);
            this._extOnCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._extOnCombo.Name = "_extOnCombo";
            this._extOnCombo.Size = new System.Drawing.Size(93, 24);
            this._extOnCombo.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 98);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 17);
            this.label9.TabIndex = 4;
            this.label9.Text = "����. ��������";
            // 
            // _keyOffCombo
            // 
            this._keyOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._keyOffCombo.FormattingEnabled = true;
            this._keyOffCombo.Location = new System.Drawing.Point(189, 47);
            this._keyOffCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._keyOffCombo.Name = "_keyOffCombo";
            this._keyOffCombo.Size = new System.Drawing.Size(93, 24);
            this._keyOffCombo.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 27);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 17);
            this.label7.TabIndex = 2;
            this.label7.Text = "���� ���������";
            // 
            // _keyOnCombo
            // 
            this._keyOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._keyOnCombo.FormattingEnabled = true;
            this._keyOnCombo.Location = new System.Drawing.Point(189, 23);
            this._keyOnCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._keyOnCombo.Name = "_keyOnCombo";
            this._keyOnCombo.Size = new System.Drawing.Size(93, 24);
            this._keyOnCombo.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 50);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "���� ��������";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._TN_typeCombo);
            this.groupBox2.Controls.Add(this.label42);
            this.groupBox2.Controls.Add(this._TNNP_dispepairCombo);
            this.groupBox2.Controls.Add(this.label41);
            this.groupBox2.Controls.Add(this._TN_dispepairCombo);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this._TNNP_Box);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this._TN_Box);
            this.groupBox2.Location = new System.Drawing.Point(11, 116);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(293, 155);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "��� ��";
            // 
            // _TN_typeCombo
            // 
            this._TN_typeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TN_typeCombo.FormattingEnabled = true;
            this._TN_typeCombo.Location = new System.Drawing.Point(99, 119);
            this._TN_typeCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._TN_typeCombo.Name = "_TN_typeCombo";
            this._TN_typeCombo.Size = new System.Drawing.Size(184, 24);
            this._TN_typeCombo.TabIndex = 12;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(8, 124);
            this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(56, 17);
            this.label42.TabIndex = 11;
            this.label42.Text = "��� ��";
            // 
            // _TNNP_dispepairCombo
            // 
            this._TNNP_dispepairCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TNNP_dispepairCombo.FormattingEnabled = true;
            this._TNNP_dispepairCombo.Location = new System.Drawing.Point(149, 94);
            this._TNNP_dispepairCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._TNNP_dispepairCombo.Name = "_TNNP_dispepairCombo";
            this._TNNP_dispepairCombo.Size = new System.Drawing.Size(133, 24);
            this._TNNP_dispepairCombo.TabIndex = 10;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(8, 100);
            this.label41.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(104, 17);
            this.label41.TabIndex = 9;
            this.label41.Text = "������. ����";
            // 
            // _TN_dispepairCombo
            // 
            this._TN_dispepairCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TN_dispepairCombo.FormattingEnabled = true;
            this._TN_dispepairCombo.Location = new System.Drawing.Point(149, 68);
            this._TN_dispepairCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._TN_dispepairCombo.Name = "_TN_dispepairCombo";
            this._TN_dispepairCombo.Size = new System.Drawing.Size(133, 24);
            this._TN_dispepairCombo.TabIndex = 8;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(8, 74);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(84, 17);
            this.label39.TabIndex = 7;
            this.label39.Text = "������. ��";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 48);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "����������� ����";
            // 
            // _TNNP_Box
            // 
            this._TNNP_Box.Location = new System.Drawing.Point(191, 43);
            this._TNNP_Box.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._TNNP_Box.Name = "_TNNP_Box";
            this._TNNP_Box.Size = new System.Drawing.Size(92, 22);
            this._TNNP_Box.TabIndex = 5;
            this._TNNP_Box.Tag = "256";
            this._TNNP_Box.Text = "0";
            this._TNNP_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 25);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 17);
            this.label6.TabIndex = 3;
            this.label6.Text = "����������� ��";
            // 
            // _TN_Box
            // 
            this._TN_Box.Location = new System.Drawing.Point(191, 18);
            this._TN_Box.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._TN_Box.Name = "_TN_Box";
            this._TN_Box.Size = new System.Drawing.Size(92, 22);
            this._TN_Box.TabIndex = 2;
            this._TN_Box.Tag = "256";
            this._TN_Box.Text = "0";
            this._TN_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this._maxTok_Box);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._TTNP_Box);
            this.groupBox1.Controls.Add(this._TT_Box);
            this.groupBox1.Location = new System.Drawing.Point(11, 7);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(293, 101);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "��������� ����";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 68);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "����. ��� ��������, I�";
            // 
            // _maxTok_Box
            // 
            this._maxTok_Box.Location = new System.Drawing.Point(189, 65);
            this._maxTok_Box.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._maxTok_Box.Name = "_maxTok_Box";
            this._maxTok_Box.Size = new System.Drawing.Size(93, 22);
            this._maxTok_Box.TabIndex = 5;
            this._maxTok_Box.Tag = "40";
            this._maxTok_Box.Text = "0";
            this._maxTok_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "��������� ��� ��, �";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 44);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "��������� ��� ����, �";
            // 
            // _TTNP_Box
            // 
            this._TTNP_Box.Location = new System.Drawing.Point(189, 41);
            this._TTNP_Box.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._TTNP_Box.Name = "_TTNP_Box";
            this._TTNP_Box.Size = new System.Drawing.Size(93, 22);
            this._TTNP_Box.TabIndex = 2;
            this._TTNP_Box.Tag = "100";
            this._TTNP_Box.Text = "0";
            this._TTNP_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _TT_Box
            // 
            this._TT_Box.Location = new System.Drawing.Point(189, 16);
            this._TT_Box.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._TT_Box.Name = "_TT_Box";
            this._TT_Box.Size = new System.Drawing.Size(93, 22);
            this._TT_Box.TabIndex = 0;
            this._TT_Box.Tag = "1500";
            this._TT_Box.Text = "0";
            this._TT_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._toolTip.SetToolTip(this._TT_Box, "��������� ��� ��������������");
            // 
            // _outputSignalsPage
            // 
            this._outputSignalsPage.Controls.Add(this.groupBox10);
            this._outputSignalsPage.Controls.Add(this.groupBox9);
            this._outputSignalsPage.Controls.Add(this.groupBox8);
            this._outputSignalsPage.Location = new System.Drawing.Point(4, 25);
            this._outputSignalsPage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._outputSignalsPage.Name = "_outputSignalsPage";
            this._outputSignalsPage.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._outputSignalsPage.Size = new System.Drawing.Size(1540, 748);
            this._outputSignalsPage.TabIndex = 1;
            this._outputSignalsPage.Text = "�������� �������";
            this._outputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._outputLogicCheckList);
            this.groupBox10.Controls.Add(this._outputLogicAcceptBut);
            this.groupBox10.Controls.Add(this._outputLogicCombo);
            this.groupBox10.Location = new System.Drawing.Point(584, 6);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox10.Size = new System.Drawing.Size(237, 620);
            this.groupBox10.TabIndex = 5;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "���������� �������";
            // 
            // _outputLogicCheckList
            // 
            this._outputLogicCheckList.CheckOnClick = true;
            this._outputLogicCheckList.FormattingEnabled = true;
            this._outputLogicCheckList.Location = new System.Drawing.Point(4, 59);
            this._outputLogicCheckList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._outputLogicCheckList.Name = "_outputLogicCheckList";
            this._outputLogicCheckList.ScrollAlwaysVisible = true;
            this._outputLogicCheckList.Size = new System.Drawing.Size(224, 514);
            this._outputLogicCheckList.TabIndex = 4;
            // 
            // _outputLogicAcceptBut
            // 
            this._outputLogicAcceptBut.Location = new System.Drawing.Point(72, 583);
            this._outputLogicAcceptBut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._outputLogicAcceptBut.Name = "_outputLogicAcceptBut";
            this._outputLogicAcceptBut.Size = new System.Drawing.Size(79, 28);
            this._outputLogicAcceptBut.TabIndex = 3;
            this._outputLogicAcceptBut.Text = "�������";
            this._outputLogicAcceptBut.UseVisualStyleBackColor = true;
            this._outputLogicAcceptBut.Click += new System.EventHandler(this._outputLogicAcceptBut_Click);
            // 
            // _outputLogicCombo
            // 
            this._outputLogicCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._outputLogicCombo.FormattingEnabled = true;
            this._outputLogicCombo.Items.AddRange(new object[] {
            "��� 1",
            "��� 2 ",
            "��� 3 ",
            "��� 4 ",
            "��� 5",
            "��� 6",
            "��� 7 ",
            "��� 8"});
            this._outputLogicCombo.Location = new System.Drawing.Point(49, 23);
            this._outputLogicCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._outputLogicCombo.Name = "_outputLogicCombo";
            this._outputLogicCombo.Size = new System.Drawing.Size(87, 24);
            this._outputLogicCombo.TabIndex = 1;
            this._outputLogicCombo.SelectedIndexChanged += new System.EventHandler(this._outputLogicCombo_SelectedIndexChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._outputIndicatorsGrid);
            this.groupBox9.Location = new System.Drawing.Point(11, 314);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox9.Size = new System.Drawing.Size(565, 313);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "����������";
            // 
            // _outputIndicatorsGrid
            // 
            this._outputIndicatorsGrid.AllowUserToAddRows = false;
            this._outputIndicatorsGrid.AllowUserToDeleteRows = false;
            this._outputIndicatorsGrid.AllowUserToResizeColumns = false;
            this._outputIndicatorsGrid.AllowUserToResizeRows = false;
            this._outputIndicatorsGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._outputIndicatorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputIndicatorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._outIndNumberCol,
            this._outIndTypeCol,
            this._outIndSignalCol,
            this._outIndResetCol,
            this._outIndAlarmCol,
            this._outIndSystemCol});
            this._outputIndicatorsGrid.Location = new System.Drawing.Point(8, 18);
            this._outputIndicatorsGrid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._outputIndicatorsGrid.MultiSelect = false;
            this._outputIndicatorsGrid.Name = "_outputIndicatorsGrid";
            this._outputIndicatorsGrid.RowHeadersVisible = false;
            this._outputIndicatorsGrid.RowHeadersWidth = 51;
            this._outputIndicatorsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputIndicatorsGrid.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this._outputIndicatorsGrid.RowTemplate.Height = 24;
            this._outputIndicatorsGrid.ShowCellErrors = false;
            this._outputIndicatorsGrid.ShowRowErrors = false;
            this._outputIndicatorsGrid.Size = new System.Drawing.Size(547, 287);
            this._outputIndicatorsGrid.TabIndex = 0;
            // 
            // _outIndNumberCol
            // 
            this._outIndNumberCol.HeaderText = "�";
            this._outIndNumberCol.MinimumWidth = 6;
            this._outIndNumberCol.Name = "_outIndNumberCol";
            this._outIndNumberCol.ReadOnly = true;
            this._outIndNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndNumberCol.Width = 20;
            // 
            // _outIndTypeCol
            // 
            this._outIndTypeCol.HeaderText = "���";
            this._outIndTypeCol.Items.AddRange(new object[] {
            "�����������",
            "�������",
            "XXXXX"});
            this._outIndTypeCol.MinimumWidth = 6;
            this._outIndTypeCol.Name = "_outIndTypeCol";
            this._outIndTypeCol.Width = 120;
            // 
            // _outIndSignalCol
            // 
            this._outIndSignalCol.HeaderText = "������";
            this._outIndSignalCol.MinimumWidth = 6;
            this._outIndSignalCol.Name = "_outIndSignalCol";
            this._outIndSignalCol.Width = 140;
            // 
            // _outIndResetCol
            // 
            this._outIndResetCol.HeaderText = "����� ���.";
            this._outIndResetCol.MinimumWidth = 6;
            this._outIndResetCol.Name = "_outIndResetCol";
            this._outIndResetCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._outIndResetCol.Width = 40;
            // 
            // _outIndAlarmCol
            // 
            this._outIndAlarmCol.HeaderText = "����� ��";
            this._outIndAlarmCol.MinimumWidth = 6;
            this._outIndAlarmCol.Name = "_outIndAlarmCol";
            this._outIndAlarmCol.Width = 40;
            // 
            // _outIndSystemCol
            // 
            this._outIndSystemCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._outIndSystemCol.HeaderText = "����� ��";
            this._outIndSystemCol.MinimumWidth = 6;
            this._outIndSystemCol.Name = "_outIndSystemCol";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._outputReleGrid);
            this.groupBox8.Location = new System.Drawing.Point(11, 6);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox8.Size = new System.Drawing.Size(565, 300);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "�������� ����";
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this._releSignalCol,
            this._releImpulseCol});
            this._outputReleGrid.Location = new System.Drawing.Point(8, 16);
            this._outputReleGrid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._outputReleGrid.MultiSelect = false;
            this._outputReleGrid.Name = "_outputReleGrid";
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidth = 51;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(547, 277);
            this._outputReleGrid.TabIndex = 0;
            // 
            // _releNumberCol
            // 
            this._releNumberCol.HeaderText = "�";
            this._releNumberCol.MinimumWidth = 6;
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 20;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releTypeCol.HeaderText = "���";
            this._releTypeCol.Items.AddRange(new object[] {
            "�����������",
            "�������",
            "XXXXX"});
            this._releTypeCol.MinimumWidth = 6;
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 120;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releSignalCol.HeaderText = "������";
            this._releSignalCol.MinimumWidth = 6;
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 140;
            // 
            // _releImpulseCol
            // 
            this._releImpulseCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._releImpulseCol.HeaderText = "������, ��";
            this._releImpulseCol.MinimumWidth = 6;
            this._releImpulseCol.Name = "_releImpulseCol";
            this._releImpulseCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _externalDefensePage
            // 
            this._externalDefensePage.Controls.Add(this._externalDefenseGrid);
            this._externalDefensePage.Location = new System.Drawing.Point(4, 25);
            this._externalDefensePage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._externalDefensePage.Name = "_externalDefensePage";
            this._externalDefensePage.Size = new System.Drawing.Size(1540, 748);
            this._externalDefensePage.TabIndex = 2;
            this._externalDefensePage.Text = "������� ������";
            this._externalDefensePage.UseVisualStyleBackColor = true;
            // 
            // _externalDefenseGrid
            // 
            this._externalDefenseGrid.AllowUserToAddRows = false;
            this._externalDefenseGrid.AllowUserToDeleteRows = false;
            this._externalDefenseGrid.AllowUserToResizeColumns = false;
            this._externalDefenseGrid.AllowUserToResizeRows = false;
            this._externalDefenseGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._externalDefenseGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._externalDefenseGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._externalDefenseGrid.ColumnHeadersHeight = 30;
            this._externalDefenseGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._externalDefenseGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extDefNumberCol,
            this._exDefModeCol,
            this._exDefBlockingCol,
            this._exDefWorkingCol,
            this._exDefWorkingTimeCol,
            this._exDefReturnCol,
            this._exDefAPVreturnCol,
            this._exDefReturnNumberCol,
            this._exDefReturnTimeCol,
            this._exDefUROVcol,
            this._exDefAPVcol,
            this._exDefAVRcol});
            this._externalDefenseGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._externalDefenseGrid.Location = new System.Drawing.Point(0, 0);
            this._externalDefenseGrid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._externalDefenseGrid.MultiSelect = false;
            this._externalDefenseGrid.Name = "_externalDefenseGrid";
            this._externalDefenseGrid.RowHeadersVisible = false;
            this._externalDefenseGrid.RowHeadersWidth = 51;
            this._externalDefenseGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDefenseGrid.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this._externalDefenseGrid.RowTemplate.Height = 24;
            this._externalDefenseGrid.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._externalDefenseGrid.Size = new System.Drawing.Size(1540, 748);
            this._externalDefenseGrid.TabIndex = 0;
            this._externalDefenseGrid.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this._externalDefenseGrid_CellBeginEdit);
            // 
            // _extDefNumberCol
            // 
            this._extDefNumberCol.HeaderText = "�";
            this._extDefNumberCol.MinimumWidth = 6;
            this._extDefNumberCol.Name = "_extDefNumberCol";
            this._extDefNumberCol.ReadOnly = true;
            this._extDefNumberCol.Width = 51;
            // 
            // _exDefModeCol
            // 
            this._exDefModeCol.HeaderText = "�����";
            this._exDefModeCol.MinimumWidth = 6;
            this._exDefModeCol.Name = "_exDefModeCol";
            this._exDefModeCol.Width = 57;
            // 
            // _exDefBlockingCol
            // 
            this._exDefBlockingCol.HeaderText = "����������";
            this._exDefBlockingCol.MinimumWidth = 6;
            this._exDefBlockingCol.Name = "_exDefBlockingCol";
            this._exDefBlockingCol.Width = 92;
            // 
            // _exDefWorkingCol
            // 
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._exDefWorkingCol.DefaultCellStyle = dataGridViewCellStyle4;
            this._exDefWorkingCol.HeaderText = "������������";
            this._exDefWorkingCol.MinimumWidth = 6;
            this._exDefWorkingCol.Name = "_exDefWorkingCol";
            this._exDefWorkingCol.Width = 111;
            // 
            // _exDefWorkingTimeCol
            // 
            this._exDefWorkingTimeCol.HeaderText = "����� ������.";
            this._exDefWorkingTimeCol.MinimumWidth = 6;
            this._exDefWorkingTimeCol.Name = "_exDefWorkingTimeCol";
            this._exDefWorkingTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._exDefWorkingTimeCol.Width = 110;
            // 
            // _exDefReturnCol
            // 
            this._exDefReturnCol.HeaderText = "����.";
            this._exDefReturnCol.MinimumWidth = 6;
            this._exDefReturnCol.Name = "_exDefReturnCol";
            this._exDefReturnCol.Width = 49;
            // 
            // _exDefAPVreturnCol
            // 
            this._exDefAPVreturnCol.HeaderText = "��� ��";
            this._exDefAPVreturnCol.MinimumWidth = 6;
            this._exDefAPVreturnCol.Name = "_exDefAPVreturnCol";
            this._exDefAPVreturnCol.Width = 64;
            // 
            // _exDefReturnNumberCol
            // 
            this._exDefReturnNumberCol.HeaderText = "���� ��������";
            this._exDefReturnNumberCol.MinimumWidth = 6;
            this._exDefReturnNumberCol.Name = "_exDefReturnNumberCol";
            this._exDefReturnNumberCol.Width = 109;
            // 
            // _exDefReturnTimeCol
            // 
            this._exDefReturnTimeCol.HeaderText = "����� ��������";
            this._exDefReturnTimeCol.MinimumWidth = 6;
            this._exDefReturnTimeCol.Name = "_exDefReturnTimeCol";
            this._exDefReturnTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._exDefReturnTimeCol.Width = 120;
            // 
            // _exDefUROVcol
            // 
            this._exDefUROVcol.HeaderText = "����";
            this._exDefUROVcol.MinimumWidth = 6;
            this._exDefUROVcol.Name = "_exDefUROVcol";
            this._exDefUROVcol.Width = 52;
            // 
            // _exDefAPVcol
            // 
            this._exDefAPVcol.HeaderText = "���";
            this._exDefAPVcol.MinimumWidth = 6;
            this._exDefAPVcol.Name = "_exDefAPVcol";
            this._exDefAPVcol.Width = 42;
            // 
            // _exDefAVRcol
            // 
            this._exDefAVRcol.HeaderText = "���";
            this._exDefAVRcol.MinimumWidth = 6;
            this._exDefAVRcol.Name = "_exDefAVRcol";
            this._exDefAVRcol.Width = 41;
            // 
            // _automaticPage
            // 
            this._automaticPage.Controls.Add(this.groupBox11);
            this._automaticPage.Controls.Add(this.groupBox13);
            this._automaticPage.Controls.Add(this.groupBox14);
            this._automaticPage.Location = new System.Drawing.Point(4, 25);
            this._automaticPage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._automaticPage.Name = "_automaticPage";
            this._automaticPage.Size = new System.Drawing.Size(1540, 748);
            this._automaticPage.TabIndex = 3;
            this._automaticPage.Text = "����������";
            this._automaticPage.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label58);
            this.groupBox11.Controls.Add(this.label59);
            this.groupBox11.Controls.Add(this.avr_disconnection);
            this.groupBox11.Controls.Add(this.avr_permit_reset_switch);
            this.groupBox11.Controls.Add(this.label61);
            this.groupBox11.Controls.Add(this.avr_time_return);
            this.groupBox11.Controls.Add(this.label62);
            this.groupBox11.Controls.Add(this.avr_time_abrasion);
            this.groupBox11.Controls.Add(this.label63);
            this.groupBox11.Controls.Add(this.avr_return);
            this.groupBox11.Controls.Add(this.label64);
            this.groupBox11.Controls.Add(this.label65);
            this.groupBox11.Controls.Add(this.avr_abrasion);
            this.groupBox11.Controls.Add(this.label66);
            this.groupBox11.Controls.Add(this.avr_reset_blocking);
            this.groupBox11.Controls.Add(this.groupBox12);
            this.groupBox11.Controls.Add(this.avr_blocking);
            this.groupBox11.Controls.Add(this.avr_start);
            this.groupBox11.Location = new System.Drawing.Point(353, 1);
            this.groupBox11.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox11.Size = new System.Drawing.Size(341, 385);
            this.groupBox11.TabIndex = 24;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "������������ ���";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(12, 356);
            this.label58.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(158, 17);
            this.label58.TabIndex = 42;
            this.label58.Text = "����� ����������, ��";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(12, 183);
            this.label59.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(85, 17);
            this.label59.TabIndex = 35;
            this.label59.Text = "������ ���";
            // 
            // avr_disconnection
            // 
            this.avr_disconnection.Location = new System.Drawing.Point(203, 353);
            this.avr_disconnection.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.avr_disconnection.Name = "avr_disconnection";
            this.avr_disconnection.Size = new System.Drawing.Size(115, 22);
            this.avr_disconnection.TabIndex = 31;
            this.avr_disconnection.Tag = "3000000";
            this.avr_disconnection.Text = "0";
            this.avr_disconnection.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // avr_permit_reset_switch
            // 
            this.avr_permit_reset_switch.AutoSize = true;
            this.avr_permit_reset_switch.Location = new System.Drawing.Point(16, 143);
            this.avr_permit_reset_switch.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.avr_permit_reset_switch.Name = "avr_permit_reset_switch";
            this.avr_permit_reset_switch.Size = new System.Drawing.Size(305, 21);
            this.avr_permit_reset_switch.TabIndex = 27;
            this.avr_permit_reset_switch.Tag = "5";
            this.avr_permit_reset_switch.Text = "���������� ������ �� ���. �����������";
            this.avr_permit_reset_switch.UseVisualStyleBackColor = true;
            this.avr_permit_reset_switch.CheckedChanged += new System.EventHandler(this.avr_CheckedChanged);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(12, 331);
            this.label61.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(138, 17);
            this.label61.TabIndex = 41;
            this.label61.Text = "����� ��������, ��";
            // 
            // avr_time_return
            // 
            this.avr_time_return.Location = new System.Drawing.Point(203, 329);
            this.avr_time_return.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.avr_time_return.Name = "avr_time_return";
            this.avr_time_return.Size = new System.Drawing.Size(115, 22);
            this.avr_time_return.TabIndex = 30;
            this.avr_time_return.Tag = "3000000";
            this.avr_time_return.Text = "0";
            this.avr_time_return.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(12, 306);
            this.label62.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(62, 17);
            this.label62.TabIndex = 40;
            this.label62.Text = "�������";
            // 
            // avr_time_abrasion
            // 
            this.avr_time_abrasion.Location = new System.Drawing.Point(203, 278);
            this.avr_time_abrasion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.avr_time_abrasion.Name = "avr_time_abrasion";
            this.avr_time_abrasion.Size = new System.Drawing.Size(115, 22);
            this.avr_time_abrasion.TabIndex = 29;
            this.avr_time_abrasion.Tag = "3000000";
            this.avr_time_abrasion.Text = "0";
            this.avr_time_abrasion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(12, 282);
            this.label63.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(173, 17);
            this.label63.TabIndex = 39;
            this.label63.Text = "����� ������������, ��";
            // 
            // avr_return
            // 
            this.avr_return.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_return.FormattingEnabled = true;
            this.avr_return.Location = new System.Drawing.Point(203, 303);
            this.avr_return.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.avr_return.Name = "avr_return";
            this.avr_return.Size = new System.Drawing.Size(115, 24);
            this.avr_return.TabIndex = 28;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(12, 257);
            this.label64.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(105, 17);
            this.label64.TabIndex = 38;
            this.label64.Text = "������������";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(12, 233);
            this.label65.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(129, 17);
            this.label65.TabIndex = 37;
            this.label65.Text = "����� ����������";
            // 
            // avr_abrasion
            // 
            this.avr_abrasion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_abrasion.FormattingEnabled = true;
            this.avr_abrasion.Location = new System.Drawing.Point(203, 252);
            this.avr_abrasion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.avr_abrasion.Name = "avr_abrasion";
            this.avr_abrasion.Size = new System.Drawing.Size(115, 24);
            this.avr_abrasion.TabIndex = 26;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(12, 208);
            this.label66.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(86, 17);
            this.label66.TabIndex = 36;
            this.label66.Text = "����������";
            // 
            // avr_reset_blocking
            // 
            this.avr_reset_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_reset_blocking.FormattingEnabled = true;
            this.avr_reset_blocking.Location = new System.Drawing.Point(203, 226);
            this.avr_reset_blocking.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.avr_reset_blocking.Name = "avr_reset_blocking";
            this.avr_reset_blocking.Size = new System.Drawing.Size(115, 24);
            this.avr_reset_blocking.TabIndex = 24;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.avr_abrasion_switch);
            this.groupBox12.Controls.Add(this.avr_supply_off);
            this.groupBox12.Controls.Add(this.avr_switch_off);
            this.groupBox12.Controls.Add(this.avr_self_off);
            this.groupBox12.Location = new System.Drawing.Point(8, 16);
            this.groupBox12.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox12.Size = new System.Drawing.Size(311, 123);
            this.groupBox12.TabIndex = 19;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "���������� �������";
            // 
            // avr_abrasion_switch
            // 
            this.avr_abrasion_switch.AutoSize = true;
            this.avr_abrasion_switch.Location = new System.Drawing.Point(8, 94);
            this.avr_abrasion_switch.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.avr_abrasion_switch.Name = "avr_abrasion_switch";
            this.avr_abrasion_switch.Size = new System.Drawing.Size(99, 21);
            this.avr_abrasion_switch.TabIndex = 26;
            this.avr_abrasion_switch.Tag = "4";
            this.avr_abrasion_switch.Text = "�� ������";
            this.avr_abrasion_switch.UseVisualStyleBackColor = true;
            this.avr_abrasion_switch.CheckedChanged += new System.EventHandler(this.avr_CheckedChanged);
            // 
            // avr_supply_off
            // 
            this.avr_supply_off.AutoSize = true;
            this.avr_supply_off.Location = new System.Drawing.Point(8, 23);
            this.avr_supply_off.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.avr_supply_off.Name = "avr_supply_off";
            this.avr_supply_off.Size = new System.Drawing.Size(101, 21);
            this.avr_supply_off.TabIndex = 23;
            this.avr_supply_off.Tag = "1";
            this.avr_supply_off.Text = "�� �������";
            this.avr_supply_off.UseVisualStyleBackColor = true;
            this.avr_supply_off.CheckedChanged += new System.EventHandler(this.avr_CheckedChanged);
            // 
            // avr_switch_off
            // 
            this.avr_switch_off.AutoSize = true;
            this.avr_switch_off.Location = new System.Drawing.Point(8, 47);
            this.avr_switch_off.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.avr_switch_off.Name = "avr_switch_off";
            this.avr_switch_off.Size = new System.Drawing.Size(132, 21);
            this.avr_switch_off.TabIndex = 24;
            this.avr_switch_off.Tag = "2";
            this.avr_switch_off.Text = "�� ����������";
            this.avr_switch_off.UseVisualStyleBackColor = true;
            this.avr_switch_off.CheckedChanged += new System.EventHandler(this.avr_CheckedChanged);
            // 
            // avr_self_off
            // 
            this.avr_self_off.AutoSize = true;
            this.avr_self_off.Location = new System.Drawing.Point(8, 70);
            this.avr_self_off.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.avr_self_off.Name = "avr_self_off";
            this.avr_self_off.Size = new System.Drawing.Size(142, 21);
            this.avr_self_off.TabIndex = 25;
            this.avr_self_off.Tag = "3";
            this.avr_self_off.Text = "��������������";
            this.avr_self_off.UseVisualStyleBackColor = true;
            this.avr_self_off.CheckedChanged += new System.EventHandler(this.avr_CheckedChanged);
            // 
            // avr_blocking
            // 
            this.avr_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_blocking.FormattingEnabled = true;
            this.avr_blocking.Location = new System.Drawing.Point(203, 201);
            this.avr_blocking.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.avr_blocking.Name = "avr_blocking";
            this.avr_blocking.Size = new System.Drawing.Size(115, 24);
            this.avr_blocking.TabIndex = 3;
            // 
            // avr_start
            // 
            this.avr_start.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_start.FormattingEnabled = true;
            this.avr_start.Location = new System.Drawing.Point(203, 175);
            this.avr_start.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.avr_start.Name = "avr_start";
            this.avr_start.Size = new System.Drawing.Size(115, 24);
            this.avr_start.TabIndex = 1;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label57);
            this.groupBox13.Controls.Add(this.lzsh_constraint);
            this.groupBox13.Controls.Add(this.lzsh_conf);
            this.groupBox13.Controls.Add(this.label48);
            this.groupBox13.Location = new System.Drawing.Point(4, 281);
            this.groupBox13.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox13.Size = new System.Drawing.Size(341, 106);
            this.groupBox13.TabIndex = 23;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "��������� ���";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(211, 27);
            this.label57.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(80, 17);
            this.label57.TabIndex = 27;
            this.label57.Text = "�������, I�";
            // 
            // lzsh_constraint
            // 
            this.lzsh_constraint.Location = new System.Drawing.Point(195, 52);
            this.lzsh_constraint.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lzsh_constraint.Name = "lzsh_constraint";
            this.lzsh_constraint.Size = new System.Drawing.Size(115, 22);
            this.lzsh_constraint.TabIndex = 24;
            this.lzsh_constraint.Tag = "40";
            this.lzsh_constraint.Text = "0";
            this.lzsh_constraint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lzsh_conf
            // 
            this.lzsh_conf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lzsh_conf.FormattingEnabled = true;
            this.lzsh_conf.Location = new System.Drawing.Point(15, 50);
            this.lzsh_conf.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lzsh_conf.Name = "lzsh_conf";
            this.lzsh_conf.Size = new System.Drawing.Size(115, 24);
            this.lzsh_conf.TabIndex = 18;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(21, 27);
            this.label48.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(104, 17);
            this.label48.TabIndex = 26;
            this.label48.Text = "������������";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.label55);
            this.groupBox14.Controls.Add(this.label47);
            this.groupBox14.Controls.Add(this.label56);
            this.groupBox14.Controls.Add(this.apv_time_4krat);
            this.groupBox14.Controls.Add(this.label52);
            this.groupBox14.Controls.Add(this.apv_time_3krat);
            this.groupBox14.Controls.Add(this.label53);
            this.groupBox14.Controls.Add(this.apv_time_2krat);
            this.groupBox14.Controls.Add(this.label54);
            this.groupBox14.Controls.Add(this.apv_time_1krat);
            this.groupBox14.Controls.Add(this.label50);
            this.groupBox14.Controls.Add(this.label51);
            this.groupBox14.Controls.Add(this.apv_time_ready);
            this.groupBox14.Controls.Add(this.label49);
            this.groupBox14.Controls.Add(this.apv_time_blocking);
            this.groupBox14.Controls.Add(this.apv_self_off);
            this.groupBox14.Controls.Add(this.apv_blocking);
            this.groupBox14.Controls.Add(this.apv_conf);
            this.groupBox14.Location = new System.Drawing.Point(4, 1);
            this.groupBox14.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox14.Size = new System.Drawing.Size(341, 272);
            this.groupBox14.TabIndex = 22;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "������������ ���";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(11, 229);
            this.label55.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(168, 17);
            this.label55.TabIndex = 34;
            this.label55.Text = "������ ��� �� �������.";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(11, 32);
            this.label47.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(104, 17);
            this.label47.TabIndex = 25;
            this.label47.Text = "������������";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(11, 204);
            this.label56.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(128, 17);
            this.label56.TabIndex = 33;
            this.label56.Text = "����� 4 �����, ��";
            // 
            // apv_time_4krat
            // 
            this.apv_time_4krat.Location = new System.Drawing.Point(195, 198);
            this.apv_time_4krat.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.apv_time_4krat.Name = "apv_time_4krat";
            this.apv_time_4krat.Size = new System.Drawing.Size(115, 22);
            this.apv_time_4krat.TabIndex = 23;
            this.apv_time_4krat.Tag = "3000000";
            this.apv_time_4krat.Text = "0";
            this.apv_time_4krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(11, 180);
            this.label52.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(128, 17);
            this.label52.TabIndex = 32;
            this.label52.Text = "����� 3 �����, ��";
            // 
            // apv_time_3krat
            // 
            this.apv_time_3krat.Location = new System.Drawing.Point(195, 174);
            this.apv_time_3krat.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.apv_time_3krat.Name = "apv_time_3krat";
            this.apv_time_3krat.Size = new System.Drawing.Size(115, 22);
            this.apv_time_3krat.TabIndex = 22;
            this.apv_time_3krat.Tag = "3000000";
            this.apv_time_3krat.Text = "0";
            this.apv_time_3krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(11, 155);
            this.label53.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(128, 17);
            this.label53.TabIndex = 31;
            this.label53.Text = "����� 2 �����, ��";
            // 
            // apv_time_2krat
            // 
            this.apv_time_2krat.Location = new System.Drawing.Point(195, 149);
            this.apv_time_2krat.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.apv_time_2krat.Name = "apv_time_2krat";
            this.apv_time_2krat.Size = new System.Drawing.Size(115, 22);
            this.apv_time_2krat.TabIndex = 21;
            this.apv_time_2krat.Tag = "3000000";
            this.apv_time_2krat.Text = "0";
            this.apv_time_2krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(11, 130);
            this.label54.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(128, 17);
            this.label54.TabIndex = 30;
            this.label54.Text = "����� 1 �����, ��";
            // 
            // apv_time_1krat
            // 
            this.apv_time_1krat.Location = new System.Drawing.Point(195, 124);
            this.apv_time_1krat.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.apv_time_1krat.Name = "apv_time_1krat";
            this.apv_time_1krat.Size = new System.Drawing.Size(115, 22);
            this.apv_time_1krat.TabIndex = 20;
            this.apv_time_1krat.Tag = "3000000";
            this.apv_time_1krat.Text = "0";
            this.apv_time_1krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(11, 106);
            this.label50.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(151, 17);
            this.label50.TabIndex = 29;
            this.label50.Text = "����� ����������, ��";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(11, 81);
            this.label51.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(155, 17);
            this.label51.TabIndex = 28;
            this.label51.Text = "����� ����������, ��";
            // 
            // apv_time_ready
            // 
            this.apv_time_ready.Location = new System.Drawing.Point(195, 100);
            this.apv_time_ready.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.apv_time_ready.Name = "apv_time_ready";
            this.apv_time_ready.Size = new System.Drawing.Size(115, 22);
            this.apv_time_ready.TabIndex = 19;
            this.apv_time_ready.Tag = "3000000";
            this.apv_time_ready.Text = "0";
            this.apv_time_ready.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(11, 57);
            this.label49.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(86, 17);
            this.label49.TabIndex = 27;
            this.label49.Text = "����������";
            // 
            // apv_time_blocking
            // 
            this.apv_time_blocking.Location = new System.Drawing.Point(195, 75);
            this.apv_time_blocking.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.apv_time_blocking.Name = "apv_time_blocking";
            this.apv_time_blocking.Size = new System.Drawing.Size(115, 22);
            this.apv_time_blocking.TabIndex = 18;
            this.apv_time_blocking.Tag = "3000000";
            this.apv_time_blocking.Text = "0";
            this.apv_time_blocking.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // apv_self_off
            // 
            this.apv_self_off.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apv_self_off.FormattingEnabled = true;
            this.apv_self_off.Location = new System.Drawing.Point(195, 223);
            this.apv_self_off.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.apv_self_off.Name = "apv_self_off";
            this.apv_self_off.Size = new System.Drawing.Size(115, 24);
            this.apv_self_off.TabIndex = 17;
            // 
            // apv_blocking
            // 
            this.apv_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apv_blocking.FormattingEnabled = true;
            this.apv_blocking.Location = new System.Drawing.Point(195, 49);
            this.apv_blocking.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.apv_blocking.Name = "apv_blocking";
            this.apv_blocking.Size = new System.Drawing.Size(115, 24);
            this.apv_blocking.TabIndex = 3;
            // 
            // apv_conf
            // 
            this.apv_conf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apv_conf.FormattingEnabled = true;
            this.apv_conf.Location = new System.Drawing.Point(195, 23);
            this.apv_conf.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.apv_conf.Name = "apv_conf";
            this.apv_conf.Size = new System.Drawing.Size(115, 24);
            this.apv_conf.TabIndex = 1;
            // 
            // _engineDefensesPage
            // 
            this._engineDefensesPage.Controls.Add(this._engineDefensesGrid);
            this._engineDefensesPage.Controls.Add(this.groupBox19);
            this._engineDefensesPage.Controls.Add(this.groupBox18);
            this._engineDefensesPage.Location = new System.Drawing.Point(4, 25);
            this._engineDefensesPage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._engineDefensesPage.Name = "_engineDefensesPage";
            this._engineDefensesPage.Size = new System.Drawing.Size(1540, 748);
            this._engineDefensesPage.TabIndex = 6;
            this._engineDefensesPage.Text = "������ ���������";
            this._engineDefensesPage.UseVisualStyleBackColor = true;
            // 
            // _engineDefensesGrid
            // 
            this._engineDefensesGrid.AllowUserToAddRows = false;
            this._engineDefensesGrid.AllowUserToDeleteRows = false;
            this._engineDefensesGrid.AllowUserToResizeColumns = false;
            this._engineDefensesGrid.AllowUserToResizeRows = false;
            this._engineDefensesGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._engineDefensesGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._engineDefensesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._engineDefensesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._engineDefenseNameCol,
            this._engineDefenseModeCol,
            this._engineDefenseConstraintCol,
            this._engineDefenseUROVcol,
            this._engineDefenseAVRcol,
            this._engineDefenseAPVcol});
            this._engineDefensesGrid.Location = new System.Drawing.Point(11, 313);
            this._engineDefensesGrid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._engineDefensesGrid.MultiSelect = false;
            this._engineDefensesGrid.Name = "_engineDefensesGrid";
            this._engineDefensesGrid.RowHeadersVisible = false;
            this._engineDefensesGrid.RowHeadersWidth = 51;
            this._engineDefensesGrid.RowTemplate.Height = 24;
            this._engineDefensesGrid.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this._engineDefensesGrid.Size = new System.Drawing.Size(443, 91);
            this._engineDefensesGrid.TabIndex = 2;
            this._engineDefensesGrid.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this._engineDefensesGrid_CellBeginEdit);
            // 
            // _engineDefenseNameCol
            // 
            this._engineDefenseNameCol.HeaderText = "";
            this._engineDefenseNameCol.MinimumWidth = 6;
            this._engineDefenseNameCol.Name = "_engineDefenseNameCol";
            this._engineDefenseNameCol.ReadOnly = true;
            this._engineDefenseNameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._engineDefenseNameCol.Width = 6;
            // 
            // _engineDefenseModeCol
            // 
            this._engineDefenseModeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._engineDefenseModeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._engineDefenseModeCol.HeaderText = "�����";
            this._engineDefenseModeCol.MinimumWidth = 6;
            this._engineDefenseModeCol.Name = "_engineDefenseModeCol";
            // 
            // _engineDefenseConstraintCol
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.NullValue = null;
            this._engineDefenseConstraintCol.DefaultCellStyle = dataGridViewCellStyle6;
            this._engineDefenseConstraintCol.HeaderText = "Q��, %";
            this._engineDefenseConstraintCol.MinimumWidth = 6;
            this._engineDefenseConstraintCol.Name = "_engineDefenseConstraintCol";
            this._engineDefenseConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._engineDefenseConstraintCol.Width = 60;
            // 
            // _engineDefenseUROVcol
            // 
            this._engineDefenseUROVcol.HeaderText = "����";
            this._engineDefenseUROVcol.MinimumWidth = 6;
            this._engineDefenseUROVcol.Name = "_engineDefenseUROVcol";
            this._engineDefenseUROVcol.Width = 52;
            // 
            // _engineDefenseAVRcol
            // 
            this._engineDefenseAVRcol.HeaderText = "���";
            this._engineDefenseAVRcol.MinimumWidth = 6;
            this._engineDefenseAVRcol.Name = "_engineDefenseAVRcol";
            this._engineDefenseAVRcol.Width = 41;
            // 
            // _engineDefenseAPVcol
            // 
            this._engineDefenseAPVcol.HeaderText = "���";
            this._engineDefenseAPVcol.MinimumWidth = 6;
            this._engineDefenseAPVcol.Name = "_engineDefenseAPVcol";
            this._engineDefenseAPVcol.Width = 42;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this._engineBlockTimeBox);
            this.groupBox19.Controls.Add(this.label34);
            this.groupBox19.Controls.Add(this.label60);
            this.groupBox19.Controls.Add(this.label38);
            this.groupBox19.Controls.Add(this._engineQmodeCombo);
            this.groupBox19.Controls.Add(this.label11);
            this.groupBox19.Controls.Add(this._engineQtimeBox);
            this.groupBox19.Controls.Add(this._engineBlockPuskBox);
            this.groupBox19.Controls.Add(this.label37);
            this.groupBox19.Controls.Add(this.label36);
            this.groupBox19.Controls.Add(this._engineBlockDurationBox);
            this.groupBox19.Controls.Add(this._engineQconstraintBox);
            this.groupBox19.Controls.Add(this.label35);
            this.groupBox19.Controls.Add(this.label40);
            this.groupBox19.Controls.Add(this._engineBlockHeatBox);
            this.groupBox19.Controls.Add(this.label33);
            this.groupBox19.Location = new System.Drawing.Point(11, 155);
            this.groupBox19.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox19.Size = new System.Drawing.Size(520, 150);
            this.groupBox19.TabIndex = 1;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "���������� �����";
            // 
            // _engineBlockTimeBox
            // 
            this._engineBlockTimeBox.Location = new System.Drawing.Point(384, 113);
            this._engineBlockTimeBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._engineBlockTimeBox.Name = "_engineBlockTimeBox";
            this._engineBlockTimeBox.Size = new System.Drawing.Size(91, 22);
            this._engineBlockTimeBox.TabIndex = 30;
            this._engineBlockTimeBox.Tag = "65000";
            this._engineBlockTimeBox.Text = "0";
            this._engineBlockTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(28, 64);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(51, 17);
            this.label34.TabIndex = 20;
            this.label34.Text = "�����";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(265, 20);
            this.label60.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(233, 17);
            this.label60.TabIndex = 43;
            this.label60.Text = "���������� �� ���������� ������";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(301, 117);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(55, 17);
            this.label38.TabIndex = 29;
            this.label38.Text = "����, �";
            // 
            // _engineQmodeCombo
            // 
            this._engineQmodeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._engineQmodeCombo.FormattingEnabled = true;
            this._engineQmodeCombo.Location = new System.Drawing.Point(113, 60);
            this._engineQmodeCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._engineQmodeCombo.Name = "_engineQmodeCombo";
            this._engineQmodeCombo.Size = new System.Drawing.Size(119, 24);
            this._engineQmodeCombo.TabIndex = 19;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(16, 20);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(189, 34);
            this.label11.TabIndex = 42;
            this.label11.Text = "���������� �� ��������� ���������";
            // 
            // _engineQtimeBox
            // 
            this._engineQtimeBox.Location = new System.Drawing.Point(113, 110);
            this._engineQtimeBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._engineQtimeBox.Name = "_engineQtimeBox";
            this._engineQtimeBox.Size = new System.Drawing.Size(91, 22);
            this._engineQtimeBox.TabIndex = 18;
            this._engineQtimeBox.Tag = "65000";
            this._engineQtimeBox.Text = "0";
            this._engineQtimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _engineBlockPuskBox
            // 
            this._engineBlockPuskBox.Location = new System.Drawing.Point(384, 43);
            this._engineBlockPuskBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._engineBlockPuskBox.Name = "_engineBlockPuskBox";
            this._engineBlockPuskBox.Size = new System.Drawing.Size(91, 22);
            this._engineBlockPuskBox.TabIndex = 24;
            this._engineBlockPuskBox.Tag = "10";
            this._engineBlockPuskBox.Text = "0";
            this._engineBlockPuskBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(28, 113);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(55, 17);
            this.label37.TabIndex = 17;
            this.label37.Text = "T���, �";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(301, 47);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(47, 17);
            this.label36.TabIndex = 23;
            this.label36.Text = "N����";
            // 
            // _engineBlockDurationBox
            // 
            this._engineBlockDurationBox.Location = new System.Drawing.Point(384, 90);
            this._engineBlockDurationBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._engineBlockDurationBox.Name = "_engineBlockDurationBox";
            this._engineBlockDurationBox.Size = new System.Drawing.Size(91, 22);
            this._engineBlockDurationBox.TabIndex = 28;
            this._engineBlockDurationBox.Tag = "65000";
            this._engineBlockDurationBox.Text = "0";
            this._engineBlockDurationBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _engineQconstraintBox
            // 
            this._engineQconstraintBox.Location = new System.Drawing.Point(113, 86);
            this._engineQconstraintBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._engineQconstraintBox.Name = "_engineQconstraintBox";
            this._engineQconstraintBox.Size = new System.Drawing.Size(91, 22);
            this._engineQconstraintBox.TabIndex = 16;
            this._engineQconstraintBox.Tag = "256";
            this._engineQconstraintBox.Text = "0";
            this._engineQconstraintBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(301, 70);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(39, 17);
            this.label35.TabIndex = 25;
            this.label35.Text = "N���";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(28, 90);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(62, 17);
            this.label40.TabIndex = 15;
            this.label40.Text = "Q���, %";
            // 
            // _engineBlockHeatBox
            // 
            this._engineBlockHeatBox.Location = new System.Drawing.Point(384, 66);
            this._engineBlockHeatBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._engineBlockHeatBox.Name = "_engineBlockHeatBox";
            this._engineBlockHeatBox.Size = new System.Drawing.Size(91, 22);
            this._engineBlockHeatBox.TabIndex = 26;
            this._engineBlockHeatBox.Tag = "10";
            this._engineBlockHeatBox.Text = "0";
            this._engineBlockHeatBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(301, 94);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(63, 17);
            this.label33.TabIndex = 27;
            this.label33.Text = "T����, �";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.label32);
            this.groupBox18.Controls.Add(this.label31);
            this.groupBox18.Controls.Add(this._engineNpuskCombo);
            this.groupBox18.Controls.Add(this._engineQresetCombo);
            this.groupBox18.Controls.Add(this._engineHeatPuskConstraintBox);
            this.groupBox18.Controls.Add(this.label29);
            this.groupBox18.Controls.Add(this._enginePuskTimeBox);
            this.groupBox18.Controls.Add(this.label30);
            this.groupBox18.Controls.Add(this._engineIpConstraintBox);
            this.groupBox18.Controls.Add(this.label27);
            this.groupBox18.Controls.Add(this._engineInBox);
            this.groupBox18.Controls.Add(this.label28);
            this.groupBox18.Controls.Add(this._engineCoolingTimeBox);
            this.groupBox18.Controls.Add(this.label26);
            this.groupBox18.Controls.Add(this._engineHeatingTimeBox);
            this.groupBox18.Controls.Add(this.label25);
            this.groupBox18.Location = new System.Drawing.Point(11, 4);
            this.groupBox18.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox18.Size = new System.Drawing.Size(520, 144);
            this.groupBox18.TabIndex = 0;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "������ ���������";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(256, 110);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(99, 17);
            this.label32.TabIndex = 15;
            this.label32.Text = "���� N ����� ";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(16, 110);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(96, 17);
            this.label31.TabIndex = 14;
            this.label31.Text = "���� Q �����";
            // 
            // _engineNpuskCombo
            // 
            this._engineNpuskCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._engineNpuskCombo.FormattingEnabled = true;
            this._engineNpuskCombo.Location = new System.Drawing.Point(372, 105);
            this._engineNpuskCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._engineNpuskCombo.Name = "_engineNpuskCombo";
            this._engineNpuskCombo.Size = new System.Drawing.Size(133, 24);
            this._engineNpuskCombo.TabIndex = 13;
            // 
            // _engineQresetCombo
            // 
            this._engineQresetCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._engineQresetCombo.FormattingEnabled = true;
            this._engineQresetCombo.Location = new System.Drawing.Point(124, 106);
            this._engineQresetCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._engineQresetCombo.Name = "_engineQresetCombo";
            this._engineQresetCombo.Size = new System.Drawing.Size(119, 24);
            this._engineQresetCombo.TabIndex = 12;
            // 
            // _engineHeatPuskConstraintBox
            // 
            this._engineHeatPuskConstraintBox.Location = new System.Drawing.Point(415, 70);
            this._engineHeatPuskConstraintBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._engineHeatPuskConstraintBox.Name = "_engineHeatPuskConstraintBox";
            this._engineHeatPuskConstraintBox.Size = new System.Drawing.Size(91, 22);
            this._engineHeatPuskConstraintBox.TabIndex = 11;
            this._engineHeatPuskConstraintBox.Tag = "256";
            this._engineHeatPuskConstraintBox.Text = "0";
            this._engineHeatPuskConstraintBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(256, 74);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(60, 17);
            this.label29.TabIndex = 10;
            this.label29.Text = "Q���, %";
            // 
            // _enginePuskTimeBox
            // 
            this._enginePuskTimeBox.Location = new System.Drawing.Point(415, 47);
            this._enginePuskTimeBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._enginePuskTimeBox.Name = "_enginePuskTimeBox";
            this._enginePuskTimeBox.Size = new System.Drawing.Size(91, 22);
            this._enginePuskTimeBox.TabIndex = 9;
            this._enginePuskTimeBox.Tag = "3000000";
            this._enginePuskTimeBox.Text = "0";
            this._enginePuskTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(256, 50);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(70, 17);
            this.label30.TabIndex = 8;
            this.label30.Text = "T����, ��";
            // 
            // _engineIpConstraintBox
            // 
            this._engineIpConstraintBox.Location = new System.Drawing.Point(415, 23);
            this._engineIpConstraintBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._engineIpConstraintBox.Name = "_engineIpConstraintBox";
            this._engineIpConstraintBox.Size = new System.Drawing.Size(91, 22);
            this._engineIpConstraintBox.TabIndex = 7;
            this._engineIpConstraintBox.Tag = "40";
            this._engineIpConstraintBox.Text = "0";
            this._engineIpConstraintBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(256, 27);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(59, 17);
            this.label27.TabIndex = 6;
            this.label27.Text = "I����, I�";
            // 
            // _engineInBox
            // 
            this._engineInBox.Location = new System.Drawing.Point(152, 70);
            this._engineInBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._engineInBox.Name = "_engineInBox";
            this._engineInBox.Size = new System.Drawing.Size(91, 22);
            this._engineInBox.TabIndex = 5;
            this._engineInBox.Tag = "40";
            this._engineInBox.Text = "0";
            this._engineInBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(16, 74);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(45, 17);
            this.label28.TabIndex = 4;
            this.label28.Text = "I��, I�";
            // 
            // _engineCoolingTimeBox
            // 
            this._engineCoolingTimeBox.Location = new System.Drawing.Point(152, 47);
            this._engineCoolingTimeBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._engineCoolingTimeBox.Name = "_engineCoolingTimeBox";
            this._engineCoolingTimeBox.Size = new System.Drawing.Size(91, 22);
            this._engineCoolingTimeBox.TabIndex = 3;
            this._engineCoolingTimeBox.Tag = "65000";
            this._engineCoolingTimeBox.Text = "0";
            this._engineCoolingTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(16, 50);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(115, 17);
            this.label26.TabIndex = 2;
            this.label26.Text = "� ����������, �";
            // 
            // _engineHeatingTimeBox
            // 
            this._engineHeatingTimeBox.Location = new System.Drawing.Point(152, 23);
            this._engineHeatingTimeBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._engineHeatingTimeBox.Name = "_engineHeatingTimeBox";
            this._engineHeatingTimeBox.Size = new System.Drawing.Size(91, 22);
            this._engineHeatingTimeBox.TabIndex = 1;
            this._engineHeatingTimeBox.Tag = "65000";
            this._engineHeatingTimeBox.Text = "0";
            this._engineHeatingTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(16, 27);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(88, 17);
            this.label25.TabIndex = 0;
            this.label25.Text = "T �������, c";
            // 
            // _defendTabPage1
            // 
            this._defendTabPage1.Controls.Add(this.groupBox15);
            this._defendTabPage1.Controls.Add(this.groupBox25);
            this._defendTabPage1.Controls.Add(this.groupBox24);
            this._defendTabPage1.Location = new System.Drawing.Point(4, 25);
            this._defendTabPage1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._defendTabPage1.Name = "_defendTabPage1";
            this._defendTabPage1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._defendTabPage1.Size = new System.Drawing.Size(1540, 748);
            this._defendTabPage1.TabIndex = 8;
            this._defendTabPage1.Text = "������";
            this._defendTabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this._tokDefenseInbox);
            this.groupBox15.Controls.Add(this.label24);
            this.groupBox15.Controls.Add(this._tokDefenseI2box);
            this.groupBox15.Controls.Add(this.label23);
            this.groupBox15.Controls.Add(this._tokDefenseI0box);
            this.groupBox15.Controls.Add(this.label22);
            this.groupBox15.Controls.Add(this._tokDefenseIbox);
            this.groupBox15.Controls.Add(this.label21);
            this.groupBox15.Location = new System.Drawing.Point(520, 9);
            this.groupBox15.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox15.Size = new System.Drawing.Size(348, 57);
            this.groupBox15.TabIndex = 9;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "������������ ���� (���� ��)";
            // 
            // _tokDefenseInbox
            // 
            this._tokDefenseInbox.Location = new System.Drawing.Point(288, 18);
            this._tokDefenseInbox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tokDefenseInbox.Name = "_tokDefenseInbox";
            this._tokDefenseInbox.Size = new System.Drawing.Size(48, 22);
            this._tokDefenseInbox.TabIndex = 7;
            this._tokDefenseInbox.Tag = "360";
            this._tokDefenseInbox.Text = "0";
            this._tokDefenseInbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(267, 22);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(19, 17);
            this.label24.TabIndex = 6;
            this.label24.Text = "In";
            // 
            // _tokDefenseI2box
            // 
            this._tokDefenseI2box.Location = new System.Drawing.Point(203, 18);
            this._tokDefenseI2box.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tokDefenseI2box.Name = "_tokDefenseI2box";
            this._tokDefenseI2box.Size = new System.Drawing.Size(48, 22);
            this._tokDefenseI2box.TabIndex = 5;
            this._tokDefenseI2box.Tag = "360";
            this._tokDefenseI2box.Text = "0";
            this._tokDefenseI2box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(181, 22);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(19, 17);
            this.label23.TabIndex = 4;
            this.label23.Text = "I2";
            // 
            // _tokDefenseI0box
            // 
            this._tokDefenseI0box.Location = new System.Drawing.Point(121, 18);
            this._tokDefenseI0box.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tokDefenseI0box.Name = "_tokDefenseI0box";
            this._tokDefenseI0box.Size = new System.Drawing.Size(48, 22);
            this._tokDefenseI0box.TabIndex = 3;
            this._tokDefenseI0box.Tag = "360";
            this._tokDefenseI0box.Text = "0";
            this._tokDefenseI0box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(100, 22);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(19, 17);
            this.label22.TabIndex = 2;
            this.label22.Text = "I0";
            // 
            // _tokDefenseIbox
            // 
            this._tokDefenseIbox.Location = new System.Drawing.Point(41, 18);
            this._tokDefenseIbox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tokDefenseIbox.Name = "_tokDefenseIbox";
            this._tokDefenseIbox.Size = new System.Drawing.Size(48, 22);
            this._tokDefenseIbox.TabIndex = 1;
            this._tokDefenseIbox.Tag = "360";
            this._tokDefenseIbox.Text = "0";
            this._tokDefenseIbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(20, 22);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(11, 17);
            this.label21.TabIndex = 0;
            this.label21.Text = "I";
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.tabControl1);
            this.groupBox25.Location = new System.Drawing.Point(12, 74);
            this.groupBox25.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox25.Size = new System.Drawing.Size(1515, 663);
            this.groupBox25.TabIndex = 1;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "������";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this._tokDefendTabPage);
            this.tabControl1.Controls.Add(this._voltageDefendTabPage);
            this.tabControl1.Controls.Add(this._frequencyDefendTabPage);
            this.tabControl1.Location = new System.Drawing.Point(9, 25);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1497, 631);
            this.tabControl1.TabIndex = 0;
            // 
            // _tokDefendTabPage
            // 
            this._tokDefendTabPage.Controls.Add(this._tokDefenseGrid4);
            this._tokDefendTabPage.Controls.Add(this._tokDefenseGrid3);
            this._tokDefendTabPage.Controls.Add(this._tokDefenseGrid2);
            this._tokDefendTabPage.Controls.Add(this._tokDefenseGrid1);
            this._tokDefendTabPage.Location = new System.Drawing.Point(4, 25);
            this._tokDefendTabPage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tokDefendTabPage.Name = "_tokDefendTabPage";
            this._tokDefendTabPage.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tokDefendTabPage.Size = new System.Drawing.Size(1489, 602);
            this._tokDefendTabPage.TabIndex = 0;
            this._tokDefendTabPage.Text = "������� ������";
            this._tokDefendTabPage.UseVisualStyleBackColor = true;
            // 
            // _tokDefenseGrid4
            // 
            this._tokDefenseGrid4.AllowUserToAddRows = false;
            this._tokDefenseGrid4.AllowUserToDeleteRows = false;
            this._tokDefenseGrid4.AllowUserToResizeColumns = false;
            this._tokDefenseGrid4.AllowUserToResizeRows = false;
            this._tokDefenseGrid4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tokDefenseGrid4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid4.BackgroundColor = System.Drawing.SystemColors.Control;
            this._tokDefenseGrid4.ColumnHeadersHeight = 29;
            this._tokDefenseGrid4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._tokDefenseGrid4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefense4NameCol,
            this._tokDefense4ModeCol,
            this._tokDefense4BlockNumberCol,
            this._tokDefense4UpuskCol,
            this._tokDefense4PuskConstraintCol,
            this._tokDefense4WorkConstraintCol,
            this._tokDefense4WorkTimeCol,
            this._tokDefense4SpeedupCol,
            this._tokDefense4SpeedupTimeCol,
            this._tokDefense4UROVCol,
            this._tokDefense4APVCol,
            this._tokDefense4AVRCol});
            this._tokDefenseGrid4.Location = new System.Drawing.Point(4, 450);
            this._tokDefenseGrid4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tokDefenseGrid4.MultiSelect = false;
            this._tokDefenseGrid4.Name = "_tokDefenseGrid4";
            this._tokDefenseGrid4.RowHeadersVisible = false;
            this._tokDefenseGrid4.RowHeadersWidth = 51;
            this._tokDefenseGrid4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid4.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this._tokDefenseGrid4.RowTemplate.Height = 24;
            this._tokDefenseGrid4.Size = new System.Drawing.Size(1479, 95);
            this._tokDefenseGrid4.TabIndex = 8;
            this._tokDefenseGrid4.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this._tokDefenseGrid4_CellBeginEdit);
            // 
            // _tokDefense4NameCol
            // 
            this._tokDefense4NameCol.Frozen = true;
            this._tokDefense4NameCol.HeaderText = "";
            this._tokDefense4NameCol.MinimumWidth = 50;
            this._tokDefense4NameCol.Name = "_tokDefense4NameCol";
            this._tokDefense4NameCol.ReadOnly = true;
            this._tokDefense4NameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4NameCol.Width = 50;
            // 
            // _tokDefense4ModeCol
            // 
            this._tokDefense4ModeCol.HeaderText = "�����";
            this._tokDefense4ModeCol.MinimumWidth = 6;
            this._tokDefense4ModeCol.Name = "_tokDefense4ModeCol";
            this._tokDefense4ModeCol.Width = 57;
            // 
            // _tokDefense4BlockNumberCol
            // 
            this._tokDefense4BlockNumberCol.HeaderText = "����������";
            this._tokDefense4BlockNumberCol.MinimumWidth = 6;
            this._tokDefense4BlockNumberCol.Name = "_tokDefense4BlockNumberCol";
            this._tokDefense4BlockNumberCol.Width = 92;
            // 
            // _tokDefense4UpuskCol
            // 
            this._tokDefense4UpuskCol.HeaderText = "���� �� U";
            this._tokDefense4UpuskCol.MinimumWidth = 6;
            this._tokDefense4UpuskCol.Name = "_tokDefense4UpuskCol";
            this._tokDefense4UpuskCol.Width = 79;
            // 
            // _tokDefense4PuskConstraintCol
            // 
            dataGridViewCellStyle7.NullValue = null;
            this._tokDefense4PuskConstraintCol.DefaultCellStyle = dataGridViewCellStyle7;
            this._tokDefense4PuskConstraintCol.HeaderText = "U����, B";
            this._tokDefense4PuskConstraintCol.MinimumWidth = 6;
            this._tokDefense4PuskConstraintCol.Name = "_tokDefense4PuskConstraintCol";
            this._tokDefense4PuskConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4PuskConstraintCol.Width = 70;
            // 
            // _tokDefense4WorkConstraintCol
            // 
            dataGridViewCellStyle8.NullValue = null;
            this._tokDefense4WorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle8;
            this._tokDefense4WorkConstraintCol.HeaderText = "I��, I�";
            this._tokDefense4WorkConstraintCol.MinimumWidth = 6;
            this._tokDefense4WorkConstraintCol.Name = "_tokDefense4WorkConstraintCol";
            this._tokDefense4WorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4WorkConstraintCol.Width = 51;
            // 
            // _tokDefense4WorkTimeCol
            // 
            dataGridViewCellStyle9.NullValue = null;
            this._tokDefense4WorkTimeCol.DefaultCellStyle = dataGridViewCellStyle9;
            this._tokDefense4WorkTimeCol.HeaderText = "T��, ��/����.";
            this._tokDefense4WorkTimeCol.MinimumWidth = 6;
            this._tokDefense4WorkTimeCol.Name = "_tokDefense4WorkTimeCol";
            this._tokDefense4WorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4WorkTimeCol.Width = 103;
            // 
            // _tokDefense4SpeedupCol
            // 
            this._tokDefense4SpeedupCol.HeaderText = "���.";
            this._tokDefense4SpeedupCol.MinimumWidth = 6;
            this._tokDefense4SpeedupCol.Name = "_tokDefense4SpeedupCol";
            this._tokDefense4SpeedupCol.Width = 41;
            // 
            // _tokDefense4SpeedupTimeCol
            // 
            this._tokDefense4SpeedupTimeCol.HeaderText = "T���, ��";
            this._tokDefense4SpeedupTimeCol.MinimumWidth = 6;
            this._tokDefense4SpeedupTimeCol.Name = "_tokDefense4SpeedupTimeCol";
            this._tokDefense4SpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4SpeedupTimeCol.Width = 68;
            // 
            // _tokDefense4UROVCol
            // 
            this._tokDefense4UROVCol.HeaderText = "����";
            this._tokDefense4UROVCol.MinimumWidth = 6;
            this._tokDefense4UROVCol.Name = "_tokDefense4UROVCol";
            this._tokDefense4UROVCol.Width = 52;
            // 
            // _tokDefense4APVCol
            // 
            this._tokDefense4APVCol.HeaderText = "���";
            this._tokDefense4APVCol.MinimumWidth = 6;
            this._tokDefense4APVCol.Name = "_tokDefense4APVCol";
            this._tokDefense4APVCol.Width = 42;
            // 
            // _tokDefense4AVRCol
            // 
            this._tokDefense4AVRCol.HeaderText = "���";
            this._tokDefense4AVRCol.MinimumWidth = 6;
            this._tokDefense4AVRCol.Name = "_tokDefense4AVRCol";
            this._tokDefense4AVRCol.Width = 41;
            // 
            // _tokDefenseGrid3
            // 
            this._tokDefenseGrid3.AllowUserToAddRows = false;
            this._tokDefenseGrid3.AllowUserToDeleteRows = false;
            this._tokDefenseGrid3.AllowUserToResizeColumns = false;
            this._tokDefenseGrid3.AllowUserToResizeRows = false;
            this._tokDefenseGrid3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tokDefenseGrid3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid3.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._tokDefenseGrid3.BackgroundColor = System.Drawing.SystemColors.Control;
            this._tokDefenseGrid3.ColumnHeadersHeight = 29;
            this._tokDefenseGrid3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._tokDefenseGrid3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefense3NameCol,
            this._tokDefense3ModeCol,
            this._tokDefense3BlockingNumberCol,
            this._tokDefense3UpuskCol,
            this._tokDefense3PuskConstraintCol,
            this._tokDefense3DirectionCol,
            this._tokDefense3BlockingExistCol,
            this._tokDefense3ParameterCol,
            this._tokDefense3WorkConstraintCol,
            this._tokDefense3FeatureCol,
            this._tokDefense3WorkTimeCol,
            this._tokDefense3SpeedupCol,
            this._tokDefense3SpeedupTimeCol,
            this._tokDefense3UROVCol,
            this._tokDefense3APVCol,
            this._tokDefense3AVRCol});
            this._tokDefenseGrid3.Location = new System.Drawing.Point(4, 228);
            this._tokDefenseGrid3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tokDefenseGrid3.MultiSelect = false;
            this._tokDefenseGrid3.Name = "_tokDefenseGrid3";
            this._tokDefenseGrid3.RowHeadersVisible = false;
            this._tokDefenseGrid3.RowHeadersWidth = 51;
            this._tokDefenseGrid3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid3.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this._tokDefenseGrid3.RowTemplate.Height = 24;
            this._tokDefenseGrid3.Size = new System.Drawing.Size(1479, 215);
            this._tokDefenseGrid3.TabIndex = 7;
            this._tokDefenseGrid3.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this._tokDefenseGrid3_CellBeginEdit);
            // 
            // _tokDefense3NameCol
            // 
            this._tokDefense3NameCol.Frozen = true;
            this._tokDefense3NameCol.HeaderText = "";
            this._tokDefense3NameCol.MinimumWidth = 50;
            this._tokDefense3NameCol.Name = "_tokDefense3NameCol";
            this._tokDefense3NameCol.ReadOnly = true;
            this._tokDefense3NameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3NameCol.Width = 50;
            // 
            // _tokDefense3ModeCol
            // 
            this._tokDefense3ModeCol.HeaderText = "�����";
            this._tokDefense3ModeCol.MinimumWidth = 6;
            this._tokDefense3ModeCol.Name = "_tokDefense3ModeCol";
            this._tokDefense3ModeCol.Width = 57;
            // 
            // _tokDefense3BlockingNumberCol
            // 
            this._tokDefense3BlockingNumberCol.HeaderText = "����������";
            this._tokDefense3BlockingNumberCol.MinimumWidth = 6;
            this._tokDefense3BlockingNumberCol.Name = "_tokDefense3BlockingNumberCol";
            this._tokDefense3BlockingNumberCol.Width = 92;
            // 
            // _tokDefense3UpuskCol
            // 
            this._tokDefense3UpuskCol.HeaderText = "���� �� U";
            this._tokDefense3UpuskCol.MinimumWidth = 6;
            this._tokDefense3UpuskCol.Name = "_tokDefense3UpuskCol";
            this._tokDefense3UpuskCol.Width = 79;
            // 
            // _tokDefense3PuskConstraintCol
            // 
            dataGridViewCellStyle11.NullValue = null;
            this._tokDefense3PuskConstraintCol.DefaultCellStyle = dataGridViewCellStyle11;
            this._tokDefense3PuskConstraintCol.HeaderText = "U����, B";
            this._tokDefense3PuskConstraintCol.MinimumWidth = 6;
            this._tokDefense3PuskConstraintCol.Name = "_tokDefense3PuskConstraintCol";
            this._tokDefense3PuskConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3PuskConstraintCol.Width = 70;
            // 
            // _tokDefense3DirectionCol
            // 
            this._tokDefense3DirectionCol.HeaderText = "�����������";
            this._tokDefense3DirectionCol.MinimumWidth = 6;
            this._tokDefense3DirectionCol.Name = "_tokDefense3DirectionCol";
            this._tokDefense3DirectionCol.Width = 103;
            // 
            // _tokDefense3BlockingExistCol
            // 
            this._tokDefense3BlockingExistCol.HeaderText = "����������";
            this._tokDefense3BlockingExistCol.MinimumWidth = 6;
            this._tokDefense3BlockingExistCol.Name = "_tokDefense3BlockingExistCol";
            this._tokDefense3BlockingExistCol.Width = 92;
            // 
            // _tokDefense3ParameterCol
            // 
            this._tokDefense3ParameterCol.HeaderText = "��������";
            this._tokDefense3ParameterCol.MinimumWidth = 6;
            this._tokDefense3ParameterCol.Name = "_tokDefense3ParameterCol";
            this._tokDefense3ParameterCol.Width = 80;
            // 
            // _tokDefense3WorkConstraintCol
            // 
            dataGridViewCellStyle12.NullValue = null;
            this._tokDefense3WorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle12;
            this._tokDefense3WorkConstraintCol.HeaderText = "I��, I�";
            this._tokDefense3WorkConstraintCol.MinimumWidth = 6;
            this._tokDefense3WorkConstraintCol.Name = "_tokDefense3WorkConstraintCol";
            this._tokDefense3WorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3WorkConstraintCol.Width = 51;
            // 
            // _tokDefense3FeatureCol
            // 
            this._tokDefense3FeatureCol.HeaderText = "���-��";
            this._tokDefense3FeatureCol.MinimumWidth = 6;
            this._tokDefense3FeatureCol.Name = "_tokDefense3FeatureCol";
            this._tokDefense3FeatureCol.Width = 59;
            // 
            // _tokDefense3WorkTimeCol
            // 
            dataGridViewCellStyle13.NullValue = null;
            this._tokDefense3WorkTimeCol.DefaultCellStyle = dataGridViewCellStyle13;
            this._tokDefense3WorkTimeCol.HeaderText = "T��, ��/����.";
            this._tokDefense3WorkTimeCol.MinimumWidth = 6;
            this._tokDefense3WorkTimeCol.Name = "_tokDefense3WorkTimeCol";
            this._tokDefense3WorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3WorkTimeCol.Width = 103;
            // 
            // _tokDefense3SpeedupCol
            // 
            this._tokDefense3SpeedupCol.HeaderText = "���.";
            this._tokDefense3SpeedupCol.MinimumWidth = 6;
            this._tokDefense3SpeedupCol.Name = "_tokDefense3SpeedupCol";
            this._tokDefense3SpeedupCol.Width = 41;
            // 
            // _tokDefense3SpeedupTimeCol
            // 
            dataGridViewCellStyle14.NullValue = null;
            this._tokDefense3SpeedupTimeCol.DefaultCellStyle = dataGridViewCellStyle14;
            this._tokDefense3SpeedupTimeCol.HeaderText = "T���, ��";
            this._tokDefense3SpeedupTimeCol.MinimumWidth = 6;
            this._tokDefense3SpeedupTimeCol.Name = "_tokDefense3SpeedupTimeCol";
            this._tokDefense3SpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3SpeedupTimeCol.Width = 68;
            // 
            // _tokDefense3UROVCol
            // 
            this._tokDefense3UROVCol.HeaderText = "����";
            this._tokDefense3UROVCol.MinimumWidth = 6;
            this._tokDefense3UROVCol.Name = "_tokDefense3UROVCol";
            this._tokDefense3UROVCol.Width = 52;
            // 
            // _tokDefense3APVCol
            // 
            this._tokDefense3APVCol.HeaderText = "���";
            this._tokDefense3APVCol.MinimumWidth = 6;
            this._tokDefense3APVCol.Name = "_tokDefense3APVCol";
            this._tokDefense3APVCol.Width = 42;
            // 
            // _tokDefense3AVRCol
            // 
            this._tokDefense3AVRCol.HeaderText = "���";
            this._tokDefense3AVRCol.MinimumWidth = 6;
            this._tokDefense3AVRCol.Name = "_tokDefense3AVRCol";
            this._tokDefense3AVRCol.Width = 41;
            // 
            // _tokDefenseGrid2
            // 
            this._tokDefenseGrid2.AllowUserToAddRows = false;
            this._tokDefenseGrid2.AllowUserToDeleteRows = false;
            this._tokDefenseGrid2.AllowUserToResizeColumns = false;
            this._tokDefenseGrid2.AllowUserToResizeRows = false;
            this._tokDefenseGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tokDefenseGrid2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._tokDefenseGrid2.BackgroundColor = System.Drawing.SystemColors.Control;
            this._tokDefenseGrid2.ColumnHeadersHeight = 29;
            this._tokDefenseGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._tokDefenseGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefense2NameCol,
            this._tokDefense2ModeCol,
            this._tokDefense2BlockNumberCol,
            this._tokDefense2PuskUCol,
            this._tokDefense2PuskConstraintCol,
            this._tokDefense2DirectionCol,
            this._tokDefense2BlockExistCol,
            this._tokDefense2ParameterCol,
            this._tokDefense2WorkConstraintCol,
            this._tokDefense2FeatureCol,
            this._tokDefense2WorkTimeCol,
            this._tokDefense2EngineCol,
            this._tokDefense2UROVCol,
            this._tokDefense2APVCol,
            this._tokDefense2AVRCol});
            this._tokDefenseGrid2.Location = new System.Drawing.Point(4, 116);
            this._tokDefenseGrid2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tokDefenseGrid2.MultiSelect = false;
            this._tokDefenseGrid2.Name = "_tokDefenseGrid2";
            this._tokDefenseGrid2.RowHeadersVisible = false;
            this._tokDefenseGrid2.RowHeadersWidth = 51;
            this._tokDefenseGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid2.RowsDefaultCellStyle = dataGridViewCellStyle19;
            this._tokDefenseGrid2.RowTemplate.Height = 24;
            this._tokDefenseGrid2.Size = new System.Drawing.Size(1479, 105);
            this._tokDefenseGrid2.TabIndex = 6;
            this._tokDefenseGrid2.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this._tokDefenseGrid2_CellBeginEdit);
            // 
            // _tokDefense2NameCol
            // 
            this._tokDefense2NameCol.Frozen = true;
            this._tokDefense2NameCol.HeaderText = "";
            this._tokDefense2NameCol.MinimumWidth = 50;
            this._tokDefense2NameCol.Name = "_tokDefense2NameCol";
            this._tokDefense2NameCol.ReadOnly = true;
            this._tokDefense2NameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense2NameCol.Width = 50;
            // 
            // _tokDefense2ModeCol
            // 
            this._tokDefense2ModeCol.HeaderText = "�����";
            this._tokDefense2ModeCol.MinimumWidth = 6;
            this._tokDefense2ModeCol.Name = "_tokDefense2ModeCol";
            this._tokDefense2ModeCol.Width = 57;
            // 
            // _tokDefense2BlockNumberCol
            // 
            this._tokDefense2BlockNumberCol.HeaderText = "����������";
            this._tokDefense2BlockNumberCol.MinimumWidth = 6;
            this._tokDefense2BlockNumberCol.Name = "_tokDefense2BlockNumberCol";
            this._tokDefense2BlockNumberCol.Width = 92;
            // 
            // _tokDefense2PuskUCol
            // 
            this._tokDefense2PuskUCol.HeaderText = "���� �� U";
            this._tokDefense2PuskUCol.MinimumWidth = 6;
            this._tokDefense2PuskUCol.Name = "_tokDefense2PuskUCol";
            this._tokDefense2PuskUCol.Width = 79;
            // 
            // _tokDefense2PuskConstraintCol
            // 
            dataGridViewCellStyle16.NullValue = null;
            this._tokDefense2PuskConstraintCol.DefaultCellStyle = dataGridViewCellStyle16;
            this._tokDefense2PuskConstraintCol.HeaderText = "U����, B";
            this._tokDefense2PuskConstraintCol.MinimumWidth = 6;
            this._tokDefense2PuskConstraintCol.Name = "_tokDefense2PuskConstraintCol";
            this._tokDefense2PuskConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense2PuskConstraintCol.Width = 70;
            // 
            // _tokDefense2DirectionCol
            // 
            this._tokDefense2DirectionCol.HeaderText = "�����������";
            this._tokDefense2DirectionCol.MinimumWidth = 6;
            this._tokDefense2DirectionCol.Name = "_tokDefense2DirectionCol";
            this._tokDefense2DirectionCol.Width = 103;
            // 
            // _tokDefense2BlockExistCol
            // 
            this._tokDefense2BlockExistCol.HeaderText = "����������";
            this._tokDefense2BlockExistCol.MinimumWidth = 6;
            this._tokDefense2BlockExistCol.Name = "_tokDefense2BlockExistCol";
            this._tokDefense2BlockExistCol.Width = 92;
            // 
            // _tokDefense2ParameterCol
            // 
            this._tokDefense2ParameterCol.HeaderText = "��������";
            this._tokDefense2ParameterCol.MinimumWidth = 6;
            this._tokDefense2ParameterCol.Name = "_tokDefense2ParameterCol";
            this._tokDefense2ParameterCol.Width = 80;
            // 
            // _tokDefense2WorkConstraintCol
            // 
            dataGridViewCellStyle17.NullValue = null;
            this._tokDefense2WorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle17;
            this._tokDefense2WorkConstraintCol.HeaderText = "I��, I�";
            this._tokDefense2WorkConstraintCol.MinimumWidth = 6;
            this._tokDefense2WorkConstraintCol.Name = "_tokDefense2WorkConstraintCol";
            this._tokDefense2WorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense2WorkConstraintCol.Width = 51;
            // 
            // _tokDefense2FeatureCol
            // 
            this._tokDefense2FeatureCol.HeaderText = "���-��";
            this._tokDefense2FeatureCol.MinimumWidth = 6;
            this._tokDefense2FeatureCol.Name = "_tokDefense2FeatureCol";
            this._tokDefense2FeatureCol.Width = 59;
            // 
            // _tokDefense2WorkTimeCol
            // 
            dataGridViewCellStyle18.NullValue = null;
            this._tokDefense2WorkTimeCol.DefaultCellStyle = dataGridViewCellStyle18;
            this._tokDefense2WorkTimeCol.HeaderText = "T��, ��/����.";
            this._tokDefense2WorkTimeCol.MinimumWidth = 6;
            this._tokDefense2WorkTimeCol.Name = "_tokDefense2WorkTimeCol";
            this._tokDefense2WorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense2WorkTimeCol.Width = 103;
            // 
            // _tokDefense2EngineCol
            // 
            this._tokDefense2EngineCol.HeaderText = "���������";
            this._tokDefense2EngineCol.MinimumWidth = 6;
            this._tokDefense2EngineCol.Name = "_tokDefense2EngineCol";
            this._tokDefense2EngineCol.Width = 83;
            // 
            // _tokDefense2UROVCol
            // 
            this._tokDefense2UROVCol.HeaderText = "����";
            this._tokDefense2UROVCol.MinimumWidth = 6;
            this._tokDefense2UROVCol.Name = "_tokDefense2UROVCol";
            this._tokDefense2UROVCol.Width = 52;
            // 
            // _tokDefense2APVCol
            // 
            this._tokDefense2APVCol.HeaderText = "���";
            this._tokDefense2APVCol.MinimumWidth = 6;
            this._tokDefense2APVCol.Name = "_tokDefense2APVCol";
            this._tokDefense2APVCol.Width = 42;
            // 
            // _tokDefense2AVRCol
            // 
            this._tokDefense2AVRCol.HeaderText = "���";
            this._tokDefense2AVRCol.MinimumWidth = 6;
            this._tokDefense2AVRCol.Name = "_tokDefense2AVRCol";
            this._tokDefense2AVRCol.Width = 41;
            // 
            // _tokDefenseGrid1
            // 
            this._tokDefenseGrid1.AllowUserToAddRows = false;
            this._tokDefenseGrid1.AllowUserToDeleteRows = false;
            this._tokDefenseGrid1.AllowUserToResizeColumns = false;
            this._tokDefenseGrid1.AllowUserToResizeRows = false;
            this._tokDefenseGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tokDefenseGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._tokDefenseGrid1.BackgroundColor = System.Drawing.SystemColors.Control;
            this._tokDefenseGrid1.ColumnHeadersHeight = 29;
            this._tokDefenseGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._tokDefenseGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefenseNameCol,
            this._tokDefenseModeCol,
            this._tokDefenseBlockNumberCol,
            this._tokDefenseU_PuskCol,
            this._tokDefensePuskConstraintCol,
            this._tokDefenseDirectionCol,
            this._tokDefenseBlockExistCol,
            this._tokDefenseParameterCol,
            this._tokDefenseWorkConstraintCol,
            this._tokDefenseFeatureCol,
            this._tokDefenseWorkTimeCol,
            this._tokDefenseSpeedUpCol,
            this._tokDefenseSpeedupTimeCol,
            this._tokDefenseUROVCol,
            this._tokDefenseAPVCol,
            this._tokDefenseAVRCol});
            this._tokDefenseGrid1.Location = new System.Drawing.Point(4, 4);
            this._tokDefenseGrid1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tokDefenseGrid1.MultiSelect = false;
            this._tokDefenseGrid1.Name = "_tokDefenseGrid1";
            this._tokDefenseGrid1.RowHeadersVisible = false;
            this._tokDefenseGrid1.RowHeadersWidth = 51;
            this._tokDefenseGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid1.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this._tokDefenseGrid1.RowTemplate.Height = 24;
            this._tokDefenseGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this._tokDefenseGrid1.Size = new System.Drawing.Size(1479, 105);
            this._tokDefenseGrid1.TabIndex = 5;
            this._tokDefenseGrid1.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this._tokDefenseGrid1_CellBeginEdit);
            // 
            // _tokDefenseNameCol
            // 
            this._tokDefenseNameCol.Frozen = true;
            this._tokDefenseNameCol.HeaderText = "";
            this._tokDefenseNameCol.MinimumWidth = 50;
            this._tokDefenseNameCol.Name = "_tokDefenseNameCol";
            this._tokDefenseNameCol.ReadOnly = true;
            this._tokDefenseNameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseNameCol.Width = 50;
            // 
            // _tokDefenseModeCol
            // 
            this._tokDefenseModeCol.HeaderText = "�����";
            this._tokDefenseModeCol.MinimumWidth = 6;
            this._tokDefenseModeCol.Name = "_tokDefenseModeCol";
            this._tokDefenseModeCol.Width = 57;
            // 
            // _tokDefenseBlockNumberCol
            // 
            this._tokDefenseBlockNumberCol.HeaderText = "����������";
            this._tokDefenseBlockNumberCol.MinimumWidth = 6;
            this._tokDefenseBlockNumberCol.Name = "_tokDefenseBlockNumberCol";
            this._tokDefenseBlockNumberCol.Width = 92;
            // 
            // _tokDefenseU_PuskCol
            // 
            this._tokDefenseU_PuskCol.HeaderText = "���� �� U";
            this._tokDefenseU_PuskCol.MinimumWidth = 6;
            this._tokDefenseU_PuskCol.Name = "_tokDefenseU_PuskCol";
            this._tokDefenseU_PuskCol.Width = 79;
            // 
            // _tokDefensePuskConstraintCol
            // 
            dataGridViewCellStyle20.NullValue = null;
            this._tokDefensePuskConstraintCol.DefaultCellStyle = dataGridViewCellStyle20;
            this._tokDefensePuskConstraintCol.HeaderText = "U����, B";
            this._tokDefensePuskConstraintCol.MinimumWidth = 6;
            this._tokDefensePuskConstraintCol.Name = "_tokDefensePuskConstraintCol";
            this._tokDefensePuskConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefensePuskConstraintCol.Width = 70;
            // 
            // _tokDefenseDirectionCol
            // 
            this._tokDefenseDirectionCol.HeaderText = "�����������";
            this._tokDefenseDirectionCol.MinimumWidth = 6;
            this._tokDefenseDirectionCol.Name = "_tokDefenseDirectionCol";
            this._tokDefenseDirectionCol.Width = 103;
            // 
            // _tokDefenseBlockExistCol
            // 
            this._tokDefenseBlockExistCol.HeaderText = "����������";
            this._tokDefenseBlockExistCol.MinimumWidth = 6;
            this._tokDefenseBlockExistCol.Name = "_tokDefenseBlockExistCol";
            this._tokDefenseBlockExistCol.Width = 92;
            // 
            // _tokDefenseParameterCol
            // 
            this._tokDefenseParameterCol.HeaderText = "��������";
            this._tokDefenseParameterCol.MinimumWidth = 6;
            this._tokDefenseParameterCol.Name = "_tokDefenseParameterCol";
            this._tokDefenseParameterCol.Width = 80;
            // 
            // _tokDefenseWorkConstraintCol
            // 
            dataGridViewCellStyle21.NullValue = null;
            this._tokDefenseWorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle21;
            this._tokDefenseWorkConstraintCol.HeaderText = "I����, I�";
            this._tokDefenseWorkConstraintCol.MinimumWidth = 6;
            this._tokDefenseWorkConstraintCol.Name = "_tokDefenseWorkConstraintCol";
            this._tokDefenseWorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseWorkConstraintCol.Width = 67;
            // 
            // _tokDefenseFeatureCol
            // 
            this._tokDefenseFeatureCol.HeaderText = "���-��";
            this._tokDefenseFeatureCol.MinimumWidth = 6;
            this._tokDefenseFeatureCol.Name = "_tokDefenseFeatureCol";
            this._tokDefenseFeatureCol.Width = 59;
            // 
            // _tokDefenseWorkTimeCol
            // 
            dataGridViewCellStyle22.NullValue = null;
            this._tokDefenseWorkTimeCol.DefaultCellStyle = dataGridViewCellStyle22;
            this._tokDefenseWorkTimeCol.HeaderText = "T��, ��/����.";
            this._tokDefenseWorkTimeCol.MinimumWidth = 6;
            this._tokDefenseWorkTimeCol.Name = "_tokDefenseWorkTimeCol";
            this._tokDefenseWorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseWorkTimeCol.Width = 103;
            // 
            // _tokDefenseSpeedUpCol
            // 
            this._tokDefenseSpeedUpCol.HeaderText = "���.";
            this._tokDefenseSpeedUpCol.MinimumWidth = 6;
            this._tokDefenseSpeedUpCol.Name = "_tokDefenseSpeedUpCol";
            this._tokDefenseSpeedUpCol.Width = 41;
            // 
            // _tokDefenseSpeedupTimeCol
            // 
            dataGridViewCellStyle23.NullValue = null;
            this._tokDefenseSpeedupTimeCol.DefaultCellStyle = dataGridViewCellStyle23;
            this._tokDefenseSpeedupTimeCol.HeaderText = "T���, ��";
            this._tokDefenseSpeedupTimeCol.MinimumWidth = 6;
            this._tokDefenseSpeedupTimeCol.Name = "_tokDefenseSpeedupTimeCol";
            this._tokDefenseSpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseSpeedupTimeCol.Width = 68;
            // 
            // _tokDefenseUROVCol
            // 
            this._tokDefenseUROVCol.HeaderText = "����";
            this._tokDefenseUROVCol.MinimumWidth = 6;
            this._tokDefenseUROVCol.Name = "_tokDefenseUROVCol";
            this._tokDefenseUROVCol.Width = 52;
            // 
            // _tokDefenseAPVCol
            // 
            this._tokDefenseAPVCol.HeaderText = "���";
            this._tokDefenseAPVCol.MinimumWidth = 6;
            this._tokDefenseAPVCol.Name = "_tokDefenseAPVCol";
            this._tokDefenseAPVCol.Width = 42;
            // 
            // _tokDefenseAVRCol
            // 
            this._tokDefenseAVRCol.HeaderText = "���";
            this._tokDefenseAVRCol.MinimumWidth = 6;
            this._tokDefenseAVRCol.Name = "_tokDefenseAVRCol";
            this._tokDefenseAVRCol.Width = 41;
            // 
            // _voltageDefendTabPage
            // 
            this._voltageDefendTabPage.Controls.Add(this._voltageDefensesGrid2);
            this._voltageDefendTabPage.Controls.Add(this._voltageDefensesGrid1);
            this._voltageDefendTabPage.Controls.Add(this._voltageDefensesGrid3);
            this._voltageDefendTabPage.Location = new System.Drawing.Point(4, 25);
            this._voltageDefendTabPage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._voltageDefendTabPage.Name = "_voltageDefendTabPage";
            this._voltageDefendTabPage.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._voltageDefendTabPage.Size = new System.Drawing.Size(1489, 602);
            this._voltageDefendTabPage.TabIndex = 1;
            this._voltageDefendTabPage.Text = "������ �� ����������";
            this._voltageDefendTabPage.UseVisualStyleBackColor = true;
            // 
            // _voltageDefensesGrid2
            // 
            this._voltageDefensesGrid2.AllowUserToAddRows = false;
            this._voltageDefensesGrid2.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid2.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid2.AllowUserToResizeRows = false;
            this._voltageDefensesGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._voltageDefensesGrid2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._voltageDefensesGrid2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._voltageDefensesGrid2.BackgroundColor = System.Drawing.SystemColors.Control;
            this._voltageDefensesGrid2.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol2,
            this._voltageDefensesModeCol2,
            this._voltageDefensesBlockingNumberCol2,
            this._voltageDefensesParameterCol2,
            this._voltageDefensesWorkConstraintCol2,
            this._voltageDefensesWorkTimeCol2,
            this._voltageDefensesReturnCol2,
            this._voltageDefensesAPVreturnCol2,
            this._voltageDefensesReturnConstraintCol2,
            this._voltageDefensesReturnTimeCol2,
            this._voltageDefensesUROVcol2,
            this._voltageDefensesAPVcol2,
            this._voltageDefensesAVRcol2});
            this._voltageDefensesGrid2.Location = new System.Drawing.Point(4, 203);
            this._voltageDefensesGrid2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._voltageDefensesGrid2.MultiSelect = false;
            this._voltageDefensesGrid2.Name = "_voltageDefensesGrid2";
            this._voltageDefensesGrid2.RowHeadersVisible = false;
            this._voltageDefensesGrid2.RowHeadersWidth = 51;
            this._voltageDefensesGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid2.RowsDefaultCellStyle = dataGridViewCellStyle28;
            this._voltageDefensesGrid2.RowTemplate.Height = 24;
            this._voltageDefensesGrid2.Size = new System.Drawing.Size(1479, 159);
            this._voltageDefensesGrid2.TabIndex = 5;
            this._voltageDefensesGrid2.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this._voltageDefensesGrid2_CellBeginEdit);
            // 
            // _voltageDefensesNameCol2
            // 
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._voltageDefensesNameCol2.DefaultCellStyle = dataGridViewCellStyle25;
            this._voltageDefensesNameCol2.HeaderText = "";
            this._voltageDefensesNameCol2.MinimumWidth = 6;
            this._voltageDefensesNameCol2.Name = "_voltageDefensesNameCol2";
            this._voltageDefensesNameCol2.ReadOnly = true;
            this._voltageDefensesNameCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol2.Width = 6;
            // 
            // _voltageDefensesModeCol2
            // 
            this._voltageDefensesModeCol2.HeaderText = "�����";
            this._voltageDefensesModeCol2.MinimumWidth = 6;
            this._voltageDefensesModeCol2.Name = "_voltageDefensesModeCol2";
            this._voltageDefensesModeCol2.Width = 57;
            // 
            // _voltageDefensesBlockingNumberCol2
            // 
            this._voltageDefensesBlockingNumberCol2.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol2.MinimumWidth = 6;
            this._voltageDefensesBlockingNumberCol2.Name = "_voltageDefensesBlockingNumberCol2";
            this._voltageDefensesBlockingNumberCol2.Width = 92;
            // 
            // _voltageDefensesParameterCol2
            // 
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesParameterCol2.DefaultCellStyle = dataGridViewCellStyle26;
            this._voltageDefensesParameterCol2.HeaderText = "��������";
            this._voltageDefensesParameterCol2.MinimumWidth = 6;
            this._voltageDefensesParameterCol2.Name = "_voltageDefensesParameterCol2";
            this._voltageDefensesParameterCol2.Width = 80;
            // 
            // _voltageDefensesWorkConstraintCol2
            // 
            this._voltageDefensesWorkConstraintCol2.HeaderText = "U��, B";
            this._voltageDefensesWorkConstraintCol2.MinimumWidth = 6;
            this._voltageDefensesWorkConstraintCol2.Name = "_voltageDefensesWorkConstraintCol2";
            this._voltageDefensesWorkConstraintCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkConstraintCol2.Width = 56;
            // 
            // _voltageDefensesWorkTimeCol2
            // 
            dataGridViewCellStyle27.NullValue = "0";
            this._voltageDefensesWorkTimeCol2.DefaultCellStyle = dataGridViewCellStyle27;
            this._voltageDefensesWorkTimeCol2.HeaderText = "T��, ��";
            this._voltageDefensesWorkTimeCol2.MinimumWidth = 6;
            this._voltageDefensesWorkTimeCol2.Name = "_voltageDefensesWorkTimeCol2";
            this._voltageDefensesWorkTimeCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkTimeCol2.Width = 62;
            // 
            // _voltageDefensesReturnCol2
            // 
            this._voltageDefensesReturnCol2.HeaderText = "����.";
            this._voltageDefensesReturnCol2.MinimumWidth = 6;
            this._voltageDefensesReturnCol2.Name = "_voltageDefensesReturnCol2";
            this._voltageDefensesReturnCol2.Width = 49;
            // 
            // _voltageDefensesAPVreturnCol2
            // 
            this._voltageDefensesAPVreturnCol2.HeaderText = "��� ��";
            this._voltageDefensesAPVreturnCol2.MinimumWidth = 6;
            this._voltageDefensesAPVreturnCol2.Name = "_voltageDefensesAPVreturnCol2";
            this._voltageDefensesAPVreturnCol2.Width = 64;
            // 
            // _voltageDefensesReturnConstraintCol2
            // 
            this._voltageDefensesReturnConstraintCol2.HeaderText = "U��, B";
            this._voltageDefensesReturnConstraintCol2.MinimumWidth = 6;
            this._voltageDefensesReturnConstraintCol2.Name = "_voltageDefensesReturnConstraintCol2";
            this._voltageDefensesReturnConstraintCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnConstraintCol2.Width = 55;
            // 
            // _voltageDefensesReturnTimeCol2
            // 
            this._voltageDefensesReturnTimeCol2.HeaderText = "T��, ��";
            this._voltageDefensesReturnTimeCol2.MinimumWidth = 6;
            this._voltageDefensesReturnTimeCol2.Name = "_voltageDefensesReturnTimeCol2";
            this._voltageDefensesReturnTimeCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnTimeCol2.Width = 61;
            // 
            // _voltageDefensesUROVcol2
            // 
            this._voltageDefensesUROVcol2.HeaderText = "����";
            this._voltageDefensesUROVcol2.MinimumWidth = 6;
            this._voltageDefensesUROVcol2.Name = "_voltageDefensesUROVcol2";
            this._voltageDefensesUROVcol2.Width = 52;
            // 
            // _voltageDefensesAPVcol2
            // 
            this._voltageDefensesAPVcol2.HeaderText = "���";
            this._voltageDefensesAPVcol2.MinimumWidth = 6;
            this._voltageDefensesAPVcol2.Name = "_voltageDefensesAPVcol2";
            this._voltageDefensesAPVcol2.Width = 42;
            // 
            // _voltageDefensesAVRcol2
            // 
            this._voltageDefensesAVRcol2.HeaderText = "���";
            this._voltageDefensesAVRcol2.MinimumWidth = 6;
            this._voltageDefensesAVRcol2.Name = "_voltageDefensesAVRcol2";
            this._voltageDefensesAVRcol2.Width = 41;
            // 
            // _voltageDefensesGrid1
            // 
            this._voltageDefensesGrid1.AllowUserToAddRows = false;
            this._voltageDefensesGrid1.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid1.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid1.AllowUserToResizeRows = false;
            this._voltageDefensesGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._voltageDefensesGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._voltageDefensesGrid1.BackgroundColor = System.Drawing.SystemColors.Control;
            this._voltageDefensesGrid1.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol1,
            this._voltageDefensesModeCol1,
            this._voltageDefensesBlockingNumberCol1,
            this._voltageDefensesParameterCol1,
            this._voltageDefensesWorkConstraintCol1,
            this._voltageDefensesWorkTimeCol1,
            this._voltageDefensesReturnCol1,
            this._voltageDefensesAPVReturlCol1,
            this._voltageDefensesReturnConstraintCol1,
            this._voltageDefensesReturnTimeCol1,
            this._voltageDefensesUROVCol1,
            this._voltageDefensesAPVCol1,
            this._voltageDefensesAVRCol1});
            this._voltageDefensesGrid1.Location = new System.Drawing.Point(4, 4);
            this._voltageDefensesGrid1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._voltageDefensesGrid1.MultiSelect = false;
            this._voltageDefensesGrid1.Name = "_voltageDefensesGrid1";
            this._voltageDefensesGrid1.RowHeadersVisible = false;
            this._voltageDefensesGrid1.RowHeadersWidth = 51;
            this._voltageDefensesGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid1.RowsDefaultCellStyle = dataGridViewCellStyle32;
            this._voltageDefensesGrid1.RowTemplate.Height = 24;
            this._voltageDefensesGrid1.Size = new System.Drawing.Size(1479, 192);
            this._voltageDefensesGrid1.TabIndex = 4;
            this._voltageDefensesGrid1.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this._voltageDefensesGrid1_CellBeginEdit);
            // 
            // _voltageDefensesNameCol1
            // 
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesNameCol1.DefaultCellStyle = dataGridViewCellStyle29;
            this._voltageDefensesNameCol1.HeaderText = "";
            this._voltageDefensesNameCol1.MinimumWidth = 6;
            this._voltageDefensesNameCol1.Name = "_voltageDefensesNameCol1";
            this._voltageDefensesNameCol1.ReadOnly = true;
            this._voltageDefensesNameCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol1.Width = 6;
            // 
            // _voltageDefensesModeCol1
            // 
            this._voltageDefensesModeCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._voltageDefensesModeCol1.HeaderText = "�����";
            this._voltageDefensesModeCol1.MinimumWidth = 6;
            this._voltageDefensesModeCol1.Name = "_voltageDefensesModeCol1";
            this._voltageDefensesModeCol1.Width = 125;
            // 
            // _voltageDefensesBlockingNumberCol1
            // 
            this._voltageDefensesBlockingNumberCol1.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol1.MinimumWidth = 6;
            this._voltageDefensesBlockingNumberCol1.Name = "_voltageDefensesBlockingNumberCol1";
            this._voltageDefensesBlockingNumberCol1.Width = 92;
            // 
            // _voltageDefensesParameterCol1
            // 
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesParameterCol1.DefaultCellStyle = dataGridViewCellStyle30;
            this._voltageDefensesParameterCol1.HeaderText = "��������";
            this._voltageDefensesParameterCol1.MinimumWidth = 6;
            this._voltageDefensesParameterCol1.Name = "_voltageDefensesParameterCol1";
            this._voltageDefensesParameterCol1.Width = 80;
            // 
            // _voltageDefensesWorkConstraintCol1
            // 
            this._voltageDefensesWorkConstraintCol1.HeaderText = "U��, B";
            this._voltageDefensesWorkConstraintCol1.MinimumWidth = 6;
            this._voltageDefensesWorkConstraintCol1.Name = "_voltageDefensesWorkConstraintCol1";
            this._voltageDefensesWorkConstraintCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkConstraintCol1.Width = 56;
            // 
            // _voltageDefensesWorkTimeCol1
            // 
            dataGridViewCellStyle31.NullValue = "0";
            this._voltageDefensesWorkTimeCol1.DefaultCellStyle = dataGridViewCellStyle31;
            this._voltageDefensesWorkTimeCol1.HeaderText = "T��, ��";
            this._voltageDefensesWorkTimeCol1.MinimumWidth = 6;
            this._voltageDefensesWorkTimeCol1.Name = "_voltageDefensesWorkTimeCol1";
            this._voltageDefensesWorkTimeCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkTimeCol1.Width = 62;
            // 
            // _voltageDefensesReturnCol1
            // 
            this._voltageDefensesReturnCol1.HeaderText = "����.";
            this._voltageDefensesReturnCol1.MinimumWidth = 6;
            this._voltageDefensesReturnCol1.Name = "_voltageDefensesReturnCol1";
            this._voltageDefensesReturnCol1.Width = 49;
            // 
            // _voltageDefensesAPVReturlCol1
            // 
            this._voltageDefensesAPVReturlCol1.HeaderText = "��� ��";
            this._voltageDefensesAPVReturlCol1.MinimumWidth = 6;
            this._voltageDefensesAPVReturlCol1.Name = "_voltageDefensesAPVReturlCol1";
            this._voltageDefensesAPVReturlCol1.Width = 64;
            // 
            // _voltageDefensesReturnConstraintCol1
            // 
            this._voltageDefensesReturnConstraintCol1.HeaderText = "U��, B";
            this._voltageDefensesReturnConstraintCol1.MinimumWidth = 6;
            this._voltageDefensesReturnConstraintCol1.Name = "_voltageDefensesReturnConstraintCol1";
            this._voltageDefensesReturnConstraintCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnConstraintCol1.Width = 55;
            // 
            // _voltageDefensesReturnTimeCol1
            // 
            this._voltageDefensesReturnTimeCol1.HeaderText = "T��, ��";
            this._voltageDefensesReturnTimeCol1.MinimumWidth = 6;
            this._voltageDefensesReturnTimeCol1.Name = "_voltageDefensesReturnTimeCol1";
            this._voltageDefensesReturnTimeCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnTimeCol1.Width = 61;
            // 
            // _voltageDefensesUROVCol1
            // 
            this._voltageDefensesUROVCol1.HeaderText = "����";
            this._voltageDefensesUROVCol1.MinimumWidth = 6;
            this._voltageDefensesUROVCol1.Name = "_voltageDefensesUROVCol1";
            this._voltageDefensesUROVCol1.Width = 52;
            // 
            // _voltageDefensesAPVCol1
            // 
            this._voltageDefensesAPVCol1.HeaderText = "���";
            this._voltageDefensesAPVCol1.MinimumWidth = 6;
            this._voltageDefensesAPVCol1.Name = "_voltageDefensesAPVCol1";
            this._voltageDefensesAPVCol1.Width = 42;
            // 
            // _voltageDefensesAVRCol1
            // 
            this._voltageDefensesAVRCol1.HeaderText = "���";
            this._voltageDefensesAVRCol1.MinimumWidth = 6;
            this._voltageDefensesAVRCol1.Name = "_voltageDefensesAVRCol1";
            this._voltageDefensesAVRCol1.Width = 41;
            // 
            // _voltageDefensesGrid3
            // 
            this._voltageDefensesGrid3.AllowUserToAddRows = false;
            this._voltageDefensesGrid3.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid3.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid3.AllowUserToResizeRows = false;
            this._voltageDefensesGrid3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._voltageDefensesGrid3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._voltageDefensesGrid3.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._voltageDefensesGrid3.BackgroundColor = System.Drawing.SystemColors.Control;
            this._voltageDefensesGrid3.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol3,
            this._voltageDefensesModeCol3,
            this._voltageDefensesBlockingNumberCol3,
            this._voltageDefensesParameterCol3,
            this._voltageDefensesWorkConstraintCol3,
            this._voltageDefensesTimeConstraintCol3,
            this._voltageDefensesReturnCol3,
            this._voltageDefensesAPVreturnCol3,
            this._voltageDefensesReturnConstraintCol3,
            this._voltageDefensesReturnTimeCol3,
            this._voltageDefensesUROVcol3,
            this._voltageDefensesAPVcol3,
            this._voltageDefensesAVRcol3});
            this._voltageDefensesGrid3.Location = new System.Drawing.Point(4, 369);
            this._voltageDefensesGrid3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._voltageDefensesGrid3.MultiSelect = false;
            this._voltageDefensesGrid3.Name = "_voltageDefensesGrid3";
            this._voltageDefensesGrid3.RowHeadersVisible = false;
            this._voltageDefensesGrid3.RowHeadersWidth = 51;
            this._voltageDefensesGrid3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid3.RowsDefaultCellStyle = dataGridViewCellStyle36;
            this._voltageDefensesGrid3.RowTemplate.Height = 24;
            this._voltageDefensesGrid3.Size = new System.Drawing.Size(1479, 145);
            this._voltageDefensesGrid3.TabIndex = 6;
            this._voltageDefensesGrid3.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this._voltageDefensesGrid3_CellBeginEdit);
            // 
            // _voltageDefensesNameCol3
            // 
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._voltageDefensesNameCol3.DefaultCellStyle = dataGridViewCellStyle33;
            this._voltageDefensesNameCol3.HeaderText = "";
            this._voltageDefensesNameCol3.MinimumWidth = 6;
            this._voltageDefensesNameCol3.Name = "_voltageDefensesNameCol3";
            this._voltageDefensesNameCol3.ReadOnly = true;
            this._voltageDefensesNameCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol3.Width = 6;
            // 
            // _voltageDefensesModeCol3
            // 
            this._voltageDefensesModeCol3.HeaderText = "�����";
            this._voltageDefensesModeCol3.MinimumWidth = 6;
            this._voltageDefensesModeCol3.Name = "_voltageDefensesModeCol3";
            this._voltageDefensesModeCol3.Width = 57;
            // 
            // _voltageDefensesBlockingNumberCol3
            // 
            this._voltageDefensesBlockingNumberCol3.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol3.MinimumWidth = 6;
            this._voltageDefensesBlockingNumberCol3.Name = "_voltageDefensesBlockingNumberCol3";
            this._voltageDefensesBlockingNumberCol3.Width = 92;
            // 
            // _voltageDefensesParameterCol3
            // 
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesParameterCol3.DefaultCellStyle = dataGridViewCellStyle34;
            this._voltageDefensesParameterCol3.HeaderText = "��������";
            this._voltageDefensesParameterCol3.MinimumWidth = 6;
            this._voltageDefensesParameterCol3.Name = "_voltageDefensesParameterCol3";
            this._voltageDefensesParameterCol3.Width = 80;
            // 
            // _voltageDefensesWorkConstraintCol3
            // 
            this._voltageDefensesWorkConstraintCol3.HeaderText = "U��, B";
            this._voltageDefensesWorkConstraintCol3.MinimumWidth = 6;
            this._voltageDefensesWorkConstraintCol3.Name = "_voltageDefensesWorkConstraintCol3";
            this._voltageDefensesWorkConstraintCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkConstraintCol3.Width = 56;
            // 
            // _voltageDefensesTimeConstraintCol3
            // 
            dataGridViewCellStyle35.NullValue = "0";
            this._voltageDefensesTimeConstraintCol3.DefaultCellStyle = dataGridViewCellStyle35;
            this._voltageDefensesTimeConstraintCol3.HeaderText = "T��, ��";
            this._voltageDefensesTimeConstraintCol3.MinimumWidth = 6;
            this._voltageDefensesTimeConstraintCol3.Name = "_voltageDefensesTimeConstraintCol3";
            this._voltageDefensesTimeConstraintCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesTimeConstraintCol3.Width = 62;
            // 
            // _voltageDefensesReturnCol3
            // 
            this._voltageDefensesReturnCol3.HeaderText = "����.";
            this._voltageDefensesReturnCol3.MinimumWidth = 6;
            this._voltageDefensesReturnCol3.Name = "_voltageDefensesReturnCol3";
            this._voltageDefensesReturnCol3.Width = 49;
            // 
            // _voltageDefensesAPVreturnCol3
            // 
            this._voltageDefensesAPVreturnCol3.HeaderText = "��� ��";
            this._voltageDefensesAPVreturnCol3.MinimumWidth = 6;
            this._voltageDefensesAPVreturnCol3.Name = "_voltageDefensesAPVreturnCol3";
            this._voltageDefensesAPVreturnCol3.Width = 64;
            // 
            // _voltageDefensesReturnConstraintCol3
            // 
            this._voltageDefensesReturnConstraintCol3.HeaderText = "U��, B";
            this._voltageDefensesReturnConstraintCol3.MinimumWidth = 6;
            this._voltageDefensesReturnConstraintCol3.Name = "_voltageDefensesReturnConstraintCol3";
            this._voltageDefensesReturnConstraintCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnConstraintCol3.Width = 55;
            // 
            // _voltageDefensesReturnTimeCol3
            // 
            this._voltageDefensesReturnTimeCol3.HeaderText = "T��, ��";
            this._voltageDefensesReturnTimeCol3.MinimumWidth = 6;
            this._voltageDefensesReturnTimeCol3.Name = "_voltageDefensesReturnTimeCol3";
            this._voltageDefensesReturnTimeCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnTimeCol3.Width = 61;
            // 
            // _voltageDefensesUROVcol3
            // 
            this._voltageDefensesUROVcol3.HeaderText = "����";
            this._voltageDefensesUROVcol3.MinimumWidth = 6;
            this._voltageDefensesUROVcol3.Name = "_voltageDefensesUROVcol3";
            this._voltageDefensesUROVcol3.Width = 52;
            // 
            // _voltageDefensesAPVcol3
            // 
            this._voltageDefensesAPVcol3.HeaderText = "���";
            this._voltageDefensesAPVcol3.MinimumWidth = 6;
            this._voltageDefensesAPVcol3.Name = "_voltageDefensesAPVcol3";
            this._voltageDefensesAPVcol3.Width = 42;
            // 
            // _voltageDefensesAVRcol3
            // 
            this._voltageDefensesAVRcol3.HeaderText = "���";
            this._voltageDefensesAVRcol3.MinimumWidth = 6;
            this._voltageDefensesAVRcol3.Name = "_voltageDefensesAVRcol3";
            this._voltageDefensesAVRcol3.Width = 41;
            // 
            // _frequencyDefendTabPage
            // 
            this._frequencyDefendTabPage.Controls.Add(this._frequenceDefensesGrid);
            this._frequencyDefendTabPage.Location = new System.Drawing.Point(4, 25);
            this._frequencyDefendTabPage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._frequencyDefendTabPage.Name = "_frequencyDefendTabPage";
            this._frequencyDefendTabPage.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._frequencyDefendTabPage.Size = new System.Drawing.Size(1489, 602);
            this._frequencyDefendTabPage.TabIndex = 2;
            this._frequencyDefendTabPage.Text = "������ �� �������";
            this._frequencyDefendTabPage.UseVisualStyleBackColor = true;
            // 
            // _frequenceDefensesGrid
            // 
            this._frequenceDefensesGrid.AllowUserToAddRows = false;
            this._frequenceDefensesGrid.AllowUserToDeleteRows = false;
            this._frequenceDefensesGrid.AllowUserToResizeColumns = false;
            this._frequenceDefensesGrid.AllowUserToResizeRows = false;
            this._frequenceDefensesGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._frequenceDefensesGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this._frequenceDefensesGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._frequenceDefensesGrid.ColumnHeadersHeight = 30;
            this._frequenceDefensesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._frequenceDefensesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._frequenceDefensesName,
            this._frequenceDefensesMode,
            this._frequenceDefensesBlockingNumber,
            this._frequenceDefensesWorkConstraint,
            this._frequenceDefensesWorkTime,
            this._frequenceDefensesReturn,
            this._frequenceDefensesAPVReturn,
            this._frequenceDefensesConstraintAPV,
            this._frequenceDefensesReturnTime,
            this._frequenceDefensesUROV,
            this._frequenceDefensesAPV,
            this._frequenceDefensesAVR});
            this._frequenceDefensesGrid.Location = new System.Drawing.Point(8, 7);
            this._frequenceDefensesGrid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._frequenceDefensesGrid.MultiSelect = false;
            this._frequenceDefensesGrid.Name = "_frequenceDefensesGrid";
            this._frequenceDefensesGrid.RowHeadersVisible = false;
            this._frequenceDefensesGrid.RowHeadersWidth = 51;
            this._frequenceDefensesGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._frequenceDefensesGrid.RowTemplate.Height = 24;
            this._frequenceDefensesGrid.Size = new System.Drawing.Size(1471, 335);
            this._frequenceDefensesGrid.TabIndex = 6;
            this._frequenceDefensesGrid.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this._frequenceDefensesGrid_CellBeginEdit);
            // 
            // _frequenceDefensesName
            // 
            this._frequenceDefensesName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._frequenceDefensesName.DefaultCellStyle = dataGridViewCellStyle37;
            this._frequenceDefensesName.HeaderText = "";
            this._frequenceDefensesName.MaxInputLength = 1;
            this._frequenceDefensesName.MinimumWidth = 6;
            this._frequenceDefensesName.Name = "_frequenceDefensesName";
            this._frequenceDefensesName.ReadOnly = true;
            this._frequenceDefensesName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesName.Width = 45;
            // 
            // _frequenceDefensesMode
            // 
            this._frequenceDefensesMode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._frequenceDefensesMode.HeaderText = "�����";
            this._frequenceDefensesMode.MinimumWidth = 6;
            this._frequenceDefensesMode.Name = "_frequenceDefensesMode";
            this._frequenceDefensesMode.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesMode.Width = 125;
            // 
            // _frequenceDefensesBlockingNumber
            // 
            this._frequenceDefensesBlockingNumber.HeaderText = "����������";
            this._frequenceDefensesBlockingNumber.MinimumWidth = 6;
            this._frequenceDefensesBlockingNumber.Name = "_frequenceDefensesBlockingNumber";
            this._frequenceDefensesBlockingNumber.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesBlockingNumber.Width = 92;
            // 
            // _frequenceDefensesWorkConstraint
            // 
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesWorkConstraint.DefaultCellStyle = dataGridViewCellStyle38;
            this._frequenceDefensesWorkConstraint.HeaderText = "F��, ��";
            this._frequenceDefensesWorkConstraint.MaxInputLength = 8;
            this._frequenceDefensesWorkConstraint.MinimumWidth = 6;
            this._frequenceDefensesWorkConstraint.Name = "_frequenceDefensesWorkConstraint";
            this._frequenceDefensesWorkConstraint.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesWorkConstraint.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesWorkConstraint.Width = 61;
            // 
            // _frequenceDefensesWorkTime
            // 
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle39.NullValue = "0";
            this._frequenceDefensesWorkTime.DefaultCellStyle = dataGridViewCellStyle39;
            this._frequenceDefensesWorkTime.HeaderText = "T��, ��";
            this._frequenceDefensesWorkTime.MaxInputLength = 8;
            this._frequenceDefensesWorkTime.MinimumWidth = 6;
            this._frequenceDefensesWorkTime.Name = "_frequenceDefensesWorkTime";
            this._frequenceDefensesWorkTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesWorkTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesWorkTime.Width = 62;
            // 
            // _frequenceDefensesReturn
            // 
            this._frequenceDefensesReturn.HeaderText = "����.";
            this._frequenceDefensesReturn.MinimumWidth = 6;
            this._frequenceDefensesReturn.Name = "_frequenceDefensesReturn";
            this._frequenceDefensesReturn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesReturn.Width = 49;
            // 
            // _frequenceDefensesAPVReturn
            // 
            this._frequenceDefensesAPVReturn.HeaderText = "��� ��";
            this._frequenceDefensesAPVReturn.MinimumWidth = 6;
            this._frequenceDefensesAPVReturn.Name = "_frequenceDefensesAPVReturn";
            this._frequenceDefensesAPVReturn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesAPVReturn.Width = 64;
            // 
            // _frequenceDefensesConstraintAPV
            // 
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesConstraintAPV.DefaultCellStyle = dataGridViewCellStyle40;
            this._frequenceDefensesConstraintAPV.HeaderText = "F��, ��";
            this._frequenceDefensesConstraintAPV.MinimumWidth = 6;
            this._frequenceDefensesConstraintAPV.Name = "_frequenceDefensesConstraintAPV";
            this._frequenceDefensesConstraintAPV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesConstraintAPV.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesConstraintAPV.Width = 60;
            // 
            // _frequenceDefensesReturnTime
            // 
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesReturnTime.DefaultCellStyle = dataGridViewCellStyle41;
            this._frequenceDefensesReturnTime.HeaderText = "T��, ��";
            this._frequenceDefensesReturnTime.MinimumWidth = 6;
            this._frequenceDefensesReturnTime.Name = "_frequenceDefensesReturnTime";
            this._frequenceDefensesReturnTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesReturnTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesReturnTime.Width = 61;
            // 
            // _frequenceDefensesUROV
            // 
            this._frequenceDefensesUROV.HeaderText = "����";
            this._frequenceDefensesUROV.MinimumWidth = 6;
            this._frequenceDefensesUROV.Name = "_frequenceDefensesUROV";
            this._frequenceDefensesUROV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesUROV.Width = 52;
            // 
            // _frequenceDefensesAPV
            // 
            this._frequenceDefensesAPV.HeaderText = "���";
            this._frequenceDefensesAPV.MinimumWidth = 6;
            this._frequenceDefensesAPV.Name = "_frequenceDefensesAPV";
            this._frequenceDefensesAPV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesAPV.Width = 42;
            // 
            // _frequenceDefensesAVR
            // 
            this._frequenceDefensesAVR.HeaderText = "���";
            this._frequenceDefensesAVR.MinimumWidth = 6;
            this._frequenceDefensesAVR.Name = "_frequenceDefensesAVR";
            this._frequenceDefensesAVR.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesAVR.Width = 41;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this._groupChangeButton);
            this.groupBox24.Controls.Add(this._reserveGroupRadioButton);
            this.groupBox24.Controls.Add(this._mainGroupRadioButton);
            this.groupBox24.Location = new System.Drawing.Point(12, 9);
            this.groupBox24.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox24.Size = new System.Drawing.Size(500, 57);
            this.groupBox24.TabIndex = 0;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "������ �������";
            // 
            // _groupChangeButton
            // 
            this._groupChangeButton.Location = new System.Drawing.Point(253, 21);
            this._groupChangeButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._groupChangeButton.Name = "_groupChangeButton";
            this._groupChangeButton.Size = new System.Drawing.Size(217, 28);
            this._groupChangeButton.TabIndex = 2;
            this._groupChangeButton.Text = "�������� --> ���������";
            this._groupChangeButton.UseVisualStyleBackColor = true;
            // 
            // _reserveGroupRadioButton
            // 
            this._reserveGroupRadioButton.AutoSize = true;
            this._reserveGroupRadioButton.Location = new System.Drawing.Point(132, 25);
            this._reserveGroupRadioButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._reserveGroupRadioButton.Name = "_reserveGroupRadioButton";
            this._reserveGroupRadioButton.Size = new System.Drawing.Size(100, 21);
            this._reserveGroupRadioButton.TabIndex = 1;
            this._reserveGroupRadioButton.Text = "���������";
            this._reserveGroupRadioButton.UseVisualStyleBackColor = true;
            // 
            // _mainGroupRadioButton
            // 
            this._mainGroupRadioButton.AutoSize = true;
            this._mainGroupRadioButton.Checked = true;
            this._mainGroupRadioButton.Location = new System.Drawing.Point(9, 25);
            this._mainGroupRadioButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._mainGroupRadioButton.Name = "_mainGroupRadioButton";
            this._mainGroupRadioButton.Size = new System.Drawing.Size(94, 21);
            this._mainGroupRadioButton.TabIndex = 0;
            this._mainGroupRadioButton.TabStop = true;
            this._mainGroupRadioButton.Text = "��������";
            this._mainGroupRadioButton.UseVisualStyleBackColor = true;
            this._mainGroupRadioButton.CheckedChanged += new System.EventHandler(this._mainGroupRadioButton_CheckedChanged);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.Location = new System.Drawing.Point(0, 784);
            this._readConfigBut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(199, 28);
            this._readConfigBut.TabIndex = 1;
            this._readConfigBut.Text = "��������� �� ����������";
            this.toolTip1.SetToolTip(this._readConfigBut, "��������� ������������ �� ���������� (CTRL+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _toolTip
            // 
            this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.Location = new System.Drawing.Point(207, 784);
            this._writeConfigBut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(199, 28);
            this._writeConfigBut.TabIndex = 2;
            this._writeConfigBut.Text = "�������� � ����������";
            this.toolTip1.SetToolTip(this._writeConfigBut, "�������� ������������ � ���������� (CTRL+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.Location = new System.Drawing.Point(1201, 784);
            this._saveConfigBut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(167, 28);
            this._saveConfigBut.TabIndex = 4;
            this._saveConfigBut.Text = "��������� � ����";
            this.toolTip1.SetToolTip(this._saveConfigBut, "��������� ������������ � ���� (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Location = new System.Drawing.Point(1027, 784);
            this._loadConfigBut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(167, 28);
            this._loadConfigBut.TabIndex = 3;
            this._loadConfigBut.Text = "��������� �� �����";
            this.toolTip1.SetToolTip(this._loadConfigBut, "��������� ������������ �� ����� (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "bin";
            this._saveConfigurationDlg.FileName = "��73X_�������";
            this._saveConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._saveConfigurationDlg.Title = "���������  ������� ��� ��73X";
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "bin";
            this._openConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "������� ������� ��� ��73X";
            // 
            // _statusStrip
            // 
            this._statusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._exchangeProgressBar,
            this._processLabel});
            this._statusStrip.Location = new System.Drawing.Point(0, 818);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this._statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this._statusStrip.Size = new System.Drawing.Size(1548, 25);
            this._statusStrip.SizingGrip = false;
            this._statusStrip.TabIndex = 5;
            this._statusStrip.Text = "statusStrip1";
            // 
            // _exchangeProgressBar
            // 
            this._exchangeProgressBar.Maximum = 80;
            this._exchangeProgressBar.Name = "_exchangeProgressBar";
            this._exchangeProgressBar.Size = new System.Drawing.Size(133, 17);
            // 
            // _processLabel
            // 
            this._processLabel.Name = "_processLabel";
            this._processLabel.Size = new System.Drawing.Size(0, 19);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1376, 784);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(167, 28);
            this.button1.TabIndex = 4;
            this.button1.Text = "��������� � HTML";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _resetSetpointsButton
            // 
            this._resetSetpointsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._resetSetpointsButton.AutoSize = true;
            this._resetSetpointsButton.Location = new System.Drawing.Point(413, 784);
            this._resetSetpointsButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._resetSetpointsButton.Name = "_resetSetpointsButton";
            this._resetSetpointsButton.Size = new System.Drawing.Size(199, 28);
            this._resetSetpointsButton.TabIndex = 40;
            this._resetSetpointsButton.Text = "�������� �������";
            this._resetSetpointsButton.UseVisualStyleBackColor = true;
            this._resetSetpointsButton.Click += new System.EventHandler(this._resetSetpointsButton_Click);
            // 
            // writeToHtmlItem
            // 
            this.writeToHtmlItem.Name = "writeToHtmlItem";
            this.writeToHtmlItem.Size = new System.Drawing.Size(252, 24);
            this.writeToHtmlItem.Text = "��������� � html";
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1548, 843);
            this.Controls.Add(this._resetSetpointsButton);
            this.Controls.Add(this._statusStrip);
            this.Controls.Add(this.button1);
            this.Controls.Add(this._saveConfigBut);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this._writeConfigBut);
            this.Controls.Add(this._readConfigBut);
            this.Controls.Add(this._tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "ConfigurationForm";
            this.Text = "ConfigurationForm";
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Mr730ConfigurationForm_KeyUp);
            this._tabControl.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this._inSignalsPage.ResumeLayout(false);
            this.groupBox23.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._outputSignalsPage.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).EndInit();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this._externalDefensePage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).EndInit();
            this._automaticPage.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this._engineDefensesPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._engineDefensesGrid)).EndInit();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this._defendTabPage1.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this._tokDefendTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid1)).EndInit();
            this._voltageDefendTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid3)).EndInit();
            this._frequencyDefendTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._frequenceDefensesGrid)).EndInit();
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl _tabControl;
        private System.Windows.Forms.TabPage _inSignalsPage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage _outputSignalsPage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox _TTNP_Box;
        private System.Windows.Forms.MaskedTextBox _TT_Box;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox _maxTok_Box;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox _TNNP_Box;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox _TN_Box;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox _constraintGroupCombo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox _signalizationCombo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox _extOffCombo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox _extOnCombo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox _keyOffCombo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox _keyOnCombo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox _switcherBlockCombo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox _switcherErrorCombo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox _switcherStateOnCombo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox _switcherStateOffCombo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.MaskedTextBox _switcherDurationBox;
        private System.Windows.Forms.MaskedTextBox _switcherTokBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.MaskedTextBox _switcherTimeBox;
        private System.Windows.Forms.MaskedTextBox _switcherImpulseBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView _logicSignalsDataGrid;
        private System.Windows.Forms.ComboBox _logicChannelsCombo;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckedListBox _dispepairCheckList;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.Button _applyLogicSignalsBut;
        private System.Windows.Forms.DataGridView _outputReleGrid;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView _outputIndicatorsGrid;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button _outputLogicAcceptBut;
        private System.Windows.Forms.ComboBox _outputLogicCombo;
        private System.Windows.Forms.CheckedListBox _outputLogicCheckList;
        private System.Windows.Forms.TabPage _externalDefensePage;
        private System.Windows.Forms.DataGridView _externalDefenseGrid;
        private System.Windows.Forms.TabPage _automaticPage;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ComboBox avr_return;
        private System.Windows.Forms.ComboBox avr_abrasion;
        private System.Windows.Forms.ComboBox avr_reset_blocking;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.ComboBox avr_blocking;
        private System.Windows.Forms.ComboBox avr_start;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.ComboBox lzsh_conf;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.ComboBox apv_self_off;
        private System.Windows.Forms.ComboBox apv_blocking;
        private System.Windows.Forms.ComboBox apv_conf;
        private System.Windows.Forms.TabPage _engineDefensesPage;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.MaskedTextBox _engineHeatPuskConstraintBox;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.MaskedTextBox _enginePuskTimeBox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.MaskedTextBox _engineIpConstraintBox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.MaskedTextBox _engineInBox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.MaskedTextBox _engineCoolingTimeBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.MaskedTextBox _engineHeatingTimeBox;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox _engineNpuskCombo;
        private System.Windows.Forms.ComboBox _engineQresetCombo;
        private System.Windows.Forms.DataGridView _engineDefensesGrid;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.StatusStrip _statusStrip;
        private System.Windows.Forms.ToolStripProgressBar _exchangeProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel _processLabel;
        private System.Windows.Forms.ComboBox _TN_typeCombo;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.ComboBox _TNNP_dispepairCombo;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox _TN_dispepairCombo;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox _manageSignalsSDTU_Combo;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox _manageSignalsExternalCombo;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox _manageSignalsKeyCombo;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.ComboBox _manageSignalsButtonCombo;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.MaskedTextBox apv_time_ready;
        private System.Windows.Forms.MaskedTextBox apv_time_blocking;
        private System.Windows.Forms.MaskedTextBox apv_time_4krat;
        private System.Windows.Forms.MaskedTextBox apv_time_3krat;
        private System.Windows.Forms.MaskedTextBox apv_time_2krat;
        private System.Windows.Forms.MaskedTextBox apv_time_1krat;
        private System.Windows.Forms.MaskedTextBox avr_disconnection;
        private System.Windows.Forms.MaskedTextBox avr_time_return;
        private System.Windows.Forms.MaskedTextBox avr_time_abrasion;
        private System.Windows.Forms.MaskedTextBox lzsh_constraint;
        private System.Windows.Forms.MaskedTextBox _releDispepairBox;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.DataGridViewTextBoxColumn _diskretChannelCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _diskretValueCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignalCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndResetCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndAlarmCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndSystemCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extDefNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefBlockingCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefWorkingCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefWorkingTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefReturnCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAPVreturnCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefReturnNumberCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefReturnTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefUROVcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAPVcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAVRcol;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.ComboBox _oscKonfComb;
        private System.Windows.Forms.TabPage _defendTabPage1;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.Button _groupChangeButton;
        private System.Windows.Forms.RadioButton _reserveGroupRadioButton;
        private System.Windows.Forms.RadioButton _mainGroupRadioButton;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage _tokDefendTabPage;
        private System.Windows.Forms.TabPage _voltageDefendTabPage;
        private System.Windows.Forms.TabPage _frequencyDefendTabPage;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.MaskedTextBox _tokDefenseInbox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.MaskedTextBox _tokDefenseI2box;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.MaskedTextBox _tokDefenseI0box;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.MaskedTextBox _tokDefenseIbox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataGridView _tokDefenseGrid4;
        private System.Windows.Forms.DataGridView _tokDefenseGrid3;
        private System.Windows.Forms.DataGridView _tokDefenseGrid2;
        private System.Windows.Forms.DataGridView _tokDefenseGrid1;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid2;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid1;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid3;
        private System.Windows.Forms.DataGridView _frequenceDefensesGrid;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MaskedTextBox _engineBlockTimeBox;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.MaskedTextBox _engineBlockDurationBox;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.MaskedTextBox _engineBlockHeatBox;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.MaskedTextBox _engineBlockPuskBox;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox _engineQmodeCombo;
        private System.Windows.Forms.MaskedTextBox _engineQtimeBox;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.MaskedTextBox _engineQconstraintBox;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.DataGridViewTextBoxColumn _engineDefenseNameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _engineDefenseModeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _engineDefenseConstraintCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _engineDefenseUROVcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _engineDefenseAVRcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _engineDefenseAPVcol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4NameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense4ModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense4BlockNumberCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4UpuskCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4PuskConstraintCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4WorkConstraintCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4WorkTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4SpeedupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4SpeedupTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4UROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4APVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4AVRCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3NameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3ModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3BlockingNumberCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3UpuskCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3PuskConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3DirectionCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3BlockingExistCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3ParameterCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3WorkConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3FeatureCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3WorkTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3SpeedupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3SpeedupTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3UROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3APVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3AVRCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense2NameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense2ModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense2BlockNumberCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense2PuskUCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense2PuskConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense2DirectionCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense2BlockExistCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense2ParameterCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense2WorkConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense2FeatureCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense2WorkTimeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense2EngineCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense2UROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense2APVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense2AVRCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseNameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseBlockNumberCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseU_PuskCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefensePuskConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseDirectionCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseBlockExistCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseParameterCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseWorkConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseFeatureCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseWorkTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseSpeedUpCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseSpeedupTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseUROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseAPVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseAVRCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesParameterCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkConstraintCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkTimeCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesReturnCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVreturnCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnConstraintCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnTimeCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesUROVcol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVcol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAVRcol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesParameterCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkConstraintCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkTimeCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesReturnCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVReturlCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnConstraintCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnTimeCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesUROVCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAVRCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesParameterCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkConstraintCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesTimeConstraintCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesReturnCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVreturnCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnConstraintCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnTimeCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesUROVcol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVcol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAVRcol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesName;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesMode;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesBlockingNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesWorkConstraint;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesWorkTime;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesReturn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesAPVReturn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesConstraintAPV;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesReturnTime;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesUROV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesAPV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesAVR;
        private System.Windows.Forms.CheckBox avr_permit_reset_switch;
        private System.Windows.Forms.CheckBox avr_abrasion_switch;
        private System.Windows.Forms.CheckBox avr_supply_off;
        private System.Windows.Forms.CheckBox avr_switch_off;
        private System.Windows.Forms.CheckBox avr_self_off;
        private System.Windows.Forms.Button _resetSetpointsButton;
        private System.Windows.Forms.ToolStripMenuItem resetSetPointStruct;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releImpulseCol;
        private System.Windows.Forms.ToolStripMenuItem writeToHtmlItem;
    }
}