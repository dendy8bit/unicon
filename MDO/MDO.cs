﻿using System;
using System.Globalization;
using System.Threading;
using BEMN.Devices;
using System.Xml.Serialization;
using System.ComponentModel;
using BEMN.MDO.MdoMeasuring.Structures;
using BEMN.MBServer;
using System.Drawing;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Interfaces;
using BEMN.MBServer.Queries;
using BEMN.MDO.MdoConfiguration.Structures;
using BEMN.MDO.MdoJournal.Structures;
using BEMN.MDO.SomeStructures;
using BEMN.MDO.SupportClasses;

namespace BEMN.MDO
{
    public class MDO : Device, IDeviceView
    {
        #region Поля

        private int _counter;
        public event Action<int> DeviceCanNumberChanged;
        #endregion

        #region Структуры
        private MemoryEntity<OneWordStruct> _stopTime;
        private MemoryEntity<RamStruct> _ram;
        private MemoryEntity<DatetimeStruct> _dateTime;
        private MemoryEntity<CommandStruct> _commandStruct;
        private MemoryEntity<FlashMemoryStruct> _flashMemStruct;
        private MemoryEntity<JournalStructV10> _journalV10;
        private MemoryEntity<JournalStructV12> _journalV12;
        private MemoryEntity<JournalInfoStruct> _journalInfo;
        private MemoryEntity<OneWordStruct> _versionEntity;
        private MemoryEntity<OneWordStruct> _versionEntitySj;
        private MemoryEntity<OneWordStruct> _updateEntity;
        private MemoryEntity<MdoCanNumberStruct> _mdoCanNumber;
        #endregion

        #region Конструкторы инициализация
        public MDO()
        {
            this.Init();
        }

        public MDO(Modbus mb)
        { 
            MB = mb;
            this.Init();
            this._counter = 0;
        }

        private void Init()
        {
            HaveVersion = true;
            this._stopTime = new MemoryEntity<OneWordStruct>("Остановка времени", this, 0x14);
            this._ram = new MemoryEntity<RamStruct>("RAM", this, 0x0);
            this._commandStruct = new MemoryEntity<CommandStruct>("KVINT", this, 0x10);
            this._flashMemStruct = new MemoryEntity<FlashMemoryStruct>("FLASHMEM", this, 0x100);
            this._journalInfo = new MemoryEntity<JournalInfoStruct>("J_INFO", this, 0x1100);
            this._journalV10 = new MemoryEntity<JournalStructV10>("JOURNAL10", this, 0x1103);
            this._journalV12 = new MemoryEntity<JournalStructV12>("JOURNAL12", this, 0x1103);
            this._dateTime = new MemoryEntity<DatetimeStruct>("DATETIME", this, 0x1000);
            this._mdoCanNumber = new MemoryEntity<MdoCanNumberStruct>("CAN_NUMBER", this, 0x11A);
            this._versionEntity = new MemoryEntity<OneWordStruct>("Версия ПО", this, 0x12);
            this._versionEntitySj = new MemoryEntity<OneWordStruct>("Версия ПО", this, 0x12);
            this._updateEntity = new MemoryEntity<OneWordStruct>("Обновление ПО", this, 0x13);
            this._flashMemStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this.RefreshNumber);
            this._mdoCanNumber.AllReadOk += this.MdoCanNumber_Changed;
            this._mdoCanNumber.AllReadFail += o =>
            {
                if (this._counter < 5)
                {
                    this._counter++;
                    this.RefreshNumber();
                }
                else
                {
                    //надо выводить месагу о том,что номер устройства не известен и надо переподключиться
                }
            };
            if (MB != null)
            {
                MB.CompleteExchange += this.MbOnCompleteExchange;
            }
        }

        private void MbOnCompleteExchange(object sender, Query query)
        {
            if (query.name == "vervion" + DeviceNumber)
            {
                if (!query.error)
                {
                    this._mdoCanNumber.LoadStruct();
                }
            }
        }

        #endregion

        #region Свойства
        public CommandStates CommandState { get; set; }
        public MemoryEntity<FlashMemoryStruct> FlashMemStr
        {
            get { return this._flashMemStruct; }
        }
        public MemoryEntity<RamStruct> Ram
        {
            get { return this._ram; }
        }
        public MemoryEntity<CommandStruct> CommandStr
        {
            get { return this._commandStruct; }
        }

        public MemoryEntity<JournalInfoStruct> JournalInfoStr
        {
            get { return this._journalInfo; }
        }

        public MemoryEntity<JournalStructV10> JournalV10Str
        {
            get { return this._journalV10; }
        }
        public MemoryEntity<JournalStructV12> JournalV12Str
        {
            get { return this._journalV12; }
        }
        public MemoryEntity<DatetimeStruct> DateTimeStr
        {
            get { return this._dateTime; }
        }
        public MemoryEntity<OneWordStruct> VersionEntity
        {
            get { return this._versionEntity; }
        }

        public MemoryEntity<OneWordStruct> VersionEntitySj
        {
            get { return this._versionEntitySj; }
        }

        public MemoryEntity<OneWordStruct> UpdateEntity
        {
            get { return this._updateEntity; }
        }

        public MemoryEntity<OneWordStruct> StopTime
        {
            get { return this._stopTime; }
        }
        #endregion
        
        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(MDO); }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mdo; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "МДО"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion
        
        private void MdoCanNumber_Changed(object sender)
        {
            this._counter = 0;
            ushort numberAndType = this._mdoCanNumber.Value.CanNumber;
            this.DeviceCanNumberChanged?.Invoke(Common.GetBits(numberAndType, 0, 1, 2, 3, 4, 5, 6, 7));
        }
        
        public override void LoadVersion(object deviceObj)
        {
            base.LoadVersion(deviceObj);
            this._counter = 0;
        }
        
        public string CreateFormCaption(IFormView form, int canNumber)
        {
            string portString = 0 == MB.Port.PortNum ? "неопределен" : MB.Port.PortNum.ToString(CultureInfo.InvariantCulture);
            string number = 0 == canNumber ? "неопределен" : canNumber.ToString(CultureInfo.InvariantCulture);
            return this.NodeName + " № " + number + " - порт " + portString + ". " + form.NodeName;
        }

        public void RefreshNumber()
        {
            if (DeviceDlgInfo.IsConnectionMode)
            {
                Thread.Sleep(1000);
                this._mdoCanNumber.LoadStruct();
            }
        }
    }
}
