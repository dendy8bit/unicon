﻿using System.Collections.Generic;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.Devices.Structures;
using BEMN.MBServer;
using BEMN.MDO.SupportClasses;

namespace BEMN.MDO.MdoJournal.Structures
{
    public struct JournalRecordStruct : IStruct, IStructInit
    {
        public ushort Year;
        public ushort Month;
        public ushort Date;
        public ushort Day;
        public ushort Hour;
        public ushort Min;
        public ushort Sec;
        public ushort Msec;
        public ushort Msg;

        public string JournalRecordTime
        {
            get
            {
                return
                    this.Date.ToString("00") + "." +
                    this.Month.ToString("00") + "." +
                    this.Year.ToString("0000") + " " +
                    this.Hour.ToString("00") + ":" +
                    this.Min.ToString("00") + ":" +
                    this.Sec.ToString("00") + "," +
                    this.Msec.ToString("000");
            }
        }

        public string JournalRecordMessage
        {
            get
            {
                return this.Msg < Strings.JournalMessageType.Count
                    ? Strings.JournalMessageType[this.Msg]
                    : this.Msg.ToString();
            }
        }



        public StructInfo GetStructInfo(int slotLen = StructBase.MAX_SLOT_LENGHT_DEFAULT)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.Year = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.Month = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.Date = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.Day = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.Hour = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.Min = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.Sec = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.Msec = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.Msg = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.Year);
            result.Add(this.Month);
            result.Add(this.Date);
            result.Add(this.Day);
            result.Add(this.Hour);
            result.Add(this.Min);
            result.Add(this.Sec);
            result.Add(this.Msec);
            result.Add(this.Msg);
            return result.ToArray();
        }
    }
}