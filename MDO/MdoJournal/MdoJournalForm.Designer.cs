﻿namespace BEMN.MDO.MdoJournal
{
    partial class MdoJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._readSysJournalBut = new System.Windows.Forms.Button();
            this._sysJournalGrid = new System.Windows.Forms.DataGridView();
            this._saveSysJournalDlg = new System.Windows.Forms.SaveFileDialog();
            this._openSysJounralDlg = new System.Windows.Forms.OpenFileDialog();
            this._deserializeSysJournalBut = new System.Windows.Forms.Button();
            this._serializeSysJournalBut = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._configProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this._sysJournalGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _readSysJournalBut
            // 
            this._readSysJournalBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readSysJournalBut.Location = new System.Drawing.Point(12, 426);
            this._readSysJournalBut.Name = "_readSysJournalBut";
            this._readSysJournalBut.Size = new System.Drawing.Size(75, 23);
            this._readSysJournalBut.TabIndex = 15;
            this._readSysJournalBut.Text = "Прочитать";
            this._readSysJournalBut.UseVisualStyleBackColor = true;
            this._readSysJournalBut.Click += new System.EventHandler(this._readSysJournalBut_Click);
            // 
            // _sysJournalGrid
            // 
            this._sysJournalGrid.AllowUserToAddRows = false;
            this._sysJournalGrid.AllowUserToDeleteRows = false;
            this._sysJournalGrid.AllowUserToResizeColumns = false;
            this._sysJournalGrid.AllowUserToResizeRows = false;
            this._sysJournalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._sysJournalGrid.BackgroundColor = System.Drawing.Color.White;
            this._sysJournalGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this._sysJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._sysJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msgCol});
            this._sysJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._sysJournalGrid.Name = "_sysJournalGrid";
            this._sysJournalGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this._sysJournalGrid.RowHeadersVisible = false;
            this._sysJournalGrid.RowHeadersWidth = 42;
            this._sysJournalGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._sysJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._sysJournalGrid.Size = new System.Drawing.Size(402, 420);
            this._sysJournalGrid.TabIndex = 14;
            // 
            // _saveSysJournalDlg
            // 
            this._saveSysJournalDlg.DefaultExt = "xml";
            this._saveSysJournalDlg.FileName = "МДО_ЖурналСистемы";
            this._saveSysJournalDlg.Filter = "МДО- журнал системы | *.xml";
            this._saveSysJournalDlg.Title = "Сохранить  журнал системы для МДО";
            // 
            // _openSysJounralDlg
            // 
            this._openSysJounralDlg.DefaultExt = "xml";
            this._openSysJounralDlg.Filter = "(*.xml) | *.xml";
            this._openSysJounralDlg.RestoreDirectory = true;
            this._openSysJounralDlg.Title = "Открыть журнал системы для МДО";
            // 
            // _deserializeSysJournalBut
            // 
            this._deserializeSysJournalBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._deserializeSysJournalBut.Location = new System.Drawing.Point(264, 426);
            this._deserializeSysJournalBut.Name = "_deserializeSysJournalBut";
            this._deserializeSysJournalBut.Size = new System.Drawing.Size(126, 23);
            this._deserializeSysJournalBut.TabIndex = 17;
            this._deserializeSysJournalBut.Text = "Загрузить из файла";
            this._deserializeSysJournalBut.UseVisualStyleBackColor = true;
            this._deserializeSysJournalBut.Click += new System.EventHandler(this._deserializeSysJournalBut_Click);
            // 
            // _serializeSysJournalBut
            // 
            this._serializeSysJournalBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._serializeSysJournalBut.Location = new System.Drawing.Point(147, 426);
            this._serializeSysJournalBut.Name = "_serializeSysJournalBut";
            this._serializeSysJournalBut.Size = new System.Drawing.Size(111, 23);
            this._serializeSysJournalBut.TabIndex = 16;
            this._serializeSysJournalBut.Text = "Сохранить в файл";
            this._serializeSysJournalBut.UseVisualStyleBackColor = true;
            this._serializeSysJournalBut.Click += new System.EventHandler(this._serializeSysJournalBut_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._configProgressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 454);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(402, 22);
            this.statusStrip1.TabIndex = 18;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _configProgressBar
            // 
            this._configProgressBar.Name = "_configProgressBar";
            this._configProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(16, 17);
            this._statusLabel.Text = "...";
            // 
            // _indexCol
            // 
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._indexCol.Width = 30;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.HeaderText = "Время";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._timeCol.Width = 65;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._msgCol.HeaderText = "Сообщение";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // MdoJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 476);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._deserializeSysJournalBut);
            this.Controls.Add(this._serializeSysJournalBut);
            this.Controls.Add(this._readSysJournalBut);
            this.Controls.Add(this._sysJournalGrid);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(418, 514);
            this.Name = "MdoJournalForm";
            this.Text = "MDO_Journal";
            this.Load += new System.EventHandler(this.MDO_Journal_Load);
            ((System.ComponentModel.ISupportInitialize)(this._sysJournalGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _readSysJournalBut;
        private System.Windows.Forms.DataGridView _sysJournalGrid;
        private System.Windows.Forms.SaveFileDialog _saveSysJournalDlg;
        private System.Windows.Forms.OpenFileDialog _openSysJounralDlg;
        private System.Windows.Forms.Button _deserializeSysJournalBut;
        private System.Windows.Forms.Button _serializeSysJournalBut;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar _configProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
    }
}