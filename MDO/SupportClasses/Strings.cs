﻿using System.Collections.Generic;

namespace BEMN.MDO.SupportClasses
{
    class Strings
    {
        public static List<string> JournalMessageType
        {
            get
            {
                return new List<string>(new[]
                    {
                        "XXXXXX",
                        "Питание включено",
                        "Датчик 1: засветка",
                        "Датчик 1: дуга",
                        "Датчик 1: неисправен",
                        "Датчик 2: засветка",
                        "Датчик 2: дуга",
                        "Датчик 2: неисправен",
                        "Датчик 3: засветка",
                        "Датчик 3: дуга",
                        "Датчик 3: неисправен",
                        "Ошибка питания 5 вольт",
                        "Ошибка теста HL",
                        "Реле 1 замкнуто",
                        "Реле 1 разомкнуто",
                        "Реле 2 замкнуто",
                        "Реле 2 разомкнуто",
                        "Реле 3 замкнуто",
                        "Реле 3 разомкнуто",
                        "Реле 4 замкнуто",
                        "Реле 4 разомкнуто",
                        "Квитирование по кнопке",
                        "Датчик 1: исправен",
                        "Датчик 2: исправен",
                        "Датчик 3: исправен",
                        "Новая конфигурация из уникона",
                        "Новая конфигурация от ТЭЗ-24",
                        "Квитирование от ТЭЗ-24",
                        "Квитирование из уникона",
                        "Питание отключено",
                        "Ошибка CAN связи",
                        "CAN восстановлен",
                        "Аппаратная ошибка CAN"
                    });
            }
        }

        public static List<string> SygnalType
        {
            get
            {
                return new List<string>(new[] { "Повторитель",
                                                       "Блинкер"
                                                       });
            }
        }

        public static List<string> DeviceType
        {
            get
            {
                return new List<string>(new[] {"Нет",
                                                      "ОЛ", 
                                                      "Ввод 1",
                                                      "Ввод 2",
                                                      "СВ 1",
                                                      "ТН", 
                                                      "ТСН"
                                                       });
            }
        }
    }
}
