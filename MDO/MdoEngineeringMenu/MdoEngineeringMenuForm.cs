﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Interfaces;
using BEMN.MDO.MdoJournal.Structures;

namespace BEMN.MDO.MdoEngineeringMenu
{
    public partial class MdoEngineeringMenuForm : Form, IFormView
    {
        private readonly MDO _device;

        private int _recsCount;
        private int _endRecIndex;
        private int _newIndex;
        
        private MemoryEntity<JournalStructV10> _journalV10;
        private MemoryEntity<JournalStructV12> _journalV12;
        private MemoryEntity<OneWordStruct> _versionEntity;
        private MemoryEntity<JournalInfoStruct> _journalInfo;

        public MdoEngineeringMenuForm()
        {
            InitializeComponent();
        }

        public MdoEngineeringMenuForm(MDO device)
        {
            this.InitializeComponent();
            this._device = device;
            this._journalInfo = device.JournalInfoStr;
            this._journalV10 = device.JournalV10Str;
            this._journalV12 = device.JournalV12Str;
            this._versionEntity = device.VersionEntitySj;
            this._versionEntity.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._journalInfo.LoadStruct);
            this._journalInfo.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.InfoStructRead);
            this._journalV10.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ClearJournalOk);
            this._journalV12.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ClearJournalOk);
        }

        private void ClearJournalOk()
        {
            this.ClearJournal();
            this._statusLabel.Text = "Журнал очищен";
        }

        private void InfoStructRead()
        {
            this.PrepareJournal();
            if (this._versionEntity.Value.Word >= 117)
            {
                this._journalV12.LoadStruct();
            }
            else
            {
                this._journalV10.LoadStruct();
            }
        }
        
        private void PrepareJournal()
        {
            //Количество записей.
            this._recsCount = this._journalInfo.Value.JournalRecordCount;
            //Смещение первой записи.
            this._endRecIndex = this._journalInfo.Value.JournalRecordEnd / new JournalRecordStruct().GetStructInfo().FullSize;
        }
    

        private void _clearJournal_Click(object sender, EventArgs e)
        {
            this.ClearJournal();
            this._statusLabel.Text = "Журнал очищен";
        }

        private void ClearJournal()
        {
            try
            {
                if (!this._device.DeviceDlgInfo.IsConnectionMode) return;
                

                if (MessageBox.Show("Данные журнала аварий будут потеряны. \r\nВы уверены, что хотите очистить журнал?",
                        string.Empty, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    this._statusLabel.Text = "Идет очистка журнала...";

                    JournalInfoStruct js = new JournalInfoStruct();

                    js.JournalRecordCount = 0;
                    js.JournalRecordEnd = 3;
                    js.JournalRecordNew = 0;

                    _journalInfo.Value = js;

                    _journalInfo.SaveStruct();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка очистки журнала");
                this._statusLabel.Text = "Очистка журнала невозможна";
            }
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MDO); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(MdoEngineeringMenuForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.programming.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Инженерное меню"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }


        #endregion


    }
}
