﻿namespace BEMN.MDO.MdoEngineeringMenu
{
    partial class MdoEngineeringMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._clearJournal = new System.Windows.Forms.Button();
            this._statusLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _clearJournal
            // 
            this._clearJournal.Location = new System.Drawing.Point(23, 12);
            this._clearJournal.Name = "_clearJournal";
            this._clearJournal.Size = new System.Drawing.Size(246, 23);
            this._clearJournal.TabIndex = 0;
            this._clearJournal.Text = "Стереть журнал";
            this._clearJournal.UseVisualStyleBackColor = true;
            this._clearJournal.Click += new System.EventHandler(this._clearJournal_Click);
            // 
            // _statusLabel
            // 
            this._statusLabel.AutoSize = true;
            this._statusLabel.Location = new System.Drawing.Point(23, 45);
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(16, 13);
            this._statusLabel.TabIndex = 1;
            this._statusLabel.Text = "...";
            // 
            // MdoEngineeringMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 67);
            this.Controls.Add(this._statusLabel);
            this.Controls.Add(this._clearJournal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MdoEngineeringMenuForm";
            this.Text = "Инженерное меню";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _clearJournal;
        private System.Windows.Forms.Label _statusLabel;
    }
}