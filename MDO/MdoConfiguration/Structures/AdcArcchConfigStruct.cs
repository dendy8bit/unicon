﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MDO.MdoConfiguration.Structures
{
    public class AdcArcchConfigStruct : StructBase
    {
        [Layout(0)] private ushort _channelOn;
        [Layout(1)] private ushort _channelLowVoltLevel;
        [Layout(2)] private ushort _channelTimeLimit;
        [Layout(3)] private ushort _testLowVoltLevel;
        [Layout(4)] private ushort _testTimeLimit;
        
        #region Свойства

        public bool Ch1Connectr1On
        {
            get { return Common.GetBit(this._channelOn, 1); }
            set { this._channelOn = Common.SetBit(this._channelOn, 1, value); }
        }

        public bool Ch1Connectr2On
        {
            get { return Common.GetBit(this._channelOn, 2); }
            set { this._channelOn = Common.SetBit(this._channelOn, 2, value); }
        }

        public bool Ch1Connectr3On
        {
            get { return Common.GetBit(this._channelOn, 3); }
            set { this._channelOn = Common.SetBit(this._channelOn, 3, value); }
        }

        public bool Ch1Connectr4On
        {
            get { return Common.GetBit(this._channelOn, 4); }
            set { this._channelOn = Common.SetBit(this._channelOn, 4, value); }
        }

        public bool ChannelOn
        {
            get { return Common.GetBit(this._channelOn, 0); }
            set { this._channelOn = Common.SetBit(this._channelOn, 0, value); }
        }

        public ushort ChannelLowVoltLevel
        {
            get { return this._channelLowVoltLevel; }
            set { this._channelLowVoltLevel = value; }
        }

        public ushort ChannelTimeLimit
        {
            get { return this._channelTimeLimit; }
            set { this._channelTimeLimit = value; }
        }

        #endregion
    }
}