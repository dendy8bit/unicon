﻿using System;
using System.Globalization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MDO.MdoConfiguration.Structures
{
    public class FlashMemoryStruct : StructBase
    {
        #region Const

        private const int ADC_ARCCH_CONFIG_COUNT = 3;
        private const int RELAY_COUNT = 4;
        #endregion

        #region Fields
        [Layout(0, Count = ADC_ARCCH_CONFIG_COUNT)] private AdcArcchConfigStruct[] _archChConfigStruct;
        [Layout(1)] private ushort _reserve;
        [Layout(2)] private TestConfigStruct _testConfigStruct;
        [Layout(3, Count = RELAY_COUNT)] private RelayConfigStruct[] _relayConfigStruct;
        [Layout(4)] private ushort _addrAndType;
        #endregion

        public AdcArcchConfigStruct[] ArchChConfigStruct
        {
            get { return this._archChConfigStruct; }
        }

        public string DeviceNumber
        {
            get
            {
                byte num = Common.LOBYTE(this._addrAndType);
                return num.ToString(CultureInfo.CurrentCulture);
            }
            set
            {
                byte hiByte = Common.HIBYTE(this._addrAndType);
                byte num = Convert.ToByte(value);
                this._addrAndType = Common.TOWORD(hiByte, num);
            }
        }
        
        #region Тестирование
        public bool TestEnabled
        {
            get { return Common.GetBit(this._testConfigStruct.TestOn, 0); }
            set { this._testConfigStruct.TestOn = Common.SetBit(this._testConfigStruct.TestOn, 0, value); }
        }


        public ushort TestTime
        {
            get { return this._testConfigStruct.TestPeriod; }
            set { this._testConfigStruct.TestPeriod = value; }
        }

        #endregion

        public RelayConfigStruct[] RelayConfigStruct
        {
            get { return this._relayConfigStruct; }
        }

        public bool ArOn
        {
            get { return Common.GetBit(this._testConfigStruct.TestOn, 1); }
            set { this._testConfigStruct.TestOn = Common.SetBit(this._testConfigStruct.TestOn, 1, value); }
        }
    }
}