﻿using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MDO.MdoConfiguration.Structures;
using BEMN.MDO.SomeStructures;
using BEMN.MDO.SupportClasses;

namespace BEMN.MDO.MdoConfiguration
{
    public partial class MdoConfigurationForm : Form, IFormView
    {
        #region [Constants]
        private const string PASS = "1111";
        private const string INCORRECT_PASSWORD = "Неверный пароль";
        private const string SETPOINTS_SUCCESSFUL_WRITE = "Уставки записаны успешно.";
        private const string WRITE_RUNNING = "Идет запись уставок.";
        private const string MDO_SAVE_MESSAGE_PATTERN = "Записать конфигурацию МДО № {0} ?";
        private const string ATTENTION = "Внимание";
        private const string DATA_CHECKING = "Проверка данных.";
        private const string FILE_IS_DAMAGED_PATTERN = "Файл {0} не является файлом уставок МДО или поврежден";
        private const string FILE_IS_LOADED_PATTERN = "Файл {0} успешно загружен";
        private const string FILE_IS_SAVED_PATTERN = "Файл {0} успешно сохранен";
        private const string SETPOINTS_READ_SUCCESSFUL = "Уставки прочитаны успешно.";
        private const string SETPOINTS_WROTE = "Уставки записаны.";
        private const string READING_SETPOINTS = "Чтение уставок.";
        private const string ERROR_READ_CONFIGURATION = "Ошибка чтения конфигурации";
        private const string ERROR_WRITE_CONFIG = "Невозможно записать конфигурацию";
        private const string PULSE_INCORRECT_VALUE = "Некорректные данные, проверьте ввод. Значение должно лежать в диапазоне 10-1000 и должно быть кратно 10.";
        private const string TEST_TIME_IS_ERROR = "Время тестирования должно быть в пределах 1-250с";
        private const string CHANNEL_VALUES_ERROR = "Неверные настройки каналов";
        private const string TEST_ERROR = "Неверное время теста";
        private const string FILE_SAVE_FAILD = "Файл не сохранен";
        private const string XML_HEAD = "MDO_SET_POINTS";
        private const string MDO_BASE_CONFIG_PATH = "\\MDO\\MDO_BaseConfig.xml";
        private const string BASE_CONFIG_LOAD_FAIL = "Невозможно загрузить базовую конфигурацию";
        #endregion [Constants]

        #region [Private fields]
        private readonly MDO _device;
        private FlashMemoryStruct _flashMemoryStruct;
        private readonly MemoryEntity<FlashMemoryStruct> _flashMemory;
        private readonly MemoryEntity<CommandStruct> _commandStruct;
        private CommandStates _cmSt;
        #endregion [Private fields]

        #region [Ctor's]
        public MdoConfigurationForm()
        {
            this.InitializeComponent();
        }

        public MdoConfigurationForm(MDO device)
        {
            this.InitializeComponent();
            this._device = device; 
            this._flashMemoryStruct = new FlashMemoryStruct();
            this._flashMemory = this._device.FlashMemStr;
            this._flashMemory.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.StructReadConfig);
            this._flashMemory.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.StructWrite);
            this._flashMemory.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.AllReadFail);
            this._flashMemory.ReadFail += HandlerHelper.CreateHandler(this, this.ReadFail);
            this._commandStruct = this._device.CommandStr;
            this._commandStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.CommandWriteOk);
            this._commandStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.StructReadConfig);
        }
        #endregion [Ctor's]

        #region Подписчики
        private void StructReadConfig()
        {
            this._flashMemoryStruct = this._flashMemory.Value;
            this.StructShow();
        }

        private void StructWrite()
        {
            if (this._device.CommandState == CommandStates.START_WRITE_FLASH_MEMORY)
            {
                this.ProgressBarInc();
                this.SaveCommand(CommandStates.END_WRITE_FLASH_MEMORY);
                MessageBox.Show(SETPOINTS_WROTE);
            }
        }

        private void CommandWriteOk()
        {
            if (this._device.CommandState == CommandStates.START_WRITE_FLASH_MEMORY)
            {
                this._flashMemory.Value = this._flashMemoryStruct;
                this._flashMemory.SaveStruct();
            }
            if (this._device.CommandState == CommandStates.END_WRITE_FLASH_MEMORY)
            {
                this._statusLabel.Text = SETPOINTS_SUCCESSFUL_WRITE;
                this.SaveCommand(CommandStates.NONE);
            }

        }

        private void ReadFail()
        {
            int newValue = this._configProgressBar.Value + 10;
            if (newValue <= this._configProgressBar.Maximum)
                this._configProgressBar.Value = newValue;
            this._statusLabel.Text = ERROR_READ_CONFIGURATION;
        }

        private void AllReadFail()
        {
            this._configProgressBar.Value = this._configProgressBar.Maximum;
            this._statusLabel.Text = ERROR_READ_CONFIGURATION;
            MessageBox.Show(ERROR_READ_CONFIGURATION);
        }
        #endregion

        #region  Подготовка формы

        private void PrepareForm()
        {
            //Каналы
            this._CH1Enabled.Checked = false;
            this._CH2Enabled.Checked = false;
            this._CH3Enabled.Checked = false;
            //Тест
            this._TestEnabled.Checked = false;
            //Реле
            this._dataGridReleView.Rows.Clear();
            this._extReleTypeCol.DataSource = Strings.SygnalType;
            for (int i = 0; i < this._flashMemoryStruct.RelayConfigStruct.Length; i++)
            {
                this._dataGridReleView.Rows.Add(i + 1, Strings.SygnalType[0], 0, false, false, false);
            }
            //Номер тип МДО
            this._deviceAddr.SelectedIndex = 0;
        }

        #endregion

        #region Дополнительные функции
        private void LoadConfigurationBlocks()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._configProgressBar.Value = 0;
            this._statusLabel.Text = READING_SETPOINTS;
            this._flashMemory.LoadStruct();
        }
        private void ProgressBarInc()
        {
            this._configProgressBar.Value = this._configProgressBar.Maximum;
        }

        /// <summary>
        /// Вывод на экран
        /// </summary>
        private void StructShow()
        {
            //Канал 1
            this._CH1Enabled.Checked = this._flashMemoryStruct.ArchChConfigStruct[0].ChannelOn;
            this._sensorChannelControl1.ReactionLevel = this._flashMemoryStruct.ArchChConfigStruct[0].ChannelLowVoltLevel;
            this._sensorChannelControl1.WaitingTime = this._flashMemoryStruct.ArchChConfigStruct[0].ChannelTimeLimit;
            //Канал 2
            this._CH2Enabled.Checked = this._flashMemoryStruct.ArchChConfigStruct[1].ChannelOn;
            this._sensorChannelControl2.ReactionLevel = this._flashMemoryStruct.ArchChConfigStruct[1].ChannelLowVoltLevel;
            this._sensorChannelControl2.WaitingTime = this._flashMemoryStruct.ArchChConfigStruct[1].ChannelTimeLimit;
            //Канал 3
            this._CH3Enabled.Checked = this._flashMemoryStruct.ArchChConfigStruct[2].ChannelOn;
            this._sensorChannelControl3.ReactionLevel = this._flashMemoryStruct.ArchChConfigStruct[2].ChannelLowVoltLevel;
            this._sensorChannelControl3.WaitingTime = this._flashMemoryStruct.ArchChConfigStruct[2].ChannelTimeLimit;
            //Тест
            this._TestEnabled.Checked = this._flashMemoryStruct.TestEnabled;
            this._TestTime.Text = this._flashMemoryStruct.TestTime.ToString(CultureInfo.InvariantCulture);
            //Реле
            bool[,] sensorsRelays = new bool[3, 4];
            for (int i = 0; i < 3; i++)
            {
                sensorsRelays[i, 0] = this._flashMemoryStruct.ArchChConfigStruct[i].Ch1Connectr1On;
                sensorsRelays[i, 1] = this._flashMemoryStruct.ArchChConfigStruct[i].Ch1Connectr2On;
                sensorsRelays[i, 2] = this._flashMemoryStruct.ArchChConfigStruct[i].Ch1Connectr3On;
                sensorsRelays[i, 3] = this._flashMemoryStruct.ArchChConfigStruct[i].Ch1Connectr4On;
            }
            for (int i = 0; i < this._flashMemoryStruct.RelayConfigStruct.Length; i++)
            {
                this._dataGridReleView[1, i].Value = this._flashMemoryStruct.RelayConfigStruct[i].RelayTypeStr;
                this._dataGridReleView[2, i].Value = this._flashMemoryStruct.RelayConfigStruct[i].RelePulseOfflineTime;
                this._dataGridReleView[3, i].Value = sensorsRelays[0, i];
                this._dataGridReleView[4, i].Value = sensorsRelays[1, i];
                this._dataGridReleView[5, i].Value = sensorsRelays[2, i];
            }
            this.arCBox.Checked = !this._flashMemoryStruct.ArOn;
            //Тип и номер устройства
            this._deviceAddr.SelectedItem = this._flashMemoryStruct.DeviceNumber;

            this.ProgressBarInc();
            this._statusLabel.Text = SETPOINTS_READ_SUCCESSFUL;
        }
        /// <summary>
        /// Запись структуры
        /// </summary>
        private bool WriteStruct()
        {
            bool ret = true;
            try
            {
                //Канал 1
                this._flashMemoryStruct.ArchChConfigStruct[0].ChannelOn = this._CH1Enabled.Checked;
                this._flashMemoryStruct.ArchChConfigStruct[0].ChannelLowVoltLevel = this._sensorChannelControl1.ReactionLevel;
                this._flashMemoryStruct.ArchChConfigStruct[0].ChannelTimeLimit = this._sensorChannelControl1.WaitingTime;
                //Канал 2
                this._flashMemoryStruct.ArchChConfigStruct[1].ChannelOn = this._CH2Enabled.Checked;
                this._flashMemoryStruct.ArchChConfigStruct[1].ChannelLowVoltLevel = this._sensorChannelControl2.ReactionLevel;
                this._flashMemoryStruct.ArchChConfigStruct[1].ChannelTimeLimit = this._sensorChannelControl2.WaitingTime;
                //Канал 3 
                this._flashMemoryStruct.ArchChConfigStruct[2].ChannelOn = this._CH3Enabled.Checked;
                this._flashMemoryStruct.ArchChConfigStruct[2].ChannelLowVoltLevel = this._sensorChannelControl3.ReactionLevel;
                this._flashMemoryStruct.ArchChConfigStruct[2].ChannelTimeLimit = this._sensorChannelControl3.WaitingTime;

            }
            catch (Exception)
            {
                ret = false;
                MessageBox.Show(CHANNEL_VALUES_ERROR);
            }

            //Тест
            this._flashMemoryStruct.TestEnabled = this._TestEnabled.Checked;
            ushort testTime;
            if (ushort.TryParse(this._TestTime.Text, out testTime))
            {
                if (this._flashMemoryStruct.TestEnabled)
                {
                    if ((testTime >= 1) & (testTime <= 250))
                    {
                        this._flashMemoryStruct.TestTime = testTime;
                    }
                    else
                    {
                        MessageBox.Show(TEST_TIME_IS_ERROR);
                        //Минимально допустимое значение
                        this._flashMemoryStruct.TestTime = 1;
                    }
                }
            }
            else
            {
                ret = false;
                MessageBox.Show(TEST_ERROR);
            }
            //Реле
            try
            {
                for (int i = 0; i < this._dataGridReleView.Rows.Count; i++)
                {
                    this._flashMemoryStruct.RelayConfigStruct[i].RelayTypeStr = this._dataGridReleView.Rows[i].Cells[1].Value.ToString();
                    if (this.PulseValidate(this._dataGridReleView, 2, i))
                        this._flashMemoryStruct.RelayConfigStruct[i].RelePulseOfflineTime = Convert.ToUInt16(this._dataGridReleView.Rows[i].Cells[2].Value);
                }
                
                bool[,] sensorsRelays = new bool[3, 4];
                for (int i = 0; i < this._flashMemoryStruct.RelayConfigStruct.Length; i++)
                {
                    sensorsRelays[0, i] = Convert.ToBoolean(this._dataGridReleView[3, i].Value);
                    sensorsRelays[1, i] = Convert.ToBoolean(this._dataGridReleView[4, i].Value);
                    sensorsRelays[2, i] = Convert.ToBoolean(this._dataGridReleView[5, i].Value);
                }

                for (int i = 0; i < 3; i++)
                {
                    this._flashMemoryStruct.ArchChConfigStruct[i].Ch1Connectr1On = sensorsRelays[i, 0];
                    this._flashMemoryStruct.ArchChConfigStruct[i].Ch1Connectr2On = sensorsRelays[i, 1];
                    this._flashMemoryStruct.ArchChConfigStruct[i].Ch1Connectr3On = sensorsRelays[i, 2];
                    this._flashMemoryStruct.ArchChConfigStruct[i].Ch1Connectr4On = sensorsRelays[i, 3];
                }
                this._flashMemoryStruct.ArOn = !this.arCBox.Checked;
            }
            catch
            {
                ret = false;
                MessageBox.Show("Конфигурация реле не записалась");
            }
            //номер и тип
            try
            {
                this._flashMemoryStruct.DeviceNumber = this._deviceAddr.SelectedItem.ToString();
            }
            catch
            {
                ret = false;
                MessageBox.Show("Номер и тип не установились");
            }
            return ret;
        }
        #endregion

        //Изменение "Время теста"
        private void TestTimeChanged()
        {
            byte result;

            if (byte.TryParse(this._TestTime.Text, out result))
            {
                if ((this._TestEnabled.Checked & (result >= 1) & (result <= 250)) | !this._TestEnabled.Checked)
                {
                    this._flashMemoryStruct.TestTime = result;
                    return;
                }
            }
            MessageBox.Show(TEST_TIME_IS_ERROR);
        }

        private void _testTime_TextChanged(object sender, EventArgs e)
        {
            this.TestTimeChanged();
        }

        #region Команда
        
        public void SaveCommand(CommandStates command)
        {
            this._device.CommandState = command;
            if (this._device.CommandState != CommandStates.NONE)
            {
                CommandStruct cm = new CommandStruct();
                cm.Kvint = (ushort)command;
                this._commandStruct.Value = cm;
                this._commandStruct.SaveStruct();
            }
        }

        #endregion Команда


        #region Обработчики событий
        private void MDO_Configuration_Load(object sender, EventArgs e)
        {
            this._device.DeviceCanNumberChanged += i =>
            {
                if (IsHandleCreated)
                    Invoke(new Action(() => Text = this._device.CreateFormCaption(this, i)));
            };
            this._device.RefreshNumber();
            this.PrepareForm();
            this.LoadBaseConfig();
            this.LoadConfigurationBlocks();
        }
        
        private void LoadBaseConfig()  //загрузка базовой конфигурации
        {
            if(!this.Deserialize(Path.GetDirectoryName(Application.ExecutablePath) + MDO_BASE_CONFIG_PATH))
            {
                this._statusLabel.Text = BASE_CONFIG_LOAD_FAIL;
            }
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.LoadConfigurationBlocks();
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._configProgressBar.Value = 0;
            this._statusLabel.Text = DATA_CHECKING;
            string saveMessage = string.Format(MDO_SAVE_MESSAGE_PATTERN, this._device.DeviceNumber);
            if (DialogResult.Yes == MessageBox.Show(saveMessage, ATTENTION, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                this._statusLabel.Text = WRITE_RUNNING;
                if (this.WriteStruct())
                {
                    this.SaveCommand(CommandStates.START_WRITE_FLASH_MEMORY);
                }
                else
                {
                    this._statusLabel.Text = ERROR_WRITE_CONFIG;
                }
            }
        }

        private void _TestEnabled_CheckedChanged(object sender, EventArgs e)
        {
            this._TestTime.Enabled = this._TestEnabled.Checked;

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            //if (this.arCBox.Checked)
            //{
            //    this._deviceAddr.Enabled = true;
            //    this._commonOptionsGroupBox.Enabled = false;
            //    this.groupBox5.Enabled = false;
            //    this._dataGridReleView.Enabled = false;
            //}
            //else if (!this.arCBox.Checked)
            //{
            //    this._deviceAddr.Enabled = false;
            //    this._commonOptionsGroupBox.Enabled = true;
            //    this.groupBox5.Enabled = true;
            //    this._dataGridReleView.Enabled = true;
            //}
            this._deviceAddr.Enabled = this.arCBox.Checked;
            this._commonOptionsGroupBox.Enabled = !this.arCBox.Checked;
            this.groupBox5.Enabled = !this.arCBox.Checked;
            this._dataGridReleView.Enabled = !this.arCBox.Checked;
        }


        private void _CH1Enabled_CheckedChanged(object sender, EventArgs e)
        {
            this._sensorChannelControl1.Enabled = this._CH1Enabled.Checked;
        }

        private void _CH2Enabled_CheckedChanged(object sender, EventArgs e)
        {
            this._sensorChannelControl2.Enabled = this._CH2Enabled.Checked;
        }

        private void _CH3Enabled_CheckedChanged(object sender, EventArgs e)
        {
            this._sensorChannelControl3.Enabled = this._CH3Enabled.Checked;
        }
        #endregion



        private void validatePassButton_Click(object sender, EventArgs e)
        {
            this.PassCheck();
        }
        
        private void PassCheck()
        {
            if (this._passTB.Text == PASS)
            {
                this._passTB.Text = string.Empty;
                this._sensorsPanel.Enabled = true;

                this._passInsertBox.Enabled = false;
            }
            else
            {
                MessageBox.Show(INCORRECT_PASSWORD);
                this._passTB.Text = string.Empty;
            }
        }

        private void _passTB_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                this.PassCheck();
                e.Handled = true;
            }
        }

        private void MdoConfigurationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._flashMemory.RemoveStructQueries();
            this._commandStruct.RemoveStructQueries();
        }

        private void _dataGridReleView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                DataGridView sourse = sender as DataGridView;
                if (sourse != null)
                    this.PulseValidate(sourse, e.ColumnIndex, e.RowIndex);
            }

        }

        private bool PulseValidate(DataGridView sourse, int column, int row)
        {
            int value;
            if (int.TryParse(sourse[column, row].Value.ToString(), out value))
            {
                if (value >= 10 & value <= 1000 & (value % 10 == 0))
                {
                    sourse[column, row].Style.BackColor = Color.White;
                    return true;
                }
            }
            sourse[column, row].Style.BackColor = Color.Red;
            MessageBox.Show(PULSE_INCORRECT_VALUE);
            return false;
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName);
            }
        }

        private void Serialize(string binFilePath)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MDO"));
                if (!this.WriteStruct())
                {
                    throw new Exception();
                }
                ushort[] values = this._flashMemoryStruct.GetValues();
                XmlElement element = doc.CreateElement(XML_HEAD);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);
                doc.Save(binFilePath);
                this._statusStrip1.Text = string.Format(FILE_IS_SAVED_PATTERN, binFilePath);
            }
            catch (Exception)
            {
                MessageBox.Show(FILE_SAVE_FAILD);
            }
        }

        private bool Deserialize(string binFilePath)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFilePath);
                XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);
                this._flashMemoryStruct.InitStruct(values);
                this.StructShow();
                this._statusLabel.Text = string.Format(FILE_IS_LOADED_PATTERN, binFilePath);
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show(string.Format(FILE_IS_DAMAGED_PATTERN, binFilePath));
                return false;
            }
        }
        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveInFile();
        }

        private void SaveInFile()
        {
            this._saveConfigurationDlg.FileName = $"МДО-1 Уставки Версия {this._device.DeviceVersion}";
            if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
            {
                this.Serialize(this._saveConfigurationDlg.FileName);
                MessageBox.Show(string.Format(FILE_IS_SAVED_PATTERN, this._saveConfigurationDlg.FileName));
            }
        }


        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MDO); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(MdoConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void MdoConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.LoadConfigurationBlocks();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.LoadConfigurationBlocks();
                return;
            }
            if (e.ClickedItem == this.writeToDevice)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
            }
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled =
                this.writeToDevice.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }
    }
}
