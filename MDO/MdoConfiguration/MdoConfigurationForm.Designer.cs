﻿using BEMN.MDO.MdoConfiguration.Controls;

namespace BEMN.MDO.MdoConfiguration
{
    partial class MdoConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._configProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this._TestTime = new System.Windows.Forms.MaskedTextBox();
            this.label31 = new System.Windows.Forms.Label();
            this._TestEnabled = new System.Windows.Forms.CheckBox();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this.arCBox = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._dataGridReleView = new System.Windows.Forms.DataGridView();
            this._extReleNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._extReleTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._extReleTypeImpCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._sensor1Column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._sensor2Column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._sensor3Column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label25 = new System.Windows.Forms.Label();
            this._deviceAddr = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this._mdoOptionsPage = new System.Windows.Forms.TabPage();
            this._networkOptionsGroupBox = new System.Windows.Forms.GroupBox();
            this._commonOptionsGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._CH3Enabled = new System.Windows.Forms.CheckBox();
            this._CH1Enabled = new System.Windows.Forms.CheckBox();
            this._CH2Enabled = new System.Windows.Forms.CheckBox();
            this._datchiksOptions = new System.Windows.Forms.TabPage();
            this._passInsertBox = new System.Windows.Forms.GroupBox();
            this._validatePassButton = new System.Windows.Forms.Button();
            this._passTB = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this._sensorsPanel = new System.Windows.Forms.Panel();
            this._sensorChannelControl1 = new BEMN.MDO.MdoConfiguration.Controls.SensorChannelControl();
            this._sensorChannelControl2 = new BEMN.MDO.MdoConfiguration.Controls.SensorChannelControl();
            this._sensorChannelControl3 = new BEMN.MDO.MdoConfiguration.Controls.SensorChannelControl();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDevice = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this._statusStrip1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridReleView)).BeginInit();
            this.tabControl1.SuspendLayout();
            this._mdoOptionsPage.SuspendLayout();
            this._networkOptionsGroupBox.SuspendLayout();
            this._commonOptionsGroupBox.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this._datchiksOptions.SuspendLayout();
            this._passInsertBox.SuspendLayout();
            this._sensorsPanel.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // _toolTip
            // 
            this._toolTip.ShowAlways = true;
            this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // _statusStrip1
            // 
            this._statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._configProgressBar,
            this._statusLabel});
            this._statusStrip1.Location = new System.Drawing.Point(0, 441);
            this._statusStrip1.Name = "_statusStrip1";
            this._statusStrip1.Size = new System.Drawing.Size(458, 22);
            this._statusStrip1.TabIndex = 3;
            this._statusStrip1.Text = "statusStrip1";
            // 
            // _configProgressBar
            // 
            this._configProgressBar.Name = "_configProgressBar";
            this._configProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(16, 17);
            this._statusLabel.Text = "...";
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.AutoSize = true;
            this._writeConfigBut.Location = new System.Drawing.Point(12, 410);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(149, 23);
            this._writeConfigBut.TabIndex = 31;
            this._writeConfigBut.Text = "Записать в устройство";
            this.toolTip1.SetToolTip(this._writeConfigBut, "Записать конфигурацию в устройство (CTRL+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.AutoSize = true;
            this._readConfigBut.Location = new System.Drawing.Point(12, 381);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(149, 23);
            this._readConfigBut.TabIndex = 30;
            this._readConfigBut.Text = "Прочитать из устройства";
            this.toolTip1.SetToolTip(this._readConfigBut, "Прочитать конфигурацию из устройства (CTRL+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.AllowDrop = true;
            this._saveConfigBut.AutoSize = true;
            this._saveConfigBut.Location = new System.Drawing.Point(297, 410);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(149, 23);
            this._saveConfigBut.TabIndex = 33;
            this._saveConfigBut.Text = "Сохранить в файл";
            this.toolTip1.SetToolTip(this._saveConfigBut, "Сохранить конфигурацию в файл (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.AutoSize = true;
            this._loadConfigBut.Location = new System.Drawing.Point(297, 381);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(149, 23);
            this._loadConfigBut.TabIndex = 32;
            this._loadConfigBut.Text = "Загрузить из файла";
            this.toolTip1.SetToolTip(this._loadConfigBut, "Загрузить конфигурацию из файла (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this._TestTime);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this._TestEnabled);
            this.groupBox4.Location = new System.Drawing.Point(6, 119);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(185, 57);
            this.groupBox4.TabIndex = 37;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Тестирование датчиков";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label30.Location = new System.Drawing.Point(138, 29);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(13, 13);
            this.label30.TabIndex = 6;
            this.label30.Text = "с";
            // 
            // _TestTime
            // 
            this._TestTime.Enabled = false;
            this._TestTime.Location = new System.Drawing.Point(63, 26);
            this._TestTime.Name = "_TestTime";
            this._TestTime.Size = new System.Drawing.Size(69, 20);
            this._TestTime.TabIndex = 5;
            this._TestTime.Tag = "1;65535";
            this._TestTime.Text = "10";
            this._TestTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._TestTime.TextChanged += new System.EventHandler(this._testTime_TextChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(8, 29);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(45, 13);
            this.label31.TabIndex = 4;
            this.label31.Text = "Период";
            // 
            // _TestEnabled
            // 
            this._TestEnabled.AutoSize = true;
            this._TestEnabled.Location = new System.Drawing.Point(154, 0);
            this._TestEnabled.Name = "_TestEnabled";
            this._TestEnabled.Size = new System.Drawing.Size(15, 14);
            this._TestEnabled.TabIndex = 0;
            this._TestEnabled.UseVisualStyleBackColor = true;
            this._TestEnabled.CheckedChanged += new System.EventHandler(this._TestEnabled_CheckedChanged);
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "bin";
            this._openConfigurationDlg.Filter = "Файл уставок МДО (*.xml)|*.xml";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "Открыть файл уставок МДО";
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "bin";
            this._saveConfigurationDlg.FileName = "МДО_уставки";
            this._saveConfigurationDlg.Filter = "Файл уставок МДО (*.xml)|*.xml";
            this._saveConfigurationDlg.Title = "Сохранение файла уставок МДО";
            // 
            // arCBox
            // 
            this.arCBox.AutoSize = true;
            this.arCBox.Location = new System.Drawing.Point(187, 0);
            this.arCBox.Name = "arCBox";
            this.arCBox.Size = new System.Drawing.Size(15, 14);
            this.arCBox.TabIndex = 6;
            this.arCBox.UseVisualStyleBackColor = true;
            this.arCBox.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._dataGridReleView);
            this.groupBox5.Location = new System.Drawing.Point(6, 186);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(416, 145);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Конфигурация реле в автономном режиме";
            // 
            // _dataGridReleView
            // 
            this._dataGridReleView.AllowUserToAddRows = false;
            this._dataGridReleView.AllowUserToDeleteRows = false;
            this._dataGridReleView.AllowUserToResizeColumns = false;
            this._dataGridReleView.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.NullValue = null;
            this._dataGridReleView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this._dataGridReleView.BackgroundColor = System.Drawing.SystemColors.Control;
            this._dataGridReleView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this._dataGridReleView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._dataGridReleView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extReleNumberCol,
            this._extReleTypeCol,
            this._extReleTypeImpCol,
            this._sensor1Column,
            this._sensor2Column,
            this._sensor3Column});
            this._dataGridReleView.Location = new System.Drawing.Point(7, 19);
            this._dataGridReleView.Name = "_dataGridReleView";
            this._dataGridReleView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this._dataGridReleView.RowHeadersVisible = false;
            this._dataGridReleView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._dataGridReleView.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this._dataGridReleView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._dataGridReleView.Size = new System.Drawing.Size(398, 116);
            this._dataGridReleView.TabIndex = 2;
            this._dataGridReleView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._dataGridReleView_CellEndEdit);
            // 
            // _extReleNumberCol
            // 
            this._extReleNumberCol.HeaderText = "№";
            this._extReleNumberCol.Name = "_extReleNumberCol";
            this._extReleNumberCol.ReadOnly = true;
            this._extReleNumberCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._extReleNumberCol.Width = 30;
            // 
            // _extReleTypeCol
            // 
            this._extReleTypeCol.HeaderText = "Тип";
            this._extReleTypeCol.Name = "_extReleTypeCol";
            this._extReleTypeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._extReleTypeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extReleTypeCol.Width = 120;
            // 
            // _extReleTypeImpCol
            // 
            this._extReleTypeImpCol.HeaderText = "Тимп, мс";
            this._extReleTypeImpCol.Name = "_extReleTypeImpCol";
            this._extReleTypeImpCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._extReleTypeImpCol.Width = 67;
            // 
            // _sensor1Column
            // 
            this._sensor1Column.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._sensor1Column.HeaderText = "Датчик 1";
            this._sensor1Column.MinimumWidth = 30;
            this._sensor1Column.Name = "_sensor1Column";
            this._sensor1Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._sensor1Column.Width = 59;
            // 
            // _sensor2Column
            // 
            this._sensor2Column.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._sensor2Column.HeaderText = "Датчик 2";
            this._sensor2Column.MinimumWidth = 30;
            this._sensor2Column.Name = "_sensor2Column";
            this._sensor2Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._sensor2Column.Width = 59;
            // 
            // _sensor3Column
            // 
            this._sensor3Column.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._sensor3Column.HeaderText = "Датчик 3";
            this._sensor3Column.MinimumWidth = 30;
            this._sensor3Column.Name = "_sensor3Column";
            this._sensor3Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._sensor3Column.Width = 59;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(20, 27);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(70, 13);
            this.label25.TabIndex = 39;
            this.label25.Text = "Номер МДО";
            // 
            // _deviceAddr
            // 
            this._deviceAddr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._deviceAddr.Enabled = false;
            this._deviceAddr.FormattingEnabled = true;
            this._deviceAddr.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24"});
            this._deviceAddr.Location = new System.Drawing.Point(112, 24);
            this._deviceAddr.Name = "_deviceAddr";
            this._deviceAddr.Size = new System.Drawing.Size(67, 21);
            this._deviceAddr.TabIndex = 41;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this._mdoOptionsPage);
            this.tabControl1.Controls.Add(this._datchiksOptions);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(442, 363);
            this.tabControl1.TabIndex = 46;
            // 
            // _mdoOptionsPage
            // 
            this._mdoOptionsPage.Controls.Add(this._networkOptionsGroupBox);
            this._mdoOptionsPage.Controls.Add(this._commonOptionsGroupBox);
            this._mdoOptionsPage.Controls.Add(this.groupBox5);
            this._mdoOptionsPage.Location = new System.Drawing.Point(4, 22);
            this._mdoOptionsPage.Name = "_mdoOptionsPage";
            this._mdoOptionsPage.Padding = new System.Windows.Forms.Padding(3);
            this._mdoOptionsPage.Size = new System.Drawing.Size(434, 337);
            this._mdoOptionsPage.TabIndex = 0;
            this._mdoOptionsPage.Text = "Настройки МДО";
            this._mdoOptionsPage.UseVisualStyleBackColor = true;
            // 
            // _networkOptionsGroupBox
            // 
            this._networkOptionsGroupBox.Controls.Add(this._deviceAddr);
            this._networkOptionsGroupBox.Controls.Add(this.label25);
            this._networkOptionsGroupBox.Controls.Add(this.arCBox);
            this._networkOptionsGroupBox.Location = new System.Drawing.Point(6, 6);
            this._networkOptionsGroupBox.Name = "_networkOptionsGroupBox";
            this._networkOptionsGroupBox.Size = new System.Drawing.Size(208, 62);
            this._networkOptionsGroupBox.TabIndex = 49;
            this._networkOptionsGroupBox.TabStop = false;
            this._networkOptionsGroupBox.Text = "Настройки при работе в системе";
            // 
            // _commonOptionsGroupBox
            // 
            this._commonOptionsGroupBox.Controls.Add(this.groupBox7);
            this._commonOptionsGroupBox.Controls.Add(this.groupBox4);
            this._commonOptionsGroupBox.Location = new System.Drawing.Point(220, 6);
            this._commonOptionsGroupBox.Name = "_commonOptionsGroupBox";
            this._commonOptionsGroupBox.Size = new System.Drawing.Size(202, 182);
            this._commonOptionsGroupBox.TabIndex = 48;
            this._commonOptionsGroupBox.TabStop = false;
            this._commonOptionsGroupBox.Text = "Общие настройки";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._CH3Enabled);
            this.groupBox7.Controls.Add(this._CH1Enabled);
            this.groupBox7.Controls.Add(this._CH2Enabled);
            this.groupBox7.Location = new System.Drawing.Point(6, 19);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(114, 98);
            this.groupBox7.TabIndex = 47;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Ввод датчиков";
            // 
            // _CH3Enabled
            // 
            this._CH3Enabled.AutoSize = true;
            this._CH3Enabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._CH3Enabled.Location = new System.Drawing.Point(7, 72);
            this._CH3Enabled.Name = "_CH3Enabled";
            this._CH3Enabled.Size = new System.Drawing.Size(72, 17);
            this._CH3Enabled.TabIndex = 45;
            this._CH3Enabled.Text = "Датчик 3";
            this._CH3Enabled.UseVisualStyleBackColor = true;
            this._CH3Enabled.CheckedChanged += new System.EventHandler(this._CH3Enabled_CheckedChanged);
            // 
            // _CH1Enabled
            // 
            this._CH1Enabled.AutoSize = true;
            this._CH1Enabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._CH1Enabled.Location = new System.Drawing.Point(7, 26);
            this._CH1Enabled.Name = "_CH1Enabled";
            this._CH1Enabled.Size = new System.Drawing.Size(72, 17);
            this._CH1Enabled.TabIndex = 43;
            this._CH1Enabled.Text = "Датчик 1";
            this._CH1Enabled.UseVisualStyleBackColor = true;
            this._CH1Enabled.CheckedChanged += new System.EventHandler(this._CH1Enabled_CheckedChanged);
            // 
            // _CH2Enabled
            // 
            this._CH2Enabled.AutoSize = true;
            this._CH2Enabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._CH2Enabled.Location = new System.Drawing.Point(7, 49);
            this._CH2Enabled.Name = "_CH2Enabled";
            this._CH2Enabled.Size = new System.Drawing.Size(72, 17);
            this._CH2Enabled.TabIndex = 44;
            this._CH2Enabled.Text = "Датчик 2";
            this._CH2Enabled.UseVisualStyleBackColor = true;
            this._CH2Enabled.CheckedChanged += new System.EventHandler(this._CH2Enabled_CheckedChanged);
            // 
            // _datchiksOptions
            // 
            this._datchiksOptions.Controls.Add(this._passInsertBox);
            this._datchiksOptions.Controls.Add(this._sensorsPanel);
            this._datchiksOptions.Location = new System.Drawing.Point(4, 22);
            this._datchiksOptions.Name = "_datchiksOptions";
            this._datchiksOptions.Padding = new System.Windows.Forms.Padding(3);
            this._datchiksOptions.Size = new System.Drawing.Size(434, 337);
            this._datchiksOptions.TabIndex = 1;
            this._datchiksOptions.Text = "Настройки датчиков";
            this._datchiksOptions.UseVisualStyleBackColor = true;
            // 
            // _passInsertBox
            // 
            this._passInsertBox.Controls.Add(this._validatePassButton);
            this._passInsertBox.Controls.Add(this._passTB);
            this._passInsertBox.Controls.Add(this.label5);
            this._passInsertBox.Location = new System.Drawing.Point(3, 270);
            this._passInsertBox.Name = "_passInsertBox";
            this._passInsertBox.Size = new System.Drawing.Size(241, 61);
            this._passInsertBox.TabIndex = 44;
            this._passInsertBox.TabStop = false;
            this._passInsertBox.Text = "Изменение уставок";
            // 
            // _validatePassButton
            // 
            this._validatePassButton.Location = new System.Drawing.Point(131, 24);
            this._validatePassButton.Name = "_validatePassButton";
            this._validatePassButton.Size = new System.Drawing.Size(104, 23);
            this._validatePassButton.TabIndex = 45;
            this._validatePassButton.Text = "Принять";
            this._validatePassButton.UseVisualStyleBackColor = true;
            this._validatePassButton.Click += new System.EventHandler(this.validatePassButton_Click);
            // 
            // _passTB
            // 
            this._passTB.Location = new System.Drawing.Point(57, 26);
            this._passTB.Name = "_passTB";
            this._passTB.PasswordChar = '*';
            this._passTB.Size = new System.Drawing.Size(68, 20);
            this._passTB.TabIndex = 45;
            this._passTB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._passTB_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 45;
            this.label5.Text = "Пароль";
            // 
            // _sensorsPanel
            // 
            this._sensorsPanel.Controls.Add(this._sensorChannelControl1);
            this._sensorsPanel.Controls.Add(this._sensorChannelControl2);
            this._sensorsPanel.Controls.Add(this._sensorChannelControl3);
            this._sensorsPanel.Enabled = false;
            this._sensorsPanel.Location = new System.Drawing.Point(6, 6);
            this._sensorsPanel.Name = "_sensorsPanel";
            this._sensorsPanel.Size = new System.Drawing.Size(425, 258);
            this._sensorsPanel.TabIndex = 43;
            // 
            // _sensorChannelControl1
            // 
            this._sensorChannelControl1.Enabled = false;
            this._sensorChannelControl1.Location = new System.Drawing.Point(3, 3);
            this._sensorChannelControl1.Name = "_sensorChannelControl1";
            this._sensorChannelControl1.Number = 1;
            this._sensorChannelControl1.ReactionLevel = ((ushort)(1));
            this._sensorChannelControl1.Size = new System.Drawing.Size(418, 79);
            this._sensorChannelControl1.TabIndex = 44;
            this._sensorChannelControl1.WaitingTime = ((ushort)(1));
            // 
            // _sensorChannelControl2
            // 
            this._sensorChannelControl2.Enabled = false;
            this._sensorChannelControl2.Location = new System.Drawing.Point(3, 86);
            this._sensorChannelControl2.Name = "_sensorChannelControl2";
            this._sensorChannelControl2.Number = 2;
            this._sensorChannelControl2.ReactionLevel = ((ushort)(1));
            this._sensorChannelControl2.Size = new System.Drawing.Size(418, 79);
            this._sensorChannelControl2.TabIndex = 43;
            this._sensorChannelControl2.WaitingTime = ((ushort)(1));
            // 
            // _sensorChannelControl3
            // 
            this._sensorChannelControl3.Enabled = false;
            this._sensorChannelControl3.Location = new System.Drawing.Point(3, 171);
            this._sensorChannelControl3.Name = "_sensorChannelControl3";
            this._sensorChannelControl3.Number = 3;
            this._sensorChannelControl3.ReactionLevel = ((ushort)(1));
            this._sensorChannelControl3.Size = new System.Drawing.Size(423, 84);
            this._sensorChannelControl3.TabIndex = 42;
            this._sensorChannelControl3.WaitingTime = ((ushort)(1));
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDevice,
            this.writeToFileItem,
            this.readFromFileItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 92);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "Прочитать из устройства";
            // 
            // writeToDevice
            // 
            this.writeToDevice.Name = "writeToDevice";
            this.writeToDevice.Size = new System.Drawing.Size(212, 22);
            this.writeToDevice.Text = "Записать в устройство";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "Запистаь в файл";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "Загрузить из файла";
            // 
            // MdoConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 463);
            this.ContextMenuStrip = this.contextMenu;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this._statusStrip1);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this._readConfigBut);
            this.Controls.Add(this._writeConfigBut);
            this.Controls.Add(this._saveConfigBut);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "MdoConfigurationForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MdoConfigurationForm_FormClosing);
            this.Load += new System.EventHandler(this.MDO_Configuration_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MdoConfigurationForm_KeyUp);
            this._statusStrip1.ResumeLayout(false);
            this._statusStrip1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dataGridReleView)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this._mdoOptionsPage.ResumeLayout(false);
            this._networkOptionsGroupBox.ResumeLayout(false);
            this._networkOptionsGroupBox.PerformLayout();
            this._commonOptionsGroupBox.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this._datchiksOptions.ResumeLayout(false);
            this._passInsertBox.ResumeLayout(false);
            this._passInsertBox.PerformLayout();
            this._sensorsPanel.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.StatusStrip _statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar _configProgressBar;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.MaskedTextBox _TestTime;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.CheckBox _TestEnabled;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView _dataGridReleView;
        private System.Windows.Forms.CheckBox arCBox;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox _deviceAddr;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage _mdoOptionsPage;
        private System.Windows.Forms.TabPage _datchiksOptions;
        private System.Windows.Forms.CheckBox _CH1Enabled;
        private System.Windows.Forms.CheckBox _CH2Enabled;
        private System.Windows.Forms.CheckBox _CH3Enabled;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Panel _sensorsPanel;
        private System.Windows.Forms.GroupBox _passInsertBox;
        private System.Windows.Forms.Button _validatePassButton;
        private System.Windows.Forms.TextBox _passTB;
        private System.Windows.Forms.Label label5;
        private Controls.SensorChannelControl _sensorChannelControl3;
        private Controls.SensorChannelControl _sensorChannelControl2;
        private Controls.SensorChannelControl _sensorChannelControl1;
        private System.Windows.Forms.GroupBox _networkOptionsGroupBox;
        private System.Windows.Forms.GroupBox _commonOptionsGroupBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extReleNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _extReleTypeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extReleTypeImpCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _sensor1Column;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _sensor2Column;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _sensor3Column;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDevice;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
    }
}