﻿namespace BEMN.MDO.MdoMeasuring
{
    partial class MdoMeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this._CH1Error = new BEMN.Forms.LedControl();
            this._CH1Arc = new BEMN.Forms.LedControl();
            this._CH1Light = new BEMN.Forms.LedControl();
            this._CH1Min = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._CH1Enabled = new BEMN.Forms.LedControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this._CH2Error = new BEMN.Forms.LedControl();
            this._CH2Arc = new BEMN.Forms.LedControl();
            this._CH2Light = new BEMN.Forms.LedControl();
            this._CH2Min = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this._CH2Enabled = new BEMN.Forms.LedControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this._CH3Error = new BEMN.Forms.LedControl();
            this._CH3Arc = new BEMN.Forms.LedControl();
            this._CH3Light = new BEMN.Forms.LedControl();
            this._CH3Min = new System.Windows.Forms.MaskedTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this._CH3Enabled = new BEMN.Forms.LedControl();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this._relay4 = new BEMN.Forms.LedControl();
            this.label18 = new System.Windows.Forms.Label();
            this._relay3 = new BEMN.Forms.LedControl();
            this.label17 = new System.Windows.Forms.Label();
            this._relay2 = new BEMN.Forms.LedControl();
            this.label16 = new System.Windows.Forms.Label();
            this._relay1 = new BEMN.Forms.LedControl();
            this.label24 = new System.Windows.Forms.Label();
            this._relayError = new BEMN.Forms.LedControl();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this._ErrorTestOn = new BEMN.Forms.LedControl();
            this.label22 = new System.Windows.Forms.Label();
            this._ErrorHL = new BEMN.Forms.LedControl();
            this.label23 = new System.Windows.Forms.Label();
            this._Error5V = new BEMN.Forms.LedControl();
            this.kvint = new System.Windows.Forms.Button();
            this.stopReading = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._stopTimeButton = new System.Windows.Forms.Button();
            this._writeDateTimeButt = new System.Windows.Forms.Button();
            this._dateTimeNowButt = new System.Windows.Forms.Button();
            this._stopCB = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this._timeClockTB = new System.Windows.Forms.MaskedTextBox();
            this._dateClockTB = new System.Windows.Forms.MaskedTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this._canError = new BEMN.Forms.LedControl();
            this._autoMode = new BEMN.Forms.LedControl();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this._CH1Error);
            this.groupBox1.Controls.Add(this._CH1Arc);
            this.groupBox1.Controls.Add(this._CH1Light);
            this.groupBox1.Controls.Add(this._CH1Min);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._CH1Enabled);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(223, 111);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "    Датчик 1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(186, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 126;
            this.label5.Text = "клк";
            // 
            // _CH1Error
            // 
            this._CH1Error.BackColor = System.Drawing.Color.Transparent;
            this._CH1Error.Location = new System.Drawing.Point(159, 89);
            this._CH1Error.Name = "_CH1Error";
            this._CH1Error.Size = new System.Drawing.Size(13, 13);
            this._CH1Error.State = BEMN.Forms.LedState.Off;
            this._CH1Error.TabIndex = 125;
            // 
            // _CH1Arc
            // 
            this._CH1Arc.BackColor = System.Drawing.Color.Transparent;
            this._CH1Arc.Location = new System.Drawing.Point(159, 71);
            this._CH1Arc.Name = "_CH1Arc";
            this._CH1Arc.Size = new System.Drawing.Size(13, 13);
            this._CH1Arc.State = BEMN.Forms.LedState.Off;
            this._CH1Arc.TabIndex = 124;
            // 
            // _CH1Light
            // 
            this._CH1Light.BackColor = System.Drawing.Color.Transparent;
            this._CH1Light.Location = new System.Drawing.Point(159, 51);
            this._CH1Light.Name = "_CH1Light";
            this._CH1Light.Size = new System.Drawing.Size(13, 13);
            this._CH1Light.State = BEMN.Forms.LedState.Off;
            this._CH1Light.TabIndex = 123;
            // 
            // _CH1Min
            // 
            this._CH1Min.Enabled = false;
            this._CH1Min.Location = new System.Drawing.Point(151, 27);
            this._CH1Min.Name = "_CH1Min";
            this._CH1Min.Size = new System.Drawing.Size(31, 20);
            this._CH1Min.TabIndex = 122;
            this._CH1Min.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 13);
            this.label4.TabIndex = 121;
            this.label4.Text = "Неисправность датчика";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 120;
            this.label3.Text = "Дуга";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 119;
            this.label2.Text = "Засветка";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 118;
            this.label1.Text = "Значение";
            // 
            // _CH1Enabled
            // 
            this._CH1Enabled.BackColor = System.Drawing.Color.Transparent;
            this._CH1Enabled.Location = new System.Drawing.Point(4, 0);
            this._CH1Enabled.Name = "_CH1Enabled";
            this._CH1Enabled.Size = new System.Drawing.Size(13, 13);
            this._CH1Enabled.State = BEMN.Forms.LedState.Off;
            this._CH1Enabled.TabIndex = 117;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this._CH2Error);
            this.groupBox2.Controls.Add(this._CH2Arc);
            this.groupBox2.Controls.Add(this._CH2Light);
            this.groupBox2.Controls.Add(this._CH2Min);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this._CH2Enabled);
            this.groupBox2.Location = new System.Drawing.Point(12, 129);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(223, 111);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "    Датчик 2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(186, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 126;
            this.label6.Text = "клк";
            // 
            // _CH2Error
            // 
            this._CH2Error.BackColor = System.Drawing.Color.Transparent;
            this._CH2Error.Location = new System.Drawing.Point(159, 88);
            this._CH2Error.Name = "_CH2Error";
            this._CH2Error.Size = new System.Drawing.Size(13, 13);
            this._CH2Error.State = BEMN.Forms.LedState.Off;
            this._CH2Error.TabIndex = 125;
            // 
            // _CH2Arc
            // 
            this._CH2Arc.BackColor = System.Drawing.Color.Transparent;
            this._CH2Arc.Location = new System.Drawing.Point(159, 70);
            this._CH2Arc.Name = "_CH2Arc";
            this._CH2Arc.Size = new System.Drawing.Size(13, 13);
            this._CH2Arc.State = BEMN.Forms.LedState.Off;
            this._CH2Arc.TabIndex = 124;
            // 
            // _CH2Light
            // 
            this._CH2Light.BackColor = System.Drawing.Color.Transparent;
            this._CH2Light.Location = new System.Drawing.Point(159, 50);
            this._CH2Light.Name = "_CH2Light";
            this._CH2Light.Size = new System.Drawing.Size(13, 13);
            this._CH2Light.State = BEMN.Forms.LedState.Off;
            this._CH2Light.TabIndex = 123;
            // 
            // _CH2Min
            // 
            this._CH2Min.Enabled = false;
            this._CH2Min.Location = new System.Drawing.Point(151, 27);
            this._CH2Min.Name = "_CH2Min";
            this._CH2Min.Size = new System.Drawing.Size(31, 20);
            this._CH2Min.TabIndex = 122;
            this._CH2Min.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 88);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 13);
            this.label7.TabIndex = 121;
            this.label7.Text = "Неисправность датчика";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 70);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 120;
            this.label8.Text = "Дуга";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 119;
            this.label9.Text = "Засветка";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 30);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 118;
            this.label10.Text = "Значение";
            // 
            // _CH2Enabled
            // 
            this._CH2Enabled.BackColor = System.Drawing.Color.Transparent;
            this._CH2Enabled.Location = new System.Drawing.Point(4, 0);
            this._CH2Enabled.Name = "_CH2Enabled";
            this._CH2Enabled.Size = new System.Drawing.Size(13, 13);
            this._CH2Enabled.State = BEMN.Forms.LedState.Off;
            this._CH2Enabled.TabIndex = 117;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this._CH3Error);
            this.groupBox3.Controls.Add(this._CH3Arc);
            this.groupBox3.Controls.Add(this._CH3Light);
            this.groupBox3.Controls.Add(this._CH3Min);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this._CH3Enabled);
            this.groupBox3.Location = new System.Drawing.Point(12, 246);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(223, 111);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "    Датчик 3";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label11.Location = new System.Drawing.Point(185, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 13);
            this.label11.TabIndex = 126;
            this.label11.Text = "клк";
            // 
            // _CH3Error
            // 
            this._CH3Error.BackColor = System.Drawing.Color.Transparent;
            this._CH3Error.Location = new System.Drawing.Point(159, 88);
            this._CH3Error.Name = "_CH3Error";
            this._CH3Error.Size = new System.Drawing.Size(13, 13);
            this._CH3Error.State = BEMN.Forms.LedState.Off;
            this._CH3Error.TabIndex = 125;
            // 
            // _CH3Arc
            // 
            this._CH3Arc.BackColor = System.Drawing.Color.Transparent;
            this._CH3Arc.Location = new System.Drawing.Point(159, 70);
            this._CH3Arc.Name = "_CH3Arc";
            this._CH3Arc.Size = new System.Drawing.Size(13, 13);
            this._CH3Arc.State = BEMN.Forms.LedState.Off;
            this._CH3Arc.TabIndex = 124;
            // 
            // _CH3Light
            // 
            this._CH3Light.BackColor = System.Drawing.Color.Transparent;
            this._CH3Light.Location = new System.Drawing.Point(159, 50);
            this._CH3Light.Name = "_CH3Light";
            this._CH3Light.Size = new System.Drawing.Size(13, 13);
            this._CH3Light.State = BEMN.Forms.LedState.Off;
            this._CH3Light.TabIndex = 123;
            // 
            // _CH3Min
            // 
            this._CH3Min.Enabled = false;
            this._CH3Min.Location = new System.Drawing.Point(151, 27);
            this._CH3Min.Name = "_CH3Min";
            this._CH3Min.Size = new System.Drawing.Size(31, 20);
            this._CH3Min.TabIndex = 122;
            this._CH3Min.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 88);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(129, 13);
            this.label12.TabIndex = 121;
            this.label12.Text = "Неисправность датчика";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 70);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 13);
            this.label13.TabIndex = 120;
            this.label13.Text = "Дуга";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 50);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 13);
            this.label14.TabIndex = 119;
            this.label14.Text = "Засветка";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 118;
            this.label15.Text = "Значение";
            // 
            // _CH3Enabled
            // 
            this._CH3Enabled.BackColor = System.Drawing.Color.Transparent;
            this._CH3Enabled.Location = new System.Drawing.Point(4, 0);
            this._CH3Enabled.Name = "_CH3Enabled";
            this._CH3Enabled.Size = new System.Drawing.Size(13, 13);
            this._CH3Enabled.State = BEMN.Forms.LedState.Off;
            this._CH3Enabled.TabIndex = 117;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this._relay4);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this._relay3);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this._relay2);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this._relay1);
            this.groupBox4.Location = new System.Drawing.Point(246, 188);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(57, 108);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Реле";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 84);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 13);
            this.label19.TabIndex = 131;
            this.label19.Text = "4";
            // 
            // _relay4
            // 
            this._relay4.BackColor = System.Drawing.Color.Transparent;
            this._relay4.Location = new System.Drawing.Point(25, 84);
            this._relay4.Name = "_relay4";
            this._relay4.Size = new System.Drawing.Size(13, 13);
            this._relay4.State = BEMN.Forms.LedState.Off;
            this._relay4.TabIndex = 130;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 66);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(13, 13);
            this.label18.TabIndex = 129;
            this.label18.Text = "3";
            // 
            // _relay3
            // 
            this._relay3.BackColor = System.Drawing.Color.Transparent;
            this._relay3.Location = new System.Drawing.Point(25, 66);
            this._relay3.Name = "_relay3";
            this._relay3.Size = new System.Drawing.Size(13, 13);
            this._relay3.State = BEMN.Forms.LedState.Off;
            this._relay3.TabIndex = 128;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 46);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(13, 13);
            this.label17.TabIndex = 127;
            this.label17.Text = "2";
            // 
            // _relay2
            // 
            this._relay2.BackColor = System.Drawing.Color.Transparent;
            this._relay2.Location = new System.Drawing.Point(25, 46);
            this._relay2.Name = "_relay2";
            this._relay2.Size = new System.Drawing.Size(13, 13);
            this._relay2.State = BEMN.Forms.LedState.Off;
            this._relay2.TabIndex = 126;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 26);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(13, 13);
            this.label16.TabIndex = 125;
            this.label16.Text = "1";
            // 
            // _relay1
            // 
            this._relay1.BackColor = System.Drawing.Color.Transparent;
            this._relay1.Location = new System.Drawing.Point(25, 26);
            this._relay1.Name = "_relay1";
            this._relay1.Size = new System.Drawing.Size(13, 13);
            this._relay1.State = BEMN.Forms.LedState.Off;
            this._relay1.TabIndex = 124;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(281, 302);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(86, 13);
            this.label24.TabIndex = 133;
            this.label24.Text = "Неисправность";
            // 
            // _relayError
            // 
            this._relayError.BackColor = System.Drawing.Color.Transparent;
            this._relayError.Location = new System.Drawing.Point(374, 302);
            this._relayError.Name = "_relayError";
            this._relayError.Size = new System.Drawing.Size(13, 13);
            this._relayError.State = BEMN.Forms.LedState.Off;
            this._relayError.TabIndex = 132;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this._ErrorTestOn);
            this.groupBox5.Controls.Add(this.label22);
            this.groupBox5.Controls.Add(this._ErrorHL);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this._Error5V);
            this.groupBox5.Location = new System.Drawing.Point(309, 193);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(87, 103);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Ошибки Невидимый";
            this.groupBox5.Visible = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 70);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(31, 13);
            this.label25.TabIndex = 129;
            this.label25.Text = "Тест";
            // 
            // _ErrorTestOn
            // 
            this._ErrorTestOn.BackColor = System.Drawing.Color.Transparent;
            this._ErrorTestOn.Location = new System.Drawing.Point(65, 70);
            this._ErrorTestOn.Name = "_ErrorTestOn";
            this._ErrorTestOn.Size = new System.Drawing.Size(13, 13);
            this._ErrorTestOn.State = BEMN.Forms.LedState.Off;
            this._ErrorTestOn.TabIndex = 128;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 50);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 13);
            this.label22.TabIndex = 127;
            this.label22.Text = "Теста HL";
            // 
            // _ErrorHL
            // 
            this._ErrorHL.BackColor = System.Drawing.Color.Transparent;
            this._ErrorHL.Location = new System.Drawing.Point(65, 50);
            this._ErrorHL.Name = "_ErrorHL";
            this._ErrorHL.Size = new System.Drawing.Size(13, 13);
            this._ErrorHL.State = BEMN.Forms.LedState.Off;
            this._ErrorHL.TabIndex = 126;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 30);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(20, 13);
            this.label23.TabIndex = 125;
            this.label23.Text = "5V";
            // 
            // _Error5V
            // 
            this._Error5V.BackColor = System.Drawing.Color.Transparent;
            this._Error5V.Location = new System.Drawing.Point(65, 31);
            this._Error5V.Name = "_Error5V";
            this._Error5V.Size = new System.Drawing.Size(13, 13);
            this._Error5V.State = BEMN.Forms.LedState.Off;
            this._Error5V.TabIndex = 124;
            // 
            // kvint
            // 
            this.kvint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.kvint.Location = new System.Drawing.Point(251, 359);
            this.kvint.Name = "kvint";
            this.kvint.Size = new System.Drawing.Size(156, 23);
            this.kvint.TabIndex = 5;
            this.kvint.Text = "Сквитировать";
            this.kvint.UseVisualStyleBackColor = true;
            this.kvint.Click += new System.EventHandler(this.kvint_Click);
            // 
            // stopReading
            // 
            this.stopReading.Location = new System.Drawing.Point(6, 45);
            this.stopReading.Name = "stopReading";
            this.stopReading.Size = new System.Drawing.Size(133, 23);
            this.stopReading.TabIndex = 7;
            this.stopReading.Text = "Остановить чтение";
            this.stopReading.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._stopTimeButton);
            this.groupBox6.Controls.Add(this._writeDateTimeButt);
            this.groupBox6.Controls.Add(this._dateTimeNowButt);
            this.groupBox6.Controls.Add(this._stopCB);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this._timeClockTB);
            this.groupBox6.Controls.Add(this._dateClockTB);
            this.groupBox6.Location = new System.Drawing.Point(246, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(167, 170);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Дата - время";
            // 
            // _stopTimeButton
            // 
            this._stopTimeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._stopTimeButton.Location = new System.Drawing.Point(5, 141);
            this._stopTimeButton.Name = "_stopTimeButton";
            this._stopTimeButton.Size = new System.Drawing.Size(156, 23);
            this._stopTimeButton.TabIndex = 7;
            this._stopTimeButton.Text = "Остановить часы";
            this._stopTimeButton.UseVisualStyleBackColor = true;
            this._stopTimeButton.Click += new System.EventHandler(this._stopTimeButton_Click);
            // 
            // _writeDateTimeButt
            // 
            this._writeDateTimeButt.Enabled = false;
            this._writeDateTimeButt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._writeDateTimeButt.Location = new System.Drawing.Point(5, 112);
            this._writeDateTimeButt.Name = "_writeDateTimeButt";
            this._writeDateTimeButt.Size = new System.Drawing.Size(156, 23);
            this._writeDateTimeButt.TabIndex = 6;
            this._writeDateTimeButt.Text = "Установить";
            this._writeDateTimeButt.UseVisualStyleBackColor = true;
            this._writeDateTimeButt.Click += new System.EventHandler(this._writeDateTimeButt_Click);
            // 
            // _dateTimeNowButt
            // 
            this._dateTimeNowButt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._dateTimeNowButt.Location = new System.Drawing.Point(5, 83);
            this._dateTimeNowButt.Name = "_dateTimeNowButt";
            this._dateTimeNowButt.Size = new System.Drawing.Size(156, 23);
            this._dateTimeNowButt.TabIndex = 5;
            this._dateTimeNowButt.Text = "Системные дата и время";
            this._dateTimeNowButt.UseVisualStyleBackColor = true;
            this._dateTimeNowButt.Click += new System.EventHandler(this._dateTimeNowButt_Click);
            // 
            // _stopCB
            // 
            this._stopCB.AutoSize = true;
            this._stopCB.Location = new System.Drawing.Point(31, 60);
            this._stopCB.Name = "_stopCB";
            this._stopCB.Size = new System.Drawing.Size(112, 17);
            this._stopCB.TabIndex = 4;
            this._stopCB.Text = "Изменить время";
            this._stopCB.UseVisualStyleBackColor = true;
            this._stopCB.CheckedChanged += new System.EventHandler(this._stopCB_CheckedChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(87, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(40, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "Время";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(2, 16);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(33, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "Дата";
            // 
            // _timeClockTB
            // 
            this._timeClockTB.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this._timeClockTB.Location = new System.Drawing.Point(90, 34);
            this._timeClockTB.Mask = "90:00:00.000";
            this._timeClockTB.Name = "_timeClockTB";
            this._timeClockTB.Size = new System.Drawing.Size(70, 20);
            this._timeClockTB.TabIndex = 1;
            this._timeClockTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _dateClockTB
            // 
            this._dateClockTB.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this._dateClockTB.Location = new System.Drawing.Point(5, 34);
            this._dateClockTB.Mask = "00/00/0000";
            this._dateClockTB.Name = "_dateClockTB";
            this._dateClockTB.Size = new System.Drawing.Size(60, 20);
            this._dateClockTB.TabIndex = 0;
            this._dateClockTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(262, 321);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(105, 13);
            this.label26.TabIndex = 135;
            this.label26.Text = "Ошибка CAN связи";
            // 
            // _canError
            // 
            this._canError.BackColor = System.Drawing.Color.Transparent;
            this._canError.Location = new System.Drawing.Point(374, 321);
            this._canError.Name = "_canError";
            this._canError.Size = new System.Drawing.Size(13, 13);
            this._canError.State = BEMN.Forms.LedState.Off;
            this._canError.TabIndex = 134;
            // 
            // _autoMode
            // 
            this._autoMode.BackColor = System.Drawing.Color.Transparent;
            this._autoMode.Location = new System.Drawing.Point(374, 340);
            this._autoMode.Name = "_autoMode";
            this._autoMode.Size = new System.Drawing.Size(13, 13);
            this._autoMode.State = BEMN.Forms.LedState.Off;
            this._autoMode.TabIndex = 134;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(302, 340);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(65, 13);
            this.label27.TabIndex = 135;
            this.label27.Text = "Авторежим";
            // 
            // MdoMeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 389);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this._autoMode);
            this.Controls.Add(this._canError);
            this.Controls.Add(this.label24);
            this.Controls.Add(this._relayError);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.kvint);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MdoMeasuringForm";
            this.Text = "MDO_Measuring";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MDO_Measuring_FormClosing);
            this.Load += new System.EventHandler(this.MDO_Measuring_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private BEMN.Forms.LedControl _CH1Enabled;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox _CH1Min;
        private BEMN.Forms.LedControl _CH1Error;
        private BEMN.Forms.LedControl _CH1Arc;
        private BEMN.Forms.LedControl _CH1Light;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private BEMN.Forms.LedControl _CH2Error;
        private BEMN.Forms.LedControl _CH2Arc;
        private BEMN.Forms.LedControl _CH2Light;
        private System.Windows.Forms.MaskedTextBox _CH2Min;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private BEMN.Forms.LedControl _CH2Enabled;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label11;
        private BEMN.Forms.LedControl _CH3Error;
        private BEMN.Forms.LedControl _CH3Arc;
        private BEMN.Forms.LedControl _CH3Light;
        private System.Windows.Forms.MaskedTextBox _CH3Min;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private BEMN.Forms.LedControl _CH3Enabled;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label19;
        private BEMN.Forms.LedControl _relay4;
        private System.Windows.Forms.Label label18;
        private BEMN.Forms.LedControl _relay3;
        private System.Windows.Forms.Label label17;
        private BEMN.Forms.LedControl _relay2;
        private System.Windows.Forms.Label label16;
        private BEMN.Forms.LedControl _relay1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label22;
        private BEMN.Forms.LedControl _ErrorHL;
        private System.Windows.Forms.Label label23;
        private BEMN.Forms.LedControl _Error5V;
        private System.Windows.Forms.Button kvint;
        private System.Windows.Forms.Button stopReading;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button _writeDateTimeButt;
        private System.Windows.Forms.Button _dateTimeNowButt;
        private System.Windows.Forms.CheckBox _stopCB;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.MaskedTextBox _timeClockTB;
        private System.Windows.Forms.MaskedTextBox _dateClockTB;
        private System.Windows.Forms.Label label24;
        private BEMN.Forms.LedControl _relayError;
        private System.Windows.Forms.Label label25;
        private BEMN.Forms.LedControl _ErrorTestOn;
        private System.Windows.Forms.Label label26;
        private BEMN.Forms.LedControl _canError;
        private System.Windows.Forms.Button _stopTimeButton;
        private BEMN.Forms.LedControl _autoMode;
        private System.Windows.Forms.Label label27;
    }
}