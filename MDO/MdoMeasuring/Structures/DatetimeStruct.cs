﻿using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.MDO.MdoMeasuring.Structures
{
    public struct DatetimeStruct : IStruct, IStructInit
    {
        public ushort years;
        public ushort month;
        public ushort date;
        public ushort day;
        public ushort hour;
        public ushort min;
        public ushort sec;
        public ushort msec;

        #region Время
        public ushort TimeDate
        {
            get
            {
                return this.date;
            }
            set
            {
                this.date = value;
            }
        }

        public ushort TimeMonth
        {
            get
            {
                return this.month;
            }
            set
            {
                this.month = value;
            }
        }

        public ushort TimeYear
        {
            get
            {
                return this.years;
            }
            set
            {
                this.years = value;
            }
        }

        public ushort TimeHour
        {
            get
            {
                return this.hour;
            }
            set
            {
                this.hour = value;
            }
        }

        public ushort TimeMinutes
        {
            get
            {
                return this.min;
            }
            set
            {
                this.min = value;
            }
        }

        public ushort TimeSeconds
        {
            get
            {
                return this.sec;
            }
            set
            {
                this.sec = value;
            }
        }

        public ushort TimeMseconds
        {
            get
            {
                return this.msec;
            }
            set
            {
                this.msec = value;
            }
        }
        #endregion

        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.years = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.month = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.date = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.day = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.hour = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.min = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.sec = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.msec = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.years);
            result.Add(this.month);
            result.Add(this.date);
            result.Add(this.day);
            result.Add(this.hour);
            result.Add(this.min);
            result.Add(this.sec);
            result.Add(this.msec);
            return result.ToArray();
        }
    }
}