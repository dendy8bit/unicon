﻿using System.Collections;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.MBServer;

namespace BEMN.MDO.MdoMeasuring.Structures
{
    public class RamStruct : StructBase
    {
        [Layout(0, Count = 3)] private StatusStruct[] _chanelStatus;
        [Layout(1)] private ushort _alarm;
        [Layout(2, Ignore = true)] private ushort _command;
        [Layout(3)] private ushort _commandAR;

        public ushort Ch1Min
        {
            get
            {
                return this._chanelStatus[0].ChannelMin;
            }
        }

        public BitArray Ch1Sygnals
        {
            get
            {
                return new BitArray(new[] { Common.LOBYTE(this._chanelStatus[0].ChannelMode) });
            }
        }

        public ushort Ch2Min
        {
            get
            {
                return this._chanelStatus[1].ChannelMin;
            }
        }

        public BitArray Ch2Sygnals
        {
            get
            {
                return new BitArray(new[] { Common.LOBYTE(this._chanelStatus[1].ChannelMode) });
            }
        }

        public ushort Ch3Min
        {
            get
            {
                return this._chanelStatus[2].ChannelMin;
            }
        }

        public BitArray Ch3Sygnals
        {
            get
            {
                return new BitArray(new[] { Common.LOBYTE(this._chanelStatus[2].ChannelMode) });
            }
        }

        public BitArray ErrorsRelaySygnals
        {
            get
            {
                var result = new BitArray(new[] { Common.LOBYTE(_alarm) });
                var index = result.Length - 1;
                result[index] = !result[index];
                return result;
            }
        }

        public bool AutoMode
        {
            get { return Common.GetBit(_commandAR, 0); }
        }
    }
}
