﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MDO.MdoMeasuring.Structures
{
    public class StatusStruct : StructBase
    {
        [Layout(0)] private ushort _channelMode;
        [Layout(1)] private ushort _channelMin;
        [Layout(2)] private ushort _channelSampleCounter;
        [Layout(3)] private ushort _channelSampleCountLimit;
        [Layout(4)] private ushort _channelLowADLevel;

        public ushort ChannelMode
        {
            get { return _channelMode; }
        }

        public ushort ChannelMin
        {
            get { return _channelMin; }
        }

        public ushort ChannelSampleCounter
        {
            get { return _channelSampleCounter; }
        }

        public ushort ChannelSampleCountLimit
        {
            get { return _channelSampleCountLimit; }
        }

        public ushort ChannelLowAdLevel
        {
            get { return _channelLowADLevel; }
        }
    }
}
