﻿using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.MDO.SomeStructures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MdoCanNumberStruct : IStruct, IStructInit
    {
        public ushort CanNumber;
        public MdoCanNumberStruct(bool a)
        {
            this.CanNumber = 0;
        }

        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }

        public void InitStruct(byte[] array)
        {
            this.CanNumber = Common.TOWORD(array[1], array[0]);
        }

        public ushort[] GetValues()
        {
            return new[] { this.CanNumber };
        }
    }
}
