﻿namespace BEMN.MDO.MdoPOInformation
{
    partial class MdoPoInformationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._versionLabel = new System.Windows.Forms.Label();
            this._updateButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _versionLabel
            // 
            this._versionLabel.AutoSize = true;
            this._versionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._versionLabel.Location = new System.Drawing.Point(12, 21);
            this._versionLabel.Name = "_versionLabel";
            this._versionLabel.Size = new System.Drawing.Size(78, 16);
            this._versionLabel.TabIndex = 0;
            this._versionLabel.Text = "Версия ПО";
            // 
            // _updateButton
            // 
            this._updateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._updateButton.Location = new System.Drawing.Point(12, 53);
            this._updateButton.Name = "_updateButton";
            this._updateButton.Size = new System.Drawing.Size(242, 23);
            this._updateButton.TabIndex = 1;
            this._updateButton.Text = "Обновить ПО";
            this._updateButton.UseVisualStyleBackColor = true;
            this._updateButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // MdoPoInformationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(266, 88);
            this.Controls.Add(this._updateButton);
            this.Controls.Add(this._versionLabel);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(282, 127);
            this.MinimumSize = new System.Drawing.Size(282, 127);
            this.Name = "MdoPoInformationForm";
            this.Text = "MdoPoInformationForm";
            this.Load += new System.EventHandler(this.MdoPoInformationForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _versionLabel;
        private System.Windows.Forms.Button _updateButton;
    }
}