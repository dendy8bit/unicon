﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace BEMN.MDO.Controls
{
    /// <summary>
    /// Настройка датчика МДО
    /// </summary>
    public partial class SensorChannelControl : UserControl
    {
        #region [Constants]

        public const int MIN_LEVEL = 1;
        public const int MAX_LEVEL = 75;
        public const int MIN_TIME = 1;
        public const int MAX_TIME = 1000;
        private const string SENSOR = "Датчик ";
        private const string TIME_INTERVAL_PATTERN = "Значение времени должно лежать в пределах {0} - {1}";
        private const string INVALID_SYMBOLS_ERROR = "Значение времени содержит недопустимые символы";
        private const string VALUE_CANT_BE_EMPTY = "Значение не может быть пустым";
        private const string LEVEL_INTERVAL_PATTERN = "Значение уровня должно лежать в пределах {0} - {1}";
        private const string DATA_IS_DAMAGED_ERROR = "Данные повреждены";
        private const char BACKSPACE = '\b';

        #endregion [Constants]


        #region [Private fields]

        private int _number;

        #endregion [Private fields]


        #region [Ctor's]

        public SensorChannelControl()
        {
            InitializeComponent();
            PrepareBar();
        }

        #endregion [Ctor's]


        #region [Properties]

        /// <summary>
        /// Порядковый номер сенсора
        /// </summary>
        public int Number
        {
            get { return _number; }
            set
            {
                if (this._number == 0)
                    this._number = value;
                this._mainGroupBox.Text = SensorChannelControl.SENSOR + this._number;
            }
        }

        public int ReactionLevel
        {
            get { return this._channelLavelBar.Value; }
            set
            {
                if (LevelIsValid(value))
                {
                    this._channelLavelBar.Value = value;
                    this._channelLavelTextBox.Text = value.ToString(CultureInfo.InvariantCulture);
                }

                else
                {
                    MessageBox.Show(DATA_IS_DAMAGED_ERROR);
                    this._channelLavelBar.Value = SensorChannelControl.MIN_LEVEL;
                    this._channelLavelTextBox.Text =
                        SensorChannelControl.MIN_LEVEL.ToString(CultureInfo.InvariantCulture);
                }
            }
        }

        public ushort WaitingTime
        {
            get
            {
                ushort time;
                if (ushort.TryParse(this._channelTimeTextBox.Text, out time))
                {
                    if (TimeIsValid(time))
                        return time;
                }
                return SensorChannelControl.MIN_TIME;

            }
            set
            {
                if (TimeIsValid(value))
                    this._channelTimeTextBox.Text = value.ToString(CultureInfo.InvariantCulture);
                else
                {
                    MessageBox.Show(DATA_IS_DAMAGED_ERROR);
                    this._channelTimeTextBox.Text = SensorChannelControl.MIN_LEVEL.ToString(CultureInfo.InvariantCulture);
                }
            }
        }
        #endregion [Properties]


        #region [Event Handlers]
        //Изменение уровня ползунком
        private void _channelLavelBar_Scroll(object sender, EventArgs e)
        {
            _channelLavelTextBox.Text = _channelLavelBar.Value.ToString(CultureInfo.InvariantCulture);
        }
        //Изменение уровня вводом значения
        private void _channelLavelPercentBar_TextChanged(object sender, EventArgs e)
        {
            InputTextValueValidate(this._channelLavelTextBox, LevelValidate);
        }
        //Изменение времени
        private void _channelTime_TextChanged(object sender, EventArgs e)
        {
            InputTextValueValidate(this._channelTimeTextBox, TimeValidate);
        }
        //Запрет на ввод всего кроме цифр
        private void NumberTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) & e.KeyChar != BACKSPACE)
                e.Handled = true;
        }

        #endregion [Event Handlers]


        #region [Help members]
        //Подготовка ползунка
        private void PrepareBar()
        {
            this._channelLavelBar.Minimum = SensorChannelControl.MIN_LEVEL;
            this._channelLavelBar.Maximum = SensorChannelControl.MAX_LEVEL;
            this._channelLavelBar.Value = SensorChannelControl.MIN_LEVEL;
            this._channelLavelBar.SmallChange = 1;
            this._channelLavelBar.LargeChange = 1;
        }
        //Проверка введённого значения
        private void InputTextValueValidate(MaskedTextBox textBox, Action<int> valueValidator)
        {
            
            int value;
            string text = textBox.Text;
            this._toolTip.Hide(textBox);
            if (string.IsNullOrEmpty(text))
            {
                this._toolTip.Show(VALUE_CANT_BE_EMPTY, textBox);
                textBox.BackColor = Color.Red;
                return;
            }
            if (int.TryParse(text, out value))
            {
                valueValidator.Invoke(value);
               
            }
            else
            {
                this._toolTip.Show(INVALID_SYMBOLS_ERROR, textBox);
                textBox.BackColor = Color.Red;
            }
        }
     
        //Проверка Времени
        private void TimeValidate(int time)
        {
            if (TimeIsValid((ushort) time))
            {
                this._channelTimeTextBox.BackColor = Color.White;
            }
            else
            {
                var errorMessage = string.Format(TIME_INTERVAL_PATTERN, MIN_TIME, MAX_TIME);
                this._toolTip.Show(errorMessage, this._channelTimeTextBox);
                this._channelTimeTextBox.BackColor = Color.Red;
            }
        }


        //Проверка Уровня
        private void LevelValidate(int level)
        {
            if (LevelIsValid(level))
            {
                _channelLavelBar.Value = level;
                this._channelLavelTextBox.BackColor = Color.White;
            }
            else
            {
                var errorMessage = string.Format(LEVEL_INTERVAL_PATTERN, MIN_LEVEL, MAX_LEVEL);
                this._toolTip.Show(errorMessage, this._channelLavelTextBox);
                this._channelLavelTextBox.BackColor = Color.Red;
            }
        }

        private bool TimeIsValid(ushort time)
        {
            return (time >= MIN_TIME) & (time <= MAX_TIME);
        }

        private bool LevelIsValid(int level)
        {
            return (level >= MIN_LEVEL) & (level <= MAX_LEVEL);
        }
        #endregion [Help members]
    }
}
