﻿namespace BEMN.MDO.Controls
{
    partial class SensorChannelControl
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._mainGroupBox = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._channelTimeTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._channelLavelTextBox = new System.Windows.Forms.MaskedTextBox();
            this._channelLavelBar = new System.Windows.Forms.TrackBar();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._mainGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._channelLavelBar)).BeginInit();
            this.SuspendLayout();
            // 
            // _mainGroupBox
            // 
            this._mainGroupBox.BackColor = System.Drawing.Color.White;
            this._mainGroupBox.Controls.Add(this.label4);
            this._mainGroupBox.Controls.Add(this.label3);
            this._mainGroupBox.Controls.Add(this._channelTimeTextBox);
            this._mainGroupBox.Controls.Add(this.label2);
            this._mainGroupBox.Controls.Add(this.label1);
            this._mainGroupBox.Controls.Add(this._channelLavelTextBox);
            this._mainGroupBox.Controls.Add(this._channelLavelBar);
            this._mainGroupBox.Location = new System.Drawing.Point(0, 0);
            this._mainGroupBox.Name = "_mainGroupBox";
            this._mainGroupBox.Size = new System.Drawing.Size(417, 80);
            this._mainGroupBox.TabIndex = 41;
            this._mainGroupBox.TabStop = false;
            this._mainGroupBox.Text = "Датчик ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(238, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "клк";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(373, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "мкс";
            // 
            // _channelTimeTextBox
            // 
            this._channelTimeTextBox.Location = new System.Drawing.Point(290, 38);
            this._channelTimeTextBox.Name = "_channelTimeTextBox";
            this._channelTimeTextBox.Size = new System.Drawing.Size(77, 20);
            this._channelTimeTextBox.TabIndex = 5;
            this._channelTimeTextBox.Tag = "1;65535";
            this._channelTimeTextBox.Text = "1";
            this._channelTimeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._channelTimeTextBox.TextChanged += new System.EventHandler(this._channelTime_TextChanged);
            this._channelTimeTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumberTextBox_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(287, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Время ожидания";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Уровень срабатывания";
            // 
            // _channelLavelTextBox
            // 
            this._channelLavelTextBox.BackColor = System.Drawing.Color.White;
            this._channelLavelTextBox.Location = new System.Drawing.Point(200, 38);
            this._channelLavelTextBox.Name = "_channelLavelTextBox";
            this._channelLavelTextBox.Size = new System.Drawing.Size(35, 20);
            this._channelLavelTextBox.TabIndex = 2;
            this._channelLavelTextBox.Tag = "1;250";
            this._channelLavelTextBox.Text = "1";
            this._channelLavelTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._channelLavelTextBox.TextChanged += new System.EventHandler(this._channelLavelPercentBar_TextChanged);
            this._channelLavelTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumberTextBox_KeyPress);
            // 
            // _channelLavelBar
            // 
            this._channelLavelBar.AutoSize = false;
            this._channelLavelBar.LargeChange = 1;
            this._channelLavelBar.Location = new System.Drawing.Point(11, 39);
            this._channelLavelBar.Maximum = 100;
            this._channelLavelBar.Name = "_channelLavelBar";
            this._channelLavelBar.Size = new System.Drawing.Size(183, 25);
            this._channelLavelBar.TabIndex = 1;
            this._channelLavelBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this._channelLavelBar.Scroll += new System.EventHandler(this._channelLavelBar_Scroll);
            // 
            // _toolTip
            // 
            this._toolTip.ShowAlways = true;
            // 
            // SensorChannelControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._mainGroupBox);
            this.Name = "SensorChannelControl";
            this.Size = new System.Drawing.Size(418, 79);
            this._mainGroupBox.ResumeLayout(false);
            this._mainGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._channelLavelBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox _mainGroupBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox _channelTimeTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox _channelLavelTextBox;
        private System.Windows.Forms.TrackBar _channelLavelBar;
        private System.Windows.Forms.ToolTip _toolTip;

    }
}
