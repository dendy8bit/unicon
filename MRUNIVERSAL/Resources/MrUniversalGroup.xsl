<?xml version="1.0" encoding="windows-1251"?>
<!-- Edited by XMLSpy® -->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>

<!-- ���������� ���������� -->
      <!-- h1 - ���������� �� ���������� -->
      <!-- h2 - ������� 1-�� ������, ������ ������� -->
      <!-- h3 - ������� 2-�� ������ -->
      <!-- h4 - ������� 3-�� ������ -->
      <!-- h5 - ������� 4-�� ������ -->
      <!-- h6 - ��������� -->

      <head>
        <style type="text/css">

<!-- �������� � ������� ����� ������ -->
      <!-- sticky1 - �������� ��������� 1-�� ������ (���������� �� ����������) -->
      <!-- sticky2 - �������� ��������� 2-�� ������ (������� 1-�� ������) -->
      <!-- sticky3 - �������� ��������� 3-�� ������ (������� 2-�� ������) -->
      <!-- sticky4 - �������� ��������� 4-�� ������ (������� 3-�� ������) -->
      <!-- sticky5 - �������� ��������� 5-�� ������ (������� 4-�� ������) -->
          @media screen {
          .sticky1 {
          position: sticky;
          top: 0;
          z-index: 6;
          text-align: center;
          }}
          @media print {
          .sticky1 {
          position: relative;
          z-index: 6;
          text-align: center;
          }}

          @media screen {
          .sticky2 {
          position: sticky;
          top: 50px;
          height: 38px;
          z-index: 5;
          text-align: center;
          background: white;
          color: #4b4b4b;
          }}
          @media print {
          .sticky2 {
          position: relative;
          z-index: 5;
          text-align: center;
          background: white;
          color: #4b4b4b;
          }}

          @media screen {
          .sticky3 {
          position: sticky;
          top: 85px;
          height: 33px;
          z-index: 4;
          text-align: center;
          background: white;
          color: #4b4b4b;
          }}
          @media print {
          .sticky3 {
          position: relartive;
          z-index: 4;
          text-align: center;
          background: white;
          color: #4b4b4b;
          }}

          @media screen {
          .sticky4 {
          position: sticky;
          top: 115px;
          height: 25px;
          z-index: 3;
          text-align: center;
          background: white;
          color: #4b4b4b;
          }}
          @media print {
          .sticky4 {
          position: relative;
          z-index: 3;
          text-align: center;
          background: white;
          color: #4b4b4b;
          }}

          @media screen {
          .sticky5 {
          position: sticky;
          top: 137px;
          z-index: 2;
          text-align: center;
          background: white;
          color: #4b4b4b;
          }}
          @media print {
          .sticky5 {
          position: relative;
          z-index: 2;
          text-align: center;
          background: white;
          color: #4b4b4b;
          }}

<!-- ����� ��� �������� � ������� ����� ������ ���������� -->
      <!-- block1 - ���� ��� �������� ��������� 1-�� ������ -->
      <!-- block2 - ���� ��� �������� ��������� 2-�� ������ -->
      <!-- block3 - ���� ��� �������� ��������� 3-�� ������ -->
      <!-- block4 - ���� ��� �������� ��������� 4-�� ������ -->
      <!-- block5 - ���� ��� �������� ��������� -->
          .block1 {
          }

          @media screen {
          h1 {
          white-space: nowrap;
          overflow: hidden;
          font-family: 'Merriweather', serif;
          position: relative;
          color: #FCF2E5;
          background: LightSlateGray;
          font-size: 30px;
          font-weight: normal;
          padding: 10px 30px;
          display: inline-block width: 100%;
          height: 32px;
          margin: 0;
          line-height: 1;
          }}
          @media print {
          h1 {
          font-family: 'Merriweather', serif;
          text-align: center;
          color: #FCF2E5;
          background: white;
          padding: 10px 10px;
          }}

          @media screen {
          h6 {
          font-size: 15px;
          font-weight: normal;
          margin-bottom: 1px;
          color: #4b4b4b;
          transition: all 1s;
          text-align: left;
          cursor: default;
          }}
          @media print {
          h6 {
          font-size: 16px;
          font-weight: normal;
          margin-bottom: 1px;
          color: black;
          transition: all 1s;
          text-align: left;
          }}

          @media screen {
          table {
          font-family: 'Merriweather', serif;
          font-size: 14px;
          cursor: default;
          border: 2px;
          border-spacing: 0;
          text-align: center;
          }}
          @media print {
          table {
          font-family: 'Merriweather', serif;
          border: 1px;
          border-spacing: 0;
          text-align: center;
          }}

          @media screen {
          th {
          background: LightSlateGray;
          color: white;
          padding: 3px 5px;
          }}
          @media print {
          th {
          font-size: 14px;
          background: none;
          color: black;
          padding: 3px 5px;
          }}

          @media screen {
          th, td {
          border-style: solid;
          border-width: 1px 1px 1px 1px;
          border-color: silver;
          }}
          @media print {
          th, td {
          border-style: solid;
          border-width: 1px 1px 1px 1px;
          border-color: black;
          page-break-inside: avoid;
          page-break-after: auto;
          }}

          @media screen {
          td {
          padding: 3px 5px;
          background: white;
          }}
          @media print {
          td {
          font-size: 12px;
          padding: 3px 5px;
          background: white;
          page-break-inside: avoid;
          }}

          .block2 {
          }

          .block3 {
          }

          .block4 h4 {
          }

          .block5 h5 {
          }

<!-- ������ ��� ������ -->
          @media print {
          h2 {
          margin-top: -25px;
          }}

          @media print {
          h3 {
          margin-top: -20px;
          }}

          @media print {
          h4 {
          margin-top: -19px;
          }}

          @media print {
          h5 {
          margin-top: -16px;
          }}

          @media print {
          h6 {
          margin-top: -14px;
          }}

        </style>
      </head>

      <body>

<!--/////����������/////-->
        <xsl:variable name="device" select="��_���/���_����������"/>
        <xsl:variable name="vers" select="��_���/������"/>
        <xsl:variable name="plant" select="��_���/����������_�����"/>
        <xsl:variable name="isPrimary" select="��_���/���������_�������"/>
        <xsl:variable name="group" select="��_���/������"/>
        
        <div class="block1">
          <div class="sticky1">
            <h1>
              ���������� - <xsl:value-of select="$device"/>, ����� - <xsl:value-of select="��_���/�����_����������"/>. ������ �� - <xsl:value-of select="$vers"/>. ���������� ����� - <xsl:value-of select="$plant"/>
            </h1>
          </div>
          <p/>

<!--///////������� "�������"///////-->
          <div class="block2">
            <div class="sticky2"><h2>�������</h2></div>

            <table>
              <tr>
                <th>���� �������� �������</th>
                <td><xsl:value-of select="��_���/����_��������_������/InputAdd"/></td>
              </tr>
            </table>
            <p/>

            <h6>��������� �������������</h6>
            <table>
              <tr>
                <th>���� ������������� � ��������� ���������</th>
              </tr>
              <tr>
                <td>
                  <xsl:for-each select="��_���/���������_�������">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
                </td>
              </tr>
            </table>
            <br/><br/><br/>
          </div>
          
          <xsl:for-each select="��_���/������������_����_�����_�������/AllGroups">
            <xsl:if test="position() = $group">
            <div class="block2">
              <div class="sticky2"><h2>������ ������� <xsl:value-of select="$group"/></h2></div>

<!--///////////�� ������� "�������" ������� "��������� ���������"///////////-->
              <div class="block3">
                <div class="sticky3"><h3>��������� ���������</h3></div>
                <p/>

                <h6>����</h6>
                <table>
                  <tr>
                    <th>��� ��</th>
                    <th>�����������</th>
                    <th>������� ����</th>
                    <th>I�, I�</th>
                    <th>ITT�, A</th>
                    <th>���������� Ia</th>
                    <th>���������� Ib</th>
                    <th>���������� Ic</th>
                    <th>ITTn, A</th>
                    <xsl:if test="$plant = 'T4N4D42R35' or $plant = 'T4N5D42R35' or $plant = 'T4N4D74R67' or $plant = 'T4N5D74R67'">
                      <th>���������� In</th>
                    </xsl:if>
                <!--<th>���������� In1</th> - ���������, ���� �5. ���� �3 - In � In1 �� ��������-->
                  </tr>
                  <tr>
                    <td><xsl:value-of select="MeasureTrans/�����_I/���_��"/></td>
                    <td><xsl:value-of select="MeasureTrans/�����_I/�����������_���"/></td>
                    <td><xsl:value-of select="MeasureTrans/�����_I/�������_����"/></td>
                    <td><xsl:value-of select="MeasureTrans/�����_I/I�"/></td>
                    <td><xsl:value-of select="MeasureTrans/�����_I/������������_��"/></td>
                    <td><xsl:value-of select="MeasureTrans/�����_I/����������_Ia"/></td>
                    <td><xsl:value-of select="MeasureTrans/�����_I/����������_Ib"/></td>
                    <td><xsl:value-of select="MeasureTrans/�����_I/����������_Ic"/></td>
                    <td><xsl:value-of select="MeasureTrans/�����_I/������������_����"/></td>
                    <xsl:if test="$plant = 'T4N4D42R35' or $plant = 'T4N5D42R35' or $plant = 'T4N4D74R67' or $plant = 'T4N5D74R67'">
                      <td><xsl:value-of select="MeasureTrans/�����_I/����������_In"/></td>
                    </xsl:if>
                <!--<td><xsl:value-of select="MeasureTrans/�����_I/����������_In1"/></td>-->
                  </tr>
                </table>
                <p/>

                <h6>���</h6>
                <table>
                  <tr>
                    <th>�����</th>
                    <th>
                      <xsl:if test="$isPrimary ='true'">X��1, �� ����./��</xsl:if>
                      <xsl:if test="$isPrimary ='false'">X��1, �� ����./��</xsl:if>
                    </th>
                    <th>
                      <xsl:if test="$isPrimary ='true'">X��2, �� ����./��</xsl:if>
                      <xsl:if test="$isPrimary ='false'">X��2, �� ����./��</xsl:if>
                    </th>
                    <th>
                      <xsl:if test="$isPrimary ='true'">X��3, �� ����./��</xsl:if>
                      <xsl:if test="$isPrimary ='false'">X��3, �� ����./��</xsl:if>
                    </th>
                    <th>
                      <xsl:if test="$isPrimary ='true'">X��4, �� ����./��</xsl:if>
                      <xsl:if test="$isPrimary ='false'">X��4, �� ����./��</xsl:if>
                    </th>
                    <th>
                      <xsl:if test="$isPrimary ='true'">X��5, �� ����./��</xsl:if>
                      <xsl:if test="$isPrimary ='false'">X��5, �� ����./��</xsl:if>
                    </th>
                    <th>L1, ��</th>
                    <th>L2, ��</th>
                    <th>L3, ��</th>
                    <th>L4, ��</th>
                  </tr>
                  <tr>
                    <td><xsl:value-of select="Omp/�����"/></td>
                    <td><xsl:value-of select="Omp/X��1"/></td>
                    <td><xsl:value-of select="Omp/X��2"/></td>
                    <td><xsl:value-of select="Omp/X��3"/></td>
                    <td><xsl:value-of select="Omp/X��4"/></td>
                    <td><xsl:value-of select="Omp/X��5"/></td>
                    <td><xsl:value-of select="Omp/L1"/></td>
                    <td><xsl:value-of select="Omp/L2"/></td>
                    <td><xsl:value-of select="Omp/L3"/></td>
                    <td><xsl:value-of select="Omp/L4"/></td>
                  </tr>
                </table>
                <p/>

                <h6>����������</h6>
                <table>
                  <tr>
                    <th>Uo</th>
                    <th>KTH�</th>
                    <th>KTHn</th>
                    <xsl:if test="$plant = 'T4N5D42R35' or $plant = 'T4N5D74R67'">
                      <th>KTHn1</th>
                    </xsl:if>
                    <xsl:if test="$device = 'MR761'">
                      <th>����</th>
                      <xsl:for-each select="MeasureTrans/�����_U/Input">
                        <xsl:if test="current() ='��������'"><th>Uca</th></xsl:if>
                      </xsl:for-each>
                    </xsl:if>
                  </tr>
                  <tr>
                    <td><xsl:value-of select="MeasureTrans/�����_U/���_Uo"/></td>
                    <td><xsl:value-of select="MeasureTrans/�����_U/KTHL"/></td>
                    <td><xsl:value-of select="MeasureTrans/�����_U/KTHX"/></td>
                    <xsl:if test="$plant = 'T4N5D42R35' or $plant = 'T4N5D74R67'">
                      <td><xsl:value-of select="MeasureTrans/�����_U/KTHX1"/></td>
                    </xsl:if>
                    <xsl:if test="$device = 'MR761'">
                      <td><xsl:value-of select="MeasureTrans/�����_U/Input"/></td>
                      <xsl:for-each select="MeasureTrans/�����_U/Input">
                        <xsl:if test="current() ='��������'">
                          <td><xsl:value-of select="../InputUca"></xsl:value-of></td>
                        </xsl:if>
                      </xsl:for-each>
                    </xsl:if>
                  </tr>
                </table>
                <p/>
                <h6>* - ���������� ����� � ����������� ��� ����� I* � ������ 3I0 ��� In</h6>
                <p/>

                <h6>�������� ������</h6>
                <table>
                  <xsl:if test="$device = 'MR761'">
                    <tr>
                      <th>T �������, �</th>
                      <th>T ����������, �</th>
                      <th>I��, I�</th>
                      <th>I����, I�</th>
                      <th>T����, ��</th>
                      <th>T����, �</th>
                      <th>Q���., %</th>
                      <th>���� Q �����</th>
                      <th>���� N �����</th>
                    </tr>
                    <tr>
                      <td><xsl:value-of select="TermConfig/T���"/></td>
                      <td><xsl:value-of select="TermConfig/T���"/></td>
                      <td><xsl:value-of select="TermConfig/I��"/></td>
                      <td><xsl:value-of select="TermConfig/I����"/></td>
                      <td><xsl:value-of select="TermConfig/T����"/></td>
                      <td><xsl:value-of select="TermConfig/T����"/></td>
                      <td><xsl:value-of select="TermConfig/Q"/></td>
                      <td><xsl:value-of select="TermConfig/Q_�����"/></td>
                      <td><xsl:value-of select="TermConfig/N_�����"/></td>
                    </tr>
                  </xsl:if>
                  <xsl:if test="$device = 'MR771'">
                    <tr>
                      <th>T �������, �</th>
                      <th>T ����������, �</th>
                      <th>I���, I�</th>
                      <th>���� Q �����</th>
                    </tr>
                    <tr>
                      <td><xsl:value-of select="TermConfig/T���"/></td>
                      <td><xsl:value-of select="TermConfig/T���"/></td>
                      <td><xsl:value-of select="TermConfig/I��"/></td>
                      <td><xsl:value-of select="TermConfig/Q_�����"/></td>
                    </tr>
                  </xsl:if>
                </table>
                <p/>

                <h6>�������� ����� ��</h6>
                <table>
                  <tr>
                    <th>������������� ��</th>
                    <th>������������� ��n</th>
                    <xsl:if test="$device = 'MR771'">
                      <th>������������� ��n1</th>
                    </xsl:if>
                    <th>I2</th>
                    <th>I2, I�</th>
                    <th>U2, �</th>
                    <th>3I0</th>
                    <th>3I0, I�</th>
                    <th>3I0, �</th>
                    <th>Umax, �</th>
                    <th>Umin, �</th>
                    <th>Imax, I�</th>
                    <th>Imin, I�</th>
                    <th>Td, ��</th>
                    <th>Ts, ��</th>
                    <th>�����</th>
                    <th>���. 3-� ���</th>
                    <th>dI, %</th>
                    <th>dU, %</th>
                  </tr>
                  <tr>
                    <td><xsl:value-of select="CheckTn/LfaultXml"/></td>
                    <td><xsl:value-of select="CheckTn/XfaultXml"/></td>
                    <xsl:if test="$device = 'MR771'">
                      <td><xsl:value-of select="MeasureTrans/�����_U/X1fault"/></td>
                    </xsl:if>
                    <td>
                      <xsl:for-each select="CheckTn/U2I2">
                        <xsl:if test="current() ='false'">���</xsl:if>
                        <xsl:if test="current() ='true'">��</xsl:if>
                      </xsl:for-each>
                    </td>
                    <td><xsl:value-of select="CheckTn/I2"/></td>
                    <td><xsl:value-of select="CheckTn/U2"/></td>
                    <td>
                      <xsl:for-each select="CheckTn/U0I0">
                        <xsl:if test="current() ='false'">���</xsl:if>
                        <xsl:if test="current() ='true'">��</xsl:if>
                      </xsl:for-each>
                    </td>
                    <td><xsl:value-of select="CheckTn/I0"/></td>
                    <td><xsl:value-of select="CheckTn/U0"/></td>
                    <td><xsl:value-of select="CheckTn/Umax"/></td>
                    <td><xsl:value-of select="CheckTn/Umin"/></td>
                    <td><xsl:value-of select="CheckTn/Imax"/></td>
                    <td><xsl:value-of select="CheckTn/Imin"/></td>
                    <td><xsl:value-of select="CheckTn/Td"/></td>
                    <td><xsl:value-of select="CheckTn/Ts"/></td>
                    <td><xsl:value-of select="CheckTn/Reset"/></td>
                    <td>
                      <xsl:for-each select="CheckTn/Phase3">
                        <xsl:if test="current() ='false'">���</xsl:if>
                        <xsl:if test="current() ='true'">��</xsl:if>
                      </xsl:for-each>
                    </td>
                    <td><xsl:value-of select="CheckTn/Idelta"/></td>
                    <td><xsl:value-of select="CheckTn/Udelta"/></td>
                  </tr>
                </table>
                <p/>

                <xsl:if test="$device = 'MR761'">
                  <h6>���������� ������</h6>
                  <table>
                    <tr>
                      <xsl:for-each select="TermConfig/�����������">
                        <xsl:if test="current() ='���'"><th>P, ���</th></xsl:if>
                        <xsl:if test="current() ='���'"><th>P, ���</th></xsl:if>
                      </xsl:for-each>
                      <th>cosf</th>
                      <th>���, %</th>
                    </tr>
                    <tr>
                      <td><xsl:value-of select="TermConfig/P"/></td>
                      <td><xsl:value-of select="DefensesSetpoints/��������/Cosf"/></td>
                      <td><xsl:value-of select="DefensesSetpoints/��������/���"/></td>
                    </tr>
                  </table>
                </xsl:if>
                <br/><br/><br/>
              </div>

<!--///////////�� ������� "�������" ������� "������"///////////-->
              <div class="block3">
                <div class="sticky3"><h3>������</h3></div>
                <p/>

<!--/////////////�� ������� "�������" ������� "������" ������� "�������. ������"/////////////-->
                <div class="block4">
                  <div class="sticky4"><h4>������������� ������</h4></div>
                  <p/>

<!--///////////////�� ������� "�������" ������� "������" ������� "�������. ������" ������� "����� ���������"///////////////-->
                  <div class="block5">
                    <div class="sticky5"><h5>����� ���������</h5></div>
                    <p/>

                    <h6>����������� ��</h6>
                    <table>
                      <th>
                        <table>
                          <tr>������ �-N1</tr>
                          <tr>
                            <th>Z0=</th>
                            <th><xsl:value-of select="ResistanceParam/R0Step1"/></th>
                            <th>+j</th>
                            <th><xsl:value-of select="ResistanceParam/X0Step1"/></th>
                            <th>
                              <xsl:if test="$isPrimary ='true'">�� ����.</xsl:if>
                              <xsl:if test="$isPrimary ='false'">�� ����.</xsl:if>
                            </th>
                          </tr>
                          <tr>
                            <th>Z1=</th>
                            <th><xsl:value-of select="ResistanceParam/R1Step1"/></th>
                            <th>+j</th>
                            <th><xsl:value-of select="ResistanceParam/X1Step1"/></th>
                            <th>
                              <xsl:if test="$isPrimary ='true'">�� ����.</xsl:if>
                              <xsl:if test="$isPrimary ='false'">�� ����.</xsl:if>
                            </th>
                          </tr>
                        </table>
                        <table>
                          <tr>������ �-N2</tr>
                          <tr>
                            <th>Z0=</th>
                            <th><xsl:value-of select="ResistanceParam/R0Step2"/></th>
                            <th>+j</th>
                            <th><xsl:value-of select="ResistanceParam/X0Step2"/></th>
                            <th>
                              <xsl:if test="$isPrimary ='true'">�� ����.</xsl:if>
                              <xsl:if test="$isPrimary ='false'">�� ����.</xsl:if>
                            </th>
                          </tr>
                          <tr>
                            <th>Z1=</th>
                            <th><xsl:value-of select="ResistanceParam/R1Step2"/></th>
                            <th>+j</th>
                            <th><xsl:value-of select="ResistanceParam/X1Step2"/></th>
                            <th>
                              <xsl:if test="$isPrimary ='true'">�� ����.</xsl:if>
                              <xsl:if test="$isPrimary ='false'">�� ����.</xsl:if>
                            </th>
                          </tr>
                        </table>
                        <table>
                          <tr>������ �-N3</tr>
                          <tr>
                            <th>Z0=</th>
                            <th><xsl:value-of select="ResistanceParam/R0Step3"/></th>
                            <th>+j</th>
                            <th><xsl:value-of select="ResistanceParam/X0Step3"/></th>
                            <th>
                              <xsl:if test="$isPrimary ='true'">�� ����.</xsl:if>
                              <xsl:if test="$isPrimary ='false'">�� ����.</xsl:if>
                            </th>
                          </tr>
                          <tr>
                            <th>Z1=</th>
                            <th><xsl:value-of select="ResistanceParam/R1Step3"/></th>
                            <th>+j</th>
                            <th><xsl:value-of select="ResistanceParam/X1Step3"/></th>
                            <th>
                              <xsl:if test="$isPrimary ='true'">�� ����.</xsl:if>
                              <xsl:if test="$isPrimary ='false'">�� ����.</xsl:if>
                            </th>
                          </tr>
                        </table>
                        <table>
                          <tr>������ �-N4</tr>
                          <tr>
                            <th>Z0=</th>
                            <th><xsl:value-of select="ResistanceParam/R0Step4"/></th>
                            <th>+j</th>
                            <th><xsl:value-of select="ResistanceParam/X0Step4"/></th>
                            <th>
                              <xsl:if test="$isPrimary ='true'">�� ����.</xsl:if>
                              <xsl:if test="$isPrimary ='false'">�� ����.</xsl:if>
                            </th>
                          </tr>
                          <tr>
                            <th>Z1=</th>
                            <th><xsl:value-of select="ResistanceParam/R1Step4"/></th>
                            <th>+j</th>
                            <th><xsl:value-of select="ResistanceParam/X1Step4"/></th>
                            <th>
                              <xsl:if test="$isPrimary ='true'">�� ����.</xsl:if>
                              <xsl:if test="$isPrimary ='false'">�� ����.</xsl:if>
                            </th>
                          </tr>
                        </table>
                        <table>
                          <tr>������ �-N5</tr>
                          <tr>
                            <th>Z0=</th>
                            <th><xsl:value-of select="ResistanceParam/R0Step5"/></th>
                            <th>+j</th>
                            <th><xsl:value-of select="ResistanceParam/X0Step5"/></th>
                            <th>
                              <xsl:if test="$isPrimary ='true'">�� ����.</xsl:if>
                              <xsl:if test="$isPrimary ='false'">�� ����.</xsl:if>
                            </th>
                          </tr>
                          <tr>
                            <th>Z1=</th>
                            <th><xsl:value-of select="ResistanceParam/R1Step5"/></th>
                            <th>+j</th>
                            <th><xsl:value-of select="ResistanceParam/X1Step5"/></th>
                            <th>
                              <xsl:if test="$isPrimary ='true'">�� ����.</xsl:if>
                              <xsl:if test="$isPrimary ='false'">�� ����.</xsl:if>
                            </th>
                          </tr>
                        </table>
                      </th>
                    </table>
                    <p/>

                    <h6>���� ������������ �������������� ��� �������� Z</h6>
                    <table>
                      <tr>
                        <th>y1, ����.</th>
                        <th>y2, ����.</th>
                      </tr>
                      <tr>
                        <td><xsl:value-of select="ResistanceParam/C1"/></td>
                        <td><xsl:value-of select="ResistanceParam/C2"/></td>
                      </tr>
                    </table>
                    <p/>

                    <h6>���� ��������</h6>
                    <table>
                      <tr>��� �������� �-�</tr>
                      <tr>
                        <th>y1, ����.</th>
                        <th>
                          <xsl:if test="$isPrimary ='true'">R1, �� ����.</xsl:if>
                          <xsl:if test="$isPrimary ='false'">R1, �� ����.</xsl:if>
                        </th>
                        <th>
                          <xsl:if test="$isPrimary ='true'">R2, �� ����.</xsl:if>
                          <xsl:if test="$isPrimary ='false'">R2, �� ����.</xsl:if>
                        </th>
                      </tr>
                      <tr>
                        <td><xsl:value-of select="AcCountLoad/CornerLine"/></td>
                        <td><xsl:value-of select="AcCountLoad/R1Line"/></td>
                        <td><xsl:value-of select="AcCountLoad/R2Line"/></td>
                      </tr>
                    </table>
                    <p/>

                    <h6>�������</h6>
                    <table>
                      <tr>
                        <th>���</th>
                        <th>
                          <xsl:if test="$isPrimary ='true'">R, �� ����.</xsl:if>
                          <xsl:if test="$isPrimary ='false'">R, �� ����.</xsl:if>
                        </th>
                        <th>
                          <xsl:if test="$isPrimary ='true'">X, �� ����.</xsl:if>
                          <xsl:if test="$isPrimary ='false'">X, �� ����.</xsl:if>
                        </th>
                        <th>
                          <xsl:if test="$isPrimary ='true'">dz, �� ����.</xsl:if>
                          <xsl:if test="$isPrimary ='false'">dz, �� ����.</xsl:if>
                        </th>
                        <xsl:for-each select="Swing/Type">
                          <xsl:if test="current() ='�������������'">
                            <th>f, ����.</th>
                          </xsl:if>
                          <xsl:if test="current() ='��������'">
                            <th>
                              <xsl:if test="$isPrimary ='true'">r, �� ����.</xsl:if>
                              <xsl:if test="$isPrimary ='false'">r, �� ����.</xsl:if>
                            </th>
                          </xsl:if>
                        </xsl:for-each>

                        <th>Tdz, ��</th>
                        <th>3I0�, I�</th>
                        <th>I�, I�</th>
                        <th>����� ���������� �� �������</th>
                        <th>T�, ��</th>
                      </tr>
                      <tr>
                        <td><xsl:value-of select="Swing/Type"/></td>
                        <td><xsl:value-of select="Swing/R1"/></td>
                        <td><xsl:value-of select="Swing/X1"/></td>
                        <td><xsl:value-of select="Swing/Dr"/></td>
                        <td><xsl:value-of select="Swing/C1"/></td>
                        <td><xsl:value-of select="Swing/Tsrab"/></td>
                        <td><xsl:value-of select="Swing/I0"/></td>
                        <td><xsl:value-of select="Swing/Imin"/></td>
                        <td>
                          <xsl:for-each select="Swing/Reset">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td><xsl:value-of select="Swing/ResetTime"/></td>
                      </tr>
                    </table>
                    <p/>

                    <h6>��������� ���</h6>
                    <table>
                      <tr>
                        <th>Umin, �</th>
                        <th>Imax, In</th>
                        <th>����� 1-�� �� ��� ����</th>
                      </tr>
                      <tr>
                        <td><xsl:value-of select="AcCountLoad/Umin"/></td>
                        <td><xsl:value-of select="AcCountLoad/Imax"/></td>
                        <td>
                          <xsl:for-each select="AcCountLoad/Config">
                            <xsl:if test="current() ='false'">���	</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                      </tr>
                    </table>
                    <br/><br/><br/>
                  </div>

<!--///////////////�� ������� "�������" ������� "������" ������� "�������. ������" ������� "Z"///////////////-->
                  <div class="block5">
                    <div class="sticky5"><h5>�������</h5></div>
                    <table>
                      <tr>
                        <th>������� �</th>
                        <th>�����</th>
                        <th>���</th>
                        <th>����������</th>
                        <th>
                          <xsl:if test="$isPrimary ='true'">R, �� ����.</xsl:if>
                          <xsl:if test="$isPrimary ='false'">R, �� ����.</xsl:if>
                        </th>
                        <th>
                          <xsl:if test="$isPrimary ='true'">X, �� ����.</xsl:if>
                          <xsl:if test="$isPrimary ='false'">X, �� ����.</xsl:if>
                        </th>
                        <th>
                          <xsl:if test="$isPrimary ='true'">r, �� ����. | f, ����.</xsl:if>
                          <xsl:if test="$isPrimary ='false'">r, �� ����. | f, ����.</xsl:if>
                        </th>
                        <th>tcp, ��</th>
                        <th>Icp, In</th>
                        <th>���� ���������</th>
                        <th>ty, ��</th>
                        <th>�����������</th>
                        <th>U����</th>
                        <th>U����, �</th>
                        <th>������</th>
                        <th>����. ������. ��</th>
                        <th>����. �� ��������</th>
                        <th>����. �� �������</th>
                        <th>��������. ��� �����.</th>
                        <th>���� �� ���</th>
                        <th>�����������</th>
                        <th>����</th>
                        <th>���</th>
                        <xsl:if test="$device = 'MR761'">
                          <th>���</th>
                        </xsl:if>
                      </tr>
                      <xsl:for-each select="DefensesSetpoints/��_�������������/���_�������������_������">
                        <tr>
                          <td><xsl:value-of select="position()"/></td>
                          <td><xsl:value-of select="Mode"/></td>
                          <td><xsl:value-of select="Type"/></td>
                          <td><xsl:value-of select="BlocInp"/></td>
                          <td><xsl:value-of select="UstR"/></td>
                          <td><xsl:value-of select="UstX"/></td>
                          <td><xsl:value-of select="Ustr"/></td>
                          <td><xsl:value-of select="Tsr"/></td>
                          <td><xsl:value-of select="Isr"/></td>
                          <td><xsl:value-of select="Acceleration"/></td>
                          <td><xsl:value-of select="Tu"/></td>
                          <td><xsl:value-of select="Direction"/></td>
                          <td>
                            <xsl:for-each select="StartOnU">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td><xsl:value-of select="Ustart"/></td>
                          <td><xsl:value-of select="Conture"/></td>
                          <td><xsl:value-of select="BlockFromTn"/></td>
                          <td>
                            <xsl:for-each select="BlockFromLoad">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td>
                            <xsl:for-each select="BlockFromSwing">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td>
                            <xsl:for-each select="SteeredModeAcceler">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td>
                            <xsl:for-each select="DamageFaza">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td>
                            <xsl:value-of select="Oscilloscope"/>
                          </td>
                          <td>
                            <xsl:for-each select="Urov">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td><xsl:value-of select="���_�����"/></td>
                          <xsl:if test="$device = 'MR761'">
                            <td><xsl:value-of select="Avr"/></td>
                          </xsl:if>
                        </tr>
                      </xsl:for-each>
                    </table>
                    <p>* - ����� �������� � ������ �-N ��� �������� ����������� �� � �����������.</p>
                    <br/><br/><br/>
                  </div>
                </div>

<!--/////////////�� ������� "�������" ������� "������" ������� "���� �����"/////////////-->
                <div class="block4">
                  <div class="sticky4"><h4>���� �����</h4></div>
                  <p/>

                  <h6>���� ����� ��� ������� ������������ �����</h6>
                  <table>
                    <tr>
                      <th>I</th>
                      <th>3I0</th>
                      <th>In</th>
                      <th>I2</th>
                    </tr>
                    <tr>
                      <td><xsl:value-of select="DefensesSetpoints/����/I"/></td>
                      <td><xsl:value-of select="DefensesSetpoints/����/I0"/></td>
                      <td><xsl:value-of select="DefensesSetpoints/����/In"/></td>
                      <td><xsl:value-of select="DefensesSetpoints/����/I2"/></td>
                    </tr>
                  </table>
                  <br/><br/><br/>
                </div>

<!--/////////////�� ������� "�������" ������� "������" ������� "������ I"/////////////-->
                <div class="block4">
                  <div class="sticky4"><h4>������ I</h4></div>
                  <p/>

                  <xsl:if test="$device = 'MR761'">
                    <h6>������ I>1-4 ������������� ����</h6>
                  </xsl:if>
                  <xsl:if test="$device = 'MR771'">
                    <h6>������ I>1-6 ������������� ����</h6>
                  </xsl:if>

                  <table>
                    <tr>
                      <th>�������</th>
                      <th>�����</th>
                      <th>I��, I� ��</th>
                      <th>U����, �</th>
                      <th>���� �� U</th>
                      <th>���. �� ������. ��</th>
                      <th>�����������</th>
                      <th>������. ����.</th>
                      <th>������</th>
                      <th>��������������</th>
                      <th>t��., ��/����</th>
                      <th>k �����. ���-��</th>
                      <th>���� ���������</th>
                      <th>ty, ��</th>
                      <th>����������</th>
                      <th>I2�/I1�, %</th>
                      <th>����. �� I2�/I1�</th>
                      <th>������. ����.</th>
                      <th>������. ��� �����.</th>
                      <th>�����������</th>
                      <th>����</th>
                      <th>���</th>
                      <xsl:if test="$device = 'MR761'">
                        <th>���</th>
                      </xsl:if>
                    </tr>
                    <xsl:for-each select="DefensesSetpoints/I/���_���/MtzMainStruct">
                      <tr>
                        <xsl:if test="$device = 'MR761'">
                          <xsl:if test="position() &lt; 5">
                            <td>������� I><xsl:value-of select="position()"/></td>
                            <td><xsl:value-of select="�����"/></td>
                            <td><xsl:value-of select="�������"/></td>
                            <td><xsl:value-of select="U_����"/></td>
                            <td><xsl:for-each select="U����"><xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                            <td><xsl:value-of select="����_��_������_��"/></td>
                            <td><xsl:value-of select="�����������"/></td>
                            <td><xsl:value-of select="������_����_x0028_I_x002A__x0029_"/></td>
                            <td><xsl:value-of select="������"/></td>
                            <td><xsl:value-of select="��������������"/></td>
                            <td><xsl:value-of select="t��"/></td>
                            <td><xsl:value-of select="K"/></td>
                            <td><xsl:value-of select="Uskor"/></td>
                            <td><xsl:value-of select="ty"/></td>
                            <td><xsl:value-of select="����������"/></td>
                            <td><xsl:value-of select="�������_2�_1�"/></td>
                            <td><xsl:for-each select="����_2�_1�"><xsl:if test="current() ='false'">���</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                            <td><xsl:for-each select="������_����"><xsl:if test="current() ='false'">���</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                            <td><xsl:for-each select="������.��������"><xsl:if test="current() ='false'">���</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                            <td><xsl:value-of select="���"/></td>
                            <td><xsl:for-each select="����"><xsl:if test="current() ='false'">���</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                            <td><xsl:value-of select="���"/></td>
                            <td><xsl:value-of select="���_����"/></td>
                          </xsl:if>
                        </xsl:if>
                        <xsl:if test="$device = 'MR771'">
                          <xsl:if test="position() &lt; 7">
                            <td>������� I><xsl:value-of select="position()"/></td>
                            <td><xsl:value-of select="�����"/></td>
                            <td><xsl:value-of select="�������"/></td>
                            <td><xsl:value-of select="U_����"/></td>
                            <td><xsl:for-each select="U����"><xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                            <td><xsl:value-of select="����_��_������_��"/></td>
                            <td><xsl:value-of select="�����������"/></td>
                            <td><xsl:value-of select="������_����_x0028_I_x002A__x0029_"/></td>
                            <td><xsl:value-of select="������"/></td>
                            <td><xsl:value-of select="��������������"/></td>
                            <td><xsl:value-of select="t��"/></td>
                            <td><xsl:value-of select="K"/></td>
                            <td><xsl:value-of select="Uskor"/></td>
                            <td><xsl:value-of select="ty"/></td>
                            <td><xsl:value-of select="����������"/></td>
                            <td><xsl:value-of select="�������_2�_1�"/></td>
                            <td><xsl:for-each select="����_2�_1�"><xsl:if test="current() ='false'">���</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                            <td><xsl:for-each select="������_����"><xsl:if test="current() ='false'">���</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                            <td><xsl:for-each select="������.��������"><xsl:if test="current() ='false'">���</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                            <td><xsl:value-of select="���"/></td>
                            <td><xsl:for-each select="����"><xsl:if test="current() ='false'">���</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                            <td><xsl:value-of select="���"/></td>
                          </xsl:if>
                        </xsl:if>
                      </tr>
                    </xsl:for-each>
                  </table>
                  <p/>

                  <xsl:if test="$device = 'MR761'">
                    <h6>������ I>5-6 ������������� ����</h6>
                    <table>
                      <tr>
                        <th>�������</th>
                        <th>�����</th>
                        <th>I��, I� ��</th>
                        <th>�������</th>
                        <th>U����, �</th>
                        <th>���� �� U</th>
                        <th>���. �� ������. ��</th>
                        <th>�����������</th>
                        <th>������. ����.</th>
                        <th>������</th>
                        <th>��������������</th>
                        <th>t��., ��/����</th>
                        <th>k �����. ���-��</th>
                        <th>���� ���������</th>
                        <th>ty, ��</th>
                        <th>����������</th>
                        <th>I2�/I1�, %</th>
                        <th>����. �� I2�/I1�</th>
                        <th>������. ����.</th>
                        <th>������. ��� �����.</th>
                        <th>�����������</th>
                        <th>����</th>
                        <th>���</th>
                        <th>���</th>
                      </tr>
                      <xsl:for-each select="DefensesSetpoints/I/���_���/MtzMainStruct">
                        <tr>
                          <xsl:if test="position() &gt; 4 and position() &lt; 7">
                            <td>������� I><xsl:value-of select="position()"/></td>
                            <td><xsl:value-of select="�����"/></td>
                            <td><xsl:value-of select="�������"/></td>
                            <td><xsl:value-of select="�������"/></td>
                            <td><xsl:value-of select="U_����"/></td>
                            <td><xsl:for-each select="U����"><xsl:if test="current() ='false'">���</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                            <td><xsl:value-of select="����_��_������_��"/></td>
                            <td><xsl:value-of select="�����������"/></td>
                            <td><xsl:value-of select="������_����_x0028_I_x002A__x0029_"/></td>
                            <td><xsl:value-of select="������"/></td>
                            <td><xsl:value-of select="��������������"/></td>
                            <td><xsl:value-of select="t��"/></td>
                            <td><xsl:value-of select="K"/></td>
                            <td><xsl:value-of select="Uskor"/></td>
                            <td><xsl:value-of select="ty"/></td>
                            <td><xsl:value-of select="����������"/></td>
                            <td><xsl:value-of select="�������_2�_1�"/></td>
                            <td><xsl:for-each select="����_2�_1�"><xsl:if test="current() ='false'">���</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                            <td><xsl:for-each select="������_����"><xsl:if test="current() ='false'">���</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                            <td><xsl:for-each select="������.��������"><xsl:if test="current() ='false'">���</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                            <td><xsl:value-of select="���"/></td>
                            <td><xsl:for-each select="����"><xsl:if test="current() ='false'">���</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                            <td><xsl:value-of select="���"/></td>
                            <td><xsl:value-of select="���_����"/></td>
                          </xsl:if>
                        </tr>
                      </xsl:for-each>
                    </table>
                  </xsl:if>
                  <p/>

                  <h6>������ I&#60; ������������ ����</h6>
                  <table>
                    <tr>
                      <th>�������</th>
                      <th>�����</th>
                      <th>I��, I� ��</th>
                      <th>������</th>
                      <th>t, ��</th>
                      <th>����������</th>
                      <th>�����������</th>
                      <th>����</th>
                      <th>���</th>
                      <xsl:if test="$device = 'MR761'">
                        <th>���</th>
                      </xsl:if>
                    </tr>
                    <xsl:for-each select="DefensesSetpoints/I/���_���/MtzMainStruct">
                      <tr>
                        <xsl:if test="position()=7">
                          <td>������� I&#60;</td>
                          <td><xsl:value-of select="�����"/></td>
                          <td><xsl:value-of select="�������"/></td>
                          <td><xsl:value-of select="������"/></td>
                          <td><xsl:value-of select="t��"/></td>
                          <td><xsl:value-of select="����������"/></td>
                          <td><xsl:value-of select="���"/></td>
                          <td>
                            <xsl:for-each select="����">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td><xsl:value-of select="���"/></td>
                          <xsl:if test="$device = 'MR761'">
                            <td><xsl:value-of select="���_����"/></td>
                          </xsl:if>
                        </xsl:if>
                      </tr>
                    </xsl:for-each>
                  </table>
                  <br/><br/><br/>
                </div>

<!--/////////////�� ������� "�������" ������� "������" ������� "������ I*"/////////////-->
                <div class="block4">
                  <div class="sticky4"><h4>������ I*</h4></div>
                  <table>
                    <tr>
                      <th>�������</th>
                      <th>�����</th>
                      <th>I, I� ��</th>
                      <th>U����, �</th>
                      <th>���� �� U</th>
                      <th>�����������</th>
                      <th>������. ������.</th>
                      <th>I*</th>
                      <th>��������������</th>
                      <th>t��., ��/����</th>
                      <th>k �����. ���-��</th>
                      <th>����������</th>
                      <th>�����������</th>
                      <th>���� ���������</th>
                      <th>ty, ��</th>
                      <th>������. ��� �����.</th>
                      <th>����</th>
                      <th>���</th>
                      <xsl:if test="$device = 'MR761'">
                        <th>���</th>
                      </xsl:if>
                    </tr>
                    <xsl:for-each select="DefensesSetpoints/I_x002A_/���_I_x002A_/DefenseStarStruct">
                      <tr>
                        <td>������� I*><xsl:value-of select="position()"/></td>
                        <td><xsl:value-of select="�����"/></td>
                        <td><xsl:value-of select="�������"/></td>
                        <td><xsl:value-of select="U_����"/></td>
                        <td>
                          <xsl:for-each select="U����">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td><xsl:value-of select="�����������"/></td>
                        <td><xsl:value-of select="������_����_x0028_I_x002A__x0029_"/></td>
                        <td><xsl:value-of select="I_x002A_"/></td>
                        <td><xsl:value-of select="��������������"/></td>
                        <td><xsl:value-of select="t��"/></td>
                        <td><xsl:value-of select="K"/></td>
                        <td><xsl:value-of select="����������"/></td>
                        <td><xsl:value-of select="���"/></td>
                        <td><xsl:value-of select="Uskor"/></td>
                        <td><xsl:value-of select="ty"/></td>
                        <td>
                          <xsl:for-each select="������.��������">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td>
                          <xsl:for-each select="����">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td><xsl:value-of select="���"/></td>
                        <xsl:if test="$device = 'MR761'">
                          <td><xsl:value-of select="���"/></td>
                        </xsl:if>
                      </tr>
                    </xsl:for-each>
                  </table>
                  <br/><br/><br/>
                </div>

<!--/////////////�� ������� "�������" ������� "������" ������� "I2I1"/////////////-->
                <div class="block4">
                  <div class="sticky4"><h4>I2I1</h4></div>
                  <table>
                    <tr>
                      <th>�����</th>
                      <td><xsl:value-of select="DefensesSetpoints/I2I1/�����"/></td>
                    </tr>
                    <tr>
                      <th>����������</th>
                      <td><xsl:value-of select="DefensesSetpoints/I2I1/����������"/></td>
                    </tr>
                    <tr>
                      <th>I2/I1, %</th>
                      <td><xsl:value-of select="DefensesSetpoints/I2I1/�������_I2_I1"/></td>
                    </tr>
                    <tr>
                      <th>tcp, ��</th>
                      <td><xsl:value-of select="DefensesSetpoints/I2I1/t��"/></td>
                    </tr>
                    <tr>
                      <th>���.</th>
                      <td><xsl:value-of select="DefensesSetpoints/I2I1/���"/></td>
                    </tr>
                    <tr>
                      <th>����</th>
                      <td>
                        <xsl:for-each select="DefensesSetpoints/I2I1/����">
                          <xsl:if test="current() ='false'">���</xsl:if>
                          <xsl:if test="current() ='true'">��</xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                    <tr>
                      <th>���</th>
                      <td><xsl:value-of select="DefensesSetpoints/I2I1/���"/></td>
                    </tr>
                    <xsl:if test="$device = 'MR761'">
                      <tr>
                        <th>���</th>
                        <td><xsl:value-of select="DefensesSetpoints/I2I1/���"/></td>
                      </tr>
                    </xsl:if>
                  </table>
                  <br/><br/><br/>
                </div>

<!--/////////////�� ������� "�������" ������� "������" ������� "���� ������� ���."/////////////-->
                <xsl:if test="$device = 'MR761'">
                  <div class="block4">
                    <div class="sticky4"><h4>������� ������</h4></div>
                    <table>
                      <tr>
                        <th>�����</th>
                        <td><xsl:value-of select="DefensesSetpoints/������������_�����_�������_������/�����"/></td>
                      </tr>
                      <tr>
                        <th>����������</th>
                        <td><xsl:value-of select="DefensesSetpoints/������������_�����_�������_������/����������"/></td>
                      </tr>
                      <tr>
                        <th>Icp, I�</th>
                        <td><xsl:value-of select="DefensesSetpoints/������������_�����_�������_������/�������"/></td>
                      </tr>
                      <tr>
                        <th>�����������</th>
                        <td>
                          <xsl:for-each select="DefensesSetpoints/������������_�����_�������_������/�����������">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                      </tr>
                    </table>
                    <br/><br/><br/>
                  </div>
                </xsl:if>

<!--/////////////�� ������� "�������" ������� "������" ������� "������ U"///////////////-->
                <div class="block4">
                  <div class="sticky4"><h4>������ U</h4></div>
                  <p/>

                  <h6>������ U></h6>
                    <table>
                      <tr>
                        <th>�������</th>
                        <th>�����</th>
                        <th>���</th>
                        <th>U��, �</th>
                        <th>t��, ��</th>
                        <th>t��, ��</th>
                        <th>U���, �</th>
                        <th>�������</th>
                        <th>����������</th>
                        <th>�����������</th>
                        <th>����</th>
                        <th>���</th>
                          <xsl:if test="$device = 'MR761'">
                        <th>���</th>
                          </xsl:if>
                        <th>��� �����.</th>
                        <th>�����</th>
                      </tr>
                    <xsl:for-each select="DefensesSetpoints/U/���_������_U/DefenceUStruct">
                      <xsl:if test="position()&lt;5">
                        <tr>
                          <td>������� U&#62;<xsl:value-of select="position()"/></td>
                          <td><xsl:value-of select="�����"/></td>
                          <td><xsl:value-of select="���_Umax"/></td>
                          <td><xsl:value-of select="�������"/></td>
                          <td><xsl:value-of select="t��"/></td>
                          <td><xsl:value-of select="t��"/></td>
                          <td><xsl:value-of select="U��"/></td>
                          <td>
                            <xsl:for-each select="U��_����_x002F_���">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td><xsl:value-of select="����������"/></td>
                          <td><xsl:value-of select="���"/></td>
                          <td>
                            <xsl:for-each select="����">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td><xsl:value-of select="���_�����"/></td>
                          <xsl:if test="$device = 'MR761'">
                            <td><xsl:value-of select="���"/></td>
                          </xsl:if>
                          <td>
                            <xsl:for-each select="���_�������">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td>
                            <xsl:for-each select="�����_�������">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                        </tr>
                      </xsl:if>
                    </xsl:for-each>
                  </table>
                  <p/>

                  <h6>������ U&#60;</h6>
                  <table>
                    <tr>
                      <th>�������</th>
                      <th>�����</th>
                      <th>���</th>
                      <th>U��, �</th>
                      <th>t��, ��</th>
                      <th>t��, ��</th>
                      <th>U���, �</th>
                      <th>�������</th>
                      <th>���������� U&#60;5�</th>
                      <th>����. �� ������. ��</th>
                      <th>����������</th>
                      <th>�����������</th>
                      <th>����</th>
                      <th>���</th>
                      <xsl:if test="$device = 'MR761'">
                        <th>���</th>
                      </xsl:if>
                      <th>��� �����.</th>
                      <th>�����</th>
                    </tr>
                    <xsl:for-each select="DefensesSetpoints/U/���_������_U/DefenceUStruct">
                      <xsl:if test="position() &lt; 5">
                        <tr>
                          <td>������� U&#60;<xsl:value-of select="position()"/></td>
                          <td><xsl:value-of select="�����"/></td>
                          <td><xsl:value-of select="���_Umin"/></td>
                          <td><xsl:value-of select="�������"/></td>
                          <td><xsl:value-of select="t��"/></td>
                          <td><xsl:value-of select="t��"/></td>
                          <td><xsl:value-of select="U��"/></td>
                          <td>
                            <xsl:for-each select="U��_����_x002F_���">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td>
                            <xsl:for-each select="����������_U_5V">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td><xsl:value-of select="����_��_������_��"/></td>
                          <td><xsl:value-of select="����������"/></td>
                          <td><xsl:value-of select="���"/></td>
                          <td>
                            <xsl:for-each select="����">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td>
                            <xsl:value-of select="���_�����"/>
                          </td>
                          <xsl:if test="$device = 'MR761'">
                            <td><xsl:value-of select="���"/></td>
                          </xsl:if>
                          <td>
                            <xsl:for-each select="���_�������">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td>
                            <xsl:for-each select="�����_�������">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                        </tr>
                      </xsl:if>
                    </xsl:for-each>
                  </table>
                  <br/><br/><br/>
                </div>

<!--/////////////�� ������� "�������" ������� "������" ������� "������ F"/////////////-->
                <div class="block4">
                  <div class="sticky4"><h4>������ F</h4></div>
                  <p/>

                  <h6>������ F&#62;</h6>
                  <table>
                    <tr>
                      <th>�������</th>
                      <th>�����</th>
                      <th>���</th>
                      <th>F��,�� | dF/dt, ��/�</th>
                      <th>U1, �</th>
                      <th>tcp, ��</th>
                      <th>t��, ��</th>
                      <th>F��, ��</th>
                      <th>�������</th>
                      <th>����������</th>
                      <th>�����������</th>
                      <th>����</th>
                      <th>���</th>
                      <xsl:if test="$device = 'MR761'">
                        <th>���</th>
                      </xsl:if>
                      <th>��� �����.</th>
                      <th>�����</th>
                    </tr>
                    <xsl:for-each select="DefensesSetpoints/Fb/���_������_F/DefenseFStruct">
                      <tr>
                        <td>������� F&#62;<xsl:value-of select="position()"/></td>
                        <td><xsl:value-of select="�����"/></td>
                        <td><xsl:value-of select="���"/></td>
                        <td><xsl:value-of select="�������"/></td>
                        <td><xsl:value-of select="U1"/></td>
                        <td><xsl:value-of select="t��"/></td>
                        <td><xsl:value-of select="ty"/></td>
                        <td><xsl:value-of select="U_����"/></td>
                        <td>
                          <xsl:for-each select="U����">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td><xsl:value-of select="����������"/></td>
                        <td><xsl:value-of select="���"/></td>
                        <td>
                          <xsl:for-each select="����">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td>
                          <xsl:value-of select="���_�����"/>
                        </td>
                        <xsl:if test="$device = 'MR761'">
                          <td>
                            <xsl:value-of select="���_�����"/>
                          </td>
                        </xsl:if>
                        <td>
                          <xsl:for-each select="���_�������">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td>
                          <xsl:for-each select="�����_�������">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                  <p/>

                  <h6>������ F&#60;</h6>
                  <table>
                    <tr>
                      <th>�������</th>
                      <th>���������</th>
                      <th>���</th>
                      <th>F��, �� | dF/dt, ��/�</th>
                      <th>U1, �</th>
                      <th>tcp, ��</th>
                      <th>t��, ��</th>
                      <th>F��, ��</th>
                      <th>�������</th>
                      <th>����������</th>
                      <th>�����������</th>
                      <th>����</th>
                      <th>���</th>
                      <xsl:if test="$device = 'MR761'">
                        <th>���</th>
                      </xsl:if>
                      <th>��� �����.</th>
                      <th>�����</th>
                    </tr>
                    <xsl:for-each select="DefensesSetpoints/Fm/���_������_F/DefenseFStruct">
                      <tr>
                        <td>������� F&#60;<xsl:value-of select="position()"/></td>
                        <td><xsl:value-of select="�����"/></td>
                        <td><xsl:value-of select="���"/></td>
                        <td><xsl:value-of select="�������"/></td>
                        <td><xsl:value-of select="U1"/></td>
                        <td><xsl:value-of select="t��"/></td>
                        <td><xsl:value-of select="ty"/></td>
                        <td><xsl:value-of select="U_����"/></td>
                        <td>
                          <xsl:for-each select="U����">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td><xsl:value-of select="����������"/></td>
                        <td><xsl:value-of select="���"/></td>
                        <td>
                          <xsl:for-each select="����">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td><xsl:value-of select="���_�����"/></td>
                        <xsl:if test="$device = 'MR761'">
                          <td><xsl:value-of select="���_�����"/></td>
                        </xsl:if>
                        <td>
                          <xsl:for-each select="���_�������">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td>
                          <xsl:for-each select="�����_�������">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                  <br/><br/><br/>
                </div>

<!--/////////////�� ������� "�������" ������� "������" ������� "������ Q"/////////////-->
                <div class="block4">
                  <div class="sticky4"><h4>������ Q</h4></div>
                  <p/>

                  <h6>���������� �� ��������� ��������� Q</h6>
                  <table>
                    <tr>
                      <th>�����</th>
                      <th>������� Q���, %</th>
                      <th>����� t���., �</th>
                    </tr>
                    <tr>
                      <td><xsl:value-of select="DefensesSetpoints/��������/�����"/></td>
                      <td><xsl:value-of select="DefensesSetpoints/��������/�������_������������"/></td>
                      <td><xsl:value-of select="DefensesSetpoints/��������/�����_������������"/></td>
                    </tr>
                  </table>
                  <p/>

                  <xsl:if test="$device = 'MR761'">
                    <h6>���������� �� ����� ������</h6>
                    <table>
                      <tr>
                        <th>����� ����� ������ N����</th>
                        <th>����� ������� ������ N���.</th>
                        <th>����� t���., �</th>
                      </tr>
                      <tr>
                        <td><xsl:value-of select="DefensesSetpoints/��������/�����_��������_������"/></td>
                        <td><xsl:value-of select="DefensesSetpoints/��������/�����_�������_������"/></td>
                        <td><xsl:value-of select="DefensesSetpoints/��������/�����_����������"/></td>
                      </tr>
                    </table>
                    <p/>
                  </xsl:if>

                  <h6>������ Q</h6>
                  <table>
                    <tr>
                      <th></th>
                      <th>�����</th>
                      <th>������� Q, %</th>
                      <th>����������</th>
                      <th>���.</th>
                      <th>����</th>
                      <th>���</th>
                      <xsl:if test="$device = 'MR761'">
                        <th>���</th>
                      </xsl:if>
                    </tr>
                    <xsl:for-each select="DefensesSetpoints/Q/���_������_Q/DefenseQStruct">
                      <xsl:if test="position()=1">
                        <tr>
                          <td>Q&#62;</td>
                          <td><xsl:value-of select="�����"/></td>
                          <td><xsl:value-of select="Ustavka"/></td>
                          <td><xsl:value-of select="����������"/></td>
                          <td><xsl:value-of select="���"/></td>
                          <td>
                            <xsl:for-each select="����">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td><xsl:value-of select="���_�����"/></td>
                          <xsl:if test="$device = 'MR761'">
                            <td><xsl:value-of select="���_�����"/></td>
                          </xsl:if>
                        </tr>
                      </xsl:if>
                      <xsl:if test="position()=2">
                        <tr>
                          <td>Q&#62;&#62;</td>
                          <td><xsl:value-of select="�����"/></td>
                          <td><xsl:value-of select="Ustavka"/></td>
                          <td><xsl:value-of select="����������"/></td>
                          <td><xsl:value-of select="���"/></td>
                          <td>
                            <xsl:for-each select="����">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td><xsl:value-of select="���_�����"/></td>
                          <xsl:if test="$device = 'MR761'">
                            <td><xsl:value-of select="���_�����"/></td>
                          </xsl:if>
                        </tr>
                      </xsl:if>
                    </xsl:for-each>
                  </table>
                  <br/><br/><br/>
                </div>

<!--/////////////�� ������� "�������" ������� "������" ������� "�������"/////////////-->
                <div class="block4">
                  <div class="sticky4"><h4>�������</h4></div>
                  <table>
                    <tr>
                      <th>�������</th>
                      <th>�����</th>
                      <th>������ ������������</th>
                      <th>t��, ��</th>
                      <th>t��, ��</th>
                      <th>������ �������</th>
                      <th>�������</th>
                      <th>������ ����������</th>
                      <th>�����������</th>
                      <th>����</th>
                      <th>���</th>
                      <xsl:if test="$device = 'MR761'">
                        <th>���</th>
                      </xsl:if>
                      <th>��� �����.</th>
                      <th>�����</th>
                    </tr>
                    <xsl:for-each select="DefensesSetpoints/�������/���_�������/DefenseExternalStruct">
                      <xsl:if test="position() &lt; 17">
                        <tr>
                          <td>������� <xsl:value-of select="position()"/></td>
                          <td><xsl:value-of select="�����"/></td>
                          <td><xsl:value-of select="����_���_�������_�����"/></td>
                          <td><xsl:value-of select="t��"/></td>
                          <td><xsl:value-of select="ty"/></td>
                          <td><xsl:value-of select="�������_���_�������_�����"/></td>
                          <td>
                            <xsl:for-each select="U����">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td><xsl:value-of select="����������"/></td>
                          <td><xsl:value-of select="���"/></td>
                          <td>
                            <xsl:for-each select="����">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td><xsl:value-of select="���_�����"/></td>
                          <xsl:if test="$device = 'MR761'">
                            <td><xsl:value-of select="���_�����"/></td>
                          </xsl:if>
                          <td>
                            <xsl:for-each select="���_�������">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td>
                            <xsl:for-each select="�����_�������">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                        </tr>
                      </xsl:if>
                    </xsl:for-each>
                  </table>
                  <br/><br/><br/>
                </div>

<!--/////////////�� ������� "�������" ������� "������" ������� "������ P"////////////-->
                <xsl:if test="$device = 'MR761'">
                  <div class="block4">
                    <div class="sticky4"><h4>������ P</h4></div>
                    <p/>

                    <h6>������ P</h6>
                    <table>
                      <tr>
                        <th>�������</th>
                        <th>�����</th>
                        <th>S��, S�</th>
                        <th>���, '</th>
                        <th>t��, ��</th>
                        <th>t��, ��</th>
                        <th>S��, S�</th>
                        <th>S��</th>
                        <th>I��, �</th>
                        <th>����������</th>
                        <th>���</th>
                        <th>���.</th>
                        <th>��� �����.</th>
                        <th>����</th>
                        <th>���</th>
                        <th>����� �������</th>
                      </tr>
                      <xsl:for-each select="DefensesSetpoints/��_��������/���_P/ReversePowerStruct">
                        <tr>
                          <td>������� P <xsl:value-of select="position()"/></td>
                          <td><xsl:value-of select="�����"/></td>
                          <td><xsl:value-of select="�������_������������"/></td>
                          <td><xsl:value-of select="����_������������"/></td>
                          <td><xsl:value-of select="t��"/></td>
                          <td><xsl:value-of select="t��"/></td>
                          <td><xsl:value-of select="�������_��������"/></td>
                          <td>
                            <xsl:for-each select="�������_��������_����_���">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                          <td><xsl:value-of select="���_������������"/></td>
                          <td><xsl:value-of select="����������"/></td>
                          <td><xsl:value-of select="���"/></td>
                          <td><xsl:value-of select="���"/></td>
                          <td><xsl:value-of select="���_�������"/></td>
                          <td><xsl:value-of select="����"/></td>
                          <td><xsl:value-of select="���"/></td>
                          <td>
                            <xsl:for-each select="�����_�������">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </table>
                    <br/><br/><br/>
                  </div>
                </xsl:if>
              </div>

<!--///////////�� ������� "�������" ������� "��"///////////-->
              <div class="block3">
                <div class="sticky3"><h3>��</h3></div>
                <p/>

                <h6>���������� ������� �</h6>
                <table>
                  <tr>
                    <th>�</th>
                    <th>��������</th>
                  </tr>
                  <xsl:for-each select="InputLogicSignal/��">
                    <xsl:if test="position() &lt; 9 ">
                      <tr>
                        <td><xsl:value-of  select="position()"/></td>
                        <td>
                          <xsl:for-each select="�������">
                            <xsl:if test="current() !='���'">
                              <xsl:if test="current() ='��'">
                                �<xsl:value-of select="position()"/>
                              </xsl:if>
                              <xsl:if test="current() ='������'">
                                ^�<xsl:value-of select="position()"/>
                              </xsl:if>
                            </xsl:if>
                          </xsl:for-each>
                        </td>
                      </tr>
                    </xsl:if>
                  </xsl:for-each>
                </table>
                <p/>

                <h6>���������� ������� ���</h6>
                <table>
                  <tr>
                    <th>�</th>
                    <th>��������</th>
                  </tr>
                  <xsl:for-each select="InputLogicSignal/��">
                    <xsl:if test="position() &gt; 8">
                      <xsl:if test="position() &lt; 17">
                        <tr>
                          <td><xsl:value-of  select="position()"/></td>
                          <td>
                            <xsl:for-each select="�������">
                              <xsl:if test="current() !='���'">
                                <xsl:if test="current() ='��'">
                                  �<xsl:value-of select="position()"/>
                                </xsl:if>
                                <xsl:if test="current() ='������'">
                                  ^�<xsl:value-of select="position()"/>
                                </xsl:if>
                              </xsl:if>
                            </xsl:for-each>
                          </td>
                        </tr>
                      </xsl:if>
                    </xsl:if>
                  </xsl:for-each>
                </table>
                <br/><br/><br/>
              </div>

<!--///////////�� ������� "�������" ������� "��� � ���"///////////-->
              <div class="block3">
                <div class="sticky3">
                  <h3>
                    <xsl:if test="$device = 'MR761'">��� � ���</xsl:if>
                    <xsl:if test="$device = 'MR771'">���</xsl:if>
                  </h3>
                </div>
                <p/>

                <xsl:if test="$device = 'MR761'">
                  <h6>���</h6>
                  <xsl:for-each select="DefensesSetpoints/���/���_���">
                    <table>
                      <!--����� ������ � �������-->
                      <tr>
                        <th>��� ���</th>
                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                      <!--� ����������� �� ���� ��� ������� ������ ���������� �������-->
                      <xsl:if test="current() ='������'">
                        <tr>
                          <th>�� ������� (�� �������)</th>
                          <td><xsl:value-of select="../��_�������"/></td>
                        </tr>
                        <tr>
                          <th>�� ����������</th>
                          <td><xsl:value-of select="../��_����������"/></td>
                        </tr>
                        <tr>
                          <th>�� ��������������</th>
                          <td><xsl:value-of select="../��_��������������"/></td>
                        </tr>
                        <tr>
                          <th>�� ������</th>
                          <td><xsl:value-of select="../��_������"/></td>
                        </tr>
                        <tr>
                          <th>������ �����</th>
                          <td><xsl:value-of select="../����"/></td>
                        </tr>
                        <tr>
                          <th>����������</th>
                          <td><xsl:value-of select="../����_����������_���"/></td>
                        </tr>
                        <tr>
                          <th>�����</th>
                          <td><xsl:value-of select="../����_�����_����������_���"/></td>
                        </tr>
                        <tr>
                          <th>��� ���������</th>
                          <td><xsl:value-of select="../����_���_������������"/></td>
                        </tr>
                        <tr>
                          <th>t��, ��</th>
                          <td><xsl:value-of select="../�����_���_������������"/></td>
                        </tr>
                        <tr>
                          <th>�������</th>
                          <td><xsl:value-of select="../����_���_�������"/></td>
                        </tr>
                        <tr>
                          <th>t���, ��</th>
                          <td><xsl:value-of select="../�����_���_�������"/></td>
                        </tr>
                        <tr>
                          <th>t����, ��</th>
                          <td><xsl:value-of select="../��������_����������_�������"/></td>
                        </tr>
                        <tr>
                          <th>�����</th>
                          <td><xsl:value-of select="../�����"/></td>
                        </tr>
                      </xsl:if>

                      <xsl:if test="current() ='����������'">
                        <tr>
                          <th>�����</th>
                          <td><xsl:value-of select="../�����_���"/></td>
                        </tr>
                        <tr>
                          <th>���������� �� ��������.</th>
                          <td>
                            <xsl:for-each select="../����������_���_��_��������">
                              <xsl:if test="current() ='false'">���</xsl:if>
                              <xsl:if test="current() ='true'">��</xsl:if>
                            </xsl:for-each>
                          </td>
                        </tr>
                        <tr>
                          <th>���������� ����</th>
                          <td><xsl:value-of select="../����������_���_����"/></td>
                        </tr>
                        <tr>
                          <th>����</th>
                          <td><xsl:value-of select="../����_���"/></td>
                        </tr>
                        <tr>
                          <th>������</th>
                          <td><xsl:value-of select="../������_���"/></td>
                        </tr>
                        <tr>
                          <th>t��, ��</th>
                          <td><xsl:value-of select="../�����_������������_���"/></td>
                        </tr>
                        <tr>
                          <th>t�����, ��</th>
                          <td><xsl:value-of select="../�����_����������_���"/></td>
                        </tr>
                        <tr>
                          <th>���� U1</th>
                          <td><xsl:value-of select="../����_U1_���"/></td>
                        </tr>
                        <tr>
                          <th>���� U2</th>
                          <td><xsl:value-of select="../����_U2_���"/></td>
                        </tr>
                        <tr>
                          <th>Umax, �</th>
                          <td><xsl:value-of select="../U_max_���"/></td>
                        </tr>
                        <tr>
                          <th>Umin, �</th>
                          <td><xsl:value-of select="../U_min_���"/></td>
                        </tr>
                      </xsl:if>
                    </table>
                  </xsl:for-each>
                </xsl:if>
                <p/>

                <h6>���</h6>
                <table>
                  <tr>
                    <th>�����</th>
                    <td><xsl:value-of select="Apv/�����"/></td>
                  </tr>
                  <tr>
                    <th>���������� �� ����</th>
                    <td>
                      <xsl:for-each select="Apv/����������_��_����">
                        <xsl:if test="current() ='false'">���</xsl:if>
                        <xsl:if test="current() ='true'">��</xsl:if>
                      </xsl:for-each>
                    </td>
                  </tr>
                  <tr>
                    <th>������ ���</th>
                    <td><xsl:value-of select="Apv/����_�������_���"/></td>
                  </tr>
                  <tr>
                    <th>t �������, ��</th>
                    <td><xsl:value-of select="Apv/�����_�������_���"/></td>
                  </tr>
                  <tr>
                    <th>��� �������</th>
                    <td><xsl:value-of select="Apv/���_�������"/></td>
                  </tr>
                  <tr>
                    <th>����������</th>
                    <td><xsl:value-of select="Apv/����_����������_���"/></td>
                  </tr>
                  <tr>
                    <th>t ����., ��</th>
                    <td><xsl:value-of select="Apv/�����_����������_���"/></td>
                  </tr>
                  <tr>
                    <th>t �����., ��</th>
                    <td><xsl:value-of select="Apv/�����_����������_���"/></td>
                  </tr>
                  <tr>
                    <th>1 ����, ��</th>
                    <td><xsl:value-of select="Apv/����1"/></td>
                  </tr>
                  <tr>
                    <th>2 ����, ��</th>
                    <td><xsl:value-of select="Apv/����2"/></td>
                  </tr>
                  <tr>
                    <th>3 ����, ��</th>
                    <td><xsl:value-of select="Apv/����3"/></td>
                  </tr>
                  <tr>
                    <th>4 ����, ��</th>
                    <td><xsl:value-of select="Apv/����4"/></td>
                  </tr>
                  <tr>
                    <th>����������.</th>
                    <td><xsl:value-of select="Apv/������_���_��_�����������������_����������_�����������"/></td>
                  </tr>
                </table>
                <br/><br/><br/>
              </div>

<!--///////////�� ������� "�������" ������� "�� � ����"///////////-->
              <div class="block3">
                <div class="sticky3"><h3>�� � ����</h3></div>
                <p/>

                <h6>����� �������</h6>
                <table>
                  <tr>
                    <th>U1, �</th>
                    <th>U2, �</th>
                    <th>Umin. ���*, �</th>
                    <th>Umin. ���*, �</th>
                    <th>Umax. ���*, �</th>
                    <th>t��, ��</th>
                    <th>t�����, ��</th>
                    <th>t ���, ��</th>
                    <th>����, %</th>
                    <th>f(U1;U2), ����</th>
                    <th>���� ���������� ��</th>
                    <th>���� ����� U1 ���, U2 ����</th>
                    <th>���� ����� U1 ����, U2 ���</th>
                    <th>���� ����� U1 ���, U2 ���</th>
                  </tr>
                  <tr>
                    <td><xsl:value-of select="Sinhronizm/U1"/></td>
                    <td><xsl:value-of select="Sinhronizm/U2"/></td>
                    <td><xsl:value-of select="Sinhronizm/Umin"/></td>
                    <td><xsl:value-of select="Sinhronizm/Umin_���"/></td>
                    <td><xsl:value-of select="Sinhronizm/Umax_���"/></td>
                    <td><xsl:value-of select="Sinhronizm/t��"/></td>
                    <td><xsl:value-of select="Sinhronizm/t�����"/></td>
                    <td><xsl:value-of select="Sinhronizm/t���"/></td>
                    <td><xsl:value-of select="Sinhronizm/����"/></td>
                    <td><xsl:value-of select="Sinhronizm/f"/></td>
                    <td><xsl:value-of select="Sinhronizm/����������"/></td>
                    <td><xsl:value-of select="Sinhronizm/�������_U1_���_U2_����"/></td>
                    <td><xsl:value-of select="Sinhronizm/�������_U1_����_U2_���"/></td>
                    <td><xsl:value-of select="DefensesSetpoints/I2I1/�������_U1_���_U2_���"/></td>
                  </tr>
                </table>
                <p/>

                <h6>������� ������� ���������</h6>
                <table>
                  <tr>
                    <table>
                      <tr>
                        <th>�����</th>
                        <th>���������� �� ������������� ��</th>
                        <th>dUmax., �</th>
                        <th>�������� ����������� (�����. �����)</th>
                        <th>dF, ��</th>
                        <th>dFi, ����</th>
                        <th>����������� ����������� (�������. �����)</th>
                        <th>dF, ��</th>
                      </tr>
                      <tr>
                        <td><xsl:value-of select="Sinhronizm/������_���_�������_���������/�����"/></td>
                        <td>
                          <xsl:for-each select="Sinhronizm/������_���_�������_���������/����.��_������.��">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td><xsl:value-of select="Sinhronizm/������_���_�������_���������/dUmax"/></td>
                        <td>
                          <xsl:for-each select="Sinhronizm/������_���_�������_���������/��������_�����������">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td><xsl:value-of select="Sinhronizm/������_���_�������_���������/����������_���������"/></td>
                        <td><xsl:value-of select="Sinhronizm/������_���_�������_���������/dFi"/></td>
                        <td>
                          <xsl:for-each select="Sinhronizm/������_���_�������_���������/�����������_�����������">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td><xsl:value-of select="Sinhronizm/������_���_�������_���������/������������_���������"/></td>
                      </tr>
                    </table>
                    <table>
                      <tr>���������� ���������</tr>
                      <tr>
                        <th>U1 ���, U2 ����</th>
                        <th>U1 ����, U2 ���</th>
                        <th>U1 ���, U2 ���</th>
                      </tr>
                      <tr>
                        <td><xsl:value-of select="Sinhronizm/������_���_�������_���������/U1���U2����"/></td>
                        <td><xsl:value-of select="Sinhronizm/������_���_�������_���������/U1����U2���"/></td>
                        <td><xsl:value-of select="Sinhronizm/������_���_�������_���������/U1���U2���"/></td>
                      </tr>
                    </table>
                  </tr>
                </table>
                <p/>

                <h6>������� ��������������� ���������</h6>
                <table>
                  <tr>
                    <table>
                      <tr>
                        <th>�����</th>
                        <th>���������� �� ������������� ��</th>
                        <th>dUmax., �</th>
                        <th>�������� ����������� (�����. �����)</th>
                        <th>dF, ��</th>
                        <th>dFi, ����</th>
                        <th>����������� ����������� (�������. �����)</th>
                        <th>dF, ��</th>
                      </tr>
                      <tr>
                        <td><xsl:value-of select="Sinhronizm/������_���_���������������_���������/�����"/></td>
                        <td>
                          <xsl:for-each select="Sinhronizm/������_���_���������������_���������/����.��_������.��">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td><xsl:value-of select="Sinhronizm/������_���_���������������_���������/dUmax"/></td>
                        <td>
                          <xsl:for-each select="Sinhronizm/������_���_���������������_���������/��������_�����������">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td><xsl:value-of select="Sinhronizm/������_���_���������������_���������/����������_���������"/></td>
                        <td><xsl:value-of select="Sinhronizm/������_���_���������������_���������/dFi"/></td>
                        <td>
                          <xsl:for-each select="Sinhronizm/������_���_���������������_���������/�����������_�����������">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td><xsl:value-of select="Sinhronizm/������_���_���������������_���������/������������_���������"/></td>
                      </tr>
                    </table>
                    <table>
                      <tr>���������� ���������</tr>
                      <tr>
                        <th>U1 ���, U2 ����</th>
                        <th>U1 ����, U2 ���</th>
                        <th>U1 ���, U2 ���</th>
                      </tr>
                      <tr>
                        <td><xsl:value-of select="Sinhronizm/������_���_���������������_���������/U1���U2����"/></td>
                        <td><xsl:value-of select="Sinhronizm/������_���_���������������_���������/U1����U2���"/></td>
                        <td><xsl:value-of select="Sinhronizm/������_���_���������������_���������/U1���U2���"/></td>
                      </tr>
                    </table>
                  </tr>
                </table>
                <p>* - ������ ������� ������������ ��� ��������� � ������������ U1 � U2*����/100</p>
                <br/><br/><br/>
              </div>

<!--///////////�� ������� "�������" ������� "�� � ��"///////////-->
              <xsl:if test="$device = 'MR771'">
                <div class="block3">
                  <div class="sticky3"><h3>�� � ��</h3></div>
                  <p/>

<!--///////////////�� ������� "�������" ������� "�� � ��" ������� "�� � �� �� ��"/////////////-->
                  <div class="block4">
                    <div class="sticky4"><h4>�� � �� �� ��</h4></div>
                    <p/>
   
                    <h6>����� ���������</h6>
                    <table>
                      <tr>
                        <th>�����</th>
                        <th>������ ��</th>
                        <xsl:for-each select="Hfl1/������_��">
                          <xsl:if test="current() ='true'">
                            <th>����. ��</th>
                            <th>����. ��</th>
                            <th>�����. ��</th>
                          </xsl:if>
                        </xsl:for-each>
                        <th>������ �N</th>
                        <xsl:for-each select="Hfl1/������_�N">
                          <xsl:if test="current() ='true'">
                            <th>����. �N</th>
                            <th>����. �N</th>
                            <th>�����. �N</th>
                          </xsl:if>
                        </xsl:for-each>
                        <th>��</th>
                        <th>���.</th>
                        <th>����</th>
                        <th>���</th>
                      </tr>
                      <tr>
                        <td><xsl:value-of select="Hfl1/�����"/></td>
                        <td>
                          <xsl:for-each select="Hfl1/������_��">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <xsl:for-each select="Hfl1/������_��">
                          <xsl:if test="current() ='true'">
                            <td><xsl:value-of select="../����_��"/></td>
                            <td><xsl:value-of select="../����_��"/></td>
                            <td><xsl:value-of select="../�����_��"/></td>
                          </xsl:if>
                        </xsl:for-each>
                        <td>
                          <xsl:for-each select="Hfl1/������_�N">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <xsl:for-each select="Hfl1/������_�N">
                          <xsl:if test="current() ='true'">
                            <td><xsl:value-of select="../����_�N"/></td>
                            <td><xsl:value-of select="../����_�N"/></td>
                            <td><xsl:value-of select="../�����_�N"/></td>
                          </xsl:if>
                        </xsl:for-each>
                        <td><xsl:value-of select="Hfl1/TC"/></td>
                        <td><xsl:value-of select="Hfl1/���"/></td>
                        <td><xsl:value-of select="Hfl1/����"/></td>
                        <td><xsl:value-of select="Hfl1/���"/></td>
                      </tr>
                    </table>
                    <p/>

                    <h6>������������ �� � ��</h6>
                    <table>
                      <tr>
                        <th>t��. ��, ��</th>
                        <th>������</th>
                        <th>��������</th>
                        <th>t��.���., ��</th>
                        <th>t���.���., ��</th>
                        <th>t����., ��</th>
                        <th>t���. �� ��, ��</th>
                        <th>����. ���.</th>
                        <th>����. ��</th>
                      </tr>
                      <tr>
                        <td><xsl:value-of select="Hfl1/�����__x0020_��������_��"/></td>
                        <td><xsl:value-of select="Hfl1/������"/></td>
                        <td><xsl:value-of select="Hfl1/��������_��"/></td>
                        <td><xsl:value-of select="Hfl1/�����_�������"/></td>
                        <td><xsl:value-of select="Hfl1/�����_���_��������"/></td>
                        <td><xsl:value-of select="Hfl1/�����_����"/></td>
                        <td><xsl:value-of select="Hfl1/�����_����_��"/></td>
                        <td><xsl:value-of select="Hfl1/����_����"/></td>
                        <td><xsl:value-of select="Hfl1/����_��"/></td>
                      </tr>
                    </table>
                    <p/>

                    <h6>������ ����. ���</h6>
                    <table>
                      <tr>
                        <th>�����</th>
                        <th>t��. ��, ��</th>
                        <th>Umin, �</th>
                        <th>������ ����������</th>
                        <th>����. �� ������. ��</th>
                      </tr>
                      <tr>
                        <td><xsl:value-of select="Hfl1/�����_���"/></td>
                        <td><xsl:value-of select="Hfl1/���_��"/></td>
                        <td><xsl:value-of select="Hfl1/Umin"/></td>
                        <td><xsl:value-of select="Hfl1/����_���"/></td>
                        <td><xsl:value-of select="Hfl1/����_�����"/></td>
                      </tr>
                    </table>
                    <p/>

                    <h6>���������� �� �������</h6>
                    <table>
                      <tr>
                        <th>�����</th>
                        <th>t����., ��</th>
                        <th>t����., ��</th>
                        <th>����.����</th>
                        <th>������ ����������</th>
                      </tr>
                      <tr>
                        <td><xsl:value-of select="Hfl1/�����_�����"/></td>
                        <td><xsl:value-of select="Hfl1/�_�������"/></td>
                        <td><xsl:value-of select="Hfl1/�_����"/></td>
                        <td>
                          <xsl:for-each select="Hfl1/����_����">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td><xsl:value-of select="Hfl1/����_������"/></td>
                      </tr>
                    </table>
                    <br/><br/><br/>
                  </div>

<!--///////////////�� ������� "�������" ������� "�� � ��" ������� "�� � �� �� ����"/////////////-->
                  <div class="block4">
                    <div class="sticky4"><h4>�� � �� �� ����</h4></div>
                    <p/>
                    
                    <h6>����� ���������</h6>
                    <table>
                      <tr>
                        <th>�����</th>
                        <th>����.</th>
                        <th>����.</th>
                        <th>�����.</th>
                        <th>�����������</th>
                        <th>��</th>
                        <th>���.</th>
                        <th>����</th>
                        <th>���</th>
                      </tr>
                      <tr>
                        <td><xsl:value-of select="Hfl2/�����_Tznp"/></td>
                        <td><xsl:value-of select="Hfl2/������"/></td>
                        <td><xsl:value-of select="Hfl2/����������"/></td>
                        <td><xsl:value-of select="Hfl2/������"/></td>
                        <td><xsl:value-of select="Hfl2/�����������"/></td>
                        <td><xsl:value-of select="Hfl2/��"/></td>
                        <td><xsl:value-of select="Hfl2/���"/></td>
                        <td><xsl:value-of select="Hfl2/����"/></td>
                        <td><xsl:value-of select="Hfl2/���"/></td>
                      </tr>
                    </table>
                    <p/>

                    <h6>������������ �� � ��</h6>
                    <table>
                      <tr>
                        <th>t��. ��, ��</th>
                        <th>������</th>
                        <th>��������</th>
                        <th>t��.���., ��</th>
                        <th>t���.���., ��</th>
                        <th>t����., ��</th>
                        <th>t���. �� ��, ��</th>
                        <th>����. ���.</th>
                        <th>����. ��</th>
                      </tr>
                      <tr>
                        <td><xsl:value-of select="Hfl2/�����_��������_��"/></td>
                        <td><xsl:value-of select="Hfl2/������"/></td>
                        <td><xsl:value-of select="Hfl2/��������_��"/></td>
                        <td><xsl:value-of select="Hfl2/�����_�������"/></td>
                        <td><xsl:value-of select="Hfl2/�����_���_��������"/></td>
                        <td><xsl:value-of select="Hfl2/�����_����"/></td>
                        <td><xsl:value-of select="Hfl2/�����_����_��"/></td>
                        <td><xsl:value-of select="Hfl2/����_����"/></td>
                        <td><xsl:value-of select="Hfl2/����_��"/></td>
                      </tr>
                    </table>
                    <p/>

                    <h6>������ ����. ���</h6>
                    <table>
                      <tr>
                        <th>�����</th>
                        <th>t��. ��, ��</th>
                        <th>Umin, �</th>
                        <th>������ ����������</th>
                        <th>����. �� ������. ��</th>
                      </tr>
                      <tr>
                        <td><xsl:value-of select="Hfl2/�����_���"/></td>
                        <td><xsl:value-of select="Hfl2/��_���"/></td>
                        <td><xsl:value-of select="Hfl2/Umin"/></td>
                        <td><xsl:value-of select="Hfl2/����_���"/></td>
                        <td><xsl:value-of select="Hfl2/����_�����"/></td>
                      </tr>
                    </table>
                    <p/>

                    <h6>���������� �� �������</h6>
                    <table>
                      <tr>
                        <th>�����</th>
                        <th>t����., ��</th>
                        <th>t����., ��</th>
                        <th>����.����</th>
                        <th>������ ����������</th>
                      </tr>
                      <tr>
                        <td><xsl:value-of select="Hfl2/�����_������"/></td>
                        <td><xsl:value-of select="Hfl2/�_�������"/></td>
                        <td><xsl:value-of select="Hfl2/�����"/></td>
                        <td>
                          <xsl:for-each select="Hfl2/����_����">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td><xsl:value-of select="Hfl2/����_������"/></td>
                      </tr>
                    </table>
                    <br/><br/><br/>
                  </div>
                </div>
              </xsl:if>
            </div>
            </xsl:if>
          </xsl:for-each>
          <br/><br/><br/>

<!--///////������� "����������� � ����"///////-->
          <div class="block2">
            <div class="sticky2"><h2>����������� � ����</h2></div>
            <p/>

            <h6>�����������</h6>
            <table>
              <tr>
                <th>��������� "���������"</th>
                <th>��������� "��������"</th>
                <th>���� �������������</th>
                <th>���� ����������</th>
                <th>������� ������� ���., ��</th>
                <th>������� ����������</th>
                <th>t�����, ��</th>
                <th>�������� ����� ����������</th>
                <th>�������� ������� ��������� ����������</th>
              </tr>
              <tr>
                <td><xsl:value-of select="��_���/������������_�����������/���������"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/��������"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/������"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/����������"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/�������"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/�������_����������"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/���������"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/��������_�����_���������_����������"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/��������_�����_�������_���������_����������"/></td>
              </tr>
            </table>
            <p/>

            <h6>����</h6>
            <table>
              <tr>
                <th>�� ����</th>
                <th>�� ��</th>
                <th>�� ����</th>
                <th>t����1, ��</th>
                <th>t����2, ��</th>
                <th>I����, I�</th>
                <th>���� �����</th>
                <th>���� ����������</th>
              </tr>
              <tr>
                <td>
                  <xsl:for-each select="��_���/����/��_����">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="��_���/����/��_��">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="��_���/����/��_����">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
                </td>
                <td><xsl:value-of select="��_���/����/t����1"/></td>
                <td><xsl:value-of select="��_���/����/t����2"/></td>
                <td><xsl:value-of select="��_���/����/���_����"/></td>
                <td><xsl:value-of select="��_���/����/����"/></td>
                <td><xsl:value-of select="��_���/����/����������"/></td>
              </tr>
            </table>
            <p/>

            <h6>����������</h6>
            <table>
              <tr>
                <th>���� ��������</th>
                <th>���� ���������</th>
                <th>������� ��������</th>
                <th>������� ���������</th>
                <th>�� ������ ������</th>
                <th>�� �����</th>
                <th>�������</th>
                <th>�� ����</th>
                <th>���������� ����</th>
              </tr>
              <tr>
                <td><xsl:value-of select="��_���/������������_�����������/����_���"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/����_����"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/����_����_��������"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/����_����_���������"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/����"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/����"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/�������"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/����"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/����������_����"/></td>
              </tr>
            </table>
            <br/><br/><br/>
          </div>

<!--///////������� "������� �������"///////-->
          <div class="block2">
            <div class="sticky2"><h2>������� �������</h2></div>
            <p/>

            <h6>������ �������</h6>
            <table>
              <tr>
                <th>������ 1</th>
                <td><xsl:value-of select="��_���/������������_�������_��������/����_���������_������_�������_1"/></td>
              </tr>
              <tr>
                <th>������ 2</th>
                <td><xsl:value-of select="��_���/������������_�������_��������/����_���������_������_�������_2"/></td>
              </tr>
              <tr>
                <th>������ 3</th>
                <td><xsl:value-of select="��_���/������������_�������_��������/����_���������_������_�������_3"/></td>
              </tr>
              <tr>
                <th>������ 4</th>
                <td><xsl:value-of select="��_���/������������_�������_��������/����_���������_������_�������_4"/></td>
              </tr>
              <tr>
                <th>������ 5</th>
                <td><xsl:value-of select="��_���/������������_�������_��������/����_���������_������_�������_5"/></td>
              </tr>
              <tr>
                <th>������ 6</th>
                <td><xsl:value-of select="��_���/������������_�������_��������/����_���������_������_�������_x002B_6"/></td>
              </tr>
            </table>
            <p/>

            <table>
              <tr>
                <th>����� ���������</th>
                <td><xsl:value-of select="��_���/������������_�������_��������/����_�����_���������"/></td>
              </tr>
            </table>
            <p/>

            <h6>��������������� ��������</h6>
            <table>
              <tr>
                <th>�</th>
                <th>���</th>
                <th>��������</th>
              </tr>
              <xsl:for-each select="��_���/������������/���_������������/AntiBounce">
                <tr>
                  <xsl:choose>
                    <xsl:when test="$plant = 'T4N4D42R35' or $plant = 'T4N5D42R35'">
                      <xsl:if test="position() &lt; 41">
                        <td><xsl:value-of select="position()"/></td>
                        <td><xsl:value-of select="@���"/></td>
                        <td><xsl:value-of select="@��������"/></td>
                      </xsl:if>
                    </xsl:when>
                    <xsl:when test="$plant = 'T4N4D74R67' or $plant = 'T4N5D74R67'">
                      <xsl:if test="position() &lt; 73">
                        <td><xsl:value-of select="position()"/></td>
                        <td><xsl:value-of select="@���"/></td>
                        <td><xsl:value-of select="@��������"/></td>
                      </xsl:if>
                    </xsl:when>
                  <xsl:otherwise/>
                  </xsl:choose>
                </tr>
              </xsl:for-each>
            </table>
            <p/>

            <h6>�������</h6>
            <table>
              <tr>
                <th>�</th>
                <th>���</th>
                <th>������</th>
                <th>��</th>
              </tr>
              <xsl:for-each select="��_���/��������/���_�������/Command">
                <tr>
                  <td><xsl:value-of select="position()"/></td>
                  <td><xsl:value-of select="@���"/></td>
                  <td>
                    <xsl:for-each select="@������">
                      <xsl:if test="current() = 'false'">���</xsl:if>
                      <xsl:if test="current() = 'true'">��</xsl:if>
                    </xsl:for-each>
                  </td>
                  <td>
                    <xsl:for-each select="@��">
                      <xsl:if test="current() = 'false'">���</xsl:if>
                      <xsl:if test="current() = 'true'">��</xsl:if>
                    </xsl:for-each>
                  </td>
                </tr>
              </xsl:for-each>
            </table>
            <br/><br/><br/>
          </div>

<!--///////������� "���"///////-->
          <div class="block2">
            <div class="sticky2"><h2>���</h2></div>
            <table>
              <tr>
                <th>�����</th>
                <th>������������</th>
              </tr>
              <xsl:for-each select="��_���/��������_����������_�������/���">
                <xsl:if test="position() &lt; 17">
                  <tr>
                    <td><xsl:value-of select = "position()"/></td>
                    <td>
                      <xsl:for-each select = "�������">
                        <xsl:if test="current() != '���'"><xsl:value-of select = "current()"/>| </xsl:if>
                        <xsl:if test="current() = '���'">���</xsl:if>
                      </xsl:for-each>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:for-each>
            </table>
            <br/><br/><br/>
          </div>

<!--///////������� "�������� �������"///////-->
          <div class="block2">
            <div class="sticky2"><h2>�������� �������</h2></div>
            <p/>

            <h6>�������� ����</h6>
            <table>
              <tr>
                <th>�����</th>
                <th>���</th>
                <th>����</th>
                <th>������</th>
                <th>������, ��</th>
              </tr>
              <xsl:for-each select="��_���/������������_����_x002C_�����������_x002C_��������������/����/���_����/����_����">
                <xsl:choose>
                  <xsl:when test="$plant = 'T4N4D42R35' or $plant = 'T4N5D42R35'">
                    <xsl:if test="position() &lt; 34">
                      <tr>
                        <td><xsl:value-of select="position() + 2"/></td>
                        <td><xsl:value-of select="@���"/></td>
                        <td><xsl:value-of select="@����"/></td>
                        <td><xsl:value-of select="@������"/></td>
                        <td><xsl:value-of select="@�����"/></td>
                      </tr>
                    </xsl:if>
                  </xsl:when>
                  <xsl:when test="$plant = 'T4N4D74R67' or $plant = 'T4N5D74R67'">
                    <xsl:if test="position() &lt; 65">
                      <tr>
                        <td><xsl:value-of select="position() + 2"/></td>
                        <td><xsl:value-of select="@���"/></td>
                        <td><xsl:value-of select="@����"/></td>
                        <td><xsl:value-of select="@������"/></td>
                        <td><xsl:value-of select="@�����"/></td>
                      </tr>
                    </xsl:if>
                  </xsl:when>
                  <xsl:otherwise/>
                </xsl:choose>
              </xsl:for-each>
            </table>
            <p/>

            <h6>����������</h6>
            <table>
              <tr>
                <th>�����</th>
                <th>���</th>
                <th>����</th>
                <th>������ "������"</th>
                <th>����</th>
                <th>������ "�������"</th>
                <th>����� ������</th>
              </tr>
              <xsl:for-each select="��_���/������������_����_x002C_�����������_x002C_��������������/����������/���_����������/����_���������">
                <xsl:choose>
                  <xsl:when test="$plant = 'T4N4D42R35' or $plant = 'T4N5D42R35'">
                    <xsl:if test="position() &lt; 36">
                      <tr>
                        <td><xsl:value-of select="position()"/></td>
                        <td><xsl:value-of select="@���"/></td>
                        <td><xsl:value-of select="@����1"/></td>
                        <td><xsl:value-of select="@������_�������"/></td>
                        <td><xsl:value-of select="@����2"/></td>
                        <td><xsl:value-of select="@������_�������"/></td>
                        <td><xsl:value-of select="@�����_������"/></td>
                      </tr>
                    </xsl:if>
                  </xsl:when>
                  <xsl:when test="$plant = 'T4N4D74R67' or $plant = 'T4N5D74R67'">
                    <xsl:if test="position() &lt; 68">
                      <tr>
                        <td><xsl:value-of select="position()"/></td>
                        <td><xsl:value-of select="@���"/></td>
                        <td><xsl:value-of select="@����1"/></td>
                        <td><xsl:value-of select="@������_�������"/></td>
                        <td><xsl:value-of select="@����2"/></td>
                        <td><xsl:value-of select="@������_�������"/></td>
                        <td><xsl:value-of select="@�����_������"/></td>
                      </tr>
                    </xsl:if>
                  </xsl:when>
                  <xsl:otherwise/>
                </xsl:choose>
              </xsl:for-each>
            </table>
            <p/>

            <h6>���� �������������</h6>
            <table>
              <tr>
                <th>���������� �������������</th>
                <th>����������� �������������</th>
                <th>������������� ��������� U</th>
                <th>������������� ��������� F</th>
                <th>������������� �����������</th>
                <th>������������� ������</th>
                <xsl:if test="$device = 'MR771'">
                  <th>������������� ���</th>
                </xsl:if>
                <th>������, ��</th>
              </tr>
              <tr>
                <td>
                  <xsl:for-each select="��_���/������������_����_x002C_�����������_x002C_��������������/����_�������������/@�������������_1">
                    <xsl:if test="current() = 'false'">���</xsl:if>
                    <xsl:if test="current() = 'true'">��</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="��_���/������������_����_x002C_�����������_x002C_��������������/����_�������������/@�������������_2">
                    <xsl:if test="current() = 'false'">���</xsl:if>
                    <xsl:if test="current() = 'true'">��</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="��_���/������������_����_x002C_�����������_x002C_��������������/����_�������������/@�������������_3">
                    <xsl:if test="current() = 'false'">���</xsl:if>
                    <xsl:if test="current() = 'true'">��</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="��_���/������������_����_x002C_�����������_x002C_��������������/����_�������������/@�������������_4">
                    <xsl:if test="current() = 'false'">���</xsl:if>
                    <xsl:if test="current() = 'true'">��</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="��_���/������������_����_x002C_�����������_x002C_��������������/����_�������������/@�������������_5">
                    <xsl:if test="current() = 'false'">���</xsl:if>
                    <xsl:if test="current() = 'true'">��</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="��_���/������������_����_x002C_�����������_x002C_��������������/����_�������������/@�������������_6">
                    <xsl:if test="current() = 'false'">���</xsl:if>
                    <xsl:if test="current() = 'true'">��</xsl:if>
                  </xsl:for-each>
                </td>
                <xsl:if test="$device = 'MR771'">
                  <td>
                    <xsl:for-each select="��_���/������������_����_x002C_�����������_x002C_��������������/����_�������������/@�������������_7">
                      <xsl:if test="current() = 'false'">���</xsl:if>
                      <xsl:if test="current() = 'true'">��</xsl:if>
                    </xsl:for-each>
                  </td>
                </xsl:if>
                <td>
                  <xsl:value-of select= "��_���/������������_����_x002C_�����������_x002C_��������������/����_�������������/@�������_����_�������������" />
                </td>
              </tr>
            </table>
            <p/>

            <h6>����� �����������</h6>
            <table>
              <xsl:for-each select="��_���/����_��������_������">
                <tr>
                  <th>1. �� ����� � ������ �������</th>
                  <td>
                    <xsl:for-each select="@�����_��_�����_�_������_�������">
                      <xsl:if test="current() = 'true'">��</xsl:if>
                      <xsl:if test="current() = 'false'">���</xsl:if>
                    </xsl:for-each>
                  </td>
                </tr>
                <tr>
                  <th>2. �� ����� � ������ ������</th>
                  <td>
                    <xsl:for-each select="@�����_��_�����_�_������_������">
                      <xsl:if test="current() = 'true'">��</xsl:if>
                      <xsl:if test="current() = 'false'">���</xsl:if>
                    </xsl:for-each>
                  </td>
                </tr>
              </xsl:for-each>
            </table>
            <p/>

            <h6>������ �������</h6>
            <table>
              <tr>
                <th>����. ������ ������� F</th>
                <td>
                  <xsl:for-each select="��_���/����_��������_������/@������_�������">
                    <xsl:if test="current() = 'true'">��</xsl:if>
                    <xsl:if test="current() = 'false'">���</xsl:if>
                  </xsl:for-each>
                </td>
              </tr>
            </table>
            <br/><br/><br/>
          </div>

<!--///////������� "���������� �������"///////-->
          <div class="block2">
            <div class="sticky2"><h2>���������� �������</h2></div>
            <p/>

            <xsl:choose>
              <xsl:when test="$plant = 'T4N4D42R35' or $plant = 'T4N5D42R35'">
                <h6>����������� ����</h6>
                <table>
                  <tr>
                    <th>�����</th>
                    <th>���</th>
                    <th>����</th>
                    <th>������</th>
                    <th>������, ��</th>
                  </tr>
                  <xsl:for-each select="��_���/������������_����_x002C_�����������_x002C_��������������/����/���_����/����_����">
                    <xsl:if test="position() &gt; 33">
                      <tr>
                        <td><xsl:value-of select="position() + 2"/></td>
                        <td><xsl:value-of select="@���"/></td>
                        <td><xsl:value-of select="@����"/></td>
                        <td><xsl:value-of select="@������"/></td>
                        <td><xsl:value-of select="@�����"/></td>
                      </tr>
                    </xsl:if>
                  </xsl:for-each>
                </table>
              </xsl:when>
              <xsl:otherwise/>
            </xsl:choose>
            <p/>

            <h6>��������������� RS-��������</h6>
            <table>
              <tr>
                <th>�</th>
                <th>���</th>
                <th>����</th>
                <th>���� R</th>
                <th>����</th>
                <th>���� S</th>
              </tr>
              <xsl:for-each select="��_���/������������_����_x002C_�����������_x002C_��������������/RS_��������/���_rs_��������/����_�������">
                <tr>
                  <td><xsl:value-of select="position()"/></td>
                  <td><xsl:value-of select="@���"/></td>
                  <td><xsl:value-of select="@����1"/></td>
                  <td><xsl:value-of select="@����_R"/></td>
                  <td><xsl:value-of select="@����2"/></td>
                  <td><xsl:value-of select="@����_S"/></td>
                </tr>
              </xsl:for-each>
            </table>
            <br/><br/><br/>
          </div>

<!--///////������� "�����������"///////-->
          <div class="block2">
            <div class="sticky2"><h2>�����������</h2></div>
            <p/>

            <h6>�����������</h6>
            <table>
              <tr>
                <th>������, ��</th>
                <th>����. ����������, %</th>
                <th>������. ��</th>
                <th>���� ����� ���.</th>
              </tr>
              <tr>
                <td><xsl:value-of select="��_���/������������_�����������/������������_���/����������_�����������"/> - <xsl:value-of select="��_���/������_������������"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/������������_���/����������"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/������������_���/��������"/></td>
                <td><xsl:value-of select="��_���/������������_�����������/����_�������_������������/@�����"/></td>
              </tr>
            </table>
            <p/>

            <h6>��������������� ���������� ������</h6>
            <table>
              <tr>
                <th>�����</th>
                <th>����</th>
                <th>������</th>
              </tr>
              <xsl:for-each select="��_���/������������_�����������/������������_�������/���_������/ChannelWithBase">
                <tr>
                  <td><xsl:value-of select="position()"/></td>
                  <td><xsl:value-of select="@����"/></td>
                  <td><xsl:value-of select="@�����"/></td>
                </tr>
              </xsl:for-each>
            </table>
            <br/><br/><br/>
          </div>

<!--///////������� "���"///////-->
          <div class="block2">
            <div class="sticky2"><h2>���</h2></div>
            <p/>
           
            <table>
              <tr>
                <th/>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <th>��� <xsl:value-of select="position()"/></th>
                </xsl:for-each>
              </tr>
              <tr>
                <th>��������</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������������_���"/></td>
                </xsl:for-each>
              </tr>

              <tr>
                <th>GoIn 1</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '1']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 2</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '2']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 3</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '3']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 4</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '4']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 5</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '5']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 6</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '6']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 7</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '7']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 8</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '8']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 9</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '9']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 10</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '10']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 11</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '11']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 12</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '12']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 13</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '13']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 14</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '14']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 15</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '15']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 16</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '16']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 17</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '17']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 18</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '18']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 19</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '19']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 20</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '20']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 21</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '21']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 22</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '22']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 23</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '23']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 24</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '24']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 25</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '25']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 26</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '26']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 27</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '27']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 28</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '28']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 29</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '29']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 30</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '30']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 31</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '31']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 32</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '32']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 33</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '33']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 34</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '34']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 35</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '35']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 36</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '36']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 37</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '37']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 38</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '38']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 39</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '39']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 40</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '40']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 41</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '41']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 42</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '42']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 43</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '43']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 44</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '44']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 45</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '45']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 46</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '46']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 47</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '47']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 48</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '48']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 49</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '49']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 50</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '50']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 51</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '51']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 52</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '52']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 53</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '53']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 54</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '54']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 55</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '55']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 56</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '56']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 57</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '57']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 58</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '58']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 59</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '59']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 60</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '60']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 61</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '61']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 62</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '62']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 63</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '63']"/></td>
                </xsl:for-each>
              </tr>
              <tr>
                <th>GoIn 64</th>
                <xsl:for-each select="��_���/����/Setpoints/Goose">
                  <td><xsl:value-of select="������_���[position() = '64']"/></td>
                </xsl:for-each>
              </tr>
              </table>
            <br/><br/><br/>
          </div>

<!--///////������� "������������ Ethernet"///////-->
          <div class="block2">
            <div class="sticky2"><h2>������������ Ethernet</h2></div>
            <p/>

            <h6>������������ Ethernet</h6>
            <table>
              <tr>
                <th>IP-�����</th>
              </tr>
              <xsl:for-each select="��_���/IP">
                <tr>
                  <td>
                    <xsl:value-of select="IpHi2"/>.
                    <xsl:value-of select="IpHi1"/>.
                    <xsl:value-of select="IpLo2"/>.
                    <xsl:value-of select="IpLo1"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>
            <p/>
          </div>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
