<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head> 
      </head>
      <body>

<!--/////����������/////-->
        <xsl:variable name="device" select="JOURNAL_TABLE/deviceInfo/deviceType"/>
        <xsl:variable name="vers" select="JOURNAL_TABLE/deviceInfo/deviceVersion"/>
        <xsl:variable name="number" select="JOURNAL_TABLE/deviceInfo/deviceNumber"/>
        <xsl:variable name="plant" select="JOURNAL_TABLE/deviceInfo/devicePlant"/> 
  
        <h1>���������� - <xsl:value-of select="$device"/>, ����� - <xsl:value-of select="$number"/>. ������ �� - <xsl:value-of select="$vers"/>. ���������� ����� - <xsl:value-of select="$plant"/></h1>
        <p/>
        <p/>
      
        <table border="1">
          <tr bgcolor="#c1ced5">            
            <th>�����</th>
            <th>�����</th>
            <th>���������</th>
		        <th>����������� ������</th>
		        <th>�������� ������������</th>
		        <th>�������� ��������� ������������</th>
		        <th>������ �������</th>
            <th>Rab</th>
            <th>Xab</th>
            <th>Rbc</th>
            <th>Xbc</th>
            <th>Rca</th>
            <th>Xca</th>
        <!--<th>������ �-N</th>-->
            <th>Ra</th>
            <th>Xa</th>
            <th>Rb</th>
            <th>Xb</th>
            <th>Rc</th>
            <th>Xc</th>
		        <th>Ia, A</th>
		        <th>Ib, A</th>
		        <th>Ic, A</th>
		        <th>I1, A</th>
		        <th>I2, A</th>
		        <th>3I0, A</th>
		        <th>In, A</th>
		        <th>I�, A</th>
        <!--<th>In1, A</th>-->        
            <th>Q</th>
		        <th>Ua, �</th>
		        <th>Ub, �</th>
		        <th>Uc, �</th>
		        <th>Uab, �</th>
		        <th>Ubc, �</th>
		        <th>Uca, �</th>
		        <th>U1, �</th>
		        <th>U2, �</th>
		        <th>3U0, �</th>
            <th>F, ��</th>
		        <th>Un, �</th>
            <xsl:if test="$plant = 'T4N5D42R35' or $plant = 'T4N5D74R67'">          
              <th>Un1, �</th>
            </xsl:if>
        <!--<th>Un1, �</th>-->
		        <th>��.������� 1-8</th>
		        <th>��.������� 9-16</th>
		        <th>��.������� 17-24</th>
		        <th>��.������� 25-32</th>
		        <th>��.������� 33-40</th>
            <xsl:if test="$plant = 'T4N4D74R67' or $plant = 'T4N5D74R67'">
              <th>��.������� 41-48</th>
              <th>��.������� 49-56</th>
              <th>��.������� 57-64</th>
              <th>��.������� 65-72</th>
            </xsl:if>
        <!--<xsl:if test="$plant = 'T4N4D74R67' or $plant = 'T4N5D74R67'">
              <th>��.������� 41-48</th>
              <th>��.������� 49-56</th>
              <th>��.������� 57-64</th>
              <th>��.������� 65-72</th>
            </xsl:if> ��������� ������ � ������ ���� ��������� ������ 42(D74). ��� D42 - �� ��������-->
          </tr>
          <xsl:for-each select="JOURNAL_TABLE/������_�������">
            <tr align="center">
              <td><xsl:value-of select="_indexColumn"/></td>
              <td><xsl:value-of select="_dateTimeColumn"/></td>
              <td><xsl:value-of select="_messageColumn"/></td>
		          <td><xsl:value-of select="_defenseColumn"/></td>
		          <td><xsl:value-of select="_paramColumn"/></td>
		          <td><xsl:value-of select="_paramValueColumn"/></td>
		          <td><xsl:value-of select="_groupColumn"/></td>
              <td><xsl:value-of select="_rabColumn"/></td>
		          <td><xsl:value-of select="_xabColumn"/></td>
		          <td><xsl:value-of select="_rbcColumn"/></td>
		          <td><xsl:value-of select="_xbcColumn"/></td>
		          <td><xsl:value-of select="_rcaColumn"/></td>
		          <td><xsl:value-of select="_xcaColumn"/></td>      
          <!--<td><xsl:value-of select="_konturFNColumn"/></td>-->
              <td><xsl:value-of select="_raColumn"/></td>
		          <td><xsl:value-of select="_xaColumn"/></td>
		          <td><xsl:value-of select="_rbColumn"/></td>
		          <td><xsl:value-of select="_xbColumn"/></td>
		          <td><xsl:value-of select="_rcColumn"/></td>
		          <td><xsl:value-of select="_xcColumn"/></td>
              <td><xsl:value-of select="_iaColumn"/></td>
              <td><xsl:value-of select="_ibColumn"/></td>
		          <td><xsl:value-of select="_icColumn"/></td>
		          <td><xsl:value-of select="_i1Column"/></td>
		          <td><xsl:value-of select="_i2Column"/></td>
		          <td><xsl:value-of select="_3i0Column"/></td>
              <td><xsl:value-of select="_inColumn"/></td>
		          <td><xsl:value-of select="_irColumn"/></td>
          <!--<td><xsl:value-of select="_in1Column"/></td>-->
              <td><xsl:value-of select="_qColumn"/></td>
		          <td><xsl:value-of select="_uaColumn"/></td>
		          <td><xsl:value-of select="_ubColumn"/></td>
		          <td><xsl:value-of select="_ucColumn"/></td>
		          <td><xsl:value-of select="_uabColumn"/></td>
              <td><xsl:value-of select="_ubcColumn"/></td>
		          <td><xsl:value-of select="_ucaColumn"/></td>
		          <td><xsl:value-of select="_u1Column"/></td>
		          <td><xsl:value-of select="_u2Column"/></td>
		          <td><xsl:value-of select="_3u0Column"/></td>
              <td><xsl:value-of select="_fColumn"/></td>
		          <td><xsl:value-of select="_unColumn"/></td>
              <xsl:if test="$plant = 'T4N5D42R35' or $plant = 'T4N5D74R67'">
                <td><xsl:value-of select="_un1Column"/></td>
              </xsl:if>
          <!--<td><xsl:value-of select="_un1Column"/></td> ���� ������ N5 �� �� �������-->           
		          <td><xsl:value-of select="_d1to8Column"/></td>
		          <td><xsl:value-of select="_d9to16Column"/></td>
		          <td><xsl:value-of select="_d17to24Column"/></td>
		          <td><xsl:value-of select="_d25to32Column"/></td>
		          <td><xsl:value-of select="_d33to40Column"/></td>
              <xsl:if test="$plant = 'T4N4D74R67' or $plant = 'T4N5D74R67'">
                <td><xsl:value-of select="_d41to48Column"/></td>
                <td><xsl:value-of select="_d49to56Column"/></td>
                <td><xsl:value-of select="_d57to64Column"/></td>
                <td><xsl:value-of select="_d65to72Column"/></td>
              </xsl:if>
          <!--<xsl:if test="$plant = 'T4N4D74R67' or $plant = 'T4N5D74R67'">
                <td><xsl:value-of select="_d41to48Column"/></td>
                <td><xsl:value-of select="_d49to56Column"/></td>
                <td><xsl:value-of select="_d57to64Column"/></td>
                <td><xsl:value-of select="_d65to72Column"/></td>
              </xsl:if> ��������� ������ � ������ ���� ��������� ������ 42(D74). ��� D42 - �� ��������-->
            </tr>
          </xsl:for-each>
        </table>	
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
