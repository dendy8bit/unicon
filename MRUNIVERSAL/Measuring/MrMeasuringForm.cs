﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MRUNIVERSAL.Configuration.Structures.MeasuringTransformer;
using BEMN.MRUNIVERSAL.Measuring.Structures;

namespace BEMN.MRUNIVERSAL.Measuring
{
    public partial class MrMeasuringForm : Form, IFormView
    {
        #region [Const]

        private const string RESET_SJ = "Сбросить новую запись в журнале системы";
        private const string RESET_AJ = "Сбросить новую запись в журнале аварий";
        private const string RESET_OSC = "Сбросить новую запись журнала осциллографа";
        private const string RESET_FAULT_SJ = "Сбросить наличие неисправности по ЖС";
        private const string RESET_INDICATION = "Сбросить блинкеры";
        private const string CURRENT_GROUP = "Группа №{0}";
        private const string SWITCH_GROUP_USTAVKI = "Переключить на группу уставок №{0}?";
        private const string RESET_HOT_STATE = "Сбросить состояние тепловой";
        private const string RESET_TN_STATE = "Сбросить неисправности ТН";
        private const string START_OSC = "Запустить осциллограф";
        private const string MEASURE_TRANS_READ_FAIL = "Параметры измерений не были загружены";

        #endregion [Const]

        #region [Private fields]

        private MRUniversal _device;
        private readonly MemoryEntity<AnalogDataBaseStruct> _analogDataBase;
        private readonly MemoryEntity<DiscretDataBaseStruct> _discretDataBase;
        private readonly MemoryEntity<OneWordStruct> _groupUstavki;
        private readonly MemoryEntity<OneWordStruct> _iMinStruct; 
        private readonly MemoryEntity<DateTimeStruct> _dateTime;
        private readonly MemoryEntity<MeasureTransStruct> _measureTrans;
        private readonly int _measTransSize;
        private MeasureTransStruct _currentMeasureTrans;
        private readonly AveragerTime<AnalogDataBaseStruct> _averagerTime;
       // private readonly MemoryEntity<ConfigNetStruct> _configNetStruct;
        private string[] _symbols;
        private bool _loaded;
        private ushort? _numGroup;
        private ushort _iMin;
        private bool _activatefailmes;
        private double _version;


        // Вкладка "Основные сигналы"
        /// <summary>
        /// Индикаторы
        /// </summary>
        private Diod[] _indicators;

        /// <summary>
        /// Дискретные входы
        /// </summary>
        private LedControl[] _discretInputs;

        /// <summary>
        /// Команды
        /// </summary>
        private LedControl[] _commandsMain;

        /// <summary>
        /// Реле
        /// </summary>
        private LedControl[] _relays;

        /// <summary>
        /// Энергонезависимые RS-триггеры
        /// </summary>
        private LedControl[] _rsTriggers;

        /// <summary>
        /// Входные логические сигналы ЛС
        /// </summary>
        private LedControl[] _inputsLogicSignals;

        /// <summary>
        /// Выходные логическе сигналы ВЛС
        /// </summary>
        private LedControl[] _outputLogicSignals;

        /// <summary>
        /// Сигналы СП-логики
        /// </summary>
        private LedControl[] _freeLogic;

        /// <summary>
        /// Сигналы БГС
        /// </summary>
        private LedControl[] _bgs;

        /// <summary>
        /// Сигналы АПВ
        /// </summary>
        private LedControl[] _apv;

        /// <summary>
        /// Сигналы КС и УППН
        /// </summary>
        private LedControl[] _sinchronizm;

        /// <summary>
        /// Общие сигналы
        /// </summary>
        private LedControl[] _commonSignals;

        /// <summary>
        /// УРОВ
        /// </summary>
        private LedControl[] _urov;

        /// <summary>
        /// Сигналы качаний
        /// </summary>
        private LedControl[] _kachanieSignals;

        /// <summary>
        /// 771 - ТУ и ТБ
        /// </summary>
        private LedControl[] _tuAndTb;

        /// <summary>
        /// 761 - АВР Реклоузера
        /// </summary>
        private LedControl[] _avr;



        // Вкладка "Защиты и неисправности"
        /// <summary>
        /// Защиты Z, 761 - 1-6, 771 - 1-10 
        /// </summary>
        private LedControl[] _defencesZ;

        /// <summary>
        /// 761 - Защиты P
        /// </summary>
        private LedControl[] _defencesP;
        
        /// <summary>
        /// Защиты I, Iменьше, Защиты I*, Защиты I2/I1
        /// </summary>
        private LedControl[] _defencesI;
        
        /// <summary>
        /// Защиты U
        /// </summary>
        private LedControl[] _defencesU;

        /// <summary>
        /// Защиты F
        /// </summary>
        private LedControl[] _defencesF;

        /// <summary>
        /// Защиты Q
        /// </summary>
        private LedControl[] _defencesQ;

        /// <summary>
        /// Внешние защиты
        /// </summary>
        private LedControl[] _externalDefenses;
        
        
        /// <summary>
        /// Неисправности
        /// </summary>
        private LedControl[] _faultsMain;
        
        /// <summary>
        /// Неисправности выключателя
        /// </summary>
        private LedControl[] _faultsSwitch;

        /// <summary>
        /// Неисправности измерения U
        /// </summary>
        private LedControl[] _faultsMeasuringU;

        /// <summary>
        /// Неисправности измерения F
        /// </summary>
        private LedControl[] _faultsMeasuringF;
        
        /// <summary>
        /// Ошибки СПЛ
        /// </summary>
        private LedControl[] _splErr;

        /// <summary>
        /// Определение повр. фазы
        /// </summary>
        private LedControl[] _phase;



        // Вкладка "Управляющие сигналы"
        /// <summary>
        /// Управляющие сигналы
        /// </summary>
        private LedControl[] _controlSignals;

        /// <summary>
        /// Команды
        /// </summary>
        private LedControl[] _commands;

        /// <summary>
        /// Управление
        /// </summary>
        private LedControl[] _controls;

        #endregion [Private fields]

        #region [Constructor]
        public MrMeasuringForm()
        {
            this.InitializeComponent();
        }

        public MrMeasuringForm(MRUniversal device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopRead;
            this._version = Common.VersionConverter(this._device.DeviceVersion);

            this._dateTime = device.DateTime;
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DateTimeLoad);

            this._groupUstavki = device.GroupUstavki;
            this._groupUstavki.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.GroupUstavkiLoaded);
            this._groupUstavki.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () => MessageBox.Show("Группа уставок успешно изменена", "Запись группы уставок", MessageBoxButtons.OK, MessageBoxIcon.Information));
            this._groupUstavki.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () => MessageBox.Show("Невозможно изменить группу уставок", "Запись группы уставок", MessageBoxButtons.OK, MessageBoxIcon.Error));
            this._device.Configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._numGroup = null;
                this._groupUstavki.LoadStruct();
            });
            
            this._discretDataBase = device.DiscretDataBase;
            this._discretDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadOk);
            this._discretDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadFail);
            
            this._iMinStruct = device.Imin;
            this._iMinStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () => this._iMin = this._iMinStruct.Value.Word);
            this._iMinStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => this._iMin = 0);

            this._analogDataBase = device.AnalogDataBase;
            this._analogDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadOk);
            this._analogDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadFail);

            this._measureTrans = device.MeasureTrans;
            this._measureTrans.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasureTransReadOk);
            this._measureTrans.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasureTransReadFail);

            this._measTransSize = new MeasureTransStruct().GetStructInfo().FullSize;

            this._averagerTime = new AveragerTime<AnalogDataBaseStruct>(1000);
            this._averagerTime.Tick += this.AveragerTimeTick;

            //if (this._version >= 1.12)
            //{
            //    this._configNetStruct = device.ConfigRsParam;
            //}

            this.Init();
            this.HideShowItems();
        }

        /// <summary>
        /// Инициализация компоннентов
        /// </summary>
        private void Init()
        {
            // Вкладка "Основные сигналы"
            // Индикаторы
            this._indicators = new[]
            {
                this._diod1, this._diod2, this._diod3, this._diod4, this._diod5, this._diod6, this._diod7, this._diod8, this._diod9, this._diod10, this._diod11, this._diod12
            };

            // Дискреты, Реле, Виртуальные реле (создаются динамически). Выводится разное количество, в зависимости от аппаратной части
            this.CreateDeviceType();

            // RS-триггеры
            this._rsTriggers = new[]
            {
                this._rst1, this._rst2, this._rst3, this._rst4, this._rst5, this._rst6, this._rst7, this._rst8,
                this._rst9, this._rst10, this._rst11, this._rst12, this._rst13, this._rst14, this._rst15, this._rst16
            };

            // ЛС
            this._inputsLogicSignals = new[]
            {
                this._ls1, this._ls2, this._ls3, this._ls4, this._ls5, this._ls6, this._ls7, this._ls8,
                this._ls9, this._ls10, this._ls11, this._ls12, this._ls13, this._ls14, this._ls15, this._ls16
            };

            // ВЛС
            this._outputLogicSignals = new[]
            {
                this._vls1, this._vls2, this._vls3, this._vls4, this._vls5, this._vls6, this._vls7, this._vls8,
                this._vls9, this._vls10, this._vls11, this._vls12, this._vls13, this._vls14, this._vls15, this._vls16
            };
            
            // Сигналы СП-логики
            this._freeLogic = new[]
            {
                this._ssl1, this._ssl2, this._ssl3, this._ssl4, this._ssl5, this._ssl6, this._ssl7, this._ssl8,
                this._ssl9, this._ssl10, this._ssl11, this._ssl12, this._ssl13, this._ssl14, this._ssl15,this._ssl16,
                this._ssl17, this._ssl18, this._ssl19, this._ssl20, this._ssl21, this._ssl22, this._ssl23,this._ssl24,
                this._ssl25, this._ssl26, this._ssl27, this._ssl28, this._ssl29, this._ssl30, this._ssl31,this._ssl32,
                this._ssl33, this._ssl34, this._ssl35, this._ssl36, this._ssl37, this._ssl38, this._ssl39,this._ssl40,
                this._ssl41, this._ssl42, this._ssl43, this._ssl44, this._ssl45, this._ssl46, this._ssl47,this._ssl48
            };

            // Сигналы БГС
            this._bgs = new[]
            {
                this._bgs1, this._bgs2, this._bgs3, this._bgs4, this._bgs5, this._bgs6, this._bgs7, this._bgs8,
                this._bgs9, this._bgs10, this._bgs11, this._bgs12, this._bgs13, this._bgs14, this._bgs15, this._bgs16
            };

            // Сигналы АПВ
            this._apv = new[]
            {
                this._puskApv, this._krat1, this._krat2, this._krat3, this._krat4,
                this._turnOnApv, this._zapretApv, this._blockApv, this._readyApv
            };
            
            // Сигналы КС и УППН
            this._sinchronizm = new[]
            {
                this._autoSinchr, this._u1noU2y, this._uyUno, this._unoUno, this._oS, this._uS, this._onKsAndYppN
            };

            // Общие сигналы
            this._commonSignals = new[]
            {
                this._fault, new LedControl(), this._acceleration, this._signalization, this._emergencyShutdown, this._switcherOff, this._switcherOn
            };

            // УРОВ
            this._urov = new[]
            {
                this._urov1, this._urov2, this._urovBlock
            };

            // Сигналы качаний
            this._kachanieSignals = new[]
            {
                this._kachanie, this._outZone, this._inZone
            };

            // 771 - ТУ и ТБ
            this._tuAndTb = new[]
            {
                this._tsDz, this._tsTznp, this._toDz, this._toTznp, this._reverseDz, this._reverseTznp,
                this._kspDz, this._kspTznp, this._echoDz, this._echoTznp
            };

            // 761 - АВР Реклоузера
            this._avr = new[]
            {
                this._avrResOn, this._avrResOff, this._avrBlock, this._avrPuskDugDefense,
                this._avrOutDis, this._avrIn, this._avrOut, this._avrReady, this._avrDeblock
            };
            
            

            // Вкладка "Защиты и неисправности"
            // Защиты Z
            this._defencesZ = new[]
            {
                this._r1IoLed, this._r1Led, this._r2IoLed, this._r2Led, this._r3IoLed, this._r3Led,
                this._r4IoLed, this._r4Led, this._r5IoLed, this._r5Led, this._r6IoLed, this._r6Led,
                this._r7IoLed, this._r7Led, this._r8IoLed, this._r8Led, this._r9IoLed, this._r9Led,
                this._r10IoLed, this._r10Led
            };

            // Защиты P
            this._defencesP = new[]
            {
                this._p1IoLed, this._p1Led, this._p2IoLed, this._p2Led
            };

            // Защиты I, Защита I<, Защиты I*, Защиты I2/I1
            this._defencesI = new[]
            {
                this._i1Io, this._i1, this._i2Io, this._i2, this._i3Io, this._i3, this._i4Io, this._i4, this._i5Io, this._i5,this._i6Io, this._i6, // I
                this._iLessIo, this._iLessSr,                                                                                                      // I<
                this._iS1Io, this._iS1, this._iS2Io,this._iS2,this._iS3Io, this._iS3, this._iS4Io, this._iS4,                                      // I*
                this._iS5Io, this._iS5, this._iS6Io, this._iS6,this._iS7Io,this._iS7,this._iS8Io,this._iS8,
                this._i2i1IoLed, this._i2i1Led                                                                                                     // I2/I1                
            };

            // Защиты U>, Защиты U<, Защиты F>, Защиты F<, Защиты Q
            this._defencesU = new[]
            {
                this._u1IoMoreLed, this._u1MoreLed, this._u2IoMoreLed, this._u2MoreLed, this._u3IoMoreLed, this._u3MoreLed, this._u4IoMoreLed, this._u4MoreLed,
                this._u1IoLessLed, this._u1LessLed, this._u2IoLessLed, this._u2LessLed, this._u3IoLessLed, this._u3LessLed, this._u4IoLessLed, this._u4LessLed
            };

            // Защиты F>, Защиты F<
            this._defencesF = new[]
            {
                this._f1IoMoreLed, this._f1MoreLed, this._f2IoMoreLed, this._f2MoreLed, this._f3IoMoreLed, this._f3MoreLed, this._f4IoMoreLed, this._f4MoreLed,
                this._f1IoLessLed, this._f1LessLed, this._f2IoLessLed, this._f2LessLed, this._f3IoLessLed, this._f3LessLed, this._f4IoLessLed, this._f4LessLed
            };

            // Защиты Q
            this._defencesQ = new[]
            {
                this._q1Led, this._q2Led
            };

            // Внешние защиты
            this._externalDefenses = new[]
            {
                this._vz1, this._vz2, this._vz3, this._vz4, this._vz5, this._vz6, this._vz7, this._vz8,
                this._vz9, this._vz10, this._vz11, this._vz12, this._vz13, this._vz14, this._vz15, this._vz16
            };

            
            // Неисправности (без Реле)
            this._faultsMain = new[]
            {
                this._faultHardware, this._faultSoftware, this._faultMeasuringU, this._faultMeasuringF,
                this._faultSwitchOff, this._faultLogic, new LedControl(), this._faultModule1, this._faultModule2,
                this._faultModule3, this._faultModule4, this._faultModule5, this._faultModule6, this._faultSetpoints,
                this._faultGroupsOfSetpoints, this._faultPassword, this._faultSystemJournal, this._faultAlarmJournal,
                this._faultOsc
            };

            // Неисправности измерения U
            this._faultsMeasuringU = new[]
            {
                this._faultExternalUn1, this._faultTn3U0, this._faultTnU2, this._faultTnObrivFaz, this._faultExternalUabc, 
                this._faultUabc5V, this._faultTN, this._faultExternalUn, this._faultUn5V
            };

            // Неисправности измерения F
            this._faultsMeasuringF = new[]
            {
                this._faultUabc10V, this._frequenceHiger60, this._frequenceLow40, this._faultVoltageChange
            };

            // Определение повр. фазы
            this._phase = new[]
            {
                this._damageA, this._damageB, this._damageC
            };

            // Неисправности выключателя
            this._faultsSwitch = new[]
            {
                this._faultOut, this._faultBlockCon, this._faultManage, this._faultUROV, this._faultSwithON, this._faultDisable1, this._faultDisable2, this._circultMeasuring
            };

            // Ошибки СПЛ
            this._splErr = new[]
            {
                this._crcConst, this._crcRazresh, this._crcProg, this._crcMenu, this._stepProgramExecution
            };



            // Вкладка "Управляющие сигналы"
            // Управляющие сигналы (_releFault выведено во вкладке "Защиты и неисправности" - > "Неисправности")
            this._controlSignals = new[]
            {
                this._newRecordSystemJournal, this._newRecordAlarmJournal, this._newRecordOscJournal,
                this._availabilityFaultSystemJournal, this._releFault, this._switchOff, this._switchOn
            };

            // Команды
            this._commandsMain = new[]
            {
                this._1, this._2, this._3, this._4, this._5, this._6,
                this._7, this._8, this._9, this._10, this._11, this._12,
                this._13, this._14, this._15, this._16, this._17, this._18,
                this._19, this._20, this._21, this._22, this._23, this._24
            };

            // Управление
            this._controls = new[]
            {
                this._control1, this._control2, this._control3, this._control4, this._control5, this._control6,
                this._control7, this._control8, this._control9, this._control10, this._control11, this._control12,
                this._control13, this._control14, this._control15, this._control16, this._control17, this._control18,
                this._control19, this._control20, this._control21, this._control22, this._control23, this._control24,
                this._control25, this._control26, this._control27, this._control28, this._control29, this._control30,
                this._control31, this._control32, this._control33, this._control34
            };

            this._commandComboBox.SelectedIndex = 0;
            this._groupCombo.SelectedIndex = 0;
        }

        #endregion [Constructor]

        #region [MemoryEntity Members]
        /// <summary>
        /// Скрывает/показывает некоторые элементы на форме в зависимости от устройства и аппаратной части
        /// </summary>
        private void HideShowItems()
        {
            switch (_device.DeviceType)
            {
                case "MR771":
                    // Вкладка "Основные сигналы" -> "АВР Реклоузера"
                    this._avrReclouserGroupBox.Visible = false;

                    // Вкладка "Защиты и неисправноти" -> "Защиты P"
                    this._defensesPGroupBox.Visible = false;
                    break;

                case "MR761":
                    // Вкладка "Основные сигналы" -> "ТУ и ТБ"
                    this._tuAndTbGroupBox.Visible = false;

                    // Для красоты отображения
                    _avrReclouserGroupBox.Location = new Point(726, 496);

                    // Вкладка "Защиты и неисправноти" -> "Защиты Z"
                    this._r7IoLed.Visible = false;
                    this._r7Led.Visible = false;
                    this._defensesZGroupBox.Size = new Size(84, 146);

                    // Вкладка "Защиты и неисправноти" -> "Защиты P"
                    this._defensesPGroupBox.Location = new Point(8, 160);
                    break;

                default:
                    break;
            }

            switch (_device.DevicePlant)
            {
                case "T4N4D42R35":
                case "T4N4D74R67":
                    // Вкладка "Аналоговая БД" -> Напряжение и частота -> Un1
                    this._un1TextBox.Visible = false;
                    this._un1Label.Visible = false;
                    this._cUn1.Visible = false;

                    // Для красивого отображения
                    this._fLabel.Location = new Point(12, 132);
                    this._fTextBox.Location = new Point(39, 129);
                    break;

                default:
                    break;
            }        
        }

        /// <summary>
        /// Метод динамически выводит указанное количество дискретов, реле, виртуальных реле, в зависимости от аппаратной части устройства
        /// </summary>
        private void CreateDeviceType()
        {
            switch (_device.Info.Plant)
            {
                case "T4N4D42R35":
                    this.CreateModules(this._relayGroupBox, new[] { 16, 16, 2 }, "Р", out this._relays, new[] { 16, 16 }, true);         // Реле и виртуальные реле
                    this.CreateModules(this._discretsGroupBox, new[] { 16, 16, 10 }, "Д-40, K-2", out this._discretInputs, new[] { 0 }); // Дискретыне входы
                    break;
                case "T4N4D74R67":
                    this.CreateModules(this._relayGroupBox, new[] { 16, 16, 16, 16, 2 }, "Р", out this._relays, new[] { 16, 1 });        // Реле и виртуальные реле
                    this.CreateModules(this._discretsGroupBox, new[] { 16, 16, 16, 16, 8 }, "Д", out this._discretInputs, new[] { 0 });  // Дискретыне входы
                    break;
                case "T4N5D42R35":
                    this.CreateModules(this._relayGroupBox, new[] { 16, 16, 2 }, "Р", out this._relays, new[] { 16, 16 }, true);         // Реле и виртуальные реле
                    this.CreateModules(this._discretsGroupBox, new[] { 16, 16, 10 }, "Д-40, К-2", out this._discretInputs, new[] { 0 }); // Дискретыне входы
                    break;
                case "T4N5D74R67":
                    this.CreateModules(this._relayGroupBox, new[] { 16, 16, 16, 16, 2 }, "Р", out this._relays, new[] { 16, 1 });        // Реле и виртуальные реле
                    this.CreateModules(this._discretsGroupBox, new[] { 16, 16, 16, 16, 8 }, "Д", out this._discretInputs, new[] { 0 });  // Дискретыне входы
                    break;
                default:
                    this.CreateModules(this._relayGroupBox, new[] { 0 }, "Р", out this._relays, new[] { 0 }, true);                      // Реле и виртуальные реле
                    this.CreateModules(this._discretsGroupBox, new[] { 0 }, "Д", out this._discretInputs, new[] { 0 });                  // Дискретыне входы
                    break;
            }
        }


        /// <summary>
        /// Метод динамически добавляет дискреты/реле/виртуальные реле
        /// </summary>
        private void CreateModules(GroupBox mainContainer, int[] counts, string character, out LedControl[] mass, int[] countVirtRele, bool virtRele = false)
        {
            List<LedControl> list = new List<LedControl>(); // список всех led-ов
            int counter = 1; // счётчик для вывода номеров сигналов для одного вида. Пример: "Р"

            int k = 0;  // счётчик для вывода номеров сигналов для нескольких видов. Пример: "Д-40, К-2"
            string resultCharacter; // название сигнала (для нескольких видов)
            int number; // порядковый номер сигнала (для нескольких видов)
            List<string> resultList = new List<string>(); // список всех сигналов (для нескольких видов)

            try
            {
                string[] characters = character.Split(',');
                if (!characters[0].Equals(character))
                {
                    foreach (var newCharacter in characters)
                    {
                        string[] temp = newCharacter.Split('-');
                        number = int.Parse(temp[1]);
                        resultCharacter = temp[0];
                        for (int i = 0; i < number; i++)
                        {
                            resultList.Add($"{resultCharacter}{i + 1}");
                        }
                    }
                }
                for (int module = 0; module < counts.Length; module++)
                {
                    Panel moduleGroup = new Panel
                    {
                        Size = new Size(48, 262),
                        Location = new Point(6 + 54 * module, 11),
                        Text = string.Empty
                    };
                    for (int relay = 0; relay < counts[module]; relay++)
                    {
                        LedControl relayLed = new LedControl { Location = new Point(6, 15 + 15 * relay + 3) };
                        list.Add(relayLed);
                        moduleGroup.Controls.Add(relayLed);

                        Label label = new Label { Location = new Point(25, 15 + 15 * relay + 3), Text = characters[0].Equals(character) ? character + counter++ : resultList[k], AutoSize = true };
                        moduleGroup.Controls.Add(label);
                        k++;
                    }
                    mainContainer.Controls.Add(moduleGroup);
                }

                if (virtRele)
                {
                    for (int module = 0; module < countVirtRele.Length; module++)
                    {
                        Panel moduleGroup = new Panel
                        {
                            Size = new Size(48, 262),
                            Location = new Point(6 + 54 * module, 11),
                            Text = string.Empty
                        };
                        for (int relay = 0; relay < countVirtRele[module]; relay++)
                        {
                            LedControl relayLed = new LedControl { Location = new Point(6, 15 + 15 * relay) };
                            list.Add(relayLed);
                            Label label = new Label { Location = new Point(25, 15 + 15 * relay), Text = character + counter++, AutoSize = true };
                            moduleGroup.Controls.Add(relayLed);
                            moduleGroup.Controls.Add(label);
                        }
                        _virtualReleGroupBox.Controls.Add(moduleGroup);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Неверно заданы параметры в методе CreateModules() класса MrMeasuringForm устройства MrUniversal.");
            }
            mass = list.ToArray();
        }

        private void GroupUstavkiLoaded()
        {
            if (this._numGroup != this._groupUstavki.Value.Word)
            {
                this._numGroup = this._groupUstavki.Value.Word;
                this._currenGroupLabel.Text = string.Format(CURRENT_GROUP, this._numGroup + 1);
                ushort measureTransAddr = this._device.GetStartAddrMeasTrans((int) this._numGroup);
                this._measureTrans.RemoveStructQueries();
                this._measureTrans.Slots[0] = new Device.slot(measureTransAddr, (ushort) (measureTransAddr + this._measTransSize));
                this._measureTrans.LoadStruct();
            }
        }

        private void AnalogBdReadOk()
        {
            this._averagerTime.Add(this._analogDataBase.Value);
        }

        private void AnalogBdReadFail()
        {
            const string errorValue = "0";
            
            // Вкладка "Аналоговая БД"
            // Токи
            // 1-ый текстбокс
            this._iaTextBox.Text = errorValue;
            this._ibTextBox.Text = errorValue;
            this._icTextBox.Text = errorValue;
            this._inTextBox.Text = errorValue;
            this._i1TextBox.Text = errorValue;
            this._i2TextBox.Text = errorValue;
            this._i30TextBox.Text = errorValue;
            this._igTextBox.Text = errorValue;

            // 2-ой текстбокс (углы)
            this._cIa.Text = errorValue;
            this._cIb.Text = errorValue;
            this._cIc.Text = errorValue;
            this._cIn.Text = errorValue;
            this._cI1.Text = errorValue;
            this._cI2.Text = errorValue;
            this._cI0.Text = errorValue;


            // Напряжения
            // 1-ый текстбокс
            this._uaTextBox.Text = errorValue;
            this._ubTextBox.Text = errorValue;
            this._ucTextBox.Text = errorValue;
            this._unTextBox.Text = errorValue;
            this._un1TextBox.Text = errorValue;
            this._fTextBox.Text = errorValue;

            this._uabTextBox.Text = errorValue;
            this._ubcTextBox.Text = errorValue;
            this._ucaTextBox.Text = errorValue;
            this._u1TextBox.Text = errorValue;
            this._u2TextBox.Text = errorValue;
            this._u30TextBox.Text = errorValue;

            // 2-ой текстбокс
            this._cUa.Text = errorValue;
            this._cUb.Text = errorValue;
            this._cUc.Text = errorValue;
            this._cUn.Text = errorValue;
            this._cUn1.Text = errorValue;

            this._cUab.Text = errorValue;
            this._cUbc.Text = errorValue;
            this._cUca.Text = errorValue;
            this._cU1.Text = errorValue;
            this._cU2.Text = errorValue;
            this._cU0.Text = errorValue;


            // Мощности
            this._pTextBox.Text = errorValue;
            this._qTextBox.Text = errorValue;
            this._cosfTextBox.Text = errorValue;


            // Сопротивления, Ом вт.
            // Межфазные
            this._zabBox.Text = errorValue;
            this._zbcBox.Text = errorValue;
            this._zcaBox.Text = errorValue;
            // Фазные N1
            this._za1Box.Text = errorValue;
            this._zb1Box.Text = errorValue;
            this._zc1Box.Text = errorValue;
            // Фазные N2
            this._za2Box.Text = errorValue;
            this._zb2Box.Text = errorValue;
            this._zc2Box.Text = errorValue;
            // Фазные N3
            this._za3Box.Text = errorValue;
            this._zb3Box.Text = errorValue;
            this._zc3Box.Text = errorValue;
            // Фазные N4
            this._za4Box.Text = errorValue;
            this._zb4Box.Text = errorValue;
            this._zc4Box.Text = errorValue;
            // Фазные N5
            this._za5Box.Text = errorValue;
            this._zb5Box.Text = errorValue;
            this._zc5Box.Text = errorValue;


            // Сопротивления, Ом перв.
            // Межфазные
            this._iZabBox.Text = errorValue;
            this._iZbcBox.Text = errorValue;
            this._iZcaBox.Text = errorValue;
            // Фазные N1
            this._iZa1Box.Text = errorValue;
            this._iZb1Box.Text = errorValue;
            this._iZc1Box.Text = errorValue;
            // Фазные N2
            this._iZa2Box.Text = errorValue;
            this._iZb2Box.Text = errorValue;
            this._iZc2Box.Text = errorValue;
            // Фазные N3
            this._iZa3Box.Text = errorValue;
            this._iZb3Box.Text = errorValue;
            this._iZc3Box.Text = errorValue;
            // Фазные N4
            this._iZa4Box.Text = errorValue;
            this._iZb4Box.Text = errorValue;
            this._iZc4Box.Text = errorValue;
            // Фазные N5
            this._iZa5Box.Text = errorValue;
            this._iZb5Box.Text = errorValue;
            this._iZc5Box.Text = errorValue;


            // Направления Z
            this.AB.Text = errorValue;
            this.BC.Text = errorValue;
            this.CA.Text = errorValue;

            this.A1.Text = errorValue;
            this.B1.Text = errorValue;
            this.C1.Text = errorValue;
            
            this.A2.Text = errorValue;
            this.B2.Text = errorValue;
            this.C2.Text = errorValue;

            this.A3.Text = errorValue;
            this.B3.Text = errorValue;
            this.C3.Text = errorValue;

            this.A4.Text = errorValue;
            this.B4.Text = errorValue;
            this.C4.Text = errorValue;

            this.A5.Text = errorValue;
            this.B5.Text = errorValue;
            this.C5.Text = errorValue;


            // Параметры RS-485
            // this._addressRs485TextBox.Text = errorValue;
            // this._speedRs485TextBox.Text = errorValue;
            // this._pauseRs485TextBox.Text = errorValue;


            // Тепловая модель
            // this._qpTextBox.Text = errorValue;


            // Контроль синхронизма
            // this._dU.Text = errorValue;
            // this._dFi.Text = errorValue;
            // this._dF.Text = errorValue;
        }

        private void AveragerTimeTick()
        {
            try
            {
                string[] symbols;
                if (this._symbols != null || this._symbols.Length < 13)
                {
                    symbols = this._symbols; // 27
                }
                else
                {
                    symbols = new string[12];
                    for (int i = 0; i < 13; i++)
                    {
                        symbols[i] = "!";
                    }
                }


                // Вкладка "Аналоговая БД"
                // Токи
                // 1-ый текстбокс
                this._iaTextBox.Text = symbols[0] + this._analogDataBase.Value.GetIa(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._ibTextBox.Text = symbols[1] + this._analogDataBase.Value.GetIb(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._icTextBox.Text = symbols[2] + this._analogDataBase.Value.GetIc(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._inTextBox.Text = symbols[5] + this._analogDataBase.Value.GetIn(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._i1TextBox.Text = this._analogDataBase.Value.GetI1(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._i2TextBox.Text = symbols[4] + this._analogDataBase.Value.GetI2(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._i30TextBox.Text = symbols[3] + this._analogDataBase.Value.Get3I0(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._igTextBox.Text = this._analogDataBase.Value.GetIg(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                
                // 2-ой текстбокс (углы)
                this._cIa.Text = this._analogDataBase.Value.CornerIa;
                this._cIb.Text = this._analogDataBase.Value.CornerIb;
                this._cIc.Text = this._analogDataBase.Value.CornerIc;
                this._cIn.Text = this._analogDataBase.Value.CornerIn;
                this._cI1.Text = this._analogDataBase.Value.CornerI1;
                this._cI2.Text = this._analogDataBase.Value.CornerI2;
                this._cI0.Text = this._analogDataBase.Value.CornerI0;
                

                // Напряжения
                // 1-ый текстбокс
                this._uaTextBox.Text = this._analogDataBase.Value.GetUa(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._ubTextBox.Text = this._analogDataBase.Value.GetUb(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._ucTextBox.Text = this._analogDataBase.Value.GetUc(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._unTextBox.Text = this._analogDataBase.Value.GetUn(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._un1TextBox.Text = this._analogDataBase.Value.GetUn1(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._fTextBox.Text = this._analogDataBase.Value.GetF(this._averagerTime.ValueList);

                this._uabTextBox.Text = this._analogDataBase.Value.GetUab(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._ubcTextBox.Text = this._analogDataBase.Value.GetUbc(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._ucaTextBox.Text = this._analogDataBase.Value.GetUca(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._u1TextBox.Text = this._analogDataBase.Value.GetU1(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._u2TextBox.Text = this._analogDataBase.Value.GetU2(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._u30TextBox.Text = this._analogDataBase.Value.Get3U0(this._averagerTime.ValueList, this._currentMeasureTrans);
               
                // 2-ой текстбокс
                this._cUa.Text = this._analogDataBase.Value.CornerUa;
                this._cUb.Text = this._analogDataBase.Value.CornerUb;
                this._cUc.Text = this._analogDataBase.Value.CornerUc;
                this._cUn.Text = this._analogDataBase.Value.CornerUn;
                this._cUn1.Text = this._analogDataBase.Value.CornerUn1;

                this._cUab.Text = this._analogDataBase.Value.CornerUab;
                this._cUbc.Text = this._analogDataBase.Value.CornerUbc;
                this._cUca.Text = this._analogDataBase.Value.CornerUca;
                this._cU1.Text = this._analogDataBase.Value.CornerU1;
                this._cU2.Text = this._analogDataBase.Value.CornerU2;
                this._cU0.Text = this._analogDataBase.Value.CornerU0;


                // Мощности
                this._pTextBox.Text = this._analogDataBase.Value.GetP(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._qTextBox.Text = this._analogDataBase.Value.GetQ(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._cosfTextBox.Text = this._analogDataBase.Value.GetCosF(this._averagerTime.ValueList);
                

                // Сопротивления, Ом вт.
                // Межфазные
                this._zabBox.Text = this._analogDataBase.Value.GetVtorZab(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);   
                this._zbcBox.Text = this._analogDataBase.Value.GetVtorZbc(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zcaBox.Text = this._analogDataBase.Value.GetVtorZca(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                // Фазные N1
                this._za1Box.Text = this._analogDataBase.Value.GetVtorZa1(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zb1Box.Text = this._analogDataBase.Value.GetVtorZb1(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zc1Box.Text = this._analogDataBase.Value.GetVtorZc1(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                // Фазные N2
                this._za2Box.Text = this._analogDataBase.Value.GetVtorZa2(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zb2Box.Text = this._analogDataBase.Value.GetVtorZb2(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zc2Box.Text = this._analogDataBase.Value.GetVtorZc2(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                // Фазные N3
                this._za3Box.Text = this._analogDataBase.Value.GetVtorZa3(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zb3Box.Text = this._analogDataBase.Value.GetVtorZb3(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zc3Box.Text = this._analogDataBase.Value.GetVtorZc3(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                // Фазные N4
                this._za4Box.Text = this._analogDataBase.Value.GetVtorZa4(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zb4Box.Text = this._analogDataBase.Value.GetVtorZb4(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zc4Box.Text = this._analogDataBase.Value.GetVtorZc4(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                // Фазные N5
                this._za5Box.Text = this._analogDataBase.Value.GetVtorZa5(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zb5Box.Text = this._analogDataBase.Value.GetVtorZb5(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zc5Box.Text = this._analogDataBase.Value.GetVtorZc5(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);


                // Сопротивления, Ом перв.
                // Межфазные
                this._iZabBox.Text = this._analogDataBase.Value.GetZab(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZbcBox.Text = this._analogDataBase.Value.GetZbc(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZcaBox.Text = this._analogDataBase.Value.GetZca(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                // Фазные N1
                this._iZa1Box.Text = this._analogDataBase.Value.GetZa1(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZb1Box.Text = this._analogDataBase.Value.GetZb1(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZc1Box.Text = this._analogDataBase.Value.GetZc1(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                // Фазные N2
                this._iZa2Box.Text = this._analogDataBase.Value.GetZa2(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZb2Box.Text = this._analogDataBase.Value.GetZb2(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZc2Box.Text = this._analogDataBase.Value.GetZc2(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                // Фазные N3
                this._iZa3Box.Text = this._analogDataBase.Value.GetZa3(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZb3Box.Text = this._analogDataBase.Value.GetZb3(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZc3Box.Text = this._analogDataBase.Value.GetZc3(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                // Фазные N4
                this._iZa4Box.Text = this._analogDataBase.Value.GetZa4(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZb4Box.Text = this._analogDataBase.Value.GetZb4(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZc4Box.Text = this._analogDataBase.Value.GetZc4(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                // Фазные N5
                this._iZa5Box.Text = this._analogDataBase.Value.GetZa5(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZb5Box.Text = this._analogDataBase.Value.GetZb5(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZc5Box.Text = this._analogDataBase.Value.GetZc5(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);


                // Направления Z
                this.AB.Text = symbols[9];
                this.BC.Text = symbols[10];
                this.CA.Text = symbols[11];

                this.A1.Text = symbols[12];
                this.B1.Text = symbols[13];
                this.C1.Text = symbols[14];

                this.A2.Text = symbols[15];
                this.B2.Text = symbols[16];
                this.C2.Text = symbols[17];

                this.A3.Text = symbols[18];
                this.B3.Text = symbols[19];
                this.C3.Text = symbols[20];

                this.A4.Text = symbols[21];
                this.B4.Text = symbols[22];
                this.C4.Text = symbols[23];

                this.A5.Text = symbols[24];
                this.B5.Text = symbols[25];
                this.C5.Text = symbols[26];


                // Параметры RS-485
                // this._addressRs485TextBox.Text = this._configNetStruct.Value.Address.ToString();
                // this._speedRs485TextBox.Text = this._configNetStruct.Value.Speed;
                // this._pauseRs485TextBox.Text = this._configNetStruct.Value.Pause + @" мс";


                // Тепловая модель
                // this._qpTextBox.Text = this._analogDataBase.Value.GetQt(this._averagerTime.ValueList);


                // Контроль синхронизма
                // this._dU.Text = this._analogDataBase.Value.GetdU(this._averagerTime.ValueList);
                // this._dFi.Text = this._analogDataBase.Value.GetdFi(this._averagerTime.ValueList);
                // this._dF.Text = this._analogDataBase.Value.GetdF(this._averagerTime.ValueList);
            }
            catch (Exception){}
        }

        private void MeasureTransReadFail()
        {
            if (this._activatefailmes)
            {
                this._activatefailmes = false;
                MessageBox.Show(MEASURE_TRANS_READ_FAIL);
            }
        }

        private void MeasureTransReadOk()
        {
            if (!this._activatefailmes) this._activatefailmes = true;
            this._currentMeasureTrans = this._measureTrans.Value;
            if (this._loaded) return;
            this._loaded = true;
            this._analogDataBase.LoadStructCycle();
            //this._configNetStruct.LoadStructCycle();
        }
        
        /// <summary>
        /// Ошибка чтения дискретной базы данных ПЕРЕДЕЛАТЬ
        /// </summary>
        private void DiscretBdReadFail()
        {
            // Вкладка "Основные сигналы"
            foreach (var indicator in this._indicators)        
            {
                indicator.TurnOff();                           // Индикаторы
            }

            this._diodJs.TurnOff();                            // Индикатор ЖС
            this._diodJa.TurnOff();                            // Индикатор ЖА

            LedManager.TurnOffLeds(this._discretInputs);       // Дискретные входы
            LedManager.TurnOffLeds(this._relays);              // Реле и Виртуальные реле
            LedManager.TurnOffLeds(this._rsTriggers);          // Энергонезависимые RS-триггеры 
            LedManager.TurnOffLeds(this._inputsLogicSignals);  // Входные логические сигналы ЛС 
            LedManager.TurnOffLeds(this._outputLogicSignals);  // Выходные логические сигналы ВЛС
            LedManager.TurnOffLeds(this._freeLogic);           // Сигналы СП-логики
            LedManager.TurnOffLeds(this._bgs);                 // Cигналы БГС
            LedManager.TurnOffLeds(this._apv);                 // Cигналы АПВ
            LedManager.TurnOffLeds(this._sinchronizm);         // Сигналы КС и УППН
            LedManager.TurnOffLeds(this._commonSignals);       // Общие сигналы
            LedManager.TurnOffLeds(this._urov);                // УРОВ
            LedManager.TurnOffLeds(this._kachanieSignals);     // Сигналы качаний
            LedManager.TurnOffLeds(this._tuAndTb);             // 771 - ТУ и ТБ
            LedManager.TurnOffLeds(this._avr);                 // 761 - АВР Реклоузера 


            // Вкладка "Защиты и неисправности"
            // Защиты
            LedManager.TurnOffLeds(this._defencesZ);           // Защиты Z
            LedManager.TurnOffLeds(this._defencesP);           // Защиты P 
            LedManager.TurnOffLeds(this._defencesI);           // Защиты I
            LedManager.TurnOffLeds(this._defencesU);           // Защиты U
            LedManager.TurnOffLeds(this._defencesF);           // Защиты F
            LedManager.TurnOffLeds(this._defencesQ);           // Защиты Q
            LedManager.TurnOffLeds(this._externalDefenses);    // Внешние защиты

            // Неисправности
            LedManager.TurnOffLeds(this._faultsMain);          // Неисправности
            LedManager.TurnOffLeds(this._faultsMeasuringU);    // Неисправности измерения U
            LedManager.TurnOffLeds(this._faultsMeasuringF);    // Неисправности измерения F
            LedManager.TurnOffLeds(this._phase);               // Определение повр. фазы
            LedManager.TurnOffLeds(this._faultsSwitch);        // Неисправности выключателя
            LedManager.TurnOffLeds(this._splErr);              // Ошибки СПЛ


            // Вкладка "Управляющие сигналы"
            LedManager.TurnOffLeds(this._controlSignals);      // Управляющие сигналы (без Неисправности ТН)
            this._faultTNcopy.State = this._faultTN.State;     // продублированный Неисправность ТН
            LedManager.TurnOffLeds(this._commandsMain);        // Команды
            LedManager.TurnOffLeds(this._controls);            // Управление
            this._logicState.State = LedState.Off;             // Свободно программируемая логика
        }

        /// <summary>
        /// Прочитана дискретная база данных
        /// </summary>
        private void DiscretBdReadOk()
        {
            DiscretDataBaseStruct discrets = this._discretDataBase.Value;
            this._symbols = discrets.Symbols;

            // Вкладка "Основные сигналы"
            for (int i = 0; i < this._indicators.Length; i++)
            {
                this._indicators[i].SetState(discrets.Indicators[i]);                   // Индикаторы
            }

            this._diodJs.OrangeDiod(discrets.IndicatorsJsJa[0]);                        // Индикатор ЖС
            this._diodJa.RedDiod(discrets.IndicatorsJsJa[1]);                           // Индикатор ЖА
            
            LedManager.SetLeds(this._discretInputs, discrets.DiscretInputs);            // Дискретные входы
            LedManager.SetLeds(this._relays, discrets.Relays);                          // Реле и Виртуальные реле
            LedManager.SetLeds(this._rsTriggers, discrets.RsTriggers);                  // Энергонезависимые RS-триггеры
            LedManager.SetLeds(this._inputsLogicSignals, discrets.InputsLogicSignals);  // Входные логические сигналы ЛС
            LedManager.SetLeds(this._outputLogicSignals, discrets.OutputLogicSignals);  // Выходные логические сигналы ВЛС
            LedManager.SetLeds(this._freeLogic, discrets.FreeLogic);                    // Сигналы СП-логики
            LedManager.SetLeds(this._bgs, discrets.Bgs);                                // Cигналы БГС
            LedManager.SetLeds(this._apv, discrets.Apv);                                // Cигналы АПВ
            LedManager.SetLeds(this._sinchronizm, discrets.SignalsKsYppn);              // Сигналы КС и УППН
            LedManager.SetLeds(this._commonSignals, discrets.CommonSignals);            // Общие сигналы
            LedManager.SetLeds(this._urov, discrets.Urov);                              // УРОВ
            LedManager.SetLeds(this._kachanieSignals, discrets.Kachanie);               // Сигналы качаний
            LedManager.SetLeds(this._tuAndTb, discrets.TuAndTb);                        // 771 - ТУ и ТБ
            LedManager.SetLeds(this._avr, discrets.Avr);                                // 761 - АВР Реклоузера


            // Вкладка "Защиты и неисправности"
            // Защиты
            LedManager.SetLeds(this._defencesZ, discrets.DefencesZ);                    // Защиты Z
            LedManager.SetLeds(this._defencesP, discrets.DefencesP);                    // Защиты P
            LedManager.SetLeds(this._defencesI, discrets.DefencesI);                    // Защиты I
            LedManager.SetLeds(this._defencesU, discrets.DefencesU);                    // Защиты U
            LedManager.SetLeds(this._defencesF, discrets.DefencesF);                    // Защиты F
            LedManager.SetLeds(this._defencesQ, discrets.DefencesQ);                    // Защиты Q
            LedManager.SetLeds(this._externalDefenses, discrets.ExternalDefenses);      // Внешние защиты

            // Неисправности
            LedManager.SetLeds(this._faultsMain, discrets.Faults);                      // Неисправности
            LedManager.SetLeds(this._faultsMeasuringU, discrets.FaultsMeasuringU);      // Неисправности измерения U
            LedManager.SetLeds(this._faultsMeasuringF, discrets.FaultsMeasuringF);      // Неисправности измерения F
            LedManager.SetLeds(this._phase, discrets.Phase);                            // Определение повр. фазы
            LedManager.SetLeds(this._faultsSwitch, discrets.FaultsSwitcher);            // Неисправности выключателя
            LedManager.SetLeds(this._splErr, discrets.FaultLogicErr);                   // Ошибки СПЛ


            // Вкладка "Управляющие сигналы"
            LedManager.SetLeds(this._controlSignals, discrets.ControlSignals);          // Управляющие сигналы (без Неисправности ТН)
            this._faultTNcopy.State = this._faultTN.State;                              // продублированный Неисправность ТН

            LedManager.SetLeds(this._commandsMain, discrets.CommandsMain);              // Команды
            LedManager.SetLeds(this._controls, discrets.Controls);                      // Управление

            bool res = discrets.ControlSignals[7] &&
                       !(discrets.FaultLogicErr[0] ||
                         discrets.FaultLogicErr[1] ||
                         discrets.FaultLogicErr[2] ||
                         discrets.FaultLogicErr[3] ||
                         discrets.FaultLogicErr[4]);
            this._logicState.State = res ? LedState.NoSignaled : LedState.Signaled;     // Свободно программируемая логика
        }

        /// <summary>
        /// Прочитанно время
        /// </summary>
        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._dateTime.Value;
        }
        #endregion MemoryEntity Members

        #region [Help members]

        private void dateTimeControl_TimeChanged()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._dateTime.Value = this._dateTimeControl.DateTime;
            this._dateTime.SaveStruct();
        }

        private void MrMeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopRead();
        }

        private void MrMeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._groupUstavki.RemoveStructQueries();
            this._discretDataBase.RemoveStructQueries();
            this._dateTime.RemoveStructQueries();
            this._analogDataBase.RemoveStructQueries();  
            this._measureTrans.RemoveStructQueries();

            this._device.ConnectionModeChanged -= this.StartStopRead;
        }

        private void StartStopRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._iMinStruct.LoadStruct();
                this._groupUstavki.LoadStructCycle();
                this._analogDataBase.LoadStructCycle();
                this._discretDataBase.LoadStructCycle();
                this._dateTime.LoadStructCycle();
            }
            else
            {
                this._groupUstavki.RemoveStructQueries();
                this._discretDataBase.RemoveStructQueries();
                this._dateTime.RemoveStructQueries();
                this._analogDataBase.RemoveStructQueries();  
                this._measureTrans.RemoveStructQueries();

                this.AnalogBdReadFail();
                this.DiscretBdReadFail();
            }
        }

        private void _resetSystemJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D01, RESET_SJ);
        }

        private void _resetAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D02, RESET_AJ);
        }

        private void _resetOscJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D03, RESET_OSC);
        }

        private void _resetAvailabilityFaultSystemJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D04, RESET_FAULT_SJ);
        }

        private void _resetAnButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D05, RESET_INDICATION);
        }

        private void _switchGroupButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this._groupCombo.SelectedIndex == this._groupUstavki.Value.Word) return;
            int ind = 0;
            if (this._groupCombo.SelectedIndex != -1)
            {
                ind = this._groupCombo.SelectedIndex;
            }
            else
            {
                this._groupCombo.SelectedIndex = ind;
            }
            bool res = MessageBox.Show(string.Format(SWITCH_GROUP_USTAVKI, ind + 1), "Группы уставок", MessageBoxButtons.YesNo) != DialogResult.Yes;
            if (res) return;
            this._groupUstavki.Value.Word = (ushort)ind;
            this._groupUstavki.SaveStruct();
        }

        private void _resetTermStateButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D0E, RESET_HOT_STATE);
        }

        private void _resetFaultTnBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D10, RESET_TN_STATE);
        }

        private void _startOscBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D11, START_OSC);
        }

        private void _breakerOnBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D09, "Включить выключатель");
        }

        private void _breakerOffBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D08, "Отключить выключатель");
        }

        private void StartLogicBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Запустить свободно программируемую логику в устройстве?", "Запуск СПЛ",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x0D0D,true, "Запуск СПЛ", this._device);
        }

        private void StopLogicBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Остановить свободно программируемую логику в устройстве? ВНИМАНИЕ! Это может привести к выводу из работы важных функций устройства", "Останов СПЛ",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x0D0C, true, "Останов СПЛ", this._device);
        }

        private void ConfirmCommand(ushort address, string command)
        {
            DialogResult res = MessageBox.Show(command + "?", "Подтверждение комманды", MessageBoxButtons.YesNo);
            if(res == DialogResult.No) return;
            this._discretDataBase.SetBitByAdress(address, command);
        }
        #endregion [Help members]
        
        private void MrMeasuringForm_Activated(object sender, EventArgs e)
        {
            this._activatefailmes = true;
        }

        private void MrMeasuringForm_Deactivate(object sender, EventArgs e)
        {
            this._activatefailmes = false;
        }

        private void _commandBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            int i = _commandComboBox.SelectedIndex;
            this.ConfirmCommand((ushort)(0x0D20 + i), "Выполнить команду " + (i + 1));
        }

        #region [IFormView Members]

        public Type FormDevice => typeof(MRUniversal);
        public bool Multishow { get; private set; }
        public Type ClassType => typeof(MrMeasuringForm);
        public bool ForceShow => false;
        public Image NodeImage  => Properties.Resources.measuring.ToBitmap();
        public string NodeName => "Измерения";
        public INodeView[] ChildNodes => new INodeView[] { };
        public bool Deletable => false;

        #endregion [INodeView Members]
    }
}
