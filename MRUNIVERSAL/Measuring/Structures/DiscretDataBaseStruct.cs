﻿using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MRUNIVERSAL.Measuring.Structures
{
    /// <summary>
    /// МР Дискретная база данных
    /// </summary>
    public class DiscretDataBaseStruct : StructBase
    {
        #region [Constants]
        private const int BASE_SIZE = 32;
        private const int ALARM_SIZE = 16;
        private const int PARAM_SIZE = 16;
        private const int GOOSEIN_SIZE = 18;                  //размер массива бд входных Goose сигналов+их достоверность
        #endregion [Constants]


        #region [Private fields]
        [Layout(0, Count = BASE_SIZE)] private ushort[] _base;       // БД общая
        [Layout(1, Count = ALARM_SIZE)] private ushort[] _alarm;     // БД неисправностей
        [Layout(2, Count = PARAM_SIZE)] private ushort[] _param;     // БД параметров
        [Layout(3, Count = GOOSEIN_SIZE)] private ushort[] _goosein; // БД входных Goose сигналов + их достоверность (пока 64 сигнала - goosein[0], 64 доставерности - goosein[4])
        #endregion [Private fields]


        #region [Properties]
        // Вкладка "Основные сигналы"
        /// <summary>
        /// Индикаторы
        /// </summary>
        public virtual List<bool[]> Indicators
        {
            get
            {
                bool[] allIndArray = Common.GetBitsArray(this._base, 455, 494); // все подряд биты
                List<bool[]> retList = new List<bool[]>();
                for (int i = 0; i < allIndArray.Length; i = i + 2) // выделяем попарно состояния одного диода
                {
                    retList.Add(new[] { allIndArray[i], allIndArray[i + 1] }); // (bool зеленый, bool красный)
                }
                return retList;
            }
        }

        /// <summary>
        /// Индикаторы ЖС и ЖА
        /// </summary>
        public virtual List<bool> IndicatorsJsJa => Common.GetBitsArray(this._base, 495, 496).ToList();

        /// <summary>
        /// Дискретные входы
        /// </summary>
        public virtual bool[] DiscretInputs
        {
            get
            {
                List<bool> retDiscr = new List<bool>();
                retDiscr.AddRange(Common.GetBitsArray(this._base, 0, 87));
                retDiscr.AddRange(Common.GetBitsArray(this._base, 344, 345));
                return retDiscr.ToArray();
            }
        }

        /// <summary>
        /// Команды
        /// </summary>
        public virtual bool[] CommandsMain => Common.GetBitsArray(this._base, 88, 111);

        /// <summary>
        /// Реле и виртуальные реле
        /// </summary>
        public virtual bool[] Relays => Common.GetBitsArray(this._base, 389, 454);

        /// <summary>
        /// RS-триггеры
        /// </summary>
        public virtual bool[] RsTriggers => Common.GetBitsArray(this._base, 112, 127);

        /// <summary>
        /// ЛС
        /// </summary>
        public virtual bool[] InputsLogicSignals => Common.GetBitsArray(this._base, 128, 143);

        /// <summary>
        /// ВЛС
        /// </summary>
        public virtual bool[] OutputLogicSignals => Common.GetBitsArray(this._base, 160, 175);
        
        /// <summary>
        /// СПЛ
        /// </summary>
        public virtual bool[] FreeLogic => Common.GetBitsArray(this._base, 176, 223);

        /// <summary>
        /// БГС
        /// </summary>
        public virtual bool[] Bgs => Common.GetBitsArray(this._base, 144, 159);

        /// <summary>
        /// АПВ
        /// </summary>
        public virtual bool[] Apv => Common.GetBitsArray(this._base, 320, 328);

        /// <summary>
        /// Сигналы КС и УППН
        /// </summary>
        public virtual bool[] SignalsKsYppn => Common.GetBitsArray(this._base, 329, 335);

        /// <summary>
        /// Общие сигналы
        /// </summary>
        public virtual bool[] CommonSignals => Common.GetBitsArray(this._base, 313, 319);
        
        /// <summary>
        /// 761 - АВР Реклоузера 
        /// </summary>
        public virtual bool[] Avr => Common.GetBitsArray(this._base, 349, 357);

        /// <summary>
        /// 771 - ТУ и ТБ
        /// </summary>
        public virtual bool[] TuAndTb => Common.GetBitsArray(this._base, 353, 362);

        // 761 - резервы с 358 по 372, 771 - резервы с 349 по 352 и с 363 по 372
        
        

        // Вкладка "Защиты и неисправности"
        /// <summary>
        /// 771 - Защиты Z 10 ступеней, 761 - Защиты Z 6 ступеней
        /// </summary>
        public virtual bool[] DefencesZ => Common.GetBitsArray(this._base, 224, 243);

        /// <summary>
        /// 761 - Защиты P
        /// </summary>
        public virtual bool[] DefencesP => Common.GetBitsArray(this._base, 238, 241);

        /// <summary>
        /// Защиты I, Iменьше, I*, I2/I1
        /// </summary>
        public virtual bool[] DefencesI => Common.GetBitsArray(this._base, 244, 275);

        /// <summary>
        /// Защиты Uбольше, Uменьше
        /// </summary>
        public virtual bool[] DefencesU => Common.GetBitsArray(this._base, 276, 291);

        /// <summary>
        /// Защиты Fбольше, Fменьше
        /// </summary>
        public virtual bool[] DefencesF => Common.GetBitsArray(this._base, 292, 307);

        /// <summary>
        /// Защиты Q
        /// </summary>
        public virtual bool[] DefencesQ => Common.GetBitsArray(this._base, 308, 309);

        // с 310 по 312 есть, но не выведены

        /// <summary>
        /// Внешние защиты
        /// </summary>
        public virtual bool[] ExternalDefenses => Common.GetBitsArray(this._base, 373, 388);

        /// <summary>
        /// Повр. фаз и качание
        /// </summary>
        public virtual bool[] Phase => Common.GetBitsArray(this._base, 336, 338);

        /// <summary>
        /// УРОВ
        /// </summary>
        public virtual bool[] Urov => Common.GetBitsArray(this._base, 346, 348);

        /// <summary>
        /// Сигналы качаний
        /// </summary>
        public virtual bool[] Kachanie => Common.GetBitsArray(this._base, 339, 341);

        // Вкладка "Управляющие сигналы"
        /// <summary>
        /// Управляющие сигналы
        /// </summary>
        public virtual bool[] ControlSignals => Common.GetBitsArray(this._base, 497, 504);

        /// <summary>
        /// Неисправности
        /// </summary>
        public virtual bool[] Faults => Common.GetBitsArray(this._alarm, 0, 18);

        /// <summary>
        /// Неисправности выключателя
        /// </summary>
        public virtual bool[] FaultsSwitcher => Common.GetBitsArray(this._alarm, 19, 26);

        /// <summary>
        /// Нисправности измерения U
        /// </summary>
        public virtual bool[] FaultsMeasuringU => Common.GetBitsArray(this._alarm, 27, 35);

        /// <summary>
        /// Нисправности измерения F
        /// </summary>
        public virtual bool[] FaultsMeasuringF => Common.GetBitsArray(this._alarm, 36, 39);

        /// <summary>
        /// Неисправность логики
        /// </summary>
        public virtual bool[] FaultLogicErr => Common.GetBitsArray(this._alarm, 40, 44);

        /// <summary>
        /// Направления токов
        /// </summary>
        public virtual string[] Symbols
        {
            get
            {
                bool[] booleanArray = Common.GetBitsArray(this._param, 0, 53);
                string[] result = new string[27];
                for (int i = 0; i < 54; i += 2)
                {
                    result[i/2] = this.GetCurrentSymbol(i, booleanArray[i], booleanArray[i + 1]);
                }
                return result;
            }
        }

        private string GetCurrentSymbol(int i, bool symbol, bool error)
        {
            string res = "";
            if (i >= 12) res = "нет";
            return error ? res : (symbol ? "-" : "+");
        }

        /// <summary>
        /// Управление
        /// </summary>
        public virtual bool[] Controls
        {
            get
            {
                List<bool> controls = new List<bool>();
                controls.AddRange(Common.GetBitsArray(this._goosein, 0, 18));
                controls.AddRange(Common.GetBitsArray(this._goosein, 64, 78));
                return controls.ToArray();
            }
        }

        /// <summary>
        /// Команды
        /// </summary>
        public virtual bool[] Commands => Common.GetBitsArray(this._goosein, 32, 55);
        
        #endregion [Properties]
    }
}
