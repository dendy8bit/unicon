﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MRUNIVERSAL.Configuration.Structures.MeasuringTransformer;

namespace BEMN.MRUNIVERSAL.Measuring.Structures
{
    /// <summary>
    /// МРUniversal Аналоговая база данных
    /// </summary>
    public class AnalogDataBaseStruct : StructBase
    {
        #region [Private fields]
        //ток
        [Layout(0)] private int _ia; // ток Ia
        [Layout(1)] private int _ib; // ток Ib
        [Layout(2)] private int _ic; // ток Ic
        [Layout(3)] private int _in; // ток 3I0 ??
        [Layout(4)] private int _in1; // ток I1
        [Layout(5)] private int _i0; // ток I2
        [Layout(6)] private int _i1; // ток In
        [Layout(7)] private int _i2; // ток Iг
        [Layout(8)] private int _ig; // ток Iг
        [Layout(9)] private int _i30; // ток Iг
        [Layout(10)] private int _iab; // ток Iг
        [Layout(11)] private int _ibc; // ток Iг
        [Layout(12)] private int _ica; // ток Iг

        //ток вторая гармоника
        [Layout(13)] private int _i2A; // ток A (не используется)
        [Layout(14)] private int _i2B; // ток B (не используется)
        [Layout(15)] private int _i2C; // ток C (не используется)

        //канал U
        [Layout(16)] private int _ua; // напряжение Ua
        [Layout(17)] private int _ub; // напряжение Ub
        [Layout(18)] private int _uc; // напряжение Uc
        [Layout(19)] private int _un; // напряжение Un
        [Layout(20)] private int _un1; // напряжение Un1 (не используется)

        //напряжения расчетные
        [Layout(21)] private int _uab; // напряжение Uab
        [Layout(22)] private int _ubc; // напряжение Ubc
        [Layout(23)] private int _uca; // напряжение Uca
        [Layout(24)] private int _u0; // напряжение 3U0 ????
        [Layout(25)] private int _u1; // напряжение U1
        [Layout(26)] private int _u2; // напряжение U2
        [Layout(27)] private int _u30; // напряжение 3U0

        // Сопротивления
        [Layout(28)] private int _zab; //полное межфазное сопративление ab
        [Layout(29)] private int _zbc; //полное межфазное сопративление bc
        [Layout(30)] private int _zca; //полное межфазное сопративление ca
        [Layout(31)] private int _za1; //полное фазное сопративление для коэфициента компенсации I0 1 при замыкания на землю фаза a
        [Layout(32)] private int _zb1; //полное фазное сопративление для коэфициента компенсации I0 1 при замыкания на землю фаза b
        [Layout(33)] private int _zc1; //полное фазное сопративление для коэфициента компенсации I0 1 при замыкания на землю фаза c
        [Layout(34)] private int _za2; //полное фазное сопративление для коэфициента компенсации I0 2 при замыкания на землю фаза a
        [Layout(35)] private int _zb2; //полное фазное сопративление для коэфициента компенсации I0 2 при замыкания на землю фаза b
        [Layout(36)] private int _zc2; //полное фазное сопративление для коэфициента компенсации I0 2 при замыкания на землю фаза c
        [Layout(37)] private int _za3; //полное фазное сопративление для коэфициента компенсации I0 3 при замыкания на землю фаза a
        [Layout(38)] private int _zb3; //полное фазное сопративление для коэфициента компенсации I0 3 при замыкания на землю фаза b
        [Layout(39)] private int _zc3; //полное фазное сопративление для коэфициента компенсации I0 3 при замыкания на землю фаза c
        [Layout(40)] private int _za4; //полное фазное сопративление для коэфициента компенсации I0 4 при замыкания на землю фаза a
        [Layout(41)] private int _zb4; //полное фазное сопративление для коэфициента компенсации I0 4 при замыкания на землю фаза b
        [Layout(42)] private int _zc4; //полное фазное сопративление для коэфициента компенсации I0 4 при замыкания на землю фаза c
        [Layout(43)] private int _za5; //полное фазное сопративление для коэфициента компенсации I0 5 при замыкания на землю фаза a
        [Layout(44)] private int _zb5; //полное фазное сопративление для коэфициента компенсации I0 5 при замыкания на землю фаза b
        [Layout(45)] private int _zc5; //полное фазное сопративление для коэфициента компенсации I0 5 при замыкания на землю фаза c
        [Layout(46)] private int _rab; //активное межфазное сопративление ab
        [Layout(47)] private int _xab; //реактивное межфазное сопративление ab
        [Layout(48)] private int _rbc; //активное межфазное сопративление bc
        [Layout(49)] private int _xbc; //реактивное межфазное сопративление bc
        [Layout(50)] private int _rca; //активное межфазное сопративление ca
        [Layout(51)] private int _xca; //реактивное межфазное сопративление ca
        [Layout(52)] private int _ra1; //активное   фазное сопративление для коэфициента компенсации I0 1 при замыкания на землю фаза a
        [Layout(53)] private int _xa1; //реактивное фазное сопративление для коэфициента компенсации I0 1 при замыкания на землю фаза a
        [Layout(54)] private int _rb1; //активное   фазное сопративление для коэфициента компенсации I0 1 при замыкания на землю фаза b
        [Layout(55)] private int _xb1; //реактивное фазное сопративление для коэфициента компенсации I0 1 при замыкания на землю фаза b
        [Layout(56)] private int _rc1; //активное   фазное сопративление для коэфициента компенсации I0 1 при замыкания на землю фаза c
        [Layout(57)] private int _xc1; //реактивное фазное сопративление для коэфициента компенсации I0 1 при замыкания на землю фаза c
        [Layout(58)] private int _ra2; //активное   фазное сопративление для коэфициента компенсации I0 2 при замыкания на землю фаза a
        [Layout(59)] private int _xa2; //реактивное фазное сопративление для коэфициента компенсации I0 2 при замыкания на землю фаза a
        [Layout(60)] private int _rb2; //активное   фазное сопративление для коэфициента компенсации I0 2 при замыкания на землю фаза b
        [Layout(61)] private int _xb2; //реактивное фазное сопративление для коэфициента компенсации I0 2 при замыкания на землю фаза b
        [Layout(62)] private int _rc2; //активное   фазное сопративление для коэфициента компенсации I0 2 при замыкания на землю фаза c
        [Layout(63)] private int _xc2; //реактивное фазное сопративление для коэфициента компенсации I0 2 при замыкания на землю фаза c
        [Layout(64)] private int _ra3; //активное   фазное сопративление для коэфициента компенсации I0 3 при замыкания на землю фаза a
        [Layout(65)] private int _xa3; //реактивное фазное сопративление для коэфициента компенсации I0 3 при замыкания на землю фаза a
        [Layout(66)] private int _rb3; //активное   фазное сопративление для коэфициента компенсации I0 3 при замыкания на землю фаза b
        [Layout(67)] private int _xb3; //реактивное фазное сопративление для коэфициента компенсации I0 3 при замыкания на землю фаза b
        [Layout(68)] private int _rc3; //активное   фазное сопративление для коэфициента компенсации I0 3 при замыкания на землю фаза c
        [Layout(69)] private int _xc3; //реактивное фазное сопративление для коэфициента компенсации I0 3 при замыкания на землю фаза c
        [Layout(70)] private int _ra4; //активное   фазное сопративление для коэфициента компенсации I0 4 при замыкания на землю фаза a
        [Layout(71)] private int _xa4; //реактивное фазное сопративление для коэфициента компенсации I0 4 при замыкания на землю фаза a
        [Layout(72)] private int _rb4; //активное   фазное сопративление для коэфициента компенсации I0 4 при замыкания на землю фаза b
        [Layout(73)] private int _xb4; //реактивное фазное сопративление для коэфициента компенсации I0 4 при замыкания на землю фаза b
        [Layout(74)] private int _rc4; //активное   фазное сопративление для коэфициента компенсации I0 4 при замыкания на землю фаза c
        [Layout(75)] private int _xc4; //реактивное фазное сопративление для коэфициента компенсации I0 4 при замыкания на землю фаза c
        [Layout(76)] private int _ra5; //активное   фазное сопративление для коэфициента компенсации I0 5 при замыкания на землю фаза a
        [Layout(77)] private int _xa5; //реактивное фазное сопративление для коэфициента компенсации I0 5 при замыкания на землю фаза a
        [Layout(78)] private int _rb5; //активное   фазное сопративление для коэфициента компенсации I0 5 при замыкания на землю фаза b
        [Layout(79)] private int _xb5; //реактивное фазное сопративление для коэфициента компенсации I0 5 при замыкания на землю фаза b
        [Layout(80)] private int _rc5; //активное   фазное сопративление для коэфициента компенсации I0 5 при замыкания на землю фаза c
        [Layout(81)] private int _xc5; //реактивное фазное сопративление для коэфициента компенсации I0 5 при замыкания на землю фаза c

        //канал F
        [Layout(82)] private int _f;            // частота
        [Layout(83)] private int _qt;		    //состояние тепловой модели
        [Layout(84)] private int _qn;		    //число пусков
        [Layout(85)] private int _qnHot;		//число горячих пусков
        [Layout(86)] private int _dU;         //для синхронизма
        [Layout(87)] private int _dFi;       //для синхронизма
        [Layout(88)] private int _dF;         //для синхронизма
        [Layout(89)] private int _dFdt;      //скорость изменения частоты (знаковая)
        [Layout(90)] private int _groupUst;  // группа уставок (1-6)
        [Layout(91)] private int _fi;           // cos fi
        [Layout(92)] private int _p;            	
        [Layout(93)] private int _q;            
        [Layout(94)] private int _ud;           //дифф. напряжение Ud = Un1 - ( (Un/3) * (КННП/КННП1) )
        [Layout(95)] private int _inCap;        //емкостной ток In*sin(fI)  fi - угол между током и напряжением (Icos*Usin-IsinUcos)/Udei
        [Layout(96)] private int _inAkt;        //активный ток  In*cos(fI)  fi - угол между током и напряжением (Icos*Ucos+IsinUsin)/Udei
        [Layout(97)] private int _res00FF;      //число оперативных отключений
        [Layout(98)] private int _resF0FF;      //число аварийных отключений
        [Layout(99)] private int _ompLatch;     //последнее ОМП при аварии

        [Layout(100)] private int _lab;          //растояние до места повреждения
        [Layout(101)] private int _lbc;          //растояние до места повреждения
        [Layout(102)] private int _lca;          //растояние до места повреждения
        [Layout(103)] private int _la1;          //растояние до места повреждения
        [Layout(104)] private int _lb1;          //растояние до места повреждения
        [Layout(105)] private int _lc1;          //растояние до места повреждения
        [Layout(106)] private int _la2;          //растояние до места повреждения
        [Layout(107)] private int _lb2;          //растояние до места повреждения
        [Layout(108)] private int _lc2;          //растояние до места повреждения
        [Layout(109)] private int _la3;          //растояние до места повреждения
        [Layout(110)] private int _lb3;          //растояние до места повреждения
        [Layout(111)] private int _lc3;          //растояние до места повреждения
        [Layout(112)] private int _la4;          //растояние до места повреждения
        [Layout(113)] private int _lb4;          //растояние до места повреждения
        [Layout(114)] private int _lc4;          //растояние до места повреждения
        [Layout(115)] private int _la5;          //растояние до места повреждения
        [Layout(116)] private int _lb5;          //растояние до места повреждения
        [Layout(117)] private int _lc5;          //растояние до места повреждения

        //свободная логика(SIGNAL FREE LOGIC)
        [Layout(118)] private int _sfl1;
        [Layout(119)] private int _sfl2;
        [Layout(120)] private int _sfl3;
        [Layout(121)] private int _sfl4;
        [Layout(122)] private int _sfl5;
        [Layout(123)] private int _sfl6;
        [Layout(124)] private int _sfl7;
        [Layout(125)] private int _sfl8;

        //Углы
        [Layout(126)] private ushort _cornerIa;
        [Layout(127)] private ushort _cornerIb;
        [Layout(128)] private ushort _cornerIc;
        [Layout(129)] private ushort _cornerIn;
        [Layout(130)] private ushort _cornerIn1;
        [Layout(131)] private ushort _cornerI0;
        [Layout(132)] private ushort _cornerI1;
        [Layout(133)] private ushort _cornerI2;
        [Layout(134)] private ushort _cornerUa;
        [Layout(135)] private ushort _cornerUb;
        [Layout(136)] private ushort _cornerUc;
        [Layout(137)] private ushort _cornerUn;
        [Layout(138)] private ushort _cornerUn1;
        [Layout(139)] private ushort _cornerUab;
        [Layout(140)] private ushort _cornerUbc;
        [Layout(141)] private ushort _cornerUca;
        [Layout(142)] private ushort _cornerU0;
        [Layout(143)] private ushort _cornerU1;
        [Layout(144)] private ushort _cornerU2;
        #endregion [Private fields]

        #region [Public members]

        // Общие методы
        private ushort GetMean(List<AnalogDataBaseStruct> list, Func<AnalogDataBaseStruct, ushort> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (ushort)(sum / (double)count);
        }

        private short GetMean(List<AnalogDataBaseStruct> list, Func<AnalogDataBaseStruct, short> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (short)(sum / (double)count);
        }

        private double GetMean(List<AnalogDataBaseStruct> list, Func<AnalogDataBaseStruct, int> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            int sum = list.Sum(func);
            return (int)(sum / (double)count);
        }







        
        public string GetQt(List<AnalogDataBaseStruct> list)
        {
            var value = this.GetMean(list, o => o._qt);
            return ValuesConverterCommon.Analog.GetQ((ushort)value);
        }

        public string GetCosF(List<AnalogDataBaseStruct> list)
        {
            var value = this.GetMean(list, o => o._fi);
            return ValuesConverterCommon.Analog.GetCosF((ushort)value);
        }

        public string GetQ(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            double value = this.GetMean(list, o => o._q);
            return ValuesConverterCommon.Analog.GetQ((int)value, measure.ChannelI.Ittl * measure.ChannelU.KthlValue);
        }

        public string GetP(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            double value = this.GetMean(list, o => o._p);
            return ValuesConverterCommon.Analog.GetP(value, measure.ChannelI.Ittl * measure.ChannelU.KthlValue);
        }

        public string GetF(List<AnalogDataBaseStruct> list)
        {
            var value = this.GetMean(list, o => o._f);
            return ValuesConverterCommon.Analog.GetF((ushort)value);
        }











        // Токи
        public string GetIa(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var value = this.GetMean(list, o => o._ia);
            return value <= iMin ? "0" : ValuesConverterCommon.Analog.GetI((ushort)value, measure.ChannelI.Ittl * 40);
        }

        public string GetIb(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var value = this.GetMean(list, o => o._ib);
            return value <= iMin ? "0" : ValuesConverterCommon.Analog.GetI((ushort)value, measure.ChannelI.Ittl * 40);
        }
        public string GetIc(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var value = this.GetMean(list, o => o._ic);
            return value <= iMin ? "0" : ValuesConverterCommon.Analog.GetI((ushort)value, measure.ChannelI.Ittl * 40);
        }
        public string GetI1(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var value = this.GetMean(list, o => o._i1);
            return value <= iMin ? "0" : ValuesConverterCommon.Analog.GetI((ushort)value, measure.ChannelI.Ittl * 40);
        }
        public string GetI2(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var value = this.GetMean(list, o => o._i2);
            return value <= iMin ? "0" : ValuesConverterCommon.Analog.GetI((ushort)value, measure.ChannelI.Ittl * 40);
        }
        
        public string Get3I0(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var value = this.GetMean(list, o => o._i30);
            return value <= iMin ? "0" : ValuesConverterCommon.Analog.GetI((ushort)value, measure.ChannelI.Ittl * 40);
        }

        public string GetIn(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var value = this.GetMean(list, o => o._in);
            return value <= iMin ? "0" : ValuesConverterCommon.Analog.GetI((ushort)value, measure.ChannelI.Ittx * 40);
        }
        public string GetIg(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var value = this.GetMean(list, o => o._ig);
            return value <= iMin ? "0" : ValuesConverterCommon.Analog.GetI((ushort)value, measure.ChannelI.Ittl * 40);
        }




        // Напряжения
        public string GetUa(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._ua);
            return ValuesConverterCommon.Analog.GetU((ushort)value, measure.ChannelU.KthlValue);
        }

        public string GetUb(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._ub);
            return ValuesConverterCommon.Analog.GetU((ushort)value, measure.ChannelU.KthlValue);
        }

        public string GetUc(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._uc);
            return ValuesConverterCommon.Analog.GetU((ushort)value, measure.ChannelU.KthlValue);
        }

        public string GetUab(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._uab);
            return ValuesConverterCommon.Analog.GetU((ushort)value, measure.ChannelU.KthlValue);
        }

        public string GetUbc(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._ubc);
            return ValuesConverterCommon.Analog.GetU((ushort)value, measure.ChannelU.KthlValue);
        }

        public string GetUca(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._uca);
            return ValuesConverterCommon.Analog.GetU((ushort)value, measure.ChannelU.KthlValue);
        }

        public string GetU1(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._u1);
            return ValuesConverterCommon.Analog.GetU((ushort)value, measure.ChannelU.KthlValue);
        }

        public string GetU2(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._u2);
            return ValuesConverterCommon.Analog.GetU((ushort)value, measure.ChannelU.KthlValue);
        }
        
        public string Get3U0(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._u30);
            return ValuesConverterCommon.Analog.GetU((ushort)value, measure.ChannelU.KthlValue);
        }

        // Направленная частота
        public string GetUn(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._un);
            return ValuesConverterCommon.Analog.GetU((ushort)value, measure.ChannelU.KthxValue);
        }

        public string GetUn1(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._un1);
            return ValuesConverterCommon.Analog.GetU((ushort)value, measure.ChannelU.Kthx1Value);
        }

        /*===========ВТОРИЧНЫЕ СОПРОТИВЛЕНИЯ============*/
        public string GetVtorZab(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rab);
            var x = this.GetMean(list, o => o._xab);
            var ia = this.GetMean(list, o => o._ia);
            var ib = this.GetMean(list, o => o._ib);
            return (ia <= iMin || ib <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }

        public string GetVtorZbc(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rbc);
            var x = this.GetMean(list, o => o._xbc);
            var ic = this.GetMean(list, o => o._ic);
            var ib = this.GetMean(list, o => o._ib);
            return (ic <= iMin || ib <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }

        public string GetVtorZca(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rca);
            var x = this.GetMean(list, o => o._xca);
            var ia = this.GetMean(list, o => o._ia);
            var ic = this.GetMean(list, o => o._ic);
            return (ia <= iMin || ic <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }

        public string GetVtorZa1(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._ra1);
            var x = this.GetMean(list, o => o._xa1);
            var ia = this.GetMean(list, o => o._ia);
            return (ia <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }

        public string GetVtorZb1(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rb1);
            var x = this.GetMean(list, o => o._xb1);
            var ib = this.GetMean(list, o => o._ib);
            return (ib <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }

        public string GetVtorZc1(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rc1);
            var x = this.GetMean(list, o => o._xc1);
            var ic = this.GetMean(list, o => o._ic);
            return (ic <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }
        public string GetVtorZa2(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._ra2);
            var x = this.GetMean(list, o => o._xa2);
            var ia = this.GetMean(list, o => o._ia);
            return (ia <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }

        public string GetVtorZb2(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rb2);
            var x = this.GetMean(list, o => o._xb2);
            var ib = this.GetMean(list, o => o._ib);
            return (ib <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }

        public string GetVtorZc2(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rc2);
            var x = this.GetMean(list, o => o._xc2);
            var ic = this.GetMean(list, o => o._ic);
            return (ic <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }

        public string GetVtorZa3(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._ra3);
            var x = this.GetMean(list, o => o._xa3);
            var ia = this.GetMean(list, o => o._ia);
            return (ia <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }

        public string GetVtorZb3(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rb3);
            var x = this.GetMean(list, o => o._xb3);
            var ib = this.GetMean(list, o => o._ib);
            return (ib <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }

        public string GetVtorZc3(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rc3);
            var x = this.GetMean(list, o => o._xc3);
            var ic = this.GetMean(list, o => o._ic);
            return (ic <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }

        public string GetVtorZa4(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._ra4);
            var x = this.GetMean(list, o => o._xa4);
            var ia = this.GetMean(list, o => o._ia);
            return (ia <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }

        public string GetVtorZb4(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rb4);
            var x = this.GetMean(list, o => o._xb4);
            var ib = this.GetMean(list, o => o._ib);
            return (ib <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }

        public string GetVtorZc4(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rc4);
            var x = this.GetMean(list, o => o._xc4);
            var ic = this.GetMean(list, o => o._ic);
            return (ic <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }

        public string GetVtorZa5(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._ra5);
            var x = this.GetMean(list, o => o._xa5);
            var ia = this.GetMean(list, o => o._ia);
            return (ia <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }

        public string GetVtorZb5(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rb5);
            var x = this.GetMean(list, o => o._xb5);
            var ib = this.GetMean(list, o => o._ib);
            return (ib <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }

        public string GetVtorZc5(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rc5);
            var x = this.GetMean(list, o => o._xc5);
            var ic = this.GetMean(list, o => o._ic);
            return (ic <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, 1, iInp);
        }
        
        //===========ПЕРВИЧНЫЕ СОПРОТИВЛЕНИЯ============
        public string GetZab(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rab);
            var x = this.GetMean(list, o => o._xab);
            var ia = this.GetMean(list, o => o._ia);
            var ib = this.GetMean(list, o => o._ib);
            return ia <= iMin || ib <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue, measure.ChannelI.Ittl);
        }

        public string GetZbc(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rbc);
            var x = this.GetMean(list, o => o._xbc);
            var ib = this.GetMean(list, o => o._ib);
            var ic = this.GetMean(list, o => o._ic);
            return ic <= iMin || ib <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZca(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rca);
            var x = this.GetMean(list, o => o._xca);
            var ia = this.GetMean(list, o => o._ia);
            var ic = this.GetMean(list, o => o._ic);
            return ia <= iMin || ic <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZa1(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._ra1);
            var x = this.GetMean(list, o => o._xa1);
            var ia = this.GetMean(list, o => o._ia);
            return ia <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZb1(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rb1);
            var x = this.GetMean(list, o => o._xb1);
            var ib = this.GetMean(list, o => o._ib);
            return ib <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZc1(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rc1);
            var x = this.GetMean(list, o => o._xc1);
            var ic = this.GetMean(list, o => o._ic);
            return ic <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZa2(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._ra2);
            var x = this.GetMean(list, o => o._xa2);
            var ia = this.GetMean(list, o => o._ia);
            return ia <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }
        
        public string GetZb2(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rb2);
            var x = this.GetMean(list, o => o._xb2);
            var ib = this.GetMean(list, o => o._ib);
            return ib <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue,measure.ChannelI.Ittl);
        }

        public string GetZc2(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rc2);
            var x = this.GetMean(list, o => o._xc2);
            var ic = this.GetMean(list, o => o._ic);
            return ic <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZa3(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._ra3);
            var x = this.GetMean(list, o => o._xa3);
            var ia = this.GetMean(list, o => o._ia);
            return ia <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZb3(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rb3);
            var x = this.GetMean(list, o => o._xb3);
            var ib = this.GetMean(list, o => o._ib);
            return ib <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZc3(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rc3);
            var x = this.GetMean(list, o => o._xc3);
            var ic = this.GetMean(list, o => o._ic);
            return ic <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZa4(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._ra4);
            var x = this.GetMean(list, o => o._xa4);
            var ia = this.GetMean(list, o => o._ia);
            return ia <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZb4(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rb4);
            var x = this.GetMean(list, o => o._xb4);
            var ib = this.GetMean(list, o => o._ib);
            return ib <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue ,measure.ChannelI.Ittl);
        }

        public string GetZc4(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rc4);
            var x = this.GetMean(list, o => o._xc4);
            var ic = this.GetMean(list, o => o._ic);
            return ic <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZa5(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._ra5);
            var x = this.GetMean(list, o => o._xa5);
            var ia = this.GetMean(list, o => o._ia);
            return ia <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZb5(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rb5);
            var x = this.GetMean(list, o => o._xb5);
            var ib = this.GetMean(list, o => o._ib);
            return ib <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }
        public string GetZc5(List<AnalogDataBaseStruct> list, MeasureTransStruct measure, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rc5);
            var x = this.GetMean(list, o => o._xc5);
            var ic = this.GetMean(list, o => o._ic);
            return ic <= iMin ? "---------" : ValuesConverterCommon.Analog.GetZint(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }
        
             
        public string GetdU(List<AnalogDataBaseStruct> list)
        {
            var value = this.GetMean(list, o => o._dU);
            return ValuesConverterCommon.Analog.GetUdouble(value);
        }

        public string GetdFi(List<AnalogDataBaseStruct> list)
        {
            var value = this.GetMean(list, o => o._dFi);
            return string.Format("{0} град.", Math.Round((double) value*360/ushort.MaxValue, 2));
        }
        
        // Направленная частота
        public string GetdF(List<AnalogDataBaseStruct> list)
        {
            var value = this.GetMean(list, o => o._dF);
            return ValuesConverterCommon.Analog.GetF((ushort)value);
        }


        //======================УГЛЫ=======================
        public string CornerIa => this._cornerIa == 1000 ? string.Empty : this._cornerIa.ToString();

        public string CornerIb => this._cornerIb == 1000 ? string.Empty : this._cornerIb.ToString();

        public string CornerIc => this._cornerIc == 1000 ? string.Empty : this._cornerIc.ToString();

        public string CornerIn => this._cornerIn == 1000 ? string.Empty : this._cornerIn.ToString();

        public string CornerUa => this._cornerUa == 1000 ? string.Empty : this._cornerUa.ToString();

        public string CornerUb => this._cornerUb == 1000 ? string.Empty : this._cornerUb.ToString();

        public string CornerUc => this._cornerUc == 1000 ? string.Empty : this._cornerUc.ToString();

        public string CornerUn => this._cornerUn == 1000 ? string.Empty : this._cornerUn.ToString();

        public string CornerUn1 => this._cornerUn1 == 1000 ? string.Empty : this._cornerUn1.ToString();

        public string CornerI0 => this._cornerI0 == 1000 ? string.Empty : this._cornerI0.ToString();

        public string CornerI1 => this._cornerI1 == 1000 ? string.Empty : this._cornerI1.ToString();

        public string CornerI2 => this._cornerI2 == 1000 ? string.Empty : this._cornerI2.ToString();
        
        public string CornerUab => this._cornerUab == 1000 ? string.Empty : this._cornerUab.ToString();

        public string CornerUbc => this._cornerUbc == 1000 ? string.Empty : this._cornerUbc.ToString();

        public string CornerUca => this._cornerUca == 1000 ? string.Empty : this._cornerUca.ToString();

        public string CornerU0 => this._cornerU0 == 1000 ? string.Empty : this._cornerU0.ToString();

        public string CornerU1 => this._cornerU1 == 1000 ? string.Empty : this._cornerU1.ToString();

        public string CornerU2 => this._cornerU2 == 1000 ? string.Empty : this._cornerU2.ToString();

        #endregion [Public members]
    }
}
