﻿using System;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.MRUNIVERSAL.Configuration.Structures.MeasuringTransformer;

namespace BEMN.MRUNIVERSAL
{
    /// <summary>
    /// Загружает уставки токов(Iтт) и напряжений (Ктн)
    /// </summary>
    public class CurrentOptionsLoader
    {
        #region [Events]
        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        public event Action LoadOk;

        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        public event Action LoadFail;
        #endregion [Events]

        #region [Private fields]
        private const int COUNT_GROUPS = 6;
        private int _numberOfGroup;
        #endregion [Private fields]

        #region [Public Properties]

        public MemoryEntity<MeasureTransStruct>[] _connections;
        public MeasureTransStruct this[int index] => this._connections[index].Value;
        #endregion [Public Properties]

        #region [Constructor's]
        public CurrentOptionsLoader(MRUniversal device) // тут класс MRUniversal не используется глобально
        {
            this._connections = new MemoryEntity<MeasureTransStruct>[COUNT_GROUPS];
            this._numberOfGroup = 0;
            for (int groupNumber = 0; groupNumber < this._connections.Length; groupNumber++)
            {
                this._connections[groupNumber] = new MemoryEntity<MeasureTransStruct>($"Параметры измерительного трансформатора гр.{groupNumber + 1}", device, device.GetStartAddrMeasTrans(groupNumber));
                this._connections[groupNumber].AllReadOk += this.Connections_AllReadOk; // читаем все группы, попадаем 5 раз в метод Connections_AllReadOk, затем вызываем событие ReadOk
                this._connections[groupNumber].AllReadFail += this.Connections_AllReadFail;
            }
        }
        #endregion [Constructor's]

        #region [Methods]
        /// <summary>
        /// Прочитана структура MeasureTransStruct 1 раз
        /// </summary>
        private void Connections_AllReadOk(object sender)
        {
            this._numberOfGroup++; // увеличиваем номер группы
            if (this._numberOfGroup < COUNT_GROUPS) // если меньше 6 групп
            {
                this._connections[this._numberOfGroup].LoadStruct(); // читаем следующую группу
            }
            else // если все прочитаны
            {
                this.LoadOk?.Invoke(); // вызываем событие всё успешно прочитано
            }
        }

        private void Connections_AllReadFail(object sender)
        {
            this.LoadFail?.Invoke();
        }

        /// <summary>
        /// Запускает загрузку уставок токов(Iтт) и напряжений(Ктн), начиная с первой группы
        /// </summary>
        public void StartRead()
        {
            this._numberOfGroup = 0;
            this._connections[0].LoadStruct();
        }
        #endregion [Methods]
    }
}
