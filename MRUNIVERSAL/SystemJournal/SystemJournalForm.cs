﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MRUNIVERSAL.Properties;
using BEMN.MRUNIVERSAL.SystemJournal.Structures;
using SchemeEditorSystem.ResourceLibs;

namespace BEMN.MRUNIVERSAL.SystemJournal
{
    public partial class SystemJournalForm : Form, IFormView
    {
        #region [IFormView Members]
        public Type FormDevice => typeof(MRUniversal);
        public bool Multishow { get; private set; }
        public INodeView[] ChildNodes => new INodeView[] { };
        public Type ClassType => typeof(SystemJournalForm);
        public bool Deletable => false;
        public bool ForceShow => false;
        public Image NodeImage => Resources.js;
        public string NodeName => SYSTEM_JOURNAL;
        #endregion [IFormView Members]
        

        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Записей в журнале - {0}";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string READ_FAIL = "Невозможно прочитать журнал";
        private const string TABLE_NAME_SYS = "МР_журнал_системы";
        private const string NUMBER_SYS = "Номер";
        private const string TIME_SYS = "Время";
        private const string MESSAGE_SYS = "Сообщение";
        private const string SYSTEM_JOURNAL = "Журнал системы";
        private const string READING_LIST_FILE = "Идет чтение файла списка подписей сигналов ЖС СПЛ";
        private const string FAIL_READ = "Невозможно прочитать список подписей сигналов ЖC СПЛ. Списки будут сформированы по умолчанию.";
        private const string LIST_FILE_NAME = "jlist.xml";
        private const string DEVICE_NAME = "MrUniversal";
        private const string SYS_JOURNAL_FILE_NAME = "Журнал cистемы {0} версия {1}";
        #endregion [Constants]


        #region [Private fields]
        private readonly MemoryEntity<OneWordStruct> _saveIndex;
        private readonly MemoryEntity<SystemJournalStruct> _systemJournal;
        private DataTable _dataTable;
        private int _recordNumber;
        private MRUniversal _device;
        private FileDriver _fileDriver;
        private JournalLoader _journalLoader;
        #endregion [Private fields]


        #region[Constructor's]
        public SystemJournalForm()
        {
            this.InitializeComponent();
        }

        public SystemJournalForm(MRUniversal device)
        {
            this.InitializeComponent();
            this._device = device;

            this._journalLoader = new JournalLoader(this._device.AllSystemJournal1024, this._device.AllSystemJournal64, this._device.IndexJS);
            this._journalLoader.JournalReadOk += HandlerHelper.CreateActionHandler(this, this.AllReadRecord);
            this._journalLoader.JournalReadFail += HandlerHelper.CreateActionHandler(this, this.FailReadJournal);

            this._fileDriver = new FileDriver(this._device, this);
        }
        #endregion[Constructor's]


        #region [Properties]
        private bool ButtonsEnabled
        {
            set => this._readJournalButton.Enabled = this._saveJournalButton.Enabled = this._loadJournalButton.Enabled = this._exportButton.Enabled = value;
        }

        /// <summary>
        /// Счётчик сообщений
        /// </summary>
        public int RecordNumber
        {
            get => this._recordNumber;
            set
            {
                this._recordNumber = value;
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                this.statusStrip1.Update();
            }
        }
        #endregion [Properties]


        #region [Help members]
        private void FailReadJournal()
        {
            this.RecordNumber = 0;
            this._statusLabel.Text = READ_FAIL;
            this.ButtonsEnabled = true;
        }

        /// <summary>
        /// Метод используется только для чтения файлов формата bin
        /// </summary>
        private void ReadRecord()
        {
            if (!this._systemJournal.Value.IsEmpty)
            {
                this.RecordNumber++;
                this._dataTable.Rows.Add
                (
                    this.RecordNumber.ToString(CultureInfo.InvariantCulture), this._systemJournal.Value.GetRecordTime, this._systemJournal.Value.GetRecordMessage
                );
                this._systemJournalGrid.Refresh();
            }
            else
            {
                this.ButtonsEnabled = true;
            }
        }

        private void AllReadRecord()
        {
            if (this._journalLoader.JournalRecords.Count != 0)
            {
                for (int i = 0; i < this._journalLoader.JournalRecords.Count; i++)
                {
                    this._dataTable.Rows.Add
                    (
                    i + 1, 
                    this._journalLoader.JournalRecords[i].GetRecordTime,
                    this._journalLoader.JournalRecords[i].GetRecordMessage
                    );
                }
                this._systemJournalGrid.Refresh();
                this.RecordNumber = this._journalLoader.JournalRecords.Count;
            }
            else
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
            }
            this.ButtonsEnabled = true;
        }

        private DataTable GetJournalDataTable()
        {
            var table = new DataTable(TABLE_NAME_SYS);
            table.Columns.Add(NUMBER_SYS);
            table.Columns.Add(TIME_SYS);
            table.Columns.Add(MESSAGE_SYS);
            return table;
        }

        private void SaveJournalToFile()
        {
            this._saveJournalDialog.FileName = $"Журнал системы {this._device.DeviceType} версия {this._device.DeviceVersion} {this._device.DevicePlant}";
            if (DialogResult.OK != this._saveJournalDialog.ShowDialog()) return;
            this._dataTable.WriteXml(this._saveJournalDialog.FileName);
            this._statusLabel.Text = JOURNAL_SAVED;
        }

        private void LoadJournalFromFile()
        {
            this._openJournalDialog.FileName = string.Format(SYS_JOURNAL_FILE_NAME, this._device.DeviceType, this._device.DeviceVersion);
            if (this._openJournalDialog.ShowDialog() != DialogResult.OK) return;
            try
            {
                this._dataTable.Clear();               
                if (Path.GetExtension(this._openJournalDialog.FileName).ToLower().Contains("bin"))
                {
                    byte[] file = File.ReadAllBytes(this._openJournalDialog.FileName);
                    SystemJournalStruct journal = new SystemJournalStruct();
                    int size = journal.GetSize();
                    List<byte> journalBytes = new List<byte>();
                    journalBytes.AddRange(Common.SwapArrayItems(file));
                    int j = 0;
                    this._recordNumber = 0;
                    int countRecord = journalBytes.Count / size;
                    for (int i = 0; j < countRecord; i = i + size)
                    {
                        journal.InitStruct(journalBytes.GetRange(i, size).ToArray());
                        journal.MessagesList = PropFormsStrings.GetNewJournalList();

                        this._systemJournal.Value = journal;
                        this.ReadRecord();
                        j++;
                    }
                }
                else
                {
                    this._dataTable.Clear();
                    this._dataTable.ReadXml(this._openJournalDialog.FileName);
                }
                this.RecordNumber = this._systemJournalGrid.Rows.Count;
            }
            catch
            {
                MessageBox.Show("Ошибка чтения файла!");
            }           
        }

        private void OnListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "Операция успешно выполнена")
                {
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes), Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            string nameDevice = "";

                            switch (Strings.DeviceName)
                            {
                                case "MR771":
                                    nameDevice = "MR771";
                                    break;
                                case "MR761":
                                    nameDevice = "MR76X";
                                    break;
                            }

                            XmlRootAttribute root = new XmlRootAttribute(nameDevice);
                            XmlSerializer serializer = new XmlSerializer(typeof (ListsOfJournals), root);
                            ListsOfJournals lists = (ListsOfJournals) serializer.Deserialize(reader);
                            this._journalLoader.MessagesList = lists.SysJournal.MessagesList;
                        }
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                MessageBox.Show(FAIL_READ, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this._journalLoader.MessagesList = PropFormsStrings.GetNewJournalList();
            }

            this._journalLoader.StartRead(); // Читаем журнал
            _statusLabel.Text = "Чтение журнала...";
        }

        private void StartReadMessagesList()
        {
            this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME); // Читаем файл
            this._dataTable.Clear();
            this._statusLabel.Text = READING_LIST_FILE;
            this.ButtonsEnabled = false;
        }

        #endregion [Help members]


        #region [Events Handlers]
        private void MrSystemJournalForm_Load(object sender, EventArgs e)
        {
            this._dataTable = this.GetJournalDataTable();
            this._systemJournalGrid.DataSource = this._dataTable;
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this.StartReadMessagesList();
            }
        }

        private void _readJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.DeviceDlgInfo.IsConnectionMode && !this._device.IsConnect) return;
            this.StartReadMessagesList();
        }

        private void _saveJournalButton_Click(object sender, EventArgs e)
        {
            this.SaveJournalToFile();
        }

        private void _loadJournalButton_Click(object sender, EventArgs e)
        {
            this.LoadJournalFromFile();
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            this._saveJournalHtmlDialog.FileName = $"Журнал системы {this._device.DeviceType} версия {this._device.DeviceVersion} {this._device.DevicePlant}";
            if (DialogResult.OK != this._saveJournalHtmlDialog.ShowDialog()) return;
            SaveToHtml(this._dataTable, this._saveJournalHtmlDialog.FileName, Resources.SystemJournal);
            this._statusLabel.Text = JOURNAL_SAVED;
        }

        public void SaveToHtml(DataTable dataTable, string fileName, string shema)
        {
            try
            {
                HtmlExport.ExportJournal(this._dataTable, this._saveJournalHtmlDialog.FileName, shema, this._device.DeviceVersion, this._device.DeviceType, this._device.DeviceNumber.ToString(), this._device.DevicePlant);
                return;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }

        private void MrSystemJournalForm_Activated(object sender, EventArgs e)
        {
            StringsSj.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);
        }
        #endregion [Events Handlers]

        private void SystemJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._systemJournal?.RemoveStructQueries();
            this._journalLoader?.ClearEvents();
        }
    }
}
