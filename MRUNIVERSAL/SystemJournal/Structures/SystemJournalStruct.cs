﻿using System;
using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MRUNIVERSAL.SystemJournal.Structures
{
    public class SystemJournalStruct : StructBase
    {
        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        [Layout(7)] private ushort _moduleErrorCode;
        [Layout(8)] private ushort _message;

        public ushort Year => this._year;
        public ushort Month => this._month;
        public ushort Date => this._date;
        public ushort Hour => this._hour;
        public ushort Minute => this._minute;
        public ushort Second => this._second;
        public ushort Millisecond => this._millisecond;
        public ushort ModuleErrorCode => this._moduleErrorCode;
        public ushort Message => this._message;

        public bool IsEmpty => this.Year + this.Month + this.Date + this.Hour + this.Minute + this.Second + this.Millisecond + this.ModuleErrorCode + this.Message == 0;

        public string GetRecordTime => $"{this.Date:d2}.{this.Month:d2}.{this.Year:d2} {this.Hour:d2}:{this.Minute:d2}:{this.Second:d2},{this.Millisecond:d3}";

        public string GetRecordMessage
        {
            get
            {
                if (this.Message == 7 || this.Message == 9 || this.Message == 11 || this.Message == 13 || this.Message == 15 || this.Message == 271) // ОШИБКИ МОДУЛЯ (1-6)
                {
                    return $"{StringsSj.Message[this.Message]} ({this.ModuleErrorCode:X2})";
                }

                if (this.Message == 279)
                {
                    int commands = Common.GetBits(_moduleErrorCode, 0, 1, 2, 3, 4);
                    int setup = Common.GetBits(_moduleErrorCode, 5, 6) >> 5;
                    int message = Common.GetBits(_moduleErrorCode, 7) >> 7;

                    try
                    {
                        return $"{CommandMessage(message, setup, commands)}";
                    }
                    catch (Exception e)
                    {
                        return $"{StringsSj.Message[this.Message]}";
                    }
                    
                }

                if (this.Message >= 500) // Список подписей начинается от 500
                {
                    return this.MessagesList[this.Message - 500];
                }
                return StringsSj.Message.Count > this.Message ? StringsSj.Message[this.Message] : this.Message.ToString();
            }
        }

        private string CommandMessage(int code1, int code2, int code3)
        {
            return $"{Code1[code1]}: {Code2[code2]} {Code3[code3 - 1]}";
        }

        private List<string> Code1 = new List<string>
        {
            "МЕНЮ",
            "СДТУ"
        };

        private List<string> Code2 = new List<string>
        {
            "ПУСК",
            "УСТАНОВИТЬ",
            "СБРОСИТЬ"
        };

        private List<string> Code3 = new List<string>
        {
            "КМД 1",
            "КМД 2",
            "КМД 3",
            "КМД 4",
            "КМД 5",
            "КМД 6",
            "КМД 7",
            "КМД 8",
            "КМД 9",
            "КМД 10",
            "КМД 11",
            "КМД 12",
            "КМД 13",
            "КМД 14",
            "КМД 15",
            "КМД 16",
            "КМД 17",
            "КМД 18",
            "КМД 19",
            "КМД 20",
            "КМД 21",
            "КМД 22",
            "КМД 23",
            "КМД 24"
        };

        public List<string> MessagesList { get; set; }
    }
}