﻿using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MRUNIVERSAL.SystemJournal.Structures
{
    public class AllSystemJournalStruct1024 : StructBase
    {
        [Layout(0, Count = 113)] private SystemJournalStruct[] _records;

        public SystemJournalStruct[] Records => this._records.Where(r => !r.IsEmpty).ToArray();
    }

    public class AllSystemJournalStruct64 : StructBase
    {
        [Layout(0, Count = 7)] private SystemJournalStruct[] _records;

        public SystemJournalStruct[] Records => this._records.Where(r => !r.IsEmpty).ToArray();
    }
}