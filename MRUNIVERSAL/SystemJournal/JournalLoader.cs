﻿using System;
using System.Collections.Generic;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MRUNIVERSAL.SystemJournal.Structures;

namespace BEMN.MRUNIVERSAL.SystemJournal
{
    public class JournalLoader
    {
        #region [Events]
        public event Action JournalReadOk;
        public event Action JournalReadFail;
        #endregion [Events]


        #region [Private fields]
        private readonly MemoryEntity<AllSystemJournalStruct1024> _full1024;
        private readonly MemoryEntity<AllSystemJournalStruct64> _full64;
        private readonly MemoryEntity<OneWordStruct> _saveIndex;
        private readonly List<SystemJournalStruct> _journalRecords;
        private int _recordNumber;
        #endregion [Private fields]


        #region [Properties]
        public List<string> MessagesList { get; set; }
        public List<SystemJournalStruct> JournalRecords => this._journalRecords;
        #endregion [Properties]


        #region [Constructor]
        public JournalLoader(MemoryEntity<AllSystemJournalStruct1024> full1024, MemoryEntity<AllSystemJournalStruct64> full64, MemoryEntity<OneWordStruct> saveIndex)
        {
            this._journalRecords = new List<SystemJournalStruct>();
            this._full1024 = full1024;
            this._full64 = full64;

            if (full1024 != null)
            {
                this._full1024.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
                this._full1024.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.OnRaiseJournalReadFail);
            }

            if (full64 != null)
            {
                this._full64.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
                this._full64.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.OnRaiseJournalReadFail);
            }

            // Запись номера
            this._saveIndex = saveIndex;
            this._saveIndex.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this.StartReadJournalRecords);
            this._saveIndex.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this.OnRaiseJournalReadFail);
        }
        #endregion [Constructor]


        #region [Methods]
        private void ReadRecord()
        {
            if (this._full1024 != null)
            {
                this._recordNumber += this._full1024.Value.Records.Length;
                foreach (SystemJournalStruct record in this._full1024.Value.Records)
                {
                    record.MessagesList = this.MessagesList;
                }
                List<SystemJournalStruct> currentRecords = new List<SystemJournalStruct>(this._full1024.Value.Records); // прочитанные записи
                this._journalRecords.AddRange(currentRecords); // все записи
                if (this._journalRecords.Count != 0 && currentRecords.Count != 0)
                {
                    this.SaveIndex();
                }
                else
                {
                    this.JournalReadOk?.Invoke();
                }
                return;
            }
            if (this._full64 != null)
            {
                this._recordNumber += this._full64.Value.Records.Length;
                foreach (SystemJournalStruct record in this._full64.Value.Records)
                {
                    record.MessagesList = this.MessagesList;
                }
                List<SystemJournalStruct> currentRecords = new List<SystemJournalStruct>(this._full64.Value.Records); // прочитанные записи
                this._journalRecords.AddRange(currentRecords); // все записи
                if (this._journalRecords.Count != 0 && currentRecords.Count != 0)
                {
                    this.SaveIndex();
                }
                else
                {
                    this.JournalReadOk?.Invoke();
                }
            }
        }

        private void OnRaiseJournalReadFail()
        {
            this.JournalReadFail?.Invoke();
        }

        private void StartReadJournalRecords()
        {
            this._full1024?.LoadStruct();
            this._full64?.LoadStruct();      
        }

        /// <summary>
        /// Запуск чтения ЖС
        /// </summary>
        public void StartRead()
        {
            this._recordNumber = 0;
            this._journalRecords.Clear();
            this.SaveIndex();
        }

        private void SaveIndex()
        {
            this._saveIndex.Value.Word = (ushort)this._recordNumber;
            this._saveIndex.SaveStruct6();
        }

        public void ClearEvents()
        {
            if (this._full1024 != null)
            {
                this._full1024.RemoveStructQueries();
                this._full1024.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
                this._full1024.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this.OnRaiseJournalReadFail);
            }

            if (this._full64 != null)
            {
                this._full64.RemoveStructQueries();
                this._full64.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
                this._full64.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this.OnRaiseJournalReadFail);
            }

            // Запись номера
            this._saveIndex.RemoveStructQueries();
            this._saveIndex.AllWriteOk -= HandlerHelper.CreateReadArrayHandler(this.StartReadJournalRecords);
            this._saveIndex.AllWriteFail -= HandlerHelper.CreateReadArrayHandler(this.OnRaiseJournalReadFail);
        }
        #endregion [Methods]      
    }
}