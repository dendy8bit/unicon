using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using BEMN.MBServer;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Ls;
using BEMN.MRUNIVERSAL.Configuration.Structures.MeasuringTransformer;
using BEMN.MRUNIVERSAL.Configuration.Structures.Oscope;
using BEMN.MRUNIVERSAL.Osc.Structures;

namespace BEMN.MRUNIVERSAL.Osc.HelpClasses
{
    /// <summary>
    /// ��������� �������� ������������
    /// </summary>
    public class CountingList
    {

        /// <summary>
        /// ������ ������ �������(� ������)
        /// </summary>
        private int _countingSize;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        private int _discretsCount;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        private int _channelsCount;
        /// <summary>
        /// ���-�� �����
        /// </summary>
        private int _currentsCount;
        /// <summary>
        /// ���-�� ����������
        /// </summary>
        private int _voltagesCount;

        public readonly string[] _iNames = new[] { "Ia", "Ib", "Ic", "In", "In1" };
        public readonly string[] _uFazNames = new[] { "Ua", "Ub", "Uc", "Un", "Un1"};
        public readonly string[] _uLineNames = new[] { "Uab", "Ubc", "Uca", "Un", "Un1"};

        private List<string> _uNames = new List<string>();

        /// <summary>
        /// ������ �������� �������� �� �����
        /// </summary>
        private readonly ushort[][] _countingArray;

        /// <summary>
        /// ������������ �������� �����
        /// </summary>
        private short[][] _baseCurrents;

        /// <summary>
        /// ������������ �������� ��� �����
        /// </summary>
        private double[] CurrentsKoefs { get; set; }
        /// <summary>
        /// ������������ �������� ��� ����������
        /// </summary>
        private double[] VoltageKoefs { get; set; }

        /// <summary>
        /// ����������� ���
        /// </summary>
        public double _minI;

        /// <summary>
        /// ������������ ���
        /// </summary>
        public double _maxI;

        /// <summary>
        /// ������������
        /// </summary>
        public double[][] _currents;

        /// <summary>
        /// ����������
        /// </summary>
        private double[][] _voltages;

        /// <summary>
        /// ������������ �������� ����������
        /// </summary>
        private short[][] _baseVoltages;

        /// <summary>
        /// ����������� ����������
        /// </summary>
        private double _minU;
        /// <summary>
        /// ������������ ����������
        /// </summary>
        private double _maxU;

        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        private int _alarm;

        private OscJournalStruct _oscJournalStruct;

        /// <summary>
        /// ������
        /// </summary>
        public ushort[][] _channels;

        /// <summary>
        /// ��������
        /// </summary>
        public ushort[][] _discrets;

        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        public int _count;

        private ChannelWithBase[] _channelsWithBase;

        private string _hdrString;
        
        private MRUniversal _device;
        
        #region [Ctor's]

        public CountingList(MRUniversal device, ushort[] pageValue, OscJournalStruct oscJournalStruct, MeasureTransStruct measure, ChannelWithBase[] channels)
        {
            this._device = device;

            this._channelsWithBase = channels;
            this._oscJournalStruct = oscJournalStruct;
            this._alarm = this._oscJournalStruct.Len - this._oscJournalStruct.After;
            
            // ��������� OscOptionsStruct c ����� "�������������"
            this.GetCountingSettings(this._device.OscOptions.Value);

            // ��������� OscOptionsStruct c ����� "������ �� ����������"
            if (this._countingSize == 0)
            {
                this.GetCountingSettings(this._device.OscOptionsDiagnostics.Value);
            }

            if (measure.ChannelU.Input == Strings.InputBinding.Last())
            {
                _uNames.AddRange(_uLineNames);
            }
            else
            {
                _uNames.AddRange(_uFazNames);
            }

            this._countingArray = new ushort[this._countingSize][];
            //����� ���������� ��������
            this._count = pageValue.Length / this._countingSize;
            //������������� �������
            for (int i = 0; i < this._countingSize; i++)
            {
                this._countingArray[i] = new ushort[this._count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n == this._countingSize)
                {
                    m++;
                    n = 0;
                }
            }
            //����
            this.SetCountinCurrents(measure);

            //����������
            
            this.SetCountingVoltage(measure);
            
            //��������
            this._discrets = this.SetDiscrets(this._discretsCount, out int increment, out bool b);//dicrets.ToArray();

            //������
            this._channels = this.SetChannels(this._channelsCount, increment, b);//channels.ToArray();

            this._hdrString = $"{_device.Info.Type} v{this._device.DeviceVersion} {_device.Info.Plant} {oscJournalStruct.GetDate} {oscJournalStruct.GetTime} ������� - {oscJournalStruct.Stage}";
        }

        private void GetCountingSettings(OscOptionsStruct settings)
        {
            this._countingSize = settings.SizeCounting;
            this._currentsCount = settings.CurrentsCount;
            this._voltagesCount = settings.VoltagesCount;
            this._discretsCount = settings.DiscretsCount;
            this._channelsCount = settings.ChannelsCount;
        }

        private ushort[][] SetDiscrets(int discrCount, out int increment, out bool b)
        {
            List<ushort[]> discrets = new List<ushort[]>();
            increment = 0;
            b = false;
            int discrWords = discrCount / 16;

            if (_device.DeviceType == "MR741")
            {
                discrets.AddRange(this.DiscretArrayInit(this._countingArray[this._currentsCount + this._voltagesCount]));
                return discrets.ToArray();
            }

            if (discrWords % 16 != 0)
            {
                b = true;
                discrWords++;
            }
            for (int i = 0; i < discrWords; i++)
            {
                if (b && i == discrWords - 1)
                {
                    ushort[][] d = this.DiscretArrayInit(this._countingArray[this._currentsCount + this._voltagesCount + increment]);
                    discrets.AddRange(d.Take(8));
                }
                else
                {
                    discrets.AddRange(this.DiscretArrayInit(this._countingArray[this._currentsCount + this._voltagesCount + increment++]));
                }
            }
            return discrets.ToArray();
        }
        

        private ushort[][] SetChannels(int channelsCount, int increment, bool b)
        {
            List<ushort[]> channels = new List<ushort[]>();
            int channelsWords = /*(double)*/ channelsCount / 16;
            //channelsWords = Math.Round(channelsWords, MidpointRounding.AwayFromZero);
            bool bb;
            if (b)
            {
                bb = (channelsCount - 8) % 16 != 0;
                channelsWords++;
            }
            else
            {
                bb = channelsCount % 16 != 0;
            }
            if (bb)
            {
                channelsWords++;
            }
            if (_device.DeviceType == "MR741")
            {
                channelsWords++;
                channelsWords++;
            }

            for (int i = 0; i < channelsWords; i++)
            {
                if (b && i == 0)
                {
                    ushort[][] d = this.DiscretArrayInit(this._countingArray[this._currentsCount + this._voltagesCount + increment++]);
                    channels.AddRange(d.Skip(8));
                }
                else if (bb && i < channelsWords - 1)
                {
                    ushort[][] d = this.DiscretArrayInit(this._countingArray[this._currentsCount + this._voltagesCount + increment]);
                    channels.AddRange(d.Take(8));
                }
                else
                {
                    channels.AddRange(this.DiscretArrayInit(this._countingArray[this._currentsCount + this._voltagesCount + increment++]));
                }
            }

            return channels.ToArray();
        }

        private void SetCountinCurrents(MeasureTransStruct measure)
        {
            this._currents = new double[_currentsCount][];
            this._baseCurrents = new short[_currentsCount][];
            double[] currentsKoefs = new double[_currentsCount];
            for (int i = 0; i < _currentsCount; i++)
            {
                this._baseCurrents[i] = this._countingArray[i].Select(a => (short)a).ToArray();

                double koef = i == _currentsCount - 1 ? measure.ChannelI.Ittx : measure.ChannelI.Ittl;

                currentsKoefs[i] = koef * 40 * 2 * Math.Sqrt(2) / 65536.0;
                this._currents[i] = this._baseCurrents[i].Select(a => currentsKoefs[i] * a).ToArray();
                this._maxI = Math.Max(this._maxI, this._currents[i].Max());
                this._minI = Math.Min(this._minI, this._currents[i].Min());
            }
            this.CurrentsKoefs = currentsKoefs;
        }

        private void SetCountingVoltage(MeasureTransStruct measure)
        {
            this._voltages = new double[_voltagesCount][];
            this._baseVoltages = new short[_voltagesCount][];
            double[] voltageKoefs = new double[_voltagesCount];

            for (int i = 0; i < _voltagesCount; i++)
            {
                this._baseVoltages[i] = this._countingArray[i + _currentsCount].Select(a => (short)a).ToArray();

                double koef;

                if (_device.Info.Plant == "T4N4D42R35" || _device.Info.Plant == "T4N4D74R67")
                {
                    if (i == _voltagesCount - 1)
                    {
                        koef = measure.ChannelU.KthxValue;
                    }
                    else
                    {
                        koef = measure.ChannelU.KthlValue;
                    }
                }
                else
                {
                    if (i == _voltagesCount - 1)
                    {
                        koef = measure.ChannelU.Kthx1Value;
                    }
                    else if (i == _voltagesCount - 2)
                    {
                        koef = measure.ChannelU.KthxValue;
                    }
                    else
                    {
                        koef = measure.ChannelU.KthlValue;
                    }
                }
                
                voltageKoefs[i] = koef * 2 * Math.Sqrt(2) / 256.0;
                this._voltages[i] = this._baseVoltages[i].Select(a => voltageKoefs[i] * (double)a).ToArray();
                this._maxU = Math.Max(this._maxU, this._voltages[i].Max());
                this._minU = Math.Min(this._minU, this._voltages[i].Min());
            }
            this.VoltageKoefs = voltageKoefs;
        }

        public CountingList(int count)
        {
            this._discrets = new ushort[this._discretsCount][];
            this._channels = new ushort[this._channelsCount][];
            this._currents = new double[this._currentsCount][];
            this._baseCurrents = new short[this._currentsCount][];

            this._voltages = new double[this._voltagesCount][];
            this._baseVoltages = new short[this._voltagesCount][];

            this._count = count;
        }
        #endregion [Ctor's]

        #region [Help members]
        /// <summary>
        /// ���������� ������ ����� � ��������������� ������ ���(�������� 0/1) 
        /// </summary>
        /// <param name="sourseArray">������ �������� ���</param>
        /// <returns></returns>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {

            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        }
        #endregion [Help members]

        public void Save(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine(this._hdrString);
                for (int i = 0; i < this._channelsWithBase.Length; i++)
                {
                    hdrFile.WriteLine("K{0} = {1}, {2}", i + 1, this._channelsWithBase[i].Channel, this._channelsWithBase[i].Base);
                }
                hdrFile.WriteLine($"T{_currentsCount}N{_voltagesCount}");
                hdrFile.WriteLine(1251);
            }

            NumberFormatInfo format = new NumberFormatInfo { NumberDecimalSeparator = "." };

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
            {
                cgfFile.WriteLine($"{_device.Info.Type},1,1991");
                cgfFile.WriteLine($"{this._currents.Length + _voltagesCount + this._discretsCount + this._channelsCount},{this._currents.Length + _voltagesCount}A,{this._discretsCount + this._channelsCount}D");

                int index = 1;
                
                for (int i = 0; i < this._currentsCount; i++)
                {
                    cgfFile.WriteLine("{0},{1},,,A,{2},0,0,-32768,32767,1,1,P", index, this._iNames[i], CurrentsKoefs[i].ToString(format));
                    index++;
                }
                
                for (int i = 0; i < this._voltagesCount; i++)
                {
                    cgfFile.WriteLine("{0},{1},,,V,{2},0,0,-32768,32767,1,1,P", index, this._uNames[i], VoltageKoefs[i].ToString(format));
                    index++;
                }

                for (int i = 0; i < this._discretsCount; i++)
                {
                    string signals = $"�{i + 1}" == Strings.SignaturesForOsc[i] ? null : Strings.SignaturesForOsc[i];
                    if (string.IsNullOrEmpty(signals))
                    {
                        cgfFile.WriteLine("{0},D{1},0", index, i + 1);
                    }
                    else
                    {
                        cgfFile.WriteLine("{0},D{1} ({2}),0", index, i + 1, signals);
                    }
                    index++;
                }

                for (int i = 0; i < this._channelsCount; i++)
                {
                    try
                    {
                        string signals = $"K{i + 1}" == Strings.SignaturesForOsc[i + _discretsCount] ? _channelsWithBase[i].ChannelStr : Strings.SignaturesForOsc[i + _discretsCount];
                        
                        cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, signals);
                        
                        index++;
                    }
                    catch (Exception)
                    {
                        cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, "Error");
                        index++;
                    }
                }
                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this._oscJournalStruct.Len);

                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeAlarm(this._alarm));
                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);
                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < this._count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}", i, i * 1000);
                    foreach (short[] current in this._baseCurrents)
                    {
                        datFile.Write(",{0}", current[i]);
                    }
                    for (int j = 0; j < this._voltagesCount; j++)
                    {
                        datFile.Write(",{0}", this._baseVoltages[j][i]);
                    }
                    
                    foreach (ushort[] discret in this._discrets)
                    {
                        datFile.Write(",{0}", discret[i]);
                    }
                    foreach (ushort[] chanel in this._channels)
                    {
                        datFile.Write(",{0}", chanel[i]);
                    }
                    datFile.WriteLine();
                }
            }
        }

        public bool IsLoad { get; private set; }
        public string FilePath { get; private set; }
        public CountingList Load(string filePath, MRUniversal device)
        {
            this._device = device;

            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string hdrString = hdrStrings[0];

            OscopeAllChannelsStruct.SetDeviceChannelsType(_device.Info.Plant);
            this._channelsCount = this._device.AllChannels.Value.Rows.Length;

            InputLogicStruct.SetDeviceDiscretsType(_device.DevicePlant);
            _discretsCount = InputLogicStruct.DiscrestCount;

            ChannelWithBase[] channelWithBase = new ChannelWithBase[this._channelsCount];
            for (int i = 0; i < this._channelsCount; i++)
            {
                string[] channelAndBase =
                    hdrStrings[1 + i].Split(new[] { '=', ' ' }, StringSplitOptions.RemoveEmptyEntries)[1]
                        .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                channelWithBase[i] = new ChannelWithBase
                {
                    Channel = Convert.ToUInt16(channelAndBase[0]),
                    Base = channelAndBase.Length > 1 ? Convert.ToByte(channelAndBase[1]) : (byte)0
                };
            }

            string[] analogChannels = hdrStrings[hdrStrings.Length - 2].Split(new[] { 'T', 'N' }, StringSplitOptions.RemoveEmptyEntries);
            int currentsCount = int.Parse(analogChannels[0]);
            int voltagesCount = int.Parse(analogChannels[1]);

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);
            double[] factors = new double[currentsCount + voltagesCount];
            for (int i = 2; i < 2 + currentsCount + voltagesCount; i++)
            {
                string res = cfgStrings[i].Split(',')[5];
                NumberFormatInfo format = new NumberFormatInfo { NumberDecimalSeparator = "." };
                factors[i - 2] = double.Parse(res, format);
            }
            
            int indexCounts = 2 + currentsCount + voltagesCount + _discretsCount + this._channelsCount + 2;
            int counts = int.Parse(cfgStrings[indexCounts].Replace("1000,", string.Empty));
            int alarm = 0;
            try
            {
                string startTime = cfgStrings[indexCounts + 1].Split(',')[1];
                string runTime = cfgStrings[indexCounts + 2].Split(',')[1];

                TimeSpan a = DateTime.Parse(runTime) - DateTime.Parse(startTime);
                alarm = (int)a.TotalMilliseconds;
            }
            catch (Exception)
            {
                // ignored
            }
            CountingList result = new CountingList(counts) { _alarm = alarm };
            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);

            double[][] currents = new double[currentsCount][];
            double[][] voltages = new double[voltagesCount][];
            ushort[][] discrets = new ushort[this._discretsCount][];
            ushort[][] channels = new ushort[this._channelsCount][];

            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new double[counts];
            }

            for (int i = 0; i < voltages.Length; i++)
            {
                voltages[i] = new double[counts];
            }

            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[counts];
            }

            for (int i = 0; i < channels.Length; i++)
            {
                channels[i] = new ushort[counts];
            }

            for (int i = 0; i < datStrings.Length; i++)
            {
                string[] means = datStrings[i].Split(',');
                for (int j = 0; j < currentsCount; j++)
                {
                    currents[j][i] = Convert.ToDouble(means[j + 2], CultureInfo.InvariantCulture) * factors[j];
                }

                for (int j = 0; j < voltagesCount; j++)
                {
                    voltages[j][i] = Convert.ToDouble(means[j + 2 + currentsCount], CultureInfo.InvariantCulture) * factors[j + currentsCount];
                }

                for (int j = 0; j < this._discretsCount; j++)
                {
                    discrets[j][i] = Convert.ToUInt16(means[j + 2 + currentsCount + voltagesCount]);
                }

                for (int j = 0; j < this._channelsCount; j++)
                {
                    channels[j][i] = Convert.ToUInt16(means[j + 2 + currentsCount + voltagesCount + this._discretsCount]);
                }
            }

            result._maxI = double.MinValue;
            result._minI = double.MaxValue;
            for (int i = 0; i < currentsCount; i++)
            {
                result._maxI = Math.Max(result._maxI, currents[i].Max());
                result._minI = Math.Min(result._minI, currents[i].Min());
            }

            result._maxU = double.MinValue;
            result._minU = double.MaxValue;
            for (int i = 0; i < voltagesCount; i++)
            {
                result._maxU = Math.Max(result._maxU, voltages[i].Max());
                result._minU = Math.Min(result._minU, voltages[i].Min());
            }

            result._currents = currents;
            result._channels = channels;
            result._discrets = discrets;
            result._voltages = voltages;
            result._channelsWithBase = channelWithBase;
            result._hdrString = hdrString;
            result.IsLoad = true;
            result.FilePath = filePath;
            return result;
        }
    }
}
