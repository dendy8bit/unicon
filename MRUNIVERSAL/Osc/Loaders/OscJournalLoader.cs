﻿using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MRUNIVERSAL.Osc.Structures;
using System;
using System.Collections.Generic;

namespace BEMN.MRUNIVERSAL.Osc.Loaders
{
    /// <summary>
    /// Загружает журнал осцилограммы
    /// </summary>
    public class OscJournalLoader
    {
        #region [Events]
        /// <summary>
        /// Успешно прочитаны все записи
        /// </summary>
        public event Action AllJournalReadOk;
        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action AllJournalReadFail;
        #endregion [Events]

        #region [Fields]
        /// <summary>
        /// все записи журнала осциллграфа
        /// </summary>
        private readonly MemoryEntity<OscJournalStruct> _oscJournalStruct;
        /// <summary>
        /// С версии 1.07 возможность читать сразу все записи журнала осциллграфа
        /// </summary>
        private readonly MemoryEntity<AllOscJournalStruct> _allOscJournalStruct;
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _refreshOscJournal;
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        private readonly List<OscJournalStruct> _oscRecords;
        /// <summary>
        /// Текущий номер записи журнала осциллографа
        /// </summary>
        private int _recordNumber;
        #endregion [Fields]

        #region [Properties]
        /// <summary>
        /// Количество записей в журнале осциллографа
        /// </summary>
        public int OscCount => this._oscRecords.Count;
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<OscJournalStruct> OscRecords => this._oscRecords;
        public object[] GetRecord => this._oscJournalStruct.Value.GetRecord;
        #endregion [Properties]

        #region [Ctor's]
        /// <summary>
        /// Загрузчик журнала осциллографа
        /// </summary>
        public OscJournalLoader(MemoryEntity<AllOscJournalStruct> allOscJournalStruct, MemoryEntity<OscJournalStruct> oscJournalStruct, MemoryEntity<OneWordStruct> refreshOscJournal)
        {
            this._oscRecords = new List<OscJournalStruct>();
            this._allOscJournalStruct = allOscJournalStruct;
            this._oscJournalStruct = oscJournalStruct;

            if (this._allOscJournalStruct != null)
            {
                this._allOscJournalStruct = allOscJournalStruct;
                this._allOscJournalStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadRecords);
                this._allOscJournalStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
            }
            if (this._oscJournalStruct != null)
            {
                this._oscJournalStruct = oscJournalStruct;
                this._oscJournalStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadRecords);
                this._oscJournalStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
            }
            
            // Запись индекса ЖО
            this._refreshOscJournal = refreshOscJournal;
            this._refreshOscJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this.LoadStruct);
            this._refreshOscJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
        }
        #endregion [Ctor's]

        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            this.AllJournalReadFail?.Invoke();
        }

        private void LoadStruct()
        {
            this._allOscJournalStruct?.LoadStruct();
            this._oscJournalStruct?.LoadStruct();
        }
        
        /// <summary>
        /// Запуск чтения журнала осциллографа
        /// </summary>
        public void StartReadJournal()
        {
            this._recordNumber = 0;
            OscJournalStruct.RecordIndex = 0;
            this.SaveIndex();
        }

        private void ReadRecords()
        {
            if (this._allOscJournalStruct != null)
            {
                if (this._allOscJournalStruct.Value.AllJournalStructs.Count != 0)
                {
                    this._oscRecords.AddRange(this._allOscJournalStruct.Value.AllJournalStructs);
                    this._recordNumber += this._allOscJournalStruct.Value.AllJournalStructs.Count;
                    this.SaveIndex();
                }
                else
                {
                    this.AllJournalReadOk?.Invoke();
                }
            }
            else
            {
                if (!this._oscJournalStruct.Value.IsEmpty)
                {
                    this._oscRecords.Add(this._oscJournalStruct.Value.Clone<OscJournalStruct>());
                    this._recordNumber++;
                    this.SaveIndex();
                }
                else
                {
                    this.AllJournalReadOk?.Invoke();
                }                
            }
        }

        public void ClearEvents()
        {
            if (this._allOscJournalStruct != null)
            {
                this._allOscJournalStruct.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this.ReadRecords);
                this._allOscJournalStruct.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
            }
            if (this._oscJournalStruct != null)
            {
                this._oscJournalStruct.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this.ReadRecords);
                this._oscJournalStruct.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
            }
            this._refreshOscJournal.AllWriteOk -= HandlerHelper.CreateReadArrayHandler(this.LoadStruct);
            this._refreshOscJournal.AllWriteFail -= HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
        }

        internal void Clear()
        {
            this._oscRecords.Clear();
        }

        /// <summary>
        /// Запись номера читаемой осциллограммы
        /// </summary>
        private void SaveIndex()
        {
            this._refreshOscJournal.Value.Word = (ushort)this._recordNumber;
            this._refreshOscJournal.SaveStruct6();
        }
    }
}
