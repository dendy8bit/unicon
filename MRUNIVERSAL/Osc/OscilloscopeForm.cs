using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Forms;
using BEMN.Forms.ValidatingClasses;
using BEMN.Interfaces;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Ls;
using BEMN.MRUNIVERSAL.Configuration.Structures.MeasuringTransformer;
using BEMN.MRUNIVERSAL.Configuration.Structures.Oscope;
using BEMN.MRUNIVERSAL.Osc.HelpClasses;
using BEMN.MRUNIVERSAL.Osc.Loaders;
using BEMN.MRUNIVERSAL.Osc.Structures;
using BEMN.MRUNIVERSAL.Properties;

namespace BEMN.MRUNIVERSAL.Osc
{
    public partial class OscilloscopeForm : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "�������������";
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string RECORDS_IN_JOURNAL = "������������ � ������� - {0}";
        private const string JOURNAL_IS_EMPTY = "������ ������������ ����";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string READ_OSC_STOPPED = "������ ������������� ����������";
        private const string READ_OSC_ERROR = "������ ������ �������������";
        private const string LIST_FILE_NAME_FOR_OSC = "oscsign.xml";
        private const string LIST_FILE_NAME = "commonp.xml";
        #endregion [Constants]

        #region [Fields]
        /// <summary>
        /// ������ �������������
        /// </summary>
        private CountingList _countingList;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscPageLoader _pageLoader;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscJournalLoader _oscJournalLoader;
        /// <summary>
        /// ��������� ������� ����� � ����������
        /// </summary>
        private readonly CurrentOptionsLoader _currentOptionsLoaderOsc;

        private readonly MemoryEntity<OscOptionsStruct> _oscilloscopeSettings;
        private readonly MemoryEntity<OscopeAllChannelsStruct> _oscopeAllChannel;

        private readonly MRUniversal _device;
        private readonly DataTable _table;

        private FileDriver _fileDriver;

        private readonly Process shlupic = new Process();

        private OscJournalStruct _journalStruct; // ���������������� ��� ������� �� ������ "��������� �������������"
        #endregion [Fields]

        #region [Properties]
        /// <summary>
        /// ���������� ����������� ������� ������������ ��� ������
        /// </summary>
        private bool CanSelectOsc
        {
            get => this._oscilloscopeCountCb.Enabled;
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
        }

        /// <summary>
        /// ������ �������������
        /// </summary>
        private CountingList CountingList
        {
            get => this._countingList ?? (this._countingList = new CountingList(0));
            set
            {
                this._countingList = value;
                this._oscShowButton.Enabled = true;
            }
        }
        #endregion [Properties]

        #region [Ctor's]
        public OscilloscopeForm()
        {
            this.InitializeComponent();
        }

        public OscilloscopeForm(MRUniversal device)
        {
            this.InitializeComponent();
            this._device = device;

            // �������� �� �������, ����� ������� ���������� 
            this.shlupic.Exited += this.ShlOnExited;

            Action failReadOscJournalDelegate = HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);

            _fileDriver = new FileDriver(_device, this);

            Strings.RelaySignals = Strings.DefaultRelaySignals;
            Strings.VlsSignals = Strings.DefaultVLSSignals;

            InputLogicStruct.SetDeviceDiscretsType(device.Info.Plant);
            OscopeAllChannelsStruct.SetDeviceChannelsType(device.Info.Plant);
            Strings.SignaturesForOsc = Strings.DefaultSignaturesForOsc;
            
            // ��������� �������
            this._pageLoader = new OscPageLoader(this._device.SetOscStartPage, this._device.OscPage);
            this._pageLoader.PageRead += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadStopped += HandlerHelper.CreateActionHandler(this, this.ReadStop);
            
            // ��������� �������
            this._oscJournalLoader = new OscJournalLoader(this._device.AllOscJournal, this._device.OscJournal, this._device.RefreshOscJournal);
            this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.ReadAllRecords);
            this._oscJournalLoader.AllJournalReadFail += HandlerHelper.CreateActionHandler(this, this.OnAllJournalReadOk);               
            
            // ������� ������������
            this._oscilloscopeSettings = this._device.OscOptions;
            this._oscilloscopeSettings.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._oscJournalLoader.StartReadJournal);
            this._oscilloscopeSettings.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);

            // ������ ������������
            this._oscopeAllChannel = this._device.AllChannels;
            this._oscopeAllChannel.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._oscilloscopeSettings.LoadStruct);
            this._oscopeAllChannel.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);

            // ��������� ������� �����
            this._currentOptionsLoaderOsc = device.CurrentOptionsLoaderOsc;
            this._currentOptionsLoaderOsc.LoadOk += this._oscopeAllChannel.LoadStruct;
            this._currentOptionsLoaderOsc.LoadFail += failReadOscJournalDelegate;

            this._table = this.GetJournalDataTable();
        }
        #endregion [Ctor's]

        #region [Methods]
        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._oscJournalReadButton.Enabled = true;
            this._oscilloscopeCountCb.Enabled = true;
            this._statusLabel.Text = READ_OSC_FAIL;
        }

        private void ReadStop()
        {
            this._statusLabel.Text = this._pageLoader.Error ? READ_OSC_ERROR : READ_OSC_STOPPED;
            this._stopReadOsc.Enabled = false;
            this._oscJournalReadButton.Enabled = true;
            this._oscilloscopeCountCb.Enabled = true;
            this._oscReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
            this._oscProgressBar.Value = 0;
        }

        /// <summary>
        /// �������� ���� ������
        /// </summary>
        private void OnAllJournalReadOk()
        {
            if (this._oscJournalLoader.OscCount == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
            }
            this._oscJournalReadButton.Enabled = true;
            this._oscilloscopeCountCb.Enabled = true;
        }

        /// <summary>
        /// �������� ���� ������
        /// </summary>
        private void ReadAllRecords()
        {
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
            this._oscilloscopeCountCb.Enabled = true;
            if (this._oscJournalLoader.OscCount == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
                return;
            }

            for (int i = 1; i <= this._oscJournalLoader.OscCount; i++)
            {
                this._oscilloscopeCountCb.Items.Add(i);
            }
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._oscJournalLoader.OscCount);

            foreach (OscJournalStruct record in this._oscJournalLoader.OscRecords)
            {
                this._table.Rows.Add(record.GetRecord);
                OscJournalStruct.RecordIndex++;
            }
            this._oscReadButton.Enabled = true;
            this._oscJournalDataGrid.Refresh();
            this._oscilloscopeCountCb.SelectedIndex = 0;

            //if (!string.IsNullOrEmpty(Framework.Framework.PasswordForSignatures))
            //{
            //    _fileDriver.Password = Framework.Framework.PasswordForSignatures;

            //    _fileDriver.ReadFile(SignaturesListRead, LIST_FILE_NAME);
            //}
            //else
            //{
            //    MessageBox.Show("��� ������ �������� ��������, ��������� ������ ������ � ���������� �������!",
            //        "��������!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }

        private void SignaturesListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "�������� ������� ���������")
                {
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            XmlRootAttribute root = new XmlRootAttribute(_device.DeviceType);
                            XmlSerializer serializer = new XmlSerializer(typeof(ListsOfConfigurations), root);
                            ListsOfConfigurations lists = (ListsOfConfigurations)serializer.Deserialize(reader);

                            AddedItemInList(lists.SignalsList.MessagesList, Strings.RelaySignals);
                            AddedItemInListVLS(lists.SignalsList.MessagesList, Strings.VlsSignals);

                            _fileDriver.ReadFile(SignaturesListReadForOsc, LIST_FILE_NAME_FOR_OSC);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("������ ������ �������� ��������. " + mes + "\n������ ����� ������������� �� ���������.", "��������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this._statusLabel.Text = "������ ������ �������� ��������";
            }

            if (readBytes.Length != 0)
            {
                try
                {

                }
                catch (Exception ex)
                {
                }
            }
        }

        private void SignaturesListReadForOsc(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "�������� ������� ���������")
                {
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            XmlRootAttribute root = new XmlRootAttribute(_device.DeviceType);
                            XmlSerializer serializer = new XmlSerializer(typeof(SignaturesForOsc), root);
                            SignaturesForOsc lists = (SignaturesForOsc)serializer.Deserialize(reader);

                            Strings.SignaturesForOsc = lists.Signatures.MessagesList;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("������ ������ �������� ��������. " + mes + "\n������ ����� ������������� �� ���������.", "��������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this._statusLabel.Text = "������ ������ �������� ��������";
            }

            if (readBytes.Length != 0)
            {
                try
                {

                }
                catch (Exception ex)
                {
                }
            }
        }

        public void AddedItemInList(List<string> signalsList, Dictionary<ushort, string> dictionary)
        {
            dictionary?.Clear();

            dictionary?.Add(0, "���");

            for (ushort i = 1; i <= signalsList.Count * 2; i++)
            {
                try
                {
                    if ((i % 2) == 0 && i != 1)
                    {
                        dictionary.Add(i, signalsList[(i - 1) / 2].Trim() + " ���.");
                    }
                    else
                    {
                        dictionary.Add(i, signalsList[i / 2].Trim());
                    }
                }
                catch (Exception exception)
                {

                }
            }
        }

        public void AddedItemInListVLS(List<string> signalsList, Dictionary<int, string> dictionary)
        {
            dictionary?.Clear();

            for (ushort i = 0; i < signalsList.Count; i++)
            {
                if (i > 72 && i < 89 || i == 0) continue;

                try
                {
                    dictionary.Add(i, signalsList[i].Trim());
                }
                catch (Exception exception)
                {

                }
            }
        }

        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            int group = this._journalStruct.GroupIndex;
            
            try
            {
                this.CountingList = new CountingList(this._device, this._pageLoader.ResultArray, this._journalStruct, this._currentOptionsLoaderOsc[group], _oscopeAllChannel.Value.Rows/*, this._currentOptionsLoader.Connections[group].Value*/);
            }
            catch (Exception e)
            {
                MessageBox.Show("������ ������������� ���������� ��� �������", "��������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this._oscReadButton.Enabled = true;
            this._oscJournalReadButton.Enabled = true;
            this._oscilloscopeCountCb.Enabled = true;
            this._oscSaveButton.Enabled = true;
            this._stopReadOsc.Enabled = false;
            this._oscLoadButton.Enabled = true;
            this._oscProgressBar.Value = this._oscProgressBar.Maximum;
        }
        
        private DataTable GetJournalDataTable()
        {
            DataTable table = new DataTable("��_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }

        private void KillProces()
        {
            try
            {
                foreach (var process in Process.GetProcesses())
                {
                    if (process.ProcessName == "shlpicr")
                    {
                        process.Kill();
                    }
                }
            }
            catch (Exception e)
            {
                this._startProcessButton.Enabled = true;
                MessageBox.Show(e.Message);
            }
        }

        private void CreateProcess()
        {
            this._startProcessButton.Enabled = false;

            if (DialogResult.OK == _fileProcessDialog.ShowDialog())
            {
                try
                {
                    this._device.MB.Port.Close();
                    this.KillProces();

                    string fileName = _fileProcessDialog.FileName;
                    string finalPath = @"EMUL " +
                                       this._device.PortNum +
                                       " " +
                                       this._synhComboBox.SelectedItem +
                                       " " +
                                       "\"" +
                                       fileName +
                                       "\"";

                    this.shlupic.StartInfo.FileName = "shlpicr.exe";
                    this.shlupic.StartInfo.CreateNoWindow = true;
                    this.shlupic.StartInfo.UseShellExecute = true;
                    this.shlupic.StartInfo.Arguments = finalPath;
                    this.shlupic.EnableRaisingEvents = true;
                    this.shlupic.Start();
                }
                catch (Exception e)
                {
                    this._startProcessButton.Enabled = true;
                    MessageBox.Show(e.Message);
                }
            }
        }
        /// <summary>
        /// �������� �����
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode) this.StartRead();
        }

        private void OscilloscopeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._pageLoader.ClearEvents();
            this._oscJournalLoader.ClearEvents();
        }

        /// <summary>
        /// �������� �������������
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (this.CountingList == null)
            {
                MessageBox.Show("������������� �� ���������");
                return;
            }
            try
            {
                if (Validator.GetVersionFromRegistry())
                {
                    string fileName;
                    if (this._countingList.IsLoad)
                    {
                        fileName = this._countingList.FilePath;
                    }
                    else
                    {
                        fileName = Validator.CreateOscFileNameCfg($"������������� {Strings.DeviceName} {_device.Info.Plant} v{this._device.DeviceVersion.Replace(".", "_")} {this._journalStruct.GetDate.Replace(".", "_")} {this._journalStruct.GetTime.Replace(":", "_").Replace(".", "_")}");
                        this._countingList.Save(fileName);
                    }
                    string oscPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe");
                    Process.Start(oscPath, fileName);
                }
                else
                {
                    if (!Settings.Default.OscFlag)
                    {
                        LoadNetForm loadNetForm = new LoadNetForm();
                        loadNetForm.ShowDialog(this);
                        Settings.Default.OscFlag = loadNetForm.OscFlag;
                        Settings.Default.Save();
                    }

                    throw new Exception();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            this._oscJournalReadButton.Enabled = false;
            this._oscLoadButton.Enabled = false;
            this._oscilloscopeCountCb.Items.Clear();
            this._oscilloscopeCountCb.SelectedIndex = -1;
            this._table.Clear();
            this._oscJournalLoader.Clear();
            this._oscJournalDataGrid.Refresh();
            this.CanSelectOsc = false;
            this._currentOptionsLoaderOsc.StartRead();
        }
        
        /// <summary>
        /// ��������� �������������
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStruct = this._oscJournalLoader.OscRecords[selectedOsc]; 
            this._pageLoader.StartRead(this._journalStruct, this._oscilloscopeSettings.Value);

            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoader.PagesCount;
            
            // �������� ����������� ���������� ������ ������������
            this._oscLoadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscReadButton.Enabled = false;
            this._oscilloscopeCountCb.Enabled = false;
            this._stopReadOsc.Enabled = true;
            this._oscJournalReadButton.Enabled = false;
        }

        /// <summary>
        /// ��������� ������������� � ����
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        {
            this._saveOscilloscopeDlg.FileName = $"������������� {_device.DeviceType} �.{_device.DeviceVersion} �� {this._journalStruct.GetDate} ����� {this._journalStruct.GetTime.Replace(":", ".")}";
            if (this._saveOscilloscopeDlg.ShowDialog() != DialogResult.OK) return;
            try
            {
                this._countingList.Save(this._saveOscilloscopeDlg.FileName);
                this._statusLabel.Text = "������������ ���������";
            }
            catch (Exception)
            {
                this._statusLabel.Text = "������ ����������";
            }
        }

        /// <summary>
        /// ��������� ������������� �� �����
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK) return;
            try
            {
                this.CountingList = this.CountingList.Load(this._openOscilloscopeDlg.FileName, this._device);
                this._statusLabel.Text = $"������������ ��������� �� ����� {this._openOscilloscopeDlg.FileName}";
                this._oscSaveButton.Enabled = false;
                this._stopReadOsc.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = "���������� ��������� ������������";
            }
        }

        /// <summary>
        /// ���������� ������ ������������
        /// </summary>
        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._pageLoader.StopRead();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscBeginColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this.checkBox1.Checked;
        }

        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            //DataGridView dataGridView = sender as DataGridView;
            //this._oscilloscopeCountCb.SelectedIndex = dataGridView.SelectedRows[0].Index;
            this._oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }

        private void _oscilloscopeCountCb_SelectedIndexChanged(object sender, EventArgs e)
        {
            var temp = sender as ComboBox;
            int index = temp.SelectedIndex;
            this._oscJournalDataGrid.ClearSelection();
            this._oscJournalDataGrid.Rows[index].Selected = true;
        }

        private void ShlOnExited(object sender, EventArgs eventArgs)
        {
            MessageBox.Show("������� ��������");
            if (this._device.MB.Port.Opened) return;
            this._device.MB.Port.Open();
            this._startProcessButton.Invoke((MethodInvoker) delegate { this._startProcessButton.Enabled = true; });
        }

        private void _startProcessButton_Click(object sender, EventArgs e)
        {
            CreateProcess();
        }
        #endregion [Methods]

        #region [IFormView Members]
        public Type FormDevice => typeof(MRUniversal);
        public bool Multishow { get; private set; }
        public Type ClassType => typeof(OscilloscopeForm);
        public bool ForceShow => false;
        public Image NodeImage => Resources.oscilloscope.ToBitmap();
        public string NodeName => OSC;
        public INodeView[] ChildNodes => new INodeView[] { };
        public bool Deletable => false;
        #endregion [IFormView Members]
    }
}