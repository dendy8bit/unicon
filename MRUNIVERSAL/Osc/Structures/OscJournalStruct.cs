﻿using System;
using System.Globalization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MRUNIVERSAL.Osc.Structures
{
    public class OscJournalStruct : StructBase
    {
        private const int OSCLEN = 1024; // Размер одной страницы 
        public static int RecordIndex;

        #region [Private fields]
        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        [Layout(7)] private ushort _reserv;
        [Layout(8)] private int _ready;             // Если 0 - осциллограмма готова
        [Layout(9)] private int _point;             // Адрес начала блока текущей осциллограммы в массиве данных (в словах)
        [Layout(10)] private int _begin;            // Адрес аварии в массиве данных (в словах)
        [Layout(11)] private int _len;              // Размер осциллограммы (в отсчётах)
        [Layout(12)] private int _after;            // Размер после аварии (в отсчётах)
        [Layout(13)] private ushort _numberDefence; // Номер (последней) сработавшей защиты 
        [Layout(14)] private ushort _sizeReference; // Размер одного отсчёта (в словах)
        #endregion [Private fields]

        #region [Properties]
        public string Stage
        {
            get
            {
                int stageIndex = Common.GetBits(this._numberDefence, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
                return Strings.AlarmJournalStage.Count >= stageIndex
                    ? Strings.AlarmJournalStage[stageIndex]
                    : stageIndex.ToString();
            }
        }

        public ushort GroupIndex => (ushort)(Common.GetBits(this._numberDefence, 12, 13, 14, 15) >> 12);
        public string Group => Validator.Get(this.GroupIndex, Strings.GroupsNames);

        /// <summary>
        /// Запись журнала осциллографа
        /// </summary>
        public object[] GetRecord
        {
            get
            {
                return new object[]
                    {
                        this.GetNumber,     // Номер
                        this.GetDate,       // Дата
                        this.GetTime,       // Время
                        this.Group,         // Группа
                        this.Stage,         // Ступень
                        this.Len,           // Размер
                        this._ready,        // Готовность
                        this.Point,         // Адрес начала
                        this.GetEnd,        // Адрес конца
                        this.Begin,         // Смещение
                        this.After,         // Размер после аварии
                        this.SizeReference  // Размер одного отсчёта
                    };
            }
        }

        /// <summary>
        /// Пустая запись (если осциллограмма не готова также признак конца журнала)
        /// </summary>
        public bool IsEmpty => this.SizeReference == 0 | this._ready != 0;

        /// <summary>
        /// Начальная страница осциллограммы
        /// </summary>
        public ushort OscStartIndex => (ushort)(this.Point / OSCLEN);

        /// <summary>
        /// Номер сообщения
        /// </summary>
        private string GetNumber
        {
            get
            {
                string result = (RecordIndex + 1).ToString();
                if (RecordIndex == 0)
                {
                    result += "(посл.)";
                }
                return result;
            }
        }

        /// <summary>
        /// Время сообщения
        /// </summary>
        public string GetTime => $"{this._hour:d2}:{this._minute:d2}:{this._second:d2}.{this._millisecond:d2}";

        /// <summary>
        /// Дата сообщения
        /// </summary>
        public string GetDate => $"{this._date:d2}.{this._month:d2}.{this._year:d2}";

        public string GetFormattedDateTimeAlarm(int alarm)
        {
            try
            {
                DateTime a = new DateTime(this._year, this._month, this._date, this._hour, this._minute, this._second, this._millisecond);
                DateTime result = a.AddMilliseconds(-alarm);
                return $"{result.Month:d2}/{result.Day:d2}/{result.Year:d2},{result.Hour:d2}:{result.Minute:d2}:{result.Second:d2}.{result.Millisecond:d3}";
            }
            catch (Exception ex)
            {
                return this.GetFormattedDateTime;
            }
        }

        public string GetFormattedDateTime => $"{this._month:d2}/{this._date:d2}/{this._year:d2},{this._hour:d2}:{this._minute:d2}:{this._second:d2}.{this._millisecond:d3}";

        private static int OscSize = 0x77FF4;

        /// <summary>
        /// Адрес конца
        /// </summary>
        private string GetEnd
        {
            get
            {
                int num = OscSize;
                int num2 = this.Point + (this.Len * this.SizeReference);
                return num2 > num ? $"{num2} [{num2 - num}]" : num2.ToString(CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// "LEN" Размер осциллограммы (в отсчётах)
        /// </summary>
        public int Len => this._len;

        /// <summary>
        /// "AFTER" Размер после аварии (в отсчётах)
        /// </summary>
        public int After => this._after;

        /// <summary>
        /// "BEGIN" Адрес аварии в массиве данных (в словах)
        /// </summary>
        public int Begin => this._begin;

        /// <summary>
        /// "POINT" Адрес начала блока текущей осциллограммы в массиве данных (в словах)
        /// </summary>
        public int Point => this._point;

        /// <summary>
        /// "REZ" Размер одного отсчёта (в словах)
        /// </summary>
        public ushort SizeReference => this._sizeReference; 

        #endregion [Private Properties]
    }
}
