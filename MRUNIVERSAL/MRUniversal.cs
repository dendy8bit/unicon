﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms;
using BEMN.Forms.DisplayInfoDictionary;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MBServer.Queries;
using BEMN.MRUNIVERSAL.AlarmJournal;
using BEMN.MRUNIVERSAL.AlarmJournal.Structures;
using BEMN.MRUNIVERSAL.BSBGL;
using BEMN.MRUNIVERSAL.Configuration;
using BEMN.MRUNIVERSAL.Configuration.Structures;
using BEMN.MRUNIVERSAL.Configuration.Structures.MeasuringTransformer;
using BEMN.MRUNIVERSAL.Configuration.Structures.Oscope;
using BEMN.MRUNIVERSAL.Emulation;
using BEMN.MRUNIVERSAL.Emulation.Structures;
using BEMN.MRUNIVERSAL.FaceSideDevice;
using BEMN.MRUNIVERSAL.Measuring;
using BEMN.MRUNIVERSAL.Measuring.Structures;
using BEMN.MRUNIVERSAL.Osc;
using BEMN.MRUNIVERSAL.Osc.Structures;
using BEMN.MRUNIVERSAL.SystemJournal;
using BEMN.MRUNIVERSAL.SystemJournal.Structures;
using BEMN.MRUNIVERSAL.Diagnostics;
using BEMN.MRUNIVERSAL.FileSharingService;

namespace BEMN.MRUNIVERSAL
{
    public class MRUniversal : Device, IDeviceView, IDeviceVersion
    {
        public const int BAUDE_RATE_MAX = 921600;
        private const ushort START_ADDR_MEAS_TRANS = 0x1318;
        private ushort _groupSetPointSize;
        private const string ERROR_SAVE_CONFIG = "Ошибка сохранения конфигурации";
        private const string WRITE_OK = "Конфигурация записана в устройство";

        /// <summary>
        /// Стартовый адрес структуры MeasureTransStruct
        /// </summary>
        public ushort GetStartAddrMeasTrans(int group)
        {
            if (string.IsNullOrEmpty(DeviceVersion)) return START_ADDR_MEAS_TRANS;
            return (ushort)(START_ADDR_MEAS_TRANS + this._groupSetPointSize * group);
        }
        public MRUniversal()
        {
            HaveVersion = true;
        }
        public MRUniversal(Modbus mb)
        {
            this.MB = mb;
            HaveVersion = true;
            this.InitMemoryEntity();
        }

        public void InitMemoryEntity()
        {
            int slotLen = this.MB.BaudeRate == BAUDE_RATE_MAX ? 1024 : 64;

            // ЖС
            if (slotLen == 1024)
            {
                this.AllSystemJournal1024 = new MemoryEntity<AllSystemJournalStruct1024>("Журнал системы", this, 0x0600, slotLen);
            }
            else
            {
                this.AllSystemJournal64 = new MemoryEntity<AllSystemJournalStruct64>("Журнал системы", this, 0x0600);
            }
            this.IndexJS = new MemoryEntity<OneWordStruct>("Индекс ЖС", this, 0x0600);
            
            // Конфигурация
            this.Configuration = new MemoryEntity<ConfigurationStruct>("Конфигурация", this, 0x1000, slotLen);
            this.Configuration771 = new MemoryEntity<ConfigurationStruct771>("Конфигурация", this, 0x1000, slotLen);

            // Измерения
            this.MeasureTrans = new MemoryEntity<MeasureTransStruct>("Параметры измерений", this, START_ADDR_MEAS_TRANS);
            this.GroupUstavki = new MemoryEntity<OneWordStruct>("Группа уставок", this, 0x0400);
            this.Imin = new MemoryEntity<OneWordStruct>("Минимально отображаемый ток", this, 0xFC05);
            this.DateTime = new MemoryEntity<DateTimeStruct>("Время и дата в устройстве", this, 0x0200);
            this.AnalogDataBase = new MemoryEntity<AnalogDataBaseStruct>("Аналоговая база данных", this, 0x0E00, slotLen);
            this.DiscretDataBase = new MemoryEntity<DiscretDataBaseStruct>("Дискретная база данных", this, 0x0D00);

            // Осциллограф
            if (slotLen == 1024)
            {
                this.AllOscJournal = new MemoryEntity<AllOscJournalStruct>("Журнал осциллографа (весь)", this, 0x0800, slotLen);
            }
            else
            {
                this.OscJournal = new MemoryEntity<OscJournalStruct>("Журнал осциллографа", this, 0x0800);
            }
            this.SetOscStartPage = new MemoryEntity<OneWordStruct>("Установка стартовой страницы осциллограммы", this, 0x900);
            this.OscOptions = new MemoryEntity<OscOptionsStruct>("Параметры осциллографа", this, 0x05A0);
            this.AllChannels = new MemoryEntity<OscopeAllChannelsStruct>("Все каналы осциллографа", this, 0x24D4, slotLen);
            this.OscPage = new MemoryEntity<OscPage>("Страница осциллографа", this, 0x900, slotLen);
            this.RefreshOscJournal = new MemoryEntity<OneWordStruct>("Индекс журнала осциллографа", this, 0x0800);
            this.CurrentOptionsLoaderOsc = new CurrentOptionsLoader(this);

            // ЖА
            if (slotLen == 1024)
            {
                this.AllAlarmJournal = new MemoryEntity<AllAlarmJournalStruct>("Журнал аварий", this, 0x0700, slotLen);
            }
            else
            {
                this.AlarmJournal = new MemoryEntity<AlarmJournalStruct>("Запись журнала аварий", this, 0x0700);
            }
            this.SetPageAlarmJournal = new MemoryEntity<OneWordStruct>("Страница ЖА", this, 0x0700);
            this.CurrentOptionsLoaderAJ = new CurrentOptionsLoader(this);
            this.AlarmJournalSize = new MemoryEntity<OneWordStruct>("Размер одной записи ЖА", this, 0x05AE); // не используется

            // Лицевая сторона
            this.FaceSizeDictionary = new DisplayInfoDictionary(Info.IsAz);

            // Эмуляция
            this._writeSructEmulation = new MemoryEntity<WriteStructEmul>("Запись аналоговых сигналов ", this, 0x5800, slotLen);
            this._writeSructEmulationNull = new MemoryEntity<WriteStructEmul>("Запись нулевых аналоговых сигналов ", this, 0x5800, slotLen);
            this._readSructEmulation = new MemoryEntity<ReadStructEmul>("Чтение времени и статуса эмуляции", this, 0x583A, slotLen);

            // Программирование
            this.SourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0x4300, slotLen);
            this.ProgramStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0E00);
            this.ProgramSignalsStruct = new MemoryEntity<LogicProgramSignals>("LoadProgramSignals_", this, 0x4100, slotLen);
            this.ProgramPageStruct = new MemoryEntity<OneWordStruct>("SaveProgrammPage", this, 0x4000, slotLen);
            this.StopSpl = new MemoryEntity<OneWordStruct>("Останов логической программы", this, 0x0D08);
            this.StartSpl = new MemoryEntity<OneWordStruct>("Старт логической программы", this, 0x0D09);
            this.StateSpl = new MemoryEntity<SomeStruct>("Состояние ошибок логики", this, 0x0D17);
            ushort[] values = new ushort[4];
            this.StateSpl.Slots = HelperFunctions.SetSlots(values, 0x0D17);
            this.StateSpl.Values = values;

            // Данные из устройства ЖС
            if (slotLen == 1024)
            {
                this.AllSystemJournal1024Diagnostics = new MemoryEntity<AllSystemJournalStruct1024>("Журнал системы", this, 0x0600, slotLen);
            }
            else
            {
                this.AllSystemJournal64Diagnostics = new MemoryEntity<AllSystemJournalStruct64>("Журнал системы", this, 0x0600);
            }
            this.IndexJSDiagnostics = new MemoryEntity<OneWordStruct>("Индекс ЖС", this, 0x0600);

            // Данные из устройства Конфигурация
            this.ConfigurationDiagnostics = new MemoryEntity<ConfigurationStruct>("Конфигурация", this, 0x1000, slotLen);
            this.Configuration771Diagnostics = new MemoryEntity<ConfigurationStruct771>("Конфигурация", this, 0x1000, slotLen);

            // Данные из устройства Осциллограф
            if (slotLen == 1024)
            {
                this.AllOscJournalDiagnostics = new MemoryEntity<AllOscJournalStruct>("Журнал осциллографа (весь)", this, 0x0800, slotLen);
            }
            else
            {
                this.OscJournalDiagnostics = new MemoryEntity<OscJournalStruct>("Журнал осциллографа", this, 0x0800);
            }
            this.SetOscStartPageDiagnostics = new MemoryEntity<OneWordStruct>("Установка стартовой страницы осциллограммы", this, 0x900);
            this.OscOptionsDiagnostics = new MemoryEntity<OscOptionsStruct>("Параметры осциллографа", this, 0x05A0);
            this.AllChannelsDiagnostics = new MemoryEntity<OscopeAllChannelsStruct>("Все каналы осциллографа", this, 0x24D4, slotLen);
            this.OscPageDiagnostics = new MemoryEntity<OscPage>("Страница осциллографа", this, 0x900, slotLen);
            this.RefreshOscJournalDiagnostics = new MemoryEntity<OneWordStruct>("Индекс журнала осциллографа", this, 0x0800);
            this.CurrentOptionsLoaderOscDiagnostics = new CurrentOptionsLoader(this);

            // Данные из устройства ЖА
            if (slotLen == 1024)
            {
                this.AllAlarmJournalDiagnostics = new MemoryEntity<AllAlarmJournalStruct>("Журнал аварий", this, 0x0700, slotLen);
            }
            else
            {
                this.AlarmJournalDiagnostics = new MemoryEntity<AlarmJournalStruct>("Запись журнала аварий", this, 0x0700);
            }
            this.SetPageAlarmJournalDiagnostics = new MemoryEntity<OneWordStruct>("Страница ЖА", this, 0x0700);
            this.CurrentOptionsLoaderAJDiagnostics = new CurrentOptionsLoader(this);
        }
        

        // Эмуляция
        private MemoryEntity<WriteStructEmul> _writeSructEmulation;
        private MemoryEntity<WriteStructEmul> _writeSructEmulationNull;
        private MemoryEntity<ReadStructEmul> _readSructEmulation;

        // Лицевая сторона
        private slot _slot = new Device.slot(0x100, 0x100 + 0x29);
        public slot DisplayInfoSlot => _slot;
        public DisplayInfoDictionary FaceSizeDictionary { get; private set; }
        
        // Измерения
        public MemoryEntity<DateTimeStruct> DateTime { get; private set; }
        public MemoryEntity<OneWordStruct> GroupUstavki { get; private set; }
        public MemoryEntity<OneWordStruct> Imin { get; private set; }
        public MemoryEntity<DiscretDataBaseStruct> DiscretDataBase { get; private set; }
        public MemoryEntity<AnalogDataBaseStruct> AnalogDataBase { get; private set; }
        public MemoryEntity<MeasureTransStruct> MeasureTrans { get; private set; }

        // ЖС
        public MemoryEntity<AllSystemJournalStruct1024> AllSystemJournal1024 { get; private set; }
        public MemoryEntity<AllSystemJournalStruct64> AllSystemJournal64 { get; private set; }
        public MemoryEntity<OneWordStruct> IndexJS { get; private set; }
        
        // ЖА
        public MemoryEntity<OneWordStruct> AlarmJournalSize { get; private set; }
        public MemoryEntity<AlarmJournalStruct> AlarmJournal { get; private set; }
        public MemoryEntity<AllAlarmJournalStruct> AllAlarmJournal { get; private set; }
        public MemoryEntity<OneWordStruct> SetPageAlarmJournal { get; private set; }
        public CurrentOptionsLoader CurrentOptionsLoaderAJ { get; set; }

        // Конфигурация
        public MemoryEntity<ConfigurationStruct> Configuration { get; private set; }
        public MemoryEntity<ConfigurationStruct771> Configuration771 { get; private set; }

        // Программирование
        public MemoryEntity<OneWordStruct> StopSpl { get; private set; }
        public MemoryEntity<OneWordStruct> StartSpl { get; private set; }
        public MemoryEntity<OneWordStruct> ProgramPageStruct { get; private set; }
        public MemoryEntity<ProgramPageStruct> ProgramPage { get; private set; }
        public MemoryEntity<StartStruct> ProgramStartStruct { get; private set; }
        public MemoryEntity<LogicProgramSignals> ProgramSignalsStruct { get; private set; }
        public MemoryEntity<SourceProgramStruct> SourceProgramStruct { get; private set; }
        public MemoryEntity<SomeStruct> StateSpl { get; private set; }

        // Эмуляция
        public MemoryEntity<WriteStructEmul> WriteStructEmulation => this._writeSructEmulation;
        public MemoryEntity<WriteStructEmul> WriteStructEmulationNull => this._writeSructEmulationNull;
        public MemoryEntity<ReadStructEmul> ReadStructEmulation => this._readSructEmulation;

        // Осциллограф
        public CurrentOptionsLoader CurrentOptionsLoaderOsc { get; private set; }
        public MemoryEntity<OscJournalStruct> OscJournal { get; private set; }
        public MemoryEntity<OneWordStruct> RefreshOscJournal { get; private set; }
        public MemoryEntity<OscPage> OscPage { get; private set; }
        public MemoryEntity<AllOscJournalStruct> AllOscJournal { get; private set; }
        public MemoryEntity<OneWordStruct> SetOscStartPage { get; private set; }
        public MemoryEntity<OscOptionsStruct> OscOptions { get; private set; }
        public MemoryEntity<OscopeAllChannelsStruct> AllChannels { get; private set; }

        // Данные из устройства для ЖС
        public MemoryEntity<AllSystemJournalStruct1024> AllSystemJournal1024Diagnostics { get; private set; }
        public MemoryEntity<AllSystemJournalStruct64> AllSystemJournal64Diagnostics { get; private set; }
        public MemoryEntity<OneWordStruct> IndexJSDiagnostics { get; private set; }

        // Данные из устройства для ЖА
        public MemoryEntity<AlarmJournalStruct> AlarmJournalDiagnostics { get; private set; }
        public MemoryEntity<AllAlarmJournalStruct> AllAlarmJournalDiagnostics { get; private set; }
        public MemoryEntity<OneWordStruct> SetPageAlarmJournalDiagnostics { get; private set; }
        public CurrentOptionsLoader CurrentOptionsLoaderAJDiagnostics { get; set; }

        // Данные из устройства для Конфигурации
        public MemoryEntity<ConfigurationStruct> ConfigurationDiagnostics { get; private set; }
        public MemoryEntity<ConfigurationStruct771> Configuration771Diagnostics { get; private set; }

        // Данные из устройства для Осциллографа
        public CurrentOptionsLoader CurrentOptionsLoaderOscDiagnostics { get; private set; }
        public MemoryEntity<OscJournalStruct> OscJournalDiagnostics { get; private set; }
        public MemoryEntity<OneWordStruct> RefreshOscJournalDiagnostics { get; private set; }
        public MemoryEntity<OscPage> OscPageDiagnostics { get; private set; }
        public MemoryEntity<AllOscJournalStruct> AllOscJournalDiagnostics { get; private set; }
        public MemoryEntity<OneWordStruct> SetOscStartPageDiagnostics { get; private set; }
        public MemoryEntity<OscOptionsStruct> OscOptionsDiagnostics { get; private set; }
        public MemoryEntity<OscopeAllChannelsStruct> AllChannelsDiagnostics { get; private set; }

        public sealed override Modbus MB
        {
            get { return mb; }
            set
            {
                if (value == null) return;
                if (mb != null)
                {
                    mb.CompleteExchange -= this.CompleteExchange;
                }
                mb = value;
                mb.CompleteExchange += this.CompleteExchange;
            }
        }

        private void CompleteExchange(object sender, Query query)
        {
            if (query.name == "LoadDisplayInfo" + DeviceNumber)
            {
                Raise(query, null, null, ref _slot);
                FaceSizeDictionary.GetDisplayInfo(_slot.Value);               
            }
            mb_CompleteExchange(sender, query);
        }

        // Лицевая сторона
        public void LoadDisplayInfo()
        {
            LoadSlotCycle(this.DeviceNumber, DisplayInfoSlot, "LoadDisplayInfo" + this.DeviceNumber, new TimeSpan(500), 10, this);
        }

        public override void LoadVersion(object deviceObj)
        {
            _infoSlot = new slot(0x500, 0x520);
            base.LoadVersion(deviceObj);
        }

        public const string T4N4D42R35 = "T4N4D42R35";
        public const string T4N4D74R67 = "T4N4D74R67";
        public const string T4N5D42R35 = "T4N5D42R35";
        public const string T4N5D74R67 = "T4N5D74R67";
        public const string T4N4D18R16 = "T4N4D18R16";
        
        private readonly List<string> _deviceNames = new List<string>
        {
            "MR771",
            "MR761",
            "MR741"
        };

        private readonly List<string> _deviceConfigs = new List<string>
        {
            T4N4D42R35,
            T4N4D74R67,
            T4N5D42R35,
            T4N5D74R67,
            T4N4D18R16
        };
        
        public Type[] Forms
        {
            get
            {
                this._groupSetPointSize = new GroupSetpoint().GetStructInfo(this.MB.BaudeRate == BAUDE_RATE_MAX ? 1024 : 64).FullSize;

                this.InitMemoryEntity();
                
                //if ((!this.DeviceDlgInfo.IsConnectionMode || !this.IsConnect) && !Framework.Framework.IsProjectOpening)
                //{
                //    ChoiseDeviceNameAndType device = new ChoiseDeviceNameAndType();
                //    device.ShowDialog();
                //    DeviceType = device.DeviceName;
                //    Info.Plant = device.DeviceConfig;
                //    DeviceVersion = device.DeviceVersion;
                //}

                Strings.DeviceName = DeviceType;
                Strings.DeviceType = Info.Plant;

                if (this.DeviceDlgInfo.IsConnectionMode && this.IsConnect)
                {
                    return new[]
                    {
                        typeof(OscilloscopeForm),
                        typeof(EmulationForm),
                        typeof(BSBGLEF),
                        typeof(ConfigurationForm),
                        typeof(MrMeasuringForm),
                        typeof(SystemJournalForm),
                        typeof(AlarmJournalForm),
                        typeof(FaceSideDeviceForm),
                        typeof(AllInformationsDeviceForm),
                        typeof(FileSharingForm)
                    };
                }
                return new[]
                {
                    typeof(OscilloscopeForm),
                    typeof(EmulationForm),
                    typeof(BSBGLEF),
                    typeof(ConfigurationForm),
                    typeof(MrMeasuringForm),
					typeof(SystemJournalForm),
                    typeof(AlarmJournalForm),   
                };
            }
        }

        public List<string> Versions => new List<string> { "3.09" };

        #region [INodeView Members]

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType => typeof(MRUniversal);

        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow => false;

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage => Resources.mrBig;
        
        [Browsable(false)]
        public string NodeName => "МР";

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes => new INodeView[] { };

        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable => true;

        #endregion [INodeView Members]
        
    }
}
