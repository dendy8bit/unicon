﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using BEMN.Devices;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Ls;
using BEMN.MRUNIVERSAL.Configuration.Structures.Oscope;
using BEMN.MRUNIVERSAL.Configuration.Structures.RelayInd;
using BEMN.MRUNIVERSAL.Configuration.Structures.RSTriggers;

namespace BEMN.MRUNIVERSAL
{
    public static class Strings
    {
        public static string DeviceType;
        public static string DeviceName;
        public static double CurrentVersion;
        public static int CurrentCount;
        public static int VoltageCount;

        /// <summary>
        /// Сигналы Реле и Индикаторы
        /// </summary>
        public static Dictionary<int, string> DefaultSignaturesSignals
        {
            get
            {
                List<string> list = new List<string>
                {
                    "НЕТ",
                    "Д1",
                    "Д2",
                    "Д3",
                    "Д4",
                    "Д5",
                    "Д6",
                    "Д7",
                    "Д8",
                    "Д9",
                    "Д10",
                    "Д11",
                    "Д12",
                    "Д13",
                    "Д14",
                    "Д15",
                    "Д16",
                    "Д17",
                    "Д18",
                    "Д19",
                    "Д20",
                    "Д21",
                    "Д22",
                    "Д23",
                    "Д24",
                    "Д25",
                    "Д26",
                    "Д27",
                    "Д28",
                    "Д29",
                    "Д30",
                    "Д31",
                    "Д32",
                    "Д33",
                    "Д34",
                    "Д35",
                    "Д36",
                    "Д37",
                    "Д38",
                    "Д39",
                    "Д40",
                    "Д41",
                    "Д42",
                    "Д43",
                    "Д44",
                    "Д45",
                    "Д46",
                    "Д47",
                    "Д48",
                    "Д49",
                    "Д50",
                    "Д51",
                    "Д52",
                    "Д53",
                    "Д54",
                    "Д55",
                    "Д56",
                    "Д57",
                    "Д58",
                    "Д59",
                    "Д60",
                    "Д61",
                    "Д62",
                    "Д63",
                    "Д64",
                    "Д65",
                    "Д66",
                    "Д67",
                    "Д68",
                    "Д69",
                    "Д70",
                    "Д71",
                    "Д72",
                    "РЕЗЕРВ",
                    "РЕЗЕРВ",
                    "РЕЗЕРВ",
                    "РЕЗЕРВ",
                    "РЕЗЕРВ",
                    "РЕЗЕРВ",
                    "РЕЗЕРВ",
                    "РЕЗЕРВ",
                    "РЕЗЕРВ",
                    "РЕЗЕРВ",
                    "РЕЗЕРВ",
                    "РЕЗЕРВ",
                    "РЕЗЕРВ",
                    "РЕЗЕРВ",
                    "РЕЗЕРВ",
                    "РЕЗЕРВ",

                    "КМД 1",
                    "КМД 2",
                    "КМД 3",
                    "КМД 4",
                    "КМД 5",
                    "КМД 6",
                    "КМД 7",
                    "КМД 8",
                    "КМД 9",
                    "КМД 10",
                    "КМД 11",
                    "КМД 12",
                    "КМД 13",
                    "КМД 14",
                    "КМД 15",
                    "КМД 16",
                    "КМД 17",
                    "КМД 18",
                    "КМД 19",
                    "КМД 20",
                    "КМД 21",
                    "КМД 22",
                    "КМД 23",
                    "КМД 24",

                    "RST1",
                    "RST2",
                    "RST3",
                    "RST4",
                    "RST5",
                    "RST6",
                    "RST7",
                    "RST8",
                    "RST9",
                    "RST10",
                    "RST11",
                    "RST12",
                    "RST13",
                    "RST14",
                    "RST15",
                    "RST16",

                    "ЛС1",
                    "ЛС2",
                    "ЛС3",
                    "ЛС4",
                    "ЛС5",
                    "ЛС6",
                    "ЛС7",
                    "ЛС8",
                    "ЛС9",
                    "ЛС10",
                    "ЛС11",
                    "ЛС12",
                    "ЛС13",
                    "ЛС14",
                    "ЛС15",
                    "ЛС16",

                    "БГС1",
                    "БГС2",
                    "БГС3",
                    "БГС4",
                    "БГС5",
                    "БГС6",
                    "БГС7",
                    "БГС8",
                    "БГС9",
                    "БГС10",
                    "БГС11",
                    "БГС12",
                    "БГС13",
                    "БГС14",
                    "БГС15",
                    "БГС16",

                    "ВЛС1",
                    "ВЛС2",
                    "ВЛС3",
                    "ВЛС4",
                    "ВЛС5",
                    "ВЛС6",
                    "ВЛС7",
                    "ВЛС8",
                    "ВЛС9",
                    "ВЛС10",
                    "ВЛС11",
                    "ВЛС12",
                    "ВЛС13",
                    "ВЛС14",
                    "ВЛС15",
                    "ВЛС16",

                    "ССЛ1",
                    "ССЛ2",
                    "ССЛ3",
                    "ССЛ4",
                    "ССЛ5",
                    "ССЛ6",
                    "ССЛ7",
                    "ССЛ8",
                    "ССЛ9",
                    "ССЛ10",
                    "ССЛ11",
                    "ССЛ12",
                    "ССЛ13",
                    "ССЛ14",
                    "ССЛ15",
                    "ССЛ16",
                    "ССЛ17",
                    "ССЛ18",
                    "ССЛ19",
                    "ССЛ20",
                    "ССЛ21",
                    "ССЛ22",
                    "ССЛ23",
                    "ССЛ24",
                    "ССЛ25",
                    "ССЛ26",
                    "ССЛ27",
                    "ССЛ28",
                    "ССЛ29",
                    "ССЛ30",
                    "ССЛ31",
                    "ССЛ32",
                    "ССЛ33",
                    "ССЛ34",
                    "ССЛ35",
                    "ССЛ36",
                    "ССЛ37",
                    "ССЛ38",
                    "ССЛ39",
                    "ССЛ40",
                    "ССЛ41",
                    "ССЛ42",
                    "ССЛ43",
                    "ССЛ44",
                    "ССЛ45",
                    "ССЛ46",
                    "ССЛ47",
                    "ССЛ48",
                    "Z 1 ИО",
                    "Z 1",
                    "Z 2 ИО",
                    "Z 2",
                    "Z 3 ИО",
                    "Z 3",
                    "Z 4 ИО",
                    "Z 4",
                    "Z 5 ИО",
                    "Z 5",
                    "Z 6 ИО",
                    "Z 6",
                    "РЕЗЕРВ 1",
                    "РЕЗЕРВ 2",
                    "P1 ИО",
                    "P1",
                    "P2 ИО",
                    "P2",
                    "РЕЗЕРВ 3",
                    "РЕЗЕРВ 4",
                    "I> 1 ИО",
                    "I> 1",
                    "I> 2 ИО",
                    "I> 2",
                    "I> 3 ИО",
                    "I> 3",
                    "I> 4 ИО",
                    "I> 4",
                    "I> 5 ИО",
                    "I> 5",
                    "I> 6 ИО",
                    "I> 6",
                    "I< ИО",
                    "I<",
                    "I*> 1 ИО",
                    "I*> 1",
                    "I*> 2 ИО",
                    "I*> 2",
                    "I*> 3 ИО",
                    "I*> 3",
                    "I*> 4 ИО",
                    "I*> 4",
                    "I*> 5 ИО",
                    "I*> 5",
                    "I*> 6 ИО",
                    "I*> 6",
                    "I*> 7 ИО",
                    "I*> 7",
                    "I*> 8 ИО",
                    "I*> 8",
                    "I2/I1> ИО",
                    "I2/I1>",
                    "U> 1 ИО",
                    "U> 1",
                    "U> 2 ИО",
                    "U> 2",
                    "U> 3 ИО",
                    "U> 3",
                    "U> 4 ИО",
                    "U> 4",
                    "U< 1 ИО",
                    "U< 1",
                    "U< 2 ИО",
                    "U< 2",
                    "U< 3 ИО",
                    "U< 3",
                    "U< 4 ИО",
                    "U< 4",
                    "F> 1 ИО",
                    "F> 1",
                    "F> 2 ИО",
                    "F> 2",
                    "F> 3 ИО",
                    "F> 3",
                    "F> 4 ИО",
                    "F> 4",
                    "F< 1 ИО",
                    "F< 1",
                    "F< 2 ИО",
                    "F< 2",
                    "F< 3 ИО",
                    "F< 3",
                    "F< 4 ИО",
                    "F< 4",
                    "Q> 1",
                    "Q> 2",
                    "БЛК.ПО Q",
                    "БЛК.ПО N", // Резерв для 771
                    "ПУСК", // Резерв для 771
                    "НЕИСПР.",
                    "РЕЗЕРВ 5",
                    "УСКпоВКЛ.",
                    "СИГНАЛ-ЦИЯ",
                    "АВАР.ОТКЛ",
                    "ОТКЛ.ВЫКЛ.",
                    "ВКЛ.ВЫКЛ.",
                    "ПУСК АПВ",
                    "АПВ 1крат",
                    "АПВ 2крат ",
                    "АПВ 3крат ",
                    "АПВ 4крат",
                    "ВКЛ.поАПВ",
                    "ЗАПРЕТ АПВ",
                    "АПВ БЛОК.",
                    "ГОТ-ТЬ АПВ",
                    "КСиУППНавт",
                    "U1-U2+",
                    "U1+U2-",
                    "U1-U2-",
                    "Условия ОС",
                    "Условия УС",
                    "КСиУППНвкл",
                    "ПОВР. Ф. A",
                    "ПОВР. Ф. B",
                    "ПОВР. Ф. C",
                    "КАЧАНИЕ",
                    "КАЧ.ВНЕШ",
                    "КАЧ.ВНУТР",
                    "НеиспТНмгн ",
                    "НеиспТНс/п",
                    "ВХОД K1",
                    "ВХОД K2",
                    "УРОВ 1",
                    "УРОВ 2",
                    "БЛК. УРОВ",
                    "АВР ВКЛ.", // rez 771
                    "АВР ОТКЛ.", // rez 771 
                    "АВР БЛК.", // rez 771
                    "ПУСК ДУГ.", // rez 771
                    "АВРвывДис", // ВЧС 1 771
                    "АВРвведен", // ВЧС 0 771
                    "АВРвывед.", // ВЧС ОТКЛ.1 771
                    "АВР ГОТОВ", // ВЧС ОТКЛ.0 771
                    "АВРдеблок", // РЕВЕРС? 1 771
                    "РЕЗЕРВ 6", // РЕВЕРС? 0 771
                    "РЕЗЕРВ 7", // РЕВЕРС 1 771
                    "РЕЗЕРВ 8", // РЕВЕРС 0 771
                    "РЕЗЕРВ 9", // Эхо 1 771
                    "РЕЗЕРВ 10", // Эхо 0 771
                    "РЕЗЕРВ 11",
                    "РЕЗЕРВ 12",
                    "РЕЗЕРВ 13",
                    "РЕЗЕРВ 14",
                    "РЕЗЕРВ 15",
                    "РЕЗЕРВ 16",
                    "РЕЗЕРВ 17",
                    "РЕЗЕРВ 18",
                    "РЕЗЕРВ 19",
                    "РЕЗЕРВ 20",
                    "ВНЕШ. 1",
                    "ВНЕШ. 2",
                    "ВНЕШ. 3",
                    "ВНЕШ. 4",
                    "ВНЕШ. 5",
                    "ВНЕШ. 6",
                    "ВНЕШ. 7",
                    "ВНЕШ. 8",
                    "ВНЕШ. 9",
                    "ВНЕШ. 10",
                    "ВНЕШ. 11",
                    "ВНЕШ. 12",
                    "ВНЕШ. 13",
                    "ВНЕШ. 14",
                    "ВНЕШ. 15",
                    "ВНЕШ. 16"
                };

                SortedDictionary<int, string> ret = new SortedDictionary<int, string>();
                for (ushort i = 0; i < list.Count; i++)
                {
                    ret.Add(i, list[i]);
                }

                switch (DeviceType)
                {
                    case MRUniversal.T4N4D42R35:
                    case MRUniversal.T4N5D42R35:
                        for (int i = 41; i < 43 + 46; i++)
                        {
                            ret[i] = "РЕЗЕРВ";
                        }
                        break;
                    case MRUniversal.T4N4D74R67:
                    case MRUniversal.T4N5D74R67:
                        for (int i = 73; i < 73 + 16; i++)
                        {
                            ret[i] = "РЕЗЕРВ";
                        }
                        break;
                    default:
                        //дискреты
                        for (int i = 17; i < 17 + 72; i++)
                        {
                            ret[i] = "РЕЗЕРВ";
                        }
                        break;
                }

                switch (DeviceName)
                {
                    case "MR771":

                        //Двигатель
                        int deleteNInd = ret.First(v => v.Value == "БЛК.ПО N").Key;

                        for (int i = deleteNInd; i < deleteNInd + 2; i++)
                        {
                            ret[i] = "РЕЗЕРВ";
                        }

                        //АВР
                        int deleteAvrInd = ret.First(v => v.Value == "АВР ВКЛ.").Key;

                        ret[deleteAvrInd] = "РЕЗЕРВ";
                        ret[deleteAvrInd + 1] = "РЕЗЕРВ";
                        ret[deleteAvrInd + 2] = "РЕЗЕРВ";
                        ret[deleteAvrInd + 3] = "РЕЗЕРВ";

                        for (int i = deleteAvrInd; i < deleteAvrInd + 4; i++)
                        {
                            ret[i] = "РЕЗЕРВ";
                        }

                        int changedNameSignalsInd = ret.First(v => v.Value == "АВРвывДис").Key;

                        ret[changedNameSignalsInd] = "ВЧС 1";
                        ret[changedNameSignalsInd + 1] = "ВЧС 0";
                        ret[changedNameSignalsInd + 2] = "ВЧС ОТКЛ.1";
                        ret[changedNameSignalsInd + 3] = "ВЧС ОТКЛ.0";
                        ret[changedNameSignalsInd + 4] = "РЕВЕРС? 1";
                        ret[changedNameSignalsInd + 5] = "РЕВЕРС? 0";
                        ret[changedNameSignalsInd + 6] = "РЕВЕРС 1";
                        ret[changedNameSignalsInd + 7] = "РЕВЕРС 0";
                        ret[changedNameSignalsInd + 8] = "Эхо 1";
                        ret[changedNameSignalsInd + 9] = "Эхо 0";

                        //Ступени по Z
                        int deleteRezInd = ret.First(v => v.Value == "РЕЗЕРВ 1").Key;
                        ret[deleteRezInd] = "Z7 ИО";
                        ret[deleteRezInd + 1] = "Z7";
                        ret[deleteRezInd + 2] = "Z8 ИО";
                        ret[deleteRezInd + 3] = "Z8";
                        ret[deleteRezInd + 4] = "Z9 ИО";
                        ret[deleteRezInd + 5] = "Z9";
                        ret[deleteRezInd + 6] = "Z10 ИО";
                        ret[deleteRezInd + 7] = "Z10";

                        //int rez5Ind = ret.First(v => v.Value == "РЕЗЕРВ 5").Key;
                        //ret.Remove(rez5Ind);

                        //int rez11Ind = ret.First(v => v.Value == "РЕЗЕРВ 11").Key;

                        //for (int i = rez11Ind; i < rez11Ind + 10; i++)
                        //{
                        //    ret.Remove(i);
                        //}

                        break;
                    default:

                        //int rez5Ind761 = ret.First(v => v.Value == "РЕЗЕРВ 5").Key;
                        //ret.Remove(rez5Ind761);

                        //int rez6Ind761 = ret.First(v => v.Value == "РЕЗЕРВ 6").Key;

                        //for (int i = rez6Ind761; i < rez6Ind761 + 15; i++)
                        //{
                        //    ret.Remove(i);
                        //}

                        break;
                }

                return ret.ToDictionary(r => r.Key, r => r.Value);
            }
        }

        public static int BaseSizeAJ { get; set; }

        public static List<string> Polar =>
            new List<string>
            {
                "+",
                "-"
            };

        public static List<string> Rotation =>
            new List<string>
            {
                "Прямое",
                "Обратное"
            };

        public static List<string> InputBinding =>
            new List<string>
            {
                "Фазное",
                "Линейное"
            };

        public static List<string> InputBindingUca =>
            new List<string>
            {
                "Измереное",
                "Расчетное"
            };

        public static List<string> Pkoef
        {
            get
            {
                return new List<string>
                {
                    "кВт",
                    "МВт"
                };
            }
        }

        public static List<string> LogicSignalNames
        {
            get
            {
                List<string> ret = new List<string>();
                for (int i = 1; i <= InputLogicStruct.DiscrestCount; i++)
                {
                    ret.Add("Д" + i);
                }
                return ret;
            }
        }

        /// <summary>
        /// Режим Защиты
        /// </summary>
        public static List<string> DefenseModesDug
        {
            get
            {
                return new List<string>
                {
                    "Выведено",
                    "Введено",
                    "Сигнализация"
                };
            }
        }

        /// <summary>
        /// Фиксация осц.
        /// </summary>
        public static List<string> OscFixation
        {
            get
            {
                return new List<string>
                {
                    "Первой",
                    "Посл."
                };
            }
        }

        /// <summary>
        /// Размер осц.
        /// </summary>
        public static List<string> OscCount
        {
            get
            {
                List<string> ret = new List<string>();
                for (int i = 1; i <= 40; i++)
                {
                    ret.Add(i.ToString());
                }
                return ret;
            }
        }

        public static List<string> ImpDlit
        {
            get
            {
                return new List<string>
                {
                    "Импульсная",
                    "Длительная",
                };
            }
        }

        /// <summary>
        /// запрещено разрешено
        /// </summary>
        public static List<string> ForbiddenAllowed
        {
            get
            {
                return new List<string>
                {
                    "Запрещено",
                    "Разрешено"
                };
            }
        }

        /// <summary>
        /// разрешено Контроль
        /// </summary>
        public static List<string> ControlForbidden
        {
            get
            {
                return new List<string>
                {
                    "Контроль",
                    "Разрешено"
                };
            }
        }

        public static List<string> LsSignals { get; set; }
        /// <summary>
        /// Сигналы ЛС
        /// </summary>
        public static List<string> DefaultLsSignals
        {
            get
            {
                List<string> ret = new List<string>();
                for (int i = 1; i <= InputLogicStruct.DiscrestCount; i++)
                {
                    ret.Add("Д" + i);
                }
                return ret;
            }
        }

        /// <summary>
        /// значения дискрет ЛС
        /// </summary>
        public static List<string> LsState
        {
            get
            {
                return new List<string>
                {
                    "НЕТ",
                    "Да",
                    "Инверс"
                };
            }
        }

        public static List<string> UMaxStages
        {
            get
            {
                return new List<string>
                {
                    "U> 1",
                    "U> 2",
                    "U> 3",
                    "U> 4"
                };
            }
        }

        public static List<string> UMinStages
        {
            get
            {
                return new List<string>
                {
                    "U< 1",
                    "U< 2",
                    "U< 3",
                    "U< 4"
                };
            }
        }

        public static List<string> FBStages
        {
            get
            {
                return new List<string>
                {
                    "F> 1",
                    "F> 2",
                    "F> 3",
                    "F> 4"
                };
            }
        }
        public static List<string> FMStages
        {
            get
            {
                return new List<string>
                {
                    "F< 1",
                    "F< 2",
                    "F< 3",
                    "F< 4"
                };
            }
        }

        public static List<string> QStages
        {
            get
            {
                return new List<string>
                {
                    "Q>",
                    "Q>>"
                };
            }
        }

        public static List<string> ExternalStages
        {
            get
            {
                return new List<string>
                {
                    "ВЗ 1",
                    "ВЗ 2",
                    "ВЗ 3",
                    "ВЗ 4",
                    "ВЗ 5",
                    "ВЗ 6",
                    "ВЗ 7",
                    "ВЗ 8",
                    "ВЗ 9",
                    "ВЗ 10",
                    "ВЗ 11",
                    "ВЗ 12",
                    "ВЗ 13",
                    "ВЗ 14",
                    "ВЗ 15",
                    "ВЗ 16"
                };
            }
        }

        /// <summary>
        /// Названия ступеней I*
        /// </summary>
        public static List<string> NamesIStar
        {
            get
            {
                return new List<string>
                {
                    "I*> 1",
                    "I*> 2",
                    "I*> 3",
                    "I*> 4",
                    "I*> 5",
                    "I*> 6",
                    "I*> 7",
                    "I*> 8"
                };
            }
        }


        /// <summary>
        /// Названия ступеней I
        /// </summary>
        public static List<string> NamesI
        {
            get
            {
                return new List<string>
                {
                    "I> 1",
                    "I> 2",
                    "I> 3",
                    "I> 4",
                    "I> 5",
                    "I> 6",
                    "I<"
                };
            }
        }

        /// <summary>
        /// Нет/Есть
        /// </summary>
        public static List<string> BeNo
        {
            get
            {
                return new List<string>
                {
                    "НЕТ",
                    "Есть"
                };
            }
        }

        public static List<string> Sign => new List<string>
        {
            "+",
            "-"
        };


        public static Dictionary<ushort, string> ExternalDafenseSrab { get; set; }

        public static Dictionary<ushort, string> ExternalDafenseSrabSetup(Dictionary<ushort, string> list)
        {
            switch (DeviceType)
                {
                    case MRUniversal.T4N4D42R35:
                    case MRUniversal.T4N5D42R35:
                        for (ushort i = 81; i < 84 + 93; i++)
                        {
                            list.Remove(i);
                        }
                        break;
                    case MRUniversal.T4N4D74R67:
                    case MRUniversal.T4N5D74R67:
                        for (ushort i = 145; i < 145 + 32; i++)
                        {
                            list.Remove(i);
                        }
                        break;
                    default:
                        //дискреты
                        for (ushort i = 33; i < 32 + 145; i++)
                        {
                            list.Remove(i);
                        }
                        break;
                }

                switch (DeviceName)
                {
                    case "MR771":

                        //Двигатель
                        ushort deleteNInd = list.First(v => v.Value == "БЛК. ПО N").Key;

                        for (ushort i = deleteNInd; i < deleteNInd + 4; i++)
                        {
                            list.Remove(i);
                        }

                        //АВР
                        ushort deleteAvrInd = list.First(v => v.Value == "АВР ВКЛ.").Key;

                        for (ushort i = deleteAvrInd; i < deleteAvrInd + 8; i++)
                        {
                            list.Remove(i);
                        }

                        ushort changedNameSignalsInd = list.First(v => v.Value == "АВРвывДис").Key;

                        list[changedNameSignalsInd] = "ВЧС 1";
                        list[(ushort)(changedNameSignalsInd + 1)] = "ВЧС 1 Инв.";
                        list[(ushort)(changedNameSignalsInd + 2)] = "ВЧС 0";
                        list[(ushort)(changedNameSignalsInd + 3)] = "ВЧС 0 Инв.";
                        list[(ushort)(changedNameSignalsInd + 4)] = "ВЧС ОТКЛ.1";
                        list[(ushort)(changedNameSignalsInd + 5)] = "ВЧС ОТКЛ.1 Инв.";
                        list[(ushort)(changedNameSignalsInd + 6)] = "ВЧС ОТКЛ.0";
                        list[(ushort)(changedNameSignalsInd + 7)] = "ВЧС ОТКЛ.0 Инв.";
                        list[(ushort)(changedNameSignalsInd + 8)] = "РЕВЕРС? 1";
                        list[(ushort)(changedNameSignalsInd + 9)] = "РЕВЕРС? 1 Инв.";
                        list[(ushort)(changedNameSignalsInd + 10)] = "РЕВЕРС? 0";
                        list[(ushort)(changedNameSignalsInd + 11)] = "РЕВЕРС? 0 Инв.";
                        list[(ushort)(changedNameSignalsInd + 12)] = "РЕВЕРС 1";
                        list[(ushort)(changedNameSignalsInd + 13)] = "РЕВЕРС 1 Инв.";
                        list[(ushort)(changedNameSignalsInd + 14)] = "РЕВЕРС 0";
                        list[(ushort)(changedNameSignalsInd + 15)] = "РЕВЕРС 0 Инв.";
                        list[(ushort)(changedNameSignalsInd + 16)] = "Эхо 1";
                        list[(ushort)(changedNameSignalsInd + 17)] = "Эхо 1 Инв.";
                        list[(ushort)(changedNameSignalsInd + 18)] = "Эхо 0";
                        list[(ushort)(changedNameSignalsInd + 19)] = "Эхо 0 Инв.";

                        //Ступени по Z
                        ushort deleteRezInd = list.First(v => v.Value == "РЕЗЕРВ 1").Key;
                        list[deleteRezInd] = "Z7 ИО";
                        list[(ushort)(deleteRezInd + 1)] = "Z7 ИО Инв.";
                        list[(ushort)(deleteRezInd + 2)] = "Z7";
                        list[(ushort)(deleteRezInd + 3)] = "Z7 Инв.";
                        list[(ushort)(deleteRezInd + 4)] = "Z8 ИО";
                        list[(ushort)(deleteRezInd + 5)] = "Z8 ИО Инв.";
                        list[(ushort)(deleteRezInd + 6)] = "Z8";
                        list[(ushort)(deleteRezInd + 7)] = "Z8 Инв.";
                        list[(ushort)(deleteRezInd + 8)] = "Z9 ИО";
                        list[(ushort)(deleteRezInd + 9)] = "Z9 ИО Инв.";
                        list[(ushort)(deleteRezInd + 10)] = "Z9";
                        list[(ushort)(deleteRezInd + 11)] = "Z9 Инв.";
                        list[(ushort)(deleteRezInd + 12)] = "Z10 ИО";
                        list[(ushort)(deleteRezInd + 13)] = "Z10 ИО Инв.";
                        list[(ushort)(deleteRezInd + 14)] = "Z10";
                        list[(ushort)(deleteRezInd + 15)] = "Z10 Инв.";

                        ushort rez5Ind = list.First(v => v.Value == "РЕЗЕРВ 5").Key;
                        list.Remove(rez5Ind);
                        list.Remove((ushort)(rez5Ind + 1));

                        ushort rez11Ind = list.First(v => v.Value == "РЕЗЕРВ 11").Key;

                        for (ushort i = rez11Ind; i < rez11Ind + 20; i++)
                        {
                            list.Remove(i);
                        }

                        break;
                    default:
                        break;
                }

                return list.ToDictionary(r => r.Key, r => r.Value);
        }
        /// <summary>
        /// СРАБ. внешних защит
        /// </summary>
        public static Dictionary<ushort, string> DefaultExternalDafenseSignals
        {
            get
            {
                List<string> list = new List<string>
                {
                    "НЕТ",
                    "Д1",
                    "Д1 Инв.",
                    "Д2",
                    "Д2 Инв.",
                    "Д3",
                    "Д3 Инв.",
                    "Д4",
                    "Д4 Инв.",
                    "Д5",
                    "Д5 Инв.",
                    "Д6",
                    "Д6 Инв.",
                    "Д7",
                    "Д7 Инв.",
                    "Д8",
                    "Д8 Инв.",
                    "Д9",
                    "Д9 Инв.",
                    "Д10",
                    "Д10 Инв.",
                    "Д11",
                    "Д11 Инв.",
                    "Д12",
                    "Д12 Инв.",
                    "Д13",
                    "Д13 Инв.",
                    "Д14",
                    "Д14 Инв.",
                    "Д15",
                    "Д15 Инв.",
                    "Д16",
                    "Д16 Инв.",
                    "Д17",
                    "Д17 Инв.",
                    "Д18",
                    "Д18 Инв.",
                    "Д19",
                    "Д19 Инв.",
                    "Д20",
                    "Д20 Инв.",
                    "Д21",
                    "Д21 Инв.",
                    "Д22",
                    "Д22 Инв.",
                    "Д23",
                    "Д23 Инв.",
                    "Д24",
                    "Д24 Инв.",
                    "Д25",
                    "Д25 Инв.",
                    "Д26",
                    "Д26 Инв.",
                    "Д27",
                    "Д27 Инв.",
                    "Д28",
                    "Д28 Инв.",
                    "Д29",
                    "Д29 Инв.",
                    "Д30",
                    "Д30 Инв.",
                    "Д31",
                    "Д31 Инв.",
                    "Д32",
                    "Д32 Инв.",
                    "Д33",
                    "Д33 Инв.",
                    "Д34",
                    "Д34 Инв.",
                    "Д35",
                    "Д35 Инв.",
                    "Д36",
                    "Д36 Инв.",
                    "Д37",
                    "Д37 Инв.",
                    "Д38",
                    "Д38 Инв.",
                    "Д39",
                    "Д39 Инв.",
                    "Д40",
                    "Д40 Инв.",
                    "Д41",
                    "Д41 Инв.",
                    "Д42",
                    "Д42 Инв.",
                    "Д43",
                    "Д43 Инв.",
                    "Д44",
                    "Д44 Инв.",
                    "Д45",
                    "Д45 Инв.",
                    "Д46",
                    "Д46 Инв.",
                    "Д47",
                    "Д47 Инв.",
                    "Д48",
                    "Д48 Инв.",
                    "Д49",
                    "Д49 Инв.",
                    "Д50",
                    "Д50 Инв.",
                    "Д51",
                    "Д51 Инв.",
                    "Д52",
                    "Д52 Инв.",
                    "Д53",
                    "Д53 Инв.",
                    "Д54",
                    "Д54 Инв.",
                    "Д55",
                    "Д55 Инв.",
                    "Д56",
                    "Д56 Инв.",
                    "Д57",
                    "Д57 Инв.",
                    "Д58",
                    "Д58 Инв.",
                    "Д59",
                    "Д59 Инв.",
                    "Д60",
                    "Д60 Инв.",
                    "Д61",
                    "Д61 Инв.",
                    "Д62",
                    "Д62 Инв.",
                    "Д63",
                    "Д63 Инв.",
                    "Д64",
                    "Д64 Инв.",
                    "Д65",
                    "Д65 Инв.",
                    "Д66",
                    "Д66 Инв.",
                    "Д67",
                    "Д67 Инв.",
                    "Д68",
                    "Д68 Инв.",
                    "Д69",
                    "Д69 Инв.",
                    "Д70",
                    "Д70 Инв.",
                    "Д71",
                    "Д71 Инв.",
                    "Д72",
                    "Д72 Инв.",
                    "Д73",
                    "Д73 Инв.",
                    "Д74",
                    "Д74 Инв.",
                    "Д75",
                    "Д75 Инв.",
                    "Д76",
                    "Д76 Инв.",
                    "Д77",
                    "Д77 Инв.",
                    "Д78",
                    "Д78 Инв.",
                    "Д79",
                    "Д79 Инв.",
                    "Д80",
                    "Д80 Инв.",
                    "Д81",
                    "Д81 Инв.",
                    "Д82",
                    "Д82 Инв.",
                    "Д83",
                    "Д83 Инв.",
                    "Д84",
                    "Д84 Инв.",
                    "Д85",
                    "Д85 Инв.",
                    "Д86",
                    "Д86 Инв.",
                    "Д87",
                    "Д87 Инв.",
                    "Д88",
                    "Д88 Инв.",

                    "КМД 1",
                    "КМД 1 Инв.",
                    "КМД 2",
                    "КМД 2 Инв.",
                    "КМД 3",
                    "КМД 3 Инв.",
                    "КМД 4",
                    "КМД 4 Инв.",
                    "КМД 5",
                    "КМД 5 Инв.",
                    "КМД 6",
                    "КМД 6 Инв.",
                    "КМД 7",
                    "КМД 7 Инв.",
                    "КМД 8",
                    "КМД 8 Инв.",
                    "КМД 9",
                    "КМД 9 Инв.",
                    "КМД 10",
                    "КМД 10 Инв.",
                    "КМД 11",
                    "КМД 11 Инв.",
                    "КМД 12",
                    "КМД 12 Инв.",
                    "КМД 13",
                    "КМД 13 Инв.",
                    "КМД 14",
                    "КМД 14 Инв.",
                    "КМД 15",
                    "КМД 15 Инв.",
                    "КМД 16",
                    "КМД 16 Инв.",
                    "КМД 17",
                    "КМД 17 Инв.",
                    "КМД 18",
                    "КМД 18 Инв.",
                    "КМД 19",
                    "КМД 19 Инв.",
                    "КМД 20",
                    "КМД 20 Инв.",
                    "КМД 21",
                    "КМД 21 Инв.",
                    "КМД 22",
                    "КМД 22 Инв.",
                    "КМД 23",
                    "КМД 23 Инв.",
                    "КМД 24",
                    "КМД 24 Инв.",
                    "RST1",
                    "RST1 Инв.",
                    "RST2",
                    "RST2 Инв.",
                    "RST3",
                    "RST3 Инв.",
                    "RST4",
                    "RST4 Инв.",
                    "RST5",
                    "RST5 Инв.",
                    "RST6",
                    "RST6 Инв.",
                    "RST7",
                    "RST7 Инв.",
                    "RST8",
                    "RST8 Инв.",
                    "RST9",
                    "RST9 Инв.",
                    "RST10",
                    "RST10 Инв.",
                    "RST11",
                    "RST11 Инв.",
                    "RST12",
                    "RST12 Инв.",
                    "RST13",
                    "RST13 Инв.",
                    "RST14",
                    "RST14 Инв.",
                    "RST15",
                    "RST15 Инв.",
                    "RST16",
                    "RST16 Инв.",

                    "ЛС1",
                    "ЛС1 Инв.",
                    "ЛС2",
                    "ЛС2 Инв.",
                    "ЛС3",
                    "ЛС3 Инв.",
                    "ЛС4",
                    "ЛС4 Инв.",
                    "ЛС5",
                    "ЛС5 Инв.",
                    "ЛС6",
                    "ЛС6 Инв.",
                    "ЛС7",
                    "ЛС7 Инв.",
                    "ЛС8",
                    "ЛС8 Инв.",
                    "ЛС9",
                    "ЛС9 Инв.",
                    "ЛС10",
                    "ЛС10 Инв.",
                    "ЛС11",
                    "ЛС11 Инв.",
                    "ЛС12",
                    "ЛС12 Инв.",
                    "ЛС13",
                    "ЛС13 Инв.",
                    "ЛС14",
                    "ЛС14 Инв.",
                    "ЛС15",
                    "ЛС15 Инв.",
                    "ЛС16",
                    "ЛС16 Инв.",
                    "БГС1",
                    "БГС1 Инв.",
                    "БГС2",
                    "БГС2 Инв.",
                    "БГС3",
                    "БГС3 Инв.",
                    "БГС4",
                    "БГС4 Инв.",
                    "БГС5",
                    "БГС5 Инв.",
                    "БГС6",
                    "БГС6 Инв.",
                    "БГС7",
                    "БГС7 Инв.",
                    "БГС8",
                    "БГС8 Инв.",
                    "БГС9",
                    "БГС9 Инв.",
                    "БГС10",
                    "БГС10 Инв.",
                    "БГС11",
                    "БГС11 Инв.",
                    "БГС12",
                    "БГС12 Инв.",
                    "БГС13",
                    "БГС13 Инв.",
                    "БГС14",
                    "БГС14 Инв.",
                    "БГС15",
                    "БГС15 Инв.",
                    "БГС16",
                    "БГС16 Инв.",
                    "ВЛС1",
                    "ВЛС1 Инв.",
                    "ВЛС2",
                    "ВЛС2 Инв.",
                    "ВЛС3",
                    "ВЛС3 Инв.",
                    "ВЛС4",
                    "ВЛС4 Инв.",
                    "ВЛС5",
                    "ВЛС5 Инв.",
                    "ВЛС6",
                    "ВЛС6 Инв.",
                    "ВЛС7",
                    "ВЛС7 Инв.",
                    "ВЛС8",
                    "ВЛС8 Инв.",
                    "ВЛС9",
                    "ВЛС9 Инв.",
                    "ВЛС10",
                    "ВЛС10 Инв.",
                    "ВЛС11",
                    "ВЛС11 Инв.",
                    "ВЛС12",
                    "ВЛС12 Инв.",
                    "ВЛС13",
                    "ВЛС13 Инв.",
                    "ВЛС14",
                    "ВЛС14 Инв.",
                    "ВЛС15",
                    "ВЛС15 Инв.",
                    "ВЛС16",
                    "ВЛС16 Инв.",
                    "ССЛ1",
                    "ССЛ1 Инв.",
                    "ССЛ2",
                    "ССЛ2 Инв.",
                    "ССЛ3",
                    "ССЛ3 Инв.",
                    "ССЛ4",
                    "ССЛ4 Инв.",
                    "ССЛ5",
                    "ССЛ5 Инв.",
                    "ССЛ6",
                    "ССЛ6 Инв.",
                    "ССЛ7",
                    "ССЛ7 Инв.",
                    "ССЛ8",
                    "ССЛ8 Инв.",
                    "ССЛ9",
                    "ССЛ9 Инв.",
                    "ССЛ10",
                    "ССЛ10 Инв.",
                    "ССЛ11",
                    "ССЛ11 Инв.",
                    "ССЛ12",
                    "ССЛ12 Инв.",
                    "ССЛ13",
                    "ССЛ13 Инв.",
                    "ССЛ14",
                    "ССЛ14 Инв.",
                    "ССЛ15",
                    "ССЛ15 Инв.",
                    "ССЛ16",
                    "ССЛ16 Инв.",
                    "ССЛ17",
                    "ССЛ17 Инв.",
                    "ССЛ18",
                    "ССЛ18 Инв.",
                    "ССЛ19",
                    "ССЛ19 Инв.",
                    "ССЛ20",
                    "ССЛ20 Инв.",
                    "ССЛ21",
                    "ССЛ21 Инв.",
                    "ССЛ22",
                    "ССЛ22 Инв.",
                    "ССЛ23",
                    "ССЛ23 Инв.",
                    "ССЛ24",
                    "ССЛ24 Инв.",
                    "ССЛ25",
                    "ССЛ25 Инв.",
                    "ССЛ26",
                    "ССЛ26 Инв.",
                    "ССЛ27",
                    "ССЛ27 Инв.",
                    "ССЛ28",
                    "ССЛ28 Инв.",
                    "ССЛ29",
                    "ССЛ29 Инв.",
                    "ССЛ30",
                    "ССЛ30 Инв.",
                    "ССЛ31",
                    "ССЛ31 Инв.",
                    "ССЛ32",
                    "ССЛ32 Инв.",
                    "ССЛ33",
                    "ССЛ33 Инв.",
                    "ССЛ34",
                    "ССЛ34 Инв.",
                    "ССЛ35",
                    "ССЛ35 Инв.",
                    "ССЛ36",
                    "ССЛ36 Инв.",
                    "ССЛ37",
                    "ССЛ37 Инв.",
                    "ССЛ38",
                    "ССЛ38 Инв.",
                    "ССЛ39",
                    "ССЛ39 Инв.",
                    "ССЛ40",
                    "ССЛ40 Инв.",
                    "ССЛ41",
                    "ССЛ41 Инв.",
                    "ССЛ42",
                    "ССЛ42 Инв.",
                    "ССЛ43",
                    "ССЛ43 Инв.",
                    "ССЛ44",
                    "ССЛ44 Инв.",
                    "ССЛ45",
                    "ССЛ45 Инв.",
                    "ССЛ46",
                    "ССЛ46 Инв.",
                    "ССЛ47",
                    "ССЛ47 Инв.",
                    "ССЛ48",
                    "ССЛ48 Инв.",
                    "Z 1 ИО",
                    "Z 1 ИО Инв.",
                    "Z 1",
                    "Z 1 Инв.",
                    "Z 2 ИО",
                    "Z 2 ИО Инв.",
                    "Z 2",
                    "Z 2 Инв.",
                    "Z 3 ИО",
                    "Z 3 ИО Инв.",
                    "Z 3",
                    "Z 3 Инв.",
                    "Z 4 ИО",
                    "Z 4 ИО Инв.",
                    "Z 4",
                    "Z 4 Инв.",
                    "Z 5 ИО",
                    "Z 5 ИО Инв.",
                    "Z 5",
                    "Z 5 Инв.",
                    "Z 6 ИО",
                    "Z 6 ИО Инв.",
                    "Z 6",
                    "Z 6 Инв.",
                    "РЕЗЕРВ 1",
                    "РЕЗЕРВ 1 Инв.",
                    "РЕЗЕРВ 2",
                    "РЕЗЕРВ 2 Инв.",
                    "P 1 ИО",
                    "P 1 ИО Инв.",
                    "P 1",
                    "P 1 Инв.",
                    "P 2 ИО",
                    "P 2 ИО Инв.",
                    "P 2",
                    "P 2 Инв.",
                    "РЕЗЕРВ 3",
                    "РЕЗЕРВ 3 Инв.",
                    "РЕЗЕРВ 4",
                    "РЕЗЕРВ 4 Инв.",
                    "I> 1 ИО",
                    "I> 1 ИО Инв.",
                    "I> 1",
                    "I> 1 Инв.",
                    "I> 2 ИО",
                    "I> 2 ИО Инв.",
                    "I> 2",
                    "I> 2 Инв.",
                    "I> 3 ИО",
                    "I> 3 ИО Инв.",
                    "I> 3",
                    "I> 3 Инв.",
                    "I> 4 ИО",
                    "I> 4 ИО Инв.",
                    "I> 4",
                    "I> 4 Инв.",
                    "I> 5 ИО",
                    "I> 5 ИО Инв.",
                    "I> 5",
                    "I> 5 Инв.",
                    "I> 6 ИО",
                    "I> 6 ИО Инв.",
                    "I> 6",
                    "I> 6 Инв.",
                    "I< ИО",
                    "I< ИО Инв.",
                    "I<",
                    "I< Инв.",
                    "I*> 1 ИО",
                    "I*> 1 ИО Инв.",
                    "I*> 1",
                    "I*> 1 Инв.",
                    "I*> 2 ИО",
                    "I*> 2 ИО Инв.",
                    "I*> 2",
                    "I*> 2 Инв.",
                    "I*> 3 ИО",
                    "I*> 3 ИО Инв.",
                    "I*> 3",
                    "I*> 3 Инв.",
                    "I*> 4 ИО",
                    "I*> 4 ИО Инв.",
                    "I*> 4",
                    "I*> 4 Инв.",
                    "I*> 5 ИО",
                    "I*> 5 ИО Инв.",
                    "I*> 5",
                    "I*> 5 Инв.",
                    "I*> 6 ИО",
                    "I*> 6 ИО Инв.",
                    "I*> 6",
                    "I*> 6 Инв.",
                    "I*> 7 ИО",
                    "I*> 7 ИО Инв.",
                    "I*> 7",
                    "I*> 7 Инв.",
                    "I*> 8 ИО",
                    "I*> 8 ИО Инв.",
                    "I*> 8",
                    "I*> 8 Инв.",
                    "I2/I1> ИО",
                    "I2/I1> ИО Инв.",
                    "I2/I1>",
                    "I2/I1> Инв.",
                    "U> 1 ИО",
                    "U> 1 ИО Инв.",
                    "U> 1",
                    "U> 1 Инв.",
                    "U> 2 ИО",
                    "U> 2 ИО Инв.",
                    "U> 2",
                    "U> 2 Инв.",
                    "U> 3 ИО",
                    "U> 3 ИО Инв.",
                    "U> 3",
                    "U> 3 Инв.",
                    "U> 4 ИО",
                    "U> 4 ИО Инв.",
                    "U> 4",
                    "U> 4 Инв.",
                    "U< 1 ИО",
                    "U< 1 ИО Инв.",
                    "U< 1",
                    "U< 1 Инв.",
                    "U< 2 ИО",
                    "U< 2 ИО Инв.",
                    "U< 2",
                    "U< 2 Инв.",
                    "U< 3 ИО",
                    "U< 3 ИО Инв.",
                    "U< 3",
                    "U< 3 Инв.",
                    "U< 4 ИО",
                    "U< 4 ИО Инв.",
                    "U< 4",
                    "U< 4 Инв.",
                    "F> 1 ИО",
                    "F> 1 ИО Инв.",
                    "F> 1",
                    "F> 1 Инв.",
                    "F> 2 ИО",
                    "F> 2 ИО Инв.",
                    "F> 2",
                    "F> 2 Инв.",
                    "F> 3 ИО",
                    "F> 3 ИО Инв.",
                    "F> 3",
                    "F> 3 Инв.",
                    "F> 4 ИО",
                    "F> 4 ИО Инв.",
                    "F> 4",
                    "F> 4 Инв.",
                    "F< 1 ИО",
                    "F< 1 ИО Инв.",
                    "F< 1",
                    "F< 1 Инв.",
                    "F< 2 ИО",
                    "F< 2 ИОИнв.",
                    "F< 2",
                    "F< 2 Инв.",
                    "F< 3 ИО",
                    "F< 3 ИО Инв.",
                    "F< 3",
                    "F< 3 Инв.",
                    "F< 4 ИО",
                    "F< 4 ИО Инв.",
                    "F< 4",
                    "F< 4 Инв.",
                    "Q> 1",
                    "Q> 1 Инв.",
                    "Q> 2",
                    "Q> 2 Инв.",
                    "БЛК.ПО Q",
                    "БЛК.ПО Q Инв.",
                    "БЛК. ПО N",
                    "БЛК. ПО N Инв.",
                    "ПУСК",
                    "ПУСК Инв.",
                    "НЕИСПР.",
                    "НЕИСПР. Инв.",
                    "РЕЗЕРВ 5",
                    "РЕЗЕРВ 5 Инв.",
                    "УСКОРЕНИЕ",
                    "УСКОРЕНИЕ Инв.",
                    "СИГНАЛ-ЦИЯ",
                    "СИГНАЛ-ЦИЯ Инв.",
                    "АВАР.ОТКЛ",
                    "АВАР.ОТКЛ Инв.",
                    "ОТКЛ.ВЫКЛ.",
                    "ОТКЛ.ВЫКЛ. Инв.",
                    "ВКЛ.ВЫКЛ.",
                    "ВКЛ.ВЫКЛ. Инв.",
                    "ПУСК АПВ",
                    "ПУСК АПВ Инв.",
                    "АПВ 1крат",
                    "АПВ 1крат Инв.",
                    "АПВ 2крат ",
                    "АПВ 2крат Инв.",
                    "АПВ 3крат ",
                    "АПВ 3крат Инв.",
                    "АПВ 4крат",
                    "АПВ 4крат Инв.",
                    "ВКЛ.поАПВ",
                    "ВКЛ.поАПВ Инв.",
                    "ЗАПРЕТ АПВ",
                    "ЗАПРЕТ АПВ Инв.",
                    "АПВ БЛОК.",
                    "АПВ БЛОК. Инв.",
                    "ГОТ-ТЬ АПВ",
                    "ГОТ-ТЬ АПВ Инв.",
                    "КСиУППНавт",
                    "КСиУППНавт Инв.",
                    "U1-U2+",
                    "U1-U2+ Инв.",
                    "U1+U2-",
                    "U1+U2- Инв.",
                    "U1-U2-",
                    "U1-U2- Инв.",
                    "Условия ОС",
                    "Условия ОС Инв.",
                    "Условия УС",
                    "Условия УС Инв.",
                    "КСиУППНвкл",
                    "КСиУППНвкл Инв.",
                    "ПОВР. Ф. A",
                    "ПОВР. Ф. A Инв.",
                    "ПОВР. Ф. B",
                    "ПОВР. Ф. B Инв.",
                    "ПОВР. Ф. C",
                    "ПОВР. Ф. C Инв.",
                    "КАЧАНИЕ",
                    "КАЧАНИЕ Инв.",
                    "КАЧ.ВНЕШ",
                    "КАЧ.ВНЕШ Инв.",
                    "КАЧ.ВНУТР",
                    "КАЧ.ВНУТР Инв.",
                    "НеиспТНмгн ",
                    "НеиспТНмгн Инв.",
                    "НеиспТН с/п",
                    "НеиспТН с/п Инв.",
                    "ВХОД K1",
                    "ВХОД K1 Инв.",
                    "ВХОД K2",
                    "ВХОД K2 Инв.",
                    "УРОВ 1",
                    "УРОВ 1 Инв.",
                    "УРОВ 2",
                    "УРОВ 2 Инв.",
                    "БЛК. УРОВ",
                    "БЛК. УРОВ Инв.",
                    "АВР ВКЛ.",
                    "АВР ВКЛ. Инв.",
                    "АВР ОТКЛ.",
                    "АВР ОТКЛ. Инв.",
                    "АВР БЛК.",
                    "АВР БЛК. Инв.",
                    "ПУСК ДУГ.",
                    "ПУСК ДУГ. Инв.",
                    "АВРвывДис",
                    "АВРвывДис Инв.",
                    "АВРвведен",
                    "АВРвведен Инв.",
                    "АВРвывед.",
                    "АВРвывед. Инв.",
                    "АВР ГОТОВ",
                    "АВР ГОТОВ Инв.",
                    "АВРдеблок",
                    "АВРдеблок Инв.",
                    "РЕЗЕРВ 6",
                    "РЕЗЕРВ 6 Инв.",
                    "РЕЗЕРВ 7",
                    "РЕЗЕРВ 7 Инв.",
                    "РЕЗЕРВ 8",
                    "РЕЗЕРВ 8 Инв.",
                    "РЕЗЕРВ 9",
                    "РЕЗЕРВ 9 Инв.",
                    "РЕЗЕРВ 10",
                    "РЕЗЕРВ 10 Инв.",
                    "РЕЗЕРВ 11",
                    "РЕЗЕРВ 11 Инв.",
                    "РЕЗЕРВ 12",
                    "РЕЗЕРВ 12 Инв.",
                    "РЕЗЕРВ 13",
                    "РЕЗЕРВ 13 Инв.",
                    "РЕЗЕРВ 14",
                    "РЕЗЕРВ 14 Инв.",
                    "РЕЗЕРВ 15",
                    "РЕЗЕРВ 15 Инв.",
                    "РЕЗЕРВ 16",
                    "РЕЗЕРВ 16 Инв.",
                    "РЕЗЕРВ 17",
                    "РЕЗЕРВ 17 Инв.",
                    "РЕЗЕРВ 18",
                    "РЕЗЕРВ 18 Инв.",
                    "РЕЗЕРВ 19",
                    "РЕЗЕРВ 19 Инв.",
                    "РЕЗЕРВ 20",
                    "РЕЗЕРВ 20 Инв.",
                };

                SortedDictionary<ushort, string> ret = new SortedDictionary<ushort, string>();
                for (ushort i = 0; i < list.Count; i++)
                {
                    ret.Add(i, list[i]);
                }

                switch (DeviceType)
                {
                    case MRUniversal.T4N4D42R35:
                    case MRUniversal.T4N5D42R35:
                        for (ushort i = 81; i < 84 + 93; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                    case MRUniversal.T4N4D74R67:
                    case MRUniversal.T4N5D74R67:
                        for (ushort i = 145; i < 145 + 32; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                    default:
                        //дискреты
                        for (ushort i = 33; i < 32 + 145; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                }

                switch (DeviceName)
                {
                    case "MR771":

                        //Двигатель
                        ushort deleteNInd = ret.First(v => v.Value == "БЛК. ПО N").Key;

                        for (ushort i = deleteNInd; i < deleteNInd + 4; i++)
                        {
                            ret.Remove(i);
                        }

                        //АВР
                        ushort deleteAvrInd = ret.First(v => v.Value == "АВР ВКЛ.").Key;

                        for (ushort i = deleteAvrInd; i < deleteAvrInd + 8; i++)
                        {
                            ret.Remove(i);
                        }

                        ushort changedNameSignalsInd = ret.First(v => v.Value == "АВРвывДис").Key;

                        ret[changedNameSignalsInd] = "ВЧС 1";
                        ret[(ushort)(changedNameSignalsInd + 1)] = "ВЧС 1 Инв.";
                        ret[(ushort)(changedNameSignalsInd + 2)] = "ВЧС 0";
                        ret[(ushort)(changedNameSignalsInd + 3)] = "ВЧС 0 Инв.";
                        ret[(ushort)(changedNameSignalsInd + 4)] = "ВЧС ОТКЛ.1";
                        ret[(ushort)(changedNameSignalsInd + 5)] = "ВЧС ОТКЛ.1 Инв.";
                        ret[(ushort)(changedNameSignalsInd + 6)] = "ВЧС ОТКЛ.0";
                        ret[(ushort)(changedNameSignalsInd + 7)] = "ВЧС ОТКЛ.0 Инв.";
                        ret[(ushort)(changedNameSignalsInd + 8)] = "РЕВЕРС? 1";
                        ret[(ushort)(changedNameSignalsInd + 9)] = "РЕВЕРС? 1 Инв.";
                        ret[(ushort)(changedNameSignalsInd + 10)] = "РЕВЕРС? 0";
                        ret[(ushort)(changedNameSignalsInd + 11)] = "РЕВЕРС? 0 Инв.";
                        ret[(ushort)(changedNameSignalsInd + 12)] = "РЕВЕРС 1";
                        ret[(ushort)(changedNameSignalsInd + 13)] = "РЕВЕРС 1 Инв.";
                        ret[(ushort)(changedNameSignalsInd + 14)] = "РЕВЕРС 0";
                        ret[(ushort)(changedNameSignalsInd + 15)] = "РЕВЕРС 0 Инв.";
                        ret[(ushort)(changedNameSignalsInd + 16)] = "Эхо 1";
                        ret[(ushort)(changedNameSignalsInd + 17)] = "Эхо 1 Инв.";
                        ret[(ushort)(changedNameSignalsInd + 18)] = "Эхо 0";
                        ret[(ushort)(changedNameSignalsInd + 19)] = "Эхо 0 Инв.";

                        //Ступени по Z
                        ushort deleteRezInd = ret.First(v => v.Value == "РЕЗЕРВ 1").Key;
                        ret[deleteRezInd] = "Z7 ИО";
                        ret[(ushort)(deleteRezInd + 1)] = "Z7 ИО Инв.";
                        ret[(ushort)(deleteRezInd + 2)] = "Z7";
                        ret[(ushort)(deleteRezInd + 3)] = "Z7 Инв.";
                        ret[(ushort)(deleteRezInd + 4)] = "Z8 ИО";
                        ret[(ushort)(deleteRezInd + 5)] = "Z8 ИО Инв.";
                        ret[(ushort)(deleteRezInd + 6)] = "Z8";
                        ret[(ushort)(deleteRezInd + 7)] = "Z8 Инв.";
                        ret[(ushort)(deleteRezInd + 8)] = "Z9 ИО";
                        ret[(ushort)(deleteRezInd + 9)] = "Z9 ИО Инв.";
                        ret[(ushort)(deleteRezInd + 10)] = "Z9";
                        ret[(ushort)(deleteRezInd + 11)] = "Z9 Инв.";
                        ret[(ushort)(deleteRezInd + 12)] = "Z10 ИО";
                        ret[(ushort)(deleteRezInd + 13)] = "Z10 ИО Инв.";
                        ret[(ushort)(deleteRezInd + 14)] = "Z10";
                        ret[(ushort)(deleteRezInd + 15)] = "Z10 Инв.";

                        ushort rez5Ind = ret.First(v => v.Value == "РЕЗЕРВ 5").Key;
                        ret.Remove(rez5Ind);
                        ret.Remove((ushort)(rez5Ind + 1));

                        ushort rez11Ind = ret.First(v => v.Value == "РЕЗЕРВ 11").Key;

                        for (ushort i = rez11Ind; i < rez11Ind + 20; i++)
                        {
                            ret.Remove(i);
                        }

                        break;
                    default:
                        break;
                }

                return ret.ToDictionary(r => r.Key, r => r.Value);
            }
        }

        public static List<string> DisableType
        {
            get
            {
                return new List<string>
                {
                    "tзапрета по фронту",
                    "с tзапрета по возврату"
                };
            }
        }
    public static List<string> HflSignalsTZNP { get; set; }
    public static List<string> DefaultHflSignalsTZNP => new List<string>
        {
            "НЕТ",
            "I*>1 ИО",
            "I*>1",
            "I*>2 ИО",
            "I*>2",
            "I*>3 ИО",
            "I*>3",
            "I*>4 ИО",
            "I*>4",
            "I*>5 ИО",
            "I*>5",
            "I*>6 ИО",
            "I*>6",
            "I*>7 ИО",
            "I*>7",
            "I*>8 ИО",
            "I*>8"
        };

        public static List<string> HflSignalsDZ { get; set; }
        public static List<string> DefaultHflSignalsDZ => new List<string>
        {
            "НЕТ",
            "Z1 ИО",
            "Z1",
            "Z2 ИО",
            "Z2",
            "Z3 ИО",
            "Z3",
            "Z4 ИО",
            "Z4",
            "Z5 ИО",
            "Z5",
            "Z6 ИО",
            "Z6",
            "Z7 ИО",
            "Z7",
            "Z8 ИО",
            "Z8",
            "Z9 ИО",
            "Z9",
            "Z10 ИО",
            "Z10"
        };

        /// <summary>
        /// Выведено/Введено
        /// </summary>
        public static List<string> OmpModes
        {
            get
            {
                return new List<string>
                {
                    "Выведено",
                    "1 участок",
                    "2 участка",
                    "3 участка",
                    "4 участка",
                    "5 участков"
                };
            }
        }

        public static List<string> NoYesDiscret
        {
            get
            {
                return new List<string>
                {
                    "НЕТ",
                    "Да",
                    "Дискрет"
                };
            }
        }

        /// <summary>
        /// U 
        /// </summary>
        public static List<string> InputU
        {
            get
            {
                return new List<string>
                {
                    "Ua",
                    "Ub",
                    "Uc",

                    "Uab",
                    "Ubc",
                    "Uca",
                    "Un"
                };
            }
        }

        /// <summary>
        /// U для синхронизма
        /// </summary>
        public static List<string> Usinhr
        {
            get
            {
                List<string> list = new List<string>
                {
                    "Ua",
                    "Ub",
                    "Uc",

                    "Uab",
                    "Ubc",
                    "Uca",
                    "Un"
                };

                switch (DeviceType)
                {
                    case "T4N5D42R35":
                    case "T4N5D74R67":
                        list.Add("Un1");
                        break;
                    default:
                        break;
                }
                return list;
            } 
        }

        /// <summary>
        /// Режим АПВ
        /// </summary>
        public static List<string> ApvModes
        {
            get
            {
                return new List<string>
                {
                    "НЕТ",
                    "1КРАТ",
                    "2КРАТ",
                    "3КРАТ",
                    "4КРАТ"
                };
            }
        }

        public static List<string> ContureV2
        {
            get
            {
                return new List<string>
                {
                    "Ф-Ф",
                    "Ф-N1",
                    "Ф-N2",
                    "Ф-N3",
                    "Ф-N4",
                    "Ф-N5"
                };
            }
        }

        public static List<string> ResistDefDirection
        {
            get
            {
                return new List<string>
                {
                    "НЕТ",
                    "Прямое",
                    "Обратное"
                };
            }
        }

        public static List<string> ResistDefType
        {
            get
            {
                return new List<string>
                {
                    "Полигональная",
                    "Круговая"
                };
            }
        }

        /// <summary>
        /// Выведено/Введено
        /// </summary>
        public static List<string> OffOn
        {
            get
            {
                return new List<string>
                {
                    "Выведено",
                    "Введено"
                };
            }
        }

        public static List<string> FreqDefType
        {
            get
            {
                return new List<string>
                {
                    "Частота",
                    "dF/dt"
                };
            }
        }

        /// <summary>
        /// тип для защит Umax
        /// </summary>
        public static List<string> UmaxDefenseMode
        {
            get
            {
                List<string> ret = new List<string>
                {
                    "Одна фаза",
                    "Все фазные",
                    "Одно линейное",
                    "Все линейные",
                    "3U0",
                    "U2",
                    "Un"
                };
                switch (DeviceType)
                {
                    case "T4N5D42R35":
                    case "T4N5D74R67":
                        ret.Add("Un1");
                        ret.Add("Ud");
                        break;
                    default:
                        break;
                }
                return ret;
            }
        }

        /// <summary>
        /// тип для защит Umin
        /// </summary>
        public static List<string> UminDefenseMode
        {
            get
            {
                List<string> list = new List<string>
                {
                    "Одна фаза",
                    "Все фазные",
                    "Одно линейное",
                    "Все линейные",
                    "Un"
                };
                switch (DeviceType)
                {
                    case "T4N5D42R35":
                    case "T4N5D74R67":
                        list.Add("Un1");
                        break;
                    default:
                        break;
                }

                return list;
            }
        }
        public static List<string> UStages
        {
            get
            {
                return new List<string>
                {
                    "U> 1",
                    "U> 2",
                    "U> 3",
                    "U> 4",
                    "U< 1",
                    "U< 2",
                    "U< 3",
                    "U< 4"
                };
            }
        }

        /// <summary>
        /// Режим Защиты
        /// </summary>
        public static List<string> DefenseModesShort
        {
            get
            {
                return new List<string>
                {
                    "Выведено",
                    "Введено",
                    "Сигнализация"
                };
            }
        }

        /// <summary>
        /// Привязка
        /// </summary>
        public static List<string> Binding
        {
            get
            {
                return new List<string>
                {
                    "C1",
                    "C2",
                    "C3",
                    "C4"
                };
            }
        }

        /// <summary>
        /// I*
        /// </summary>
        public static List<string> I =>
            new List<string>
            {
                "3I0",
                "I2",
                "In",
                "Iг",
                "Iем",
                "Iак"
            };

        public static List<string> Apv =>
            new List<string>
            {
                "Запрет",
                "Пуск"
            };

        /// <summary>
        /// Логика
        /// </summary>
        public static List<string> Logic =>
            new List<string>
            {
                "Одна фаза",
                "Две фазы",
                "Три фазы"
            };

        /// <summary>
        /// Режим Осц
        /// </summary>
        public static List<string> OscModes =>
            new List<string>
            {
                "Выведено",
                "Пуск по ИО",
                "Пуск по Защите"
            };


        /// <summary>
        /// Направление
        /// </summary>
        public static List<string> Direction =>
            new List<string>
            {
                "НЕТ",
                "Прямое",
                "Обратное"
            };

        /// <summary>
        /// Недост. напр(I*)
        /// </summary>
        public static List<string> Undir =>
            new List<string>
            {
                "Ненапр.",
                "Блок."
            };

        public static List<string> HflDirection => new List<string>
        {
            "3I0", "In"
        };

        public static List<string> HflModes => new List<string>
        {
            "Выведено", "ТБ", "ТУ недоохв.", "ТУ переохв."
        };

        public static List<string> HflDeblock => new List<string>
        {
            "Выведено", "Без перез.", "С перез."
        };

        public static List<string> HflEcho => new List<string>
        {
            "Выведено", "Эхо", "Эхо и откл."
        };

        public static List<string> BlockTn
        {
            get
            {
                return new List<string>
                {
                    "НЕТ",
                    "Неиспр.ТН с/п+мгн.",
                    "Мгн. неиспр. ТН",
                    "Неиспр.ТН с/п"
                };
            }
        }

        /// <summary>
        /// Тип ТН (U0)
        /// </summary>
        public static List<string> UoType
        {
            get
            {
                return new List<string>
                {
                    "Un",
                    "3U0"
                };
            }
        }

        /// <summary>
        /// Тип ТТ
        /// </summary>
        public static List<string> TtType
        {
            get
            {
                return new List<string>
                {
                    "Ia, Ib, Ic",
                    "Ia, Ic"
                };
            }
        }
        /// <summary>
        /// Токовый вход
        /// </summary>
        public static List<string> InpIn
        {
            get
            {
                return new List<string>
                {
                    "1 А",
                    "5 А"
                };
            }
        }

        public static List<string> Reserve
        {
            get
            {
                return new List<string>
                {
                    "НЕТ",
                    "HSR",
                    "PRP"
                };
            }
        }

        public static List<string> ReleyType
        {
            get
            {
                return new List<string>
                {
                    "Повторитель",
                    "Блинкер"
                };
            }
        }

        public static List<string> InpOporSignals
        {
            get
            {
                List<string> list = new List<string>
                {
                    "Ia",
                    "Ib",
                    "Ic",
                    "In",
                    "Ua",
                    "Ub",
                    "Uc",
                    "Un"
                };
                switch (DeviceType)
                {
                    case "T4N5D42R35":
                    case "T4N5D74R67":
                        list.Add("Un1");
                        break;
                    default:
                        break;
                }
                return list;
            }
        }

        public static Dictionary<ushort, string> SwitchSignals { get; set; }

        public static Dictionary<ushort, string> SwitchSignalsSetup(Dictionary<ushort, string> list)
        {
            switch (DeviceType)
            {
                case MRUniversal.T4N4D42R35:
                case MRUniversal.T4N5D42R35:
                    for (ushort i = 81; i < 84 + 93; i++)
                    {
                        list.Remove(i);
                    }
                    break;
                case MRUniversal.T4N4D74R67:
                case MRUniversal.T4N5D74R67:
                    for (ushort i = 145; i < 145 + 32; i++)
                    {
                        list.Remove(i);
                    }
                    break;
                default:
                    //дискреты
                    for (ushort i = 33; i < 32 + 145; i++)
                    {
                        list.Remove(i);
                    }
                    break;
            }

            return list.ToDictionary(r => r.Key, r => r.Value);
        }
        /// <summary>
        /// Сигналы выключателя
        /// </summary>
        public static Dictionary<ushort, string> DefaultSwitchSignals
        {
            get
            {
                List<string> list = new List<string>
                {
                    "НЕТ",
                    "Д1",
                    "Д1 Инв.",
                    "Д2",
                    "Д2 Инв.",
                    "Д3",
                    "Д3 Инв.",
                    "Д4",
                    "Д4 Инв.",
                    "Д5",
                    "Д5 Инв.",
                    "Д6",
                    "Д6 Инв.",
                    "Д7",
                    "Д7 Инв.",
                    "Д8",
                    "Д8 Инв.",
                    "Д9",
                    "Д9 Инв.",
                    "Д10",
                    "Д10 Инв.",
                    "Д11",
                    "Д11 Инв.",
                    "Д12",
                    "Д12 Инв.",
                    "Д13",
                    "Д13 Инв.",
                    "Д14",
                    "Д14 Инв.",
                    "Д15",
                    "Д15 Инв.",
                    "Д16",
                    "Д16 Инв.",
                    "Д17",
                    "Д17 Инв.",
                    "Д18",
                    "Д18 Инв.",
                    "Д19",
                    "Д19 Инв.",
                    "Д20",
                    "Д20 Инв.",
                    "Д21",
                    "Д21 Инв.",
                    "Д22",
                    "Д22 Инв.",
                    "Д23",
                    "Д23 Инв.",
                    "Д24",
                    "Д24 Инв.",
                    "Д25",
                    "Д25 Инв.",
                    "Д26",
                    "Д26 Инв.",
                    "Д27",
                    "Д27 Инв.",
                    "Д28",
                    "Д28 Инв.",
                    "Д29",
                    "Д29 Инв.",
                    "Д30",
                    "Д30 Инв.",
                    "Д31",
                    "Д31 Инв.",
                    "Д32",
                    "Д32 Инв.",
                    "Д33",
                    "Д33 Инв.",
                    "Д34",
                    "Д34 Инв.",
                    "Д35",
                    "Д35 Инв.",
                    "Д36",
                    "Д36 Инв.",
                    "Д37",
                    "Д37 Инв.",
                    "Д38",
                    "Д38 Инв.",
                    "Д39",
                    "Д39 Инв.",
                    "Д40",
                    "Д40 Инв.",
                    "Д41",
                    "Д41 Инв.",
                    "Д42",
                    "Д42 Инв.",
                    "Д43",
                    "Д43 Инв.",
                    "Д44",
                    "Д44 Инв.",
                    "Д45",
                    "Д45 Инв.",
                    "Д46",
                    "Д46 Инв.",
                    "Д47",
                    "Д47 Инв.",
                    "Д48",
                    "Д48 Инв.",
                    "Д49",
                    "Д49 Инв.",
                    "Д50",
                    "Д50 Инв.",
                    "Д51",
                    "Д51 Инв.",
                    "Д52",
                    "Д52 Инв.",
                    "Д53",
                    "Д53 Инв.",
                    "Д54",
                    "Д54 Инв.",
                    "Д55",
                    "Д55 Инв.",
                    "Д56",
                    "Д56 Инв.",
                    "Д57",
                    "Д57 Инв.",
                    "Д58",
                    "Д58 Инв.",
                    "Д59",
                    "Д59 Инв.",
                    "Д60",
                    "Д60 Инв.",
                    "Д61",
                    "Д61 Инв.",
                    "Д62",
                    "Д62 Инв.",
                    "Д63",
                    "Д63 Инв.",
                    "Д64",
                    "Д64 Инв.",
                    "Д65",
                    "Д65 Инв.",
                    "Д66",
                    "Д66 Инв.",
                    "Д67",
                    "Д67 Инв.",
                    "Д68",
                    "Д68 Инв.",
                    "Д69",
                    "Д69 Инв.",
                    "Д70",
                    "Д70 Инв.",
                    "Д71",
                    "Д71 Инв.",
                    "Д72",
                    "Д72 Инв.",
                    "Д73",
                    "Д73 Инв.",
                    "Д74",
                    "Д74 Инв.",
                    "Д75",
                    "Д75 Инв.",
                    "Д76",
                    "Д76 Инв.",
                    "Д77",
                    "Д77 Инв.",
                    "Д78",
                    "Д78 Инв.",
                    "Д79",
                    "Д79 Инв.",
                    "Д80",
                    "Д80 Инв.",
                    "Д81",
                    "Д81 Инв.",
                    "Д82",
                    "Д82 Инв.",
                    "Д83",
                    "Д83 Инв.",
                    "Д84",
                    "Д84 Инв.",
                    "Д85",
                    "Д85 Инв.",
                    "Д86",
                    "Д86 Инв.",
                    "Д87",
                    "Д87 Инв.",
                    "Д88",
                    "Д88 Инв.",
                    "КМД 1",
                    "КМД 1 Инв.",
                    "КМД 2",
                    "КМД 2 Инв.",
                    "КМД 3",
                    "КМД 3 Инв.",
                    "КМД 4",
                    "КМД 4 Инв.",
                    "КМД 5",
                    "КМД 5 Инв.",
                    "КМД 6",
                    "КМД 6 Инв.",
                    "КМД 7",
                    "КМД 7 Инв.",
                    "КМД 8",
                    "КМД 8 Инв.",
                    "КМД 9",
                    "КМД 9 Инв.",
                    "КМД 10",
                    "КМД 10 Инв.",
                    "КМД 11",
                    "КМД 11 Инв.",
                    "КМД 12",
                    "КМД 12 Инв.",
                    "КМД 13",
                    "КМД 13 Инв.",
                    "КМД 14",
                    "КМД 14 Инв.",
                    "КМД 15",
                    "КМД 15 Инв.",
                    "КМД 16",
                    "КМД 16 Инв.",
                    "КМД 17",
                    "КМД 17 Инв.",
                    "КМД 18",
                    "КМД 18 Инв.",
                    "КМД 19",
                    "КМД 19 Инв.",
                    "КМД 20",
                    "КМД 20 Инв.",
                    "КМД 21",
                    "КМД 21 Инв.",
                    "КМД 22",
                    "КМД 22 Инв.",
                    "КМД 23",
                    "КМД 23 Инв.",
                    "КМД 24",
                    "КМД 24 Инв.",
                    "RST1",
                    "RST1 Инв.",
                    "RST2",
                    "RST2 Инв.",
                    "RST3",
                    "RST3 Инв.",
                    "RST4",
                    "RST4 Инв.",
                    "RST5",
                    "RST5 Инв.",
                    "RST6",
                    "RST6 Инв.",
                    "RST7",
                    "RST7 Инв.",
                    "RST8",
                    "RST8 Инв.",
                    "RST9",
                    "RST9 Инв.",
                    "RST10",
                    "RST10 Инв.",
                    "RST11",
                    "RST11 Инв.",
                    "RST12",
                    "RST12 Инв.",
                    "RST13",
                    "RST13 Инв.",
                    "RST14",
                    "RST14 Инв.",
                    "RST15",
                    "RST15 Инв.",
                    "RST16",
                    "RST16 Инв.",

                    "ЛС1",
                    "ЛС1 Инв.",
                    "ЛС2",
                    "ЛС2 Инв.",
                    "ЛС3",
                    "ЛС3 Инв.",
                    "ЛС4",
                    "ЛС4 Инв.",
                    "ЛС5",
                    "ЛС5 Инв.",
                    "ЛС6",
                    "ЛС6 Инв.",
                    "ЛС7",
                    "ЛС7 Инв.",
                    "ЛС8",
                    "ЛС8 Инв.",
                    "ЛС9",
                    "ЛС9 Инв.",
                    "ЛС10",
                    "ЛС10 Инв.",
                    "ЛС11",
                    "ЛС11 Инв.",
                    "ЛС12",
                    "ЛС12 Инв.",
                    "ЛС13",
                    "ЛС13 Инв.",
                    "ЛС14",
                    "ЛС14 Инв.",
                    "ЛС15",
                    "ЛС15 Инв.",
                    "ЛС16",
                    "ЛС16 Инв.",
                    "БГС1",
                    "БГС1 Инв.",
                    "БГС2",
                    "БГС2 Инв.",
                    "БГС3",
                    "БГС3 Инв.",
                    "БГС4",
                    "БГС4 Инв.",
                    "БГС5",
                    "БГС5 Инв.",
                    "БГС6",
                    "БГС6 Инв.",
                    "БГС7",
                    "БГС7 Инв.",
                    "БГС8",
                    "БГС8 Инв.",
                    "БГС9",
                    "БГС9 Инв.",
                    "БГС10",
                    "БГС10 Инв.",
                    "БГС11",
                    "БГС11 Инв.",
                    "БГС12",
                    "БГС12 Инв.",
                    "БГС13",
                    "БГС13 Инв.",
                    "БГС14",
                    "БГС14 Инв.",
                    "БГС15",
                    "БГС15 Инв.",
                    "БГС16",
                    "БГС16 Инв.",
                    "ВЛС1",
                    "ВЛС1 Инв.",
                    "ВЛС2",
                    "ВЛС2 Инв.",
                    "ВЛС3",
                    "ВЛС3 Инв.",
                    "ВЛС4",
                    "ВЛС4 Инв.",
                    "ВЛС5",
                    "ВЛС5 Инв.",
                    "ВЛС6",
                    "ВЛС6 Инв.",
                    "ВЛС7",
                    "ВЛС7 Инв.",
                    "ВЛС8",
                    "ВЛС8 Инв.",
                    "ВЛС9",
                    "ВЛС9 Инв.",
                    "ВЛС10",
                    "ВЛС10 Инв.",
                    "ВЛС11",
                    "ВЛС11 Инв.",
                    "ВЛС12",
                    "ВЛС12 Инв.",
                    "ВЛС13",
                    "ВЛС13 Инв.",
                    "ВЛС14",
                    "ВЛС14 Инв.",
                    "ВЛС15",
                    "ВЛС15 Инв.",
                    "ВЛС16",
                    "ВЛС16 Инв.",
                    "ССЛ1",
                    "ССЛ1 Инв.",
                    "ССЛ2",
                    "ССЛ2 Инв.",
                    "ССЛ3",
                    "ССЛ3 Инв.",
                    "ССЛ4",
                    "ССЛ4 Инв.",
                    "ССЛ5",
                    "ССЛ5 Инв.",
                    "ССЛ6",
                    "ССЛ6 Инв.",
                    "ССЛ7",
                    "ССЛ7 Инв.",
                    "ССЛ8",
                    "ССЛ8 Инв.",
                    "ССЛ9",
                    "ССЛ9 Инв.",
                    "ССЛ10",
                    "ССЛ10 Инв.",
                    "ССЛ11",
                    "ССЛ11 Инв.",
                    "ССЛ12",
                    "ССЛ12 Инв.",
                    "ССЛ13",
                    "ССЛ13 Инв.",
                    "ССЛ14",
                    "ССЛ14 Инв.",
                    "ССЛ15",
                    "ССЛ15 Инв.",
                    "ССЛ16",
                    "ССЛ16 Инв.",
                    "ССЛ17",
                    "ССЛ17 Инв.",
                    "ССЛ18",
                    "ССЛ18 Инв.",
                    "ССЛ19",
                    "ССЛ19 Инв.",
                    "ССЛ20",
                    "ССЛ20 Инв.",
                    "ССЛ21",
                    "ССЛ21 Инв.",
                    "ССЛ22",
                    "ССЛ22 Инв.",
                    "ССЛ23",
                    "ССЛ23 Инв.",
                    "ССЛ24",
                    "ССЛ24 Инв.",
                    "ССЛ25",
                    "ССЛ25 Инв.",
                    "ССЛ26",
                    "ССЛ26 Инв.",
                    "ССЛ27",
                    "ССЛ27 Инв.",
                    "ССЛ28",
                    "ССЛ28 Инв.",
                    "ССЛ29",
                    "ССЛ29 Инв.",
                    "ССЛ30",
                    "ССЛ30 Инв.",
                    "ССЛ31",
                    "ССЛ31 Инв.",
                    "ССЛ32",
                    "ССЛ32 Инв.",
                    "ССЛ33",
                    "ССЛ33 Инв.",
                    "ССЛ34",
                    "ССЛ34 Инв.",
                    "ССЛ35",
                    "ССЛ35 Инв.",
                    "ССЛ36",
                    "ССЛ36 Инв.",
                    "ССЛ37",
                    "ССЛ37 Инв.",
                    "ССЛ38",
                    "ССЛ38 Инв.",
                    "ССЛ39",
                    "ССЛ39 Инв.",
                    "ССЛ40",
                    "ССЛ40 Инв.",
                    "ССЛ41",
                    "ССЛ41 Инв.",
                    "ССЛ42",
                    "ССЛ42 Инв.",
                    "ССЛ43",
                    "ССЛ43 Инв.",
                    "ССЛ44",
                    "ССЛ44 Инв.",
                    "ССЛ45",
                    "ССЛ45 Инв.",
                    "ССЛ46",
                    "ССЛ46 Инв.",
                    "ССЛ47",
                    "ССЛ47 Инв.",
                    "ССЛ48",
                    "ССЛ48 Инв."
                };

                SortedDictionary<ushort, string> ret = new SortedDictionary<ushort, string>();
                for (ushort i = 0; i < list.Count; i++)
                {
                    ret.Add(i, list[i]);
                }

                switch (DeviceType)
                {
                    case MRUniversal.T4N4D42R35:
                    case MRUniversal.T4N5D42R35:
                        for (ushort i = 81; i < 84 + 93; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                    case MRUniversal.T4N4D74R67:
                    case MRUniversal.T4N5D74R67:
                        for (ushort i = 145; i < 145 + 32; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                    default:
                        //дискреты
                        for (ushort i = 33; i < 32 + 145; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                }
               
                return ret.ToDictionary(r => r.Key, r => r.Value);
            }
        }

        public static List<string> TypeAVR
        {
            get
            {
                return new List<string>
                {
                    "Секции",
                    "Реклоузера"
                };
            }
        }

        public static List<string> RSPriority
        {
            get
            {
                return new List<string>
                {
                    "R",
                    "S"
                };
            }
        }

        public static List<string> ModeRele
        {
            get
            {
                return new List<string>
                {
                    "Статический",
                    "Стат. зеленый",
                    "Стат. красный",
                    "Мигающий"
                };
            }
        }

        public static List<string> StageDefType
        {
            get
            {
                return new List<string>
                {
                    "Ступень P 1",
                    "Ступень P 2"
                };
            }
        }

        public static List<string> Mode
        {
            get
            {
                return new List<string>
                {
                    "Выведено",
                    "Введено",
                    "Сигнализация",
                    "Отключение"
                };
            }
        }

        public static List<string> TypeCommand => new List<string>{"Испульсный", "Длительный"};

        public static List<string> ModeAntiBounce
        {
            get
            {
                return new List<string>
                {
                    "Перем. оперток",
                    "Пост. оперток",
                };
            }
        }

        public static List<string> CountAntiBounce
        {
            get
            {
                List<string> list = new List<string>();
                int ms = 0;
                for (int i = 0; i < 64; i++)
                {
                    list.Add(ms + " мс");
                    ms += 5;
                }

                return list;
            }
        }

        public static List<string> CountCommands
        {
            get
            {
                List<string> list = new List<string>();
                for (int i = 0; i < 24; i++)
                {
                    list.Add((i + 1).ToString());
                }
                return list;
            }
        }
        
        public static List<string> ModesLightOsc
        {
            get
            {
                return new List<string>
                {
                    "Выведено",
                    "Пуск по ИО",
                    "Пуск по защите"
                };
            }
        }
        public static List<string> YesNo
        {
            get
            {
                return new List<string>
                {
                    "НЕТ",
                    "Есть"
                };
            }
        }

        public static List<string> ModesLight
        {
            get
            {
                return new List<string>
                {
                    "Выведено",
                    "Введено"
                };
            }
        }

        public static List<string> Characteristic
        {
            get
            {
                return new List<string>
                {
                    "Независимая",
                    "Зависимая"
                };
            }
        }

        public static List<string> UBtypes => new List<string>
        {
            "Одна фаза", "Все фазы", "Одно лин.", "Все лин.","3U0","U2", "Un"
        };

        public static List<string> UMtypes => new List<string>
        {
            "Одна фаза", "Все фазы", "Одно лин.", "Все лин.", "Un"
        };

        public static List<string> Unames => new List<string>
        {
            "U> 1","U> 2", "U< 1","U< 2"
        };

        public static List<string> ConfigIgssh => new List<string>
        {
            "Ig2СШ2","Ig3ПО"
        };


        public static List<string> LogicValues
        {
            get
            {
                return new List<string>
                {
                    "НЕТ",
                    "Да",
                    "Инверс"
                };
            }
        }

        public static List<string> SignalType
        {
            get
            {
                return new List<string>
                {
                    "Повторитель",
                    "Блинкер"
                };
            }
        }

        public static List<string> TtFault
        {
            get
            {
                return new List<string>
                {
                    "Выведен",
                    "Неисправность",
                    "Блок+Неисправность"
                };
            }
        }

        public static List<string> ResetTT
        {
            get
            {
                return new List<string>
                {
                    "Ручной",
                    "Автомат."
                };
            }
        }

        public static List<string> KONTR
        {
            get
            {
                return new List<string>
                {
                    "По току",
                    "БК+ТОК"
                };
            }
        }

        public static List<string> Forbidden
        {
            get
            {
                return new List<string>
                {
                    "Запрещено",
                    "Разрешено"
                };
            }
        }

        /// <summary>
        /// Названия реле
        /// </summary>
        //public static List<string> RelayNames
        //{
        //    get
        //    {
        //        List<string> relayNames = new List<string>();
        //        for (int i = 0; i < AllReleStruct.RELAY_MEMORY_COUNT; i++)
        //        {
        //            relayNames.Add((i + 1).ToString(CultureInfo.InvariantCulture));
        //        }
        //        return relayNames;
        //    }
        //}

        /// <summary>
        /// Названия индикаторов
        /// </summary>
        //public static List<string> IndNames
        //{
        //    get
        //    {
        //        var indNames = new List<string>();

        //        for (int i = 0; i < AllIndicatorsStruct.COUNT; i++)
        //        {
        //            indNames.Add((i + 1).ToString());
        //        }
        //        return indNames;
        //    }
        //}

        //public static List<string> LogicSignalNames
        //{
        //    get
        //    {
        //        List<string> ret = new List<string>();
        //        for (int i = 1; i <= InputLogicStruct.DiscrestCount; i++)
        //        {
        //            ret.Add("Д" + i);
        //        }
        //        return ret;
        //    }
        //}

        /// <summary>
        /// Названия каналов c базами
        /// </summary>
        public static List<string> OscChannelWithBaseNames
        {
            get
            {
                List<string> channelNames = new List<string>();
                for (int i = 0; i < OscopeAllChannelsStruct.KANAL_COUNT; i++)
                {
                    channelNames.Add((i + 1).ToString(CultureInfo.InvariantCulture));
                }
                return channelNames;
            }
        }

        /// <summary>
        /// Названия присоединений
        /// </summary>
        //public static List<string> ConnectionNames
        //{
        //    get
        //    {
        //        List<string> indNames = new List<string>();
        //        for (int i = 1; i <= AllUrovConnectionStruct.CurrentUrovConnectionCount; i++)
        //        {
        //            indNames.Add("Присоединение " + i);
        //        }
        //        return indNames;
        //    }
        //}

        /// <summary>
        /// Названия индикаторов
        /// </summary>
        public static List<string> DefNames
        {
            get
            {
                return new List<string>
                {
                    "Iд1 СШ1",
                    "Iд2 СШ2",
                    "Iд3 ПО",
                    "Iд1м СШ1",
                    "Iд2м СШ2",
                    "Iд3м ПО"
                };
            }
        }

        //public static List<string> ExternalnNames
        //{
        //    get
        //    {
        //        var indNames = new List<string>();


        //        for (int i = 0; i < AllExternalDefenseStruct.CurrentCount; i++)
        //        {
        //            indNames.Add("ВЗ-" + (i + 1));
        //        }
        //        return indNames;
        //    }
        //}

        public static List<string> OscLength
        {
            get
            {
                return new List<string>
                {
                    "1",
                    "2",
                    "3",
                    "4",
                    "5",
                    "6",
                    "7",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "15",
                    "16",
                    "17",
                    "18",
                    "19",
                    "20",
                    "21",
                    "22",
                    "23",
                    "24",
                    "25",
                    "26",
                    "27",
                    "28",
                    "29",
                    "30",
                    "31",
                    "32",
                    "33",
                    "34",
                    "35",
                    "36",
                    "37",
                    "38",
                    "39",
                    "40"
                };
            }
        }
        public static List<string> TypeI
        {
            get
            {
                return new List<string>
                {
                    "Всегда",
                    "Работа",
                    "Пуск"
                };
            }
        }

        public static Dictionary<ushort, string> RelaySignals { get; set; }

        public static Dictionary<ushort, string> RelaySignalsSetup(Dictionary<ushort, string> list)
        {
            switch (DeviceType)
            {
                case MRUniversal.T4N4D42R35:
                case MRUniversal.T4N5D42R35:
                    for (ushort i = 81; i < 84 + 93; i++)
                    {
                        list.Remove(i);
                    }
                    break;
                case MRUniversal.T4N4D74R67:
                case MRUniversal.T4N5D74R67:
                    for (ushort i = 145; i < 145 + 32; i++)
                    {
                        list.Remove(i);
                    }
                    break;
                default:
                    //дискреты
                    for (ushort i = 33; i < 32 + 145; i++)
                    {
                        list.Remove(i);
                    }
                    break;
            }

            switch (DeviceName)
            {
                case "MR771":

                    //Двигатель
                    ushort deleteNInd = 623;//key for deleted БЛОК. по N

                    for (ushort i = deleteNInd; i < deleteNInd + 4; i++)
                    {
                        list.Remove(i);
                    }

                    //АВР
                    ushort deleteAvrInd = 699;

                    for (ushort i = deleteAvrInd; i < deleteAvrInd + 8; i++)
                    {
                        list.Remove(i);
                    }

                    ushort changedNameSignalsInd = 707;//АВРвывДис

                    list[changedNameSignalsInd] = "ТС ДЗ";
                    list[(ushort)(changedNameSignalsInd + 1)] = "ТС ДЗ Инв.";
                    list[(ushort)(changedNameSignalsInd + 2)] = "ТС ТЗНП";
                    list[(ushort)(changedNameSignalsInd + 3)] = "ТС ТЗНП Инв.";
                    list[(ushort)(changedNameSignalsInd + 4)] = "ТО ДЗ";
                    list[(ushort)(changedNameSignalsInd + 5)] = "ТО ДЗ Инв.";
                    list[(ushort)(changedNameSignalsInd + 6)] = "ТО ТЗНП";
                    list[(ushort)(changedNameSignalsInd + 7)] = "ТО ТЗНП Инв.";
                    list[(ushort)(changedNameSignalsInd + 8)] = "РЕВ. ДЗ";
                    list[(ushort)(changedNameSignalsInd + 9)] = "РЕВ. ДЗ Инв.";
                    list[(ushort)(changedNameSignalsInd + 10)] = "РЕВ ТЗНП";
                    list[(ushort)(changedNameSignalsInd + 11)] = "РЕВ ТЗНП Инв.";
                    list[(ushort)(changedNameSignalsInd + 12)] = "КСП ДЗ";
                    list[(ushort)(changedNameSignalsInd + 13)] = "КСП ДЗ Инв.";
                    list[(ushort)(changedNameSignalsInd + 14)] = "КСП ТЗНП";
                    list[(ushort)(changedNameSignalsInd + 15)] = "КСП ТЗНП Инв.";
                    list[(ushort)(changedNameSignalsInd + 16)] = "Эхо ДЗ";
                    list[(ushort)(changedNameSignalsInd + 17)] = "Эхо ДЗ Инв.";
                    list[(ushort)(changedNameSignalsInd + 18)] = "Эхо ТЗНП";
                    list[(ushort)(changedNameSignalsInd + 19)] = "Эхо ТЗНП Инв.";

                    //Ступени по Z
                    ushort deleteRezInd = 473;//Reserve1
                    list[deleteRezInd] = "Z7 ИО";
                    list[(ushort)(deleteRezInd + 1)] = "Z7 ИО Инв.";
                    list[(ushort)(deleteRezInd + 2)] = "Z7";
                    list[(ushort)(deleteRezInd + 3)] = "Z7 Инв.";
                    list[(ushort)(deleteRezInd + 4)] = "Z8 ИО";
                    list[(ushort)(deleteRezInd + 5)] = "Z8 ИО Инв.";
                    list[(ushort)(deleteRezInd + 6)] = "Z8";
                    list[(ushort)(deleteRezInd + 7)] = "Z8 Инв.";
                    list[(ushort)(deleteRezInd + 8)] = "Z9 ИО";
                    list[(ushort)(deleteRezInd + 9)] = "Z9 ИО Инв.";
                    list[(ushort)(deleteRezInd + 10)] = "Z9";
                    list[(ushort)(deleteRezInd + 11)] = "Z9 Инв.";
                    list[(ushort)(deleteRezInd + 12)] = "Z10 ИО";
                    list[(ushort)(deleteRezInd + 13)] = "Z10 ИО Инв.";
                    list[(ushort)(deleteRezInd + 14)] = "Z10";
                    list[(ushort)(deleteRezInd + 15)] = "Z10 Инв.";

                    ushort rez5Ind = 629;//Reserve5
                    list.Remove(rez5Ind);
                    list.Remove((ushort)(rez5Ind + 1));

                    ushort rez11Ind = 727;//Reserve11

                    for (ushort i = rez11Ind; i < rez11Ind + 20; i++)
                    {
                        list.Remove(i);
                    }

                    break;
                default:
                    break;
            }

            return list.ToDictionary(r => r.Key, r => r.Value);
        }
        public static Dictionary<ushort, string> DefaultRelaySignals
        {
            get
            {
                List<string> list = new List<string>
                {
                    "НЕТ",
                    "Д1",
                    "Д1 Инв.",
                    "Д2",
                    "Д2 Инв.",
                    "Д3",
                    "Д3 Инв.",
                    "Д4",
                    "Д4 Инв.",
                    "Д5",
                    "Д5 Инв.",
                    "Д6",
                    "Д6 Инв.",
                    "Д7",
                    "Д7 Инв.",
                    "Д8",
                    "Д8 Инв.",
                    "Д9",
                    "Д9 Инв.",
                    "Д10",
                    "Д10 Инв.",
                    "Д11",
                    "Д11 Инв.",
                    "Д12",
                    "Д12 Инв.",
                    "Д13",
                    "Д13 Инв.",
                    "Д14",
                    "Д14 Инв.",
                    "Д15",
                    "Д15 Инв.",
                    "Д16",
                    "Д16 Инв.",
                    "Д17",
                    "Д17 Инв.",
                    "Д18",
                    "Д18 Инв.",
                    "Д19",
                    "Д19 Инв.",
                    "Д20",
                    "Д20 Инв.",
                    "Д21",
                    "Д21 Инв.",
                    "Д22",
                    "Д22 Инв.",
                    "Д23",
                    "Д23 Инв.",
                    "Д24",
                    "Д24 Инв.",
                    "Д25",
                    "Д25 Инв.",
                    "Д26",
                    "Д26 Инв.",
                    "Д27",
                    "Д27 Инв.",
                    "Д28",
                    "Д28 Инв.",
                    "Д29",
                    "Д29 Инв.",
                    "Д30",
                    "Д30 Инв.",
                    "Д31",
                    "Д31 Инв.",
                    "Д32",
                    "Д32 Инв.",
                    "Д33",
                    "Д33 Инв.",
                    "Д34",
                    "Д34 Инв.",
                    "Д35",
                    "Д35 Инв.",
                    "Д36",
                    "Д36 Инв.",
                    "Д37",
                    "Д37 Инв.",
                    "Д38",
                    "Д38 Инв.",
                    "Д39",
                    "Д39 Инв.",
                    "Д40",
                    "Д40 Инв.",
                    "Д41",
                    "Д41 Инв.",
                    "Д42",
                    "Д42 Инв.",
                    "Д43",
                    "Д43 Инв.",
                    "Д44",
                    "Д44 Инв.",
                    "Д45",
                    "Д45 Инв.",
                    "Д46",
                    "Д46 Инв.",
                    "Д47",
                    "Д47 Инв.",
                    "Д48",
                    "Д48 Инв.",
                    "Д49",
                    "Д49 Инв.",
                    "Д50",
                    "Д50 Инв.",
                    "Д51",
                    "Д51 Инв.",
                    "Д52",
                    "Д52 Инв.",
                    "Д53",
                    "Д53 Инв.",
                    "Д54",
                    "Д54 Инв.",
                    "Д55",
                    "Д55 Инв.",
                    "Д56",
                    "Д56 Инв.",
                    "Д57",
                    "Д57 Инв.",
                    "Д58",
                    "Д58 Инв.",
                    "Д59",
                    "Д59 Инв.",
                    "Д60",
                    "Д60 Инв.",
                    "Д61",
                    "Д61 Инв.",
                    "Д62",
                    "Д62 Инв.",
                    "Д63",
                    "Д63 Инв.",
                    "Д64",
                    "Д64 Инв.",
                    "Д65",
                    "Д65 Инв.",
                    "Д66",
                    "Д66 Инв.",
                    "Д67",
                    "Д67 Инв.",
                    "Д68",
                    "Д68 Инв.",
                    "Д69",
                    "Д69 Инв.",
                    "Д70",
                    "Д70 Инв.",
                    "Д71",
                    "Д71 Инв.",
                    "Д72",
                    "Д72 Инв.",
                    "Д73",
                    "Д73 Инв.",
                    "Д74",
                    "Д74 Инв.",
                    "Д75",
                    "Д75 Инв.",
                    "Д76",
                    "Д76 Инв.",
                    "Д77",
                    "Д77 Инв.",
                    "Д78",
                    "Д78 Инв.",
                    "Д79",
                    "Д79 Инв.",
                    "Д80",
                    "Д80 Инв.",
                    "Д81",
                    "Д81 Инв.",
                    "Д82",
                    "Д82 Инв.",
                    "Д83",
                    "Д83 Инв.",
                    "Д84",
                    "Д84 Инв.",
                    "Д85",
                    "Д85 Инв.",
                    "Д86",
                    "Д86 Инв.",
                    "Д87",
                    "Д87 Инв.",
                    "Д88",
                    "Д88 Инв.",

                    "КМД 1",
                    "КМД 1 Инв.",
                    "КМД 2",
                    "КМД 2 Инв.",
                    "КМД 3",
                    "КМД 3 Инв.",
                    "КМД 4",
                    "КМД 4 Инв.",
                    "КМД 5",
                    "КМД 5 Инв.",
                    "КМД 6",
                    "КМД 6 Инв.",
                    "КМД 7",
                    "КМД 7 Инв.",
                    "КМД 8",
                    "КМД 8 Инв.",
                    "КМД 9",
                    "КМД 9 Инв.",
                    "КМД 10",
                    "КМД 10 Инв.",
                    "КМД 11",
                    "КМД 11 Инв.",
                    "КМД 12",
                    "КМД 12 Инв.",
                    "КМД 13",
                    "КМД 13 Инв.",
                    "КМД 14",
                    "КМД 14 Инв.",
                    "КМД 15",
                    "КМД 15 Инв.",
                    "КМД 16",
                    "КМД 16 Инв.",
                    "КМД 17",
                    "КМД 17 Инв.",
                    "КМД 18",
                    "КМД 18 Инв.",
                    "КМД 19",
                    "КМД 19 Инв.",
                    "КМД 20",
                    "КМД 20 Инв.",
                    "КМД 21",
                    "КМД 21 Инв.",
                    "КМД 22",
                    "КМД 22 Инв.",
                    "КМД 23",
                    "КМД 23 Инв.",
                    "КМД 24",
                    "КМД 24 Инв.",
                    "RST1",
                    "RST1 Инв.",
                    "RST2",
                    "RST2 Инв.",
                    "RST3",
                    "RST3 Инв.",
                    "RST4",
                    "RST4 Инв.",
                    "RST5",
                    "RST5 Инв.",
                    "RST6",
                    "RST6 Инв.",
                    "RST7",
                    "RST7 Инв.",
                    "RST8",
                    "RST8 Инв.",
                    "RST9",
                    "RST9 Инв.",
                    "RST10",
                    "RST10 Инв.",
                    "RST11",
                    "RST11 Инв.",
                    "RST12",
                    "RST12 Инв.",
                    "RST13",
                    "RST13 Инв.",
                    "RST14",
                    "RST14 Инв.",
                    "RST15",
                    "RST15 Инв.",
                    "RST16",
                    "RST16 Инв.",

                    "ЛС1",
                    "ЛС1 Инв.",
                    "ЛС2",
                    "ЛС2 Инв.",
                    "ЛС3",
                    "ЛС3 Инв.",
                    "ЛС4",
                    "ЛС4 Инв.",
                    "ЛС5",
                    "ЛС5 Инв.",
                    "ЛС6",
                    "ЛС6 Инв.",
                    "ЛС7",
                    "ЛС7 Инв.",
                    "ЛС8",
                    "ЛС8 Инв.",
                    "ЛС9",
                    "ЛС9 Инв.",
                    "ЛС10",
                    "ЛС10 Инв.",
                    "ЛС11",
                    "ЛС11 Инв.",
                    "ЛС12",
                    "ЛС12 Инв.",
                    "ЛС13",
                    "ЛС13 Инв.",
                    "ЛС14",
                    "ЛС14 Инв.",
                    "ЛС15",
                    "ЛС15 Инв.",
                    "ЛС16",
                    "ЛС16 Инв.",
                    "БГС1",
                    "БГС1 Инв.",
                    "БГС2",
                    "БГС2 Инв.",
                    "БГС3",
                    "БГС3 Инв.",
                    "БГС4",
                    "БГС4 Инв.",
                    "БГС5",
                    "БГС5 Инв.",
                    "БГС6",
                    "БГС6 Инв.",
                    "БГС7",
                    "БГС7 Инв.",
                    "БГС8",
                    "БГС8 Инв.",
                    "БГС9",
                    "БГС9 Инв.",
                    "БГС10",
                    "БГС10 Инв.",
                    "БГС11",
                    "БГС11 Инв.",
                    "БГС12",
                    "БГС12 Инв.",
                    "БГС13",
                    "БГС13 Инв.",
                    "БГС14",
                    "БГС14 Инв.",
                    "БГС15",
                    "БГС15 Инв.",
                    "БГС16",
                    "БГС16 Инв.",
                    "ВЛС1",
                    "ВЛС1 Инв.",
                    "ВЛС2",
                    "ВЛС2 Инв.",
                    "ВЛС3",
                    "ВЛС3 Инв.",
                    "ВЛС4",
                    "ВЛС4 Инв.",
                    "ВЛС5",
                    "ВЛС5 Инв.",
                    "ВЛС6",
                    "ВЛС6 Инв.",
                    "ВЛС7",
                    "ВЛС7 Инв.",
                    "ВЛС8",
                    "ВЛС8 Инв.",
                    "ВЛС9",
                    "ВЛС9 Инв.",
                    "ВЛС10",
                    "ВЛС10 Инв.",
                    "ВЛС11",
                    "ВЛС11 Инв.",
                    "ВЛС12",
                    "ВЛС12 Инв.",
                    "ВЛС13",
                    "ВЛС13 Инв.",
                    "ВЛС14",
                    "ВЛС14 Инв.",
                    "ВЛС15",
                    "ВЛС15 Инв.",
                    "ВЛС16",
                    "ВЛС16 Инв.",
                    "ССЛ1",
                    "ССЛ1 Инв.",
                    "ССЛ2",
                    "ССЛ2 Инв.",
                    "ССЛ3",
                    "ССЛ3 Инв.",
                    "ССЛ4",
                    "ССЛ4 Инв.",
                    "ССЛ5",
                    "ССЛ5 Инв.",
                    "ССЛ6",
                    "ССЛ6 Инв.",
                    "ССЛ7",
                    "ССЛ7 Инв.",
                    "ССЛ8",
                    "ССЛ8 Инв.",
                    "ССЛ9",
                    "ССЛ9 Инв.",
                    "ССЛ10",
                    "ССЛ10 Инв.",
                    "ССЛ11",
                    "ССЛ11 Инв.",
                    "ССЛ12",
                    "ССЛ12 Инв.",
                    "ССЛ13",
                    "ССЛ13 Инв.",
                    "ССЛ14",
                    "ССЛ14 Инв.",
                    "ССЛ15",
                    "ССЛ15 Инв.",
                    "ССЛ16",
                    "ССЛ16 Инв.",
                    "ССЛ17",
                    "ССЛ17 Инв.",
                    "ССЛ18",
                    "ССЛ18 Инв.",
                    "ССЛ19",
                    "ССЛ19 Инв.",
                    "ССЛ20",
                    "ССЛ20 Инв.",
                    "ССЛ21",
                    "ССЛ21 Инв.",
                    "ССЛ22",
                    "ССЛ22 Инв.",
                    "ССЛ23",
                    "ССЛ23 Инв.",
                    "ССЛ24",
                    "ССЛ24 Инв.",
                    "ССЛ25",
                    "ССЛ25 Инв.",
                    "ССЛ26",
                    "ССЛ26 Инв.",
                    "ССЛ27",
                    "ССЛ27 Инв.",
                    "ССЛ28",
                    "ССЛ28 Инв.",
                    "ССЛ29",
                    "ССЛ29 Инв.",
                    "ССЛ30",
                    "ССЛ30 Инв.",
                    "ССЛ31",
                    "ССЛ31 Инв.",
                    "ССЛ32",
                    "ССЛ32 Инв.",
                    "ССЛ33",
                    "ССЛ33 Инв.",
                    "ССЛ34",
                    "ССЛ34 Инв.",
                    "ССЛ35",
                    "ССЛ35 Инв.",
                    "ССЛ36",
                    "ССЛ36 Инв.",
                    "ССЛ37",
                    "ССЛ37 Инв.",
                    "ССЛ38",
                    "ССЛ38 Инв.",
                    "ССЛ39",
                    "ССЛ39 Инв.",
                    "ССЛ40",
                    "ССЛ40 Инв.",
                    "ССЛ41",
                    "ССЛ41 Инв.",
                    "ССЛ42",
                    "ССЛ42 Инв.",
                    "ССЛ43",
                    "ССЛ43 Инв.",
                    "ССЛ44",
                    "ССЛ44 Инв.",
                    "ССЛ45",
                    "ССЛ45 Инв.",
                    "ССЛ46",
                    "ССЛ46 Инв.",
                    "ССЛ47",
                    "ССЛ47 Инв.",
                    "ССЛ48",
                    "ССЛ48 Инв.",
                    "Z 1 ИО",
                    "Z 1 ИО Инв.",
                    "Z 1",
                    "Z 1 Инв.",
                    "Z 2 ИО",
                    "Z 2 ИО Инв.",
                    "Z 2",
                    "Z 2 Инв.",
                    "Z 3 ИО",
                    "Z 3 ИО Инв.",
                    "Z 3",
                    "Z 3 Инв.",
                    "Z 4 ИО",
                    "Z 4 ИО Инв.",
                    "Z 4",
                    "Z 4 Инв.",
                    "Z 5 ИО",
                    "Z 5 ИО Инв.",
                    "Z 5",
                    "Z 5 Инв.",
                    "Z 6 ИО",
                    "Z 6 ИО Инв.",
                    "Z 6",
                    "Z 6 Инв.",
                    "РЕЗЕРВ 1",
                    "РЕЗЕРВ 1 Инв.",
                    "РЕЗЕРВ 2",
                    "РЕЗЕРВ 2 Инв.",
                    "P 1 ИО",
                    "P 1 ИО Инв.",
                    "P 1",
                    "P 1 Инв.",
                    "P 2 ИО",
                    "P 2 ИО Инв.",
                    "P 2",
                    "P 2 Инв.",
                    "РЕЗЕРВ 3",
                    "РЕЗЕРВ 3 Инв.",
                    "РЕЗЕРВ 4",
                    "РЕЗЕРВ 4 Инв.",
                    "I> 1 ИО",
                    "I> 1 ИО Инв.",
                    "I> 1",
                    "I> 1 Инв.",
                    "I> 2 ИО",
                    "I> 2 ИО Инв.",
                    "I> 2",
                    "I> 2 Инв.",
                    "I> 3 ИО",
                    "I> 3 ИО Инв.",
                    "I> 3",
                    "I> 3 Инв.",
                    "I> 4 ИО",
                    "I> 4 ИО Инв.",
                    "I> 4",
                    "I> 4 Инв.",
                    "I> 5 ИО",
                    "I> 5 ИО Инв.",
                    "I> 5",
                    "I> 5 Инв.",
                    "I> 6 ИО",
                    "I> 6 ИО Инв.",
                    "I> 6",
                    "I> 6 Инв.",
                    "I< ИО",
                    "I< ИО Инв.",
                    "I<",
                    "I< Инв.",
                    "I*> 1 ИО",
                    "I*> 1 ИО Инв.",
                    "I*> 1",
                    "I*> 1 Инв.",
                    "I*> 2 ИО",
                    "I*> 2 ИО Инв.",
                    "I*> 2",
                    "I*> 2 Инв.",
                    "I*> 3 ИО",
                    "I*> 3 ИО Инв.",
                    "I*> 3",
                    "I*> 3 Инв.",
                    "I*> 4 ИО",
                    "I*> 4 ИО Инв.",
                    "I*> 4",
                    "I*> 4 Инв.",
                    "I*> 5 ИО",
                    "I*> 5 ИО Инв.",
                    "I*> 5",
                    "I*> 5 Инв.",
                    "I*> 6 ИО",
                    "I*> 6 ИО Инв.",
                    "I*> 6",
                    "I*> 6 Инв.",
                    "I*> 7 ИО",
                    "I*> 7 ИО Инв.",
                    "I*> 7",
                    "I*> 7 Инв.",
                    "I*> 8 ИО",
                    "I*> 8 ИО Инв.",
                    "I*> 8",
                    "I*> 8 Инв.",
                    "I2/I1> ИО",
                    "I2/I1> ИО Инв.",
                    "I2/I1>",
                    "I2/I1> Инв.",
                    "U> 1 ИО",
                    "U> 1 ИО Инв.",
                    "U> 1",
                    "U> 1 Инв.",
                    "U> 2 ИО",
                    "U> 2 ИО Инв.",
                    "U> 2",
                    "U> 2 Инв.",
                    "U> 3 ИО",
                    "U> 3 ИО Инв.",
                    "U> 3",
                    "U> 3 Инв.",
                    "U> 4 ИО",
                    "U> 4 ИО Инв.",
                    "U> 4",
                    "U> 4 Инв.",
                    "U< 1 ИО",
                    "U< 1 ИО Инв.",
                    "U< 1",
                    "U< 1 Инв.",
                    "U< 2 ИО",
                    "U< 2 ИО Инв.",
                    "U< 2",
                    "U< 2 Инв.",
                    "U< 3 ИО",
                    "U< 3 ИО Инв.",
                    "U< 3",
                    "U< 3 Инв.",
                    "U< 4 ИО",
                    "U< 4 ИО Инв.",
                    "U< 4",
                    "U< 4 Инв.",
                    "F> 1 ИО",
                    "F> 1 ИО Инв.",
                    "F> 1",
                    "F> 1 Инв.",
                    "F> 2 ИО",
                    "F> 2 ИО Инв.",
                    "F> 2",
                    "F> 2 Инв.",
                    "F> 3 ИО",
                    "F> 3 ИО Инв.",
                    "F> 3",
                    "F> 3 Инв.",
                    "F> 4 ИО",
                    "F> 4 ИО Инв.",
                    "F> 4",
                    "F> 4 Инв.",
                    "F< 1 ИО",
                    "F< 1 ИО Инв.",
                    "F< 1",
                    "F< 1 Инв.",
                    "F< 2 ИО",
                    "F< 2 ИОИнв.",
                    "F< 2",
                    "F< 2 Инв.",
                    "F< 3 ИО",
                    "F< 3 ИО Инв.",
                    "F< 3",
                    "F< 3 Инв.",
                    "F< 4 ИО",
                    "F< 4 ИО Инв.",
                    "F< 4",
                    "F< 4 Инв.",
                    "Q> 1",
                    "Q> 1 Инв.",
                    "Q> 2",
                    "Q> 2 Инв.",
                    "БЛК.ПО Q",
                    "БЛК.ПО Q Инв.",
                    "БЛК. ПО N",
                    "БЛК. ПО N Инв.",
                    "ПУСК",
                    "ПУСК Инв.",
                    "НЕИСПР.",
                    "НЕИСПР. Инв.",
                    "РЕЗЕРВ 5",
                    "РЕЗЕРВ 5 Инв.",
                    "УСКОРЕНИЕ",
                    "УСКОРЕНИЕ Инв.",
                    "СИГНАЛ-ЦИЯ",
                    "СИГНАЛ-ЦИЯ Инв.",
                    "АВАР.ОТКЛ",
                    "АВАР.ОТКЛ Инв.",
                    "ОТКЛ.ВЫКЛ.",
                    "ОТКЛ.ВЫКЛ. Инв.",
                    "ВКЛ.ВЫКЛ.",
                    "ВКЛ.ВЫКЛ. Инв.",
                    "ПУСК АПВ",
                    "ПУСК АПВ Инв.",
                    "АПВ 1крат",
                    "АПВ 1крат Инв.",
                    "АПВ 2крат ",
                    "АПВ 2крат Инв.",
                    "АПВ 3крат ",
                    "АПВ 3крат Инв.",
                    "АПВ 4крат",
                    "АПВ 4крат Инв.",
                    "ВКЛ.поАПВ",
                    "ВКЛ.поАПВ Инв.",
                    "ЗАПРЕТ АПВ",
                    "ЗАПРЕТ АПВ Инв.",
                    "АПВ БЛОК.",
                    "АПВ БЛОК. Инв.",
                    "ГОТ-ТЬ АПВ",
                    "ГОТ-ТЬ АПВ Инв.",
                    "КСиУППНавт",
                    "КСиУППНавт Инв.",
                    "U1-U2+",
                    "U1-U2+ Инв.",
                    "U1+U2-",
                    "U1+U2- Инв.",
                    "U1-U2-",
                    "U1-U2- Инв.",
                    "Условия ОС",
                    "Условия ОС Инв.",
                    "Условия УС",
                    "Условия УС Инв.",
                    "КСиУППНвкл",
                    "КСиУППНвкл Инв.",
                    "ПОВР. Ф. A",
                    "ПОВР. Ф. A Инв.",
                    "ПОВР. Ф. B",
                    "ПОВР. Ф. B Инв.",
                    "ПОВР. Ф. C",
                    "ПОВР. Ф. C Инв.",
                    "КАЧАНИЕ",
                    "КАЧАНИЕ Инв.",
                    "КАЧ.ВНЕШ",
                    "КАЧ.ВНЕШ Инв.",
                    "КАЧ.ВНУТР",
                    "КАЧ.ВНУТР Инв.",
                    "НеиспТНмгн ",
                    "НеиспТНмгн Инв.",
                    "НеиспТН с/п",
                    "НеиспТН с/п Инв.",
                    "ВХОД K1",
                    "ВХОД K1 Инв.",
                    "ВХОД K2",
                    "ВХОД K2 Инв.",
                    "УРОВ 1",
                    "УРОВ 1 Инв.",
                    "УРОВ 2",
                    "УРОВ 2 Инв.",
                    "БЛК. УРОВ",
                    "БЛК. УРОВ Инв.",
                    "АВР ВКЛ.",
                    "АВР ВКЛ. Инв.",
                    "АВР ОТКЛ.",
                    "АВР ОТКЛ. Инв.",
                    "АВР БЛК.",
                    "АВР БЛК. Инв.",
                    "ПУСК ДУГ.",
                    "ПУСК ДУГ. Инв.",
                    "АВРвывДис",
                    "АВРвывДис Инв.",
                    "АВРвведен",
                    "АВРвведен Инв.",
                    "АВРвывед.",
                    "АВРвывед. Инв.",
                    "АВР ГОТОВ",
                    "АВР ГОТОВ Инв.",
                    "АВРдеблок",
                    "АВРдеблок Инв.",
                    "РЕЗЕРВ 6",
                    "РЕЗЕРВ 6 Инв.",
                    "РЕЗЕРВ 7",
                    "РЕЗЕРВ 7 Инв.",
                    "РЕЗЕРВ 8",
                    "РЕЗЕРВ 8 Инв.",
                    "РЕЗЕРВ 9",
                    "РЕЗЕРВ 9 Инв.",
                    "РЕЗЕРВ 10",
                    "РЕЗЕРВ 10 Инв.",
                    "РЕЗЕРВ 11",
                    "РЕЗЕРВ 11 Инв.",
                    "РЕЗЕРВ 12",
                    "РЕЗЕРВ 12 Инв.",
                    "РЕЗЕРВ 13",
                    "РЕЗЕРВ 13 Инв.",
                    "РЕЗЕРВ 14",
                    "РЕЗЕРВ 14 Инв.",
                    "РЕЗЕРВ 15",
                    "РЕЗЕРВ 15 Инв.",
                    "РЕЗЕРВ 16",
                    "РЕЗЕРВ 16 Инв.",
                    "РЕЗЕРВ 17",
                    "РЕЗЕРВ 17 Инв.",
                    "РЕЗЕРВ 18",
                    "РЕЗЕРВ 18 Инв.",
                    "РЕЗЕРВ 19",
                    "РЕЗЕРВ 19 Инв.",
                    "РЕЗЕРВ 20",
                    "РЕЗЕРВ 20 Инв.",
                    "ВНЕШ. 1",
                    "ВНЕШ. 1 Инв.",
                    "ВНЕШ. 2",
                    "ВНЕШ. 2 Инв.",
                    "ВНЕШ. 3",
                    "ВНЕШ. 3 Инв.",
                    "ВНЕШ. 4",
                    "ВНЕШ. 4 Инв.",
                    "ВНЕШ. 5",
                    "ВНЕШ. 5 Инв.",
                    "ВНЕШ. 6",
                    "ВНЕШ. 6 Инв.",
                    "ВНЕШ. 7",
                    "ВНЕШ. 7 Инв.",
                    "ВНЕШ. 8",
                    "ВНЕШ. 8 Инв.",
                    "ВНЕШ. 9",
                    "ВНЕШ. 9 Инв.",
                    "ВНЕШ. 10",
                    "ВНЕШ. 10 Инв.",
                    "ВНЕШ. 11",
                    "ВНЕШ. 11 Инв.",
                    "ВНЕШ. 12",
                    "ВНЕШ. 12 Инв.",
                    "ВНЕШ. 13",
                    "ВНЕШ. 13 Инв.",
                    "ВНЕШ. 14",
                    "ВНЕШ. 14 Инв.",
                    "ВНЕШ. 15",
                    "ВНЕШ. 15 Инв.",
                    "ВНЕШ. 16",
                    "ВНЕШ. 16 Инв."
                };

                SortedDictionary<ushort, string> ret = new SortedDictionary<ushort, string>();
                for (ushort i = 0; i < list.Count; i++)
                {
                    ret.Add(i, list[i]);
                }
                
                switch (DeviceType)
                {
                    case MRUniversal.T4N4D42R35:
                    case MRUniversal.T4N5D42R35:
                        for (ushort i = 81; i < 84 + 93; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                    case MRUniversal.T4N4D74R67:
                    case MRUniversal.T4N5D74R67:
                        for (ushort i = 145; i < 145 + 32; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                    default:
                        //дискреты
                        for (ushort i = 33; i < 32 + 145; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                }

                switch (DeviceName)
                {
                    case "MR771":

                        //Двигатель
                        ushort deleteNInd = ret.First(v => v.Value == "БЛК. ПО N").Key;

                        for (ushort i = deleteNInd; i < deleteNInd + 4; i++)
                        {
                            ret.Remove(i);
                        }

                        //АВР
                        ushort deleteAvrInd = ret.First(v => v.Value == "АВР ВКЛ.").Key;

                        for (ushort i = deleteAvrInd; i < deleteAvrInd + 8; i++)
                        {
                            ret.Remove(i);
                        }

                        ushort changedNameSignalsInd = ret.First(v => v.Value == "АВРвывДис").Key;

                        ret[changedNameSignalsInd] = "ТС ДЗ";
                        ret[(ushort) (changedNameSignalsInd + 1)] = "ТС ДЗ Инв.";
                        ret[(ushort) (changedNameSignalsInd + 2)] = "ТС ТЗНП";
                        ret[(ushort) (changedNameSignalsInd + 3)] = "ТС ТЗНП Инв.";
                        ret[(ushort) (changedNameSignalsInd + 4)] = "ТО ДЗ";
                        ret[(ushort) (changedNameSignalsInd + 5)] = "ТО ДЗ Инв.";
                        ret[(ushort) (changedNameSignalsInd + 6)] = "ТО ТЗНП";
                        ret[(ushort) (changedNameSignalsInd + 7)] = "ТО ТЗНП Инв.";
                        ret[(ushort) (changedNameSignalsInd + 8)] = "РЕВ. ДЗ";
                        ret[(ushort) (changedNameSignalsInd + 9)] = "РЕВ. ДЗ Инв.";
                        ret[(ushort) (changedNameSignalsInd + 10)] = "РЕВ ТЗНП";
                        ret[(ushort) (changedNameSignalsInd + 11)] = "РЕВ ТЗНП Инв.";
                        ret[(ushort) (changedNameSignalsInd + 12)] = "КСП ДЗ";
                        ret[(ushort) (changedNameSignalsInd + 13)] = "КСП ДЗ Инв.";
                        ret[(ushort) (changedNameSignalsInd + 14)] = "КСП ТЗНП";
                        ret[(ushort) (changedNameSignalsInd + 15)] = "КСП ТЗНП Инв.";
                        ret[(ushort) (changedNameSignalsInd + 16)] = "Эхо ДЗ";
                        ret[(ushort) (changedNameSignalsInd + 17)] = "Эхо ДЗ Инв.";
                        ret[(ushort) (changedNameSignalsInd + 18)] = "Эхо ТЗНП";
                        ret[(ushort) (changedNameSignalsInd + 19)] = "Эхо ТЗНП Инв.";

                        //Ступени по Z
                        ushort deleteRezInd = ret.First(v => v.Value == "РЕЗЕРВ 1").Key;
                        ret[deleteRezInd] = "Z7 ИО";
                        ret[(ushort) (deleteRezInd + 1)] = "Z7 ИО Инв.";
                        ret[(ushort) (deleteRezInd + 2)] = "Z7";
                        ret[(ushort) (deleteRezInd + 3)] = "Z7 Инв.";
                        ret[(ushort) (deleteRezInd + 4)] = "Z8 ИО";
                        ret[(ushort) (deleteRezInd + 5)] = "Z8 ИО Инв.";
                        ret[(ushort) (deleteRezInd + 6)] = "Z8";
                        ret[(ushort) (deleteRezInd + 7)] = "Z8 Инв.";
                        ret[(ushort) (deleteRezInd + 8)] = "Z9 ИО";
                        ret[(ushort) (deleteRezInd + 9)] = "Z9 ИО Инв.";
                        ret[(ushort) (deleteRezInd + 10)] = "Z9";
                        ret[(ushort) (deleteRezInd + 11)] = "Z9 Инв.";
                        ret[(ushort) (deleteRezInd + 12)] = "Z10 ИО";
                        ret[(ushort) (deleteRezInd + 13)] = "Z10 ИО Инв.";
                        ret[(ushort) (deleteRezInd + 14)] = "Z10";
                        ret[(ushort) (deleteRezInd + 15)] = "Z10 Инв.";

                        ushort rez5Ind = ret.First(v => v.Value == "РЕЗЕРВ 5").Key;
                        ret.Remove(rez5Ind);
                        ret.Remove((ushort) (rez5Ind + 1));

                        ushort rez11Ind = ret.First(v => v.Value == "РЕЗЕРВ 11").Key;

                        for (ushort i = rez11Ind; i < rez11Ind + 20; i++)
                        {
                            ret.Remove(i);
                        }

                        break;
                    default:
                        break;
                }

                return ret.ToDictionary(r => r.Key, r => r.Value);
            }
        }

        public static List<string> MTZJoin
        {
            get
            {
                List<string> ret = new List<string>
                {
                    "Прис. 1",
                    "Прис. 2",
                    "Прис. 3",
                    "Прис. 4",
                    "Прис. 5",
                    "Прис. 6",
                    "Прис. 7",
                    "Прис. 8",
                    "Прис. 9",
                    "Прис. 10",
                    "Прис. 11",
                    "Прис. 12",
                    "Прис. 13",
                    "Прис. 14",
                    "Прис. 15",
                    "Прис. 16",
                    "Прис. 17",
                    "Прис. 18",
                    "Прис. 19",
                    "Прис. 20",
                    "Прис. 21",
                    "Прис. 22",
                    "Прис. 23",
                    "Прис. 24"
                };
                return ret;
            }
        }
        /// <summary>
        ///Сообщения (Журнал аварий)
        /// </summary>
        public static List<string> AlarmJournalMessage
        {
            get
            {
                return new List<string>
                {
                    "АВАРИЯ",
                    "СИГНАЛ-ЦИЯ",
                    "РАБОТА",
                    "ОТКЛЮЧЕНИЕ",
                    "УРОВ 1",
                    "УРОВ 2",
                    "УРОВ 3",
                    "УРОВ",
                    "АВАРИЯ",
                    "ЛОГИКА",
                    "СООБЩЕНИЕ"
                };
            }
        }
        public static List<string> Otkl
        {
            get
            {
                List<string> ret = new List<string>
                {
                    "СШ1",
                    "СШ2",
                    "ПО",
                    "Прис. 1",
                    "Прис. 2",
                    "Прис. 3",
                    "Прис. 4",
                    "Прис. 5",
                    "Прис. 6",
                    "Прис. 7",
                    "Прис. 8",
                    "Прис. 9",
                    "Прис. 10",
                    "Прис. 11",
                    "Прис. 12",
                    "Прис. 13",
                    "Прис. 14",
                    "Прис. 15",
                    "Прис. 16",
                    "Прис. 17",
                    "Прис. 18",
                    "Прис. 19",
                    "Прис. 20",
                    "Прис. 21",
                    "Прис. 22",
                    "Прис. 23",
                    "Прис. 24"
                };
                
                return ret;
            }
        }

        public static List<string> GroupsNames
        {
            get
            {
                return new List<string>
                {
                    "Группа 1",
                    "Группа 2",
                    "Группа 3",
                    "Группа 4",
                    "Группа 5",
                    "Группа 6"
                };
            }
        }

        public static List<string> CopyGroupsNames
        {
            get
            {
                return new List<string>
                {
                    "Группа 1",
                    "Группа 2",
                    "Группа 3",
                    "Группа 4",
                    "Группа 5",
                    "Группа 6",
                    "Все"
                };
            }
        }

        /// <summary>
        /// Коэффициенты Kthl(x)
        /// </summary>
        public static List<string> KthKoefs
        {
            get
            {
                return new List<string>
                {
                    "1",
                    "1000"
                };
            }
        }
       
        public static List<string> Join
        {
            get
            {
                return new List<string>
                    {
                        "НЕТ",
                        "СШ1",
                        "СШ2",
                        "СВ+СШ1",
                        "СВ+СШ2",
                        "СВ1",
                        "СВ2",
                        "От входа"
                    };
            }
        }
         
        public static List<string> MtzNames
        {
            get
            {
                var res = new List<string>();
                for (int i = 0; i < 32; i++)
                {
                    res.Add("I> " + (i + 1));
                }

                return res;
            }
        }

        public static List<string> TtNames
        {
            get
            {
                return new List<string>
                    {
                        "СШ1",
                        "СШ2",
                        "ПО"
                    };
            }
        }
        
        public static List<string> OscBases
        {
            get
            {
                List<string> ret = new List<string>
                {
                    "Б1", "Б2", "Б3", "Б4"
                };
                return ret;
            }
        }

        /// <summary>
        /// Названия индикаторов
        /// </summary>
        public static List<string> IndNames
        {
            get
            {
                List<string> indNames = new List<string>();
                for (int i = 0; i < AllIndicatorsStruct.INDICATORS_COUNT; i++)
                {
                    if (i > 11)
                    {
                        indNames.Add("В" + (i + 1).ToString(CultureInfo.InvariantCulture));
                        continue;
                    }
                    indNames.Add((i + 1).ToString(CultureInfo.InvariantCulture));
                }
                return indNames;
            }
        }

        /// <summary>
        /// Названия RS-Триггеров
        /// </summary>
        public static List<string> RsTriggersName
        {
            get
            {
                List<string> indNames = new List<string>();
                for (int i = 0; i < AllRsTriggersStruct.RSTRIGGERS_COUNT_CONST; i++)
                {
                    indNames.Add((i + 1).ToString(CultureInfo.InvariantCulture));
                }
                return indNames;
            }
        }

        /// <summary>
        /// Названия реле
        /// </summary>
        public static List<string> RelayNames
        {
            get
            {
                List<string> relayNames = new List<string>();
                for (int i = 0; i < AllReleOutputStruct.RELAY_COUNT_CONST; i++)
                {
                    relayNames.Add((i + 3).ToString(CultureInfo.InvariantCulture));
                }
                return relayNames;
            }
        }

        /// <summary>
        /// Названия каналов
        /// </summary>
        public static List<string> OscChannelNames
        {
            get
            {
                List<string> channelNames = new List<string>();
                for (int i = 0; i < OscopeAllChannelsStruct.KANAL_COUNT; i++)
                {
                    channelNames.Add((i + 1).ToString(CultureInfo.InvariantCulture));
                }
                return channelNames;
            }
        }

        public static List<Dictionary<ushort,string>> OscChannelSignals
        {
            get
            {
                List<Dictionary<ushort, string>> ret = new List<Dictionary<ushort, string>>
                {
                    RelaySignals,
                    Faults,
                    Parameters,
                    GooseSignals
                };
                return ret;
            }
        }

        /// <summary>
        /// Названия RS-Триггеров
        /// </summary>
        //public static List<string> RsTriggersName
        //{
        //    get
        //    {
        //        List<string> indNames = new List<string>();
        //        for (int i = 0; i < AllRsTriggersStruct.RSTRIGGERS_COUNT_CONST; i++)
        //        {
        //            indNames.Add((i + 1).ToString(CultureInfo.InvariantCulture));
        //        }
        //        return indNames;
        //    }
        //}

        public static List<string> GooseNames
        {
            get
            {
                return new List<string>
                {
                    "БГС1",
                    "БГС2",
                    "БГС3",
                    "БГС4",
                    "БГС5",
                    "БГС6",
                    "БГС7",
                    "БГС8",
                    "БГС9",
                    "БГС10",
                    "БГС11",
                    "БГС12",
                    "БГС13",
                    "БГС14",
                    "БГС15",
                    "БГС16"
                };
            }
        }

        public static List<string> GooseConfig
        {
            get
            {
                return new List<string>
                {
                    "И",
                    "ИЛИ"
                };
            }
        }

        public static List<string> GooseSignal
        {
            get
            {
                return new List<string>
                {
                    "Выведено",
                    "Сигнал",
                    "V",
                    "Сигнал*V"
                };
            }
        }

        public static Dictionary<ushort, string> GooseSignals
        {
            get
            {
                string[] strings =
                {
                    "НЕТ",
                    "GoIn1",
                    "GoIn1 Инв.",
                    "GoIn2",
                    "GoIn2 Инв.",
                    "GoIn3",
                    "GoIn3 Инв.",
                    "GoIn4",
                    "GoIn4 Инв.",
                    "GoIn5",
                    "GoIn5 Инв.",
                    "GoIn6",
                    "GoIn6 Инв.",
                    "GoIn7",
                    "GoIn7 Инв.",
                    "GoIn8",
                    "GoIn8 Инв.",
                    "GoIn9",
                    "GoIn9 Инв.",
                    "GoIn10",
                    "GoIn10 Инв.",
                    "GoIn11",
                    "GoIn11 Инв.",
                    "GoIn12",
                    "GoIn12 Инв.",
                    "GoIn13",
                    "GoIn13 Инв.",
                    "GoIn14",
                    "GoIn14 Инв.",
                    "GoIn15",
                    "GoIn15 Инв.",
                    "GoIn16",
                    "GoIn16 Инв.",
                    "GoIn17",
                    "GoIn17 Инв.",
                    "GoIn18",
                    "GoIn18 Инв.",
                    "GoIn19",
                    "GoIn19 Инв.",
                    "GoIn20",
                    "GoIn20 Инв.",
                    "GoIn21",
                    "GoIn21 Инв.",
                    "GoIn22",
                    "GoIn22 Инв.",
                    "GoIn23",
                    "GoIn23 Инв.",
                    "GoIn24",
                    "GoIn24 Инв.",
                    "GoIn25",
                    "GoIn25 Инв.",
                    "GoIn26",
                    "GoIn26 Инв.",
                    "GoIn27",
                    "GoIn27 Инв.",
                    "GoIn28",
                    "GoIn28 Инв.",
                    "GoIn29",
                    "GoIn29 Инв.",
                    "GoIn30",
                    "GoIn30 Инв.",
                    "GoIn31",
                    "GoIn31 Инв.",
                    "GoIn32",
                    "GoIn32 Инв.",
                    "GoIn33",
                    "GoIn33 Инв.",
                    "GoIn34",
                    "GoIn34 Инв.",
                    "GoIn35",
                    "GoIn35 Инв.",
                    "GoIn36",
                    "GoIn36 Инв.",
                    "GoIn37",
                    "GoIn37 Инв.",
                    "GoIn38",
                    "GoIn38 Инв.",
                    "GoIn39",
                    "GoIn839 Инв.",
                    "GoIn40",
                    "GoIn40 Инв.",
                    "GoIn41",
                    "GoIn41 Инв.",
                    "GoIn42",
                    "GoIn42 Инв.",
                    "GoIn43",
                    "GoIn43 Инв.",
                    "GoIn44",
                    "GoIn44 Инв.",
                    "GoIn45",
                    "GoIn45 Инв.",
                    "GoIn46",
                    "GoIn46 Инв.",
                    "GoIn47",
                    "GoIn47 Инв.",
                    "GoIn48",
                    "GoIn48 Инв.",
                    "GoIn49",
                    "GoIn49 Инв.",
                    "GoIn50",
                    "GoIn50 Инв.",
                    "GoIn51",
                    "GoIn51 Инв.",
                    "GoIn52",
                    "GoIn52 Инв.",
                    "GoIn53",
                    "GoIn53 Инв.",
                    "GoIn54",
                    "GoIn54 Инв.",
                    "GoIn55",
                    "GoIn55 Инв.",
                    "GoIn56",
                    "GoIn56 Инв.",
                    "GoIn57",
                    "GoIn57 Инв.",
                    "GoIn58",
                    "GoIn58 Инв.",
                    "GoIn59",
                    "GoIn59 Инв.",
                    "GoIn60",
                    "GoIn60 Инв.",
                    "GoIn61",
                    "GoIn61 Инв.",
                    "GoIn62",
                    "GoIn62 Инв.",
                    "GoIn63",
                    "GoIn63 Инв.",
                    "GoIn64",
                    "GoIn64 Инв.",

                    "GoIn1 valid",
                    "GoIn1 valid Инв.",
                    "GoIn2 valid",
                    "GoIn2 valid Инв.",
                    "GoIn3 valid",
                    "GoIn3 valid Инв.",
                    "GoIn4 valid",
                    "GoIn4 valid Инв.",
                    "GoIn5 valid",
                    "GoIn5 valid Инв.",
                    "GoIn6 valid",
                    "GoIn6 valid Инв.",
                    "GoIn7 valid",
                    "GoIn7 valid Инв.",
                    "GoIn8 valid",
                    "GoIn8 valid Инв.",
                    "GoIn9 valid",
                    "GoIn9 valid Инв.",
                    "GoIn10 valid",
                    "GoIn10 valid Инв.",
                    "GoIn11 valid",
                    "GoIn11 valid Инв.",
                    "GoIn12 valid",
                    "GoIn12 valid Инв.",
                    "GoIn13 valid",
                    "GoIn13 valid Инв.",
                    "GoIn14 valid",
                    "GoIn14 valid Инв.",
                    "GoIn15 valid",
                    "GoIn15 valid Инв.",
                    "GoIn16 valid",
                    "GoIn16 valid Инв.",
                    "GoIn17 valid",
                    "GoIn17 valid Инв.",
                    "GoIn18 valid",
                    "GoIn18 valid Инв.",
                    "GoIn19 valid",
                    "GoIn19 valid Инв.",
                    "GoIn20 valid",
                    "GoIn20 valid Инв.",
                    "GoIn21 valid",
                    "GoIn21 valid Инв.",
                    "GoIn22 valid",
                    "GoIn22 valid Инв.",
                    "GoIn23 valid",
                    "GoIn23 valid Инв.",
                    "GoIn24 valid",
                    "GoIn24 valid Инв.",
                    "GoIn25 valid",
                    "GoIn25 valid Инв.",
                    "GoIn26 valid",
                    "GoIn26 valid Инв.",
                    "GoIn27 valid",
                    "GoIn27 valid Инв.",
                    "GoIn28 valid",
                    "GoIn28 valid Инв.",
                    "GoIn29 valid",
                    "GoIn29 valid Инв.",
                    "GoIn30 valid",
                    "GoIn30 valid Инв.",
                    "GoIn31 valid",
                    "GoIn31 valid Инв.",
                    "GoIn32 valid",
                    "GoIn32 valid Инв.",
                    "GoIn33 valid",
                    "GoIn33 valid Инв.",
                    "GoIn34 valid",
                    "GoIn34 valid Инв.",
                    "GoIn35 valid",
                    "GoIn35 valid Инв.",
                    "GoIn36 valid",
                    "GoIn36 valid Инв.",
                    "GoIn37 valid",
                    "GoIn37 valid Инв.",
                    "GoIn38 valid",
                    "GoIn38 valid Инв.",
                    "GoIn39 valid",
                    "GoIn39 valid Инв.",
                    "GoIn40 valid",
                    "GoIn40 valid Инв.",
                    "GoIn41 valid",
                    "GoIn41 valid Инв.",
                    "GoIn42 valid",
                    "GoIn42 valid Инв.",
                    "GoIn43 valid",
                    "GoIn43 valid Инв.",
                    "GoIn44 valid",
                    "GoIn44 valid Инв.",
                    "GoIn45 valid",
                    "GoIn45 valid Инв.",
                    "GoIn46 valid",
                    "GoIn46 valid Инв.",
                    "GoIn47 valid",
                    "GoIn47 valid Инв.",
                    "GoIn48 valid",
                    "GoIn48 valid Инв.",
                    "GoIn49 valid",
                    "GoIn49 valid Инв.",
                    "GoIn50 valid",
                    "GoIn50 valid Инв.",
                    "GoIn51 valid",
                    "GoIn51 valid Инв.",
                    "GoIn52 valid",
                    "GoIn52 valid Инв.",
                    "GoIn53 valid",
                    "GoIn53 valid Инв.",
                    "GoIn54 valid",
                    "GoIn54 valid Инв.",
                    "GoIn55 valid",
                    "GoIn55 valid Инв.",
                    "GoIn56 valid",
                    "GoIn56 valid Инв.",
                    "GoIn57 valid",
                    "GoIn57 valid Инв.",
                    "GoIn58 valid",
                    "GoIn58 valid Инв.",
                    "GoIn59 valid",
                    "GoIn59 valid Инв.",
                    "GoIn60 valid",
                    "GoIn60 valid Инв.",
                    "GoIn61 valid",
                    "GoIn61 valid Инв.",
                    "GoIn62 valid",
                    "GoIn62 valid Инв.",
                    "GoIn63 valid",
                    "GoIn63 valid Инв.",
                    "GoIn64 valid",
                    "GoIn64 valid Инв."
                };

                Dictionary<ushort, string> ret = new Dictionary<ushort, string>();
                for (ushort i = 0; i < strings.Length; i++)
                {
                    ret.Add(i, strings[i]);
                }

                return ret;
            }
        }

        /// <summary>
        /// Ступень (Журнал аварий)
        /// </summary>
        public static List<string> AlarmJournalStage
        {
            get
            {
                List<string> ret = new List<string>
                {
                    "I> 1",
                    "I> 2",
                    "I> 3",
                    "I> 4",
                    "I> 5",
                    "I> 6",
                    "I< ",
                    "I*> 1",
                    "I*> 2",
                    "I*> 3",
                    "I*> 4",
                    "I*> 5",
                    "I*> 6",
                    "I*> 7",
                    "I*> 8",
                    "I2/I1",
                    "U> 1",
                    "U> 2",
                    "U> 3",
                    "U> 4",
                    "U< 1",
                    "U< 2",
                    "U< 3",
                    "U< 4",
                    "F> 1",
                    "F> 2",
                    "F> 3",
                    "F> 4",
                    "F< 1",
                    "F< 2",
                    "F< 3",
                    "F< 4",
                    "Z 1",
                    "Z 2",
                    "Z 3",
                    "Z 4",
                    "Z 5",
                    "Z 6",
                    "Z 7",
                    "P 1",
                    "P 2",
                    "Z 10",
                    "Q>",
                    "Q>>",
                    "ВНЕШ. 1",
                    "ВНЕШ. 2",
                    "ВНЕШ. 3",
                    "ВНЕШ. 4",
                    "ВНЕШ. 5",
                    "ВНЕШ. 6",
                    "ВНЕШ. 7",
                    "ВНЕШ. 8",
                    "ВНЕШ. 9",
                    "ВНЕШ. 10",
                    "ВНЕШ. 11",
                    "ВНЕШ. 12",
                    "ВНЕШ. 13",
                    "ВНЕШ. 14",
                    "ВНЕШ. 15",
                    "ВНЕШ. 16",
                    "ОМП",
                    "ЖА СПЛ",
                    "БЛОК. ПО ТЕПЛ. МОДЕЛИ",            //62
                    "",                                 //63
                    "ПУСК ОСЦ-ФА ОТ ДИСКР. СИГНАЛА",    //64
                    "ПУСК ОСЦ-ФА ИЗ МЕНЮ",              //65
                    "ПУСК ОСЦ-ФА ПО ИНТЕРФЕЙСУ",        //66
                    "ПУСК ДУГОВОЙ ЗАЩИТЫ",               //67
                    "P 1",                              //68 
                    "P 2"                              //69 
                };
                
                return ret;
            }
        }
        
        /// <summary>
        /// Сигналы неисправностей
        /// </summary>
        public static Dictionary<ushort, string> Faults
        {
            get
            {
                List<string> list = new List<string>
                {
                    "НЕТ",
                    "Аппаратная неиспр.",
                    "Аппаратная неиспр. Инв.",
                    "Программн. неиспр.",
                    "Программн. неиспр. Инв.",
                    "Неисправность измерений U",
                    "Неисправность измерений U Инв.",
                    "Неисправность измерений F",
                    "Неисправность измерений F Инв.",
                    "Неисправность выключателя",
                    "Неисправность выключателя Инв.",
                    "Неисправность логики",
                    "Неисправность логики Инв.",
                    "ВЧС",
                    "ВЧС Инв.",
                    "Неисправность мод.1",
                    "Неисправность мод.1 Инв.",
                    "Неисправность мод.2",
                    "Неисправность мод.2 Инв",
                    "Неисправность мод.3",
                    "Неисправность мод.3 Инв.",
                    "Неисправность мод.4",
                    "Неисправность мод.4 Инв.",
                    "Неисправность мод.5",
                    "Неисправность мод.5 Инв.",
                    "Неисправность мод.6",
                    "Неисправность мод.6 Инв.",
                    "Неисправность уставок",
                    "Неисправность уставок Инв.",
                    "Неисправность группы уст.",
                    "Неисправность группы уст. Инв.",
                    "Неисправность пароля",
                    "Неисправность пароля Инв.",
                    "Неисправность ЖС",
                    "Неисправность ЖС Инв.",
                    "Неисправность ЖА",
                    "Неисправность ЖА Инв.",
                    "Неисправность осциллографа",
                    "Неисправность осциллографа Инв.",
                    "Внешняя неиспр. выкл-ля",
                    "Внешняя неиспр. выкл-ля Инв.",
                    "Неиспр. выкл-ля по блок-конт.",
                    "Неиспр. выкл-ля по блок-конт. Инв.",
                    "Неиспр. управл. выкл-лем",
                    "Неиспр. управл. выкл-лем Инв.",
                    "Отказ выключателя",
                    "Отказ выключателя Инв.",
                    "Неиспр. цепи включения",
                    "Неиспр. цепи включения Инв.",
                    "Неиспр. цепи отключения 1",
                    "Неиспр. цепи отключения 1 Инв.",
                    "Неиспр. цепи отключения 2",
                    "Неиспр. цепи отключения 2 Инв.",
                    "Цикл",
                    "Цикл Инв.",
                    "Резерв 1",
                    "Резерв 1 Инв.",
                    "ТН: 3U0",
                    "ТН: 3U0 Инв.",
                    "ТН: U2",
                    "ТН: U2 Инв.",
                    "ТН: обрыв",
                    "ТН: обрыв Инв.",
                    "Внеш. Uabc",
                    "Внеш. Uabc Инв.",
                    "Uabc < 5B",
                    "Uabc < 5B Инв.",
                    "TH",
                    "TH Инв.",
                    "Внеш. Un",
                    "Внеш. Un Инв.",
                    "Резерв 2",
                    "Резерв 2 Инв.",
                    "Uabc < 10B",
                    "Uabc < 10B Инв.",
                    "F > 60Гц",
                    "F > 60Гц Инв.",
                    "F < 40Гц",
                    "F < 40Гц Инв.",
                    "Ошибка расчета частоты",
                    "Ошибка расчета частоты Инв.",
                    "Ошибка логики: CRC константы",
                    "Ошибка логики: CRC константы Инв.",
                    "Ошибка логики: CRC разр.",
                    "Ошибка логики: CRC разр. Инв.",
                    "Ошибка логики: CRC программы",
                    "Ошибка логики: CRC программы Инв.",
                    "Ошибка логики: CRC меню",
                    "Ошибка логики: CRC меню Инв.",
                    "Ошибка логики: выполнение",
                    "Ошибка логики: выполнение Инв.",
                    "Эмуляция1",
                    "Эмуляция1 Инв.",
                    "Резерв 3",
                    "Резерв 3 Инв.",
                    "Резерв 4",
                    "Резерв 4 Инв.",
                    "ВЧС 1",
                    "ВЧС 1 Инв.",
                    "ВЧС 0",
                    "ВЧС 0 Инв.",
                    "Период",
                    "Период Инв.",
                };

                SortedDictionary<ushort, string> ret = new SortedDictionary<ushort, string>();
                for (ushort i = 0; i < list.Count; i++)
                {
                    ret.Add(i, list[i]);
                }

                ushort reserv1 = ret.First(r => r.Value == "Резерв 1").Key;
                ushort reserv2 = ret.First(r => r.Value == "Резерв 2").Key;
                ushort reserv3 = ret.First(r => r.Value == "Резерв 3").Key;
                ushort reserv4 = ret.First(r => r.Value == "Резерв 4").Key;

                switch (DeviceType)
                {
                    case "T4N5D42R35":
                    case "T4N5D74R67":
                        
                        ret[reserv1] = "Внешняя неисправность Un1";
                        ret[(ushort) (reserv1 + 1)] = "Внешняя неисправность Un1 Инв.";

                        for (ushort i = reserv2; i < reserv2 + 2; i++)
                        {
                            ret.Remove(i);
                        }

                        for (ushort i = reserv3; i < reserv3 + 2; i++)
                        {
                            ret.Remove(i);
                        }

                        for (ushort i = reserv4; i < reserv4 + 2; i++)
                        {
                            ret.Remove(i);
                        }

                        break;
                    case "T4N4D42R35":
                    case "T4N4D74R67":

                        for (ushort i = reserv1; i < reserv1 + 2; i++)
                        {
                            ret.Remove(i);
                        }

                        for (ushort i = reserv2; i < reserv2 + 2; i++)
                        {
                            ret.Remove(i);
                        }

                        for (ushort i = reserv3; i < reserv3 + 2; i++)
                        {
                            ret.Remove(i);
                        }

                        for (ushort i = reserv4; i < reserv4 + 2; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                }

                return ret.ToDictionary(r=>r.Key, r=>r.Value);
            }
        }

        /// <summary>
        /// Сигналы параметров
        /// </summary>
        public static Dictionary<ushort, string> Parameters
        {
            get
            {
                List<string> list = new List<string>
                {
                    "НЕТ",
                    "Направл. А",
                    "Направл. А Инв.",
                    "Ошибка А",
                    "Ошибка А Инв.",
                    "Направл. B",
                    "Направл. B Инв.",
                    "Ошибка B",
                    "Ошибка B Инв.",
                    "Направл. C",
                    "Направл. C Инв.",
                    "Ошибка C",
                    "Ошибка C Инв.",
                    "Направл. 0",
                    "Направл. 0 Инв.",
                    "Ошибка 0",
                    "Ошибка 0 Инв.",
                    "Направл. 2",
                    "Направл. 2 Инв.",
                    "Ошибка 2",
                    "Ошибка 2 Инв.",
                    "Направл. N",
                    "Направл. N Инв.",
                    "Ошибка N",
                    "Ошибка N Инв.",
                    "Направл. ZA",
                    "Направл. ZA Инв.",
                    "Ошибка ZA",
                    "Ошибка ZA Инв.",
                    "Направл. ZB",
                    "Направл. ZB Инв.",
                    "Ошибка ZB",
                    "Ошибка ZB Инв.",
                    "Направл. ZC",
                    "Направл. ZC Инв.",
                    "Ошибка ZC",
                    "Ошибка ZC Инв.",
                    "Направл. ZAB",
                    "Направл. ZAB Инв.",
                    "Ошибка ZAB",
                    "Ошибка ZAB Инв.",
                    "Направл. ZBC",
                    "Направл. ZBC Инв.",
                    "Ошибка ZBC",
                    "Ошибка ZBC Инв.",
                    "Направл. ZCA",
                    "Направл. ZCA Инв.",
                    "Ошибка ZCA",
                    "Ошибка ZCA Инв.",
                    "Направл. ZA1",
                    "Направл. ZA1 Инв.",
                    "Ошибка ZA1",
                    "Ошибка ZA1 Инв.",
                    "Направл. ZB1",
                    "Направл. ZB1 Инв.",
                    "Ошибка ZB1",
                    "Ошибка ZB1 Инв.",
                    "Направл. ZC1",
                    "Направл. ZC1 Инв.",
                    "Ошибка ZC1",
                    "Ошибка ZC1 Инв.",
                    "Направл. ZA2",
                    "Направл. ZA2 Инв.",
                    "Ошибка ZA2",
                    "Ошибка ZA2 Инв.",
                    "Направл. ZB2",
                    "Направл. ZB2 Инв.",
                    "Ошибка ZB2",
                    "Ошибка ZB2 Инв.",
                    "Направл. ZC2",
                    "Направл. ZC2 Инв.",
                    "Ошибка ZC2",
                    "Ошибка ZC2 Инв.",
                    "Направл. ZA3",
                    "Направл. ZA3 Инв.",
                    "Ошибка ZA3",
                    "Ошибка ZA3 Инв.",
                    "Направл. ZB3",
                    "Направл. ZB3 Инв.",
                    "Ошибка ZB3",
                    "Ошибка ZB3 Инв.",
                    "Направл. ZC3",
                    "Направл. ZC3 Инв.",
                    "Ошибка ZC3",
                    "Ошибка ZC3 Инв.",
                    "Направл. ZA2",
                    "Направл. ZA2 Инв.",
                    "Ошибка ZA4",
                    "Ошибка ZA4 Инв.",
                    "Направл. ZB4",
                    "Направл. ZB4 Инв.",
                    "Ошибка ZB4",
                    "Ошибка ZB4 Инв.",
                    "Направл. ZC4",
                    "Направл. ZC4 Инв.",
                    "Ошибка ZC4",
                    "Ошибка ZC4 Инв.",
                    "Направл. ZA5",
                    "Направл. ZA5 Инв.",
                    "Ошибка ZA5",
                    "Ошибка ZA5 Инв.",
                    "Направл. ZB5",
                    "Направл. ZB5 Инв.",
                    "Ошибка ZB5",
                    "Ошибка ZB5 Инв.",
                    "Направл. ZC5",
                    "Направл. ZC5 Инв.",
                    "Ошибка ZC5",
                    "Ошибка ZC5 Инв.",
                };
                
                SortedDictionary<ushort, string> ret = new SortedDictionary<ushort, string>();
                for (ushort i = 0; i < list.Count; i++)
                {
                    ret.Add(i, list[i]);
                }
                
                return ret.ToDictionary(r => r.Key, r => r.Value);

            }
        }
        
        public static List<string> OscFix
        {
            get
            {
                return new List<string>
                    {
                        "Первой",
                        "Посл."
                    };
            }
        }


        #region New Signals

        public static Dictionary<int, string> VlsSignals { get; set; }
        public static Dictionary<int, string> VlsSignalsConfig(Dictionary<int, string> list)
        {
            switch (DeviceType)
            {
                case MRUniversal.T4N4D42R35:
                case MRUniversal.T4N5D42R35:
                    for (int i = 40; i < 42 + 46; i++)
                    {
                        list.Remove(i);
                    }
                    break;
                case MRUniversal.T4N4D74R67:
                case MRUniversal.T4N5D74R67:
                    for (int i = 72; i < 72 + 16; i++)
                    {
                        list.Remove(i);
                    }
                    break;
                default:
                    //дискреты
                    for (int i = 16; i < 16 + 72; i++)
                    {
                        list.Remove(i);
                    }
                    break;
            }

            switch (DeviceName)
            {
                case "MR771":

                    //Двигатель
                    int deleteNInd = list.First(v => v.Value == "БЛК.ПО N").Key;

                    for (int i = deleteNInd; i < deleteNInd + 2; i++)
                    {
                        list.Remove(i);
                    }

                    //АВР
                    int deleteAvrInd = list.First(v => v.Value == "АВР ВКЛ.").Key;

                    for (int i = deleteAvrInd; i < deleteAvrInd + 4; i++)
                    {
                        list.Remove(i);
                    }

                    int changedNameSignalsInd = list.First(v => v.Value == "АВРвывДис").Key;

                    list[changedNameSignalsInd] = "ВЧС 1";
                    list[changedNameSignalsInd + 1] = "ВЧС 0";
                    list[changedNameSignalsInd + 2] = "ВЧС ОТКЛ.1";
                    list[changedNameSignalsInd + 3] = "ВЧС ОТКЛ.0";
                    list[changedNameSignalsInd + 4] = "РЕВЕРС? 1";
                    list[changedNameSignalsInd + 5] = "РЕВЕРС? 0";
                    list[changedNameSignalsInd + 6] = "РЕВЕРС 1";
                    list[changedNameSignalsInd + 7] = "РЕВЕРС 0";
                    list[changedNameSignalsInd + 8] = "Эхо 1";
                    list[changedNameSignalsInd + 9] = "Эхо 0";

                    //Ступени по Z
                    int deleteRezInd = list.First(v => v.Value == "РЕЗЕРВ 1").Key;
                    list[deleteRezInd] = "Z7 ИО";
                    list[deleteRezInd + 1] = "Z7";
                    list[deleteRezInd + 2] = "Z8 ИО";
                    list[deleteRezInd + 3] = "Z8";
                    list[deleteRezInd + 4] = "Z9 ИО";
                    list[deleteRezInd + 5] = "Z9";
                    list[deleteRezInd + 6] = "Z10 ИО";
                    list[deleteRezInd + 7] = "Z10";

                    int rez5Ind = list.First(v => v.Value == "РЕЗЕРВ 5").Key;
                    list.Remove(rez5Ind);

                    int rez11Ind = list.First(v => v.Value == "РЕЗЕРВ 11").Key;

                    for (int i = rez11Ind; i < rez11Ind + 10; i++)
                    {
                        list.Remove(i);
                    }

                    break;
                default:

                    //int rez5Ind761 = list.First(v => v.Value == "РЕЗЕРВ 5").Key;
                    //list.Remove(rez5Ind761);

                    //int rez6Ind761 = list.First(v => v.Value == "РЕЗЕРВ 6").Key;

                    //for (int i = rez6Ind761; i < rez6Ind761 + 15; i++)
                    //{
                    //    list.Remove(i);
                    //}

                    break;
            }

            return list.ToDictionary(r => r.Key, r => r.Value);
        }
        /// <summary>
        /// Сигналы Реле и Индикаторы
        /// </summary>
        public static Dictionary<int, string> DefaultVLSSignals
        {
            get
            {
                List<string> list = new List<string>
                {
                    "Д1",
                    "Д2",
                    "Д3",
                    "Д4",
                    "Д5",
                    "Д6",
                    "Д7",
                    "Д8",
                    "Д9",
                    "Д10",
                    "Д11",
                    "Д12",
                    "Д13",
                    "Д14",
                    "Д15",
                    "Д16",
                    "Д17",
                    "Д18",
                    "Д19",
                    "Д20",
                    "Д21",
                    "Д22",
                    "Д23",
                    "Д24",
                    "Д25",
                    "Д26",
                    "Д27",
                    "Д28",
                    "Д29",
                    "Д30",
                    "Д31",
                    "Д32",
                    "Д33",
                    "Д34",
                    "Д35",
                    "Д36",
                    "Д37",
                    "Д38",
                    "Д39",
                    "Д40",
                    "Д41",
                    "Д42",
                    "Д43",
                    "Д44",
                    "Д45",
                    "Д46",
                    "Д47",
                    "Д48",
                    "Д49",
                    "Д50",
                    "Д51",
                    "Д52",
                    "Д53",
                    "Д54",
                    "Д55",
                    "Д56",
                    "Д57",
                    "Д58",
                    "Д59",
                    "Д60",
                    "Д61",
                    "Д62",
                    "Д63",
                    "Д64",
                    "Д65",
                    "Д66",
                    "Д67",
                    "Д68",
                    "Д69",
                    "Д70",
                    "Д71",
                    "Д72",
                    "Д73",
                    "Д74",
                    "Д75",
                    "Д76",
                    "Д77",
                    "Д78",
                    "Д79",
                    "Д80",
                    "Д81",
                    "Д82",
                    "Д83",
                    "Д84",
                    "Д85",
                    "Д86",
                    "Д87",
                    "Д88",
                    "КМД 1",
                    "КМД 2",
                    "КМД 3",
                    "КМД 4",
                    "КМД 5",
                    "КМД 6",
                    "КМД 7",
                    "КМД 8",
                    "КМД 9",
                    "КМД 10",
                    "КМД 11",
                    "КМД 12",
                    "КМД 13",
                    "КМД 14",
                    "КМД 15",
                    "КМД 16",
                    "КМД 17",
                    "КМД 18",
                    "КМД 19",
                    "КМД 20",
                    "КМД 21",
                    "КМД 22",
                    "КМД 23",
                    "КМД 24",
                    "RST1",
                    "RST2",
                    "RST3",
                    "RST4",
                    "RST5",
                    "RST6",
                    "RST7",
                    "RST8",
                    "RST9",
                    "RST10",
                    "RST11",
                    "RST12",
                    "RST13",
                    "RST14",
                    "RST15",
                    "RST16",
                    
                    "ЛС1",
                    "ЛС2",
                    "ЛС3",
                    "ЛС4",
                    "ЛС5",
                    "ЛС6",
                    "ЛС7",
                    "ЛС8",
                    "ЛС9",
                    "ЛС10",
                    "ЛС11",
                    "ЛС12",
                    "ЛС13",
                    "ЛС14",
                    "ЛС15",
                    "ЛС16",

                    "БГС1",
                    "БГС2",
                    "БГС3",
                    "БГС4",
                    "БГС5",
                    "БГС6",
                    "БГС7",
                    "БГС8",
                    "БГС9",
                    "БГС10",
                    "БГС11",
                    "БГС12",
                    "БГС13",
                    "БГС14",
                    "БГС15",
                    "БГС16",

                    "ССЛ1",
                    "ССЛ2",
                    "ССЛ3",
                    "ССЛ4",
                    "ССЛ5",
                    "ССЛ6",
                    "ССЛ7",
                    "ССЛ8",
                    "ССЛ9",
                    "ССЛ10",
                    "ССЛ11",
                    "ССЛ12",
                    "ССЛ13",
                    "ССЛ14",
                    "ССЛ15",
                    "ССЛ16",
                    "ССЛ17",
                    "ССЛ18",
                    "ССЛ19",
                    "ССЛ20",
                    "ССЛ21",
                    "ССЛ22",
                    "ССЛ23",
                    "ССЛ24",
                    "ССЛ25",
                    "ССЛ26",
                    "ССЛ27",
                    "ССЛ28",
                    "ССЛ29",
                    "ССЛ30",
                    "ССЛ31",
                    "ССЛ32",
                    "ССЛ33",
                    "ССЛ34",
                    "ССЛ35",
                    "ССЛ36",
                    "ССЛ37",
                    "ССЛ38",
                    "ССЛ39",
                    "ССЛ40",
                    "ССЛ41",
                    "ССЛ42",
                    "ССЛ43",
                    "ССЛ44",
                    "ССЛ45",
                    "ССЛ46",
                    "ССЛ47",
                    "ССЛ48",
                    "Z 1 ИО",
                    "Z 1",
                    "Z 2 ИО",
                    "Z 2",
                    "Z 3 ИО",
                    "Z 3",
                    "Z 4 ИО",
                    "Z 4",
                    "Z 5 ИО",
                    "Z 5",
                    "Z 6 ИО",
                    "Z 6",
                    "РЕЗЕРВ 1",
                    "РЕЗЕРВ 2",
                    "P1 ИО",
                    "P1",
                    "P2 ИО",
                    "P2",
                    "РЕЗЕРВ 3",
                    "РЕЗЕРВ 4",
                    "I> 1 ИО",
                    "I> 1",
                    "I> 2 ИО",
                    "I> 2",
                    "I> 3 ИО",
                    "I> 3",
                    "I> 4 ИО",
                    "I> 4",
                    "I> 5 ИО",
                    "I> 5",
                    "I> 6 ИО",
                    "I> 6",
                    "I< ИО",
                    "I<",
                    "I*> 1 ИО",
                    "I*> 1",
                    "I*> 2 ИО",
                    "I*> 2",
                    "I*> 3 ИО",
                    "I*> 3",
                    "I*> 4 ИО",
                    "I*> 4",
                    "I*> 5 ИО",
                    "I*> 5",
                    "I*> 6 ИО",
                    "I*> 6",
                    "I*> 7 ИО",
                    "I*> 7",
                    "I*> 8 ИО",
                    "I*> 8",
                    "I2/I1> ИО",
                    "I2/I1>",
                    "U> 1 ИО",
                    "U> 1",
                    "U> 2 ИО",
                    "U> 2",
                    "U> 3 ИО",
                    "U> 3",
                    "U> 4 ИО",
                    "U> 4",
                    "U< 1 ИО",
                    "U< 1",
                    "U< 2 ИО",
                    "U< 2",
                    "U< 3 ИО",
                    "U< 3",
                    "U< 4 ИО",
                    "U< 4",
                    "F> 1 ИО",
                    "F> 1",
                    "F> 2 ИО",
                    "F> 2",
                    "F> 3 ИО",
                    "F> 3",
                    "F> 4 ИО",
                    "F> 4",
                    "F< 1 ИО",
                    "F< 1",
                    "F< 2 ИО",
                    "F< 2",
                    "F< 3 ИО",
                    "F< 3",
                    "F< 4 ИО",
                    "F< 4",
                    "Q> 1",
                    "Q> 2",
                    "БЛК.ПО Q",
                    "БЛК.ПО N", // Резерв для 771
                    "ПУСК", // Резерв для 771
                    "НЕИСПР.",
                    "РЕЗЕРВ 5",
                    "УСКпоВКЛ.",
                    "СИГНАЛ-ЦИЯ",
                    "АВАР.ОТКЛ",
                    "ОТКЛ.ВЫКЛ.",
                    "ВКЛ.ВЫКЛ.",
                    "ПУСК АПВ",
                    "АПВ 1крат",
                    "АПВ 2крат ",
                    "АПВ 3крат ",
                    "АПВ 4крат",
                    "ВКЛ.поАПВ",
                    "ЗАПРЕТ АПВ",
                    "АПВ БЛОК.",
                    "ГОТ-ТЬ АПВ",
                    "КСиУППНавт",
                    "U1-U2+",
                    "U1+U2-",
                    "U1-U2-",
                    "Условия ОС",
                    "Условия УС",
                    "КСиУППНвкл",
                    "ПОВР. Ф. A",
                    "ПОВР. Ф. B",
                    "ПОВР. Ф. C",
                    "КАЧАНИЕ",
                    "КАЧ.ВНЕШ",
                    "КАЧ.ВНУТР",
                    "НеиспТНмгн ",
                    "НеиспТНс/п",
                    "ВХОД K1",
                    "ВХОД K2",
                    "УРОВ 1",
                    "УРОВ 2",
                    "БЛК. УРОВ",
                    "АВР ВКЛ.", // rez 771
                    "АВР ОТКЛ.", // rez 771 
                    "АВР БЛК.", // rez 771
                    "ПУСК ДУГ.", // rez 771
                    "АВРвывДис", // ВЧС 1 771
                    "АВРвведен", // ВЧС 0 771
                    "АВРвывед.", // ВЧС ОТКЛ.1 771
                    "АВР ГОТОВ", // ВЧС ОТКЛ.0 771
                    "АВРдеблок", // РЕВЕРС? 1 771
                    "РЕЗЕРВ 6", // РЕВЕРС? 0 771
                    "РЕЗЕРВ 7", // РЕВЕРС 1 771
                    "РЕЗЕРВ 8", // РЕВЕРС 0 771
                    "РЕЗЕРВ 9", // Эхо 1 771
                    "РЕЗЕРВ 10", // Эхо 0 771
                    "РЕЗЕРВ 11",
                    "РЕЗЕРВ 12",
                    "РЕЗЕРВ 13",
                    "РЕЗЕРВ 14",
                    "РЕЗЕРВ 15",
                    "РЕЗЕРВ 16",
                    "РЕЗЕРВ 17",
                    "РЕЗЕРВ 18",
                    "РЕЗЕРВ 19",
                    "РЕЗЕРВ 20",
                    "ВНЕШ. 1",
                    "ВНЕШ. 2",
                    "ВНЕШ. 3",
                    "ВНЕШ. 4",
                    "ВНЕШ. 5",
                    "ВНЕШ. 6",
                    "ВНЕШ. 7",
                    "ВНЕШ. 8",
                    "ВНЕШ. 9",
                    "ВНЕШ. 10",
                    "ВНЕШ. 11",
                    "ВНЕШ. 12",
                    "ВНЕШ. 13",
                    "ВНЕШ. 14",
                    "ВНЕШ. 15",
                    "ВНЕШ. 16"
                };

                SortedDictionary<int, string> ret = new SortedDictionary<int, string>();
                for (int i = 0; i < list.Count; i++)
                {
                    ret.Add(i, list[i]);
                }

                switch (DeviceType)
                {
                    case MRUniversal.T4N4D42R35:
                    case MRUniversal.T4N5D42R35:
                        for (int i = 40; i < 42 + 46; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                    case MRUniversal.T4N4D74R67:
                    case MRUniversal.T4N5D74R67:
                        for (int i = 72; i < 72 + 16; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                    default:
                        //дискреты
                        for (int i = 16; i < 16 + 72; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                }

                switch(DeviceName)
                {
                    case "MR771":

                        //Двигатель
                        int deleteNInd = ret.First(v => v.Value == "БЛК.ПО N").Key;

                        for (int i = deleteNInd; i < deleteNInd + 2; i++)
                        {
                            ret.Remove(i);
                        }

                        //АВР
                        int deleteAvrInd = ret.First(v => v.Value == "АВР ВКЛ.").Key;

                        for (int i = deleteAvrInd; i < deleteAvrInd + 4; i++)
                        {
                            ret.Remove(i);
                        }

                        int changedNameSignalsInd = ret.First(v => v.Value == "АВРвывДис").Key;

                        ret[changedNameSignalsInd] = "ВЧС 1";
                        ret[changedNameSignalsInd + 1] = "ВЧС 0";
                        ret[changedNameSignalsInd + 2] = "ВЧС ОТКЛ.1";
                        ret[changedNameSignalsInd + 3] = "ВЧС ОТКЛ.0";
                        ret[changedNameSignalsInd + 4] = "РЕВЕРС? 1";
                        ret[changedNameSignalsInd + 5] = "РЕВЕРС? 0";
                        ret[changedNameSignalsInd + 6] = "РЕВЕРС 1";
                        ret[changedNameSignalsInd + 7] = "РЕВЕРС 0";
                        ret[changedNameSignalsInd + 8] = "Эхо 1";
                        ret[changedNameSignalsInd + 9] = "Эхо 0";

                        //Ступени по Z
                        int deleteRezInd = ret.First(v => v.Value == "РЕЗЕРВ 1").Key;
                        ret[deleteRezInd] = "Z7 ИО";
                        ret[deleteRezInd + 1] = "Z7";
                        ret[deleteRezInd + 2] = "Z8 ИО";
                        ret[deleteRezInd + 3] = "Z8";
                        ret[deleteRezInd + 4] = "Z9 ИО";
                        ret[deleteRezInd + 5] = "Z9";
                        ret[deleteRezInd + 6] = "Z10 ИО";
                        ret[deleteRezInd + 7] = "Z10";

                        int rez5Ind = ret.First(v => v.Value == "РЕЗЕРВ 5").Key;
                        ret.Remove(rez5Ind);

                        int rez11Ind = ret.First(v => v.Value == "РЕЗЕРВ 11").Key;

                        for (int i = rez11Ind; i < rez11Ind + 10; i++)
                        {
                            ret.Remove(i);
                        }

                        break;
                    default:

                        //int rez5Ind761 = ret.First(v => v.Value == "РЕЗЕРВ 5").Key;
                        //ret.Remove(rez5Ind761);

                        //int rez6Ind761 = ret.First(v => v.Value == "РЕЗЕРВ 6").Key;

                        //for (int i = rez6Ind761; i < rez6Ind761 + 15; i++)
                        //{
                        //    ret.Remove(i);
                        //}

                        break;
                }

                return ret.ToDictionary(r => r.Key, r => r.Value);
            }
        }
       
        /// <summary>
        /// Журнал аварий. Сработавший параметр
        /// </summary>
        public static List<string> AlarmJournalTrigged
        {
            get
            {
                List<string> ret = new List<string>
                    {
                        "Iд",       //0
                        "Iдb",      //1
                        "Iдc",      //2
                        "Iд",       //3
                        "Iдb",      //4
                        "Iдc",      //5
                        "Iд",       //6
                        "Iдb",      //7
                        "Iдc",      //8
                        "Iт",       //9
                        "Iтb",      //10
                        "Iтc",      //11
                        "Iт",       //12
                        "Iтb",      //13
                        "Iтc",      //14
                        "Iт",       //15
                        "Iтb",      //16
                        "Iтc",      //17
                        "Iприс.1",  //18
                        "Iприс.2",  //19
                        "Iприс.3",  //20
                        "Iприс.4",  //21
                        "Iприс.5",  //22
                        "Iприс.6",  //23
                        "Iприс.7",  //24
                        "Iприс.8",  //25
                        "Iприс.9",  //26
                        "Iприс.10", //27
                        "Iприс.11", //28
                        "Iприс.12", //29
                        "Iприс.13", //30
                        "Iприс.14", //31
                        "Iприс.15", //32
                        "Iприс.16", //33
                        "Iприс.17", //34
                        "Iприс.18", //35
                        "Iприс.19", //36
                        "Iприс.20", //37
                        "Iприс.21", //38
                        "Iприс.22", //39
                        "Iприс.23", //40
                        "Iприс.24", //41
                        "",         //42
                        "",         //43
                        ""          //44 СПЛ
                    };

                if (DeviceType == "T20N4D40R35" || DeviceType == "T20N4D32R43")
                {
                    int ind = ret.IndexOf("Iприс.21");
                    ret.RemoveAt(ind);
                    ret.RemoveAt(ind);
                    ret.RemoveAt(ind);
                    ret.RemoveAt(ind);
                    ret.Insert(ind, "Un");
                    ret.Insert(ind, "Uc");
                    ret.Insert(ind, "Ub");
                    ret.Insert(ind, "Ua");
                    ret.Add("res1");
                    ret.Add("res2");
                    ret.Add("res3");
                    ret.Add("");//СПЛ
                    ret.Add("Uab");
                    ret.Add("Ubс");
                    ret.Add("Uca");
                    ret.Add("3U0");
                    ret.Add("U1");
                    ret.Add("U2");
                }

                return ret;
            }
        }

        public static List<string> DefaultSignaturesForOsc
        {
            get
            {
                List<string> ret = new List<string>();

                for (int i = 0; i < InputLogicStruct.DiscrestCount; i++)
                {
                    ret.Add($"Д{i + 1}");
                }

                for (int i = 0; i < OscopeAllChannelsStruct.ChannelsCount; i++)
                {
                    ret.Add($"K{i + 1}");
                }

                return ret;
            }

        }

        public static List<string> SignaturesForOsc { get; set; }
        public static List<string> OscList { get; set; }
        #endregion
    }
}
