﻿using System;
using System.Xml.Serialization;

namespace BEMN.MRUNIVERSAL.Diagnostics
{
    [Serializable]
    [XmlRoot("DocumentElement")]
    [XmlType("МР_журнал_аварий")]
    public class AJ
    {
        [XmlElement(ElementName = "_indexColumn")]
        public string RecordNumber { get; set; }
        [XmlElement(ElementName = "_dateTimeColumn")]
        public string DateTime { get; set; }
        [XmlElement(ElementName = "_messageColumn")]
        public string Message { get; set; }
        [XmlElement(ElementName = "_defenseColumn")]
        public string TriggeredDefence { get; set; }
        [XmlElement(ElementName = "_paramColumn")]
        public string Parametr { get; set; }
        [XmlElement(ElementName = "_paramValueColumn")]
        public string ParametrValue { get; set; }
        [XmlElement(ElementName = "_groupColumn")]
        public string Group { get; set; }
        [XmlElement(ElementName = "_rabColumn")]
        public string Rab { get; set; }
        [XmlElement(ElementName = "_xabColumn")]
        public string Xab { get; set; }
        [XmlElement(ElementName = "_rbcColumn")]
        public string Rbc { get; set; }
        [XmlElement(ElementName = "_xbcColumn")]
        public string Xbc { get; set; }
        [XmlElement(ElementName = "_rcaColumn")]
        public string Rca { get; set; }
        [XmlElement(ElementName = "_xcaColumn")]
        public string Xca { get; set; }
        [XmlElement(ElementName = "_raColumn")]
        public string Ra { get; set; }
        [XmlElement(ElementName = "_xaColumn")]
        public string Xa { get; set; }
        [XmlElement(ElementName = "_rbColumn")]
        public string Rb { get; set; }
        [XmlElement(ElementName = "_xbColumn")]
        public string Xb { get; set; }
        [XmlElement(ElementName = "_rcColumn")]
        public string Rc { get; set; }
        [XmlElement(ElementName = "_xcColumn")]
        public string Xc { get; set; }
        [XmlElement(ElementName = "_iaColumn")]
        public string Ia { get; set; }
        [XmlElement(ElementName = "_ibColumn")]
        public string Ib { get; set; }
        [XmlElement(ElementName = "_icColumn")]
        public string Ic { get; set; }
        [XmlElement(ElementName = "_i1Column")]
        public string I1 { get; set; }
        [XmlElement(ElementName = "_i2Column")]
        public string I2 { get; set; }
        [XmlElement(ElementName = "_3i0Column")]
        public string I0 { get; set; }
        [XmlElement(ElementName = "_inColumn")]
        public string In { get; set; }
        [XmlElement(ElementName = "_irColumn")]
        public string Ig { get; set; }
        [XmlElement(ElementName = "_in1Column")]
        public string In1 { get; set; }
        [XmlElement(ElementName = "_qColumn")]
        public string Q { get; set; }
        [XmlElement(ElementName = "_uaColumn")]
        public string Ua { get; set; }
        [XmlElement(ElementName = "_ubColumn")]
        public string Ub { get; set; }
        [XmlElement(ElementName = "_ucColumn")]
        public string Uc { get; set; }
        [XmlElement(ElementName = "_uabColumn")]
        public string Uab { get; set; }
        [XmlElement(ElementName = "_ubcColumn")]
        public string Ubc { get; set; }
        [XmlElement(ElementName = "_ucaColumn")]
        public string Uca { get; set; }
        [XmlElement(ElementName = "_u1Column")]
        public string U1 { get; set; }
        [XmlElement(ElementName = "_u2Column")]
        public string U2 { get; set; }
        [XmlElement(ElementName = "_3u0Column")]
        public string U0 { get; set; }
        [XmlElement(ElementName = "_fColumn")]
        public string F { get; set; }
        [XmlElement(ElementName = "_unColumn")]
        public string Un { get; set; }
        [XmlElement(ElementName = "_un1Column")]
        public string Un1 { get; set; }
        [XmlElement(ElementName = "_d1to8Column")]
        public string D1To8 { get; set; }
        [XmlElement(ElementName = "_d9to16Column")]
        public string D9To16 { get; set; }
        [XmlElement(ElementName = "_d17to24Column")]
        public string D17To24 { get; set; }
        [XmlElement(ElementName = "_d25to32Column")]
        public string D25To32 { get; set; }
        [XmlElement(ElementName = "_d33to40Column")]
        public string D33To40 { get; set; }
        [XmlElement(ElementName = "_d41to48Column")]
        public string D41To48 { get; set; }
        [XmlElement(ElementName = "_d49to56Column")]
        public string D49To56 { get; set; }
        [XmlElement(ElementName = "_d57to64Column")]
        public string D57To64 { get; set; }
        [XmlElement(ElementName = "_d65to72Column")]
        public string D65To72 { get; set; }

        public AJ() { }

        public AJ(string recordNumber, string dateTime, string message, string triggeredDefence, string parametr, string parametrValue,
            string group, string rab, string xab, string rbc, string xbc, string rca,
            string xca, string ra, string xa, string rb, string xb, string rc, string xc,
            string ia,string ib, string ic, string i1, string i2, string i0, string iN, string ig, string in1, string q,
            string ua, string ub, string uc, string uab, string ubc, string uca, string u1, string u2, string u0, 
            string f, string un, string un1, string d1to8, string d9to16, string d17to24, string d25to32, string d33to40,
            string d41to48, string d49to56, string d57to64, string d65to72)
        {
            this.RecordNumber = recordNumber;
            this.DateTime = dateTime;
            this.Message = message;
            this.TriggeredDefence = triggeredDefence;
            this.Parametr = parametr;
            this.ParametrValue = parametrValue;
            this.Group = group;
            this.Rab = rab;
            this.Xab = xab;
            this.Rbc = rbc;
            this.Xbc = xbc;
            this.Rca = rca;
            this.Xca = xca;
            this.Ra = ra;
            this.Xa = xa;
            this.Rb = rb;
            this.Xb = xb;
            this.Rc = rc;
            this.Xc = xc;
            this.Ia = ia;
            this.Ib = ib;
            this.Ic = ic;
            this.I1 = i1;
            this.I2 = i2;
            this.I0 = i0;
            this.In = iN;
            this.Ig = ig;
            this.In1 = in1;
            this.Ua = ua;
            this.Ub = ub;
            this.Uc = uc;
            this.Uab = uab;
            this.Ubc = ubc;
            this.Uca = uca;
            this.U1 = u1;
            this.U2 = u2;
            this.U0 = u0;
            this.Un = un;
            this.Un1 = un1;
            this.F = f;
            this.Q = q;
            this.D1To8 = d1to8;
            this.D9To16 = d9to16;
            this.D17To24 = d17to24;
            this.D25To32 = d25to32;
            this.D33To40 = d33to40;
            this.D41To48 = d41to48;
            this.D49To56 = d49to56;
            this.D57To64 = d57to64;
            this.D65To72 = d65to72;
        }
    }
}
