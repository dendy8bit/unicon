﻿using System;
using System.Xml.Serialization;

namespace BEMN.MRUNIVERSAL.Diagnostics
{
    [Serializable]
    [XmlRoot("DocumentElement")]
    [XmlType("МР_журнал_системы")]
    public class SystemJournal
    {
        [XmlElement(ElementName = "Номер")]
        public string RecordNumber { get; set; }
        [XmlElement(ElementName = "Время")]
        public string RecordTime { get; set; }
        [XmlElement(ElementName = "Сообщение")]
        public string RecordMessage { get; set; }

        public SystemJournal() { }

        public SystemJournal(string recordNumber, string recordTime, string recordMessage)
        {
            this.RecordNumber = recordNumber;
            this.RecordTime = recordTime;
            this.RecordMessage = recordMessage;
        }
    }
}
