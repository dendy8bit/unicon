﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Compressor;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using SchemeEditorSystem.ResourceLibs;
using BEMN.MRUNIVERSAL.SystemJournal;
using BEMN.MRUNIVERSAL.AlarmJournal.Structures;
using BEMN.MRUNIVERSAL.AlarmJournal;
using BEMN.MRUNIVERSAL.Configuration.Structures;
using BEMN.MRUNIVERSAL.Configuration.Structures.MeasuringTransformer;
using BEMN.MRUNIVERSAL.Configuration.Structures.Oscope;
using BEMN.MRUNIVERSAL.Osc.HelpClasses;
using BEMN.MRUNIVERSAL.Osc.Structures;
using System.Threading;

namespace BEMN.MRUNIVERSAL.Diagnostics
{
    public partial class AllInformationsDeviceForm : Form, IFormView
    {
        // Основные
        private MRUniversal _device;
        private MessageBoxForm _messageBoxForm;
        private FileDriver _fileDriver;
        private const string ARH_NANE = "logarch.zip";
        private int _count; // счетчик показа сообщений: если равен 0, то показывает resultMessage
        private int _countOscForRead; // счетчик читаемых осциллограмм
        private IEnumerable<CheckBox> _checkBoxs;

        
        // ЖС
        private readonly MemoryEntity<OneWordStruct> _saveIndex;
        private int _recordNumber;
        private JournalLoader _systemJournalLoader;
        private List<SystemJournal> _systemJournalData;
        

        // ЖА
        private readonly MemoryEntity<OneWordStruct> _setPageAlarmJournal;
        private readonly MemoryEntity<AlarmJournalStruct> _alarmJournal;
        private readonly AlarmJournalLoader _journalLoader;
        private readonly CurrentOptionsLoader _currentOptionsLoaderAJ;
        private List<AJ> _alarmJournalData;


        // Конфигурация
        private readonly MemoryEntity<ConfigurationStruct> _configuration;
        private readonly MemoryEntity<ConfigurationStruct771> _configuration771;
        private ConfigurationStruct _currentSetpointsStruct;
        private ConfigurationStruct771 _currentSetpointsStruct771;


        // Осциллограф
        private readonly OscPageLoaderDiagnostics _pageLoader;
        private readonly OscJournalLoaderDiagnostics _oscJournalLoader;
        private readonly CurrentOptionsLoader _currentOptionsLoaderOsc;
        private readonly MemoryEntity<OscOptionsStruct> _oscOptions;
        private CountingList _countingList;
        private OscJournalStruct _journalStruct;
        private MemoryEntity<OscopeAllChannelsStruct> _oscopeStruct;


        #region [Constructor]
        public AllInformationsDeviceForm()
        {
            this.InitializeComponent();
        }

        public AllInformationsDeviceForm(MRUniversal device)
        {
            this.InitializeComponent();

            this._device = device;
            this._messageBoxForm = new MessageBoxForm();
            this._checkBoxs = new List<CheckBox>();
            this._oscCountMTB.MaxLength = 2;

            // ЖС
            this._systemJournalData = new List<SystemJournal>();
            this._systemJournalLoader = new JournalLoader(this._device.AllSystemJournal1024Diagnostics, this._device.AllSystemJournal64Diagnostics, this._device.IndexJSDiagnostics);
            this._systemJournalLoader.JournalReadOk += HandlerHelper.CreateActionHandler(this, this.AllReadSJRecords);
            this._systemJournalLoader.JournalReadFail += HandlerHelper.CreateActionHandler(this, () => this._count--);
            this._systemJournalLoader.MessagesList = PropFormsStrings.GetNewJournalList();

            // ЖА
            this._alarmJournalData = new List<AJ>();
            this._currentOptionsLoaderAJ = this._device.CurrentOptionsLoaderAJDiagnostics;
            this._currentOptionsLoaderAJ.LoadOk += HandlerHelper.CreateActionHandler(this, this.StartReadJournal);
            this._currentOptionsLoaderAJ.LoadFail += HandlerHelper.CreateActionHandler(this, () =>
            {
                this._count--;
                this._messageBoxForm.ShowMessage("Ошибка сохранения ЖА");
                Thread.Sleep(3000);
            });
            this._journalLoader = new AlarmJournalLoader(this._device.AllAlarmJournalDiagnostics, this._device.AlarmJournalDiagnostics, this._device.SetPageAlarmJournalDiagnostics);
            this._journalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.AllReadAJRecords);
            this._journalLoader.AllJournalReadFail += HandlerHelper.CreateActionHandler(this, () => this._count--);
            this._journalLoader.MessagesList = PropFormsStrings.GetNewAlarmList();

            // Конфигурация
            switch (this._device.DeviceType)
            {
                case "MR761":
                    this._configuration = this._device.ConfigurationDiagnostics;
                    this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                    this._configuration.ReadFail += HandlerHelper.CreateHandler(this, () =>
                    {
                        this._configuration.RemoveStructQueries();
                        this._count--;
                    });
                    this._currentSetpointsStruct = new ConfigurationStruct();
                    break;
                case "MR771":
                    this._configuration771 = this._device.Configuration771Diagnostics;
                    this._configuration771.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                    this._configuration771.ReadFail += HandlerHelper.CreateHandler(this, () =>
                    {
                        this._configuration771.RemoveStructQueries();
                        this._count--;
                    });
                    this._currentSetpointsStruct771 = new ConfigurationStruct771();
                    break;
                default:
                    this._configuration = this._device.ConfigurationDiagnostics;
                    this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                    this._configuration.ReadFail += HandlerHelper.CreateHandler(this, () =>
                    {
                        this._configuration.RemoveStructQueries();
                        this._count--;
                    });
                    this._currentSetpointsStruct = new ConfigurationStruct();
                    break;
            }


            // Осциллограф
            // Загрузчик страниц
            this._pageLoader = new OscPageLoaderDiagnostics(this._device.SetOscStartPageDiagnostics, this._device.OscPageDiagnostics);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            
            // Загрузчик журнала
            this._oscJournalLoader = new OscJournalLoaderDiagnostics(this._device.AllOscJournalDiagnostics, this._device.OscJournalDiagnostics, this._device.RefreshOscJournalDiagnostics);
            this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, ReadAllRecords);
            this._oscJournalLoader.AllJournalReadFail += HandlerHelper.CreateActionHandler(this, () => this.OscFailRead("записей осциллографа"));
            
            // Уставки осцилографа
            this._oscOptions = this._device.OscOptionsDiagnostics;
            this._oscOptions.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._oscJournalLoader.Clear();
                this._oscJournalLoader.StartReadJournal();
            });
            this._oscOptions.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => this.OscFailRead("уставок осциллографа"));

            // Каналы осциллографа
            this._oscopeStruct = this._device.AllChannelsDiagnostics;
            this._oscopeStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () => this._oscOptions.LoadStruct());
            this._oscopeStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => this.OscFailRead("каналов осциллографа"));
            
            // Загрузчик уставок токов
            this._currentOptionsLoaderOsc = this._device.CurrentOptionsLoaderOscDiagnostics;
            this._currentOptionsLoaderOsc.LoadOk += this._oscopeStruct.LoadStruct; // успешно прочитанная структура измерений вызовет метод чтения структуры
            this._currentOptionsLoaderOsc.LoadFail += () => this.OscFailRead("уставок токов");
            
            this._fileDriver = new FileDriver(this._device, this);
        }

        #endregion [Constructor]


        #region [AJ Methods]
        private void StartReadJournal()
        {
            this._journalLoader.Clear();
            this._journalLoader.StartRead();
        }

        private void StartReadOption()
        {
            this._currentOptionsLoaderAJ.StartRead();
        }

        private void AllReadAJRecords()
        {
            this._count--;
            this._alarmJournalData.Clear();
            if (this._journalLoader.AlarmJournalRecords.Count == 0)
            {
                this._jaCB.Enabled = false;
                return;
            }
            this._recordNumber = 1;
            foreach (AlarmJournalStruct record in this._journalLoader.AlarmJournalRecords)
            {
                try
                {
                    MeasureTransStruct measure = this._currentOptionsLoaderAJ._connections[record.GroupOfSetpoints].Value;
                    AJ alarmJournalItem = new AJ(
                        this._recordNumber.ToString(), // №                                                                                                     0
                        record.GetDateTime, // 0-6 (Дата/Время)                                                                                                 1
                        StringsAj.Message[record.Message], // 7 (Сообщение)                                                                                     2
                        this.GetTriggeredDefence(record), // 8 : 0-7 биты (Сработанная защита)                                                                  3
                        this.GetParameter(record), // 8 : 8-15 биты (Параметр)                                                                                  4
                        this.GetParametrValue(record, measure), //                                                                                              5   
                        StringsAj.AlarmJournalSetpointsGroup[record.GroupOfSetpoints], // 9 : 0-2 биты (Группа уставок)                                         6
                        ValuesConverterCommon.Analog.GetResist((short)record.Rab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 12                     7 
                        ValuesConverterCommon.Analog.GetResist((short)record.Xab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 13                     8
                        ValuesConverterCommon.Analog.GetResist((short)record.Rbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 14                     9
                        ValuesConverterCommon.Analog.GetResist((short)record.Xbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 15                     10
                        ValuesConverterCommon.Analog.GetResist((short)record.Rca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 16                     11
                        ValuesConverterCommon.Analog.GetResist((short)record.Xca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 17                     12
                        // this.GetStep(record.NumberOfTriggeredParametr),                                                              // 8 : 8-15 биты?       13
                        ValuesConverterCommon.Analog.GetResist((short)record.Ra1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 18                     14
                        ValuesConverterCommon.Analog.GetResist((short)record.Xa1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 19                     15
                        ValuesConverterCommon.Analog.GetResist((short)record.Rb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 20                     16
                        ValuesConverterCommon.Analog.GetResist((short)record.Xb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 21                     17
                        ValuesConverterCommon.Analog.GetResist((short)record.Rc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 22                     18
                        ValuesConverterCommon.Analog.GetResist((short)record.Xc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 23                     19
                        ValuesConverterCommon.Analog.GetI(record.Ia, measure.ChannelI.Ittl * 40),                                     // 25                     20
                        ValuesConverterCommon.Analog.GetI(record.Ib, measure.ChannelI.Ittl * 40),                                     // 26                     21  
                        ValuesConverterCommon.Analog.GetI(record.Ic, measure.ChannelI.Ittl * 40),                                     // 27                     22
                        ValuesConverterCommon.Analog.GetI(record.I1, measure.ChannelI.Ittl * 40),                                     // 30                     23
                        ValuesConverterCommon.Analog.GetI(record.I2, measure.ChannelI.Ittl * 40),                                     // 29                     24   
                        ValuesConverterCommon.Analog.GetI(record.I0, measure.ChannelI.Ittl * 40),                                     // 28                     25  
                        ValuesConverterCommon.Analog.GetI(record.In, measure.ChannelI.Ittx * 40),                                     // 32                     26
                        ValuesConverterCommon.Analog.GetI(record.Ig, measure.ChannelI.Ittl * 40),                                     // 33                     27
                        ValuesConverterCommon.Analog.GetI(record.In1, measure.ChannelI.Ittl * 40),                                    // 34                     28
                        ValuesConverterCommon.Analog.GetQ(record.Q),                                                                  // 31                     29
                        ValuesConverterCommon.Analog.GetU(record.Ua, measure.ChannelU.KthlValue),                                     // 35                     30
                        ValuesConverterCommon.Analog.GetU(record.Ub, measure.ChannelU.KthlValue),                                     // 36                     31
                        ValuesConverterCommon.Analog.GetU(record.Uc, measure.ChannelU.KthlValue),                                     // 37                     32
                        ValuesConverterCommon.Analog.GetU(record.Uab, measure.ChannelU.KthlValue),                                    // 38                     33
                        ValuesConverterCommon.Analog.GetU(record.Ubc, measure.ChannelU.KthlValue),                                    // 39                     34
                        ValuesConverterCommon.Analog.GetU(record.Uca, measure.ChannelU.KthlValue),                                    // 40                     35
                        ValuesConverterCommon.Analog.GetU(record.U1, measure.ChannelU.KthlValue),                                     // 43                     36
                        ValuesConverterCommon.Analog.GetU(record.U2, measure.ChannelU.KthlValue),                                     // 42                     37
                        ValuesConverterCommon.Analog.GetU(record.U0, measure.ChannelU.KthlValue),                                     // 41                     38
                        ValuesConverterCommon.Analog.GetF(record.F),                                                                  // 44                     39
                        ValuesConverterCommon.Analog.GetU(record.Un, measure.ChannelU.KthxValue),                                     // 45                     40
                        ValuesConverterCommon.Analog.GetU(record.Un1, measure.ChannelU.Kthx1Value),                                   // 46                     41
                        record.D1To8,                                                                                                 // 47 : 0-7 биты          42
                        record.D9To16,                                                                                                // 47 : 8-15 биты         43  
                        record.D17To24,                                                                                               // 48 : 0-7 биты          44        
                        record.D25To32,                                                                                               // 48 : 8-15 биты         45
                        record.D33To40,                                                                                               // 49 : 0-7 биты          46
                        record.D41To48,                                                                                               // 50 : 8-15 биты         47
                        record.D49To56,                                                                                               // 51 : 0-7 биты          48
                        record.D57To64,                                                                                               // 52 : 8-15 биты         49
                        record.D65To72                                                                                                // 53 : 0-7 биты          50
                        );

                    this._alarmJournalData.Add(alarmJournalItem);
                }
                catch (Exception e)
                {
                    
                }
                this._recordNumber++;
            }

            try
            {
                XmlSerializer formatter = new XmlSerializer(typeof(List<AJ>));
                using (FileStream file = new FileStream($"{this.folderBrowserDialog.SelectedPath}\\Журнал Аварий {this._device.DeviceType} версия {this._device.DeviceVersion}.xml", FileMode.OpenOrCreate))
                {
                    formatter.Serialize(file, this._alarmJournalData);
                }

                if (this._count == 0)
                {
                    this._messageBoxForm.ShowResultMessage("Сохранение данных завершено успешно");
                }
                else
                {
                    this._messageBoxForm.ShowMessage("Файл ЖА сохранен");
                }
                
            }
            catch (Exception e)
            {
            }
        }

        private string GetTriggeredDefence(AlarmJournalStruct record)
        {
            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.07 && this._device.AllAlarmJournal != null)
            {
                return record.TriggeredDefense == 61
                    ? this._journalLoader.MessagesList[record.ValueOfTriggeredParametr]
                    : StringsAj.TriggeredDefense[record.TriggeredDefense];
            }
            return record.TriggeredDefense == 61
                ? this._alarmJournal.Value.MessagesList[record.ValueOfTriggeredParametr]
                : StringsAj.TriggeredDefense[record.TriggeredDefense];
        }

        private string GetParameter(AlarmJournalStruct record)
        {
            if (record.TriggeredDefense == 15 || record.TriggeredDefense >= 44 && record.TriggeredDefense <= 59 || record.TriggeredDefense == 61)
            {
                return string.Empty;
            }
            return StringsAj.Parametr[record.NumberOfTriggeredParametr];
        }

        private string GetParametrValue(AlarmJournalStruct record, MeasureTransStruct measure)
        {
            int parameter = record.NumberOfTriggeredParametr; // 8 : 8-15
            ushort value = record.ValueOfTriggeredParametr;   // 10
            if (parameter >= 36 && parameter <= 40 || parameter == 42 || parameter == 65)
            {
                return ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittl * 40);
            }
            if (parameter == 41 || parameter == 43)
            {
                return ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittx * 40);
            }
            if (parameter >= 0 && parameter <= 35)
            {
                ushort value1 = record.ValueOfTriggeredParametr1;
                return ValuesConverterCommon.Analog.GetZ((short)value, (short)value1, measure.ChannelU.KthlValue, measure.ChannelI.Ittl);
            }
            if (parameter >= 44 && parameter <= 52)
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
            }
            if (parameter == 53)
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthxValue);
            }
            if (parameter == 54 || parameter == 69)
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.Kthx1Value);
            }
            if (parameter == 55)
            {
                return ValuesConverterCommon.Analog.GetF(value);
            }
            //if (parameter == StringsAj.OmpInd)
            //{
            //    return ValuesConverterCommon.Analog.GetOmp(value);
            //}
            if (parameter == 60)
            {
                return ValuesConverterCommon.Analog.GetQ(value);
            }
            if (parameter == 61 || parameter == 67 || parameter == 68 || parameter >= 56 && parameter <= 58)
            {
                return string.Empty;
            }
            if (parameter == 64)
            {
                return ValuesConverterCommon.Analog.GetSignF((short)value);
            }
            return value.ToString();
        }

        #endregion [AJ Methods]


        #region [SJ Methods]

        private void OnListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "Операция успешно выполнена")
                {
                    //_messageBoxForm.ShowMessage("Подписи прочитаны");
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            string nameDevice = "";
                            switch (_device.DeviceType)
                            {
                                case "MR771":
                                    nameDevice = "MR771";
                                    break;
                                case "MR761":
                                    nameDevice = "MR76X";
                                    break;
                            }

                            XmlRootAttribute root = new XmlRootAttribute(nameDevice);
                            XmlSerializer serializer = new XmlSerializer(typeof(ListsOfJournals), root);
                            ListsOfJournals lists = (ListsOfJournals) serializer.Deserialize(reader);                          
                        }
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                this._systemJournalLoader.MessagesList = PropFormsStrings.GetNewJournalList();
            }

            this._systemJournalLoader.StartRead();
        }
        
        /// <summary>
        /// Счётчик сообщений
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
            set { this._recordNumber = value; }
        }

        private void AllReadSJRecords()
        {
            this._count--;
            this._systemJournalData.Clear();
            if (this._systemJournalLoader.JournalRecords.Count != 0)
            {
                for (int i = 0; i < this._systemJournalLoader.JournalRecords.Count; i++)
                {
                    SystemJournal sj = new SystemJournal((i + 1).ToString(),
                        this._systemJournalLoader.JournalRecords[i].GetRecordTime,
                        this._systemJournalLoader.JournalRecords[i].GetRecordMessage
                    );
                    this._systemJournalData.Add(sj);
                }

                try
                {
                    XmlSerializer formatter = new XmlSerializer(typeof(List<SystemJournal>));
                    using (FileStream file = new FileStream($"{this.folderBrowserDialog.SelectedPath}\\Журнал Системы {this._device.DeviceType} версия {this._device.DeviceVersion}.xml", FileMode.OpenOrCreate))
                    {
                        formatter.Serialize(file, this._systemJournalData);
                    }

                    if (this._count == 0)
                    {
                        this._messageBoxForm.ShowResultMessage("Сохранение данных завершено успешно");
                    }
                    else
                    {
                        this._messageBoxForm.ShowMessage("Файл ЖС сохранен");
                    }
                }
                catch (Exception e)
                {
                }
            }
            else
            {
               //_messageBoxForm.ShowResultMessage("ЖC прочитан");
            }
            this._fileDriver.CloseAll(this.OnClosedFiles);
        }

        #endregion [SJ Methods]


        #region [Configuration Methods]

        private void ConfigurationReadOk()
        {
            this._count--;
            switch (this._device.DeviceType)
            {
                case "MR761":
                    this._currentSetpointsStruct = this._configuration.Value;
                    break;
                case "MR771":
                    this._currentSetpointsStruct771 = this._configuration771.Value;
                    break;
            }
            
            this.Serialize($"{this.folderBrowserDialog.SelectedPath}\\Конфигурация {this._device.DeviceType} версия {this._device.DeviceVersion}.xml");
        }

        public void Serialize(string binFileName)
        {
            try
            {
                string nameDevice = "";
                ushort[] values = {};
                string xmlHead = "";

                switch (_device.DeviceType)
                {
                    case "MR771":
                        nameDevice = "MR771";
                        values = this._currentSetpointsStruct771.GetValues();
                        xmlHead = "MR771_SET_POINTS";
                        break;
                    case "MR761":
                        nameDevice = "MR76X";
                        values = this._currentSetpointsStruct.GetValues();
                        xmlHead = "MR761_SET_POINTS";
                        break;
                }

                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement(nameDevice));
                XmlElement element = doc.CreateElement(xmlHead);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);
                doc.Save(binFileName);

                if (this._count == 0)
                {
                    this._messageBoxForm.ShowResultMessage("Сохранение данных завершено успешно");
                }
                else
                {
                    this._messageBoxForm.ShowMessage("Файл конфигурации сохранен");
                }
            }
            catch{}
        }

        private void StartRead()
        {
            switch (this._device.DeviceType)
            {
                case "MR771":
                    this._configuration771.LoadStruct();
                    break;
                case "MR761":
                    this._configuration.LoadStruct();
                    break;
            }
        }

        #endregion [Configuration Methods]


        #region [Programming Methods]

        private void LogicReadOfDevice(byte[] readBytes, string mess)
        {
            this._count--;

            if (readBytes != null && readBytes.Length != 0 && mess == "Операция успешно выполнена")
            {
                this.LoadProjectFromBin(this.Uncompress(readBytes));
            }
            this._fileDriver.CloseAll(this.OnClosedFiles);
        }
        private byte[] Uncompress(byte[] readBytes)
        {
            ushort[] readData = Common.TOWORDS(readBytes, false);
            ZIPCompressor compr = new ZIPCompressor();
            byte[] compressed = new byte[readData[0]];
            for (int i = 3; i < (compressed.Length + 1) / 2 + 3; i++)
            {
                if ((i - 2) * 2 + 1 < compressed.Length)
                {
                    compressed[(i - 3) * 2 + 1] = (byte)(readData[i] >> 8);
                }
                compressed[(i - 3) * 2] = (byte)readData[i];
            }
            return compr.Decompress(compressed);
        }

        private void LoadProjectFromBin(byte[] uncompressed)
        {
            //Создаем файл логики, чтобы с ним работать
            try
            {
                using (FileStream fs = new FileStream($"{this.folderBrowserDialog.SelectedPath}\\logic.xml",
                    FileMode.Create, FileAccess.Write,
                    FileShare.None, uncompressed.Length,
                    false))
                {
                    fs.Write(uncompressed, 0, uncompressed.Length);
                    fs.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load($"{this.folderBrowserDialog.SelectedPath}\\logic.xml");

                XmlNodeList aNodes = doc.SelectNodes("//Source/SchematicData");
                
                if (aNodes != null)
                {
                    foreach (XmlNode aNode in aNodes)
                    {
                        XmlAttribute device = doc.CreateAttribute("device");
                        device.Value = this._device.DeviceType;
                        aNode.Attributes?.Append(device);
                    }
                }

                doc.Save($"{this.folderBrowserDialog.SelectedPath}\\Проект логики {this._device.DeviceType} версия {this._device.DeviceVersion}.prj");

                //Удаляем файл т.к. он больше не нужен
                File.Delete($"{this.folderBrowserDialog.SelectedPath}\\logic.xml");

                if (this._count == 0)
                {
                    this._messageBoxForm.ShowResultMessage($"Сохранение данных завершено успешно");
                }
                else
                {
                    this._messageBoxForm.ShowMessage("Файл логики сохранен");
                }
            }
            catch (Exception e)
            {
            }
        }

        #endregion [Programming Methods]


        #region [Osc Methods]

        private void OscFailRead(string messageParam)
        {
            this._oscCB.Enabled = false;
            this._messageBoxForm.ShowDialog();
            this._messageBoxForm.ShowResultMessage($"Ошибка чтения {messageParam}");
        }


        /// <summary>
        /// Осцилограмма успешно загружена из устройства
        /// </summary>
        private void OscReadOk()
        {
            this._messageBoxForm.ShowMessage("Сохранение осциллограмм");
            Thread.Sleep(300);
            this._count--;
            this._countOscForRead++;

            int group = this._journalStruct.GroupIndex;
            try
            {
                this.CountingList = new CountingList(this._device, this._pageLoader.ResultArray, this._journalStruct, this._currentOptionsLoaderOsc._connections[group].Value, this._oscopeStruct.Value.Rows);
                Directory.CreateDirectory($"{this.folderBrowserDialog.SelectedPath}\\Осцилограммы");
                string date = this._oscJournalLoader.OscRecords[this._countOscForRead - 1].GetDate;
                string time = this._oscJournalLoader.OscRecords[this._countOscForRead - 1].GetTime.Replace(":", ".");
                Directory.CreateDirectory($"{this.folderBrowserDialog.SelectedPath}\\Осцилограммы\\Оциллограмма {date} {time}");
                this._countingList.Save($"{this.folderBrowserDialog.SelectedPath}\\Осцилограммы\\Оциллограмма {date} {time}\\Осциллограмма_{this._device.DeviceType}");

                if (this._countOscForRead < Convert.ToInt32(this._oscCountMTB.Text))
                {
                    this._journalStruct = this._oscJournalLoader.OscRecords[this._countOscForRead];
                    this._pageLoader.StartRead(this._journalStruct, this._oscOptions.Value);
                }

                if (this._count == 0)
                {
                    this._messageBoxForm.ShowResultMessage("Сохранение данных завершено успешно");
                }
                else
                {
                    this._messageBoxForm.ShowMessage("Файл осциллограммы сохранен");
                }
            }
            catch(Exception ex)
            {
                this._messageBoxForm.ShowResultMessage("Ошибка сохранения осциллограммы");
            }
        }

        /// <summary>
        /// Данные осц
        /// </summary>
        public CountingList CountingList
        {
            get { return this._countingList; }
            set
            {
                this._countingList = value;
            }
        }

        /// <summary>
        /// Прочитан весь журнал
        /// </summary>
        private void ReadAllRecords()
        {
            this._oscCB.Enabled = this._oscJournalLoader.OscCount != 0;
            this._oscCountMTB.Enabled = this._oscJournalLoader.OscCount != 0;
            this._oscCountMTB.Text = this._oscJournalLoader.OscCount != 0 ? "1" : "";
            this._oscInDeviceLabel.Text = $"В устройстве {this._oscJournalLoader.OscCount} осц.";           
        }

        private void StartReadOsc()
        {
            this._journalStruct = this._oscJournalLoader.OscRecords[this._countOscForRead];
            this._pageLoader.StartRead(this._journalStruct, this._oscOptions.Value);
        }

        #endregion [Osc Methods]


        #region [Main Methods]
        private void OnClosedFiles(bool res, string message)
        {
            if (!res) this._messageBoxForm.ShowMessage(@"Не удалось закрыть открытые файлы в устройстве. " + message);
        }

        public void SaveInfo(IEnumerable<CheckBox> checkBoxs)
        {
            this._alarmJournalData.Clear();
            this._systemJournalData.Clear();
            this._count = 0;
            this._countOscForRead = 0;

            foreach (var item in checkBoxs)
            {
                if (item.Checked && item.Text == "Журнал Системы")
                {
                    this._count++;
                    this.StartReadSJ();
                }

                if (item.Checked && item.Text == "Журнал Аварий")
                {
                    this._count++;
                    this.StartReadOption();
                }

                if (item.Checked && item.Text == "Конфигурация")
                {
                    this._count++;
                    this.StartRead();
                }

                if (item.Checked && item.Text == "Программирование (логика)")
                {
                    this._count++;
                    this._fileDriver.ReadFile(this.LogicReadOfDevice, ARH_NANE);
                }

                if (item.Checked && item.Text == "Осциллограммы")
                {
                    this._count += Convert.ToInt32(this._oscCountMTB.Text);
                    this.StartReadOsc();
                }
            }

            this._messageBoxForm.SetProgressBarStyle(ProgressBarStyle.Marquee);
            this._messageBoxForm.OkBtnVisibility = false;
            this._messageBoxForm.ShowDialog("Идет сохранение данных");
        }

        private void StartReadSJ()
        {
            this._messageBoxForm.ShowMessage("Чтение ЖС");
            this._systemJournalLoader.StartRead();
        }

        private void CheckCB()
        {
            this._checkBoxs = Controls.OfType<CheckBox>();
            this._saveDataButton.Enabled = this._checkBoxs.Any(ch => ch.Checked);
        }

        private void saveData_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this.folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                this.SaveInfo(this._checkBoxs);
            }
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            this.CheckCB();
        }

        private void AllInformationsDeviceForm_Load(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentOptionsLoaderOsc.StartRead(); // читает первую запись
            this.CheckCB();
        }

        private void _oscCountMTB_TextChanged(object sender, EventArgs e)
        {
            this._lastOscLabel.Visible = this._oscCountMTB.Text == "1";

            try
            {
                if (this._oscCountMTB.Text.Length > 2 || this._oscCountMTB.Text.Any(char.IsLetter))
                {
                    this._oscCountMTB.Text = this._oscCountMTB.Text.Remove(this._oscCountMTB.Text.Length - 1);
                    this._oscCountMTB.SelectionStart = this._oscCountMTB.Text.Length;
                }

                if (this._oscCountMTB.Text == "" || this._oscCountMTB.Text == "0")
                {
                    this._oscCountMTB.Text = "1";
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion [Main Methods]


        #region [IFormView]
        public Type FormDevice => typeof(MRUniversal);
        public bool Multishow { get; private set; }
        public Type ClassType => typeof(AllInformationsDeviceForm);
        public bool ForceShow => false;
        public Image NodeImage => Framework.Properties.Resources.information_icon.ToBitmap();
        public string NodeName => "Данные из устройства";
        public INodeView[] ChildNodes => new INodeView[] { };
        public bool Deletable => false;
        #endregion [IFormView]
    }
}
