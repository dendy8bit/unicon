﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.Export;
using BEMN.Forms.TreeView;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Int;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MRUNIVERSAL.Properties;
using BEMN.MRUNIVERSAL.Configuration.Structures;
using BEMN.MRUNIVERSAL.Configuration.Structures.AntiBounce;
using BEMN.MRUNIVERSAL.Configuration.Structures.Commands;
using BEMN.MRUNIVERSAL.Configuration.Structures.ConfigSystem;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Apv;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Avr;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.CheckTn;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Engine;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.External;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.F;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.HFL;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.I;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.I2I1;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.InputSignals;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Istar;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Ls;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Omp;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.P;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Q;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Sihronizm;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.StartArc;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.U;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Z;
using BEMN.MRUNIVERSAL.Configuration.Structures.Goose;
using BEMN.MRUNIVERSAL.Configuration.Structures.MeasuringTransformer;
using BEMN.MRUNIVERSAL.Configuration.Structures.Oscope;
using BEMN.MRUNIVERSAL.Configuration.Structures.RelayInd;
using BEMN.MRUNIVERSAL.Configuration.Structures.RSTriggers;
using BEMN.MRUNIVERSAL.Configuration.Structures.Switch;
using BEMN.MRUNIVERSAL.Configuration.Structures.UROV;
using BEMN.MRUNIVERSAL.Configuration.Structures.Vls;

namespace BEMN.MRUNIVERSAL.Configuration
{
    public partial class ConfigurationForm : Form, IFormView
    {
        #region [Constants]

        private const string READING_CONFIG = "Чтение конфигурации";
        private const string WRITING_CONFIG_CAPTION = "Запись конфигурации";

        private const string READ_OK = "Конфигурация прочитана";
        private const string READ_FAIL = "Конфигурация не может быть прочитана";

        private const string WRITING_CONFIG = "Идет запись конфигурации";
        private const string WRITE_OK = "Конфигурация записана в устройство";
        private const string WRITE_FAIL = "Конфигурация не может быть записана";

        private const string FILE_SAVE_FAIL = "Невозможно сохранить файл";
        private const string FILE_LOAD_FAIL = "Невозможно загрузить файл";

        private const string XML_HEAD = "_SET_POINTS";
        private const string BASE_CONFIG_PATH = "\\MRUNIVERSAL\\{0}_BaseConfig_v{1}_{2}.xml";
        private const string BASE_CONFIG_LOAD_SUCCESSFULLY = "Базовые уставки успешно загружены";
        
        private const string READING_BDLIST = "Идет чтение подписей сигналов";
        private const string WRITING_BDLIST = "Идет запись подписей сигналов";
        private const string LIST_FILE_NAME_FOR_OSC = "oscsign.xml";
        private const string LIST_FILE_NAME = "commonp.xml";

        private string _ipHi2Mem;
        private string _ipHi1Mem;
        private string _ipLo2Mem;
        private string _ipLo1Mem;

        private FileDriver _fileDriver;
        #endregion [Constants]

        #region [Private fields]

        private readonly MRUniversal _device;

        private readonly MemoryEntity<ConfigurationStruct> _configuration;
        private readonly MemoryEntity<ConfigurationStruct771> _configuration771;
        
        private NewDgwValidatior<AllMtzMainStruct, MtzMainStruct> _iValidator;

        private ConfigurationStruct _currentSetpointsStruct;
        private ConfigurationStruct771 _currentSetpointsStruct771;

        private MaskedTextBox[] _primary;
        private MaskedTextBox[] _second;

        private List<TabPage> _lsTabPages;
        private List<TabPage> _vlsTabPages;
        private List<TabControl> _tabControls;

        private ComboboxSelector _groupSetpointSelector;
        private CheckedListBox[] _vlsBoxesGr;
        
        private bool isPasswordError;
        private bool isPasswordInputed;
        private double _version;
        private bool _return;

        private MessageBoxForm _formCheck;
        
        #region [Валидаторы]

        #region Уставки группы

        #region [Защиты]

        /// <summary>
        /// Конфигурация измерительного трансформатора
        /// </summary>
        private StructUnion<MeasureTransStruct> _measureTransUnion;
        // ЛС
        private NewDgwValidatior<InputLogicStruct>[] _inputLogicValidator;
        private List<DataGridView> dataGridsViewLsAND = new List<DataGridView>();
        private List<DataGridView> dataGridsViewLsOR = new List<DataGridView>();
        // ВЛС
        private NewCheckedListBoxValidator<OutputLogicStruct>[] _vlsValidator;
        private StructUnion<AllOutputLogicSignalStruct> _vlsUnion;
        private List<CheckedListBox> allVlsCheckedListBoxs = new List<CheckedListBox>();
       
        private StructUnion<GroupSetpoint> _setpointUnion;
        private StructUnion<GroupSetpoint771> _setpointUnion771;
        private SetpointsValidator<AllGroupSetpointStruct, GroupSetpoint> _setpointsValidator;
        private SetpointsValidator<AllGroupSetpointStruct771, GroupSetpoint771> _setpointsValidator771;

        private StructUnion<DefensesSetpointsStruct> _defensesUnion;
        private StructUnion<DefensesSetpointsStruct771> _defensesUnion771;

        // Выключатель
        private NewStructValidator<SwitchStruct> _switchValidator;
        // Входные сигналы
        private NewStructValidator<InputSignalStruct> _inputSignalsValidator;
        private StructUnion<OscopeStruct> _oscopeUnion;
        private StructUnion<AutomaticsParametersStruct> _automaticsParametersUnion;
        private NewStructValidator<ConfigIpStruct> _ethernetValidator;
        private NewStructValidator<ConfigAddStruct> _configAddValidator;
        private NewStructValidator<UrovStruct> _urovValidator;
        private SetpointsValidator<GooseConfig, Goose> _allGooseSetpointValidator;
        private DgvValidatorWithDepend<AllAntiBounce, AntiBounce> _antiBounceValidatior;
        private DgvValidatorWithDepend<AllComands, Command> _commandsValidator;
        #endregion Уставки

        #endregion

        private StructUnion<ConfigurationStruct> _configurationValidator;
        private StructUnion<ConfigurationStruct771> _configurationValidator771;
        #endregion [Валидаторы]

        #endregion [Private fields]

        #region [Ctor's]

        public ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public ConfigurationForm(MRUniversal device)
        {
            this._device = device;

            this._version = Common.VersionConverter(this._device.DeviceVersion);
            Strings.CurrentVersion = this._version;

            _fileDriver = new FileDriver(_device, this);
            this._formCheck = new MessageBoxForm();

            AllReleOutputStruct.SetDeviceRelaysType(device.Info.Plant);
            InputLogicStruct.SetDeviceDiscretsType(device.Info.Plant);
            AllAntiBounce.SetDeviceAntibounceType(device.Info.Plant);
            OscopeAllChannelsStruct.SetDeviceChannelsType(device.Info.Plant);

            this.InitializeComponent();
            
            //SetupToolTipForTabControls();
            InitDefaultList();
            InitSignaturesControl();
            
            switch (_device.DeviceType)
            {
                case "MR771":
                    this._configuration771 = device.Configuration771;

                    this._configuration771.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                    this._configuration771.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationWriteOk);

                    this._configuration771.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                    this._configuration771.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);

                    this._configuration771.ReadFail += HandlerHelper.CreateHandler(this, () =>
                    {
                        this._configuration771.RemoveStructQueries();
                        this.ConfigurationReadFail();
                    });
                    this._configuration771.WriteFail += HandlerHelper.CreateHandler(this, () =>
                    {
                        this._configuration771.RemoveStructQueries();
                        this.ConfigurationWriteFail();
                    });
                    this._currentSetpointsStruct771 = new ConfigurationStruct771();

                    this._progressBar.Maximum = this._configuration771.Slots.Count;
                    break;
                default:
                    this._configuration = device.Configuration;

                    this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                    this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationWriteOk);

                    this._configuration.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                    this._configuration.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);

                    this._configuration.ReadFail += HandlerHelper.CreateHandler(this, () =>
                    {
                        this._configuration.RemoveStructQueries();
                        this.ConfigurationReadFail();
                    });
                    this._configuration.WriteFail += HandlerHelper.CreateHandler(this, () =>
                    {
                        this._configuration.RemoveStructQueries();
                        this.ConfigurationWriteFail();
                    });
                    this._currentSetpointsStruct = new ConfigurationStruct();

                    this._progressBar.Maximum = this._configuration.Slots.Count;
                    break;
            }

            this.HideElement();
            
            this._copySetpoinsGroupComboBox.DataSource = Strings.CopyGroupsNames;
            this._groupSetpointSelector = new ComboboxSelector(this._setpointsComboBox, Strings.GroupsNames);
            this._groupSetpointSelector.OnRefreshInfoTable += OnRefreshInfoTable;

            this.Init();

            AddLsAndVls();

            switch (_device.DeviceType)
            {
                case "MR771":
                    this._setpointsValidator771 = new SetpointsValidator<AllGroupSetpointStruct771, GroupSetpoint771>
                        (this._groupSetpointSelector, this._setpointUnion771);

                    this._configurationValidator771 = new StructUnion<ConfigurationStruct771>
                    (
                        this._setpointsValidator771,
                        _switchValidator,
                        _inputSignalsValidator,
                        _oscopeUnion,
                        _automaticsParametersUnion,
                        _ethernetValidator,
                        _configAddValidator,
                        _urovValidator,
                        _allGooseSetpointValidator,
                        _vlsUnion,
                        _antiBounceValidatior,
                        _commandsValidator
                    );
                    break;
                default:
                    this._setpointsValidator = new SetpointsValidator<AllGroupSetpointStruct, GroupSetpoint>
                        (this._groupSetpointSelector, this._setpointUnion);

                    this._configurationValidator = new StructUnion<ConfigurationStruct>
                    (
                        this._setpointsValidator,
                        _switchValidator,
                        _inputSignalsValidator,
                        _oscopeUnion,
                        _automaticsParametersUnion,
                        _ethernetValidator,
                        _configAddValidator,
                        _urovValidator,
                        _allGooseSetpointValidator,
                        _vlsUnion,
                        _antiBounceValidatior,
                        _commandsValidator
                    );
                    break;
            }
        }

        private void AddLsAndVls()
        {
            // ЛС
            dataGridsViewLsAND.Add(this._lsAndDgv1Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv2Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv3Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv4Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv5Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv6Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv7Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv8Gr1);

            dataGridsViewLsOR.Add(this._lsOrDgv1Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv2Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv3Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv4Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv5Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv6Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv7Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv8Gr1);

            //ВЛС
            allVlsCheckedListBoxs.Add(this.VLSclb1Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb2Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb3Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb4Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb5Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb6Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb7Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb8Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb9Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb10Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb11Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb12Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb13Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb14Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb15Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb16Gr1);
        }

        private void Init()
        {
            this.resistanceDefTabCtr1.Initialization(_device);
            this.resistanceDefTabCtr1.IsPrimary = false;

            if (_device.Info.VoltagesCount == 4)
            {
                _kthn1Label.Visible = false;
                _KTHX1_BoxGr1.Visible = false;
                _kthn1XLabel.Visible = false;
                _KTHX1koef_comboGr1.Visible = false;
                _faultTHn1Label.Visible = false;
                _errorX1_comboGr1.Visible = false;
            }

            this._second = new[] { this._xud1s, this._xud2s, this._xud3s, this._xud4s, this._xud5s };
            this._primary = new[] { this._xud1p, this._xud2p, this._xud3p, this._xud4p, this._xud5p };

            bool visibility = Common.VersionConverter(this._device.DeviceVersion) >= 1.05;

            this._difensesFBDataGrid.Columns[2].Visible = visibility;
            this._difensesFBDataGrid.Columns[4].Visible = visibility;

            this._difensesFMDataGrid.Columns[2].Visible = visibility;
            this._difensesFMDataGrid.Columns[4].Visible = visibility;
            #region Уставки

            #region [Защиты]

            //углы
            NewStructValidator<CornerStruct> cornerValidator = new NewStructValidator<CornerStruct>
                (
                this._toolTip,
                new ControlInfoText(this._iCornerGr1, RulesContainer.UshortTo360),
                new ControlInfoText(this._i0CornerGr1, RulesContainer.UshortTo360),
                new ControlInfoText(this._inCornerGr1, RulesContainer.UshortTo360),
                new ControlInfoText(this._i2CornerGr1, RulesContainer.UshortTo360)
                );

            if (_device.DeviceType == "MR771")
            {
                // I
                _iValidator = new NewDgwValidatior<AllMtzMainStruct, MtzMainStruct>
                   (
                   new[] { this._difensesI1DataGrid, this._difensesI3DataGrid },
                   new[] { 6, 1 },
                   this._toolTip,
                   new ColumnInfoCombo(Strings.NamesI, ColumnsType.NAME),
                   new ColumnInfoCombo(Strings.Mode),
                   new ColumnInfoText(RulesContainer.Ustavka40),
                   new ColumnInfoCombo(Strings.TypeI, ColumnsType.COMBO, false, false),
                   new ColumnInfoText(RulesContainer.Ustavka256, true, false), //4
                   new ColumnInfoCheck(true, false),
                   new ColumnInfoCombo(Strings.BlockTn),
                   new ColumnInfoCombo(Strings.Direction, ColumnsType.COMBO, true, false),
                   new ColumnInfoCombo(Strings.Undir, ColumnsType.COMBO, true, false),
                   new ColumnInfoCombo(Strings.Logic),
                   new ColumnInfoCombo(Strings.Characteristic, ColumnsType.COMBO, true, false),
                   new ColumnInfoText(RulesContainer.TimeRule),
                   new ColumnInfoText(RulesContainer.Ushort100To4000, true, false),
                   new ColumnInfoCombo(Strings.RelaySignals, true, false),
                   new ColumnInfoText(RulesContainer.TimeRule, true, false),
                   new ColumnInfoCombo(Strings.SwitchSignals),
                   new ColumnInfoText(RulesContainer.Ustavka100, true, false),
                   new ColumnInfoCheck(true, false),
                   new ColumnInfoCheck(true, false),
                   new ColumnInfoCheck(),
                   new ColumnInfoCombo(Strings.OscModes),
                   new ColumnInfoCheck(),
                   new ColumnInfoCombo(Strings.Apv),
                   new ColumnInfoCombo(Strings.Apv)
                   )
                {
                    TurnOff = new[]
                   {
                    new TurnOffDgv
                        (
                        this._difensesI1DataGrid,
                        new TurnOffRule(1, Strings.Mode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)
                        ),
                    new TurnOffDgv
                        (
                        this._difensesI3DataGrid,
                        new TurnOffRule(1, Strings.Mode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)
                        )
                }
                };
            }

            else
            {
                // I
                _iValidator = new NewDgwValidatior<AllMtzMainStruct, MtzMainStruct>
                   (
                   new[] { this._difensesI1DataGrid, this._difensesI2DataGrid, this._difensesI3DataGrid },
                   new[] { 4, 2, 1 },
                   this._toolTip,
                   new ColumnInfoCombo(Strings.NamesI, ColumnsType.NAME),
                   new ColumnInfoCombo(Strings.Mode),
                   new ColumnInfoText(RulesContainer.Ustavka40),
                   new ColumnInfoCombo(Strings.TypeI, ColumnsType.COMBO, false, true, false),
                   new ColumnInfoText(RulesContainer.Ustavka256, true, true, false), //4
                   new ColumnInfoCheck(true, true, false),
                   new ColumnInfoCombo(Strings.BlockTn),
                   new ColumnInfoCombo(Strings.Direction, ColumnsType.COMBO, true, true, false),
                   new ColumnInfoCombo(Strings.Undir, ColumnsType.COMBO, true, true, false),
                   new ColumnInfoCombo(Strings.Logic),
                   new ColumnInfoCombo(Strings.Characteristic, ColumnsType.COMBO, true, true, false),
                   new ColumnInfoText(RulesContainer.TimeRule),
                   new ColumnInfoText(RulesContainer.Ushort100To4000, true, true, false),
                   new ColumnInfoCombo(Strings.RelaySignals, true, true, false),
                   new ColumnInfoText(RulesContainer.TimeRule, true, true, false),
                   new ColumnInfoCombo(Strings.SwitchSignals),
                   new ColumnInfoText(RulesContainer.Ustavka100, true, true, false),
                   new ColumnInfoCheck(true, true, false),
                   new ColumnInfoCheck(true, true, false),
                   new ColumnInfoCheck(),
                   new ColumnInfoCombo(Strings.OscModes),
                   new ColumnInfoCheck(),
                   new ColumnInfoCombo(Strings.Apv),
                   new ColumnInfoCombo(Strings.Apv)
                   )
                {
                    TurnOff = new[]
                   {
                    new TurnOffDgv
                        (
                        this._difensesI1DataGrid,
                        new TurnOffRule(1, Strings.Mode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)
                        ),
                    new TurnOffDgv
                        (
                        this._difensesI2DataGrid,
                        new TurnOffRule(1, Strings.Mode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)
                        ),
                    new TurnOffDgv
                        (
                        this._difensesI3DataGrid,
                        new TurnOffRule(1, Strings.Mode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)
                        )
                }
                };
            }


            //I*
            NewDgwValidatior<AllDefenseStarStruct, DefenseStarStruct> iStarValidator = new NewDgwValidatior<AllDefenseStarStruct, DefenseStarStruct>
                (
                this._difensesI0DataGrid,
                AllDefenseStarStruct.DEF_COUNT,
                this._toolTip,
                new ColumnInfoCombo(Strings.NamesIStar, ColumnsType.NAME),
                new ColumnInfoCombo(Strings.Mode),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(RulesContainer.Ustavka256),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(Strings.Direction),
                new ColumnInfoCombo(Strings.Undir),
                new ColumnInfoCombo(Strings.I),
                new ColumnInfoCombo(Strings.Characteristic),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ushort100To4000),
                new ColumnInfoCombo(Strings.SwitchSignals),
                new ColumnInfoCombo(Strings.OscModes),
                new ColumnInfoCombo(Strings.RelaySignals),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(Strings.Apv),
                new ColumnInfoCombo(Strings.Apv)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesI0DataGrid,
                        new TurnOffRule(1, Strings.Mode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18)
                        )
                }
            };

            NewStructValidator<DefenseI2I1Struct> i2I1Validator = new NewStructValidator<DefenseI2I1Struct>
            (
                this._toolTip,
                new ControlInfoCombo(this.I2I1ModeCombo, Strings.Mode),
                new ControlInfoCombo(this.I2I1BlockingCombo, Strings.SwitchSignals),
                new ControlInfoText(this.I2I1TB, RulesContainer.Ustavka100),
                new ControlInfoText(this.I2I1tcpTB, RulesContainer.TimeRule),
                new ControlInfoCombo(this.I2I1OscCombo, Strings.OscModes),
                new ControlInfoCheck(this._urovI2I1CheckBox),
                new ControlInfoCombo(this.i2i1APV1, Strings.Apv),
                new ControlInfoCombo(this.i2i1AVR, Strings.Apv),
                new ControlInfoCombo(this._discretIn3Cmb, Strings.SwitchSignals)
            );

            NewStructValidator<StartArcProt> startArcProtValidator = new NewStructValidator<StartArcProt>(
                this._toolTip,
                new ControlInfoCombo(this.StartArcModeComboBox, Strings.DefenseModesDug),
                new ControlInfoCombo(this.StartBlockArcComboBox, Strings.SwitchSignals),
                new ControlInfoText(this.IcpArcTextBox, RulesContainer.Ustavka40),
                new ControlInfoCheck(this.OscArc)
            );

            NewDgwValidatior<AllDefenceUStruct, DefenceUStruct> uValidator = new NewDgwValidatior<AllDefenceUStruct, DefenceUStruct>
               (
               new[] { this._difensesUBDataGrid, this._difensesUMDataGrid },
               new[] { 4, 4 },
               this._toolTip,
               new ColumnInfoCombo(Strings.UStages, ColumnsType.NAME), //0
               new ColumnInfoCombo(Strings.Mode),
               new ColumnInfoCombo(Strings.UmaxDefenseMode, ColumnsType.COMBO, true, false), //1
               new ColumnInfoCombo(Strings.UminDefenseMode, ColumnsType.COMBO, false, true), //2
               new ColumnInfoText(RulesContainer.Ustavka256), //3
               new ColumnInfoText(RulesContainer.TimeRule), //4
               new ColumnInfoText(RulesContainer.TimeRule), //5
               new ColumnInfoText(RulesContainer.Ustavka256), //6
               new ColumnInfoCheck(), //7
               new ColumnInfoCheck(false, true), //8
               new ColumnInfoCombo(Strings.BlockTn, ColumnsType.COMBO, false, true),
               new ColumnInfoCombo(Strings.SwitchSignals), //9
               new ColumnInfoCombo(Strings.OscModes),
               new ColumnInfoCheck(),
               new ColumnInfoCheck(),
               new ColumnInfoCombo(Strings.Apv),
               new ColumnInfoCheck(),
               new ColumnInfoCombo(Strings.Apv)
               )
            {
                TurnOff = new[]
               {
                    new TurnOffDgv
                        (
                        this._difensesUBDataGrid,
                        new TurnOffRule(1, Strings.Mode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17)
                        ),
                    new TurnOffDgv
                        (
                        this._difensesUMDataGrid,
                        new TurnOffRule(1, Strings.Mode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17)
                        )
                }
            };

            Func<IValidatingRule> fBDefFunc = () =>
            {
                if (!this._difensesFBDataGrid.Columns[2].Visible)
                {
                    return RulesContainer.Ustavka40To60;
                }
                try
                {
                    DataGridViewCell cell = this._difensesFBDataGrid.Tag as DataGridViewCell;
                    if (cell == null ||
                        this._difensesFBDataGrid[2, cell.RowIndex].Value.ToString() == Strings.FreqDefType[0])
                        return RulesContainer.Ustavka40To60;
                    return new CustomDoubleRule(0.05, 10);
                }
                catch
                {
                    return RulesContainer.Ustavka40To60;
                }
            };

            //F>
            NewDgwValidatior<AllDefenseFStruct, DefenseFStruct> fBValidator = new NewDgwValidatior<AllDefenseFStruct, DefenseFStruct>
                (
                this._difensesFBDataGrid, 4, this._toolTip,
                new ColumnInfoCombo(Strings.FBStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(Strings.Mode), //1
                new ColumnInfoCombo(Strings.FreqDefType), //2
                new ColumnInfoTextDependent(fBDefFunc), //3
                new ColumnInfoText(RulesContainer.Ustavka256), //4
                new ColumnInfoText(RulesContainer.TimeRule), //5
                new ColumnInfoText(RulesContainer.TimeRule), //6
                new ColumnInfoText(RulesContainer.Ustavka40To60), //7
                new ColumnInfoCheck(), //8
                new ColumnInfoCombo(Strings.SwitchSignals), //9
                new ColumnInfoCombo(Strings.OscModes), //10
                new ColumnInfoCheck(), //11
                new ColumnInfoCheck(), //12
                new ColumnInfoCombo(Strings.Apv),//13
                new ColumnInfoCheck(),  //15
                new ColumnInfoCombo(Strings.Apv)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesFBDataGrid,
                        new TurnOffRule(1, Strings.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
                        new TurnOffRule(2, Strings.FreqDefType[0], false, 4)
                        )
                }
            };

            Func<IValidatingRule> fMDefFunc = () =>
            {
                if (!this._difensesFMDataGrid.Columns[2].Visible)
                {
                    return RulesContainer.Ustavka40To60;
                }
                try
                {
                    DataGridViewCell cell = this._difensesFMDataGrid.Tag as DataGridViewCell;
                    if (cell == null || this._difensesFMDataGrid[2, cell.RowIndex].Value.ToString() == Strings.FreqDefType[0])
                        return RulesContainer.Ustavka40To60;
                    return new CustomDoubleRule(0.05, 10);
                }
                catch
                {
                    return RulesContainer.Ustavka40To60;
                }
            };
            //F<
            NewDgwValidatior<AllDefenseFStruct, DefenseFStruct> fMValidator = new NewDgwValidatior<AllDefenseFStruct, DefenseFStruct>
                (
                this._difensesFMDataGrid, 4, this._toolTip,
                new ColumnInfoCombo(Strings.FMStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(Strings.Mode), //1
                new ColumnInfoCombo(Strings.FreqDefType), //2
                new ColumnInfoTextDependent(fMDefFunc), //3
                new ColumnInfoText(RulesContainer.Ustavka256), //4
                new ColumnInfoText(RulesContainer.TimeRule), //5
                new ColumnInfoText(RulesContainer.TimeRule), //6
                new ColumnInfoText(RulesContainer.Ustavka40To60), //7
                new ColumnInfoCheck(), //8
                new ColumnInfoCombo(Strings.SwitchSignals), //9
                new ColumnInfoCombo(Strings.OscModes), //10
                new ColumnInfoCheck(), //11
                new ColumnInfoCheck(), //12
                new ColumnInfoCombo(Strings.Apv),//13
                new ColumnInfoCheck(),  //14
                new ColumnInfoCombo(Strings.Apv)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesFMDataGrid,
                        new TurnOffRule(1, Strings.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
                        new TurnOffRule(2, Strings.FreqDefType[0], false, 4)
                        )
                }
            };

            NewDgwValidatior<AllDefenseQStruct, DefenseQStruct> qValidator = new NewDgwValidatior<AllDefenseQStruct, DefenseQStruct>(
                this._engineDefensesGrid,
                2,
                this._toolTip,
                new ColumnInfoCombo(Strings.QStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(Strings.Mode),
                new ColumnInfoText(RulesContainer.Ustavka256), //2
                new ColumnInfoCombo(Strings.SwitchSignals), //3
                new ColumnInfoCombo(Strings.OscModes),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(Strings.Apv),
                new ColumnInfoCombo(Strings.Apv)
            )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                    (
                        this._engineDefensesGrid,
                        new TurnOffRule(1, Strings.Mode[0], true, 2, 3, 4, 5, 6, 7)
                    )
                }
            };

            NewDgwValidatior<AllDefenseExternalStruct, DefenseExternalStruct> externalValidator = new NewDgwValidatior<AllDefenseExternalStruct, DefenseExternalStruct>
                (
                this._externalDefenses,
                16,
                this._toolTip,
                new ColumnInfoCombo(Strings.ExternalStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(Strings.Mode),
                new ColumnInfoCombo(Strings.ExternalDafenseSrab),
                new ColumnInfoText(RulesContainer.TimeRule), //3
                new ColumnInfoText(RulesContainer.TimeRule), //4
                new ColumnInfoCombo(Strings.ExternalDafenseSrab),//5
                new ColumnInfoCheck(), //6
                new ColumnInfoCombo(Strings.ExternalDafenseSrab), //7
                new ColumnInfoCombo(Strings.OscModes),//8
                new ColumnInfoCheck(),//9
                new ColumnInfoCheck(),
                new ColumnInfoCombo(Strings.Apv),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(Strings.Apv)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._externalDefenses,
                        new TurnOffRule(1, Strings.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)
                        )
                }
            };

            NewStructValidator<DefenseTermBlockStruct> termBlockValidator = new NewStructValidator<DefenseTermBlockStruct>
            (
                this._toolTip,
                new ControlInfoCombo(this._engineQmode, Strings.OffOn),
                new ControlInfoText(this._engineQconstraint, RulesContainer.Ustavka256),
                new ControlInfoText(this._engineQtime, RulesContainer.UshortRule),
                new ControlInfoText(this._numHot, new CustomUshortRule(0, 10)),
                new ControlInfoText(this._numCold, new CustomUshortRule(0, 10)),
                new ControlInfoText(this._waitNumBlock, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._cosfP, new CustomDoubleRule(0, 0.99)),
                new ControlInfoText(this._kpdP, RulesContainer.UshortTo100)
            );

            NewDgwValidatior<AllDefenseReversPower, ReversePowerStruct> reversPowerValidator = new NewDgwValidatior
                <AllDefenseReversPower, ReversePowerStruct>
                (
                    this._reversePowerGrid,
                    2,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.StageDefType, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.Mode),
                    new ColumnInfoText(new SignValidatingRule(-2.50, 2.50)),
                    new ColumnInfoText(RulesContainer.UshortFazaRule),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoText(new SignValidatingRule(-2.50, 2.50)),
                    new ColumnInfoCheck(),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoCombo(Strings.SwitchSignals),
                    new ColumnInfoCombo(Strings.BlockTn),
                    new ColumnInfoCombo(Strings.OscModes),
                    new ColumnInfoCombo(Strings.OffOn),
                    new ColumnInfoCombo(Strings.OffOn),
                    new ColumnInfoCombo(Strings.Apv),
                    new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._reversePowerGrid,
                            new TurnOffRule(1, Strings.Mode[0], true,
                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
                        )
                    }
            };

            // АВР
            NewStructValidator<AvrStruct> avrValidator = new NewStructValidator<AvrStruct>
            (
                this._toolTip,
                new ControlInfoCombo(this._avrBySignal, Strings.BeNo),
                new ControlInfoCombo(this._avrByOff, Strings.BeNo),
                new ControlInfoCombo(this._avrBySelfOff, Strings.BeNo),
                new ControlInfoCombo(this._avrByDiff, Strings.BeNo),
                new ControlInfoCombo(this._avrSIGNOn, Strings.SwitchSignals),
                new ControlInfoCombo(this._avrBlocking, Strings.SwitchSignals),
                new ControlInfoCombo(this._avrBlockClear, Strings.SwitchSignals),
                new ControlInfoCombo(this._avrResolve, Strings.SwitchSignals),
                new ControlInfoCombo(this._avrBack, Strings.SwitchSignals),
                new ControlInfoText(this._avrTSr, RulesContainer.TimeRule),
                new ControlInfoText(this._avrTBack, RulesContainer.TimeRule),
                new ControlInfoText(this._avrTOff, RulesContainer.TimeRule),
                new ControlInfoCombo(this._avrClear, Strings.ForbiddenAllowed),
                new ControlInfoCombo(this._avrTypeComboBox, Strings.TypeAVR),
                new ControlInfoCombo(this._avrRecModeComboBox, Strings.ModesLight),
                new ControlInfoCheck(this._avrRecBlockSelfCheck),
                new ControlInfoCombo(this._avrRecBlockSDTUCB, Strings.SwitchSignals),
                new ControlInfoCombo(this._avrRecKeyComboBox, Strings.SwitchSignals),
                new ControlInfoCombo(this._avrRecBanComboBox, Strings.SwitchSignals),
                new ControlInfoText(this._tBlockRecMTB, RulesContainer.TimeRule),
                new ControlInfoText(this._avrReadyRecMTB, RulesContainer.TimeRule),
                new ControlInfoCombo(this._avrRecInputU1CB, Strings.InputU),
                new ControlInfoCombo(this._avrRecInputU2CB, Strings.InputU),
                new ControlInfoText(this._avrRecUmaxMTB, RulesContainer.Ustavka256),
                new ControlInfoText(this._avrRecUminMTB, RulesContainer.Ustavka256)
            );

            switch (_device.DeviceType)
            {
                case "MR771":
                    _defensesUnion771 = new StructUnion<DefensesSetpointsStruct771>
                    (
                        cornerValidator,
                        _iValidator,
                        iStarValidator,
                        i2I1Validator,
                        startArcProtValidator,
                        uValidator,
                        fBValidator,
                        fMValidator,
                        qValidator,
                        termBlockValidator,
                        externalValidator,
                        this.resistanceDefTabCtr1.ResistanceUnion771
                    );
                    break;
                default:
                    _defensesUnion = new StructUnion<DefensesSetpointsStruct>
                    (
                        cornerValidator,
                        _iValidator,
                        iStarValidator,
                        i2I1Validator,
                        startArcProtValidator,
                        uValidator,
                        fBValidator,
                        fMValidator,
                        qValidator,
                        termBlockValidator,
                        externalValidator,
                        this.resistanceDefTabCtr1.ResistanceUnion,
                        reversPowerValidator,
                        avrValidator
                    );
                    break;
            }

            #endregion [защиты]

            // Контроль цепей ТН
            NewStructValidator<CheckTnStruct> chechTnValidator = new NewStructValidator<CheckTnStruct>(
                this._toolTip,
                new ControlInfoCombo(this._errorL_comboGr1, Strings.SwitchSignals),
                new ControlInfoCombo(this._errorX_comboGr1, Strings.SwitchSignals),
                new ControlInfoCheck(this._i2u2CheckGr1),
                new ControlInfoText(this._u2ContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._i2ContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoCheck(this._i0u0CheckGr1),
                new ControlInfoText(this._u0ContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._i0ContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._uMinContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._uMaxContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._iMinContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._iMaxContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._iDelContrCepGr1, RulesContainer.Ustavka100),
                new ControlInfoText(this._uDelContrCepGr1, RulesContainer.Ustavka100),
                new ControlInfoText(this._tdContrCepGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._tsContrCepGr1, RulesContainer.TimeRule),
                new ControlInfoCombo(this._resetTnGr1, Strings.SwitchSignals),
                new ControlInfoCheck(this.checkBox1)
                );
            //АПВ
            NewStructValidator<ApvStruct> apvValidator = new NewStructValidator<ApvStruct>(
                this._toolTip,
                new ControlInfoCombo(this._apvModeGr1, Strings.ApvModes),
                new ControlInfoCheck(this._blokFromUrov),
                new ControlInfoCombo(this._disableApv, Strings.SwitchSignals),
                new ControlInfoText(this._timeDisable, RulesContainer.TimeRule),
                new ControlInfoCombo(this._typeDisable, Strings.DisableType),
                new ControlInfoCombo(this._apvBlockGr1, Strings.SwitchSignals),
                new ControlInfoText(this._apvTblockGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvTreadyGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvKrat1Gr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvKrat2Gr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvKrat3Gr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvKrat4Gr1, RulesContainer.TimeRule),
                new ControlInfoCombo(this._apvSwitchOffGr1, Strings.BeNo)
                );

            //Тепловая модель
            NewStructValidator<TermConfigStruct> termConfValidator = new NewStructValidator<TermConfigStruct>(
                this._toolTip,
                new ControlInfoText(this._engineHeatingTimeGr1, RulesContainer.UshortTo65534),
                new ControlInfoText(this._engineCoolingTimeGr1, RulesContainer.UshortTo65534),
                new ControlInfoText(this._engineIdv, RulesContainer.Ustavka40),
                new ControlInfoText(this._iStartBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._tStartBox, new CustomIntRule(0, 3276700)),
                new ControlInfoText(this._qBox, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._engineQresetGr1, Strings.SwitchSignals),
                new ControlInfoCombo(this._engineNreset, Strings.SwitchSignals),
                new ControlInfoText(this._tCount, RulesContainer.UshortTo65534),
                new ControlInfoText(this._ppd, new CustomDoubleRule(0, 128)),
                new ControlInfoCombo(this._ppdCombo, Strings.Pkoef)
                );

            // Измерительный трансформатор
            NewStructValidator<KanalITransStruct> iTransValidator = new NewStructValidator<KanalITransStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._TT_typeComboGr1, Strings.TtType),
                new ControlInfoCombo(this._alternationCB, Strings.Rotation),
                new ControlInfoCombo(this._inpIn, Strings.InpIn),
                new ControlInfoText(this._Im_BoxGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._ITTL_BoxGr1, RulesContainer.UshortRule),
                new ControlInfoCombo(this._polarIa, Strings.Polar),
                new ControlInfoCombo(this._polarIb, Strings.Polar),
                new ControlInfoCombo(this._polarIc, Strings.Polar),
                new ControlInfoCombo(this._polarIn, Strings.Polar),
                new ControlInfoText(this._ITTX_BoxGr1, RulesContainer.UshortRule),
                new ControlInfoCombo(this._polarIn1, Strings.Polar)
                );

            NewStructValidator<KanalUTransStruct> uTransValidator = new NewStructValidator<KanalUTransStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._Uo_typeComboGr1, Strings.UoType),
                new ControlInfoText(this._KTHL_BoxGr1, RulesContainer.Ustavka128),
                new ControlInfoText(this._KTHX_BoxGr1, RulesContainer.Ustavka128),
                new ControlInfoText(this._KTHX1_BoxGr1, RulesContainer.Ustavka128),
                new ControlInfoCombo(this._KTHLkoef_comboGr1, Strings.KthKoefs),
                new ControlInfoCombo(this._KTHXkoef_comboGr1, Strings.KthKoefs),
                new ControlInfoCombo(this._KTHX1koef_comboGr1, Strings.KthKoefs),
                new ControlInfoCombo(this._errorX1_comboGr1, Strings.SwitchSignals),
                new ControlInfoCombo(_inputUcaComboBox, Strings.InputBinding),
                new ControlInfoCombo(_ucaComboBox, Strings.InputBindingUca)
                );
            this._measureTransUnion = new StructUnion<MeasureTransStruct>(iTransValidator, uTransValidator);
            
            DataGridView[] lsBoxes =
            {
                this._lsAndDgv1Gr1, this._lsAndDgv2Gr1, this._lsAndDgv3Gr1, this._lsAndDgv4Gr1,
                this._lsAndDgv5Gr1, this._lsAndDgv6Gr1, this._lsAndDgv7Gr1, this._lsAndDgv8Gr1,
                this._lsOrDgv1Gr1, this._lsOrDgv2Gr1, this._lsOrDgv3Gr1, this._lsOrDgv4Gr1,
                this._lsOrDgv5Gr1, this._lsOrDgv6Gr1, this._lsOrDgv7Gr1, this._lsOrDgv8Gr1
            };
            foreach (DataGridView gridView in lsBoxes)
            {
                gridView.CellBeginEdit += this.GridOnCellBeginEdit;
            }
            this._inputLogicValidator = new NewDgwValidatior<InputLogicStruct>[AllInputLogicStruct.LOGIC_COUNT];
            for (int i = 0; i < AllInputLogicStruct.LOGIC_COUNT; i++)
            {
                this._inputLogicValidator[i] = new NewDgwValidatior<InputLogicStruct>
                    (
                    lsBoxes[i],
                    InputLogicStruct.DiscrestCount,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.LsSignals, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.LsState)
                    );
            }
            StructUnion<AllInputLogicStruct> inputLogicUnion = new StructUnion<AllInputLogicStruct>(this._inputLogicValidator);

            // ВЛС
            _vlsBoxesGr = new[]
            {
                this.VLSclb1Gr1, this.VLSclb2Gr1, this.VLSclb3Gr1, this.VLSclb4Gr1, this.VLSclb5Gr1, this.VLSclb6Gr1,
                this.VLSclb7Gr1, this.VLSclb8Gr1,this.VLSclb9Gr1, this.VLSclb10Gr1, this.VLSclb11Gr1, this.VLSclb12Gr1,
                this.VLSclb13Gr1, this.VLSclb14Gr1, this.VLSclb15Gr1, this.VLSclb16Gr1
            };
            this._vlsValidator = new NewCheckedListBoxValidator<OutputLogicStruct>[AllOutputLogicSignalStruct.LOGIC_COUNT];
            for (ushort i = 0; i < AllOutputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                this._vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicStruct>(_vlsBoxesGr[i], Strings.VlsSignals);
            }
            this._vlsUnion = new StructUnion<AllOutputLogicSignalStruct>(this._vlsValidator);

            // Синхронизм
            NewStructValidator<SinhronizmAddStruct> manualSinhronizmValidator = new NewStructValidator<SinhronizmAddStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._sinhrManualModeGr1, Strings.OffOn),
                new ControlInfoText(this._sinhrManualUmaxGr1, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._sinhrManualNoYesGr1, Strings.NoYesDiscret),
                new ControlInfoCombo(this._sinhrManualYesNoGr1, Strings.NoYesDiscret),
                new ControlInfoCombo(this._sinhrManualNoNoGr1, Strings.NoYesDiscret),
                new ControlInfoText(this._sinhrManualdFGr1, new CustomDoubleRule(0, 0.5)),
                new ControlInfoText(this._sinhrManualdFiGr1, new CustomUshortRule(0, 100)),
                new ControlInfoText(this._sinhrManualdFnoGr1, new CustomDoubleRule(0, 0.5)),
                new ControlInfoCheck(this._waitSinchrManual),
                new ControlInfoCheck(this._catchSinchrManual),
                new ControlInfoCheck(this._blockTNmanual)
                );

            NewStructValidator<SinhronizmAddStruct> autoSinhronizmValidator = new NewStructValidator<SinhronizmAddStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._sinhrAutoModeGr1, Strings.OffOn),
                new ControlInfoText(this._sinhrAutoUmaxGr1, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._sinhrAutoNoYesGr1, Strings.NoYesDiscret),
                new ControlInfoCombo(this._sinhrAutoYesNoGr1, Strings.NoYesDiscret),
                new ControlInfoCombo(this._sinhrAutoNoNoGr1, Strings.NoYesDiscret),
                new ControlInfoText(this._sinhrAutodFGr1, new CustomDoubleRule(0, 0.5)),
                new ControlInfoText(this._sinhrAutodFiGr1, new CustomUshortRule(0, 100)),
                new ControlInfoText(this._sinhrAutodFnoGr1, new CustomDoubleRule(0, 0.5)),
                new ControlInfoCheck(this._waitSinchrAuto),
                new ControlInfoCheck(this._catchSinchrAuto),
                new ControlInfoCheck(this._blockTNauto)
                );

            NewStructValidator<SinhronizmStruct> sinhronizmValidator = new NewStructValidator<SinhronizmStruct>
                (
                this._toolTip,
                new ControlInfoText(this._sinhrUminOtsGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrUminNalGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrUmaxNalGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrTwaitGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._sinhrTsinhrGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._sinhrTonGr1, RulesContainer.UshortTo600),
                new ControlInfoCombo(this._sinhrU1Gr1, Strings.Usinhr),
                new ControlInfoCombo(this._sinhrU2Gr1, Strings.Usinhr),
                new ControlInfoValidator(manualSinhronizmValidator),
                new ControlInfoValidator(autoSinhronizmValidator),
                new ControlInfoCombo(this._blockSinhCmb, Strings.SwitchSignals),
                new ControlInfoCombo(this._discretIn1Cmb, Strings.SwitchSignals),
                new ControlInfoCombo(this._discretIn2Cmb, Strings.SwitchSignals),
                new ControlInfoText(this._sinhrKamp, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrF, new CustomUshortRule(0, 360))
                );

            // ОМП
            NewStructValidator<ConfigurationOpmStruct> ompValidator = new NewStructValidator<ConfigurationOpmStruct>(
                this._toolTip,
                new ControlInfoCombo(this._OMPmode_comboGr1, Strings.OmpModes),
                new ControlInfoText(this._xud1s, new DoubleToComaRule(0, 2, 4)),
                new ControlInfoText(this._xud2s, new DoubleToComaRule(0, 2, 4)),
                new ControlInfoText(this._xud3s, new DoubleToComaRule(0, 2, 4)),
                new ControlInfoText(this._xud4s, new DoubleToComaRule(0, 2, 4)),
                new ControlInfoText(this._xud5s, new DoubleToComaRule(0, 2, 4)),
                new ControlInfoText(this._l1, RulesContainer.Ustavka256),
                new ControlInfoText(this._l2, RulesContainer.Ustavka256),
                new ControlInfoText(this._l3, RulesContainer.Ustavka256),
                new ControlInfoText(this._l4, RulesContainer.Ustavka256)
            );

            NewStructValidator<DzStruct> hfl1Validator = new NewStructValidator<DzStruct>(
                this._toolTip,
                new ControlInfoCombo(this.modeDZ, Strings.HflModes),
                new ControlInfoCheck(this.konturFF),
                new ControlInfoCombo(this.abbrevDZFF, Strings.HflSignalsDZ),
                new ControlInfoCombo(this.extendDZFF, Strings.HflSignalsDZ),
                new ControlInfoCombo(this.reverseDZFF, Strings.HflSignalsDZ),
                new ControlInfoCheck(this.konturFN),
                new ControlInfoCombo(this.abbrevDZFN, Strings.HflSignalsDZ),
                new ControlInfoCombo(this.extendDZFN, Strings.HflSignalsDZ),
                new ControlInfoCombo(this.reverseDZFN, Strings.HflSignalsDZ),
                new ControlInfoCombo(this.inpTSDZ, Strings.SwitchSignals),
                new ControlInfoText(this.tvzTSDZ, RulesContainer.TimeRule),
                new ControlInfoCombo(this.deblockDZ, Strings.HflDeblock),
                new ControlInfoCombo(this.controlDZ, Strings.SwitchSignals),
                new ControlInfoText(this.tsrabDebDZ, RulesContainer.TimeRule),
                new ControlInfoText(this.tminImpDZ, RulesContainer.TimeRule),
                new ControlInfoText(this.toffDZ, RulesContainer.TimeRule),
                new ControlInfoText(this.toffTBDZ, RulesContainer.TimeRule),
                new ControlInfoCombo(this.blockSwithDZ, Strings.SwitchSignals),
                new ControlInfoCombo(this.blockTSDZ, Strings.SwitchSignals),
                new ControlInfoCombo(this.OscDZ, Strings.Apv),
                new ControlInfoCombo(this.urovDZ, Strings.OffOn),
                new ControlInfoCombo(this.apvDZ, Strings.Apv),
                new ControlInfoCombo(this.modeEchoDZ, Strings.HflEcho),
                new ControlInfoText(this.tsrabTSDZ, RulesContainer.TimeRule),
                new ControlInfoText(this.uminDZ, RulesContainer.Ustavka256),
                new ControlInfoCombo(this.blockEchoDZ, Strings.SwitchSignals),
                new ControlInfoCombo(this.blockTnDZ, Strings.BlockTn),
                new ControlInfoCombo(this.modeReverseDZ, Strings.OffOn),
                new ControlInfoText(this.treverseDZ, RulesContainer.TimeRule),
                new ControlInfoText(this.tficsDZ, RulesContainer.TimeRule),
                new ControlInfoCheck(this.zoneDZ),
                new ControlInfoCombo(this.blockReverseDZ, Strings.SwitchSignals)
                );

            NewStructValidator<TznpStruct> hfl2Validator = new NewStructValidator<TznpStruct>(
                this._toolTip,
                new ControlInfoCombo(this.modeDZNP, Strings.HflModes),
                new ControlInfoCombo(this.abbrevDZNP, Strings.HflSignalsTZNP),
                new ControlInfoCombo(this.extendDZNP, Strings.HflSignalsTZNP),
                new ControlInfoCombo(this.reverseDZNP, Strings.HflSignalsTZNP),
                new ControlInfoCombo(this.inpTSDZNP, Strings.SwitchSignals),
                new ControlInfoText(this.tvzTSDZNP, RulesContainer.TimeRule),
                new ControlInfoCombo(this.deblockDZNP, Strings.HflDeblock),
                new ControlInfoCombo(this.controlDZNP, Strings.SwitchSignals),
                new ControlInfoText(this.tsrabDebDZNP, RulesContainer.TimeRule),
                new ControlInfoText(this.tminImpDZNP, RulesContainer.TimeRule),
                new ControlInfoText(this.toffDZNP, RulesContainer.TimeRule),
                new ControlInfoText(this.toffTBDZNP, RulesContainer.TimeRule),
                new ControlInfoCombo(this.blockSwitchDZNP, Strings.SwitchSignals),
                new ControlInfoCombo(this.blockTSDZNP, Strings.SwitchSignals),
                new ControlInfoCombo(this.directionDZNP, Strings.HflDirection),
                new ControlInfoCombo(this.oscDZNP, Strings.Apv),
                new ControlInfoCombo(this.urovDZNP, Strings.OffOn),
                new ControlInfoCombo(this.apvDZNP, Strings.Apv),
                new ControlInfoCombo(this.modeEchoDZNP, Strings.HflEcho),
                new ControlInfoText(this.tsrabTSDZNP, RulesContainer.TimeRule),
                new ControlInfoText(this.uminDZNP, RulesContainer.Ustavka256),
                new ControlInfoCombo(this.blockEchoDZNP, Strings.SwitchSignals),
                new ControlInfoCombo(this.blockTnDZNP, Strings.BlockTn),
                new ControlInfoCombo(this.modeReverseDZNP, Strings.OffOn),
                new ControlInfoText(this.treverseDZNP, RulesContainer.TimeRule),
                new ControlInfoText(this.tficsDZNP, RulesContainer.TimeRule),
                new ControlInfoCheck(this.zoneDZNP),
                new ControlInfoCombo(this.blockReverseDZNP, Strings.SwitchSignals)
                );
            switch (_device.DeviceType)
            {
                case "MR771":
                    StructUnion<ConfigResistDiagramStruct771> resistConfigUnion771 = new StructUnion<ConfigResistDiagramStruct771>
                    (
                        this.resistanceDefTabCtr1.ResistValidator,
                        this.resistanceDefTabCtr1.LoadValidator,
                        this.resistanceDefTabCtr1.SwingValidator,
                        this.resistanceDefTabCtr1.ResistanceUnion771,
                        this._measureTransUnion
                    );
                    this.resistanceDefTabCtr1.ResistConfigUnion771 = resistConfigUnion771;
                    break;
                default:
                    StructUnion<ConfigResistDiagramStruct> resistConfigUnion = new StructUnion<ConfigResistDiagramStruct>
                    (
                        this.resistanceDefTabCtr1.ResistValidator,
                        this.resistanceDefTabCtr1.LoadValidator,
                        this.resistanceDefTabCtr1.SwingValidator,
                        this.resistanceDefTabCtr1.ResistanceUnion,
                        this._measureTransUnion
                    );
                    this.resistanceDefTabCtr1.ResistConfigUnion = resistConfigUnion;
                    break;
            }
            switch (_device.DeviceType)
            {
                case "MR771":
                    this._setpointUnion771 = new StructUnion<GroupSetpoint771>(
                        _defensesUnion771,
                        this.resistanceDefTabCtr1.ResistValidator,
                        this.resistanceDefTabCtr1.LoadValidator,
                        chechTnValidator,
                        this.resistanceDefTabCtr1.SwingValidator,
                        apvValidator,
                        termConfValidator,
                        this._measureTransUnion,
                        inputLogicUnion,
                        sinhronizmValidator,
                        ompValidator,
                        hfl1Validator,
                        hfl2Validator
                    );
                    break;
                default:
                    this._setpointUnion = new StructUnion<GroupSetpoint>(
                        _defensesUnion,
                        this.resistanceDefTabCtr1.ResistValidator,
                        this.resistanceDefTabCtr1.LoadValidator,
                        chechTnValidator,
                        this.resistanceDefTabCtr1.SwingValidator,
                        apvValidator,
                        termConfValidator,
                        this._measureTransUnion,
                        inputLogicUnion,
                        sinhronizmValidator,
                        ompValidator
                    //hfl1Validator,
                    //hfl2Validator
                    );
                    break;
            }
            
            #endregion Уставки
            // Выключатель
            _switchValidator = new NewStructValidator<SwitchStruct>
                (
                this._toolTip,
                   new ControlInfoCombo(this._switchOff, Strings.SwitchSignals),
                   new ControlInfoCombo(this._switchOn, Strings.SwitchSignals),
                   new ControlInfoCombo(this._switchError, Strings.SwitchSignals),
                   new ControlInfoCombo(this._switchBlock, Strings.SwitchSignals),
                   new ControlInfoText(this._switchImp, RulesContainer.TimeRule),
                   new ControlInfoText(this._switchTUskor, RulesContainer.TimeRule),
                   new ControlInfoCombo(this._switchKontCep, Strings.OffOn),
                   new ControlInfoCombo(this._controlSolenoidCombo, Strings.SwitchSignals),
                   new ControlInfoCombo(this._switchKeyOn, Strings.SwitchSignals),
                   new ControlInfoCombo(this._switchKeyOff, Strings.SwitchSignals),
                   new ControlInfoCombo(this._switchVneshOn, Strings.SwitchSignals),
                   new ControlInfoCombo(this._switchVneshOff, Strings.SwitchSignals),
                   new ControlInfoCombo(this._switchButtons, Strings.ForbiddenAllowed),
                   new ControlInfoCombo(this._switchKey, Strings.ControlForbidden),
                   new ControlInfoCombo(this._switchVnesh, Strings.ControlForbidden),
                   new ControlInfoCombo(this._switchSDTU, Strings.ForbiddenAllowed),
                   new ControlInfoCombo(this._comandOtkl, Strings.ImpDlit),
                   new ControlInfoCombo(this._blockSDTU, Strings.SwitchSignals)
                   );
            // Входные сигналы
            _inputSignalsValidator = new NewStructValidator<InputSignalStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._grUst1ComboBox, Strings.SwitchSignals),
                new ControlInfoCombo(this._grUst2ComboBox, Strings.SwitchSignals),
                new ControlInfoCombo(this._grUst3ComboBox, Strings.SwitchSignals),
                new ControlInfoCombo(this._grUst4ComboBox, Strings.SwitchSignals),
                new ControlInfoCombo(this._grUst5ComboBox, Strings.SwitchSignals),
                new ControlInfoCombo(this._grUst6ComboBox, Strings.SwitchSignals),
                new ControlInfoCombo(this._indComboBox, Strings.SwitchSignals)
                );

            #region [Осц]
            NewStructValidator<OscopeConfigStruct> oscopeConfigValidator = new NewStructValidator<OscopeConfigStruct>
                     (
                     this._toolTip,
                     new ControlInfoText(this._oscWriteLength, RulesContainer.UshortTo100),
                     new ControlInfoCombo(this._oscFix, Strings.OscFixation),
                     new ControlInfoCombo(this._oscLength, Strings.OscCount)
                     );

            Func<string, Dictionary<ushort, string>> func = str =>
            {
                if (string.IsNullOrEmpty(str)) return Strings.OscChannelSignals[0];
                int index = Strings.OscBases.IndexOf(str);
                return index != -1 ? Strings.OscChannelSignals[index] : Strings.OscChannelSignals[0];
            };

            Func<string, Dictionary<ushort, string>> funcRs= str =>
            {
                if (string.IsNullOrEmpty(str)) return Strings.OscChannelSignals[0];
                int index = Strings.OscBases.IndexOf(str);
                return index != -1 ? Strings.OscChannelSignals[index] : Strings.OscChannelSignals[0];
            };

            DgvValidatorWithDepend<AllRsTriggersStruct, RsTriggersStruct> rsTriggersValidatior = new DgvValidatorWithDepend<AllRsTriggersStruct, RsTriggersStruct>
            (
                this._rsTriggersDataGrid,
                AllRsTriggersStruct.RSTRIGGERS_COUNT_CONST,
                this._toolTip,
                new ColumnInfoCombo(Strings.RsTriggersName, ColumnsType.NAME),
                new ColumnInfoCombo(Strings.RSPriority),
                new ColumnInfoComboControl(Strings.OscBases, 3),
                new ColumnInfoDictionaryComboDependent(funcRs, 2),
                new ColumnInfoComboControl(Strings.OscBases, 5),
                new ColumnInfoDictionaryComboDependent(funcRs, 4)
            );

            DgvValidatorWithDepend<OscopeAllChannelsStruct, ChannelWithBase> channelsValidator = new DgvValidatorWithDepend<OscopeAllChannelsStruct, ChannelWithBase>
                (
                this._oscChannelsGrid,
                OscopeAllChannelsStruct.ChannelsCount,
                this._toolTip,
                new ColumnInfoCombo(Strings.OscChannelNames, ColumnsType.NAME),
                new ColumnInfoComboControl(Strings.OscBases, 2),
                new ColumnInfoDictionaryComboDependent(func, 1)
                );
            this._oscChannelsGrid.CellBeginEdit += this.GridOnCellBeginEdit;
            NewStructValidator<ChannelStruct> startOscChannelValidator = new NewStructValidator<ChannelStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this.oscStartCb, Strings.RelaySignals)
                );

            _oscopeUnion = new StructUnion<OscopeStruct>(oscopeConfigValidator, startOscChannelValidator, channelsValidator);

            #endregion [Осц]

            #region [Реле и Индикаторы]

            DgvValidatorWithDepend<AllReleOutputStruct, ReleOutputStruct> relayValidator =
                new DgvValidatorWithDepend<AllReleOutputStruct, ReleOutputStruct>
                (
                    new[] { this._outputReleGrid, this._virtualReleDataGrid },
                    new[] { AllReleOutputStruct.CurrentCount - 2, AllReleOutputStruct.RELAY_COUNT_CONST - AllReleOutputStruct.CurrentCount },
                    this._toolTip,
                    new ColumnInfoCombo(Strings.RelayNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.SignalType),
                    new ColumnInfoComboControl(Strings.OscBases, 3),
                    new ColumnInfoDictionaryComboDependent(func, 2),
                    new ColumnInfoText(RulesContainer.TimeRule)
                );

            this._outputReleGrid.CellBeginEdit += this.GridOnCellBeginEdit;
            this._virtualReleDataGrid.CellBeginEdit += this.GridOnCellBeginEdit;

            DgvValidatorWithDepend<AllIndicatorsStruct, IndicatorsStruct> indicatorValidator =
                new DgvValidatorWithDepend<AllIndicatorsStruct, IndicatorsStruct>
                (
                    this._outputIndicatorsGrid,
                    AllIndicatorsStruct.INDICATORS_COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.IndNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.ReleyType),
                    new ColumnInfoComboControl(Strings.OscBases, 3),
                    new ColumnInfoDictionaryComboDependent(func, 2),
                    new ColumnInfoComboControl(Strings.OscBases, 5),
                    new ColumnInfoDictionaryComboDependent(func, 4),
                    new ColumnInfoCombo(Strings.ModeRele)
                );
            this._outputIndicatorsGrid.CellBeginEdit += this.GridOnCellBeginEdit;
            NewStructValidator<FaultStruct> faultValidator = new NewStructValidator<FaultStruct>
                (
                this._toolTip,
                new ControlInfoCheck(this._fault1CheckBox),
                new ControlInfoCheck(this._fault2CheckBox),
                new ControlInfoCheck(this._fault3CheckBox),
                new ControlInfoCheck(this._fault4CheckBox),
                new ControlInfoCheck(this._fault5CheckBox),
                new ControlInfoCheck(this._fault6CheckBox),
                new ControlInfoCheck(this._fault7CheckBox),
                new ControlInfoText(this._impTB, RulesContainer.TimeRule)
                );

            _automaticsParametersUnion = new StructUnion<AutomaticsParametersStruct>
                (relayValidator, rsTriggersValidatior, indicatorValidator, faultValidator);

            #endregion [Реле и Индикаторы]

            _configAddValidator = new NewStructValidator<ConfigAddStruct>
                (this._toolTip,
                new ControlInfoCombo(this._inpAddCombo, Strings.InpOporSignals),
                new ControlInfoCheck(this._resetSystemCheckBox),
                new ControlInfoCheck(this._resetAlarmCheckBox),
                new ControlInfoCheck(this._fixErrorFCheckBox)
                );

            _urovValidator = new NewStructValidator<UrovStruct>
                (
                this._toolTip,
                new ControlInfoCheck(this._urovIcheck),
                new ControlInfoCheck(this._urovBkCheck),
                new ControlInfoCheck(this._urovSelf),
                new ControlInfoText(this._tUrov1, RulesContainer.TimeRule),
                new ControlInfoText(this._tUrov2, RulesContainer.TimeRule),
                new ControlInfoText(this._Iurov, RulesContainer.Ustavka40),
                new ControlInfoCombo(this._urovPusk, Strings.SwitchSignals),
                new ControlInfoCombo(this._urovBlock, Strings.SwitchSignals)
                );

            ComboBox[] bgs =
                {
                    this.goin1,this.goin2,this.goin3,this.goin4,this.goin5,this.goin6,this.goin7,this.goin8,this.goin9,this.goin10,
                    this.goin11,this.goin12,this.goin13,this.goin14,this.goin15,this.goin16,this.goin17,this.goin18,this.goin19,this.goin20,
                    this.goin21,this.goin22,this.goin23,this.goin24,this.goin25,this.goin26,this.goin27,this.goin28,this.goin29,this.goin30,
                    this.goin31,this.goin32,this.goin33,this.goin34,this.goin35,this.goin36,this.goin37,this.goin38,this.goin39,this.goin40,
                    this.goin41,this.goin42,this.goin43,this.goin44,this.goin45,this.goin46,this.goin47,this.goin48,this.goin49,this.goin50,
                    this.goin51,this.goin52,this.goin53,this.goin54,this.goin55,this.goin56,this.goin57,this.goin58,this.goin59,this.goin60,
                    this.goin61,this.goin62,this.goin63,this.goin64
                };

            _ethernetValidator = new NewStructValidator<ConfigIpStruct>(
                this._toolTip,
                new ControlInfoText(this._ipLo1, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipLo2, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipHi1, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipHi2, new CustomUshortRule(0, 255)),
                new ControlInfoCombo(this._reserveCB, Strings.Reserve));

            NewStructValidator<Goose> gooseValidator = new NewStructValidator<Goose>
                (
                this._toolTip,
                new ControlInfoCombo(this.operationBGS, Strings.GooseConfig),
                new ControlInfoMultiCombo(bgs, Strings.GooseSignal)
                );

            _allGooseSetpointValidator = new SetpointsValidator<GooseConfig, Goose>
                    (new ComboboxSelector(this.currentBGS, Strings.GooseNames), gooseValidator);

            _antiBounceValidatior = new DgvValidatorWithDepend<AllAntiBounce, AntiBounce>
                (
                    this._antiBounceDataGridView,
                    AllAntiBounce.AntiBounceCount,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.LsSignals, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.ModeAntiBounce),
                    new ColumnInfoCombo(Strings.CountAntiBounce)
                )
                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._antiBounceDataGridView,
                            new TurnOffRule(1, Strings.ModeAntiBounce[0], true, 2)
                        )
                    }
                };

                _commandsValidator = new DgvValidatorWithDepend<AllComands, Command>
                (
                    this._commandsDataGridView,
                    24,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.CountCommands, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.ImpDlit),
                    new ColumnInfoCheck(),
                    new ColumnInfoCheck()
                    );
            
            Func<IValidatingRule> currentFuncOmpPrimary = () =>
            {
                double koef = this.GetKoeff();
                return new DoubleToComaRule(0, 2 * koef, 4);
            };

            //нужен только для инициализации валидаторов текстбоксов
            NewStructValidator<OneWordStruct> _primaryResistValidator = new NewStructValidator<OneWordStruct>
                (
                this._toolTip,
                new ControlInfoTextDependent(this._xud1p, currentFuncOmpPrimary),
                new ControlInfoTextDependent(this._xud2p, currentFuncOmpPrimary),
                new ControlInfoTextDependent(this._xud3p, currentFuncOmpPrimary),
                new ControlInfoTextDependent(this._xud4p, currentFuncOmpPrimary),
                new ControlInfoTextDependent(this._xud5p, currentFuncOmpPrimary)
                );
        }

        private static void InitDefaultList()
        {
            Strings.RelaySignals = Strings.DefaultRelaySignals;
            Strings.SwitchSignals = Strings.DefaultSwitchSignals;
            Strings.VlsSignals = Strings.DefaultVLSSignals;
            Strings.ExternalDafenseSrab = Strings.DefaultExternalDafenseSignals;
            Strings.LsSignals = Strings.DefaultLsSignals;
            Strings.HflSignalsDZ = Strings.DefaultHflSignalsDZ;
            Strings.HflSignalsTZNP = Strings.DefaultHflSignalsTZNP;
            Strings.SignaturesForOsc = Strings.DefaultSignaturesForOsc;
        }

        private void InitSignaturesControl()
        {
            signaturesSignalsConfigControl.SignalsList = Strings.DefaultSignaturesSignals.Values.ToList();
            signaturesSignalsConfigControl.DefaultList = Strings.DefaultSignaturesSignals.Values.ToList();
            signaturesSignalsConfigControl.DiskretsCount = _device.Info.DiskretsCount - 2;
            signaturesSignalsConfigControl.AcceptedSignals += UpdateElementsInForm;
            signaturesOsc.CopySignalsAction += CopySignalsInOscDataGrid;

            signaturesOsc.FillDefaultList(InputLogicStruct.DiscrestCount, OscopeAllChannelsStruct.ChannelsCount);
        }

        private void CopySignalsInOscDataGrid()
        {
            var diskretsList = AddSignalsInListFromDataGridOscBase(out var oscList);

            Strings.OscList = new List<string>();
            Strings.OscList.AddRange(oscList);

            signaturesOsc.CopySignalsInDataGrid(diskretsList, oscList);
        }

        private List<string> AddSignalsInListFromDataGridOscBase(out List<string> oscList)
        {
            List<string> diskretsList = new List<string>();
            oscList = new List<string>();

            diskretsList.AddRange(Strings.VlsSignals.Values.ToList().GetRange(0, _device.Info.DiskretsCount - 2));

            for (int i = 0; i < _oscChannelsGrid.RowCount; i++)
            {
                if (_oscChannelsGrid.Rows[i].Cells[2].Value.ToString() != "НЕТ")
                {
                    oscList.Add(_oscChannelsGrid.Rows[i].Cells[2].Value.ToString());
                }
                else
                {
                    oscList.Add("");
                }
            }
            return diskretsList;
        }

        private void SetupToolTipForTabControls()
        {
            _lsTabPages = new List<TabPage>
            {
                tabPage39,
                tabPage40,
                tabPage41,
                tabPage42,
                tabPage43,
                tabPage44,
                tabPage45,
                tabPage46,
                tabPage31,
                tabPage32,
                tabPage33,
                tabPage34,
                tabPage35,
                tabPage36,
                tabPage37,
                tabPage38,
            };

            _vlsTabPages = new List<TabPage>
            {
                VLS1,
                VLS2,
                VLS3,
                VLS4,
                VLS5,
                VLS6,
                VLS7,
                VLS8,
                VLS9,
                VLS10,
                VLS11,
                VLS12,
                VLS13,
                VLS14,
                VLS15,
                VLS16,
            };

            _tabControls = new List<TabControl>
            {
                _lsFirsTabControl,
                _lsSecondTabControl,
                _vlsTabControl
            };

            for (int i = 0; i < _tabControls.Count; i++)
            {
                _tabControls[i].ShowToolTips = true;
            }
        }

        public void UpdateStatus(string status, bool cursorWait = false, bool isFormLoad = false)
        {
            this.Invoke(new Action(() =>
            {
                this._statusLabel.Text = status;
                //update the statusStrip
                this.statusStrip1.Update();
                if (cursorWait)
                    this.Cursor = Cursors.WaitCursor;
                else
                    this.Cursor = Cursors.Default;
            }));
        }

        private void UpdateElementsInForm()
        {
            UpdateStatus("Применение подписей сигналов", true);

            RestoreSetpoints();

            UpdateStatus("Применение подписей сигналов успешно завершено");
        }

        private void RestoreSetpoints()
        {
            ushort[] configValues;
            byte[] value;

            switch (_device.DeviceType)
            {
                case "MR771":
                    this._currentSetpointsStruct771 = this._configurationValidator771.Get();
                    configValues = this._currentSetpointsStruct771.GetValues();
                    value = Common.TOBYTES(configValues, false);
                    break;
                default:
                    configValues = this._currentSetpointsStruct.GetValues();
                    value = Common.TOBYTES(configValues, false);
                    break;
            }

            UpdateListsInStringsConfig();

            Init();

            switch (_device.DeviceType)
            {
                case "MR771":
                    this._currentSetpointsStruct771.InitStruct(value);
                    break;
                default:
                    this._currentSetpointsStruct.InitStruct(value);
                    break;
            }

            ShowConfiguration();
        }

        private void UpdateListsInStringsConfig()
        {
            try
            {
                signaturesSignalsConfigControl.SetToolTipForTabPages(_lsTabPages,
                    Strings.DefaultSignaturesSignals.Values.ToList().FindIndex(item => item.Contains("ЛС1")));
                signaturesSignalsConfigControl.SetToolTipForTabPages(_vlsTabPages,
                    Strings.DefaultSignaturesSignals.Values.ToList().FindIndex(item => item.Contains("ВЛС1")));

                signaturesSignalsConfigControl.AddedItemInList(null, Strings.RelaySignals, true);
                //Strings.RelaySignalsSetup(Strings.RelaySignals);

                signaturesSignalsConfigControl.AddedItemInListInvSignals("Z 1 ИО", "", null, Strings.SwitchSignals, true);
                //Strings.SwitchSignalsSetup(Strings.SwitchSignals);

                signaturesSignalsConfigControl.AddedItemInListInvSignals("ВНЕШ", "", null, Strings.ExternalDafenseSrab, true);
                //Strings.ExternalDafenseSrabSetup(Strings.ExternalDafenseSrab);

                signaturesSignalsConfigControl.AddedItemInListVLS("ВЛС1", "ВЛС16", "", null, Strings.VlsSignals, true);
                //Strings.VlsSignalsConfig(Strings.VlsSignals);

                signaturesSignalsConfigControl.AddedItemInListVLS("НЕТ", "ССЛ48", "I> 1 ИО", Strings.HflSignalsDZ);
                signaturesSignalsConfigControl.AddedItemInListVLS("НЕТ", "I<", "I2/I1> ИО", Strings.HflSignalsTZNP);
            }
            catch (Exception e) { }

        }

        private void HideElement()
        {
            signaturesSignalsConfigControl.Enabled = Framework.Framework.IsWriteSignatures;
            signaturesOsc.Enabled = Framework.Framework.IsWriteSignatures;

            switch (_device.DeviceType)
            {
                case "MR771":
                    //Токовый вход
                    //_inpIn.Visible = false;
                    //label53.Visible = false;

                    //Паспортные данные
                    passportDataGroup.Visible = false;

                    //Тепловая модель
                    _iStartBox.Visible = false;
                    label48.Visible = false;
                    _tStartBox.Visible = false;
                    label92.Visible = false;
                    _tCount.Visible = false;
                    label135.Visible = false;
                    _qBox.Visible = false;
                    label140.Visible = false;
                    _engineNreset.Visible = false;

                    _idvLabel.Text = "Iдоп, Iн";

                    _inputQResetLabel.Location = new Point(12, 79);
                    _engineQresetGr1.Location = new Point(90, 76);
                    _paramEngineGroupBox.Size = new Size(224, 100);

                    //Вкладки пуск договой и по мощности
                    //_startArcDefence.Visible = false;
                    //_powerDefence.Visible = false;
                    _defencesTabControl.TabPages.Remove(_startArcDefence);
                    _defencesTabControl.TabPages.Remove(_powerDefence);

                    //Удаляем АВР
                    //_setpointsTab.TabPages["_apvAvrTabPage"].Name = "АПВ";
                    _apvAvrTabPage.Text = "АПВ";
                    _avrGroupBox.Visible = false;
                    _apvGroupBox.Location = new Point(3, 3);

                    //Защиты

                    // Уставки->Защиты->Защиты Q->Блокировка по числу пусков (групбокс)
                    groupBox20.Visible = false;

                    _I14GroupBox.Text = "Защиты I> максимального тока";
                    _I14GroupBox.Size = new Size(935, 224);

                    _ILGroupBox.Location = new Point(3, 233);

                    _I56GroupBox.Visible = false;

                    _difensesI1DataGrid.Columns["_avrColumnI14"].Visible = false;
                    _difensesI2DataGrid.Columns["_avrColumnI56"].Visible = false;
                    _difensesI2DataGrid.Columns["_conditionColumnI56"].Visible = false;
                    _difensesI3DataGrid.Columns["_avrColumnIL"].Visible = false;

                    _difensesI0DataGrid.Columns["_avrColumnIStar"].Visible = false;

                    _avrI2I1Label.Visible = false;
                    i2i1AVR.Visible = false;

                    _difensesUBDataGrid.Columns["_avrColumnUM"].Visible = false;
                    _difensesUMDataGrid.Columns["_avrColumnUL"].Visible = false;

                    _difensesFBDataGrid.Columns["_avrColumnFM"].Visible = false;
                    _difensesFMDataGrid.Columns["_avrColumnFL"].Visible = false;

                    _engineDefensesGrid.Columns["_avrColumnEngine"].Visible = false;

                    _externalDefenses.Columns["_avrColumnExt"].Visible = false;

                    //Выходные сигналы - Неисправности
                    _vchsFaultLabel.Visible = true;
                    _fault7CheckBox.Visible = true;
                    break;
                case "MR761":
                    this._setpointsTab.TabPages["_tuAndTbTabPage"].Parent = null;
                    break;
            }

            switch (_device.DevicePlant)
            {
                case "T3...":
                    // Уставки->Параметры измерений->Тока: Полярность In, In1 - visible false; 
                    this.label152.Visible = false;
                    this._polarIn.Visible = false;
                    this.label255.Visible = false;
                    this._polarIn1.Visible = false;
                    break;
                case "T4N4D42R35":
                case "T4N4D74R67":
                case "T4N5D42R35":
                case "T4N5D74R67":
                    // Уставки->Параметры измерений->Тока: Полярность In1 - visible false; 
                    this.label255.Visible = false;
                    this._polarIn1.Visible = false;
                    break;
                default:
                    break;
            }
        }

        
        #endregion [Ctor's]

        #region [MemoryEntity Events Handlers]

        /// <summary>
        /// Конфигурация успешно прочитана
        /// </summary>
        private void ConfigurationReadOk()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = READ_OK;

            switch (_device.DeviceType)
            {
                case "MR771":
                    this._currentSetpointsStruct771 = this._configuration771.Value;
                    break;
                default:
                    this._currentSetpointsStruct = this._configuration.Value;
                    break;
            }

            signaturesSignalsConfigControl.FillSignaturesSignalsDgv(signaturesSignalsConfigControl.SignalsList, Strings.DefaultSignaturesSignals.Values.ToList());
            AddSignalsInListFromDataGridOscBase(out var oscList);

            Strings.OscList = new List<string>();
            Strings.OscList.AddRange(oscList);
            signaturesOsc.FillSignaturesSignalsDgv(signaturesOsc.Messages, InputLogicStruct.DiscrestCount, oscList);
            
            this.ShowConfiguration();

            UpdateStatus(READ_OK);

            _formCheck?.ShowResultMessage(READ_OK);
        }

        /// <summary>
        /// Ошибка чтения конфигурации
        /// </summary>
        private void ConfigurationReadFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = READ_FAIL;

            signaturesSignalsConfigControl.FillSignaturesSignalsDgv(signaturesSignalsConfigControl.SignalsList, Strings.DefaultSignaturesSignals.Values.ToList());
            AddSignalsInListFromDataGridOscBase(out var oscList);

            Strings.OscList = new List<string>();
            Strings.OscList.AddRange(oscList);
            signaturesOsc.FillSignaturesSignalsDgv(signaturesOsc.Messages, InputLogicStruct.DiscrestCount, oscList);

            _formCheck?.ShowResultMessage(READ_FAIL);
        }

        /// <summary>
        /// Конфигурация успешно записана
        /// </summary>
        private void ConfigurationWriteOk()
        {
            if (Framework.Framework.IsWriteSignatures)
            {
                UpdateStatus(WRITING_BDLIST, true);

                _formCheck.ShowMessage(WRITING_BDLIST);

                AddSignalsInListFromDataGridOscBase(out var oscList);

                Strings.OscList = new List<string>();
                Strings.OscList.AddRange(oscList);

                signaturesSignalsConfigControl.PreparationListSignatures();
                WritingListSignatures(signaturesSignalsConfigControl.SignalsList);
            }

            else
            {
                ConfigAndSignaturesWrited();
            }
        }

        private void WritingListSignatures(List<string> list)
        {
            WritingSignaturesProcess(list);

            this._statusLabel.Text = "Идет запись подписей сигналов";
        }

        private void WritingSignaturesProcess(List<string> list)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (XmlTextWriter writer = new XmlTextWriter(stream, Encoding.GetEncoding(1251)))
                {
                    writer.Formatting = Formatting.Indented;
                    signaturesSignalsConfigControl.ListConfiguration(
                        this._device.DeviceType,
                        list,
                        Common.VersionConverter(_device.DeviceVersion)
                    ).XmlToFile(writer);
                    byte[] buff = stream.GetBuffer().Take((int)stream.Length).ToArray();

                    this._fileDriver.WriteFile(this.WriteSignaturesIsCompleted, buff, LIST_FILE_NAME);
                }
            }
        }

        private void WriteSignaturesIsCompleted(bool sucсess, string mes)
        {
            switch (mes)
            {
                case "Неверный пароль":

                    if (DialogResult.OK == MessageBox.Show(
                            "Введен неверный пароль!" + "\nПожалуйста, обратитесь к поставщику производителя.", "Внимание!", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning))
                    {
                        ConfigAndSignaturesWrited();
                    }
                    break;

                case "Операция успешно выполнена":

                    this._statusLabel.Text = "Запись подписей успешно завершена";

                    try
                    {
                        signaturesSignalsConfigControl.ClearDgv();
                        signaturesSignalsConfigControl.FillSignaturesSignalsDgv(signaturesSignalsConfigControl.SignalsList, Strings.DefaultSignaturesSignals.Values.ToList());

                        UpdateListsInStringsConfig();

                        signaturesOsc.PreparationListSignatures();
                        WritingListSignaturesForOsc(signaturesOsc.Messages);

                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Ошибка заполнения таблицы");
                    }
                    break;
                default:
                    ConfigAndSignaturesWrited();
                    break;
            }
        }

        private void WritingListSignaturesForOsc(List<string> list)
        {
            WritingSignaturesProcessForOsc(list);
        }

        private void WritingSignaturesProcessForOsc(List<string> list)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (XmlTextWriter writer = new XmlTextWriter(stream, Encoding.GetEncoding(1251)))
                {
                    writer.Formatting = Formatting.Indented;

                    signaturesOsc.SettingsSignaturesForOsc(
                        this._device.DeviceType,
                        list,
                        Common.VersionConverter(_device.DeviceVersion)
                    ).XmlToFile(writer);

                    byte[] buff = stream.GetBuffer().Take((int)stream.Length).ToArray();

                    this._fileDriver.WriteFile(this.WriteSignaturesForOscIsCompleted, buff, LIST_FILE_NAME_FOR_OSC);
                }
            }
        }

        private void WriteSignaturesForOscIsCompleted(bool sucсess, string mes)
        {
            switch (mes)
            {
                case "Операция успешно выполнена":
                    this._statusLabel.Text = "Запись подписей успешно завершена";

                    RestoreSetpoints();
                    
                    AddSignalsInListFromDataGridOscBase(out var oscList);

                    Strings.OscList = new List<string>();
                    Strings.OscList.AddRange(oscList);

                    signaturesOsc.FillSignaturesSignalsDgv(signaturesOsc.Messages, InputLogicStruct.DiscrestCount, oscList);

                    ConfigAndSignaturesWrited();
                    break;
                default:
                    RestoreSetpoints();

                    ConfigAndSignaturesWrited();
                    break;
            }
        }

        private void ConfigAndSignaturesWrited()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = WRITE_OK;
            this._device.SetBit(this._device.DeviceNumber, 0xd00, true, "Сохранить конфигурацию", this._device);
            
            UpdateStatus(WRITE_OK);
            _formCheck.ShowResultMessage(WRITE_OK);
        }

        //private void ClearVlsAndLs()
        //{
        //    allVlsCheckedListBoxs.Clear();
        //    dataGridsViewLsOR.Clear();
        //    dataGridsViewLsAND.Clear();
        //}

        /// <summary>
        /// Ошибка записи конфигурации
        /// </summary>
        private void ConfigurationWriteFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = WRITE_FAIL;

            UpdateStatus(WRITE_FAIL);
            _formCheck.ShowResultMessage(WRITE_FAIL);
        }

        #endregion [MemoryEntity Events Handlers]

        #region [Help members]

        private void GridOnCellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this.contextMenu.Tag = sender;
        }
        
        /// <summary>
        /// Выводит все данные на экран
        /// </summary>
        private void ShowConfiguration()
        {
            switch (_device.DeviceType)
            {
                case "MR771":
                    this._configurationValidator771.Set(this._currentSetpointsStruct771);
                    break;
                default:
                    this._configurationValidator.Set(this._currentSetpointsStruct);
                    break;
            }
            
            if (this._resistCheck.Checked) this.GetPrimaryResistValue();
            this.resistanceDefTabCtr1.RefreshResistInfoTable();
            
            //ClearVlsAndLs();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
        }

        /// <summary>
        /// Сохранение значений IP
        /// </summary>
        public void GetIpAddress()
        {
            this._ipHi2Mem = this._ipHi2.Text;
            this._ipHi1Mem = this._ipHi1.Text;
            this._ipLo2Mem = this._ipLo2.Text;
            this._ipLo1Mem = this._ipLo1.Text;
        }

        public void SetIpAddress()
        {
            this._ipHi2.Text = this._ipHi2Mem;
            this._ipHi1.Text = this._ipHi1Mem;
            this._ipLo2.Text = this._ipLo2Mem;
            this._ipLo1.Text = this._ipLo1Mem;
        }

        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._resetSetpointsButton.Enabled = !value;
                this._clearSetpointBtn.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }

        /// <summary>
        /// Читает все данные с экрана
        /// </summary>
        private bool WriteConfiguration()
        {
            this.IsProcess = true;
            string message;
            this._statusLabel.Text = "Проверка уставок";
            this.statusStrip1.Update();

            switch (_device.DeviceType)
            {
                case "MR771":
                    if (this._configurationValidator771.Check(out message, true))
                    {
                        if (this._resistCheck.Checked)
                            this.GetSecondResistValue(); //если галочка стоит, то переводим во вторичные единицы
                        this._statusLabel.Text = WRITING_CONFIG;
                        this._currentSetpointsStruct771 = this._configurationValidator771.Get();
                        this._configuration771.Value = this._currentSetpointsStruct771;
                        return true;
                    }
                    break;
                default:
                    if (this._configurationValidator.Check(out message, true))
                    {
                        if (this._resistCheck.Checked)
                            this.GetSecondResistValue(); //если галочка стоит, то переводим во вторичные единицы
                        this._statusLabel.Text = WRITING_CONFIG;
                        this._currentSetpointsStruct = this._configurationValidator.Get();
                        this._configuration.Value = this._currentSetpointsStruct;
                        return true;
                    }
                    break;
            }
            
            MessageBox.Show("Обнаружены неккоректные уставки. Конфигурация не может быть записана.");
            this.IsProcess = false;
            return false;
        }

        /// <summary>
        /// Запуск чтения конфигурации
        /// </summary>
        private void StartRead()
        {
            if (!this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.IsProcess = true;
            this._statusLabel.Text = READING_CONFIG;
            this._progressBar.Value = 0;

            switch (_device.DeviceType)
            {
                case "MR771":
                    this._configuration771.LoadStruct();
                    break;
                default:
                    this._configuration.LoadStruct();
                    break;
            }
        }

        private double GetKoeff()
        {
            double ittf, ktnf, tokIn;
            MeasureTransStruct str = this._measureTransUnion.Get();
            ittf = str.ChannelI.Ittl;
            ktnf = str.ChannelU.KthlValue;
            tokIn = str.ChannelI.InpInValue;
            return ktnf / ittf * tokIn;
        }

        private void GetSecondResistValue()
        {
            this.IsProcess = true;
            double koef = 1 / this.GetKoeff();
            for (int i = 0; i < this._primary.Length; i++)
            {
                this._second[i].Text = string.Format("{0:F4}", Convert.ToDouble(this._primary[i].Text) * koef);
            }
            this.resistanceDefTabCtr1.GetSecondValue(koef);
            this.resistanceDefTabCtr1.RefreshResistInfoTable();
            this.IsProcess = false;
        }

        private void GetPrimaryResistValue()
        {
            this.IsProcess = true;
            double koef = this.GetKoeff();
            for (int i = 0; i < this._second.Length; i++)
            {
                this._primary[i].Text = string.Format("{0:F4}", Convert.ToDouble(this._second[i].Text) * koef);
            }
            this.resistanceDefTabCtr1.GetPrimaryValue(koef);
            this.resistanceDefTabCtr1.RefreshResistInfoTable();
            this.IsProcess = false;
        }

        private bool IsHideShowUca
        {
            set
            {
                _ucaLabel.Visible = !value;
                _ucaComboBox.Visible = !value;
            }
        }
        private void InitFormCheck(string caption, string message)
        {
            _formCheck.SetProgressBarStyle(ProgressBarStyle.Marquee);
            _formCheck.Caption = caption;
            _formCheck.OkBtnVisibility = false;
            _formCheck.ShowDialog(message);
        }

        private void ReadConfigAndSignals()
        {
            UpdateStatus(READING_CONFIG, true);

            IsProcess = true;
            
            _fileDriver.ReadFile(SignaturesListRead, LIST_FILE_NAME);

            InitFormCheck(READING_CONFIG, READING_BDLIST);
        }

        private void SignaturesListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "Операция успешно выполнена")
                {
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            XmlRootAttribute root = new XmlRootAttribute(_device.DeviceType);
                            XmlSerializer serializer = new XmlSerializer(typeof(ListsOfConfigurations), root);
                            ListsOfConfigurations lists = (ListsOfConfigurations)serializer.Deserialize(reader);

                            signaturesSignalsConfigControl.SignalsList = lists.SignalsList.MessagesList;

                            _fileDriver.ReadFile(SignaturesForOscListRead, LIST_FILE_NAME_FOR_OSC);
                        }
                    }
                }
                else if (mes == "Неверный пароль")
                {
                    if (DialogResult.OK ==
                        MessageBox.Show(
                            "Введен неверный пароль!" + "\nПожалуйста, обратитесь к поставщику производителя.", "Внимание!", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning))
                    {
                        throw new Exception();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                signaturesSignalsConfigControl.SignalsList = Strings.DefaultSignaturesSignals.Values.ToList();

                signaturesSignalsConfigControl.AddedItemInList(Strings.RelaySignals.Values.ToList());

                signaturesSignalsConfigControl.FillSignaturesSignalsDgv(signaturesSignalsConfigControl.SignalsList, Strings.DefaultSignaturesSignals.Values.ToList());

                signaturesOsc.Messages = Strings.DefaultSignaturesForOsc;

                _fileDriver.ReadFile(SignaturesForOscListRead, LIST_FILE_NAME_FOR_OSC);
            }
        }

        private void SignaturesForOscListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "Операция успешно выполнена")
                {
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            XmlRootAttribute root = new XmlRootAttribute(_device.DeviceType);
                            XmlSerializer serializer = new XmlSerializer(typeof(SignaturesForOsc), root);
                            SignaturesForOsc lists = (SignaturesForOsc)serializer.Deserialize(reader);

                            signaturesOsc.Messages = lists.Signatures.MessagesList;

                            _formCheck.ShowMessage(READING_CONFIG);

                            StartRead();

                            UpdateListsInStringsConfig();

                            try { Init(); }
                            catch (Exception e) { }
                        }
                    }
                }

                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                this.StartRead();
                
                try { Init(); }
                catch (Exception e) { }
            }
        }

        private void WriteConfig()
        {
            if (!this._device.DeviceDlgInfo.IsConnectionMode) return;

            DialogResult result = MessageBox.Show(
                $"Записать конфигурацию {_device.DeviceType} №{this._device.DeviceNumber}? " +
                $"\nВ устройство будет записан IP-адрес: {this._ipHi2.Text}.{this._ipHi1.Text}.{this._ipLo2.Text}.{this._ipLo1.Text}!",
                "Запись", MessageBoxButtons.YesNo);
            if (result != DialogResult.Yes) return;

            this._progressBar.Value = 0;

            if (!this.WriteConfiguration()) return;

            UpdateStatus(WRITING_CONFIG_CAPTION, true);

            switch (_device.DeviceType)
            {
                case "MR771":
                    this._configuration771.SaveStruct();
                    break;
                default:
                    this._configuration.SaveStruct();
                    break;
            }

            InitFormCheck(WRITING_CONFIG_CAPTION, WRITING_CONFIG);
        }

        private void InitSignaturesStruct()
        {
            signaturesSignalsConfigControl.ListConfiguration(
                this._device.DeviceType,
                Strings.DefaultSignaturesSignals.Values.ToList(),
                Common.VersionConverter(_device.DeviceVersion)
            );
            signaturesOsc.SettingsSignaturesForOsc(
                this._device.DeviceType,
                Strings.DefaultSignaturesForOsc,
                100);
            signaturesSignalsConfigControl.FillSignaturesSignalsDgv(signaturesSignalsConfigControl.SignalsList,
                Strings.DefaultSignaturesSignals.Values.ToList());

            AddSignalsInListFromDataGridOscBase(out var oscList);

            Strings.OscList = new List<string>();
            Strings.OscList.AddRange(oscList);

            signaturesOsc.FillSignaturesSignalsDgv(signaturesOsc.Messages, InputLogicStruct.DiscrestCount, oscList);
        }

        private static void CursorWaiting(bool useWaitCursor)
        {
            Application.UseWaitCursor = useWaitCursor;
            Cursor.Current = useWaitCursor ? Cursors.WaitCursor : Cursors.Default;
        }
        #endregion [Help members]

        #region [Event handlers]

        private void _grid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void _passport_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string var = this._ppdCombo.SelectedItem.ToString();

                double p = double.Parse(this._ppd.Text);
                double cosf = double.Parse(this._cosfP.Text);
                double kpd = double.Parse(this._kpdP.Text);

                double sn = Math.Abs(kpd) > 0 && Math.Abs(cosf) > 0 ? p * 100 / (cosf * kpd) : 0;

                this._sn.Text = $"{sn:F2} {var}";
            }
            catch
            {
                this._sn.Text = "0";
            }
        }

        private void passportDataGroup_Enter(object sender, EventArgs e)
        {

        }

        private void _avrTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            _avrReclouserGroupBox.Visible = _avrTypeComboBox.SelectedIndex == 1;
        }
        
        private void comboBox_selectedIndexChanged(object sender, EventArgs e)
        {
            IsHideShowUca = _inputUcaComboBox.SelectedIndex == 0;
        }

        private void Mr771ConfigurationForm_Load(object sender, EventArgs e)
        {
            if (this._device.IsConnect && _device.DeviceDlgInfo.IsConnectionMode)
            {
                this.ReadConfigAndSignals();
            }
            else
            {
                this.ResetSetpoints(false);
            }
        }

        private void Mr771ConfigurationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._configuration771?.RemoveStructQueries();
            this._configuration?.RemoveStructQueries();
        }

        private void Mr771ConfigurationForm102_Activated(object sender, EventArgs e)
        {
            Strings.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);
        }
        
        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            if (_device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this.ReadConfigAndSignals();
            }
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            if (!this._device.DeviceDlgInfo.IsConnectionMode && !this._device.IsConnect) return;

            this.WriteConfig();
        }
        
        private void _oscLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            if (comboBox == null) return;
            int ind = comboBox.SelectedIndex;

            int oscSize;

            switch (_device.DeviceType)
            {
                case "MR771":
                    oscSize = 50477 * 2;
                    break;
                default:
                    oscSize = 61293 * 2;
                    break;
            }
             

            this._oscSizeTextBox.Text = (oscSize / (ind + 2)).ToString();
        }

        private void _resetSetpointsButton_Click(object sender, EventArgs e)
        {
            this.GetIpAddress();
            this.ResetSetpoints(true);
            this.SetIpAddress();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
        }

        private void _clearSetpointsButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Обнулить уставки?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;

            switch (_device.DeviceType)
            {
                case "MR771":
                    this._configurationValidator771.Reset("MR771");
                    break;
                default:
                    this._configurationValidator.Reset("Universal");
                    break;
            }
            
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
        }

        private void _resistCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (this._return)
            {
                this._return = false;
                return;
            }
            if (this._resistCheck.Checked)
            {
                DialogResult res =
                    MessageBox.Show("Сопротивления будут пересчитаны в первичные величины. Обратите внимание на корректность введенных значений IТТф и KТНф",
                        "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                string mes;

                bool check = this._measureTransUnion.Check(out mes, false)
                             && this.resistanceDefTabCtr1.ResistValidator.Check(out mes, false)
                             && this.resistanceDefTabCtr1.LoadValidator.Check(out mes, false)
                             && this.resistanceDefTabCtr1.SwingValidator.Check(out mes, false);

                if (check)
                {
                    if (res == DialogResult.OK)
                    {
                        this.omp1.Visible = false;
                        this.omp2.Visible = true;
                        this.resistanceDefTabCtr1.IsPrimary = true;
                        this.GetPrimaryResistValue();
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Невозможно произвести пересчет - обнаружены некорректные уставки", "Внимание",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                this._return = true;
                this._resistCheck.Checked = false;
            }
            else
            {
                DialogResult res = MessageBox.Show("Сопротивления будут пересчитаны во вторичные величины к размерности Ом*Iн.вт. Обратите внимание на корректность введенных значений IТТф и KТНф",
                    "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                string mes;

                bool check = this._measureTransUnion.Check(out mes, false)
                             && this.resistanceDefTabCtr1.ResistValidator.Check(out mes, false)
                             && this.resistanceDefTabCtr1.LoadValidator.Check(out mes, false)
                             && this.resistanceDefTabCtr1.SwingValidator.Check(out mes, false);
                if (check)
                {
                    if (res == DialogResult.OK)
                    {
                        this.omp1.Visible = true;
                        this.omp2.Visible = false;
                        this.resistanceDefTabCtr1.IsPrimary = false;
                        this.GetSecondResistValue();
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Невозможно произвести пересчет - обнаружены некорректные уставки", "Внимание",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                this._return = true;
                this._resistCheck.Checked = true;
            }
        }

        private void Mr771ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }
        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ContextMenuStrip menu = (ContextMenuStrip)sender;
            if (menu == null) return;
            DataGridView dgv = menu.Tag as DataGridView;
            dgv?.EndEdit();
            menu.Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.ReadConfigAndSignals();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }
            if (e.ClickedItem == this.resetSetpointsItem)
            {
                this.GetIpAddress();
                this.ResetSetpoints(true);
                this.SetIpAddress();

                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
                TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
                TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                DialogResult res = MessageBox.Show("Обнулить уставки?", "Внимание", MessageBoxButtons.YesNo);
                if (res != DialogResult.Yes) return;

                switch (_device.DeviceType)
                {
                    case "MR771":
                        this._configurationValidator771.Reset();
                        break;
                    default:
                        this._configurationValidator.Reset();
                        break;
                }
                
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
                TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
                TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtml();
            }
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.contextMenu.Items.Clear();
            this.contextMenu.Items.AddRange(new ToolStripItem[]
            {
                this.readFromDeviceItem,
                this.writeToDeviceItem,
                this.resetSetpointsItem,
                this.clearSetpointsItem,
                this.readFromFileItem,
                this.writeToFileItem,
                this.writeToHtmlItem
            });
            this.readFromDeviceItem.Enabled = this.writeToDeviceItem.Enabled = this._device.DeviceDlgInfo.IsConnectionMode;
        }

        private void konturFF_CheckedChanged(object sender, EventArgs e)
        {
            this.konturFFGroup.Enabled = this.konturFF.Checked;
        }

        private void konturFN_CheckedChanged(object sender, EventArgs e)
        {
            this.konturFNGroup.Enabled = this.konturFN.Checked;
        }

        private void _ipHi2_KeyUp(object sender, KeyEventArgs e)
        {
            MaskedTextBox temp = (MaskedTextBox)sender;
            if (temp.Text.Length == 3)
            {
                this._ipHi1.Focus();
            }
        }

        private void _ipHi1_KeyUp(object sender, KeyEventArgs e)
        {
            MaskedTextBox temp = (MaskedTextBox)sender;
            if (temp.Text.Length == 3)
            {
                this._ipLo2.Focus();
            }
        }

        private void _ipLo2_KeyUp(object sender, KeyEventArgs e)
        {
            MaskedTextBox temp = (MaskedTextBox)sender;
            if (temp.Text.Length == 3)
            {
                this._ipLo1.Focus();
            }
        }

        #region [Обнуление сигналов ЛС 1-8]

        /// <summary>
        /// Отслеживаем, какой из табов выбран в ЛС 1-8
        /// </summary>

        public bool _tP39;
        public bool _tP40;
        public bool _tP41;
        public bool _tP42;
        public bool _tP43;
        public bool _tP44;
        public bool _tP45;
        public bool _tP46;

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_lsFirsTabControl.SelectedTab == tabPage39)
            {
                _tP39 = true;
                _tP40 = false;
                _tP41 = false;
                _tP42 = false;
                _tP43 = false;
                _tP44 = false;
                _tP45 = false;
                _tP46 = false;
                return;
            }
            if (_lsFirsTabControl.SelectedTab == tabPage40)
            {
                _tP39 = false;
                _tP40 = true;
                _tP41 = false;
                _tP42 = false;
                _tP43 = false;
                _tP44 = false;
                _tP45 = false;
                _tP46 = false;
                return;
            }
            if (_lsFirsTabControl.SelectedTab == tabPage41)
            {
                _tP39 = false;
                _tP40 = false;
                _tP41 = true;
                _tP42 = false;
                _tP43 = false;
                _tP44 = false;
                _tP45 = false;
                _tP46 = false;
                return;
            }
            if (_lsFirsTabControl.SelectedTab == tabPage42)
            {
                _tP39 = false;
                _tP40 = false;
                _tP41 = false;
                _tP42 = true;
                _tP43 = false;
                _tP44 = false;
                _tP45 = false;
                _tP46 = false;
                return;
            }
            if (_lsFirsTabControl.SelectedTab == tabPage43)
            {
                _tP39 = false;
                _tP40 = false;
                _tP41 = false;
                _tP42 = false;
                _tP43 = true;
                _tP44 = false;
                _tP45 = false;
                _tP46 = false;
                return;
            }
            if (_lsFirsTabControl.SelectedTab == tabPage44)
            {
                _tP39 = false;
                _tP40 = false;
                _tP41 = false;
                _tP42 = false;
                _tP43 = false;
                _tP44 = true;
                _tP45 = false;
                _tP46 = false;
                return;
            }
            if (_lsFirsTabControl.SelectedTab == tabPage45)
            {
                _tP39 = false;
                _tP40 = false;
                _tP41 = false;
                _tP42 = false;
                _tP43 = false;
                _tP44 = false;
                _tP45 = true;
                _tP46 = false;
                return;
            }
            if (_lsFirsTabControl.SelectedTab == tabPage46)
            {
                _tP39 = false;
                _tP40 = false;
                _tP41 = false;
                _tP42 = false;
                _tP43 = false;
                _tP44 = false;
                _tP45 = false;
                _tP46 = true;
                return;
            }
        }
        
        /// <summary>
        /// Обнуление сигналов ЛС 1-8
        /// </summary>
        private void ResetCurrentLs()
        {
            if (_tP39)
            {
                for (int i = 0; i < _lsAndDgv1Gr1.Rows.Count; i++)
                {
                    _lsAndDgv1Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
                }
            }
            if (_tP40)
            {
                for (int i = 0; i < _lsAndDgv2Gr1.Rows.Count; i++)
                {
                    _lsAndDgv2Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
                }
            }
            if (_tP41)
            {
                for (int i = 0; i < _lsAndDgv3Gr1.Rows.Count; i++)
                {
                    _lsAndDgv3Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
                }
            }
            if (_tP42)
            {
                for (int i = 0; i < _lsAndDgv4Gr1.Rows.Count; i++)
                {
                    _lsAndDgv4Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
                }
            }
            if (_tP43)
            {
                for (int i = 0; i < _lsAndDgv5Gr1.Rows.Count; i++)
                {
                    _lsAndDgv5Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
                }
            }
            if (_tP44)
            {
                for (int i = 0; i < _lsAndDgv6Gr1.Rows.Count; i++)
                {
                    _lsAndDgv6Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
                }
            }
            if (_tP45)
            {
                for (int i = 0; i < _lsAndDgv7Gr1.Rows.Count; i++)
                {
                    _lsAndDgv7Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
                }
            }
            if (_tP46)
            {
                for (int i = 0; i < _lsAndDgv8Gr1.Rows.Count; i++)
                {
                    _lsAndDgv8Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
                }
            }
        }

        private void ClearAllLs()
        {
            for (int i = 0; i < _lsAndDgv1Gr1.Rows.Count; i++)
            {
                _lsAndDgv1Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
            }
            
            for (int i = 0; i < _lsAndDgv2Gr1.Rows.Count; i++)
            {
                _lsAndDgv2Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
            }
            
            
            for (int i = 0; i < _lsAndDgv3Gr1.Rows.Count; i++)
            {
                _lsAndDgv3Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
            }
            
            
            for (int i = 0; i < _lsAndDgv4Gr1.Rows.Count; i++)
            {
                _lsAndDgv4Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
            }
            
            for (int i = 0; i < _lsAndDgv5Gr1.Rows.Count; i++)
            {
                _lsAndDgv5Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
            }
            
            for (int i = 0; i < _lsAndDgv6Gr1.Rows.Count; i++)
            {
                _lsAndDgv6Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
            }
            
            for (int i = 0; i < _lsAndDgv7Gr1.Rows.Count; i++)
            {
                _lsAndDgv7Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
            }

            for (int i = 0; i < _lsAndDgv8Gr1.Rows.Count; i++)
            {
                _lsAndDgv8Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
            }
            
        }

        private void _clearCurrentLsButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить сигнал ЛС", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;
            if (_lsFirsTabControl.SelectedTab == tabPage39)
            {
                _tP39 = true;
            }
            this.ResetCurrentLs();
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
        }

        private void _clearAllLsSygnalButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить все сигналы ЛС 1-8?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;
            this.ClearAllLs();
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
        }

        #endregion

        #region [Обнуление сигналов ЛС 9-16]

        public bool _tP31;
        public bool _tP32;
        public bool _tP33;
        public bool _tP34;
        public bool _tP35;
        public bool _tP36;
        public bool _tP37;
        public bool _tP38;

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_lsSecondTabControl.SelectedTab == tabPage31)
            {
                _tP31 = true;
                _tP32 = false;
                _tP33 = false;
                _tP34 = false;
                _tP35 = false;
                _tP36 = false;
                _tP37 = false;
                _tP38 = false;
                return;
            }
            if (_lsSecondTabControl.SelectedTab == tabPage32)
            {
                _tP31 = false;
                _tP32 = true;
                _tP33 = false;
                _tP34 = false;
                _tP35 = false;
                _tP36 = false;
                _tP37 = false;
                _tP38 = false;
                return;
            }
            if (_lsSecondTabControl.SelectedTab == tabPage33)
            {
                _tP31 = false;
                _tP32 = false;
                _tP33 = true;
                _tP34 = false;
                _tP35 = false;
                _tP36 = false;
                _tP37 = false;
                _tP38 = false;
                return;
            }
            if (_lsSecondTabControl.SelectedTab == tabPage34)
            {
                _tP31 = false;
                _tP32 = false;
                _tP33 = false;
                _tP34 = true;
                _tP35 = false;
                _tP36 = false;
                _tP37 = false;
                _tP38 = false;
                return;
            }
            if (_lsSecondTabControl.SelectedTab == tabPage35)
            {
                _tP31 = false;
                _tP32 = false;
                _tP33 = false;
                _tP34 = false;
                _tP35 = true;
                _tP36 = false;
                _tP37 = false;
                _tP38 = false;
                return;
            }
            if (_lsSecondTabControl.SelectedTab == tabPage36)
            {
                _tP31 = false;
                _tP32 = false;
                _tP33 = false;
                _tP34 = false;
                _tP35 = false;
                _tP36 = true;
                _tP37 = false;
                _tP38 = false;
                return;
            }
            if (_lsSecondTabControl.SelectedTab == tabPage37)
            {
                _tP31 = false;
                _tP32 = false;
                _tP33 = false;
                _tP34 = false;
                _tP35 = false;
                _tP36 = false;
                _tP37 = true;
                _tP38 = false;
                return;
            }
            if (_lsSecondTabControl.SelectedTab == tabPage38)
            {
                _tP31 = false;
                _tP32 = false;
                _tP33 = false;
                _tP34 = false;
                _tP35 = false;
                _tP36 = false;
                _tP37 = false;
                _tP38 = true;
                return;
            }
        }

        /// <summary>
        /// Обнуление сигналов ЛС 9-16
        /// </summary>
        private void ResetCurrentLsn()
        {
            if (_tP31)
            {
                for (int i = 0; i < _lsOrDgv1Gr1.Rows.Count; i++)
                {
                    _lsOrDgv1Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
                }
            }
            if (_tP32)
            {
                for (int i = 0; i < _lsOrDgv2Gr1.Rows.Count; i++)
                {
                    _lsOrDgv2Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
                }
            }
            if (_tP33)
            {
                for (int i = 0; i < _lsOrDgv3Gr1.Rows.Count; i++)
                {
                    _lsOrDgv3Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
                }
            }
            if (_tP34)
            {
                for (int i = 0; i < _lsOrDgv4Gr1.Rows.Count; i++)
                {
                    _lsOrDgv4Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
                }
            }
            if (_tP35)
            {
                for (int i = 0; i < _lsOrDgv5Gr1.Rows.Count; i++)
                {
                    _lsOrDgv5Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
                }
            }
            if (_tP36)
            {
                for (int i = 0; i < _lsOrDgv6Gr1.Rows.Count; i++)
                {
                    _lsOrDgv6Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
                }
            }
            if (_tP37)
            {
                for (int i = 0; i < _lsOrDgv7Gr1.Rows.Count; i++)
                {
                    _lsOrDgv7Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
                }
            }
            if (_tP38)
            {
                for (int i = 0; i < _lsOrDgv8Gr1.Rows.Count; i++)
                {
                    _lsOrDgv8Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
                }
            }
        }

        private void ClearAllLsn()
        {
            for (int i = 0; i < _lsOrDgv1Gr1.Rows.Count; i++)
            {
                _lsOrDgv1Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
            }

            for (int i = 0; i < _lsOrDgv2Gr1.Rows.Count; i++)
            {
                _lsOrDgv2Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
            }


            for (int i = 0; i < _lsOrDgv3Gr1.Rows.Count; i++)
            {
                _lsOrDgv3Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
            }


            for (int i = 0; i < _lsOrDgv4Gr1.Rows.Count; i++)
            {
                _lsOrDgv4Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
            }

            for (int i = 0; i < _lsOrDgv5Gr1.Rows.Count; i++)
            {
                _lsOrDgv5Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
            }

            for (int i = 0; i < _lsOrDgv6Gr1.Rows.Count; i++)
            {
                _lsOrDgv6Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
            }

            for (int i = 0; i < _lsOrDgv7Gr1.Rows.Count; i++)
            {
                _lsOrDgv7Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
            }

            for (int i = 0; i < _lsOrDgv8Gr1.Rows.Count; i++)
            {
                _lsOrDgv8Gr1.Rows[i].Cells[1].Value = Strings.LsState[0];
            }

        }

        private void _clearCurrentLsnButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить сигнал ЛС?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;

            if (_lsSecondTabControl.SelectedTab == tabPage31)
            {
                _tP31 = true;
            }

            this.ResetCurrentLsn();
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
        }

        private void _clearAllLsnSygnalButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить все сигналы ЛС 9-16?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;
            this.ClearAllLsn();
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
        }

        #endregion

        #region [Обнуление сигналов ВЛС]

        private bool _tBVLS1;
        private bool _tBVLS2;
        private bool _tBVLS3;
        private bool _tBVLS4;
        private bool _tBVLS5;
        private bool _tBVLS6;
        private bool _tBVLS7;
        private bool _tBVLS8;
        private bool _tBVLS9;
        private bool _tBVLS10;
        private bool _tBVLS11;
        private bool _tBVLS12;
        private bool _tBVLS13;
        private bool _tBVLS14;
        private bool _tBVLS15;
        private bool _tBVLS16;
        
        private void ResetCurrentVlsSignal()
        {
            if (_tBVLS1)
            {
                for (int i = 0; i < VLSclb1Gr1.Items.Count; i++)
                {
                    VLSclb1Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS2)
            {
                for (int i = 0; i < VLSclb2Gr1.Items.Count; i++)
                {
                    VLSclb2Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS3)
            {
                for (int i = 0; i < VLSclb3Gr1.Items.Count; i++)
                {
                    VLSclb3Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS4)
            {
                for (int i = 0; i < VLSclb4Gr1.Items.Count; i++)
                {
                    VLSclb4Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS5)
            {
                for (int i = 0; i < VLSclb5Gr1.Items.Count; i++)
                {
                    VLSclb5Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS6)
            {
                for (int i = 0; i < VLSclb6Gr1.Items.Count; i++)
                {
                    VLSclb6Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS7)
            {
                for (int i = 0; i < VLSclb7Gr1.Items.Count; i++)
                {
                    VLSclb7Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS8)
            {
                for (int i = 0; i < VLSclb8Gr1.Items.Count; i++)
                {
                    VLSclb8Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS9)
            {
                for (int i = 0; i < VLSclb9Gr1.Items.Count; i++)
                {
                    VLSclb9Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS10)
            {
                for (int i = 0; i < VLSclb10Gr1.Items.Count; i++)
                {
                    VLSclb10Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS11)
            {
                for (int i = 0; i < VLSclb11Gr1.Items.Count; i++)
                {
                    VLSclb11Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS12)
            {
                for (int i = 0; i < VLSclb12Gr1.Items.Count; i++)
                {
                    VLSclb12Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS13)
            {
                for (int i = 0; i < VLSclb13Gr1.Items.Count; i++)
                {
                    VLSclb13Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS14)
            {
                for (int i = 0; i < VLSclb14Gr1.Items.Count; i++)
                {
                    VLSclb14Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS15)
            {
                for (int i = 0; i < VLSclb15Gr1.Items.Count; i++)
                {
                    VLSclb1Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS16)
            {
                for (int i = 0; i < VLSclb16Gr1.Items.Count; i++)
                {
                    VLSclb16Gr1.SetItemChecked(i, false);
                }
            }
        }

        private void _resetCurrentVlsButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить сигнал ВЛС?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;

            if (_vlsTabControl.SelectedTab == VLS1)
            {
                this._tBVLS1 = true;
            }
            this.ResetCurrentVlsSignal();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
        }

        private void _resetAllVlsButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить все сигнал ВЛС?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;
            this._vlsUnion.Reset();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
        }

        private void tabControl2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.ChangeTextVls();

            if (_vlsTabControl.SelectedTab == VLS1)
            {
                _tBVLS1 = true;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS2)
            {
                _tBVLS1 = false;
                _tBVLS2 = true;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS3)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = true;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS4)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = true;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS5)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = true;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS6)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = true;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS7)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = true;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS8)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = true;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS9)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = true;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS10)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = true;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS11)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = true;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS12)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = true;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS13)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = true;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS14)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = true;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS15)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = true;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS16)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = true;
                return;
            }

        }

        #endregion
        
        #endregion [Event handlers]

        #region [Save/Load File Members]

        private void ResetSetpoints(bool isDialog)   //загрузка базовых уставок из файла, isDialog - показывает будет ли диалог
        {
            if (isDialog)
            {
                DialogResult res = MessageBox.Show("Загрузить базовые уставки", "Внимание", MessageBoxButtons.YesNo);
                if (res != DialogResult.Yes) return;
            }
            try
            {
                XmlDocument doc = new XmlDocument();
               
                doc.Load(Path.GetDirectoryName(Application.ExecutablePath) + string.Format(BASE_CONFIG_PATH, _device.DeviceType, this._device.DeviceVersion, _device.Info.Plant));
                
                XmlNode a = doc.FirstChild.SelectSingleNode($"{_device.DeviceType}" + XML_HEAD);
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);

                switch (_device.DeviceType)
                {
                    case "MR771":
                        this._currentSetpointsStruct771.InitStruct(values);
                        break;
                    default:
                        this._currentSetpointsStruct.InitStruct(values);
                        break;
                }

                InitSignaturesStruct();

                this.ShowConfiguration();

                this._statusLabel.Text = BASE_CONFIG_LOAD_SUCCESSFULLY;
            }
            catch(Exception ex)
            {
                if (isDialog)
                {
                    if (MessageBox.Show("Невозможно загрузить файл стандартной конфигурации. \nОбнулить уставки?",
                            "Внимание", MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        return;
                    }
                }
                switch (_device.DeviceType)
                {
                    case "MR771":
                        this._configurationValidator771.Reset();
                        break;
                    default:
                        this._configurationValidator.Reset();
                        break;
                }

                InitSignaturesStruct();
            }
        }
        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveInFile();
        }

        private void SaveInFile()
        {
            if (this.WriteConfiguration())
            {

                UpdateStatus("Идет запись конфигурации в файл");
                this._saveConfigurationDlg.Title = $"Сохранить уставки для {_device.Info.Type}";
                this._saveConfigurationDlg.Filter = $"Уставки {_device.Info.Type} (*.xml) | *.xml";
                this._saveConfigurationDlg.FileName = $"{_device.Info.Type}_Уставки_версия_{this._device.DeviceVersion}.xml";
                if (DialogResult.OK != this._saveConfigurationDlg.ShowDialog())
                {
                    this.IsProcess = false;
                    this._statusLabel.Text = string.Empty;
                    return;
                }
                this.Serialize(this._saveConfigurationDlg.FileName);
            }
        }
        private void ReadFromFile()
        {
            _openConfigurationDlg.Title = $"Открыть уставки для {_device.Info.Type}";
            _openConfigurationDlg.Filter = $"Уставки {_device.Info.Type} (*.xml) | *.xml";
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                UpdateStatus("Загрузка конфигурации из файла", true);

                this.Deserialize(this._openConfigurationDlg.FileName);
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
                TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
                TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
            }
        }
        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement($"{_device.DeviceType}"));

                ushort[] configValues;
                List<string> signalsInDeviceValues = null;
                List<string> signalsUserValues = null;
                List<string> signalsInDeviceValuesForOsc = null;
                List<string> signalsUserValuesForOsc = null;

                signalsInDeviceValues = signaturesSignalsConfigControl.SaveToFile(out signalsInDeviceValues, 1);
                signalsUserValues = signaturesSignalsConfigControl.SaveToFile(out signalsUserValues, 2);
                signalsInDeviceValuesForOsc = signaturesOsc.SaveToFile(out signalsInDeviceValuesForOsc, 1);
                signalsUserValuesForOsc = signaturesOsc.SaveToFile(out signalsUserValuesForOsc, 2);

                switch (_device.DeviceType)
                {
                    case "MR771":
                        configValues = this._currentSetpointsStruct771.GetValues();
                        break;
                    default:
                        configValues = this._currentSetpointsStruct.GetValues();
                        break;
                }

                XmlElement configElement = doc.CreateElement($"{_device.DeviceType}" + XML_HEAD);
                configElement.InnerText = Convert.ToBase64String(Common.TOBYTES(configValues, false));

                XmlAttribute typeDevice = doc.CreateAttribute("TYPE");
                typeDevice.Value = _device.DeviceType;
                configElement.Attributes.Append(typeDevice);

                XmlAttribute partDevice = doc.CreateAttribute("CONFIG");
                partDevice.Value = _device.DevicePlant;
                configElement.Attributes.Append(partDevice);
                
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }

                XmlElement sugnatures = doc.CreateElement("SIGNATURES");

                XmlElement sugnaturesForSignals = doc.CreateElement("SIGNATURES_FOR_SIGN");
                XmlElement sugnaturesForOsc = doc.CreateElement("SIGNATURES_FOR_OSC");

                XmlElement signaturesInDeviceElementForSignals = doc.CreateElement("SIGNATURES_IN_DEVICE");
                XmlElement signaturesUserElement = doc.CreateElement("SIGNATURES_USERS");

                XmlElement signaturesInDeviceElementForOsc = doc.CreateElement("SIGNATURES_IN_DEVICE");

                XmlElement signaturesUserElementForOsc = doc.CreateElement("SIGNATURES_USERS");

                if (signalsInDeviceValues != null)
                    for (int i = 0; i < signalsInDeviceValues.Count; i++)
                    {
                        XmlElement signalsCount = doc.CreateElement($"SIGNALS_{i + 1}");
                        XmlText signalsText = doc.CreateTextNode(signalsInDeviceValues[i]);
                        signalsCount.AppendChild(signalsText);
                        signaturesInDeviceElementForSignals.AppendChild(signalsCount);
                    }

                if (signalsUserValues != null)
                    for (int i = 0; i < signalsUserValues.Count; i++)
                    {
                        XmlElement signalsCount = doc.CreateElement($"SIGNALS_{i + 1}");
                        XmlText signalsText = doc.CreateTextNode(signalsUserValues[i]);
                        signalsCount.AppendChild(signalsText);
                        signaturesUserElement.AppendChild(signalsCount);
                    }

                if (signalsInDeviceValuesForOsc != null)
                    for (int i = 0; i < signalsInDeviceValuesForOsc.Count; i++)
                    {
                        XmlElement signalsCount = doc.CreateElement($"SIGNALS_{i + 1}");
                        XmlText signalsText = doc.CreateTextNode(signalsInDeviceValuesForOsc[i]);
                        signalsCount.AppendChild(signalsText);
                        signaturesInDeviceElementForOsc.AppendChild(signalsCount);
                    }

                if (signalsUserValuesForOsc != null)
                    for (int i = 0; i < signalsUserValuesForOsc.Count; i++)
                    {
                        XmlElement signalsCount = doc.CreateElement($"SIGNALS_{i + 1}");
                        XmlText signalsText = doc.CreateTextNode(signalsUserValuesForOsc[i]);
                        signalsCount.AppendChild(signalsText);
                        signaturesUserElementForOsc.AppendChild(signalsCount);
                    }

                sugnaturesForSignals.AppendChild(signaturesInDeviceElementForSignals);
                sugnaturesForSignals.AppendChild(signaturesUserElement);

                sugnaturesForOsc.AppendChild(signaturesInDeviceElementForOsc);
                sugnaturesForOsc.AppendChild(signaturesUserElementForOsc);

                sugnatures.AppendChild(sugnaturesForSignals);
                sugnatures.AppendChild(sugnaturesForOsc);

                doc.DocumentElement.AppendChild(configElement);

                doc.DocumentElement.AppendChild(sugnatures);

                doc.Save(binFileName);
                this.IsProcess = false;

                UpdateStatus(string.Format("Файл {0} успешно сохранён", binFileName));
            }
            catch
            {
                UpdateStatus(string.Format("Ошибка сохранения файла {0}", binFileName));
                MessageBox.Show(FILE_SAVE_FAIL);
                this.IsProcess = false;
            }
        }

        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);

                XmlNode configNode = doc.FirstChild.SelectSingleNode($"{_device.DeviceType}" + XML_HEAD);

                if (configNode == null)
                {
                    UpdateStatus(string.Format("Ошибка загрузки файла {0}", binFileName));
                    MessageBox.Show(FILE_LOAD_FAIL + ". Устройство не соответсвует конфигурации.",
                        "Ошибка загрузки файла конфигурации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (configNode.Attributes != null)
                {
                    var config = configNode.Attributes[0].Value;
                    var partConfig = configNode.Attributes[1].Value;

                    if (_device.DeviceType != config && _device.DevicePlant != partConfig)
                    {
                        UpdateStatus(string.Format("Ошибка загрузки файла {0}", binFileName));
                        MessageBox.Show(FILE_LOAD_FAIL + ". Устройство и аппаратная часть не соответсвует конфигурации.", "Ошибка загрузки файла конфигурации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (_device.DeviceType != config)
                    {
                        UpdateStatus(string.Format("Ошибка загрузки файла {0}", binFileName));
                        MessageBox.Show(FILE_LOAD_FAIL + ". Устройство не соответсвует конфигурации.", "Ошибка загрузки файла конфигурации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    
                    if (_device.DevicePlant != partConfig)
                    {
                        UpdateStatus(string.Format("Ошибка загрузки файла {0}", binFileName));
                        MessageBox.Show(FILE_LOAD_FAIL + ". Аппаратная часть не соответсвует конфигурации.", "Ошибка загрузки файла конфигурации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }

                byte[] values = Convert.FromBase64String(configNode.InnerText);

                XmlNode signatures = doc.LastChild.SelectSingleNode("SIGNATURES");

                XmlNode signalsNode = signatures.FirstChild;
                XmlNode oscNode = signatures.LastChild;

                XmlNode inDeviceNodeForSign = signalsNode.FirstChild;
                XmlNode inUsersNodeForSing = signalsNode.LastChild;

                XmlNode inDeviceNodeForOsc = oscNode.FirstChild;
                XmlNode inUsersNodeForOsc = oscNode.LastChild;

                List<string> signalsInDeviceForSign = new List<string>();
                List<string> signalsUserForSign = new List<string>();

                List<string> signalsInDeviceForOsc = new List<string>();
                List<string> signalsUserForOsc = new List<string>();


                if (inDeviceNodeForSign != null)
                    for (int i = 0; i < inDeviceNodeForSign.ChildNodes.Count; i++)
                    {
                        signalsInDeviceForSign.Add(inDeviceNodeForSign.ChildNodes[i].InnerText);
                    }

                if (inUsersNodeForSing != null)
                    for (int i = 0; i < inUsersNodeForSing.ChildNodes.Count; i++)
                    {
                        signalsUserForSign.Add(inUsersNodeForSing.ChildNodes[i].InnerText);
                    }

                signaturesSignalsConfigControl.ClearDgv();
                if (inDeviceNodeForSign != null || inUsersNodeForSing != null)
                {
                    signaturesSignalsConfigControl.SignalsList = signalsInDeviceForSign;
                    signaturesSignalsConfigControl.FillSignaturesSignalsDgv(
                        signaturesSignalsConfigControl.SignalsList, Strings.DefaultSignaturesSignals.Values.ToList(),
                        true, signalsInDeviceForSign, signalsUserForSign);
                }
                else
                {
                    signaturesSignalsConfigControl.FillSignaturesSignalsDgv(
                        signaturesSignalsConfigControl.SignalsList, Strings.DefaultSignaturesSignals.Values.ToList(),
                        true);
                }

                if (inDeviceNodeForOsc != null)
                    for (int i = 0; i < inDeviceNodeForOsc.ChildNodes.Count; i++)
                    {
                        signalsInDeviceForOsc.Add(inDeviceNodeForOsc.ChildNodes[i].InnerText);
                    }

                if (inUsersNodeForOsc != null)
                    for (int i = 0; i < inUsersNodeForOsc.ChildNodes.Count; i++)
                    {
                        signalsUserForOsc.Add(inUsersNodeForOsc.ChildNodes[i].InnerText);
                    }

                signaturesOsc.ClearDgv();
                if (inDeviceNodeForOsc != null || inUsersNodeForOsc != null)
                {
                    signaturesOsc.Messages = signalsInDeviceForOsc;
                    signaturesOsc.FillSignaturesSignalsDgv(signalsInDeviceForOsc, InputLogicStruct.DiscrestCount, null, true, signalsInDeviceForOsc,
                        signalsUserForOsc);
                }
                else
                {
                    signaturesOsc.FillSignaturesSignalsDgv(signalsInDeviceForOsc, InputLogicStruct.DiscrestCount, null, true);
                }

                UpdateListsInStringsConfig();
                
                Init();

                switch (_device.DeviceType)
                {
                    case "MR771":
                        this._currentSetpointsStruct771.InitStruct(values);
                        break;
                    default:
                        this._currentSetpointsStruct.InitStruct(values);
                        break;
                }

                this.ShowConfiguration();
                
                UpdateStatus(string.Format("Файл {0} успешно загружен", binFileName));
            }
            catch (Exception e)
            {
                UpdateStatus(string.Format("Ошибка загрузки файла {0}", binFileName));

                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }
        private void SaveToHtml()
        {
            try
            {
                if (this.WriteConfiguration())
                {
                    this._statusLabel.Text = "Идет запись конфигурации в HTML";
                    ExportGroupForm exportGroup = new ExportGroupForm();
                    if (exportGroup.ShowDialog() != DialogResult.OK)
                    {
                        this.IsProcess = false;
                        this._statusLabel.Text = string.Empty;
                        return;
                    }

                    switch (_device.DeviceType)
                    {
                        case "MR771":
                            this._currentSetpointsStruct771.DeviceVersion = this._version;
                            this._currentSetpointsStruct771.DeviceType = this._device.DeviceType;
                            this._currentSetpointsStruct771.DeviceNumber = this._device.DeviceNumber.ToString();
                            this._currentSetpointsStruct771.DevicePlant = this._device.DevicePlant;
                            this._currentSetpointsStruct771.Primary = this._resistCheck.Checked;
                            this._currentSetpointsStruct771.SizeOsc = this._oscSizeTextBox.Text;
                            this._currentSetpointsStruct771.Group = (int)exportGroup.SelectedGroup;
                            this.SaveToHtml(this._currentSetpointsStruct771, exportGroup.SelectedGroup);
                            break;
                        default:
                            this._currentSetpointsStruct.DeviceVersion = this._version;
                            this._currentSetpointsStruct.DeviceType = this._device.DeviceType;
                            this._currentSetpointsStruct.DeviceNumber = this._device.DeviceNumber.ToString();
                            this._currentSetpointsStruct.DevicePlant = this._device.DevicePlant;
                            this._currentSetpointsStruct.Primary = this._resistCheck.Checked;
                            this._currentSetpointsStruct.SizeOsc = this._oscSizeTextBox.Text;
                            this._currentSetpointsStruct.Group = (int)exportGroup.SelectedGroup;
                            this.SaveToHtml(this._currentSetpointsStruct, exportGroup.SelectedGroup);
                            break;
                    }
                }
                this.IsProcess = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения");
                this.IsProcess = false;
            }
        }

        private void SaveToHtml(StructBase str, ExportStruct group)
        {
            string fileName;
            if (group == ExportStruct.ALL)
            {
                fileName = $"{this._device.DeviceType}_Уставки_версия_{this._device.DeviceVersion.Replace(".", "_")}_{this._device.DevicePlant}_все группы";
                this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MrUniversalAll, fileName, str);
            }
            else
            {
                fileName = $"{this._device.DeviceType}_Уставки_версия_{this._device.DeviceVersion.Replace(".", "_")}_{this._device.DevicePlant}_группа{(int)group}";
                this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MrUniversalGroup, fileName, str);
            }
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

       
        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtml();
        }
        #endregion [Save/Load File Members]

        #region [Switch setpoints]

        private void _applyCopySetpoinsButton_Click(object sender, EventArgs e)
        {
            string message;

            switch (_device.DeviceType)
            {
                case "MR771":
                    if (this._setpointUnion771.Check(out message, true))
                    {
                        GroupSetpoint771[] allSetpoints =
                            this.CopySetpoints<AllGroupSetpointStruct771, GroupSetpoint771>(this._setpointUnion771, this._setpointsValidator771);
                        this._currentSetpointsStruct771.AllGroupSetpoints.Setpoints = allSetpoints;
                        this._setpointsValidator771.Set(this._currentSetpointsStruct771.AllGroupSetpoints);
                    }
                    else
                    {
                        MessageBox.Show("Обнаружены некорректные уставки");
                        return;
                    }
                    break;
                default:
                    if (this._setpointUnion.Check(out message, true))
                    {
                        GroupSetpoint[] allSetpoints =
                            this.CopySetpoints<AllGroupSetpointStruct, GroupSetpoint>(this._setpointUnion, this._setpointsValidator);
                        this._currentSetpointsStruct.AllGroupSetpoints.Setpoints = allSetpoints;
                        this._setpointsValidator.Set(this._currentSetpointsStruct.AllGroupSetpoints);
                    }
                    else
                    {
                        MessageBox.Show("Обнаружены некорректные уставки");
                        return;
                    }
                    break;
            }
            
            
            
            MessageBox.Show("Копирование завершено");
        }

        private T2[] CopySetpoints<T1, T2>(IValidator union, IValidator setpointValidator)
            where T1 : StructBase, ISetpointContainer<T2> where T2 : StructBase
        {
            T2 temp = (T2) union.Get();
            T2[] allSetpoints = ((T1) setpointValidator.Get()).Setpoints;
            if ((string) this._copySetpoinsGroupComboBox.SelectedItem == Strings.CopyGroupsNames.Last())
            {
                for (int i = 0; i < allSetpoints.Length; i++)
                {
                    allSetpoints[i] = temp.Clone<T2>();
                }
            }
            else
            {
                allSetpoints[this._copySetpoinsGroupComboBox.SelectedIndex] = temp.Clone<T2>();
                allSetpoints[this._setpointsComboBox.SelectedIndex] = temp.Clone<T2>();
            }
            return allSetpoints;
        }

        private void OnRefreshInfoTable()
        {
            //this.resistanceDefTabCtr1.RefreshResistInfoTable();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
        }
        
        #endregion

        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof(MRUniversal); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(ConfigurationForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        #endregion [IFormView Members]       

        #region [Event CreateTree]
        private void VLSclbGr1_SelectedValueChanged(object sender, EventArgs e)
        {
            TreeViewVLS.StateNodes(this.treeViewForVLS);
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandCurrentTreeNode(this._vlsTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
        }
        private void treeViewForVLS_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Left) return;
            TreeViewVLS.DeleteNode(sender, e, this.contextMenu, this.allVlsCheckedListBoxs);
            TreeViewVLS.StateNodes(this.treeViewForVLS);
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
        }
        private void _lsAndDgvGr1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            TreeViewLS.StateNodesAND(this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
            TreeViewLS.ExpandCurrentTreeNode(this._lsFirsTabControl, this.treeViewForLsAND);
            TreeViewLS.ExpandTreeNodesAND(this.treeViewForLsAND);
        }
        private void _lsOrDgvGr1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            TreeViewLS.StateNodesOR(this.treeViewForLsOR);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
            TreeViewLS.ExpandCurrentTreeNode(this._lsSecondTabControl, this.treeViewForLsOR);
            TreeViewLS.ExpandTreeNodesOR(this.treeViewForLsOR);
        }
        private void treeViewForLsAND_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewLS.DeleteNode(sender, e, this.contextMenu, this.dataGridsViewLsAND);
            TreeViewLS.StateNodesAND(this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
            TreeViewLS.ExpandTreeNodesAND(this.treeViewForLsAND);
        }
        private void treeViewForLsOR_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewLS.DeleteNode(sender, e, this.contextMenu, this.dataGridsViewLsOR);
            TreeViewLS.StateNodesOR(this.treeViewForLsOR);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
            TreeViewLS.ExpandTreeNodesOR(this.treeViewForLsOR);
        }

        #endregion [Event CreateTree]
    }
}