﻿namespace BEMN.MRUNIVERSAL.Configuration
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle76 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle77 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle78 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle79 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle80 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle85 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle91 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle92 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle93 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle94 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle95 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle96 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle97 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle98 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle99 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle100 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle101 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle102 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle103 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle104 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle105 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle106 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle107 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle108 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle109 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle110 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle111 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle112 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle113 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle114 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle115 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle116 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle117 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle118 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle119 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle120 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle121 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle122 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle123 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle124 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle125 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle126 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle127 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle128 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle129 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle130 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle131 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle132 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle133 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle134 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle135 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle136 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle137 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle138 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle139 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle140 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle141 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle142 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle143 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle144 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle145 = new System.Windows.Forms.DataGridViewCellStyle();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetSetpointsItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearSetpointsItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToHtmlItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label109 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this._saveToXmlButton = new System.Windows.Forms.Button();
            this._clearSetpointBtn = new System.Windows.Forms.Button();
            this._resetSetpointsButton = new System.Windows.Forms.Button();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this._progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._saveXmlDialog = new System.Windows.Forms.SaveFileDialog();
            this.saveFileResistCharacteristic = new System.Windows.Forms.SaveFileDialog();
            this._gooseTabPage = new System.Windows.Forms.TabPage();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.goin64 = new System.Windows.Forms.ComboBox();
            this.goin48 = new System.Windows.Forms.ComboBox();
            this.goin32 = new System.Windows.Forms.ComboBox();
            this.label281 = new System.Windows.Forms.Label();
            this.label282 = new System.Windows.Forms.Label();
            this.label283 = new System.Windows.Forms.Label();
            this.goin63 = new System.Windows.Forms.ComboBox();
            this.goin47 = new System.Windows.Forms.ComboBox();
            this.goin31 = new System.Windows.Forms.ComboBox();
            this.label284 = new System.Windows.Forms.Label();
            this.label285 = new System.Windows.Forms.Label();
            this.label286 = new System.Windows.Forms.Label();
            this.goin62 = new System.Windows.Forms.ComboBox();
            this.goin46 = new System.Windows.Forms.ComboBox();
            this.goin30 = new System.Windows.Forms.ComboBox();
            this.label287 = new System.Windows.Forms.Label();
            this.label288 = new System.Windows.Forms.Label();
            this.label289 = new System.Windows.Forms.Label();
            this.goin61 = new System.Windows.Forms.ComboBox();
            this.goin45 = new System.Windows.Forms.ComboBox();
            this.goin29 = new System.Windows.Forms.ComboBox();
            this.label290 = new System.Windows.Forms.Label();
            this.label291 = new System.Windows.Forms.Label();
            this.label292 = new System.Windows.Forms.Label();
            this.goin60 = new System.Windows.Forms.ComboBox();
            this.goin44 = new System.Windows.Forms.ComboBox();
            this.goin28 = new System.Windows.Forms.ComboBox();
            this.label293 = new System.Windows.Forms.Label();
            this.label294 = new System.Windows.Forms.Label();
            this.label295 = new System.Windows.Forms.Label();
            this.goin59 = new System.Windows.Forms.ComboBox();
            this.goin43 = new System.Windows.Forms.ComboBox();
            this.goin27 = new System.Windows.Forms.ComboBox();
            this.label296 = new System.Windows.Forms.Label();
            this.label297 = new System.Windows.Forms.Label();
            this.label298 = new System.Windows.Forms.Label();
            this.goin58 = new System.Windows.Forms.ComboBox();
            this.goin42 = new System.Windows.Forms.ComboBox();
            this.goin26 = new System.Windows.Forms.ComboBox();
            this.label299 = new System.Windows.Forms.Label();
            this.label300 = new System.Windows.Forms.Label();
            this.label301 = new System.Windows.Forms.Label();
            this.goin57 = new System.Windows.Forms.ComboBox();
            this.goin41 = new System.Windows.Forms.ComboBox();
            this.goin25 = new System.Windows.Forms.ComboBox();
            this.label302 = new System.Windows.Forms.Label();
            this.label303 = new System.Windows.Forms.Label();
            this.label304 = new System.Windows.Forms.Label();
            this.goin56 = new System.Windows.Forms.ComboBox();
            this.goin40 = new System.Windows.Forms.ComboBox();
            this.goin24 = new System.Windows.Forms.ComboBox();
            this.label305 = new System.Windows.Forms.Label();
            this.label306 = new System.Windows.Forms.Label();
            this.label307 = new System.Windows.Forms.Label();
            this.goin55 = new System.Windows.Forms.ComboBox();
            this.goin39 = new System.Windows.Forms.ComboBox();
            this.goin23 = new System.Windows.Forms.ComboBox();
            this.label308 = new System.Windows.Forms.Label();
            this.label309 = new System.Windows.Forms.Label();
            this.label310 = new System.Windows.Forms.Label();
            this.goin54 = new System.Windows.Forms.ComboBox();
            this.goin38 = new System.Windows.Forms.ComboBox();
            this.goin22 = new System.Windows.Forms.ComboBox();
            this.label311 = new System.Windows.Forms.Label();
            this.label312 = new System.Windows.Forms.Label();
            this.label313 = new System.Windows.Forms.Label();
            this.goin53 = new System.Windows.Forms.ComboBox();
            this.goin37 = new System.Windows.Forms.ComboBox();
            this.goin21 = new System.Windows.Forms.ComboBox();
            this.label314 = new System.Windows.Forms.Label();
            this.label315 = new System.Windows.Forms.Label();
            this.label316 = new System.Windows.Forms.Label();
            this.goin52 = new System.Windows.Forms.ComboBox();
            this.goin36 = new System.Windows.Forms.ComboBox();
            this.goin20 = new System.Windows.Forms.ComboBox();
            this.label317 = new System.Windows.Forms.Label();
            this.label318 = new System.Windows.Forms.Label();
            this.label319 = new System.Windows.Forms.Label();
            this.goin51 = new System.Windows.Forms.ComboBox();
            this.goin35 = new System.Windows.Forms.ComboBox();
            this.goin19 = new System.Windows.Forms.ComboBox();
            this.label320 = new System.Windows.Forms.Label();
            this.label321 = new System.Windows.Forms.Label();
            this.label322 = new System.Windows.Forms.Label();
            this.goin50 = new System.Windows.Forms.ComboBox();
            this.goin34 = new System.Windows.Forms.ComboBox();
            this.goin18 = new System.Windows.Forms.ComboBox();
            this.label323 = new System.Windows.Forms.Label();
            this.label324 = new System.Windows.Forms.Label();
            this.label325 = new System.Windows.Forms.Label();
            this.goin49 = new System.Windows.Forms.ComboBox();
            this.label326 = new System.Windows.Forms.Label();
            this.goin33 = new System.Windows.Forms.ComboBox();
            this.label327 = new System.Windows.Forms.Label();
            this.goin17 = new System.Windows.Forms.ComboBox();
            this.label328 = new System.Windows.Forms.Label();
            this.goin16 = new System.Windows.Forms.ComboBox();
            this.label329 = new System.Windows.Forms.Label();
            this.goin15 = new System.Windows.Forms.ComboBox();
            this.label330 = new System.Windows.Forms.Label();
            this.goin14 = new System.Windows.Forms.ComboBox();
            this.label331 = new System.Windows.Forms.Label();
            this.goin13 = new System.Windows.Forms.ComboBox();
            this.label332 = new System.Windows.Forms.Label();
            this.goin12 = new System.Windows.Forms.ComboBox();
            this.label333 = new System.Windows.Forms.Label();
            this.goin11 = new System.Windows.Forms.ComboBox();
            this.label334 = new System.Windows.Forms.Label();
            this.goin10 = new System.Windows.Forms.ComboBox();
            this.label335 = new System.Windows.Forms.Label();
            this.goin9 = new System.Windows.Forms.ComboBox();
            this.label336 = new System.Windows.Forms.Label();
            this.goin8 = new System.Windows.Forms.ComboBox();
            this.label337 = new System.Windows.Forms.Label();
            this.goin7 = new System.Windows.Forms.ComboBox();
            this.label338 = new System.Windows.Forms.Label();
            this.goin6 = new System.Windows.Forms.ComboBox();
            this.label339 = new System.Windows.Forms.Label();
            this.goin5 = new System.Windows.Forms.ComboBox();
            this.label340 = new System.Windows.Forms.Label();
            this.goin4 = new System.Windows.Forms.ComboBox();
            this.label341 = new System.Windows.Forms.Label();
            this.goin3 = new System.Windows.Forms.ComboBox();
            this.label342 = new System.Windows.Forms.Label();
            this.goin2 = new System.Windows.Forms.ComboBox();
            this.label343 = new System.Windows.Forms.Label();
            this.goin1 = new System.Windows.Forms.ComboBox();
            this.label344 = new System.Windows.Forms.Label();
            this.label345 = new System.Windows.Forms.Label();
            this.operationBGS = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.currentBGS = new System.Windows.Forms.ComboBox();
            this._automatPage = new System.Windows.Forms.TabPage();
            this.UROVgroupBox = new System.Windows.Forms.GroupBox();
            this._urovSelf = new System.Windows.Forms.CheckBox();
            this._urovBkCheck = new System.Windows.Forms.CheckBox();
            this._urovIcheck = new System.Windows.Forms.CheckBox();
            this._Iurov = new System.Windows.Forms.MaskedTextBox();
            this.label37 = new System.Windows.Forms.Label();
            this._urovBlock = new System.Windows.Forms.ComboBox();
            this._tUrov2 = new System.Windows.Forms.MaskedTextBox();
            this._tUrov1 = new System.Windows.Forms.MaskedTextBox();
            this._urovPusk = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.label96 = new System.Windows.Forms.Label();
            this._comandOtkl = new System.Windows.Forms.ComboBox();
            this._controlSolenoidCombo = new System.Windows.Forms.ComboBox();
            this._switchKontCep = new System.Windows.Forms.ComboBox();
            this._switchTUskor = new System.Windows.Forms.MaskedTextBox();
            this._switchImp = new System.Windows.Forms.MaskedTextBox();
            this._switchBlock = new System.Windows.Forms.ComboBox();
            this._switchError = new System.Windows.Forms.ComboBox();
            this._switchOn = new System.Windows.Forms.ComboBox();
            this.label51 = new System.Windows.Forms.Label();
            this._switchOff = new System.Windows.Forms.ComboBox();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.groupBox33 = new System.Windows.Forms.GroupBox();
            this._blockSDTU = new System.Windows.Forms.ComboBox();
            this._switchSDTU = new System.Windows.Forms.ComboBox();
            this._switchVnesh = new System.Windows.Forms.ComboBox();
            this._switchKey = new System.Windows.Forms.ComboBox();
            this._switchButtons = new System.Windows.Forms.ComboBox();
            this._switchVneshOff = new System.Windows.Forms.ComboBox();
            this._switchVneshOn = new System.Windows.Forms.ComboBox();
            this._switchKeyOff = new System.Windows.Forms.ComboBox();
            this._blockSDTUlabel = new System.Windows.Forms.Label();
            this._switchKeyOn = new System.Windows.Forms.ComboBox();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this._systemPage = new System.Windows.Forms.TabPage();
            this._signaturesForOscGroupBox = new System.Windows.Forms.GroupBox();
            this.signaturesOsc = new BEMN.Forms.SignaturesOscControl();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._oscChannelsGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._baseCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._oscSygnal = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.oscStartCb = new System.Windows.Forms.ComboBox();
            this.label65 = new System.Windows.Forms.Label();
            this._oscSizeTextBox = new System.Windows.Forms.MaskedTextBox();
            this._oscWriteLength = new System.Windows.Forms.MaskedTextBox();
            this._oscFix = new System.Windows.Forms.ComboBox();
            this._oscLength = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this._outputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox175 = new System.Windows.Forms.GroupBox();
            this._outputIndicatorsGrid = new System.Windows.Forms.DataGridView();
            this._outIndNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outIndTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.baseGreenColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.baseRedColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._out1IndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignal2Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this._fault7CheckBox = new System.Windows.Forms.CheckBox();
            this._vchsFaultLabel = new System.Windows.Forms.Label();
            this._fault6CheckBox = new System.Windows.Forms.CheckBox();
            this.label121 = new System.Windows.Forms.Label();
            this._fault5CheckBox = new System.Windows.Forms.CheckBox();
            this._fault4CheckBox = new System.Windows.Forms.CheckBox();
            this._fault3CheckBox = new System.Windows.Forms.CheckBox();
            this._fault2CheckBox = new System.Windows.Forms.CheckBox();
            this.label111 = new System.Windows.Forms.Label();
            this._fault1CheckBox = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this._impTB = new System.Windows.Forms.MaskedTextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.baseColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releWaitCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox54 = new System.Windows.Forms.GroupBox();
            this.label193 = new System.Windows.Forms.Label();
            this._fixErrorFCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this._resetAlarmCheckBox = new System.Windows.Forms.CheckBox();
            this._resetSystemCheckBox = new System.Windows.Forms.CheckBox();
            this.label110 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this._inputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox59 = new System.Windows.Forms.GroupBox();
            this._commandsDataGridView = new System.Windows.Forms.DataGridView();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._passCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._jsCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this._antiBounceDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._indComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label80 = new System.Windows.Forms.Label();
            this._grUst6ComboBox = new System.Windows.Forms.ComboBox();
            this.label79 = new System.Windows.Forms.Label();
            this._grUst5ComboBox = new System.Windows.Forms.ComboBox();
            this.label78 = new System.Windows.Forms.Label();
            this._grUst4ComboBox = new System.Windows.Forms.ComboBox();
            this.label77 = new System.Windows.Forms.Label();
            this._grUst3ComboBox = new System.Windows.Forms.ComboBox();
            this.label76 = new System.Windows.Forms.Label();
            this._grUst2ComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this._grUst1ComboBox = new System.Windows.Forms.ComboBox();
            this._setpointPage = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._inpAddCombo = new System.Windows.Forms.ComboBox();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this._resistCheck = new System.Windows.Forms.CheckBox();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this._applyCopySetpoinsButton = new System.Windows.Forms.Button();
            this._copySetpoinsGroupComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._setpointsTab = new System.Windows.Forms.TabControl();
            this._measureTransPage = new System.Windows.Forms.TabPage();
            this._paramEngineGroupBox = new System.Windows.Forms.GroupBox();
            this.label140 = new System.Windows.Forms.Label();
            this._inputQResetLabel = new System.Windows.Forms.Label();
            this._engineNreset = new System.Windows.Forms.ComboBox();
            this._engineQresetGr1 = new System.Windows.Forms.ComboBox();
            this._qBox = new System.Windows.Forms.MaskedTextBox();
            this.label135 = new System.Windows.Forms.Label();
            this._tCount = new System.Windows.Forms.MaskedTextBox();
            this.label92 = new System.Windows.Forms.Label();
            this._tStartBox = new System.Windows.Forms.MaskedTextBox();
            this.label48 = new System.Windows.Forms.Label();
            this._iStartBox = new System.Windows.Forms.MaskedTextBox();
            this._iStartLabel = new System.Windows.Forms.Label();
            this._engineIdv = new System.Windows.Forms.MaskedTextBox();
            this._idvLabel = new System.Windows.Forms.Label();
            this._engineCoolingTimeGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label170 = new System.Windows.Forms.Label();
            this._engineHeatingTimeGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label171 = new System.Windows.Forms.Label();
            this.passportDataGroup = new System.Windows.Forms.GroupBox();
            this._sn = new System.Windows.Forms.MaskedTextBox();
            this.label220 = new System.Windows.Forms.Label();
            this._kpdP = new System.Windows.Forms.MaskedTextBox();
            this.label221 = new System.Windows.Forms.Label();
            this._cosfP = new System.Windows.Forms.MaskedTextBox();
            this.label225 = new System.Windows.Forms.Label();
            this.label226 = new System.Windows.Forms.Label();
            this._ppd = new System.Windows.Forms.MaskedTextBox();
            this._ppdCombo = new System.Windows.Forms.ComboBox();
            this.groupBox48 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label139 = new System.Windows.Forms.Label();
            this._resetTnGr1 = new System.Windows.Forms.ComboBox();
            this._errorX1_comboGr1 = new System.Windows.Forms.ComboBox();
            this._faultTHn1Label = new System.Windows.Forms.Label();
            this._errorX_comboGr1 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this._i0u0CheckGr1 = new System.Windows.Forms.CheckBox();
            this._i2u2CheckGr1 = new System.Windows.Forms.CheckBox();
            this._tdContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this._errorL_comboGr1 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this._iMinContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label130 = new System.Windows.Forms.Label();
            this._uMinContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label125 = new System.Windows.Forms.Label();
            this._u0ContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label138 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this._u2ContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this._tsContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this._uDelContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label128 = new System.Windows.Forms.Label();
            this._iDelContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this._iMaxContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label123 = new System.Windows.Forms.Label();
            this._uMaxContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label120 = new System.Windows.Forms.Label();
            this._i0ContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label122 = new System.Windows.Forms.Label();
            this._i2ContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.omp2 = new System.Windows.Forms.Panel();
            this.label113 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this._xud5p = new System.Windows.Forms.MaskedTextBox();
            this._xud4p = new System.Windows.Forms.MaskedTextBox();
            this._xud3p = new System.Windows.Forms.MaskedTextBox();
            this.label158 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this._xud2p = new System.Windows.Forms.MaskedTextBox();
            this._xud1p = new System.Windows.Forms.MaskedTextBox();
            this.label119 = new System.Windows.Forms.Label();
            this.omp1 = new System.Windows.Forms.Panel();
            this.label115 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this._xud3s = new System.Windows.Forms.MaskedTextBox();
            this._l12 = new System.Windows.Forms.Label();
            this._xud5s = new System.Windows.Forms.MaskedTextBox();
            this._l13 = new System.Windows.Forms.Label();
            this._xud4s = new System.Windows.Forms.MaskedTextBox();
            this._xud2s = new System.Windows.Forms.MaskedTextBox();
            this._xud1s = new System.Windows.Forms.MaskedTextBox();
            this.label118 = new System.Windows.Forms.Label();
            this._OMPmode_comboGr1 = new System.Windows.Forms.ComboBox();
            this.label117 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this._l2 = new System.Windows.Forms.MaskedTextBox();
            this._l1 = new System.Windows.Forms.MaskedTextBox();
            this._l3 = new System.Windows.Forms.MaskedTextBox();
            this._l4 = new System.Windows.Forms.MaskedTextBox();
            this.label116 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._ucaLabel = new System.Windows.Forms.Label();
            this._ucaComboBox = new System.Windows.Forms.ComboBox();
            this._inputUcaComboBox = new System.Windows.Forms.ComboBox();
            this.label47 = new System.Windows.Forms.Label();
            this._KTHX1koef_comboGr1 = new System.Windows.Forms.ComboBox();
            this._KTHXkoef_comboGr1 = new System.Windows.Forms.ComboBox();
            this._kthn1XLabel = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this._kthn1Label = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this._KTHLkoef_comboGr1 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this._Uo_typeComboGr1 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this._KTHX1_BoxGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this._KTHX_BoxGr1 = new System.Windows.Forms.MaskedTextBox();
            this._KTHL_BoxGr1 = new System.Windows.Forms.MaskedTextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._inpIn = new System.Windows.Forms.ComboBox();
            this.label256 = new System.Windows.Forms.Label();
            this._polarIn1 = new System.Windows.Forms.ComboBox();
            this._polarIn = new System.Windows.Forms.ComboBox();
            this._polarIc = new System.Windows.Forms.ComboBox();
            this._polarIb = new System.Windows.Forms.ComboBox();
            this._polarIa = new System.Windows.Forms.ComboBox();
            this.label255 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this._alternationCB = new System.Windows.Forms.ComboBox();
            this.label53 = new System.Windows.Forms.Label();
            this._TT_typeComboGr1 = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._ITTX_BoxGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._ITTL_BoxGr1 = new System.Windows.Forms.MaskedTextBox();
            this._Im_BoxGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this._defences = new System.Windows.Forms.TabPage();
            this._defencesTabControl = new System.Windows.Forms.TabControl();
            this._distDefence = new System.Windows.Forms.TabPage();
            this.resistanceDefTabCtr1 = new BEMN.MRUNIVERSAL.Configuration.ResistanceDefTabCtr();
            this._cornersDefence = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this._i2CornerGr1 = new System.Windows.Forms.MaskedTextBox();
            this._inCornerGr1 = new System.Windows.Forms.MaskedTextBox();
            this._i0CornerGr1 = new System.Windows.Forms.MaskedTextBox();
            this._iCornerGr1 = new System.Windows.Forms.MaskedTextBox();
            this._defenceI = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this._I14GroupBox = new System.Windows.Forms.GroupBox();
            this._difensesI1DataGrid = new System.Windows.Forms.DataGridView();
            this._iStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iIColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iUStartColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iUstartYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._blockOtTn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iDirectColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iUnDirectColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iLogicColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iCharColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iTColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iKColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._inUsk = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iTyColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iI2I1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iI2I1YNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._iBlockingDirectColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._nenaprUsk = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._iOscModeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iUROVModeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._iAPVModeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._avrColumnI14 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._ILGroupBox = new System.Windows.Forms.GroupBox();
            this._difensesI3DataGrid = new System.Windows.Forms.DataGridView();
            this._i8StageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i8ModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8IColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8UStartColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i8UstartYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8DirectColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8UnDirectColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8LogicColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8CharColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8TColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i8KColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8TyColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i8BlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8I2I1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i8I2I1YNColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8BlockingDirectColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i8OscModeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8UROVModeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i8APVModeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._avrColumnIL = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._I56GroupBox = new System.Windows.Forms.GroupBox();
            this._difensesI2DataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn88 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._conditionColumnI56 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn71 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn89 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn90 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn91 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn92 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn93 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn72 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn73 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn94 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn74 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn95 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn75 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn11 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn15 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn96 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn19 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn97 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._avrColumnI56 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._defenceIStar = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.groupBox60 = new System.Windows.Forms.GroupBox();
            this._difensesI0DataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn43 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn16 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn44 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn45 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn46 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn47 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn48 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn49 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn50 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn17 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn18 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn51 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._avrColumnIStar = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._defenceI2I1 = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._urovI2I1CheckBox = new System.Windows.Forms.CheckBox();
            this.i2i1AVR = new System.Windows.Forms.ComboBox();
            this.i2i1APV1 = new System.Windows.Forms.ComboBox();
            this.I2I1OscCombo = new System.Windows.Forms.ComboBox();
            this.I2I1tcpTB = new System.Windows.Forms.MaskedTextBox();
            this._avrI2I1Label = new System.Windows.Forms.Label();
            this.I2I1BlockingCombo = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label215 = new System.Windows.Forms.Label();
            this.I2I1TB = new System.Windows.Forms.MaskedTextBox();
            this.I2I1ModeCombo = new System.Windows.Forms.ComboBox();
            this.label216 = new System.Windows.Forms.Label();
            this._startArcDefence = new System.Windows.Forms.TabPage();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.OscArc = new System.Windows.Forms.CheckBox();
            this.StartBlockArcComboBox = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.IcpArcTextBox = new System.Windows.Forms.MaskedTextBox();
            this.StartArcModeComboBox = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this._defenceU = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.groupBox62 = new System.Windows.Forms.GroupBox();
            this._difensesUBDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn9 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn9 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn10 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn10 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._avrColumnUM = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox63 = new System.Windows.Forms.GroupBox();
            this._difensesUMDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn59 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn61 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn26 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn27 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn62 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn63 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn64 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn30 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn28 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn65 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn31 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._avrColumnUL = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._defenceF = new System.Windows.Forms.TabPage();
            this.panel10 = new System.Windows.Forms.Panel();
            this.groupBox64 = new System.Windows.Forms.GroupBox();
            this._difensesFBDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn66 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn67 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._u1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn32 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn68 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn69 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn35 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn33 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn70 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn36 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._avrColumnFM = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox65 = new System.Windows.Forms.GroupBox();
            this._difensesFMDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn71 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn72 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn61 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._u1Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn63 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn64 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn65 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn37 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn73 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn74 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn40 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn38 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn75 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn41 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._avrColumnFL = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._defenceQ = new System.Windows.Forms.TabPage();
            this._engineDefensesDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn86 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn87 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._engineDefenseAPVcol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._engineDefensesGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn84 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn105 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn85 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn106 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn107 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn108 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._avrColumnEngine = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.label217 = new System.Windows.Forms.Label();
            this.label218 = new System.Windows.Forms.Label();
            this._numHot = new System.Windows.Forms.MaskedTextBox();
            this._numCold = new System.Windows.Forms.MaskedTextBox();
            this.label219 = new System.Windows.Forms.Label();
            this._waitNumBlock = new System.Windows.Forms.MaskedTextBox();
            this.groupBox66 = new System.Windows.Forms.GroupBox();
            this.label222 = new System.Windows.Forms.Label();
            this.label223 = new System.Windows.Forms.Label();
            this._engineQconstraint = new System.Windows.Forms.MaskedTextBox();
            this.label224 = new System.Windows.Forms.Label();
            this._engineQtime = new System.Windows.Forms.MaskedTextBox();
            this._engineQmode = new System.Windows.Forms.ComboBox();
            this._externalDefence = new System.Windows.Forms.TabPage();
            this._externalDefenses = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn68 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn80 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn81 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn69 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn70 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn82 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn44 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn83 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn84 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn47 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn45 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn85 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn48 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._avrColumnExt = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._powerDefence = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._reversePowerGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._ssr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._usr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tsr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tvz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._svz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._svzbeno = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._isr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._block = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._blk = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._osc = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._apvvoz = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._urov = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._apv = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._resetstep = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._lsGr = new System.Windows.Forms.TabPage();
            this.groupBox56 = new System.Windows.Forms.GroupBox();
            this.treeViewForLsOR = new System.Windows.Forms.TreeView();
            this.groupBox55 = new System.Windows.Forms.GroupBox();
            this.treeViewForLsAND = new System.Windows.Forms.TreeView();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this._lsSecondTabControl = new System.Windows.Forms.TabControl();
            this.tabPage31 = new System.Windows.Forms.TabPage();
            this._lsOrDgv1Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn15 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage32 = new System.Windows.Forms.TabPage();
            this._lsOrDgv2Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn16 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage33 = new System.Windows.Forms.TabPage();
            this._lsOrDgv3Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn17 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage34 = new System.Windows.Forms.TabPage();
            this._lsOrDgv4Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn18 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage35 = new System.Windows.Forms.TabPage();
            this._lsOrDgv5Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn19 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage36 = new System.Windows.Forms.TabPage();
            this._lsOrDgv6Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn20 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage37 = new System.Windows.Forms.TabPage();
            this._lsOrDgv7Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn21 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage38 = new System.Windows.Forms.TabPage();
            this._lsOrDgv8Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn22 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox45 = new System.Windows.Forms.GroupBox();
            this._lsFirsTabControl = new System.Windows.Forms.TabControl();
            this.tabPage39 = new System.Windows.Forms.TabPage();
            this._lsAndDgv1Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn23 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage40 = new System.Windows.Forms.TabPage();
            this._lsAndDgv2Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn24 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage41 = new System.Windows.Forms.TabPage();
            this._lsAndDgv3Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn25 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage42 = new System.Windows.Forms.TabPage();
            this._lsAndDgv4Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn26 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage43 = new System.Windows.Forms.TabPage();
            this._lsAndDgv5Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn27 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage44 = new System.Windows.Forms.TabPage();
            this._lsAndDgv6Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn28 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage45 = new System.Windows.Forms.TabPage();
            this._lsAndDgv7Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn29 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage46 = new System.Windows.Forms.TabPage();
            this._lsAndDgv8Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn30 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._apvAvrTabPage = new System.Windows.Forms.TabPage();
            this._avrGroupBox = new System.Windows.Forms.GroupBox();
            this._avrReclouserGroupBox = new System.Windows.Forms.GroupBox();
            this._avrRecBlockSDTUCB = new System.Windows.Forms.ComboBox();
            this.label174 = new System.Windows.Forms.Label();
            this._avrRecBlockSelfCheck = new System.Windows.Forms.CheckBox();
            this.label181 = new System.Windows.Forms.Label();
            this._avrRecInputU2CB = new System.Windows.Forms.ComboBox();
            this._avrRecUminMTB = new System.Windows.Forms.MaskedTextBox();
            this.label179 = new System.Windows.Forms.Label();
            this.label182 = new System.Windows.Forms.Label();
            this._avrRecInputU1CB = new System.Windows.Forms.ComboBox();
            this._avrRecUmaxMTB = new System.Windows.Forms.MaskedTextBox();
            this.label180 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this._avrRecBanComboBox = new System.Windows.Forms.ComboBox();
            this.label178 = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            this._avrReadyRecMTB = new System.Windows.Forms.MaskedTextBox();
            this._avrRecKeyComboBox = new System.Windows.Forms.ComboBox();
            this._tBlockRecMTB = new System.Windows.Forms.MaskedTextBox();
            this.label175 = new System.Windows.Forms.Label();
            this._avrRecModeComboBox = new System.Windows.Forms.ComboBox();
            this.label173 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this._avrTypeComboBox = new System.Windows.Forms.ComboBox();
            this._avrSectionGroupBox = new System.Windows.Forms.GroupBox();
            this.label153 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this._avrBySignal = new System.Windows.Forms.ComboBox();
            this._avrClear = new System.Windows.Forms.ComboBox();
            this._avrByOff = new System.Windows.Forms.ComboBox();
            this.label155 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this._avrTOff = new System.Windows.Forms.MaskedTextBox();
            this._avrBySelfOff = new System.Windows.Forms.ComboBox();
            this.label156 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this._avrTBack = new System.Windows.Forms.MaskedTextBox();
            this._avrByDiff = new System.Windows.Forms.ComboBox();
            this.label91 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this._avrBack = new System.Windows.Forms.ComboBox();
            this._avrSIGNOn = new System.Windows.Forms.ComboBox();
            this.label160 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this._avrTSr = new System.Windows.Forms.MaskedTextBox();
            this._avrBlocking = new System.Windows.Forms.ComboBox();
            this.label161 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this._avrResolve = new System.Windows.Forms.ComboBox();
            this._avrBlockClear = new System.Windows.Forms.ComboBox();
            this.label163 = new System.Windows.Forms.Label();
            this._apvGroupBox = new System.Windows.Forms.GroupBox();
            this._blokFromUrov = new System.Windows.Forms.CheckBox();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this._apvKrat4Gr1 = new System.Windows.Forms.MaskedTextBox();
            this._apvKrat3Gr1 = new System.Windows.Forms.MaskedTextBox();
            this.label144 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this._apvSwitchOffGr1 = new System.Windows.Forms.ComboBox();
            this._apvKrat2Gr1 = new System.Windows.Forms.MaskedTextBox();
            this._apvKrat1Gr1 = new System.Windows.Forms.MaskedTextBox();
            this._apvTreadyGr1 = new System.Windows.Forms.MaskedTextBox();
            this._apvTblockGr1 = new System.Windows.Forms.MaskedTextBox();
            this._apvBlockGr1 = new System.Windows.Forms.ComboBox();
            this._timeDisable = new System.Windows.Forms.MaskedTextBox();
            this.label87 = new System.Windows.Forms.Label();
            this._typeDisable = new System.Windows.Forms.ComboBox();
            this.label95 = new System.Windows.Forms.Label();
            this._disableApv = new System.Windows.Forms.ComboBox();
            this.label149 = new System.Windows.Forms.Label();
            this._apvModeGr1 = new System.Windows.Forms.ComboBox();
            this.label150 = new System.Windows.Forms.Label();
            this._ksiunpTabPage = new System.Windows.Forms.TabPage();
            this.label82 = new System.Windows.Forms.Label();
            this.groupBox39 = new System.Windows.Forms.GroupBox();
            this._blockTNauto = new System.Windows.Forms.CheckBox();
            this.groupBox40 = new System.Windows.Forms.GroupBox();
            this._catchSinchrAuto = new System.Windows.Forms.CheckBox();
            this._sinhrAutodFnoGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox41 = new System.Windows.Forms.GroupBox();
            this._waitSinchrAuto = new System.Windows.Forms.CheckBox();
            this._sinhrAutodFGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this._sinhrAutodFiGr1 = new System.Windows.Forms.MaskedTextBox();
            this.groupBox42 = new System.Windows.Forms.GroupBox();
            this._sinhrAutoNoNoGr1 = new System.Windows.Forms.ComboBox();
            this._sinhrAutoYesNoGr1 = new System.Windows.Forms.ComboBox();
            this._sinhrAutoNoYesGr1 = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this._sinhrAutoUmaxGr1 = new System.Windows.Forms.MaskedTextBox();
            this._sinhrAutoModeGr1 = new System.Windows.Forms.ComboBox();
            this._dUmaxAutoLabel = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.groupBox35 = new System.Windows.Forms.GroupBox();
            this._blockTNmanual = new System.Windows.Forms.CheckBox();
            this.groupBox38 = new System.Windows.Forms.GroupBox();
            this._catchSinchrManual = new System.Windows.Forms.CheckBox();
            this._sinhrManualdFnoGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.groupBox37 = new System.Windows.Forms.GroupBox();
            this._waitSinchrManual = new System.Windows.Forms.CheckBox();
            this._sinhrManualdFGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this._sinhrManualdFiGr1 = new System.Windows.Forms.MaskedTextBox();
            this.groupBox36 = new System.Windows.Forms.GroupBox();
            this._sinhrManualNoNoGr1 = new System.Windows.Forms.ComboBox();
            this._sinhrManualYesNoGr1 = new System.Windows.Forms.ComboBox();
            this._sinhrManualNoYesGr1 = new System.Windows.Forms.ComboBox();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this._sinhrManualUmaxGr1 = new System.Windows.Forms.MaskedTextBox();
            this._sinhrManualModeGr1 = new System.Windows.Forms.ComboBox();
            this._dUmaxManualLabel = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.groupBox34 = new System.Windows.Forms.GroupBox();
            this._discretIn3Cmb = new System.Windows.Forms.ComboBox();
            this.label85 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this._sinhrF = new System.Windows.Forms.MaskedTextBox();
            this._discretIn2Cmb = new System.Windows.Forms.ComboBox();
            this._sinhrKamp = new System.Windows.Forms.MaskedTextBox();
            this._sinhrTonGr1 = new System.Windows.Forms.MaskedTextBox();
            this._discretIn1Cmb = new System.Windows.Forms.ComboBox();
            this.label151 = new System.Windows.Forms.Label();
            this._blockSinhCmb = new System.Windows.Forms.ComboBox();
            this._sinhrTsinhrGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label141 = new System.Windows.Forms.Label();
            this._blockSinhLabel = new System.Windows.Forms.Label();
            this._sinhrTwaitGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this._sinhrUmaxNalGr1 = new System.Windows.Forms.MaskedTextBox();
            this._sinhrUminNalGr1 = new System.Windows.Forms.MaskedTextBox();
            this._sinhrUminOtsGr1 = new System.Windows.Forms.MaskedTextBox();
            this._sinhrU2Gr1 = new System.Windows.Forms.ComboBox();
            this._sinhrU1Gr1 = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this._tuAndTbTabPage = new System.Windows.Forms.TabPage();
            this.tabControl100 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox50 = new System.Windows.Forms.GroupBox();
            this.konturFN = new System.Windows.Forms.CheckBox();
            this.konturFF = new System.Windows.Forms.CheckBox();
            this.konturFNGroup = new System.Windows.Forms.GroupBox();
            this.abbrevDZFN = new System.Windows.Forms.ComboBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label183 = new System.Windows.Forms.Label();
            this.reverseDZFN = new System.Windows.Forms.ComboBox();
            this.extendDZFN = new System.Windows.Forms.ComboBox();
            this.konturFFGroup = new System.Windows.Forms.GroupBox();
            this.abbrevDZFF = new System.Windows.Forms.ComboBox();
            this.label184 = new System.Windows.Forms.Label();
            this.label185 = new System.Windows.Forms.Label();
            this.label186 = new System.Windows.Forms.Label();
            this.reverseDZFF = new System.Windows.Forms.ComboBox();
            this.extendDZFF = new System.Windows.Forms.ComboBox();
            this.modeDZ = new System.Windows.Forms.ComboBox();
            this.label187 = new System.Windows.Forms.Label();
            this.inpTSDZ = new System.Windows.Forms.ComboBox();
            this.label188 = new System.Windows.Forms.Label();
            this.label189 = new System.Windows.Forms.Label();
            this.label190 = new System.Windows.Forms.Label();
            this.label191 = new System.Windows.Forms.Label();
            this.apvDZ = new System.Windows.Forms.ComboBox();
            this.urovDZ = new System.Windows.Forms.ComboBox();
            this.OscDZ = new System.Windows.Forms.ComboBox();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.zoneDZ = new System.Windows.Forms.CheckBox();
            this.tficsDZ = new System.Windows.Forms.MaskedTextBox();
            this.treverseDZ = new System.Windows.Forms.MaskedTextBox();
            this.label192 = new System.Windows.Forms.Label();
            this.label194 = new System.Windows.Forms.Label();
            this.label195 = new System.Windows.Forms.Label();
            this.label196 = new System.Windows.Forms.Label();
            this.label197 = new System.Windows.Forms.Label();
            this.blockReverseDZ = new System.Windows.Forms.ComboBox();
            this.modeReverseDZ = new System.Windows.Forms.ComboBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.uminDZ = new System.Windows.Forms.MaskedTextBox();
            this.tsrabTSDZ = new System.Windows.Forms.MaskedTextBox();
            this.label198 = new System.Windows.Forms.Label();
            this.label199 = new System.Windows.Forms.Label();
            this.label200 = new System.Windows.Forms.Label();
            this.label201 = new System.Windows.Forms.Label();
            this.label202 = new System.Windows.Forms.Label();
            this.blockTnDZ = new System.Windows.Forms.ComboBox();
            this.blockEchoDZ = new System.Windows.Forms.ComboBox();
            this.modeEchoDZ = new System.Windows.Forms.ComboBox();
            this.groupBox44 = new System.Windows.Forms.GroupBox();
            this.toffTBDZ = new System.Windows.Forms.MaskedTextBox();
            this.toffDZ = new System.Windows.Forms.MaskedTextBox();
            this.tminImpDZ = new System.Windows.Forms.MaskedTextBox();
            this.tsrabDebDZ = new System.Windows.Forms.MaskedTextBox();
            this.tvzTSDZ = new System.Windows.Forms.MaskedTextBox();
            this.label203 = new System.Windows.Forms.Label();
            this.label204 = new System.Windows.Forms.Label();
            this.label205 = new System.Windows.Forms.Label();
            this.label206 = new System.Windows.Forms.Label();
            this.label207 = new System.Windows.Forms.Label();
            this.label208 = new System.Windows.Forms.Label();
            this.label209 = new System.Windows.Forms.Label();
            this.label210 = new System.Windows.Forms.Label();
            this.label211 = new System.Windows.Forms.Label();
            this.blockSwithDZ = new System.Windows.Forms.ComboBox();
            this.deblockDZ = new System.Windows.Forms.ComboBox();
            this.controlDZ = new System.Windows.Forms.ComboBox();
            this.blockTSDZ = new System.Windows.Forms.ComboBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox47 = new System.Windows.Forms.GroupBox();
            this.groupBox52 = new System.Windows.Forms.GroupBox();
            this.abbrevDZNP = new System.Windows.Forms.ComboBox();
            this.label227 = new System.Windows.Forms.Label();
            this.label228 = new System.Windows.Forms.Label();
            this.label229 = new System.Windows.Forms.Label();
            this.reverseDZNP = new System.Windows.Forms.ComboBox();
            this.extendDZNP = new System.Windows.Forms.ComboBox();
            this.modeDZNP = new System.Windows.Forms.ComboBox();
            this.label230 = new System.Windows.Forms.Label();
            this.inpTSDZNP = new System.Windows.Forms.ComboBox();
            this.label231 = new System.Windows.Forms.Label();
            this.label232 = new System.Windows.Forms.Label();
            this.label233 = new System.Windows.Forms.Label();
            this.label234 = new System.Windows.Forms.Label();
            this.apvDZNP = new System.Windows.Forms.ComboBox();
            this.urovDZNP = new System.Windows.Forms.ComboBox();
            this.oscDZNP = new System.Windows.Forms.ComboBox();
            this.directionDZNP = new System.Windows.Forms.ComboBox();
            this.label235 = new System.Windows.Forms.Label();
            this.groupBox53 = new System.Windows.Forms.GroupBox();
            this.toffTBDZNP = new System.Windows.Forms.MaskedTextBox();
            this.toffDZNP = new System.Windows.Forms.MaskedTextBox();
            this.tminImpDZNP = new System.Windows.Forms.MaskedTextBox();
            this.tsrabDebDZNP = new System.Windows.Forms.MaskedTextBox();
            this.tvzTSDZNP = new System.Windows.Forms.MaskedTextBox();
            this.label236 = new System.Windows.Forms.Label();
            this.label237 = new System.Windows.Forms.Label();
            this.label238 = new System.Windows.Forms.Label();
            this.label239 = new System.Windows.Forms.Label();
            this.label240 = new System.Windows.Forms.Label();
            this.label241 = new System.Windows.Forms.Label();
            this.label242 = new System.Windows.Forms.Label();
            this.label243 = new System.Windows.Forms.Label();
            this.label244 = new System.Windows.Forms.Label();
            this.blockSwitchDZNP = new System.Windows.Forms.ComboBox();
            this.deblockDZNP = new System.Windows.Forms.ComboBox();
            this.controlDZNP = new System.Windows.Forms.ComboBox();
            this.blockTSDZNP = new System.Windows.Forms.ComboBox();
            this.groupBox46 = new System.Windows.Forms.GroupBox();
            this.zoneDZNP = new System.Windows.Forms.CheckBox();
            this.tficsDZNP = new System.Windows.Forms.MaskedTextBox();
            this.treverseDZNP = new System.Windows.Forms.MaskedTextBox();
            this.label245 = new System.Windows.Forms.Label();
            this.label246 = new System.Windows.Forms.Label();
            this.label247 = new System.Windows.Forms.Label();
            this.label248 = new System.Windows.Forms.Label();
            this.label249 = new System.Windows.Forms.Label();
            this.blockReverseDZNP = new System.Windows.Forms.ComboBox();
            this.modeReverseDZNP = new System.Windows.Forms.ComboBox();
            this.groupBox58 = new System.Windows.Forms.GroupBox();
            this.uminDZNP = new System.Windows.Forms.MaskedTextBox();
            this.tsrabTSDZNP = new System.Windows.Forms.MaskedTextBox();
            this.label250 = new System.Windows.Forms.Label();
            this.label251 = new System.Windows.Forms.Label();
            this.label252 = new System.Windows.Forms.Label();
            this.label253 = new System.Windows.Forms.Label();
            this.label254 = new System.Windows.Forms.Label();
            this.blockTnDZNP = new System.Windows.Forms.ComboBox();
            this.blockEchoDZNP = new System.Windows.Forms.ComboBox();
            this.modeEchoDZNP = new System.Windows.Forms.ComboBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._setpointsComboBox = new System.Windows.Forms.ComboBox();
            this.dataGridViewTextBoxColumn66 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn76 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn67 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn77 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn78 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn42 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn43 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn79 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._configurationTabControl = new System.Windows.Forms.TabControl();
            this._vlsTab = new System.Windows.Forms.TabPage();
            this.groupBox57 = new System.Windows.Forms.GroupBox();
            this.treeViewForVLS = new System.Windows.Forms.TreeView();
            this.groupBox49 = new System.Windows.Forms.GroupBox();
            this._vlsTabControl = new System.Windows.Forms.TabControl();
            this.VLS1 = new System.Windows.Forms.TabPage();
            this.VLSclb1Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS2 = new System.Windows.Forms.TabPage();
            this.VLSclb2Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS3 = new System.Windows.Forms.TabPage();
            this.VLSclb3Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS4 = new System.Windows.Forms.TabPage();
            this.VLSclb4Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS5 = new System.Windows.Forms.TabPage();
            this.VLSclb5Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS6 = new System.Windows.Forms.TabPage();
            this.VLSclb6Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS7 = new System.Windows.Forms.TabPage();
            this.VLSclb7Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS8 = new System.Windows.Forms.TabPage();
            this.VLSclb8Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS9 = new System.Windows.Forms.TabPage();
            this.VLSclb9Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS10 = new System.Windows.Forms.TabPage();
            this.VLSclb10Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS11 = new System.Windows.Forms.TabPage();
            this.VLSclb11Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS12 = new System.Windows.Forms.TabPage();
            this.VLSclb12Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS13 = new System.Windows.Forms.TabPage();
            this.VLSclb13Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS14 = new System.Windows.Forms.TabPage();
            this.VLSclb14Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS15 = new System.Windows.Forms.TabPage();
            this.VLSclb15Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS16 = new System.Windows.Forms.TabPage();
            this.VLSclb16Gr1 = new System.Windows.Forms.CheckedListBox();
            this._logicTabPage = new System.Windows.Forms.TabPage();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this._rsTriggersDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn83 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn102 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._baseRColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._sygnalsRColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._baseSColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._sColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this._virtualReleDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn81 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn99 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn100 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn101 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn82 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ethernetPage = new System.Windows.Forms.TabPage();
            this._reserveGroupBox = new System.Windows.Forms.GroupBox();
            this._reserveCB = new System.Windows.Forms.ComboBox();
            this.groupBox51 = new System.Windows.Forms.GroupBox();
            this._ipLo1 = new System.Windows.Forms.MaskedTextBox();
            this._ipLo2 = new System.Windows.Forms.MaskedTextBox();
            this._ipHi1 = new System.Windows.Forms.MaskedTextBox();
            this._ipHi2 = new System.Windows.Forms.MaskedTextBox();
            this.label214 = new System.Windows.Forms.Label();
            this.label213 = new System.Windows.Forms.Label();
            this.label212 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this._signaturesSignalsPage = new System.Windows.Forms.TabPage();
            this.signaturesSignalsConfigControl = new BEMN.Forms.SignaturesSignalsConfigControl();
            this._externalDifSbrosColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifAPVRetColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifAPV1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifAPV = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifUROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifVozvrYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifVozvrColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifTvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifTsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifSrabColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this._gooseTabPage.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this._automatPage.SuspendLayout();
            this.UROVgroupBox.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.groupBox33.SuspendLayout();
            this._systemPage.SuspendLayout();
            this._signaturesForOscGroupBox.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._oscChannelsGrid)).BeginInit();
            this.groupBox3.SuspendLayout();
            this._outputSignalsPage.SuspendLayout();
            this.groupBox175.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).BeginInit();
            this.groupBox31.SuspendLayout();
            this.groupBox23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this.groupBox54.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this._inputSignalsPage.SuspendLayout();
            this.groupBox59.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._commandsDataGridView)).BeginInit();
            this.groupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._antiBounceDataGridView)).BeginInit();
            this.groupBox18.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this._setpointPage.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._setpointsTab.SuspendLayout();
            this._measureTransPage.SuspendLayout();
            this._paramEngineGroupBox.SuspendLayout();
            this.passportDataGroup.SuspendLayout();
            this.groupBox48.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.omp2.SuspendLayout();
            this.omp1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this._defences.SuspendLayout();
            this._defencesTabControl.SuspendLayout();
            this._distDefence.SuspendLayout();
            this._cornersDefence.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this._defenceI.SuspendLayout();
            this.panel7.SuspendLayout();
            this._I14GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesI1DataGrid)).BeginInit();
            this._ILGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesI3DataGrid)).BeginInit();
            this._I56GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesI2DataGrid)).BeginInit();
            this._defenceIStar.SuspendLayout();
            this.panel8.SuspendLayout();
            this.groupBox60.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesI0DataGrid)).BeginInit();
            this._defenceI2I1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this._startArcDefence.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this._defenceU.SuspendLayout();
            this.panel9.SuspendLayout();
            this.groupBox62.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesUBDataGrid)).BeginInit();
            this.groupBox63.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesUMDataGrid)).BeginInit();
            this._defenceF.SuspendLayout();
            this.panel10.SuspendLayout();
            this.groupBox64.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesFBDataGrid)).BeginInit();
            this.groupBox65.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesFMDataGrid)).BeginInit();
            this._defenceQ.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._engineDefensesDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._engineDefensesGrid)).BeginInit();
            this.groupBox20.SuspendLayout();
            this.groupBox66.SuspendLayout();
            this._externalDefence.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenses)).BeginInit();
            this._powerDefence.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._reversePowerGrid)).BeginInit();
            this._lsGr.SuspendLayout();
            this.groupBox56.SuspendLayout();
            this.groupBox55.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this._lsSecondTabControl.SuspendLayout();
            this.tabPage31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv1Gr1)).BeginInit();
            this.tabPage32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv2Gr1)).BeginInit();
            this.tabPage33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv3Gr1)).BeginInit();
            this.tabPage34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv4Gr1)).BeginInit();
            this.tabPage35.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv5Gr1)).BeginInit();
            this.tabPage36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv6Gr1)).BeginInit();
            this.tabPage37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv7Gr1)).BeginInit();
            this.tabPage38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv8Gr1)).BeginInit();
            this.groupBox45.SuspendLayout();
            this._lsFirsTabControl.SuspendLayout();
            this.tabPage39.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv1Gr1)).BeginInit();
            this.tabPage40.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv2Gr1)).BeginInit();
            this.tabPage41.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv3Gr1)).BeginInit();
            this.tabPage42.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv4Gr1)).BeginInit();
            this.tabPage43.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv5Gr1)).BeginInit();
            this.tabPage44.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv6Gr1)).BeginInit();
            this.tabPage45.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv7Gr1)).BeginInit();
            this.tabPage46.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv8Gr1)).BeginInit();
            this._apvAvrTabPage.SuspendLayout();
            this._avrGroupBox.SuspendLayout();
            this._avrReclouserGroupBox.SuspendLayout();
            this._avrSectionGroupBox.SuspendLayout();
            this._apvGroupBox.SuspendLayout();
            this._ksiunpTabPage.SuspendLayout();
            this.groupBox39.SuspendLayout();
            this.groupBox40.SuspendLayout();
            this.groupBox41.SuspendLayout();
            this.groupBox42.SuspendLayout();
            this.groupBox35.SuspendLayout();
            this.groupBox38.SuspendLayout();
            this.groupBox37.SuspendLayout();
            this.groupBox36.SuspendLayout();
            this.groupBox34.SuspendLayout();
            this._tuAndTbTabPage.SuspendLayout();
            this.tabControl100.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox50.SuspendLayout();
            this.konturFNGroup.SuspendLayout();
            this.konturFFGroup.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox44.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox47.SuspendLayout();
            this.groupBox52.SuspendLayout();
            this.groupBox53.SuspendLayout();
            this.groupBox46.SuspendLayout();
            this.groupBox58.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this._configurationTabControl.SuspendLayout();
            this._vlsTab.SuspendLayout();
            this.groupBox57.SuspendLayout();
            this.groupBox49.SuspendLayout();
            this._vlsTabControl.SuspendLayout();
            this.VLS1.SuspendLayout();
            this.VLS2.SuspendLayout();
            this.VLS3.SuspendLayout();
            this.VLS4.SuspendLayout();
            this.VLS5.SuspendLayout();
            this.VLS6.SuspendLayout();
            this.VLS7.SuspendLayout();
            this.VLS8.SuspendLayout();
            this.VLS9.SuspendLayout();
            this.VLS10.SuspendLayout();
            this.VLS11.SuspendLayout();
            this.VLS12.SuspendLayout();
            this.VLS13.SuspendLayout();
            this.VLS14.SuspendLayout();
            this.VLS15.SuspendLayout();
            this.VLS16.SuspendLayout();
            this._logicTabPage.SuspendLayout();
            this.groupBox28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rsTriggersDataGrid)).BeginInit();
            this.groupBox27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._virtualReleDataGrid)).BeginInit();
            this._ethernetPage.SuspendLayout();
            this._reserveGroupBox.SuspendLayout();
            this.groupBox51.SuspendLayout();
            this._signaturesSignalsPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.resetSetpointsItem,
            this.clearSetpointsItem,
            this.readFromFileItem,
            this.writeToFileItem,
            this.writeToHtmlItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(223, 158);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(222, 22);
            this.readFromDeviceItem.Text = "Прочитать из устройства";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(222, 22);
            this.writeToDeviceItem.Text = "Записать в устройство";
            // 
            // resetSetpointsItem
            // 
            this.resetSetpointsItem.Name = "resetSetpointsItem";
            this.resetSetpointsItem.Size = new System.Drawing.Size(222, 22);
            this.resetSetpointsItem.Text = "Загрузить базовые уставки";
            // 
            // clearSetpointsItem
            // 
            this.clearSetpointsItem.Name = "clearSetpointsItem";
            this.clearSetpointsItem.Size = new System.Drawing.Size(222, 22);
            this.clearSetpointsItem.Text = "Обнулить уставки";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(222, 22);
            this.readFromFileItem.Text = "Загрузить из файла";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(222, 22);
            this.writeToFileItem.Text = "Сохранить в файл";
            // 
            // writeToHtmlItem
            // 
            this.writeToHtmlItem.Name = "writeToHtmlItem";
            this.writeToHtmlItem.Size = new System.Drawing.Size(222, 22);
            this.writeToHtmlItem.Text = "Сохранить в HTML";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(6, 116);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(169, 13);
            this.label109.TabIndex = 19;
            this.label109.Text = "5. Неисправность выключателя";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this._saveToXmlButton);
            this.panel1.Controls.Add(this._clearSetpointBtn);
            this.panel1.Controls.Add(this._resetSetpointsButton);
            this.panel1.Controls.Add(this._writeConfigBut);
            this.panel1.Controls.Add(this.statusStrip1);
            this.panel1.Controls.Add(this._readConfigBut);
            this.panel1.Controls.Add(this._saveConfigBut);
            this.panel1.Controls.Add(this._loadConfigBut);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 582);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(989, 52);
            this.panel1.TabIndex = 33;
            // 
            // _saveToXmlButton
            // 
            this._saveToXmlButton.AllowDrop = true;
            this._saveToXmlButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveToXmlButton.AutoSize = true;
            this._saveToXmlButton.Location = new System.Drawing.Point(849, 5);
            this._saveToXmlButton.Name = "_saveToXmlButton";
            this._saveToXmlButton.Size = new System.Drawing.Size(136, 23);
            this._saveToXmlButton.TabIndex = 31;
            this._saveToXmlButton.Text = "Сохранить в HTML";
            this._toolTip.SetToolTip(this._saveToXmlButton, "Сохранить конфигурацию в HTML файл");
            this._saveToXmlButton.UseVisualStyleBackColor = true;
            this._saveToXmlButton.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _clearSetpointBtn
            // 
            this._clearSetpointBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._clearSetpointBtn.AutoSize = true;
            this._clearSetpointBtn.Location = new System.Drawing.Point(448, 5);
            this._clearSetpointBtn.Name = "_clearSetpointBtn";
            this._clearSetpointBtn.Size = new System.Drawing.Size(123, 23);
            this._clearSetpointBtn.TabIndex = 30;
            this._clearSetpointBtn.Text = "Обнулить уставки";
            this._clearSetpointBtn.UseVisualStyleBackColor = true;
            this._clearSetpointBtn.Click += new System.EventHandler(this._clearSetpointsButton_Click);
            // 
            // _resetSetpointsButton
            // 
            this._resetSetpointsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._resetSetpointsButton.AutoSize = true;
            this._resetSetpointsButton.Location = new System.Drawing.Point(298, 5);
            this._resetSetpointsButton.Name = "_resetSetpointsButton";
            this._resetSetpointsButton.Size = new System.Drawing.Size(144, 23);
            this._resetSetpointsButton.TabIndex = 30;
            this._resetSetpointsButton.Text = "Загрузить баз. уставки";
            this._toolTip.SetToolTip(this._resetSetpointsButton, "Загрузить базовые уставки");
            this._resetSetpointsButton.UseVisualStyleBackColor = true;
            this._resetSetpointsButton.Click += new System.EventHandler(this._resetSetpointsButton_Click);
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeConfigBut.AutoSize = true;
            this._writeConfigBut.Location = new System.Drawing.Point(158, 5);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(134, 23);
            this._writeConfigBut.TabIndex = 27;
            this._writeConfigBut.Text = "Записать в устройство";
            this._toolTip.SetToolTip(this._writeConfigBut, "Записать конфигурацию в устройство (CTRL+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this._progressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 30);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(989, 22);
            this.statusStrip1.TabIndex = 26;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // _progressBar
            // 
            this._progressBar.Maximum = 90;
            this._progressBar.Name = "_progressBar";
            this._progressBar.Size = new System.Drawing.Size(100, 16);
            this._progressBar.Step = 1;
            this._progressBar.Visible = false;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readConfigBut.AutoSize = true;
            this._readConfigBut.Location = new System.Drawing.Point(3, 5);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(151, 23);
            this._readConfigBut.TabIndex = 26;
            this._readConfigBut.Text = "Прочитать из устройства";
            this._toolTip.SetToolTip(this._readConfigBut, "Прочитать конфигурацию из устройства (CTRL+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.AllowDrop = true;
            this._saveConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveConfigBut.AutoSize = true;
            this._saveConfigBut.Location = new System.Drawing.Point(734, 5);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(109, 23);
            this._saveConfigBut.TabIndex = 29;
            this._saveConfigBut.Text = "Сохранить в файл";
            this._toolTip.SetToolTip(this._saveConfigBut, "Сохранить конфигурацию в файл (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._loadConfigBut.AutoSize = true;
            this._loadConfigBut.Location = new System.Drawing.Point(604, 5);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(124, 23);
            this._loadConfigBut.TabIndex = 28;
            this._loadConfigBut.Text = "Загрузить из файла";
            this._toolTip.SetToolTip(this._loadConfigBut, "Загрузить конфигурацию из файла (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "xml";
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "xml";
            this._openConfigurationDlg.RestoreDirectory = true;
            // 
            // _saveXmlDialog
            // 
            this._saveXmlDialog.Filter = "(*.html) | *.html";
            // 
            // saveFileResistCharacteristic
            // 
            this.saveFileResistCharacteristic.Filter = "(*.pco) | *.pco";
            // 
            // _gooseTabPage
            // 
            this._gooseTabPage.Controls.Add(this.groupBox14);
            this._gooseTabPage.Controls.Add(this.label36);
            this._gooseTabPage.Controls.Add(this.currentBGS);
            this._gooseTabPage.Location = new System.Drawing.Point(4, 22);
            this._gooseTabPage.Name = "_gooseTabPage";
            this._gooseTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._gooseTabPage.Size = new System.Drawing.Size(981, 553);
            this._gooseTabPage.TabIndex = 11;
            this._gooseTabPage.Text = "БГС";
            this._gooseTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.groupBox19);
            this.groupBox14.Controls.Add(this.label345);
            this.groupBox14.Controls.Add(this.operationBGS);
            this.groupBox14.Location = new System.Drawing.Point(6, 36);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(950, 511);
            this.groupBox14.TabIndex = 9;
            this.groupBox14.TabStop = false;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.goin64);
            this.groupBox19.Controls.Add(this.goin48);
            this.groupBox19.Controls.Add(this.goin32);
            this.groupBox19.Controls.Add(this.label281);
            this.groupBox19.Controls.Add(this.label282);
            this.groupBox19.Controls.Add(this.label283);
            this.groupBox19.Controls.Add(this.goin63);
            this.groupBox19.Controls.Add(this.goin47);
            this.groupBox19.Controls.Add(this.goin31);
            this.groupBox19.Controls.Add(this.label284);
            this.groupBox19.Controls.Add(this.label285);
            this.groupBox19.Controls.Add(this.label286);
            this.groupBox19.Controls.Add(this.goin62);
            this.groupBox19.Controls.Add(this.goin46);
            this.groupBox19.Controls.Add(this.goin30);
            this.groupBox19.Controls.Add(this.label287);
            this.groupBox19.Controls.Add(this.label288);
            this.groupBox19.Controls.Add(this.label289);
            this.groupBox19.Controls.Add(this.goin61);
            this.groupBox19.Controls.Add(this.goin45);
            this.groupBox19.Controls.Add(this.goin29);
            this.groupBox19.Controls.Add(this.label290);
            this.groupBox19.Controls.Add(this.label291);
            this.groupBox19.Controls.Add(this.label292);
            this.groupBox19.Controls.Add(this.goin60);
            this.groupBox19.Controls.Add(this.goin44);
            this.groupBox19.Controls.Add(this.goin28);
            this.groupBox19.Controls.Add(this.label293);
            this.groupBox19.Controls.Add(this.label294);
            this.groupBox19.Controls.Add(this.label295);
            this.groupBox19.Controls.Add(this.goin59);
            this.groupBox19.Controls.Add(this.goin43);
            this.groupBox19.Controls.Add(this.goin27);
            this.groupBox19.Controls.Add(this.label296);
            this.groupBox19.Controls.Add(this.label297);
            this.groupBox19.Controls.Add(this.label298);
            this.groupBox19.Controls.Add(this.goin58);
            this.groupBox19.Controls.Add(this.goin42);
            this.groupBox19.Controls.Add(this.goin26);
            this.groupBox19.Controls.Add(this.label299);
            this.groupBox19.Controls.Add(this.label300);
            this.groupBox19.Controls.Add(this.label301);
            this.groupBox19.Controls.Add(this.goin57);
            this.groupBox19.Controls.Add(this.goin41);
            this.groupBox19.Controls.Add(this.goin25);
            this.groupBox19.Controls.Add(this.label302);
            this.groupBox19.Controls.Add(this.label303);
            this.groupBox19.Controls.Add(this.label304);
            this.groupBox19.Controls.Add(this.goin56);
            this.groupBox19.Controls.Add(this.goin40);
            this.groupBox19.Controls.Add(this.goin24);
            this.groupBox19.Controls.Add(this.label305);
            this.groupBox19.Controls.Add(this.label306);
            this.groupBox19.Controls.Add(this.label307);
            this.groupBox19.Controls.Add(this.goin55);
            this.groupBox19.Controls.Add(this.goin39);
            this.groupBox19.Controls.Add(this.goin23);
            this.groupBox19.Controls.Add(this.label308);
            this.groupBox19.Controls.Add(this.label309);
            this.groupBox19.Controls.Add(this.label310);
            this.groupBox19.Controls.Add(this.goin54);
            this.groupBox19.Controls.Add(this.goin38);
            this.groupBox19.Controls.Add(this.goin22);
            this.groupBox19.Controls.Add(this.label311);
            this.groupBox19.Controls.Add(this.label312);
            this.groupBox19.Controls.Add(this.label313);
            this.groupBox19.Controls.Add(this.goin53);
            this.groupBox19.Controls.Add(this.goin37);
            this.groupBox19.Controls.Add(this.goin21);
            this.groupBox19.Controls.Add(this.label314);
            this.groupBox19.Controls.Add(this.label315);
            this.groupBox19.Controls.Add(this.label316);
            this.groupBox19.Controls.Add(this.goin52);
            this.groupBox19.Controls.Add(this.goin36);
            this.groupBox19.Controls.Add(this.goin20);
            this.groupBox19.Controls.Add(this.label317);
            this.groupBox19.Controls.Add(this.label318);
            this.groupBox19.Controls.Add(this.label319);
            this.groupBox19.Controls.Add(this.goin51);
            this.groupBox19.Controls.Add(this.goin35);
            this.groupBox19.Controls.Add(this.goin19);
            this.groupBox19.Controls.Add(this.label320);
            this.groupBox19.Controls.Add(this.label321);
            this.groupBox19.Controls.Add(this.label322);
            this.groupBox19.Controls.Add(this.goin50);
            this.groupBox19.Controls.Add(this.goin34);
            this.groupBox19.Controls.Add(this.goin18);
            this.groupBox19.Controls.Add(this.label323);
            this.groupBox19.Controls.Add(this.label324);
            this.groupBox19.Controls.Add(this.label325);
            this.groupBox19.Controls.Add(this.goin49);
            this.groupBox19.Controls.Add(this.label326);
            this.groupBox19.Controls.Add(this.goin33);
            this.groupBox19.Controls.Add(this.label327);
            this.groupBox19.Controls.Add(this.goin17);
            this.groupBox19.Controls.Add(this.label328);
            this.groupBox19.Controls.Add(this.goin16);
            this.groupBox19.Controls.Add(this.label329);
            this.groupBox19.Controls.Add(this.goin15);
            this.groupBox19.Controls.Add(this.label330);
            this.groupBox19.Controls.Add(this.goin14);
            this.groupBox19.Controls.Add(this.label331);
            this.groupBox19.Controls.Add(this.goin13);
            this.groupBox19.Controls.Add(this.label332);
            this.groupBox19.Controls.Add(this.goin12);
            this.groupBox19.Controls.Add(this.label333);
            this.groupBox19.Controls.Add(this.goin11);
            this.groupBox19.Controls.Add(this.label334);
            this.groupBox19.Controls.Add(this.goin10);
            this.groupBox19.Controls.Add(this.label335);
            this.groupBox19.Controls.Add(this.goin9);
            this.groupBox19.Controls.Add(this.label336);
            this.groupBox19.Controls.Add(this.goin8);
            this.groupBox19.Controls.Add(this.label337);
            this.groupBox19.Controls.Add(this.goin7);
            this.groupBox19.Controls.Add(this.label338);
            this.groupBox19.Controls.Add(this.goin6);
            this.groupBox19.Controls.Add(this.label339);
            this.groupBox19.Controls.Add(this.goin5);
            this.groupBox19.Controls.Add(this.label340);
            this.groupBox19.Controls.Add(this.goin4);
            this.groupBox19.Controls.Add(this.label341);
            this.groupBox19.Controls.Add(this.goin3);
            this.groupBox19.Controls.Add(this.label342);
            this.groupBox19.Controls.Add(this.goin2);
            this.groupBox19.Controls.Add(this.label343);
            this.groupBox19.Controls.Add(this.goin1);
            this.groupBox19.Controls.Add(this.label344);
            this.groupBox19.Location = new System.Drawing.Point(6, 50);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(645, 455);
            this.groupBox19.TabIndex = 8;
            this.groupBox19.TabStop = false;
            // 
            // goin64
            // 
            this.goin64.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin64.FormattingEnabled = true;
            this.goin64.Location = new System.Drawing.Point(553, 424);
            this.goin64.Name = "goin64";
            this.goin64.Size = new System.Drawing.Size(75, 21);
            this.goin64.TabIndex = 33;
            // 
            // goin48
            // 
            this.goin48.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin48.FormattingEnabled = true;
            this.goin48.Location = new System.Drawing.Point(384, 424);
            this.goin48.Name = "goin48";
            this.goin48.Size = new System.Drawing.Size(75, 21);
            this.goin48.TabIndex = 33;
            // 
            // goin32
            // 
            this.goin32.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin32.FormattingEnabled = true;
            this.goin32.Location = new System.Drawing.Point(217, 424);
            this.goin32.Name = "goin32";
            this.goin32.Size = new System.Drawing.Size(75, 21);
            this.goin32.TabIndex = 33;
            // 
            // label281
            // 
            this.label281.AutoSize = true;
            this.label281.Location = new System.Drawing.Point(505, 427);
            this.label281.Name = "label281";
            this.label281.Size = new System.Drawing.Size(42, 13);
            this.label281.TabIndex = 14;
            this.label281.Text = "GoIn64";
            // 
            // label282
            // 
            this.label282.AutoSize = true;
            this.label282.Location = new System.Drawing.Point(336, 427);
            this.label282.Name = "label282";
            this.label282.Size = new System.Drawing.Size(42, 13);
            this.label282.TabIndex = 14;
            this.label282.Text = "GoIn48";
            // 
            // label283
            // 
            this.label283.AutoSize = true;
            this.label283.Location = new System.Drawing.Point(169, 427);
            this.label283.Name = "label283";
            this.label283.Size = new System.Drawing.Size(42, 13);
            this.label283.TabIndex = 14;
            this.label283.Text = "GoIn32";
            // 
            // goin63
            // 
            this.goin63.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin63.FormattingEnabled = true;
            this.goin63.Location = new System.Drawing.Point(553, 397);
            this.goin63.Name = "goin63";
            this.goin63.Size = new System.Drawing.Size(75, 21);
            this.goin63.TabIndex = 29;
            // 
            // goin47
            // 
            this.goin47.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin47.FormattingEnabled = true;
            this.goin47.Location = new System.Drawing.Point(384, 397);
            this.goin47.Name = "goin47";
            this.goin47.Size = new System.Drawing.Size(75, 21);
            this.goin47.TabIndex = 29;
            // 
            // goin31
            // 
            this.goin31.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin31.FormattingEnabled = true;
            this.goin31.Location = new System.Drawing.Point(217, 397);
            this.goin31.Name = "goin31";
            this.goin31.Size = new System.Drawing.Size(75, 21);
            this.goin31.TabIndex = 29;
            // 
            // label284
            // 
            this.label284.AutoSize = true;
            this.label284.Location = new System.Drawing.Point(505, 400);
            this.label284.Name = "label284";
            this.label284.Size = new System.Drawing.Size(42, 13);
            this.label284.TabIndex = 15;
            this.label284.Text = "GoIn63";
            // 
            // label285
            // 
            this.label285.AutoSize = true;
            this.label285.Location = new System.Drawing.Point(336, 400);
            this.label285.Name = "label285";
            this.label285.Size = new System.Drawing.Size(42, 13);
            this.label285.TabIndex = 15;
            this.label285.Text = "GoIn47";
            // 
            // label286
            // 
            this.label286.AutoSize = true;
            this.label286.Location = new System.Drawing.Point(169, 400);
            this.label286.Name = "label286";
            this.label286.Size = new System.Drawing.Size(42, 13);
            this.label286.TabIndex = 15;
            this.label286.Text = "GoIn31";
            // 
            // goin62
            // 
            this.goin62.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin62.FormattingEnabled = true;
            this.goin62.Location = new System.Drawing.Point(553, 370);
            this.goin62.Name = "goin62";
            this.goin62.Size = new System.Drawing.Size(75, 21);
            this.goin62.TabIndex = 25;
            // 
            // goin46
            // 
            this.goin46.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin46.FormattingEnabled = true;
            this.goin46.Location = new System.Drawing.Point(384, 370);
            this.goin46.Name = "goin46";
            this.goin46.Size = new System.Drawing.Size(75, 21);
            this.goin46.TabIndex = 25;
            // 
            // goin30
            // 
            this.goin30.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin30.FormattingEnabled = true;
            this.goin30.Location = new System.Drawing.Point(217, 370);
            this.goin30.Name = "goin30";
            this.goin30.Size = new System.Drawing.Size(75, 21);
            this.goin30.TabIndex = 25;
            // 
            // label287
            // 
            this.label287.AutoSize = true;
            this.label287.Location = new System.Drawing.Point(505, 373);
            this.label287.Name = "label287";
            this.label287.Size = new System.Drawing.Size(42, 13);
            this.label287.TabIndex = 17;
            this.label287.Text = "GoIn62";
            // 
            // label288
            // 
            this.label288.AutoSize = true;
            this.label288.Location = new System.Drawing.Point(336, 373);
            this.label288.Name = "label288";
            this.label288.Size = new System.Drawing.Size(42, 13);
            this.label288.TabIndex = 17;
            this.label288.Text = "GoIn46";
            // 
            // label289
            // 
            this.label289.AutoSize = true;
            this.label289.Location = new System.Drawing.Point(169, 373);
            this.label289.Name = "label289";
            this.label289.Size = new System.Drawing.Size(42, 13);
            this.label289.TabIndex = 17;
            this.label289.Text = "GoIn30";
            // 
            // goin61
            // 
            this.goin61.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin61.FormattingEnabled = true;
            this.goin61.Location = new System.Drawing.Point(553, 343);
            this.goin61.Name = "goin61";
            this.goin61.Size = new System.Drawing.Size(75, 21);
            this.goin61.TabIndex = 21;
            // 
            // goin45
            // 
            this.goin45.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin45.FormattingEnabled = true;
            this.goin45.Location = new System.Drawing.Point(384, 343);
            this.goin45.Name = "goin45";
            this.goin45.Size = new System.Drawing.Size(75, 21);
            this.goin45.TabIndex = 21;
            // 
            // goin29
            // 
            this.goin29.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin29.FormattingEnabled = true;
            this.goin29.Location = new System.Drawing.Point(217, 343);
            this.goin29.Name = "goin29";
            this.goin29.Size = new System.Drawing.Size(75, 21);
            this.goin29.TabIndex = 21;
            // 
            // label290
            // 
            this.label290.AutoSize = true;
            this.label290.Location = new System.Drawing.Point(505, 346);
            this.label290.Name = "label290";
            this.label290.Size = new System.Drawing.Size(42, 13);
            this.label290.TabIndex = 16;
            this.label290.Text = "GoIn61";
            // 
            // label291
            // 
            this.label291.AutoSize = true;
            this.label291.Location = new System.Drawing.Point(336, 346);
            this.label291.Name = "label291";
            this.label291.Size = new System.Drawing.Size(42, 13);
            this.label291.TabIndex = 16;
            this.label291.Text = "GoIn45";
            // 
            // label292
            // 
            this.label292.AutoSize = true;
            this.label292.Location = new System.Drawing.Point(169, 346);
            this.label292.Name = "label292";
            this.label292.Size = new System.Drawing.Size(42, 13);
            this.label292.TabIndex = 16;
            this.label292.Text = "GoIn29";
            // 
            // goin60
            // 
            this.goin60.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin60.FormattingEnabled = true;
            this.goin60.Location = new System.Drawing.Point(553, 316);
            this.goin60.Name = "goin60";
            this.goin60.Size = new System.Drawing.Size(75, 21);
            this.goin60.TabIndex = 27;
            // 
            // goin44
            // 
            this.goin44.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin44.FormattingEnabled = true;
            this.goin44.Location = new System.Drawing.Point(384, 316);
            this.goin44.Name = "goin44";
            this.goin44.Size = new System.Drawing.Size(75, 21);
            this.goin44.TabIndex = 27;
            // 
            // goin28
            // 
            this.goin28.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin28.FormattingEnabled = true;
            this.goin28.Location = new System.Drawing.Point(217, 316);
            this.goin28.Name = "goin28";
            this.goin28.Size = new System.Drawing.Size(75, 21);
            this.goin28.TabIndex = 27;
            // 
            // label293
            // 
            this.label293.AutoSize = true;
            this.label293.Location = new System.Drawing.Point(505, 319);
            this.label293.Name = "label293";
            this.label293.Size = new System.Drawing.Size(42, 13);
            this.label293.TabIndex = 13;
            this.label293.Text = "GoIn60";
            // 
            // label294
            // 
            this.label294.AutoSize = true;
            this.label294.Location = new System.Drawing.Point(336, 319);
            this.label294.Name = "label294";
            this.label294.Size = new System.Drawing.Size(42, 13);
            this.label294.TabIndex = 13;
            this.label294.Text = "GoIn44";
            // 
            // label295
            // 
            this.label295.AutoSize = true;
            this.label295.Location = new System.Drawing.Point(169, 319);
            this.label295.Name = "label295";
            this.label295.Size = new System.Drawing.Size(42, 13);
            this.label295.TabIndex = 13;
            this.label295.Text = "GoIn28";
            // 
            // goin59
            // 
            this.goin59.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin59.FormattingEnabled = true;
            this.goin59.Location = new System.Drawing.Point(553, 289);
            this.goin59.Name = "goin59";
            this.goin59.Size = new System.Drawing.Size(75, 21);
            this.goin59.TabIndex = 23;
            // 
            // goin43
            // 
            this.goin43.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin43.FormattingEnabled = true;
            this.goin43.Location = new System.Drawing.Point(384, 289);
            this.goin43.Name = "goin43";
            this.goin43.Size = new System.Drawing.Size(75, 21);
            this.goin43.TabIndex = 23;
            // 
            // goin27
            // 
            this.goin27.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin27.FormattingEnabled = true;
            this.goin27.Location = new System.Drawing.Point(217, 289);
            this.goin27.Name = "goin27";
            this.goin27.Size = new System.Drawing.Size(75, 21);
            this.goin27.TabIndex = 23;
            // 
            // label296
            // 
            this.label296.AutoSize = true;
            this.label296.Location = new System.Drawing.Point(505, 292);
            this.label296.Name = "label296";
            this.label296.Size = new System.Drawing.Size(42, 13);
            this.label296.TabIndex = 11;
            this.label296.Text = "GoIn59";
            // 
            // label297
            // 
            this.label297.AutoSize = true;
            this.label297.Location = new System.Drawing.Point(336, 292);
            this.label297.Name = "label297";
            this.label297.Size = new System.Drawing.Size(42, 13);
            this.label297.TabIndex = 11;
            this.label297.Text = "GoIn43";
            // 
            // label298
            // 
            this.label298.AutoSize = true;
            this.label298.Location = new System.Drawing.Point(169, 292);
            this.label298.Name = "label298";
            this.label298.Size = new System.Drawing.Size(42, 13);
            this.label298.TabIndex = 11;
            this.label298.Text = "GoIn27";
            // 
            // goin58
            // 
            this.goin58.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin58.FormattingEnabled = true;
            this.goin58.Location = new System.Drawing.Point(553, 262);
            this.goin58.Name = "goin58";
            this.goin58.Size = new System.Drawing.Size(75, 21);
            this.goin58.TabIndex = 31;
            // 
            // goin42
            // 
            this.goin42.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin42.FormattingEnabled = true;
            this.goin42.Location = new System.Drawing.Point(384, 262);
            this.goin42.Name = "goin42";
            this.goin42.Size = new System.Drawing.Size(75, 21);
            this.goin42.TabIndex = 31;
            // 
            // goin26
            // 
            this.goin26.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin26.FormattingEnabled = true;
            this.goin26.Location = new System.Drawing.Point(217, 262);
            this.goin26.Name = "goin26";
            this.goin26.Size = new System.Drawing.Size(75, 21);
            this.goin26.TabIndex = 31;
            // 
            // label299
            // 
            this.label299.AutoSize = true;
            this.label299.Location = new System.Drawing.Point(505, 265);
            this.label299.Name = "label299";
            this.label299.Size = new System.Drawing.Size(42, 13);
            this.label299.TabIndex = 10;
            this.label299.Text = "GoIn58";
            // 
            // label300
            // 
            this.label300.AutoSize = true;
            this.label300.Location = new System.Drawing.Point(336, 265);
            this.label300.Name = "label300";
            this.label300.Size = new System.Drawing.Size(42, 13);
            this.label300.TabIndex = 10;
            this.label300.Text = "GoIn42";
            // 
            // label301
            // 
            this.label301.AutoSize = true;
            this.label301.Location = new System.Drawing.Point(169, 265);
            this.label301.Name = "label301";
            this.label301.Size = new System.Drawing.Size(42, 13);
            this.label301.TabIndex = 10;
            this.label301.Text = "GoIn26";
            // 
            // goin57
            // 
            this.goin57.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin57.FormattingEnabled = true;
            this.goin57.Location = new System.Drawing.Point(553, 235);
            this.goin57.Name = "goin57";
            this.goin57.Size = new System.Drawing.Size(75, 21);
            this.goin57.TabIndex = 19;
            // 
            // goin41
            // 
            this.goin41.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin41.FormattingEnabled = true;
            this.goin41.Location = new System.Drawing.Point(384, 235);
            this.goin41.Name = "goin41";
            this.goin41.Size = new System.Drawing.Size(75, 21);
            this.goin41.TabIndex = 19;
            // 
            // goin25
            // 
            this.goin25.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin25.FormattingEnabled = true;
            this.goin25.Location = new System.Drawing.Point(217, 235);
            this.goin25.Name = "goin25";
            this.goin25.Size = new System.Drawing.Size(75, 21);
            this.goin25.TabIndex = 19;
            // 
            // label302
            // 
            this.label302.AutoSize = true;
            this.label302.Location = new System.Drawing.Point(505, 238);
            this.label302.Name = "label302";
            this.label302.Size = new System.Drawing.Size(42, 13);
            this.label302.TabIndex = 2;
            this.label302.Text = "GoIn57";
            // 
            // label303
            // 
            this.label303.AutoSize = true;
            this.label303.Location = new System.Drawing.Point(336, 238);
            this.label303.Name = "label303";
            this.label303.Size = new System.Drawing.Size(42, 13);
            this.label303.TabIndex = 2;
            this.label303.Text = "GoIn41";
            // 
            // label304
            // 
            this.label304.AutoSize = true;
            this.label304.Location = new System.Drawing.Point(169, 238);
            this.label304.Name = "label304";
            this.label304.Size = new System.Drawing.Size(42, 13);
            this.label304.TabIndex = 2;
            this.label304.Text = "GoIn25";
            // 
            // goin56
            // 
            this.goin56.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin56.FormattingEnabled = true;
            this.goin56.Location = new System.Drawing.Point(553, 208);
            this.goin56.Name = "goin56";
            this.goin56.Size = new System.Drawing.Size(75, 21);
            this.goin56.TabIndex = 18;
            // 
            // goin40
            // 
            this.goin40.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin40.FormattingEnabled = true;
            this.goin40.Location = new System.Drawing.Point(384, 208);
            this.goin40.Name = "goin40";
            this.goin40.Size = new System.Drawing.Size(75, 21);
            this.goin40.TabIndex = 18;
            // 
            // goin24
            // 
            this.goin24.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin24.FormattingEnabled = true;
            this.goin24.Location = new System.Drawing.Point(217, 208);
            this.goin24.Name = "goin24";
            this.goin24.Size = new System.Drawing.Size(75, 21);
            this.goin24.TabIndex = 18;
            // 
            // label305
            // 
            this.label305.AutoSize = true;
            this.label305.Location = new System.Drawing.Point(505, 211);
            this.label305.Name = "label305";
            this.label305.Size = new System.Drawing.Size(42, 13);
            this.label305.TabIndex = 9;
            this.label305.Text = "GoIn56";
            // 
            // label306
            // 
            this.label306.AutoSize = true;
            this.label306.Location = new System.Drawing.Point(336, 211);
            this.label306.Name = "label306";
            this.label306.Size = new System.Drawing.Size(42, 13);
            this.label306.TabIndex = 9;
            this.label306.Text = "GoIn40";
            // 
            // label307
            // 
            this.label307.AutoSize = true;
            this.label307.Location = new System.Drawing.Point(169, 211);
            this.label307.Name = "label307";
            this.label307.Size = new System.Drawing.Size(42, 13);
            this.label307.TabIndex = 9;
            this.label307.Text = "GoIn24";
            // 
            // goin55
            // 
            this.goin55.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin55.FormattingEnabled = true;
            this.goin55.Location = new System.Drawing.Point(553, 181);
            this.goin55.Name = "goin55";
            this.goin55.Size = new System.Drawing.Size(75, 21);
            this.goin55.TabIndex = 20;
            // 
            // goin39
            // 
            this.goin39.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin39.FormattingEnabled = true;
            this.goin39.Location = new System.Drawing.Point(384, 181);
            this.goin39.Name = "goin39";
            this.goin39.Size = new System.Drawing.Size(75, 21);
            this.goin39.TabIndex = 20;
            // 
            // goin23
            // 
            this.goin23.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin23.FormattingEnabled = true;
            this.goin23.Location = new System.Drawing.Point(217, 181);
            this.goin23.Name = "goin23";
            this.goin23.Size = new System.Drawing.Size(75, 21);
            this.goin23.TabIndex = 20;
            // 
            // label308
            // 
            this.label308.AutoSize = true;
            this.label308.Location = new System.Drawing.Point(505, 184);
            this.label308.Name = "label308";
            this.label308.Size = new System.Drawing.Size(42, 13);
            this.label308.TabIndex = 8;
            this.label308.Text = "GoIn55";
            // 
            // label309
            // 
            this.label309.AutoSize = true;
            this.label309.Location = new System.Drawing.Point(336, 184);
            this.label309.Name = "label309";
            this.label309.Size = new System.Drawing.Size(42, 13);
            this.label309.TabIndex = 8;
            this.label309.Text = "GoIn39";
            // 
            // label310
            // 
            this.label310.AutoSize = true;
            this.label310.Location = new System.Drawing.Point(169, 184);
            this.label310.Name = "label310";
            this.label310.Size = new System.Drawing.Size(42, 13);
            this.label310.TabIndex = 8;
            this.label310.Text = "GoIn23";
            // 
            // goin54
            // 
            this.goin54.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin54.FormattingEnabled = true;
            this.goin54.Location = new System.Drawing.Point(553, 154);
            this.goin54.Name = "goin54";
            this.goin54.Size = new System.Drawing.Size(75, 21);
            this.goin54.TabIndex = 22;
            // 
            // goin38
            // 
            this.goin38.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin38.FormattingEnabled = true;
            this.goin38.Location = new System.Drawing.Point(384, 154);
            this.goin38.Name = "goin38";
            this.goin38.Size = new System.Drawing.Size(75, 21);
            this.goin38.TabIndex = 22;
            // 
            // goin22
            // 
            this.goin22.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin22.FormattingEnabled = true;
            this.goin22.Location = new System.Drawing.Point(217, 154);
            this.goin22.Name = "goin22";
            this.goin22.Size = new System.Drawing.Size(75, 21);
            this.goin22.TabIndex = 22;
            // 
            // label311
            // 
            this.label311.AutoSize = true;
            this.label311.Location = new System.Drawing.Point(505, 157);
            this.label311.Name = "label311";
            this.label311.Size = new System.Drawing.Size(42, 13);
            this.label311.TabIndex = 7;
            this.label311.Text = "GoIn54";
            // 
            // label312
            // 
            this.label312.AutoSize = true;
            this.label312.Location = new System.Drawing.Point(336, 157);
            this.label312.Name = "label312";
            this.label312.Size = new System.Drawing.Size(42, 13);
            this.label312.TabIndex = 7;
            this.label312.Text = "GoIn38";
            // 
            // label313
            // 
            this.label313.AutoSize = true;
            this.label313.Location = new System.Drawing.Point(169, 157);
            this.label313.Name = "label313";
            this.label313.Size = new System.Drawing.Size(42, 13);
            this.label313.TabIndex = 7;
            this.label313.Text = "GoIn22";
            // 
            // goin53
            // 
            this.goin53.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin53.FormattingEnabled = true;
            this.goin53.Location = new System.Drawing.Point(553, 127);
            this.goin53.Name = "goin53";
            this.goin53.Size = new System.Drawing.Size(75, 21);
            this.goin53.TabIndex = 24;
            // 
            // goin37
            // 
            this.goin37.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin37.FormattingEnabled = true;
            this.goin37.Location = new System.Drawing.Point(384, 127);
            this.goin37.Name = "goin37";
            this.goin37.Size = new System.Drawing.Size(75, 21);
            this.goin37.TabIndex = 24;
            // 
            // goin21
            // 
            this.goin21.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin21.FormattingEnabled = true;
            this.goin21.Location = new System.Drawing.Point(217, 127);
            this.goin21.Name = "goin21";
            this.goin21.Size = new System.Drawing.Size(75, 21);
            this.goin21.TabIndex = 24;
            // 
            // label314
            // 
            this.label314.AutoSize = true;
            this.label314.Location = new System.Drawing.Point(505, 130);
            this.label314.Name = "label314";
            this.label314.Size = new System.Drawing.Size(42, 13);
            this.label314.TabIndex = 6;
            this.label314.Text = "GoIn53";
            // 
            // label315
            // 
            this.label315.AutoSize = true;
            this.label315.Location = new System.Drawing.Point(336, 130);
            this.label315.Name = "label315";
            this.label315.Size = new System.Drawing.Size(42, 13);
            this.label315.TabIndex = 6;
            this.label315.Text = "GoIn37";
            // 
            // label316
            // 
            this.label316.AutoSize = true;
            this.label316.Location = new System.Drawing.Point(169, 130);
            this.label316.Name = "label316";
            this.label316.Size = new System.Drawing.Size(42, 13);
            this.label316.TabIndex = 6;
            this.label316.Text = "GoIn21";
            // 
            // goin52
            // 
            this.goin52.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin52.FormattingEnabled = true;
            this.goin52.Location = new System.Drawing.Point(553, 100);
            this.goin52.Name = "goin52";
            this.goin52.Size = new System.Drawing.Size(75, 21);
            this.goin52.TabIndex = 26;
            // 
            // goin36
            // 
            this.goin36.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin36.FormattingEnabled = true;
            this.goin36.Location = new System.Drawing.Point(384, 100);
            this.goin36.Name = "goin36";
            this.goin36.Size = new System.Drawing.Size(75, 21);
            this.goin36.TabIndex = 26;
            // 
            // goin20
            // 
            this.goin20.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin20.FormattingEnabled = true;
            this.goin20.Location = new System.Drawing.Point(217, 100);
            this.goin20.Name = "goin20";
            this.goin20.Size = new System.Drawing.Size(75, 21);
            this.goin20.TabIndex = 26;
            // 
            // label317
            // 
            this.label317.AutoSize = true;
            this.label317.Location = new System.Drawing.Point(505, 103);
            this.label317.Name = "label317";
            this.label317.Size = new System.Drawing.Size(42, 13);
            this.label317.TabIndex = 5;
            this.label317.Text = "GoIn52";
            // 
            // label318
            // 
            this.label318.AutoSize = true;
            this.label318.Location = new System.Drawing.Point(336, 103);
            this.label318.Name = "label318";
            this.label318.Size = new System.Drawing.Size(42, 13);
            this.label318.TabIndex = 5;
            this.label318.Text = "GoIn36";
            // 
            // label319
            // 
            this.label319.AutoSize = true;
            this.label319.Location = new System.Drawing.Point(169, 103);
            this.label319.Name = "label319";
            this.label319.Size = new System.Drawing.Size(42, 13);
            this.label319.TabIndex = 5;
            this.label319.Text = "GoIn20";
            // 
            // goin51
            // 
            this.goin51.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin51.FormattingEnabled = true;
            this.goin51.Location = new System.Drawing.Point(553, 73);
            this.goin51.Name = "goin51";
            this.goin51.Size = new System.Drawing.Size(75, 21);
            this.goin51.TabIndex = 28;
            // 
            // goin35
            // 
            this.goin35.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin35.FormattingEnabled = true;
            this.goin35.Location = new System.Drawing.Point(384, 73);
            this.goin35.Name = "goin35";
            this.goin35.Size = new System.Drawing.Size(75, 21);
            this.goin35.TabIndex = 28;
            // 
            // goin19
            // 
            this.goin19.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin19.FormattingEnabled = true;
            this.goin19.Location = new System.Drawing.Point(217, 73);
            this.goin19.Name = "goin19";
            this.goin19.Size = new System.Drawing.Size(75, 21);
            this.goin19.TabIndex = 28;
            // 
            // label320
            // 
            this.label320.AutoSize = true;
            this.label320.Location = new System.Drawing.Point(505, 76);
            this.label320.Name = "label320";
            this.label320.Size = new System.Drawing.Size(42, 13);
            this.label320.TabIndex = 4;
            this.label320.Text = "GoIn51";
            // 
            // label321
            // 
            this.label321.AutoSize = true;
            this.label321.Location = new System.Drawing.Point(336, 76);
            this.label321.Name = "label321";
            this.label321.Size = new System.Drawing.Size(42, 13);
            this.label321.TabIndex = 4;
            this.label321.Text = "GoIn35";
            // 
            // label322
            // 
            this.label322.AutoSize = true;
            this.label322.Location = new System.Drawing.Point(169, 76);
            this.label322.Name = "label322";
            this.label322.Size = new System.Drawing.Size(42, 13);
            this.label322.TabIndex = 4;
            this.label322.Text = "GoIn19";
            // 
            // goin50
            // 
            this.goin50.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin50.FormattingEnabled = true;
            this.goin50.Location = new System.Drawing.Point(553, 46);
            this.goin50.Name = "goin50";
            this.goin50.Size = new System.Drawing.Size(75, 21);
            this.goin50.TabIndex = 30;
            // 
            // goin34
            // 
            this.goin34.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin34.FormattingEnabled = true;
            this.goin34.Location = new System.Drawing.Point(384, 46);
            this.goin34.Name = "goin34";
            this.goin34.Size = new System.Drawing.Size(75, 21);
            this.goin34.TabIndex = 30;
            // 
            // goin18
            // 
            this.goin18.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin18.FormattingEnabled = true;
            this.goin18.Location = new System.Drawing.Point(217, 46);
            this.goin18.Name = "goin18";
            this.goin18.Size = new System.Drawing.Size(75, 21);
            this.goin18.TabIndex = 30;
            // 
            // label323
            // 
            this.label323.AutoSize = true;
            this.label323.Location = new System.Drawing.Point(505, 49);
            this.label323.Name = "label323";
            this.label323.Size = new System.Drawing.Size(42, 13);
            this.label323.TabIndex = 3;
            this.label323.Text = "GoIn50";
            // 
            // label324
            // 
            this.label324.AutoSize = true;
            this.label324.Location = new System.Drawing.Point(336, 49);
            this.label324.Name = "label324";
            this.label324.Size = new System.Drawing.Size(42, 13);
            this.label324.TabIndex = 3;
            this.label324.Text = "GoIn34";
            // 
            // label325
            // 
            this.label325.AutoSize = true;
            this.label325.Location = new System.Drawing.Point(169, 49);
            this.label325.Name = "label325";
            this.label325.Size = new System.Drawing.Size(42, 13);
            this.label325.TabIndex = 3;
            this.label325.Text = "GoIn18";
            // 
            // goin49
            // 
            this.goin49.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin49.FormattingEnabled = true;
            this.goin49.Location = new System.Drawing.Point(553, 19);
            this.goin49.Name = "goin49";
            this.goin49.Size = new System.Drawing.Size(75, 21);
            this.goin49.TabIndex = 32;
            // 
            // label326
            // 
            this.label326.AutoSize = true;
            this.label326.Location = new System.Drawing.Point(505, 22);
            this.label326.Name = "label326";
            this.label326.Size = new System.Drawing.Size(42, 13);
            this.label326.TabIndex = 12;
            this.label326.Text = "GoIn49";
            // 
            // goin33
            // 
            this.goin33.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin33.FormattingEnabled = true;
            this.goin33.Location = new System.Drawing.Point(384, 19);
            this.goin33.Name = "goin33";
            this.goin33.Size = new System.Drawing.Size(75, 21);
            this.goin33.TabIndex = 32;
            // 
            // label327
            // 
            this.label327.AutoSize = true;
            this.label327.Location = new System.Drawing.Point(336, 22);
            this.label327.Name = "label327";
            this.label327.Size = new System.Drawing.Size(42, 13);
            this.label327.TabIndex = 12;
            this.label327.Text = "GoIn33";
            // 
            // goin17
            // 
            this.goin17.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin17.FormattingEnabled = true;
            this.goin17.Location = new System.Drawing.Point(217, 19);
            this.goin17.Name = "goin17";
            this.goin17.Size = new System.Drawing.Size(75, 21);
            this.goin17.TabIndex = 32;
            // 
            // label328
            // 
            this.label328.AutoSize = true;
            this.label328.Location = new System.Drawing.Point(169, 22);
            this.label328.Name = "label328";
            this.label328.Size = new System.Drawing.Size(42, 13);
            this.label328.TabIndex = 12;
            this.label328.Text = "GoIn17";
            // 
            // goin16
            // 
            this.goin16.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin16.FormattingEnabled = true;
            this.goin16.Location = new System.Drawing.Point(54, 424);
            this.goin16.Name = "goin16";
            this.goin16.Size = new System.Drawing.Size(75, 21);
            this.goin16.TabIndex = 1;
            // 
            // label329
            // 
            this.label329.AutoSize = true;
            this.label329.Location = new System.Drawing.Point(6, 427);
            this.label329.Name = "label329";
            this.label329.Size = new System.Drawing.Size(42, 13);
            this.label329.TabIndex = 0;
            this.label329.Text = "GoIn16";
            // 
            // goin15
            // 
            this.goin15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin15.FormattingEnabled = true;
            this.goin15.Location = new System.Drawing.Point(54, 397);
            this.goin15.Name = "goin15";
            this.goin15.Size = new System.Drawing.Size(75, 21);
            this.goin15.TabIndex = 1;
            // 
            // label330
            // 
            this.label330.AutoSize = true;
            this.label330.Location = new System.Drawing.Point(6, 400);
            this.label330.Name = "label330";
            this.label330.Size = new System.Drawing.Size(42, 13);
            this.label330.TabIndex = 0;
            this.label330.Text = "GoIn15";
            // 
            // goin14
            // 
            this.goin14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin14.FormattingEnabled = true;
            this.goin14.Location = new System.Drawing.Point(54, 370);
            this.goin14.Name = "goin14";
            this.goin14.Size = new System.Drawing.Size(75, 21);
            this.goin14.TabIndex = 1;
            // 
            // label331
            // 
            this.label331.AutoSize = true;
            this.label331.Location = new System.Drawing.Point(6, 373);
            this.label331.Name = "label331";
            this.label331.Size = new System.Drawing.Size(42, 13);
            this.label331.TabIndex = 0;
            this.label331.Text = "GoIn14";
            // 
            // goin13
            // 
            this.goin13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin13.FormattingEnabled = true;
            this.goin13.Location = new System.Drawing.Point(54, 343);
            this.goin13.Name = "goin13";
            this.goin13.Size = new System.Drawing.Size(75, 21);
            this.goin13.TabIndex = 1;
            // 
            // label332
            // 
            this.label332.AutoSize = true;
            this.label332.Location = new System.Drawing.Point(6, 346);
            this.label332.Name = "label332";
            this.label332.Size = new System.Drawing.Size(42, 13);
            this.label332.TabIndex = 0;
            this.label332.Text = "GoIn13";
            // 
            // goin12
            // 
            this.goin12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin12.FormattingEnabled = true;
            this.goin12.Location = new System.Drawing.Point(54, 316);
            this.goin12.Name = "goin12";
            this.goin12.Size = new System.Drawing.Size(75, 21);
            this.goin12.TabIndex = 1;
            // 
            // label333
            // 
            this.label333.AutoSize = true;
            this.label333.Location = new System.Drawing.Point(6, 319);
            this.label333.Name = "label333";
            this.label333.Size = new System.Drawing.Size(42, 13);
            this.label333.TabIndex = 0;
            this.label333.Text = "GoIn12";
            // 
            // goin11
            // 
            this.goin11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin11.FormattingEnabled = true;
            this.goin11.Location = new System.Drawing.Point(54, 289);
            this.goin11.Name = "goin11";
            this.goin11.Size = new System.Drawing.Size(75, 21);
            this.goin11.TabIndex = 1;
            // 
            // label334
            // 
            this.label334.AutoSize = true;
            this.label334.Location = new System.Drawing.Point(6, 292);
            this.label334.Name = "label334";
            this.label334.Size = new System.Drawing.Size(42, 13);
            this.label334.TabIndex = 0;
            this.label334.Text = "GoIn11";
            // 
            // goin10
            // 
            this.goin10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin10.FormattingEnabled = true;
            this.goin10.Location = new System.Drawing.Point(54, 262);
            this.goin10.Name = "goin10";
            this.goin10.Size = new System.Drawing.Size(75, 21);
            this.goin10.TabIndex = 1;
            // 
            // label335
            // 
            this.label335.AutoSize = true;
            this.label335.Location = new System.Drawing.Point(6, 265);
            this.label335.Name = "label335";
            this.label335.Size = new System.Drawing.Size(42, 13);
            this.label335.TabIndex = 0;
            this.label335.Text = "GoIn10";
            // 
            // goin9
            // 
            this.goin9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin9.FormattingEnabled = true;
            this.goin9.Location = new System.Drawing.Point(54, 235);
            this.goin9.Name = "goin9";
            this.goin9.Size = new System.Drawing.Size(75, 21);
            this.goin9.TabIndex = 1;
            // 
            // label336
            // 
            this.label336.AutoSize = true;
            this.label336.Location = new System.Drawing.Point(6, 238);
            this.label336.Name = "label336";
            this.label336.Size = new System.Drawing.Size(36, 13);
            this.label336.TabIndex = 0;
            this.label336.Text = "GoIn9";
            // 
            // goin8
            // 
            this.goin8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin8.FormattingEnabled = true;
            this.goin8.Location = new System.Drawing.Point(54, 208);
            this.goin8.Name = "goin8";
            this.goin8.Size = new System.Drawing.Size(75, 21);
            this.goin8.TabIndex = 1;
            // 
            // label337
            // 
            this.label337.AutoSize = true;
            this.label337.Location = new System.Drawing.Point(6, 211);
            this.label337.Name = "label337";
            this.label337.Size = new System.Drawing.Size(36, 13);
            this.label337.TabIndex = 0;
            this.label337.Text = "GoIn8";
            // 
            // goin7
            // 
            this.goin7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin7.FormattingEnabled = true;
            this.goin7.Location = new System.Drawing.Point(54, 181);
            this.goin7.Name = "goin7";
            this.goin7.Size = new System.Drawing.Size(75, 21);
            this.goin7.TabIndex = 1;
            // 
            // label338
            // 
            this.label338.AutoSize = true;
            this.label338.Location = new System.Drawing.Point(6, 184);
            this.label338.Name = "label338";
            this.label338.Size = new System.Drawing.Size(36, 13);
            this.label338.TabIndex = 0;
            this.label338.Text = "GoIn7";
            // 
            // goin6
            // 
            this.goin6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin6.FormattingEnabled = true;
            this.goin6.Location = new System.Drawing.Point(54, 154);
            this.goin6.Name = "goin6";
            this.goin6.Size = new System.Drawing.Size(75, 21);
            this.goin6.TabIndex = 1;
            // 
            // label339
            // 
            this.label339.AutoSize = true;
            this.label339.Location = new System.Drawing.Point(6, 157);
            this.label339.Name = "label339";
            this.label339.Size = new System.Drawing.Size(36, 13);
            this.label339.TabIndex = 0;
            this.label339.Text = "GoIn6";
            // 
            // goin5
            // 
            this.goin5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin5.FormattingEnabled = true;
            this.goin5.Location = new System.Drawing.Point(54, 127);
            this.goin5.Name = "goin5";
            this.goin5.Size = new System.Drawing.Size(75, 21);
            this.goin5.TabIndex = 1;
            // 
            // label340
            // 
            this.label340.AutoSize = true;
            this.label340.Location = new System.Drawing.Point(6, 130);
            this.label340.Name = "label340";
            this.label340.Size = new System.Drawing.Size(36, 13);
            this.label340.TabIndex = 0;
            this.label340.Text = "GoIn5";
            // 
            // goin4
            // 
            this.goin4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin4.FormattingEnabled = true;
            this.goin4.Location = new System.Drawing.Point(54, 100);
            this.goin4.Name = "goin4";
            this.goin4.Size = new System.Drawing.Size(75, 21);
            this.goin4.TabIndex = 1;
            // 
            // label341
            // 
            this.label341.AutoSize = true;
            this.label341.Location = new System.Drawing.Point(6, 103);
            this.label341.Name = "label341";
            this.label341.Size = new System.Drawing.Size(36, 13);
            this.label341.TabIndex = 0;
            this.label341.Text = "GoIn4";
            // 
            // goin3
            // 
            this.goin3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin3.FormattingEnabled = true;
            this.goin3.Location = new System.Drawing.Point(54, 73);
            this.goin3.Name = "goin3";
            this.goin3.Size = new System.Drawing.Size(75, 21);
            this.goin3.TabIndex = 1;
            // 
            // label342
            // 
            this.label342.AutoSize = true;
            this.label342.Location = new System.Drawing.Point(6, 76);
            this.label342.Name = "label342";
            this.label342.Size = new System.Drawing.Size(36, 13);
            this.label342.TabIndex = 0;
            this.label342.Text = "GoIn3";
            // 
            // goin2
            // 
            this.goin2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin2.FormattingEnabled = true;
            this.goin2.Location = new System.Drawing.Point(54, 46);
            this.goin2.Name = "goin2";
            this.goin2.Size = new System.Drawing.Size(75, 21);
            this.goin2.TabIndex = 1;
            // 
            // label343
            // 
            this.label343.AutoSize = true;
            this.label343.Location = new System.Drawing.Point(6, 49);
            this.label343.Name = "label343";
            this.label343.Size = new System.Drawing.Size(36, 13);
            this.label343.TabIndex = 0;
            this.label343.Text = "GoIn2";
            // 
            // goin1
            // 
            this.goin1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin1.FormattingEnabled = true;
            this.goin1.Location = new System.Drawing.Point(54, 19);
            this.goin1.Name = "goin1";
            this.goin1.Size = new System.Drawing.Size(75, 21);
            this.goin1.TabIndex = 1;
            // 
            // label344
            // 
            this.label344.AutoSize = true;
            this.label344.Location = new System.Drawing.Point(6, 22);
            this.label344.Name = "label344";
            this.label344.Size = new System.Drawing.Size(36, 13);
            this.label344.TabIndex = 0;
            this.label344.Text = "GoIn1";
            // 
            // label345
            // 
            this.label345.AutoSize = true;
            this.label345.Location = new System.Drawing.Point(6, 22);
            this.label345.Name = "label345";
            this.label345.Size = new System.Drawing.Size(57, 13);
            this.label345.TabIndex = 6;
            this.label345.Text = "Операция";
            // 
            // operationBGS
            // 
            this.operationBGS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.operationBGS.FormattingEnabled = true;
            this.operationBGS.Location = new System.Drawing.Point(69, 19);
            this.operationBGS.Name = "operationBGS";
            this.operationBGS.Size = new System.Drawing.Size(112, 21);
            this.operationBGS.TabIndex = 7;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(6, 12);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(89, 13);
            this.label36.TabIndex = 6;
            this.label36.Text = "Выбранный БГС";
            // 
            // currentBGS
            // 
            this.currentBGS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.currentBGS.FormattingEnabled = true;
            this.currentBGS.Location = new System.Drawing.Point(101, 9);
            this.currentBGS.Name = "currentBGS";
            this.currentBGS.Size = new System.Drawing.Size(102, 21);
            this.currentBGS.TabIndex = 7;
            // 
            // _automatPage
            // 
            this._automatPage.Controls.Add(this.UROVgroupBox);
            this._automatPage.Controls.Add(this.groupBox32);
            this._automatPage.Controls.Add(this.groupBox33);
            this._automatPage.Location = new System.Drawing.Point(4, 22);
            this._automatPage.Name = "_automatPage";
            this._automatPage.Size = new System.Drawing.Size(981, 553);
            this._automatPage.TabIndex = 9;
            this._automatPage.Text = "Выключатель и УРОВ";
            this._automatPage.UseVisualStyleBackColor = true;
            // 
            // UROVgroupBox
            // 
            this.UROVgroupBox.Controls.Add(this._urovSelf);
            this.UROVgroupBox.Controls.Add(this._urovBkCheck);
            this.UROVgroupBox.Controls.Add(this._urovIcheck);
            this.UROVgroupBox.Controls.Add(this._Iurov);
            this.UROVgroupBox.Controls.Add(this.label37);
            this.UROVgroupBox.Controls.Add(this._urovBlock);
            this.UROVgroupBox.Controls.Add(this._tUrov2);
            this.UROVgroupBox.Controls.Add(this._tUrov1);
            this.UROVgroupBox.Controls.Add(this._urovPusk);
            this.UROVgroupBox.Controls.Add(this.label38);
            this.UROVgroupBox.Controls.Add(this.label39);
            this.UROVgroupBox.Controls.Add(this.label126);
            this.UROVgroupBox.Controls.Add(this.label35);
            this.UROVgroupBox.Controls.Add(this.label34);
            this.UROVgroupBox.Controls.Add(this.label33);
            this.UROVgroupBox.Controls.Add(this.label127);
            this.UROVgroupBox.Location = new System.Drawing.Point(267, 3);
            this.UROVgroupBox.Name = "UROVgroupBox";
            this.UROVgroupBox.Size = new System.Drawing.Size(218, 188);
            this.UROVgroupBox.TabIndex = 14;
            this.UROVgroupBox.TabStop = false;
            this.UROVgroupBox.Text = "УРОВ";
            // 
            // _urovSelf
            // 
            this._urovSelf.AutoSize = true;
            this._urovSelf.Location = new System.Drawing.Point(106, 57);
            this._urovSelf.Name = "_urovSelf";
            this._urovSelf.Size = new System.Drawing.Size(15, 14);
            this._urovSelf.TabIndex = 27;
            this._urovSelf.UseVisualStyleBackColor = true;
            // 
            // _urovBkCheck
            // 
            this._urovBkCheck.AutoSize = true;
            this._urovBkCheck.Location = new System.Drawing.Point(106, 37);
            this._urovBkCheck.Name = "_urovBkCheck";
            this._urovBkCheck.Size = new System.Drawing.Size(15, 14);
            this._urovBkCheck.TabIndex = 27;
            this._urovBkCheck.UseVisualStyleBackColor = true;
            // 
            // _urovIcheck
            // 
            this._urovIcheck.AutoSize = true;
            this._urovIcheck.Location = new System.Drawing.Point(106, 19);
            this._urovIcheck.Name = "_urovIcheck";
            this._urovIcheck.Size = new System.Drawing.Size(15, 14);
            this._urovIcheck.TabIndex = 27;
            this._urovIcheck.UseVisualStyleBackColor = true;
            // 
            // _Iurov
            // 
            this._Iurov.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Iurov.Location = new System.Drawing.Point(106, 125);
            this._Iurov.Name = "_Iurov";
            this._Iurov.Size = new System.Drawing.Size(105, 20);
            this._Iurov.TabIndex = 24;
            this._Iurov.Tag = "3276700";
            this._Iurov.Text = "0";
            this._Iurov.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(6, 127);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(48, 13);
            this.label37.TabIndex = 15;
            this.label37.Text = "Iуров, Iн";
            // 
            // _urovBlock
            // 
            this._urovBlock.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._urovBlock.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._urovBlock.FormattingEnabled = true;
            this._urovBlock.Location = new System.Drawing.Point(106, 98);
            this._urovBlock.Name = "_urovBlock";
            this._urovBlock.Size = new System.Drawing.Size(105, 21);
            this._urovBlock.TabIndex = 26;
            // 
            // _tUrov2
            // 
            this._tUrov2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._tUrov2.Location = new System.Drawing.Point(106, 161);
            this._tUrov2.Name = "_tUrov2";
            this._tUrov2.Size = new System.Drawing.Size(105, 20);
            this._tUrov2.TabIndex = 23;
            this._tUrov2.Tag = "40";
            this._tUrov2.Text = "0";
            this._tUrov2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _tUrov1
            // 
            this._tUrov1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._tUrov1.Location = new System.Drawing.Point(106, 142);
            this._tUrov1.Name = "_tUrov1";
            this._tUrov1.Size = new System.Drawing.Size(105, 20);
            this._tUrov1.TabIndex = 22;
            this._tUrov1.Tag = "3276700";
            this._tUrov1.Text = "0";
            this._tUrov1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _urovPusk
            // 
            this._urovPusk.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._urovPusk.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._urovPusk.BackColor = System.Drawing.SystemColors.Window;
            this._urovPusk.FormattingEnabled = true;
            this._urovPusk.Location = new System.Drawing.Point(106, 77);
            this._urovPusk.Name = "_urovPusk";
            this._urovPusk.Size = new System.Drawing.Size(105, 21);
            this._urovPusk.TabIndex = 21;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(6, 163);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(59, 13);
            this.label38.TabIndex = 14;
            this.label38.Text = "tуров2, мс";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 145);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(59, 13);
            this.label39.TabIndex = 13;
            this.label39.Text = "tуров1, мс";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(6, 101);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(94, 13);
            this.label126.TabIndex = 12;
            this.label126.Text = "Вход блокировки";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 57);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(48, 13);
            this.label35.TabIndex = 11;
            this.label35.Text = "На себя";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 37);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(38, 13);
            this.label34.TabIndex = 11;
            this.label34.Text = "По БК";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 19);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(46, 13);
            this.label33.TabIndex = 11;
            this.label33.Text = "По току";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(6, 80);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(63, 13);
            this.label127.TabIndex = 11;
            this.label127.Text = "Вход пуска";
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this.label96);
            this.groupBox32.Controls.Add(this._comandOtkl);
            this.groupBox32.Controls.Add(this._controlSolenoidCombo);
            this.groupBox32.Controls.Add(this._switchKontCep);
            this.groupBox32.Controls.Add(this._switchTUskor);
            this.groupBox32.Controls.Add(this._switchImp);
            this.groupBox32.Controls.Add(this._switchBlock);
            this.groupBox32.Controls.Add(this._switchError);
            this.groupBox32.Controls.Add(this._switchOn);
            this.groupBox32.Controls.Add(this.label51);
            this.groupBox32.Controls.Add(this._switchOff);
            this.groupBox32.Controls.Add(this.label97);
            this.groupBox32.Controls.Add(this.label98);
            this.groupBox32.Controls.Add(this.label99);
            this.groupBox32.Controls.Add(this.label93);
            this.groupBox32.Controls.Add(this.label90);
            this.groupBox32.Controls.Add(this.label89);
            this.groupBox32.Controls.Add(this.label88);
            this.groupBox32.Location = new System.Drawing.Point(8, 3);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(253, 232);
            this.groupBox32.TabIndex = 14;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "Выключатель";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(6, 119);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(115, 13);
            this.label96.TabIndex = 16;
            this.label96.Text = "Команда отключения";
            // 
            // _comandOtkl
            // 
            this._comandOtkl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comandOtkl.FormattingEnabled = true;
            this._comandOtkl.Location = new System.Drawing.Point(142, 116);
            this._comandOtkl.Name = "_comandOtkl";
            this._comandOtkl.Size = new System.Drawing.Size(105, 21);
            this._comandOtkl.TabIndex = 27;
            // 
            // _controlSolenoidCombo
            // 
            this._controlSolenoidCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._controlSolenoidCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._controlSolenoidCombo.FormattingEnabled = true;
            this._controlSolenoidCombo.Location = new System.Drawing.Point(142, 198);
            this._controlSolenoidCombo.Name = "_controlSolenoidCombo";
            this._controlSolenoidCombo.Size = new System.Drawing.Size(105, 21);
            this._controlSolenoidCombo.TabIndex = 26;
            // 
            // _switchKontCep
            // 
            this._switchKontCep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchKontCep.FormattingEnabled = true;
            this._switchKontCep.Location = new System.Drawing.Point(142, 161);
            this._switchKontCep.Name = "_switchKontCep";
            this._switchKontCep.Size = new System.Drawing.Size(105, 21);
            this._switchKontCep.TabIndex = 26;
            // 
            // _switchTUskor
            // 
            this._switchTUskor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._switchTUskor.Location = new System.Drawing.Point(142, 135);
            this._switchTUskor.Name = "_switchTUskor";
            this._switchTUskor.Size = new System.Drawing.Size(105, 20);
            this._switchTUskor.TabIndex = 25;
            this._switchTUskor.Tag = "3276700";
            this._switchTUskor.Text = "20";
            this._switchTUskor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switchImp
            // 
            this._switchImp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._switchImp.Location = new System.Drawing.Point(142, 99);
            this._switchImp.Name = "_switchImp";
            this._switchImp.Size = new System.Drawing.Size(105, 20);
            this._switchImp.TabIndex = 24;
            this._switchImp.Tag = "3276700";
            this._switchImp.Text = "0";
            this._switchImp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switchBlock
            // 
            this._switchBlock.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchBlock.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchBlock.BackColor = System.Drawing.SystemColors.Window;
            this._switchBlock.FormattingEnabled = true;
            this._switchBlock.Location = new System.Drawing.Point(142, 75);
            this._switchBlock.Name = "_switchBlock";
            this._switchBlock.Size = new System.Drawing.Size(105, 21);
            this._switchBlock.TabIndex = 21;
            // 
            // _switchError
            // 
            this._switchError.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchError.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchError.FormattingEnabled = true;
            this._switchError.Location = new System.Drawing.Point(142, 55);
            this._switchError.Name = "_switchError";
            this._switchError.Size = new System.Drawing.Size(105, 21);
            this._switchError.TabIndex = 20;
            // 
            // _switchOn
            // 
            this._switchOn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchOn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchOn.FormattingEnabled = true;
            this._switchOn.Location = new System.Drawing.Point(142, 35);
            this._switchOn.Name = "_switchOn";
            this._switchOn.Size = new System.Drawing.Size(105, 21);
            this._switchOn.TabIndex = 19;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(6, 195);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(124, 26);
            this.label51.TabIndex = 17;
            this.label51.Text = "Контроль второго\r\nсоленоида отключения";
            // 
            // _switchOff
            // 
            this._switchOff.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchOff.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchOff.FormattingEnabled = true;
            this._switchOff.Location = new System.Drawing.Point(142, 15);
            this._switchOff.Name = "_switchOff";
            this._switchOff.Size = new System.Drawing.Size(105, 21);
            this._switchOff.TabIndex = 18;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(6, 160);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(88, 26);
            this.label97.TabIndex = 17;
            this.label97.Text = "Контроль цепей\r\nуправления";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(6, 138);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(59, 13);
            this.label98.TabIndex = 16;
            this.label98.Text = "tускор, мс";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(6, 102);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(139, 13);
            this.label99.TabIndex = 15;
            this.label99.Text = "Импульс сигнала упр., мс";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(6, 78);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(94, 13);
            this.label93.TabIndex = 12;
            this.label93.Text = "Вход блокировки";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(6, 58);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(111, 13);
            this.label90.TabIndex = 11;
            this.label90.Text = "Вход неисправности";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(6, 38);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(124, 13);
            this.label89.TabIndex = 10;
            this.label89.Text = "Состояние \"Включено\"";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(6, 18);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(130, 13);
            this.label88.TabIndex = 9;
            this.label88.Text = "Состояние \"Отключено\"";
            // 
            // groupBox33
            // 
            this.groupBox33.Controls.Add(this._blockSDTU);
            this.groupBox33.Controls.Add(this._switchSDTU);
            this.groupBox33.Controls.Add(this._switchVnesh);
            this.groupBox33.Controls.Add(this._switchKey);
            this.groupBox33.Controls.Add(this._switchButtons);
            this.groupBox33.Controls.Add(this._switchVneshOff);
            this.groupBox33.Controls.Add(this._switchVneshOn);
            this.groupBox33.Controls.Add(this._switchKeyOff);
            this.groupBox33.Controls.Add(this._blockSDTUlabel);
            this.groupBox33.Controls.Add(this._switchKeyOn);
            this.groupBox33.Controls.Add(this.label101);
            this.groupBox33.Controls.Add(this.label102);
            this.groupBox33.Controls.Add(this.label103);
            this.groupBox33.Controls.Add(this.label104);
            this.groupBox33.Controls.Add(this.label105);
            this.groupBox33.Controls.Add(this.label106);
            this.groupBox33.Controls.Add(this.label107);
            this.groupBox33.Controls.Add(this.label108);
            this.groupBox33.Location = new System.Drawing.Point(8, 241);
            this.groupBox33.Name = "groupBox33";
            this.groupBox33.Size = new System.Drawing.Size(253, 200);
            this.groupBox33.TabIndex = 15;
            this.groupBox33.TabStop = false;
            this.groupBox33.Text = "Управление";
            // 
            // _blockSDTU
            // 
            this._blockSDTU.FormattingEnabled = true;
            this._blockSDTU.Location = new System.Drawing.Point(142, 174);
            this._blockSDTU.Name = "_blockSDTU";
            this._blockSDTU.Size = new System.Drawing.Size(105, 21);
            this._blockSDTU.TabIndex = 34;
            // 
            // _switchSDTU
            // 
            this._switchSDTU.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchSDTU.FormattingEnabled = true;
            this._switchSDTU.Location = new System.Drawing.Point(142, 154);
            this._switchSDTU.Name = "_switchSDTU";
            this._switchSDTU.Size = new System.Drawing.Size(105, 21);
            this._switchSDTU.TabIndex = 34;
            // 
            // _switchVnesh
            // 
            this._switchVnesh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchVnesh.FormattingEnabled = true;
            this._switchVnesh.Location = new System.Drawing.Point(142, 134);
            this._switchVnesh.Name = "_switchVnesh";
            this._switchVnesh.Size = new System.Drawing.Size(105, 21);
            this._switchVnesh.TabIndex = 33;
            // 
            // _switchKey
            // 
            this._switchKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchKey.FormattingEnabled = true;
            this._switchKey.Location = new System.Drawing.Point(142, 115);
            this._switchKey.Name = "_switchKey";
            this._switchKey.Size = new System.Drawing.Size(105, 21);
            this._switchKey.TabIndex = 32;
            // 
            // _switchButtons
            // 
            this._switchButtons.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchButtons.FormattingEnabled = true;
            this._switchButtons.Location = new System.Drawing.Point(142, 95);
            this._switchButtons.Name = "_switchButtons";
            this._switchButtons.Size = new System.Drawing.Size(105, 21);
            this._switchButtons.TabIndex = 31;
            // 
            // _switchVneshOff
            // 
            this._switchVneshOff.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchVneshOff.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchVneshOff.FormattingEnabled = true;
            this._switchVneshOff.Location = new System.Drawing.Point(142, 75);
            this._switchVneshOff.Name = "_switchVneshOff";
            this._switchVneshOff.Size = new System.Drawing.Size(105, 21);
            this._switchVneshOff.TabIndex = 30;
            // 
            // _switchVneshOn
            // 
            this._switchVneshOn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchVneshOn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchVneshOn.FormattingEnabled = true;
            this._switchVneshOn.Location = new System.Drawing.Point(142, 55);
            this._switchVneshOn.Name = "_switchVneshOn";
            this._switchVneshOn.Size = new System.Drawing.Size(105, 21);
            this._switchVneshOn.TabIndex = 29;
            // 
            // _switchKeyOff
            // 
            this._switchKeyOff.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchKeyOff.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchKeyOff.FormattingEnabled = true;
            this._switchKeyOff.Location = new System.Drawing.Point(142, 35);
            this._switchKeyOff.Name = "_switchKeyOff";
            this._switchKeyOff.Size = new System.Drawing.Size(105, 21);
            this._switchKeyOff.TabIndex = 28;
            // 
            // _blockSDTUlabel
            // 
            this._blockSDTUlabel.AutoSize = true;
            this._blockSDTUlabel.Location = new System.Drawing.Point(6, 177);
            this._blockSDTUlabel.Name = "_blockSDTUlabel";
            this._blockSDTUlabel.Size = new System.Drawing.Size(102, 13);
            this._blockSDTUlabel.TabIndex = 25;
            this._blockSDTUlabel.Text = "Блокировка СДТУ";
            // 
            // _switchKeyOn
            // 
            this._switchKeyOn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchKeyOn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchKeyOn.FormattingEnabled = true;
            this._switchKeyOn.Location = new System.Drawing.Point(142, 15);
            this._switchKeyOn.Name = "_switchKeyOn";
            this._switchKeyOn.Size = new System.Drawing.Size(105, 21);
            this._switchKeyOn.TabIndex = 27;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(6, 157);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(54, 13);
            this.label101.TabIndex = 25;
            this.label101.Text = "От СДТУ";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(6, 137);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(52, 13);
            this.label102.TabIndex = 24;
            this.label102.Text = "Внешнее";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(6, 118);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(54, 13);
            this.label103.TabIndex = 23;
            this.label103.Text = "От ключа";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(6, 98);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(96, 13);
            this.label104.TabIndex = 22;
            this.label104.Text = "От кнопок пульта";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(6, 78);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(108, 13);
            this.label105.TabIndex = 21;
            this.label105.Text = "Внешнее отключить";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(6, 58);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(103, 13);
            this.label106.TabIndex = 20;
            this.label106.Text = "Внешнее включить";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(6, 38);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(89, 13);
            this.label107.TabIndex = 19;
            this.label107.Text = "Ключ отключить";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(6, 18);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(84, 13);
            this.label108.TabIndex = 18;
            this.label108.Text = "Ключ включить";
            // 
            // _systemPage
            // 
            this._systemPage.Controls.Add(this._signaturesForOscGroupBox);
            this._systemPage.Controls.Add(this.groupBox11);
            this._systemPage.Controls.Add(this.groupBox3);
            this._systemPage.Location = new System.Drawing.Point(4, 22);
            this._systemPage.Name = "_systemPage";
            this._systemPage.Size = new System.Drawing.Size(981, 553);
            this._systemPage.TabIndex = 8;
            this._systemPage.Text = "Осциллограф";
            this._systemPage.UseVisualStyleBackColor = true;
            // 
            // _signaturesForOscGroupBox
            // 
            this._signaturesForOscGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._signaturesForOscGroupBox.Controls.Add(this.signaturesOsc);
            this._signaturesForOscGroupBox.Location = new System.Drawing.Point(591, 3);
            this._signaturesForOscGroupBox.Name = "_signaturesForOscGroupBox";
            this._signaturesForOscGroupBox.Size = new System.Drawing.Size(380, 547);
            this._signaturesForOscGroupBox.TabIndex = 7;
            this._signaturesForOscGroupBox.TabStop = false;
            this._signaturesForOscGroupBox.Text = "Подписи каналов осциллографа";
            // 
            // signaturesOsc
            // 
            this.signaturesOsc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.signaturesOsc.Location = new System.Drawing.Point(3, 16);
            this.signaturesOsc.Messages = null;
            this.signaturesOsc.Name = "signaturesOsc";
            this.signaturesOsc.Size = new System.Drawing.Size(374, 528);
            this.signaturesOsc.TabIndex = 5;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._oscChannelsGrid);
            this.groupBox11.Location = new System.Drawing.Point(265, 3);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(323, 547);
            this.groupBox11.TabIndex = 4;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Программируемые дискретные каналы";
            // 
            // _oscChannelsGrid
            // 
            this._oscChannelsGrid.AllowUserToAddRows = false;
            this._oscChannelsGrid.AllowUserToDeleteRows = false;
            this._oscChannelsGrid.AllowUserToResizeColumns = false;
            this._oscChannelsGrid.AllowUserToResizeRows = false;
            this._oscChannelsGrid.BackgroundColor = System.Drawing.Color.White;
            this._oscChannelsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._oscChannelsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn17,
            this._baseCol,
            this._oscSygnal});
            this._oscChannelsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._oscChannelsGrid.Location = new System.Drawing.Point(3, 16);
            this._oscChannelsGrid.Name = "_oscChannelsGrid";
            this._oscChannelsGrid.RowHeadersVisible = false;
            this._oscChannelsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._oscChannelsGrid.RowTemplate.Height = 24;
            this._oscChannelsGrid.ShowCellErrors = false;
            this._oscChannelsGrid.ShowRowErrors = false;
            this._oscChannelsGrid.Size = new System.Drawing.Size(317, 528);
            this._oscChannelsGrid.TabIndex = 27;
            this._oscChannelsGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn17.Frozen = true;
            this.dataGridViewTextBoxColumn17.HeaderText = "Канал";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn17.Width = 44;
            // 
            // _baseCol
            // 
            this._baseCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._baseCol.DefaultCellStyle = dataGridViewCellStyle2;
            this._baseCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._baseCol.HeaderText = "База";
            this._baseCol.Name = "_baseCol";
            this._baseCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._baseCol.Width = 38;
            // 
            // _oscSygnal
            // 
            this._oscSygnal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._oscSygnal.DefaultCellStyle = dataGridViewCellStyle3;
            this._oscSygnal.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._oscSygnal.HeaderText = "Сигнал";
            this._oscSygnal.Name = "_oscSygnal";
            this._oscSygnal.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._oscSygnal.Width = 49;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.oscStartCb);
            this.groupBox3.Controls.Add(this.label65);
            this.groupBox3.Controls.Add(this._oscSizeTextBox);
            this.groupBox3.Controls.Add(this._oscWriteLength);
            this.groupBox3.Controls.Add(this._oscFix);
            this.groupBox3.Controls.Add(this._oscLength);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Location = new System.Drawing.Point(8, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(253, 124);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Осциллограф";
            // 
            // oscStartCb
            // 
            this.oscStartCb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.oscStartCb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.oscStartCb.FormattingEnabled = true;
            this.oscStartCb.Location = new System.Drawing.Point(121, 91);
            this.oscStartCb.Name = "oscStartCb";
            this.oscStartCb.Size = new System.Drawing.Size(121, 21);
            this.oscStartCb.TabIndex = 30;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(5, 94);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(87, 13);
            this.label65.TabIndex = 29;
            this.label65.Text = "Вход пуска осц.";
            // 
            // _oscSizeTextBox
            // 
            this._oscSizeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._oscSizeTextBox.Location = new System.Drawing.Point(171, 23);
            this._oscSizeTextBox.Name = "_oscSizeTextBox";
            this._oscSizeTextBox.ReadOnly = true;
            this._oscSizeTextBox.Size = new System.Drawing.Size(71, 20);
            this._oscSizeTextBox.TabIndex = 28;
            this._oscSizeTextBox.Tag = "3000000";
            this._oscSizeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _oscWriteLength
            // 
            this._oscWriteLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._oscWriteLength.Location = new System.Drawing.Point(121, 44);
            this._oscWriteLength.Name = "_oscWriteLength";
            this._oscWriteLength.Size = new System.Drawing.Size(121, 20);
            this._oscWriteLength.TabIndex = 23;
            this._oscWriteLength.Tag = "3000000";
            this._oscWriteLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _oscFix
            // 
            this._oscFix.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscFix.FormattingEnabled = true;
            this._oscFix.Location = new System.Drawing.Point(121, 64);
            this._oscFix.Name = "_oscFix";
            this._oscFix.Size = new System.Drawing.Size(121, 21);
            this._oscFix.TabIndex = 13;
            // 
            // _oscLength
            // 
            this._oscLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscLength.FormattingEnabled = true;
            this._oscLength.Location = new System.Drawing.Point(121, 23);
            this._oscLength.Name = "_oscLength";
            this._oscLength.Size = new System.Drawing.Size(44, 21);
            this._oscLength.TabIndex = 12;
            this._oscLength.SelectedIndexChanged += new System.EventHandler(this._oscLength_SelectedIndexChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(5, 67);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(66, 13);
            this.label41.TabIndex = 2;
            this.label41.Text = "Фиксац. по";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(5, 46);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(113, 13);
            this.label42.TabIndex = 1;
            this.label42.Text = "Длит. предзаписи, %";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(5, 26);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(66, 13);
            this.label43.TabIndex = 0;
            this.label43.Text = "Размер, мс";
            // 
            // _outputSignalsPage
            // 
            this._outputSignalsPage.Controls.Add(this.groupBox175);
            this._outputSignalsPage.Controls.Add(this.groupBox31);
            this._outputSignalsPage.Controls.Add(this.groupBox23);
            this._outputSignalsPage.Controls.Add(this.groupBox54);
            this._outputSignalsPage.Controls.Add(this.groupBox13);
            this._outputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._outputSignalsPage.Name = "_outputSignalsPage";
            this._outputSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._outputSignalsPage.Size = new System.Drawing.Size(981, 553);
            this._outputSignalsPage.TabIndex = 10;
            this._outputSignalsPage.Text = "Выходные сигналы";
            this._outputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox175
            // 
            this.groupBox175.Controls.Add(this._outputIndicatorsGrid);
            this.groupBox175.Location = new System.Drawing.Point(404, 3);
            this.groupBox175.Name = "groupBox175";
            this.groupBox175.Size = new System.Drawing.Size(564, 330);
            this.groupBox175.TabIndex = 26;
            this.groupBox175.TabStop = false;
            this.groupBox175.Text = "Индикаторы";
            // 
            // _outputIndicatorsGrid
            // 
            this._outputIndicatorsGrid.AllowUserToAddRows = false;
            this._outputIndicatorsGrid.AllowUserToDeleteRows = false;
            this._outputIndicatorsGrid.AllowUserToResizeColumns = false;
            this._outputIndicatorsGrid.AllowUserToResizeRows = false;
            this._outputIndicatorsGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputIndicatorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputIndicatorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._outIndNumberCol,
            this._outIndTypeCol,
            this.baseGreenColumn,
            this._outIndSignalCol,
            this.baseRedColumn,
            this._out1IndSignalCol,
            this._outIndSignal2Col});
            this._outputIndicatorsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._outputIndicatorsGrid.Location = new System.Drawing.Point(3, 16);
            this._outputIndicatorsGrid.Name = "_outputIndicatorsGrid";
            this._outputIndicatorsGrid.RowHeadersVisible = false;
            this._outputIndicatorsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._outputIndicatorsGrid.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this._outputIndicatorsGrid.RowTemplate.Height = 24;
            this._outputIndicatorsGrid.ShowCellErrors = false;
            this._outputIndicatorsGrid.ShowRowErrors = false;
            this._outputIndicatorsGrid.Size = new System.Drawing.Size(558, 311);
            this._outputIndicatorsGrid.TabIndex = 0;
            this._outputIndicatorsGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // _outIndNumberCol
            // 
            this._outIndNumberCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._outIndNumberCol.HeaderText = "№";
            this._outIndNumberCol.Name = "_outIndNumberCol";
            this._outIndNumberCol.ReadOnly = true;
            this._outIndNumberCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndNumberCol.Width = 24;
            // 
            // _outIndTypeCol
            // 
            this._outIndTypeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._outIndTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndTypeCol.HeaderText = "Тип";
            this._outIndTypeCol.Name = "_outIndTypeCol";
            this._outIndTypeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndTypeCol.Width = 32;
            // 
            // baseGreenColumn
            // 
            this.baseGreenColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.baseGreenColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.baseGreenColumn.HeaderText = "База";
            this.baseGreenColumn.Name = "baseGreenColumn";
            this.baseGreenColumn.Width = 38;
            // 
            // _outIndSignalCol
            // 
            this._outIndSignalCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._outIndSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndSignalCol.HeaderText = "Сигнал \"зеленый\"";
            this._outIndSignalCol.Name = "_outIndSignalCol";
            this._outIndSignalCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndSignalCol.Width = 95;
            // 
            // baseRedColumn
            // 
            this.baseRedColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.baseRedColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.baseRedColumn.HeaderText = "База";
            this.baseRedColumn.Name = "baseRedColumn";
            this.baseRedColumn.Width = 38;
            // 
            // _out1IndSignalCol
            // 
            this._out1IndSignalCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._out1IndSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._out1IndSignalCol.HeaderText = "Сигнал \"красный\"";
            this._out1IndSignalCol.Name = "_out1IndSignalCol";
            this._out1IndSignalCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._out1IndSignalCol.Width = 95;
            // 
            // _outIndSignal2Col
            // 
            this._outIndSignal2Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._outIndSignal2Col.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndSignal2Col.HeaderText = "Режим работы";
            this._outIndSignal2Col.Name = "_outIndSignal2Col";
            this._outIndSignal2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndSignal2Col.Width = 79;
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this._fault7CheckBox);
            this.groupBox31.Controls.Add(this._vchsFaultLabel);
            this.groupBox31.Controls.Add(this._fault6CheckBox);
            this.groupBox31.Controls.Add(this.label121);
            this.groupBox31.Controls.Add(this._fault5CheckBox);
            this.groupBox31.Controls.Add(this._fault4CheckBox);
            this.groupBox31.Controls.Add(this._fault3CheckBox);
            this.groupBox31.Controls.Add(this._fault2CheckBox);
            this.groupBox31.Controls.Add(this.label111);
            this.groupBox31.Controls.Add(this._fault1CheckBox);
            this.groupBox31.Controls.Add(this.label18);
            this.groupBox31.Controls.Add(this.label23);
            this.groupBox31.Controls.Add(this.label44);
            this.groupBox31.Controls.Add(this.label45);
            this.groupBox31.Controls.Add(this._impTB);
            this.groupBox31.Controls.Add(this.label46);
            this.groupBox31.Location = new System.Drawing.Point(404, 336);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(317, 208);
            this.groupBox31.TabIndex = 21;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "Реле неисправность";
            // 
            // _fault7CheckBox
            // 
            this._fault7CheckBox.AutoSize = true;
            this._fault7CheckBox.Location = new System.Drawing.Point(182, 162);
            this._fault7CheckBox.Name = "_fault7CheckBox";
            this._fault7CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault7CheckBox.TabIndex = 22;
            this._fault7CheckBox.UseVisualStyleBackColor = true;
            this._fault7CheckBox.Visible = false;
            // 
            // _vchsFaultLabel
            // 
            this._vchsFaultLabel.AutoSize = true;
            this._vchsFaultLabel.Location = new System.Drawing.Point(6, 163);
            this._vchsFaultLabel.Name = "_vchsFaultLabel";
            this._vchsFaultLabel.Size = new System.Drawing.Size(123, 13);
            this._vchsFaultLabel.TabIndex = 21;
            this._vchsFaultLabel.Text = "7. Неисправность ВЧС";
            this._vchsFaultLabel.Visible = false;
            // 
            // _fault6CheckBox
            // 
            this._fault6CheckBox.AutoSize = true;
            this._fault6CheckBox.Location = new System.Drawing.Point(182, 140);
            this._fault6CheckBox.Name = "_fault6CheckBox";
            this._fault6CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault6CheckBox.TabIndex = 20;
            this._fault6CheckBox.UseVisualStyleBackColor = true;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(6, 141);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(136, 13);
            this.label121.TabIndex = 19;
            this.label121.Text = "6. Неисправность логики";
            // 
            // _fault5CheckBox
            // 
            this._fault5CheckBox.AutoSize = true;
            this._fault5CheckBox.Location = new System.Drawing.Point(182, 117);
            this._fault5CheckBox.Name = "_fault5CheckBox";
            this._fault5CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault5CheckBox.TabIndex = 18;
            this._fault5CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault4CheckBox
            // 
            this._fault4CheckBox.AutoSize = true;
            this._fault4CheckBox.Location = new System.Drawing.Point(182, 93);
            this._fault4CheckBox.Name = "_fault4CheckBox";
            this._fault4CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault4CheckBox.TabIndex = 18;
            this._fault4CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault3CheckBox
            // 
            this._fault3CheckBox.AutoSize = true;
            this._fault3CheckBox.Location = new System.Drawing.Point(182, 69);
            this._fault3CheckBox.Name = "_fault3CheckBox";
            this._fault3CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault3CheckBox.TabIndex = 17;
            this._fault3CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault2CheckBox
            // 
            this._fault2CheckBox.AutoSize = true;
            this._fault2CheckBox.Location = new System.Drawing.Point(182, 45);
            this._fault2CheckBox.Name = "_fault2CheckBox";
            this._fault2CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault2CheckBox.TabIndex = 16;
            this._fault2CheckBox.UseVisualStyleBackColor = true;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(6, 118);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(169, 13);
            this.label111.TabIndex = 14;
            this.label111.Text = "5. Неисправность выключателя";
            // 
            // _fault1CheckBox
            // 
            this._fault1CheckBox.AutoSize = true;
            this._fault1CheckBox.Location = new System.Drawing.Point(182, 21);
            this._fault1CheckBox.Name = "_fault1CheckBox";
            this._fault1CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault1CheckBox.TabIndex = 15;
            this._fault1CheckBox.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 94);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(166, 13);
            this.label18.TabIndex = 14;
            this.label18.Text = "4. Неисправность измерений F";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 70);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(168, 13);
            this.label23.TabIndex = 13;
            this.label23.Text = "3. Неисправность измерений U";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 46);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(170, 13);
            this.label44.TabIndex = 12;
            this.label44.Text = "2. Программная неисправность";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(6, 22);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(159, 13);
            this.label45.TabIndex = 11;
            this.label45.Text = "1. Аппаратная неисправность";
            // 
            // _impTB
            // 
            this._impTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._impTB.Location = new System.Drawing.Point(93, 182);
            this._impTB.Name = "_impTB";
            this._impTB.Size = new System.Drawing.Size(104, 20);
            this._impTB.TabIndex = 7;
            this._impTB.Text = "0";
            this._impTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(8, 184);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(67, 13);
            this.label46.TabIndex = 3;
            this.label46.Text = "Tвозвр., мс";
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this._outputReleGrid);
            this.groupBox23.Location = new System.Drawing.Point(3, 3);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(398, 541);
            this.groupBox23.TabIndex = 25;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Выходные реле";
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this.baseColumn,
            this._releSignalCol,
            this._releWaitCol});
            this._outputReleGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._outputReleGrid.Location = new System.Drawing.Point(3, 16);
            this._outputReleGrid.Name = "_outputReleGrid";
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(392, 522);
            this._outputReleGrid.TabIndex = 0;
            this._outputReleGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // _releNumberCol
            // 
            this._releNumberCol.HeaderText = "№";
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 25;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._releTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releTypeCol.HeaderText = "Тип";
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 32;
            // 
            // baseColumn
            // 
            this.baseColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.baseColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.baseColumn.HeaderText = "База";
            this.baseColumn.Name = "baseColumn";
            this.baseColumn.Width = 38;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._releSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releSignalCol.HeaderText = "Сигнал";
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 49;
            // 
            // _releWaitCol
            // 
            this._releWaitCol.HeaderText = "Твозвр, мс";
            this._releWaitCol.Name = "_releWaitCol";
            this._releWaitCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releWaitCol.Width = 70;
            // 
            // groupBox54
            // 
            this.groupBox54.Controls.Add(this.label193);
            this.groupBox54.Controls.Add(this._fixErrorFCheckBox);
            this.groupBox54.Location = new System.Drawing.Point(727, 406);
            this.groupBox54.Name = "groupBox54";
            this.groupBox54.Size = new System.Drawing.Size(215, 50);
            this.groupBox54.TabIndex = 22;
            this.groupBox54.TabStop = false;
            this.groupBox54.Text = "Работа частоты";
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Location = new System.Drawing.Point(6, 23);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(132, 13);
            this.label193.TabIndex = 22;
            this.label193.Text = "Фикс. ошибка расчета F";
            // 
            // _fixErrorFCheckBox
            // 
            this._fixErrorFCheckBox.AutoSize = true;
            this._fixErrorFCheckBox.Location = new System.Drawing.Point(185, 23);
            this._fixErrorFCheckBox.Name = "_fixErrorFCheckBox";
            this._fixErrorFCheckBox.Size = new System.Drawing.Size(15, 14);
            this._fixErrorFCheckBox.TabIndex = 23;
            this._fixErrorFCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this._resetAlarmCheckBox);
            this.groupBox13.Controls.Add(this._resetSystemCheckBox);
            this.groupBox13.Controls.Add(this.label110);
            this.groupBox13.Controls.Add(this.label52);
            this.groupBox13.Location = new System.Drawing.Point(727, 336);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(215, 71);
            this.groupBox13.TabIndex = 22;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Сброс индикаторов";
            // 
            // _resetAlarmCheckBox
            // 
            this._resetAlarmCheckBox.AutoSize = true;
            this._resetAlarmCheckBox.Location = new System.Drawing.Point(185, 46);
            this._resetAlarmCheckBox.Name = "_resetAlarmCheckBox";
            this._resetAlarmCheckBox.Size = new System.Drawing.Size(15, 14);
            this._resetAlarmCheckBox.TabIndex = 21;
            this._resetAlarmCheckBox.UseVisualStyleBackColor = true;
            // 
            // _resetSystemCheckBox
            // 
            this._resetSystemCheckBox.AutoSize = true;
            this._resetSystemCheckBox.Location = new System.Drawing.Point(185, 21);
            this._resetSystemCheckBox.Name = "_resetSystemCheckBox";
            this._resetSystemCheckBox.Size = new System.Drawing.Size(15, 14);
            this._resetSystemCheckBox.TabIndex = 19;
            this._resetSystemCheckBox.UseVisualStyleBackColor = true;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(6, 45);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(152, 13);
            this.label110.TabIndex = 20;
            this.label110.Text = "2. По входу в журнал аварий";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(6, 22);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(161, 13);
            this.label52.TabIndex = 19;
            this.label52.Text = "1. По входу в журнал системы";
            // 
            // _inputSignalsPage
            // 
            this._inputSignalsPage.Controls.Add(this.groupBox59);
            this._inputSignalsPage.Controls.Add(this.groupBox22);
            this._inputSignalsPage.Controls.Add(this.groupBox18);
            this._inputSignalsPage.Controls.Add(this.groupBox15);
            this._inputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._inputSignalsPage.Name = "_inputSignalsPage";
            this._inputSignalsPage.Size = new System.Drawing.Size(981, 553);
            this._inputSignalsPage.TabIndex = 7;
            this._inputSignalsPage.Text = "Входные сигналы";
            this._inputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox59
            // 
            this.groupBox59.Controls.Add(this._commandsDataGridView);
            this.groupBox59.Location = new System.Drawing.Point(514, 1);
            this.groupBox59.Name = "groupBox59";
            this.groupBox59.Size = new System.Drawing.Size(244, 539);
            this.groupBox59.TabIndex = 15;
            this.groupBox59.TabStop = false;
            this.groupBox59.Text = "Команды";
            // 
            // _commandsDataGridView
            // 
            this._commandsDataGridView.AllowUserToAddRows = false;
            this._commandsDataGridView.AllowUserToDeleteRows = false;
            this._commandsDataGridView.AllowUserToResizeColumns = false;
            this._commandsDataGridView.AllowUserToResizeRows = false;
            this._commandsDataGridView.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._commandsDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this._commandsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._commandsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column5,
            this._typeCol,
            this._passCol,
            this._jsCol});
            this._commandsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._commandsDataGridView.Location = new System.Drawing.Point(3, 16);
            this._commandsDataGridView.Name = "_commandsDataGridView";
            this._commandsDataGridView.RowHeadersVisible = false;
            this._commandsDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._commandsDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this._commandsDataGridView.Size = new System.Drawing.Size(238, 520);
            this._commandsDataGridView.TabIndex = 0;
            this._commandsDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // Column5
            // 
            this.Column5.HeaderText = "№";
            this.Column5.Name = "Column5";
            this.Column5.Width = 20;
            // 
            // _typeCol
            // 
            this._typeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._typeCol.HeaderText = "Тип";
            this._typeCol.Name = "_typeCol";
            this._typeCol.Width = 32;
            // 
            // _passCol
            // 
            this._passCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._passCol.HeaderText = "Пароль";
            this._passCol.Name = "_passCol";
            this._passCol.Width = 51;
            // 
            // _jsCol
            // 
            this._jsCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._jsCol.HeaderText = "ЖС";
            this._jsCol.Name = "_jsCol";
            this._jsCol.Width = 31;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this._antiBounceDataGridView);
            this.groupBox22.Location = new System.Drawing.Point(268, 1);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(240, 539);
            this.groupBox22.TabIndex = 14;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Антидребезговая задержка";
            // 
            // _antiBounceDataGridView
            // 
            this._antiBounceDataGridView.AllowUserToAddRows = false;
            this._antiBounceDataGridView.AllowUserToDeleteRows = false;
            this._antiBounceDataGridView.AllowUserToResizeColumns = false;
            this._antiBounceDataGridView.AllowUserToResizeRows = false;
            this._antiBounceDataGridView.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._antiBounceDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this._antiBounceDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._antiBounceDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewComboBoxColumn2,
            this.dataGridViewComboBoxColumn3});
            this._antiBounceDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._antiBounceDataGridView.Location = new System.Drawing.Point(3, 16);
            this._antiBounceDataGridView.Name = "_antiBounceDataGridView";
            this._antiBounceDataGridView.RowHeadersVisible = false;
            this._antiBounceDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._antiBounceDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this._antiBounceDataGridView.RowTemplate.Height = 24;
            this._antiBounceDataGridView.Size = new System.Drawing.Size(234, 520);
            this._antiBounceDataGridView.TabIndex = 1;
            this._antiBounceDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "№";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 40;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.HeaderText = "Тип";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn2.Width = 32;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn3.Width = 61;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this._indComboBox);
            this.groupBox18.Location = new System.Drawing.Point(8, 191);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(254, 52);
            this.groupBox18.TabIndex = 4;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Сброс блинкеров";
            // 
            // _indComboBox
            // 
            this._indComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._indComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._indComboBox.FormattingEnabled = true;
            this._indComboBox.Location = new System.Drawing.Point(76, 19);
            this._indComboBox.Name = "_indComboBox";
            this._indComboBox.Size = new System.Drawing.Size(167, 21);
            this._indComboBox.TabIndex = 0;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label80);
            this.groupBox15.Controls.Add(this._grUst6ComboBox);
            this.groupBox15.Controls.Add(this.label79);
            this.groupBox15.Controls.Add(this._grUst5ComboBox);
            this.groupBox15.Controls.Add(this.label78);
            this.groupBox15.Controls.Add(this._grUst4ComboBox);
            this.groupBox15.Controls.Add(this.label77);
            this.groupBox15.Controls.Add(this._grUst3ComboBox);
            this.groupBox15.Controls.Add(this.label76);
            this.groupBox15.Controls.Add(this._grUst2ComboBox);
            this.groupBox15.Controls.Add(this.label1);
            this.groupBox15.Controls.Add(this._grUst1ComboBox);
            this.groupBox15.Location = new System.Drawing.Point(8, 1);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(254, 184);
            this.groupBox15.TabIndex = 3;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Группы уставок";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(6, 156);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(51, 13);
            this.label80.TabIndex = 11;
            this.label80.Text = "Группа 6";
            // 
            // _grUst6ComboBox
            // 
            this._grUst6ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst6ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst6ComboBox.FormattingEnabled = true;
            this._grUst6ComboBox.Location = new System.Drawing.Point(76, 153);
            this._grUst6ComboBox.Name = "_grUst6ComboBox";
            this._grUst6ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst6ComboBox.TabIndex = 10;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(6, 129);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(51, 13);
            this.label79.TabIndex = 9;
            this.label79.Text = "Группа 5";
            // 
            // _grUst5ComboBox
            // 
            this._grUst5ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst5ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst5ComboBox.FormattingEnabled = true;
            this._grUst5ComboBox.Location = new System.Drawing.Point(76, 126);
            this._grUst5ComboBox.Name = "_grUst5ComboBox";
            this._grUst5ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst5ComboBox.TabIndex = 8;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(6, 102);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(51, 13);
            this.label78.TabIndex = 7;
            this.label78.Text = "Группа 4";
            // 
            // _grUst4ComboBox
            // 
            this._grUst4ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst4ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst4ComboBox.FormattingEnabled = true;
            this._grUst4ComboBox.Location = new System.Drawing.Point(76, 99);
            this._grUst4ComboBox.Name = "_grUst4ComboBox";
            this._grUst4ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst4ComboBox.TabIndex = 6;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(6, 75);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(51, 13);
            this.label77.TabIndex = 5;
            this.label77.Text = "Группа 3";
            // 
            // _grUst3ComboBox
            // 
            this._grUst3ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst3ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst3ComboBox.FormattingEnabled = true;
            this._grUst3ComboBox.Location = new System.Drawing.Point(76, 72);
            this._grUst3ComboBox.Name = "_grUst3ComboBox";
            this._grUst3ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst3ComboBox.TabIndex = 4;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(6, 48);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(51, 13);
            this.label76.TabIndex = 3;
            this.label76.Text = "Группа 2";
            // 
            // _grUst2ComboBox
            // 
            this._grUst2ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst2ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst2ComboBox.FormattingEnabled = true;
            this._grUst2ComboBox.Location = new System.Drawing.Point(76, 45);
            this._grUst2ComboBox.Name = "_grUst2ComboBox";
            this._grUst2ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst2ComboBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Группа 1";
            // 
            // _grUst1ComboBox
            // 
            this._grUst1ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst1ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst1ComboBox.FormattingEnabled = true;
            this._grUst1ComboBox.Location = new System.Drawing.Point(76, 18);
            this._grUst1ComboBox.Name = "_grUst1ComboBox";
            this._grUst1ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst1ComboBox.TabIndex = 0;
            // 
            // _setpointPage
            // 
            this._setpointPage.Controls.Add(this.groupBox5);
            this._setpointPage.Controls.Add(this.groupBox25);
            this._setpointPage.Controls.Add(this.groupBox24);
            this._setpointPage.Controls.Add(this.groupBox1);
            this._setpointPage.Controls.Add(this.groupBox7);
            this._setpointPage.Location = new System.Drawing.Point(4, 22);
            this._setpointPage.Name = "_setpointPage";
            this._setpointPage.Size = new System.Drawing.Size(981, 553);
            this._setpointPage.TabIndex = 4;
            this._setpointPage.Text = "Уставки";
            this._setpointPage.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._inpAddCombo);
            this.groupBox5.Location = new System.Drawing.Point(562, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(139, 54);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Вход опорного сигнала";
            // 
            // _inpAddCombo
            // 
            this._inpAddCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inpAddCombo.FormattingEnabled = true;
            this._inpAddCombo.Location = new System.Drawing.Point(6, 21);
            this._inpAddCombo.Name = "_inpAddCombo";
            this._inpAddCombo.Size = new System.Drawing.Size(127, 21);
            this._inpAddCombo.TabIndex = 0;
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this._resistCheck);
            this.groupBox25.Location = new System.Drawing.Point(292, 3);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(264, 54);
            this.groupBox25.TabIndex = 5;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "Выводимое сопротивление";
            // 
            // _resistCheck
            // 
            this._resistCheck.AutoSize = true;
            this._resistCheck.Location = new System.Drawing.Point(6, 21);
            this._resistCheck.Name = "_resistCheck";
            this._resistCheck.Size = new System.Drawing.Size(252, 17);
            this._resistCheck.TabIndex = 4;
            this._resistCheck.Text = "Ввод сопротивления в первичных значениях";
            this._resistCheck.UseVisualStyleBackColor = true;
            this._resistCheck.CheckedChanged += new System.EventHandler(this._resistCheck_CheckedChanged);
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this._applyCopySetpoinsButton);
            this.groupBox24.Controls.Add(this._copySetpoinsGroupComboBox);
            this.groupBox24.Location = new System.Drawing.Point(112, 3);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(174, 54);
            this.groupBox24.TabIndex = 3;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Копировать в";
            // 
            // _applyCopySetpoinsButton
            // 
            this._applyCopySetpoinsButton.Location = new System.Drawing.Point(95, 17);
            this._applyCopySetpoinsButton.Name = "_applyCopySetpoinsButton";
            this._applyCopySetpoinsButton.Size = new System.Drawing.Size(75, 23);
            this._applyCopySetpoinsButton.TabIndex = 1;
            this._applyCopySetpoinsButton.Text = "Применить";
            this._applyCopySetpoinsButton.UseVisualStyleBackColor = true;
            this._applyCopySetpoinsButton.Click += new System.EventHandler(this._applyCopySetpoinsButton_Click);
            // 
            // _copySetpoinsGroupComboBox
            // 
            this._copySetpoinsGroupComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._copySetpoinsGroupComboBox.FormattingEnabled = true;
            this._copySetpoinsGroupComboBox.Location = new System.Drawing.Point(6, 19);
            this._copySetpoinsGroupComboBox.Name = "_copySetpoinsGroupComboBox";
            this._copySetpoinsGroupComboBox.Size = new System.Drawing.Size(83, 21);
            this._copySetpoinsGroupComboBox.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this._setpointsTab);
            this.groupBox1.Location = new System.Drawing.Point(3, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(972, 491);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // _setpointsTab
            // 
            this._setpointsTab.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this._setpointsTab.Controls.Add(this._measureTransPage);
            this._setpointsTab.Controls.Add(this._defences);
            this._setpointsTab.Controls.Add(this._lsGr);
            this._setpointsTab.Controls.Add(this._apvAvrTabPage);
            this._setpointsTab.Controls.Add(this._ksiunpTabPage);
            this._setpointsTab.Controls.Add(this._tuAndTbTabPage);
            this._setpointsTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this._setpointsTab.Location = new System.Drawing.Point(3, 16);
            this._setpointsTab.Name = "_setpointsTab";
            this._setpointsTab.SelectedIndex = 0;
            this._setpointsTab.Size = new System.Drawing.Size(966, 472);
            this._setpointsTab.TabIndex = 2;
            // 
            // _measureTransPage
            // 
            this._measureTransPage.Controls.Add(this._paramEngineGroupBox);
            this._measureTransPage.Controls.Add(this.passportDataGroup);
            this._measureTransPage.Controls.Add(this.groupBox48);
            this._measureTransPage.Controls.Add(this.groupBox2);
            this._measureTransPage.Controls.Add(this.groupBox4);
            this._measureTransPage.Controls.Add(this.groupBox10);
            this._measureTransPage.Controls.Add(this.label81);
            this._measureTransPage.Controls.Add(this.label74);
            this._measureTransPage.Location = new System.Drawing.Point(4, 25);
            this._measureTransPage.Name = "_measureTransPage";
            this._measureTransPage.Size = new System.Drawing.Size(958, 443);
            this._measureTransPage.TabIndex = 15;
            this._measureTransPage.Text = "Параметры измерений";
            this._measureTransPage.UseVisualStyleBackColor = true;
            // 
            // _paramEngineGroupBox
            // 
            this._paramEngineGroupBox.Controls.Add(this.label140);
            this._paramEngineGroupBox.Controls.Add(this._inputQResetLabel);
            this._paramEngineGroupBox.Controls.Add(this._engineNreset);
            this._paramEngineGroupBox.Controls.Add(this._engineQresetGr1);
            this._paramEngineGroupBox.Controls.Add(this._qBox);
            this._paramEngineGroupBox.Controls.Add(this.label135);
            this._paramEngineGroupBox.Controls.Add(this._tCount);
            this._paramEngineGroupBox.Controls.Add(this.label92);
            this._paramEngineGroupBox.Controls.Add(this._tStartBox);
            this._paramEngineGroupBox.Controls.Add(this.label48);
            this._paramEngineGroupBox.Controls.Add(this._iStartBox);
            this._paramEngineGroupBox.Controls.Add(this._iStartLabel);
            this._paramEngineGroupBox.Controls.Add(this._engineIdv);
            this._paramEngineGroupBox.Controls.Add(this._idvLabel);
            this._paramEngineGroupBox.Controls.Add(this._engineCoolingTimeGr1);
            this._paramEngineGroupBox.Controls.Add(this.label170);
            this._paramEngineGroupBox.Controls.Add(this._engineHeatingTimeGr1);
            this._paramEngineGroupBox.Controls.Add(this.label171);
            this._paramEngineGroupBox.Location = new System.Drawing.Point(467, 3);
            this._paramEngineGroupBox.Name = "_paramEngineGroupBox";
            this._paramEngineGroupBox.Size = new System.Drawing.Size(216, 217);
            this._paramEngineGroupBox.TabIndex = 39;
            this._paramEngineGroupBox.TabStop = false;
            this._paramEngineGroupBox.Text = "Тепловая модель";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(12, 175);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(75, 13);
            this.label140.TabIndex = 14;
            this.label140.Text = "Вход N сброс";
            // 
            // _inputQResetLabel
            // 
            this._inputQResetLabel.AutoSize = true;
            this._inputQResetLabel.Location = new System.Drawing.Point(12, 155);
            this._inputQResetLabel.Name = "_inputQResetLabel";
            this._inputQResetLabel.Size = new System.Drawing.Size(75, 13);
            this._inputQResetLabel.TabIndex = 14;
            this._inputQResetLabel.Text = "Вход Q сброс";
            // 
            // _engineNreset
            // 
            this._engineNreset.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._engineNreset.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._engineNreset.FormattingEnabled = true;
            this._engineNreset.Location = new System.Drawing.Point(90, 172);
            this._engineNreset.Name = "_engineNreset";
            this._engineNreset.Size = new System.Drawing.Size(114, 21);
            this._engineNreset.TabIndex = 12;
            // 
            // _engineQresetGr1
            // 
            this._engineQresetGr1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._engineQresetGr1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._engineQresetGr1.FormattingEnabled = true;
            this._engineQresetGr1.Location = new System.Drawing.Point(90, 152);
            this._engineQresetGr1.Name = "_engineQresetGr1";
            this._engineQresetGr1.Size = new System.Drawing.Size(114, 21);
            this._engineQresetGr1.TabIndex = 12;
            // 
            // _qBox
            // 
            this._qBox.Location = new System.Drawing.Point(135, 133);
            this._qBox.Name = "_qBox";
            this._qBox.Size = new System.Drawing.Size(69, 20);
            this._qBox.TabIndex = 3;
            this._qBox.Tag = "65000";
            this._qBox.Text = "0";
            this._qBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(12, 136);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(46, 13);
            this.label135.TabIndex = 2;
            this.label135.Text = "Qгор, %";
            // 
            // _tCount
            // 
            this._tCount.Location = new System.Drawing.Point(135, 114);
            this._tCount.Name = "_tCount";
            this._tCount.Size = new System.Drawing.Size(69, 20);
            this._tCount.TabIndex = 3;
            this._tCount.Tag = "65000";
            this._tCount.Text = "0";
            this._tCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(12, 117);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(49, 13);
            this.label92.TabIndex = 2;
            this.label92.Text = "Tдлит, с";
            // 
            // _tStartBox
            // 
            this._tStartBox.Location = new System.Drawing.Point(135, 95);
            this._tStartBox.Name = "_tStartBox";
            this._tStartBox.Size = new System.Drawing.Size(69, 20);
            this._tStartBox.TabIndex = 3;
            this._tStartBox.Tag = "65000";
            this._tStartBox.Text = "0";
            this._tStartBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(12, 98);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(57, 13);
            this.label48.TabIndex = 2;
            this.label48.Text = "Tпуск, мс";
            // 
            // _iStartBox
            // 
            this._iStartBox.Location = new System.Drawing.Point(135, 76);
            this._iStartBox.Name = "_iStartBox";
            this._iStartBox.Size = new System.Drawing.Size(69, 20);
            this._iStartBox.TabIndex = 3;
            this._iStartBox.Tag = "65000";
            this._iStartBox.Text = "0";
            this._iStartBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _iStartLabel
            // 
            this._iStartLabel.AutoSize = true;
            this._iStartLabel.Location = new System.Drawing.Point(12, 79);
            this._iStartLabel.Name = "_iStartLabel";
            this._iStartLabel.Size = new System.Drawing.Size(48, 13);
            this._iStartLabel.TabIndex = 2;
            this._iStartLabel.Text = "Iпуск, Iн";
            // 
            // _engineIdv
            // 
            this._engineIdv.Location = new System.Drawing.Point(135, 57);
            this._engineIdv.Name = "_engineIdv";
            this._engineIdv.Size = new System.Drawing.Size(69, 20);
            this._engineIdv.TabIndex = 3;
            this._engineIdv.Tag = "65000";
            this._engineIdv.Text = "0";
            this._engineIdv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _idvLabel
            // 
            this._idvLabel.AutoSize = true;
            this._idvLabel.Location = new System.Drawing.Point(12, 60);
            this._idvLabel.Name = "_idvLabel";
            this._idvLabel.Size = new System.Drawing.Size(37, 13);
            this._idvLabel.TabIndex = 2;
            this._idvLabel.Text = "Iдв, Iн";
            // 
            // _engineCoolingTimeGr1
            // 
            this._engineCoolingTimeGr1.Location = new System.Drawing.Point(135, 38);
            this._engineCoolingTimeGr1.Name = "_engineCoolingTimeGr1";
            this._engineCoolingTimeGr1.Size = new System.Drawing.Size(69, 20);
            this._engineCoolingTimeGr1.TabIndex = 3;
            this._engineCoolingTimeGr1.Tag = "65000";
            this._engineCoolingTimeGr1.Text = "0";
            this._engineCoolingTimeGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(12, 41);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(90, 13);
            this.label170.TabIndex = 2;
            this.label170.Text = "Т охлаждения, с";
            // 
            // _engineHeatingTimeGr1
            // 
            this._engineHeatingTimeGr1.Location = new System.Drawing.Point(135, 19);
            this._engineHeatingTimeGr1.Name = "_engineHeatingTimeGr1";
            this._engineHeatingTimeGr1.Size = new System.Drawing.Size(69, 20);
            this._engineHeatingTimeGr1.TabIndex = 1;
            this._engineHeatingTimeGr1.Tag = "65000";
            this._engineHeatingTimeGr1.Text = "0";
            this._engineHeatingTimeGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Location = new System.Drawing.Point(12, 22);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(67, 13);
            this.label171.TabIndex = 0;
            this.label171.Text = "T нагрева,c";
            // 
            // passportDataGroup
            // 
            this.passportDataGroup.Controls.Add(this._sn);
            this.passportDataGroup.Controls.Add(this.label220);
            this.passportDataGroup.Controls.Add(this._kpdP);
            this.passportDataGroup.Controls.Add(this.label221);
            this.passportDataGroup.Controls.Add(this._cosfP);
            this.passportDataGroup.Controls.Add(this.label225);
            this.passportDataGroup.Controls.Add(this.label226);
            this.passportDataGroup.Controls.Add(this._ppd);
            this.passportDataGroup.Controls.Add(this._ppdCombo);
            this.passportDataGroup.Location = new System.Drawing.Point(227, 300);
            this.passportDataGroup.Name = "passportDataGroup";
            this.passportDataGroup.Size = new System.Drawing.Size(234, 109);
            this.passportDataGroup.TabIndex = 38;
            this.passportDataGroup.TabStop = false;
            this.passportDataGroup.Text = "Паспортные данные";
            this.passportDataGroup.Enter += new System.EventHandler(this.passportDataGroup_Enter);
            // 
            // _sn
            // 
            this._sn.Location = new System.Drawing.Point(147, 84);
            this._sn.Name = "_sn";
            this._sn.ReadOnly = true;
            this._sn.Size = new System.Drawing.Size(81, 20);
            this._sn.TabIndex = 47;
            this._sn.Tag = "65000";
            this._sn.Text = "0";
            this._sn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label220
            // 
            this.label220.AutoSize = true;
            this.label220.Location = new System.Drawing.Point(6, 87);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(135, 13);
            this.label220.TabIndex = 46;
            this.label220.Text = "Sн = Pн*100/(cosf*КПД) =";
            // 
            // _kpdP
            // 
            this._kpdP.Location = new System.Drawing.Point(57, 57);
            this._kpdP.Name = "_kpdP";
            this._kpdP.Size = new System.Drawing.Size(47, 20);
            this._kpdP.TabIndex = 45;
            this._kpdP.Text = "0";
            this._kpdP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label221
            // 
            this.label221.AutoSize = true;
            this.label221.Location = new System.Drawing.Point(6, 60);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(45, 13);
            this.label221.TabIndex = 44;
            this.label221.Text = "КПД, %";
            // 
            // _cosfP
            // 
            this._cosfP.Location = new System.Drawing.Point(57, 38);
            this._cosfP.Name = "_cosfP";
            this._cosfP.Size = new System.Drawing.Size(47, 20);
            this._cosfP.TabIndex = 42;
            this._cosfP.Text = "0";
            this._cosfP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label225
            // 
            this.label225.AutoSize = true;
            this.label225.Location = new System.Drawing.Point(6, 41);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(27, 13);
            this.label225.TabIndex = 40;
            this.label225.Text = "cosf";
            // 
            // label226
            // 
            this.label226.AutoSize = true;
            this.label226.Location = new System.Drawing.Point(6, 21);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(14, 13);
            this.label226.TabIndex = 41;
            this.label226.Text = "P";
            // 
            // _ppd
            // 
            this._ppd.Location = new System.Drawing.Point(57, 19);
            this._ppd.Name = "_ppd";
            this._ppd.Size = new System.Drawing.Size(47, 20);
            this._ppd.TabIndex = 43;
            this._ppd.Text = "0";
            this._ppd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._ppd.TextChanged += new System.EventHandler(this._passport_TextChanged);
            // 
            // _ppdCombo
            // 
            this._ppdCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._ppdCombo.FormattingEnabled = true;
            this._ppdCombo.Location = new System.Drawing.Point(105, 19);
            this._ppdCombo.Name = "_ppdCombo";
            this._ppdCombo.Size = new System.Drawing.Size(49, 21);
            this._ppdCombo.TabIndex = 39;
            // 
            // groupBox48
            // 
            this.groupBox48.Controls.Add(this.checkBox1);
            this.groupBox48.Controls.Add(this.label139);
            this.groupBox48.Controls.Add(this._resetTnGr1);
            this.groupBox48.Controls.Add(this._errorX1_comboGr1);
            this.groupBox48.Controls.Add(this._faultTHn1Label);
            this.groupBox48.Controls.Add(this._errorX_comboGr1);
            this.groupBox48.Controls.Add(this.label6);
            this.groupBox48.Controls.Add(this._i0u0CheckGr1);
            this.groupBox48.Controls.Add(this._i2u2CheckGr1);
            this.groupBox48.Controls.Add(this._tdContrCepGr1);
            this.groupBox48.Controls.Add(this._errorL_comboGr1);
            this.groupBox48.Controls.Add(this.label10);
            this.groupBox48.Controls.Add(this.label137);
            this.groupBox48.Controls.Add(this._iMinContrCepGr1);
            this.groupBox48.Controls.Add(this.label130);
            this.groupBox48.Controls.Add(this._uMinContrCepGr1);
            this.groupBox48.Controls.Add(this.label125);
            this.groupBox48.Controls.Add(this._u0ContrCepGr1);
            this.groupBox48.Controls.Add(this.label138);
            this.groupBox48.Controls.Add(this.label136);
            this.groupBox48.Controls.Add(this.label124);
            this.groupBox48.Controls.Add(this.label131);
            this.groupBox48.Controls.Add(this.label129);
            this.groupBox48.Controls.Add(this._u2ContrCepGr1);
            this.groupBox48.Controls.Add(this._tsContrCepGr1);
            this.groupBox48.Controls.Add(this._uDelContrCepGr1);
            this.groupBox48.Controls.Add(this.label128);
            this.groupBox48.Controls.Add(this._iDelContrCepGr1);
            this.groupBox48.Controls.Add(this._iMaxContrCepGr1);
            this.groupBox48.Controls.Add(this.label123);
            this.groupBox48.Controls.Add(this._uMaxContrCepGr1);
            this.groupBox48.Controls.Add(this.label120);
            this.groupBox48.Controls.Add(this._i0ContrCepGr1);
            this.groupBox48.Controls.Add(this.label122);
            this.groupBox48.Controls.Add(this._i2ContrCepGr1);
            this.groupBox48.Location = new System.Drawing.Point(227, 3);
            this.groupBox48.Name = "groupBox48";
            this.groupBox48.Size = new System.Drawing.Size(234, 296);
            this.groupBox48.TabIndex = 5;
            this.groupBox48.TabStop = false;
            this.groupBox48.Text = "Контроль цепей ТН";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(17, 228);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(89, 17);
            this.checkBox1.TabIndex = 43;
            this.checkBox1.Text = "Обр. 3-х фаз";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(14, 206);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(38, 13);
            this.label139.TabIndex = 42;
            this.label139.Text = "Сброс";
            // 
            // _resetTnGr1
            // 
            this._resetTnGr1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._resetTnGr1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._resetTnGr1.FormattingEnabled = true;
            this._resetTnGr1.Location = new System.Drawing.Point(73, 203);
            this._resetTnGr1.Name = "_resetTnGr1";
            this._resetTnGr1.Size = new System.Drawing.Size(93, 21);
            this._resetTnGr1.TabIndex = 41;
            // 
            // _errorX1_comboGr1
            // 
            this._errorX1_comboGr1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._errorX1_comboGr1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._errorX1_comboGr1.FormattingEnabled = true;
            this._errorX1_comboGr1.Location = new System.Drawing.Point(124, 59);
            this._errorX1_comboGr1.Name = "_errorX1_comboGr1";
            this._errorX1_comboGr1.Size = new System.Drawing.Size(95, 21);
            this._errorX1_comboGr1.TabIndex = 40;
            // 
            // _faultTHn1Label
            // 
            this._faultTHn1Label.AutoSize = true;
            this._faultTHn1Label.Location = new System.Drawing.Point(14, 62);
            this._faultTHn1Label.Name = "_faultTHn1Label";
            this._faultTHn1Label.Size = new System.Drawing.Size(78, 13);
            this._faultTHn1Label.TabIndex = 39;
            this._faultTHn1Label.Text = "Неиспр. ТНn1";
            // 
            // _errorX_comboGr1
            // 
            this._errorX_comboGr1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._errorX_comboGr1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._errorX_comboGr1.FormattingEnabled = true;
            this._errorX_comboGr1.Location = new System.Drawing.Point(124, 39);
            this._errorX_comboGr1.Name = "_errorX_comboGr1";
            this._errorX_comboGr1.Size = new System.Drawing.Size(95, 21);
            this._errorX_comboGr1.TabIndex = 40;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.TabIndex = 39;
            this.label6.Text = "Неиспр. ТНn";
            // 
            // _i0u0CheckGr1
            // 
            this._i0u0CheckGr1.AutoSize = true;
            this._i0u0CheckGr1.Location = new System.Drawing.Point(16, 105);
            this._i0u0CheckGr1.Name = "_i0u0CheckGr1";
            this._i0u0CheckGr1.Size = new System.Drawing.Size(15, 14);
            this._i0u0CheckGr1.TabIndex = 3;
            this._i0u0CheckGr1.UseVisualStyleBackColor = true;
            // 
            // _i2u2CheckGr1
            // 
            this._i2u2CheckGr1.AutoSize = true;
            this._i2u2CheckGr1.Location = new System.Drawing.Point(16, 84);
            this._i2u2CheckGr1.Name = "_i2u2CheckGr1";
            this._i2u2CheckGr1.Size = new System.Drawing.Size(15, 14);
            this._i2u2CheckGr1.TabIndex = 3;
            this._i2u2CheckGr1.UseVisualStyleBackColor = true;
            // 
            // _tdContrCepGr1
            // 
            this._tdContrCepGr1.Location = new System.Drawing.Point(73, 162);
            this._tdContrCepGr1.Name = "_tdContrCepGr1";
            this._tdContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._tdContrCepGr1.TabIndex = 2;
            this._tdContrCepGr1.Text = "0";
            this._tdContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _errorL_comboGr1
            // 
            this._errorL_comboGr1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._errorL_comboGr1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._errorL_comboGr1.FormattingEnabled = true;
            this._errorL_comboGr1.Location = new System.Drawing.Point(124, 19);
            this._errorL_comboGr1.Name = "_errorL_comboGr1";
            this._errorL_comboGr1.Size = new System.Drawing.Size(95, 21);
            this._errorL_comboGr1.TabIndex = 35;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "Неиспр. ТН";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(14, 165);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(40, 13);
            this.label137.TabIndex = 1;
            this.label137.Text = "Td, мс";
            // 
            // _iMinContrCepGr1
            // 
            this._iMinContrCepGr1.Location = new System.Drawing.Point(172, 142);
            this._iMinContrCepGr1.Name = "_iMinContrCepGr1";
            this._iMinContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._iMinContrCepGr1.TabIndex = 2;
            this._iMinContrCepGr1.Text = "0";
            this._iMinContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(126, 145);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(41, 13);
            this.label130.TabIndex = 1;
            this.label130.Text = "Imin, Iн";
            // 
            // _uMinContrCepGr1
            // 
            this._uMinContrCepGr1.Location = new System.Drawing.Point(172, 122);
            this._uMinContrCepGr1.Name = "_uMinContrCepGr1";
            this._uMinContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._uMinContrCepGr1.TabIndex = 2;
            this._uMinContrCepGr1.Text = "0";
            this._uMinContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(126, 125);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(44, 13);
            this.label125.TabIndex = 1;
            this.label125.Text = "Umin, В";
            // 
            // _u0ContrCepGr1
            // 
            this._u0ContrCepGr1.Location = new System.Drawing.Point(172, 102);
            this._u0ContrCepGr1.Name = "_u0ContrCepGr1";
            this._u0ContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._u0ContrCepGr1.TabIndex = 2;
            this._u0ContrCepGr1.Text = "0";
            this._u0ContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(14, 186);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(39, 13);
            this.label138.TabIndex = 1;
            this.label138.Text = "Ts, мс";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(14, 270);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(35, 13);
            this.label136.TabIndex = 1;
            this.label136.Text = "dU, %";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(126, 105);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(40, 13);
            this.label124.TabIndex = 1;
            this.label124.Text = "3U0, В";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(14, 250);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(30, 13);
            this.label131.TabIndex = 1;
            this.label131.Text = "dI, %";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(14, 144);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(44, 13);
            this.label129.TabIndex = 1;
            this.label129.Text = "Imax, Iн";
            // 
            // _u2ContrCepGr1
            // 
            this._u2ContrCepGr1.Location = new System.Drawing.Point(172, 82);
            this._u2ContrCepGr1.Name = "_u2ContrCepGr1";
            this._u2ContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._u2ContrCepGr1.TabIndex = 2;
            this._u2ContrCepGr1.Text = "0";
            this._u2ContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _tsContrCepGr1
            // 
            this._tsContrCepGr1.Location = new System.Drawing.Point(73, 182);
            this._tsContrCepGr1.Name = "_tsContrCepGr1";
            this._tsContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._tsContrCepGr1.TabIndex = 2;
            this._tsContrCepGr1.Text = "0";
            this._tsContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _uDelContrCepGr1
            // 
            this._uDelContrCepGr1.Location = new System.Drawing.Point(73, 267);
            this._uDelContrCepGr1.Name = "_uDelContrCepGr1";
            this._uDelContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._uDelContrCepGr1.TabIndex = 2;
            this._uDelContrCepGr1.Text = "0";
            this._uDelContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(14, 125);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(44, 13);
            this.label128.TabIndex = 1;
            this.label128.Text = "Umax,В";
            // 
            // _iDelContrCepGr1
            // 
            this._iDelContrCepGr1.Location = new System.Drawing.Point(73, 247);
            this._iDelContrCepGr1.Name = "_iDelContrCepGr1";
            this._iDelContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._iDelContrCepGr1.TabIndex = 2;
            this._iDelContrCepGr1.Text = "0";
            this._iDelContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _iMaxContrCepGr1
            // 
            this._iMaxContrCepGr1.Location = new System.Drawing.Point(73, 142);
            this._iMaxContrCepGr1.Name = "_iMaxContrCepGr1";
            this._iMaxContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._iMaxContrCepGr1.TabIndex = 2;
            this._iMaxContrCepGr1.Text = "0";
            this._iMaxContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(30, 105);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(37, 13);
            this.label123.TabIndex = 1;
            this.label123.Text = "3I0, Iн";
            // 
            // _uMaxContrCepGr1
            // 
            this._uMaxContrCepGr1.Location = new System.Drawing.Point(73, 122);
            this._uMaxContrCepGr1.Name = "_uMaxContrCepGr1";
            this._uMaxContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._uMaxContrCepGr1.TabIndex = 2;
            this._uMaxContrCepGr1.Text = "0";
            this._uMaxContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(132, 86);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(34, 13);
            this.label120.TabIndex = 1;
            this.label120.Text = "U2, В";
            // 
            // _i0ContrCepGr1
            // 
            this._i0ContrCepGr1.Location = new System.Drawing.Point(73, 102);
            this._i0ContrCepGr1.Name = "_i0ContrCepGr1";
            this._i0ContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._i0ContrCepGr1.TabIndex = 2;
            this._i0ContrCepGr1.Text = "0";
            this._i0ContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(30, 85);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(31, 13);
            this.label122.TabIndex = 1;
            this.label122.Text = "I2, Iн";
            // 
            // _i2ContrCepGr1
            // 
            this._i2ContrCepGr1.Location = new System.Drawing.Point(73, 82);
            this._i2ContrCepGr1.Name = "_i2ContrCepGr1";
            this._i2ContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._i2ContrCepGr1.TabIndex = 2;
            this._i2ContrCepGr1.Text = "0";
            this._i2ContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.omp2);
            this.groupBox2.Controls.Add(this.label119);
            this.groupBox2.Controls.Add(this.omp1);
            this.groupBox2.Controls.Add(this.label118);
            this.groupBox2.Controls.Add(this._OMPmode_comboGr1);
            this.groupBox2.Controls.Add(this.label117);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this._l2);
            this.groupBox2.Controls.Add(this._l1);
            this.groupBox2.Controls.Add(this._l3);
            this.groupBox2.Controls.Add(this._l4);
            this.groupBox2.Controls.Add(this.label116);
            this.groupBox2.Location = new System.Drawing.Point(689, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(201, 217);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ОМП";
            // 
            // omp2
            // 
            this.omp2.Controls.Add(this.label113);
            this.omp2.Controls.Add(this.label112);
            this.omp2.Controls.Add(this.label157);
            this.omp2.Controls.Add(this._xud5p);
            this.omp2.Controls.Add(this._xud4p);
            this.omp2.Controls.Add(this._xud3p);
            this.omp2.Controls.Add(this.label158);
            this.omp2.Controls.Add(this.label162);
            this.omp2.Controls.Add(this._xud2p);
            this.omp2.Controls.Add(this._xud1p);
            this.omp2.Location = new System.Drawing.Point(1, 38);
            this.omp2.Name = "omp2";
            this.omp2.Size = new System.Drawing.Size(195, 97);
            this.omp2.TabIndex = 6;
            this.omp2.Visible = false;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(6, 79);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(102, 13);
            this.label113.TabIndex = 34;
            this.label113.Text = "Xуд5, Ом перв./км";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(6, 60);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(102, 13);
            this.label112.TabIndex = 34;
            this.label112.Text = "Xуд4, Ом перв./км";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(6, 41);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(102, 13);
            this.label157.TabIndex = 34;
            this.label157.Text = "Xуд3, Ом перв./км";
            // 
            // _xud5p
            // 
            this._xud5p.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xud5p.Location = new System.Drawing.Point(134, 77);
            this._xud5p.Name = "_xud5p";
            this._xud5p.Size = new System.Drawing.Size(60, 20);
            this._xud5p.TabIndex = 33;
            this._xud5p.Tag = "40";
            this._xud5p.Text = "0";
            this._xud5p.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _xud4p
            // 
            this._xud4p.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xud4p.Location = new System.Drawing.Point(134, 58);
            this._xud4p.Name = "_xud4p";
            this._xud4p.Size = new System.Drawing.Size(60, 20);
            this._xud4p.TabIndex = 33;
            this._xud4p.Tag = "40";
            this._xud4p.Text = "0";
            this._xud4p.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _xud3p
            // 
            this._xud3p.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xud3p.Location = new System.Drawing.Point(134, 39);
            this._xud3p.Name = "_xud3p";
            this._xud3p.Size = new System.Drawing.Size(60, 20);
            this._xud3p.TabIndex = 33;
            this._xud3p.Tag = "40";
            this._xud3p.Text = "0";
            this._xud3p.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(6, 3);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(102, 13);
            this.label158.TabIndex = 32;
            this.label158.Text = "Xуд1, Ом перв./км";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Location = new System.Drawing.Point(6, 22);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(102, 13);
            this.label162.TabIndex = 31;
            this.label162.Text = "Xуд2, Ом перв./км";
            // 
            // _xud2p
            // 
            this._xud2p.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xud2p.Location = new System.Drawing.Point(134, 20);
            this._xud2p.Name = "_xud2p";
            this._xud2p.Size = new System.Drawing.Size(60, 20);
            this._xud2p.TabIndex = 30;
            this._xud2p.Tag = "100";
            this._xud2p.Text = "0";
            this._xud2p.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _xud1p
            // 
            this._xud1p.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xud1p.Location = new System.Drawing.Point(134, 1);
            this._xud1p.Name = "_xud1p";
            this._xud1p.Size = new System.Drawing.Size(60, 20);
            this._xud1p.TabIndex = 29;
            this._xud1p.Tag = "1500";
            this._xud1p.Text = "0";
            this._xud1p.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(7, 194);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(39, 13);
            this.label119.TabIndex = 34;
            this.label119.Text = "L4, км";
            // 
            // omp1
            // 
            this.omp1.Controls.Add(this.label115);
            this.omp1.Controls.Add(this.label13);
            this.omp1.Controls.Add(this.label114);
            this.omp1.Controls.Add(this._xud3s);
            this.omp1.Controls.Add(this._l12);
            this.omp1.Controls.Add(this._xud5s);
            this.omp1.Controls.Add(this._l13);
            this.omp1.Controls.Add(this._xud4s);
            this.omp1.Controls.Add(this._xud2s);
            this.omp1.Controls.Add(this._xud1s);
            this.omp1.Location = new System.Drawing.Point(1, 38);
            this.omp1.Name = "omp1";
            this.omp1.Size = new System.Drawing.Size(195, 97);
            this.omp1.TabIndex = 6;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(3, 79);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(101, 13);
            this.label115.TabIndex = 34;
            this.label115.Text = "Xуд5, Ом втор./км";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 41);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(101, 13);
            this.label13.TabIndex = 34;
            this.label13.Text = "Xуд3, Ом втор./км";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(3, 60);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(101, 13);
            this.label114.TabIndex = 34;
            this.label114.Text = "Xуд4, Ом втор./км";
            // 
            // _xud3s
            // 
            this._xud3s.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xud3s.Location = new System.Drawing.Point(134, 39);
            this._xud3s.Name = "_xud3s";
            this._xud3s.Size = new System.Drawing.Size(60, 20);
            this._xud3s.TabIndex = 33;
            this._xud3s.Tag = "40";
            this._xud3s.Text = "0";
            this._xud3s.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _l12
            // 
            this._l12.AutoSize = true;
            this._l12.Location = new System.Drawing.Point(3, 3);
            this._l12.Name = "_l12";
            this._l12.Size = new System.Drawing.Size(101, 13);
            this._l12.TabIndex = 32;
            this._l12.Text = "Xуд1, Ом втор./км";
            // 
            // _xud5s
            // 
            this._xud5s.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xud5s.Location = new System.Drawing.Point(134, 77);
            this._xud5s.Name = "_xud5s";
            this._xud5s.Size = new System.Drawing.Size(60, 20);
            this._xud5s.TabIndex = 33;
            this._xud5s.Tag = "40";
            this._xud5s.Text = "0";
            this._xud5s.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _l13
            // 
            this._l13.AutoSize = true;
            this._l13.Location = new System.Drawing.Point(3, 22);
            this._l13.Name = "_l13";
            this._l13.Size = new System.Drawing.Size(101, 13);
            this._l13.TabIndex = 31;
            this._l13.Text = "Xуд2, Ом втор./км";
            // 
            // _xud4s
            // 
            this._xud4s.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xud4s.Location = new System.Drawing.Point(134, 58);
            this._xud4s.Name = "_xud4s";
            this._xud4s.Size = new System.Drawing.Size(60, 20);
            this._xud4s.TabIndex = 33;
            this._xud4s.Tag = "40";
            this._xud4s.Text = "0";
            this._xud4s.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _xud2s
            // 
            this._xud2s.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xud2s.Location = new System.Drawing.Point(134, 20);
            this._xud2s.Name = "_xud2s";
            this._xud2s.Size = new System.Drawing.Size(60, 20);
            this._xud2s.TabIndex = 30;
            this._xud2s.Tag = "100";
            this._xud2s.Text = "0";
            this._xud2s.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _xud1s
            // 
            this._xud1s.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xud1s.Location = new System.Drawing.Point(134, 1);
            this._xud1s.Name = "_xud1s";
            this._xud1s.Size = new System.Drawing.Size(60, 20);
            this._xud1s.TabIndex = 29;
            this._xud1s.Tag = "1500";
            this._xud1s.Text = "0";
            this._xud1s.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(7, 155);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(39, 13);
            this.label118.TabIndex = 34;
            this.label118.Text = "L2, км";
            // 
            // _OMPmode_comboGr1
            // 
            this._OMPmode_comboGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._OMPmode_comboGr1.FormattingEnabled = true;
            this._OMPmode_comboGr1.Location = new System.Drawing.Point(112, 16);
            this._OMPmode_comboGr1.Name = "_OMPmode_comboGr1";
            this._OMPmode_comboGr1.Size = new System.Drawing.Size(84, 21);
            this._OMPmode_comboGr1.TabIndex = 24;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(7, 175);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(39, 13);
            this.label117.TabIndex = 34;
            this.label117.Text = "L3, км";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Режим";
            // 
            // _l2
            // 
            this._l2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._l2.Location = new System.Drawing.Point(135, 154);
            this._l2.Name = "_l2";
            this._l2.Size = new System.Drawing.Size(60, 20);
            this._l2.TabIndex = 33;
            this._l2.Tag = "40";
            this._l2.Text = "0";
            this._l2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _l1
            // 
            this._l1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._l1.Location = new System.Drawing.Point(135, 135);
            this._l1.Name = "_l1";
            this._l1.Size = new System.Drawing.Size(60, 20);
            this._l1.TabIndex = 30;
            this._l1.Tag = "100";
            this._l1.Text = "0";
            this._l1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _l3
            // 
            this._l3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._l3.Location = new System.Drawing.Point(135, 173);
            this._l3.Name = "_l3";
            this._l3.Size = new System.Drawing.Size(60, 20);
            this._l3.TabIndex = 33;
            this._l3.Tag = "40";
            this._l3.Text = "0";
            this._l3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _l4
            // 
            this._l4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._l4.Location = new System.Drawing.Point(135, 192);
            this._l4.Name = "_l4";
            this._l4.Size = new System.Drawing.Size(60, 20);
            this._l4.TabIndex = 33;
            this._l4.Tag = "40";
            this._l4.Text = "0";
            this._l4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(7, 137);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(39, 13);
            this.label116.TabIndex = 31;
            this.label116.Text = "L1, км";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._ucaLabel);
            this.groupBox4.Controls.Add(this._ucaComboBox);
            this.groupBox4.Controls.Add(this._inputUcaComboBox);
            this.groupBox4.Controls.Add(this.label47);
            this.groupBox4.Controls.Add(this._KTHX1koef_comboGr1);
            this.groupBox4.Controls.Add(this._KTHXkoef_comboGr1);
            this.groupBox4.Controls.Add(this._kthn1XLabel);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this._kthn1Label);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this._KTHLkoef_comboGr1);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this._Uo_typeComboGr1);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this._KTHX1_BoxGr1);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this._KTHX_BoxGr1);
            this.groupBox4.Controls.Add(this._KTHL_BoxGr1);
            this.groupBox4.Location = new System.Drawing.Point(5, 249);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(216, 105);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Напряжения";
            // 
            // _ucaLabel
            // 
            this._ucaLabel.AutoSize = true;
            this._ucaLabel.Location = new System.Drawing.Point(6, 126);
            this._ucaLabel.Name = "_ucaLabel";
            this._ucaLabel.Size = new System.Drawing.Size(27, 13);
            this._ucaLabel.TabIndex = 42;
            this._ucaLabel.Text = "Uca";
            this._ucaLabel.Visible = false;
            // 
            // _ucaComboBox
            // 
            this._ucaComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._ucaComboBox.FormattingEnabled = true;
            this._ucaComboBox.Location = new System.Drawing.Point(69, 123);
            this._ucaComboBox.Name = "_ucaComboBox";
            this._ucaComboBox.Size = new System.Drawing.Size(85, 21);
            this._ucaComboBox.TabIndex = 41;
            this._ucaComboBox.Visible = false;
            // 
            // _inputUcaComboBox
            // 
            this._inputUcaComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inputUcaComboBox.FormattingEnabled = true;
            this._inputUcaComboBox.Location = new System.Drawing.Point(69, 102);
            this._inputUcaComboBox.Name = "_inputUcaComboBox";
            this._inputUcaComboBox.Size = new System.Drawing.Size(85, 21);
            this._inputUcaComboBox.TabIndex = 40;
            this._inputUcaComboBox.Visible = false;
            this._inputUcaComboBox.SelectedIndexChanged += new System.EventHandler(this.comboBox_selectedIndexChanged);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(6, 106);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(31, 13);
            this.label47.TabIndex = 39;
            this.label47.Text = "Вход";
            this.label47.Visible = false;
            // 
            // _KTHX1koef_comboGr1
            // 
            this._KTHX1koef_comboGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._KTHX1koef_comboGr1.FormattingEnabled = true;
            this._KTHX1koef_comboGr1.Location = new System.Drawing.Point(153, 78);
            this._KTHX1koef_comboGr1.Name = "_KTHX1koef_comboGr1";
            this._KTHX1koef_comboGr1.Size = new System.Drawing.Size(51, 21);
            this._KTHX1koef_comboGr1.TabIndex = 38;
            // 
            // _KTHXkoef_comboGr1
            // 
            this._KTHXkoef_comboGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._KTHXkoef_comboGr1.FormattingEnabled = true;
            this._KTHXkoef_comboGr1.Location = new System.Drawing.Point(153, 58);
            this._KTHXkoef_comboGr1.Name = "_KTHXkoef_comboGr1";
            this._KTHXkoef_comboGr1.Size = new System.Drawing.Size(51, 21);
            this._KTHXkoef_comboGr1.TabIndex = 38;
            // 
            // _kthn1XLabel
            // 
            this._kthn1XLabel.AutoSize = true;
            this._kthn1XLabel.Location = new System.Drawing.Point(135, 81);
            this._kthn1XLabel.Name = "_kthn1XLabel";
            this._kthn1XLabel.Size = new System.Drawing.Size(12, 13);
            this._kthn1XLabel.TabIndex = 37;
            this._kthn1XLabel.Text = "x";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(135, 61);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(12, 13);
            this.label11.TabIndex = 37;
            this.label11.Text = "x";
            // 
            // _kthn1Label
            // 
            this._kthn1Label.AutoSize = true;
            this._kthn1Label.Location = new System.Drawing.Point(6, 81);
            this._kthn1Label.Name = "_kthn1Label";
            this._kthn1Label.Size = new System.Drawing.Size(41, 13);
            this._kthn1Label.TabIndex = 36;
            this._kthn1Label.Text = "КТНn1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 36;
            this.label8.Text = "КТНn";
            // 
            // _KTHLkoef_comboGr1
            // 
            this._KTHLkoef_comboGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._KTHLkoef_comboGr1.FormattingEnabled = true;
            this._KTHLkoef_comboGr1.Location = new System.Drawing.Point(153, 38);
            this._KTHLkoef_comboGr1.Name = "_KTHLkoef_comboGr1";
            this._KTHLkoef_comboGr1.Size = new System.Drawing.Size(51, 21);
            this._KTHLkoef_comboGr1.TabIndex = 33;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(135, 41);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "x";
            // 
            // _Uo_typeComboGr1
            // 
            this._Uo_typeComboGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._Uo_typeComboGr1.FormattingEnabled = true;
            this._Uo_typeComboGr1.Location = new System.Drawing.Point(69, 18);
            this._Uo_typeComboGr1.Name = "_Uo_typeComboGr1";
            this._Uo_typeComboGr1.Size = new System.Drawing.Size(60, 21);
            this._Uo_typeComboGr1.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "Uo*";
            // 
            // _KTHX1_BoxGr1
            // 
            this._KTHX1_BoxGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._KTHX1_BoxGr1.Location = new System.Drawing.Point(69, 79);
            this._KTHX1_BoxGr1.Name = "_KTHX1_BoxGr1";
            this._KTHX1_BoxGr1.Size = new System.Drawing.Size(60, 20);
            this._KTHX1_BoxGr1.TabIndex = 24;
            this._KTHX1_BoxGr1.Tag = "100";
            this._KTHX1_BoxGr1.Text = "0";
            this._KTHX1_BoxGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "КТНф";
            // 
            // _KTHX_BoxGr1
            // 
            this._KTHX_BoxGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._KTHX_BoxGr1.Location = new System.Drawing.Point(69, 59);
            this._KTHX_BoxGr1.Name = "_KTHX_BoxGr1";
            this._KTHX_BoxGr1.Size = new System.Drawing.Size(60, 20);
            this._KTHX_BoxGr1.TabIndex = 24;
            this._KTHX_BoxGr1.Tag = "100";
            this._KTHX_BoxGr1.Text = "0";
            this._KTHX_BoxGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _KTHL_BoxGr1
            // 
            this._KTHL_BoxGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._KTHL_BoxGr1.Location = new System.Drawing.Point(69, 39);
            this._KTHL_BoxGr1.Name = "_KTHL_BoxGr1";
            this._KTHL_BoxGr1.Size = new System.Drawing.Size(60, 20);
            this._KTHL_BoxGr1.TabIndex = 23;
            this._KTHL_BoxGr1.Tag = "1500";
            this._KTHL_BoxGr1.Text = "0";
            this._KTHL_BoxGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._inpIn);
            this.groupBox10.Controls.Add(this.label256);
            this.groupBox10.Controls.Add(this._polarIn1);
            this.groupBox10.Controls.Add(this._polarIn);
            this.groupBox10.Controls.Add(this._polarIc);
            this.groupBox10.Controls.Add(this._polarIb);
            this.groupBox10.Controls.Add(this._polarIa);
            this.groupBox10.Controls.Add(this.label255);
            this.groupBox10.Controls.Add(this.label152);
            this.groupBox10.Controls.Add(this.label49);
            this.groupBox10.Controls.Add(this.label26);
            this.groupBox10.Controls.Add(this.label24);
            this.groupBox10.Controls.Add(this._alternationCB);
            this.groupBox10.Controls.Add(this.label53);
            this.groupBox10.Controls.Add(this._TT_typeComboGr1);
            this.groupBox10.Controls.Add(this.label25);
            this.groupBox10.Controls.Add(this.label3);
            this.groupBox10.Controls.Add(this._ITTX_BoxGr1);
            this.groupBox10.Controls.Add(this.label2);
            this.groupBox10.Controls.Add(this.label4);
            this.groupBox10.Controls.Add(this._ITTL_BoxGr1);
            this.groupBox10.Controls.Add(this._Im_BoxGr1);
            this.groupBox10.Location = new System.Drawing.Point(5, 3);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(216, 245);
            this.groupBox10.TabIndex = 0;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Тока";
            // 
            // _inpIn
            // 
            this._inpIn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inpIn.FormattingEnabled = true;
            this._inpIn.Location = new System.Drawing.Point(136, 54);
            this._inpIn.Name = "_inpIn";
            this._inpIn.Size = new System.Drawing.Size(60, 21);
            this._inpIn.TabIndex = 35;
            // 
            // label256
            // 
            this.label256.AutoSize = true;
            this.label256.Location = new System.Drawing.Point(7, 57);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(78, 13);
            this.label256.TabIndex = 34;
            this.label256.Text = "Токовый вход";
            // 
            // _polarIn1
            // 
            this._polarIn1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._polarIn1.FormattingEnabled = true;
            this._polarIn1.Location = new System.Drawing.Point(136, 214);
            this._polarIn1.Name = "_polarIn1";
            this._polarIn1.Size = new System.Drawing.Size(60, 21);
            this._polarIn1.TabIndex = 33;
            this._polarIn1.Visible = false;
            // 
            // _polarIn
            // 
            this._polarIn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._polarIn.FormattingEnabled = true;
            this._polarIn.Location = new System.Drawing.Point(136, 193);
            this._polarIn.Name = "_polarIn";
            this._polarIn.Size = new System.Drawing.Size(60, 21);
            this._polarIn.TabIndex = 32;
            // 
            // _polarIc
            // 
            this._polarIc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._polarIc.FormattingEnabled = true;
            this._polarIc.Location = new System.Drawing.Point(136, 153);
            this._polarIc.Name = "_polarIc";
            this._polarIc.Size = new System.Drawing.Size(60, 21);
            this._polarIc.TabIndex = 31;
            // 
            // _polarIb
            // 
            this._polarIb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._polarIb.FormattingEnabled = true;
            this._polarIb.Location = new System.Drawing.Point(136, 133);
            this._polarIb.Name = "_polarIb";
            this._polarIb.Size = new System.Drawing.Size(60, 21);
            this._polarIb.TabIndex = 30;
            // 
            // _polarIa
            // 
            this._polarIa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._polarIa.FormattingEnabled = true;
            this._polarIa.Location = new System.Drawing.Point(136, 113);
            this._polarIa.Name = "_polarIa";
            this._polarIa.Size = new System.Drawing.Size(60, 21);
            this._polarIa.TabIndex = 29;
            // 
            // label255
            // 
            this.label255.AutoSize = true;
            this.label255.Location = new System.Drawing.Point(7, 217);
            this.label255.Name = "label255";
            this.label255.Size = new System.Drawing.Size(86, 13);
            this.label255.TabIndex = 28;
            this.label255.Text = "Полярность In1";
            this.label255.Visible = false;
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(7, 196);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(80, 13);
            this.label152.TabIndex = 27;
            this.label152.Text = "Полярность In";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(7, 156);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(80, 13);
            this.label49.TabIndex = 26;
            this.label49.Text = "Полярность Ic";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(7, 136);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(80, 13);
            this.label26.TabIndex = 25;
            this.label26.Text = "Полярность Ib";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(7, 116);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(80, 13);
            this.label24.TabIndex = 24;
            this.label24.Text = "Полярность Ia";
            // 
            // _alternationCB
            // 
            this._alternationCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._alternationCB.FormattingEnabled = true;
            this._alternationCB.Location = new System.Drawing.Point(112, 34);
            this._alternationCB.Name = "_alternationCB";
            this._alternationCB.Size = new System.Drawing.Size(84, 21);
            this._alternationCB.TabIndex = 22;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(7, 37);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(75, 13);
            this.label53.TabIndex = 21;
            this.label53.Text = "Чередование";
            // 
            // _TT_typeComboGr1
            // 
            this._TT_typeComboGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TT_typeComboGr1.FormattingEnabled = true;
            this._TT_typeComboGr1.Location = new System.Drawing.Point(112, 13);
            this._TT_typeComboGr1.Name = "_TT_typeComboGr1";
            this._TT_typeComboGr1.Size = new System.Drawing.Size(84, 21);
            this._TT_typeComboGr1.TabIndex = 22;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 16);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(43, 13);
            this.label25.TabIndex = 21;
            this.label25.Text = "Тип ТT";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "ITTn, A";
            // 
            // _ITTX_BoxGr1
            // 
            this._ITTX_BoxGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ITTX_BoxGr1.Location = new System.Drawing.Point(136, 174);
            this._ITTX_BoxGr1.Name = "_ITTX_BoxGr1";
            this._ITTX_BoxGr1.Size = new System.Drawing.Size(60, 20);
            this._ITTX_BoxGr1.TabIndex = 19;
            this._ITTX_BoxGr1.Tag = "40";
            this._ITTX_BoxGr1.Text = "0";
            this._ITTX_BoxGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Iм, Iн";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "ITTф, A";
            // 
            // _ITTL_BoxGr1
            // 
            this._ITTL_BoxGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ITTL_BoxGr1.Location = new System.Drawing.Point(136, 94);
            this._ITTL_BoxGr1.Name = "_ITTL_BoxGr1";
            this._ITTL_BoxGr1.Size = new System.Drawing.Size(60, 20);
            this._ITTL_BoxGr1.TabIndex = 16;
            this._ITTL_BoxGr1.Tag = "100";
            this._ITTL_BoxGr1.Text = "0";
            this._ITTL_BoxGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _Im_BoxGr1
            // 
            this._Im_BoxGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Im_BoxGr1.Location = new System.Drawing.Point(136, 74);
            this._Im_BoxGr1.Name = "_Im_BoxGr1";
            this._Im_BoxGr1.Size = new System.Drawing.Size(60, 20);
            this._Im_BoxGr1.TabIndex = 15;
            this._Im_BoxGr1.Tag = "1500";
            this._Im_BoxGr1.Text = "0";
            this._Im_BoxGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(18, 388);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(152, 13);
            this.label81.TabIndex = 29;
            this.label81.Text = "защит I* в режиме 3I0 или In";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(4, 374);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(215, 13);
            this.label74.TabIndex = 29;
            this.label74.Text = "* - Напряжение пуска и поляризации для";
            // 
            // _defences
            // 
            this._defences.Controls.Add(this._defencesTabControl);
            this._defences.Location = new System.Drawing.Point(4, 25);
            this._defences.Name = "_defences";
            this._defences.Padding = new System.Windows.Forms.Padding(3);
            this._defences.Size = new System.Drawing.Size(958, 443);
            this._defences.TabIndex = 21;
            this._defences.Text = "Защиты";
            this._defences.UseVisualStyleBackColor = true;
            // 
            // _defencesTabControl
            // 
            this._defencesTabControl.Controls.Add(this._distDefence);
            this._defencesTabControl.Controls.Add(this._cornersDefence);
            this._defencesTabControl.Controls.Add(this._defenceI);
            this._defencesTabControl.Controls.Add(this._defenceIStar);
            this._defencesTabControl.Controls.Add(this._defenceI2I1);
            this._defencesTabControl.Controls.Add(this._startArcDefence);
            this._defencesTabControl.Controls.Add(this._defenceU);
            this._defencesTabControl.Controls.Add(this._defenceF);
            this._defencesTabControl.Controls.Add(this._defenceQ);
            this._defencesTabControl.Controls.Add(this._externalDefence);
            this._defencesTabControl.Controls.Add(this._powerDefence);
            this._defencesTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._defencesTabControl.Location = new System.Drawing.Point(3, 3);
            this._defencesTabControl.Name = "_defencesTabControl";
            this._defencesTabControl.SelectedIndex = 0;
            this._defencesTabControl.Size = new System.Drawing.Size(952, 437);
            this._defencesTabControl.TabIndex = 0;
            // 
            // _distDefence
            // 
            this._distDefence.BackColor = System.Drawing.SystemColors.Control;
            this._distDefence.Controls.Add(this.resistanceDefTabCtr1);
            this._distDefence.Location = new System.Drawing.Point(4, 22);
            this._distDefence.Name = "_distDefence";
            this._distDefence.Padding = new System.Windows.Forms.Padding(3);
            this._distDefence.Size = new System.Drawing.Size(944, 411);
            this._distDefence.TabIndex = 9;
            this._distDefence.Text = "Дистанц. защиты";
            // 
            // resistanceDefTabCtr1
            // 
            this.resistanceDefTabCtr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resistanceDefTabCtr1.IsPrimary = false;
            this.resistanceDefTabCtr1.Location = new System.Drawing.Point(3, 3);
            this.resistanceDefTabCtr1.Name = "resistanceDefTabCtr1";
            this.resistanceDefTabCtr1.ResistConfigUnion = null;
            this.resistanceDefTabCtr1.ResistConfigUnion771 = null;
            this.resistanceDefTabCtr1.Size = new System.Drawing.Size(938, 405);
            this.resistanceDefTabCtr1.TabIndex = 0;
            // 
            // _cornersDefence
            // 
            this._cornersDefence.BackColor = System.Drawing.SystemColors.Control;
            this._cornersDefence.Controls.Add(this.groupBox16);
            this._cornersDefence.Location = new System.Drawing.Point(4, 22);
            this._cornersDefence.Name = "_cornersDefence";
            this._cornersDefence.Padding = new System.Windows.Forms.Padding(3);
            this._cornersDefence.Size = new System.Drawing.Size(944, 411);
            this._cornersDefence.TabIndex = 8;
            this._cornersDefence.Text = "Углы линии";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.label32);
            this.groupBox16.Controls.Add(this.label31);
            this.groupBox16.Controls.Add(this.label28);
            this.groupBox16.Controls.Add(this.label134);
            this.groupBox16.Controls.Add(this.label133);
            this.groupBox16.Controls.Add(this.label132);
            this.groupBox16.Controls.Add(this.label100);
            this.groupBox16.Controls.Add(this._i2CornerGr1);
            this.groupBox16.Controls.Add(this._inCornerGr1);
            this.groupBox16.Controls.Add(this._i0CornerGr1);
            this.groupBox16.Controls.Add(this._iCornerGr1);
            this.groupBox16.Location = new System.Drawing.Point(3, 3);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(333, 124);
            this.groupBox16.TabIndex = 39;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Углы линии для токовых направленных защит";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(166, 94);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(83, 13);
            this.label32.TabIndex = 12;
            this.label32.Text = "в режиме по I2";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(8, 91);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(83, 13);
            this.label31.TabIndex = 11;
            this.label31.Text = "в режиме по In";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(165, 48);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(89, 13);
            this.label28.TabIndex = 10;
            this.label28.Text = "в режиме по 3I0";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(5, 78);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(107, 13);
            this.label134.TabIndex = 9;
            this.label134.Text = " fin для ступеней I*>";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(166, 81);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(104, 13);
            this.label133.TabIndex = 8;
            this.label133.Text = "fi2 для ступеней I*>";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(165, 35);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(104, 13);
            this.label132.TabIndex = 7;
            this.label132.Text = "fi0 для ступеней I*>";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(9, 39);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(100, 13);
            this.label100.TabIndex = 6;
            this.label100.Text = "fi1 для ступеней I>";
            // 
            // _i2CornerGr1
            // 
            this._i2CornerGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._i2CornerGr1.Location = new System.Drawing.Point(278, 81);
            this._i2CornerGr1.Name = "_i2CornerGr1";
            this._i2CornerGr1.Size = new System.Drawing.Size(38, 20);
            this._i2CornerGr1.TabIndex = 5;
            this._i2CornerGr1.Tag = "360";
            this._i2CornerGr1.Text = "0";
            this._i2CornerGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _inCornerGr1
            // 
            this._inCornerGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._inCornerGr1.Location = new System.Drawing.Point(119, 81);
            this._inCornerGr1.Name = "_inCornerGr1";
            this._inCornerGr1.Size = new System.Drawing.Size(38, 20);
            this._inCornerGr1.TabIndex = 4;
            this._inCornerGr1.Tag = "360";
            this._inCornerGr1.Text = "0";
            this._inCornerGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _i0CornerGr1
            // 
            this._i0CornerGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._i0CornerGr1.Location = new System.Drawing.Point(278, 37);
            this._i0CornerGr1.Name = "_i0CornerGr1";
            this._i0CornerGr1.Size = new System.Drawing.Size(38, 20);
            this._i0CornerGr1.TabIndex = 3;
            this._i0CornerGr1.Tag = "360";
            this._i0CornerGr1.Text = "0";
            this._i0CornerGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _iCornerGr1
            // 
            this._iCornerGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._iCornerGr1.Location = new System.Drawing.Point(119, 37);
            this._iCornerGr1.Name = "_iCornerGr1";
            this._iCornerGr1.Size = new System.Drawing.Size(38, 20);
            this._iCornerGr1.TabIndex = 2;
            this._iCornerGr1.Tag = "360";
            this._iCornerGr1.Text = "0";
            this._iCornerGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _defenceI
            // 
            this._defenceI.BackColor = System.Drawing.SystemColors.Control;
            this._defenceI.Controls.Add(this.panel7);
            this._defenceI.Location = new System.Drawing.Point(4, 22);
            this._defenceI.Name = "_defenceI";
            this._defenceI.Padding = new System.Windows.Forms.Padding(3);
            this._defenceI.Size = new System.Drawing.Size(944, 411);
            this._defenceI.TabIndex = 0;
            this._defenceI.Text = "Защиты I";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.Control;
            this.panel7.Controls.Add(this._I14GroupBox);
            this.panel7.Controls.Add(this._ILGroupBox);
            this.panel7.Controls.Add(this._I56GroupBox);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(938, 405);
            this.panel7.TabIndex = 8;
            // 
            // _I14GroupBox
            // 
            this._I14GroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._I14GroupBox.Controls.Add(this._difensesI1DataGrid);
            this._I14GroupBox.Location = new System.Drawing.Point(3, 9);
            this._I14GroupBox.Name = "_I14GroupBox";
            this._I14GroupBox.Size = new System.Drawing.Size(935, 170);
            this._I14GroupBox.TabIndex = 8;
            this._I14GroupBox.TabStop = false;
            this._I14GroupBox.Text = "Защиты I>1-4 максимального тока";
            // 
            // _difensesI1DataGrid
            // 
            this._difensesI1DataGrid.AllowUserToAddRows = false;
            this._difensesI1DataGrid.AllowUserToDeleteRows = false;
            this._difensesI1DataGrid.AllowUserToResizeColumns = false;
            this._difensesI1DataGrid.AllowUserToResizeRows = false;
            this._difensesI1DataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesI1DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesI1DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._iStageColumn,
            this._iModesColumn,
            this._iIColumn,
            this.Column8,
            this._iUStartColumn,
            this._iUstartYNColumn,
            this._blockOtTn,
            this._iDirectColumn,
            this._iUnDirectColumn,
            this._iLogicColumn,
            this._iCharColumn,
            this._iTColumn,
            this._iKColumn,
            this._inUsk,
            this._iTyColumn,
            this._iBlockingColumn,
            this._iI2I1Column,
            this._iI2I1YNColumn,
            this._iBlockingDirectColumn,
            this._nenaprUsk,
            this._iOscModeColumn,
            this._iUROVModeColumn,
            this._iAPVModeColumn,
            this._avrColumnI14});
            this._difensesI1DataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesI1DataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesI1DataGrid.MultiSelect = false;
            this._difensesI1DataGrid.Name = "_difensesI1DataGrid";
            this._difensesI1DataGrid.RowHeadersVisible = false;
            this._difensesI1DataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesI1DataGrid.RowTemplate.Height = 24;
            this._difensesI1DataGrid.ShowCellErrors = false;
            this._difensesI1DataGrid.ShowRowErrors = false;
            this._difensesI1DataGrid.Size = new System.Drawing.Size(929, 151);
            this._difensesI1DataGrid.TabIndex = 3;
            this._difensesI1DataGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // _iStageColumn
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._iStageColumn.DefaultCellStyle = dataGridViewCellStyle10;
            this._iStageColumn.Frozen = true;
            this._iStageColumn.HeaderText = "";
            this._iStageColumn.Name = "_iStageColumn";
            this._iStageColumn.ReadOnly = true;
            this._iStageColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iStageColumn.Width = 30;
            // 
            // _iModesColumn
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iModesColumn.DefaultCellStyle = dataGridViewCellStyle11;
            this._iModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._iModesColumn.HeaderText = "Режим";
            this._iModesColumn.Name = "_iModesColumn";
            this._iModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iModesColumn.Width = 90;
            // 
            // _iIColumn
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iIColumn.DefaultCellStyle = dataGridViewCellStyle12;
            this._iIColumn.HeaderText = "Iср, Iн тт";
            this._iIColumn.Name = "_iIColumn";
            this._iIColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iIColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iIColumn.Width = 40;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Column8";
            this.Column8.Name = "Column8";
            this.Column8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column8.Visible = false;
            // 
            // _iUStartColumn
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iUStartColumn.DefaultCellStyle = dataGridViewCellStyle13;
            this._iUStartColumn.HeaderText = "Uпуск, В";
            this._iUStartColumn.MaxInputLength = 6;
            this._iUStartColumn.Name = "_iUStartColumn";
            this._iUStartColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iUStartColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iUStartColumn.Width = 50;
            // 
            // _iUstartYNColumn
            // 
            this._iUstartYNColumn.HeaderText = "Пуск по U";
            this._iUstartYNColumn.Name = "_iUstartYNColumn";
            this._iUstartYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iUstartYNColumn.Width = 40;
            // 
            // _blockOtTn
            // 
            this._blockOtTn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._blockOtTn.HeaderText = "Блк.от неиспр.ТН";
            this._blockOtTn.Name = "_blockOtTn";
            this._blockOtTn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._blockOtTn.Width = 120;
            // 
            // _iDirectColumn
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iDirectColumn.DefaultCellStyle = dataGridViewCellStyle14;
            this._iDirectColumn.HeaderText = "Направл.";
            this._iDirectColumn.Name = "_iDirectColumn";
            this._iDirectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iDirectColumn.Width = 70;
            // 
            // _iUnDirectColumn
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iUnDirectColumn.DefaultCellStyle = dataGridViewCellStyle15;
            this._iUnDirectColumn.HeaderText = "Недост. направл.";
            this._iUnDirectColumn.Name = "_iUnDirectColumn";
            this._iUnDirectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iUnDirectColumn.Width = 70;
            // 
            // _iLogicColumn
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iLogicColumn.DefaultCellStyle = dataGridViewCellStyle16;
            this._iLogicColumn.HeaderText = "Логика";
            this._iLogicColumn.Name = "_iLogicColumn";
            this._iLogicColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iLogicColumn.Width = 80;
            // 
            // _iCharColumn
            // 
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iCharColumn.DefaultCellStyle = dataGridViewCellStyle17;
            this._iCharColumn.HeaderText = "Характ-ка";
            this._iCharColumn.Name = "_iCharColumn";
            this._iCharColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iCharColumn.Width = 80;
            // 
            // _iTColumn
            // 
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iTColumn.DefaultCellStyle = dataGridViewCellStyle18;
            this._iTColumn.HeaderText = "tср.,мс/ коэф";
            this._iTColumn.Name = "_iTColumn";
            this._iTColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iTColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iTColumn.Width = 50;
            // 
            // _iKColumn
            // 
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iKColumn.DefaultCellStyle = dataGridViewCellStyle19;
            this._iKColumn.HeaderText = "kз.х.";
            this._iKColumn.Name = "_iKColumn";
            this._iKColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iKColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iKColumn.Width = 40;
            // 
            // _inUsk
            // 
            this._inUsk.HeaderText = "Вход ускорения";
            this._inUsk.Name = "_inUsk";
            this._inUsk.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._inUsk.Width = 120;
            // 
            // _iTyColumn
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iTyColumn.DefaultCellStyle = dataGridViewCellStyle20;
            this._iTyColumn.HeaderText = "Ty, мс";
            this._iTyColumn.Name = "_iTyColumn";
            this._iTyColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iTyColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iTyColumn.Width = 50;
            // 
            // _iBlockingColumn
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iBlockingColumn.DefaultCellStyle = dataGridViewCellStyle21;
            this._iBlockingColumn.HeaderText = "Блокировка";
            this._iBlockingColumn.Name = "_iBlockingColumn";
            this._iBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iBlockingColumn.Width = 90;
            // 
            // _iI2I1Column
            // 
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iI2I1Column.DefaultCellStyle = dataGridViewCellStyle22;
            this._iI2I1Column.HeaderText = "I2г/I1г, %";
            this._iI2I1Column.Name = "_iI2I1Column";
            this._iI2I1Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iI2I1Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iI2I1Column.Width = 50;
            // 
            // _iI2I1YNColumn
            // 
            this._iI2I1YNColumn.HeaderText = "Блк. I2г/I1г";
            this._iI2I1YNColumn.Name = "_iI2I1YNColumn";
            this._iI2I1YNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iI2I1YNColumn.Width = 50;
            // 
            // _iBlockingDirectColumn
            // 
            this._iBlockingDirectColumn.HeaderText = "Перекр. блок.";
            this._iBlockingDirectColumn.Name = "_iBlockingDirectColumn";
            this._iBlockingDirectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iBlockingDirectColumn.Width = 50;
            // 
            // _nenaprUsk
            // 
            this._nenaprUsk.HeaderText = "Ненапр. при ускор.";
            this._nenaprUsk.Name = "_nenaprUsk";
            this._nenaprUsk.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._nenaprUsk.Width = 70;
            // 
            // _iOscModeColumn
            // 
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iOscModeColumn.DefaultCellStyle = dataGridViewCellStyle23;
            this._iOscModeColumn.HeaderText = "Осциллограф";
            this._iOscModeColumn.Name = "_iOscModeColumn";
            this._iOscModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _iUROVModeColumn
            // 
            this._iUROVModeColumn.HeaderText = "УРОВ";
            this._iUROVModeColumn.Name = "_iUROVModeColumn";
            this._iUROVModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iUROVModeColumn.Width = 45;
            // 
            // _iAPVModeColumn
            // 
            this._iAPVModeColumn.HeaderText = "АПВ";
            this._iAPVModeColumn.Name = "_iAPVModeColumn";
            this._iAPVModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iAPVModeColumn.Width = 65;
            // 
            // _avrColumnI14
            // 
            this._avrColumnI14.HeaderText = "АВР";
            this._avrColumnI14.Name = "_avrColumnI14";
            this._avrColumnI14.Width = 65;
            // 
            // _ILGroupBox
            // 
            this._ILGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ILGroupBox.Controls.Add(this._difensesI3DataGrid);
            this._ILGroupBox.Location = new System.Drawing.Point(3, 311);
            this._ILGroupBox.Name = "_ILGroupBox";
            this._ILGroupBox.Size = new System.Drawing.Size(932, 79);
            this._ILGroupBox.TabIndex = 9;
            this._ILGroupBox.TabStop = false;
            this._ILGroupBox.Text = "Защита I< минимального тока";
            // 
            // _difensesI3DataGrid
            // 
            this._difensesI3DataGrid.AllowUserToAddRows = false;
            this._difensesI3DataGrid.AllowUserToDeleteRows = false;
            this._difensesI3DataGrid.AllowUserToResizeColumns = false;
            this._difensesI3DataGrid.AllowUserToResizeRows = false;
            this._difensesI3DataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesI3DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesI3DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._i8StageColumn,
            this._i8ModesColumn,
            this._i8IColumn,
            this.Column10,
            this._i8UStartColumn,
            this._i8UstartYNColumn,
            this.Column1,
            this._i8DirectColumn,
            this._i8UnDirectColumn,
            this._i8LogicColumn,
            this._i8CharColumn,
            this._i8TColumn,
            this._i8KColumn,
            this.Column2,
            this._i8TyColumn,
            this._i8BlockingColumn,
            this._i8I2I1Column,
            this._i8I2I1YNColumn,
            this._i8BlockingDirectColumn,
            this.Column6,
            this._i8OscModeColumn,
            this._i8UROVModeColumn,
            this._i8APVModeColumn,
            this._avrColumnIL});
            this._difensesI3DataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesI3DataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesI3DataGrid.MultiSelect = false;
            this._difensesI3DataGrid.Name = "_difensesI3DataGrid";
            this._difensesI3DataGrid.RowHeadersVisible = false;
            this._difensesI3DataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesI3DataGrid.RowTemplate.Height = 24;
            this._difensesI3DataGrid.ShowCellErrors = false;
            this._difensesI3DataGrid.ShowRowErrors = false;
            this._difensesI3DataGrid.Size = new System.Drawing.Size(926, 60);
            this._difensesI3DataGrid.TabIndex = 3;
            this._difensesI3DataGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // _i8StageColumn
            // 
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._i8StageColumn.DefaultCellStyle = dataGridViewCellStyle24;
            this._i8StageColumn.Frozen = true;
            this._i8StageColumn.HeaderText = "";
            this._i8StageColumn.Name = "_i8StageColumn";
            this._i8StageColumn.ReadOnly = true;
            this._i8StageColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8StageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i8StageColumn.Width = 30;
            // 
            // _i8ModesColumn
            // 
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8ModesColumn.DefaultCellStyle = dataGridViewCellStyle25;
            this._i8ModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._i8ModesColumn.HeaderText = "Режим";
            this._i8ModesColumn.Name = "_i8ModesColumn";
            this._i8ModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _i8IColumn
            // 
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8IColumn.DefaultCellStyle = dataGridViewCellStyle26;
            this._i8IColumn.HeaderText = "Iср, Iн тт";
            this._i8IColumn.Name = "_i8IColumn";
            this._i8IColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8IColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i8IColumn.Width = 65;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Column10";
            this.Column10.Name = "Column10";
            this.Column10.Visible = false;
            // 
            // _i8UStartColumn
            // 
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8UStartColumn.DefaultCellStyle = dataGridViewCellStyle27;
            this._i8UStartColumn.HeaderText = "Uпуск, В";
            this._i8UStartColumn.MaxInputLength = 6;
            this._i8UStartColumn.Name = "_i8UStartColumn";
            this._i8UStartColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8UStartColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i8UStartColumn.Visible = false;
            this._i8UStartColumn.Width = 80;
            // 
            // _i8UstartYNColumn
            // 
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle28.NullValue = false;
            this._i8UstartYNColumn.DefaultCellStyle = dataGridViewCellStyle28;
            this._i8UstartYNColumn.HeaderText = "Пуск по U";
            this._i8UstartYNColumn.Name = "_i8UstartYNColumn";
            this._i8UstartYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8UstartYNColumn.Visible = false;
            this._i8UstartYNColumn.Width = 80;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Блок.от неиспр.ТН";
            this.Column1.Name = "Column1";
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column1.Visible = false;
            // 
            // _i8DirectColumn
            // 
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8DirectColumn.DefaultCellStyle = dataGridViewCellStyle29;
            this._i8DirectColumn.HeaderText = "Направление";
            this._i8DirectColumn.Name = "_i8DirectColumn";
            this._i8DirectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8DirectColumn.Visible = false;
            this._i8DirectColumn.Width = 85;
            // 
            // _i8UnDirectColumn
            // 
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8UnDirectColumn.DefaultCellStyle = dataGridViewCellStyle30;
            this._i8UnDirectColumn.HeaderText = "Недост.напр";
            this._i8UnDirectColumn.Name = "_i8UnDirectColumn";
            this._i8UnDirectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8UnDirectColumn.Visible = false;
            this._i8UnDirectColumn.Width = 80;
            // 
            // _i8LogicColumn
            // 
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8LogicColumn.DefaultCellStyle = dataGridViewCellStyle31;
            this._i8LogicColumn.HeaderText = "Логика";
            this._i8LogicColumn.Name = "_i8LogicColumn";
            this._i8LogicColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8LogicColumn.Width = 90;
            // 
            // _i8CharColumn
            // 
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8CharColumn.DefaultCellStyle = dataGridViewCellStyle32;
            this._i8CharColumn.HeaderText = "Характ-ка";
            this._i8CharColumn.Name = "_i8CharColumn";
            this._i8CharColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8CharColumn.Visible = false;
            // 
            // _i8TColumn
            // 
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8TColumn.DefaultCellStyle = dataGridViewCellStyle33;
            this._i8TColumn.HeaderText = "t, мс";
            this._i8TColumn.Name = "_i8TColumn";
            this._i8TColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8TColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i8TColumn.Width = 40;
            // 
            // _i8KColumn
            // 
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8KColumn.DefaultCellStyle = dataGridViewCellStyle34;
            this._i8KColumn.HeaderText = "k завис. хар-ки";
            this._i8KColumn.Name = "_i8KColumn";
            this._i8KColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8KColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i8KColumn.Visible = false;
            this._i8KColumn.Width = 110;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Вход ускорения";
            this.Column2.Name = "Column2";
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column2.Visible = false;
            // 
            // _i8TyColumn
            // 
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8TyColumn.DefaultCellStyle = dataGridViewCellStyle35;
            this._i8TyColumn.HeaderText = "Ty, мс";
            this._i8TyColumn.Name = "_i8TyColumn";
            this._i8TyColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8TyColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i8TyColumn.Visible = false;
            this._i8TyColumn.Width = 65;
            // 
            // _i8BlockingColumn
            // 
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8BlockingColumn.DefaultCellStyle = dataGridViewCellStyle36;
            this._i8BlockingColumn.HeaderText = "Блокировка";
            this._i8BlockingColumn.Name = "_i8BlockingColumn";
            this._i8BlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8BlockingColumn.Width = 90;
            // 
            // _i8I2I1Column
            // 
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8I2I1Column.DefaultCellStyle = dataGridViewCellStyle37;
            this._i8I2I1Column.HeaderText = "I2г/I1г ,%";
            this._i8I2I1Column.Name = "_i8I2I1Column";
            this._i8I2I1Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8I2I1Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i8I2I1Column.Visible = false;
            this._i8I2I1Column.Width = 65;
            // 
            // _i8I2I1YNColumn
            // 
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8I2I1YNColumn.DefaultCellStyle = dataGridViewCellStyle38;
            this._i8I2I1YNColumn.HeaderText = "Блок. по I2г/I1г";
            this._i8I2I1YNColumn.Name = "_i8I2I1YNColumn";
            this._i8I2I1YNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8I2I1YNColumn.Visible = false;
            this._i8I2I1YNColumn.Width = 90;
            // 
            // _i8BlockingDirectColumn
            // 
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8BlockingDirectColumn.DefaultCellStyle = dataGridViewCellStyle39;
            this._i8BlockingDirectColumn.HeaderText = "Перекр. Блок.";
            this._i8BlockingDirectColumn.Name = "_i8BlockingDirectColumn";
            this._i8BlockingDirectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8BlockingDirectColumn.Visible = false;
            this._i8BlockingDirectColumn.Width = 90;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Column6";
            this.Column6.Name = "Column6";
            this.Column6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column6.Visible = false;
            // 
            // _i8OscModeColumn
            // 
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8OscModeColumn.DefaultCellStyle = dataGridViewCellStyle40;
            this._i8OscModeColumn.HeaderText = "Осциллограф";
            this._i8OscModeColumn.Name = "_i8OscModeColumn";
            this._i8OscModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8OscModeColumn.Width = 110;
            // 
            // _i8UROVModeColumn
            // 
            this._i8UROVModeColumn.HeaderText = "УРОВ";
            this._i8UROVModeColumn.Name = "_i8UROVModeColumn";
            this._i8UROVModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8UROVModeColumn.Width = 50;
            // 
            // _i8APVModeColumn
            // 
            this._i8APVModeColumn.HeaderText = "АПВ";
            this._i8APVModeColumn.Name = "_i8APVModeColumn";
            this._i8APVModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i8APVModeColumn.Width = 65;
            // 
            // _avrColumnIL
            // 
            this._avrColumnIL.HeaderText = "АВР";
            this._avrColumnIL.Name = "_avrColumnIL";
            this._avrColumnIL.Width = 65;
            // 
            // _I56GroupBox
            // 
            this._I56GroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._I56GroupBox.Controls.Add(this._difensesI2DataGrid);
            this._I56GroupBox.Location = new System.Drawing.Point(3, 185);
            this._I56GroupBox.Name = "_I56GroupBox";
            this._I56GroupBox.Size = new System.Drawing.Size(932, 120);
            this._I56GroupBox.TabIndex = 7;
            this._I56GroupBox.TabStop = false;
            this._I56GroupBox.Text = "Защиты I>5-6 максимального тока";
            // 
            // _difensesI2DataGrid
            // 
            this._difensesI2DataGrid.AllowUserToAddRows = false;
            this._difensesI2DataGrid.AllowUserToDeleteRows = false;
            this._difensesI2DataGrid.AllowUserToResizeColumns = false;
            this._difensesI2DataGrid.AllowUserToResizeRows = false;
            this._difensesI2DataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesI2DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesI2DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewComboBoxColumn88,
            this.dataGridViewTextBoxColumn6,
            this._conditionColumnI56,
            this.dataGridViewTextBoxColumn71,
            this.dataGridViewCheckBoxColumn4,
            this.dataGridViewComboBoxColumn89,
            this.dataGridViewComboBoxColumn90,
            this.dataGridViewComboBoxColumn91,
            this.dataGridViewComboBoxColumn92,
            this.dataGridViewComboBoxColumn93,
            this.dataGridViewTextBoxColumn72,
            this.dataGridViewTextBoxColumn73,
            this.dataGridViewComboBoxColumn94,
            this.dataGridViewTextBoxColumn74,
            this.dataGridViewComboBoxColumn95,
            this.dataGridViewTextBoxColumn75,
            this.dataGridViewCheckBoxColumn5,
            this.dataGridViewCheckBoxColumn11,
            this.dataGridViewCheckBoxColumn15,
            this.dataGridViewComboBoxColumn96,
            this.dataGridViewCheckBoxColumn19,
            this.dataGridViewComboBoxColumn97,
            this._avrColumnI56});
            this._difensesI2DataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesI2DataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesI2DataGrid.MultiSelect = false;
            this._difensesI2DataGrid.Name = "_difensesI2DataGrid";
            this._difensesI2DataGrid.RowHeadersVisible = false;
            this._difensesI2DataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesI2DataGrid.RowTemplate.Height = 24;
            this._difensesI2DataGrid.ShowCellErrors = false;
            this._difensesI2DataGrid.ShowRowErrors = false;
            this._difensesI2DataGrid.Size = new System.Drawing.Size(926, 101);
            this._difensesI2DataGrid.TabIndex = 3;
            this._difensesI2DataGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle41;
            this.dataGridViewTextBoxColumn5.Frozen = true;
            this.dataGridViewTextBoxColumn5.HeaderText = "";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Width = 30;
            // 
            // dataGridViewComboBoxColumn88
            // 
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn88.DefaultCellStyle = dataGridViewCellStyle42;
            this.dataGridViewComboBoxColumn88.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn88.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn88.Name = "dataGridViewComboBoxColumn88";
            this.dataGridViewComboBoxColumn88.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn88.Width = 90;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle43;
            this.dataGridViewTextBoxColumn6.HeaderText = "Iср, Iн тт";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Width = 40;
            // 
            // _conditionColumnI56
            // 
            this._conditionColumnI56.HeaderText = "Условие";
            this._conditionColumnI56.Name = "_conditionColumnI56";
            this._conditionColumnI56.Width = 65;
            // 
            // dataGridViewTextBoxColumn71
            // 
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn71.DefaultCellStyle = dataGridViewCellStyle44;
            this.dataGridViewTextBoxColumn71.HeaderText = "Uпуск, В";
            this.dataGridViewTextBoxColumn71.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn71.Name = "dataGridViewTextBoxColumn71";
            this.dataGridViewTextBoxColumn71.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn71.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn71.Width = 50;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.HeaderText = "Пуск по U";
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn4.Width = 40;
            // 
            // dataGridViewComboBoxColumn89
            // 
            this.dataGridViewComboBoxColumn89.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn89.HeaderText = "Блк.от неиспр.ТН";
            this.dataGridViewComboBoxColumn89.Name = "dataGridViewComboBoxColumn89";
            this.dataGridViewComboBoxColumn89.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn89.Width = 120;
            // 
            // dataGridViewComboBoxColumn90
            // 
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn90.DefaultCellStyle = dataGridViewCellStyle45;
            this.dataGridViewComboBoxColumn90.HeaderText = "Направл.";
            this.dataGridViewComboBoxColumn90.Name = "dataGridViewComboBoxColumn90";
            this.dataGridViewComboBoxColumn90.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn90.Width = 70;
            // 
            // dataGridViewComboBoxColumn91
            // 
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn91.DefaultCellStyle = dataGridViewCellStyle46;
            this.dataGridViewComboBoxColumn91.HeaderText = "Недост. направл.";
            this.dataGridViewComboBoxColumn91.Name = "dataGridViewComboBoxColumn91";
            this.dataGridViewComboBoxColumn91.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn91.Width = 70;
            // 
            // dataGridViewComboBoxColumn92
            // 
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn92.DefaultCellStyle = dataGridViewCellStyle47;
            this.dataGridViewComboBoxColumn92.HeaderText = "Логика";
            this.dataGridViewComboBoxColumn92.Name = "dataGridViewComboBoxColumn92";
            this.dataGridViewComboBoxColumn92.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn92.Width = 80;
            // 
            // dataGridViewComboBoxColumn93
            // 
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn93.DefaultCellStyle = dataGridViewCellStyle48;
            this.dataGridViewComboBoxColumn93.HeaderText = "Характ-ка";
            this.dataGridViewComboBoxColumn93.Name = "dataGridViewComboBoxColumn93";
            this.dataGridViewComboBoxColumn93.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn93.Width = 80;
            // 
            // dataGridViewTextBoxColumn72
            // 
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn72.DefaultCellStyle = dataGridViewCellStyle49;
            this.dataGridViewTextBoxColumn72.HeaderText = "tср.,мс/ коэф";
            this.dataGridViewTextBoxColumn72.Name = "dataGridViewTextBoxColumn72";
            this.dataGridViewTextBoxColumn72.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn72.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn72.Width = 50;
            // 
            // dataGridViewTextBoxColumn73
            // 
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn73.DefaultCellStyle = dataGridViewCellStyle50;
            this.dataGridViewTextBoxColumn73.HeaderText = "kз.х.";
            this.dataGridViewTextBoxColumn73.Name = "dataGridViewTextBoxColumn73";
            this.dataGridViewTextBoxColumn73.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn73.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn73.Width = 40;
            // 
            // dataGridViewComboBoxColumn94
            // 
            this.dataGridViewComboBoxColumn94.HeaderText = "Вход ускорения";
            this.dataGridViewComboBoxColumn94.Name = "dataGridViewComboBoxColumn94";
            this.dataGridViewComboBoxColumn94.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn94.Width = 120;
            // 
            // dataGridViewTextBoxColumn74
            // 
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn74.DefaultCellStyle = dataGridViewCellStyle51;
            this.dataGridViewTextBoxColumn74.HeaderText = "Ty, мс";
            this.dataGridViewTextBoxColumn74.Name = "dataGridViewTextBoxColumn74";
            this.dataGridViewTextBoxColumn74.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn74.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn74.Width = 50;
            // 
            // dataGridViewComboBoxColumn95
            // 
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn95.DefaultCellStyle = dataGridViewCellStyle52;
            this.dataGridViewComboBoxColumn95.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn95.Name = "dataGridViewComboBoxColumn95";
            this.dataGridViewComboBoxColumn95.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn95.Width = 90;
            // 
            // dataGridViewTextBoxColumn75
            // 
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn75.DefaultCellStyle = dataGridViewCellStyle53;
            this.dataGridViewTextBoxColumn75.HeaderText = "I2г/I1г, %";
            this.dataGridViewTextBoxColumn75.Name = "dataGridViewTextBoxColumn75";
            this.dataGridViewTextBoxColumn75.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn75.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn75.Width = 50;
            // 
            // dataGridViewCheckBoxColumn5
            // 
            this.dataGridViewCheckBoxColumn5.HeaderText = "Блк. I2г/I1г";
            this.dataGridViewCheckBoxColumn5.Name = "dataGridViewCheckBoxColumn5";
            this.dataGridViewCheckBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn5.Width = 50;
            // 
            // dataGridViewCheckBoxColumn11
            // 
            this.dataGridViewCheckBoxColumn11.HeaderText = "Перекр. блок.";
            this.dataGridViewCheckBoxColumn11.Name = "dataGridViewCheckBoxColumn11";
            this.dataGridViewCheckBoxColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn11.Width = 50;
            // 
            // dataGridViewCheckBoxColumn15
            // 
            this.dataGridViewCheckBoxColumn15.HeaderText = "Ненапр. при ускор.";
            this.dataGridViewCheckBoxColumn15.Name = "dataGridViewCheckBoxColumn15";
            this.dataGridViewCheckBoxColumn15.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn15.Width = 70;
            // 
            // dataGridViewComboBoxColumn96
            // 
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn96.DefaultCellStyle = dataGridViewCellStyle54;
            this.dataGridViewComboBoxColumn96.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn96.Name = "dataGridViewComboBoxColumn96";
            this.dataGridViewComboBoxColumn96.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewCheckBoxColumn19
            // 
            this.dataGridViewCheckBoxColumn19.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn19.Name = "dataGridViewCheckBoxColumn19";
            this.dataGridViewCheckBoxColumn19.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn19.Width = 45;
            // 
            // dataGridViewComboBoxColumn97
            // 
            this.dataGridViewComboBoxColumn97.HeaderText = "АПВ";
            this.dataGridViewComboBoxColumn97.Name = "dataGridViewComboBoxColumn97";
            this.dataGridViewComboBoxColumn97.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn97.Width = 65;
            // 
            // _avrColumnI56
            // 
            this._avrColumnI56.HeaderText = "АВР";
            this._avrColumnI56.Name = "_avrColumnI56";
            this._avrColumnI56.Width = 65;
            // 
            // _defenceIStar
            // 
            this._defenceIStar.Controls.Add(this.panel8);
            this._defenceIStar.Location = new System.Drawing.Point(4, 22);
            this._defenceIStar.Name = "_defenceIStar";
            this._defenceIStar.Padding = new System.Windows.Forms.Padding(3);
            this._defenceIStar.Size = new System.Drawing.Size(944, 411);
            this._defenceIStar.TabIndex = 1;
            this._defenceIStar.Text = "Защиты I*";
            this._defenceIStar.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.Control;
            this.panel8.Controls.Add(this.groupBox60);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(938, 405);
            this.panel8.TabIndex = 6;
            // 
            // groupBox60
            // 
            this.groupBox60.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox60.Controls.Add(this._difensesI0DataGrid);
            this.groupBox60.Location = new System.Drawing.Point(0, 3);
            this.groupBox60.Name = "groupBox60";
            this.groupBox60.Size = new System.Drawing.Size(935, 265);
            this.groupBox60.TabIndex = 4;
            this.groupBox60.TabStop = false;
            this.groupBox60.Text = "Защиты I*";
            // 
            // _difensesI0DataGrid
            // 
            this._difensesI0DataGrid.AllowUserToAddRows = false;
            this._difensesI0DataGrid.AllowUserToDeleteRows = false;
            this._difensesI0DataGrid.AllowUserToResizeColumns = false;
            this._difensesI0DataGrid.AllowUserToResizeRows = false;
            this._difensesI0DataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesI0DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesI0DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewComboBoxColumn43,
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40,
            this.dataGridViewCheckBoxColumn16,
            this.dataGridViewComboBoxColumn44,
            this.dataGridViewComboBoxColumn45,
            this.dataGridViewComboBoxColumn46,
            this.dataGridViewComboBoxColumn47,
            this.dataGridViewTextBoxColumn41,
            this.dataGridViewTextBoxColumn42,
            this.dataGridViewComboBoxColumn48,
            this.dataGridViewComboBoxColumn49,
            this.dataGridViewComboBoxColumn50,
            this.dataGridViewTextBoxColumn43,
            this.dataGridViewCheckBoxColumn17,
            this.dataGridViewCheckBoxColumn18,
            this.dataGridViewComboBoxColumn51,
            this._avrColumnIStar});
            this._difensesI0DataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesI0DataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesI0DataGrid.MultiSelect = false;
            this._difensesI0DataGrid.Name = "_difensesI0DataGrid";
            this._difensesI0DataGrid.RowHeadersVisible = false;
            this._difensesI0DataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesI0DataGrid.RowTemplate.Height = 24;
            this._difensesI0DataGrid.ShowCellErrors = false;
            this._difensesI0DataGrid.ShowRowErrors = false;
            this._difensesI0DataGrid.Size = new System.Drawing.Size(929, 246);
            this._difensesI0DataGrid.TabIndex = 3;
            this._difensesI0DataGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn38
            // 
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn38.DefaultCellStyle = dataGridViewCellStyle55;
            this.dataGridViewTextBoxColumn38.Frozen = true;
            this.dataGridViewTextBoxColumn38.HeaderText = "";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.ReadOnly = true;
            this.dataGridViewTextBoxColumn38.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn38.Width = 30;
            // 
            // dataGridViewComboBoxColumn43
            // 
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn43.DefaultCellStyle = dataGridViewCellStyle56;
            this.dataGridViewComboBoxColumn43.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn43.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn43.Name = "dataGridViewComboBoxColumn43";
            this.dataGridViewComboBoxColumn43.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn39
            // 
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn39.DefaultCellStyle = dataGridViewCellStyle57;
            this.dataGridViewTextBoxColumn39.HeaderText = "I, Iн тт";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn39.Width = 40;
            // 
            // dataGridViewTextBoxColumn40
            // 
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn40.DefaultCellStyle = dataGridViewCellStyle58;
            this.dataGridViewTextBoxColumn40.HeaderText = "Uпуск, В";
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            this.dataGridViewTextBoxColumn40.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn40.Width = 50;
            // 
            // dataGridViewCheckBoxColumn16
            // 
            this.dataGridViewCheckBoxColumn16.HeaderText = "Пуск по U";
            this.dataGridViewCheckBoxColumn16.Name = "dataGridViewCheckBoxColumn16";
            this.dataGridViewCheckBoxColumn16.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn16.Width = 50;
            // 
            // dataGridViewComboBoxColumn44
            // 
            this.dataGridViewComboBoxColumn44.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn44.DefaultCellStyle = dataGridViewCellStyle59;
            this.dataGridViewComboBoxColumn44.HeaderText = "Направл.";
            this.dataGridViewComboBoxColumn44.Name = "dataGridViewComboBoxColumn44";
            this.dataGridViewComboBoxColumn44.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn44.Width = 60;
            // 
            // dataGridViewComboBoxColumn45
            // 
            this.dataGridViewComboBoxColumn45.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn45.DefaultCellStyle = dataGridViewCellStyle60;
            this.dataGridViewComboBoxColumn45.HeaderText = "Недост. направл.";
            this.dataGridViewComboBoxColumn45.Name = "dataGridViewComboBoxColumn45";
            this.dataGridViewComboBoxColumn45.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn45.Width = 91;
            // 
            // dataGridViewComboBoxColumn46
            // 
            this.dataGridViewComboBoxColumn46.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle61.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn46.DefaultCellStyle = dataGridViewCellStyle61;
            this.dataGridViewComboBoxColumn46.HeaderText = "I*";
            this.dataGridViewComboBoxColumn46.Name = "dataGridViewComboBoxColumn46";
            this.dataGridViewComboBoxColumn46.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn46.Width = 20;
            // 
            // dataGridViewComboBoxColumn47
            // 
            this.dataGridViewComboBoxColumn47.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle62.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn47.DefaultCellStyle = dataGridViewCellStyle62;
            this.dataGridViewComboBoxColumn47.HeaderText = "Характ-ка";
            this.dataGridViewComboBoxColumn47.Name = "dataGridViewComboBoxColumn47";
            this.dataGridViewComboBoxColumn47.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn47.Width = 64;
            // 
            // dataGridViewTextBoxColumn41
            // 
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn41.DefaultCellStyle = dataGridViewCellStyle63;
            this.dataGridViewTextBoxColumn41.HeaderText = "tср.,мс/ коэф";
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            this.dataGridViewTextBoxColumn41.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn41.Width = 50;
            // 
            // dataGridViewTextBoxColumn42
            // 
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn42.DefaultCellStyle = dataGridViewCellStyle64;
            this.dataGridViewTextBoxColumn42.HeaderText = "kз.х.";
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn42.Width = 40;
            // 
            // dataGridViewComboBoxColumn48
            // 
            this.dataGridViewComboBoxColumn48.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn48.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn48.Name = "dataGridViewComboBoxColumn48";
            this.dataGridViewComboBoxColumn48.Width = 74;
            // 
            // dataGridViewComboBoxColumn49
            // 
            this.dataGridViewComboBoxColumn49.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle65.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn49.DefaultCellStyle = dataGridViewCellStyle65;
            this.dataGridViewComboBoxColumn49.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn49.Name = "dataGridViewComboBoxColumn49";
            this.dataGridViewComboBoxColumn49.Width = 82;
            // 
            // dataGridViewComboBoxColumn50
            // 
            this.dataGridViewComboBoxColumn50.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn50.HeaderText = "Вход ускорения";
            this.dataGridViewComboBoxColumn50.Name = "dataGridViewComboBoxColumn50";
            this.dataGridViewComboBoxColumn50.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn50.Width = 84;
            // 
            // dataGridViewTextBoxColumn43
            // 
            dataGridViewCellStyle66.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn43.DefaultCellStyle = dataGridViewCellStyle66;
            this.dataGridViewTextBoxColumn43.HeaderText = "ty, мс";
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.Width = 50;
            // 
            // dataGridViewCheckBoxColumn17
            // 
            this.dataGridViewCheckBoxColumn17.FillWeight = 80F;
            this.dataGridViewCheckBoxColumn17.HeaderText = "Ненаправл. при ускор.";
            this.dataGridViewCheckBoxColumn17.Name = "dataGridViewCheckBoxColumn17";
            this.dataGridViewCheckBoxColumn17.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn17.Width = 70;
            // 
            // dataGridViewCheckBoxColumn18
            // 
            this.dataGridViewCheckBoxColumn18.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn18.Name = "dataGridViewCheckBoxColumn18";
            this.dataGridViewCheckBoxColumn18.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn18.Width = 45;
            // 
            // dataGridViewComboBoxColumn51
            // 
            this.dataGridViewComboBoxColumn51.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn51.HeaderText = "АПВ";
            this.dataGridViewComboBoxColumn51.Name = "dataGridViewComboBoxColumn51";
            this.dataGridViewComboBoxColumn51.Width = 35;
            // 
            // _avrColumnIStar
            // 
            this._avrColumnIStar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._avrColumnIStar.HeaderText = "АВР";
            this._avrColumnIStar.Name = "_avrColumnIStar";
            this._avrColumnIStar.Width = 34;
            // 
            // _defenceI2I1
            // 
            this._defenceI2I1.BackColor = System.Drawing.SystemColors.Control;
            this._defenceI2I1.Controls.Add(this.groupBox8);
            this._defenceI2I1.Location = new System.Drawing.Point(4, 22);
            this._defenceI2I1.Name = "_defenceI2I1";
            this._defenceI2I1.Padding = new System.Windows.Forms.Padding(3);
            this._defenceI2I1.Size = new System.Drawing.Size(944, 411);
            this._defenceI2I1.TabIndex = 2;
            this._defenceI2I1.Text = "I2I1";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._urovI2I1CheckBox);
            this.groupBox8.Controls.Add(this.i2i1AVR);
            this.groupBox8.Controls.Add(this.i2i1APV1);
            this.groupBox8.Controls.Add(this.I2I1OscCombo);
            this.groupBox8.Controls.Add(this.I2I1tcpTB);
            this.groupBox8.Controls.Add(this._avrI2I1Label);
            this.groupBox8.Controls.Add(this.I2I1BlockingCombo);
            this.groupBox8.Controls.Add(this.label27);
            this.groupBox8.Controls.Add(this.label29);
            this.groupBox8.Controls.Add(this.label54);
            this.groupBox8.Controls.Add(this.label55);
            this.groupBox8.Controls.Add(this.label56);
            this.groupBox8.Controls.Add(this.label215);
            this.groupBox8.Controls.Add(this.I2I1TB);
            this.groupBox8.Controls.Add(this.I2I1ModeCombo);
            this.groupBox8.Controls.Add(this.label216);
            this.groupBox8.Location = new System.Drawing.Point(6, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(318, 199);
            this.groupBox8.TabIndex = 13;
            this.groupBox8.TabStop = false;
            // 
            // _urovI2I1CheckBox
            // 
            this._urovI2I1CheckBox.AutoSize = true;
            this._urovI2I1CheckBox.Location = new System.Drawing.Point(125, 118);
            this._urovI2I1CheckBox.Name = "_urovI2I1CheckBox";
            this._urovI2I1CheckBox.Size = new System.Drawing.Size(15, 14);
            this._urovI2I1CheckBox.TabIndex = 37;
            this._urovI2I1CheckBox.UseVisualStyleBackColor = true;
            // 
            // i2i1AVR
            // 
            this.i2i1AVR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.i2i1AVR.FormattingEnabled = true;
            this.i2i1AVR.Location = new System.Drawing.Point(125, 159);
            this.i2i1AVR.Name = "i2i1AVR";
            this.i2i1AVR.Size = new System.Drawing.Size(119, 21);
            this.i2i1AVR.TabIndex = 34;
            // 
            // i2i1APV1
            // 
            this.i2i1APV1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.i2i1APV1.FormattingEnabled = true;
            this.i2i1APV1.Location = new System.Drawing.Point(125, 137);
            this.i2i1APV1.Name = "i2i1APV1";
            this.i2i1APV1.Size = new System.Drawing.Size(119, 21);
            this.i2i1APV1.TabIndex = 34;
            // 
            // I2I1OscCombo
            // 
            this.I2I1OscCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.I2I1OscCombo.FormattingEnabled = true;
            this.I2I1OscCombo.Location = new System.Drawing.Point(125, 94);
            this.I2I1OscCombo.Name = "I2I1OscCombo";
            this.I2I1OscCombo.Size = new System.Drawing.Size(119, 21);
            this.I2I1OscCombo.TabIndex = 34;
            // 
            // I2I1tcpTB
            // 
            this.I2I1tcpTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.I2I1tcpTB.Location = new System.Drawing.Point(125, 74);
            this.I2I1tcpTB.Name = "I2I1tcpTB";
            this.I2I1tcpTB.Size = new System.Drawing.Size(119, 20);
            this.I2I1tcpTB.TabIndex = 33;
            this.I2I1tcpTB.Tag = "1500";
            this.I2I1tcpTB.Text = "20";
            this.I2I1tcpTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _avrI2I1Label
            // 
            this._avrI2I1Label.AutoSize = true;
            this._avrI2I1Label.Location = new System.Drawing.Point(6, 164);
            this._avrI2I1Label.Name = "_avrI2I1Label";
            this._avrI2I1Label.Size = new System.Drawing.Size(28, 13);
            this._avrI2I1Label.TabIndex = 30;
            this._avrI2I1Label.Text = "АВР";
            // 
            // I2I1BlockingCombo
            // 
            this.I2I1BlockingCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.I2I1BlockingCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.I2I1BlockingCombo.FormattingEnabled = true;
            this.I2I1BlockingCombo.Location = new System.Drawing.Point(125, 33);
            this.I2I1BlockingCombo.Name = "I2I1BlockingCombo";
            this.I2I1BlockingCombo.Size = new System.Drawing.Size(119, 21);
            this.I2I1BlockingCombo.TabIndex = 32;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 142);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(29, 13);
            this.label27.TabIndex = 30;
            this.label27.Text = "АПВ";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 122);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(37, 13);
            this.label29.TabIndex = 29;
            this.label29.Text = "УРОВ";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(6, 98);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(33, 13);
            this.label54.TabIndex = 28;
            this.label54.Text = "ОСЦ.";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(6, 76);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(42, 13);
            this.label55.TabIndex = 27;
            this.label55.Text = "tcp, мс";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(6, 56);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(44, 13);
            this.label56.TabIndex = 26;
            this.label56.Text = "I2/I1, %";
            // 
            // label215
            // 
            this.label215.AutoSize = true;
            this.label215.Location = new System.Drawing.Point(6, 36);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(68, 13);
            this.label215.TabIndex = 25;
            this.label215.Text = "Блокировка";
            // 
            // I2I1TB
            // 
            this.I2I1TB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.I2I1TB.Location = new System.Drawing.Point(125, 54);
            this.I2I1TB.Name = "I2I1TB";
            this.I2I1TB.Size = new System.Drawing.Size(119, 20);
            this.I2I1TB.TabIndex = 24;
            this.I2I1TB.Tag = "1500";
            this.I2I1TB.Text = "0";
            this.I2I1TB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // I2I1ModeCombo
            // 
            this.I2I1ModeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.I2I1ModeCombo.FormattingEnabled = true;
            this.I2I1ModeCombo.Location = new System.Drawing.Point(125, 13);
            this.I2I1ModeCombo.Name = "I2I1ModeCombo";
            this.I2I1ModeCombo.Size = new System.Drawing.Size(119, 21);
            this.I2I1ModeCombo.TabIndex = 23;
            // 
            // label216
            // 
            this.label216.AutoSize = true;
            this.label216.Location = new System.Drawing.Point(6, 16);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(42, 13);
            this.label216.TabIndex = 0;
            this.label216.Text = "Режим";
            // 
            // _startArcDefence
            // 
            this._startArcDefence.BackColor = System.Drawing.SystemColors.Control;
            this._startArcDefence.Controls.Add(this.groupBox21);
            this._startArcDefence.Location = new System.Drawing.Point(4, 22);
            this._startArcDefence.Name = "_startArcDefence";
            this._startArcDefence.Padding = new System.Windows.Forms.Padding(3);
            this._startArcDefence.Size = new System.Drawing.Size(944, 411);
            this._startArcDefence.TabIndex = 11;
            this._startArcDefence.Text = "Пуск Дуговой Защ.";
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.OscArc);
            this.groupBox21.Controls.Add(this.StartBlockArcComboBox);
            this.groupBox21.Controls.Add(this.label19);
            this.groupBox21.Controls.Add(this.label20);
            this.groupBox21.Controls.Add(this.label21);
            this.groupBox21.Controls.Add(this.IcpArcTextBox);
            this.groupBox21.Controls.Add(this.StartArcModeComboBox);
            this.groupBox21.Controls.Add(this.label22);
            this.groupBox21.Location = new System.Drawing.Point(6, 6);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(318, 106);
            this.groupBox21.TabIndex = 38;
            this.groupBox21.TabStop = false;
            // 
            // OscArc
            // 
            this.OscArc.AutoSize = true;
            this.OscArc.Location = new System.Drawing.Point(125, 77);
            this.OscArc.Name = "OscArc";
            this.OscArc.Size = new System.Drawing.Size(15, 14);
            this.OscArc.TabIndex = 37;
            this.OscArc.UseVisualStyleBackColor = true;
            // 
            // StartBlockArcComboBox
            // 
            this.StartBlockArcComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.StartBlockArcComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.StartBlockArcComboBox.FormattingEnabled = true;
            this.StartBlockArcComboBox.Location = new System.Drawing.Point(125, 33);
            this.StartBlockArcComboBox.Name = "StartBlockArcComboBox";
            this.StartBlockArcComboBox.Size = new System.Drawing.Size(119, 21);
            this.StartBlockArcComboBox.TabIndex = 32;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 78);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(76, 13);
            this.label19.TabIndex = 28;
            this.label19.Text = "Осциллограф";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 56);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(37, 13);
            this.label20.TabIndex = 26;
            this.label20.Text = "Iср, Iн";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 36);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 13);
            this.label21.TabIndex = 25;
            this.label21.Text = "Блокировка";
            // 
            // IcpArcTextBox
            // 
            this.IcpArcTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IcpArcTextBox.Location = new System.Drawing.Point(125, 54);
            this.IcpArcTextBox.Name = "IcpArcTextBox";
            this.IcpArcTextBox.Size = new System.Drawing.Size(119, 20);
            this.IcpArcTextBox.TabIndex = 24;
            this.IcpArcTextBox.Tag = "1500";
            this.IcpArcTextBox.Text = "0";
            this.IcpArcTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // StartArcModeComboBox
            // 
            this.StartArcModeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StartArcModeComboBox.FormattingEnabled = true;
            this.StartArcModeComboBox.Location = new System.Drawing.Point(125, 13);
            this.StartArcModeComboBox.Name = "StartArcModeComboBox";
            this.StartArcModeComboBox.Size = new System.Drawing.Size(119, 21);
            this.StartArcModeComboBox.TabIndex = 23;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 16);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(42, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Режим";
            // 
            // _defenceU
            // 
            this._defenceU.Controls.Add(this.panel9);
            this._defenceU.Location = new System.Drawing.Point(4, 22);
            this._defenceU.Name = "_defenceU";
            this._defenceU.Padding = new System.Windows.Forms.Padding(3);
            this._defenceU.Size = new System.Drawing.Size(944, 411);
            this._defenceU.TabIndex = 3;
            this._defenceU.Text = "Защиты U";
            this._defenceU.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.Control;
            this.panel9.Controls.Add(this.groupBox62);
            this.panel9.Controls.Add(this.groupBox63);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(3, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(938, 405);
            this.panel9.TabIndex = 7;
            // 
            // groupBox62
            // 
            this.groupBox62.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox62.Controls.Add(this._difensesUBDataGrid);
            this.groupBox62.Location = new System.Drawing.Point(3, 3);
            this.groupBox62.Name = "groupBox62";
            this.groupBox62.Size = new System.Drawing.Size(932, 174);
            this.groupBox62.TabIndex = 4;
            this.groupBox62.TabStop = false;
            this.groupBox62.Text = "Защиты U>";
            // 
            // _difensesUBDataGrid
            // 
            this._difensesUBDataGrid.AllowUserToAddRows = false;
            this._difensesUBDataGrid.AllowUserToDeleteRows = false;
            this._difensesUBDataGrid.AllowUserToResizeColumns = false;
            this._difensesUBDataGrid.AllowUserToResizeRows = false;
            this._difensesUBDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesUBDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesUBDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewComboBoxColumn5,
            this.dataGridViewComboBoxColumn6,
            this.Column3,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewCheckBoxColumn6,
            this.dataGridViewCheckBoxColumn7,
            this.dataGridViewComboBoxColumn7,
            this.dataGridViewComboBoxColumn8,
            this.dataGridViewComboBoxColumn9,
            this.dataGridViewCheckBoxColumn8,
            this.dataGridViewCheckBoxColumn9,
            this.dataGridViewComboBoxColumn10,
            this.dataGridViewCheckBoxColumn10,
            this._avrColumnUM});
            this._difensesUBDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesUBDataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesUBDataGrid.MultiSelect = false;
            this._difensesUBDataGrid.Name = "_difensesUBDataGrid";
            this._difensesUBDataGrid.RowHeadersVisible = false;
            this._difensesUBDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesUBDataGrid.RowTemplate.Height = 24;
            this._difensesUBDataGrid.ShowCellErrors = false;
            this._difensesUBDataGrid.ShowRowErrors = false;
            this._difensesUBDataGrid.Size = new System.Drawing.Size(926, 155);
            this._difensesUBDataGrid.TabIndex = 5;
            this._difensesUBDataGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle67.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle67;
            this.dataGridViewTextBoxColumn7.Frozen = true;
            this.dataGridViewTextBoxColumn7.HeaderText = "";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Width = 40;
            // 
            // dataGridViewComboBoxColumn5
            // 
            dataGridViewCellStyle68.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn5.DefaultCellStyle = dataGridViewCellStyle68;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewComboBoxColumn6
            // 
            this.dataGridViewComboBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle69.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn6.DefaultCellStyle = dataGridViewCellStyle69;
            this.dataGridViewComboBoxColumn6.HeaderText = "Тип";
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn6.Width = 32;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle70;
            this.dataGridViewTextBoxColumn8.HeaderText = "Uср, В";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Width = 50;
            // 
            // dataGridViewTextBoxColumn9
            // 
            dataGridViewCellStyle71.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle71;
            this.dataGridViewTextBoxColumn9.HeaderText = "tср, мс";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Width = 50;
            // 
            // dataGridViewTextBoxColumn10
            // 
            dataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle72;
            this.dataGridViewTextBoxColumn10.HeaderText = "tвз, мс";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Width = 50;
            // 
            // dataGridViewTextBoxColumn11
            // 
            dataGridViewCellStyle73.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle73;
            this.dataGridViewTextBoxColumn11.HeaderText = "Uвз, В";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Width = 50;
            // 
            // dataGridViewCheckBoxColumn6
            // 
            this.dataGridViewCheckBoxColumn6.HeaderText = "Возврат";
            this.dataGridViewCheckBoxColumn6.Name = "dataGridViewCheckBoxColumn6";
            this.dataGridViewCheckBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn6.Width = 55;
            // 
            // dataGridViewCheckBoxColumn7
            // 
            this.dataGridViewCheckBoxColumn7.HeaderText = "Блок. U<5В";
            this.dataGridViewCheckBoxColumn7.Name = "dataGridViewCheckBoxColumn7";
            this.dataGridViewCheckBoxColumn7.Visible = false;
            this.dataGridViewCheckBoxColumn7.Width = 50;
            // 
            // dataGridViewComboBoxColumn7
            // 
            this.dataGridViewComboBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn7.DefaultCellStyle = dataGridViewCellStyle74;
            this.dataGridViewComboBoxColumn7.HeaderText = "Блок. от неиспр. ТН";
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            this.dataGridViewComboBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn7.Visible = false;
            // 
            // dataGridViewComboBoxColumn8
            // 
            this.dataGridViewComboBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle75.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn8.DefaultCellStyle = dataGridViewCellStyle75;
            this.dataGridViewComboBoxColumn8.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn8.Name = "dataGridViewComboBoxColumn8";
            this.dataGridViewComboBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn8.Width = 74;
            // 
            // dataGridViewComboBoxColumn9
            // 
            this.dataGridViewComboBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle76.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn9.DefaultCellStyle = dataGridViewCellStyle76;
            this.dataGridViewComboBoxColumn9.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn9.Name = "dataGridViewComboBoxColumn9";
            this.dataGridViewComboBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn9.Width = 82;
            // 
            // dataGridViewCheckBoxColumn8
            // 
            this.dataGridViewCheckBoxColumn8.HeaderText = "АПВ вз.";
            this.dataGridViewCheckBoxColumn8.Name = "dataGridViewCheckBoxColumn8";
            this.dataGridViewCheckBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn8.Width = 50;
            // 
            // dataGridViewCheckBoxColumn9
            // 
            this.dataGridViewCheckBoxColumn9.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn9.Name = "dataGridViewCheckBoxColumn9";
            this.dataGridViewCheckBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn9.Width = 45;
            // 
            // dataGridViewComboBoxColumn10
            // 
            this.dataGridViewComboBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn10.HeaderText = "АПВ";
            this.dataGridViewComboBoxColumn10.Name = "dataGridViewComboBoxColumn10";
            this.dataGridViewComboBoxColumn10.Width = 35;
            // 
            // dataGridViewCheckBoxColumn10
            // 
            this.dataGridViewCheckBoxColumn10.HeaderText = "Сброс ступени";
            this.dataGridViewCheckBoxColumn10.Name = "dataGridViewCheckBoxColumn10";
            this.dataGridViewCheckBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn10.Width = 60;
            // 
            // _avrColumnUM
            // 
            this._avrColumnUM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._avrColumnUM.HeaderText = "АВР";
            this._avrColumnUM.Name = "_avrColumnUM";
            this._avrColumnUM.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._avrColumnUM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._avrColumnUM.Width = 53;
            // 
            // groupBox63
            // 
            this.groupBox63.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox63.Controls.Add(this._difensesUMDataGrid);
            this.groupBox63.Location = new System.Drawing.Point(3, 183);
            this.groupBox63.Name = "groupBox63";
            this.groupBox63.Size = new System.Drawing.Size(932, 169);
            this.groupBox63.TabIndex = 5;
            this.groupBox63.TabStop = false;
            this.groupBox63.Text = "Защиты U<";
            // 
            // _difensesUMDataGrid
            // 
            this._difensesUMDataGrid.AllowUserToAddRows = false;
            this._difensesUMDataGrid.AllowUserToDeleteRows = false;
            this._difensesUMDataGrid.AllowUserToResizeColumns = false;
            this._difensesUMDataGrid.AllowUserToResizeRows = false;
            this._difensesUMDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesUMDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesUMDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn49,
            this.dataGridViewComboBoxColumn59,
            this.Column4,
            this.dataGridViewComboBoxColumn61,
            this.dataGridViewTextBoxColumn50,
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewTextBoxColumn52,
            this.dataGridViewTextBoxColumn53,
            this.dataGridViewCheckBoxColumn26,
            this.dataGridViewCheckBoxColumn27,
            this.dataGridViewComboBoxColumn62,
            this.dataGridViewComboBoxColumn63,
            this.dataGridViewComboBoxColumn64,
            this.dataGridViewCheckBoxColumn30,
            this.dataGridViewCheckBoxColumn28,
            this.dataGridViewComboBoxColumn65,
            this.dataGridViewCheckBoxColumn31,
            this._avrColumnUL});
            this._difensesUMDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesUMDataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesUMDataGrid.MultiSelect = false;
            this._difensesUMDataGrid.Name = "_difensesUMDataGrid";
            this._difensesUMDataGrid.RowHeadersVisible = false;
            this._difensesUMDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesUMDataGrid.RowTemplate.Height = 24;
            this._difensesUMDataGrid.ShowCellErrors = false;
            this._difensesUMDataGrid.ShowRowErrors = false;
            this._difensesUMDataGrid.Size = new System.Drawing.Size(926, 150);
            this._difensesUMDataGrid.TabIndex = 4;
            this._difensesUMDataGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn49
            // 
            dataGridViewCellStyle77.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn49.DefaultCellStyle = dataGridViewCellStyle77;
            this.dataGridViewTextBoxColumn49.Frozen = true;
            this.dataGridViewTextBoxColumn49.HeaderText = "";
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.ReadOnly = true;
            this.dataGridViewTextBoxColumn49.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn49.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn49.Width = 40;
            // 
            // dataGridViewComboBoxColumn59
            // 
            dataGridViewCellStyle78.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn59.DefaultCellStyle = dataGridViewCellStyle78;
            this.dataGridViewComboBoxColumn59.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn59.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn59.Name = "dataGridViewComboBoxColumn59";
            this.dataGridViewComboBoxColumn59.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Column4";
            this.Column4.Name = "Column4";
            this.Column4.Visible = false;
            // 
            // dataGridViewComboBoxColumn61
            // 
            this.dataGridViewComboBoxColumn61.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle79.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn61.DefaultCellStyle = dataGridViewCellStyle79;
            this.dataGridViewComboBoxColumn61.HeaderText = "Тип";
            this.dataGridViewComboBoxColumn61.Name = "dataGridViewComboBoxColumn61";
            this.dataGridViewComboBoxColumn61.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn61.Width = 32;
            // 
            // dataGridViewTextBoxColumn50
            // 
            dataGridViewCellStyle80.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn50.DefaultCellStyle = dataGridViewCellStyle80;
            this.dataGridViewTextBoxColumn50.HeaderText = "Uср, В";
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            this.dataGridViewTextBoxColumn50.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn50.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn50.Width = 50;
            // 
            // dataGridViewTextBoxColumn51
            // 
            dataGridViewCellStyle81.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn51.DefaultCellStyle = dataGridViewCellStyle81;
            this.dataGridViewTextBoxColumn51.HeaderText = "tср, мс";
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn51.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn51.Width = 50;
            // 
            // dataGridViewTextBoxColumn52
            // 
            dataGridViewCellStyle82.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn52.DefaultCellStyle = dataGridViewCellStyle82;
            this.dataGridViewTextBoxColumn52.HeaderText = "tвз, мс";
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            this.dataGridViewTextBoxColumn52.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn52.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn52.Width = 50;
            // 
            // dataGridViewTextBoxColumn53
            // 
            dataGridViewCellStyle83.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn53.DefaultCellStyle = dataGridViewCellStyle83;
            this.dataGridViewTextBoxColumn53.HeaderText = "Uвз, В";
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            this.dataGridViewTextBoxColumn53.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn53.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn53.Width = 50;
            // 
            // dataGridViewCheckBoxColumn26
            // 
            this.dataGridViewCheckBoxColumn26.HeaderText = "Возврат";
            this.dataGridViewCheckBoxColumn26.Name = "dataGridViewCheckBoxColumn26";
            this.dataGridViewCheckBoxColumn26.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn26.Width = 55;
            // 
            // dataGridViewCheckBoxColumn27
            // 
            this.dataGridViewCheckBoxColumn27.HeaderText = "Блок. U<5В";
            this.dataGridViewCheckBoxColumn27.Name = "dataGridViewCheckBoxColumn27";
            this.dataGridViewCheckBoxColumn27.Width = 50;
            // 
            // dataGridViewComboBoxColumn62
            // 
            this.dataGridViewComboBoxColumn62.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle84.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn62.DefaultCellStyle = dataGridViewCellStyle84;
            this.dataGridViewComboBoxColumn62.HeaderText = "Блок. от неиспр. ТН";
            this.dataGridViewComboBoxColumn62.Name = "dataGridViewComboBoxColumn62";
            this.dataGridViewComboBoxColumn62.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn62.Width = 90;
            // 
            // dataGridViewComboBoxColumn63
            // 
            this.dataGridViewComboBoxColumn63.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle85.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn63.DefaultCellStyle = dataGridViewCellStyle85;
            this.dataGridViewComboBoxColumn63.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn63.Name = "dataGridViewComboBoxColumn63";
            this.dataGridViewComboBoxColumn63.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn63.Width = 74;
            // 
            // dataGridViewComboBoxColumn64
            // 
            this.dataGridViewComboBoxColumn64.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle86.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn64.DefaultCellStyle = dataGridViewCellStyle86;
            this.dataGridViewComboBoxColumn64.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn64.Name = "dataGridViewComboBoxColumn64";
            this.dataGridViewComboBoxColumn64.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn64.Width = 82;
            // 
            // dataGridViewCheckBoxColumn30
            // 
            this.dataGridViewCheckBoxColumn30.HeaderText = "АПВ вз.";
            this.dataGridViewCheckBoxColumn30.Name = "dataGridViewCheckBoxColumn30";
            this.dataGridViewCheckBoxColumn30.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn30.Width = 50;
            // 
            // dataGridViewCheckBoxColumn28
            // 
            this.dataGridViewCheckBoxColumn28.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn28.Name = "dataGridViewCheckBoxColumn28";
            this.dataGridViewCheckBoxColumn28.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn28.Width = 45;
            // 
            // dataGridViewComboBoxColumn65
            // 
            this.dataGridViewComboBoxColumn65.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn65.HeaderText = "АПВ";
            this.dataGridViewComboBoxColumn65.Name = "dataGridViewComboBoxColumn65";
            this.dataGridViewComboBoxColumn65.Width = 35;
            // 
            // dataGridViewCheckBoxColumn31
            // 
            this.dataGridViewCheckBoxColumn31.HeaderText = "Сброс ступени";
            this.dataGridViewCheckBoxColumn31.Name = "dataGridViewCheckBoxColumn31";
            this.dataGridViewCheckBoxColumn31.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn31.Width = 60;
            // 
            // _avrColumnUL
            // 
            this._avrColumnUL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._avrColumnUL.HeaderText = "АВР";
            this._avrColumnUL.Name = "_avrColumnUL";
            this._avrColumnUL.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._avrColumnUL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._avrColumnUL.Width = 53;
            // 
            // _defenceF
            // 
            this._defenceF.Controls.Add(this.panel10);
            this._defenceF.Location = new System.Drawing.Point(4, 22);
            this._defenceF.Name = "_defenceF";
            this._defenceF.Padding = new System.Windows.Forms.Padding(3);
            this._defenceF.Size = new System.Drawing.Size(944, 411);
            this._defenceF.TabIndex = 4;
            this._defenceF.Text = "Защиты F";
            this._defenceF.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.Control;
            this.panel10.Controls.Add(this.groupBox64);
            this.panel10.Controls.Add(this.groupBox65);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(3, 3);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(938, 405);
            this.panel10.TabIndex = 8;
            // 
            // groupBox64
            // 
            this.groupBox64.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox64.Controls.Add(this._difensesFBDataGrid);
            this.groupBox64.Location = new System.Drawing.Point(3, 3);
            this.groupBox64.Name = "groupBox64";
            this.groupBox64.Size = new System.Drawing.Size(932, 190);
            this.groupBox64.TabIndex = 5;
            this.groupBox64.TabStop = false;
            this.groupBox64.Text = "Защиты F>";
            // 
            // _difensesFBDataGrid
            // 
            this._difensesFBDataGrid.AllowUserToAddRows = false;
            this._difensesFBDataGrid.AllowUserToDeleteRows = false;
            this._difensesFBDataGrid.AllowUserToResizeColumns = false;
            this._difensesFBDataGrid.AllowUserToResizeRows = false;
            this._difensesFBDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesFBDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesFBDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn54,
            this.dataGridViewComboBoxColumn66,
            this.dataGridViewComboBoxColumn67,
            this.dataGridViewTextBoxColumn55,
            this._u1Column,
            this.dataGridViewTextBoxColumn57,
            this.dataGridViewTextBoxColumn58,
            this.dataGridViewTextBoxColumn59,
            this.dataGridViewCheckBoxColumn32,
            this.dataGridViewComboBoxColumn68,
            this.dataGridViewComboBoxColumn69,
            this.dataGridViewCheckBoxColumn35,
            this.dataGridViewCheckBoxColumn33,
            this.dataGridViewComboBoxColumn70,
            this.dataGridViewCheckBoxColumn36,
            this._avrColumnFM});
            this._difensesFBDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesFBDataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesFBDataGrid.MultiSelect = false;
            this._difensesFBDataGrid.Name = "_difensesFBDataGrid";
            this._difensesFBDataGrid.RowHeadersVisible = false;
            this._difensesFBDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesFBDataGrid.RowTemplate.Height = 24;
            this._difensesFBDataGrid.ShowCellErrors = false;
            this._difensesFBDataGrid.ShowRowErrors = false;
            this._difensesFBDataGrid.Size = new System.Drawing.Size(926, 171);
            this._difensesFBDataGrid.TabIndex = 3;
            this._difensesFBDataGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn54
            // 
            dataGridViewCellStyle87.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn54.DefaultCellStyle = dataGridViewCellStyle87;
            this.dataGridViewTextBoxColumn54.Frozen = true;
            this.dataGridViewTextBoxColumn54.HeaderText = "";
            this.dataGridViewTextBoxColumn54.Name = "dataGridViewTextBoxColumn54";
            this.dataGridViewTextBoxColumn54.ReadOnly = true;
            this.dataGridViewTextBoxColumn54.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn54.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn54.Width = 40;
            // 
            // dataGridViewComboBoxColumn66
            // 
            dataGridViewCellStyle88.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn66.DefaultCellStyle = dataGridViewCellStyle88;
            this.dataGridViewComboBoxColumn66.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn66.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn66.Name = "dataGridViewComboBoxColumn66";
            this.dataGridViewComboBoxColumn66.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewComboBoxColumn67
            // 
            this.dataGridViewComboBoxColumn67.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn67.HeaderText = "Тип";
            this.dataGridViewComboBoxColumn67.Name = "dataGridViewComboBoxColumn67";
            this.dataGridViewComboBoxColumn67.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn67.Width = 32;
            // 
            // dataGridViewTextBoxColumn55
            // 
            dataGridViewCellStyle89.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn55.DefaultCellStyle = dataGridViewCellStyle89;
            this.dataGridViewTextBoxColumn55.HeaderText = "Fср, Гц | dF/dt, Гц/с";
            this.dataGridViewTextBoxColumn55.Name = "dataGridViewTextBoxColumn55";
            this.dataGridViewTextBoxColumn55.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn55.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn55.Width = 75;
            // 
            // _u1Column
            // 
            dataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._u1Column.DefaultCellStyle = dataGridViewCellStyle90;
            this._u1Column.HeaderText = "U1, В";
            this._u1Column.Name = "_u1Column";
            this._u1Column.Width = 70;
            // 
            // dataGridViewTextBoxColumn57
            // 
            dataGridViewCellStyle91.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn57.DefaultCellStyle = dataGridViewCellStyle91;
            this.dataGridViewTextBoxColumn57.HeaderText = "tср, мс";
            this.dataGridViewTextBoxColumn57.Name = "dataGridViewTextBoxColumn57";
            this.dataGridViewTextBoxColumn57.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn57.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn57.Width = 55;
            // 
            // dataGridViewTextBoxColumn58
            // 
            dataGridViewCellStyle92.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn58.DefaultCellStyle = dataGridViewCellStyle92;
            this.dataGridViewTextBoxColumn58.HeaderText = "tвз, мс";
            this.dataGridViewTextBoxColumn58.Name = "dataGridViewTextBoxColumn58";
            this.dataGridViewTextBoxColumn58.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn58.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn58.Width = 55;
            // 
            // dataGridViewTextBoxColumn59
            // 
            dataGridViewCellStyle93.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn59.DefaultCellStyle = dataGridViewCellStyle93;
            this.dataGridViewTextBoxColumn59.HeaderText = "Fвз, Гц";
            this.dataGridViewTextBoxColumn59.Name = "dataGridViewTextBoxColumn59";
            this.dataGridViewTextBoxColumn59.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn59.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn59.Width = 55;
            // 
            // dataGridViewCheckBoxColumn32
            // 
            this.dataGridViewCheckBoxColumn32.HeaderText = "Возврат";
            this.dataGridViewCheckBoxColumn32.Name = "dataGridViewCheckBoxColumn32";
            this.dataGridViewCheckBoxColumn32.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn32.Width = 55;
            // 
            // dataGridViewComboBoxColumn68
            // 
            this.dataGridViewComboBoxColumn68.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle94.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn68.DefaultCellStyle = dataGridViewCellStyle94;
            this.dataGridViewComboBoxColumn68.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn68.Name = "dataGridViewComboBoxColumn68";
            this.dataGridViewComboBoxColumn68.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn68.Width = 74;
            // 
            // dataGridViewComboBoxColumn69
            // 
            this.dataGridViewComboBoxColumn69.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle95.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn69.DefaultCellStyle = dataGridViewCellStyle95;
            this.dataGridViewComboBoxColumn69.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn69.Name = "dataGridViewComboBoxColumn69";
            this.dataGridViewComboBoxColumn69.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn69.Width = 82;
            // 
            // dataGridViewCheckBoxColumn35
            // 
            this.dataGridViewCheckBoxColumn35.HeaderText = "АПВ возвр.";
            this.dataGridViewCheckBoxColumn35.Name = "dataGridViewCheckBoxColumn35";
            this.dataGridViewCheckBoxColumn35.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn35.Width = 50;
            // 
            // dataGridViewCheckBoxColumn33
            // 
            this.dataGridViewCheckBoxColumn33.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn33.Name = "dataGridViewCheckBoxColumn33";
            this.dataGridViewCheckBoxColumn33.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn33.Width = 45;
            // 
            // dataGridViewComboBoxColumn70
            // 
            this.dataGridViewComboBoxColumn70.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn70.HeaderText = "АПВ";
            this.dataGridViewComboBoxColumn70.Name = "dataGridViewComboBoxColumn70";
            this.dataGridViewComboBoxColumn70.Width = 35;
            // 
            // dataGridViewCheckBoxColumn36
            // 
            this.dataGridViewCheckBoxColumn36.HeaderText = "Сброс ступени";
            this.dataGridViewCheckBoxColumn36.Name = "dataGridViewCheckBoxColumn36";
            this.dataGridViewCheckBoxColumn36.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn36.Width = 60;
            // 
            // _avrColumnFM
            // 
            this._avrColumnFM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._avrColumnFM.HeaderText = "АВР";
            this._avrColumnFM.Name = "_avrColumnFM";
            this._avrColumnFM.Width = 34;
            // 
            // groupBox65
            // 
            this.groupBox65.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox65.Controls.Add(this._difensesFMDataGrid);
            this.groupBox65.Location = new System.Drawing.Point(3, 199);
            this.groupBox65.Name = "groupBox65";
            this.groupBox65.Size = new System.Drawing.Size(932, 190);
            this.groupBox65.TabIndex = 6;
            this.groupBox65.TabStop = false;
            this.groupBox65.Text = "Защиты F<";
            // 
            // _difensesFMDataGrid
            // 
            this._difensesFMDataGrid.AllowUserToAddRows = false;
            this._difensesFMDataGrid.AllowUserToDeleteRows = false;
            this._difensesFMDataGrid.AllowUserToResizeColumns = false;
            this._difensesFMDataGrid.AllowUserToResizeRows = false;
            this._difensesFMDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesFMDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesFMDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn60,
            this.dataGridViewComboBoxColumn71,
            this.dataGridViewComboBoxColumn72,
            this.dataGridViewTextBoxColumn61,
            this._u1Column1,
            this.dataGridViewTextBoxColumn63,
            this.dataGridViewTextBoxColumn64,
            this.dataGridViewTextBoxColumn65,
            this.dataGridViewCheckBoxColumn37,
            this.dataGridViewComboBoxColumn73,
            this.dataGridViewComboBoxColumn74,
            this.dataGridViewCheckBoxColumn40,
            this.dataGridViewCheckBoxColumn38,
            this.dataGridViewComboBoxColumn75,
            this.dataGridViewCheckBoxColumn41,
            this._avrColumnFL});
            this._difensesFMDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesFMDataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesFMDataGrid.MultiSelect = false;
            this._difensesFMDataGrid.Name = "_difensesFMDataGrid";
            this._difensesFMDataGrid.RowHeadersVisible = false;
            this._difensesFMDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesFMDataGrid.RowTemplate.Height = 24;
            this._difensesFMDataGrid.ShowCellErrors = false;
            this._difensesFMDataGrid.ShowRowErrors = false;
            this._difensesFMDataGrid.Size = new System.Drawing.Size(926, 171);
            this._difensesFMDataGrid.TabIndex = 4;
            this._difensesFMDataGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn60
            // 
            dataGridViewCellStyle96.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn60.DefaultCellStyle = dataGridViewCellStyle96;
            this.dataGridViewTextBoxColumn60.Frozen = true;
            this.dataGridViewTextBoxColumn60.HeaderText = "";
            this.dataGridViewTextBoxColumn60.Name = "dataGridViewTextBoxColumn60";
            this.dataGridViewTextBoxColumn60.ReadOnly = true;
            this.dataGridViewTextBoxColumn60.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn60.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn60.Width = 40;
            // 
            // dataGridViewComboBoxColumn71
            // 
            dataGridViewCellStyle97.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn71.DefaultCellStyle = dataGridViewCellStyle97;
            this.dataGridViewComboBoxColumn71.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn71.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn71.Name = "dataGridViewComboBoxColumn71";
            this.dataGridViewComboBoxColumn71.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewComboBoxColumn72
            // 
            this.dataGridViewComboBoxColumn72.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn72.HeaderText = "Тип";
            this.dataGridViewComboBoxColumn72.Name = "dataGridViewComboBoxColumn72";
            this.dataGridViewComboBoxColumn72.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn72.Width = 32;
            // 
            // dataGridViewTextBoxColumn61
            // 
            dataGridViewCellStyle98.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn61.DefaultCellStyle = dataGridViewCellStyle98;
            this.dataGridViewTextBoxColumn61.HeaderText = "Fср, Гц | dF/dt, Гц/с";
            this.dataGridViewTextBoxColumn61.Name = "dataGridViewTextBoxColumn61";
            this.dataGridViewTextBoxColumn61.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn61.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn61.Width = 75;
            // 
            // _u1Column1
            // 
            dataGridViewCellStyle99.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._u1Column1.DefaultCellStyle = dataGridViewCellStyle99;
            this._u1Column1.HeaderText = "U1, В";
            this._u1Column1.Name = "_u1Column1";
            this._u1Column1.Width = 70;
            // 
            // dataGridViewTextBoxColumn63
            // 
            dataGridViewCellStyle100.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn63.DefaultCellStyle = dataGridViewCellStyle100;
            this.dataGridViewTextBoxColumn63.HeaderText = "tср, мс";
            this.dataGridViewTextBoxColumn63.Name = "dataGridViewTextBoxColumn63";
            this.dataGridViewTextBoxColumn63.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn63.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn63.Width = 55;
            // 
            // dataGridViewTextBoxColumn64
            // 
            dataGridViewCellStyle101.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn64.DefaultCellStyle = dataGridViewCellStyle101;
            this.dataGridViewTextBoxColumn64.HeaderText = "tвз, мс";
            this.dataGridViewTextBoxColumn64.Name = "dataGridViewTextBoxColumn64";
            this.dataGridViewTextBoxColumn64.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn64.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn64.Width = 55;
            // 
            // dataGridViewTextBoxColumn65
            // 
            dataGridViewCellStyle102.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn65.DefaultCellStyle = dataGridViewCellStyle102;
            this.dataGridViewTextBoxColumn65.HeaderText = "Fвз, Гц";
            this.dataGridViewTextBoxColumn65.Name = "dataGridViewTextBoxColumn65";
            this.dataGridViewTextBoxColumn65.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn65.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn65.Width = 55;
            // 
            // dataGridViewCheckBoxColumn37
            // 
            this.dataGridViewCheckBoxColumn37.HeaderText = "Возврат";
            this.dataGridViewCheckBoxColumn37.Name = "dataGridViewCheckBoxColumn37";
            this.dataGridViewCheckBoxColumn37.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn37.Width = 55;
            // 
            // dataGridViewComboBoxColumn73
            // 
            this.dataGridViewComboBoxColumn73.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle103.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn73.DefaultCellStyle = dataGridViewCellStyle103;
            this.dataGridViewComboBoxColumn73.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn73.Name = "dataGridViewComboBoxColumn73";
            this.dataGridViewComboBoxColumn73.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn73.Width = 74;
            // 
            // dataGridViewComboBoxColumn74
            // 
            this.dataGridViewComboBoxColumn74.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle104.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn74.DefaultCellStyle = dataGridViewCellStyle104;
            this.dataGridViewComboBoxColumn74.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn74.Name = "dataGridViewComboBoxColumn74";
            this.dataGridViewComboBoxColumn74.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn74.Width = 82;
            // 
            // dataGridViewCheckBoxColumn40
            // 
            this.dataGridViewCheckBoxColumn40.HeaderText = "АПВ возвр.";
            this.dataGridViewCheckBoxColumn40.Name = "dataGridViewCheckBoxColumn40";
            this.dataGridViewCheckBoxColumn40.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn40.Width = 50;
            // 
            // dataGridViewCheckBoxColumn38
            // 
            this.dataGridViewCheckBoxColumn38.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn38.Name = "dataGridViewCheckBoxColumn38";
            this.dataGridViewCheckBoxColumn38.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn38.Width = 45;
            // 
            // dataGridViewComboBoxColumn75
            // 
            this.dataGridViewComboBoxColumn75.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn75.HeaderText = "АПВ";
            this.dataGridViewComboBoxColumn75.Name = "dataGridViewComboBoxColumn75";
            this.dataGridViewComboBoxColumn75.Width = 35;
            // 
            // dataGridViewCheckBoxColumn41
            // 
            this.dataGridViewCheckBoxColumn41.HeaderText = "Сброс ступени";
            this.dataGridViewCheckBoxColumn41.Name = "dataGridViewCheckBoxColumn41";
            this.dataGridViewCheckBoxColumn41.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn41.Width = 60;
            // 
            // _avrColumnFL
            // 
            this._avrColumnFL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._avrColumnFL.HeaderText = "АВР";
            this._avrColumnFL.Name = "_avrColumnFL";
            this._avrColumnFL.Width = 34;
            // 
            // _defenceQ
            // 
            this._defenceQ.BackColor = System.Drawing.SystemColors.Control;
            this._defenceQ.Controls.Add(this._engineDefensesDataGrid);
            this._defenceQ.Controls.Add(this._engineDefensesGrid);
            this._defenceQ.Controls.Add(this.groupBox20);
            this._defenceQ.Controls.Add(this.groupBox66);
            this._defenceQ.Location = new System.Drawing.Point(4, 22);
            this._defenceQ.Name = "_defenceQ";
            this._defenceQ.Padding = new System.Windows.Forms.Padding(3);
            this._defenceQ.Size = new System.Drawing.Size(944, 411);
            this._defenceQ.TabIndex = 5;
            this._defenceQ.Text = "Защиты Q";
            // 
            // _engineDefensesDataGrid
            // 
            this._engineDefensesDataGrid.AllowUserToAddRows = false;
            this._engineDefensesDataGrid.AllowUserToDeleteRows = false;
            this._engineDefensesDataGrid.AllowUserToResizeColumns = false;
            this._engineDefensesDataGrid.AllowUserToResizeRows = false;
            this._engineDefensesDataGrid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._engineDefensesDataGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._engineDefensesDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._engineDefensesDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewComboBoxColumn4,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewComboBoxColumn86,
            this.dataGridViewComboBoxColumn87,
            this.dataGridViewCheckBoxColumn3,
            this._engineDefenseAPVcol,
            this.Column19});
            this._engineDefensesDataGrid.Location = new System.Drawing.Point(-13901, -6099);
            this._engineDefensesDataGrid.MinimumSize = new System.Drawing.Size(622, 74);
            this._engineDefensesDataGrid.Name = "_engineDefensesDataGrid";
            this._engineDefensesDataGrid.RowHeadersVisible = false;
            this._engineDefensesDataGrid.RowTemplate.Height = 24;
            this._engineDefensesDataGrid.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this._engineDefensesDataGrid.Size = new System.Drawing.Size(660, 74);
            this._engineDefensesDataGrid.TabIndex = 11;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 40;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 40;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle105.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle105.NullValue = null;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle105;
            this.dataGridViewTextBoxColumn4.HeaderText = "Уставка Q, %";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 50;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 90;
            // 
            // dataGridViewComboBoxColumn86
            // 
            this.dataGridViewComboBoxColumn86.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn86.MinimumWidth = 100;
            this.dataGridViewComboBoxColumn86.Name = "dataGridViewComboBoxColumn86";
            this.dataGridViewComboBoxColumn86.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn86.Width = 110;
            // 
            // dataGridViewComboBoxColumn87
            // 
            this.dataGridViewComboBoxColumn87.HeaderText = "Осц.";
            this.dataGridViewComboBoxColumn87.Name = "dataGridViewComboBoxColumn87";
            this.dataGridViewComboBoxColumn87.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn87.Width = 110;
            // 
            // dataGridViewCheckBoxColumn3
            // 
            this.dataGridViewCheckBoxColumn3.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn3.MinimumWidth = 50;
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            this.dataGridViewCheckBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn3.Width = 50;
            // 
            // _engineDefenseAPVcol
            // 
            this._engineDefenseAPVcol.HeaderText = "АПВ";
            this._engineDefenseAPVcol.MinimumWidth = 50;
            this._engineDefenseAPVcol.Name = "_engineDefenseAPVcol";
            this._engineDefenseAPVcol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._engineDefenseAPVcol.Width = 65;
            // 
            // Column19
            // 
            this.Column19.HeaderText = "АВР";
            this.Column19.Name = "Column19";
            this.Column19.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column19.Width = 65;
            // 
            // _engineDefensesGrid
            // 
            this._engineDefensesGrid.AllowUserToAddRows = false;
            this._engineDefensesGrid.AllowUserToDeleteRows = false;
            this._engineDefensesGrid.AllowUserToResizeColumns = false;
            this._engineDefensesGrid.AllowUserToResizeRows = false;
            this._engineDefensesGrid.BackgroundColor = System.Drawing.Color.White;
            this._engineDefensesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._engineDefensesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn84,
            this.dataGridViewComboBoxColumn105,
            this.dataGridViewTextBoxColumn85,
            this.dataGridViewComboBoxColumn106,
            this.dataGridViewComboBoxColumn107,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewComboBoxColumn108,
            this._avrColumnEngine});
            this._engineDefensesGrid.Location = new System.Drawing.Point(3, 117);
            this._engineDefensesGrid.Name = "_engineDefensesGrid";
            this._engineDefensesGrid.RowHeadersVisible = false;
            this._engineDefensesGrid.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this._engineDefensesGrid.Size = new System.Drawing.Size(702, 70);
            this._engineDefensesGrid.TabIndex = 9;
            this._engineDefensesGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn84
            // 
            this.dataGridViewTextBoxColumn84.HeaderText = "";
            this.dataGridViewTextBoxColumn84.MinimumWidth = 40;
            this.dataGridViewTextBoxColumn84.Name = "dataGridViewTextBoxColumn84";
            this.dataGridViewTextBoxColumn84.ReadOnly = true;
            this.dataGridViewTextBoxColumn84.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn84.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn84.Width = 40;
            // 
            // dataGridViewComboBoxColumn105
            // 
            this.dataGridViewComboBoxColumn105.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn105.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn105.MinimumWidth = 120;
            this.dataGridViewComboBoxColumn105.Name = "dataGridViewComboBoxColumn105";
            this.dataGridViewComboBoxColumn105.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn105.Width = 120;
            // 
            // dataGridViewTextBoxColumn85
            // 
            dataGridViewCellStyle106.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle106.NullValue = null;
            this.dataGridViewTextBoxColumn85.DefaultCellStyle = dataGridViewCellStyle106;
            this.dataGridViewTextBoxColumn85.HeaderText = "Уставка Q, %";
            this.dataGridViewTextBoxColumn85.MinimumWidth = 50;
            this.dataGridViewTextBoxColumn85.Name = "dataGridViewTextBoxColumn85";
            this.dataGridViewTextBoxColumn85.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn85.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn85.Width = 85;
            // 
            // dataGridViewComboBoxColumn106
            // 
            this.dataGridViewComboBoxColumn106.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn106.MinimumWidth = 100;
            this.dataGridViewComboBoxColumn106.Name = "dataGridViewComboBoxColumn106";
            this.dataGridViewComboBoxColumn106.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewComboBoxColumn107
            // 
            this.dataGridViewComboBoxColumn107.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn107.MinimumWidth = 150;
            this.dataGridViewComboBoxColumn107.Name = "dataGridViewComboBoxColumn107";
            this.dataGridViewComboBoxColumn107.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn107.Width = 150;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn1.MinimumWidth = 50;
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn1.Width = 50;
            // 
            // dataGridViewComboBoxColumn108
            // 
            this.dataGridViewComboBoxColumn108.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn108.HeaderText = "АПВ";
            this.dataGridViewComboBoxColumn108.Name = "dataGridViewComboBoxColumn108";
            // 
            // _avrColumnEngine
            // 
            this._avrColumnEngine.HeaderText = "АВР";
            this._avrColumnEngine.Name = "_avrColumnEngine";
            this._avrColumnEngine.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._avrColumnEngine.Width = 70;
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.label217);
            this.groupBox20.Controls.Add(this.label218);
            this.groupBox20.Controls.Add(this._numHot);
            this.groupBox20.Controls.Add(this._numCold);
            this.groupBox20.Controls.Add(this.label219);
            this.groupBox20.Controls.Add(this._waitNumBlock);
            this.groupBox20.Location = new System.Drawing.Point(233, 3);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(242, 108);
            this.groupBox20.TabIndex = 7;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Блокировка по числу пусков";
            // 
            // label217
            // 
            this.label217.AutoSize = true;
            this.label217.Location = new System.Drawing.Point(6, 50);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(150, 13);
            this.label217.TabIndex = 14;
            this.label217.Text = "Число горячих пусков Nгор.";
            // 
            // label218
            // 
            this.label218.AutoSize = true;
            this.label218.Location = new System.Drawing.Point(6, 24);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(146, 13);
            this.label218.TabIndex = 0;
            this.label218.Text = "Общее число пусков Nпуск";
            // 
            // _numHot
            // 
            this._numHot.Location = new System.Drawing.Point(162, 47);
            this._numHot.Name = "_numHot";
            this._numHot.Size = new System.Drawing.Size(69, 20);
            this._numHot.TabIndex = 1;
            this._numHot.Tag = "256";
            this._numHot.Text = "0";
            this._numHot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _numCold
            // 
            this._numCold.Location = new System.Drawing.Point(162, 21);
            this._numCold.Name = "_numCold";
            this._numCold.Size = new System.Drawing.Size(69, 20);
            this._numCold.TabIndex = 1;
            this._numCold.Tag = "256";
            this._numCold.Text = "0";
            this._numCold.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label219
            // 
            this.label219.AutoSize = true;
            this.label219.Location = new System.Drawing.Point(6, 76);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(76, 13);
            this.label219.TabIndex = 6;
            this.label219.Text = "Время tблк, c";
            // 
            // _waitNumBlock
            // 
            this._waitNumBlock.Location = new System.Drawing.Point(162, 73);
            this._waitNumBlock.Name = "_waitNumBlock";
            this._waitNumBlock.Size = new System.Drawing.Size(69, 20);
            this._waitNumBlock.TabIndex = 7;
            this._waitNumBlock.Tag = "65000";
            this._waitNumBlock.Text = "0";
            this._waitNumBlock.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox66
            // 
            this.groupBox66.Controls.Add(this.label222);
            this.groupBox66.Controls.Add(this.label223);
            this.groupBox66.Controls.Add(this._engineQconstraint);
            this.groupBox66.Controls.Add(this.label224);
            this.groupBox66.Controls.Add(this._engineQtime);
            this.groupBox66.Controls.Add(this._engineQmode);
            this.groupBox66.Location = new System.Drawing.Point(3, 3);
            this.groupBox66.Name = "groupBox66";
            this.groupBox66.Size = new System.Drawing.Size(224, 108);
            this.groupBox66.TabIndex = 6;
            this.groupBox66.TabStop = false;
            this.groupBox66.Text = "Блокировка по тепловому состоянию Q";
            // 
            // label222
            // 
            this.label222.AutoSize = true;
            this.label222.Location = new System.Drawing.Point(9, 24);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(42, 13);
            this.label222.TabIndex = 14;
            this.label222.Text = "Режим";
            // 
            // label223
            // 
            this.label223.AutoSize = true;
            this.label223.Location = new System.Drawing.Point(9, 51);
            this.label223.Name = "label223";
            this.label223.Size = new System.Drawing.Size(93, 13);
            this.label223.TabIndex = 0;
            this.label223.Text = "Уставка Qблк, %";
            // 
            // _engineQconstraint
            // 
            this._engineQconstraint.Location = new System.Drawing.Point(111, 44);
            this._engineQconstraint.Name = "_engineQconstraint";
            this._engineQconstraint.Size = new System.Drawing.Size(69, 20);
            this._engineQconstraint.TabIndex = 1;
            this._engineQconstraint.Tag = "256";
            this._engineQconstraint.Text = "0";
            this._engineQconstraint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label224
            // 
            this.label224.AutoSize = true;
            this.label224.Location = new System.Drawing.Point(9, 70);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(76, 13);
            this.label224.TabIndex = 6;
            this.label224.Text = "Время tблк, c";
            // 
            // _engineQtime
            // 
            this._engineQtime.Location = new System.Drawing.Point(111, 64);
            this._engineQtime.Name = "_engineQtime";
            this._engineQtime.Size = new System.Drawing.Size(69, 20);
            this._engineQtime.TabIndex = 7;
            this._engineQtime.Tag = "65000";
            this._engineQtime.Text = "0";
            this._engineQtime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _engineQmode
            // 
            this._engineQmode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._engineQmode.FormattingEnabled = true;
            this._engineQmode.Location = new System.Drawing.Point(90, 19);
            this._engineQmode.Name = "_engineQmode";
            this._engineQmode.Size = new System.Drawing.Size(90, 21);
            this._engineQmode.TabIndex = 12;
            // 
            // _externalDefence
            // 
            this._externalDefence.Controls.Add(this._externalDefenses);
            this._externalDefence.Location = new System.Drawing.Point(4, 22);
            this._externalDefence.Name = "_externalDefence";
            this._externalDefence.Padding = new System.Windows.Forms.Padding(3);
            this._externalDefence.Size = new System.Drawing.Size(944, 411);
            this._externalDefence.TabIndex = 6;
            this._externalDefence.Text = "Внешние";
            this._externalDefence.UseVisualStyleBackColor = true;
            // 
            // _externalDefenses
            // 
            this._externalDefenses.AllowUserToAddRows = false;
            this._externalDefenses.AllowUserToDeleteRows = false;
            this._externalDefenses.AllowUserToResizeColumns = false;
            this._externalDefenses.AllowUserToResizeRows = false;
            this._externalDefenses.BackgroundColor = System.Drawing.Color.White;
            this._externalDefenses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._externalDefenses.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn68,
            this.dataGridViewComboBoxColumn80,
            this.dataGridViewComboBoxColumn81,
            this.dataGridViewTextBoxColumn69,
            this.dataGridViewTextBoxColumn70,
            this.dataGridViewComboBoxColumn82,
            this.dataGridViewCheckBoxColumn44,
            this.dataGridViewComboBoxColumn83,
            this.dataGridViewComboBoxColumn84,
            this.dataGridViewCheckBoxColumn47,
            this.dataGridViewCheckBoxColumn45,
            this.dataGridViewComboBoxColumn85,
            this.dataGridViewCheckBoxColumn48,
            this._avrColumnExt});
            this._externalDefenses.Dock = System.Windows.Forms.DockStyle.Fill;
            this._externalDefenses.Location = new System.Drawing.Point(3, 3);
            this._externalDefenses.MultiSelect = false;
            this._externalDefenses.Name = "_externalDefenses";
            this._externalDefenses.RowHeadersVisible = false;
            this._externalDefenses.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._externalDefenses.RowTemplate.Height = 24;
            this._externalDefenses.ShowCellErrors = false;
            this._externalDefenses.ShowRowErrors = false;
            this._externalDefenses.Size = new System.Drawing.Size(938, 405);
            this._externalDefenses.TabIndex = 5;
            this._externalDefenses.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn68
            // 
            this.dataGridViewTextBoxColumn68.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle107.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn68.DefaultCellStyle = dataGridViewCellStyle107;
            this.dataGridViewTextBoxColumn68.Frozen = true;
            this.dataGridViewTextBoxColumn68.HeaderText = "";
            this.dataGridViewTextBoxColumn68.Name = "dataGridViewTextBoxColumn68";
            this.dataGridViewTextBoxColumn68.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn68.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn68.Width = 5;
            // 
            // dataGridViewComboBoxColumn80
            // 
            this.dataGridViewComboBoxColumn80.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle108.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn80.DefaultCellStyle = dataGridViewCellStyle108;
            this.dataGridViewComboBoxColumn80.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn80.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn80.Name = "dataGridViewComboBoxColumn80";
            this.dataGridViewComboBoxColumn80.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn80.Width = 48;
            // 
            // dataGridViewComboBoxColumn81
            // 
            this.dataGridViewComboBoxColumn81.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle109.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn81.DefaultCellStyle = dataGridViewCellStyle109;
            this.dataGridViewComboBoxColumn81.HeaderText = "Сигнал срабатывания";
            this.dataGridViewComboBoxColumn81.Name = "dataGridViewComboBoxColumn81";
            this.dataGridViewComboBoxColumn81.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn81.Width = 113;
            // 
            // dataGridViewTextBoxColumn69
            // 
            dataGridViewCellStyle110.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn69.DefaultCellStyle = dataGridViewCellStyle110;
            this.dataGridViewTextBoxColumn69.HeaderText = "tср, мс";
            this.dataGridViewTextBoxColumn69.Name = "dataGridViewTextBoxColumn69";
            this.dataGridViewTextBoxColumn69.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn69.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn69.Width = 90;
            // 
            // dataGridViewTextBoxColumn70
            // 
            dataGridViewCellStyle111.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn70.DefaultCellStyle = dataGridViewCellStyle111;
            this.dataGridViewTextBoxColumn70.HeaderText = "tвз, мс";
            this.dataGridViewTextBoxColumn70.Name = "dataGridViewTextBoxColumn70";
            this.dataGridViewTextBoxColumn70.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn70.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn70.Width = 90;
            // 
            // dataGridViewComboBoxColumn82
            // 
            this.dataGridViewComboBoxColumn82.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle112.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn82.DefaultCellStyle = dataGridViewCellStyle112;
            this.dataGridViewComboBoxColumn82.HeaderText = "Сигнал возврата";
            this.dataGridViewComboBoxColumn82.Name = "dataGridViewComboBoxColumn82";
            this.dataGridViewComboBoxColumn82.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn82.Width = 89;
            // 
            // dataGridViewCheckBoxColumn44
            // 
            this.dataGridViewCheckBoxColumn44.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewCheckBoxColumn44.HeaderText = "Возврат";
            this.dataGridViewCheckBoxColumn44.Name = "dataGridViewCheckBoxColumn44";
            this.dataGridViewCheckBoxColumn44.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn44.Width = 55;
            // 
            // dataGridViewComboBoxColumn83
            // 
            this.dataGridViewComboBoxColumn83.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle113.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn83.DefaultCellStyle = dataGridViewCellStyle113;
            this.dataGridViewComboBoxColumn83.HeaderText = "Сигнал блокировки";
            this.dataGridViewComboBoxColumn83.Name = "dataGridViewComboBoxColumn83";
            this.dataGridViewComboBoxColumn83.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn83.Width = 101;
            // 
            // dataGridViewComboBoxColumn84
            // 
            this.dataGridViewComboBoxColumn84.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle114.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn84.DefaultCellStyle = dataGridViewCellStyle114;
            this.dataGridViewComboBoxColumn84.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn84.Name = "dataGridViewComboBoxColumn84";
            this.dataGridViewComboBoxColumn84.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn84.Width = 82;
            // 
            // dataGridViewCheckBoxColumn47
            // 
            this.dataGridViewCheckBoxColumn47.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewCheckBoxColumn47.HeaderText = "АПВ вз.";
            this.dataGridViewCheckBoxColumn47.Name = "dataGridViewCheckBoxColumn47";
            this.dataGridViewCheckBoxColumn47.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn47.Width = 48;
            // 
            // dataGridViewCheckBoxColumn45
            // 
            this.dataGridViewCheckBoxColumn45.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewCheckBoxColumn45.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn45.Name = "dataGridViewCheckBoxColumn45";
            this.dataGridViewCheckBoxColumn45.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn45.Width = 43;
            // 
            // dataGridViewComboBoxColumn85
            // 
            this.dataGridViewComboBoxColumn85.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn85.HeaderText = "АПВ";
            this.dataGridViewComboBoxColumn85.Name = "dataGridViewComboBoxColumn85";
            this.dataGridViewComboBoxColumn85.Width = 35;
            // 
            // dataGridViewCheckBoxColumn48
            // 
            this.dataGridViewCheckBoxColumn48.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewCheckBoxColumn48.HeaderText = "Сброс ступени";
            this.dataGridViewCheckBoxColumn48.Name = "dataGridViewCheckBoxColumn48";
            this.dataGridViewCheckBoxColumn48.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn48.Width = 78;
            // 
            // _avrColumnExt
            // 
            this._avrColumnExt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle115.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._avrColumnExt.DefaultCellStyle = dataGridViewCellStyle115;
            this._avrColumnExt.HeaderText = "АВР";
            this._avrColumnExt.Name = "_avrColumnExt";
            this._avrColumnExt.Width = 34;
            // 
            // _powerDefence
            // 
            this._powerDefence.BackColor = System.Drawing.SystemColors.Control;
            this._powerDefence.Controls.Add(this.groupBox6);
            this._powerDefence.Location = new System.Drawing.Point(4, 22);
            this._powerDefence.Name = "_powerDefence";
            this._powerDefence.Padding = new System.Windows.Forms.Padding(3);
            this._powerDefence.Size = new System.Drawing.Size(944, 411);
            this._powerDefence.TabIndex = 10;
            this._powerDefence.Text = "Защиты P";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._reversePowerGrid);
            this.groupBox6.Location = new System.Drawing.Point(3, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(936, 126);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Защиты P";
            // 
            // _reversePowerGrid
            // 
            this._reversePowerGrid.AllowUserToAddRows = false;
            this._reversePowerGrid.AllowUserToDeleteRows = false;
            this._reversePowerGrid.AllowUserToResizeColumns = false;
            this._reversePowerGrid.AllowUserToResizeRows = false;
            this._reversePowerGrid.BackgroundColor = System.Drawing.Color.White;
            this._reversePowerGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._reversePowerGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewComboBoxColumn1,
            this._ssr,
            this._usr,
            this._tsr,
            this._tvz,
            this._svz,
            this._svzbeno,
            this._isr,
            this._block,
            this._blk,
            this._osc,
            this._apvvoz,
            this._urov,
            this._apv,
            this._resetstep});
            this._reversePowerGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._reversePowerGrid.Location = new System.Drawing.Point(3, 16);
            this._reversePowerGrid.MultiSelect = false;
            this._reversePowerGrid.Name = "_reversePowerGrid";
            this._reversePowerGrid.RowHeadersVisible = false;
            this._reversePowerGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._reversePowerGrid.RowTemplate.Height = 24;
            this._reversePowerGrid.ShowCellErrors = false;
            this._reversePowerGrid.ShowRowErrors = false;
            this._reversePowerGrid.Size = new System.Drawing.Size(930, 107);
            this._reversePowerGrid.TabIndex = 4;
            this._reversePowerGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle116.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle116;
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 75;
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle117.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle117;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _ssr
            // 
            this._ssr.HeaderText = "Sср, Sн";
            this._ssr.Name = "_ssr";
            this._ssr.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._ssr.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ssr.Width = 60;
            // 
            // _usr
            // 
            this._usr.HeaderText = "Уср, \'";
            this._usr.Name = "_usr";
            this._usr.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._usr.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._usr.Width = 60;
            // 
            // _tsr
            // 
            this._tsr.HeaderText = "tср, мс";
            this._tsr.Name = "_tsr";
            this._tsr.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._tsr.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tsr.Width = 60;
            // 
            // _tvz
            // 
            this._tvz.HeaderText = "tвз, мс";
            this._tvz.Name = "_tvz";
            this._tvz.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._tvz.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tvz.Width = 60;
            // 
            // _svz
            // 
            this._svz.HeaderText = "Sвз, Sн";
            this._svz.Name = "_svz";
            this._svz.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._svz.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._svz.Width = 60;
            // 
            // _svzbeno
            // 
            this._svzbeno.HeaderText = "Sвз, (Есть, Нет)";
            this._svzbeno.Name = "_svzbeno";
            this._svzbeno.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _isr
            // 
            this._isr.HeaderText = "Iср, н";
            this._isr.Name = "_isr";
            this._isr.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._isr.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._isr.Width = 60;
            // 
            // _block
            // 
            this._block.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._block.HeaderText = "Блокировка";
            this._block.Name = "_block";
            this._block.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._block.Width = 74;
            // 
            // _blk
            // 
            this._blk.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._blk.HeaderText = "Блк. от неиспр. ТН";
            this._blk.Name = "_blk";
            this._blk.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._blk.Width = 85;
            // 
            // _osc
            // 
            this._osc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._osc.HeaderText = "Осц.";
            this._osc.Name = "_osc";
            this._osc.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._osc.Width = 36;
            // 
            // _apvvoz
            // 
            this._apvvoz.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._apvvoz.HeaderText = "АПВвозвр";
            this._apvvoz.Name = "_apvvoz";
            this._apvvoz.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._apvvoz.Width = 65;
            // 
            // _urov
            // 
            this._urov.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._urov.HeaderText = "УРОВ";
            this._urov.Name = "_urov";
            this._urov.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._urov.Width = 43;
            // 
            // _apv
            // 
            this._apv.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._apv.HeaderText = "АПВ";
            this._apv.Name = "_apv";
            this._apv.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._apv.Width = 35;
            // 
            // _resetstep
            // 
            this._resetstep.HeaderText = "Сброс ступени";
            this._resetstep.Name = "_resetstep";
            this._resetstep.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._resetstep.Width = 60;
            // 
            // _lsGr
            // 
            this._lsGr.Controls.Add(this.groupBox56);
            this._lsGr.Controls.Add(this.groupBox55);
            this._lsGr.Controls.Add(this.groupBox26);
            this._lsGr.Controls.Add(this.groupBox45);
            this._lsGr.Location = new System.Drawing.Point(4, 25);
            this._lsGr.Name = "_lsGr";
            this._lsGr.Padding = new System.Windows.Forms.Padding(3);
            this._lsGr.Size = new System.Drawing.Size(958, 443);
            this._lsGr.TabIndex = 19;
            this._lsGr.Text = "ЛС";
            this._lsGr.UseVisualStyleBackColor = true;
            // 
            // groupBox56
            // 
            this.groupBox56.Controls.Add(this.treeViewForLsOR);
            this.groupBox56.Location = new System.Drawing.Point(705, 3);
            this.groupBox56.Name = "groupBox56";
            this.groupBox56.Size = new System.Drawing.Size(200, 437);
            this.groupBox56.TabIndex = 14;
            this.groupBox56.TabStop = false;
            this.groupBox56.Text = "Логические сигналы ИЛИ";
            // 
            // treeViewForLsOR
            // 
            this.treeViewForLsOR.Location = new System.Drawing.Point(19, 19);
            this.treeViewForLsOR.Name = "treeViewForLsOR";
            this.treeViewForLsOR.Size = new System.Drawing.Size(164, 409);
            this.treeViewForLsOR.TabIndex = 12;
            this.treeViewForLsOR.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeViewForLsOR_NodeMouseClick);
            // 
            // groupBox55
            // 
            this.groupBox55.Controls.Add(this.treeViewForLsAND);
            this.groupBox55.Location = new System.Drawing.Point(485, 3);
            this.groupBox55.Name = "groupBox55";
            this.groupBox55.Size = new System.Drawing.Size(200, 437);
            this.groupBox55.TabIndex = 13;
            this.groupBox55.TabStop = false;
            this.groupBox55.Text = "Логические сигналы И";
            // 
            // treeViewForLsAND
            // 
            this.treeViewForLsAND.Location = new System.Drawing.Point(18, 19);
            this.treeViewForLsAND.Name = "treeViewForLsAND";
            this.treeViewForLsAND.Size = new System.Drawing.Size(166, 409);
            this.treeViewForLsAND.TabIndex = 11;
            this.treeViewForLsAND.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeViewForLsAND_NodeMouseClick);
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this._lsSecondTabControl);
            this.groupBox26.Location = new System.Drawing.Point(205, 3);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(193, 437);
            this.groupBox26.TabIndex = 10;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Логические сигналы ИЛИ";
            // 
            // _lsSecondTabControl
            // 
            this._lsSecondTabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this._lsSecondTabControl.Controls.Add(this.tabPage31);
            this._lsSecondTabControl.Controls.Add(this.tabPage32);
            this._lsSecondTabControl.Controls.Add(this.tabPage33);
            this._lsSecondTabControl.Controls.Add(this.tabPage34);
            this._lsSecondTabControl.Controls.Add(this.tabPage35);
            this._lsSecondTabControl.Controls.Add(this.tabPage36);
            this._lsSecondTabControl.Controls.Add(this.tabPage37);
            this._lsSecondTabControl.Controls.Add(this.tabPage38);
            this._lsSecondTabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._lsSecondTabControl.Location = new System.Drawing.Point(3, 16);
            this._lsSecondTabControl.Multiline = true;
            this._lsSecondTabControl.Name = "_lsSecondTabControl";
            this._lsSecondTabControl.SelectedIndex = 0;
            this._lsSecondTabControl.Size = new System.Drawing.Size(187, 418);
            this._lsSecondTabControl.TabIndex = 2;
            this._lsSecondTabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage31
            // 
            this.tabPage31.Controls.Add(this._lsOrDgv1Gr1);
            this.tabPage31.Location = new System.Drawing.Point(4, 49);
            this.tabPage31.Name = "tabPage31";
            this.tabPage31.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage31.Size = new System.Drawing.Size(179, 365);
            this.tabPage31.TabIndex = 0;
            this.tabPage31.Text = "ЛС9";
            this.tabPage31.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv1Gr1
            // 
            this._lsOrDgv1Gr1.AllowUserToAddRows = false;
            this._lsOrDgv1Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv1Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv1Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv1Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv1Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv1Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv1Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewComboBoxColumn15});
            this._lsOrDgv1Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv1Gr1.MultiSelect = false;
            this._lsOrDgv1Gr1.Name = "_lsOrDgv1Gr1";
            this._lsOrDgv1Gr1.RowHeadersVisible = false;
            this._lsOrDgv1Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle118.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv1Gr1.RowsDefaultCellStyle = dataGridViewCellStyle118;
            this._lsOrDgv1Gr1.RowTemplate.Height = 20;
            this._lsOrDgv1Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv1Gr1.ShowCellErrors = false;
            this._lsOrDgv1Gr1.ShowCellToolTips = false;
            this._lsOrDgv1Gr1.ShowEditingIcon = false;
            this._lsOrDgv1Gr1.ShowRowErrors = false;
            this._lsOrDgv1Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsOrDgv1Gr1.TabIndex = 2;
            this._lsOrDgv1Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsOrDgvGr1_CellEndEdit);
            this._lsOrDgv1Gr1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "№";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Width = 24;
            // 
            // dataGridViewComboBoxColumn15
            // 
            this.dataGridViewComboBoxColumn15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn15.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn15.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn15.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn15.Name = "dataGridViewComboBoxColumn15";
            // 
            // tabPage32
            // 
            this.tabPage32.Controls.Add(this._lsOrDgv2Gr1);
            this.tabPage32.Location = new System.Drawing.Point(4, 49);
            this.tabPage32.Name = "tabPage32";
            this.tabPage32.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage32.Size = new System.Drawing.Size(179, 365);
            this.tabPage32.TabIndex = 1;
            this.tabPage32.Text = "ЛС10";
            this.tabPage32.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv2Gr1
            // 
            this._lsOrDgv2Gr1.AllowUserToAddRows = false;
            this._lsOrDgv2Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv2Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv2Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv2Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv2Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv2Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv2Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewComboBoxColumn16});
            this._lsOrDgv2Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv2Gr1.MultiSelect = false;
            this._lsOrDgv2Gr1.Name = "_lsOrDgv2Gr1";
            this._lsOrDgv2Gr1.RowHeadersVisible = false;
            this._lsOrDgv2Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle119.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv2Gr1.RowsDefaultCellStyle = dataGridViewCellStyle119;
            this._lsOrDgv2Gr1.RowTemplate.Height = 20;
            this._lsOrDgv2Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv2Gr1.ShowCellErrors = false;
            this._lsOrDgv2Gr1.ShowCellToolTips = false;
            this._lsOrDgv2Gr1.ShowEditingIcon = false;
            this._lsOrDgv2Gr1.ShowRowErrors = false;
            this._lsOrDgv2Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsOrDgv2Gr1.TabIndex = 4;
            this._lsOrDgv2Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsOrDgvGr1_CellEndEdit);
            this._lsOrDgv2Gr1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "№";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.Width = 24;
            // 
            // dataGridViewComboBoxColumn16
            // 
            this.dataGridViewComboBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn16.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn16.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn16.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn16.Name = "dataGridViewComboBoxColumn16";
            // 
            // tabPage33
            // 
            this.tabPage33.Controls.Add(this._lsOrDgv3Gr1);
            this.tabPage33.Location = new System.Drawing.Point(4, 49);
            this.tabPage33.Name = "tabPage33";
            this.tabPage33.Size = new System.Drawing.Size(179, 365);
            this.tabPage33.TabIndex = 2;
            this.tabPage33.Text = "ЛС11";
            this.tabPage33.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv3Gr1
            // 
            this._lsOrDgv3Gr1.AllowUserToAddRows = false;
            this._lsOrDgv3Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv3Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv3Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv3Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv3Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv3Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv3Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewComboBoxColumn17});
            this._lsOrDgv3Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv3Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv3Gr1.MultiSelect = false;
            this._lsOrDgv3Gr1.Name = "_lsOrDgv3Gr1";
            this._lsOrDgv3Gr1.RowHeadersVisible = false;
            this._lsOrDgv3Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle120.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv3Gr1.RowsDefaultCellStyle = dataGridViewCellStyle120;
            this._lsOrDgv3Gr1.RowTemplate.Height = 20;
            this._lsOrDgv3Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv3Gr1.ShowCellErrors = false;
            this._lsOrDgv3Gr1.ShowCellToolTips = false;
            this._lsOrDgv3Gr1.ShowEditingIcon = false;
            this._lsOrDgv3Gr1.ShowRowErrors = false;
            this._lsOrDgv3Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsOrDgv3Gr1.TabIndex = 4;
            this._lsOrDgv3Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsOrDgvGr1_CellEndEdit);
            this._lsOrDgv3Gr1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "№";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn18.Width = 24;
            // 
            // dataGridViewComboBoxColumn17
            // 
            this.dataGridViewComboBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn17.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn17.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn17.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn17.Name = "dataGridViewComboBoxColumn17";
            // 
            // tabPage34
            // 
            this.tabPage34.Controls.Add(this._lsOrDgv4Gr1);
            this.tabPage34.Location = new System.Drawing.Point(4, 49);
            this.tabPage34.Name = "tabPage34";
            this.tabPage34.Size = new System.Drawing.Size(179, 365);
            this.tabPage34.TabIndex = 3;
            this.tabPage34.Text = "ЛС12";
            this.tabPage34.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv4Gr1
            // 
            this._lsOrDgv4Gr1.AllowUserToAddRows = false;
            this._lsOrDgv4Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv4Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv4Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv4Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv4Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv4Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv4Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewComboBoxColumn18});
            this._lsOrDgv4Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv4Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv4Gr1.MultiSelect = false;
            this._lsOrDgv4Gr1.Name = "_lsOrDgv4Gr1";
            this._lsOrDgv4Gr1.RowHeadersVisible = false;
            this._lsOrDgv4Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle121.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv4Gr1.RowsDefaultCellStyle = dataGridViewCellStyle121;
            this._lsOrDgv4Gr1.RowTemplate.Height = 20;
            this._lsOrDgv4Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv4Gr1.ShowCellErrors = false;
            this._lsOrDgv4Gr1.ShowCellToolTips = false;
            this._lsOrDgv4Gr1.ShowEditingIcon = false;
            this._lsOrDgv4Gr1.ShowRowErrors = false;
            this._lsOrDgv4Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsOrDgv4Gr1.TabIndex = 4;
            this._lsOrDgv4Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsOrDgvGr1_CellEndEdit);
            this._lsOrDgv4Gr1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.HeaderText = "№";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn19.Width = 24;
            // 
            // dataGridViewComboBoxColumn18
            // 
            this.dataGridViewComboBoxColumn18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn18.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn18.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn18.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn18.Name = "dataGridViewComboBoxColumn18";
            // 
            // tabPage35
            // 
            this.tabPage35.Controls.Add(this._lsOrDgv5Gr1);
            this.tabPage35.Location = new System.Drawing.Point(4, 49);
            this.tabPage35.Name = "tabPage35";
            this.tabPage35.Size = new System.Drawing.Size(179, 365);
            this.tabPage35.TabIndex = 4;
            this.tabPage35.Text = "ЛС13";
            this.tabPage35.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv5Gr1
            // 
            this._lsOrDgv5Gr1.AllowUserToAddRows = false;
            this._lsOrDgv5Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv5Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv5Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv5Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv5Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv5Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv5Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewComboBoxColumn19});
            this._lsOrDgv5Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv5Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv5Gr1.MultiSelect = false;
            this._lsOrDgv5Gr1.Name = "_lsOrDgv5Gr1";
            this._lsOrDgv5Gr1.RowHeadersVisible = false;
            this._lsOrDgv5Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle122.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv5Gr1.RowsDefaultCellStyle = dataGridViewCellStyle122;
            this._lsOrDgv5Gr1.RowTemplate.Height = 20;
            this._lsOrDgv5Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv5Gr1.ShowCellErrors = false;
            this._lsOrDgv5Gr1.ShowCellToolTips = false;
            this._lsOrDgv5Gr1.ShowEditingIcon = false;
            this._lsOrDgv5Gr1.ShowRowErrors = false;
            this._lsOrDgv5Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsOrDgv5Gr1.TabIndex = 4;
            this._lsOrDgv5Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsOrDgvGr1_CellEndEdit);
            this._lsOrDgv5Gr1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.HeaderText = "№";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn20.Width = 24;
            // 
            // dataGridViewComboBoxColumn19
            // 
            this.dataGridViewComboBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn19.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn19.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn19.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn19.Name = "dataGridViewComboBoxColumn19";
            // 
            // tabPage36
            // 
            this.tabPage36.Controls.Add(this._lsOrDgv6Gr1);
            this.tabPage36.Location = new System.Drawing.Point(4, 49);
            this.tabPage36.Name = "tabPage36";
            this.tabPage36.Size = new System.Drawing.Size(179, 365);
            this.tabPage36.TabIndex = 5;
            this.tabPage36.Text = "ЛС14";
            this.tabPage36.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv6Gr1
            // 
            this._lsOrDgv6Gr1.AllowUserToAddRows = false;
            this._lsOrDgv6Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv6Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv6Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv6Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv6Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv6Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv6Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewComboBoxColumn20});
            this._lsOrDgv6Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv6Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv6Gr1.MultiSelect = false;
            this._lsOrDgv6Gr1.Name = "_lsOrDgv6Gr1";
            this._lsOrDgv6Gr1.RowHeadersVisible = false;
            this._lsOrDgv6Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle123.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv6Gr1.RowsDefaultCellStyle = dataGridViewCellStyle123;
            this._lsOrDgv6Gr1.RowTemplate.Height = 20;
            this._lsOrDgv6Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv6Gr1.ShowCellErrors = false;
            this._lsOrDgv6Gr1.ShowCellToolTips = false;
            this._lsOrDgv6Gr1.ShowEditingIcon = false;
            this._lsOrDgv6Gr1.ShowRowErrors = false;
            this._lsOrDgv6Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsOrDgv6Gr1.TabIndex = 4;
            this._lsOrDgv6Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsOrDgvGr1_CellEndEdit);
            this._lsOrDgv6Gr1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.HeaderText = "№";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn21.Width = 24;
            // 
            // dataGridViewComboBoxColumn20
            // 
            this.dataGridViewComboBoxColumn20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn20.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn20.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn20.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn20.Name = "dataGridViewComboBoxColumn20";
            // 
            // tabPage37
            // 
            this.tabPage37.Controls.Add(this._lsOrDgv7Gr1);
            this.tabPage37.Location = new System.Drawing.Point(4, 49);
            this.tabPage37.Name = "tabPage37";
            this.tabPage37.Size = new System.Drawing.Size(179, 365);
            this.tabPage37.TabIndex = 6;
            this.tabPage37.Text = "ЛС15";
            this.tabPage37.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv7Gr1
            // 
            this._lsOrDgv7Gr1.AllowUserToAddRows = false;
            this._lsOrDgv7Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv7Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv7Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv7Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv7Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv7Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv7Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewComboBoxColumn21});
            this._lsOrDgv7Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv7Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv7Gr1.MultiSelect = false;
            this._lsOrDgv7Gr1.Name = "_lsOrDgv7Gr1";
            this._lsOrDgv7Gr1.RowHeadersVisible = false;
            this._lsOrDgv7Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle124.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv7Gr1.RowsDefaultCellStyle = dataGridViewCellStyle124;
            this._lsOrDgv7Gr1.RowTemplate.Height = 20;
            this._lsOrDgv7Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv7Gr1.ShowCellErrors = false;
            this._lsOrDgv7Gr1.ShowCellToolTips = false;
            this._lsOrDgv7Gr1.ShowEditingIcon = false;
            this._lsOrDgv7Gr1.ShowRowErrors = false;
            this._lsOrDgv7Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsOrDgv7Gr1.TabIndex = 4;
            this._lsOrDgv7Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsOrDgvGr1_CellEndEdit);
            this._lsOrDgv7Gr1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.HeaderText = "№";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn22.Width = 24;
            // 
            // dataGridViewComboBoxColumn21
            // 
            this.dataGridViewComboBoxColumn21.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn21.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn21.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn21.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn21.Name = "dataGridViewComboBoxColumn21";
            // 
            // tabPage38
            // 
            this.tabPage38.Controls.Add(this._lsOrDgv8Gr1);
            this.tabPage38.Location = new System.Drawing.Point(4, 49);
            this.tabPage38.Name = "tabPage38";
            this.tabPage38.Size = new System.Drawing.Size(179, 365);
            this.tabPage38.TabIndex = 7;
            this.tabPage38.Text = "ЛС16";
            this.tabPage38.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv8Gr1
            // 
            this._lsOrDgv8Gr1.AllowUserToAddRows = false;
            this._lsOrDgv8Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv8Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv8Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv8Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv8Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv8Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv8Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewComboBoxColumn22});
            this._lsOrDgv8Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv8Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv8Gr1.MultiSelect = false;
            this._lsOrDgv8Gr1.Name = "_lsOrDgv8Gr1";
            this._lsOrDgv8Gr1.RowHeadersVisible = false;
            this._lsOrDgv8Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle125.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv8Gr1.RowsDefaultCellStyle = dataGridViewCellStyle125;
            this._lsOrDgv8Gr1.RowTemplate.Height = 20;
            this._lsOrDgv8Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv8Gr1.ShowCellErrors = false;
            this._lsOrDgv8Gr1.ShowCellToolTips = false;
            this._lsOrDgv8Gr1.ShowEditingIcon = false;
            this._lsOrDgv8Gr1.ShowRowErrors = false;
            this._lsOrDgv8Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsOrDgv8Gr1.TabIndex = 4;
            this._lsOrDgv8Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsOrDgvGr1_CellEndEdit);
            this._lsOrDgv8Gr1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.HeaderText = "№";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn23.Width = 24;
            // 
            // dataGridViewComboBoxColumn22
            // 
            this.dataGridViewComboBoxColumn22.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn22.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn22.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn22.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn22.Name = "dataGridViewComboBoxColumn22";
            // 
            // groupBox45
            // 
            this.groupBox45.Controls.Add(this._lsFirsTabControl);
            this.groupBox45.Location = new System.Drawing.Point(6, 3);
            this.groupBox45.Name = "groupBox45";
            this.groupBox45.Size = new System.Drawing.Size(193, 437);
            this.groupBox45.TabIndex = 9;
            this.groupBox45.TabStop = false;
            this.groupBox45.Text = "Логические сигналы И";
            // 
            // _lsFirsTabControl
            // 
            this._lsFirsTabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this._lsFirsTabControl.Controls.Add(this.tabPage39);
            this._lsFirsTabControl.Controls.Add(this.tabPage40);
            this._lsFirsTabControl.Controls.Add(this.tabPage41);
            this._lsFirsTabControl.Controls.Add(this.tabPage42);
            this._lsFirsTabControl.Controls.Add(this.tabPage43);
            this._lsFirsTabControl.Controls.Add(this.tabPage44);
            this._lsFirsTabControl.Controls.Add(this.tabPage45);
            this._lsFirsTabControl.Controls.Add(this.tabPage46);
            this._lsFirsTabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._lsFirsTabControl.Location = new System.Drawing.Point(3, 16);
            this._lsFirsTabControl.Multiline = true;
            this._lsFirsTabControl.Name = "_lsFirsTabControl";
            this._lsFirsTabControl.SelectedIndex = 0;
            this._lsFirsTabControl.Size = new System.Drawing.Size(187, 418);
            this._lsFirsTabControl.TabIndex = 2;
            this._lsFirsTabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tabPage39
            // 
            this.tabPage39.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage39.Controls.Add(this._lsAndDgv1Gr1);
            this.tabPage39.Location = new System.Drawing.Point(4, 49);
            this.tabPage39.Name = "tabPage39";
            this.tabPage39.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage39.Size = new System.Drawing.Size(179, 365);
            this.tabPage39.TabIndex = 0;
            this.tabPage39.Text = "ЛС1";
            // 
            // _lsAndDgv1Gr1
            // 
            this._lsAndDgv1Gr1.AllowUserToAddRows = false;
            this._lsAndDgv1Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv1Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv1Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv1Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv1Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv1Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv1Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewComboBoxColumn23});
            this._lsAndDgv1Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv1Gr1.MultiSelect = false;
            this._lsAndDgv1Gr1.Name = "_lsAndDgv1Gr1";
            this._lsAndDgv1Gr1.RowHeadersVisible = false;
            this._lsAndDgv1Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle126.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv1Gr1.RowsDefaultCellStyle = dataGridViewCellStyle126;
            this._lsAndDgv1Gr1.RowTemplate.Height = 20;
            this._lsAndDgv1Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv1Gr1.ShowCellErrors = false;
            this._lsAndDgv1Gr1.ShowCellToolTips = false;
            this._lsAndDgv1Gr1.ShowEditingIcon = false;
            this._lsAndDgv1Gr1.ShowRowErrors = false;
            this._lsAndDgv1Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsAndDgv1Gr1.TabIndex = 2;
            this._lsAndDgv1Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsAndDgvGr1_CellEndEdit);
            this._lsAndDgv1Gr1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.HeaderText = "№";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn24.Width = 24;
            // 
            // dataGridViewComboBoxColumn23
            // 
            this.dataGridViewComboBoxColumn23.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn23.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn23.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn23.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn23.Name = "dataGridViewComboBoxColumn23";
            // 
            // tabPage40
            // 
            this.tabPage40.Controls.Add(this._lsAndDgv2Gr1);
            this.tabPage40.Location = new System.Drawing.Point(4, 49);
            this.tabPage40.Name = "tabPage40";
            this.tabPage40.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage40.Size = new System.Drawing.Size(179, 365);
            this.tabPage40.TabIndex = 1;
            this.tabPage40.Text = "ЛС2";
            this.tabPage40.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv2Gr1
            // 
            this._lsAndDgv2Gr1.AllowUserToAddRows = false;
            this._lsAndDgv2Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv2Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv2Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv2Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv2Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv2Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv2Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewComboBoxColumn24});
            this._lsAndDgv2Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv2Gr1.MultiSelect = false;
            this._lsAndDgv2Gr1.Name = "_lsAndDgv2Gr1";
            this._lsAndDgv2Gr1.RowHeadersVisible = false;
            this._lsAndDgv2Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle127.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv2Gr1.RowsDefaultCellStyle = dataGridViewCellStyle127;
            this._lsAndDgv2Gr1.RowTemplate.Height = 20;
            this._lsAndDgv2Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv2Gr1.ShowCellErrors = false;
            this._lsAndDgv2Gr1.ShowCellToolTips = false;
            this._lsAndDgv2Gr1.ShowEditingIcon = false;
            this._lsAndDgv2Gr1.ShowRowErrors = false;
            this._lsAndDgv2Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsAndDgv2Gr1.TabIndex = 3;
            this._lsAndDgv2Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsAndDgvGr1_CellEndEdit);
            this._lsAndDgv2Gr1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.HeaderText = "№";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn25.Width = 24;
            // 
            // dataGridViewComboBoxColumn24
            // 
            this.dataGridViewComboBoxColumn24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn24.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn24.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn24.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn24.Name = "dataGridViewComboBoxColumn24";
            // 
            // tabPage41
            // 
            this.tabPage41.Controls.Add(this._lsAndDgv3Gr1);
            this.tabPage41.Location = new System.Drawing.Point(4, 49);
            this.tabPage41.Name = "tabPage41";
            this.tabPage41.Size = new System.Drawing.Size(179, 365);
            this.tabPage41.TabIndex = 2;
            this.tabPage41.Text = "ЛС3";
            this.tabPage41.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv3Gr1
            // 
            this._lsAndDgv3Gr1.AllowUserToAddRows = false;
            this._lsAndDgv3Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv3Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv3Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv3Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv3Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv3Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv3Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewComboBoxColumn25});
            this._lsAndDgv3Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv3Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv3Gr1.MultiSelect = false;
            this._lsAndDgv3Gr1.Name = "_lsAndDgv3Gr1";
            this._lsAndDgv3Gr1.RowHeadersVisible = false;
            this._lsAndDgv3Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle128.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv3Gr1.RowsDefaultCellStyle = dataGridViewCellStyle128;
            this._lsAndDgv3Gr1.RowTemplate.Height = 20;
            this._lsAndDgv3Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv3Gr1.ShowCellErrors = false;
            this._lsAndDgv3Gr1.ShowCellToolTips = false;
            this._lsAndDgv3Gr1.ShowEditingIcon = false;
            this._lsAndDgv3Gr1.ShowRowErrors = false;
            this._lsAndDgv3Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsAndDgv3Gr1.TabIndex = 3;
            this._lsAndDgv3Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsAndDgvGr1_CellEndEdit);
            this._lsAndDgv3Gr1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.HeaderText = "№";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn26.Width = 24;
            // 
            // dataGridViewComboBoxColumn25
            // 
            this.dataGridViewComboBoxColumn25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn25.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn25.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn25.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn25.Name = "dataGridViewComboBoxColumn25";
            // 
            // tabPage42
            // 
            this.tabPage42.Controls.Add(this._lsAndDgv4Gr1);
            this.tabPage42.Location = new System.Drawing.Point(4, 49);
            this.tabPage42.Name = "tabPage42";
            this.tabPage42.Size = new System.Drawing.Size(179, 365);
            this.tabPage42.TabIndex = 3;
            this.tabPage42.Text = "ЛС4";
            this.tabPage42.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv4Gr1
            // 
            this._lsAndDgv4Gr1.AllowUserToAddRows = false;
            this._lsAndDgv4Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv4Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv4Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv4Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv4Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv4Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv4Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewComboBoxColumn26});
            this._lsAndDgv4Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv4Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv4Gr1.MultiSelect = false;
            this._lsAndDgv4Gr1.Name = "_lsAndDgv4Gr1";
            this._lsAndDgv4Gr1.RowHeadersVisible = false;
            this._lsAndDgv4Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle129.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv4Gr1.RowsDefaultCellStyle = dataGridViewCellStyle129;
            this._lsAndDgv4Gr1.RowTemplate.Height = 20;
            this._lsAndDgv4Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv4Gr1.ShowCellErrors = false;
            this._lsAndDgv4Gr1.ShowCellToolTips = false;
            this._lsAndDgv4Gr1.ShowEditingIcon = false;
            this._lsAndDgv4Gr1.ShowRowErrors = false;
            this._lsAndDgv4Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsAndDgv4Gr1.TabIndex = 3;
            this._lsAndDgv4Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsAndDgvGr1_CellEndEdit);
            this._lsAndDgv4Gr1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.HeaderText = "№";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn27.Width = 24;
            // 
            // dataGridViewComboBoxColumn26
            // 
            this.dataGridViewComboBoxColumn26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn26.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn26.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn26.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn26.Name = "dataGridViewComboBoxColumn26";
            // 
            // tabPage43
            // 
            this.tabPage43.Controls.Add(this._lsAndDgv5Gr1);
            this.tabPage43.Location = new System.Drawing.Point(4, 49);
            this.tabPage43.Name = "tabPage43";
            this.tabPage43.Size = new System.Drawing.Size(179, 365);
            this.tabPage43.TabIndex = 4;
            this.tabPage43.Text = "ЛС5";
            this.tabPage43.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv5Gr1
            // 
            this._lsAndDgv5Gr1.AllowUserToAddRows = false;
            this._lsAndDgv5Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv5Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv5Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv5Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv5Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv5Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv5Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewComboBoxColumn27});
            this._lsAndDgv5Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv5Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv5Gr1.MultiSelect = false;
            this._lsAndDgv5Gr1.Name = "_lsAndDgv5Gr1";
            this._lsAndDgv5Gr1.RowHeadersVisible = false;
            this._lsAndDgv5Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle130.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv5Gr1.RowsDefaultCellStyle = dataGridViewCellStyle130;
            this._lsAndDgv5Gr1.RowTemplate.Height = 20;
            this._lsAndDgv5Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv5Gr1.ShowCellErrors = false;
            this._lsAndDgv5Gr1.ShowCellToolTips = false;
            this._lsAndDgv5Gr1.ShowEditingIcon = false;
            this._lsAndDgv5Gr1.ShowRowErrors = false;
            this._lsAndDgv5Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsAndDgv5Gr1.TabIndex = 3;
            this._lsAndDgv5Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsAndDgvGr1_CellEndEdit);
            this._lsAndDgv5Gr1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.HeaderText = "№";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn28.Width = 24;
            // 
            // dataGridViewComboBoxColumn27
            // 
            this.dataGridViewComboBoxColumn27.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn27.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn27.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn27.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn27.Name = "dataGridViewComboBoxColumn27";
            // 
            // tabPage44
            // 
            this.tabPage44.Controls.Add(this._lsAndDgv6Gr1);
            this.tabPage44.Location = new System.Drawing.Point(4, 49);
            this.tabPage44.Name = "tabPage44";
            this.tabPage44.Size = new System.Drawing.Size(179, 365);
            this.tabPage44.TabIndex = 5;
            this.tabPage44.Text = "ЛС6";
            this.tabPage44.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv6Gr1
            // 
            this._lsAndDgv6Gr1.AllowUserToAddRows = false;
            this._lsAndDgv6Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv6Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv6Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv6Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv6Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv6Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv6Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewComboBoxColumn28});
            this._lsAndDgv6Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv6Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv6Gr1.MultiSelect = false;
            this._lsAndDgv6Gr1.Name = "_lsAndDgv6Gr1";
            this._lsAndDgv6Gr1.RowHeadersVisible = false;
            this._lsAndDgv6Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle131.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv6Gr1.RowsDefaultCellStyle = dataGridViewCellStyle131;
            this._lsAndDgv6Gr1.RowTemplate.Height = 20;
            this._lsAndDgv6Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv6Gr1.ShowCellErrors = false;
            this._lsAndDgv6Gr1.ShowCellToolTips = false;
            this._lsAndDgv6Gr1.ShowEditingIcon = false;
            this._lsAndDgv6Gr1.ShowRowErrors = false;
            this._lsAndDgv6Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsAndDgv6Gr1.TabIndex = 3;
            this._lsAndDgv6Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsAndDgvGr1_CellEndEdit);
            this._lsAndDgv6Gr1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.HeaderText = "№";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            this.dataGridViewTextBoxColumn29.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn29.Width = 24;
            // 
            // dataGridViewComboBoxColumn28
            // 
            this.dataGridViewComboBoxColumn28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn28.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn28.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn28.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn28.Name = "dataGridViewComboBoxColumn28";
            // 
            // tabPage45
            // 
            this.tabPage45.Controls.Add(this._lsAndDgv7Gr1);
            this.tabPage45.Location = new System.Drawing.Point(4, 49);
            this.tabPage45.Name = "tabPage45";
            this.tabPage45.Size = new System.Drawing.Size(179, 365);
            this.tabPage45.TabIndex = 6;
            this.tabPage45.Text = "ЛС7";
            this.tabPage45.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv7Gr1
            // 
            this._lsAndDgv7Gr1.AllowUserToAddRows = false;
            this._lsAndDgv7Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv7Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv7Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv7Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv7Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv7Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv7Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewComboBoxColumn29});
            this._lsAndDgv7Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv7Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv7Gr1.MultiSelect = false;
            this._lsAndDgv7Gr1.Name = "_lsAndDgv7Gr1";
            this._lsAndDgv7Gr1.RowHeadersVisible = false;
            this._lsAndDgv7Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle132.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv7Gr1.RowsDefaultCellStyle = dataGridViewCellStyle132;
            this._lsAndDgv7Gr1.RowTemplate.Height = 20;
            this._lsAndDgv7Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv7Gr1.ShowCellErrors = false;
            this._lsAndDgv7Gr1.ShowCellToolTips = false;
            this._lsAndDgv7Gr1.ShowEditingIcon = false;
            this._lsAndDgv7Gr1.ShowRowErrors = false;
            this._lsAndDgv7Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsAndDgv7Gr1.TabIndex = 3;
            this._lsAndDgv7Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsAndDgvGr1_CellEndEdit);
            this._lsAndDgv7Gr1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.HeaderText = "№";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            this.dataGridViewTextBoxColumn30.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn30.Width = 24;
            // 
            // dataGridViewComboBoxColumn29
            // 
            this.dataGridViewComboBoxColumn29.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn29.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn29.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn29.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn29.Name = "dataGridViewComboBoxColumn29";
            // 
            // tabPage46
            // 
            this.tabPage46.Controls.Add(this._lsAndDgv8Gr1);
            this.tabPage46.Location = new System.Drawing.Point(4, 49);
            this.tabPage46.Name = "tabPage46";
            this.tabPage46.Size = new System.Drawing.Size(179, 365);
            this.tabPage46.TabIndex = 7;
            this.tabPage46.Text = "ЛС8";
            this.tabPage46.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv8Gr1
            // 
            this._lsAndDgv8Gr1.AllowUserToAddRows = false;
            this._lsAndDgv8Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv8Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv8Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv8Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv8Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv8Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv8Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewComboBoxColumn30});
            this._lsAndDgv8Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv8Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv8Gr1.MultiSelect = false;
            this._lsAndDgv8Gr1.Name = "_lsAndDgv8Gr1";
            this._lsAndDgv8Gr1.RowHeadersVisible = false;
            this._lsAndDgv8Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle133.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv8Gr1.RowsDefaultCellStyle = dataGridViewCellStyle133;
            this._lsAndDgv8Gr1.RowTemplate.Height = 20;
            this._lsAndDgv8Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv8Gr1.ShowCellErrors = false;
            this._lsAndDgv8Gr1.ShowCellToolTips = false;
            this._lsAndDgv8Gr1.ShowEditingIcon = false;
            this._lsAndDgv8Gr1.ShowRowErrors = false;
            this._lsAndDgv8Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsAndDgv8Gr1.TabIndex = 3;
            this._lsAndDgv8Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsAndDgvGr1_CellEndEdit);
            this._lsAndDgv8Gr1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.HeaderText = "№";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            this.dataGridViewTextBoxColumn31.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn31.Width = 24;
            // 
            // dataGridViewComboBoxColumn30
            // 
            this.dataGridViewComboBoxColumn30.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn30.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn30.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn30.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn30.Name = "dataGridViewComboBoxColumn30";
            // 
            // _apvAvrTabPage
            // 
            this._apvAvrTabPage.Controls.Add(this._avrGroupBox);
            this._apvAvrTabPage.Controls.Add(this._apvGroupBox);
            this._apvAvrTabPage.Location = new System.Drawing.Point(4, 25);
            this._apvAvrTabPage.Name = "_apvAvrTabPage";
            this._apvAvrTabPage.Size = new System.Drawing.Size(958, 443);
            this._apvAvrTabPage.TabIndex = 16;
            this._apvAvrTabPage.Text = "АПВ И АВР";
            this._apvAvrTabPage.UseVisualStyleBackColor = true;
            // 
            // _avrGroupBox
            // 
            this._avrGroupBox.Controls.Add(this._avrReclouserGroupBox);
            this._avrGroupBox.Controls.Add(this.label172);
            this._avrGroupBox.Controls.Add(this._avrTypeComboBox);
            this._avrGroupBox.Controls.Add(this._avrSectionGroupBox);
            this._avrGroupBox.Location = new System.Drawing.Point(3, 3);
            this._avrGroupBox.Name = "_avrGroupBox";
            this._avrGroupBox.Size = new System.Drawing.Size(311, 350);
            this._avrGroupBox.TabIndex = 2;
            this._avrGroupBox.TabStop = false;
            this._avrGroupBox.Text = "АВР";
            // 
            // _avrReclouserGroupBox
            // 
            this._avrReclouserGroupBox.Controls.Add(this._avrRecBlockSDTUCB);
            this._avrReclouserGroupBox.Controls.Add(this.label174);
            this._avrReclouserGroupBox.Controls.Add(this._avrRecBlockSelfCheck);
            this._avrReclouserGroupBox.Controls.Add(this.label181);
            this._avrReclouserGroupBox.Controls.Add(this._avrRecInputU2CB);
            this._avrReclouserGroupBox.Controls.Add(this._avrRecUminMTB);
            this._avrReclouserGroupBox.Controls.Add(this.label179);
            this._avrReclouserGroupBox.Controls.Add(this.label182);
            this._avrReclouserGroupBox.Controls.Add(this._avrRecInputU1CB);
            this._avrReclouserGroupBox.Controls.Add(this._avrRecUmaxMTB);
            this._avrReclouserGroupBox.Controls.Add(this.label180);
            this._avrReclouserGroupBox.Controls.Add(this.label177);
            this._avrReclouserGroupBox.Controls.Add(this._avrRecBanComboBox);
            this._avrReclouserGroupBox.Controls.Add(this.label178);
            this._avrReclouserGroupBox.Controls.Add(this.label176);
            this._avrReclouserGroupBox.Controls.Add(this._avrReadyRecMTB);
            this._avrReclouserGroupBox.Controls.Add(this._avrRecKeyComboBox);
            this._avrReclouserGroupBox.Controls.Add(this._tBlockRecMTB);
            this._avrReclouserGroupBox.Controls.Add(this.label175);
            this._avrReclouserGroupBox.Controls.Add(this._avrRecModeComboBox);
            this._avrReclouserGroupBox.Controls.Add(this.label173);
            this._avrReclouserGroupBox.Location = new System.Drawing.Point(23, 43);
            this._avrReclouserGroupBox.Name = "_avrReclouserGroupBox";
            this._avrReclouserGroupBox.Size = new System.Drawing.Size(269, 301);
            this._avrReclouserGroupBox.TabIndex = 3;
            this._avrReclouserGroupBox.TabStop = false;
            this._avrReclouserGroupBox.Text = "Конфигурация АВР";
            // 
            // _avrRecBlockSDTUCB
            // 
            this._avrRecBlockSDTUCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrRecBlockSDTUCB.FormattingEnabled = true;
            this._avrRecBlockSDTUCB.Location = new System.Drawing.Point(145, 83);
            this._avrRecBlockSDTUCB.Name = "_avrRecBlockSDTUCB";
            this._avrRecBlockSDTUCB.Size = new System.Drawing.Size(113, 21);
            this._avrRecBlockSDTUCB.TabIndex = 32;
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(22, 86);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(102, 13);
            this.label174.TabIndex = 33;
            this.label174.Text = "Блокировка СДТУ";
            // 
            // _avrRecBlockSelfCheck
            // 
            this._avrRecBlockSelfCheck.AutoSize = true;
            this._avrRecBlockSelfCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._avrRecBlockSelfCheck.Location = new System.Drawing.Point(21, 59);
            this._avrRecBlockSelfCheck.Name = "_avrRecBlockSelfCheck";
            this._avrRecBlockSelfCheck.Size = new System.Drawing.Size(157, 17);
            this._avrRecBlockSelfCheck.TabIndex = 31;
            this._avrRecBlockSelfCheck.Text = "Блокировка по самооткл.";
            this._avrRecBlockSelfCheck.UseVisualStyleBackColor = true;
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Location = new System.Drawing.Point(22, 246);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(44, 13);
            this.label181.TabIndex = 30;
            this.label181.Text = "Umin, B";
            // 
            // _avrRecInputU2CB
            // 
            this._avrRecInputU2CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrRecInputU2CB.FormattingEnabled = true;
            this._avrRecInputU2CB.Location = new System.Drawing.Point(145, 204);
            this._avrRecInputU2CB.Name = "_avrRecInputU2CB";
            this._avrRecInputU2CB.Size = new System.Drawing.Size(113, 21);
            this._avrRecInputU2CB.TabIndex = 25;
            // 
            // _avrRecUminMTB
            // 
            this._avrRecUminMTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._avrRecUminMTB.Location = new System.Drawing.Point(145, 244);
            this._avrRecUminMTB.Name = "_avrRecUminMTB";
            this._avrRecUminMTB.Size = new System.Drawing.Size(113, 20);
            this._avrRecUminMTB.TabIndex = 29;
            this._avrRecUminMTB.Tag = "3276700";
            this._avrRecUminMTB.Text = "0";
            this._avrRecUminMTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(22, 207);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(48, 13);
            this.label179.TabIndex = 26;
            this.label179.Text = "Вход U2";
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(22, 228);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(47, 13);
            this.label182.TabIndex = 28;
            this.label182.Text = "Umax, B";
            // 
            // _avrRecInputU1CB
            // 
            this._avrRecInputU1CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrRecInputU1CB.FormattingEnabled = true;
            this._avrRecInputU1CB.Location = new System.Drawing.Point(145, 183);
            this._avrRecInputU1CB.Name = "_avrRecInputU1CB";
            this._avrRecInputU1CB.Size = new System.Drawing.Size(113, 21);
            this._avrRecInputU1CB.TabIndex = 23;
            // 
            // _avrRecUmaxMTB
            // 
            this._avrRecUmaxMTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._avrRecUmaxMTB.Location = new System.Drawing.Point(145, 225);
            this._avrRecUmaxMTB.Name = "_avrRecUmaxMTB";
            this._avrRecUmaxMTB.Size = new System.Drawing.Size(113, 20);
            this._avrRecUmaxMTB.TabIndex = 27;
            this._avrRecUmaxMTB.Tag = "3276700";
            this._avrRecUmaxMTB.Text = "0";
            this._avrRecUmaxMTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Location = new System.Drawing.Point(22, 186);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(48, 13);
            this.label180.TabIndex = 24;
            this.label180.Text = "Вход U1";
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Location = new System.Drawing.Point(23, 168);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(61, 13);
            this.label177.TabIndex = 22;
            this.label177.Text = "tготов., мс";
            // 
            // _avrRecBanComboBox
            // 
            this._avrRecBanComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrRecBanComboBox.FormattingEnabled = true;
            this._avrRecBanComboBox.Location = new System.Drawing.Point(145, 123);
            this._avrRecBanComboBox.Name = "_avrRecBanComboBox";
            this._avrRecBanComboBox.Size = new System.Drawing.Size(113, 21);
            this._avrRecBanComboBox.TabIndex = 10;
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Location = new System.Drawing.Point(23, 149);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(45, 13);
            this.label178.TabIndex = 21;
            this.label178.Text = "tср., мс";
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Location = new System.Drawing.Point(22, 126);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(43, 13);
            this.label176.TabIndex = 11;
            this.label176.Text = "Запрет";
            // 
            // _avrReadyRecMTB
            // 
            this._avrReadyRecMTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._avrReadyRecMTB.Location = new System.Drawing.Point(145, 163);
            this._avrReadyRecMTB.Name = "_avrReadyRecMTB";
            this._avrReadyRecMTB.Size = new System.Drawing.Size(113, 20);
            this._avrReadyRecMTB.TabIndex = 20;
            this._avrReadyRecMTB.Tag = "3276700";
            this._avrReadyRecMTB.Text = "0";
            this._avrReadyRecMTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _avrRecKeyComboBox
            // 
            this._avrRecKeyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrRecKeyComboBox.FormattingEnabled = true;
            this._avrRecKeyComboBox.Location = new System.Drawing.Point(145, 103);
            this._avrRecKeyComboBox.Name = "_avrRecKeyComboBox";
            this._avrRecKeyComboBox.Size = new System.Drawing.Size(113, 21);
            this._avrRecKeyComboBox.TabIndex = 8;
            // 
            // _tBlockRecMTB
            // 
            this._tBlockRecMTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._tBlockRecMTB.Location = new System.Drawing.Point(145, 144);
            this._tBlockRecMTB.Name = "_tBlockRecMTB";
            this._tBlockRecMTB.Size = new System.Drawing.Size(113, 20);
            this._tBlockRecMTB.TabIndex = 19;
            this._tBlockRecMTB.Tag = "3276700";
            this._tBlockRecMTB.Text = "0";
            this._tBlockRecMTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(22, 106);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(33, 13);
            this.label175.TabIndex = 9;
            this.label175.Text = "Ключ";
            // 
            // _avrRecModeComboBox
            // 
            this._avrRecModeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrRecModeComboBox.FormattingEnabled = true;
            this._avrRecModeComboBox.Location = new System.Drawing.Point(145, 28);
            this._avrRecModeComboBox.Name = "_avrRecModeComboBox";
            this._avrRecModeComboBox.Size = new System.Drawing.Size(113, 21);
            this._avrRecModeComboBox.TabIndex = 3;
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Location = new System.Drawing.Point(22, 31);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(42, 13);
            this.label173.TabIndex = 2;
            this.label173.Text = "Режим";
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Location = new System.Drawing.Point(39, 17);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(50, 13);
            this.label172.TabIndex = 29;
            this.label172.Text = "Тип АВР";
            // 
            // _avrTypeComboBox
            // 
            this._avrTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrTypeComboBox.FormattingEnabled = true;
            this._avrTypeComboBox.Location = new System.Drawing.Point(155, 14);
            this._avrTypeComboBox.Name = "_avrTypeComboBox";
            this._avrTypeComboBox.Size = new System.Drawing.Size(113, 21);
            this._avrTypeComboBox.TabIndex = 28;
            this._avrTypeComboBox.SelectedIndexChanged += new System.EventHandler(this._avrTypeComboBox_SelectedIndexChanged);
            // 
            // _avrSectionGroupBox
            // 
            this._avrSectionGroupBox.Controls.Add(this.label153);
            this._avrSectionGroupBox.Controls.Add(this.label169);
            this._avrSectionGroupBox.Controls.Add(this.label154);
            this._avrSectionGroupBox.Controls.Add(this._avrBySignal);
            this._avrSectionGroupBox.Controls.Add(this._avrClear);
            this._avrSectionGroupBox.Controls.Add(this._avrByOff);
            this._avrSectionGroupBox.Controls.Add(this.label155);
            this._avrSectionGroupBox.Controls.Add(this.label168);
            this._avrSectionGroupBox.Controls.Add(this._avrTOff);
            this._avrSectionGroupBox.Controls.Add(this._avrBySelfOff);
            this._avrSectionGroupBox.Controls.Add(this.label156);
            this._avrSectionGroupBox.Controls.Add(this.label167);
            this._avrSectionGroupBox.Controls.Add(this._avrTBack);
            this._avrSectionGroupBox.Controls.Add(this._avrByDiff);
            this._avrSectionGroupBox.Controls.Add(this.label91);
            this._avrSectionGroupBox.Controls.Add(this.label166);
            this._avrSectionGroupBox.Controls.Add(this._avrBack);
            this._avrSectionGroupBox.Controls.Add(this._avrSIGNOn);
            this._avrSectionGroupBox.Controls.Add(this.label160);
            this._avrSectionGroupBox.Controls.Add(this.label165);
            this._avrSectionGroupBox.Controls.Add(this._avrTSr);
            this._avrSectionGroupBox.Controls.Add(this._avrBlocking);
            this._avrSectionGroupBox.Controls.Add(this.label161);
            this._avrSectionGroupBox.Controls.Add(this.label164);
            this._avrSectionGroupBox.Controls.Add(this._avrResolve);
            this._avrSectionGroupBox.Controls.Add(this._avrBlockClear);
            this._avrSectionGroupBox.Controls.Add(this.label163);
            this._avrSectionGroupBox.Location = new System.Drawing.Point(23, 43);
            this._avrSectionGroupBox.Name = "_avrSectionGroupBox";
            this._avrSectionGroupBox.Size = new System.Drawing.Size(269, 301);
            this._avrSectionGroupBox.TabIndex = 27;
            this._avrSectionGroupBox.TabStop = false;
            this._avrSectionGroupBox.Text = "Конфигурация АВР";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(15, 35);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(71, 13);
            this.label153.TabIndex = 26;
            this.label153.Text = "(по питанию)";
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Location = new System.Drawing.Point(16, 21);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(64, 13);
            this.label169.TabIndex = 1;
            this.label169.Text = "От сигнала";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(16, 275);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(38, 13);
            this.label154.TabIndex = 25;
            this.label154.Text = "Сброс";
            // 
            // _avrBySignal
            // 
            this._avrBySignal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrBySignal.FormattingEnabled = true;
            this._avrBySignal.Location = new System.Drawing.Point(132, 25);
            this._avrBySignal.Name = "_avrBySignal";
            this._avrBySignal.Size = new System.Drawing.Size(113, 21);
            this._avrBySignal.TabIndex = 0;
            // 
            // _avrClear
            // 
            this._avrClear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrClear.FormattingEnabled = true;
            this._avrClear.Location = new System.Drawing.Point(132, 272);
            this._avrClear.Name = "_avrClear";
            this._avrClear.Size = new System.Drawing.Size(113, 21);
            this._avrClear.TabIndex = 24;
            // 
            // _avrByOff
            // 
            this._avrByOff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrByOff.FormattingEnabled = true;
            this._avrByOff.Location = new System.Drawing.Point(132, 55);
            this._avrByOff.Name = "_avrByOff";
            this._avrByOff.Size = new System.Drawing.Size(113, 21);
            this._avrByOff.TabIndex = 2;
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(16, 256);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(53, 13);
            this.label155.TabIndex = 23;
            this.label155.Text = "tоткл, мс";
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Location = new System.Drawing.Point(16, 58);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(86, 13);
            this.label168.TabIndex = 3;
            this.label168.Text = "По отключению";
            // 
            // _avrTOff
            // 
            this._avrTOff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._avrTOff.Location = new System.Drawing.Point(132, 253);
            this._avrTOff.Name = "_avrTOff";
            this._avrTOff.Size = new System.Drawing.Size(113, 20);
            this._avrTOff.TabIndex = 22;
            this._avrTOff.Tag = "3276700";
            this._avrTOff.Text = "0";
            this._avrTOff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _avrBySelfOff
            // 
            this._avrBySelfOff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrBySelfOff.FormattingEnabled = true;
            this._avrBySelfOff.Location = new System.Drawing.Point(132, 75);
            this._avrBySelfOff.Name = "_avrBySelfOff";
            this._avrBySelfOff.Size = new System.Drawing.Size(113, 21);
            this._avrBySelfOff.TabIndex = 4;
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(16, 237);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(48, 13);
            this.label156.TabIndex = 21;
            this.label156.Text = "tвоз, мс";
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(16, 78);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(112, 13);
            this.label167.TabIndex = 5;
            this.label167.Text = "По самоотключению";
            // 
            // _avrTBack
            // 
            this._avrTBack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._avrTBack.Location = new System.Drawing.Point(132, 234);
            this._avrTBack.Name = "_avrTBack";
            this._avrTBack.Size = new System.Drawing.Size(113, 20);
            this._avrTBack.TabIndex = 20;
            this._avrTBack.Tag = "3276700";
            this._avrTBack.Text = "0";
            this._avrTBack.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _avrByDiff
            // 
            this._avrByDiff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrByDiff.FormattingEnabled = true;
            this._avrByDiff.Location = new System.Drawing.Point(132, 95);
            this._avrByDiff.Name = "_avrByDiff";
            this._avrByDiff.Size = new System.Drawing.Size(113, 21);
            this._avrByDiff.TabIndex = 6;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(16, 217);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(49, 13);
            this.label91.TabIndex = 19;
            this.label91.Text = "Возврат";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(16, 98);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(62, 13);
            this.label166.TabIndex = 7;
            this.label166.Text = "По защите";
            // 
            // _avrBack
            // 
            this._avrBack.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._avrBack.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._avrBack.FormattingEnabled = true;
            this._avrBack.Location = new System.Drawing.Point(132, 214);
            this._avrBack.Name = "_avrBack";
            this._avrBack.Size = new System.Drawing.Size(113, 21);
            this._avrBack.TabIndex = 18;
            // 
            // _avrSIGNOn
            // 
            this._avrSIGNOn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._avrSIGNOn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._avrSIGNOn.FormattingEnabled = true;
            this._avrSIGNOn.Location = new System.Drawing.Point(132, 115);
            this._avrSIGNOn.Name = "_avrSIGNOn";
            this._avrSIGNOn.Size = new System.Drawing.Size(113, 21);
            this._avrSIGNOn.TabIndex = 8;
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(16, 198);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(42, 13);
            this.label160.TabIndex = 17;
            this.label160.Text = "tср, мс";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Location = new System.Drawing.Point(16, 118);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(75, 13);
            this.label165.TabIndex = 9;
            this.label165.Text = "Сигнал пуска";
            // 
            // _avrTSr
            // 
            this._avrTSr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._avrTSr.Location = new System.Drawing.Point(132, 195);
            this._avrTSr.Name = "_avrTSr";
            this._avrTSr.Size = new System.Drawing.Size(113, 20);
            this._avrTSr.TabIndex = 16;
            this._avrTSr.Tag = "3276700";
            this._avrTSr.Text = "0";
            this._avrTSr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _avrBlocking
            // 
            this._avrBlocking.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._avrBlocking.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._avrBlocking.FormattingEnabled = true;
            this._avrBlocking.Location = new System.Drawing.Point(132, 135);
            this._avrBlocking.Name = "_avrBlocking";
            this._avrBlocking.Size = new System.Drawing.Size(113, 21);
            this._avrBlocking.TabIndex = 10;
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(16, 178);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(87, 13);
            this.label161.TabIndex = 15;
            this.label161.Text = "АВР разрешено";
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Location = new System.Drawing.Point(16, 138);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(68, 13);
            this.label164.TabIndex = 11;
            this.label164.Text = "Блокировка";
            // 
            // _avrResolve
            // 
            this._avrResolve.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._avrResolve.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._avrResolve.FormattingEnabled = true;
            this._avrResolve.Location = new System.Drawing.Point(132, 175);
            this._avrResolve.Name = "_avrResolve";
            this._avrResolve.Size = new System.Drawing.Size(113, 21);
            this._avrResolve.TabIndex = 14;
            // 
            // _avrBlockClear
            // 
            this._avrBlockClear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._avrBlockClear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._avrBlockClear.FormattingEnabled = true;
            this._avrBlockClear.Location = new System.Drawing.Point(132, 155);
            this._avrBlockClear.Name = "_avrBlockClear";
            this._avrBlockClear.Size = new System.Drawing.Size(113, 21);
            this._avrBlockClear.TabIndex = 12;
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Location = new System.Drawing.Point(16, 158);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(101, 13);
            this.label163.TabIndex = 13;
            this.label163.Text = "Сброс блокировки";
            // 
            // _apvGroupBox
            // 
            this._apvGroupBox.Controls.Add(this._blokFromUrov);
            this._apvGroupBox.Controls.Add(this.label142);
            this._apvGroupBox.Controls.Add(this.label143);
            this._apvGroupBox.Controls.Add(this._apvKrat4Gr1);
            this._apvGroupBox.Controls.Add(this._apvKrat3Gr1);
            this._apvGroupBox.Controls.Add(this.label144);
            this._apvGroupBox.Controls.Add(this.label145);
            this._apvGroupBox.Controls.Add(this.label146);
            this._apvGroupBox.Controls.Add(this.label147);
            this._apvGroupBox.Controls.Add(this.label94);
            this._apvGroupBox.Controls.Add(this.label148);
            this._apvGroupBox.Controls.Add(this._apvSwitchOffGr1);
            this._apvGroupBox.Controls.Add(this._apvKrat2Gr1);
            this._apvGroupBox.Controls.Add(this._apvKrat1Gr1);
            this._apvGroupBox.Controls.Add(this._apvTreadyGr1);
            this._apvGroupBox.Controls.Add(this._apvTblockGr1);
            this._apvGroupBox.Controls.Add(this._apvBlockGr1);
            this._apvGroupBox.Controls.Add(this._timeDisable);
            this._apvGroupBox.Controls.Add(this.label87);
            this._apvGroupBox.Controls.Add(this._typeDisable);
            this._apvGroupBox.Controls.Add(this.label95);
            this._apvGroupBox.Controls.Add(this._disableApv);
            this._apvGroupBox.Controls.Add(this.label149);
            this._apvGroupBox.Controls.Add(this._apvModeGr1);
            this._apvGroupBox.Controls.Add(this.label150);
            this._apvGroupBox.Location = new System.Drawing.Point(320, 3);
            this._apvGroupBox.Name = "_apvGroupBox";
            this._apvGroupBox.Size = new System.Drawing.Size(248, 284);
            this._apvGroupBox.TabIndex = 0;
            this._apvGroupBox.TabStop = false;
            this._apvGroupBox.Text = "АПВ";
            // 
            // _blokFromUrov
            // 
            this._blokFromUrov.AutoSize = true;
            this._blokFromUrov.Location = new System.Drawing.Point(9, 43);
            this._blokFromUrov.Name = "_blokFromUrov";
            this._blokFromUrov.Size = new System.Drawing.Size(134, 17);
            this._blokFromUrov.TabIndex = 18;
            this._blokFromUrov.Text = "Блокировка от УРОВ";
            this._blokFromUrov.UseVisualStyleBackColor = true;
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(6, 240);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(60, 13);
            this.label142.TabIndex = 17;
            this.label142.Text = "4 Крат, мс";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(6, 221);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(60, 13);
            this.label143.TabIndex = 16;
            this.label143.Text = "3 Крат, мс";
            // 
            // _apvKrat4Gr1
            // 
            this._apvKrat4Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apvKrat4Gr1.Location = new System.Drawing.Point(95, 238);
            this._apvKrat4Gr1.Name = "_apvKrat4Gr1";
            this._apvKrat4Gr1.Size = new System.Drawing.Size(82, 20);
            this._apvKrat4Gr1.TabIndex = 15;
            this._apvKrat4Gr1.Tag = "3276700";
            this._apvKrat4Gr1.Text = "20";
            this._apvKrat4Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apvKrat3Gr1
            // 
            this._apvKrat3Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apvKrat3Gr1.Location = new System.Drawing.Point(95, 219);
            this._apvKrat3Gr1.Name = "_apvKrat3Gr1";
            this._apvKrat3Gr1.Size = new System.Drawing.Size(82, 20);
            this._apvKrat3Gr1.TabIndex = 14;
            this._apvKrat3Gr1.Tag = "3276700";
            this._apvKrat3Gr1.Text = "20";
            this._apvKrat3Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(6, 259);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(73, 13);
            this.label144.TabIndex = 13;
            this.label144.Text = "Самоотключ.";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(6, 203);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(60, 13);
            this.label145.TabIndex = 12;
            this.label145.Text = "2 Крат, мс";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(6, 184);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(60, 13);
            this.label146.TabIndex = 11;
            this.label146.Text = "1 Крат, мс";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(6, 165);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(64, 13);
            this.label147.TabIndex = 10;
            this.label147.Text = "t готов., мс";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(6, 146);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(60, 13);
            this.label94.TabIndex = 9;
            this.label94.Text = "t блок., мс";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(6, 87);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(74, 13);
            this.label148.TabIndex = 9;
            this.label148.Text = "t запрета, мс";
            // 
            // _apvSwitchOffGr1
            // 
            this._apvSwitchOffGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._apvSwitchOffGr1.FormattingEnabled = true;
            this._apvSwitchOffGr1.Location = new System.Drawing.Point(95, 257);
            this._apvSwitchOffGr1.Name = "_apvSwitchOffGr1";
            this._apvSwitchOffGr1.Size = new System.Drawing.Size(147, 21);
            this._apvSwitchOffGr1.TabIndex = 8;
            // 
            // _apvKrat2Gr1
            // 
            this._apvKrat2Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apvKrat2Gr1.Location = new System.Drawing.Point(95, 200);
            this._apvKrat2Gr1.Name = "_apvKrat2Gr1";
            this._apvKrat2Gr1.Size = new System.Drawing.Size(82, 20);
            this._apvKrat2Gr1.TabIndex = 7;
            this._apvKrat2Gr1.Tag = "3276700";
            this._apvKrat2Gr1.Text = "20";
            this._apvKrat2Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apvKrat1Gr1
            // 
            this._apvKrat1Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apvKrat1Gr1.Location = new System.Drawing.Point(95, 181);
            this._apvKrat1Gr1.Name = "_apvKrat1Gr1";
            this._apvKrat1Gr1.Size = new System.Drawing.Size(82, 20);
            this._apvKrat1Gr1.TabIndex = 6;
            this._apvKrat1Gr1.Tag = "3276700";
            this._apvKrat1Gr1.Text = "20";
            this._apvKrat1Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apvTreadyGr1
            // 
            this._apvTreadyGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apvTreadyGr1.Location = new System.Drawing.Point(95, 162);
            this._apvTreadyGr1.Name = "_apvTreadyGr1";
            this._apvTreadyGr1.Size = new System.Drawing.Size(82, 20);
            this._apvTreadyGr1.TabIndex = 5;
            this._apvTreadyGr1.Tag = "3276700";
            this._apvTreadyGr1.Text = "20";
            this._apvTreadyGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apvTblockGr1
            // 
            this._apvTblockGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apvTblockGr1.Location = new System.Drawing.Point(95, 143);
            this._apvTblockGr1.Name = "_apvTblockGr1";
            this._apvTblockGr1.Size = new System.Drawing.Size(82, 20);
            this._apvTblockGr1.TabIndex = 4;
            this._apvTblockGr1.Tag = "3276700";
            this._apvTblockGr1.Text = "20";
            this._apvTblockGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apvBlockGr1
            // 
            this._apvBlockGr1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._apvBlockGr1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._apvBlockGr1.FormattingEnabled = true;
            this._apvBlockGr1.Location = new System.Drawing.Point(95, 123);
            this._apvBlockGr1.Name = "_apvBlockGr1";
            this._apvBlockGr1.Size = new System.Drawing.Size(147, 21);
            this._apvBlockGr1.TabIndex = 3;
            // 
            // _timeDisable
            // 
            this._timeDisable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._timeDisable.Location = new System.Drawing.Point(95, 84);
            this._timeDisable.Name = "_timeDisable";
            this._timeDisable.Size = new System.Drawing.Size(82, 20);
            this._timeDisable.TabIndex = 4;
            this._timeDisable.Tag = "3276700";
            this._timeDisable.Text = "20";
            this._timeDisable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(6, 126);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(68, 13);
            this.label87.TabIndex = 2;
            this.label87.Text = "Блокировка";
            // 
            // _typeDisable
            // 
            this._typeDisable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._typeDisable.FormattingEnabled = true;
            this._typeDisable.Location = new System.Drawing.Point(95, 103);
            this._typeDisable.Name = "_typeDisable";
            this._typeDisable.Size = new System.Drawing.Size(147, 21);
            this._typeDisable.TabIndex = 3;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(6, 106);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(70, 13);
            this.label95.TabIndex = 2;
            this.label95.Text = "Вид запрета";
            // 
            // _disableApv
            // 
            this._disableApv.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._disableApv.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._disableApv.FormattingEnabled = true;
            this._disableApv.Location = new System.Drawing.Point(95, 64);
            this._disableApv.Name = "_disableApv";
            this._disableApv.Size = new System.Drawing.Size(147, 21);
            this._disableApv.TabIndex = 3;
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(6, 67);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(68, 13);
            this.label149.TabIndex = 2;
            this.label149.Text = "Запрет АПВ";
            // 
            // _apvModeGr1
            // 
            this._apvModeGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._apvModeGr1.FormattingEnabled = true;
            this._apvModeGr1.Location = new System.Drawing.Point(95, 19);
            this._apvModeGr1.Name = "_apvModeGr1";
            this._apvModeGr1.Size = new System.Drawing.Size(147, 21);
            this._apvModeGr1.TabIndex = 1;
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(6, 22);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(42, 13);
            this.label150.TabIndex = 0;
            this.label150.Text = "Режим";
            // 
            // _ksiunpTabPage
            // 
            this._ksiunpTabPage.Controls.Add(this.label82);
            this._ksiunpTabPage.Controls.Add(this.groupBox39);
            this._ksiunpTabPage.Controls.Add(this.groupBox35);
            this._ksiunpTabPage.Controls.Add(this.groupBox34);
            this._ksiunpTabPage.Controls.Add(this.label30);
            this._ksiunpTabPage.Location = new System.Drawing.Point(4, 25);
            this._ksiunpTabPage.Name = "_ksiunpTabPage";
            this._ksiunpTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._ksiunpTabPage.Size = new System.Drawing.Size(958, 443);
            this._ksiunpTabPage.TabIndex = 12;
            this._ksiunpTabPage.Text = "КС и УППН";
            this._ksiunpTabPage.UseVisualStyleBackColor = true;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(18, 3);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(393, 13);
            this.label82.TabIndex = 28;
            this.label82.Text = "Контроль синхронизма и условий постановки под напряжение (КС и УППН)";
            // 
            // groupBox39
            // 
            this.groupBox39.Controls.Add(this._blockTNauto);
            this.groupBox39.Controls.Add(this.groupBox40);
            this.groupBox39.Controls.Add(this.groupBox41);
            this.groupBox39.Controls.Add(this.groupBox42);
            this.groupBox39.Controls.Add(this._sinhrAutoUmaxGr1);
            this.groupBox39.Controls.Add(this._sinhrAutoModeGr1);
            this.groupBox39.Controls.Add(this._dUmaxAutoLabel);
            this.groupBox39.Controls.Add(this.label75);
            this.groupBox39.Location = new System.Drawing.Point(592, 23);
            this.groupBox39.Name = "groupBox39";
            this.groupBox39.Size = new System.Drawing.Size(288, 303);
            this.groupBox39.TabIndex = 17;
            this.groupBox39.TabStop = false;
            this.groupBox39.Text = "Уставки автоматического включения";
            // 
            // _blockTNauto
            // 
            this._blockTNauto.AutoSize = true;
            this._blockTNauto.Location = new System.Drawing.Point(12, 43);
            this._blockTNauto.Name = "_blockTNauto";
            this._blockTNauto.Size = new System.Drawing.Size(200, 17);
            this._blockTNauto.TabIndex = 34;
            this._blockTNauto.Text = "Блокировка по неисправности ТН";
            this._blockTNauto.UseVisualStyleBackColor = true;
            // 
            // groupBox40
            // 
            this.groupBox40.Controls.Add(this._catchSinchrAuto);
            this.groupBox40.Controls.Add(this._sinhrAutodFnoGr1);
            this.groupBox40.Controls.Add(this.label17);
            this.groupBox40.Location = new System.Drawing.Point(6, 252);
            this.groupBox40.Name = "groupBox40";
            this.groupBox40.Size = new System.Drawing.Size(276, 45);
            this.groupBox40.TabIndex = 33;
            this.groupBox40.TabStop = false;
            this.groupBox40.Text = "       Улавливание синхронизма(несихр. режим)";
            // 
            // _catchSinchrAuto
            // 
            this._catchSinchrAuto.AutoSize = true;
            this._catchSinchrAuto.Location = new System.Drawing.Point(6, 0);
            this._catchSinchrAuto.Name = "_catchSinchrAuto";
            this._catchSinchrAuto.Size = new System.Drawing.Size(15, 14);
            this._catchSinchrAuto.TabIndex = 25;
            this._catchSinchrAuto.UseVisualStyleBackColor = true;
            // 
            // _sinhrAutodFnoGr1
            // 
            this._sinhrAutodFnoGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrAutodFnoGr1.Location = new System.Drawing.Point(99, 19);
            this._sinhrAutodFnoGr1.Name = "_sinhrAutodFnoGr1";
            this._sinhrAutodFnoGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrAutodFnoGr1.TabIndex = 23;
            this._sinhrAutodFnoGr1.Tag = "40";
            this._sinhrAutodFnoGr1.Text = "0";
            this._sinhrAutodFnoGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(43, 21);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(37, 13);
            this.label17.TabIndex = 12;
            this.label17.Text = "dF, Гц";
            // 
            // groupBox41
            // 
            this.groupBox41.Controls.Add(this._waitSinchrAuto);
            this.groupBox41.Controls.Add(this._sinhrAutodFGr1);
            this.groupBox41.Controls.Add(this.label61);
            this.groupBox41.Controls.Add(this.label62);
            this.groupBox41.Controls.Add(this._sinhrAutodFiGr1);
            this.groupBox41.Location = new System.Drawing.Point(6, 184);
            this.groupBox41.Name = "groupBox41";
            this.groupBox41.Size = new System.Drawing.Size(276, 64);
            this.groupBox41.TabIndex = 32;
            this.groupBox41.TabStop = false;
            this.groupBox41.Text = "       Ожидание синхронизма(синхр. режим)";
            // 
            // _waitSinchrAuto
            // 
            this._waitSinchrAuto.AutoSize = true;
            this._waitSinchrAuto.Location = new System.Drawing.Point(6, 0);
            this._waitSinchrAuto.Name = "_waitSinchrAuto";
            this._waitSinchrAuto.Size = new System.Drawing.Size(15, 14);
            this._waitSinchrAuto.TabIndex = 25;
            this._waitSinchrAuto.UseVisualStyleBackColor = true;
            // 
            // _sinhrAutodFGr1
            // 
            this._sinhrAutodFGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrAutodFGr1.Location = new System.Drawing.Point(99, 19);
            this._sinhrAutodFGr1.Name = "_sinhrAutodFGr1";
            this._sinhrAutodFGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrAutodFGr1.TabIndex = 23;
            this._sinhrAutodFGr1.Tag = "40";
            this._sinhrAutodFGr1.Text = "0";
            this._sinhrAutodFGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(43, 21);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(37, 13);
            this.label61.TabIndex = 12;
            this.label61.Text = "dF, Гц";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(43, 40);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(47, 13);
            this.label62.TabIndex = 13;
            this.label62.Text = "dfi, град";
            // 
            // _sinhrAutodFiGr1
            // 
            this._sinhrAutodFiGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrAutodFiGr1.Location = new System.Drawing.Point(99, 38);
            this._sinhrAutodFiGr1.Name = "_sinhrAutodFiGr1";
            this._sinhrAutodFiGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrAutodFiGr1.TabIndex = 24;
            this._sinhrAutodFiGr1.Tag = "3276700";
            this._sinhrAutodFiGr1.Text = "0";
            this._sinhrAutodFiGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox42
            // 
            this.groupBox42.Controls.Add(this._sinhrAutoNoNoGr1);
            this.groupBox42.Controls.Add(this._sinhrAutoYesNoGr1);
            this.groupBox42.Controls.Add(this._sinhrAutoNoYesGr1);
            this.groupBox42.Controls.Add(this.label66);
            this.groupBox42.Controls.Add(this.label72);
            this.groupBox42.Controls.Add(this.label73);
            this.groupBox42.Location = new System.Drawing.Point(6, 94);
            this.groupBox42.Name = "groupBox42";
            this.groupBox42.Size = new System.Drawing.Size(276, 82);
            this.groupBox42.TabIndex = 31;
            this.groupBox42.TabStop = false;
            this.groupBox42.Text = "Разрешение включения";
            // 
            // _sinhrAutoNoNoGr1
            // 
            this._sinhrAutoNoNoGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrAutoNoNoGr1.FormattingEnabled = true;
            this._sinhrAutoNoNoGr1.Location = new System.Drawing.Point(99, 54);
            this._sinhrAutoNoNoGr1.Name = "_sinhrAutoNoNoGr1";
            this._sinhrAutoNoNoGr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrAutoNoNoGr1.TabIndex = 35;
            // 
            // _sinhrAutoYesNoGr1
            // 
            this._sinhrAutoYesNoGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrAutoYesNoGr1.FormattingEnabled = true;
            this._sinhrAutoYesNoGr1.Location = new System.Drawing.Point(99, 34);
            this._sinhrAutoYesNoGr1.Name = "_sinhrAutoYesNoGr1";
            this._sinhrAutoYesNoGr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrAutoYesNoGr1.TabIndex = 34;
            // 
            // _sinhrAutoNoYesGr1
            // 
            this._sinhrAutoNoYesGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrAutoNoYesGr1.FormattingEnabled = true;
            this._sinhrAutoNoYesGr1.Location = new System.Drawing.Point(99, 14);
            this._sinhrAutoNoYesGr1.Name = "_sinhrAutoNoYesGr1";
            this._sinhrAutoNoYesGr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrAutoNoYesGr1.TabIndex = 33;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(6, 57);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(81, 13);
            this.label66.TabIndex = 32;
            this.label66.Text = "U1 нет, U2 нет";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(6, 37);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(87, 13);
            this.label72.TabIndex = 31;
            this.label72.Text = "U1 есть, U2 нет";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(6, 17);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(87, 13);
            this.label73.TabIndex = 30;
            this.label73.Text = "U1 нет, U2 есть";
            // 
            // _sinhrAutoUmaxGr1
            // 
            this._sinhrAutoUmaxGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrAutoUmaxGr1.Location = new System.Drawing.Point(89, 66);
            this._sinhrAutoUmaxGr1.Name = "_sinhrAutoUmaxGr1";
            this._sinhrAutoUmaxGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrAutoUmaxGr1.TabIndex = 22;
            this._sinhrAutoUmaxGr1.Tag = "3276700";
            this._sinhrAutoUmaxGr1.Text = "0";
            this._sinhrAutoUmaxGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrAutoModeGr1
            // 
            this._sinhrAutoModeGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrAutoModeGr1.FormattingEnabled = true;
            this._sinhrAutoModeGr1.Location = new System.Drawing.Point(89, 16);
            this._sinhrAutoModeGr1.Name = "_sinhrAutoModeGr1";
            this._sinhrAutoModeGr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrAutoModeGr1.TabIndex = 18;
            // 
            // _dUmaxAutoLabel
            // 
            this._dUmaxAutoLabel.AutoSize = true;
            this._dUmaxAutoLabel.Location = new System.Drawing.Point(12, 67);
            this._dUmaxAutoLabel.Name = "_dUmaxAutoLabel";
            this._dUmaxAutoLabel.Size = new System.Drawing.Size(56, 13);
            this._dUmaxAutoLabel.TabIndex = 11;
            this._dUmaxAutoLabel.Text = "dUmax., В";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(12, 18);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(42, 13);
            this.label75.TabIndex = 9;
            this.label75.Text = "Режим";
            // 
            // groupBox35
            // 
            this.groupBox35.Controls.Add(this._blockTNmanual);
            this.groupBox35.Controls.Add(this.groupBox38);
            this.groupBox35.Controls.Add(this.groupBox37);
            this.groupBox35.Controls.Add(this.groupBox36);
            this.groupBox35.Controls.Add(this._sinhrManualUmaxGr1);
            this.groupBox35.Controls.Add(this._sinhrManualModeGr1);
            this.groupBox35.Controls.Add(this._dUmaxManualLabel);
            this.groupBox35.Controls.Add(this.label67);
            this.groupBox35.Location = new System.Drawing.Point(298, 23);
            this.groupBox35.Name = "groupBox35";
            this.groupBox35.Size = new System.Drawing.Size(288, 303);
            this.groupBox35.TabIndex = 16;
            this.groupBox35.TabStop = false;
            this.groupBox35.Text = "Уставки ручного включения";
            // 
            // _blockTNmanual
            // 
            this._blockTNmanual.AutoSize = true;
            this._blockTNmanual.Location = new System.Drawing.Point(15, 43);
            this._blockTNmanual.Name = "_blockTNmanual";
            this._blockTNmanual.Size = new System.Drawing.Size(200, 17);
            this._blockTNmanual.TabIndex = 34;
            this._blockTNmanual.Text = "Блокировка по неисправности ТН";
            this._blockTNmanual.UseVisualStyleBackColor = true;
            // 
            // groupBox38
            // 
            this.groupBox38.Controls.Add(this._catchSinchrManual);
            this.groupBox38.Controls.Add(this._sinhrManualdFnoGr1);
            this.groupBox38.Controls.Add(this.label71);
            this.groupBox38.Location = new System.Drawing.Point(6, 252);
            this.groupBox38.Name = "groupBox38";
            this.groupBox38.Size = new System.Drawing.Size(276, 45);
            this.groupBox38.TabIndex = 33;
            this.groupBox38.TabStop = false;
            this.groupBox38.Text = "      Улавливание синхронизма(несинхр. режим)";
            // 
            // _catchSinchrManual
            // 
            this._catchSinchrManual.AutoSize = true;
            this._catchSinchrManual.Location = new System.Drawing.Point(6, 0);
            this._catchSinchrManual.Name = "_catchSinchrManual";
            this._catchSinchrManual.Size = new System.Drawing.Size(15, 14);
            this._catchSinchrManual.TabIndex = 25;
            this._catchSinchrManual.UseVisualStyleBackColor = true;
            // 
            // _sinhrManualdFnoGr1
            // 
            this._sinhrManualdFnoGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrManualdFnoGr1.Location = new System.Drawing.Point(99, 19);
            this._sinhrManualdFnoGr1.Name = "_sinhrManualdFnoGr1";
            this._sinhrManualdFnoGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrManualdFnoGr1.TabIndex = 23;
            this._sinhrManualdFnoGr1.Tag = "40";
            this._sinhrManualdFnoGr1.Text = "0";
            this._sinhrManualdFnoGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(43, 21);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(37, 13);
            this.label71.TabIndex = 12;
            this.label71.Text = "dF, Гц";
            // 
            // groupBox37
            // 
            this.groupBox37.Controls.Add(this._waitSinchrManual);
            this.groupBox37.Controls.Add(this._sinhrManualdFGr1);
            this.groupBox37.Controls.Add(this.label64);
            this.groupBox37.Controls.Add(this.label63);
            this.groupBox37.Controls.Add(this._sinhrManualdFiGr1);
            this.groupBox37.Location = new System.Drawing.Point(6, 182);
            this.groupBox37.Name = "groupBox37";
            this.groupBox37.Size = new System.Drawing.Size(276, 64);
            this.groupBox37.TabIndex = 32;
            this.groupBox37.TabStop = false;
            this.groupBox37.Text = "       Ожидание синхронизма(синхр. режим)";
            // 
            // _waitSinchrManual
            // 
            this._waitSinchrManual.AutoSize = true;
            this._waitSinchrManual.Location = new System.Drawing.Point(6, 0);
            this._waitSinchrManual.Name = "_waitSinchrManual";
            this._waitSinchrManual.Size = new System.Drawing.Size(15, 14);
            this._waitSinchrManual.TabIndex = 25;
            this._waitSinchrManual.UseVisualStyleBackColor = true;
            // 
            // _sinhrManualdFGr1
            // 
            this._sinhrManualdFGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrManualdFGr1.Location = new System.Drawing.Point(99, 19);
            this._sinhrManualdFGr1.Name = "_sinhrManualdFGr1";
            this._sinhrManualdFGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrManualdFGr1.TabIndex = 23;
            this._sinhrManualdFGr1.Tag = "40";
            this._sinhrManualdFGr1.Text = "0";
            this._sinhrManualdFGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(43, 21);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(37, 13);
            this.label64.TabIndex = 12;
            this.label64.Text = "dF, Гц";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(43, 40);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(47, 13);
            this.label63.TabIndex = 13;
            this.label63.Text = "dfi, град";
            // 
            // _sinhrManualdFiGr1
            // 
            this._sinhrManualdFiGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrManualdFiGr1.Location = new System.Drawing.Point(99, 38);
            this._sinhrManualdFiGr1.Name = "_sinhrManualdFiGr1";
            this._sinhrManualdFiGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrManualdFiGr1.TabIndex = 24;
            this._sinhrManualdFiGr1.Tag = "3276700";
            this._sinhrManualdFiGr1.Text = "0";
            this._sinhrManualdFiGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox36
            // 
            this.groupBox36.Controls.Add(this._sinhrManualNoNoGr1);
            this.groupBox36.Controls.Add(this._sinhrManualYesNoGr1);
            this.groupBox36.Controls.Add(this._sinhrManualNoYesGr1);
            this.groupBox36.Controls.Add(this.label68);
            this.groupBox36.Controls.Add(this.label69);
            this.groupBox36.Controls.Add(this.label70);
            this.groupBox36.Location = new System.Drawing.Point(6, 94);
            this.groupBox36.Name = "groupBox36";
            this.groupBox36.Size = new System.Drawing.Size(276, 82);
            this.groupBox36.TabIndex = 31;
            this.groupBox36.TabStop = false;
            this.groupBox36.Text = "Разрешение включения";
            // 
            // _sinhrManualNoNoGr1
            // 
            this._sinhrManualNoNoGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrManualNoNoGr1.FormattingEnabled = true;
            this._sinhrManualNoNoGr1.Location = new System.Drawing.Point(99, 54);
            this._sinhrManualNoNoGr1.Name = "_sinhrManualNoNoGr1";
            this._sinhrManualNoNoGr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrManualNoNoGr1.TabIndex = 35;
            // 
            // _sinhrManualYesNoGr1
            // 
            this._sinhrManualYesNoGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrManualYesNoGr1.FormattingEnabled = true;
            this._sinhrManualYesNoGr1.Location = new System.Drawing.Point(99, 34);
            this._sinhrManualYesNoGr1.Name = "_sinhrManualYesNoGr1";
            this._sinhrManualYesNoGr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrManualYesNoGr1.TabIndex = 34;
            // 
            // _sinhrManualNoYesGr1
            // 
            this._sinhrManualNoYesGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrManualNoYesGr1.FormattingEnabled = true;
            this._sinhrManualNoYesGr1.Location = new System.Drawing.Point(99, 14);
            this._sinhrManualNoYesGr1.Name = "_sinhrManualNoYesGr1";
            this._sinhrManualNoYesGr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrManualNoYesGr1.TabIndex = 33;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(6, 57);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(81, 13);
            this.label68.TabIndex = 32;
            this.label68.Text = "U1 нет, U2 нет";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(6, 37);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(87, 13);
            this.label69.TabIndex = 31;
            this.label69.Text = "U1 есть, U2 нет";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(6, 17);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(87, 13);
            this.label70.TabIndex = 30;
            this.label70.Text = "U1 нет, U2 есть";
            // 
            // _sinhrManualUmaxGr1
            // 
            this._sinhrManualUmaxGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrManualUmaxGr1.Location = new System.Drawing.Point(89, 66);
            this._sinhrManualUmaxGr1.Name = "_sinhrManualUmaxGr1";
            this._sinhrManualUmaxGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrManualUmaxGr1.TabIndex = 22;
            this._sinhrManualUmaxGr1.Tag = "3276700";
            this._sinhrManualUmaxGr1.Text = "0";
            this._sinhrManualUmaxGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrManualModeGr1
            // 
            this._sinhrManualModeGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrManualModeGr1.FormattingEnabled = true;
            this._sinhrManualModeGr1.Location = new System.Drawing.Point(89, 16);
            this._sinhrManualModeGr1.Name = "_sinhrManualModeGr1";
            this._sinhrManualModeGr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrManualModeGr1.TabIndex = 18;
            // 
            // _dUmaxManualLabel
            // 
            this._dUmaxManualLabel.AutoSize = true;
            this._dUmaxManualLabel.Location = new System.Drawing.Point(12, 67);
            this._dUmaxManualLabel.Name = "_dUmaxManualLabel";
            this._dUmaxManualLabel.Size = new System.Drawing.Size(56, 13);
            this._dUmaxManualLabel.TabIndex = 11;
            this._dUmaxManualLabel.Text = "dUmax., В";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(12, 18);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(42, 13);
            this.label67.TabIndex = 9;
            this.label67.Text = "Режим";
            // 
            // groupBox34
            // 
            this.groupBox34.Controls.Add(this._discretIn3Cmb);
            this.groupBox34.Controls.Add(this.label85);
            this.groupBox34.Controls.Add(this.label84);
            this.groupBox34.Controls.Add(this.label83);
            this.groupBox34.Controls.Add(this._sinhrF);
            this.groupBox34.Controls.Add(this._discretIn2Cmb);
            this.groupBox34.Controls.Add(this._sinhrKamp);
            this.groupBox34.Controls.Add(this._sinhrTonGr1);
            this.groupBox34.Controls.Add(this._discretIn1Cmb);
            this.groupBox34.Controls.Add(this.label151);
            this.groupBox34.Controls.Add(this._blockSinhCmb);
            this.groupBox34.Controls.Add(this._sinhrTsinhrGr1);
            this.groupBox34.Controls.Add(this.label141);
            this.groupBox34.Controls.Add(this._blockSinhLabel);
            this.groupBox34.Controls.Add(this._sinhrTwaitGr1);
            this.groupBox34.Controls.Add(this.label14);
            this.groupBox34.Controls.Add(this.label15);
            this.groupBox34.Controls.Add(this.label16);
            this.groupBox34.Controls.Add(this._sinhrUmaxNalGr1);
            this.groupBox34.Controls.Add(this._sinhrUminNalGr1);
            this.groupBox34.Controls.Add(this._sinhrUminOtsGr1);
            this.groupBox34.Controls.Add(this._sinhrU2Gr1);
            this.groupBox34.Controls.Add(this._sinhrU1Gr1);
            this.groupBox34.Controls.Add(this.label40);
            this.groupBox34.Controls.Add(this.label57);
            this.groupBox34.Controls.Add(this.label58);
            this.groupBox34.Controls.Add(this.label59);
            this.groupBox34.Controls.Add(this.label60);
            this.groupBox34.Location = new System.Drawing.Point(6, 23);
            this.groupBox34.Name = "groupBox34";
            this.groupBox34.Size = new System.Drawing.Size(286, 303);
            this.groupBox34.TabIndex = 15;
            this.groupBox34.TabStop = false;
            this.groupBox34.Text = "Общие уставки";
            // 
            // _discretIn3Cmb
            // 
            this._discretIn3Cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._discretIn3Cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._discretIn3Cmb.FormattingEnabled = true;
            this._discretIn3Cmb.Location = new System.Drawing.Point(165, 270);
            this._discretIn3Cmb.Name = "_discretIn3Cmb";
            this._discretIn3Cmb.Size = new System.Drawing.Size(105, 21);
            this._discretIn3Cmb.TabIndex = 38;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(12, 273);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(141, 13);
            this.label85.TabIndex = 37;
            this.label85.Text = "Вход ввода U1 нет, U2 нет";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(12, 253);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(147, 13);
            this.label84.TabIndex = 36;
            this.label84.Text = "Вход ввода U1 есть, U2 нет";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(12, 233);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(147, 13);
            this.label83.TabIndex = 35;
            this.label83.Text = "Вход ввода U1 нет, U2 есть";
            // 
            // _sinhrF
            // 
            this._sinhrF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrF.Location = new System.Drawing.Point(165, 191);
            this._sinhrF.Name = "_sinhrF";
            this._sinhrF.Size = new System.Drawing.Size(105, 20);
            this._sinhrF.TabIndex = 30;
            this._sinhrF.Tag = "3276700";
            this._sinhrF.Text = "0";
            this._sinhrF.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _discretIn2Cmb
            // 
            this._discretIn2Cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._discretIn2Cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._discretIn2Cmb.FormattingEnabled = true;
            this._discretIn2Cmb.Location = new System.Drawing.Point(165, 250);
            this._discretIn2Cmb.Name = "_discretIn2Cmb";
            this._discretIn2Cmb.Size = new System.Drawing.Size(105, 21);
            this._discretIn2Cmb.TabIndex = 34;
            // 
            // _sinhrKamp
            // 
            this._sinhrKamp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrKamp.Location = new System.Drawing.Point(165, 172);
            this._sinhrKamp.Name = "_sinhrKamp";
            this._sinhrKamp.Size = new System.Drawing.Size(105, 20);
            this._sinhrKamp.TabIndex = 30;
            this._sinhrKamp.Tag = "3276700";
            this._sinhrKamp.Text = "0";
            this._sinhrKamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrTonGr1
            // 
            this._sinhrTonGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrTonGr1.Location = new System.Drawing.Point(165, 153);
            this._sinhrTonGr1.Name = "_sinhrTonGr1";
            this._sinhrTonGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrTonGr1.TabIndex = 30;
            this._sinhrTonGr1.Tag = "3276700";
            this._sinhrTonGr1.Text = "0";
            this._sinhrTonGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _discretIn1Cmb
            // 
            this._discretIn1Cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._discretIn1Cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._discretIn1Cmb.FormattingEnabled = true;
            this._discretIn1Cmb.Location = new System.Drawing.Point(165, 230);
            this._discretIn1Cmb.Name = "_discretIn1Cmb";
            this._discretIn1Cmb.Size = new System.Drawing.Size(105, 21);
            this._discretIn1Cmb.TabIndex = 33;
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(12, 191);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(76, 13);
            this.label151.TabIndex = 27;
            this.label151.Text = "f(U1;U2), град";
            // 
            // _blockSinhCmb
            // 
            this._blockSinhCmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._blockSinhCmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._blockSinhCmb.FormattingEnabled = true;
            this._blockSinhCmb.Location = new System.Drawing.Point(165, 210);
            this._blockSinhCmb.Name = "_blockSinhCmb";
            this._blockSinhCmb.Size = new System.Drawing.Size(105, 21);
            this._blockSinhCmb.TabIndex = 18;
            // 
            // _sinhrTsinhrGr1
            // 
            this._sinhrTsinhrGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrTsinhrGr1.Location = new System.Drawing.Point(165, 134);
            this._sinhrTsinhrGr1.Name = "_sinhrTsinhrGr1";
            this._sinhrTsinhrGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrTsinhrGr1.TabIndex = 29;
            this._sinhrTsinhrGr1.Tag = "40";
            this._sinhrTsinhrGr1.Text = "20";
            this._sinhrTsinhrGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(12, 172);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(48, 13);
            this.label141.TabIndex = 27;
            this.label141.Text = "Камп, %";
            // 
            // _blockSinhLabel
            // 
            this._blockSinhLabel.AutoSize = true;
            this._blockSinhLabel.Location = new System.Drawing.Point(12, 212);
            this._blockSinhLabel.Name = "_blockSinhLabel";
            this._blockSinhLabel.Size = new System.Drawing.Size(111, 13);
            this._blockSinhLabel.TabIndex = 9;
            this._blockSinhLabel.Text = "Вход блокировки КС";
            // 
            // _sinhrTwaitGr1
            // 
            this._sinhrTwaitGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrTwaitGr1.Location = new System.Drawing.Point(165, 115);
            this._sinhrTwaitGr1.Name = "_sinhrTwaitGr1";
            this._sinhrTwaitGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrTwaitGr1.TabIndex = 28;
            this._sinhrTwaitGr1.Tag = "3276700";
            this._sinhrTwaitGr1.Text = "20";
            this._sinhrTwaitGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 153);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "t вкл, мс";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 134);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 13);
            this.label15.TabIndex = 26;
            this.label15.Text = "tсинхр, мс";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 114);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "tож, мс";
            // 
            // _sinhrUmaxNalGr1
            // 
            this._sinhrUmaxNalGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrUmaxNalGr1.Location = new System.Drawing.Point(165, 96);
            this._sinhrUmaxNalGr1.Name = "_sinhrUmaxNalGr1";
            this._sinhrUmaxNalGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrUmaxNalGr1.TabIndex = 24;
            this._sinhrUmaxNalGr1.Tag = "3276700";
            this._sinhrUmaxNalGr1.Text = "0";
            this._sinhrUmaxNalGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrUminNalGr1
            // 
            this._sinhrUminNalGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrUminNalGr1.Location = new System.Drawing.Point(165, 77);
            this._sinhrUminNalGr1.Name = "_sinhrUminNalGr1";
            this._sinhrUminNalGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrUminNalGr1.TabIndex = 23;
            this._sinhrUminNalGr1.Tag = "40";
            this._sinhrUminNalGr1.Text = "0";
            this._sinhrUminNalGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrUminOtsGr1
            // 
            this._sinhrUminOtsGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrUminOtsGr1.Location = new System.Drawing.Point(165, 58);
            this._sinhrUminOtsGr1.Name = "_sinhrUminOtsGr1";
            this._sinhrUminOtsGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrUminOtsGr1.TabIndex = 22;
            this._sinhrUminOtsGr1.Tag = "3276700";
            this._sinhrUminOtsGr1.Text = "0";
            this._sinhrUminOtsGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrU2Gr1
            // 
            this._sinhrU2Gr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrU2Gr1.FormattingEnabled = true;
            this._sinhrU2Gr1.Location = new System.Drawing.Point(165, 38);
            this._sinhrU2Gr1.Name = "_sinhrU2Gr1";
            this._sinhrU2Gr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrU2Gr1.TabIndex = 19;
            // 
            // _sinhrU1Gr1
            // 
            this._sinhrU1Gr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrU1Gr1.FormattingEnabled = true;
            this._sinhrU1Gr1.Location = new System.Drawing.Point(165, 18);
            this._sinhrU1Gr1.Name = "_sinhrU1Gr1";
            this._sinhrU1Gr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrU1Gr1.TabIndex = 18;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(12, 96);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(75, 13);
            this.label40.TabIndex = 13;
            this.label40.Text = "Umax. нал*, В";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(12, 77);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(72, 13);
            this.label57.TabIndex = 12;
            this.label57.Text = "Umin. нал*, В";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(12, 57);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(71, 13);
            this.label58.TabIndex = 11;
            this.label58.Text = "Umin. отс*, В";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(12, 38);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(34, 13);
            this.label59.TabIndex = 10;
            this.label59.Text = "U2, В";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(12, 18);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(34, 13);
            this.label60.TabIndex = 9;
            this.label60.Text = "U1, В";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(17, 329);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(439, 13);
            this.label30.TabIndex = 27;
            this.label30.Text = "* - данные уставки используются для сравнения с напряжениями U1 и U2*Камп/100";
            // 
            // _tuAndTbTabPage
            // 
            this._tuAndTbTabPage.Controls.Add(this.tabControl100);
            this._tuAndTbTabPage.Location = new System.Drawing.Point(4, 25);
            this._tuAndTbTabPage.Name = "_tuAndTbTabPage";
            this._tuAndTbTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._tuAndTbTabPage.Size = new System.Drawing.Size(958, 443);
            this._tuAndTbTabPage.TabIndex = 22;
            this._tuAndTbTabPage.Text = "ТУ и ТБ";
            this._tuAndTbTabPage.UseVisualStyleBackColor = true;
            // 
            // tabControl100
            // 
            this.tabControl100.Controls.Add(this.tabPage2);
            this.tabControl100.Controls.Add(this.tabPage3);
            this.tabControl100.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl100.Location = new System.Drawing.Point(3, 3);
            this.tabControl100.Name = "tabControl100";
            this.tabControl100.SelectedIndex = 0;
            this.tabControl100.Size = new System.Drawing.Size(952, 437);
            this.tabControl100.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox50);
            this.tabPage2.Controls.Add(this.groupBox30);
            this.tabPage2.Controls.Add(this.groupBox12);
            this.tabPage2.Controls.Add(this.groupBox44);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(944, 411);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "ТУ и ТБ по ДЗ";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox50
            // 
            this.groupBox50.Controls.Add(this.konturFN);
            this.groupBox50.Controls.Add(this.konturFF);
            this.groupBox50.Controls.Add(this.konturFNGroup);
            this.groupBox50.Controls.Add(this.konturFFGroup);
            this.groupBox50.Controls.Add(this.modeDZ);
            this.groupBox50.Controls.Add(this.label187);
            this.groupBox50.Controls.Add(this.inpTSDZ);
            this.groupBox50.Controls.Add(this.label188);
            this.groupBox50.Controls.Add(this.label189);
            this.groupBox50.Controls.Add(this.label190);
            this.groupBox50.Controls.Add(this.label191);
            this.groupBox50.Controls.Add(this.apvDZ);
            this.groupBox50.Controls.Add(this.urovDZ);
            this.groupBox50.Controls.Add(this.OscDZ);
            this.groupBox50.Location = new System.Drawing.Point(6, 6);
            this.groupBox50.Name = "groupBox50";
            this.groupBox50.Size = new System.Drawing.Size(214, 329);
            this.groupBox50.TabIndex = 1;
            this.groupBox50.TabStop = false;
            this.groupBox50.Text = "Общие настройки";
            // 
            // konturFN
            // 
            this.konturFN.AutoSize = true;
            this.konturFN.Location = new System.Drawing.Point(83, 143);
            this.konturFN.Name = "konturFN";
            this.konturFN.Size = new System.Drawing.Size(15, 14);
            this.konturFN.TabIndex = 2;
            this.konturFN.UseVisualStyleBackColor = true;
            this.konturFN.CheckedChanged += new System.EventHandler(this.konturFN_CheckedChanged);
            // 
            // konturFF
            // 
            this.konturFF.AutoSize = true;
            this.konturFF.Location = new System.Drawing.Point(84, 46);
            this.konturFF.Name = "konturFF";
            this.konturFF.Size = new System.Drawing.Size(15, 14);
            this.konturFF.TabIndex = 2;
            this.konturFF.UseVisualStyleBackColor = true;
            this.konturFF.CheckedChanged += new System.EventHandler(this.konturFF_CheckedChanged);
            // 
            // konturFNGroup
            // 
            this.konturFNGroup.Controls.Add(this.abbrevDZFN);
            this.konturFNGroup.Controls.Add(this.label50);
            this.konturFNGroup.Controls.Add(this.label86);
            this.konturFNGroup.Controls.Add(this.label183);
            this.konturFNGroup.Controls.Add(this.reverseDZFN);
            this.konturFNGroup.Controls.Add(this.extendDZFN);
            this.konturFNGroup.Enabled = false;
            this.konturFNGroup.Location = new System.Drawing.Point(6, 144);
            this.konturFNGroup.Name = "konturFNGroup";
            this.konturFNGroup.Size = new System.Drawing.Size(202, 92);
            this.konturFNGroup.TabIndex = 2;
            this.konturFNGroup.TabStop = false;
            this.konturFNGroup.Text = "Контур ФN";
            // 
            // abbrevDZFN
            // 
            this.abbrevDZFN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.abbrevDZFN.FormattingEnabled = true;
            this.abbrevDZFN.Location = new System.Drawing.Point(86, 19);
            this.abbrevDZFN.Name = "abbrevDZFN";
            this.abbrevDZFN.Size = new System.Drawing.Size(100, 21);
            this.abbrevDZFN.TabIndex = 0;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(6, 42);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(59, 13);
            this.label50.TabIndex = 1;
            this.label50.Text = "Расш. ФN";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(6, 22);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(57, 13);
            this.label86.TabIndex = 1;
            this.label86.Text = "Сокр. ФN";
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Location = new System.Drawing.Point(6, 62);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(63, 13);
            this.label183.TabIndex = 1;
            this.label183.Text = "Обрат. ФN";
            // 
            // reverseDZFN
            // 
            this.reverseDZFN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.reverseDZFN.FormattingEnabled = true;
            this.reverseDZFN.Location = new System.Drawing.Point(86, 59);
            this.reverseDZFN.Name = "reverseDZFN";
            this.reverseDZFN.Size = new System.Drawing.Size(100, 21);
            this.reverseDZFN.TabIndex = 0;
            // 
            // extendDZFN
            // 
            this.extendDZFN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.extendDZFN.FormattingEnabled = true;
            this.extendDZFN.Location = new System.Drawing.Point(86, 39);
            this.extendDZFN.Name = "extendDZFN";
            this.extendDZFN.Size = new System.Drawing.Size(100, 21);
            this.extendDZFN.TabIndex = 0;
            // 
            // konturFFGroup
            // 
            this.konturFFGroup.Controls.Add(this.abbrevDZFF);
            this.konturFFGroup.Controls.Add(this.label184);
            this.konturFFGroup.Controls.Add(this.label185);
            this.konturFFGroup.Controls.Add(this.label186);
            this.konturFFGroup.Controls.Add(this.reverseDZFF);
            this.konturFFGroup.Controls.Add(this.extendDZFF);
            this.konturFFGroup.Enabled = false;
            this.konturFFGroup.Location = new System.Drawing.Point(6, 46);
            this.konturFFGroup.Name = "konturFFGroup";
            this.konturFFGroup.Size = new System.Drawing.Size(202, 92);
            this.konturFFGroup.TabIndex = 2;
            this.konturFFGroup.TabStop = false;
            this.konturFFGroup.Text = "Контур ФФ";
            // 
            // abbrevDZFF
            // 
            this.abbrevDZFF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.abbrevDZFF.FormattingEnabled = true;
            this.abbrevDZFF.Location = new System.Drawing.Point(86, 19);
            this.abbrevDZFF.Name = "abbrevDZFF";
            this.abbrevDZFF.Size = new System.Drawing.Size(100, 21);
            this.abbrevDZFF.TabIndex = 0;
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Location = new System.Drawing.Point(6, 42);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(62, 13);
            this.label184.TabIndex = 1;
            this.label184.Text = "Расш. ФФ";
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(6, 22);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(60, 13);
            this.label185.TabIndex = 1;
            this.label185.Text = "Сокр. ФФ";
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(6, 62);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(66, 13);
            this.label186.TabIndex = 1;
            this.label186.Text = "Обрат. ФФ";
            // 
            // reverseDZFF
            // 
            this.reverseDZFF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.reverseDZFF.FormattingEnabled = true;
            this.reverseDZFF.Location = new System.Drawing.Point(86, 59);
            this.reverseDZFF.Name = "reverseDZFF";
            this.reverseDZFF.Size = new System.Drawing.Size(100, 21);
            this.reverseDZFF.TabIndex = 0;
            // 
            // extendDZFF
            // 
            this.extendDZFF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.extendDZFF.FormattingEnabled = true;
            this.extendDZFF.Location = new System.Drawing.Point(86, 39);
            this.extendDZFF.Name = "extendDZFF";
            this.extendDZFF.Size = new System.Drawing.Size(100, 21);
            this.extendDZFF.TabIndex = 0;
            // 
            // modeDZ
            // 
            this.modeDZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.modeDZ.FormattingEnabled = true;
            this.modeDZ.Location = new System.Drawing.Point(92, 19);
            this.modeDZ.Name = "modeDZ";
            this.modeDZ.Size = new System.Drawing.Size(100, 21);
            this.modeDZ.TabIndex = 0;
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Location = new System.Drawing.Point(12, 305);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(29, 13);
            this.label187.TabIndex = 1;
            this.label187.Text = "АПВ";
            // 
            // inpTSDZ
            // 
            this.inpTSDZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.inpTSDZ.FormattingEnabled = true;
            this.inpTSDZ.Location = new System.Drawing.Point(92, 242);
            this.inpTSDZ.Name = "inpTSDZ";
            this.inpTSDZ.Size = new System.Drawing.Size(100, 21);
            this.inpTSDZ.TabIndex = 0;
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Location = new System.Drawing.Point(12, 285);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(37, 13);
            this.label188.TabIndex = 1;
            this.label188.Text = "УРОВ";
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Location = new System.Drawing.Point(12, 21);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(42, 13);
            this.label189.TabIndex = 1;
            this.label189.Text = "Режим";
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Location = new System.Drawing.Point(12, 265);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(30, 13);
            this.label190.TabIndex = 1;
            this.label190.Text = "Осц.";
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.Location = new System.Drawing.Point(12, 245);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(21, 13);
            this.label191.TabIndex = 1;
            this.label191.Text = "ТС";
            // 
            // apvDZ
            // 
            this.apvDZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apvDZ.FormattingEnabled = true;
            this.apvDZ.Location = new System.Drawing.Point(92, 302);
            this.apvDZ.Name = "apvDZ";
            this.apvDZ.Size = new System.Drawing.Size(100, 21);
            this.apvDZ.TabIndex = 0;
            // 
            // urovDZ
            // 
            this.urovDZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.urovDZ.FormattingEnabled = true;
            this.urovDZ.Location = new System.Drawing.Point(92, 282);
            this.urovDZ.Name = "urovDZ";
            this.urovDZ.Size = new System.Drawing.Size(100, 21);
            this.urovDZ.TabIndex = 0;
            // 
            // OscDZ
            // 
            this.OscDZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OscDZ.FormattingEnabled = true;
            this.OscDZ.Location = new System.Drawing.Point(92, 262);
            this.OscDZ.Name = "OscDZ";
            this.OscDZ.Size = new System.Drawing.Size(100, 21);
            this.OscDZ.TabIndex = 0;
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.zoneDZ);
            this.groupBox30.Controls.Add(this.tficsDZ);
            this.groupBox30.Controls.Add(this.treverseDZ);
            this.groupBox30.Controls.Add(this.label192);
            this.groupBox30.Controls.Add(this.label194);
            this.groupBox30.Controls.Add(this.label195);
            this.groupBox30.Controls.Add(this.label196);
            this.groupBox30.Controls.Add(this.label197);
            this.groupBox30.Controls.Add(this.blockReverseDZ);
            this.groupBox30.Controls.Add(this.modeReverseDZ);
            this.groupBox30.Location = new System.Drawing.Point(440, 133);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(254, 122);
            this.groupBox30.TabIndex = 0;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "Блокировка по реверсу";
            // 
            // zoneDZ
            // 
            this.zoneDZ.AutoSize = true;
            this.zoneDZ.Location = new System.Drawing.Point(121, 80);
            this.zoneDZ.Name = "zoneDZ";
            this.zoneDZ.Size = new System.Drawing.Size(15, 14);
            this.zoneDZ.TabIndex = 7;
            this.zoneDZ.UseVisualStyleBackColor = true;
            // 
            // tficsDZ
            // 
            this.tficsDZ.Location = new System.Drawing.Point(121, 57);
            this.tficsDZ.Name = "tficsDZ";
            this.tficsDZ.Size = new System.Drawing.Size(123, 20);
            this.tficsDZ.TabIndex = 6;
            this.tficsDZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // treverseDZ
            // 
            this.treverseDZ.Location = new System.Drawing.Point(121, 38);
            this.treverseDZ.Name = "treverseDZ";
            this.treverseDZ.Size = new System.Drawing.Size(123, 20);
            this.treverseDZ.TabIndex = 5;
            this.treverseDZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Location = new System.Drawing.Point(6, 61);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(56, 13);
            this.label192.TabIndex = 1;
            this.label192.Text = "tфикс, мс";
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Location = new System.Drawing.Point(6, 100);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(85, 13);
            this.label194.TabIndex = 1;
            this.label194.Text = "Сигнал блок-ки";
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Location = new System.Drawing.Point(6, 80);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(62, 13);
            this.label195.TabIndex = 1;
            this.label195.Text = "Сокр. зона";
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Location = new System.Drawing.Point(6, 42);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(56, 13);
            this.label196.TabIndex = 1;
            this.label196.Text = "tулав., мс";
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Location = new System.Drawing.Point(6, 22);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(42, 13);
            this.label197.TabIndex = 1;
            this.label197.Text = "Режим";
            // 
            // blockReverseDZ
            // 
            this.blockReverseDZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.blockReverseDZ.FormattingEnabled = true;
            this.blockReverseDZ.Location = new System.Drawing.Point(121, 96);
            this.blockReverseDZ.Name = "blockReverseDZ";
            this.blockReverseDZ.Size = new System.Drawing.Size(123, 21);
            this.blockReverseDZ.TabIndex = 0;
            // 
            // modeReverseDZ
            // 
            this.modeReverseDZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.modeReverseDZ.FormattingEnabled = true;
            this.modeReverseDZ.Location = new System.Drawing.Point(121, 18);
            this.modeReverseDZ.Name = "modeReverseDZ";
            this.modeReverseDZ.Size = new System.Drawing.Size(123, 21);
            this.modeReverseDZ.TabIndex = 0;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.uminDZ);
            this.groupBox12.Controls.Add(this.tsrabTSDZ);
            this.groupBox12.Controls.Add(this.label198);
            this.groupBox12.Controls.Add(this.label199);
            this.groupBox12.Controls.Add(this.label200);
            this.groupBox12.Controls.Add(this.label201);
            this.groupBox12.Controls.Add(this.label202);
            this.groupBox12.Controls.Add(this.blockTnDZ);
            this.groupBox12.Controls.Add(this.blockEchoDZ);
            this.groupBox12.Controls.Add(this.modeEchoDZ);
            this.groupBox12.Location = new System.Drawing.Point(440, 6);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(254, 121);
            this.groupBox12.TabIndex = 0;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Логика откл. КСП";
            // 
            // uminDZ
            // 
            this.uminDZ.Location = new System.Drawing.Point(121, 57);
            this.uminDZ.Name = "uminDZ";
            this.uminDZ.Size = new System.Drawing.Size(123, 20);
            this.uminDZ.TabIndex = 6;
            this.uminDZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tsrabTSDZ
            // 
            this.tsrabTSDZ.Location = new System.Drawing.Point(121, 38);
            this.tsrabTSDZ.Name = "tsrabTSDZ";
            this.tsrabTSDZ.Size = new System.Drawing.Size(123, 20);
            this.tsrabTSDZ.TabIndex = 5;
            this.tsrabTSDZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Location = new System.Drawing.Point(6, 61);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(44, 13);
            this.label198.TabIndex = 1;
            this.label198.Text = "Umin, В";
            // 
            // label199
            // 
            this.label199.AutoSize = true;
            this.label199.Location = new System.Drawing.Point(6, 100);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(109, 13);
            this.label199.TabIndex = 1;
            this.label199.Text = "Блок. от неиспр. ТН";
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Location = new System.Drawing.Point(6, 80);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(85, 13);
            this.label200.TabIndex = 1;
            this.label200.Text = "Сигнал блок-ки";
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Location = new System.Drawing.Point(6, 42);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(62, 13);
            this.label201.TabIndex = 1;
            this.label201.Text = "tср. ТС, мс";
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Location = new System.Drawing.Point(6, 22);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(42, 13);
            this.label202.TabIndex = 1;
            this.label202.Text = "Режим";
            // 
            // blockTnDZ
            // 
            this.blockTnDZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.blockTnDZ.FormattingEnabled = true;
            this.blockTnDZ.Location = new System.Drawing.Point(121, 96);
            this.blockTnDZ.Name = "blockTnDZ";
            this.blockTnDZ.Size = new System.Drawing.Size(123, 21);
            this.blockTnDZ.TabIndex = 0;
            // 
            // blockEchoDZ
            // 
            this.blockEchoDZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.blockEchoDZ.FormattingEnabled = true;
            this.blockEchoDZ.Location = new System.Drawing.Point(121, 76);
            this.blockEchoDZ.Name = "blockEchoDZ";
            this.blockEchoDZ.Size = new System.Drawing.Size(123, 21);
            this.blockEchoDZ.TabIndex = 0;
            // 
            // modeEchoDZ
            // 
            this.modeEchoDZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.modeEchoDZ.FormattingEnabled = true;
            this.modeEchoDZ.Location = new System.Drawing.Point(121, 18);
            this.modeEchoDZ.Name = "modeEchoDZ";
            this.modeEchoDZ.Size = new System.Drawing.Size(123, 21);
            this.modeEchoDZ.TabIndex = 0;
            // 
            // groupBox44
            // 
            this.groupBox44.Controls.Add(this.toffTBDZ);
            this.groupBox44.Controls.Add(this.toffDZ);
            this.groupBox44.Controls.Add(this.tminImpDZ);
            this.groupBox44.Controls.Add(this.tsrabDebDZ);
            this.groupBox44.Controls.Add(this.tvzTSDZ);
            this.groupBox44.Controls.Add(this.label203);
            this.groupBox44.Controls.Add(this.label204);
            this.groupBox44.Controls.Add(this.label205);
            this.groupBox44.Controls.Add(this.label206);
            this.groupBox44.Controls.Add(this.label207);
            this.groupBox44.Controls.Add(this.label208);
            this.groupBox44.Controls.Add(this.label209);
            this.groupBox44.Controls.Add(this.label210);
            this.groupBox44.Controls.Add(this.label211);
            this.groupBox44.Controls.Add(this.blockSwithDZ);
            this.groupBox44.Controls.Add(this.deblockDZ);
            this.groupBox44.Controls.Add(this.controlDZ);
            this.groupBox44.Controls.Add(this.blockTSDZ);
            this.groupBox44.Location = new System.Drawing.Point(226, 6);
            this.groupBox44.Name = "groupBox44";
            this.groupBox44.Size = new System.Drawing.Size(208, 204);
            this.groupBox44.TabIndex = 0;
            this.groupBox44.TabStop = false;
            this.groupBox44.Text = "Конфигурация ТУ ТБ";
            // 
            // toffTBDZ
            // 
            this.toffTBDZ.Location = new System.Drawing.Point(99, 135);
            this.toffTBDZ.Name = "toffTBDZ";
            this.toffTBDZ.Size = new System.Drawing.Size(100, 20);
            this.toffTBDZ.TabIndex = 6;
            this.toffTBDZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // toffDZ
            // 
            this.toffDZ.Location = new System.Drawing.Point(99, 116);
            this.toffDZ.Name = "toffDZ";
            this.toffDZ.Size = new System.Drawing.Size(100, 20);
            this.toffDZ.TabIndex = 6;
            this.toffDZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tminImpDZ
            // 
            this.tminImpDZ.Location = new System.Drawing.Point(99, 97);
            this.tminImpDZ.Name = "tminImpDZ";
            this.tminImpDZ.Size = new System.Drawing.Size(100, 20);
            this.tminImpDZ.TabIndex = 6;
            this.tminImpDZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tsrabDebDZ
            // 
            this.tsrabDebDZ.Location = new System.Drawing.Point(99, 78);
            this.tsrabDebDZ.Name = "tsrabDebDZ";
            this.tsrabDebDZ.Size = new System.Drawing.Size(100, 20);
            this.tsrabDebDZ.TabIndex = 6;
            this.tsrabDebDZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tvzTSDZ
            // 
            this.tvzTSDZ.Location = new System.Drawing.Point(99, 19);
            this.tvzTSDZ.Name = "tvzTSDZ";
            this.tvzTSDZ.Size = new System.Drawing.Size(100, 20);
            this.tvzTSDZ.TabIndex = 5;
            this.tvzTSDZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Location = new System.Drawing.Point(10, 157);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(58, 13);
            this.label203.TabIndex = 1;
            this.label203.Text = "Блок. отк.";
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Location = new System.Drawing.Point(10, 138);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(82, 13);
            this.label204.TabIndex = 1;
            this.label204.Text = "tотк. по ТБ, мс";
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Location = new System.Drawing.Point(10, 100);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(76, 13);
            this.label205.TabIndex = 1;
            this.label205.Text = "tмин.имп., мс";
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Location = new System.Drawing.Point(10, 119);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(56, 13);
            this.label206.TabIndex = 1;
            this.label206.Text = "tоткл., мс";
            // 
            // label207
            // 
            this.label207.AutoSize = true;
            this.label207.Location = new System.Drawing.Point(10, 81);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(66, 13);
            this.label207.TabIndex = 1;
            this.label207.Text = "tср.деб., мс";
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Location = new System.Drawing.Point(10, 61);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(55, 13);
            this.label208.TabIndex = 1;
            this.label208.Text = "Контроль";
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.Location = new System.Drawing.Point(10, 41);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(46, 13);
            this.label209.TabIndex = 1;
            this.label209.Text = "Деблок";
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Location = new System.Drawing.Point(10, 22);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(62, 13);
            this.label210.TabIndex = 1;
            this.label210.Text = "tвз. ТС, мс";
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.Location = new System.Drawing.Point(10, 177);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(52, 13);
            this.label211.TabIndex = 1;
            this.label211.Text = "Блок. ТС";
            // 
            // blockSwithDZ
            // 
            this.blockSwithDZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.blockSwithDZ.FormattingEnabled = true;
            this.blockSwithDZ.Location = new System.Drawing.Point(99, 154);
            this.blockSwithDZ.Name = "blockSwithDZ";
            this.blockSwithDZ.Size = new System.Drawing.Size(100, 21);
            this.blockSwithDZ.TabIndex = 0;
            // 
            // deblockDZ
            // 
            this.deblockDZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.deblockDZ.FormattingEnabled = true;
            this.deblockDZ.Location = new System.Drawing.Point(99, 38);
            this.deblockDZ.Name = "deblockDZ";
            this.deblockDZ.Size = new System.Drawing.Size(100, 21);
            this.deblockDZ.TabIndex = 0;
            // 
            // controlDZ
            // 
            this.controlDZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.controlDZ.FormattingEnabled = true;
            this.controlDZ.Location = new System.Drawing.Point(99, 58);
            this.controlDZ.Name = "controlDZ";
            this.controlDZ.Size = new System.Drawing.Size(100, 21);
            this.controlDZ.TabIndex = 0;
            // 
            // blockTSDZ
            // 
            this.blockTSDZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.blockTSDZ.FormattingEnabled = true;
            this.blockTSDZ.Location = new System.Drawing.Point(99, 174);
            this.blockTSDZ.Name = "blockTSDZ";
            this.blockTSDZ.Size = new System.Drawing.Size(100, 21);
            this.blockTSDZ.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox47);
            this.tabPage3.Controls.Add(this.groupBox53);
            this.tabPage3.Controls.Add(this.groupBox46);
            this.tabPage3.Controls.Add(this.groupBox58);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(944, 411);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "ТУ и ТБ по ТЗНП";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox47
            // 
            this.groupBox47.Controls.Add(this.groupBox52);
            this.groupBox47.Controls.Add(this.modeDZNP);
            this.groupBox47.Controls.Add(this.label230);
            this.groupBox47.Controls.Add(this.inpTSDZNP);
            this.groupBox47.Controls.Add(this.label231);
            this.groupBox47.Controls.Add(this.label232);
            this.groupBox47.Controls.Add(this.label233);
            this.groupBox47.Controls.Add(this.label234);
            this.groupBox47.Controls.Add(this.apvDZNP);
            this.groupBox47.Controls.Add(this.urovDZNP);
            this.groupBox47.Controls.Add(this.oscDZNP);
            this.groupBox47.Controls.Add(this.directionDZNP);
            this.groupBox47.Controls.Add(this.label235);
            this.groupBox47.Location = new System.Drawing.Point(6, 6);
            this.groupBox47.Name = "groupBox47";
            this.groupBox47.Size = new System.Drawing.Size(214, 258);
            this.groupBox47.TabIndex = 4;
            this.groupBox47.TabStop = false;
            this.groupBox47.Text = "Общие настройки";
            // 
            // groupBox52
            // 
            this.groupBox52.Controls.Add(this.abbrevDZNP);
            this.groupBox52.Controls.Add(this.label227);
            this.groupBox52.Controls.Add(this.label228);
            this.groupBox52.Controls.Add(this.label229);
            this.groupBox52.Controls.Add(this.reverseDZNP);
            this.groupBox52.Controls.Add(this.extendDZNP);
            this.groupBox52.Location = new System.Drawing.Point(6, 46);
            this.groupBox52.Name = "groupBox52";
            this.groupBox52.Size = new System.Drawing.Size(202, 92);
            this.groupBox52.TabIndex = 2;
            this.groupBox52.TabStop = false;
            this.groupBox52.Text = "Контур";
            // 
            // abbrevDZNP
            // 
            this.abbrevDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.abbrevDZNP.FormattingEnabled = true;
            this.abbrevDZNP.Location = new System.Drawing.Point(86, 19);
            this.abbrevDZNP.Name = "abbrevDZNP";
            this.abbrevDZNP.Size = new System.Drawing.Size(100, 21);
            this.abbrevDZNP.TabIndex = 0;
            // 
            // label227
            // 
            this.label227.AutoSize = true;
            this.label227.Location = new System.Drawing.Point(6, 42);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(37, 13);
            this.label227.TabIndex = 1;
            this.label227.Text = "Расш.";
            // 
            // label228
            // 
            this.label228.AutoSize = true;
            this.label228.Location = new System.Drawing.Point(6, 22);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(35, 13);
            this.label228.TabIndex = 1;
            this.label228.Text = "Сокр.";
            // 
            // label229
            // 
            this.label229.AutoSize = true;
            this.label229.Location = new System.Drawing.Point(6, 62);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(41, 13);
            this.label229.TabIndex = 1;
            this.label229.Text = "Обрат.";
            // 
            // reverseDZNP
            // 
            this.reverseDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.reverseDZNP.FormattingEnabled = true;
            this.reverseDZNP.Location = new System.Drawing.Point(86, 59);
            this.reverseDZNP.Name = "reverseDZNP";
            this.reverseDZNP.Size = new System.Drawing.Size(100, 21);
            this.reverseDZNP.TabIndex = 0;
            // 
            // extendDZNP
            // 
            this.extendDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.extendDZNP.FormattingEnabled = true;
            this.extendDZNP.Location = new System.Drawing.Point(86, 39);
            this.extendDZNP.Name = "extendDZNP";
            this.extendDZNP.Size = new System.Drawing.Size(100, 21);
            this.extendDZNP.TabIndex = 0;
            // 
            // modeDZNP
            // 
            this.modeDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.modeDZNP.FormattingEnabled = true;
            this.modeDZNP.Location = new System.Drawing.Point(92, 19);
            this.modeDZNP.Name = "modeDZNP";
            this.modeDZNP.Size = new System.Drawing.Size(100, 21);
            this.modeDZNP.TabIndex = 0;
            // 
            // label230
            // 
            this.label230.AutoSize = true;
            this.label230.Location = new System.Drawing.Point(12, 232);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(29, 13);
            this.label230.TabIndex = 1;
            this.label230.Text = "АПВ";
            // 
            // inpTSDZNP
            // 
            this.inpTSDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.inpTSDZNP.FormattingEnabled = true;
            this.inpTSDZNP.Location = new System.Drawing.Point(92, 169);
            this.inpTSDZNP.Name = "inpTSDZNP";
            this.inpTSDZNP.Size = new System.Drawing.Size(100, 21);
            this.inpTSDZNP.TabIndex = 0;
            // 
            // label231
            // 
            this.label231.AutoSize = true;
            this.label231.Location = new System.Drawing.Point(12, 212);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(37, 13);
            this.label231.TabIndex = 1;
            this.label231.Text = "УРОВ";
            // 
            // label232
            // 
            this.label232.AutoSize = true;
            this.label232.Location = new System.Drawing.Point(12, 21);
            this.label232.Name = "label232";
            this.label232.Size = new System.Drawing.Size(42, 13);
            this.label232.TabIndex = 1;
            this.label232.Text = "Режим";
            // 
            // label233
            // 
            this.label233.AutoSize = true;
            this.label233.Location = new System.Drawing.Point(12, 192);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(30, 13);
            this.label233.TabIndex = 1;
            this.label233.Text = "Осц.";
            // 
            // label234
            // 
            this.label234.AutoSize = true;
            this.label234.Location = new System.Drawing.Point(12, 172);
            this.label234.Name = "label234";
            this.label234.Size = new System.Drawing.Size(21, 13);
            this.label234.TabIndex = 1;
            this.label234.Text = "ТС";
            // 
            // apvDZNP
            // 
            this.apvDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apvDZNP.FormattingEnabled = true;
            this.apvDZNP.Location = new System.Drawing.Point(92, 229);
            this.apvDZNP.Name = "apvDZNP";
            this.apvDZNP.Size = new System.Drawing.Size(100, 21);
            this.apvDZNP.TabIndex = 0;
            // 
            // urovDZNP
            // 
            this.urovDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.urovDZNP.FormattingEnabled = true;
            this.urovDZNP.Location = new System.Drawing.Point(92, 209);
            this.urovDZNP.Name = "urovDZNP";
            this.urovDZNP.Size = new System.Drawing.Size(100, 21);
            this.urovDZNP.TabIndex = 0;
            // 
            // oscDZNP
            // 
            this.oscDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.oscDZNP.FormattingEnabled = true;
            this.oscDZNP.Location = new System.Drawing.Point(92, 189);
            this.oscDZNP.Name = "oscDZNP";
            this.oscDZNP.Size = new System.Drawing.Size(100, 21);
            this.oscDZNP.TabIndex = 0;
            // 
            // directionDZNP
            // 
            this.directionDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.directionDZNP.FormattingEnabled = true;
            this.directionDZNP.Location = new System.Drawing.Point(92, 149);
            this.directionDZNP.Name = "directionDZNP";
            this.directionDZNP.Size = new System.Drawing.Size(100, 21);
            this.directionDZNP.TabIndex = 0;
            // 
            // label235
            // 
            this.label235.AutoSize = true;
            this.label235.Location = new System.Drawing.Point(12, 152);
            this.label235.Name = "label235";
            this.label235.Size = new System.Drawing.Size(75, 13);
            this.label235.TabIndex = 1;
            this.label235.Text = "Направление";
            // 
            // groupBox53
            // 
            this.groupBox53.Controls.Add(this.toffTBDZNP);
            this.groupBox53.Controls.Add(this.toffDZNP);
            this.groupBox53.Controls.Add(this.tminImpDZNP);
            this.groupBox53.Controls.Add(this.tsrabDebDZNP);
            this.groupBox53.Controls.Add(this.tvzTSDZNP);
            this.groupBox53.Controls.Add(this.label236);
            this.groupBox53.Controls.Add(this.label237);
            this.groupBox53.Controls.Add(this.label238);
            this.groupBox53.Controls.Add(this.label239);
            this.groupBox53.Controls.Add(this.label240);
            this.groupBox53.Controls.Add(this.label241);
            this.groupBox53.Controls.Add(this.label242);
            this.groupBox53.Controls.Add(this.label243);
            this.groupBox53.Controls.Add(this.label244);
            this.groupBox53.Controls.Add(this.blockSwitchDZNP);
            this.groupBox53.Controls.Add(this.deblockDZNP);
            this.groupBox53.Controls.Add(this.controlDZNP);
            this.groupBox53.Controls.Add(this.blockTSDZNP);
            this.groupBox53.Location = new System.Drawing.Point(226, 6);
            this.groupBox53.Name = "groupBox53";
            this.groupBox53.Size = new System.Drawing.Size(208, 220);
            this.groupBox53.TabIndex = 3;
            this.groupBox53.TabStop = false;
            this.groupBox53.Text = "Конфигурация ТУ ТБ";
            // 
            // toffTBDZNP
            // 
            this.toffTBDZNP.Location = new System.Drawing.Point(99, 135);
            this.toffTBDZNP.Name = "toffTBDZNP";
            this.toffTBDZNP.Size = new System.Drawing.Size(100, 20);
            this.toffTBDZNP.TabIndex = 6;
            this.toffTBDZNP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // toffDZNP
            // 
            this.toffDZNP.Location = new System.Drawing.Point(99, 116);
            this.toffDZNP.Name = "toffDZNP";
            this.toffDZNP.Size = new System.Drawing.Size(100, 20);
            this.toffDZNP.TabIndex = 6;
            this.toffDZNP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tminImpDZNP
            // 
            this.tminImpDZNP.Location = new System.Drawing.Point(99, 97);
            this.tminImpDZNP.Name = "tminImpDZNP";
            this.tminImpDZNP.Size = new System.Drawing.Size(100, 20);
            this.tminImpDZNP.TabIndex = 6;
            this.tminImpDZNP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tsrabDebDZNP
            // 
            this.tsrabDebDZNP.Location = new System.Drawing.Point(99, 78);
            this.tsrabDebDZNP.Name = "tsrabDebDZNP";
            this.tsrabDebDZNP.Size = new System.Drawing.Size(100, 20);
            this.tsrabDebDZNP.TabIndex = 6;
            this.tsrabDebDZNP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tvzTSDZNP
            // 
            this.tvzTSDZNP.Location = new System.Drawing.Point(99, 19);
            this.tvzTSDZNP.Name = "tvzTSDZNP";
            this.tvzTSDZNP.Size = new System.Drawing.Size(100, 20);
            this.tvzTSDZNP.TabIndex = 5;
            this.tvzTSDZNP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label236
            // 
            this.label236.AutoSize = true;
            this.label236.Location = new System.Drawing.Point(10, 157);
            this.label236.Name = "label236";
            this.label236.Size = new System.Drawing.Size(58, 13);
            this.label236.TabIndex = 1;
            this.label236.Text = "Блок. отк.";
            // 
            // label237
            // 
            this.label237.AutoSize = true;
            this.label237.Location = new System.Drawing.Point(10, 138);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(82, 13);
            this.label237.TabIndex = 1;
            this.label237.Text = "tотк. по ТБ, мс";
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.Location = new System.Drawing.Point(10, 100);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(76, 13);
            this.label238.TabIndex = 1;
            this.label238.Text = "tмин.имп., мс";
            // 
            // label239
            // 
            this.label239.AutoSize = true;
            this.label239.Location = new System.Drawing.Point(10, 119);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(56, 13);
            this.label239.TabIndex = 1;
            this.label239.Text = "tоткл., мс";
            // 
            // label240
            // 
            this.label240.AutoSize = true;
            this.label240.Location = new System.Drawing.Point(10, 81);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(66, 13);
            this.label240.TabIndex = 1;
            this.label240.Text = "tср.деб., мс";
            // 
            // label241
            // 
            this.label241.AutoSize = true;
            this.label241.Location = new System.Drawing.Point(10, 61);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(55, 13);
            this.label241.TabIndex = 1;
            this.label241.Text = "Контроль";
            // 
            // label242
            // 
            this.label242.AutoSize = true;
            this.label242.Location = new System.Drawing.Point(10, 41);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(46, 13);
            this.label242.TabIndex = 1;
            this.label242.Text = "Деблок";
            // 
            // label243
            // 
            this.label243.AutoSize = true;
            this.label243.Location = new System.Drawing.Point(10, 22);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(62, 13);
            this.label243.TabIndex = 1;
            this.label243.Text = "tвз. ТС, мс";
            // 
            // label244
            // 
            this.label244.AutoSize = true;
            this.label244.Location = new System.Drawing.Point(10, 177);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(52, 13);
            this.label244.TabIndex = 1;
            this.label244.Text = "Блок. ТС";
            // 
            // blockSwitchDZNP
            // 
            this.blockSwitchDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.blockSwitchDZNP.FormattingEnabled = true;
            this.blockSwitchDZNP.Location = new System.Drawing.Point(99, 154);
            this.blockSwitchDZNP.Name = "blockSwitchDZNP";
            this.blockSwitchDZNP.Size = new System.Drawing.Size(100, 21);
            this.blockSwitchDZNP.TabIndex = 0;
            // 
            // deblockDZNP
            // 
            this.deblockDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.deblockDZNP.FormattingEnabled = true;
            this.deblockDZNP.Location = new System.Drawing.Point(99, 38);
            this.deblockDZNP.Name = "deblockDZNP";
            this.deblockDZNP.Size = new System.Drawing.Size(100, 21);
            this.deblockDZNP.TabIndex = 0;
            // 
            // controlDZNP
            // 
            this.controlDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.controlDZNP.FormattingEnabled = true;
            this.controlDZNP.Location = new System.Drawing.Point(99, 58);
            this.controlDZNP.Name = "controlDZNP";
            this.controlDZNP.Size = new System.Drawing.Size(100, 21);
            this.controlDZNP.TabIndex = 0;
            // 
            // blockTSDZNP
            // 
            this.blockTSDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.blockTSDZNP.FormattingEnabled = true;
            this.blockTSDZNP.Location = new System.Drawing.Point(99, 174);
            this.blockTSDZNP.Name = "blockTSDZNP";
            this.blockTSDZNP.Size = new System.Drawing.Size(100, 21);
            this.blockTSDZNP.TabIndex = 0;
            // 
            // groupBox46
            // 
            this.groupBox46.Controls.Add(this.zoneDZNP);
            this.groupBox46.Controls.Add(this.tficsDZNP);
            this.groupBox46.Controls.Add(this.treverseDZNP);
            this.groupBox46.Controls.Add(this.label245);
            this.groupBox46.Controls.Add(this.label246);
            this.groupBox46.Controls.Add(this.label247);
            this.groupBox46.Controls.Add(this.label248);
            this.groupBox46.Controls.Add(this.label249);
            this.groupBox46.Controls.Add(this.blockReverseDZNP);
            this.groupBox46.Controls.Add(this.modeReverseDZNP);
            this.groupBox46.Location = new System.Drawing.Point(440, 133);
            this.groupBox46.Name = "groupBox46";
            this.groupBox46.Size = new System.Drawing.Size(254, 122);
            this.groupBox46.TabIndex = 1;
            this.groupBox46.TabStop = false;
            this.groupBox46.Text = "Блокировка по реверсу";
            // 
            // zoneDZNP
            // 
            this.zoneDZNP.AutoSize = true;
            this.zoneDZNP.Location = new System.Drawing.Point(121, 80);
            this.zoneDZNP.Name = "zoneDZNP";
            this.zoneDZNP.Size = new System.Drawing.Size(15, 14);
            this.zoneDZNP.TabIndex = 8;
            this.zoneDZNP.UseVisualStyleBackColor = true;
            // 
            // tficsDZNP
            // 
            this.tficsDZNP.Location = new System.Drawing.Point(121, 57);
            this.tficsDZNP.Name = "tficsDZNP";
            this.tficsDZNP.Size = new System.Drawing.Size(123, 20);
            this.tficsDZNP.TabIndex = 6;
            this.tficsDZNP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // treverseDZNP
            // 
            this.treverseDZNP.Location = new System.Drawing.Point(121, 38);
            this.treverseDZNP.Name = "treverseDZNP";
            this.treverseDZNP.Size = new System.Drawing.Size(123, 20);
            this.treverseDZNP.TabIndex = 5;
            this.treverseDZNP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.Location = new System.Drawing.Point(6, 61);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(56, 13);
            this.label245.TabIndex = 1;
            this.label245.Text = "tфикс, мс";
            // 
            // label246
            // 
            this.label246.AutoSize = true;
            this.label246.Location = new System.Drawing.Point(6, 100);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(85, 13);
            this.label246.TabIndex = 1;
            this.label246.Text = "Сигнал блок-ки";
            // 
            // label247
            // 
            this.label247.AutoSize = true;
            this.label247.Location = new System.Drawing.Point(6, 80);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(62, 13);
            this.label247.TabIndex = 1;
            this.label247.Text = "Сокр. зона";
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.Location = new System.Drawing.Point(6, 42);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(56, 13);
            this.label248.TabIndex = 1;
            this.label248.Text = "tулав., мс";
            // 
            // label249
            // 
            this.label249.AutoSize = true;
            this.label249.Location = new System.Drawing.Point(6, 22);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(42, 13);
            this.label249.TabIndex = 1;
            this.label249.Text = "Режим";
            // 
            // blockReverseDZNP
            // 
            this.blockReverseDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.blockReverseDZNP.FormattingEnabled = true;
            this.blockReverseDZNP.Location = new System.Drawing.Point(121, 96);
            this.blockReverseDZNP.Name = "blockReverseDZNP";
            this.blockReverseDZNP.Size = new System.Drawing.Size(123, 21);
            this.blockReverseDZNP.TabIndex = 0;
            // 
            // modeReverseDZNP
            // 
            this.modeReverseDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.modeReverseDZNP.FormattingEnabled = true;
            this.modeReverseDZNP.Location = new System.Drawing.Point(121, 18);
            this.modeReverseDZNP.Name = "modeReverseDZNP";
            this.modeReverseDZNP.Size = new System.Drawing.Size(123, 21);
            this.modeReverseDZNP.TabIndex = 0;
            // 
            // groupBox58
            // 
            this.groupBox58.Controls.Add(this.uminDZNP);
            this.groupBox58.Controls.Add(this.tsrabTSDZNP);
            this.groupBox58.Controls.Add(this.label250);
            this.groupBox58.Controls.Add(this.label251);
            this.groupBox58.Controls.Add(this.label252);
            this.groupBox58.Controls.Add(this.label253);
            this.groupBox58.Controls.Add(this.label254);
            this.groupBox58.Controls.Add(this.blockTnDZNP);
            this.groupBox58.Controls.Add(this.blockEchoDZNP);
            this.groupBox58.Controls.Add(this.modeEchoDZNP);
            this.groupBox58.Location = new System.Drawing.Point(440, 6);
            this.groupBox58.Name = "groupBox58";
            this.groupBox58.Size = new System.Drawing.Size(254, 121);
            this.groupBox58.TabIndex = 2;
            this.groupBox58.TabStop = false;
            this.groupBox58.Text = "Логика откл. КСП";
            // 
            // uminDZNP
            // 
            this.uminDZNP.Location = new System.Drawing.Point(121, 57);
            this.uminDZNP.Name = "uminDZNP";
            this.uminDZNP.Size = new System.Drawing.Size(123, 20);
            this.uminDZNP.TabIndex = 6;
            this.uminDZNP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tsrabTSDZNP
            // 
            this.tsrabTSDZNP.Location = new System.Drawing.Point(121, 38);
            this.tsrabTSDZNP.Name = "tsrabTSDZNP";
            this.tsrabTSDZNP.Size = new System.Drawing.Size(123, 20);
            this.tsrabTSDZNP.TabIndex = 5;
            this.tsrabTSDZNP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label250
            // 
            this.label250.AutoSize = true;
            this.label250.Location = new System.Drawing.Point(6, 61);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(44, 13);
            this.label250.TabIndex = 1;
            this.label250.Text = "Umin, В";
            // 
            // label251
            // 
            this.label251.AutoSize = true;
            this.label251.Location = new System.Drawing.Point(6, 100);
            this.label251.Name = "label251";
            this.label251.Size = new System.Drawing.Size(109, 13);
            this.label251.TabIndex = 1;
            this.label251.Text = "Блок. от неиспр. ТН";
            // 
            // label252
            // 
            this.label252.AutoSize = true;
            this.label252.Location = new System.Drawing.Point(6, 80);
            this.label252.Name = "label252";
            this.label252.Size = new System.Drawing.Size(85, 13);
            this.label252.TabIndex = 1;
            this.label252.Text = "Сигнал блок-ки";
            // 
            // label253
            // 
            this.label253.AutoSize = true;
            this.label253.Location = new System.Drawing.Point(6, 42);
            this.label253.Name = "label253";
            this.label253.Size = new System.Drawing.Size(62, 13);
            this.label253.TabIndex = 1;
            this.label253.Text = "tср. ТС, мс";
            // 
            // label254
            // 
            this.label254.AutoSize = true;
            this.label254.Location = new System.Drawing.Point(6, 22);
            this.label254.Name = "label254";
            this.label254.Size = new System.Drawing.Size(42, 13);
            this.label254.TabIndex = 1;
            this.label254.Text = "Режим";
            // 
            // blockTnDZNP
            // 
            this.blockTnDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.blockTnDZNP.FormattingEnabled = true;
            this.blockTnDZNP.Location = new System.Drawing.Point(121, 96);
            this.blockTnDZNP.Name = "blockTnDZNP";
            this.blockTnDZNP.Size = new System.Drawing.Size(123, 21);
            this.blockTnDZNP.TabIndex = 0;
            // 
            // blockEchoDZNP
            // 
            this.blockEchoDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.blockEchoDZNP.FormattingEnabled = true;
            this.blockEchoDZNP.Location = new System.Drawing.Point(121, 76);
            this.blockEchoDZNP.Name = "blockEchoDZNP";
            this.blockEchoDZNP.Size = new System.Drawing.Size(123, 21);
            this.blockEchoDZNP.TabIndex = 0;
            // 
            // modeEchoDZNP
            // 
            this.modeEchoDZNP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.modeEchoDZNP.FormattingEnabled = true;
            this.modeEchoDZNP.Location = new System.Drawing.Point(121, 18);
            this.modeEchoDZNP.Name = "modeEchoDZNP";
            this.modeEchoDZNP.Size = new System.Drawing.Size(123, 21);
            this.modeEchoDZNP.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._setpointsComboBox);
            this.groupBox7.Location = new System.Drawing.Point(3, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(103, 54);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Группы уставок";
            // 
            // _setpointsComboBox
            // 
            this._setpointsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._setpointsComboBox.FormattingEnabled = true;
            this._setpointsComboBox.Location = new System.Drawing.Point(6, 19);
            this._setpointsComboBox.Name = "_setpointsComboBox";
            this._setpointsComboBox.Size = new System.Drawing.Size(88, 21);
            this._setpointsComboBox.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn66
            // 
            this.dataGridViewTextBoxColumn66.HeaderText = "";
            this.dataGridViewTextBoxColumn66.MinimumWidth = 40;
            this.dataGridViewTextBoxColumn66.Name = "dataGridViewTextBoxColumn66";
            this.dataGridViewTextBoxColumn66.ReadOnly = true;
            this.dataGridViewTextBoxColumn66.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn66.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn66.Width = 40;
            // 
            // dataGridViewComboBoxColumn76
            // 
            this.dataGridViewComboBoxColumn76.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn76.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn76.MinimumWidth = 120;
            this.dataGridViewComboBoxColumn76.Name = "dataGridViewComboBoxColumn76";
            this.dataGridViewComboBoxColumn76.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn76.Width = 120;
            // 
            // dataGridViewTextBoxColumn67
            // 
            dataGridViewCellStyle134.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle134.NullValue = null;
            this.dataGridViewTextBoxColumn67.DefaultCellStyle = dataGridViewCellStyle134;
            this.dataGridViewTextBoxColumn67.HeaderText = "Уставка Q, %";
            this.dataGridViewTextBoxColumn67.MinimumWidth = 50;
            this.dataGridViewTextBoxColumn67.Name = "dataGridViewTextBoxColumn67";
            this.dataGridViewTextBoxColumn67.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn67.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn67.Width = 85;
            // 
            // dataGridViewComboBoxColumn77
            // 
            this.dataGridViewComboBoxColumn77.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn77.MinimumWidth = 100;
            this.dataGridViewComboBoxColumn77.Name = "dataGridViewComboBoxColumn77";
            this.dataGridViewComboBoxColumn77.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewComboBoxColumn78
            // 
            this.dataGridViewComboBoxColumn78.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn78.MinimumWidth = 150;
            this.dataGridViewComboBoxColumn78.Name = "dataGridViewComboBoxColumn78";
            this.dataGridViewComboBoxColumn78.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn78.Width = 150;
            // 
            // dataGridViewCheckBoxColumn42
            // 
            this.dataGridViewCheckBoxColumn42.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn42.MinimumWidth = 50;
            this.dataGridViewCheckBoxColumn42.Name = "dataGridViewCheckBoxColumn42";
            this.dataGridViewCheckBoxColumn42.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn42.Width = 50;
            // 
            // dataGridViewCheckBoxColumn43
            // 
            this.dataGridViewCheckBoxColumn43.HeaderText = "АПВ";
            this.dataGridViewCheckBoxColumn43.MinimumWidth = 50;
            this.dataGridViewCheckBoxColumn43.Name = "dataGridViewCheckBoxColumn43";
            this.dataGridViewCheckBoxColumn43.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn43.Visible = false;
            // 
            // dataGridViewComboBoxColumn79
            // 
            this.dataGridViewComboBoxColumn79.HeaderText = "АПВ";
            this.dataGridViewComboBoxColumn79.Name = "dataGridViewComboBoxColumn79";
            this.dataGridViewComboBoxColumn79.Width = 72;
            // 
            // _configurationTabControl
            // 
            this._configurationTabControl.ContextMenuStrip = this.contextMenu;
            this._configurationTabControl.Controls.Add(this._setpointPage);
            this._configurationTabControl.Controls.Add(this._automatPage);
            this._configurationTabControl.Controls.Add(this._inputSignalsPage);
            this._configurationTabControl.Controls.Add(this._vlsTab);
            this._configurationTabControl.Controls.Add(this._outputSignalsPage);
            this._configurationTabControl.Controls.Add(this._logicTabPage);
            this._configurationTabControl.Controls.Add(this._systemPage);
            this._configurationTabControl.Controls.Add(this._gooseTabPage);
            this._configurationTabControl.Controls.Add(this._ethernetPage);
            this._configurationTabControl.Controls.Add(this._signaturesSignalsPage);
            this._configurationTabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._configurationTabControl.Location = new System.Drawing.Point(0, 0);
            this._configurationTabControl.MinimumSize = new System.Drawing.Size(820, 579);
            this._configurationTabControl.Name = "_configurationTabControl";
            this._configurationTabControl.SelectedIndex = 0;
            this._configurationTabControl.Size = new System.Drawing.Size(989, 579);
            this._configurationTabControl.TabIndex = 31;
            // 
            // _vlsTab
            // 
            this._vlsTab.BackColor = System.Drawing.SystemColors.Control;
            this._vlsTab.Controls.Add(this.groupBox57);
            this._vlsTab.Controls.Add(this.groupBox49);
            this._vlsTab.Location = new System.Drawing.Point(4, 22);
            this._vlsTab.Name = "_vlsTab";
            this._vlsTab.Padding = new System.Windows.Forms.Padding(3);
            this._vlsTab.Size = new System.Drawing.Size(981, 553);
            this._vlsTab.TabIndex = 15;
            this._vlsTab.Text = "ВЛС";
            // 
            // groupBox57
            // 
            this.groupBox57.Controls.Add(this.treeViewForVLS);
            this.groupBox57.Location = new System.Drawing.Point(434, 3);
            this.groupBox57.Name = "groupBox57";
            this.groupBox57.Size = new System.Drawing.Size(239, 436);
            this.groupBox57.TabIndex = 18;
            this.groupBox57.TabStop = false;
            this.groupBox57.Text = "Список всех ВЛС";
            // 
            // treeViewForVLS
            // 
            this.treeViewForVLS.Location = new System.Drawing.Point(16, 19);
            this.treeViewForVLS.Name = "treeViewForVLS";
            this.treeViewForVLS.Size = new System.Drawing.Size(207, 402);
            this.treeViewForVLS.TabIndex = 12;
            this.treeViewForVLS.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeViewForVLS_NodeMouseClick);
            // 
            // groupBox49
            // 
            this.groupBox49.Controls.Add(this._vlsTabControl);
            this.groupBox49.Location = new System.Drawing.Point(3, 3);
            this.groupBox49.Name = "groupBox49";
            this.groupBox49.Size = new System.Drawing.Size(405, 436);
            this.groupBox49.TabIndex = 17;
            this.groupBox49.TabStop = false;
            this.groupBox49.Text = "ВЛС";
            // 
            // _vlsTabControl
            // 
            this._vlsTabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this._vlsTabControl.Controls.Add(this.VLS1);
            this._vlsTabControl.Controls.Add(this.VLS2);
            this._vlsTabControl.Controls.Add(this.VLS3);
            this._vlsTabControl.Controls.Add(this.VLS4);
            this._vlsTabControl.Controls.Add(this.VLS5);
            this._vlsTabControl.Controls.Add(this.VLS6);
            this._vlsTabControl.Controls.Add(this.VLS7);
            this._vlsTabControl.Controls.Add(this.VLS8);
            this._vlsTabControl.Controls.Add(this.VLS9);
            this._vlsTabControl.Controls.Add(this.VLS10);
            this._vlsTabControl.Controls.Add(this.VLS11);
            this._vlsTabControl.Controls.Add(this.VLS12);
            this._vlsTabControl.Controls.Add(this.VLS13);
            this._vlsTabControl.Controls.Add(this.VLS14);
            this._vlsTabControl.Controls.Add(this.VLS15);
            this._vlsTabControl.Controls.Add(this.VLS16);
            this._vlsTabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._vlsTabControl.Location = new System.Drawing.Point(3, 16);
            this._vlsTabControl.Multiline = true;
            this._vlsTabControl.Name = "_vlsTabControl";
            this._vlsTabControl.SelectedIndex = 0;
            this._vlsTabControl.Size = new System.Drawing.Size(399, 414);
            this._vlsTabControl.TabIndex = 0;
            // 
            // VLS1
            // 
            this.VLS1.Controls.Add(this.VLSclb1Gr1);
            this.VLS1.Location = new System.Drawing.Point(4, 49);
            this.VLS1.Name = "VLS1";
            this.VLS1.Padding = new System.Windows.Forms.Padding(3);
            this.VLS1.Size = new System.Drawing.Size(391, 361);
            this.VLS1.TabIndex = 0;
            this.VLS1.Text = "ВЛС 1";
            this.VLS1.UseVisualStyleBackColor = true;
            // 
            // VLSclb1Gr1
            // 
            this.VLSclb1Gr1.CheckOnClick = true;
            this.VLSclb1Gr1.FormattingEnabled = true;
            this.VLSclb1Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb1Gr1.Name = "VLSclb1Gr1";
            this.VLSclb1Gr1.ScrollAlwaysVisible = true;
            this.VLSclb1Gr1.Size = new System.Drawing.Size(191, 349);
            this.VLSclb1Gr1.TabIndex = 6;
            this.VLSclb1Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS2
            // 
            this.VLS2.Controls.Add(this.VLSclb2Gr1);
            this.VLS2.Location = new System.Drawing.Point(4, 49);
            this.VLS2.Name = "VLS2";
            this.VLS2.Padding = new System.Windows.Forms.Padding(3);
            this.VLS2.Size = new System.Drawing.Size(391, 361);
            this.VLS2.TabIndex = 1;
            this.VLS2.Text = "ВЛС 2";
            this.VLS2.UseVisualStyleBackColor = true;
            // 
            // VLSclb2Gr1
            // 
            this.VLSclb2Gr1.CheckOnClick = true;
            this.VLSclb2Gr1.FormattingEnabled = true;
            this.VLSclb2Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb2Gr1.Name = "VLSclb2Gr1";
            this.VLSclb2Gr1.ScrollAlwaysVisible = true;
            this.VLSclb2Gr1.Size = new System.Drawing.Size(191, 349);
            this.VLSclb2Gr1.TabIndex = 6;
            this.VLSclb2Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS3
            // 
            this.VLS3.Controls.Add(this.VLSclb3Gr1);
            this.VLS3.Location = new System.Drawing.Point(4, 49);
            this.VLS3.Name = "VLS3";
            this.VLS3.Size = new System.Drawing.Size(391, 361);
            this.VLS3.TabIndex = 2;
            this.VLS3.Text = "ВЛС   3";
            this.VLS3.UseVisualStyleBackColor = true;
            // 
            // VLSclb3Gr1
            // 
            this.VLSclb3Gr1.CheckOnClick = true;
            this.VLSclb3Gr1.FormattingEnabled = true;
            this.VLSclb3Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb3Gr1.Name = "VLSclb3Gr1";
            this.VLSclb3Gr1.ScrollAlwaysVisible = true;
            this.VLSclb3Gr1.Size = new System.Drawing.Size(191, 349);
            this.VLSclb3Gr1.TabIndex = 6;
            this.VLSclb3Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS4
            // 
            this.VLS4.Controls.Add(this.VLSclb4Gr1);
            this.VLS4.Location = new System.Drawing.Point(4, 49);
            this.VLS4.Name = "VLS4";
            this.VLS4.Size = new System.Drawing.Size(391, 361);
            this.VLS4.TabIndex = 3;
            this.VLS4.Text = "ВЛС 4";
            this.VLS4.UseVisualStyleBackColor = true;
            // 
            // VLSclb4Gr1
            // 
            this.VLSclb4Gr1.CheckOnClick = true;
            this.VLSclb4Gr1.FormattingEnabled = true;
            this.VLSclb4Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb4Gr1.Name = "VLSclb4Gr1";
            this.VLSclb4Gr1.ScrollAlwaysVisible = true;
            this.VLSclb4Gr1.Size = new System.Drawing.Size(191, 349);
            this.VLSclb4Gr1.TabIndex = 6;
            this.VLSclb4Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS5
            // 
            this.VLS5.Controls.Add(this.VLSclb5Gr1);
            this.VLS5.Location = new System.Drawing.Point(4, 49);
            this.VLS5.Name = "VLS5";
            this.VLS5.Size = new System.Drawing.Size(391, 361);
            this.VLS5.TabIndex = 4;
            this.VLS5.Text = "ВЛС   5";
            this.VLS5.UseVisualStyleBackColor = true;
            // 
            // VLSclb5Gr1
            // 
            this.VLSclb5Gr1.CheckOnClick = true;
            this.VLSclb5Gr1.FormattingEnabled = true;
            this.VLSclb5Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb5Gr1.Name = "VLSclb5Gr1";
            this.VLSclb5Gr1.ScrollAlwaysVisible = true;
            this.VLSclb5Gr1.Size = new System.Drawing.Size(191, 349);
            this.VLSclb5Gr1.TabIndex = 6;
            this.VLSclb5Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS6
            // 
            this.VLS6.Controls.Add(this.VLSclb6Gr1);
            this.VLS6.Location = new System.Drawing.Point(4, 49);
            this.VLS6.Name = "VLS6";
            this.VLS6.Size = new System.Drawing.Size(391, 361);
            this.VLS6.TabIndex = 5;
            this.VLS6.Text = "ВЛС  6";
            this.VLS6.UseVisualStyleBackColor = true;
            // 
            // VLSclb6Gr1
            // 
            this.VLSclb6Gr1.CheckOnClick = true;
            this.VLSclb6Gr1.FormattingEnabled = true;
            this.VLSclb6Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb6Gr1.Name = "VLSclb6Gr1";
            this.VLSclb6Gr1.ScrollAlwaysVisible = true;
            this.VLSclb6Gr1.Size = new System.Drawing.Size(191, 349);
            this.VLSclb6Gr1.TabIndex = 6;
            this.VLSclb6Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS7
            // 
            this.VLS7.Controls.Add(this.VLSclb7Gr1);
            this.VLS7.Location = new System.Drawing.Point(4, 49);
            this.VLS7.Name = "VLS7";
            this.VLS7.Size = new System.Drawing.Size(391, 361);
            this.VLS7.TabIndex = 6;
            this.VLS7.Text = "ВЛС 7";
            this.VLS7.UseVisualStyleBackColor = true;
            // 
            // VLSclb7Gr1
            // 
            this.VLSclb7Gr1.CheckOnClick = true;
            this.VLSclb7Gr1.FormattingEnabled = true;
            this.VLSclb7Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb7Gr1.Name = "VLSclb7Gr1";
            this.VLSclb7Gr1.ScrollAlwaysVisible = true;
            this.VLSclb7Gr1.Size = new System.Drawing.Size(191, 349);
            this.VLSclb7Gr1.TabIndex = 6;
            this.VLSclb7Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS8
            // 
            this.VLS8.Controls.Add(this.VLSclb8Gr1);
            this.VLS8.Location = new System.Drawing.Point(4, 49);
            this.VLS8.Name = "VLS8";
            this.VLS8.Size = new System.Drawing.Size(391, 361);
            this.VLS8.TabIndex = 7;
            this.VLS8.Text = "ВЛС   8";
            this.VLS8.UseVisualStyleBackColor = true;
            // 
            // VLSclb8Gr1
            // 
            this.VLSclb8Gr1.CheckOnClick = true;
            this.VLSclb8Gr1.FormattingEnabled = true;
            this.VLSclb8Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb8Gr1.Name = "VLSclb8Gr1";
            this.VLSclb8Gr1.ScrollAlwaysVisible = true;
            this.VLSclb8Gr1.Size = new System.Drawing.Size(191, 349);
            this.VLSclb8Gr1.TabIndex = 6;
            this.VLSclb8Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS9
            // 
            this.VLS9.Controls.Add(this.VLSclb9Gr1);
            this.VLS9.Location = new System.Drawing.Point(4, 49);
            this.VLS9.Name = "VLS9";
            this.VLS9.Size = new System.Drawing.Size(391, 361);
            this.VLS9.TabIndex = 8;
            this.VLS9.Text = "ВЛС9";
            this.VLS9.UseVisualStyleBackColor = true;
            // 
            // VLSclb9Gr1
            // 
            this.VLSclb9Gr1.CheckOnClick = true;
            this.VLSclb9Gr1.FormattingEnabled = true;
            this.VLSclb9Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb9Gr1.Name = "VLSclb9Gr1";
            this.VLSclb9Gr1.ScrollAlwaysVisible = true;
            this.VLSclb9Gr1.Size = new System.Drawing.Size(191, 349);
            this.VLSclb9Gr1.TabIndex = 6;
            this.VLSclb9Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS10
            // 
            this.VLS10.Controls.Add(this.VLSclb10Gr1);
            this.VLS10.Location = new System.Drawing.Point(4, 49);
            this.VLS10.Name = "VLS10";
            this.VLS10.Size = new System.Drawing.Size(391, 361);
            this.VLS10.TabIndex = 9;
            this.VLS10.Text = "ВЛС10";
            this.VLS10.UseVisualStyleBackColor = true;
            // 
            // VLSclb10Gr1
            // 
            this.VLSclb10Gr1.CheckOnClick = true;
            this.VLSclb10Gr1.FormattingEnabled = true;
            this.VLSclb10Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb10Gr1.Name = "VLSclb10Gr1";
            this.VLSclb10Gr1.ScrollAlwaysVisible = true;
            this.VLSclb10Gr1.Size = new System.Drawing.Size(191, 349);
            this.VLSclb10Gr1.TabIndex = 6;
            this.VLSclb10Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS11
            // 
            this.VLS11.Controls.Add(this.VLSclb11Gr1);
            this.VLS11.Location = new System.Drawing.Point(4, 49);
            this.VLS11.Name = "VLS11";
            this.VLS11.Size = new System.Drawing.Size(391, 361);
            this.VLS11.TabIndex = 10;
            this.VLS11.Text = "ВЛС11";
            this.VLS11.UseVisualStyleBackColor = true;
            // 
            // VLSclb11Gr1
            // 
            this.VLSclb11Gr1.CheckOnClick = true;
            this.VLSclb11Gr1.FormattingEnabled = true;
            this.VLSclb11Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb11Gr1.Name = "VLSclb11Gr1";
            this.VLSclb11Gr1.ScrollAlwaysVisible = true;
            this.VLSclb11Gr1.Size = new System.Drawing.Size(191, 349);
            this.VLSclb11Gr1.TabIndex = 6;
            this.VLSclb11Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS12
            // 
            this.VLS12.Controls.Add(this.VLSclb12Gr1);
            this.VLS12.Location = new System.Drawing.Point(4, 49);
            this.VLS12.Name = "VLS12";
            this.VLS12.Size = new System.Drawing.Size(391, 361);
            this.VLS12.TabIndex = 11;
            this.VLS12.Text = "ВЛС12";
            this.VLS12.UseVisualStyleBackColor = true;
            // 
            // VLSclb12Gr1
            // 
            this.VLSclb12Gr1.CheckOnClick = true;
            this.VLSclb12Gr1.FormattingEnabled = true;
            this.VLSclb12Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb12Gr1.Name = "VLSclb12Gr1";
            this.VLSclb12Gr1.ScrollAlwaysVisible = true;
            this.VLSclb12Gr1.Size = new System.Drawing.Size(191, 349);
            this.VLSclb12Gr1.TabIndex = 6;
            this.VLSclb12Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS13
            // 
            this.VLS13.Controls.Add(this.VLSclb13Gr1);
            this.VLS13.Location = new System.Drawing.Point(4, 49);
            this.VLS13.Name = "VLS13";
            this.VLS13.Size = new System.Drawing.Size(391, 361);
            this.VLS13.TabIndex = 12;
            this.VLS13.Text = "ВЛС13";
            this.VLS13.UseVisualStyleBackColor = true;
            // 
            // VLSclb13Gr1
            // 
            this.VLSclb13Gr1.CheckOnClick = true;
            this.VLSclb13Gr1.FormattingEnabled = true;
            this.VLSclb13Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb13Gr1.Name = "VLSclb13Gr1";
            this.VLSclb13Gr1.ScrollAlwaysVisible = true;
            this.VLSclb13Gr1.Size = new System.Drawing.Size(191, 349);
            this.VLSclb13Gr1.TabIndex = 6;
            this.VLSclb13Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS14
            // 
            this.VLS14.Controls.Add(this.VLSclb14Gr1);
            this.VLS14.Location = new System.Drawing.Point(4, 49);
            this.VLS14.Name = "VLS14";
            this.VLS14.Size = new System.Drawing.Size(391, 361);
            this.VLS14.TabIndex = 13;
            this.VLS14.Text = "ВЛС14";
            this.VLS14.UseVisualStyleBackColor = true;
            // 
            // VLSclb14Gr1
            // 
            this.VLSclb14Gr1.CheckOnClick = true;
            this.VLSclb14Gr1.FormattingEnabled = true;
            this.VLSclb14Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb14Gr1.Name = "VLSclb14Gr1";
            this.VLSclb14Gr1.ScrollAlwaysVisible = true;
            this.VLSclb14Gr1.Size = new System.Drawing.Size(191, 349);
            this.VLSclb14Gr1.TabIndex = 6;
            this.VLSclb14Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS15
            // 
            this.VLS15.Controls.Add(this.VLSclb15Gr1);
            this.VLS15.Location = new System.Drawing.Point(4, 49);
            this.VLS15.Name = "VLS15";
            this.VLS15.Size = new System.Drawing.Size(391, 361);
            this.VLS15.TabIndex = 14;
            this.VLS15.Text = "ВЛС15";
            this.VLS15.UseVisualStyleBackColor = true;
            // 
            // VLSclb15Gr1
            // 
            this.VLSclb15Gr1.CheckOnClick = true;
            this.VLSclb15Gr1.FormattingEnabled = true;
            this.VLSclb15Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb15Gr1.Name = "VLSclb15Gr1";
            this.VLSclb15Gr1.ScrollAlwaysVisible = true;
            this.VLSclb15Gr1.Size = new System.Drawing.Size(191, 349);
            this.VLSclb15Gr1.TabIndex = 6;
            this.VLSclb15Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS16
            // 
            this.VLS16.Controls.Add(this.VLSclb16Gr1);
            this.VLS16.Location = new System.Drawing.Point(4, 49);
            this.VLS16.Name = "VLS16";
            this.VLS16.Size = new System.Drawing.Size(391, 361);
            this.VLS16.TabIndex = 15;
            this.VLS16.Text = "ВЛС16";
            this.VLS16.UseVisualStyleBackColor = true;
            // 
            // VLSclb16Gr1
            // 
            this.VLSclb16Gr1.CheckOnClick = true;
            this.VLSclb16Gr1.FormattingEnabled = true;
            this.VLSclb16Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb16Gr1.Name = "VLSclb16Gr1";
            this.VLSclb16Gr1.ScrollAlwaysVisible = true;
            this.VLSclb16Gr1.Size = new System.Drawing.Size(191, 349);
            this.VLSclb16Gr1.TabIndex = 6;
            this.VLSclb16Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // _logicTabPage
            // 
            this._logicTabPage.Controls.Add(this.groupBox28);
            this._logicTabPage.Controls.Add(this.groupBox27);
            this._logicTabPage.Location = new System.Drawing.Point(4, 22);
            this._logicTabPage.Name = "_logicTabPage";
            this._logicTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._logicTabPage.Size = new System.Drawing.Size(981, 553);
            this._logicTabPage.TabIndex = 14;
            this._logicTabPage.Text = "Логические сигналы";
            this._logicTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this._rsTriggersDataGrid);
            this.groupBox28.Location = new System.Drawing.Point(428, 3);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(496, 402);
            this.groupBox28.TabIndex = 27;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Энергонезависимые RS-триггеры";
            // 
            // _rsTriggersDataGrid
            // 
            this._rsTriggersDataGrid.AllowUserToAddRows = false;
            this._rsTriggersDataGrid.AllowUserToDeleteRows = false;
            this._rsTriggersDataGrid.AllowUserToResizeColumns = false;
            this._rsTriggersDataGrid.AllowUserToResizeRows = false;
            this._rsTriggersDataGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle135.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle135.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle135.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle135.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle135.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle135.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle135.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._rsTriggersDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle135;
            this._rsTriggersDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._rsTriggersDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn83,
            this.dataGridViewComboBoxColumn102,
            this._baseRColumn,
            this._sygnalsRColumn,
            this._baseSColumn,
            this._sColumn});
            this._rsTriggersDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._rsTriggersDataGrid.Location = new System.Drawing.Point(3, 16);
            this._rsTriggersDataGrid.Name = "_rsTriggersDataGrid";
            this._rsTriggersDataGrid.RowHeadersVisible = false;
            this._rsTriggersDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle136.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._rsTriggersDataGrid.RowsDefaultCellStyle = dataGridViewCellStyle136;
            this._rsTriggersDataGrid.RowTemplate.Height = 24;
            this._rsTriggersDataGrid.ShowCellErrors = false;
            this._rsTriggersDataGrid.ShowRowErrors = false;
            this._rsTriggersDataGrid.Size = new System.Drawing.Size(490, 383);
            this._rsTriggersDataGrid.TabIndex = 0;
            this._rsTriggersDataGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn83
            // 
            this.dataGridViewTextBoxColumn83.HeaderText = "№";
            this.dataGridViewTextBoxColumn83.Name = "dataGridViewTextBoxColumn83";
            this.dataGridViewTextBoxColumn83.ReadOnly = true;
            this.dataGridViewTextBoxColumn83.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn83.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn83.Width = 25;
            // 
            // dataGridViewComboBoxColumn102
            // 
            this.dataGridViewComboBoxColumn102.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn102.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn102.HeaderText = "Тип";
            this.dataGridViewComboBoxColumn102.Name = "dataGridViewComboBoxColumn102";
            this.dataGridViewComboBoxColumn102.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn102.Width = 32;
            // 
            // _baseRColumn
            // 
            this._baseRColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._baseRColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._baseRColumn.HeaderText = "База";
            this._baseRColumn.Name = "_baseRColumn";
            this._baseRColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._baseRColumn.Width = 38;
            // 
            // _sygnalsRColumn
            // 
            this._sygnalsRColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._sygnalsRColumn.HeaderText = "Вход R";
            this._sygnalsRColumn.Name = "_sygnalsRColumn";
            this._sygnalsRColumn.Width = 48;
            // 
            // _baseSColumn
            // 
            this._baseSColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._baseSColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._baseSColumn.HeaderText = "База";
            this._baseSColumn.Name = "_baseSColumn";
            this._baseSColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._baseSColumn.Width = 38;
            // 
            // _sColumn
            // 
            this._sColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._sColumn.HeaderText = "Вход S";
            this._sColumn.Name = "_sColumn";
            this._sColumn.Width = 47;
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this._virtualReleDataGrid);
            this.groupBox27.Location = new System.Drawing.Point(3, 3);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(419, 541);
            this.groupBox27.TabIndex = 26;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Виртуальные  реле";
            // 
            // _virtualReleDataGrid
            // 
            this._virtualReleDataGrid.AllowUserToAddRows = false;
            this._virtualReleDataGrid.AllowUserToDeleteRows = false;
            this._virtualReleDataGrid.AllowUserToResizeColumns = false;
            this._virtualReleDataGrid.AllowUserToResizeRows = false;
            this._virtualReleDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._virtualReleDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._virtualReleDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn81,
            this.dataGridViewComboBoxColumn99,
            this.dataGridViewComboBoxColumn100,
            this.dataGridViewComboBoxColumn101,
            this.dataGridViewTextBoxColumn82});
            this._virtualReleDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._virtualReleDataGrid.Location = new System.Drawing.Point(3, 16);
            this._virtualReleDataGrid.Name = "_virtualReleDataGrid";
            this._virtualReleDataGrid.RowHeadersVisible = false;
            this._virtualReleDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle137.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._virtualReleDataGrid.RowsDefaultCellStyle = dataGridViewCellStyle137;
            this._virtualReleDataGrid.RowTemplate.Height = 24;
            this._virtualReleDataGrid.ShowCellErrors = false;
            this._virtualReleDataGrid.ShowRowErrors = false;
            this._virtualReleDataGrid.Size = new System.Drawing.Size(413, 522);
            this._virtualReleDataGrid.TabIndex = 1;
            this._virtualReleDataGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._grid_DataError);
            // 
            // dataGridViewTextBoxColumn81
            // 
            this.dataGridViewTextBoxColumn81.HeaderText = "№";
            this.dataGridViewTextBoxColumn81.Name = "dataGridViewTextBoxColumn81";
            this.dataGridViewTextBoxColumn81.ReadOnly = true;
            this.dataGridViewTextBoxColumn81.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn81.Width = 25;
            // 
            // dataGridViewComboBoxColumn99
            // 
            this.dataGridViewComboBoxColumn99.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn99.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn99.HeaderText = "Тип";
            this.dataGridViewComboBoxColumn99.Name = "dataGridViewComboBoxColumn99";
            this.dataGridViewComboBoxColumn99.Width = 32;
            // 
            // dataGridViewComboBoxColumn100
            // 
            this.dataGridViewComboBoxColumn100.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn100.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn100.HeaderText = "База";
            this.dataGridViewComboBoxColumn100.Name = "dataGridViewComboBoxColumn100";
            this.dataGridViewComboBoxColumn100.Width = 38;
            // 
            // dataGridViewComboBoxColumn101
            // 
            this.dataGridViewComboBoxColumn101.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn101.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn101.HeaderText = "Сигнал";
            this.dataGridViewComboBoxColumn101.Name = "dataGridViewComboBoxColumn101";
            this.dataGridViewComboBoxColumn101.Width = 49;
            // 
            // dataGridViewTextBoxColumn82
            // 
            this.dataGridViewTextBoxColumn82.HeaderText = "Твозвр, мс";
            this.dataGridViewTextBoxColumn82.Name = "dataGridViewTextBoxColumn82";
            this.dataGridViewTextBoxColumn82.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn82.Width = 70;
            // 
            // _ethernetPage
            // 
            this._ethernetPage.Controls.Add(this._reserveGroupBox);
            this._ethernetPage.Controls.Add(this.groupBox51);
            this._ethernetPage.Location = new System.Drawing.Point(4, 22);
            this._ethernetPage.Name = "_ethernetPage";
            this._ethernetPage.Padding = new System.Windows.Forms.Padding(3);
            this._ethernetPage.Size = new System.Drawing.Size(981, 553);
            this._ethernetPage.TabIndex = 12;
            this._ethernetPage.Text = "Конфигурация Ethernet";
            this._ethernetPage.UseVisualStyleBackColor = true;
            // 
            // _reserveGroupBox
            // 
            this._reserveGroupBox.Controls.Add(this._reserveCB);
            this._reserveGroupBox.Location = new System.Drawing.Point(275, 6);
            this._reserveGroupBox.Name = "_reserveGroupBox";
            this._reserveGroupBox.Size = new System.Drawing.Size(113, 52);
            this._reserveGroupBox.TabIndex = 1;
            this._reserveGroupBox.TabStop = false;
            this._reserveGroupBox.Text = "Резервирование";
            // 
            // _reserveCB
            // 
            this._reserveCB.FormattingEnabled = true;
            this._reserveCB.Location = new System.Drawing.Point(9, 19);
            this._reserveCB.Name = "_reserveCB";
            this._reserveCB.Size = new System.Drawing.Size(89, 21);
            this._reserveCB.TabIndex = 0;
            // 
            // groupBox51
            // 
            this.groupBox51.Controls.Add(this._ipLo1);
            this.groupBox51.Controls.Add(this._ipLo2);
            this.groupBox51.Controls.Add(this._ipHi1);
            this.groupBox51.Controls.Add(this._ipHi2);
            this.groupBox51.Controls.Add(this.label214);
            this.groupBox51.Controls.Add(this.label213);
            this.groupBox51.Controls.Add(this.label212);
            this.groupBox51.Controls.Add(this.label159);
            this.groupBox51.Location = new System.Drawing.Point(8, 6);
            this.groupBox51.Name = "groupBox51";
            this.groupBox51.Size = new System.Drawing.Size(261, 52);
            this.groupBox51.TabIndex = 0;
            this.groupBox51.TabStop = false;
            this.groupBox51.Text = "Конфигурация Ethernet";
            // 
            // _ipLo1
            // 
            this._ipLo1.Location = new System.Drawing.Point(215, 19);
            this._ipLo1.Mask = "000";
            this._ipLo1.Name = "_ipLo1";
            this._ipLo1.PromptChar = ' ';
            this._ipLo1.Size = new System.Drawing.Size(38, 20);
            this._ipLo1.TabIndex = 1003;
            // 
            // _ipLo2
            // 
            this._ipLo2.Location = new System.Drawing.Point(163, 19);
            this._ipLo2.Mask = "000";
            this._ipLo2.Name = "_ipLo2";
            this._ipLo2.PromptChar = ' ';
            this._ipLo2.Size = new System.Drawing.Size(38, 20);
            this._ipLo2.TabIndex = 1002;
            this._ipLo2.KeyUp += new System.Windows.Forms.KeyEventHandler(this._ipLo2_KeyUp);
            // 
            // _ipHi1
            // 
            this._ipHi1.Location = new System.Drawing.Point(112, 19);
            this._ipHi1.Mask = "000";
            this._ipHi1.Name = "_ipHi1";
            this._ipHi1.PromptChar = ' ';
            this._ipHi1.Size = new System.Drawing.Size(38, 20);
            this._ipHi1.TabIndex = 1001;
            this._ipHi1.KeyUp += new System.Windows.Forms.KeyEventHandler(this._ipHi1_KeyUp);
            // 
            // _ipHi2
            // 
            this._ipHi2.Location = new System.Drawing.Point(60, 19);
            this._ipHi2.Mask = "000";
            this._ipHi2.Name = "_ipHi2";
            this._ipHi2.PromptChar = ' ';
            this._ipHi2.Size = new System.Drawing.Size(38, 20);
            this._ipHi2.TabIndex = 1000;
            this._ipHi2.KeyUp += new System.Windows.Forms.KeyEventHandler(this._ipHi2_KeyUp);
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Location = new System.Drawing.Point(152, 26);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(10, 13);
            this.label214.TabIndex = 9;
            this.label214.Text = ".";
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Location = new System.Drawing.Point(203, 26);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(10, 13);
            this.label213.TabIndex = 9;
            this.label213.Text = ".";
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Location = new System.Drawing.Point(100, 26);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(10, 13);
            this.label212.TabIndex = 9;
            this.label212.Text = ".";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Location = new System.Drawing.Point(6, 22);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(50, 13);
            this.label159.TabIndex = 9;
            this.label159.Text = "IP-адрес";
            // 
            // _signaturesSignalsPage
            // 
            this._signaturesSignalsPage.Controls.Add(this.signaturesSignalsConfigControl);
            this._signaturesSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._signaturesSignalsPage.Name = "_signaturesSignalsPage";
            this._signaturesSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._signaturesSignalsPage.Size = new System.Drawing.Size(981, 553);
            this._signaturesSignalsPage.TabIndex = 13;
            this._signaturesSignalsPage.Text = "Подписи списка сигналов";
            this._signaturesSignalsPage.UseVisualStyleBackColor = true;
            // 
            // signaturesSignalsConfigControl
            // 
            this.signaturesSignalsConfigControl.DefaultList = null;
            this.signaturesSignalsConfigControl.Location = new System.Drawing.Point(0, 0);
            this.signaturesSignalsConfigControl.Name = "signaturesSignalsConfigControl";
            this.signaturesSignalsConfigControl.SignalsList = null;
            this.signaturesSignalsConfigControl.Size = new System.Drawing.Size(324, 550);
            this.signaturesSignalsConfigControl.TabIndex = 0;
            // 
            // _externalDifSbrosColumn
            // 
            this._externalDifSbrosColumn.HeaderText = "Сброс";
            this._externalDifSbrosColumn.Name = "_externalDifSbrosColumn";
            this._externalDifSbrosColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifSbrosColumn.Width = 40;
            // 
            // _externalDifAPVRetColumn
            // 
            this._externalDifAPVRetColumn.HeaderText = "АПВ вз.";
            this._externalDifAPVRetColumn.Name = "_externalDifAPVRetColumn";
            this._externalDifAPVRetColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifAPVRetColumn.Width = 35;
            // 
            // _externalDifAPV1
            // 
            this._externalDifAPV1.HeaderText = "АПВ";
            this._externalDifAPV1.Name = "_externalDifAPV1";
            this._externalDifAPV1.Width = 70;
            // 
            // _externalDifAPV
            // 
            this._externalDifAPV.HeaderText = "АПВ";
            this._externalDifAPV.Name = "_externalDifAPV";
            this._externalDifAPV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifAPV.Visible = false;
            this._externalDifAPV.Width = 35;
            // 
            // _externalDifUROVColumn
            // 
            this._externalDifUROVColumn.HeaderText = "УРОВ";
            this._externalDifUROVColumn.Name = "_externalDifUROVColumn";
            this._externalDifUROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifUROVColumn.Width = 40;
            // 
            // _externalDifOscColumn
            // 
            dataGridViewCellStyle138.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifOscColumn.DefaultCellStyle = dataGridViewCellStyle138;
            this._externalDifOscColumn.HeaderText = "Осциллограф";
            this._externalDifOscColumn.Name = "_externalDifOscColumn";
            this._externalDifOscColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifOscColumn.Width = 110;
            // 
            // _externalDifBlockingColumn
            // 
            dataGridViewCellStyle139.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifBlockingColumn.DefaultCellStyle = dataGridViewCellStyle139;
            this._externalDifBlockingColumn.HeaderText = "Сигнал блокировки";
            this._externalDifBlockingColumn.Name = "_externalDifBlockingColumn";
            this._externalDifBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifBlockingColumn.Width = 130;
            // 
            // _externalDifVozvrYNColumn
            // 
            this._externalDifVozvrYNColumn.HeaderText = "Возврат";
            this._externalDifVozvrYNColumn.Name = "_externalDifVozvrYNColumn";
            this._externalDifVozvrYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifVozvrYNColumn.Width = 53;
            // 
            // _externalDifVozvrColumn
            // 
            dataGridViewCellStyle140.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifVozvrColumn.DefaultCellStyle = dataGridViewCellStyle140;
            this._externalDifVozvrColumn.HeaderText = "Сигнал возврата";
            this._externalDifVozvrColumn.Name = "_externalDifVozvrColumn";
            this._externalDifVozvrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifVozvrColumn.Width = 130;
            // 
            // _externalDifTvzColumn
            // 
            dataGridViewCellStyle141.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifTvzColumn.DefaultCellStyle = dataGridViewCellStyle141;
            this._externalDifTvzColumn.HeaderText = "tвз [мс]";
            this._externalDifTvzColumn.Name = "_externalDifTvzColumn";
            this._externalDifTvzColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifTvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifTvzColumn.Width = 50;
            // 
            // _externalDifTsrColumn
            // 
            dataGridViewCellStyle142.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifTsrColumn.DefaultCellStyle = dataGridViewCellStyle142;
            this._externalDifTsrColumn.HeaderText = "tср [мс]";
            this._externalDifTsrColumn.Name = "_externalDifTsrColumn";
            this._externalDifTsrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifTsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifTsrColumn.Width = 50;
            // 
            // _externalDifSrabColumn
            // 
            dataGridViewCellStyle143.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifSrabColumn.DefaultCellStyle = dataGridViewCellStyle143;
            this._externalDifSrabColumn.HeaderText = "Сигнал срабатывания";
            this._externalDifSrabColumn.Name = "_externalDifSrabColumn";
            this._externalDifSrabColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifSrabColumn.Width = 130;
            // 
            // _externalDifModesColumn
            // 
            dataGridViewCellStyle144.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifModesColumn.DefaultCellStyle = dataGridViewCellStyle144;
            this._externalDifModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._externalDifModesColumn.HeaderText = "Режим";
            this._externalDifModesColumn.Name = "_externalDifModesColumn";
            this._externalDifModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _externalDifStageColumn
            // 
            dataGridViewCellStyle145.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._externalDifStageColumn.DefaultCellStyle = dataGridViewCellStyle145;
            this._externalDifStageColumn.Frozen = true;
            this._externalDifStageColumn.HeaderText = "";
            this._externalDifStageColumn.Name = "_externalDifStageColumn";
            this._externalDifStageColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifStageColumn.Width = 50;
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(989, 634);
            this.ContextMenuStrip = this.contextMenu;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this._configurationTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "ConfigurationForm";
            this.Text = "ConfigurationForm";
            this.Activated += new System.EventHandler(this.Mr771ConfigurationForm102_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Mr771ConfigurationForm_FormClosing);
            this.Load += new System.EventHandler(this.Mr771ConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Mr771ConfigurationForm_KeyUp);
            this.contextMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this._gooseTabPage.ResumeLayout(false);
            this._gooseTabPage.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this._automatPage.ResumeLayout(false);
            this.UROVgroupBox.ResumeLayout(false);
            this.UROVgroupBox.PerformLayout();
            this.groupBox32.ResumeLayout(false);
            this.groupBox32.PerformLayout();
            this.groupBox33.ResumeLayout(false);
            this.groupBox33.PerformLayout();
            this._systemPage.ResumeLayout(false);
            this._signaturesForOscGroupBox.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._oscChannelsGrid)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this._outputSignalsPage.ResumeLayout(false);
            this.groupBox175.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).EndInit();
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this.groupBox54.ResumeLayout(false);
            this.groupBox54.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this._inputSignalsPage.ResumeLayout(false);
            this.groupBox59.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._commandsDataGridView)).EndInit();
            this.groupBox22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._antiBounceDataGridView)).EndInit();
            this.groupBox18.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this._setpointPage.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.groupBox24.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this._setpointsTab.ResumeLayout(false);
            this._measureTransPage.ResumeLayout(false);
            this._measureTransPage.PerformLayout();
            this._paramEngineGroupBox.ResumeLayout(false);
            this._paramEngineGroupBox.PerformLayout();
            this.passportDataGroup.ResumeLayout(false);
            this.passportDataGroup.PerformLayout();
            this.groupBox48.ResumeLayout(false);
            this.groupBox48.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.omp2.ResumeLayout(false);
            this.omp2.PerformLayout();
            this.omp1.ResumeLayout(false);
            this.omp1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this._defences.ResumeLayout(false);
            this._defencesTabControl.ResumeLayout(false);
            this._distDefence.ResumeLayout(false);
            this._cornersDefence.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this._defenceI.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this._I14GroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesI1DataGrid)).EndInit();
            this._ILGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesI3DataGrid)).EndInit();
            this._I56GroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesI2DataGrid)).EndInit();
            this._defenceIStar.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.groupBox60.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesI0DataGrid)).EndInit();
            this._defenceI2I1.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this._startArcDefence.ResumeLayout(false);
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this._defenceU.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.groupBox62.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesUBDataGrid)).EndInit();
            this.groupBox63.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesUMDataGrid)).EndInit();
            this._defenceF.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.groupBox64.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesFBDataGrid)).EndInit();
            this.groupBox65.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesFMDataGrid)).EndInit();
            this._defenceQ.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._engineDefensesDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._engineDefensesGrid)).EndInit();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox66.ResumeLayout(false);
            this.groupBox66.PerformLayout();
            this._externalDefence.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenses)).EndInit();
            this._powerDefence.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._reversePowerGrid)).EndInit();
            this._lsGr.ResumeLayout(false);
            this.groupBox56.ResumeLayout(false);
            this.groupBox55.ResumeLayout(false);
            this.groupBox26.ResumeLayout(false);
            this._lsSecondTabControl.ResumeLayout(false);
            this.tabPage31.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv1Gr1)).EndInit();
            this.tabPage32.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv2Gr1)).EndInit();
            this.tabPage33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv3Gr1)).EndInit();
            this.tabPage34.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv4Gr1)).EndInit();
            this.tabPage35.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv5Gr1)).EndInit();
            this.tabPage36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv6Gr1)).EndInit();
            this.tabPage37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv7Gr1)).EndInit();
            this.tabPage38.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv8Gr1)).EndInit();
            this.groupBox45.ResumeLayout(false);
            this._lsFirsTabControl.ResumeLayout(false);
            this.tabPage39.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv1Gr1)).EndInit();
            this.tabPage40.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv2Gr1)).EndInit();
            this.tabPage41.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv3Gr1)).EndInit();
            this.tabPage42.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv4Gr1)).EndInit();
            this.tabPage43.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv5Gr1)).EndInit();
            this.tabPage44.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv6Gr1)).EndInit();
            this.tabPage45.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv7Gr1)).EndInit();
            this.tabPage46.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv8Gr1)).EndInit();
            this._apvAvrTabPage.ResumeLayout(false);
            this._avrGroupBox.ResumeLayout(false);
            this._avrGroupBox.PerformLayout();
            this._avrReclouserGroupBox.ResumeLayout(false);
            this._avrReclouserGroupBox.PerformLayout();
            this._avrSectionGroupBox.ResumeLayout(false);
            this._avrSectionGroupBox.PerformLayout();
            this._apvGroupBox.ResumeLayout(false);
            this._apvGroupBox.PerformLayout();
            this._ksiunpTabPage.ResumeLayout(false);
            this._ksiunpTabPage.PerformLayout();
            this.groupBox39.ResumeLayout(false);
            this.groupBox39.PerformLayout();
            this.groupBox40.ResumeLayout(false);
            this.groupBox40.PerformLayout();
            this.groupBox41.ResumeLayout(false);
            this.groupBox41.PerformLayout();
            this.groupBox42.ResumeLayout(false);
            this.groupBox42.PerformLayout();
            this.groupBox35.ResumeLayout(false);
            this.groupBox35.PerformLayout();
            this.groupBox38.ResumeLayout(false);
            this.groupBox38.PerformLayout();
            this.groupBox37.ResumeLayout(false);
            this.groupBox37.PerformLayout();
            this.groupBox36.ResumeLayout(false);
            this.groupBox36.PerformLayout();
            this.groupBox34.ResumeLayout(false);
            this.groupBox34.PerformLayout();
            this._tuAndTbTabPage.ResumeLayout(false);
            this.tabControl100.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox50.ResumeLayout(false);
            this.groupBox50.PerformLayout();
            this.konturFNGroup.ResumeLayout(false);
            this.konturFNGroup.PerformLayout();
            this.konturFFGroup.ResumeLayout(false);
            this.konturFFGroup.PerformLayout();
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox44.ResumeLayout(false);
            this.groupBox44.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox47.ResumeLayout(false);
            this.groupBox47.PerformLayout();
            this.groupBox52.ResumeLayout(false);
            this.groupBox52.PerformLayout();
            this.groupBox53.ResumeLayout(false);
            this.groupBox53.PerformLayout();
            this.groupBox46.ResumeLayout(false);
            this.groupBox46.PerformLayout();
            this.groupBox58.ResumeLayout(false);
            this.groupBox58.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this._configurationTabControl.ResumeLayout(false);
            this._vlsTab.ResumeLayout(false);
            this.groupBox57.ResumeLayout(false);
            this.groupBox49.ResumeLayout(false);
            this._vlsTabControl.ResumeLayout(false);
            this.VLS1.ResumeLayout(false);
            this.VLS2.ResumeLayout(false);
            this.VLS3.ResumeLayout(false);
            this.VLS4.ResumeLayout(false);
            this.VLS5.ResumeLayout(false);
            this.VLS6.ResumeLayout(false);
            this.VLS7.ResumeLayout(false);
            this.VLS8.ResumeLayout(false);
            this.VLS9.ResumeLayout(false);
            this.VLS10.ResumeLayout(false);
            this.VLS11.ResumeLayout(false);
            this.VLS12.ResumeLayout(false);
            this.VLS13.ResumeLayout(false);
            this.VLS14.ResumeLayout(false);
            this.VLS15.ResumeLayout(false);
            this.VLS16.ResumeLayout(false);
            this._logicTabPage.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._rsTriggersDataGrid)).EndInit();
            this.groupBox27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._virtualReleDataGrid)).EndInit();
            this._ethernetPage.ResumeLayout(false);
            this._reserveGroupBox.ResumeLayout(false);
            this.groupBox51.ResumeLayout(false);
            this.groupBox51.PerformLayout();
            this._signaturesSignalsPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar _progressBar;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.Button _resetSetpointsButton;
        private System.Windows.Forms.Button _saveToXmlButton;
        private System.Windows.Forms.SaveFileDialog _saveXmlDialog;
        private System.Windows.Forms.SaveFileDialog saveFileResistCharacteristic;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem resetSetpointsItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToHtmlItem;
        private System.Windows.Forms.TabPage _gooseTabPage;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.ComboBox goin64;
        private System.Windows.Forms.ComboBox goin48;
        private System.Windows.Forms.ComboBox goin32;
        private System.Windows.Forms.Label label281;
        private System.Windows.Forms.Label label282;
        private System.Windows.Forms.Label label283;
        private System.Windows.Forms.ComboBox goin63;
        private System.Windows.Forms.ComboBox goin47;
        private System.Windows.Forms.ComboBox goin31;
        private System.Windows.Forms.Label label284;
        private System.Windows.Forms.Label label285;
        private System.Windows.Forms.Label label286;
        private System.Windows.Forms.ComboBox goin62;
        private System.Windows.Forms.ComboBox goin46;
        private System.Windows.Forms.ComboBox goin30;
        private System.Windows.Forms.Label label287;
        private System.Windows.Forms.Label label288;
        private System.Windows.Forms.Label label289;
        private System.Windows.Forms.ComboBox goin61;
        private System.Windows.Forms.ComboBox goin45;
        private System.Windows.Forms.ComboBox goin29;
        private System.Windows.Forms.Label label290;
        private System.Windows.Forms.Label label291;
        private System.Windows.Forms.Label label292;
        private System.Windows.Forms.ComboBox goin60;
        private System.Windows.Forms.ComboBox goin44;
        private System.Windows.Forms.ComboBox goin28;
        private System.Windows.Forms.Label label293;
        private System.Windows.Forms.Label label294;
        private System.Windows.Forms.Label label295;
        private System.Windows.Forms.ComboBox goin59;
        private System.Windows.Forms.ComboBox goin43;
        private System.Windows.Forms.ComboBox goin27;
        private System.Windows.Forms.Label label296;
        private System.Windows.Forms.Label label297;
        private System.Windows.Forms.Label label298;
        private System.Windows.Forms.ComboBox goin58;
        private System.Windows.Forms.ComboBox goin42;
        private System.Windows.Forms.ComboBox goin26;
        private System.Windows.Forms.Label label299;
        private System.Windows.Forms.Label label300;
        private System.Windows.Forms.Label label301;
        private System.Windows.Forms.ComboBox goin57;
        private System.Windows.Forms.ComboBox goin41;
        private System.Windows.Forms.ComboBox goin25;
        private System.Windows.Forms.Label label302;
        private System.Windows.Forms.Label label303;
        private System.Windows.Forms.Label label304;
        private System.Windows.Forms.ComboBox goin56;
        private System.Windows.Forms.ComboBox goin40;
        private System.Windows.Forms.ComboBox goin24;
        private System.Windows.Forms.Label label305;
        private System.Windows.Forms.Label label306;
        private System.Windows.Forms.Label label307;
        private System.Windows.Forms.ComboBox goin55;
        private System.Windows.Forms.ComboBox goin39;
        private System.Windows.Forms.ComboBox goin23;
        private System.Windows.Forms.Label label308;
        private System.Windows.Forms.Label label309;
        private System.Windows.Forms.Label label310;
        private System.Windows.Forms.ComboBox goin54;
        private System.Windows.Forms.ComboBox goin38;
        private System.Windows.Forms.ComboBox goin22;
        private System.Windows.Forms.Label label311;
        private System.Windows.Forms.Label label312;
        private System.Windows.Forms.Label label313;
        private System.Windows.Forms.ComboBox goin53;
        private System.Windows.Forms.ComboBox goin37;
        private System.Windows.Forms.ComboBox goin21;
        private System.Windows.Forms.Label label314;
        private System.Windows.Forms.Label label315;
        private System.Windows.Forms.Label label316;
        private System.Windows.Forms.ComboBox goin52;
        private System.Windows.Forms.ComboBox goin36;
        private System.Windows.Forms.ComboBox goin20;
        private System.Windows.Forms.Label label317;
        private System.Windows.Forms.Label label318;
        private System.Windows.Forms.Label label319;
        private System.Windows.Forms.ComboBox goin51;
        private System.Windows.Forms.ComboBox goin35;
        private System.Windows.Forms.ComboBox goin19;
        private System.Windows.Forms.Label label320;
        private System.Windows.Forms.Label label321;
        private System.Windows.Forms.Label label322;
        private System.Windows.Forms.ComboBox goin50;
        private System.Windows.Forms.ComboBox goin34;
        private System.Windows.Forms.ComboBox goin18;
        private System.Windows.Forms.Label label323;
        private System.Windows.Forms.Label label324;
        private System.Windows.Forms.Label label325;
        private System.Windows.Forms.ComboBox goin49;
        private System.Windows.Forms.Label label326;
        private System.Windows.Forms.ComboBox goin33;
        private System.Windows.Forms.Label label327;
        private System.Windows.Forms.ComboBox goin17;
        private System.Windows.Forms.Label label328;
        private System.Windows.Forms.ComboBox goin16;
        private System.Windows.Forms.Label label329;
        private System.Windows.Forms.ComboBox goin15;
        private System.Windows.Forms.Label label330;
        private System.Windows.Forms.ComboBox goin14;
        private System.Windows.Forms.Label label331;
        private System.Windows.Forms.ComboBox goin13;
        private System.Windows.Forms.Label label332;
        private System.Windows.Forms.ComboBox goin12;
        private System.Windows.Forms.Label label333;
        private System.Windows.Forms.ComboBox goin11;
        private System.Windows.Forms.Label label334;
        private System.Windows.Forms.ComboBox goin10;
        private System.Windows.Forms.Label label335;
        private System.Windows.Forms.ComboBox goin9;
        private System.Windows.Forms.Label label336;
        private System.Windows.Forms.ComboBox goin8;
        private System.Windows.Forms.Label label337;
        private System.Windows.Forms.ComboBox goin7;
        private System.Windows.Forms.Label label338;
        private System.Windows.Forms.ComboBox goin6;
        private System.Windows.Forms.Label label339;
        private System.Windows.Forms.ComboBox goin5;
        private System.Windows.Forms.Label label340;
        private System.Windows.Forms.ComboBox goin4;
        private System.Windows.Forms.Label label341;
        private System.Windows.Forms.ComboBox goin3;
        private System.Windows.Forms.Label label342;
        private System.Windows.Forms.ComboBox goin2;
        private System.Windows.Forms.Label label343;
        private System.Windows.Forms.ComboBox goin1;
        private System.Windows.Forms.Label label344;
        private System.Windows.Forms.Label label345;
        private System.Windows.Forms.ComboBox operationBGS;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox currentBGS;
        private System.Windows.Forms.TabPage _automatPage;
        private System.Windows.Forms.GroupBox UROVgroupBox;
        private System.Windows.Forms.CheckBox _urovSelf;
        private System.Windows.Forms.CheckBox _urovBkCheck;
        private System.Windows.Forms.CheckBox _urovIcheck;
        private System.Windows.Forms.ComboBox _urovBlock;
        private System.Windows.Forms.MaskedTextBox _Iurov;
        private System.Windows.Forms.MaskedTextBox _tUrov2;
        private System.Windows.Forms.MaskedTextBox _tUrov1;
        private System.Windows.Forms.ComboBox _urovPusk;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.ComboBox _comandOtkl;
        private System.Windows.Forms.ComboBox _controlSolenoidCombo;
        private System.Windows.Forms.ComboBox _switchKontCep;
        private System.Windows.Forms.MaskedTextBox _switchTUskor;
        private System.Windows.Forms.MaskedTextBox _switchImp;
        private System.Windows.Forms.ComboBox _switchBlock;
        private System.Windows.Forms.ComboBox _switchError;
        private System.Windows.Forms.ComboBox _switchOn;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.ComboBox _switchOff;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.GroupBox groupBox33;
        private System.Windows.Forms.ComboBox _blockSDTU;
        private System.Windows.Forms.ComboBox _switchSDTU;
        private System.Windows.Forms.ComboBox _switchVnesh;
        private System.Windows.Forms.ComboBox _switchKey;
        private System.Windows.Forms.ComboBox _switchButtons;
        private System.Windows.Forms.ComboBox _switchVneshOff;
        private System.Windows.Forms.ComboBox _switchVneshOn;
        private System.Windows.Forms.ComboBox _switchKeyOff;
        private System.Windows.Forms.Label _blockSDTUlabel;
        private System.Windows.Forms.ComboBox _switchKeyOn;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.TabPage _systemPage;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.DataGridView _oscChannelsGrid;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox oscStartCb;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.MaskedTextBox _oscSizeTextBox;
        private System.Windows.Forms.MaskedTextBox _oscWriteLength;
        private System.Windows.Forms.ComboBox _oscFix;
        private System.Windows.Forms.ComboBox _oscLength;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TabPage _outputSignalsPage;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.CheckBox _resetAlarmCheckBox;
        private System.Windows.Forms.CheckBox _resetSystemCheckBox;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.CheckBox _fault6CheckBox;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.CheckBox _fault5CheckBox;
        private System.Windows.Forms.CheckBox _fault4CheckBox;
        private System.Windows.Forms.CheckBox _fault3CheckBox;
        private System.Windows.Forms.CheckBox _fault2CheckBox;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.CheckBox _fault1CheckBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.MaskedTextBox _impTB;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TabPage _inputSignalsPage;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.ComboBox _indComboBox;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.ComboBox _grUst6ComboBox;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.ComboBox _grUst5ComboBox;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.ComboBox _grUst4ComboBox;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.ComboBox _grUst3ComboBox;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.ComboBox _grUst2ComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox _grUst1ComboBox;
        private System.Windows.Forms.TabPage _setpointPage;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox _inpAddCombo;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.CheckBox _resistCheck;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.Button _applyCopySetpoinsButton;
        private System.Windows.Forms.ComboBox _copySetpoinsGroupComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox _setpointsComboBox;
        private System.Windows.Forms.TabControl _configurationTabControl;
        private System.Windows.Forms.TabPage _ethernetPage;
        private System.Windows.Forms.GroupBox groupBox51;
        private System.Windows.Forms.MaskedTextBox _ipLo1;
        private System.Windows.Forms.MaskedTextBox _ipLo2;
        private System.Windows.Forms.MaskedTextBox _ipHi1;
        private System.Windows.Forms.MaskedTextBox _ipHi2;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.Label label213;
        private System.Windows.Forms.Label label212;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Button _clearSetpointBtn;
        private System.Windows.Forms.ToolStripMenuItem clearSetpointsItem;
        private System.Windows.Forms.CheckBox _fixErrorFCheckBox;
        private System.Windows.Forms.Label label193;
        private System.Windows.Forms.GroupBox groupBox54;
        private System.Windows.Forms.TabControl _setpointsTab;
        private System.Windows.Forms.TabPage _measureTransPage;
        private System.Windows.Forms.GroupBox groupBox48;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.ComboBox _resetTnGr1;
        private System.Windows.Forms.ComboBox _errorX1_comboGr1;
        private System.Windows.Forms.Label _faultTHn1Label;
        private System.Windows.Forms.ComboBox _errorX_comboGr1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox _i0u0CheckGr1;
        private System.Windows.Forms.CheckBox _i2u2CheckGr1;
        private System.Windows.Forms.MaskedTextBox _tdContrCepGr1;
        private System.Windows.Forms.ComboBox _errorL_comboGr1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.MaskedTextBox _iMinContrCepGr1;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.MaskedTextBox _uMinContrCepGr1;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.MaskedTextBox _u0ContrCepGr1;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.MaskedTextBox _u2ContrCepGr1;
        private System.Windows.Forms.MaskedTextBox _tsContrCepGr1;
        private System.Windows.Forms.MaskedTextBox _uDelContrCepGr1;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.MaskedTextBox _iDelContrCepGr1;
        private System.Windows.Forms.MaskedTextBox _iMaxContrCepGr1;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.MaskedTextBox _uMaxContrCepGr1;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.MaskedTextBox _i0ContrCepGr1;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.MaskedTextBox _i2ContrCepGr1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel omp2;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.MaskedTextBox _xud5p;
        private System.Windows.Forms.MaskedTextBox _xud4p;
        private System.Windows.Forms.MaskedTextBox _xud3p;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.MaskedTextBox _xud2p;
        private System.Windows.Forms.MaskedTextBox _xud1p;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Panel omp1;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.MaskedTextBox _xud3s;
        private System.Windows.Forms.Label _l12;
        private System.Windows.Forms.MaskedTextBox _xud5s;
        private System.Windows.Forms.Label _l13;
        private System.Windows.Forms.MaskedTextBox _xud4s;
        private System.Windows.Forms.MaskedTextBox _xud2s;
        private System.Windows.Forms.MaskedTextBox _xud1s;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.ComboBox _OMPmode_comboGr1;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MaskedTextBox _l2;
        private System.Windows.Forms.MaskedTextBox _l1;
        private System.Windows.Forms.MaskedTextBox _l3;
        private System.Windows.Forms.MaskedTextBox _l4;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox _KTHX1koef_comboGr1;
        private System.Windows.Forms.ComboBox _KTHXkoef_comboGr1;
        private System.Windows.Forms.Label _kthn1XLabel;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label _kthn1Label;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox _KTHLkoef_comboGr1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox _Uo_typeComboGr1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox _KTHX1_BoxGr1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox _KTHX_BoxGr1;
        private System.Windows.Forms.MaskedTextBox _KTHL_BoxGr1;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.ComboBox _alternationCB;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.ComboBox _TT_typeComboGr1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox _ITTX_BoxGr1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox _ITTL_BoxGr1;
        private System.Windows.Forms.MaskedTextBox _Im_BoxGr1;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TabPage _lsGr;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.TabControl _lsSecondTabControl;
        private System.Windows.Forms.TabPage tabPage31;
        private System.Windows.Forms.DataGridView _lsOrDgv1Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn15;
        private System.Windows.Forms.TabPage tabPage32;
        private System.Windows.Forms.DataGridView _lsOrDgv2Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn16;
        private System.Windows.Forms.TabPage tabPage33;
        private System.Windows.Forms.DataGridView _lsOrDgv3Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn17;
        private System.Windows.Forms.TabPage tabPage34;
        private System.Windows.Forms.DataGridView _lsOrDgv4Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn18;
        private System.Windows.Forms.TabPage tabPage35;
        private System.Windows.Forms.DataGridView _lsOrDgv5Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn19;
        private System.Windows.Forms.TabPage tabPage36;
        private System.Windows.Forms.DataGridView _lsOrDgv6Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn20;
        private System.Windows.Forms.TabPage tabPage37;
        private System.Windows.Forms.DataGridView _lsOrDgv7Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn21;
        private System.Windows.Forms.TabPage tabPage38;
        private System.Windows.Forms.DataGridView _lsOrDgv8Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn22;
        private System.Windows.Forms.GroupBox groupBox45;
        private System.Windows.Forms.TabControl _lsFirsTabControl;
        private System.Windows.Forms.TabPage tabPage39;
        private System.Windows.Forms.DataGridView _lsAndDgv1Gr1;
        private System.Windows.Forms.TabPage tabPage40;
        private System.Windows.Forms.DataGridView _lsAndDgv2Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn24;
        private System.Windows.Forms.TabPage tabPage41;
        private System.Windows.Forms.DataGridView _lsAndDgv3Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn25;
        private System.Windows.Forms.TabPage tabPage42;
        private System.Windows.Forms.DataGridView _lsAndDgv4Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn26;
        private System.Windows.Forms.TabPage tabPage43;
        private System.Windows.Forms.DataGridView _lsAndDgv5Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn27;
        private System.Windows.Forms.TabPage tabPage44;
        private System.Windows.Forms.DataGridView _lsAndDgv6Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn28;
        private System.Windows.Forms.TabPage tabPage45;
        private System.Windows.Forms.DataGridView _lsAndDgv7Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn29;
        private System.Windows.Forms.TabPage tabPage46;
        private System.Windows.Forms.DataGridView _lsAndDgv8Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn30;
        private System.Windows.Forms.TabPage _apvAvrTabPage;
        private System.Windows.Forms.GroupBox _apvGroupBox;
        private System.Windows.Forms.CheckBox _blokFromUrov;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.MaskedTextBox _apvKrat4Gr1;
        private System.Windows.Forms.MaskedTextBox _apvKrat3Gr1;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.ComboBox _apvSwitchOffGr1;
        private System.Windows.Forms.MaskedTextBox _apvKrat2Gr1;
        private System.Windows.Forms.MaskedTextBox _apvKrat1Gr1;
        private System.Windows.Forms.MaskedTextBox _apvTreadyGr1;
        private System.Windows.Forms.MaskedTextBox _apvTblockGr1;
        private System.Windows.Forms.ComboBox _apvBlockGr1;
        private System.Windows.Forms.MaskedTextBox _timeDisable;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.ComboBox _typeDisable;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.ComboBox _disableApv;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.ComboBox _apvModeGr1;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.TabPage _ksiunpTabPage;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.GroupBox groupBox39;
        private System.Windows.Forms.CheckBox _blockTNauto;
        private System.Windows.Forms.GroupBox groupBox40;
        private System.Windows.Forms.CheckBox _catchSinchrAuto;
        private System.Windows.Forms.MaskedTextBox _sinhrAutodFnoGr1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox41;
        private System.Windows.Forms.CheckBox _waitSinchrAuto;
        private System.Windows.Forms.MaskedTextBox _sinhrAutodFGr1;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.MaskedTextBox _sinhrAutodFiGr1;
        private System.Windows.Forms.GroupBox groupBox42;
        private System.Windows.Forms.ComboBox _sinhrAutoNoNoGr1;
        private System.Windows.Forms.ComboBox _sinhrAutoYesNoGr1;
        private System.Windows.Forms.ComboBox _sinhrAutoNoYesGr1;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.MaskedTextBox _sinhrAutoUmaxGr1;
        private System.Windows.Forms.ComboBox _sinhrAutoModeGr1;
        private System.Windows.Forms.Label _dUmaxAutoLabel;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.GroupBox groupBox35;
        private System.Windows.Forms.CheckBox _blockTNmanual;
        private System.Windows.Forms.GroupBox groupBox38;
        private System.Windows.Forms.CheckBox _catchSinchrManual;
        private System.Windows.Forms.MaskedTextBox _sinhrManualdFnoGr1;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.GroupBox groupBox37;
        private System.Windows.Forms.CheckBox _waitSinchrManual;
        private System.Windows.Forms.MaskedTextBox _sinhrManualdFGr1;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.MaskedTextBox _sinhrManualdFiGr1;
        private System.Windows.Forms.GroupBox groupBox36;
        private System.Windows.Forms.ComboBox _sinhrManualNoNoGr1;
        private System.Windows.Forms.ComboBox _sinhrManualYesNoGr1;
        private System.Windows.Forms.ComboBox _sinhrManualNoYesGr1;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.MaskedTextBox _sinhrManualUmaxGr1;
        private System.Windows.Forms.ComboBox _sinhrManualModeGr1;
        private System.Windows.Forms.Label _dUmaxManualLabel;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.GroupBox groupBox34;
        private System.Windows.Forms.ComboBox _discretIn3Cmb;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.MaskedTextBox _sinhrF;
        private System.Windows.Forms.ComboBox _discretIn2Cmb;
        private System.Windows.Forms.MaskedTextBox _sinhrKamp;
        private System.Windows.Forms.MaskedTextBox _sinhrTonGr1;
        private System.Windows.Forms.ComboBox _discretIn1Cmb;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.ComboBox _blockSinhCmb;
        private System.Windows.Forms.MaskedTextBox _sinhrTsinhrGr1;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label _blockSinhLabel;
        private System.Windows.Forms.MaskedTextBox _sinhrTwaitGr1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.MaskedTextBox _sinhrUmaxNalGr1;
        private System.Windows.Forms.MaskedTextBox _sinhrUminNalGr1;
        private System.Windows.Forms.MaskedTextBox _sinhrUminOtsGr1;
        private System.Windows.Forms.ComboBox _sinhrU2Gr1;
        private System.Windows.Forms.ComboBox _sinhrU1Gr1;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.GroupBox _reserveGroupBox;
        private System.Windows.Forms.ComboBox _reserveCB;
        private System.Windows.Forms.GroupBox groupBox56;
        private System.Windows.Forms.TreeView treeViewForLsOR;
        private System.Windows.Forms.GroupBox groupBox55;
        private System.Windows.Forms.TreeView treeViewForLsAND;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn23;
        private System.Windows.Forms.TabPage _signaturesSignalsPage;
        private Forms.SignaturesSignalsConfigControl signaturesSignalsConfigControl;
        private System.Windows.Forms.TabPage _defences;
        private System.Windows.Forms.TabControl _defencesTabControl;
        private System.Windows.Forms.TabPage _defenceIStar;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.GroupBox groupBox60;
        private System.Windows.Forms.DataGridView _difensesI0DataGrid;
        private System.Windows.Forms.TabPage _defenceI2I1;
        private System.Windows.Forms.TabPage _defenceU;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.GroupBox groupBox62;
        private System.Windows.Forms.GroupBox groupBox63;
        private System.Windows.Forms.DataGridView _difensesUMDataGrid;
        private System.Windows.Forms.TabPage _defenceF;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.GroupBox groupBox64;
        private System.Windows.Forms.DataGridView _difensesFBDataGrid;
        private System.Windows.Forms.GroupBox groupBox65;
        private System.Windows.Forms.DataGridView _difensesFMDataGrid;
        private System.Windows.Forms.TabPage _defenceQ;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn66;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn76;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn67;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn77;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn78;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn42;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn43;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn79;
        private System.Windows.Forms.GroupBox groupBox66;
        private System.Windows.Forms.Label label222;
        private System.Windows.Forms.Label label223;
        private System.Windows.Forms.MaskedTextBox _engineQconstraint;
        private System.Windows.Forms.Label label224;
        private System.Windows.Forms.MaskedTextBox _engineQtime;
        private System.Windows.Forms.ComboBox _engineQmode;
        private System.Windows.Forms.TabPage _externalDefence;
        private System.Windows.Forms.DataGridView _externalDefenses;
        private System.Windows.Forms.TabPage _cornersDefence;
        private System.Windows.Forms.TabPage _distDefence;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifSbrosColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifAPVRetColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifAPV1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifAPV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifUROVColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifOscColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifBlockingColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifVozvrYNColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifVozvrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifTvzColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifTsrColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifSrabColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifModesColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifStageColumn;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.CheckBox _urovI2I1CheckBox;
        private System.Windows.Forms.ComboBox i2i1AVR;
        private System.Windows.Forms.ComboBox i2i1APV1;
        private System.Windows.Forms.ComboBox I2I1OscCombo;
        private System.Windows.Forms.MaskedTextBox I2I1tcpTB;
        private System.Windows.Forms.Label _avrI2I1Label;
        private System.Windows.Forms.ComboBox I2I1BlockingCombo;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label215;
        private System.Windows.Forms.MaskedTextBox I2I1TB;
        private System.Windows.Forms.ComboBox I2I1ModeCombo;
        private System.Windows.Forms.Label label216;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.MaskedTextBox _i2CornerGr1;
        private System.Windows.Forms.MaskedTextBox _inCornerGr1;
        private System.Windows.Forms.MaskedTextBox _i0CornerGr1;
        private System.Windows.Forms.MaskedTextBox _iCornerGr1;
        private System.Windows.Forms.TabPage _powerDefence;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.Label label217;
        private System.Windows.Forms.Label label218;
        private System.Windows.Forms.MaskedTextBox _numHot;
        private System.Windows.Forms.MaskedTextBox _numCold;
        private System.Windows.Forms.Label label219;
        private System.Windows.Forms.MaskedTextBox _waitNumBlock;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView _reversePowerGrid;
        private System.Windows.Forms.TabPage _startArcDefence;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.CheckBox OscArc;
        private System.Windows.Forms.ComboBox StartBlockArcComboBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.MaskedTextBox IcpArcTextBox;
        private System.Windows.Forms.ComboBox StartArcModeComboBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.DataGridView _antiBounceDataGridView;
        private System.Windows.Forms.GroupBox groupBox175;
        private System.Windows.Forms.DataGridView _outputIndicatorsGrid;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.DataGridView _outputReleGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn baseColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releWaitCol;
        private System.Windows.Forms.TabPage _logicTabPage;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.DataGridView _virtualReleDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn81;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn99;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn100;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn101;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn82;
        private ResistanceDefTabCtr resistanceDefTabCtr1;
        private System.Windows.Forms.DataGridView _engineDefensesGrid;
        private System.Windows.Forms.GroupBox _avrGroupBox;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.ComboBox _avrClear;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.MaskedTextBox _avrTOff;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.MaskedTextBox _avrTBack;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.ComboBox _avrBack;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.MaskedTextBox _avrTSr;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.ComboBox _avrResolve;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.ComboBox _avrBlockClear;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.ComboBox _avrBlocking;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.ComboBox _avrSIGNOn;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.ComboBox _avrByDiff;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.ComboBox _avrBySelfOff;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.ComboBox _avrByOff;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.ComboBox _avrBySignal;
        private System.Windows.Forms.GroupBox passportDataGroup;
        private System.Windows.Forms.MaskedTextBox _sn;
        private System.Windows.Forms.Label label220;
        private System.Windows.Forms.MaskedTextBox _kpdP;
        private System.Windows.Forms.Label label221;
        private System.Windows.Forms.MaskedTextBox _cosfP;
        private System.Windows.Forms.Label label225;
        private System.Windows.Forms.Label label226;
        private System.Windows.Forms.MaskedTextBox _ppd;
        private System.Windows.Forms.ComboBox _ppdCombo;
        private System.Windows.Forms.GroupBox _paramEngineGroupBox;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label _inputQResetLabel;
        private System.Windows.Forms.ComboBox _engineNreset;
        private System.Windows.Forms.ComboBox _engineQresetGr1;
        private System.Windows.Forms.MaskedTextBox _qBox;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.MaskedTextBox _tCount;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.MaskedTextBox _tStartBox;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.MaskedTextBox _iStartBox;
        private System.Windows.Forms.Label _iStartLabel;
        private System.Windows.Forms.MaskedTextBox _engineIdv;
        private System.Windows.Forms.Label _idvLabel;
        private System.Windows.Forms.MaskedTextBox _engineCoolingTimeGr1;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.MaskedTextBox _engineHeatingTimeGr1;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.TabPage _vlsTab;
        private System.Windows.Forms.GroupBox groupBox57;
        private System.Windows.Forms.TreeView treeViewForVLS;
        private System.Windows.Forms.GroupBox groupBox49;
        private System.Windows.Forms.TabControl _vlsTabControl;
        private System.Windows.Forms.TabPage VLS1;
        private System.Windows.Forms.CheckedListBox VLSclb1Gr1;
        private System.Windows.Forms.TabPage VLS2;
        private System.Windows.Forms.CheckedListBox VLSclb2Gr1;
        private System.Windows.Forms.TabPage VLS3;
        private System.Windows.Forms.CheckedListBox VLSclb3Gr1;
        private System.Windows.Forms.TabPage VLS4;
        private System.Windows.Forms.CheckedListBox VLSclb4Gr1;
        private System.Windows.Forms.TabPage VLS5;
        private System.Windows.Forms.CheckedListBox VLSclb5Gr1;
        private System.Windows.Forms.TabPage VLS6;
        private System.Windows.Forms.CheckedListBox VLSclb6Gr1;
        private System.Windows.Forms.TabPage VLS7;
        private System.Windows.Forms.CheckedListBox VLSclb7Gr1;
        private System.Windows.Forms.TabPage VLS8;
        private System.Windows.Forms.CheckedListBox VLSclb8Gr1;
        private System.Windows.Forms.TabPage VLS9;
        private System.Windows.Forms.CheckedListBox VLSclb9Gr1;
        private System.Windows.Forms.TabPage VLS10;
        private System.Windows.Forms.CheckedListBox VLSclb10Gr1;
        private System.Windows.Forms.TabPage VLS11;
        private System.Windows.Forms.CheckedListBox VLSclb11Gr1;
        private System.Windows.Forms.TabPage VLS12;
        private System.Windows.Forms.CheckedListBox VLSclb12Gr1;
        private System.Windows.Forms.TabPage VLS13;
        private System.Windows.Forms.CheckedListBox VLSclb13Gr1;
        private System.Windows.Forms.TabPage VLS14;
        private System.Windows.Forms.CheckedListBox VLSclb14Gr1;
        private System.Windows.Forms.TabPage VLS15;
        private System.Windows.Forms.CheckedListBox VLSclb15Gr1;
        private System.Windows.Forms.TabPage VLS16;
        private System.Windows.Forms.CheckedListBox VLSclb16Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.ComboBox _avrTypeComboBox;
        private System.Windows.Forms.GroupBox _avrSectionGroupBox;
        private System.Windows.Forms.GroupBox _avrReclouserGroupBox;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.ComboBox _avrRecInputU2CB;
        private System.Windows.Forms.MaskedTextBox _avrRecUminMTB;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.ComboBox _avrRecInputU1CB;
        private System.Windows.Forms.MaskedTextBox _avrRecUmaxMTB;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.ComboBox _avrRecBanComboBox;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.MaskedTextBox _avrReadyRecMTB;
        private System.Windows.Forms.ComboBox _avrRecKeyComboBox;
        private System.Windows.Forms.MaskedTextBox _tBlockRecMTB;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.ComboBox _avrRecModeComboBox;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.CheckBox _avrRecBlockSelfCheck;
        private System.Windows.Forms.ComboBox _avrRecBlockSDTUCB;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.DataGridView _engineDefensesDataGrid;
        private System.Windows.Forms.DataGridView _difensesUBDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn86;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn87;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _engineDefenseAPVcol;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column19;
        private System.Windows.Forms.Label _ucaLabel;
        private System.Windows.Forms.ComboBox _ucaComboBox;
        private System.Windows.Forms.ComboBox _inputUcaComboBox;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ssr;
        private System.Windows.Forms.DataGridViewTextBoxColumn _usr;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tsr;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tvz;
        private System.Windows.Forms.DataGridViewTextBoxColumn _svz;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _svzbeno;
        private System.Windows.Forms.DataGridViewTextBoxColumn _isr;
        private System.Windows.Forms.DataGridViewComboBoxColumn _block;
        private System.Windows.Forms.DataGridViewComboBoxColumn _blk;
        private System.Windows.Forms.DataGridViewComboBoxColumn _osc;
        private System.Windows.Forms.DataGridViewComboBoxColumn _apvvoz;
        private System.Windows.Forms.DataGridViewComboBoxColumn _urov;
        private System.Windows.Forms.DataGridViewComboBoxColumn _apv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _resetstep;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn baseGreenColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignalCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn baseRedColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _out1IndSignalCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignal2Col;
        private System.Windows.Forms.TabPage _tuAndTbTabPage;
        private System.Windows.Forms.TabControl tabControl100;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox50;
        private System.Windows.Forms.CheckBox konturFN;
        private System.Windows.Forms.CheckBox konturFF;
        private System.Windows.Forms.GroupBox konturFNGroup;
        private System.Windows.Forms.ComboBox abbrevDZFN;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.ComboBox reverseDZFN;
        private System.Windows.Forms.ComboBox extendDZFN;
        private System.Windows.Forms.GroupBox konturFFGroup;
        private System.Windows.Forms.ComboBox abbrevDZFF;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.ComboBox reverseDZFF;
        private System.Windows.Forms.ComboBox extendDZFF;
        private System.Windows.Forms.ComboBox modeDZ;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.ComboBox inpTSDZ;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.ComboBox apvDZ;
        private System.Windows.Forms.ComboBox urovDZ;
        private System.Windows.Forms.ComboBox OscDZ;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.CheckBox zoneDZ;
        private System.Windows.Forms.MaskedTextBox tficsDZ;
        private System.Windows.Forms.MaskedTextBox treverseDZ;
        private System.Windows.Forms.Label label192;
        private System.Windows.Forms.Label label194;
        private System.Windows.Forms.Label label195;
        private System.Windows.Forms.Label label196;
        private System.Windows.Forms.Label label197;
        private System.Windows.Forms.ComboBox blockReverseDZ;
        private System.Windows.Forms.ComboBox modeReverseDZ;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.MaskedTextBox uminDZ;
        private System.Windows.Forms.MaskedTextBox tsrabTSDZ;
        private System.Windows.Forms.Label label198;
        private System.Windows.Forms.Label label199;
        private System.Windows.Forms.Label label200;
        private System.Windows.Forms.Label label201;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.ComboBox blockTnDZ;
        private System.Windows.Forms.ComboBox blockEchoDZ;
        private System.Windows.Forms.ComboBox modeEchoDZ;
        private System.Windows.Forms.GroupBox groupBox44;
        private System.Windows.Forms.MaskedTextBox toffTBDZ;
        private System.Windows.Forms.MaskedTextBox toffDZ;
        private System.Windows.Forms.MaskedTextBox tminImpDZ;
        private System.Windows.Forms.MaskedTextBox tsrabDebDZ;
        private System.Windows.Forms.MaskedTextBox tvzTSDZ;
        private System.Windows.Forms.Label label203;
        private System.Windows.Forms.Label label204;
        private System.Windows.Forms.Label label205;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.Label label207;
        private System.Windows.Forms.Label label208;
        private System.Windows.Forms.Label label209;
        private System.Windows.Forms.Label label210;
        private System.Windows.Forms.Label label211;
        private System.Windows.Forms.ComboBox blockSwithDZ;
        private System.Windows.Forms.ComboBox deblockDZ;
        private System.Windows.Forms.ComboBox controlDZ;
        private System.Windows.Forms.ComboBox blockTSDZ;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox47;
        private System.Windows.Forms.GroupBox groupBox52;
        private System.Windows.Forms.ComboBox abbrevDZNP;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.Label label228;
        private System.Windows.Forms.Label label229;
        private System.Windows.Forms.ComboBox reverseDZNP;
        private System.Windows.Forms.ComboBox extendDZNP;
        private System.Windows.Forms.ComboBox modeDZNP;
        private System.Windows.Forms.Label label230;
        private System.Windows.Forms.ComboBox inpTSDZNP;
        private System.Windows.Forms.Label label231;
        private System.Windows.Forms.Label label232;
        private System.Windows.Forms.Label label233;
        private System.Windows.Forms.Label label234;
        private System.Windows.Forms.ComboBox apvDZNP;
        private System.Windows.Forms.ComboBox urovDZNP;
        private System.Windows.Forms.ComboBox oscDZNP;
        private System.Windows.Forms.ComboBox directionDZNP;
        private System.Windows.Forms.Label label235;
        private System.Windows.Forms.GroupBox groupBox53;
        private System.Windows.Forms.MaskedTextBox toffTBDZNP;
        private System.Windows.Forms.MaskedTextBox toffDZNP;
        private System.Windows.Forms.MaskedTextBox tminImpDZNP;
        private System.Windows.Forms.MaskedTextBox tsrabDebDZNP;
        private System.Windows.Forms.MaskedTextBox tvzTSDZNP;
        private System.Windows.Forms.Label label236;
        private System.Windows.Forms.Label label237;
        private System.Windows.Forms.Label label238;
        private System.Windows.Forms.Label label239;
        private System.Windows.Forms.Label label240;
        private System.Windows.Forms.Label label241;
        private System.Windows.Forms.Label label242;
        private System.Windows.Forms.Label label243;
        private System.Windows.Forms.Label label244;
        private System.Windows.Forms.ComboBox blockSwitchDZNP;
        private System.Windows.Forms.ComboBox deblockDZNP;
        private System.Windows.Forms.ComboBox controlDZNP;
        private System.Windows.Forms.ComboBox blockTSDZNP;
        private System.Windows.Forms.GroupBox groupBox46;
        private System.Windows.Forms.CheckBox zoneDZNP;
        private System.Windows.Forms.MaskedTextBox tficsDZNP;
        private System.Windows.Forms.MaskedTextBox treverseDZNP;
        private System.Windows.Forms.Label label245;
        private System.Windows.Forms.Label label246;
        private System.Windows.Forms.Label label247;
        private System.Windows.Forms.Label label248;
        private System.Windows.Forms.Label label249;
        private System.Windows.Forms.ComboBox blockReverseDZNP;
        private System.Windows.Forms.ComboBox modeReverseDZNP;
        private System.Windows.Forms.GroupBox groupBox58;
        private System.Windows.Forms.MaskedTextBox uminDZNP;
        private System.Windows.Forms.MaskedTextBox tsrabTSDZNP;
        private System.Windows.Forms.Label label250;
        private System.Windows.Forms.Label label251;
        private System.Windows.Forms.Label label252;
        private System.Windows.Forms.Label label253;
        private System.Windows.Forms.Label label254;
        private System.Windows.Forms.ComboBox blockTnDZNP;
        private System.Windows.Forms.ComboBox blockEchoDZNP;
        private System.Windows.Forms.ComboBox modeEchoDZNP;
        private System.Windows.Forms.GroupBox groupBox59;
        private System.Windows.Forms.DataGridView _commandsDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewComboBoxColumn _typeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _passCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _jsCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn16;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn44;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn45;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn46;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn48;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn49;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn17;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn18;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn51;
        private System.Windows.Forms.DataGridViewComboBoxColumn _avrColumnIStar;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn7;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn7;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn8;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn9;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn8;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn9;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn10;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn10;
        private System.Windows.Forms.DataGridViewComboBoxColumn _avrColumnUM;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn59;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn61;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn26;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn27;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn62;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn63;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn64;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn30;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn28;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn65;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn31;
        private System.Windows.Forms.DataGridViewComboBoxColumn _avrColumnUL;
        private System.Windows.Forms.TabPage _defenceI;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.GroupBox _I14GroupBox;
        private System.Windows.Forms.DataGridView _difensesI1DataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _iModesColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iIColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iUStartColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _iUstartYNColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _blockOtTn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _iDirectColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _iUnDirectColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _iLogicColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _iCharColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iTColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iKColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _inUsk;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iTyColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _iBlockingColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iI2I1Column;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _iI2I1YNColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _iBlockingDirectColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _nenaprUsk;
        private System.Windows.Forms.DataGridViewComboBoxColumn _iOscModeColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _iUROVModeColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _iAPVModeColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _avrColumnI14;
        private System.Windows.Forms.GroupBox _ILGroupBox;
        private System.Windows.Forms.DataGridView _difensesI3DataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _i8StageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i8ModesColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _i8IColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn _i8UStartColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _i8UstartYNColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i8DirectColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i8UnDirectColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i8LogicColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i8CharColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _i8TColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _i8KColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _i8TyColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i8BlockingColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _i8I2I1Column;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i8I2I1YNColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i8BlockingDirectColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column6;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i8OscModeColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _i8UROVModeColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i8APVModeColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _avrColumnIL;
        private System.Windows.Forms.GroupBox _I56GroupBox;
        private System.Windows.Forms.DataGridView _difensesI2DataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn88;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewComboBoxColumn _conditionColumnI56;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn71;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn89;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn90;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn91;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn92;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn93;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn72;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn73;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn94;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn74;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn95;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn75;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn15;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn96;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn19;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn97;
        private System.Windows.Forms.DataGridViewComboBoxColumn _avrColumnI56;
        private System.Windows.Forms.CheckBox _fault7CheckBox;
        private System.Windows.Forms.Label _vchsFaultLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn84;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn105;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn85;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn106;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn107;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn108;
        private System.Windows.Forms.DataGridViewComboBoxColumn _avrColumnEngine;
        private System.Windows.Forms.ComboBox _polarIn1;
        private System.Windows.Forms.ComboBox _polarIn;
        private System.Windows.Forms.ComboBox _polarIc;
        private System.Windows.Forms.ComboBox _polarIb;
        private System.Windows.Forms.ComboBox _polarIa;
        private System.Windows.Forms.Label label255;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn68;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn80;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn81;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn69;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn70;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn82;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn44;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn83;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn84;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn47;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn45;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn85;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn48;
        private System.Windows.Forms.DataGridViewComboBoxColumn _avrColumnExt;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn60;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn71;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn72;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn61;
        private System.Windows.Forms.DataGridViewTextBoxColumn _u1Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn63;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn64;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn65;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn37;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn73;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn74;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn40;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn38;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn75;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn41;
        private System.Windows.Forms.DataGridViewComboBoxColumn _avrColumnFL;
        private System.Windows.Forms.ComboBox _inpIn;
        private System.Windows.Forms.Label label256;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn54;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn66;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn67;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn55;
        private System.Windows.Forms.DataGridViewTextBoxColumn _u1Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn57;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn58;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn59;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn32;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn68;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn69;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn35;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn33;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn70;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn36;
        private System.Windows.Forms.DataGridViewComboBoxColumn _avrColumnFM;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewComboBoxColumn _baseCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _oscSygnal;
        private System.Windows.Forms.GroupBox _signaturesForOscGroupBox;
        private Forms.SignaturesOscControl signaturesOsc;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.DataGridView _rsTriggersDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn83;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn102;
        private System.Windows.Forms.DataGridViewComboBoxColumn _baseRColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _sygnalsRColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _baseSColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _sColumn;
    }
}