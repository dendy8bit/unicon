﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MRUNIVERSAL.Configuration.Structures.MeasuringTransformer
{
    /// <summary>
    /// Конфигурация измерительного трансформатора I
    /// </summary>
    public class KanalITransStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _ittl; //конфигурация ТТ - номинальный первичный ток, конфигурация ТН - коэфициэнт
        [Layout(1)] private ushort _ittx; //конфигурация ТТНП - номинальный первичный ток нулувой последовательности, конфигурация ТННП - коэфициэнт
        [Layout(2)] private ushort _ittx1;//конфигурация - номинальный первичный ток In1, конфигурация - коэфициэнт Un1
        [Layout(3)] private ushort _polarityL; //резерв, вход внешней неисправности тн (трансформатора напряжения)
        [Layout(4)] private ushort _polarityX;//резерв, вход внешней неисправности тн (трансформатора напряжения нулевой последовательности)
        [Layout(5)] private ushort _binding; //(для трансформатора тока тип ТТ, для трансформатора напряжения тип ТН),71 бит - токовый вход(1А или 5А)
        [Layout(6)] private ushort _imax; //max ток нагрузки,резерв
        [Layout(7)] private ushort _automattn1; //резерв для I, для U вход внешней неисправности Un1

        #endregion [Private fields]
        

        #region [Properties]
        /// <summary>
        /// тип ТТ
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "тип_ТТ")]
        public string ItypeXml
        {
            get { return Validator.Get(this._binding, Strings.TtType, 0); }
            set { this._binding = Validator.Set(value,Strings.TtType, this._binding, 0) ; }
        }

        /// <summary>
        /// Чередование фаз
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Чередование_фаз")]
        public string PhaseRotation
        {
            get { return Validator.Get(this._binding, Strings.Rotation, 5); }
            set { this._binding = Validator.Set(value, Strings.Rotation, this._binding, 5); }
        }

        /// <summary>
        /// Токовый вход
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Токовый_вход")]
        public string InpIn
        {
            get { return Validator.Get(this._binding, Strings.InpIn, 7); }
            set { this._binding = Validator.Set(value, Strings.InpIn, this._binding, 7); }
        }

        /// <summary>
        /// Iм
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Iм")]
        public double Im
        {
            get { return ValuesConverterCommon.GetIn(this._imax); }
            set { this._imax = ValuesConverterCommon.SetIn(value); }
        }

        /// <summary>
        /// конфигурация ТТ
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "конфигурация_ТТ")]
        public ushort Ittl
        {
            get { return this._ittl; }
            set { this._ittl = value; }
        }
        
        [BindingProperty(5)]
        [XmlElement(ElementName = "Полярность_Ia")]
        public string PolarIa
        {
            get { return Validator.Get(this._binding, Strings.Polar, 15); }
            set { this._binding = Validator.Set(value, Strings.Polar, this._binding, 15); }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "Полярность_Ib")]
        public string PolarIb
        {
            get { return Validator.Get(this._binding, Strings.Polar, 14); }
            set { this._binding = Validator.Set(value, Strings.Polar, this._binding, 14); }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "Полярность_Ic")]
        public string PolarIc
        {
            get { return Validator.Get(this._binding, Strings.Polar, 13); }
            set { this._binding = Validator.Set(value, Strings.Polar, this._binding, 13); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Полярность_In")]
        public string PolarIn
        {
            get { return Validator.Get(this._binding, Strings.Polar, 12); }
            set { this._binding = Validator.Set(value, Strings.Polar, this._binding, 12); }
        }

        /// <summary>
        /// конфигурация ТТНП
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "конфигурация_ТТНП")]
        public ushort Ittx
        {
            get { return this._ittx; }
            set { this._ittx = value; }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Полярность_In1")]
        public string PolarIn1
        {
            get { return Validator.Get(this._binding, Strings.Polar, 11); }
            set { this._binding = Validator.Set(value, Strings.Polar, this._binding, 11); }
        }

        public int InpInValue
        {
            get { return Common.GetBit(this._binding, 7) ? 5 : 1; }
        }
        #endregion [Properties]
    }
}
