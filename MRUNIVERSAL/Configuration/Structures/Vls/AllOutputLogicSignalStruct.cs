﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MRUNIVERSAL.Configuration.Structures.Vls
{
    /// <summary>
    /// конфигурациия выходных логических сигналов
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "Конфигурация_ВЛС")]
    public class AllOutputLogicSignalStruct : StructBase, IXmlSerializable
    {
        public const int LOGIC_COUNT = 16;

        [XmlArray(ElementName = "Все_ВЛС")] [Layout(0, Count = LOGIC_COUNT)] private OutputLogicStruct[] _logicSignals;


        [BindingProperty(0)]
        [XmlIgnore]
        public OutputLogicStruct this[int index]
        {
            get { return this._logicSignals[index]; }
            set { this._logicSignals[index] = value; }
        }
        
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {

        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            for (int i = 0; i < LOGIC_COUNT; i++)
            {

                writer.WriteStartElement("ВЛС");
                this[i].WriteXml(writer);
                writer.WriteEndElement();
            }
        }
    }
}
