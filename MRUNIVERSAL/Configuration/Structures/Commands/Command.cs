﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MRUNIVERSAL.Configuration.Structures.Commands
{
    public class Command : StructBase
    {
        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Тип")]
        public string TypeCommand
        {
            get { return Validator.Get(CommandType, Strings.ImpDlit); }
            set { this.CommandType = Validator.Set(value, Strings.ImpDlit); }
        }

        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Пароль")]
        public bool PasswordCommand
        {
            get { return this.PasswordCommandValue; }
            set { this.PasswordCommandValue = value; }
        }


        [BindingProperty(2)]
        [XmlAttribute(AttributeName = "ЖС")]
        public bool JSCommand
        {
            get { return this.JSCommandValue; }
            set { this.JSCommandValue = value; }
        }

        public ushort CommandType { get; set; }

        public bool PasswordCommandValue { get; set; }
        public bool JSCommandValue { get; set; }

    }
}
