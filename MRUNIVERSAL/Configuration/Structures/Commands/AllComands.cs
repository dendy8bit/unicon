﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.MBServer;

namespace BEMN.MRUNIVERSAL.Configuration.Structures.Commands
{
    public class AllComands : StructBase, IDgvRowsContainer<Command>
    {
        [Layout(0, Count = 6)] public ushort[] _commands;

        [XmlArray(ElementName = "Все_команды")]
        public Command[] Rows
        {
            get { return GetCommand(); }
            set { SetCommand(value); }
        }

        public Command[] GetCommand()
        {
            List<Command> command = new List<Command>();
            for (int i = 0; i < 6; i++)
            {
                command.Add(new Command
                {
                    CommandType = (ushort) (Common.GetBits(_commands[i], 0) >> 0),
                    PasswordCommand = Common.GetBit(_commands[i], 1),
                    JSCommand = Common.GetBit(_commands[i], 2)
                });
                command.Add(new Command
                {
                    CommandType = (ushort)(Common.GetBits(_commands[i], 4) >> 4),
                    PasswordCommand = Common.GetBit(_commands[i], 5),
                    JSCommand = Common.GetBit(_commands[i], 6)
                });
                command.Add(new Command
                {
                    CommandType = (ushort)(Common.GetBits(_commands[i], 8) >> 8),
                    PasswordCommand = Common.GetBit(_commands[i], 9),
                    JSCommand = Common.GetBit(_commands[i], 10)
                });
                command.Add(new Command
                {
                    CommandType = (ushort)(Common.GetBits(_commands[i], 12) >> 12),
                    PasswordCommand = Common.GetBit(_commands[i], 13),
                    JSCommand = Common.GetBit(_commands[i], 14)
                });
            }
            return command.ToArray();
        }

        public void SetCommand(Command[] value)
        {
            int i = 0;

            List<ushort> records = new List<ushort>();
            List<ushort> recordsInWord = new List<ushort>();

            ushort valueTypeCommandFirst = 0;
            ushort valuePasswordCommandFirst = 0;
            ushort valueJsCommandFirst = 0;

            ushort valueTypeCommandSecond = 0;
            ushort valuePasswordCommandSecond = 0;
            ushort valueJsCommandSecond = 0;

            ushort valueTypeCommandThird = 0;
            ushort valuePasswordCommandThird = 0;
            ushort valueJsCommandThird = 0;

            ushort valueTypeCommandFourth = 0;
            ushort valuePasswordCommandFourth = 0;
            ushort valueJsCommandFourth = 0;


            foreach (var command in value)
            {

                if (i == 0 || i == 4 || i == 8 || i == 12 || i == 16 || i == 20)
                {
                    valueTypeCommandFirst = Validator.Set(command.TypeCommand, Strings.ImpDlit, valueTypeCommandFirst, 0);
                    valuePasswordCommandFirst = Common.SetBit(valuePasswordCommandFirst, 1, command.PasswordCommand);
                    valueJsCommandFirst = Common.SetBit(valueJsCommandFirst, 2, command.JSCommand);

                    records.Add((ushort)(valueTypeCommandFirst + valuePasswordCommandFirst + valueJsCommandFirst));
                }

                if (i == 1 || i == 5 || i == 9 || i == 13 || i == 17 || i == 21)
                {
                    valueTypeCommandSecond = Validator.Set(command.TypeCommand, Strings.ImpDlit, valueTypeCommandSecond, 4);
                    valuePasswordCommandSecond = Common.SetBit(valuePasswordCommandSecond, 5, command.PasswordCommand);
                    valueJsCommandSecond = Common.SetBit(valueJsCommandSecond, 6, command.JSCommand);

                    records.Add((ushort)(valueTypeCommandSecond + valuePasswordCommandSecond + valueJsCommandSecond));
                }

                if (i == 2 || i == 6 || i == 10 || i == 14 || i == 18 || i == 22)
                {
                    valueTypeCommandThird = Validator.Set(command.TypeCommand, Strings.ImpDlit, valueTypeCommandThird, 8);
                    valuePasswordCommandThird = Common.SetBit(valuePasswordCommandThird, 9, command.PasswordCommand);
                    valueJsCommandThird = Common.SetBit(valueJsCommandThird, 10, command.JSCommand);

                    records.Add((ushort)(valueTypeCommandThird + valuePasswordCommandThird + valueJsCommandThird));
                }

                if (i == 3 || i == 7 || i == 11 || i == 15 || i == 19 || i == 23)
                {
                    valueTypeCommandFourth = Validator.Set(command.TypeCommand, Strings.ImpDlit, valueTypeCommandFourth, 12);
                    valuePasswordCommandFourth = Common.SetBit(valuePasswordCommandFourth, 13, command.PasswordCommand);
                    valueJsCommandFourth = Common.SetBit(valueJsCommandFourth, 14, command.JSCommand);

                    records.Add((ushort)(valueTypeCommandFourth + valuePasswordCommandFourth + valueJsCommandFourth));
                }

                i++;
            }

            //Translate record in word
            for (int j = 0; j < value.Length; j += 4)
            {
                recordsInWord.Add((ushort)(records[j] + records[j + 1] + records[j + 2] + records[j + 3]));
            }

            //Write record
            for (int j = 0; j < 6; j++)
            {
                _commands[j] = recordsInWord[j];
            }
        }
    }
}
