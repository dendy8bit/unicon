﻿using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MRUNIVERSAL.Configuration.Structures.Goose
{
    public class GooseConfig : StructBase, ISetpointContainer<MRUNIVERSAL.Configuration.Structures.Goose.Goose>
    {
        public const int GOOSE_COUNT = 16;

        [Layout(0)] ushort _config1;
        [Layout(1)] ushort _config2;
        [Layout(2, Count = GOOSE_COUNT)] GooseInput[] _gin;

        public MRUNIVERSAL.Configuration.Structures.Goose.Goose[] Setpoints
        {
            get
            {
                List<MRUNIVERSAL.Configuration.Structures.Goose.Goose> ret = this._gin.Select((g, i) => new MRUNIVERSAL.Configuration.Structures.Goose.Goose {GooseConfig = this.GetConfig(i), GooseInputs = g.GoIn}).ToList();
                return ret.ToArray();
            }
            set
            {
                for (int i = 0; i < value.Length; i++)
                {
                    this.SetConfig(value[i].GooseConfig, i);
                    this._gin[i].GoIn = value[i].GooseInputs;
                }
            }
        }

        private string GetConfig(int ind)
        {
            return Validator.Get(this._config1, Strings.GooseConfig, ind);
        }

        private void SetConfig(string s, int ind)
        {
            this._config1 = Validator.Set(s, Strings.GooseConfig, this._config1, ind);
        }
    }
}
