﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MRUNIVERSAL.Configuration.Structures.AntiBounce;
using BEMN.MRUNIVERSAL.Configuration.Structures.Commands;
using BEMN.MRUNIVERSAL.Configuration.Structures.ConfigSystem;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.InputSignals;
using BEMN.MRUNIVERSAL.Configuration.Structures.Goose;
using BEMN.MRUNIVERSAL.Configuration.Structures.Oscope;
using BEMN.MRUNIVERSAL.Configuration.Structures.RelayInd;
using BEMN.MRUNIVERSAL.Configuration.Structures.Switch;
using BEMN.MRUNIVERSAL.Configuration.Structures.UROV;
using BEMN.MRUNIVERSAL.Configuration.Structures.Vls;

namespace BEMN.MRUNIVERSAL.Configuration.Structures
{
    [Serializable]
    [XmlRoot(ElementName = "МР_Все")]
    public class ConfigurationStruct : StructBase
    {
        [XmlElement(ElementName = "Версия")]
        public double DeviceVersion { get; set; }

        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }

        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType { get; set; }

        [XmlElement(ElementName = "Аппаратная_часть")]
        public string DevicePlant { get; set; }

        [XmlElement(ElementName = "Первичные_уставки")]
        public bool Primary { get; set; }

        [XmlElement(ElementName = "Размер_осциллограмы")]
        public string SizeOsc { get; set; }

        [XmlElement(ElementName = "Группа")]
        public int Group { get; set; }

        [Layout(0)] private AllGroupSetpointStruct _allGroupSetpoints;  //все защиты
        [Layout(1)] private SwitchStruct _sw;
        [Layout(2)] private InputSignalStruct _impsg;
        [Layout(3)] private OscopeStruct _osc;
        [Layout(4)] private AutomaticsParametersStruct _automatics;
        [Layout(5)] private ConfigRs485Struct _configNet;
        [Layout(6)] private ConfigIpStruct _configIPStruct;
        [Layout(7)] private ConfigRs485Struct _configNet1; //дополнительно для конфиг 2-го 485
        [Layout(8, Count = 122, Ignore = true)] private ushort[] _res;
        [Layout(9)] private ConfigAddStruct _cnfAdd;
        [Layout(10)] private UrovStruct _urov;
        [Layout(11)] private GooseConfig _gooseConfig;
        [Layout(12)] private AllOutputLogicSignalStruct _outputLogicSignal;
        [Layout(13)] private AllAntiBounce _allAntiBounce;
        [Layout(14)] private AllComands _allCommands;

        /// <summary>
        /// Группы уставок
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_всех_групп_уставок")]
        [BindingProperty(0)]
        public AllGroupSetpointStruct AllGroupSetpoints
        {
            get { return this._allGroupSetpoints; }
            set { this._allGroupSetpoints = value; }
        }

        /// <summary>
        /// конфигурациия выключателя
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_выключателя")]
        [BindingProperty(1)]
        public SwitchStruct Sw
        {
            get { return this._sw; }
            set { this._sw = value; }
        }
        /// <summary>
        /// конфигурациия входных сигналов
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_входных_сигналов")]
        [BindingProperty(2)]
        public InputSignalStruct Impsg
        {
            get { return this._impsg; }
            set { this._impsg = value; }
        }
        /// <summary>
        /// конфигурациия осцилографа
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_осцилографа")]
        [BindingProperty(3)]
        public OscopeStruct Osc
        {
            get { return this._osc; }
            set { this._osc = value; }
        }

        [XmlElement(ElementName = "Конфигурация_реле,индикаторов,неисправностей")]
        [BindingProperty(4)]
        public AutomaticsParametersStruct Automatics
        {
            get { return this._automatics; }
            set { this._automatics = value; }
        }

        [XmlElement(ElementName = "IP")]
        [BindingProperty(5)]
        public ConfigIpStruct ConfigIPStruct
        {
            get { return this._configIPStruct; }
            set { this._configIPStruct = value; }
        }

        [XmlElement(ElementName = "Вход_опорного_канала")]
        [BindingProperty(6)]
        public ConfigAddStruct ConfigAdd
        {
            get { return this._cnfAdd; }
            set { this._cnfAdd = value; }
        }

        /// <summary>
        /// УРОВ
        /// </summary>
        [XmlElement(ElementName = "УРОВ")]
        [BindingProperty(7)]
        public UrovStruct Urov
        {
            get { return this._urov; }
            set { this._urov = value; }
        }

        /// <summary>
        /// Конфигурация гусов
        /// </summary>
        [XmlElement(ElementName = "Гусы")]
        [BindingProperty(8)]
        public GooseConfig GooseConfig
        {
            get { return this._gooseConfig; }
            set { this._gooseConfig = value; }
        }

        /// <summary>
        /// Конфигурация ВЛС
        /// </summary>
        [XmlElement(ElementName = "Выходные_логические_сигналы")]
        [BindingProperty(9)]
        public AllOutputLogicSignalStruct OutputLogicSignal
        {
            get { return this._outputLogicSignal; }
            set { this._outputLogicSignal = value; }
        }

        /// <summary>
        /// Конфигурация Антидребезгов
        /// </summary>
        [XmlElement(ElementName = "Антидребезги")]
        [BindingProperty(10)]
        public AllAntiBounce AllAntiBounce
        {
            get { return this._allAntiBounce; }
            set { this._allAntiBounce = value; }
        }

        /// <summary>
        /// Конфигурация Комманд
        /// </summary>
        [XmlElement(ElementName = "Комманды")]
        [BindingProperty(11)]
        public AllComands AllComands
        {
            get { return this._allCommands; }
            set { this._allCommands = value; }
        }
    }
}
