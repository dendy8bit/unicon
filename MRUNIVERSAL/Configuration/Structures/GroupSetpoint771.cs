﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.AcCountLoad;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Apv;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.CheckTn;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Engine;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.HFL;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Ls;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Omp;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Sihronizm;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Swing;
using BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Z;
using BEMN.MRUNIVERSAL.Configuration.Structures.MeasuringTransformer;

namespace BEMN.MRUNIVERSAL.Configuration.Structures
{
    public class GroupSetpoint771 : StructBase
    {
        #region Fields
        [Layout(0)] private DefensesSetpointsStruct771 _defensesSetpoints;
        [Layout(1)] private ApvStruct _apv;
        [Layout(2)] private AllInputLogicStruct _inputLogicSignal;
        [Layout(3)] private ResistanceStruct _resistanceParam;
        [Layout(4)] private AcCountLoadStruct _acCountLoad;
        [Layout(5)] private CheckTnStruct _checkTn;
        [Layout(6)] private SwingStruct _swing;
        [Layout(7)] private TermConfigStruct _termConfig;
        [Layout(8)] private MeasureTransStruct _measureTrans;
        [Layout(9)] private SinhronizmStruct _sinhronizm;
        [Layout(10)] private ConfigurationOpmStruct _omp;
        [Layout(11)] private DzStruct _hfl1;
        [Layout(12)] private TznpStruct _hfl2;
        #endregion

        #region Property
        [BindingProperty(0)]
        public DefensesSetpointsStruct771 DefensesSetpoints
        {
            get { return this._defensesSetpoints; }
            set { this._defensesSetpoints = value; }
        }
        
        [BindingProperty(1)]
        public ResistanceStruct ResistanceParam
        {
            get { return this._resistanceParam; }
            set { this._resistanceParam = value; }
        }

        [BindingProperty(2)]
        public AcCountLoadStruct AcCountLoad
        {
            get { return this._acCountLoad; }
            set { this._acCountLoad = value; }
        }

        [BindingProperty(3)]
        public CheckTnStruct CheckTn
        {
            get { return this._checkTn; }
            set { this._checkTn = value; }
        }

        [BindingProperty(4)]
        public SwingStruct Swing
        {
            get { return this._swing; }
            set { this._swing = value; }
        }

        [BindingProperty(5)]
        public ApvStruct Apv
        {
            get { return this._apv; }
            set { this._apv = value; }
        }

        [BindingProperty(6)]
        public TermConfigStruct TermConfig
        {
            get { return this._termConfig; }
            set { this._termConfig = value; }
        }

        [BindingProperty(7)]
        public MeasureTransStruct MeasureTrans
        {
            get { return this._measureTrans; }
            set { this._measureTrans = value; }
        }

        [BindingProperty(8)]
        public AllInputLogicStruct InputLogicSignal
        {
            get { return this._inputLogicSignal; }
            set { this._inputLogicSignal = value; }
        }

        [BindingProperty(9)]
        public SinhronizmStruct Sinhronizm
        {
            get { return this._sinhronizm; }
            set { this._sinhronizm = value; }
        }

        [BindingProperty(10)]
        public ConfigurationOpmStruct Omp
        {
            get { return this._omp; }
            set { this._omp = value; }
        }

        [BindingProperty(11)]
        public DzStruct Hfl1
        {
            get { return this._hfl1; }
            set { this._hfl1 = value; }
        }

        [BindingProperty(12)]
        public TznpStruct Hfl2
        {
            get { return this._hfl2; }
            set { this._hfl2 = value; }
        }

        #endregion Property
    }
}
