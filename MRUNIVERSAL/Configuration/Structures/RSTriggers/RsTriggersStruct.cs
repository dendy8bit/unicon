﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MRUNIVERSAL.Configuration.Structures.RSTriggers
{
    [XmlType(TypeName = "Один_триггер")]
    public class RsTriggersStruct : StructBase
    {
        [Layout(0)] private ushort _set;
        [Layout(1)] private ushort _reset;
        [Layout(2)] private ushort _config;
        [Layout(3)] private ushort _rez;

        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Тип")]
        public string TypeRs
        {
            get { return Validator.Get(this._config, Strings.RSPriority, 0); }
            set { this._config = Validator.Set(value, Strings.RSPriority, _config, 0); }
        }

        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "База1")]
        public string Base1
        {
            get { return Validator.Get(this._config, Strings.OscBases, 12, 13); }
            set { this._config = Validator.Set(value, Strings.OscBases, this._config, 12, 13); }
        }

        [BindingProperty(2)]
        [XmlAttribute(AttributeName = "Вход_R")]
        public string InputR
        {
            get
            {
                Dictionary<ushort, string> list = Strings.OscChannelSignals[CurrentBase(Base1)];
                return Validator.Get(this._reset, list);
            }
            set
            {
                Dictionary<ushort, string> list = Strings.OscChannelSignals[CurrentBase(Base1)];
                this._reset = Validator.Set(value, list);
            }
        }

        [BindingProperty(3)]
        [XmlAttribute(AttributeName = "База2")]
        public string Base2
        {
            get { return Validator.Get(this._config, Strings.OscBases, 14, 15); }
            set { this._config = Validator.Set(value, Strings.OscBases, this._config, 14, 15); }
        }

        [BindingProperty(4)]
        [XmlAttribute(AttributeName = "Вход_S")]
        public string InputS
        {
            get
            {
                Dictionary<ushort, string> list = Strings.OscChannelSignals[CurrentBase(Base2)];
                return Validator.Get(this._set, list);
            }
            set
            {
                Dictionary<ushort, string> list = Strings.OscChannelSignals[CurrentBase(Base2)];
                this._set = Validator.Set(value, list);
            }
        }

        private int CurrentBase(string baseString)
        {
            int indexBase = Convert.ToInt32(Regex.Replace(baseString, @"[^\d]+", ""));
            return indexBase - 1;
        }

    }
}
