﻿using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Avr
{
    /// <summary>
    /// Конфигурация АВР
    /// </summary>
    public class AvrStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _config; //конфигурация
        [Layout(1)] private ushort _block; //вход блокировки АВР
        [Layout(2)] private ushort _clear; //вход сброс блокировки АВР
        [Layout(3)] private ushort _start; //вход сигнала запуск АВР
        [Layout(4)] private ushort _on; //вход АВР срабатывания
        [Layout(5)] private ushort _timeOn; //время АВР срабатывания
        [Layout(6)] private ushort _off; //вход АВР возврат
        [Layout(7)] private ushort _timeOff; //время АВР возврат
        [Layout(8)] private ushort _timeOtkl; //задержка отключения резерва
        [Layout(9)] private ushort _res1; // Запрет реклоузера
        [Layout(10)] private ushort _res2;
        [Layout(11)] private ushort _res3;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// от сигнала
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "от_сигнала")]
        public string BySignalXml
        {
            get
            {
                string ret = Validator.Get(this._config, Strings.BeNo, 0);
                return !Common.GetBit(_config, 4) ? ret : Strings.BeNo.First();
            }
            set
            {
                if(!Common.GetBit(_config, 4)) this._config = Validator.Set(value, Strings.BeNo, this._config, 0);
            }
        }

        /// <summary>
        /// по отключению
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "по_отключению")]
        public string ByOffXml
        {
            get { return Validator.Get(this._config, Strings.BeNo, 2); }
            set { this._config = Validator.Set(value, Strings.BeNo, this._config, 2); }
        }

        /// <summary>
        /// по самоотключению
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "по_самоотключению")]
        public string BySelfOffXml
        {
            get
            {
                string ret = Validator.Get(this._config, Strings.BeNo, 1);
                return !Common.GetBit(_config, 4) ? ret : Strings.BeNo.First();
            }
            set { if (!Common.GetBit(_config, 4)) this._config = Validator.Set(value, Strings.BeNo, this._config, 1); }
        }

        /// <summary>
        /// по защите
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "по_защите")]
        public string ByDefenseXml
        {
            get { return Validator.Get(this._config, Strings.BeNo, 3); }
            set { this._config = Validator.Set(value, Strings.BeNo, this._config, 3); }
        }
        /// <summary>
        /// Пуск
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Пуск")]
        public string StartXml
        {
            get
            {
                string ret = Validator.Get(this._start, Strings.SwitchSignals);
                return !Common.GetBit(_config, 4) ? ret : Strings.SwitchSignals.First().Value;
            }
            set { if (!Common.GetBit(_config, 4)) this._start = Validator.Set(value, Strings.SwitchSignals); }
        }
        /// <summary>
        /// вход блокировки АВР
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "вход_блокировки_АВР")]
        public string BlockingXml
        {
            get
            {
                string ret = Validator.Get(this._block, Strings.SwitchSignals);
                return !Common.GetBit(_config, 4) ? ret : Strings.SwitchSignals.First().Value;
            }
            set { if (!Common.GetBit(_config, 4)) this._block = Validator.Set(value, Strings.SwitchSignals); }
        }

        /// <summary>
        /// вход сброс блокировки АВР
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "вход_сброс_блокировки_АВР")]
        public string ClearXml
        {
            get
            {
                string ret = Validator.Get(this._clear, Strings.SwitchSignals);
                return !Common.GetBit(_config, 4) ? ret : Strings.SwitchSignals.First().Value;
            }
            set { if (!Common.GetBit(_config, 4)) this._clear = Validator.Set(value, Strings.SwitchSignals); }
        }


        /// <summary>
        /// вход АВР срабатывания
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "вход_АВР_срабатывания")]
        public string OnXml
        {
            get
            {
                string ret = Validator.Get(this._on, Strings.SwitchSignals);
                return !Common.GetBit(_config, 4) ? ret : Strings.SwitchSignals.First().Value;
            }
            set { if (!Common.GetBit(_config, 4)) this._on = Validator.Set(value, Strings.SwitchSignals); }
        }

        /// <summary>
        /// вход АВР возврат
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "вход_АВР_возврат")]
        public string BackXml
        {
            get
            {
                string ret = Validator.Get(this._off, Strings.SwitchSignals);
                return !Common.GetBit(_config, 4) ? ret : Strings.SwitchSignals.First().Value;
            }
            set { if (!Common.GetBit(_config, 4)) this._off = Validator.Set(value, Strings.SwitchSignals); }
        }

        /// <summary>
        /// время АВР срабатывания
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "время_АВР_срабатывания")]
        public int TimeOn
        {
            get
            {
                int ret = ValuesConverterCommon.GetWaitTime(this._timeOn);
                return !Common.GetBit(_config, 4) ? ret : 0;
            }
            set { if (!Common.GetBit(_config, 4)) this._timeOn = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// время АВР возврат
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "время_АВР_возврат")]
        public int TimeBack
        {
            get
            {
                int ret = ValuesConverterCommon.GetWaitTime(this._timeOff);
                return !Common.GetBit(_config, 4) ? ret : 0;
            }
            set { if (!Common.GetBit(_config, 4)) this._timeOff = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// задержка отключения резерва
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "задержка_отключения_резерва")]
        public int TimeOtkl
        {
            get
            {
                int ret = ValuesConverterCommon.GetWaitTime(this._timeOtkl);
                return !Common.GetBit(_config, 4) ? ret : 0;
            }
            set { if (!Common.GetBit(_config, 4)) this._timeOtkl = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Сброс
        /// </summary>
        [BindingProperty(12)]
        [XmlElement(ElementName = "Сброс")]
        public string ResetXml
        {
            get { return Validator.Get(this._config, Strings.ForbiddenAllowed, 7); }
            set { this._config = Validator.Set(value, Strings.ForbiddenAllowed, this._config, 7); }
        }

        /// <summary>
        /// Сброс
        /// </summary>
        [BindingProperty(13)]
        [XmlElement(ElementName = "Тип_АВР")]
        public string TypeAVR
        {
            get { return Validator.Get(this._config, Strings.TypeAVR, 4); }
            set { this._config = Validator.Set(value, Strings.TypeAVR, this._config, 4); }
        }

        /// <summary>
        /// Режим реклозера
        /// </summary>
        [BindingProperty(14)]
        [XmlElement(ElementName = "Режим_рек")]
        public string ModeRec
        {
            get
            {
                string ret = Validator.Get(this._config, Strings.ModesLight, 0);
                return TypeAVR == Strings.TypeAVR.Last() ? ret : Strings.ModesLight.First();
            }
            set
            {
                if(Common.GetBit(_config, 4)) this._config = Validator.Set(value, Strings.ModesLight, this._config, 0);
            }
        }

        /// <summary>
        /// Блокировка по самоотключению
        /// </summary>
        [BindingProperty(15)]
        [XmlElement(ElementName = "Блокировка_рек_по_самооткл")]
        public bool BlockSelfRec
        {
            get
            {
                bool ret = Common.GetBit(this._config, 1);
                return TypeAVR == Strings.TypeAVR.Last() && ret;
            }
            set { if (Common.GetBit(_config, 4)) this._config = Common.SetBit(this._config, 1, value); }
        }

        /// <summary>
        /// Блокировка СДТУ
        /// </summary>
        [BindingProperty(16)]
        [XmlElement(ElementName = "Блокировка_рек_СДТУ")]
        public string BlockRecSDTURec
        {
            get
            {
                string ret = Validator.Get(this._start, Strings.SwitchSignals);
                return TypeAVR == Strings.TypeAVR.Last() ? ret : Strings.SwitchSignals.First().Value;
            }
            set { if (Common.GetBit(_config, 4)) this._start = Validator.Set(value, Strings.SwitchSignals); }
        }

        /// <summary>
        /// Ключ
        /// </summary>
        [BindingProperty(17)]
        [XmlElement(ElementName = "Ключ_рек")]
        public string KeyRec
        {
            get
            {
                string ret = Validator.Get(this._clear, Strings.SwitchSignals);
                return TypeAVR == Strings.TypeAVR.Last() ? ret : Strings.SwitchSignals.First().Value;
            }
            set { if (Common.GetBit(_config, 4)) this._clear = Validator.Set(value, Strings.SwitchSignals); }
        }

        /// <summary>
        /// Запрет
        /// </summary>
        [BindingProperty(18)]
        [XmlElement(ElementName = "Запрет_рек")]
        public string BanRec
        {
            get
            {
                string ret = Validator.Get(this._res1, Strings.SwitchSignals);
                return TypeAVR == Strings.TypeAVR.Last() ? ret : Strings.SwitchSignals.First().Value;
            }
            set { this._res1 = Validator.Set(value, Strings.SwitchSignals); }
        }

        /// <summary>
        /// Время срабатывания
        /// </summary>
        [BindingProperty(19)]
        [XmlElement(ElementName = "Время_срабатывания_рек")]
        public int TimeTriggerRec
        {
            get
            {
                int ret = ValuesConverterCommon.GetWaitTime(this._timeOn);
                return TypeAVR == Strings.TypeAVR.Last() ? ret : 0;
            }
            set { if (Common.GetBit(_config, 4)) this._timeOn = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Время готовности
        /// </summary>
        [BindingProperty(20)]
        [XmlElement(ElementName = "Время_готовности_рек")]
        public int TimeReady
        {
            get
            {
                int ret = ValuesConverterCommon.GetWaitTime(this._timeOff);
                return TypeAVR == Strings.TypeAVR.Last() ? ret : 0;
            }
            set { if (Common.GetBit(_config, 4)) this._timeOff = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Вход U1
        /// </summary>
        [BindingProperty(21)]
        [XmlElement(ElementName = "Вход_U1_рек")]
        public string InputU1Rec
        {
            get
            {
                string ret = Validator.Get(this._off, Strings.InputU);
                return TypeAVR == Strings.TypeAVR.Last() ? ret : Strings.InputU.First();
            }
            set { if (Common.GetBit(_config, 4)) this._off = Validator.Set(value, Strings.InputU); }
        }

        /// <summary>
        /// Вход U2
        /// </summary>
        [BindingProperty(22)]
        [XmlElement(ElementName = "Вход_U2_рек")]
        public string InputU2Rec
        {
            get
            {
                string ret = Validator.Get(this._timeOtkl, Strings.InputU);
                return TypeAVR == Strings.TypeAVR.Last() ? ret : Strings.InputU.First();
            }
            set { if (Common.GetBit(_config, 4)) this._timeOtkl = Validator.Set(value, Strings.InputU); }
        }

        /// <summary>
        /// U max
        /// </summary>
        [BindingProperty(23)]
        [XmlElement(ElementName = "U_max_рек")]
        public double UmaxRec
        {
            get
            {
                double ret = ValuesConverterCommon.GetUstavka256(this._block);
                return TypeAVR == Strings.TypeAVR.Last() ? ret : 0;
            }
            set { if (Common.GetBit(_config, 4)) this._block = ValuesConverterCommon.SetUstavka256(value); }
        }

        /// <summary>
        /// U min
        /// </summary>
        [BindingProperty(24)]
        [XmlElement(ElementName = "U_min_рек")]
        public double UminRec
        {
            get
            {
                double ret = ValuesConverterCommon.GetUstavka256(this._on);
                return TypeAVR == Strings.TypeAVR.Last() ? ret : 0;
            }
            set { if (Common.GetBit(_config, 4)) this._on = ValuesConverterCommon.SetUstavka256(value); }
        }

        #endregion [Properties]
    }
}
