﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Istar
{
    /// <summary>
    /// I*
    /// </summary>
    [XmlRoot(ElementName = "защиты_I*")]
    public class DefenseStarStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _config1;//конфигурация дополнительная (АПВ - выведено/введено, осциллограф)
        [Layout(2)] private ushort _block; //вход блокировки
        [Layout(3)] private ushort _ust; //уставка срабатывания_
        [Layout(4)] private ushort _time; //время срабатывания_
        [Layout(5)] private ushort _k; //коэфиц. зависимой хар-ки
        [Layout(6)] private ushort _u; //уставка пуска по напряжению
        [Layout(7)] private ushort _tu; //время ускорения_
        [Layout(8)] private ushort _i21; // уставка в %
        [Layout(9)] private ushort _usk; //вход по ускорению 

        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// Режим
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string ModeXml
        {
            get { return Validator.Get(this._config, Strings.Mode, 0, 1); }
            set { this._config = Validator.Set(value, Strings.Mode, this._config, 0, 1); }
        }
        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Уставка")]
        public double Ustavka
        {
            get { return ValuesConverterCommon.GetIn(this._ust); }
            set { this._ust = ValuesConverterCommon.SetIn(value); }
        }
        /// <summary>
        /// U пуск
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "U_пуск")]
        public double UStart
        {
            get { return ValuesConverterCommon.GetUstavka256(this._u); }
            set { this._u = ValuesConverterCommon.SetUstavka256(value); }
        }
        /// <summary>
        /// Uпуск (есть/нет)
        /// </summary>
        [XmlElement(ElementName = "Uпуск")]
        [BindingProperty(3)]
        public bool UStartBool
        {
            get { return Common.GetBit(this._config, 3); }
            set { this._config = Common.SetBit(this._config, 3, value); }
        }
        /// <summary>
        /// Направление
        /// </summary>
        [XmlElement(ElementName = "Направление")]
        [BindingProperty(4)]
        public string DirectionXml
        {
            get { return Validator.Get(this._config, Strings.Direction, 6, 7); }
            set { this._config = Validator.Set(value, Strings.Direction, this._config, 6, 7); }
        }
        /// <summary>
        /// Недост. напр(I*)
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "Недост_напр(I*)")]
        public string UndirXml
        {
            get { return Validator.Get(this._config, Strings.Undir, 8); }
            set { this._config = Validator.Set(value, Strings.Undir, this._config, 8); }
        }
        /// <summary>
        /// I*
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "I*")]
        public string IstarXml
        {
            get { return Validator.Get(this._config, Strings.I, 12, 13, 14); }
            set { this._config = Validator.Set(value, Strings.I, this._config, 12, 13, 14); }
        }

        /// <summary>
        /// Характеристика
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "Характеристика")]
        public string CharacteristicXml
        {
            get { return Validator.Get(this._config, Strings.Characteristic, 4); }
            set { this._config = Validator.Set(value, Strings.Characteristic, this._config, 4); }
        }
        /// <summary>
        /// tср, время срабатывания
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "tср")]
        public int TimeSrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// K
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "K")]
        public ushort K
        {
            get { return this._k; }
            set { this._k = value; }
        }
        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "Блокировка")]
        public string BlockXml
        {
            get { return Validator.Get(this._block, Strings.SwitchSignals); }
            set { this._block = Validator.Set(value, Strings.SwitchSignals); }
        }
        /// <summary>
        /// Осц
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "Осц")]
        public string OscXml
        {
            get { return Validator.Get(this._config1, Strings.OscModes, 4, 5); }
            set { this._config1 = Validator.Set(value, Strings.OscModes, this._config1, 4, 5); }
        }

        [BindingProperty(12)]
        public string Uskor
        {
            get { return Validator.Get(this._usk, Strings.RelaySignals); }
            set { this._usk = Validator.Set(value, Strings.RelaySignals); }
        }

        /// <summary>
        /// ty
        /// </summary>
        [BindingProperty(13)]
        [XmlElement(ElementName = "ty")]
        public int TimeY
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tu); }
            set { this._tu = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// Ty (есть/нет)
        /// </summary>
        [BindingProperty(14)]
        [XmlElement(ElementName = "Ненапр.приУСКОР")]
        public bool NenaprUskor
        {
            get { return Common.GetBit(this._config1, 7); }
            set { this._config1 = Common.SetBit(this._config1, 7, value); }
        }
        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(15)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }

        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(16)]
        [XmlElement(ElementName = "АПВ")]
        public string ApvMode
        {
            get { return Validator.Get(this._config1, Strings.Apv, 0); }
            set { this._config1 = Validator.Set(value, Strings.Apv, this._config1, 0); }
        }

        /// <summary>
        /// АВР
        /// </summary>
        [BindingProperty(17)]
        [XmlElement(ElementName = "АВР")]
        public string AvrMode
        {
            get { return Validator.Get(this._config1, Strings.Apv, 1); }
            set { this._config1 = Validator.Set(value, Strings.Apv, this._config1, 1); }
        }
        #endregion [Properties]
    }
}
