﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.InputSignals
{
    /// <summary>
    /// Конфигурациия входных сигналов
    /// </summary>
    [XmlRoot(ElementName = "Конфигурациия_входных_сигналов")]
    public class InputSignalStruct :StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _groopUst1;  //вход аварийная группа уставок 1
        [Layout(1)] private ushort _groopUst2;  //вход аварийная группа уставок 2
        [Layout(2)] private ushort _groopUst3;  //вход аварийная группа уставок 3
        [Layout(3)] private ushort _groopUst4;  //вход аварийная группа уставок 4
        [Layout(4)] private ushort _groopUst5;  //вход аварийная группа уставок 5
        [Layout(5)] private ushort _groopUst6;  //вход аварийная группа уставок 6
        [Layout(6)] private ushort _clrInd;     //вход сброс индикации 
        [Layout(7)] private ushort _res;
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// вход аварийная группа уставок 1
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "вход_аварийная_группа_уставок_1")]
        public string Crash1Xml
        {
            get { return Validator.Get(this._groopUst1,Strings.SwitchSignals) ; }
            set { this._groopUst1 = Validator.Set(value,Strings.SwitchSignals) ; }
        }
        /// <summary>
        /// вход аварийная группа уставок
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "вход_аварийная_группа_уставок_2")]
        public string Crash2Xml
        {
            get { return Validator.Get(this._groopUst2, Strings.SwitchSignals); }
            set { this._groopUst2 = Validator.Set(value, Strings.SwitchSignals); }
        }
        /// <summary>
        /// вход аварийная группа уставок 3
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "вход_аварийная_группа_уставок_3")]
        public string Crash3Xml
        {
            get { return Validator.Get(this._groopUst3, Strings.SwitchSignals); }
            set { this._groopUst3 = Validator.Set(value, Strings.SwitchSignals); }
        }
        /// <summary>
        /// вход аварийная группа уставок
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "вход_аварийная_группа_уставок_4")]
        public string Crash4Xml
        {
            get { return Validator.Get(this._groopUst4, Strings.SwitchSignals); }
            set { this._groopUst4 = Validator.Set(value, Strings.SwitchSignals); }
        }
        /// <summary>
        /// вход аварийная группа уставок
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "вход_аварийная_группа_уставок_5")]
        public string Crash5Xml
        {
            get { return Validator.Get(this._groopUst5, Strings.SwitchSignals); }
            set { this._groopUst5 = Validator.Set(value, Strings.SwitchSignals); }
        }
        /// <summary>
        /// вход аварийная группа уставок
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "вход_аварийная_группа_уставок+6")]
        public string CrashXml
        {
            get { return Validator.Get(this._groopUst6, Strings.SwitchSignals); }
            set { this._groopUst6 = Validator.Set(value, Strings.SwitchSignals); }
        }
        
        /// <summary>
        /// вход сброс индикации
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "вход_сброс_индикации")]
        public string ResetIndicationXml
        {
            get { return Validator.Get(this._clrInd,Strings.SwitchSignals); }
            set { this._clrInd = Validator.Set(value, Strings.SwitchSignals); }
        } 
        #endregion [Properties]   
    }
}
