﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.U
{
    public class AllDefenceUStruct : StructBase, IDgvRowsContainer<DefenceUStruct>
    {
        [Layout(0, Count = 8)]
        private DefenceUStruct[] _u; //мтз U>

        [XmlArray(ElementName = "Все_защиты_U")]
        public DefenceUStruct[] Rows
        {
            get { return this._u; }
            set { this._u = value; }
        }
    }
}
