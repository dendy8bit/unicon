﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Ls
{
    /// <summary>
    /// Конфигурация входного логического сигнала
    /// </summary>
    [XmlRoot(ElementName = "Конфигурация_одного_ЛС")]
    public class InputLogicStruct : StructBase, IXmlSerializable
    {
        private static int _discretsCount;
        public static int DiscrestCount => _discretsCount;

        public const int DISCRETS_COUNT = 40;

        #region [Private fields]

        [Layout(0)] private ushort _a1;
        [Layout(1)] private ushort _a2;
        [Layout(2)] private ushort _a3;
        [Layout(3)] private ushort _a4;
        [Layout(4)] private ushort _a5;
        [Layout(5)] private ushort _a6;
        [Layout(6)] private ushort _a7;
        [Layout(7)] private ushort _a8;
        [Layout(8)] private ushort _a9;
        [Layout(9)] private ushort _a10;

        #endregion [Private fields]

        [XmlIgnore]
        private ushort[] Mass
        {
            get
            {
                return new[]
                {
                    this._a1,
                    this._a2,
                    this._a3,
                    this._a4,
                    this._a5,
                    this._a6,
                    this._a7,
                    this._a8,
                    this._a9,
                    this._a10,
                };
            }
            set
            {
                this._a1 = value[0];
                this._a2 = value[1];
                this._a3 = value[2];
                this._a4 = value[3];
                this._a5 = value[4];
                this._a6 = value[5];
                this._a7 = value[6];
                this._a8 = value[7];
                this._a9 = value[8];
                this._a10 = value[9];
            }
        }

        [BindingProperty(0)]
        [XmlIgnore]
        public string this[int ls]
        {
            get
            {
                ushort sourse = this.Mass[ls/8];
                int pos = (ls*2)%16;
                return Validator.Get(sourse, Strings.LogicValues, pos, pos + 1);
            }

            set
            {
                ushort sourse = this.Mass[ls/8];
                int pos = (ls*2)%16;
                sourse = Validator.Set(value, Strings.LogicValues, sourse, pos, pos + 1);
                ushort[] mass = this.Mass;
                mass[ls/8] = sourse;
                this.Mass = mass;
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {

        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {

            for (int i = 0; i < DiscrestCount; i++)
            {
                writer.WriteElementString("Дискрет", this[i]);
            }
        }

        public static void SetDeviceDiscretsType(string type)
        {
            switch (type)
            {
                case MRUniversal.T4N4D42R35:
                case MRUniversal.T4N5D42R35:
                    _discretsCount = 40;
                    break;
                case MRUniversal.T4N4D74R67:
                case MRUniversal.T4N5D74R67:
                    _discretsCount = 72;
                    break;
                default:
                    _discretsCount = 16;
                    break;
            }
        }
    }
}
