﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Z
{
    public class AllResistanceDefensesStruct771 : StructBase
    {
        public const int RESIST_DEF_COUNT = 10;
        [Layout(0, Count = RESIST_DEF_COUNT)] private ResistanceDefensesStruct[] _resistanceDefenses;

        [BindingProperty(0)]
        public ResistanceDefensesStruct this[int index]
        {
            get { return this._resistanceDefenses[index]; }
            set { this._resistanceDefenses[index] = value; }
        }
        [XmlElement(ElementName = "Все_дистанционные_защиты")]
        public ResistanceDefensesStruct[] ResistanceDefenses
        {
            get { return this._resistanceDefenses; }
            set { this._resistanceDefenses = value; }
        }
    }
}
