﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Omp
{
    /// <summary>
    /// конфигурациия ОМП
    /// </summary> 
    public class ConfigurationOpmStruct :StructBase
    {
        #region [Private fields] 
        [Layout(0)] private ushort _x1;     
        [Layout(1)] private ushort _x2;
        [Layout(2)] private ushort _x3;     
        [Layout(3)] private ushort _x4;
        [Layout(4)] private ushort _x5;     
        [Layout(5)] private ushort _l1;
        [Layout(6)] private ushort _l2;     
        [Layout(7)] private ushort _l3;
        [Layout(8)] private ushort _l4;			
        [Layout(9)] private ushort _config;			

        #endregion [Private fields]


        #region [Properties]
       
        /// <summary>
        /// Режим
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string ModeXml
        {
            get { return Validator.Get(this._config, Strings.OmpModes); }
            set { this._config =Validator.Set(value, Strings.OmpModes) ; }
        }
        
        [BindingProperty(1)]
        [XmlElement(ElementName = "Xуд1")]
        public double Xud1
        {
            get { return ValuesConverterCommon.GetUstavka2(this._x1); }
            set { this._x1 = ValuesConverterCommon.SetUstavka2(value); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Xуд2")]
        public double Xud2
        {
            get { return ValuesConverterCommon.GetUstavka2(this._x2); }
            set { this._x2 = ValuesConverterCommon.SetUstavka2(value); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Xуд3")]
        public double Xud3
        {
            get { return ValuesConverterCommon.GetUstavka2(this._x3); }
            set { this._x3 = ValuesConverterCommon.SetUstavka2(value); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Xуд4")]
        public double Xud4
        {
            get { return ValuesConverterCommon.GetUstavka2(this._x4); }
            set { this._x4 = ValuesConverterCommon.SetUstavka2(value); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Xуд5")]
        public double Xud5
        {
            get { return ValuesConverterCommon.GetUstavka2(this._x5); }
            set { this._x5 = ValuesConverterCommon.SetUstavka2(value); }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "L1")]
        public double L1
        {
            get { return ValuesConverterCommon.GetUstavka256(this._l1); }
            set { this._l1 = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "L2")]
        public double L2
        {
            get { return ValuesConverterCommon.GetUstavka256(this._l2); }
            set { this._l2 = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "L3")]
        public double L3
        {
            get { return ValuesConverterCommon.GetUstavka256(this._l3); }
            set { this._l3 = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "L4")]
        public double L4
        {
            get { return ValuesConverterCommon.GetUstavka256(this._l4); }
            set { this._l4 = ValuesConverterCommon.SetUstavka256(value); }
        }
        #endregion [Properties]
    }
}
