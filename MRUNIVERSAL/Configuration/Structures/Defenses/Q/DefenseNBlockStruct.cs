﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MRUNIVERSAL.Configuration.Structures.Defenses.Q
{
    /// <summary>
    /// блокировка пуска двигателя по числу пусков
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "блокировка_пуска_двигателя_по_числу_пусков")]
    public class DefenseNBlockStruct :StructBase
    {
        /// <summary>
        /// число горячих пусков двигателя
        /// </summary>
        [Layout(0)]
        [XmlElement(ElementName = "число_горячих_пусков_двигателя")] private ushort _hotStarts;

        /// <summary>
        /// число холодных пусков двигателя
        /// </summary>
        [Layout(1)]
        [XmlElement(ElementName = "число_холодных_пусков_двигателя")] private ushort _coldStarts;

        /// <summary>
        /// время блокировки
        /// </summary>
        [Layout(2)]
        [XmlElement(ElementName = "время_блокировки")] private ushort _blockTime;

        [Layout(3)]
        [XmlIgnore]
        private ushort _rez; //время за которое считается число пусков


        /// <summary>
        /// число горячих пусков двигателя
        /// </summary>
        [BindingProperty(0)]
        public ushort HotStarts
        {
            get { return _hotStarts; }
            set { _hotStarts = value; }
        }

        /// <summary>
        /// число холодных пусков двигателя
        /// </summary>
        [BindingProperty(1)]
        public ushort ColdStarts
        {
            get { return _coldStarts; }
            set { _coldStarts = value; }
        }

        /// <summary>
        /// время блокировки
        /// </summary>
        [BindingProperty(2)]
        public ushort BlockTime
        {
            get { return _blockTime; }
            set { _blockTime = value; }
        }
    }
}
