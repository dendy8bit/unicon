﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MRUNIVERSAL.Configuration.Structures.RelayInd
{
    /// <summary>
    /// параметры выходных реле
    /// </summary>
    [XmlType(TypeName = "Одно_реле")]
    public class ReleOutputStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _signal;
        [Layout(1)] private ushort _type;
        [Layout(2)] private ushort _wait;
        [Layout(3)] private ushort _rez;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Тип
        /// </summary>
        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Тип")]
        public string TypeXml
        {
            get { return Validator.Get(this._type, Strings.SignalType, 0); }
            set { this._type = Validator.Set(value, Strings.SignalType, this._type, 0); }
        }

        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "База")]
        public string Base
        {
            get { return Validator.Get(this._type, Strings.OscBases, 12, 13); }
            set { this._type = Validator.Set(value, Strings.OscBases, this._type, 12, 13); }
        }

        /// <summary>
        /// Сигнал
        /// </summary>
        [BindingProperty(2)]
        [XmlAttribute(AttributeName = "Сигнал")]
        public string SignalXml
        {
            get
            {
                Dictionary<ushort, string> list = Strings.OscChannelSignals[CurrentBase(Base)];
                return Validator.Get(this._signal, list);
            }
            set
            {
                Dictionary<ushort, string> list = Strings.OscChannelSignals[CurrentBase(Base)];
                this._signal = Validator.Set(value, list);
            }
        }

        /// <summary>
        /// Время
        /// </summary>
        [BindingProperty(3)]
        [XmlAttribute(AttributeName = "Время")]
        public int Wait
        {
            get { return ValuesConverterCommon.GetWaitTime(this._wait); }
            set { this._wait = ValuesConverterCommon.SetWaitTime(value); }
        }

        private int CurrentBase(string baseString)
        {
            int indexBase = Convert.ToInt32(Regex.Replace(baseString, @"[^\d]+", ""));
            return indexBase - 1;
        }

        #endregion [Properties]
    }
}
