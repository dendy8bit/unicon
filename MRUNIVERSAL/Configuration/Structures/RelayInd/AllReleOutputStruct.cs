using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MRUNIVERSAL.Configuration.Structures.RelayInd
{
    /// <summary>
    /// ��� ����
    /// </summary>
    public class AllReleOutputStruct : StructBase, IDgvRowsContainer<ReleOutputStruct>
    {
        public const int RELAY_COUNT_CONST = 66;
        private static int _currentCount;
        public static int CurrentCount => _currentCount;

        /// <summary>
        /// ����
        /// </summary>
        [Layout(0, Count = RELAY_COUNT_CONST)] private ReleOutputStruct[] _relays;

        /// <summary>
        /// ����
        /// </summary>
        [XmlArray(ElementName = "���_����")]

        public ReleOutputStruct[] Rows
        {
            get { return this._relays; }
            set { this._relays = value; }
        }

        public static void SetDeviceRelaysType(string type)
        {
            switch (type)
            {
                case MRUniversal.T4N4D42R35:
                case MRUniversal.T4N5D42R35:
                    _currentCount = 34;
                    break;
                case MRUniversal.T4N4D74R67:
                case MRUniversal.T4N5D74R67:
                    _currentCount = 66;
                    break;
                default:
                    _currentCount = 16;
                    break;
            }
        }
    }
}