﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MRUNIVERSAL.Configuration.Structures
{
    public class AllGroupSetpointStruct771 : StructBase, ISetpointContainer<GroupSetpoint771>
    {
        private const int GROUP_COUNT = 6;

        [Layout(0, Count = GROUP_COUNT)] private GroupSetpoint771[] _groupSetpoints;

        [XmlElement(ElementName = "AllGroups")]
        public GroupSetpoint771[] Setpoints
        {
            get
            {
                var res = new GroupSetpoint771[GROUP_COUNT];
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    res[i] = this._groupSetpoints[i].Clone<GroupSetpoint771>();
                }
                return res;
            }
            set
            {
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    this._groupSetpoints[i] = value[i];
                }
            }
        }
    }
}
