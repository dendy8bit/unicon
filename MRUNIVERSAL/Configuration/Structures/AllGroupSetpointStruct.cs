﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using System.Xml.Serialization;

namespace BEMN.MRUNIVERSAL.Configuration.Structures
{
    public class AllGroupSetpointStruct : StructBase, ISetpointContainer<GroupSetpoint>
    {
        private const int GROUP_COUNT = 6;

        [Layout(0, Count = GROUP_COUNT)] private GroupSetpoint[] _groupSetpoints;

        [XmlElement(ElementName = "AllGroups")]
        public GroupSetpoint[] Setpoints
        {
            get
            {
                var res = new GroupSetpoint[GROUP_COUNT];
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    res[i] = this._groupSetpoints[i].Clone<GroupSetpoint>();
                }
                return res;
            }
            set
            {
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    this._groupSetpoints[i] = value[i];
                }
            }
        }
    }
}
