﻿namespace BEMN.MRUNIVERSAL.FaceSideDevice
{
    partial class FaceSideDeviceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FaceSideDeviceForm));
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox2 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox3 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox4 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox5 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox6 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox7 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox8 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox9 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox10 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox11 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox12 = new System.Windows.Forms.MaskedTextBox();
            this.diodOff = new BEMN.Forms.Diod();
            this.diodOn = new BEMN.Forms.Diod();
            this.diod12 = new BEMN.Forms.Diod();
            this.diod11 = new BEMN.Forms.Diod();
            this.diod10 = new BEMN.Forms.Diod();
            this.diod9 = new BEMN.Forms.Diod();
            this.diod8 = new BEMN.Forms.Diod();
            this.diod7 = new BEMN.Forms.Diod();
            this.diod6 = new BEMN.Forms.Diod();
            this.diod5 = new BEMN.Forms.Diod();
            this.diod4 = new BEMN.Forms.Diod();
            this.diod3 = new BEMN.Forms.Diod();
            this.diod2 = new BEMN.Forms.Diod();
            this.diod1 = new BEMN.Forms.Diod();
            this.diodJA = new BEMN.Forms.Diod();
            this.diodJS = new BEMN.Forms.Diod();
            this.SuspendLayout();
            // 
            // richTextBox
            // 
            this.richTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.richTextBox.Enabled = false;
            this.richTextBox.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox.ForeColor = System.Drawing.SystemColors.Highlight;
            this.richTextBox.Location = new System.Drawing.Point(136, 46);
            this.richTextBox.MaxLength = 20;
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.ReadOnly = true;
            this.richTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBox.Size = new System.Drawing.Size(225, 81);
            this.richTextBox.TabIndex = 0;
            this.richTextBox.Text = "";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(241, 156);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(30, 31);
            this.button1.TabIndex = 999999999;
            this.button1.TabStop = false;
            this.button1.Tag = "7";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button_Clicked);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button2.BackgroundImage")));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Location = new System.Drawing.Point(332, 156);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(30, 31);
            this.button2.TabIndex = 999999999;
            this.button2.TabStop = false;
            this.button2.Tag = "8";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button_Clicked);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Gray;
            this.button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button3.BackgroundImage")));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Location = new System.Drawing.Point(241, 246);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(30, 31);
            this.button3.TabIndex = 999999999;
            this.button3.TabStop = false;
            this.button3.Tag = "5";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button_Clicked);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Gray;
            this.button4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button4.BackgroundImage")));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Location = new System.Drawing.Point(332, 246);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(30, 31);
            this.button4.TabIndex = 999999999;
            this.button4.TabStop = false;
            this.button4.Tag = "6";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button_Clicked);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button5.BackgroundImage")));
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.Location = new System.Drawing.Point(286, 277);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(30, 31);
            this.button5.TabIndex = 999999999;
            this.button5.TabStop = false;
            this.button5.Tag = "1";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button_Clicked);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button6.BackgroundImage")));
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button6.Location = new System.Drawing.Point(241, 308);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(30, 31);
            this.button6.TabIndex = 999999999;
            this.button6.TabStop = false;
            this.button6.Tag = "3";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button_Clicked);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button7.BackgroundImage")));
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button7.Location = new System.Drawing.Point(332, 308);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(30, 31);
            this.button7.TabIndex = 999999999;
            this.button7.TabStop = false;
            this.button7.Tag = "2";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button_Clicked);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button8.BackgroundImage")));
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button8.Location = new System.Drawing.Point(286, 339);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(30, 31);
            this.button8.TabIndex = 999999999;
            this.button8.TabStop = false;
            this.button8.Tag = "4";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button_Clicked);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button9.BackgroundImage")));
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button9.Location = new System.Drawing.Point(72, 89);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(30, 31);
            this.button9.TabIndex = 999999999;
            this.button9.TabStop = false;
            this.button9.Tag = "10";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button_Clicked);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button10.BackgroundImage")));
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button10.Location = new System.Drawing.Point(72, 52);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(30, 31);
            this.button10.TabIndex = 999999999;
            this.button10.TabStop = false;
            this.button10.Tag = "9";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button_Clicked);
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(72, 157);
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(113, 20);
            this.maskedTextBox1.TabIndex = 0;
            this.maskedTextBox1.TabStop = false;
            this.maskedTextBox1.Visible = false;
            // 
            // maskedTextBox2
            // 
            this.maskedTextBox2.Location = new System.Drawing.Point(72, 179);
            this.maskedTextBox2.Name = "maskedTextBox2";
            this.maskedTextBox2.Size = new System.Drawing.Size(113, 20);
            this.maskedTextBox2.TabIndex = 0;
            this.maskedTextBox2.TabStop = false;
            this.maskedTextBox2.Visible = false;
            // 
            // maskedTextBox3
            // 
            this.maskedTextBox3.Location = new System.Drawing.Point(72, 202);
            this.maskedTextBox3.Name = "maskedTextBox3";
            this.maskedTextBox3.Size = new System.Drawing.Size(113, 20);
            this.maskedTextBox3.TabIndex = 0;
            this.maskedTextBox3.TabStop = false;
            this.maskedTextBox3.Visible = false;
            // 
            // maskedTextBox4
            // 
            this.maskedTextBox4.Location = new System.Drawing.Point(72, 225);
            this.maskedTextBox4.Name = "maskedTextBox4";
            this.maskedTextBox4.Size = new System.Drawing.Size(113, 20);
            this.maskedTextBox4.TabIndex = 0;
            this.maskedTextBox4.TabStop = false;
            this.maskedTextBox4.Visible = false;
            // 
            // maskedTextBox5
            // 
            this.maskedTextBox5.Location = new System.Drawing.Point(72, 248);
            this.maskedTextBox5.Name = "maskedTextBox5";
            this.maskedTextBox5.Size = new System.Drawing.Size(113, 20);
            this.maskedTextBox5.TabIndex = 0;
            this.maskedTextBox5.TabStop = false;
            this.maskedTextBox5.Visible = false;
            // 
            // maskedTextBox6
            // 
            this.maskedTextBox6.Location = new System.Drawing.Point(72, 270);
            this.maskedTextBox6.Name = "maskedTextBox6";
            this.maskedTextBox6.Size = new System.Drawing.Size(113, 20);
            this.maskedTextBox6.TabIndex = 0;
            this.maskedTextBox6.TabStop = false;
            this.maskedTextBox6.Visible = false;
            // 
            // maskedTextBox7
            // 
            this.maskedTextBox7.Location = new System.Drawing.Point(72, 293);
            this.maskedTextBox7.Name = "maskedTextBox7";
            this.maskedTextBox7.Size = new System.Drawing.Size(113, 20);
            this.maskedTextBox7.TabIndex = 0;
            this.maskedTextBox7.TabStop = false;
            this.maskedTextBox7.Visible = false;
            // 
            // maskedTextBox8
            // 
            this.maskedTextBox8.Location = new System.Drawing.Point(72, 316);
            this.maskedTextBox8.Name = "maskedTextBox8";
            this.maskedTextBox8.Size = new System.Drawing.Size(113, 20);
            this.maskedTextBox8.TabIndex = 0;
            this.maskedTextBox8.TabStop = false;
            this.maskedTextBox8.Visible = false;
            // 
            // maskedTextBox9
            // 
            this.maskedTextBox9.Location = new System.Drawing.Point(72, 339);
            this.maskedTextBox9.Name = "maskedTextBox9";
            this.maskedTextBox9.Size = new System.Drawing.Size(113, 20);
            this.maskedTextBox9.TabIndex = 0;
            this.maskedTextBox9.TabStop = false;
            this.maskedTextBox9.Visible = false;
            // 
            // maskedTextBox10
            // 
            this.maskedTextBox10.Location = new System.Drawing.Point(72, 362);
            this.maskedTextBox10.Name = "maskedTextBox10";
            this.maskedTextBox10.Size = new System.Drawing.Size(113, 20);
            this.maskedTextBox10.TabIndex = 0;
            this.maskedTextBox10.TabStop = false;
            this.maskedTextBox10.Visible = false;
            // 
            // maskedTextBox11
            // 
            this.maskedTextBox11.Location = new System.Drawing.Point(72, 385);
            this.maskedTextBox11.Name = "maskedTextBox11";
            this.maskedTextBox11.Size = new System.Drawing.Size(113, 20);
            this.maskedTextBox11.TabIndex = 0;
            this.maskedTextBox11.TabStop = false;
            this.maskedTextBox11.Visible = false;
            // 
            // maskedTextBox12
            // 
            this.maskedTextBox12.Location = new System.Drawing.Point(72, 407);
            this.maskedTextBox12.Name = "maskedTextBox12";
            this.maskedTextBox12.Size = new System.Drawing.Size(113, 20);
            this.maskedTextBox12.TabIndex = 0;
            this.maskedTextBox12.TabStop = false;
            this.maskedTextBox12.Visible = false;
            // 
            // diodOff
            // 
            this.diodOff.BackColor = System.Drawing.Color.Transparent;
            this.diodOff.Location = new System.Drawing.Point(44, 99);
            this.diodOff.Name = "diodOff";
            this.diodOff.Size = new System.Drawing.Size(14, 14);
            this.diodOff.State = BEMN.Forms.DiodState.NOT_SET;
            this.diodOff.TabIndex = 26;
            // 
            // diodOn
            // 
            this.diodOn.BackColor = System.Drawing.Color.Transparent;
            this.diodOn.Location = new System.Drawing.Point(44, 63);
            this.diodOn.Name = "diodOn";
            this.diodOn.Size = new System.Drawing.Size(14, 14);
            this.diodOn.State = BEMN.Forms.DiodState.NOT_SET;
            this.diodOn.TabIndex = 25;
            // 
            // diod12
            // 
            this.diod12.BackColor = System.Drawing.Color.Transparent;
            this.diod12.Location = new System.Drawing.Point(42, 415);
            this.diod12.Name = "diod12";
            this.diod12.Size = new System.Drawing.Size(14, 14);
            this.diod12.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod12.TabIndex = 24;
            // 
            // diod11
            // 
            this.diod11.BackColor = System.Drawing.Color.Transparent;
            this.diod11.Location = new System.Drawing.Point(42, 393);
            this.diod11.Name = "diod11";
            this.diod11.Size = new System.Drawing.Size(14, 14);
            this.diod11.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod11.TabIndex = 23;
            // 
            // diod10
            // 
            this.diod10.BackColor = System.Drawing.Color.Transparent;
            this.diod10.Location = new System.Drawing.Point(42, 369);
            this.diod10.Name = "diod10";
            this.diod10.Size = new System.Drawing.Size(14, 14);
            this.diod10.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod10.TabIndex = 22;
            // 
            // diod9
            // 
            this.diod9.BackColor = System.Drawing.Color.Transparent;
            this.diod9.Location = new System.Drawing.Point(42, 347);
            this.diod9.Name = "diod9";
            this.diod9.Size = new System.Drawing.Size(14, 14);
            this.diod9.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod9.TabIndex = 21;
            // 
            // diod8
            // 
            this.diod8.BackColor = System.Drawing.Color.Transparent;
            this.diod8.Location = new System.Drawing.Point(42, 324);
            this.diod8.Name = "diod8";
            this.diod8.Size = new System.Drawing.Size(14, 14);
            this.diod8.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod8.TabIndex = 20;
            // 
            // diod7
            // 
            this.diod7.BackColor = System.Drawing.Color.Transparent;
            this.diod7.Location = new System.Drawing.Point(42, 301);
            this.diod7.Name = "diod7";
            this.diod7.Size = new System.Drawing.Size(14, 14);
            this.diod7.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod7.TabIndex = 19;
            // 
            // diod6
            // 
            this.diod6.BackColor = System.Drawing.Color.Transparent;
            this.diod6.Location = new System.Drawing.Point(42, 278);
            this.diod6.Name = "diod6";
            this.diod6.Size = new System.Drawing.Size(14, 14);
            this.diod6.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod6.TabIndex = 18;
            // 
            // diod5
            // 
            this.diod5.BackColor = System.Drawing.Color.Transparent;
            this.diod5.Location = new System.Drawing.Point(42, 256);
            this.diod5.Name = "diod5";
            this.diod5.Size = new System.Drawing.Size(14, 14);
            this.diod5.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod5.TabIndex = 17;
            // 
            // diod4
            // 
            this.diod4.BackColor = System.Drawing.Color.Transparent;
            this.diod4.Location = new System.Drawing.Point(42, 233);
            this.diod4.Name = "diod4";
            this.diod4.Size = new System.Drawing.Size(14, 14);
            this.diod4.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod4.TabIndex = 16;
            // 
            // diod3
            // 
            this.diod3.BackColor = System.Drawing.Color.Transparent;
            this.diod3.Location = new System.Drawing.Point(42, 210);
            this.diod3.Name = "diod3";
            this.diod3.Size = new System.Drawing.Size(14, 14);
            this.diod3.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod3.TabIndex = 15;
            // 
            // diod2
            // 
            this.diod2.BackColor = System.Drawing.Color.Transparent;
            this.diod2.Location = new System.Drawing.Point(42, 187);
            this.diod2.Name = "diod2";
            this.diod2.Size = new System.Drawing.Size(14, 14);
            this.diod2.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod2.TabIndex = 14;
            // 
            // diod1
            // 
            this.diod1.BackColor = System.Drawing.Color.Transparent;
            this.diod1.Location = new System.Drawing.Point(42, 165);
            this.diod1.Name = "diod1";
            this.diod1.Size = new System.Drawing.Size(14, 14);
            this.diod1.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod1.TabIndex = 13;
            // 
            // diodJA
            // 
            this.diodJA.BackColor = System.Drawing.Color.Transparent;
            this.diodJA.Location = new System.Drawing.Point(214, 165);
            this.diodJA.Name = "diodJA";
            this.diodJA.Size = new System.Drawing.Size(14, 14);
            this.diodJA.State = BEMN.Forms.DiodState.NOT_SET;
            this.diodJA.TabIndex = 12;
            // 
            // diodJS
            // 
            this.diodJS.BackColor = System.Drawing.Color.Transparent;
            this.diodJS.Location = new System.Drawing.Point(305, 165);
            this.diodJS.Name = "diodJS";
            this.diodJS.Size = new System.Drawing.Size(14, 14);
            this.diodJS.State = BEMN.Forms.DiodState.NOT_SET;
            this.diodJS.TabIndex = 11;
            // 
            // FaceSideDeviceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(422, 459);
            this.Controls.Add(this.maskedTextBox12);
            this.Controls.Add(this.maskedTextBox11);
            this.Controls.Add(this.maskedTextBox10);
            this.Controls.Add(this.maskedTextBox9);
            this.Controls.Add(this.maskedTextBox8);
            this.Controls.Add(this.maskedTextBox7);
            this.Controls.Add(this.maskedTextBox6);
            this.Controls.Add(this.maskedTextBox5);
            this.Controls.Add(this.maskedTextBox4);
            this.Controls.Add(this.maskedTextBox3);
            this.Controls.Add(this.maskedTextBox2);
            this.Controls.Add(this.maskedTextBox1);
            this.Controls.Add(this.diodOff);
            this.Controls.Add(this.diodOn);
            this.Controls.Add(this.diod12);
            this.Controls.Add(this.diod11);
            this.Controls.Add(this.diod10);
            this.Controls.Add(this.diod9);
            this.Controls.Add(this.diod8);
            this.Controls.Add(this.diod7);
            this.Controls.Add(this.diod6);
            this.Controls.Add(this.diod5);
            this.Controls.Add(this.diod4);
            this.Controls.Add(this.diod3);
            this.Controls.Add(this.diod2);
            this.Controls.Add(this.diod1);
            this.Controls.Add(this.diodJA);
            this.Controls.Add(this.diodJS);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.richTextBox);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(438, 498);
            this.MinimumSize = new System.Drawing.Size(438, 498);
            this.Name = "FaceSideDeviceForm";
            this.Text = "Передняя панель устройства";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FaceSideDeviceForm_FormClosing);
            this.Load += new System.EventHandler(this.FaceSideDeviceForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FaceSideDeviceForm_KeyUp);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private Forms.Diod diodJS;
        private Forms.Diod diodJA;
        private Forms.Diod diod1;
        private Forms.Diod diod2;
        private Forms.Diod diod3;
        private Forms.Diod diod4;
        private Forms.Diod diod5;
        private Forms.Diod diod6;
        private Forms.Diod diod7;
        private Forms.Diod diod8;
        private Forms.Diod diod9;
        private Forms.Diod diod10;
        private Forms.Diod diod11;
        private Forms.Diod diod12;
        private Forms.Diod diodOn;
        private Forms.Diod diodOff;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.MaskedTextBox maskedTextBox2;
        private System.Windows.Forms.MaskedTextBox maskedTextBox3;
        private System.Windows.Forms.MaskedTextBox maskedTextBox4;
        private System.Windows.Forms.MaskedTextBox maskedTextBox5;
        private System.Windows.Forms.MaskedTextBox maskedTextBox6;
        private System.Windows.Forms.MaskedTextBox maskedTextBox7;
        private System.Windows.Forms.MaskedTextBox maskedTextBox8;
        private System.Windows.Forms.MaskedTextBox maskedTextBox9;
        private System.Windows.Forms.MaskedTextBox maskedTextBox10;
        private System.Windows.Forms.MaskedTextBox maskedTextBox11;
        private System.Windows.Forms.MaskedTextBox maskedTextBox12;
    }
}