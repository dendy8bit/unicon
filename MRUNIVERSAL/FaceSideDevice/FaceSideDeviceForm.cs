﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Interfaces;

namespace BEMN.MRUNIVERSAL.FaceSideDevice
{
    public partial class FaceSideDeviceForm : Form, IFormView
    {
        private MRUniversal _device;

        private MemoryEntity<OneWordStruct> _display;
        private Diod[] _diods;
        private MemoryEntity<IndicatorsStruct> _indicatorsMemoryEntity;
        private bool _isPasswordChecked;
        private PasswordForm _passwordForm;
        private bool _isShowedMessage;
        private string _password = "КИЗЯ";

        public FaceSideDeviceForm()
        {
            this.InitializeComponent();
        }

        public FaceSideDeviceForm(MRUniversal device)
        {
            this.InitializeComponent();
            
            this._passwordForm = new PasswordForm();

            this._device = device;
            
            this._display = new MemoryEntity<OneWordStruct>("DisplayMove" + this._device.DeviceNumber, device, 0x0000);

            this._device.FaceSizeDictionary.DisplayInfoLoadOk += HandlerHelper.CreateHandler(this, () =>
            {
                this.richTextBox.Clear();
                
                string a = this._device.FaceSizeDictionary.DisplayInfo.Insert(20, "\n");
                string b = a.Insert(41, "\n");
                string c = b.Insert(62, "\n");

                this.richTextBox.Text = c;

            });
            this._device.FaceSizeDictionary.DisplayInfoLoadFail += HandlerHelper.CreateHandler(this, () =>
            {
                this.richTextBox.Text = "Ошибка чтении информации!";
            });
            
            this._indicatorsMemoryEntity = new MemoryEntity<IndicatorsStruct>("IndicatorsLed" + device.DeviceNumber, device, 0x012A);
            this._indicatorsMemoryEntity.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.LoadIndicatorsOk);
            this._indicatorsMemoryEntity.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.LoadIndicatorsFail);
            
            this._diods = new[]
            {
                this.diodJS,
                this.diodJA,
                this.diodOn,
                this.diodOff,
                this.diod1,
                this.diod2,
                this.diod3,
                this.diod4,
                this.diod5,
                this.diod6,
                this.diod7,
                this.diod8,
                this.diod9,
                this.diod10,
                this.diod11,
                this.diod12,
            };
        }

        private void LoadIndicatorsFail()
        {
            foreach (var indicator in this._diods)
            {
                indicator.TurnOff();
            }
        }

        public void LoadIndicatorsOk()
        {
            IndicatorsStruct indicators = this._indicatorsMemoryEntity.Value;

            try
            {
                int j = 0;

                for (int i = 0; i < this._diods.Length; i++)
                {
                    switch (i)
                    {
                        case 0:
                            this._diods[0].OrangeDiod(indicators.Indicators[0][1]);
                            j++;
                            break;
                        case 2:
                            this._diods[2].SetStateForSwitch(indicators.Indicators[2][1]);
                            break;
                        case 3:
                            this._diods[3].SetStateForSwitch(indicators.Indicators[2][0]);
                            j++;
                            break;
                        default:
                            this._diods[i].SetState(indicators.Indicators[j]);
                            j++;
                            break;
                    }                   
                }
            }
            catch (Exception)
            {
            }          
        }





        private bool CheckPassword()
        {
            if (!this._isPasswordChecked)
            {
                if (!this._isShowedMessage)
                {
                    MessageBox.Show("Для активации дополнительного функционала, требуется ввести пароль!", "Внимание!",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this._isShowedMessage = true;
                }

                if (this._passwordForm.ShowDialog() == DialogResult.Cancel) return false;

                if (this._passwordForm.Password != this._password)
                {
                    MessageBox.Show("Введен неверный пароль. Обратитесь к производителю устройства.", "Внимание!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                MessageBox.Show("Пароль принят", "Внимание!",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                this._isPasswordChecked = true;
            }

            return true;
        }

        
        private void FaceSideDeviceForm_Load(object sender, EventArgs e)
        {
            if (_device.IsConnect)
            {
                this._device.LoadDisplayInfo();
                this._indicatorsMemoryEntity.LoadStructCycle(new TimeSpan(100));
            }

            foreach (var item in Controls.OfType<Button>())
            {
                item.PreviewKeyDown += this.buttons_PreviewKeyDown;
            }
        }

        private void buttons_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                case Keys.Down:
                case Keys.Left:
                case Keys.Right:
                    e.IsInputKey = true;
                    break;
                default:
                    break;
            }
        }

        private void FaceSideDeviceForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode && this.CheckPassword())
            {
                switch (e.KeyCode)
                {
                    case Keys.Up:
                        this._display.SaveOneWord(0x1);
                        break;
                    case Keys.Down:
                        this._display.SaveOneWord(0x4);
                        break;
                    case Keys.Right:
                        this._display.SaveOneWord(0x2);
                        break;
                    case Keys.Left:
                        this._display.SaveOneWord(0x3);
                        break;
                    case Keys.Enter:
                        this._display.SaveOneWord(0x6);
                        break;
                    case Keys.Escape:
                    case Keys.Back:
                        this._display.SaveOneWord(0x5);
                        break;
                    case Keys.A:
                        this._display.SaveOneWord(0x7);
                        break;
                    case Keys.S:
                        this._display.SaveOneWord(0x8);
                        break;
                    case Keys.I:
                        this._display.SaveOneWord(0x9);
                        break;
                    case Keys.O:
                        this._display.SaveOneWord(0x10);
                        break;
                    default:
                        break;
                }
                this.ActiveControl = null;
            }
        }

        private void button_Clicked(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode && this.CheckPassword())
            {
                switch (Convert.ToInt32(((Button)sender).Tag))
                {
                    case 1:
                        this._display.SaveOneWord(0x1);
                        break;
                    case 2:
                        this._display.SaveOneWord(0x2);
                        break;
                    case 3:
                        this._display.SaveOneWord(0x3);
                        break;
                    case 4:
                        this._display.SaveOneWord(0x4);
                        break;
                    case 5:
                        this._display.SaveOneWord(0x5);
                        break;
                    case 6:
                        this._display.SaveOneWord(0x6);
                        break;
                    case 7:
                        this._display.SaveOneWord(0x7);
                        break;
                    case 8:
                        this._display.SaveOneWord(0x8);
                        break;
                    case 9:
                        this._display.SaveOneWord(0x9);
                        break;
                    case 10:
                        this._display.SaveOneWord(0xa);
                        break;
                    default:
                        break;
                }
            }

            this.ActiveControl = null;
        }

        private void FaceSideDeviceForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.MB.RemoveQuery("LoadDisplayInfo" + this._device.DeviceNumber);
            this._indicatorsMemoryEntity.RemoveStructQueries();
        }

        #region IFormView

        public Type FormDevice => typeof(MRUniversal);

        public bool Multishow { get; private set; }

        public Type ClassType => typeof(FaceSideDeviceForm);

        public bool ForceShow => false;

        public Image NodeImage => Framework.Properties.Resources.mrBig;

        public string NodeName => "Лицевая панель";

        public INodeView[] ChildNodes => new INodeView[]{};

        public bool Deletable => false;

        #endregion

       
    }
}
