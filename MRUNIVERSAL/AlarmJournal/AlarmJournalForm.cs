﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MRUNIVERSAL.AlarmJournal.Structures;
using BEMN.MRUNIVERSAL.Configuration.Structures.MeasuringTransformer;
using BEMN.MRUNIVERSAL.Properties;
using SchemeEditorSystem.ResourceLibs;

namespace BEMN.MRUNIVERSAL.AlarmJournal
{
    public partial class AlarmJournalForm : Form, IFormView
    {
        #region [IFormView Members]
        public Type FormDevice => typeof(MRUniversal);
        public bool Multishow { get; private set; }
        public INodeView[] ChildNodes => new INodeView[] { };
        public Type ClassType => typeof(AlarmJournalForm);
        public bool Deletable => false;
        public bool ForceShow => false;
        public Image NodeImage => Framework.Properties.Resources.ja;
        public string NodeName => "Журнал аварий";
        #endregion [IFormView Members]        


        #region [Constants]
        private const string TABLE_NAME = "МР_журнал_аварий"; 
        private const string LIST_FILE_NAME = "jlist.xml";
        private const string FAIL_READ = "Невозможно прочитать список подписей сигналов ЖА СПЛ. Списки будут сформированы по умолчанию.";
        private const string READING_LIST_FILE = "Идет чтение файла списка подписей сигналов ЖА СПЛ";
        private const string READ_AJ = "Идёт чтение журнала аварий";
        private const string READ_AJ_FAIL = "Невозможно прочитать журнал аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        private const string RECORDS_IN_JOURNAL = "Аварий в журнале - {0}";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        #endregion [Constants]


        #region [Private fields]
        private readonly AlarmJournalLoader _journalLoader;
        private readonly CurrentOptionsLoader _currentOptionsLoaderAJ;
        private readonly MemoryEntity<AlarmJournalStruct> _alarmJournal;
        private readonly MemoryEntity<OneWordStruct> _setPageAlarmJournal;

        private DataTable _table; // наша таблица
        private int _recordNumber; // номер записи
        private MRUniversal _device;
        private List<string> _messages; // список с сообщениями
        private FileDriver _fileDriver;
        #endregion [Private fields]

        
        #region [Constructor's]
        public AlarmJournalForm()
        {
            InitializeComponent();
        }

        public AlarmJournalForm(MRUniversal device)
        {
            this.InitializeComponent();
            this._device = device;

            this._currentOptionsLoaderAJ = this._device.CurrentOptionsLoaderAJ;
            this._currentOptionsLoaderAJ.LoadOk += HandlerHelper.CreateActionHandler(this, this.StartReadJournal);
            this._currentOptionsLoaderAJ.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadJournal);

            this._journalLoader = new AlarmJournalLoader(this._device.AllAlarmJournal, this._device.AlarmJournal, this._device.SetPageAlarmJournal);
            this._journalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.ReadAllRecords);
            this._journalLoader.AllJournalReadFail += HandlerHelper.CreateActionHandler(this, this.FailReadJournal);

            this._fileDriver = new FileDriver(device, this);
        }
        #endregion [Constructor's]


        #region [Methods]
        private void StartReadJournal()
        {
            this._table.Clear();
            this._recordNumber = 0;

            this._journalLoader.Clear();
            this._journalLoader.StartRead();
        }

        private void FailReadJournal()
        {
            this._statusLabel.Text = READ_AJ_FAIL;
            this.ButtonsEnabled = true;
        }
        
        private bool ButtonsEnabled
        {
            set => this._readButton.Enabled = this._saveButton.Enabled = this._loadButton.Enabled = this._saveHtmlButton.Enabled = value;
        }
        
        /// <summary>
        /// Заносит данные прочитанной структуры в таблицу
        /// </summary>
        private void ReadAllRecords()
        {
            if (this._journalLoader.AlarmJournalRecords.Count == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
                this.ButtonsEnabled = true;
                return;
            }
            this._recordNumber = 1;
            foreach (AlarmJournalStruct record in this._journalLoader.AlarmJournalRecords)
            {
                try
                {
                    MeasureTransStruct measure = this._currentOptionsLoaderAJ[record.GroupOfSetpoints];
                    this._table.Rows.Add
                    (
                        this._recordNumber, // №                                                                                                                0
                        record.GetDateTime, // 0-6 (Дата/Время)                                                                                                 1
                        StringsAj.Message[record.Message], // 7 (Сообщение)                                                                                     2
                        this.GetTriggeredDefence(record), // 8 : 0-7 биты (Сработанная защита)                                                                  3
                        this.GetParameter(record), // 8 : 8-15 биты (Параметр)                                                                                  4
                        this.GetParametrValue(record, measure), //                                                                                              5   
                        StringsAj.AlarmJournalSetpointsGroup[record.GroupOfSetpoints], // 9 : 0-2 биты (Группа уставок)                                         6
                        ValuesConverterCommon.Analog.GetResist((short)record.Rab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 12                     7 
                        ValuesConverterCommon.Analog.GetResist((short)record.Xab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 13                     8
                        ValuesConverterCommon.Analog.GetResist((short)record.Rbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 14                     9
                        ValuesConverterCommon.Analog.GetResist((short)record.Xbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 15                     10
                        ValuesConverterCommon.Analog.GetResist((short)record.Rca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 16                     11
                        ValuesConverterCommon.Analog.GetResist((short)record.Xca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 17                     12
                      // this.GetStep(record.NumberOfTriggeredParametr),                                                              // 8 : 8-15 биты          13
                        ValuesConverterCommon.Analog.GetResist((short)record.Ra1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 18                     14
                        ValuesConverterCommon.Analog.GetResist((short)record.Xa1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 19                     15
                        ValuesConverterCommon.Analog.GetResist((short)record.Rb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 20                     16
                        ValuesConverterCommon.Analog.GetResist((short)record.Xb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 21                     17
                        ValuesConverterCommon.Analog.GetResist((short)record.Rc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 22                     18
                        ValuesConverterCommon.Analog.GetResist((short)record.Xc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue), // 23                     19
                        ValuesConverterCommon.Analog.GetI(record.Ia, measure.ChannelI.Ittl * 40),                                     // 25                     20
                        ValuesConverterCommon.Analog.GetI(record.Ib, measure.ChannelI.Ittl * 40),                                     // 26                     21  
                        ValuesConverterCommon.Analog.GetI(record.Ic, measure.ChannelI.Ittl * 40),                                     // 27                     22
                        ValuesConverterCommon.Analog.GetI(record.I1, measure.ChannelI.Ittl * 40),                                     // 30                     23
                        ValuesConverterCommon.Analog.GetI(record.I2, measure.ChannelI.Ittl * 40),                                     // 29                     24   
                        ValuesConverterCommon.Analog.GetI(record.I0, measure.ChannelI.Ittl * 40),                                     // 28                     25  
                        ValuesConverterCommon.Analog.GetI(record.In, measure.ChannelI.Ittx * 40),                                     // 32                     26
                        ValuesConverterCommon.Analog.GetI(record.Ig, measure.ChannelI.Ittl * 40),                                     // 33                     27
                        ValuesConverterCommon.Analog.GetI(record.In1, measure.ChannelI.Ittl * 40),                                    // 34                     28
                        ValuesConverterCommon.Analog.GetQ(record.Q),                                                                  // 31                     29
                        ValuesConverterCommon.Analog.GetU(record.Ua, measure.ChannelU.KthlValue),                                     // 35                     30
                        ValuesConverterCommon.Analog.GetU(record.Ub, measure.ChannelU.KthlValue),                                     // 36                     31
                        ValuesConverterCommon.Analog.GetU(record.Uc, measure.ChannelU.KthlValue),                                     // 37                     32
                        ValuesConverterCommon.Analog.GetU(record.Uab, measure.ChannelU.KthlValue),                                    // 38                     33
                        ValuesConverterCommon.Analog.GetU(record.Ubc, measure.ChannelU.KthlValue),                                    // 39                     34
                        ValuesConverterCommon.Analog.GetU(record.Uca, measure.ChannelU.KthlValue),                                    // 40                     35
                        ValuesConverterCommon.Analog.GetU(record.U1, measure.ChannelU.KthlValue),                                     // 43                     36
                        ValuesConverterCommon.Analog.GetU(record.U2, measure.ChannelU.KthlValue),                                     // 42                     37
                        ValuesConverterCommon.Analog.GetU(record.U0, measure.ChannelU.KthlValue),                                     // 41                     38
                        ValuesConverterCommon.Analog.GetF(record.F),                                                                  // 44                     39
                        ValuesConverterCommon.Analog.GetU(record.Un, measure.ChannelU.KthxValue),                                     // 45                     40
                        ValuesConverterCommon.Analog.GetU(record.Un1, measure.ChannelU.Kthx1Value),                                   // 46                     41
                        record.D1To8,                                                                                                 // 47 : 0-7 биты          42
                        record.D9To16,                                                                                                // 47 : 8-15 биты         43  
                        record.D17To24,                                                                                               // 48 : 0-7 биты          44        
                        record.D25To32,                                                                                               // 48 : 8-15 биты         45
                        record.D33To40,                                                                                               // 49 : 0-7 биты          46
                        record.D41To48,                                                                                               // 50 : 8-15 биты         47
                        record.D49To56,                                                                                               // 51 : 0-7 биты          48
                        record.D57To64,                                                                                               // 52 : 8-15 биты         49
                        record.D65To72                                                                                                // 53 : 0-7 биты          50                       
                    );
                }
                catch (Exception e)
                {
                    
                }
                this._recordNumber++;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber - 1);
            this.statusStrip1.Update();
            this.ButtonsEnabled = true;
        }

        private string GetTriggeredDefence(AlarmJournalStruct record) => record.TriggeredDefense == 61 ? this._messages[record.ValueOfTriggeredParametr] : StringsAj.TriggeredDefense[record.TriggeredDefense];
        
        private string GetParameter(AlarmJournalStruct record)
        {
            if (record.TriggeredDefense == 15 || record.TriggeredDefense >= 44 && record.TriggeredDefense <= 59 || record.TriggeredDefense == 61)
            {
                return string.Empty;
            }
            return StringsAj.Parametr[record.NumberOfTriggeredParametr];
        }

        private string GetParametrValue(AlarmJournalStruct record, MeasureTransStruct measure)
        {
            int parameter = record.NumberOfTriggeredParametr; // 8 : 8-15
            ushort value = record.ValueOfTriggeredParametr;   // 10
            if (parameter >= 36 && parameter <= 40 || parameter == 42 || parameter == 65)
            {
                return ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittl * 40);
            }
            if (parameter == 41 || parameter == 43)
            {
                return ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittx * 40);
            }
            if (parameter >= 0 && parameter <= 35)
            {
                ushort value1 = record.ValueOfTriggeredParametr1;
                return ValuesConverterCommon.Analog.GetZ((short)value, (short)value1, measure.ChannelU.KthlValue, measure.ChannelI.Ittl);
            }
            if (parameter >= 44 && parameter <= 52)
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
            }
            if (parameter == 53)
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthxValue);
            }
            if (parameter == 54 || parameter == 69)
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.Kthx1Value);
            }
            if (parameter == 55)
            {
                return ValuesConverterCommon.Analog.GetF(value);
            }
            //if (parameter == StringsAj.OmpInd)
            //{
            //    return ValuesConverterCommon.Analog.GetOmp(value);
            //}
            if (parameter == 60)
            {
                return ValuesConverterCommon.Analog.GetQ(value);
            }
            if (parameter == 61 || parameter == 67 || parameter == 68 || parameter >= 56 && parameter <= 58)
            {
                return string.Empty;
            }
            if (parameter == 64)
            {
                return ValuesConverterCommon.Analog.GetSignF((short)value);
            }
            return value.ToString();
        }

        /// <summary>
        /// Возможно понадобится
        /// </summary>
        private string GetStep(int param)
        {
            if (param >= 0 && param <= 11 || param >= 36) return $"Контур Ф-N{1}";
            if (param > 11 && param <= 17)                return $"Контур Ф-N{2}";
            if (param > 17 && param <= 23)                return $"Контур Ф-N{3}";
            if (param > 23 && param <= 29)                return $"Контур Ф-N{4}";
            if (param > 29 && param <= 35)                return $"Контур Ф-N{5}";
            return $"Контур Ф-N{1}"; // по умолчанию 1я ступень
        }

        #endregion [Methods]

        private void AlarmJournalForm_Load(object sender, EventArgs e)
        {
            this._table = this.GetJournalDataTable();
            this._alarmJournalGrid.DataSource = this._table;

            if (!this._device.IsConnect || !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME);
            this.ButtonsEnabled = false;
            this._statusLabel.Text = READING_LIST_FILE;
            this.statusStrip1.Update();
        }

        private DataTable GetJournalDataTable()
        {
            DataTable table = new DataTable(TABLE_NAME);
            for (int j = 0; j < this._alarmJournalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._alarmJournalGrid.Columns[j].Name);
            }            

            switch (_device.DevicePlant)
            {
                case "T4N4D42R35":
                    _in1Column.Visible = false;
                    _un1Column.Visible = false;
                    _d41to48Column.Visible = false;
                    _d49to56Column.Visible = false;
                    _d57to64Column.Visible = false;
                    _d65to72Column.Visible = false;
                    break;

                case "T4N4D74R67":
                    _in1Column.Visible = false;
                    _un1Column.Visible = false;
                    break;

                case "T4N5D42R35":
                    _in1Column.Visible = false;
                    _d41to48Column.Visible = false;
                    _d49to56Column.Visible = false;
                    _d57to64Column.Visible = false;
                    _d65to72Column.Visible = false;
                    break;

                case "T4N5D74R67":
                    _in1Column.Visible = false;
                    break;
            }
            return table;
        }


        private void OnListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "Операция успешно выполнена")
                {
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            string nameDevice = "";

                            switch (_device.DeviceType)
                            {
                                case "MR771":
                                    nameDevice = "MR771";
                                    break;
                                case "MR761":
                                    nameDevice = "MR76X";
                                    break;
                            }

                            XmlRootAttribute root = new XmlRootAttribute(nameDevice);
                            XmlSerializer serializer = new XmlSerializer(typeof(ListsOfJournals), root);
                            ListsOfJournals lists = (ListsOfJournals)serializer.Deserialize(reader);
                            this._messages = lists.AlarmJournal.MessagesList;
                        }
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(FAIL_READ, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this._messages = PropFormsStrings.GetNewAlarmList();
            }

            this._statusLabel.Text = READ_AJ;
            this._currentOptionsLoaderAJ.StartRead();
        }



        private void SaveToHtml()
        {
            this._saveAlarmJournalHtmlDialog.FileName = $"Журнал аварий {this._device.DeviceType} версия {this._device.DeviceVersion} {this._device.DevicePlant}";
            if (DialogResult.OK != this._saveAlarmJournalHtmlDialog.ShowDialog()) return;
            SaveToHtml(this._table, this._saveAlarmJournalHtmlDialog.FileName, Resources.AlarmJournal);
            this._statusLabel.Text = JOURNAL_SAVED;

        }

        public void SaveToHtml(DataTable dataTable, string fileName, string shema)
        {
            try
            {
                HtmlExport.ExportJournal(this._table, this._saveAlarmJournalHtmlDialog.FileName, shema, this._device.DeviceVersion, this._device.DeviceType, this._device.DeviceNumber.ToString(), this._device.DevicePlant);
                return;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }

        private void _saveButton_Click(object sender, EventArgs e)
        {
            this._saveAlarmJournalDialog.FileName = $"Журнал аварий {this._device.DeviceType} версия {this._device.DeviceVersion} {this._device.DevicePlant}";

            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }

            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
            }
        }

        private void _saveHtmlButton_Click(object sender, EventArgs e)
        {
            SaveToHtml();
        }

        private void _readButton_Click(object sender, EventArgs e)
        {
            if (!this._device.DeviceDlgInfo.IsConnectionMode && !_device.IsConnect) return;
            this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME);
            this.ButtonsEnabled = false;
            this._statusLabel.Text = READING_LIST_FILE;
            this.statusStrip1.Update();
        }

        private void _loadButton_Click(object sender, EventArgs e)
        {
            int j = 0;
            if (this._openAlarmJournalDialog.ShowDialog() != DialogResult.OK) return;
            if (Path.GetExtension(this._openAlarmJournalDialog.FileName).ToLower().Contains("bin"))
            {
                byte[] file = File.ReadAllBytes(this._openAlarmJournalDialog.FileName);
                AlarmJournalStruct journal = new AlarmJournalStruct();
                int size = journal.GetSize();
                List<byte> journalBytes = new List<byte>();
                journalBytes.AddRange(Common.SwapArrayItems(file));
                int countRecord = journalBytes.Count / size;
                for (int i = 0; j < countRecord - 2; i = i + size)
                {
                    journal.InitStruct(journalBytes.GetRange(i, size).ToArray());
                    this._alarmJournal.Value = journal;
                    this.ReadAllRecords();
                    j++;
                }
            }
            else
            {
                this._table.Clear();
                this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._table.Rows.Count);
            }
        }

        private void AlarmJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._alarmJournal?.RemoveStructQueries();
            this._journalLoader?.ClearEvents();
        }
    }
}
