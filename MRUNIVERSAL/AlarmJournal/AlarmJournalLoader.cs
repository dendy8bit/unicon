﻿using System;
using System.Collections.Generic;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MRUNIVERSAL.AlarmJournal.Structures;

namespace BEMN.MRUNIVERSAL.AlarmJournal
{
    public class AlarmJournalLoader
    {
        #region [Events]
        /// <summary>
        /// Успешно прочитаны все записи ЖА
        /// </summary>
        public event Action AllJournalReadOk;
        /// <summary>
        /// Ошибка при чтении ЖА
        /// </summary>
        public event Action AllJournalReadFail;
        #endregion [Events]


        #region [Private fields]
        /// <summary>
        /// ЖА - 1024 слова 
        /// </summary>
        private readonly MemoryEntity<AllAlarmJournalStruct> _allAlarmJournalStruct;
        /// <summary>
        /// ЖА - 64 слова
        /// </summary>
        private readonly MemoryEntity<AlarmJournalStruct> _alarmJournalStruct;
        /// <summary>
        /// Структура записи номер записи ЖА
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _saveIndex;
        /// <summary>
        /// Список структур "AlarmJournalStruct"
        /// </summary>
        private readonly List<AlarmJournalStruct> _journalRecords;
        /// <summary>
        /// Текущий номер записи ЖА
        /// </summary>
        private ushort _recordNumber;
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// Список структур "AlarmJournalStruct"
        /// </summary>
        public List<AlarmJournalStruct> AlarmJournalRecords => this._journalRecords;

        public List<string> MessagesList { get; set; }
        #endregion [Properties]


        #region [Constructor]
        public AlarmJournalLoader(MemoryEntity<AllAlarmJournalStruct> allAlarmJournalStruct, MemoryEntity<AlarmJournalStruct> alarmJournalStruct, MemoryEntity<OneWordStruct> saveIndex)
        {
            this._journalRecords = new List<AlarmJournalStruct>();

            if (allAlarmJournalStruct != null)
            {
                this._allAlarmJournalStruct = allAlarmJournalStruct;
                this._allAlarmJournalStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadRecords); // Добавляем записи в список и записываем следующий индекс
                this._allAlarmJournalStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.OnAllJournalReadFail); // Метод вызывающий событие AllJournalReadFail
            }
            else
            {
                this._alarmJournalStruct = alarmJournalStruct;
                this._alarmJournalStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadRecords); // Добавляем записи в список и записываем следующий индекс
                this._alarmJournalStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.OnAllJournalReadFail); // Метод вызывающий событие AllJournalReadFail
            }

            // Запись номера ЖА
            this._saveIndex = saveIndex;
            this._saveIndex.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this.LoadStruct); // читает структуру AllAlarmJournalStruct или AlarmJournalStruct
            this._saveIndex.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this.OnAllJournalReadFail); // невозможно записать номер читаемой записи ЖА в устройство
        }
        #endregion [Constructor]


        #region [Methods]
        /// <summary>
        /// Запуск чтения журнала аварий (начинается с записи индекса)
        /// </summary>
        public void StartRead()
        {
            this._recordNumber = 0;
            this.SaveIndex();
        }


        private void ReadRecords()
        {
            if (this._allAlarmJournalStruct != null)
            {
                if (this._allAlarmJournalStruct.Value.AllJournalStruct.Count != 0)
                {
                    this._recordNumber += (ushort) this._allAlarmJournalStruct.Value.AllJournalStruct.Count;
                    this._journalRecords.AddRange(this._allAlarmJournalStruct.Value.AllJournalStruct);
                    this.SaveIndex();
                }
                else // если записей не осталось вызываем AllJournalReadOk - конец чтения ЖА
                {
                    this.AllJournalReadOk?.Invoke();
                }
            }
            else
            {
                if (!this._alarmJournalStruct.Value.IsEmpty)
                {
                    this._recordNumber++;
                    var journalRecord = this._alarmJournalStruct.Value.Clone<AlarmJournalStruct>();
                    this._journalRecords.Add(journalRecord);
                    this.SaveIndex();
                }
                else
                {
                    this.AllJournalReadOk?.Invoke();
                }
            }
        }

        private void SaveIndex()
        {
            this._saveIndex.Value.Word = this._recordNumber;
            this._saveIndex.SaveStruct6();
        }

        private void OnAllJournalReadFail()
        {
            this.AllJournalReadFail?.Invoke();
        }

        private void LoadStruct()
        {
            this._allAlarmJournalStruct?.LoadStruct();
            this._alarmJournalStruct?.LoadStruct();
        }

        public void ClearEvents()
        {
            if (this._allAlarmJournalStruct != null)
            {
                this._allAlarmJournalStruct.RemoveStructQueries();
                this._allAlarmJournalStruct.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this.ReadRecords);
                this._allAlarmJournalStruct.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this.OnAllJournalReadFail);
            }
            if (this._alarmJournalStruct != null)
            {
                this._alarmJournalStruct.RemoveStructQueries();
                this._alarmJournalStruct.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this.ReadRecords);
                this._alarmJournalStruct.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this.OnAllJournalReadFail);
            }
            this._saveIndex.RemoveStructQueries();
            this._saveIndex.AllWriteOk -= HandlerHelper.CreateReadArrayHandler(this.LoadStruct);
            this._saveIndex.AllWriteFail -= HandlerHelper.CreateReadArrayHandler(this.OnAllJournalReadFail);
        }

        internal void Clear()
        {
            this._journalRecords.Clear();
        }
        #endregion [Methods]
    }
}