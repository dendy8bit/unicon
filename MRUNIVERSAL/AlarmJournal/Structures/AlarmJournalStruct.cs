﻿using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;

namespace BEMN.MRUNIVERSAL.AlarmJournal.Structures
{
    public class AlarmJournalStruct : StructBase
    {
        public const int BASE_SIZE = 56;

        #region [Private fields]
        /// <summary>
        /// 0 - _year;
        /// 1 - _month;
        /// 2 - _date;
        /// 3 - _hour;
        /// 4 - _minute;
        /// 5 - _second;
        /// 6 - _millisecond;
        ///
        /// 7 - _message;
        ///
        /// 8 - _numOfDefAndNumOfTrigParam;
        /// 9 - _groupOfSetpointsAndTypeDmg;
        /// 10 - _valueOfTriggeredParametr;
        /// 11 - _valueOfTriggeredParametr1;
        ///
        /// 12 - _rab;
        /// 13 - _xab;
        /// 14 - _rbc;
        /// 15 - _xbc;
        /// 16 - _rca;
        /// 17 - _xca;
        ///
        /// 18 - _ra1;
        /// 19 - _xa1;
        /// 20 - _rb1;
        ///
        /// 21 - _xb1;
        /// 22 - _rc1;
        /// 23 - _xc1;
        ///
        /// 24 - _omp;
        ///
        /// 25 - _ia;
        /// 26 - _ib;
        /// 27 - _ic;
        /// 28 - _i0;
        /// 29 - _i2;
        /// 30 - _i1;
        ///
        /// 31 - _q;
        ///
        /// 32 - _in;
        /// 33 - _ig;
        /// 34 - _in1;
        ///
        /// 35 - _ua;
        /// 36 - _ub;
        /// 37 - _uc;
        ///
        /// 38 - _uab;
        /// 39 - _ubc;
        /// 40 - _uca;
        ///
        /// 41 - _u0;
        /// 42 - _u2;
        /// 43 - _u1;
        ///
        /// 44 - _f;
        ///
        /// 45 - _un;
        /// 46 - _un1;
        /// 
        /// 47 - _d1;
        /// 48 - _d2;
        /// 49 - _d3;
        /// 50 - _d4;
        /// 51 - _d5;
        /// 52 - _d6;
        /// 53 - _d7;
        ///
        /// 54 - _rez;
        /// 55 - _spl;
        /// 56 - _omperr;
        ///
        /// </summary>
        [Layout(0, Count = BASE_SIZE)] private ushort[] _words;
        #endregion [Private fields]

        #region [Properties]
        public List<string> MessagesList { get; set; }

        public ushort Year => this._words[0];
        public ushort Month => this._words[1];
        public ushort Date => this._words[2];
        public ushort Hour => this._words[3];
        public ushort Minute => this._words[4];
        public ushort Second => this._words[5];
        public ushort Millisecond => this._words[6];

        public ushort Message => this._words[7];

        public int TriggeredDefense => Common.GetBits(this._words[8], 0, 1, 2, 3, 4, 5, 6, 7);
        public int NumberOfTriggeredParametr => Common.GetBits(this._words[8], 8, 9, 10, 11, 12, 13, 14, 15) >> 8;

        public int GroupOfSetpoints => Common.GetBits(this._words[9], 0, 1, 2);
        public int TypeOfDmg => Common.GetBits(this._words[9], 8, 9, 10, 11) >> 8;

        public ushort ValueOfTriggeredParametr => this._words[10];
        public ushort ValueOfTriggeredParametr1 => this._words[11];

        public ushort Rab => this._words[12];
        public ushort Xab => this._words[13];
        public ushort Rbc => this._words[14];
        public ushort Xbc => this._words[15];
        public ushort Rca => this._words[16];
        public ushort Xca => this._words[17];

        public ushort Ra1 => this._words[18];
        public ushort Xa1 => this._words[19];
        public ushort Rb1 => this._words[20];
        public ushort Xb1 => this._words[21];
        public ushort Rc1 => this._words[22];
        public ushort Xc1 => this._words[23];

        public ushort OMP => this._words[24];

        public ushort Ia => this._words[25];
        public ushort Ib => this._words[26];
        public ushort Ic => this._words[27];
        public ushort I0 => this._words[28];
        public ushort I2 => this._words[29];
        public ushort I1 => this._words[30];

        public ushort Q => this._words[31];

        public ushort In => this._words[32];
        public ushort Ig => this._words[33];
        public ushort In1 => this._words[34];

        public ushort Ua => this._words[35];
        public ushort Ub => this._words[36];
        public ushort Uc => this._words[37];
        public ushort Uab => this._words[38];
        public ushort Ubc => this._words[39];
        public ushort Uca => this._words[40];
        public ushort U0 => this._words[41];
        public ushort U2 => this._words[42];
        public ushort U1 => this._words[43];

        public ushort F => this._words[44];

        public ushort Un => this._words[45];
        public ushort Un1 => this._words[46];

        public ushort D1 => this._words[47];
        public ushort D2 => this._words[48];
        public ushort D3 => this._words[49];
        public ushort D4 => this._words[50];
        public ushort D5 => this._words[51];
        public ushort D6 => this._words[52];
        public ushort D7 => this._words[53];

        private ushort Rez => this._words[54];
        private ushort Spl => this._words[55];
        private ushort Omperr => this._words[56]; // нет
        
        /// <summary>
        /// true если во всех полях 0, условие конца ЖА
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                var sum = this.Year + this.Month + this.Date + this.Hour + this.Minute + this.Second + this.Millisecond + this.Message;
                return sum == 0;
            }
        }

        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetDateTime => $"{this.Date:d2}.{this.Month:d2}.{this.Year:d2} {this.Hour:d2}:{this.Minute:d2}:{this.Second:d2},{this.Millisecond:d3}";
   
        /// <summary>
        /// //Значение сработанного параметра
        /// </summary>
        public string GetValueTriggedOption(List<ushort> factors)
        {
            if (this.NumberOfTriggeredParametr < 18)
                return ValuesConverterCommon.Analog.GetI901(this.ValueOfTriggeredParametr, factors[0]);
            if (this.NumberOfTriggeredParametr < 42)
            {
                //int parametr = this.NumberOfTriggeredParametr - 18;
                return ValuesConverterCommon.Analog.GetI901(this.ValueOfTriggeredParametr, factors[3]);
            }
            //if (this._numberOfTriggeredParametr < 44)
            //return Strings.AlarmJournalExternalDefenseTriggedOption[this._valueOfTriggeredParametr];
            //if (this._numberOfTriggeredParametr == 44)
            //return string.Format(SPL_PATTERN, this._valueOfTriggeredParametr);
            return string.Empty;
        }

        
        public string D1To8 => Common.ByteToMask(Common.LOBYTE(this.D1), true);
        public string D9To16 => Common.ByteToMask(Common.HIBYTE(this.D1), true);
        public string D17To24 => Common.ByteToMask(Common.LOBYTE(this.D2), true);
        public string D25To32 => Common.ByteToMask(Common.HIBYTE(this.D2), true);
        public string D33To40 => Common.ByteToMask(Common.LOBYTE(this.D3), true);
        public string D41To48 => Common.ByteToMask(Common.HIBYTE(this.D3), true);
        public string D49To56 => Common.ByteToMask(Common.LOBYTE(this.D4), true);
        public string D57To64 => Common.ByteToMask(Common.HIBYTE(this.D4), true);
        public string D65To72 => Common.ByteToMask(Common.LOBYTE(this.D5), true);
        public string D73To80 => Common.ByteToMask(Common.HIBYTE(this.D5), true);
        public string D81To88 => Common.ByteToMask(Common.LOBYTE(this.D6), true);
        public string D89To96 => Common.ByteToMask(Common.HIBYTE(this.D6), true);
        public string D97To104 => Common.ByteToMask(Common.LOBYTE(this.D7), true);
        public string D105To112 => Common.ByteToMask(Common.HIBYTE(this.D7), true);

        #endregion [Properties]
    }
}
