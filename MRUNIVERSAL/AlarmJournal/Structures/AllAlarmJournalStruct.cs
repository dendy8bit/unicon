﻿using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MRUNIVERSAL.AlarmJournal.Structures
{
    public class AllAlarmJournalStruct : StructBase
    {
        [Layout(0, Count = 18)] AlarmJournalStruct[] _alarmJournalStruct;

        public List<AlarmJournalStruct> AllJournalStruct
        {
            get { return new List<AlarmJournalStruct>(this._alarmJournalStruct.Where(j => !j.IsEmpty)); }
        }
    }
}
