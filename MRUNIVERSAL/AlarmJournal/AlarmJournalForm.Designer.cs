﻿namespace BEMN.MRUNIVERSAL.AlarmJournal
{
    partial class AlarmJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            this._alarmJournalGrid = new System.Windows.Forms.DataGridView();
            this._indexColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dateTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._messageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._defenseColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._paramColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._paramValueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._groupColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._rabColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xabColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._rbcColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xbcColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._rcaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xcaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._raColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._rbColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xbColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._rcColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xcColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ibColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._icColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i2Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._3i0Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._inColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._irColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._in1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._qColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ubColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ucColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uabColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ubcColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ucaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._u1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._u2Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._3u0Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._fColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._unColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._un1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d1to8Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d9to16Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d17to24Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d25to32Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d33to40Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d41to48Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d49to56Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d57to64Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d65to72Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._readButton = new System.Windows.Forms.Button();
            this._loadButton = new System.Windows.Forms.Button();
            this._saveButton = new System.Windows.Forms.Button();
            this._saveHtmlButton = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._openAlarmJournalDialog = new System.Windows.Forms.OpenFileDialog();
            this._saveAlarmJournalDialog = new System.Windows.Forms.SaveFileDialog();
            this._saveAlarmJournalHtmlDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _alarmJournalGrid
            // 
            this._alarmJournalGrid.AllowUserToAddRows = false;
            this._alarmJournalGrid.AllowUserToDeleteRows = false;
            this._alarmJournalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._alarmJournalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._alarmJournalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._alarmJournalGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._alarmJournalGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this._alarmJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._alarmJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexColumn,
            this._dateTimeColumn,
            this._messageColumn,
            this._defenseColumn,
            this._paramColumn,
            this._paramValueColumn,
            this._groupColumn,
            this._rabColumn,
            this._xabColumn,
            this._rbcColumn,
            this._xbcColumn,
            this._rcaColumn,
            this._xcaColumn,
            this._raColumn,
            this._xaColumn,
            this._rbColumn,
            this._xbColumn,
            this._rcColumn,
            this._xcColumn,
            this._iaColumn,
            this._ibColumn,
            this._icColumn,
            this._i1Column,
            this._i2Column,
            this._3i0Column,
            this._inColumn,
            this._irColumn,
            this._in1Column,
            this._qColumn,
            this._uaColumn,
            this._ubColumn,
            this._ucColumn,
            this._uabColumn,
            this._ubcColumn,
            this._ucaColumn,
            this._u1Column,
            this._u2Column,
            this._3u0Column,
            this._fColumn,
            this._unColumn,
            this._un1Column,
            this._d1to8Column,
            this._d9to16Column,
            this._d17to24Column,
            this._d25to32Column,
            this._d33to40Column,
            this._d41to48Column,
            this._d49to56Column,
            this._d57to64Column,
            this._d65to72Column});
            this._alarmJournalGrid.Location = new System.Drawing.Point(3, 2);
            this._alarmJournalGrid.Name = "_alarmJournalGrid";
            this._alarmJournalGrid.ReadOnly = true;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._alarmJournalGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this._alarmJournalGrid.RowHeadersVisible = false;
            this._alarmJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._alarmJournalGrid.Size = new System.Drawing.Size(699, 535);
            this._alarmJournalGrid.TabIndex = 1;
            // 
            // _indexColumn
            // 
            this._indexColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._indexColumn.DataPropertyName = "_indexColumn";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._indexColumn.DefaultCellStyle = dataGridViewCellStyle11;
            this._indexColumn.HeaderText = "№";
            this._indexColumn.Name = "_indexColumn";
            this._indexColumn.ReadOnly = true;
            this._indexColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexColumn.Width = 24;
            // 
            // _dateTimeColumn
            // 
            this._dateTimeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._dateTimeColumn.DataPropertyName = "_dateTimeColumn";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._dateTimeColumn.DefaultCellStyle = dataGridViewCellStyle12;
            this._dateTimeColumn.HeaderText = "Дата/Время";
            this._dateTimeColumn.Name = "_dateTimeColumn";
            this._dateTimeColumn.ReadOnly = true;
            this._dateTimeColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dateTimeColumn.Width = 77;
            // 
            // _messageColumn
            // 
            this._messageColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._messageColumn.DataPropertyName = "_messageColumn";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._messageColumn.DefaultCellStyle = dataGridViewCellStyle13;
            this._messageColumn.HeaderText = "Тип события";
            this._messageColumn.Name = "_messageColumn";
            this._messageColumn.ReadOnly = true;
            this._messageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._messageColumn.Width = 70;
            // 
            // _defenseColumn
            // 
            this._defenseColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._defenseColumn.DataPropertyName = "_defenseColumn";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._defenseColumn.DefaultCellStyle = dataGridViewCellStyle14;
            this._defenseColumn.HeaderText = "Сообщение";
            this._defenseColumn.Name = "_defenseColumn";
            this._defenseColumn.ReadOnly = true;
            this._defenseColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._defenseColumn.Width = 71;
            // 
            // _paramColumn
            // 
            this._paramColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._paramColumn.DataPropertyName = "_paramColumn";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._paramColumn.DefaultCellStyle = dataGridViewCellStyle15;
            this._paramColumn.HeaderText = "Параметр срабатывания";
            this._paramColumn.Name = "_paramColumn";
            this._paramColumn.ReadOnly = true;
            this._paramColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._paramColumn.Width = 126;
            // 
            // _paramValueColumn
            // 
            this._paramValueColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._paramValueColumn.DataPropertyName = "_paramValueColumn";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._paramValueColumn.DefaultCellStyle = dataGridViewCellStyle16;
            this._paramValueColumn.HeaderText = "Значение параметра срабатывания";
            this._paramValueColumn.Name = "_paramValueColumn";
            this._paramValueColumn.ReadOnly = true;
            this._paramValueColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._paramValueColumn.Width = 176;
            // 
            // _groupColumn
            // 
            this._groupColumn.DataPropertyName = "_groupColumn";
            this._groupColumn.HeaderText = "Группа уставок";
            this._groupColumn.Name = "_groupColumn";
            this._groupColumn.ReadOnly = true;
            this._groupColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._groupColumn.Width = 82;
            // 
            // _rabColumn
            // 
            this._rabColumn.DataPropertyName = "_rabColumn";
            this._rabColumn.HeaderText = "Rab";
            this._rabColumn.Name = "_rabColumn";
            this._rabColumn.ReadOnly = true;
            this._rabColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._rabColumn.Width = 33;
            // 
            // _xabColumn
            // 
            this._xabColumn.DataPropertyName = "_xabColumn";
            this._xabColumn.HeaderText = "Xab";
            this._xabColumn.Name = "_xabColumn";
            this._xabColumn.ReadOnly = true;
            this._xabColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._xabColumn.Width = 32;
            // 
            // _rbcColumn
            // 
            this._rbcColumn.DataPropertyName = "_rbcColumn";
            this._rbcColumn.HeaderText = "Rbc";
            this._rbcColumn.Name = "_rbcColumn";
            this._rbcColumn.ReadOnly = true;
            this._rbcColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._rbcColumn.Width = 33;
            // 
            // _xbcColumn
            // 
            this._xbcColumn.DataPropertyName = "_xbcColumn";
            this._xbcColumn.HeaderText = "Xbc";
            this._xbcColumn.Name = "_xbcColumn";
            this._xbcColumn.ReadOnly = true;
            this._xbcColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._xbcColumn.Width = 32;
            // 
            // _rcaColumn
            // 
            this._rcaColumn.DataPropertyName = "_rcaColumn";
            this._rcaColumn.HeaderText = "Rca";
            this._rcaColumn.Name = "_rcaColumn";
            this._rcaColumn.ReadOnly = true;
            this._rcaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._rcaColumn.Width = 33;
            // 
            // _xcaColumn
            // 
            this._xcaColumn.DataPropertyName = "_xcaColumn";
            this._xcaColumn.HeaderText = "Xca";
            this._xcaColumn.Name = "_xcaColumn";
            this._xcaColumn.ReadOnly = true;
            this._xcaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._xcaColumn.Width = 32;
            // 
            // _raColumn
            // 
            this._raColumn.DataPropertyName = "_raColumn";
            this._raColumn.HeaderText = "Ra";
            this._raColumn.Name = "_raColumn";
            this._raColumn.ReadOnly = true;
            this._raColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._raColumn.Width = 27;
            // 
            // _xaColumn
            // 
            this._xaColumn.DataPropertyName = "_xaColumn";
            this._xaColumn.HeaderText = "Xa";
            this._xaColumn.Name = "_xaColumn";
            this._xaColumn.ReadOnly = true;
            this._xaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._xaColumn.Width = 26;
            // 
            // _rbColumn
            // 
            this._rbColumn.DataPropertyName = "_rbColumn";
            this._rbColumn.HeaderText = "Rb";
            this._rbColumn.Name = "_rbColumn";
            this._rbColumn.ReadOnly = true;
            this._rbColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._rbColumn.Width = 27;
            // 
            // _xbColumn
            // 
            this._xbColumn.DataPropertyName = "_xbColumn";
            this._xbColumn.HeaderText = "Xb";
            this._xbColumn.Name = "_xbColumn";
            this._xbColumn.ReadOnly = true;
            this._xbColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._xbColumn.Width = 26;
            // 
            // _rcColumn
            // 
            this._rcColumn.DataPropertyName = "_rcColumn";
            this._rcColumn.HeaderText = "Rc";
            this._rcColumn.Name = "_rcColumn";
            this._rcColumn.ReadOnly = true;
            this._rcColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._rcColumn.Width = 27;
            // 
            // _xcColumn
            // 
            this._xcColumn.DataPropertyName = "_xcColumn";
            this._xcColumn.HeaderText = "Xc";
            this._xcColumn.Name = "_xcColumn";
            this._xcColumn.ReadOnly = true;
            this._xcColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._xcColumn.Width = 26;
            // 
            // _iaColumn
            // 
            this._iaColumn.DataPropertyName = "_iaColumn";
            this._iaColumn.HeaderText = "Ia";
            this._iaColumn.Name = "_iaColumn";
            this._iaColumn.ReadOnly = true;
            this._iaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iaColumn.Width = 22;
            // 
            // _ibColumn
            // 
            this._ibColumn.DataPropertyName = "_ibColumn";
            this._ibColumn.HeaderText = "Ib";
            this._ibColumn.Name = "_ibColumn";
            this._ibColumn.ReadOnly = true;
            this._ibColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ibColumn.Width = 22;
            // 
            // _icColumn
            // 
            this._icColumn.DataPropertyName = "_icColumn";
            this._icColumn.HeaderText = "Ic";
            this._icColumn.Name = "_icColumn";
            this._icColumn.ReadOnly = true;
            this._icColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._icColumn.Width = 22;
            // 
            // _i1Column
            // 
            this._i1Column.DataPropertyName = "_i1Column";
            this._i1Column.HeaderText = "I1";
            this._i1Column.Name = "_i1Column";
            this._i1Column.ReadOnly = true;
            this._i1Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i1Column.Width = 22;
            // 
            // _i2Column
            // 
            this._i2Column.DataPropertyName = "_i2Column";
            this._i2Column.HeaderText = "I2";
            this._i2Column.Name = "_i2Column";
            this._i2Column.ReadOnly = true;
            this._i2Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i2Column.Width = 22;
            // 
            // _3i0Column
            // 
            this._3i0Column.DataPropertyName = "_3i0Column";
            this._3i0Column.HeaderText = "3I0";
            this._3i0Column.Name = "_3i0Column";
            this._3i0Column.ReadOnly = true;
            this._3i0Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._3i0Column.Width = 28;
            // 
            // _inColumn
            // 
            this._inColumn.DataPropertyName = "_inColumn";
            this._inColumn.HeaderText = "In";
            this._inColumn.Name = "_inColumn";
            this._inColumn.ReadOnly = true;
            this._inColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._inColumn.Width = 22;
            // 
            // _irColumn
            // 
            this._irColumn.DataPropertyName = "_irColumn";
            this._irColumn.HeaderText = "Ir";
            this._irColumn.Name = "_irColumn";
            this._irColumn.ReadOnly = true;
            this._irColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._irColumn.Width = 19;
            // 
            // _in1Column
            // 
            this._in1Column.DataPropertyName = "_in1Column";
            this._in1Column.HeaderText = "In1";
            this._in1Column.Name = "_in1Column";
            this._in1Column.ReadOnly = true;
            this._in1Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._in1Column.Width = 28;
            // 
            // _qColumn
            // 
            this._qColumn.DataPropertyName = "_qColumn";
            this._qColumn.HeaderText = "Q";
            this._qColumn.Name = "_qColumn";
            this._qColumn.ReadOnly = true;
            this._qColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._qColumn.Width = 21;
            // 
            // _uaColumn
            // 
            this._uaColumn.DataPropertyName = "_uaColumn";
            this._uaColumn.HeaderText = "Ua";
            this._uaColumn.Name = "_uaColumn";
            this._uaColumn.ReadOnly = true;
            this._uaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uaColumn.Width = 27;
            // 
            // _ubColumn
            // 
            this._ubColumn.DataPropertyName = "_ubColumn";
            this._ubColumn.HeaderText = "Ub";
            this._ubColumn.Name = "_ubColumn";
            this._ubColumn.ReadOnly = true;
            this._ubColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ubColumn.Width = 27;
            // 
            // _ucColumn
            // 
            this._ucColumn.DataPropertyName = "_ucColumn";
            this._ucColumn.HeaderText = "Uc";
            this._ucColumn.Name = "_ucColumn";
            this._ucColumn.ReadOnly = true;
            this._ucColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ucColumn.Width = 27;
            // 
            // _uabColumn
            // 
            this._uabColumn.DataPropertyName = "_uabColumn";
            this._uabColumn.HeaderText = "Uab";
            this._uabColumn.Name = "_uabColumn";
            this._uabColumn.ReadOnly = true;
            this._uabColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uabColumn.Width = 33;
            // 
            // _ubcColumn
            // 
            this._ubcColumn.DataPropertyName = "_ubcColumn";
            this._ubcColumn.HeaderText = "Ubc";
            this._ubcColumn.Name = "_ubcColumn";
            this._ubcColumn.ReadOnly = true;
            this._ubcColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ubcColumn.Width = 33;
            // 
            // _ucaColumn
            // 
            this._ucaColumn.DataPropertyName = "_ucaColumn";
            this._ucaColumn.HeaderText = "Uca";
            this._ucaColumn.Name = "_ucaColumn";
            this._ucaColumn.ReadOnly = true;
            this._ucaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ucaColumn.Width = 33;
            // 
            // _u1Column
            // 
            this._u1Column.DataPropertyName = "_u1Column";
            this._u1Column.HeaderText = "U1";
            this._u1Column.Name = "_u1Column";
            this._u1Column.ReadOnly = true;
            this._u1Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._u1Column.Width = 27;
            // 
            // _u2Column
            // 
            this._u2Column.DataPropertyName = "_u2Column";
            this._u2Column.HeaderText = "U2";
            this._u2Column.Name = "_u2Column";
            this._u2Column.ReadOnly = true;
            this._u2Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._u2Column.Width = 27;
            // 
            // _3u0Column
            // 
            this._3u0Column.DataPropertyName = "_3u0Column";
            this._3u0Column.HeaderText = "3U0";
            this._3u0Column.Name = "_3u0Column";
            this._3u0Column.ReadOnly = true;
            this._3u0Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._3u0Column.Width = 33;
            // 
            // _fColumn
            // 
            this._fColumn.DataPropertyName = "_fColumn";
            this._fColumn.HeaderText = "F";
            this._fColumn.Name = "_fColumn";
            this._fColumn.ReadOnly = true;
            this._fColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._fColumn.Width = 19;
            // 
            // _unColumn
            // 
            this._unColumn.DataPropertyName = "_unColumn";
            this._unColumn.HeaderText = "Un";
            this._unColumn.Name = "_unColumn";
            this._unColumn.ReadOnly = true;
            this._unColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._unColumn.Width = 27;
            // 
            // _un1Column
            // 
            this._un1Column.DataPropertyName = "_un1Column";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._un1Column.DefaultCellStyle = dataGridViewCellStyle17;
            this._un1Column.HeaderText = "Un1";
            this._un1Column.Name = "_un1Column";
            this._un1Column.ReadOnly = true;
            this._un1Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._un1Column.Width = 33;
            // 
            // _d1to8Column
            // 
            this._d1to8Column.DataPropertyName = "_d1to8Column";
            this._d1to8Column.HeaderText = "Д [1 - 8]";
            this._d1to8Column.Name = "_d1to8Column";
            this._d1to8Column.ReadOnly = true;
            this._d1to8Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d1to8Column.Width = 39;
            // 
            // _d9to16Column
            // 
            this._d9to16Column.DataPropertyName = "_d9to16Column";
            this._d9to16Column.HeaderText = "Д [8 - 16]";
            this._d9to16Column.Name = "_d9to16Column";
            this._d9to16Column.ReadOnly = true;
            this._d9to16Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d9to16Column.Width = 39;
            // 
            // _d17to24Column
            // 
            this._d17to24Column.DataPropertyName = "_d17to24Column";
            this._d17to24Column.HeaderText = "Д [17 - 24]";
            this._d17to24Column.Name = "_d17to24Column";
            this._d17to24Column.ReadOnly = true;
            this._d17to24Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d17to24Column.Width = 44;
            // 
            // _d25to32Column
            // 
            this._d25to32Column.DataPropertyName = "_d25to32Column";
            this._d25to32Column.HeaderText = "Д [25 - 32]";
            this._d25to32Column.Name = "_d25to32Column";
            this._d25to32Column.ReadOnly = true;
            this._d25to32Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d25to32Column.Width = 44;
            // 
            // _d33to40Column
            // 
            this._d33to40Column.DataPropertyName = "_d33to40Column";
            this._d33to40Column.HeaderText = "Д [33 - 40]";
            this._d33to40Column.Name = "_d33to40Column";
            this._d33to40Column.ReadOnly = true;
            this._d33to40Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d33to40Column.Width = 44;
            // 
            // _d41to48Column
            // 
            this._d41to48Column.DataPropertyName = "_d41to48Column";
            this._d41to48Column.HeaderText = "Д [41 - 48]";
            this._d41to48Column.Name = "_d41to48Column";
            this._d41to48Column.ReadOnly = true;
            this._d41to48Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d41to48Column.Width = 44;
            // 
            // _d49to56Column
            // 
            this._d49to56Column.DataPropertyName = "_d49to56Column";
            this._d49to56Column.HeaderText = "Д [49 - 56]";
            this._d49to56Column.Name = "_d49to56Column";
            this._d49to56Column.ReadOnly = true;
            this._d49to56Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d49to56Column.Width = 44;
            // 
            // _d57to64Column
            // 
            this._d57to64Column.DataPropertyName = "_d57to64Column";
            this._d57to64Column.HeaderText = "Д [57 - 64]";
            this._d57to64Column.Name = "_d57to64Column";
            this._d57to64Column.ReadOnly = true;
            this._d57to64Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d57to64Column.Width = 44;
            // 
            // _d65to72Column
            // 
            this._d65to72Column.DataPropertyName = "_d65to72Column";
            this._d65to72Column.HeaderText = "Д [65 - 72]";
            this._d65to72Column.Name = "_d65to72Column";
            this._d65to72Column.ReadOnly = true;
            this._d65to72Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d65to72Column.Width = 44;
            // 
            // _readButton
            // 
            this._readButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readButton.Location = new System.Drawing.Point(3, 543);
            this._readButton.Name = "_readButton";
            this._readButton.Size = new System.Drawing.Size(122, 23);
            this._readButton.TabIndex = 2;
            this._readButton.Text = "Прочитать";
            this._readButton.UseVisualStyleBackColor = true;
            this._readButton.Click += new System.EventHandler(this._readButton_Click);
            // 
            // _loadButton
            // 
            this._loadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._loadButton.Location = new System.Drawing.Point(324, 543);
            this._loadButton.Name = "_loadButton";
            this._loadButton.Size = new System.Drawing.Size(122, 23);
            this._loadButton.TabIndex = 3;
            this._loadButton.Text = "Загрузить из файла";
            this._loadButton.UseVisualStyleBackColor = true;
            this._loadButton.Click += new System.EventHandler(this._loadButton_Click);
            // 
            // _saveButton
            // 
            this._saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveButton.Location = new System.Drawing.Point(452, 543);
            this._saveButton.Name = "_saveButton";
            this._saveButton.Size = new System.Drawing.Size(122, 23);
            this._saveButton.TabIndex = 4;
            this._saveButton.Text = "Сохранить в файл";
            this._saveButton.UseVisualStyleBackColor = true;
            this._saveButton.Click += new System.EventHandler(this._saveButton_Click);
            // 
            // _saveHtmlButton
            // 
            this._saveHtmlButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveHtmlButton.Location = new System.Drawing.Point(580, 543);
            this._saveHtmlButton.Name = "_saveHtmlButton";
            this._saveHtmlButton.Size = new System.Drawing.Size(122, 23);
            this._saveHtmlButton.TabIndex = 5;
            this._saveHtmlButton.Text = "Cохранить в HTML";
            this._saveHtmlButton.UseVisualStyleBackColor = true;
            this._saveHtmlButton.Click += new System.EventHandler(this._saveHtmlButton_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 569);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(706, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(10, 17);
            this._statusLabel.Text = ".";
            // 
            // _openAlarmJournalDialog
            // 
            this._openAlarmJournalDialog.DefaultExt = "xml";
            this._openAlarmJournalDialog.FileName = "Журнал аварий МР";
            this._openAlarmJournalDialog.Filter = "(*.xml) | *.xml";
            // 
            // _saveAlarmJournalDialog
            // 
            this._saveAlarmJournalDialog.DefaultExt = "xml";
            this._saveAlarmJournalDialog.FileName = "Журнал аварий МР";
            this._saveAlarmJournalDialog.Filter = "(*.xml) | *.xml";
            // 
            // _saveAlarmJournalHtmlDialog
            // 
            this._saveAlarmJournalHtmlDialog.DefaultExt = "html";
            this._saveAlarmJournalHtmlDialog.FileName = "Журнал аварий МР";
            this._saveAlarmJournalHtmlDialog.Filter = "(*.html) | *.html";
            // 
            // AlarmJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 591);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._saveHtmlButton);
            this.Controls.Add(this._saveButton);
            this.Controls.Add(this._loadButton);
            this.Controls.Add(this._readButton);
            this.Controls.Add(this._alarmJournalGrid);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1200, 800);
            this.MinimumSize = new System.Drawing.Size(722, 630);
            this.Name = "AlarmJournalForm";
            this.Text = "AlarmJournalForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AlarmJournalForm_FormClosing);
            this.Load += new System.EventHandler(this.AlarmJournalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView _alarmJournalGrid;
        private System.Windows.Forms.Button _readButton;
        private System.Windows.Forms.Button _loadButton;
        private System.Windows.Forms.Button _saveButton;
        private System.Windows.Forms.Button _saveHtmlButton;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.OpenFileDialog _openAlarmJournalDialog;
        private System.Windows.Forms.SaveFileDialog _saveAlarmJournalDialog;
        private System.Windows.Forms.SaveFileDialog _saveAlarmJournalHtmlDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dateTimeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _messageColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _defenseColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _paramColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _paramValueColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _groupColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _rabColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xabColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _rbcColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xbcColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _rcaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xcaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _raColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _rbColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xbColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _rcColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xcColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ibColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _icColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _i1Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _i2Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _3i0Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _inColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _irColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _in1Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _qColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ubColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ucColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uabColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ubcColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ucaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _u1Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _u2Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _3u0Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _fColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _unColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _un1Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d1to8Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d9to16Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d17to24Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d25to32Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d33to40Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d41to48Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d49to56Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d57to64Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d65to72Column;
    }
}