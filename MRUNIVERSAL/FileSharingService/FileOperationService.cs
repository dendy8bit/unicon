﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using BEMN.Devices;
using BEMN.MR771.FileSharingService.FileOperations;

namespace BEMN.MR771.FileSharingService
{
    public class FileOperationService
    {
        private byte _currentSessionNum;
        private string _password;
        private Device _device;
        private Queue<Operator> _operatorsQueue;
        private Thread _queueThread;
        private Dictionary<int, string> _messagesDictionary;

        private SessionNumberOperator _sessionNumberOperator;

        public FileOperationService(Device device)
        {
            this._device = device;
            this._currentSessionNum = 0;
            this._operatorsQueue = new Queue<Operator>();
            this._queueThread = new Thread(this.QueueCheckCycle);
            this.InitErrorMessages();
        }

        private void GeneratePassword()
        {
            string pass = "АААА"; //русские буквы А
            Encoding ascii = Encoding.GetEncoding("windows-1251");
            byte[] asciiBytes = ascii.GetBytes(pass);
            this._password = string.Empty;
            foreach (byte c in asciiBytes)
            {
                this._currentSessionNum = (byte)((this._currentSessionNum * 99 + 53) % 256);
                this._password += ((byte)(c + this._currentSessionNum)).ToString("X2");
            }
        }

        private bool InsertQueue(Operator op)
        {
            if (!this._operatorsQueue.Any(o => o.OperatorName == op.OperatorName && o.CurrentDevice.Equals(op.CurrentDevice)))
            {
                this._operatorsQueue.Enqueue(op);
                if (this._queueThread.ThreadState != ThreadState.Running)
                {
                    this._queueThread.Start();
                }
                return true;
            }
            return false;
        }

        private void QueueCheckCycle()
        {
            while (this._operatorsQueue.Count != 0)
            {
                Operator op = this._operatorsQueue.Dequeue();
                this.PerformCurrentOperator(op);
            }
        }

        private void PerformCurrentOperator(Operator op)
        {
            if (op is SessionNumberOperator)
            {
                ((SessionNumberOperator)op).Perform(this.SessionNumberRead, null);
            }
        }

        public void GetSessionNumber()
        {
            if(this._currentSessionNum != 0) return;
            this._sessionNumberOperator = new SessionNumberOperator(this._device, "SessionNumberOperator");
            this.InsertQueue(this._sessionNumberOperator);
        }

        private void SessionNumberRead(int key)
        {
            if (key == 0)
            {
                this._currentSessionNum = this._sessionNumberOperator.SessionNumber;
            }
            else
            {
                
            }
        }

        private void InitErrorMessages()
        {
            this._messagesDictionary = new Dictionary<int, string>
            {
                {0,"Операция успешно выполнена"},
                {1,"Обращение к зарезервированной памяти"},
                {2,"Подана неверная команда"},
                {5,"Слишком много открытых файлов"},
                {6,"Файл еще не открыт"},
                {7,"Неверный пароль"},
                {8,"Ошибка дескриптора файла"},
                {9,"Ошибка CRC"},
                {101,"Произошла невосстановимая ошибка на низком уровне"},
                {102,"Ошибка структуры FAT на томе или рабочая область испорчена"},
                {103,"Диск не готов"},
                {104,"Файл не найден"},
                {105,"Не найден путь"},
                {106,"Указанная строка содержит недопустимое имя"},
                {107,"Отканано в доступе"},
                {108,"Файл или папка с таким именем уже существуют"},
                {109,"Предоставленная структура объекта\nфайла/директории ошибочна"},
                {110,"Дествие произведено на защищенном\nот записи носителе данных"},
                {111,"Указан недопустимый номер диска"},
                {112,"Рабочая область логического диска\nне зарегистрирована"},
                {113,"На диске нет рабочего тома с файловой системой FAT"},
                {114,"Функция остановлена перед началом форматирования"},
                {115,"Функция остановлена из-за таймаута\nв безопасном управлении потоками"},
                {116,"Доступ к файлу отклонен управлением\nсовместного доступа к файлу.\nПерезагрузите, пожалуйста, устройство"},
                {117,"Недостаточно памяти для выполнения операции"},
                {118,"Количество открытых файлов достигло\nмаксимального количества"},
                {119,"Указанный параметр недопустим"},
                {224,"Операция не была выполнена"},
                {255,"Нет связи с устройством, невозможно выполнить операцию"}
            };
        }
    }
}
