﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using BEMN.Compressor;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MRUNIVERSAL.Properties;
using BMTCD.HelperClasses;
using BMTCD.MR771;
using Crownwood.Magic.Common;
using Crownwood.Magic.Docking;
using Crownwood.Magic.Menus;
using SchemeEditorSystem;
using SchemeEditorSystem.descriptors;
using SchemeEditorSystem.ResourceLibs;

namespace BEMN.MRUNIVERSAL.BSBGL
{
    public partial class BSBGLEF : Form, IFormView
    {
        #region Variables
        private const int COUNT_EXCHANGES_PROGRAM = 64; //Для аналоговых сигналов защит
        private const int RST_BAUDE_RATE = 115200;
        private MemoryEntity<StartStruct> _startProgramStruct;
        private MemoryEntity<SourceProgramStruct> _currentSourceProgramStruct;
        private MemoryEntity<LogicProgramSignals> _currentSignalsStruct;
        private MemoryEntity<OneWordStruct> _currentProgramPage;
        private MessageBoxForm _formCheck;
        private OutputWindow _outputWindow;
        private Content _outputContent;
        private MRUniversal _device;
        private FileDriver _fileDriver;
        private NewSchematicForm _newForm;
        private DockingManager _manager;
        private NewLogicCompiler _compiller;
        private bool _isOnSimulateMode;
        private bool _isCompileScheme;
        private LibraryBox _libraryWindow;
        private Content _libraryContent;
        private VisualStyle _style;
        private StatusBar _statusBar;
        private Crownwood.Magic.Controls.TabControl _filler;
        private ushort[] _compressedZip;
        private ushort[] _binFile;
        private ushort _pageIndex;
        private ToolStrip _toolStrip;
        private MenuControl _mainMenu;
        private string _docName;
        private string _projName;
        #endregion

        #region Constructor

        public BSBGLEF()
        {
            this.InitializeComponent();
        }

        public BSBGLEF(MRUniversal device)
        {
            this._device = device;
            this.InitializeComponent();
            this.InitForm();
            this._currentSourceProgramStruct = this._device.SourceProgramStruct;
            this._startProgramStruct = this._device.ProgramStartStruct;
            this._currentSignalsStruct = this._device.ProgramSignalsStruct;
            this._currentProgramPage = this._device.ProgramPageStruct;
            //Страница программа
            this._currentProgramPage.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ProgramPageSaveOk);
            this._currentProgramPage.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramSaveFail);
            //Программа
            this._currentSourceProgramStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStructWriteOk);
            this._currentSourceProgramStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramSaveFail);
            this._currentSourceProgramStruct.WriteOk += HandlerHelper.CreateHandler(this, this.ProgressbarPerformStep);
            this._currentSourceProgramStruct.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._currentSourceProgramStruct.RemoveStructQueries();
                this.ProgramSaveFail();
            });
            //Архив программы
            this._fileDriver = new FileDriver(this._device, this);
            //Старт программы
            this._startProgramStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStartOk);
            this._startProgramStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStartSaveFail);

            this._device.StopSpl.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
             MessageBox.Show("Выполнение свободно программируемой логики остановлено", "Останов СПЛ", MessageBoxButtons.OK, MessageBoxIcon.Information));
            this._device.StopSpl.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Невозможно выполнить команду", "", MessageBoxButtons.OK, MessageBoxIcon.Error));
            this._device.StartSpl.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._device.StateSpl.LoadStruct);
            this._device.StartSpl.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Невозможно выполнить команду", "", MessageBoxButtons.OK, MessageBoxIcon.Error));
            this._device.StateSpl.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {

                bool res = !Common.GetBit(this._device.StateSpl.Value.Values[0], 3)
                && (Common.GetBit(this._device.StateSpl.Value.Values[3], 8)
                || Common.GetBit(this._device.StateSpl.Value.Values[3], 9)
                || Common.GetBit(this._device.StateSpl.Value.Values[3], 10)
                || Common.GetBit(this._device.StateSpl.Value.Values[3], 11)
                || Common.GetBit(this._device.StateSpl.Value.Values[3], 12));
                
               
                if (res)
                {
                    MessageBox.Show("Состояние логики: ОШИБКА", "Запуск логики", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Состояние логики: ЗАПУЩЕНА", "Запуск логики", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
            });
            this._device.StateSpl.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Невозможно прочитать состояние логики", "Запуск логики", MessageBoxButtons.OK, MessageBoxIcon.Error));
            
            //Загрузка сигналов
            this._currentSignalsStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ProgramSignalsLoadOk);
            this._currentSignalsStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramSignalsLoadFail);

            this._docName = "Документ";
            this._projName = "Проект";
        }


        private void InitForm()
        {
            this._style = VisualStyle.IDE;
            this._manager = new DockingManager(this, this._style);
            this._newForm = new NewSchematicForm();
            this._compiller = new NewLogicCompiler(_device.DeviceType);
            this._formCheck = new MessageBoxForm();
        }
        #endregion

        #region Form Event Handlers

        private void BSBGLEF_FormClosing(object sender, FormClosingEventArgs e)
        {
            PropFormsStrings.JournalDictionary.Remove(this._device);
            PropFormsStrings.AlarmDictionary.Remove(this._device);
            if (this._filler.TabPages.Count != 0)
            {
                if (MessageBox.Show("Сохранить текущий проект на диске ?", "Закрытие проекта", MessageBoxButtons.YesNo)
                    == DialogResult.Yes)
                {
                    this.SaveProjectDoc();
                }
            }
            this._currentSignalsStruct.RemoveStructQueries();
            this._fileDriver.CloseAll(this.OnClosedFiles);
        }

        private void ProgressbarPerformStep()
        {
            this._formCheck.ProgramExchangeOk();
        }

        private void OnClosedFiles(bool res, string message)
        {
            if (!res) MessageBox.Show(@"Не удалось закрыть открытые файлы в устройстве. " + message);
        }

        private void BSBGLEF_Load(object sender, EventArgs e)
        {
            this._filler = new Crownwood.Magic.Controls.TabControl
            {
                Appearance = Crownwood.Magic.Controls.TabControl.VisualAppearance.MultiDocument,
                Dock = DockStyle.Fill,
                Style = this._style,
                IDEPixelBorder = true
            };
            Controls.Add(this._filler);
            this._filler.ClosePressed += new EventHandler(this.OnFileClose);
            // Reduce the amount of flicker that occurs when windows are redocked within
            // the container. As this prevents unsightly backcolors being drawn in the
            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            // Create the object that manages the docking state
            this._manager = new DockingManager(this, this._style);
            // Ensure that the RichTextBox is always the innermost control
            this._manager.InnerControl = this._filler;
            // Create and setup the StatusBar object
            this._statusBar = new StatusBar();
            this._statusBar.Dock = DockStyle.Bottom;
            this._statusBar.ShowPanels = true;
            // Create and setup a single panel for the StatusBar
            StatusBarPanel statusBarPanel = new StatusBarPanel();
            statusBarPanel.AutoSize = StatusBarPanelAutoSize.Spring;
            this._statusBar.Panels.Add(statusBarPanel);
            Controls.Add(this._statusBar);
            this._toolStrip = this.CreateToolStrip();
            Controls.Add(this._toolStrip);
            this._mainMenu = this.CreateMenus();
            Controls.Add(this._mainMenu);
            this._manager.OuterControl = this._statusBar;
            this.CreateOutputWindow();
            this.CreateLibraryWindow();
            Width = 800;
            Height = 600;
            PropFormsStrings.JournalDictionary.Add(this._device, PropFormsStrings.GetNewJournalList());
            PropFormsStrings.AlarmDictionary.Add(this._device, PropFormsStrings.GetNewAlarmList());
        }
        
        protected void DefineContentState(Content c)
        {
            c.CaptionBar = true;
            c.CloseButton = true;
        }
        #endregion

        #region DeviceEventHandlers
        
        private void ProgramPageSaveOk()
        {
            try
            {
                if (this._pageIndex < 4)
                {
                    SourceProgramStruct ps = new SourceProgramStruct();
                    ushort[] _program = new ushort[1024];
                    Array.ConstrainedCopy(this._binFile, this._pageIndex * 1024, _program, 0, 1024);
                    ps.InitStruct(Common.TOBYTES(_program, false));
                    this._currentSourceProgramStruct.Value = ps;
                    this._currentSourceProgramStruct.SaveStruct();
                }
            }
            catch
            { }
        }

        private void ProgramSaveFail()
        {
            this.OutMessage(InformationMessages.ERROR_LOADING_PROGRAM_IN_DEVICE);
            this._formCheck.Fail = true;
            this._formCheck.ShowResultMessage(InformationMessages.ERROR_LOADING_PROGRAM_IN_DEVICE);
            this.OnStop();
        }

        private void ProgramStructWriteOk()
        {
            this._pageIndex++;
            if (this._pageIndex < 4)
            {
                ushort[] values = new ushort[1];
                values[0] = this._pageIndex;
                OneWordStruct pp = new OneWordStruct();
                pp.InitStruct(Common.TOBYTES(values, false));
                this._currentProgramPage.Value = pp;
                this._currentProgramPage.SaveStruct();
            }
            else
            {
                this._formCheck.ShowMessage(InformationMessages.PROGRAM_SAVE_OK);
                var values = new ushort[1];
                values[0] = 0x00FF;
                StartStruct ss = new StartStruct();
                ss.InitStruct(Common.TOBYTES(values, false));
                this._startProgramStruct.Value = ss;
                this._startProgramStruct.SaveStruct5();
            }
        }

        private void ProgramStartOk()
        {
            this._formCheck.SetProgressBarStyle(ProgressBarStyle.Marquee);
            this._formCheck.ShowMessage(InformationMessages.LOADING_ARCHIVE_IN_DEVICE);
            byte[] wBytes = Common.TOBYTES(this._compressedZip, false);
            this._fileDriver.WriteFile(this.OnFileWriten, wBytes, ARH_NANE);
        }

        private void ProgramStartSaveFail()
        {
            this.OutMessage(InformationMessages.ERROR_PROGRAM_START);
            this._formCheck.Fail = true;
            this._formCheck.ShowResultMessage(InformationMessages.ERROR_PROGRAM_START);
            this.OnStop();
        }

        private void StartLoadCurrentSignals()
        {
            LogicProgramSignals ps = new LogicProgramSignals(this._compiller.GetRamRequired());
            this._currentSignalsStruct.Value = ps;
            this._currentSignalsStruct.Values = ps.Values;
            this._currentSignalsStruct.Slots = HelperFunctions.SetSlots(ps.Values, 0x4100);
            this._currentSignalsStruct.LoadStructCycle();
            this._formCheck.ShowResultMessage(InformationMessages.PROGRAM_SAVE_OK_EMULATOR_RUNNING);
        }

        void ProgramSignalsLoadFail()
        {
            //Останавливаем отрисовку
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
            if (this._isOnSimulateMode)
            {
                this.OutMessage(InformationMessages.ERROR_VARIABLES_UPDATED);
            }
        }
        private void ProgramSignalsLoadOk()
        {
            if (this._isOnSimulateMode)
            {
                try
                {
                    this._compiller.UpdateVol(this._currentSignalsStruct.Values);
                    foreach (BSBGLTab src in this._filler.TabPages)
                    {
                        src.Schematic.RedrawBack();
                        src.Schematic.RedrawSchematic();
                    }
                    this.OutMessage(InformationMessages.VARIABLES_UPDATED);
                }
                catch
                {
                    MessageBox.Show("В процессе работы в режиме отладки логики возникла ошибка");
                }
            }
        }

        #endregion

        private void OutMessage(string str)
        {
            this._outputWindow.AddMessage(str + "\r\n");
        }

        #region Docking Forms Code
        private void CreateOutputWindow()
        {
            this._outputWindow = new OutputWindow();
            this._outputContent = this._manager.Contents.Add(this._outputWindow, "Сообщения");
            this.DefineContentState(this._outputContent);
            this._manager.AddContentWithState(this._outputContent, State.DockBottom);
        }

        private void CreateLibraryWindow()
        {
            this._libraryWindow = new LibraryBox(Resources.BlockLib);
            this._libraryContent = this._manager.Contents.Add(this._libraryWindow, "Библиотека");
            this.DefineContentState(this._libraryContent);
            this._manager.AddContentWithState(this._libraryContent, State.DockRight);
        }

        #endregion
        
        #region Toolbox

        protected ToolStrip CreateToolStrip()
        {
            List<ToolStripSeparator> toolStripSepList = new List<ToolStripSeparator>();
            for (int i = 0; i < 9; i++)
            {
                ToolStripSeparator tls = new ToolStripSeparator
                {
                    Name = "toolStripSeparator" + i,
                    Size = new Size(6, 25)
                };
                if (i >= 6) tls.Margin = new Padding(20, 0, 0, 0);
                toolStripSepList.Add(tls);
            }
            ToolStrip mainToolStrip = new ToolStrip();
            ToolStripButton newToolStripButton = new ToolStripButton();
            ToolStripButton openToolStripButton = new ToolStripButton();
            ToolStripButton openProjToolStripButton = new ToolStripButton();
            ToolStripButton saveToolStripButton = new ToolStripButton();
            ToolStripButton saveProjToolStripButton = new ToolStripButton();
            ToolStripButton printToolStripButton = new ToolStripButton();
            ToolStripButton undoToolStripButton = new ToolStripButton();
            ToolStripButton redoToolStripButton = new ToolStripButton();
            ToolStripButton cutToolStripButton = new ToolStripButton();
            ToolStripButton copyToolStripButton = new ToolStripButton();
            ToolStripButton pasteToolStripButton = new ToolStripButton();
            ToolStripButton compilToolStripButton = new ToolStripButton();
            ToolStripButton stopToolStripButton = new ToolStripButton();
            ToolStripButton startToolStripButton = new ToolStripButton
            {
                DisplayStyle = ToolStripItemDisplayStyle.Image,
                Image = Resources.startemul,
                ImageTransparentColor = Color.Magenta,
                Size = new Size(23, 22),
                Text = "Запустить эмуляцию СПЛ",
            };
            startToolStripButton.Click += this.StartSplEmulBtnClick;

            ToolStripButton getArchiveBtn = new ToolStripButton
            {
                DisplayStyle = ToolStripItemDisplayStyle.Image,
                Image = Resources.openfromdevice,
                ImageTransparentColor = Color.Magenta,
                Size = new Size(23, 22),
                Text = "Загрузить архив логической программы из устройства",
            };
            getArchiveBtn.Click += this.OpenFromDeviceBtnClick;

            ToolStripButton startLogicBtn = new ToolStripButton
            {
                DisplayStyle = ToolStripItemDisplayStyle.Image,
                Image = Resources.go,
                ImageTransparentColor = Color.Magenta,
                Size = new Size(23, 22),
                Text = "Запустить СПЛ в устройстве",
            };
            startLogicBtn.Click += this.StartLogicProgram;

            ToolStripButton stopLogicBtn = new ToolStripButton()
            {
                DisplayStyle = ToolStripItemDisplayStyle.Image,
                Image = Properties.Resources.stop1,
                ImageTransparentColor = Color.Magenta,
                Size = new Size(23, 22),
                Text = "Остановить выполнение СПЛ в устройстве",
            };
            stopLogicBtn.Click += this.StopLogicProgram;

            ToolStripButton compilOfflineToolStripButton = new ToolStripButton();

            mainToolStrip.Items.Add(newToolStripButton);
            mainToolStrip.Items.Add(toolStripSepList[0]);
            mainToolStrip.Items.Add(openToolStripButton);
            mainToolStrip.Items.Add(openProjToolStripButton);
            mainToolStrip.Items.Add(toolStripSepList[1]);
            mainToolStrip.Items.Add(saveToolStripButton);
            mainToolStrip.Items.Add(saveProjToolStripButton);
            mainToolStrip.Items.Add(toolStripSepList[2]);
            mainToolStrip.Items.Add(printToolStripButton);
            mainToolStrip.Items.Add(toolStripSepList[3]);
            mainToolStrip.Items.Add(undoToolStripButton);
            mainToolStrip.Items.Add(redoToolStripButton);
            mainToolStrip.Items.Add(toolStripSepList[4]);
            mainToolStrip.Items.Add(cutToolStripButton);
            mainToolStrip.Items.Add(copyToolStripButton);
            mainToolStrip.Items.Add(pasteToolStripButton);
            mainToolStrip.Items.Add(toolStripSepList[5]);
            mainToolStrip.Items.Add(compilToolStripButton);
            mainToolStrip.Items.Add(getArchiveBtn);

            //mainToolStrip.Items.Add(setListBtn);

            mainToolStrip.Items.Add(toolStripSepList[6]);
            mainToolStrip.Items.Add(startToolStripButton);
            mainToolStrip.Items.Add(stopToolStripButton);
            mainToolStrip.Items.Add(toolStripSepList[7]);
            mainToolStrip.Items.Add(startLogicBtn);
            mainToolStrip.Items.Add(stopLogicBtn);
            mainToolStrip.Items.Add(toolStripSepList[8]);
            mainToolStrip.Items.Add(compilOfflineToolStripButton);

            mainToolStrip.Location = new Point(0, 0);
            mainToolStrip.Name = "MainToolStrip";
            mainToolStrip.Size = new Size(816, 25);
            mainToolStrip.TabIndex = 0;
            mainToolStrip.Text = "toolStrip1";

            newToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            newToolStripButton.Image = Resources.filenew;
            newToolStripButton.ImageTransparentColor = Color.Magenta;
            newToolStripButton.Name = "newToolStripButton";
            newToolStripButton.Size = new Size(23, 22);
            newToolStripButton.Text = "Новая схема";
            newToolStripButton.Click += new EventHandler(this.OnFileNew);

            openToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            openToolStripButton.Image = Resources.fileopen;
            openToolStripButton.ImageTransparentColor = Color.Magenta;
            openToolStripButton.Name = "openToolStripButton";
            openToolStripButton.Size = new Size(23, 22);
            openToolStripButton.Text = "Открыть документ";
            openToolStripButton.Click += new EventHandler(this.OnFileOpen);

            openProjToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            openProjToolStripButton.Image = Resources.fileopenproject;
            openProjToolStripButton.ImageTransparentColor = Color.Magenta;
            openProjToolStripButton.Name = "openProjToolStripButton";
            openProjToolStripButton.Size = new Size(23, 22);
            openProjToolStripButton.Text = "Открыть проект";
            openProjToolStripButton.Click += new EventHandler(this.OnFileOpenProject);
            
            saveToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            saveToolStripButton.Image = Resources.filesave;
            saveToolStripButton.ImageTransparentColor = Color.Magenta;
            saveToolStripButton.Name = "saveToolStripButton";
            saveToolStripButton.Size = new Size(23, 22);
            saveToolStripButton.Text = "Сохранить документ";
            saveToolStripButton.Click += new EventHandler(this.OnFileSave);

            saveProjToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            saveProjToolStripButton.Image = Resources.filesaveproject;
            saveProjToolStripButton.ImageTransparentColor = Color.Magenta;
            saveProjToolStripButton.Name = "saveProjToolStripButton";
            saveProjToolStripButton.Size = new Size(23, 22);
            saveProjToolStripButton.Text = "Сохранить проект";
            saveProjToolStripButton.Click += new EventHandler(this.OnFileSaveProject);

            printToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            printToolStripButton.Image = Resources.print;
            printToolStripButton.ImageTransparentColor = Color.Magenta;
            printToolStripButton.Name = "printToolStripButton";
            printToolStripButton.Size = new Size(23, 22);
            printToolStripButton.Text = "Печать";
            printToolStripButton.Click += new EventHandler(this.printToolStripButton_Click);

            undoToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            undoToolStripButton.Image = Resources.undo;
            undoToolStripButton.ImageTransparentColor = Color.Magenta;
            undoToolStripButton.Name = "undoToolStripButton";
            undoToolStripButton.Size = new Size(23, 22);
            undoToolStripButton.Text = "Отмена";
            undoToolStripButton.Click += new EventHandler(this.OnUndo);

            redoToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            redoToolStripButton.Image = Resources.redo;
            redoToolStripButton.ImageTransparentColor = Color.Magenta;
            redoToolStripButton.Name = "redoToolStripButton";
            redoToolStripButton.Size = new Size(23, 22);
            redoToolStripButton.Text = "Восстановить отмененое";
            redoToolStripButton.Click += new EventHandler(this.OnRedo);

            cutToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            cutToolStripButton.Image = Resources.cut;
            cutToolStripButton.ImageTransparentColor = Color.Magenta;
            cutToolStripButton.Name = "cutToolStripButton";
            cutToolStripButton.Size = new Size(23, 22);
            cutToolStripButton.Text = "Вырезать";
            cutToolStripButton.Click += new EventHandler(this.OnEditCut);

            copyToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            copyToolStripButton.Image = Resources.copy;
            copyToolStripButton.ImageTransparentColor = Color.Magenta;
            copyToolStripButton.Name = "copyToolStripButton";
            copyToolStripButton.Size = new Size(23, 22);
            copyToolStripButton.Text = "Копировать";
            copyToolStripButton.Click += new EventHandler(this.OnEditCopy);

            pasteToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            pasteToolStripButton.Image = Resources.paste;
            pasteToolStripButton.ImageTransparentColor = Color.Magenta;
            pasteToolStripButton.Name = "pasteToolStripButton";
            pasteToolStripButton.Size = new Size(23, 22);
            pasteToolStripButton.Text = "Вставить";
            pasteToolStripButton.Click += new EventHandler(this.OnEditPaste);
            
            compilToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            compilToolStripButton.Image = Resources.compileanddownload;
            compilToolStripButton.ImageTransparentColor = Color.Magenta;
            compilToolStripButton.Name = "CompilToolStripButton";
            compilToolStripButton.Size = new Size(23, 22);
            compilToolStripButton.Text = "Загрузить логическую программу и архив в устройство. Запустить эмулятор";
            compilToolStripButton.Click += new EventHandler(this.StartWriteToDeviceMainArchive);
            
            stopToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            stopToolStripButton.Image = Resources.stop;
            stopToolStripButton.ImageTransparentColor = Color.Magenta;
            stopToolStripButton.Name = "CompilToDeviceToolStripButton";
            stopToolStripButton.Size = new Size(23, 22);
            stopToolStripButton.Text = "Остановка эмуляции";
            stopToolStripButton.Click += new EventHandler(this.OnStop);

            compilOfflineToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            compilOfflineToolStripButton.Image = Resources.calculator.ToBitmap();
            compilOfflineToolStripButton.ImageTransparentColor = Color.Magenta;
            compilOfflineToolStripButton.Name = "CompilOfflineToolStripButton";
            compilOfflineToolStripButton.Size = new Size(23, 22);
            compilOfflineToolStripButton.Text = "Проверка заполнения схемы";
            compilOfflineToolStripButton.Click += new EventHandler(this.OnCompileOffline);

            return mainToolStrip;
        }
        #endregion

        #region MAIN MENU
        protected MenuControl CreateMenus()
        {
            MenuControl topMenu = new MenuControl();
            topMenu.Style = this._style;
            topMenu.MultiLine = false;

            MenuCommand topFile = new MenuCommand("&Файл");
            MenuCommand topEdit = new MenuCommand("Правка");
            MenuCommand topView = new MenuCommand("Вид");

            topMenu.MenuCommands.AddRange(new MenuCommand[] { topFile, topEdit, topView/*, topSettings,*/ });

            //File
            MenuCommand topFileNew = new MenuCommand("Новый", new EventHandler(this.OnFileNew));
            MenuCommand topFileOpen = new MenuCommand("Открыть документ", new EventHandler(this.OnFileOpen));
            MenuCommand topFileOpenProject = new MenuCommand("Открыть проект", new EventHandler(this.OnFileOpenProject));
            MenuCommand topFileClose = new MenuCommand("Закрыть документ", new EventHandler(this.OnFileClose));
            MenuCommand topFileCloseProject = new MenuCommand("Закрыть проект", new EventHandler(this.OnFileCloseProject));
            MenuCommand topFileSave = new MenuCommand("Сохранить документ", new EventHandler(this.OnFileSave));
            MenuCommand topFileSaveProject = new MenuCommand("Сохранить проект", new EventHandler(this.OnFileSaveProject));
            topFile.MenuCommands.AddRange(new MenuCommand[] { topFileNew, topFileOpen, topFileOpenProject,
                                                              topFileClose,topFileCloseProject,
                                                              topFileSave, topFileSaveProject });
            //Edit
            MenuCommand topEditCopy = new MenuCommand("Копировать", new EventHandler(this.OnEditCopy));
            MenuCommand topEditPaste = new MenuCommand("Вставить", new EventHandler(this.OnEditPaste));

            topEdit.MenuCommands.AddRange(new MenuCommand[] { topEditCopy, topEditPaste });
            //View
            MenuCommand topViewToolWindow = new MenuCommand("Библиотека", new EventHandler(this.OnViewToolWindow));
            topView.MenuCommands.AddRange(new MenuCommand[] {topViewToolWindow});
            topMenu.Dock = DockStyle.Top;
            return topMenu;
        }

        protected void OnFileNew(object sender, EventArgs e)
        {
            this._newForm.ShowDialog();
            if (DialogResult.OK != this._newForm.DialogResult) return;
            BSBGLTab myTab = new BSBGLTab();
            myTab.InitializeBSBGLSheet(this._newForm.NameOfSchema, this._newForm.sheetFormat, this._device, this._manager);
            myTab.Selected = true;
            this._filler.TabPages.Add(myTab);
            this.OutMessage("Создан новый лист схемы: " + this._newForm.NameOfSchema);
            myTab.Schematic.Focus();
        }


        private void StartLogicProgram(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Запустить свободно программируемую логику в устройстве?", "Запуск СПЛ",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.StartSpl.Value.Word = 0x00FF;
            this._device.StartSpl.SaveStruct5();
        }

        private void StopLogicProgram(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Остановить свободно программируемую логику в устройстве? ВНИМАНИЕ! Это может привести к выводу из работы важных функций устройства", "Останов СПЛ",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.StopSpl.Value.Word = 0x00FF;
            this._device.StopSpl.SaveStruct5();
        }

     
        void printToolStripButton_Click(object sender, EventArgs e)
        {
            // Initialize the dialog's PrinterSettings property to hold user
            // defined printer settings.
            this.pageSetupDialog.PageSettings =
                new System.Drawing.Printing.PageSettings();
            // Initialize dialog's PrinterSettings property to hold user
            // set printer settings.
            this.pageSetupDialog.PrinterSettings =
                new System.Drawing.Printing.PrinterSettings();
            //Do not show the network in the printer dialog.
            this.pageSetupDialog.ShowNetwork = false;
            //Show the dialog storing the result.
            DialogResult result = this.pageSetupDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.printDocument.DefaultPageSettings = this.pageSetupDialog.PageSettings;
                this.printPreviewDialog.Document = this.printDocument;
                // Call the ShowDialog method. This will trigger the document's
                // PrintPage event.
                this.printPreviewDialog.ShowDialog();
                DialogResult result1 = this.printDialog.ShowDialog();
                if (result1 == DialogResult.OK)
                {
                    this.printDocument.Print();
                }
            }
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                float scale = (float)e.PageBounds.Width /
                              (float)_sTab.Schematic.SizeX;
                _sTab.Schematic.DrawIntoGraphic(e.Graphics, scale);
            }
        }

        protected void OnUndo(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.Undo();
            }
        }
        protected void OnRedo(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.Redo();
            }
        }

        protected void OnEditCut(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.CopyFromXML();
                _sTab.Schematic.DeleteEvent();
            }
        }

        protected void OnEditCopy(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.CopyFromXML();
            }
        }

        protected void OnEditPaste(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.PasteFromXML();
            }
        }

        protected void OnFileOpen(object sender, EventArgs e)
        {
            this.openFileDialog.Filter = "dcm файлы(*.dcm)|*.dcm";
            this.openFileDialog.FileName = "Документ";
            if (this.openFileDialog.ShowDialog() != DialogResult.OK) return;
            BSBGLTab tab = new BSBGLTab {Selected = true};
            tab.InitializeBSBGLSheet(this.openFileDialog.FileName, SheetFormat.A0_L, this._device, this._manager);
            using (XmlTextReader reader = new XmlTextReader(this.openFileDialog.FileName))
            {
                reader.WhitespaceHandling = WhitespaceHandling.None;
                reader.Read();
                reader.Read(); //вход в BSBGL_DocFile
                if (tab.Schematic.ReadXml(reader, _device.DeviceType))
                {
                    tab.UpdateTitle();
                }
                else
                {
                    MessageBox.Show("Данный документ не является документом логической программы МР761", "Внимание",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            List<BSBGLTab> tabs = new List<BSBGLTab>(this._filler.TabPages.Cast<BSBGLTab>());
            tabs.Add(tab);
            if (this.CheckOutputs(tabs))
            {
                tab.UpdateLists();
                this._filler.TabPages.Add(tab);
            }
        }

        private bool CheckOutputs(List<BSBGLTab> tabs)
        {
            List<BlockDesc> descList = new List<BlockDesc>();
            foreach (BSBGLTab tab in tabs)
            {
                descList.AddRange(tab.Schematic.BlockManager.GetBlockList()
                    .Select(b => b.GetDescription())
                    .Where(bd => bd.userData.ContainsKey("inOutType")));
            }
            List<BlockDesc[]> dublicateBlocks = new List<BlockDesc[]>();
            int count = descList.Count;
            for (int i = 0; i < count; i++)
            {
                for (int j = 0; j < count; j++)
                {
                    if (descList[i] == descList[j]) continue;
                    if (descList[i].userData["inOutType"] == descList[j].userData["inOutType"]
                        && descList[i].TypeElem == descList[j].TypeElem &&
                        (descList[i].TypeElem == "out" || descList[i].TypeElem == "journal" || descList[i].TypeElem == "journalA"))
                    {
                        dublicateBlocks.Add(new[] { descList[i], descList[j] });
                        descList.Remove(descList[j]);
                        count--;
                        j--;
                    }
                }
            }

            if (dublicateBlocks.Count != 0)
            {
                string s = string.Empty;
                for (int i = 0; i < dublicateBlocks.Count; i++)
                {
                    s += i == dublicateBlocks.Count - 1
                        ? string.Format("{0} и {1}", dublicateBlocks[i][0].name, dublicateBlocks[i][1].name)
                        : string.Format("{0} и {1}, ", dublicateBlocks[i][0].name, dublicateBlocks[i][1].name);
                }
                MessageBox.Show(string.Format(
                        "Следующие выходные элементы имеют одинаковые значения ССЛ: {0}. Это может привести к некорректной работе логической программы! Измените значение ССЛ на не занятое другим элементом.",
                        s), "ВНИМАНИЕ!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return true;
        }

        protected void OnFileOpenProject(object sender, EventArgs e)
        {
            this.openFileDialog.Filter = "prj файлы(*.prj)|*.prj";
            this.openFileDialog.FileName = "Проект";
            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (this._filler.SelectedTab != null)
                {
                    switch (MessageBox.Show("Сохранить текущий проект на диске ?"
                                                    , "Закрытие проекта", MessageBoxButtons.YesNoCancel))
                    {
                        case DialogResult.Yes:
                            if (this.SaveProjectDoc())
                            {
                                this._filler.TabPages.Clear();
                            }
                            else
                            {
                                return;
                            }
                            break;
                        case DialogResult.No:
                            this._filler.TabPages.Clear();
                            break;
                        case DialogResult.Cancel:
                            return;
                    }
                }
                using (XmlTextReader reader = new XmlTextReader(this.openFileDialog.FileName))
                {
                    List<BSBGLTab> tabs = new List<BSBGLTab>();
                    reader.WhitespaceHandling = WhitespaceHandling.None;
                    while (reader.Read())
                    {
                        if (reader.Name == "Source")
                        {
                            BSBGLTab tab = new BSBGLTab();
                            tab.InitializeBSBGLSheet(reader.GetAttribute("name"), SheetFormat.A0_L, this._device, this._manager);
                            tab.Selected = true;
                            if (tab.Schematic.ReadXml(reader, _device.DeviceType))
                            {
                                tab.UpdateTitle();
                                tabs.Add(tab);
                                tab.UpdateLists();
                            }
                            else
                            {
                                MessageBox.Show("Данный проект не является проектом логической программы МР761",
                                    "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                        }
                    }

                    this.OutMessage("Загружен проект.");

                    this._filler.TabPages.AddRange(tabs.ToArray());
                }
            }
        }

        protected void OnFileClose(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab == null) return;
            switch (MessageBox.Show("Сохранить документ на диске ?", "Закрытие документа", MessageBoxButtons.YesNoCancel))
            {
                case DialogResult.Yes:
                    if (this.SaveDocument())
                    {
                        BSBGLTab tab = (BSBGLTab)this._filler.SelectedTab;
                        this._filler.TabPages.Remove(tab);
                        tab.Dispose();
                    }
                    break;
                case DialogResult.No:
                {
                    BSBGLTab tab = (BSBGLTab)this._filler.SelectedTab;
                    this._filler.TabPages.Remove(tab);
                    tab.Dispose();
                }
                    break;
                case DialogResult.Cancel:
                    break;
            }
        }

        private void OnFileCloseProject(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab == null) return;
            this.FileCloseProject();
        }

        private DialogResult FileCloseProject()
        {
            switch (MessageBox.Show("Сохранить проект на диске ?", "Закрытие проекта", MessageBoxButtons.YesNoCancel))
            {
                case DialogResult.Yes:
                    this.SaveProjectDoc();                    
                    foreach (BSBGLTab tabPage in this._filler.TabPages)
                    {
                        tabPage.Dispose();
                    }
                    this._filler.TabPages.Clear();                    
                    return DialogResult.Yes;
                case DialogResult.No:
                    foreach (BSBGLTab tabPage in this._filler.TabPages)
                    {
                        tabPage.Dispose();
                    }
                    this._filler.TabPages.Clear();
                    return DialogResult.No;
                case DialogResult.Cancel:
                    return DialogResult.Cancel;
                default:
                    return DialogResult.Cancel;
            }
        }

        protected void OnFileSave(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab == null)
            {
                MessageBox.Show("Проект пуст", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            this.SaveDocument();
        }

        protected void OnFileSaveProject(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab == null)
            {
                MessageBox.Show("Проект пуст", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            this.SaveProjectDoc();
        }

        protected void OnViewToolWindow(object sender, EventArgs e)
        {
            this._manager.ShowContent(this._libraryContent);
        }

        private void OnCompileOffline(object sender, EventArgs e)
        {
            this.CompileProjectOffline();
        }
        static double myRound(double x, int precision)
        {
            return ((int)(x * Math.Pow(10.0, precision)) / Math.Pow(10.0, precision));
        }

        private void CompileProjectOffline()
        {
            if (this._filler.TabPages.Count == 0) return;
            this._compiller.ResetCompiller();
            try
            {
                foreach (BSBGLTab src in this._filler.TabPages)
                {
                    this._compiller.AddSource(src.TabName, src.Schematic);
                    this.OutMessage("Компиляция схемы: " + src.TabName);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Ошибка компиляции", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.OnStop();
                return;
            }

            this._binFile = this._compiller.Make(_device.DeviceType, Common.VersionConverter(this._device.DeviceVersion));

            double percent = (float)this._compiller.Binarysize * 100 / 2047;
            string outMessage = "Пpоцент заполнения схемы: " + myRound(percent, 1) + "%.";
            this.OutMessage("Пpоцент заполнения схемы: " + myRound(percent, 1) + "%.");

            if (/*this._compiller.BinarySize > 2047*/ percent > 95)
            {
                MessageBox.Show(outMessage + "\nПревышен критический объем логики (95%)!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.OnStop();
                return;
            }

            if (this._binFile.Length == 0)
            {
                MessageBox.Show("Бинарный файл логической программы нулевой !", "Ошибка компиляции", MessageBoxButtons.OK);
            }

            MessageBox.Show(outMessage, "Результат компиляции схемы", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        protected bool CompileProject()
        {
            if (this._filler.TabPages.Count == 0) return false;
            this._compiller.ResetCompiller();
            try
            {
                foreach (BSBGLTab src in this._filler.TabPages)
                {
                    this._compiller.AddSource(src.TabName, src.Schematic);
                    this.OutMessage("Компиляция схемы: " + src.TabName);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Ошибка компиляции", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                src.Schematic.StartDebugMode();
            }
            
            this._binFile = this._compiller.Make(_device.DeviceType, Common.VersionConverter(this._device.DeviceVersion));
                   
            if (!this._isCompileScheme)
            {
                double percent = (float)this._compiller.Binarysize * 100 / 2047;
                string outMessage = "Пpоцент заполнения схемы: " + myRound(percent, 1) + "%.";
                this.OutMessage("Пpоцент заполнения схемы: " + myRound(percent, 1) + "%.");

                if (percent > 95)
                {
                    DialogResult dialogResult = MessageBox.Show(outMessage + "\nПревышен критический объем логики (95%)!\n Хотите записать логику в устройство?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        return true;
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        this.OnStop();
                        return false;
                    }
                    return false;
                }
                if (this._binFile.Length == 0)
                {
                    MessageBox.Show("Бинарный файл логической программы нулевой !", "Ошибка компиляции", MessageBoxButtons.OK);
                    return false;
                }

                if (MessageBox.Show(outMessage + "\nХотите записать логику в устройство?", "Загрузка логической программы в устройство", MessageBoxButtons.OKCancel,
                        MessageBoxIcon.Question) != DialogResult.OK) return false;
            }
            return true;
        }

        private void SaveCompiledProject()
        {
            foreach (BSBGLTab tabPage in this._filler.TabPages)
            {
                tabPage.Schematic.StartDebugMode();
            }

            this._pageIndex = 0;
            ushort[] values = new ushort[1];
            values[0] = this._pageIndex;
            OneWordStruct pp = new OneWordStruct();
            pp.InitStruct(Common.TOBYTES(values, false));
            this._currentProgramPage.Value = pp;
            this._currentProgramPage.SaveStruct();
            this._isOnSimulateMode = true;
            this._formCheck.SetProgressBarStyle(ProgressBarStyle.Blocks);
            this._formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGRAM);
            this._formCheck.ShowMessage(InformationMessages.LOADING_PROGRAM_IN_DEVICE);
        }

        private void OnStop(object sender, EventArgs e)
        {
            this.OnStop();
        }
        private void OnStop()
        {
            if (this._device.MB.BaudeRate <= RST_BAUDE_RATE && !this._device.MB.NetworkEnabled)
            {
                MessageBox.Show("Запись и чтение логики не поддерживается по интерфейсу RS-485", "Внимание",
                    MessageBoxButtons.OK);
                return;
            }
            this._isOnSimulateMode = false;
            this._currentSignalsStruct.RemoveStructQueries();
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
        }

        private bool SaveDocument()
        {
            this.saveFileDialog.Filter = "dcm файлы(*.dcm)|*.dcm";
            this.saveFileDialog.FileName = this._docName;
            DialogResult saveFileRzult = this.saveFileDialog.ShowDialog();
            this._docName = this.saveFileDialog.FileName;
            if (saveFileRzult == DialogResult.OK)
            {
                MemoryStream memstream = new MemoryStream();
                XmlTextWriter writer = new XmlTextWriter(memstream, Encoding.UTF8);
                writer.Formatting = Formatting.Indented;
                writer.WriteStartDocument();
                writer.WriteStartElement("BSBGL_DocFile");
                if (this._filler.SelectedTab is BSBGLTab)
                {
                    BSBGLTab sTab = (BSBGLTab) this._filler.SelectedTab;
                    sTab.Schematic.WriteXml(writer, _device.DeviceType);
                }
                
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();

                byte[] uncompressed = memstream.ToArray();
                memstream.Close();
                FileStream fs = new FileStream(this.saveFileDialog.FileName, FileMode.Create, FileAccess.Write,
                    FileShare.None, uncompressed.Length, false);
                fs.Write(uncompressed, 0, uncompressed.Length);
                fs.Close();
                return true;
            }
            return false;
        }

        private bool SaveProjectDoc()
        {
            this.saveFileDialog.Filter = "prj файлы(*.prj)|*.prj";
            this.saveFileDialog.FileName = this._projName;
            DialogResult saveFileRzult = this.saveFileDialog.ShowDialog();
            this._projName = this.saveFileDialog.FileName;
            if (saveFileRzult != DialogResult.OK) return false;
            MemoryStream memstream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(memstream, Encoding.UTF8);
            writer.Formatting = Formatting.Indented;
            writer.WriteStartDocument();
            writer.WriteStartElement("BSBGL_ProjectFile");
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("name", src.TabName);
                src.Schematic.WriteXml(writer, _device.DeviceType);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();
            byte[] uncompressed = memstream.ToArray();
            memstream.Close();
            FileStream fs = new FileStream(this.saveFileDialog.FileName, FileMode.Create, FileAccess.Write,
                FileShare.None, uncompressed.Length, false);
            fs.Write(uncompressed, 0, uncompressed.Length);
            fs.Close();

            return true;
        }

        #endregion
        
        #region Archive Logic
        private const string ARH_NANE = "logarch.zip";
        private const string FILE_NOT_FOUND = "Файл не найден";
        private const string JOURNAL_LIST_FILE_NAME = "jlist.xml";
        
        private void SetLists(byte[] readBytes)
        {
            using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes), Encoding.GetEncoding("windows-1251")))
            {
                using (XmlTextReader reader = new XmlTextReader(streamReader))
                {
                    switch (_device.DeviceType)
                    {
                        case "MR771":
                            try
                            {
                                ListsOfJournals lists = new ListsOfJournals("MR771");
                                lists.XmlFromFile(reader);
                                PropFormsStrings.JournalDictionary[this._device].Clear();
                                PropFormsStrings.JournalDictionary[this._device].AddRange(lists.SysJournal.MessagesList);
                                PropFormsStrings.AlarmDictionary[this._device].Clear();
                                PropFormsStrings.AlarmDictionary[this._device].AddRange(lists.AlarmJournal.MessagesList);
                            }
                            catch (Exception)
                            {
                                PropFormsStrings.JournalDictionary[this._device].Clear();
                                PropFormsStrings.JournalDictionary[this._device].AddRange(PropFormsStrings.GetNewJournalList());
                                PropFormsStrings.AlarmDictionary[this._device].Clear();
                                PropFormsStrings.AlarmDictionary[this._device].AddRange(PropFormsStrings.GetNewAlarmList());
                            }
                            break;
                        case "MR761":
                            try
                            {
                                ListsOfJournals lists = new ListsOfJournals("MR76X");
                                lists.XmlFromFile(reader);
                                PropFormsStrings.JournalDictionary[this._device].Clear();
                                PropFormsStrings.JournalDictionary[this._device].AddRange(lists.SysJournal.MessagesList);
                                PropFormsStrings.AlarmDictionary[this._device].Clear();
                                PropFormsStrings.AlarmDictionary[this._device].AddRange(lists.AlarmJournal.MessagesList);
                            }
                            catch (Exception)
                            {
                                PropFormsStrings.JournalDictionary[this._device].Clear();
                                PropFormsStrings.JournalDictionary[this._device].AddRange(PropFormsStrings.GetNewJournalList());
                                PropFormsStrings.AlarmDictionary[this._device].Clear();
                                PropFormsStrings.AlarmDictionary[this._device].AddRange(PropFormsStrings.GetNewAlarmList());
                            }
                            break;
                    }
                }
            }
        }

        private void OpenFromDeviceBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this._filler.TabPages.Count != 0 && this.FileCloseProject() == DialogResult.Cancel)
            {
                return;
            }
            if (this._device.MB.BaudeRate <= RST_BAUDE_RATE && !this._device.MB.NetworkEnabled)
            {
                MessageBox.Show("Запись и чтение логики не поддерживается по интерфейсу RS-485", "Внимание",
                    MessageBoxButtons.OK);
                return;
            }
            if (MessageBox.Show("Прочитать архив логической программы из устройства?", "Прочитать архив",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
            this._formCheck = new MessageBoxForm();
            if (!this._fileDriver.ReadFile(this.ReadStarted, JOURNAL_LIST_FILE_NAME))
            {
                MessageBox.Show(
                    "Файл \"jlist.xml\" уже используется. Дождитесь завершения предыдущей операции или перезагрузите устройство",
                    "Внимание", MessageBoxButtons.OK);
                return;
            }
            this._formCheck.Caption = "Загрузка списка";
            this._formCheck.SetProgressBarStyle(ProgressBarStyle.Marquee);
            this._formCheck.OkBtnVisibility = false;
            this._formCheck.ShowDialog("Идет загрузка списка сигналов ЖА и ЖС");
        }

        private void ReadStarted(byte[] readBytes, string mes)
        {
            if (readBytes != null && readBytes.Length != 0)
            {
                this.SetLists(readBytes);
            }
            else
            {
                MessageBox.Show(string.Format("Невозможно прочитать файл {0}. {1}", JOURNAL_LIST_FILE_NAME, mes),
                    "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            if (!this._fileDriver.ReadFile(this.OnArchRead, ARH_NANE))
            {
                MessageBox.Show("Файл \"logarch.zip\" уже используется. Дождитесь завершения предыдущей операции или перезагрузите устройство",
                    "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            this._formCheck.Caption = "Чтение архива";
            this._formCheck.ShowMessage("Идет загрузка архива. Пожалуйста, подождите");
        }

        private void OnArchRead(byte[] readBytes, string mess)
        {
            if ((_fileDriver.MessagesDictionary[115] == mess || _fileDriver.MessagesDictionary[116] == mess) && _fileDriver.FailCounter < 2)
            {
                _fileDriver.FailCounter++;
                Thread.Sleep(1000);
                this._fileDriver.ReadFile(this.EmulReadStarted, JOURNAL_LIST_FILE_NAME);
            }
            else
            {
                _fileDriver.FailCounter = 0;
                try
                {
                    if (readBytes.Length != 0)
                    {
                        this.LoadProjectFromBin(this.Uncompress(readBytes));
                        this._formCheck.ShowResultMessage(InformationMessages.PROJECT_LOADED_OK_OF_DEVICE);
                    }
                    else
                    {
                        throw new Exception(mess);
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    mess = "Ошибка разархивирования схемы. Файл архива поврежден";
                    this._formCheck.Caption = "Ошибка";
                    this._formCheck.ShowResultMessage(mess);
                }
                catch (Exception)
                {
                    this._formCheck.Caption = "Ошибка";
                    this._formCheck.ShowResultMessage(mess);
                }
            }
        }

        private void StartWriteToDeviceMainArchive(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this._device.MB.BaudeRate <= RST_BAUDE_RATE && !this._device.MB.NetworkEnabled)
            {
                MessageBox.Show("Запись и чтение логики не поддерживается по интерфейсу RS-485", "Внимание",
                    MessageBoxButtons.OK);
                return;
            }
            this._compressedZip = this.CompresseProject();
            if (this._compressedZip.Length == 0) return;

            if (MessageBox.Show("Записать логическую программу в устройство?", "Запись", MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) == DialogResult.No) return;
            if (!this.CompileProject()) return;
            this.StartWriteList();
        }

        private void StartWriteList()
        {
            string nameDevice = "";

            switch (_device.DeviceType)
            {
                case "MR771":
                    nameDevice = "MR771";
                    break;
                case "MR761":
                    nameDevice = "MR76X";
                    break;
            }

            ListsOfJournals lists = new ListsOfJournals(nameDevice);
            lists.SysJournal.MessagesList = PropFormsStrings.JournalDictionary[this._device];
            lists.AlarmJournal.MessagesList = PropFormsStrings.AlarmDictionary[this._device];
            lists.Version = Common.VersionConverter(this._device.DeviceVersion);
            using (MemoryStream stream = new MemoryStream())
            {
                using (XmlTextWriter writer = new XmlTextWriter(stream, Encoding.GetEncoding(1251)))
                {
                    writer.Formatting = Formatting.Indented;
                    lists.XmlToFile(writer);
                    byte[] buff = stream.GetBuffer().Take((int)stream.Length).ToArray();
                    this._fileDriver.WriteFile(this.ListsWritten, buff, JOURNAL_LIST_FILE_NAME);
                    this._formCheck.SetProgressBarStyle(ProgressBarStyle.Marquee);
                    this._formCheck.OkBtnVisibility = false;
                    this._formCheck.Caption = "Запись списка";
                    this._formCheck.ShowDialog("Запись списка подписей сигналов ЖС и ЖА СПЛ");
                }
            }
        }

        private void ListsWritten(bool b, string mes)
        {
            if ((_fileDriver.MessagesDictionary[115] == mes || _fileDriver.MessagesDictionary[116] == mes) && _fileDriver.FailCounter < 2)
            {
                _fileDriver.FailCounter++;
                Thread.Sleep(1000);
                byte[] wBytes = Common.TOBYTES(this._compressedZip, false);
                this._fileDriver.WriteFile(this.ListsWritten, wBytes, JOURNAL_LIST_FILE_NAME);
            }
            else
            {
                _fileDriver.FailCounter = 0;
                if (b)
                {
                    this._fileDriver.DeleteFile(this.OnFileDeleted, ARH_NANE);
                    this._formCheck.Caption = "Запись архива";
                    this._formCheck.ShowMessage("Удаление старого архива логической программы");
                }
                else
                {
                    this._formCheck.ShowResultMessage(mes);
                }
            }
            
        }

        private void OnFileDeleted(bool res, string message)
        {
            if (res || string.Equals(message, FILE_NOT_FOUND, StringComparison.CurrentCultureIgnoreCase))
            {
                this.SaveCompiledProject();
            }
            else
            {
                this._formCheck.ShowResultMessage(message);
            }
        }

        private void OnFileWriten(bool res, string message)
        {
            if ((_fileDriver.MessagesDictionary[115] == message || _fileDriver.MessagesDictionary[116] == message) && _fileDriver.FailCounter < 2)
            {
                _fileDriver.FailCounter++;
                Thread.Sleep(1000);
                byte[] wBytes = Common.TOBYTES(this._compressedZip, false);
                this._fileDriver.WriteFile(this.OnFileWriten, wBytes, ARH_NANE);
            }
            else
            {
                _fileDriver.FailCounter = 0;
                if (res)
                {
                    this._formCheck.ShowResultMessage(InformationMessages.PROGRAM_ARCHIVE_SAVE_OK_START_PROGRAM);
                    this.StartLoadCurrentSignals();
                }
                else
                {
                    this._formCheck.ShowResultMessage(message);
                }
            }
        }

        private void StartSplEmulBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this._device.MB.BaudeRate <= RST_BAUDE_RATE && !this._device.MB.NetworkEnabled)
            {
                MessageBox.Show("Запись и чтение логики не поддерживается по интерфейсу RS-485", "Внимание",
                    MessageBoxButtons.OK);
                return;
            }
            if (MessageBox.Show("Запустить эмуляцию логической программы?", "Запуск эмуляции",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.04)
            {
                if (!this._fileDriver.ReadFile(this.EmulReadStarted, JOURNAL_LIST_FILE_NAME))
                {
                    MessageBox.Show(
                        "Файл \"jlist.xml\" уже используется. Дождитесь завершения предыдущей операции или перезагрузите устройство",
                        "Внимание", MessageBoxButtons.OK);
                    return;
                }
                this._formCheck.Caption = "Загрузка списка";
                this._formCheck.SetProgressBarStyle(ProgressBarStyle.Marquee);
                this._formCheck.OkBtnVisibility = false;
                this._formCheck.ShowDialog("Идет загрузка списка сигналов ЖА и ЖС");
            }
            else
            {
                if (!this._fileDriver.ReadFile(this.OnArchEmulReaded, ARH_NANE))
                {
                    MessageBox.Show("Файл уже используется. Дождитесь завершения предыдущей операции", "Внимание",
                        MessageBoxButtons.OK);
                    return;
                }

                this._formCheck.SetProgressBarStyle(ProgressBarStyle.Marquee);
                this._formCheck.Caption = "Чтение архива";
                this._formCheck.ShowDialog("Идет загрузка архива. Пожалуйста, подождите");
            }
        }

        private void EmulReadStarted(byte[] readBytes, string mes)
        {
            if ((_fileDriver.MessagesDictionary[115] == mes || _fileDriver.MessagesDictionary[116] == mes) && _fileDriver.FailCounter < 2)
            {
                _fileDriver.FailCounter++;
                Thread.Sleep(1000);
                this._fileDriver.ReadFile(this.EmulReadStarted, JOURNAL_LIST_FILE_NAME);
            }
            else
            {
                _fileDriver.FailCounter = 0;
                if (readBytes != null && readBytes.Length != 0 && this._fileDriver.MessagesDictionary.First(m => m.Value == mes).Key == 0)
                {
                    this.SetLists(readBytes);
                }
                else
                {
                    MessageBox.Show(string.Format("Невозможно прочитать файл {0}. {1}", JOURNAL_LIST_FILE_NAME, mes),
                        "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                if (!this._fileDriver.ReadFile(this.OnArchEmulReaded, ARH_NANE))
                {
                    MessageBox.Show("Файл \"logarch.zip\" уже используется. Дождитесь завершения предыдущей операции или перезагрузите устройство",
                        "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                this._formCheck.Caption = "Чтение архива";
                this._formCheck.ShowMessage("Идет загрузка архива. Пожалуйста, подождите");
            }
            
        }

        private void OnArchEmulReaded(byte[] readBytes, string mes)
        {
            if (readBytes.Length != 0 && this._fileDriver.MessagesDictionary.First(m => m.Value == mes).Key == 0)
            {
                byte[] fromDevice = this.Uncompress(readBytes);
                if (this._filler.TabPages.Count != 0 )
                {
                    if (this.CompareScheme(fromDevice))
                    {
                        DialogResult res =
                            MessageBox.Show(
                                "Архив логической программы, загруженной из устройства, не совпадает с проектом, открытым в униконе. Продолжить эмуляцию с загруженным архивом?",
                                "ВНИМАНИЕ", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (res == DialogResult.No)
                        {
                            this._formCheck.ShowResultMessage("Отменено пользователем");
                            return;
                        }
                    }
                    this._filler.TabPages.Clear();
                }
                
                this.LoadProjectFromBin(fromDevice);
                
                this._isOnSimulateMode = true;
                this._isCompileScheme = true;
                this.CompileProject();
                foreach (BSBGLTab tabPage in this._filler.TabPages)
                {
                    tabPage.Schematic.StartDebugMode();
                }
                this._isCompileScheme = false;
                this.StartLoadCurrentSignals();
            }
            else
            {
                this._formCheck.Caption = "Ошибка";
                this._formCheck.ShowResultMessage("Невозможно загрузить архив \"logarch.zip\"\n" + mes);
            }
        }

        private ushort[] CompresseProject()
        {
            ZIPCompressor compr = new ZIPCompressor();
            byte[] uncompressed = this.MakeBinFromXml();
            if(uncompressed.Length == 0) return new ushort[0];
            byte[] compressed = compr.Compress(uncompressed);
            ushort[] compressedWords = new ushort[(compressed.Length + 1) / 2 + 3]; //Размер хранилища
            compressedWords[0] = (ushort)compressed.Length; // размер сжатого проекта (байты)
            compressedWords[1] = 0x0001; // Версия упаковщика
            compressedWords[2] = 0x0000; // СRС архива проекта
            for (int i = 3; i < (compressed.Length + 1) / 2 + 3; i++)
            {
                if ((i - 2) * 2 + 1 < compressed.Length)
                {
                    compressedWords[i] = (ushort)(compressed[(i - 3) * 2 + 1] << 8);
                }
                else
                {
                    compressedWords[i] = 0;
                }
                compressedWords[i] += compressed[(i - 3) * 2];
            }
            return compressedWords;
        }

        private byte[] MakeBinFromXml()
        {
            MemoryStream memstream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(memstream, Encoding.UTF8);
            writer.Formatting = Formatting.Indented;
            writer.WriteStartDocument();
            writer.WriteStartElement("BSBGL_ProjectFile");
            if (this._filler.TabPages.Count == 0) return new byte[0];
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("name", src.TabName);
                src.Schematic.WriteXml(writer);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();
            return memstream.ToArray();
        }

        private void LoadProjectFromBin(byte[] uncompressed)
        {
            MemoryStream memstream = new MemoryStream();
            memstream.Write(uncompressed, 0, uncompressed.Length);
            memstream.Seek(0, SeekOrigin.Begin);
            XmlTextReader reader = new XmlTextReader(memstream);
            reader.WhitespaceHandling = WhitespaceHandling.None;
            while (reader.Read())
            {
                if ((reader.Name != "Source") || (reader.NodeType == XmlNodeType.EndElement)) continue;
                BSBGLTab myTab = new BSBGLTab();
                myTab.InitializeBSBGLSheet(reader.GetAttribute("name"), SheetFormat.A0_L, this._device, this._manager);
                myTab.Selected = true;
                this._filler.TabPages.Add(myTab);
                myTab.Schematic.ReadXml(reader);
            }
            reader.Close();
        }

        private bool CompareScheme(byte[] fromDevice)
        {
            byte[] fromScheme = this.MakeBinFromXml();
            return fromScheme.Length == 0 || fromScheme.Length != fromDevice.Length ||
                   fromScheme.Where((t, i) => t != fromDevice[i]).Any();
        }

        private byte[] Uncompress(byte[] readBytes)
        {
            ushort[] readData = Common.TOWORDS(readBytes, false);
            ZIPCompressor compr = new ZIPCompressor();
            byte[] compressed = new byte[readData[0]];
            for (int i = 3; i < (compressed.Length + 1) / 2 + 3; i++)
            {
                if ((i - 2) * 2 + 1 < compressed.Length)
                {
                    compressed[(i - 3) * 2 + 1] = (byte)(readData[i] >> 8);
                }
                compressed[(i - 3) * 2] = (byte)readData[i];
            }
            return compr.Decompress(compressed);
        }
        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MRUniversal); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(BSBGLEF); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.programming.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Программирование"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}
