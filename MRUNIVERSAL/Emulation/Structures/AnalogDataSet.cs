﻿namespace BEMN.MRUNIVERSAL.Emulation.Structures
{
    public class AnalogDataSet
    {
        private int _countI;
        private int _countU;

        private InputAnalogDataI[] _dataI;
        private InputAnalogDataU[] _dataU;

        public AnalogDataSet(int countI, int countU)
        {
            _countI = countI;
            _countU = countU;

            _dataI = new InputAnalogDataI[_countI];
            _dataU = new InputAnalogDataU[_countU];
        }

        public InputAnalogDataI[] DataI
        {
            get { return _dataI; }
        }

        public InputAnalogDataU[] DataU
        {
            get { return _dataU; }
        }

        public void SetData(InputAnalogData[] allData)
        {
            for (int i = 0; i < _countI; i++)
            {
                _dataI[i] = new InputAnalogDataI();
                _dataI[i].SetData(allData[i]);
            }
            for (int i = _countI; i < _countI + _countU; i++)
            {
                _dataU[i - _countI] = new InputAnalogDataU();
                _dataU[i - _countI].SetData(allData[i]);
            }
        }

        public InputAnalogData[] GetData()
        {
            InputAnalogData[] ret = new InputAnalogData[WriteStructEmul.COUNT];

            for (int i = 0; i < _countI; i++)
            {
                ret[i] = _dataI[i].GetData();
            }
            for (int i = _countI; i < _countI + _countU; i++)
            {
                ret[i] = _dataU[i - _countI].GetData();
            }

            if (_countI + _countU < WriteStructEmul.COUNT)
            {
                for (int i = _countI+_countU; i < WriteStructEmul.COUNT; i++)
                {
                    ret[i] = new InputAnalogData();
                }
            }

            return ret;
        }
    }
}
