﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MRUNIVERSAL.Emulation.Structures
{
    [Serializable]
    public class DiscretsSignals : StructBase
    {
        #region[Constants]
        public const int LOGIC_COUNT = 5;
        #endregion [Constants] 

        #region [Private fields]
        [Layout(0, Count = LOGIC_COUNT)]private ushort[] _discretsSignals; 
        #endregion [Private fields]

        #region Public Fields
        [BindingProperty(0)]
        [XmlElement(ElementName = "D1")]
        public bool D1
        {
            get { return Common.GetBit(this._discretsSignals[0], 0); }
            set { this._discretsSignals[0]=Common.SetBit(this._discretsSignals[0], 0, value); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "D2")]
        public bool D2
        {
            get { return Common.GetBit(this._discretsSignals[0], 1); }
            set { this._discretsSignals[0]=Common.SetBit(this._discretsSignals[0], 1, value); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "D3")]
        public bool D3
        {
            get { return Common.GetBit(this._discretsSignals[0], 2); }
            set { this._discretsSignals[0]=Common.SetBit(this._discretsSignals[0], 2, value); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "D4")]
        public bool D4
        {
            get { return Common.GetBit(this._discretsSignals[0], 3); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 3, value); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "D5")]
        public bool D5
        {
            get { return Common.GetBit(this._discretsSignals[0], 4); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 4, value); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "D6")]
        public bool D6
        {
            get { return Common.GetBit(this._discretsSignals[0], 5); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 5, value); }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "D7")]
        public bool D7
        {
            get { return Common.GetBit(this._discretsSignals[0], 6); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 6, value); }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "D8")]
        public bool D8
        {
            get { return Common.GetBit(this._discretsSignals[0], 7); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 7, value); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "D9")]
        public bool D9
        {
            get { return Common.GetBit(this._discretsSignals[0], 8); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 8, value); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "D10")]
        public bool D10
        {
            get { return Common.GetBit(this._discretsSignals[0], 9); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 9, value); }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "D11")]
        public bool D11
        {
            get { return Common.GetBit(this._discretsSignals[0], 10); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 10, value); }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "D12")]
        public bool D12
        {
            get { return Common.GetBit(this._discretsSignals[0], 11); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 11, value); }
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "D13")]
        public bool D13
        {
            get { return Common.GetBit(this._discretsSignals[0], 12); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 12, value); }
        }

        [BindingProperty(13)]
        [XmlElement(ElementName = "D14")]
        public bool D14
        {
            get { return Common.GetBit(this._discretsSignals[0], 13); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 13, value); }
        }

        [BindingProperty(14)]
        [XmlElement(ElementName = "D15")]
        public bool D15
        {
            get { return Common.GetBit(this._discretsSignals[0], 14); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 14, value); }
        }

        [BindingProperty(15)]
        [XmlElement(ElementName = "D16")]
        public bool D16
        {
            get { return Common.GetBit(this._discretsSignals[0], 15); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 15, value); }
        }

        [BindingProperty(16)]
        [XmlElement(ElementName = "D17")]
        public bool D17
        {
            get { return Common.GetBit(this._discretsSignals[1], 0); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 0, value); }
        }

        [BindingProperty(17)]
        [XmlElement(ElementName = "D18")]
        public bool D18
        {
            get { return Common.GetBit(this._discretsSignals[1], 1); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 1, value); }
        }

        [BindingProperty(18)]
        [XmlElement(ElementName = "D19")]
        public bool D19
        {
            get { return Common.GetBit(this._discretsSignals[1], 2); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 2, value); }
        }

        [BindingProperty(19)]
        [XmlElement(ElementName = "D20")]
        public bool D20
        {
            get { return Common.GetBit(this._discretsSignals[1], 3); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 3, value); }
        }

        [BindingProperty(20)]
        [XmlElement(ElementName = "D21")]
        public bool D21
        {
            get { return Common.GetBit(this._discretsSignals[1], 4); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 4, value); }
        }

        [BindingProperty(21)]
        [XmlElement(ElementName = "D22")]
        public bool D22
        {
            get { return Common.GetBit(this._discretsSignals[1], 5); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 5, value); }
        }

        [BindingProperty(22)]
        [XmlElement(ElementName = "D23")]
        public bool D23
        {
            get { return Common.GetBit(this._discretsSignals[1], 6); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 6, value); }
        }

        [BindingProperty(23)]
        [XmlElement(ElementName = "D24")]
        public bool D24
        {
            get { return Common.GetBit(this._discretsSignals[1], 7); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 7, value); }
        }

        [BindingProperty(24)]
        [XmlElement(ElementName = "D25")]
        public bool D25
        {
            get { return Common.GetBit(this._discretsSignals[1], 8); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 8, value); }
        }

        [BindingProperty(25)]
        [XmlElement(ElementName = "D26")]
        public bool D26
        {
            get { return Common.GetBit(this._discretsSignals[1], 9); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 9, value); }
        }

        [BindingProperty(26)]
        [XmlElement(ElementName = "D27")]
        public bool D27
        {
            get { return Common.GetBit(this._discretsSignals[1], 10); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 10, value); }
        }

        [BindingProperty(27)]
        [XmlElement(ElementName = "D28")]
        public bool D28
        {
            get { return Common.GetBit(this._discretsSignals[1], 11); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 11, value); }
        }

        [BindingProperty(28)]
        [XmlElement(ElementName = "D29")]
        public bool D29
        {
            get { return Common.GetBit(this._discretsSignals[1], 12); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 12, value); }
        }

        [BindingProperty(29)]
        [XmlElement(ElementName = "D30")]
        public bool D30
        {
            get { return Common.GetBit(this._discretsSignals[1], 13); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 13, value); }
        }
        [BindingProperty(30)]
        [XmlElement(ElementName = "D31")]
        public bool D31
        {
            get { return Common.GetBit(this._discretsSignals[1], 14); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 14, value); }
        }

        [BindingProperty(31)]
        [XmlElement(ElementName = "D32")]
        public bool D32
        {
            get { return Common.GetBit(this._discretsSignals[1], 15); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 15, value); }
        }

        [BindingProperty(32)]
        [XmlElement(ElementName = "D33")]
        public bool D33
        {
            get { return Common.GetBit(this._discretsSignals[2], 0); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 0, value); }
        }

        [BindingProperty(33)]
        [XmlElement(ElementName = "D34")]
        public bool D34
        {
            get { return Common.GetBit(this._discretsSignals[2], 1); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 1, value); }
        }

        [BindingProperty(34)]
        [XmlElement(ElementName = "D35")]
        public bool D35
        {
            get { return Common.GetBit(this._discretsSignals[2], 2); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 2, value); }
        }

        [BindingProperty(35)]
        [XmlElement(ElementName = "D36")]
        public bool D36
        {
            get { return Common.GetBit(this._discretsSignals[2], 3); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 3, value); }
        }

        [BindingProperty(36)]
        [XmlElement(ElementName = "D37")]
        public bool D37
        {
            get { return Common.GetBit(this._discretsSignals[2], 4); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 4, value); }
        }

        [BindingProperty(37)]
        [XmlElement(ElementName = "D38")]
        public bool D38
        {
            get { return Common.GetBit(this._discretsSignals[2], 5); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 5, value); }
        }

        [BindingProperty(38)]
        [XmlElement(ElementName = "D39")]
        public bool D39
        {
            get { return Common.GetBit(this._discretsSignals[2], 6); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 6, value); }
        }

        [BindingProperty(39)]
        [XmlElement(ElementName = "D40")]
        public bool D40
        {
            get { return Common.GetBit(this._discretsSignals[2], 7); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 7, value); }
        }

        [BindingProperty(40)]
        [XmlElement(ElementName = "D41")]
        public bool D41
        {
            get { return Common.GetBit(this._discretsSignals[2], 8); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 8, value); }
        }
        [BindingProperty(41)]
        [XmlElement(ElementName = "D42")]
        public bool D42
        {
            get { return Common.GetBit(this._discretsSignals[2], 9); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 9, value); }
        }
        [BindingProperty(42)]
        [XmlElement(ElementName = "D43")]
        public bool D43
        {
            get { return Common.GetBit(this._discretsSignals[2], 10); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 10, value); }
        }
        [BindingProperty(43)]
        [XmlElement(ElementName = "D44")]
        public bool D44
        {
            get { return Common.GetBit(this._discretsSignals[2], 11); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 11, value); }
        }
        [BindingProperty(44)]
        [XmlElement(ElementName = "D45")]
        public bool D45
        {
            get { return Common.GetBit(this._discretsSignals[2], 12); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 12, value); }
        }
        [BindingProperty(45)]
        [XmlElement(ElementName = "D46")]
        public bool D46
        {
            get { return Common.GetBit(this._discretsSignals[2], 13); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 13, value); }
        }
        [BindingProperty(46)]
        [XmlElement(ElementName = "D47")]
        public bool D47
        {
            get { return Common.GetBit(this._discretsSignals[2], 14); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 14, value); }
        }
        [BindingProperty(47)]
        [XmlElement(ElementName = "D48")]
        public bool D48
        {
            get { return Common.GetBit(this._discretsSignals[2], 15); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 15, value); }
        }
        [BindingProperty(48)]
        [XmlElement(ElementName = "D49")]
        public bool D49
        {
            get { return Common.GetBit(this._discretsSignals[3], 0); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 0, value); }
        }
        [BindingProperty(49)]
        [XmlElement(ElementName = "D50")]
        public bool D50
        {
            get { return Common.GetBit(this._discretsSignals[3], 1); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 1, value); }
        }
        [BindingProperty(50)]
        [XmlElement(ElementName = "D51")]
        public bool D51
        {
            get { return Common.GetBit(this._discretsSignals[3], 2); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 2, value); }
        }
        [BindingProperty(51)]
        [XmlElement(ElementName = "D52")]
        public bool D52
        {
            get { return Common.GetBit(this._discretsSignals[3], 3); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 3, value); }
        }
        [BindingProperty(52)]
        [XmlElement(ElementName = "D53")]
        public bool D53
        {
            get { return Common.GetBit(this._discretsSignals[3], 4); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 4, value); }
        }
        [BindingProperty(53)]
        [XmlElement(ElementName = "D54")]
        public bool D54
        {
            get { return Common.GetBit(this._discretsSignals[3], 5); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 5, value); }
        }
        [BindingProperty(54)]
        [XmlElement(ElementName = "D55")]
        public bool D55
        {
            get { return Common.GetBit(this._discretsSignals[3], 6); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 6, value); }
        }
        [BindingProperty(55)]
        [XmlElement(ElementName = "D56")]
        public bool D56
        {
            get { return Common.GetBit(this._discretsSignals[3], 7); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 7, value); }
        }

        [BindingProperty(56)]
        [XmlElement(ElementName = "D57")]
        public bool D57
        {
            get { return Common.GetBit(this._discretsSignals[3], 8); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 8, value); }
        }

        [BindingProperty(57)]
        [XmlElement(ElementName = "D58")]
        public bool D58
        {
            get { return Common.GetBit(this._discretsSignals[3], 9); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 9, value); }
        }

        [BindingProperty(58)]
        [XmlElement(ElementName = "D59")]
        public bool D59
        {
            get { return Common.GetBit(this._discretsSignals[3], 10); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 10, value); }
        }

        [BindingProperty(59)]
        [XmlElement(ElementName = "D60")]
        public bool D60
        {
            get { return Common.GetBit(this._discretsSignals[3], 11); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 11, value); }
        }

        [BindingProperty(60)]
        [XmlElement(ElementName = "D61")]
        public bool D61
        {
            get { return Common.GetBit(this._discretsSignals[3], 12); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 12, value); }
        }

        [BindingProperty(61)]
        [XmlElement(ElementName = "D62")]
        public bool D62
        {
            get { return Common.GetBit(this._discretsSignals[3], 13); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 13, value); }
        }

        [BindingProperty(62)]
        [XmlElement(ElementName = "D63")]
        public bool D63
        {
            get { return Common.GetBit(this._discretsSignals[3], 14); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 14, value); }
        }

        [BindingProperty(63)]
        [XmlElement(ElementName = "D64")]
        public bool D64
        {
            get { return Common.GetBit(this._discretsSignals[3], 15); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 15, value); }
        }

        [BindingProperty(64)]
        [XmlElement(ElementName = "D65")]
        public bool D65
        {
            get { return Common.GetBit(this._discretsSignals[4], 0); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 0, value); }
        }

        [BindingProperty(65)]
        [XmlElement(ElementName = "D66")]
        public bool D66
        {
            get { return Common.GetBit(this._discretsSignals[4], 1); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 1, value); }
        }

        [BindingProperty(66)]
        [XmlElement(ElementName = "D67")]
        public bool D67
        {
            get { return Common.GetBit(this._discretsSignals[4], 2); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 2, value); }
        }

        [BindingProperty(67)]
        [XmlElement(ElementName = "D68")]
        public bool D68
        {
            get { return Common.GetBit(this._discretsSignals[4], 3); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 3, value); }
        }

        [BindingProperty(68)]
        [XmlElement(ElementName = "D69")]
        public bool D69
        {
            get { return Common.GetBit(this._discretsSignals[4], 4); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 4, value); }
        }

        [BindingProperty(69)]
        [XmlElement(ElementName = "D70")]
        public bool D70
        {
            get { return Common.GetBit(this._discretsSignals[4], 5); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 5, value); }
        }

        [BindingProperty(70)]
        [XmlElement(ElementName = "D71")]
        public bool D71
        {
            get { return Common.GetBit(this._discretsSignals[4], 6); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 6, value); }
        }

        [BindingProperty(71)]
        [XmlElement(ElementName = "D72")]
        public bool D72
        {
            get { return Common.GetBit(this._discretsSignals[4], 7); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 7, value); }
        }

        [BindingProperty(72)]
        [XmlElement(ElementName = "K1")]
        public bool K1
        {
            get { return Common.GetBit(this._discretsSignals[4], 8); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 8, value); }
        }
        [BindingProperty(73)]
        [XmlElement(ElementName = "K2")]
        public bool K2
        {
            get { return Common.GetBit(this._discretsSignals[4], 9); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 9, value); }
        }

        #endregion
    }
}
