﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace BEMN.MRUNIVERSAL.Emulation.Structures
{
    [Serializable]
    public struct EmulScene
    {
        [XmlElement(ElementName = "NAME_SCENE_VALUE")]
        public string NameScene { get; set; }

        [XmlElement(ElementName = "TIME_SCENE_VALUE")]
        public int Time { get; set; }

        [XmlElement(ElementName = "IU_VALUE")]
        public List<int> Value { get; set; }

        [XmlElement(ElementName = "DISKRETS_STATE_VALUE")]
        public List<CheckState> DiskretsState { get; set; }
        
        public EmulScene(string nameScene, int time, List<int> value, List<CheckState> diskretsState)
        {
            NameScene = nameScene;
            Time = time;
            Value = value;
            DiskretsState = diskretsState;
        }
    }
}
