﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MRUNIVERSAL.Emulation.Structures
{
    public class TimeAndSignalStruct : StructBase
    {
        [Layout(0)] private ushort _statusTime; // 
        [Layout(1)] private ushort _signal; // сигнал в БД

        /// <summary>
        /// необходимо для обнуления 
        /// </summary>
        [XmlElement(ElementName = "FOR_RESET_SETPOINTS_VALUE")]
        public bool IsRestart
        {
            get { return true; }
            set { this._statusTime = Common.SetBit(this._statusTime, 15, value); }
        }


        /// <summary>
        /// необходимо для сброса расчета временм
        /// </summary>
        [XmlIgnore]
        public bool StopTime
        {
            get { return true; }
            set
            {
                if (value)
                {
                    this._statusTime = Common.SetBits(this._statusTime, 0, 0, 1);
                }

            }
        }
        /// <summary>
        /// необходимо для недопущения старта рассчета времени повторно,так как после выставления первого бита он автоматически сбрасывается (костыль)
        /// </summary>
        [XmlIgnore]
        public bool WriteTimeOk
        {
            set
            {
                this._statusTime = Common.SetBits(this._statusTime, 2, 0);
            }
        }

        #region [Properties] 

        /// <summary>
        /// Cтатус для расчета времени
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "STATUS_FOR_CALCULATION_TIME_VALUE")]
        public bool StatusTime
        {
            get
            {

                return Common.GetBit(this._statusTime, 0);
            }
            set
            {
                if (value)
                {
                    this._statusTime = 0;
                    this._statusTime = Common.SetBit(this._statusTime, 0, true);
                }
                else
                {
                    return;
                }
            }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "STOP_TIMING_SIGNAL_VALUE")]
        public string Signal
        {
            get { return Validator.Get(this._signal, Strings.RelaySignals); }
            set { this._signal = Validator.Set(value, Strings.RelaySignals); }
        }

        #endregion

    }
}
