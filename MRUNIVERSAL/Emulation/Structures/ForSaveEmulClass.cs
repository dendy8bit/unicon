﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BEMN.MRUNIVERSAL.Emulation.Structures
{
    [Serializable]
    public class ForSaveEmulClass
    {
        [XmlElement(ElementName = "SCENE_INFO")]
        public List<EmulScene> EmulScene { get; set; }
        [XmlElement(ElementName = "MAIN_INFO")]
        public WriteStructEmul WriteStruct { get; set; }
        
    }
}
