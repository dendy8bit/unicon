﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MRUNIVERSAL.Emulation.Structures
{
    [Serializable]
    public class WriteStructEmul : StructBase
    {
        #region [Private Fiels]
        
        public const int COUNT = 10;

        [Layout(0)] private TimeAndSignalStruct _timeAndSignal;
        [Layout(1, Count = COUNT)] private InputAnalogData[] _inputAnalog; // входные аналогивые данные
        [Layout(2)] private DiscretsSignals _diskretInput; // дискретные данные (64 сигнала)

        #endregion

        [XmlElement(ElementName = "TIME_SIGNAL_DATA")]
        public TimeAndSignalStruct TimeSignal
        {
            get { return this._timeAndSignal; }
            set { this._timeAndSignal = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [XmlArray("ANALOG_DATA")]
        [XmlArrayItem(Type = typeof(InputAnalogData))]
        public InputAnalogData[] AllAnalogData
        {
            get { return this._inputAnalog; }
            set { this._inputAnalog = value; }
        }
        
        /// <summary>
        /// Список дискретных синалов
        /// </summary>
        [XmlElement(ElementName = "DISKRETS_DATA")]
        public DiscretsSignals DiscretInputs
        {
            get { return this._diskretInput; }
            set { this._diskretInput = value; }
        }
    }
}


