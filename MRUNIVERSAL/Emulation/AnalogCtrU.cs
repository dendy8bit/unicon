﻿using System.Windows.Forms;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.MRUNIVERSAL.Emulation.Structures;

namespace BEMN.MRUNIVERSAL.Emulation
{
    public partial class AnalogCtrU : UserControl
    {
        public Button[] UpButtons { get; }
        public Button[] DownButtons { get; }
        public MaskedTextBox[] TextBoxes { get; }
        public Label[] Labels { get; }

        public AnalogCtrU() : this("I", "f", "F")
        {

        }

        private NewStructValidator<InputAnalogDataU> _inputAnalogDataUValidation;
        
        public AnalogCtrU(string analogName, string fazaName, string freqName)
        {
            InitializeComponent();

            _amplitudeLabel.Text = analogName;
            _fazaLabel.Text = fazaName;
            _frequencyLabel.Text = freqName;

            this._inputAnalogDataUValidation = new NewStructValidator<InputAnalogDataU>
            (   this.toolTip,
                new ControlInfoText(this.UMTB, new CustomDoubleRule(0, 256)),
                new ControlInfoText(this.fazaMTBU, new CustomDoubleRule(0, 359)),
                new ControlInfoText(this.FMTBR, new CustomDoubleRule(0, 500))
            );

            FMTBR.Text = 50.ToString();

            UpButtons = new Button[]
            {
                this._amplitudeUp, this._fazaUp, this._frequencyUp
            };
            DownButtons = new Button[]
            {
                this._amplitudeDown, this._fazaDown, this._frequencyDown
            };
            TextBoxes = new MaskedTextBox[]
            {
                this.UMTB, this.fazaMTBU, this.FMTBR
            };
            Labels = new Label[]
            {
                this._amplitudeLabel, this._fazaLabel, this._frequencyLabel
            };

        }
        
        public InputAnalogDataU GetDataU()
        {
            return _inputAnalogDataUValidation.Get();
        }

        public void SetDataU(InputAnalogDataU data)
        {
            _inputAnalogDataUValidation.Set(data);
        }


    }
}
