﻿namespace BEMN.MRUNIVERSAL.Emulation
{
    partial class EmulationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._startTime = new System.Windows.Forms.CheckBox();
            this._signal = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.label31 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this._status = new System.Windows.Forms.MaskedTextBox();
            this._step = new System.Windows.Forms.NumericUpDown();
            this._getTimeGroupBox = new System.Windows.Forms.GroupBox();
            this._timeSignal = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.groupBoxDiskrets = new System.Windows.Forms.GroupBox();
            this.panelDiskretsTime = new System.Windows.Forms.Panel();
            this.groupBoxUtime = new System.Windows.Forms.GroupBox();
            this.panelUCheck = new System.Windows.Forms.Panel();
            this.groupBoxItime = new System.Windows.Forms.GroupBox();
            this.panelICheck = new System.Windows.Forms.Panel();
            this._writeEmulButton = new System.Windows.Forms.CheckBox();
            this.label34 = new System.Windows.Forms.Label();
            this.groupBoxInputSignal = new System.Windows.Forms.GroupBox();
            this._stopSceneButton = new System.Windows.Forms.Button();
            this._showSceneParamButton = new System.Windows.Forms.Button();
            this._deleteSceneButton = new System.Windows.Forms.Button();
            this._startSceneButton = new System.Windows.Forms.Button();
            this._scenesLabel = new System.Windows.Forms.Label();
            this._sceneComboBox = new System.Windows.Forms.ComboBox();
            this._addSceneEmulButton = new System.Windows.Forms.Button();
            this.groupBoxInputDiskrets = new System.Windows.Forms.GroupBox();
            this.panelDiscrets = new System.Windows.Forms.Panel();
            this.groupBoxI = new System.Windows.Forms.GroupBox();
            this.panelI = new System.Windows.Forms.Panel();
            this.groupBoxU = new System.Windows.Forms.GroupBox();
            this.panelU = new System.Windows.Forms.Panel();
            this._time = new System.Windows.Forms.Label();
            this._labelStatus = new System.Windows.Forms.Label();
            this._kvit = new System.Windows.Forms.Button();
            this.kvitTooltip = new System.Windows.Forms.ToolTip(this.components);
            this._exitEmulation = new System.Windows.Forms.CheckBox();
            this._fileProcessDialog = new System.Windows.Forms.OpenFileDialog();
            this._fromXmlBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this._statusLedControl = new BEMN.Forms.LedControl();
            ((System.ComponentModel.ISupportInitialize)(this._step)).BeginInit();
            this._getTimeGroupBox.SuspendLayout();
            this.groupBoxDiskrets.SuspendLayout();
            this.groupBoxUtime.SuspendLayout();
            this.groupBoxItime.SuspendLayout();
            this.groupBoxInputSignal.SuspendLayout();
            this.groupBoxInputDiskrets.SuspendLayout();
            this.groupBoxI.SuspendLayout();
            this.groupBoxU.SuspendLayout();
            this.SuspendLayout();
            // 
            // _startTime
            // 
            this._startTime.AutoSize = true;
            this._startTime.Location = new System.Drawing.Point(6, 74);
            this._startTime.Name = "_startTime";
            this._startTime.Size = new System.Drawing.Size(173, 17);
            this._startTime.TabIndex = 0;
            this._startTime.Text = "Старт по любому изменению";
            this._startTime.UseVisualStyleBackColor = true;
            this._startTime.CheckedChanged += new System.EventHandler(this._startTime_CheckedChanged);
            // 
            // _signal
            // 
            this._signal.FormattingEnabled = true;
            this._signal.Location = new System.Drawing.Point(6, 47);
            this._signal.Name = "_signal";
            this._signal.Size = new System.Drawing.Size(167, 21);
            this._signal.TabIndex = 1;
            this._signal.SelectedIndexChanged += new System.EventHandler(this._signal_SelectedIndexChanged);
            // 
            // label31
            // 
            this.label31.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(16, 501);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(30, 13);
            this.label31.TabIndex = 65;
            this.label31.Text = "Шаг:";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.Location = new System.Drawing.Point(152, 525);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(140, 33);
            this.button2.TabIndex = 67;
            this.button2.Text = "Прочитать эмуляцию";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.readEmulation_Click);
            // 
            // label32
            // 
            this.label32.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label32.Location = new System.Drawing.Point(6, 483);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(118, 26);
            this.label32.TabIndex = 68;
            this.label32.Text = "Время с момента подачи воздействия";
            // 
            // label33
            // 
            this.label33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(113, 492);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(135, 13);
            this.label33.TabIndex = 70;
            this.label33.Text = "Время режима эмуляции";
            // 
            // _status
            // 
            this._status.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._status.Location = new System.Drawing.Point(181, 489);
            this._status.Name = "_status";
            this._status.Size = new System.Drawing.Size(29, 20);
            this._status.TabIndex = 73;
            this._status.Visible = false;
            this._status.TextChanged += new System.EventHandler(this._status_TextChanged);
            // 
            // _step
            // 
            this._step.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._step.DecimalPlaces = 2;
            this._step.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this._step.Location = new System.Drawing.Point(52, 499);
            this._step.Name = "_step";
            this._step.Size = new System.Drawing.Size(58, 20);
            this._step.TabIndex = 74;
            this._step.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // _getTimeGroupBox
            // 
            this._getTimeGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._getTimeGroupBox.Controls.Add(this._timeSignal);
            this._getTimeGroupBox.Controls.Add(this.label35);
            this._getTimeGroupBox.Controls.Add(this.label32);
            this._getTimeGroupBox.Controls.Add(this.groupBoxDiskrets);
            this._getTimeGroupBox.Controls.Add(this.groupBoxUtime);
            this._getTimeGroupBox.Controls.Add(this.groupBoxItime);
            this._getTimeGroupBox.Controls.Add(this._startTime);
            this._getTimeGroupBox.Controls.Add(this._signal);
            this._getTimeGroupBox.Controls.Add(this._status);
            this._getTimeGroupBox.Location = new System.Drawing.Point(554, 3);
            this._getTimeGroupBox.Name = "_getTimeGroupBox";
            this._getTimeGroupBox.Size = new System.Drawing.Size(316, 516);
            this._getTimeGroupBox.TabIndex = 75;
            this._getTimeGroupBox.TabStop = false;
            this._getTimeGroupBox.Text = "Расчёт времени";
            // 
            // _timeSignal
            // 
            this._timeSignal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._timeSignal.AutoSize = true;
            this._timeSignal.Location = new System.Drawing.Point(139, 486);
            this._timeSignal.Name = "_timeSignal";
            this._timeSignal.Size = new System.Drawing.Size(0, 13);
            this._timeSignal.TabIndex = 78;
            // 
            // label35
            // 
            this.label35.Location = new System.Drawing.Point(6, 15);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(167, 29);
            this.label35.TabIndex = 77;
            this.label35.Text = "Сигнал окончания расчёта времени";
            // 
            // groupBoxDiskrets
            // 
            this.groupBoxDiskrets.Controls.Add(this.panelDiskretsTime);
            this.groupBoxDiskrets.Location = new System.Drawing.Point(156, 97);
            this.groupBoxDiskrets.Name = "groupBoxDiskrets";
            this.groupBoxDiskrets.Size = new System.Drawing.Size(149, 378);
            this.groupBoxDiskrets.TabIndex = 76;
            this.groupBoxDiskrets.TabStop = false;
            this.groupBoxDiskrets.Text = "Дискреты";
            // 
            // panelDiskretsTime
            // 
            this.panelDiskretsTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDiskretsTime.Location = new System.Drawing.Point(3, 16);
            this.panelDiskretsTime.Name = "panelDiskretsTime";
            this.panelDiskretsTime.Size = new System.Drawing.Size(143, 359);
            this.panelDiskretsTime.TabIndex = 82;
            // 
            // groupBoxUtime
            // 
            this.groupBoxUtime.Controls.Add(this.panelUCheck);
            this.groupBoxUtime.Location = new System.Drawing.Point(6, 192);
            this.groupBoxUtime.Name = "groupBoxUtime";
            this.groupBoxUtime.Size = new System.Drawing.Size(147, 107);
            this.groupBoxUtime.TabIndex = 76;
            this.groupBoxUtime.TabStop = false;
            this.groupBoxUtime.Text = "Напряжения ";
            // 
            // panelUCheck
            // 
            this.panelUCheck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelUCheck.Location = new System.Drawing.Point(3, 16);
            this.panelUCheck.Name = "panelUCheck";
            this.panelUCheck.Size = new System.Drawing.Size(141, 88);
            this.panelUCheck.TabIndex = 1;
            // 
            // groupBoxItime
            // 
            this.groupBoxItime.Controls.Add(this.panelICheck);
            this.groupBoxItime.Location = new System.Drawing.Point(6, 97);
            this.groupBoxItime.Name = "groupBoxItime";
            this.groupBoxItime.Size = new System.Drawing.Size(147, 92);
            this.groupBoxItime.TabIndex = 76;
            this.groupBoxItime.TabStop = false;
            this.groupBoxItime.Text = "Токи";
            // 
            // panelICheck
            // 
            this.panelICheck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelICheck.Location = new System.Drawing.Point(3, 16);
            this.panelICheck.Name = "panelICheck";
            this.panelICheck.Size = new System.Drawing.Size(141, 73);
            this.panelICheck.TabIndex = 1;
            // 
            // _writeEmulButton
            // 
            this._writeEmulButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeEmulButton.Appearance = System.Windows.Forms.Appearance.Button;
            this._writeEmulButton.BackColor = System.Drawing.SystemColors.Control;
            this._writeEmulButton.Location = new System.Drawing.Point(13, 525);
            this._writeEmulButton.Name = "_writeEmulButton";
            this._writeEmulButton.Size = new System.Drawing.Size(121, 33);
            this._writeEmulButton.TabIndex = 77;
            this._writeEmulButton.Text = "Записать эмуляцию ";
            this._writeEmulButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._writeEmulButton.UseVisualStyleBackColor = false;
            this._writeEmulButton.CheckedChanged += new System.EventHandler(this._writeEmulButton_CheckedChanged);
            // 
            // label34
            // 
            this.label34.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(246, 492);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(71, 13);
            this.label34.TabIndex = 74;
            this.label34.Text = "(мин:сек,мс)";
            // 
            // groupBoxInputSignal
            // 
            this.groupBoxInputSignal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxInputSignal.Controls.Add(this._stopSceneButton);
            this.groupBoxInputSignal.Controls.Add(this._showSceneParamButton);
            this.groupBoxInputSignal.Controls.Add(this._deleteSceneButton);
            this.groupBoxInputSignal.Controls.Add(this._startSceneButton);
            this.groupBoxInputSignal.Controls.Add(this._scenesLabel);
            this.groupBoxInputSignal.Controls.Add(this._sceneComboBox);
            this.groupBoxInputSignal.Controls.Add(this._addSceneEmulButton);
            this.groupBoxInputSignal.Controls.Add(this.groupBoxInputDiskrets);
            this.groupBoxInputSignal.Controls.Add(this.groupBoxI);
            this.groupBoxInputSignal.Controls.Add(this.groupBoxU);
            this.groupBoxInputSignal.Location = new System.Drawing.Point(6, 3);
            this.groupBoxInputSignal.Name = "groupBoxInputSignal";
            this.groupBoxInputSignal.Size = new System.Drawing.Size(542, 475);
            this.groupBoxInputSignal.TabIndex = 78;
            this.groupBoxInputSignal.TabStop = false;
            this.groupBoxInputSignal.Text = "Входные сигналы";
            this.groupBoxInputSignal.TextChanged += new System.EventHandler(this._time_TextChanged);
            // 
            // _stopSceneButton
            // 
            this._stopSceneButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._stopSceneButton.Enabled = false;
            this._stopSceneButton.Location = new System.Drawing.Point(285, 446);
            this._stopSceneButton.Name = "_stopSceneButton";
            this._stopSceneButton.Size = new System.Drawing.Size(76, 23);
            this._stopSceneButton.TabIndex = 204;
            this._stopSceneButton.Text = "Стоп";
            this._stopSceneButton.UseVisualStyleBackColor = true;
            this._stopSceneButton.Visible = false;
            this._stopSceneButton.Click += new System.EventHandler(this._stopSceneButton_Click);
            // 
            // _showSceneParamButton
            // 
            this._showSceneParamButton.Location = new System.Drawing.Point(195, 388);
            this._showSceneParamButton.Name = "_showSceneParamButton";
            this._showSceneParamButton.Size = new System.Drawing.Size(166, 23);
            this._showSceneParamButton.TabIndex = 203;
            this._showSceneParamButton.Text = "Посмотреть сценарий";
            this._showSceneParamButton.UseVisualStyleBackColor = true;
            this._showSceneParamButton.Visible = false;
            this._showSceneParamButton.Click += new System.EventHandler(this._showSceneParamButton_Click);
            // 
            // _deleteSceneButton
            // 
            this._deleteSceneButton.Location = new System.Drawing.Point(195, 417);
            this._deleteSceneButton.Name = "_deleteSceneButton";
            this._deleteSceneButton.Size = new System.Drawing.Size(166, 23);
            this._deleteSceneButton.TabIndex = 202;
            this._deleteSceneButton.Text = "Удалить сценарий";
            this._deleteSceneButton.UseVisualStyleBackColor = true;
            this._deleteSceneButton.Visible = false;
            this._deleteSceneButton.Click += new System.EventHandler(this._deleteSceneButton_Click);
            // 
            // _startSceneButton
            // 
            this._startSceneButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._startSceneButton.Enabled = false;
            this._startSceneButton.Location = new System.Drawing.Point(195, 446);
            this._startSceneButton.Name = "_startSceneButton";
            this._startSceneButton.Size = new System.Drawing.Size(74, 23);
            this._startSceneButton.TabIndex = 201;
            this._startSceneButton.Text = "Старт";
            this._startSceneButton.UseVisualStyleBackColor = true;
            this._startSceneButton.Visible = false;
            this._startSceneButton.Click += new System.EventHandler(this._startSceneButton_Click);
            // 
            // _scenesLabel
            // 
            this._scenesLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this._scenesLabel.AutoSize = true;
            this._scenesLabel.Location = new System.Drawing.Point(6, 392);
            this._scenesLabel.Name = "_scenesLabel";
            this._scenesLabel.Size = new System.Drawing.Size(56, 13);
            this._scenesLabel.TabIndex = 200;
            this._scenesLabel.Text = "Сценарий";
            this._scenesLabel.Visible = false;
            // 
            // _sceneComboBox
            // 
            this._sceneComboBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this._sceneComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sceneComboBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._sceneComboBox.FormattingEnabled = true;
            this._sceneComboBox.Location = new System.Drawing.Point(68, 389);
            this._sceneComboBox.Name = "_sceneComboBox";
            this._sceneComboBox.Size = new System.Drawing.Size(121, 21);
            this._sceneComboBox.TabIndex = 199;
            this._sceneComboBox.Visible = false;
            this._sceneComboBox.SelectedIndexChanged += new System.EventHandler(this._sceneComboBox_SelectedIndexChanged);
            // 
            // _addSceneEmulButton
            // 
            this._addSceneEmulButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._addSceneEmulButton.Location = new System.Drawing.Point(6, 446);
            this._addSceneEmulButton.Name = "_addSceneEmulButton";
            this._addSceneEmulButton.Size = new System.Drawing.Size(122, 23);
            this._addSceneEmulButton.TabIndex = 88;
            this._addSceneEmulButton.Text = "Добавить сценарий";
            this._addSceneEmulButton.UseVisualStyleBackColor = true;
            this._addSceneEmulButton.Click += new System.EventHandler(this._addSceneEmulButton_Click);
            // 
            // groupBoxInputDiskrets
            // 
            this.groupBoxInputDiskrets.Controls.Add(this.panelDiscrets);
            this.groupBoxInputDiskrets.Location = new System.Drawing.Point(379, 15);
            this.groupBoxInputDiskrets.Name = "groupBoxInputDiskrets";
            this.groupBoxInputDiskrets.Size = new System.Drawing.Size(152, 454);
            this.groupBoxInputDiskrets.TabIndex = 65;
            this.groupBoxInputDiskrets.TabStop = false;
            this.groupBoxInputDiskrets.Text = "Дискреты";
            // 
            // panelDiscrets
            // 
            this.panelDiscrets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDiscrets.Location = new System.Drawing.Point(3, 16);
            this.panelDiscrets.Name = "panelDiscrets";
            this.panelDiscrets.Size = new System.Drawing.Size(146, 435);
            this.panelDiscrets.TabIndex = 42;
            // 
            // groupBoxI
            // 
            this.groupBoxI.AutoSize = true;
            this.groupBoxI.Controls.Add(this.panelI);
            this.groupBoxI.Location = new System.Drawing.Point(6, 15);
            this.groupBoxI.Name = "groupBoxI";
            this.groupBoxI.Size = new System.Drawing.Size(367, 128);
            this.groupBoxI.TabIndex = 198;
            this.groupBoxI.TabStop = false;
            this.groupBoxI.Text = "Токи";
            // 
            // panelI
            // 
            this.panelI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelI.Location = new System.Drawing.Point(3, 16);
            this.panelI.Name = "panelI";
            this.panelI.Size = new System.Drawing.Size(361, 109);
            this.panelI.TabIndex = 0;
            // 
            // groupBoxU
            // 
            this.groupBoxU.AutoSize = true;
            this.groupBoxU.Controls.Add(this.panelU);
            this.groupBoxU.Location = new System.Drawing.Point(6, 149);
            this.groupBoxU.Name = "groupBoxU";
            this.groupBoxU.Size = new System.Drawing.Size(367, 128);
            this.groupBoxU.TabIndex = 197;
            this.groupBoxU.TabStop = false;
            this.groupBoxU.Text = "Напряжения";
            // 
            // panelU
            // 
            this.panelU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelU.Location = new System.Drawing.Point(3, 16);
            this.panelU.Name = "panelU";
            this.panelU.Size = new System.Drawing.Size(361, 109);
            this.panelU.TabIndex = 0;
            // 
            // _time
            // 
            this._time.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._time.AutoSize = true;
            this._time.Location = new System.Drawing.Point(344, 494);
            this._time.Name = "_time";
            this._time.Size = new System.Drawing.Size(0, 13);
            this._time.TabIndex = 79;
            this._time.TextChanged += new System.EventHandler(this._time_TextChanged);
            // 
            // _labelStatus
            // 
            this._labelStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._labelStatus.AutoSize = true;
            this._labelStatus.Location = new System.Drawing.Point(32, 569);
            this._labelStatus.Name = "_labelStatus";
            this._labelStatus.Size = new System.Drawing.Size(136, 13);
            this._labelStatus.TabIndex = 81;
            this._labelStatus.Text = "Нет связи с устройством";
            // 
            // _kvit
            // 
            this._kvit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._kvit.Location = new System.Drawing.Point(298, 525);
            this._kvit.Name = "_kvit";
            this._kvit.Size = new System.Drawing.Size(140, 33);
            this._kvit.TabIndex = 82;
            this._kvit.Text = "Квитировать";
            this._kvit.UseVisualStyleBackColor = true;
            this._kvit.Click += new System.EventHandler(this._kvit_Click);
            // 
            // _exitEmulation
            // 
            this._exitEmulation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._exitEmulation.AutoSize = true;
            this._exitEmulation.Location = new System.Drawing.Point(116, 507);
            this._exitEmulation.Name = "_exitEmulation";
            this._exitEmulation.Size = new System.Drawing.Size(201, 17);
            this._exitEmulation.TabIndex = 83;
            this._exitEmulation.Text = "Не выходить из режима эмуляции";
            this._exitEmulation.UseVisualStyleBackColor = true;
            this._exitEmulation.CheckedChanged += new System.EventHandler(this._exitEmulation_CheckedChanged);
            // 
            // _fromXmlBtn
            // 
            this._fromXmlBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._fromXmlBtn.Location = new System.Drawing.Point(517, 525);
            this._fromXmlBtn.Name = "_fromXmlBtn";
            this._fromXmlBtn.Size = new System.Drawing.Size(71, 33);
            this._fromXmlBtn.TabIndex = 87;
            this._fromXmlBtn.Text = "Загрузить";
            this._fromXmlBtn.UseVisualStyleBackColor = true;
            this._fromXmlBtn.Click += new System.EventHandler(this._fromXmlBtn_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(444, 525);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 33);
            this.button1.TabIndex = 86;
            this.button1.Text = "Сохранить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.saveToXmlButton_Click);
            // 
            // _statusLedControl
            // 
            this._statusLedControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._statusLedControl.Location = new System.Drawing.Point(13, 569);
            this._statusLedControl.Name = "_statusLedControl";
            this._statusLedControl.Size = new System.Drawing.Size(13, 13);
            this._statusLedControl.State = BEMN.Forms.LedState.Off;
            this._statusLedControl.TabIndex = 80;
            // 
            // EmulationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 587);
            this.Controls.Add(this._fromXmlBtn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this._exitEmulation);
            this.Controls.Add(this._kvit);
            this.Controls.Add(this._labelStatus);
            this.Controls.Add(this._statusLedControl);
            this.Controls.Add(this._time);
            this.Controls.Add(this.groupBoxInputSignal);
            this.Controls.Add(this.label34);
            this.Controls.Add(this._writeEmulButton);
            this.Controls.Add(this.label33);
            this.Controls.Add(this._getTimeGroupBox);
            this.Controls.Add(this._step);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label31);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "EmulationForm";
            this.Text = "EmulationForm";
            this.Activated += new System.EventHandler(this.EmulationForm_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EmulationForm_FormClosed);
            this.Load += new System.EventHandler(this.EmulationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._step)).EndInit();
            this._getTimeGroupBox.ResumeLayout(false);
            this._getTimeGroupBox.PerformLayout();
            this.groupBoxDiskrets.ResumeLayout(false);
            this.groupBoxUtime.ResumeLayout(false);
            this.groupBoxItime.ResumeLayout(false);
            this.groupBoxInputSignal.ResumeLayout(false);
            this.groupBoxInputSignal.PerformLayout();
            this.groupBoxInputDiskrets.ResumeLayout(false);
            this.groupBoxI.ResumeLayout(false);
            this.groupBoxU.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox _startTime;
        private System.Windows.Forms.ComboBox _signal;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.MaskedTextBox _status;
        private System.Windows.Forms.NumericUpDown _step;
        private System.Windows.Forms.GroupBox _getTimeGroupBox;
        private System.Windows.Forms.GroupBox groupBoxDiskrets;
        private System.Windows.Forms.CheckBox _writeEmulButton;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.GroupBox groupBoxInputSignal;
        private System.Windows.Forms.GroupBox groupBoxU;
        private System.Windows.Forms.GroupBox groupBoxInputDiskrets;
        private System.Windows.Forms.GroupBox groupBoxI;
        private System.Windows.Forms.Label _timeSignal;
        private System.Windows.Forms.Label _time;
        private Forms.LedControl _statusLedControl;
        private System.Windows.Forms.Label _labelStatus;
        private System.Windows.Forms.Button _kvit;
        private System.Windows.Forms.ToolTip kvitTooltip;
        private System.Windows.Forms.CheckBox _exitEmulation;
        private System.Windows.Forms.Panel panelI;
        private System.Windows.Forms.Panel panelU;
        private System.Windows.Forms.Panel panelDiscrets;
        private System.Windows.Forms.Panel panelDiskretsTime;
        private System.Windows.Forms.GroupBox groupBoxUtime;
        private System.Windows.Forms.Panel panelUCheck;
        private System.Windows.Forms.GroupBox groupBoxItime;
        private System.Windows.Forms.Panel panelICheck;
        private System.Windows.Forms.OpenFileDialog _fileProcessDialog;
        private System.Windows.Forms.Button _fromXmlBtn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button _addSceneEmulButton;
        private System.Windows.Forms.Button _startSceneButton;
        private System.Windows.Forms.Label _scenesLabel;
        private System.Windows.Forms.ComboBox _sceneComboBox;
        private System.Windows.Forms.Button _deleteSceneButton;
        private System.Windows.Forms.Button _showSceneParamButton;
        private System.Windows.Forms.Button _stopSceneButton;
    }
}