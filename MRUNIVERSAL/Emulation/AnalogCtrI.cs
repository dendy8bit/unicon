﻿using System.Windows.Forms;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.MRUNIVERSAL.Emulation.Structures;

namespace BEMN.MRUNIVERSAL.Emulation
{
    public partial class AnalogCtrI : UserControl
    {   
        
        public Button[] UpButtons { get; }
        public Button[] DownButtons { get; }
        public MaskedTextBox[] TextBoxes { get; }
        public Label[] Labels { get; }

        public AnalogCtrI() : this("I", "f", "F")
        {

        }

        private NewStructValidator<InputAnalogDataI> _inputAnalogDataIValidation;

        public AnalogCtrI(string analogName, string fazaName, string freqName)
        {
            InitializeComponent();

            _amplitudeLabel.Text = analogName;
            _fazaLabel.Text = fazaName;
            _frequencyLabel.Text = freqName;

            this._inputAnalogDataIValidation = new NewStructValidator<InputAnalogDataI>
            (this.toolTip,
                new ControlInfoText(this.IMTB, new CustomDoubleRule(0, 200)),
                new ControlInfoText(this.fazaMTB, new CustomDoubleRule(0, 359)),
                new ControlInfoText(this.FMTBA, new CustomDoubleRule(0, 500))
            );

            FMTBA.Text = 50.ToString();

            UpButtons = new Button[]
            {
                this._amplitudeUp, this._fazaUp, this._frequencyUp
            };
            DownButtons = new Button[]
            {
                this._amplitudeDown, this._fazaDown, this._frequencyDown
            };
            TextBoxes = new MaskedTextBox[]
            {
                this.IMTB, this.fazaMTB, this.FMTBA
            };
            Labels = new Label[]
            {
                this._amplitudeLabel, this._fazaLabel, this._frequencyLabel
            };
        }

        public InputAnalogDataI GetDataI()
        {
            return _inputAnalogDataIValidation.Get();
        }

        public void SetDataI(InputAnalogDataI data)
        {
            _inputAnalogDataIValidation.Set(data);
        }

    }
    
}
