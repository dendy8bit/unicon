using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using SchemeEditorSystem.BlockS;

namespace SchemeEditorSystem.LineS
{
    public class Connection
    {
        #region Variable declaration

        private List<LnkPoint> _dotList;
        private int _allign;
        private float _scale;
        //compile data
        private bool _error;

        private bool _checkLine;

        public bool Check { get; private set; }
        #endregion

        /// <summary>
        /// ���������� ������ ���� ����� ����������
        /// </summary>
        public List<LnkPoint> LnkPointList
        {
            get { return this._dotList; }
        }
        /// <summary>
        /// ����, ��� ����� �� ����� ������������ �����
        /// </summary>
        public bool IsCorrectLine { get; set; }

        #region Init methods
        public Connection(int inpAllign, float inpScale,string inpVarName)
        {
            this._dotList = new List<LnkPoint>();
            this._allign = inpAllign;
            this._scale = inpScale;
            this.VarName = inpVarName;
        }

        public Connection(LnkPoint first, int inpAllign, float inpScale, string inpVarName)
        {
            this._dotList = new List<LnkPoint>();
            this._dotList.Add(first);
            this._allign = inpAllign;
            this._scale = inpScale;
            this.VarName = inpVarName;
            this._dotList[0].LastPoint = true;
        }
        
        //Probably will be emproved
        public void DrawAddPointPhantom(System.Drawing.Graphics gr, int x, int y)
        {
            gr.DrawLine(System.Drawing.Pens.Blue,
                       x*this._scale - 3,
                        y *this._scale - 3,
                         x *this._scale + 3,
                          y *this._scale + 3);
            gr.DrawLine(System.Drawing.Pens.Blue,
                        x *this._scale + 3,
                         y *this._scale - 3,
                          x *this._scale - 3,
                           y *this._scale + 3);

        }

        public void AddNextPoint(int x, int y,bool addingPoint = true)
        {
            if (this.CheckForLoop(x, y)) return;
            this.AddNextDot(x, y, addingPoint);
        }

        public void AddNextDot(int x, int y, bool addingPoint, bool auto = true)
        {
            if (this._dotList.Count != 0)
            {
                LnkPoint lastDot = this._dotList.FindLast(d => d.LastPoint);
                LnkPoint autoDot = auto ? this.AddAutoPoints(x, y, lastDot) : null;
                LnkPoint newDot = autoDot == null ? lastDot.CreateNearDot(x, y) : autoDot.CreateNearDot(x, y);
                lastDot.LastPoint = false;
                newDot.LastPoint = true;
                newDot.AddingPoint = addingPoint;
                this._dotList.Add(newDot);
            }
            else
            {
                LnkPoint startDot = new LnkPoint(x, y, this._allign, this._scale);
                startDot.AddingPoint = addingPoint;
                startDot.LastPoint = true;
                this._dotList.Add(startDot);
            }
        }

        private LnkPoint AddAutoPoints(int x, int y, LnkPoint lastDot)
        {
            //���� ����� �� ����� ����� � ����������, �� �� ��������� �������������
            if ((Math.Abs(lastDot.X - x) <= this._allign / 2) || (Math.Abs(lastDot.Y - y) <= this._allign / 2)) return null;
            LnkPoint autoDot;
            if ((this._dotList.Count > 1) && (lastDot.Y == this._dotList[this._dotList.Count - 2].Y))
            {
                autoDot = lastDot.CreateNearDot(lastDot.X, y);
            }
            else
            {
                autoDot = lastDot.CreateNearDot(x, lastDot.Y);
            }
            autoDot.AddingPoint = false;
            autoDot.LastPoint = false;
            this._dotList.Add(autoDot);
            return autoDot;
        }

        #endregion

        public void ResetProcessed()
        {
            foreach (LnkPoint i in this._dotList)
            {
                i.ResetProsessed();
            }
        }

        public void SetScale(float scale)
        {
            this._scale = scale;
            foreach (LnkPoint i in this._dotList)
            {
                i.Scale  = scale;
            }
        }

        public bool IsOnSegment(int X, int Y)
        {
            this.ResetProcessed();
            var lastDot = this._dotList.FindLast(d => d.LastPoint);
            return lastDot.IsOnSegment(X, Y);
        }

        public bool IsOnLine(int X, int Y)
        {
            if (this.IsOnSegment(X, Y)) return true;
            foreach(LnkPoint i in this._dotList)
            {
                if (i.IsOnPoint(X, Y)) return true;
            }
            return false;
        }

        public bool SelSegment(int X, int Y)
        {
            this.ResetProcessed();
            var lastDot = this._dotList.FindLast(d => d.LastPoint);
            return lastDot.SelSegment(X, Y);
        }

        /// <summary>
        /// ������� ������������� ��� ������������ �����
        /// </summary>
        public void CorrectPointsOfLine()
        {
            if (this.IsCorrectLine) return;
            bool find = false;
            LnkPoint removedPoint = null;
            foreach (LnkPoint lnkPoint1 in this.LnkPointList.Where(lnkPoint1 => !lnkPoint1.IsCorrect))
            {
                foreach (LnkPoint lnkPoint2 in this.LnkPointList
                    .Where(lnkPoint2 => lnkPoint2!=lnkPoint1)
                    .Where(lnkPoint2 => lnkPoint2.X == lnkPoint1.X && lnkPoint2.Y == lnkPoint1.Y
                                        && lnkPoint1.NearDotsList.Contains(lnkPoint2) 
                                        && lnkPoint2.NearDotsList.Contains(lnkPoint1)))
                {
                    if (lnkPoint1 is Pin && !(lnkPoint2 is Pin))
                    {
                        this.ReplaceTwoOnOneDot(lnkPoint1, lnkPoint2);
                        removedPoint = lnkPoint2;
                        find = true;
                    }
                    else if (lnkPoint2 is Pin && !(lnkPoint1 is Pin))
                    {
                        this.ReplaceTwoOnOneDot(lnkPoint2, lnkPoint1);
                        removedPoint = lnkPoint1;
                        find = true;
                    }
                    else if(lnkPoint1 is Pin && lnkPoint2 is Pin)
                    {
                        Pin pin1 = lnkPoint1 as Pin;
                        Pin pin2 = lnkPoint2 as Pin;
                        if (pin1.Orientation == pin2.Orientation)
                        {
                            this.ReplaceTwoOnOneDot(lnkPoint1, lnkPoint2);
                            removedPoint = lnkPoint2;
                            find = true;
                        }
                    }
                    break;
                }
                if (find)
                {
                    break;
                }
                lnkPoint1.IsCorrect = true;
            }
            if (find)
            {
                this.LnkPointList.Remove(removedPoint);
            }
            this.IsCorrectLine = true;
        }

        private void ReplaceTwoOnOneDot(LnkPoint point1, LnkPoint point2)
        {
            point1.NearDotsList.Remove(point2);
            point2.NearDotsList.Remove(point1);
            point1.NearDotsList.AddRange(point2.NearDotsList);
            foreach (var npoint in point2.NearDotsList)
            {
                npoint.NearDotsList.Remove(point2);
                npoint.NearDotsList.Add(point1);
            }
        }
        
        /// <summary>
        /// ���������� 2 ���������� � 1 � ���������� ���� ���������� ��������
        /// </summary>
        /// <param name="connect">�����, � ������� ������ ������� ������������</param>
        /// <returns>����</returns>
        public bool UnionConnectionIfPossible(Connection connect)
        {
            if (this.FindOverlapPoints(connect))
            {
                return true;
            }

            LnkPoint unionPoint = connect.LnkPointList[0]; // �������� ������ ����� � �����
            if (this.TryUnionConnections(connect, unionPoint)) return true;

            unionPoint = connect.LnkPointList[connect.LnkPointList.Count - 1]; // �������� ��������� ���� � �����
            return this.TryUnionConnections(connect, unionPoint);
        }

        /// <summary>
        /// ����� � ����������� ����� � ����������� ������������
        /// </summary>
        /// <param name="connect"></param>
        /// <returns></returns>
        private bool FindOverlapPoints(Connection connect)
        {
            foreach (LnkPoint point in this.LnkPointList)
            {
                foreach (LnkPoint connPoint in connect.LnkPointList.Where(connPoint => connPoint.X == point.X && connPoint.Y == point.Y))
                {
                    point.UnionOverlapPoints(connPoint);
                    foreach (var item in connect._dotList)
                    {
                        item.LastPoint = false;
                    }
                    this.RefreshList();
                    return true;
                }
            }
            return false;
        }

        private bool TryUnionConnections(Connection connect, LnkPoint unionPoint)
        {
            this.ResetProcessed();
            LnkPoint lastDot = this._dotList.FirstOrDefault(d => d.LastPoint);
            if (lastDot == null) return false;
            if (lastDot.UnionLineIfPossible(unionPoint))
            {
                foreach (var item in connect._dotList)
                {
                    item.LastPoint = false;
                }
                this.RefreshList();
                return true;
            }
            return false;
        }
        
        private bool CheckForLoop(int x, int y)
        {
            x = (int)((this._allign * Math.Round((double)(x) /this._allign)));
            y = (int)((this._allign * Math.Round((double)(y) /this._allign)));
            foreach (LnkPoint point1 in this._dotList)
            {
                if (point1.X == x && point1.Y == y)
                {
                    return true;
                }
                foreach (var point2 in this._dotList)
                {
                    if (point1 == point2) continue;
                    if (this.IsOnLine(point1.X,point1.Y,point2.X, point2.Y, x, y)) return true;
                }
            }
            return false;
        }

        private bool IsOnLine(int xp1,int yp1, int xp2, int yp2, int x, int y)
        {
            if (xp1 == x && xp2 == x)
            {
                int max = Math.Max(yp1, yp2);
                int min = Math.Min(yp1, yp2);
                if (max - y > 0 && min - y < 0) return true;
            }
            if (yp1 == y && yp2 == y)
            {
                int max = Math.Max(xp1, xp2);
                int min = Math.Min(xp1, xp2);
                if (max - x > 0 && min - x < 0) return true;
            }
            return false;
        }

        public void CheckedPoint(int x, int y, int sizeX, int sizeY)
        {
            Check = true;
            foreach (LnkPoint point in _dotList)
            {
                if (!point.Selected) continue; 
                point.CheckOutOfRangePoint(x, y, sizeX, sizeY, out _checkLine);
                if (!_checkLine)
                {
                    Check = false;
                }
            }
        }
        /// <summary>
        /// ���������� ���������� �����
        /// </summary>
        /// <param name="x">����������� �� �</param>
        /// <param name="y">����������� �� �</param>
        public void MoveSelected(int x, int y, int sizeX, int sizeY)
        {
            if (Math.Abs(x) < (double)this._allign/2 && Math.Abs(y) < (double)this._allign/2) return;
            var autoPoints = new List<LnkPoint>();
            foreach (LnkPoint point in this._dotList)
            {
                if (!point.Selected || (point is Pin && (point as Pin).Moved)) continue;
                if (point is Pin)
                {
                    var autoPoint = new LnkPoint(0,0,0,0);
                    if (x < 0 || y < 0)
                    {
                        autoPoint = new LnkPoint(point.X, point.Y, this._allign, this._scale)
                        {
                            AddingPoint = false,
                            LastPoint = false,
                            Selected = true
                        };
                    }
                    else
                    {
                        autoPoint = new LnkPoint(point.X + x, point.Y + y, this._allign, this._scale)
                        {
                            AddingPoint = false,
                            LastPoint = false,
                            Selected = true
                        };
                    }
                    
                    autoPoint.NearDotsList.AddRange(point.NearDotsList);
                    point.NearDotsList.Clear();
                    point.NearDotsList.Add(autoPoint);
                    point.Selected = false;
                    foreach (var nearPoint in autoPoint.NearDotsList)
                    {
                        nearPoint.NearDotsList.Add(autoPoint);
                        nearPoint.NearDotsList.Remove(point);
                    }
                    autoPoint.NearDotsList.Add(point);
                    autoPoints.Add(autoPoint);
                }
                else
                {
                    point.Move(x, y, sizeX, sizeY);
                }
            }
            this._dotList.AddRange(autoPoints);
        }

        public void AreaSelection(int ltX, int ltY, int rlX, int rlY)
        {
            foreach (LnkPoint i in this._dotList)
            {
                i.Selected = i.IsInArea(ltX, ltY, rlX, rlY);
            }
        }
        
        /// <summary>
        /// ������� ���������� �����, ����� ����� ��� �����
        /// </summary>
        public List<Connection> DeleteSelected()
        {
            var selectedPoints = this._dotList.Where(d => d.Selected).Select(d => d).ToList();
            if (selectedPoints.Count == 1)
            {
                return this.DeleteOnePoint(selectedPoints[0]);             
            }
            if(selectedPoints.Count > 1)
            {
                return this.DeleteTwoAndMorePoints(selectedPoints);
            }
            return new List<Connection>();
        }

        private List<Connection> DeleteOnePoint(LnkPoint delPoint)
        {
            List<Connection> retList = new List<Connection>();
            foreach (var nearPoint in delPoint.NearDotsList)
            {
                nearPoint.NearDotsList.Remove(delPoint);
                Connection newCon = new Connection(nearPoint, this._allign, this._scale, "v");
                newCon.RefreshList();
                if (newCon.LnkPointList.Count >= 2) retList.Add(newCon);
            }
            delPoint.Selected = false;
            delPoint.NearDotsList.Clear();
            return retList;
        }

        private List<Connection> DeleteTwoAndMorePoints(List<LnkPoint> selectedPoints)
        {
            List<Connection> retList = new List<Connection>();
            foreach (var selectedPoint in selectedPoints)
            {
                if (selectedPoint.NearDotsList.Count(p => p.Selected) != 1)
                {
                    continue;
                }
                var nearSelectedPoint = selectedPoint.NearDotsList.First(p => p.Selected);
                selectedPoint.NearDotsList.Remove(nearSelectedPoint);
                nearSelectedPoint.NearDotsList.Remove(selectedPoint);
                // ��������� �����, �� ������� � ��� ���������, ����� ��������� ���������� �����
                // �� ������ ���������� ��������� ���������� �����
                LnkPoint startPoint = new LnkPoint(selectedPoint.X,selectedPoint.Y, this._allign, this._scale)
                {
                    Selected = false
                };
                startPoint.NearDotsList.AddRange(selectedPoint.NearDotsList);
                foreach (var point in startPoint.NearDotsList)
                {
                    point.AddNearDot(startPoint);
                    point.NearDotsList.Remove(selectedPoint);
                }
                selectedPoint.NearDotsList.Clear();
                Connection connection = new Connection(startPoint, this._allign, this._scale, this.VarName);
                connection.RefreshList();
                var connections = connection.DeleteSelected();
                if (connections.Count != 0)
                {
                    retList.AddRange(connections.Where(c => c.LnkPointList.Count > 1).Select(c => c));
                }
                else if(connection.LnkPointList.Count > 1)
                {
                    retList.Add(connection);
                }
            }
            return retList;
        }
        
        public void SetSelected(bool inpSel)
        {
            foreach(LnkPoint i in this._dotList)
            {
                i.Selected = inpSel;
            }
        }

        public bool IsSelected(int x, int y)
        {
            List<LnkPoint> selectedPoints = this._dotList.Where(d => d.Selected).ToList();
            return selectedPoints.Any(p => p.IsOnSegment(x, y));
        }

        public bool IsSelected()
        {
            return this._dotList.Any(i => i.Selected);
        }

        public bool Select(int x, int y)
        {
            this.SetSelected(false);
            LnkPoint point = this._dotList.FirstOrDefault(p => p.IsOnPoint(x, y));
            if (point != null)
            {
                point.Selected = true;
                return true;
            }

            if (this.SelSegment(x, y))
            {
                return true;
            }
            
            return false;
        }

        /// <summary>
        /// ������������� ����� ���������
        /// </summary>
        public bool Error
        {
            get { return this._error; }
            set { this._error = value; }
        }

        //filling list from LnkPoints (load method)
        private void RefreshList()
        {
            this.ResetProcessed();
            var lastDot = this._dotList.FirstOrDefault(d => d.LastPoint);
            if (lastDot == null) return;
            this._dotList.Clear();
            lastDot.AddToList(this._dotList);
        }
        /// <summary>
        /// �������� ������������� �����
        /// </summary>
        public void RemoveDublicatePoint()
        {
            this.ResetProcessed();
            List<LnkPoint> dublicatePoints = new List<LnkPoint>();
            while (this.FindDublicate(dublicatePoints))
            {
                dublicatePoints[0].NearDotsList.Remove(dublicatePoints[1]);
                if (dublicatePoints[0].NearDotsList.Count > 0)      //���� � ��������� ����� ���� ������
                {
                    foreach (LnkPoint near in dublicatePoints[0].NearDotsList) //� ���� ������� ��������� ������� �� �� �������
                    {
                        near.NearDotsList.Remove(dublicatePoints[0]);
                        if (near.NearDotsList.Contains(dublicatePoints[1])) continue;
                        near.NearDotsList.Add(dublicatePoints[1]); //��������� � ������ ������ �����
                    }

                    foreach (LnkPoint lnkPoint in dublicatePoints[0].NearDotsList)//�� ������ ����� ��������� ������ �������
                    {
                        if (dublicatePoints[1].NearDotsList.Contains(lnkPoint)) continue;
                        dublicatePoints[1].NearDotsList.Add(lnkPoint);
                    }
                }
                dublicatePoints[1].NearDotsList.Remove(dublicatePoints[0]);
                this._dotList.Remove(dublicatePoints[0]);
            }
        }

        private bool FindDublicate(List<LnkPoint> dublicatePoints)
        {
            dublicatePoints.Clear();
            foreach (var lnkPoint in this._dotList) // ���������� ������ ������������� �����
            {
                var dublicate = this._dotList.FirstOrDefault(p => p.X == lnkPoint.X && p.Y == lnkPoint.Y && p!=lnkPoint);
                if (dublicate != null)
                {
                    dublicatePoints.Add(dublicate);
                    dublicatePoints.Add(lnkPoint);
                    return true;
                }
            }
            return false;
        }

        #region Drawing methods

        public void DrawDebug(System.Drawing.Graphics gr)
        {
            this.ResetProcessed();
            if (this._dotList.Count != 0)
            {
                var lastDot = this._dotList.FirstOrDefault(d => d.LastPoint);
                if (lastDot == null) return;
                lastDot.DrawDebug(gr, this.VarValue);
            }
        }

        public void Draw(System.Drawing.Graphics gr)
        {
            this.ResetProcessed();
            if (this._dotList.Count != 0)
            {
                var lastDot = this._dotList.FirstOrDefault(d => d.LastPoint);
                if (lastDot == null) return;
                lastDot.Draw(gr, this._error);
            } 
        }

        public void DrawTransparent(System.Drawing.Graphics gr)
        {
            this.ResetProcessed();
            if (this._dotList.Count != 0)
            {
                var lastDot = this._dotList.FirstOrDefault(d => d.LastPoint);
                if (lastDot == null) return;
                lastDot.DrawTransparent(gr);
            }
        }

        public void DrawTransparentSelOnly(System.Drawing.Graphics gr)
        {
            this.ResetProcessed();
            if (this._dotList.Count != 0)
            {
                var lastDot = this._dotList.FirstOrDefault(d => d.LastPoint);
                if (lastDot == null) return;
                lastDot.DrawTransparentSelOnly(gr);
            }
        }

        public void DrawOnePhantom(System.Drawing.Graphics gr, int relX, int relY)
        {
            this.ResetProcessed();
            var lastDot = this._dotList.FirstOrDefault(d => d.LastPoint);
            if (lastDot == null) return;
            lastDot.DrawPhantom(gr, relX, relY);
        }

        public void DrawPhantom(System.Drawing.Graphics gr, int relX, int relY)
        {
            this.ResetProcessed();
            var lastDot = this._dotList.FirstOrDefault(d => d.LastPoint);
            if (lastDot == null) return;
            lastDot.DrawPhantomSelOnly(gr, relX, relY);
        }

        #endregion

        #region Compile Members

        /// <summary>
        /// �������� ���������� ����������
        /// </summary>
        public string VarName
        {
            get; set;
        }
        /// <summary>
        /// �������� ���������� ����������(�������)
        /// </summary>
        public int VarValue
        {
            get; set;
        }
        #endregion

        #region save / load

        public void PasteFromXML(XmlTextReader reader)
        {
            reader.Read();//entering  FirstDot
            var startDot = new LnkPoint(Convert.ToInt32(reader.GetAttribute("posX")) + this._allign * 2,
                    Convert.ToInt32(reader.GetAttribute("posY")) + this._allign * 2, this._allign, this._scale);
            this._dotList.Add(startDot);
            startDot.PasteFromXML(reader);

            this._dotList.Clear();
            startDot.AddToListNegative(this._dotList);
            reader.Read();
        }

        public void ExportToXML(XmlTextWriter writer)
        {
            this.ResetProcessed();
            writer.WriteStartElement("Connection");
            writer.WriteAttributeString("varName", this.VarName);
            var lastDot = this._dotList.FindLast(p=>p.NearDotsList.Count == 1); // ������� ��������� ����� � ������ ��� ���������
            lastDot.LastPoint = true;
            lastDot.ExportToXMLAsStartDot(writer);
            writer.WriteEndElement();
        }

        public void ImportFromXML(XmlTextReader reader)
        {
            reader.Read();//entering  FirstDot
            LnkPoint startDot = new LnkPoint(Convert.ToInt32(reader.GetAttribute("posX")),
                    Convert.ToInt32(reader.GetAttribute("posY")), this._allign, this._scale)
            {
                LastPoint = true, 
                AddingPoint = Convert.ToBoolean(reader.GetAttribute("addingPoint"))
            };

            startDot.ImportFromXML(reader);
            this._dotList.Clear();
            startDot.AddToListNegative(this._dotList);
            reader.Read();
        }

        #endregion
    }
}
