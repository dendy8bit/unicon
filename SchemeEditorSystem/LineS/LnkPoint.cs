using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml;
using SchemeEditorSystem.BlockS;
using SchemeEditorSystem.descriptors;

namespace SchemeEditorSystem.LineS
{
    /// <summary>
    /// ����� ����� �� �����
    /// </summary>
    public class LnkPoint
    {
        protected List<LnkPoint> nearDotsList;
        protected bool processed;
        protected bool selected;
        protected bool lastPoint;
        protected bool addingPoint;
        protected int posX;
        protected int posY;
        protected int allign;
        protected float scale;

        public bool CheckOutPoint { get; private set; }

        #region Init

        //����������� ����� ����������. ���� ������������ ���������� � ���������

        public LnkPoint(int posX, int posY, int inpAllign, float inpScale)
        {
            allign = inpAllign;
            scale = inpScale;
            selected = false;
            this.SetPosition(posX, posY);
            nearDotsList = new List<LnkPoint>();
            processed = true;
            addingPoint = true;
        }
        #endregion

        #region Properties
        /// <summary>
        /// �������
        /// </summary>
        public float Scale
        {
            get { return scale; }
            set { scale = value; }
        }
        /// <summary>
        /// ���������, ������� �� ������ �����
        /// </summary>
        public bool Selected
        {
            get { return selected; }
            set { selected = value; }
        }
        /// <summary>
        /// ������� ����� �� �
        /// </summary>
        public int X
        {
            get { return posX; }
            set { posX = value; }
        }
        /// <summary>
        /// ������� ����� �� Y
        /// </summary>
        public int Y
        {
            get { return posY; }
            set { posY = value; }
        }
        /// <summary>
        /// ������ �����-�������
        /// </summary>
        public List<LnkPoint> NearDotsList
        {
            get { return nearDotsList; }
        }
        /// <summary>
        /// ����, ��� ����� �������� ��������� ������������
        /// </summary>
        public bool LastPoint
        {
            get { return lastPoint; }
            set { lastPoint = value; }
        }
        /// <summary>
        /// ���������, ����� �� ����� ������������ �� ������ �����
        /// </summary>
        public bool AddingPoint
        {
            get { return addingPoint; }
            set { addingPoint = value; }
        }
        /// <summary>
        /// ���� ����, ��� ����� ����� ���������� ���������
        /// </summary>
        public bool IsCorrect { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// ��������� �������� ����� � ������ � �������
        /// </summary>
        /// <param name="added">�������� �����</param>
        public void AddNearDot(LnkPoint added)
        {
            nearDotsList.Add(added);
        }
        /// <summary>
        /// ������� "������" � ������ �����
        /// </summary>
        /// <param name="x">���������� �������� ����� �� X</param>
        /// <param name="y">���������� �������� ����� �� Y</param>
        /// <param name="adding">�������� ����������� ����������� ������������� ����� ��� �����</param>
        /// <returns>����� �������� �����</returns>
        public LnkPoint CreateNearDot(int x, int y, bool adding = true)
        {
            LnkPoint newPoint = new LnkPoint(x, y, allign, scale){AddingPoint = adding};
            newPoint.AddNearDot(this);
            nearDotsList.Add(newPoint);
            return newPoint;
        }

        private int AllignNumber(int num)
        {
            return (int)((allign * Math.Round((double)(num) / (double)(allign))));
        }
        /// <summary>
        /// ������������� ��������� �����
        /// </summary>
        /// <param name="x">���������� �� �</param>
        /// <param name="y">���������� �� Y</param>
        public void SetPosition(int x, int y)
        {
            posX = AllignNumber(x);
            posY = AllignNumber(y);
        }
        /// <summary>
        /// ������������ �����
        /// </summary>
        /// <param name="x">X-����������</param>
        /// <param name="y">Y-����������</param>
        public void Move(int x, int y, int sizeX, int sizeY)
        {
            //int dX = posX;
            //int dY = posY;

            posX += AllignNumber(x);
            posY += AllignNumber(y);

            //if (posY >= sizeY - 15 || posX >= sizeX - 30 || posY <= 0 || posX <= 0)
            //{
            //    posX = dX;
            //    posY = dY;
            //}
            
        }

        public void CheckOutOfRangePoint(int x, int y, int sizeX, int sizeY, out bool check)
        {
            int dX = posX;
            int dY = posY;

            dX += AllignNumber(x);
            dY += AllignNumber(y);

            if (dY >= sizeY || dX >= sizeX - 30 || dY < 5 || dX < 5)
            {
                check = false;
            }
            else
            {
                check = true;
            }
        }

        public void ResetProsessed()
        {
            if (!processed) return;
            this.processed = false;
            foreach (var nearPoint in nearDotsList)
            {
                nearPoint.ResetProsessed();
            }
        }
        /// <summary>
        /// ������� ������ �������� �����
        /// </summary>
        public void DeleteAllConnections()
        {
            foreach (var point in nearDotsList)
            {
                point.NearDotsList.Remove(this);
            }
            nearDotsList.Clear();
        }

        public bool IsInArea(int ltX, int ltY, int rlX, int rlY)
        {
            return ltX < posX && ltY < posY && rlX > posX && rlY > posY;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"> ���������� ���� �� X</param>
        /// <param name="y"> ���������� ���� �� Y</param>
        /// <returns></returns>
        public bool IsOnPoint(int x, int y)
        {
            return x > (posX - allign / 2) &&
                   y > (posY - allign / 2) &&
                   x < (posX + allign / 2) &&
                   y < (posY + allign / 2);
        }
        #endregion

        #region Logic

        public bool SelSegment(int x, int y)
        {
            if (processed) return false;
            processed = true;
            foreach (LnkPoint i in nearDotsList)
            {
                if (IsOnLine(x, y, this.X, this.Y, i.X, i.Y))
                {
                    this.Selected = true;
                    i.Selected = true;
                    return true;
                }
                if (i.SelSegment(x, y))
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsOnSegment(int x, int y)
        {
            if (!processed)
            {
                processed = true;
                foreach (LnkPoint i in nearDotsList)
                {
                    if (IsOnLine(x, y, this.X, this.Y, i.X, i.Y))
                    {
                        return true;
                    }
                    if (i.IsOnSegment(x, y))
                    {
                        return true;
                    }
                }
                return false;
            }
            return false;
        }

        public bool IsOnSelSegment(int X, int Y)
        {
            if (!processed)
            {
                processed = true;
                foreach (LnkPoint i in nearDotsList)
                {
                    if (this.Selected && i.Selected && IsOnLine((int)(X ), (int)(Y ), this.X, this.Y,
                        i.X, i.Y))
                    {
                        return true;
                    }

                    if (i.IsOnSelSegment(X, Y))
                    {
                        return true;
                    }

                }
                return false;
            }
            else
                return false;
        }

        /// <summary>
        /// ���������� ����� �� ������ � ������ ������������
        /// </summary>
        /// <param name="unionPoint">����� � ������������, ������������ � �������</param>
        /// <returns></returns>
        public bool UnionOverlapPoints(LnkPoint unionPoint)
        {
            if (this.AddingPoint || unionPoint.AddingPoint)
            {
                this.nearDotsList.AddRange(unionPoint.NearDotsList);
                foreach (LnkPoint j in unionPoint.NearDotsList)
                {
                    j.AddNearDot(this);
                }
                unionPoint.DeleteAllConnections();
                return true;
            }
            return false;
        }

        /// <summary>
        /// ����� ��������� �� ����������� ����������� ���� ����� �����
        /// </summary>
        /// <param name="unionPoint">�����, �� ������� ��������� ������� �����������</param>
        public bool UnionLineIfPossible(LnkPoint unionPoint)
        {
            if (this.processed) return false;
            this.processed = true;
            //if ((unionPoint.X == this.X) && (unionPoint.Y == this.Y))
            //{
            //    if (this.AddingPoint || unionPoint.AddingPoint)
            //    {
            //        this.nearDotsList.AddRange(unionPoint.NearDotsList);
            //        foreach (LnkPoint j in unionPoint.NearDotsList)
            //        {
            //            j.AddNearDot(this);
            //        }
            //        unionPoint.DeleteAllConnections();
            //        return true;
            //    }
            //}
            foreach (LnkPoint nearDot in this.nearDotsList)
            {
                if (IsOnLine(unionPoint.X, unionPoint.Y, this.X, this.Y, nearDot.X, nearDot.Y) && (this.AddingPoint||unionPoint.AddingPoint))
                {
                    unionPoint.AddNearDot(this);
                    unionPoint.AddNearDot(nearDot);
                    nearDot.AddNearDot(unionPoint);
                    this.NearDotsList.Add(unionPoint);
                    
                    nearDot.NearDotsList.Remove(this);
                    this.NearDotsList.Remove(nearDot);
                    return true;
                }
                if (nearDot.UnionLineIfPossible(unionPoint))
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// ����������� ������ ������ ����� �����
        /// </summary>
        /// <param name="list">������ �����</param>
        public void AddToList(List<LnkPoint> list)
        {
            if (processed) return;
            processed = true;
            list.Add(this);
            foreach (LnkPoint i in nearDotsList)
            {
                i.AddToList(list);
            }
        }

        public void AddToListNegative(List<LnkPoint> list)
        {
            if (!processed) return;
            processed = false;
            list.Add(this);
            foreach (LnkPoint i in nearDotsList)
            {
                i.AddToListNegative(list);
            }
        }
        
        public bool IsOnLine(int x, int y, int lineX1, int lineY1, int lineX2, int lineY2)
        {
            Point p1 = new Point(lineX1,lineY1);
            Point p2 = new Point(lineX2, lineY2);
            Point mp = new Point(x, y);

            Point v = p2 - (Size)p1;
            Point w = mp - (Size)p1;

            double c1 = (w.X * v.X + w.Y * v.Y);
            if (c1 <= 0) return this.Distance(mp, p1) < this.allign;

            double c2 = (v.X * v.X + v.Y * v.Y);
            if (c2 <= c1) return this.Distance(mp, p2) < this.allign;

            double b = c1 / c2;
            Point Pb = new Point((int)(p1.X + v.X * b), (int)(p1.Y + v.Y * b));

            return this.Distance(mp, Pb) < this.allign;
        }

        private double Distance(Point a, Point b)
        {
            return Math.Sqrt((a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y));
        }

        #endregion

        #region Drawing
        
        public void DrawDebug(System.Drawing.Graphics gr,int value)
        {
            InternalDraw(gr, value == 0 ? Brushes.Blue : Brushes.Red, 0, 0, true, true, 3.0F, false, null, value);
        }
        
        public void Draw(System.Drawing.Graphics gr,bool error)
        {
            InternalDraw(gr, error ? Brushes.Red : Brushes.Black, 0, 0, true, false, 1, error);
        }

        public void DrawTransparent(Graphics gr)
        {
            InternalDraw(gr, Brushes.LightSlateGray, 0, 0, true, true);
        }

        public void DrawTransparentSelOnly(Graphics gr)
        {
            InternalDraw(gr, Brushes.LightSlateGray, 0, 0);
        }

        public void DrawPhantom(Graphics gr, int relX, int relY)
        {
            InternalDraw(gr, Brushes.LightBlue, relX, relY, true, true);
        }

        public void DrawPhantomSelOnly(Graphics gr, int relX, int relY)
        {
            this.MovigDraw(gr, Brushes.LightBlue, relX, relY);
        }

        private void MovigDraw(Graphics gr, Brush color, int relX, int relY, LnkPoint inpPoint = null)
        {
            if (this.processed) return;
            this.processed = true;
            Pen pen = new Pen(color){Width = 1};
            switch (nearDotsList.Count)
            {
                case 1:
                case 2:
                    if (this.Selected)
                    {
                        gr.DrawRectangle(pen, (this.X + relX - 1)*Scale, (this.Y + relY - 1)*Scale, 2*Scale, 2*Scale);
                    }
                    if (this is Pin && this.Selected)
                    {
                        gr.FillEllipse(color, (this.X + relX - 1)*Scale, (this.Y + relY - 1)*Scale, (float) (1.3*Scale),
                            (float) (1.3*Scale));
                    }
                    break;
            }
            foreach (LnkPoint i in nearDotsList)
            {
                if (relX == 0 && relY == 0)
                {
                    if (i != inpPoint)
                        if (this.Selected && i.Selected)
                        {
                            //��������� �����
                            gr.DrawLine(System.Drawing.Pens.LightSlateGray,
                                (this.X + relX) * Scale,
                                (this.Y + relY) * Scale,
                                (i.X + relX) * Scale,
                                (i.Y + relY) * Scale);
                        }
                        else
                        {
                            gr.DrawLine(System.Drawing.Pens.Black,
                                (this.X + relX) * Scale,
                                (this.Y + relY) * Scale,
                                (i.X + relX) * Scale,
                                (i.Y + relY) * Scale);
                        }
                    if (i != null)
                        i.MovigDraw(gr, color, relX, relY, this);
                }
                else
                {
                    if (i != inpPoint)
                        if (this.Selected && i.Selected)
                        {
                            //��������� �����
                            gr.DrawLine(pen, (this.X + relX)*Scale, (this.Y + relY)*Scale, (i.X + relX)*Scale,
                                (i.Y + relY)*Scale);
                        }
                    i.MovigDraw(gr, color, relX, relY, this);
                }
            }
        }

        public void InternalDraw(Graphics gr, Brush color, int relX, int relY, bool drawAll = false,
            bool singlecolor = false, float lineWidth = 1, bool error = false, LnkPoint inpPoint = null, int value = 0)
        {
            if (this.processed) return;
            this.processed = true;
            Pen pen = new Pen(color);
            pen.Width = lineWidth;
            if (error)
            {
                pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                pen.DashOffset = allign;
            }
            Pen pendet = singlecolor ? pen : (this.Selected ? Pens.Red : Pens.Black);
            //��������� �����
            switch (nearDotsList.Count)
            {
                case 1:
                case 2:
                    if (this.Selected)
                    {
                        gr.DrawRectangle(pendet,
                            (this.X + relX - 1)*Scale,
                            (this.Y + relY - 1)*Scale, 2*Scale, 2*Scale);
                    }
                    if (this is Pin)
                    {
                        gr.FillEllipse(color,
                            (this.X + relX - 1)*Scale,
                            (this.Y + relY - 1)*Scale, (float)(1.3*Scale), (float)(1.3*Scale));
                    }
                    break;
                default:
                    if (this.Selected)
                    {
                        gr.FillEllipse(Brushes.Red,
                            (this.X + relX - 1)*Scale,
                            (this.Y + relY - 1)*Scale, 2*Scale, 2*Scale);
                    }
                    else if (relX == 0 && relY == 0)
                    {
                        gr.FillEllipse(color,
                            (this.X + relX - 1)*Scale,
                            (this.Y + relY - 1)*Scale, 2*Scale, 2*Scale);
                    }
                    break;
            }
            foreach (LnkPoint i in nearDotsList)
            {
                if (!drawAll && relX == 0 && relY == 0)
                {
                    if (i != inpPoint)
                        if ((this.Selected && i.Selected))
                        {
                            //��������� �����
                            gr.DrawLine(System.Drawing.Pens.LightSlateGray,
                                (this.X + relX)*Scale,
                                (this.Y + relY)*Scale,
                                (i.X + relX)*Scale,
                                (i.Y + relY)*Scale);
                        }
                        else
                        {
                            gr.DrawLine(System.Drawing.Pens.Black,
                                (this.X + relX)*Scale,
                                (this.Y + relY)*Scale,
                                (i.X + relX)*Scale,
                                (i.Y + relY)*Scale);
                        }
                    if (i != null)
                        i.InternalDraw(gr, color, relX, relY, false, singlecolor, lineWidth, error, this, value);
                }
                else
                {
                    if (i != inpPoint)
                        if ((this.Selected && i.Selected) || (drawAll))
                        {
                            //��������� �����
                            gr.DrawLine(i.Selected ? pendet : pen,
                                (this.X + relX)*Scale,
                                (this.Y + relY)*Scale,
                                (i.X + relX)*Scale,
                                (i.Y + relY)*Scale);
                        }

                    if (this.lastPoint && this.nearDotsList.Count == 1)
                    {
                        StringFormat tempStrFormat = new StringFormat();
                        tempStrFormat.Alignment = StringAlignment.Near;
                        gr.DrawString(value.ToString(), new Font(FontFamily.GenericSansSerif, 0.5f*allign*scale),
                            color, (this.X + relX)*Scale, (this.Y + relY)*Scale, tempStrFormat);
                    }
                    i.InternalDraw(gr, color, relX, relY, drawAll, singlecolor, lineWidth, error, this,  value);
                }
            }
        }
        #endregion

        #region save / load
        public void PasteFromXML(XmlTextReader reader)
        {
            //reader.Read();//entering LnkPoint tag
            reader.Read();//entering NearLnkPoints tag
            int count = Convert.ToInt32(reader.GetAttribute("count"));
            reader.Read();
            for (int i = 0; i < count; i++)
            {
                LnkPoint nearDot = this.CreateNearDot(Convert.ToInt32(reader.GetAttribute("posX")) + allign * 2,
                                                            Convert.ToInt32(reader.GetAttribute("posY")) + allign * 2);
                nearDot.PasteFromXML(reader);
                reader.Read();
            }

            if (count != 0) reader.Read();
        }

        public void ExportToXMLAsStartDot(XmlTextWriter writer)
        {
            if (processed) return;
            processed = true;

            writer.WriteStartElement("LnkPoint");

            writer.WriteAttributeString("posX", posX.ToString());
            writer.WriteAttributeString("posY", posY.ToString());
            writer.WriteAttributeString("addingPoint", addingPoint.ToString());

            writer.WriteStartElement("NearLnkPoints");
            int temp = nearDotsList.Count;
            writer.WriteAttributeString("count", temp.ToString());
            foreach (LnkPoint i in nearDotsList)
            {
                i.ExportToXML(writer);
            }

            writer.WriteEndElement();//end of nearLnkPoints
            writer.WriteEndElement();//end of LnkPoint
        }


        public void ExportToXML(XmlTextWriter writer)
        {
            if (!processed)
            {
                processed = true;
                writer.WriteStartElement("LnkPoint");

                writer.WriteAttributeString("posX", posX.ToString());
                writer.WriteAttributeString("posY", posY.ToString());
                writer.WriteAttributeString("addingPoint", addingPoint.ToString());
                
                writer.WriteStartElement("NearLnkPoints");
                int temp = nearDotsList.Count-1;
                writer.WriteAttributeString("count", temp.ToString());
                foreach (LnkPoint i in nearDotsList)
                {
                    i.ExportToXML(writer);
                }

                writer.WriteEndElement();//end of nearLnkPoints
                writer.WriteEndElement();//end of LnkPoint
            }
        }

        public void ImportFromXML(XmlTextReader reader)
        {
            //reader.Read();//entering LnkPoint tag
            reader.Read();//entering NearLnkPoints tag
            int count = Convert.ToInt32(reader.GetAttribute("count"));
            reader.Read();
            for (int i = 0; i < count; i++)
            {
                LnkPoint nearDot = this.CreateNearDot(Convert.ToInt32(reader.GetAttribute("posX")),
                    Convert.ToInt32(reader.GetAttribute("posY")), Convert.ToBoolean(reader.GetAttribute("addingPoint")));

                nearDot.ImportFromXML(reader);
                reader.Read();
            }

            if (count != 0) reader.Read();
        }
        
        #endregion
    }
}
