﻿using System;
using System.Runtime.Serialization;

namespace SchemeEditorSystem.compiler
{
    /// <summary>
    /// Исключение при ошибке компиляции логики
    /// </summary>
    [Serializable]
    public sealed class CompileLogicException : ApplicationException, ISerializable
    {
        private readonly string _message;

        /// <summary>
        /// Флаги ошибок
        /// </summary>
        public LogicErrorFlag ErrorFlags { get; private set; }

        /// <summary>
        /// Пояснения по ошибкам логической схемы
        /// </summary>
        public override string Message
        {
            get { return this._message; }
        }

        /// <summary>
        /// Конструктор исключения компиляции логики
        /// </summary>
        /// <param name="errorFlags">Флаги ошибок</param>
        public CompileLogicException(LogicErrorFlag errorFlags, string schemeName)
        {
            this.ErrorFlags = errorFlags;
            this._message = this.GetMessage(schemeName);
        }

        private string GetMessage(string schemeName)
        {
            string message = string.Empty;
            if ((this.ErrorFlags & LogicErrorFlag.NO_CONNECTION) != 0)
            {
                message += "нет связей на схеме";
            }
            if ((this.ErrorFlags & LogicErrorFlag.NO_INPUT) != 0)
            {
                message += "Связь не имеет входного сигнала";
            }
            if ((this.ErrorFlags & LogicErrorFlag.NO_OUTPUT) != 0)
            {
                message += message.Length == 0 ? "Связь не имеет выхода" : ", связь не имеет выхода";
            }
            if ((this.ErrorFlags & LogicErrorFlag.NO_IN_OUT) != 0)
            {
                message += message.Length == 0 ? "Имеется не подлюченная связь" : ", имеется неподлюченная связь";
            }
            if ((this.ErrorFlags & LogicErrorFlag.LOTS_OF_INPUTS) != 0)
            {
                message += message.Length == 0
                    ? "Связь имеет более одного входного сигнала"
                    : ", связь имеет более одного входного сигнала";
            }
            if ((this.ErrorFlags & LogicErrorFlag.POINT_IS_NOT_CONNECTED) != 0)
            {
                message += message.Length == 0
                    ? "Связь имеет не подключенную точку"
                    : ", ивязь имеет не подключенную точку";
            }
            if ((this.ErrorFlags & LogicErrorFlag.PIN_IS_NOT_CONNECTED) != 0)
            {
                message += message.Length == 0 ? "Имеется не подлюченный блок" : ", имеется не подлюченный блок";
            }
            if ((this.ErrorFlags & LogicErrorFlag.PIN_INTO_PIN) != 0)
            {
                message += message.Length == 0 ? "Имеются блоки соприкасающиеся друг с другом" : ", имеются блоки соприкасающиеся друг с другом";
            }
            return string.Format("На схеме \"{0}\" имеются следующие ошибки: {1}", schemeName, message);
        }
    }
}
