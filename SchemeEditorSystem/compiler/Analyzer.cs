using System.Collections.Generic;
using System.Linq;
using System.Xml;
using SchemeEditorSystem.BlockS;
using SchemeEditorSystem.descriptors;
using SchemeEditorSystem.LineS;

namespace SchemeEditorSystem.compiler
{
    public class Analyzer
    {
        private List<Connection> _lineList;
        private List<Block> _blockList;

        /// <summary>
        /// ����� ������ ���������� �����
        /// </summary>
        public LogicErrorFlag ErrorFlags { get; private set; }

        private void ResetCompData()
        {
            foreach (Connection connection in this._lineList)
            {
                connection.IsCorrectLine = false;
                foreach (LnkPoint point in connection.LnkPointList)
                {
                    point.IsCorrect = false;
                }

            }
            foreach (Block i in this._blockList)
            {
                i.ResetPinNames();
            }
        }

        public void WriteAnalysedData(XmlWriter writer)
        {
            this.SortOnPriority();
            writer.WriteStartElement("CompileData");
            writer.WriteStartElement("VariableList");
            writer.WriteAttributeString("num", this._lineList.Count.ToString());
            foreach (Connection i in this._lineList)
            {
                writer.WriteStartElement("variable");
                writer.WriteAttributeString("name", i.VarName);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteStartElement("BlockList");
            writer.WriteAttributeString("num", this._blockList.Count.ToString());
            //find the max priority
            int maxPriority = this._blockList.Max(b => b.Priority);
            for (int i = maxPriority; i > -1; i--)
            {
                foreach (Block block in this._blockList)
                {
                    if (block.Priority == i)
                    {
                        block.WriteCompileData(writer);
                    }
                }
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        private void SortOnPriority()
        {
            foreach (Block block in this._blockList)
            {
                block.ResetPriority();
            }
            List<Block> outputs =
                this._blockList.Where(b => b.PinList.Count == 1 && b.PinList[0].Orientation == PinOrientation.PO_LEFT)
                    .Select(b => b)
                    .ToList();
            foreach (Block output in outputs)
            {
                foreach (Block block in this._blockList)
                {
                    block.Analyzed = false;
                }
                this.SortOnPriority(output);
            }
        }

        private void SortOnPriority(Block block)
        {
            List<Pin> inputList =
                block.PinList.Where(p => p.Orientation == PinOrientation.PO_LEFT).Select(p => p).ToList();
            if (inputList.Count == 0) return;
            foreach (Pin input in inputList)
            {
                Connection connection = this._lineList.First(l => l.LnkPointList.Contains(input));
                Pin output =
                    connection.LnkPointList.OfType<Pin>().First(p => p.Orientation == PinOrientation.PO_RIGHT);
                Block curBlock = this._blockList.First(b => b.PinList.Contains(output));
                if (!curBlock.Analyzed)
                {
                    curBlock.Priority = input.Priority + 1;
                    curBlock.Analyzed = true;
                    this.SortOnPriority(curBlock);
                }
            }
        }

        private bool AnalyseScheme()
        {
            this.ErrorFlags = LogicErrorFlag.CORRECT;
            if (this._lineList.Count == 0) //��� �������
            {
                this.ErrorFlags |= LogicErrorFlag.NO_CONNECTION;
                return true;
            }
            foreach (Connection line in this._lineList)
            {
                bool lineError = false;
                line.CorrectPointsOfLine();
                int[] inOut = new int[2];
                foreach (Block block in this._blockList)
                {
                    block.SetVarNames(line, inOut);
                }
                if (inOut[0] == 0 && inOut[1] > 1)
                {
                    this.ErrorFlags |= LogicErrorFlag.NO_INPUT; // ��� �������� ������� (����� �� �����)
                    lineError = true;
                }
                if (inOut[0] > 1 && inOut[1] == 0)
                {
                    this.ErrorFlags |= LogicErrorFlag.NO_OUTPUT; // ��� ��������� ������� (���� �� ����)
                    lineError = true;
                }
                if (inOut[0] == 0 && inOut[1] == 0)
                {
                    this.ErrorFlags |= LogicErrorFlag.NO_IN_OUT; // ��� �� �����, �� ������
                    lineError = true;
                }
                if (inOut[0] > 1 && inOut[1] != 0)
                {
                    this.ErrorFlags |= LogicErrorFlag.LOTS_OF_INPUTS; // ������� �������� ������ ������
                    lineError = true;
                }
                if (line.LnkPointList.Any(point => point.NearDotsList.Count == 1 && !(point is Pin))) // ������� �����
                {
                    this.ErrorFlags |= LogicErrorFlag.POINT_IS_NOT_CONNECTED;
                    lineError = true;
                }
                line.Error = lineError;
            }
            foreach (Block b in this._blockList)
                b.Error = false;
            for (int i = 0; i < this._blockList.Count; i++)
            {
                for (int j = i + 1; j < this._blockList.Count; j++)
                {
                    foreach (var pnList in this._blockList[i].PinList)
                    {
                        if (this._blockList[j].PinList.Any(b2 => b2.X == pnList.X && b2.Y == pnList.Y))
                        {
                            this._blockList[j].Error = true;
                            this._blockList[i].Error = true;
                            this.ErrorFlags |= LogicErrorFlag.PIN_INTO_PIN;
                        }
                    }
                }

            }
            if (this._blockList.Any(b => b.PinList.Any(p => p.VarName == string.Empty)))
                this.ErrorFlags |= LogicErrorFlag.PIN_IS_NOT_CONNECTED; // ������� �������������� �����
            return this.ErrorFlags != LogicErrorFlag.CORRECT;
        }

        /// <summary>
        /// ����������� ����� �� ������ ������, � ��� �� ����������� ������� ���������� ����������
        /// </summary>
        /// <param name="inpLineList">������ ������</param>
        /// <param name="inpBlockList">������ ����������</param>
        /// <returns>���� ����� �� ����� ������, �� false</returns>
        public bool Analyse(List<Connection> inpLineList, List<Block> inpBlockList)
        {
            this._lineList = inpLineList;
            this._blockList = inpBlockList;
            this.ResetCompData();
            return this.AnalyseScheme();
        }
    }
}

