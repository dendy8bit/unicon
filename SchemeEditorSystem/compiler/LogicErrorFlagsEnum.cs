﻿using System;

namespace SchemeEditorSystem.compiler
{
    /// <summary>
    /// Флаги ошибок логики
    /// </summary>
    [Flags, Serializable]
    public enum LogicErrorFlag
    {
        /// <summary>
        /// Схема не имеет ошибок
        /// </summary>
        CORRECT = 0x0000,
        /// <summary>
        /// Нет связей
        /// </summary>
        NO_CONNECTION = 0x0001,
        /// <summary>
        /// Связь не имеет входного сигнала
        /// </summary>
        NO_INPUT = 0x0002,
        /// <summary>
        /// Связь не имеет выхода
        /// </summary>
        NO_OUTPUT = 0x0004,
        /// <summary>
        /// Связь не имеет входа и выхода
        /// </summary>
        NO_IN_OUT =  0x0008,
        /// <summary>
        /// В свзязи больше одного входного сигнала
        /// </summary>
        LOTS_OF_INPUTS = 0x0010,
        /// <summary>
        /// Свзязь не подключена
        /// </summary>
        POINT_IS_NOT_CONNECTED = 0x0020,
        /// <summary>
        /// Блок не подключен
        /// </summary>
        PIN_IS_NOT_CONNECTED = 0x0040,
        ///<summary>
        /// Блоки соприкасаются друг с другом
        /// </summary>
        PIN_INTO_PIN = 0x0080
    }
}
