﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SchemeEditorSystem.BlockS;
using SchemeEditorSystem.managers;

namespace SchemeEditorSystem
{
    public partial class InvalidBlockList : Form
    {
        private PropertiesManager _manager;
        private Action _redraw;
        /// <summary>
        /// Указывает, есть ли неверные блоки
        /// </summary>
        public bool BlockListIsEmpty
        {
            get { return this.listBox1.Items.Count == 0; }
        }

        public InvalidBlockList(List<Block> list, PropertiesManager manager, Action redraw)
        {
            this.InitializeComponent();
            this.FillListBox(list);
            this._manager = manager;
            this._redraw = redraw;
        }

        private void FillListBox(List<Block> list)
        {
            this.listBox1.Items.AddRange(list.ToArray());
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox listBox = (ListBox) sender;
            Block block = listBox.SelectedItem as Block;
            if (block == null) return;
            if (this._manager.OpenPropertyFormWithAnswer(block.GetDescription()))
            {
                this._redraw.Invoke();
                listBox.Items.Remove(block);
            }
        }
        /// <summary>
        /// Удаляет блок из листа
        /// </summary>
        public void RemoveBlockFromList(Block removingBlock)
        {
            if (this.listBox1.Items.Contains(removingBlock) && IsHandleCreated)
            {
                Action<Block> a = this.RemoveBlock;
                IAsyncResult res = BeginInvoke(a, removingBlock);
                res.AsyncWaitHandle.WaitOne(100);
                EndInvoke(res);
                res.AsyncWaitHandle.Close();
            }
        }

        private void RemoveBlock(Block removingBlock)
        {
            this.listBox1.Items.Remove(removingBlock);
        }
    }
}
