namespace SchemeEditorSystem
{
    partial class LibraryBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupComboBox = new System.Windows.Forms.ComboBox();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.LibListView = new System.Windows.Forms.ListView();
            this.blockPreView1 = new BlockPreView();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupComboBox
            // 
            this.GroupComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.GroupComboBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.GroupComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.GroupComboBox.FormattingEnabled = true;
            this.GroupComboBox.Items.AddRange(new object[] {
            "������� ���������� ��������",
            "��������",
            "�������",
            "��������",
            "���������"});
            this.GroupComboBox.Location = new System.Drawing.Point(0, 0);
            this.GroupComboBox.Name = "GroupComboBox";
            this.GroupComboBox.Size = new System.Drawing.Size(193, 21);
            this.GroupComboBox.TabIndex = 0;
            this.GroupComboBox.Text = "�������� ������ ���������";
            this.GroupComboBox.SelectedIndexChanged += new System.EventHandler(this.GroupComboBox_SelectedIndexChanged);
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 21);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.LibListView);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.blockPreView1);
            this.splitContainer.Size = new System.Drawing.Size(193, 587);
            this.splitContainer.SplitterDistance = 299;
            this.splitContainer.TabIndex = 1;
            // 
            // LibListView
            // 
            this.LibListView.AllowDrop = true;
            this.LibListView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.LibListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LibListView.FullRowSelect = true;
            this.LibListView.GridLines = true;
            this.LibListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.LibListView.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LibListView.Location = new System.Drawing.Point(0, 0);
            this.LibListView.MultiSelect = false;
            this.LibListView.Name = "LibListView";
            this.LibListView.Size = new System.Drawing.Size(193, 299);
            this.LibListView.TabIndex = 0;
            this.LibListView.UseCompatibleStateImageBehavior = false;
            this.LibListView.View = System.Windows.Forms.View.Details;
            this.LibListView.SelectedIndexChanged += new System.EventHandler(this.LibListView_SelectedIndexChanged);
            this.LibListView.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.LibListView_ItemDrag);
            // 
            // blockPreView1
            // 
            this.blockPreView1.AllowDrop = true;
            this.blockPreView1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.blockPreView1.BackColor = System.Drawing.Color.White;
            this.blockPreView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.blockPreView1.Cursor = System.Windows.Forms.Cursors.Cross;
            this.blockPreView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.blockPreView1.ImeMode = System.Windows.Forms.ImeMode.Close;
            this.blockPreView1.Location = new System.Drawing.Point(0, 0);
            this.blockPreView1.Name = "blockPreView1";
            this.blockPreView1.Size = new System.Drawing.Size(193, 284);
            this.blockPreView1.TabIndex = 0;
            this.blockPreView1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.blockPreView1_MouseMove);
            // 
            // LibraryBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(193, 608);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.GroupComboBox);
            this.Name = "LibraryBox";
            this.Text = "LibraryBox";
            this.Load += new System.EventHandler(this.LibraryBox_Load);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox GroupComboBox;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.ListView LibListView;
        private BlockPreView blockPreView1;
    }
}