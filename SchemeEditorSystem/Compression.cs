////////////////////////////////////////////
//          Compression.dll               //
//      CodeGate Compression Services     //
//        Developer: Sakno Roman V.       //
////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.IO;

namespace BEMN.Compressor
{
    using WORD = System.UInt16;

    /// <summary>
    /// ��������� ����������
    /// </summary>
    public interface ICompressor
    {
        byte[] Compress(byte[] bytes);
        byte[] Decompress(byte[] bytes);
        IAsyncResult BeginCompress(byte[] bytes, AsyncCallback callback);
        byte[] EndCompress(AsyncCallback callback);
        IAsyncResult BeginDecompress(Stream bytes, AsyncCallback callback);
        Stream EndDecompress(AsyncCallback callback);
        Stream Compress(Stream stream);
        Stream Decompress(Stream stream);
        string CompressorName { get;}
    }


 /// <summary>
    /// GZIP ���������
 /// </summary>
    public sealed class ZIPCompressor : ICompressor
    {
        private const string M_COMPRESSOR_NAME = "GZIP";


        /// <summary>
        /// ���������� ������ ����
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public byte[] Compress(byte[] bytes)
        {
            MemoryStream ms = new MemoryStream();
            System.IO.Compression.GZipStream zip = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Compress, true);
            zip.Write(bytes, 0, bytes.Length);
            zip.Close();
            byte[] result = new byte[ms.Length];
            ms.Seek(0, SeekOrigin.Begin);
            ms.Read(result, 0, (int)ms.Length);
            ms.Close();
            return result;
        }

        /// <summary>
        /// ������������� ������ ����
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public byte[] Decompress(byte[] bytes)
        {
            MemoryStream ms = new MemoryStream(bytes);
            Stream zip = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Decompress);
            List<byte> buffer = new List<byte>();
            int temp = -1;
            int index = 0;
            try
            {
                while ((temp = zip.ReadByte()) != -1)
                {
                    buffer.Add((byte)temp);
                    index++;
                }
            }catch
            {
                int f = 0;
            }
            ms.Close();
            zip.Close();
            return buffer.ToArray();
        }

        /// <summary>
        /// ���������� ���������� ������ ����
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public IAsyncResult BeginCompress(byte[] bytes, AsyncCallback callback)
        {
            return null;
        }
        /// <summary>
        /// ���������� ����������� ���������
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public byte[] EndCompress(AsyncCallback callback)
        {
            return null;
        }
        /// <summary>
        /// ���������� ������������� ������ ����
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public IAsyncResult BeginDecompress(Stream bytes, AsyncCallback callback)
        {
            return null;
        }
        /// <summary>
        /// ���������� ����������� ������������
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public Stream EndDecompress(AsyncCallback callback)
        {
            return null;
        }
        /// <summary>
        /// ���������� ������ �� ������
        /// </summary>
        public Stream Compress(Stream stream)
        {
            byte[] buffer=new byte[stream.Length];
            stream.Read(buffer, 0, (int)buffer.Length);
            return new MemoryStream(Compress(buffer));
        }
        /// <summary>
        /// ������������� ������ �� ������
        /// </summary>
        public Stream Decompress(Stream stream)
        {
            byte[] buffer = new byte[stream.Length];
            stream.Read(buffer, 0, (int)buffer.Length);
            return new MemoryStream(Decompress(buffer));
        }
        public string CompressorName { get { return M_COMPRESSOR_NAME;} }
    }

}
