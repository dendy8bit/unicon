using System;
using System.Collections.Generic;
using System.Xml;
using SchemeEditorSystem.BlockS;

namespace SchemeEditorSystem.descriptors
{
    public class BlockDesc
    {
        public string name;
        public string TypeElem;
        public string group;
        public string propType;
        public string description;
        //the list of contacts
        public List<PinDesc> listPinDesc;
        public Dictionary<string, string> userData;
        
        public int posX;
        public int posY;

        public BlockDesc(BlockDesc bd)
        {
            name = bd.name;
            propType = bd.propType;
            TypeElem = bd.TypeElem;
            description = bd.description;
            group = bd.group;
            listPinDesc = new List<PinDesc>(bd.listPinDesc);
            userData = new Dictionary<string, string>(bd.userData); 
        }
        public BlockDesc()
        {
            listPinDesc = new List<PinDesc>();
            userData = new Dictionary<string, string>();
            name = string.Empty;
            propType = string.Empty;
            TypeElem = string.Empty;
            description = string.Empty;
            group = string.Empty;
        }

        public void Init(string inpType, int inpPosX, int inpPosY)
        {
            TypeElem = inpType;
            posX = inpPosX;
            posY = inpPosY;
        }

        public void Init(string inpName, string inpType, int inpPosX, int inpPosY)
        {
            name = inpName;
            TypeElem = inpType;

            posX = inpPosX;
            posY = inpPosY;
        }

        public void AddPin(string name,PinType type,PinOrientation orient, int position)
        {
            PinDesc pin = new PinDesc();
            pin.PinName=name;
            pin.PinType=type;
            pin.Orientation=orient;
            pin.Position=position;
            listPinDesc.Add(pin);
        }
        
        public void AddUserProp(string propName,string value)
        {
            //userData.Add(propName, value);
            userData[propName] = value;
        }

        #region save/load
        
        public void PropertiesFromXML(XmlTextReader reader)
        {
            //reader.Read(); // entering Block tag
            name = reader.GetAttribute("name");
            TypeElem = reader.GetAttribute("type");
            group = reader.GetAttribute("group");
            propType = reader.GetAttribute("propType");
            description = reader.GetAttribute("description");
            //UserData = reader.GetAttribute("UserData");
            posX = Convert.ToInt32(reader.GetAttribute("posX"));
            posY = Convert.ToInt32(reader.GetAttribute("posY"));
            reader.Read(); // entering PinData tag
            int Count = Convert.ToInt32(reader.GetAttribute("count"));
            for (int i = 0; i < Count; i++)
			{
			    reader.Read();
                string pName = reader.GetAttribute("name");
                PinType pType = Pin.GetType(reader.GetAttribute("type"));
                PinOrientation pOrientation = Pin.GetPinOrientation(reader.GetAttribute("orient"));
                int pPosition = Convert.ToInt32(reader.GetAttribute("position"));
                listPinDesc.Add(new PinDesc(pName,pType,pOrientation,pPosition));
			}
            if(Count!=0) reader.Read();//exit from PinData tag

            reader.Read();
            Count = Convert.ToInt32(reader.GetAttribute("count"));
            for (int i = 0; i < Count; i++)
			{
			    reader.Read();
                this.AddUserProp(reader.GetAttribute("key"),reader.GetAttribute("value"));
			}
            if(Count!=0)    reader.Read();
            reader.Read();//exit from Block tag
        }
        
        #endregion

        public bool IsValid()
        {
            return listPinDesc != null && this.userData != null;
        }
        // ����� ���� ������ ����� �����, �� �� �� ��������� ���������� ������ ������� �������� ������
        //public bool IsValid()
        //{
        //    return this.TypeElem.Length <= 10;
        //}
    }
}
