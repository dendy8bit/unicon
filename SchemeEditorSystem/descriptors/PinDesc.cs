namespace SchemeEditorSystem.descriptors
{
    /// <summary>
    /// ����� ������ ��� ���������
    /// </summary>
    public enum PinType
    {
        /// <summary>
        /// ������ �����
        /// </summary>
        PT_DIRECT,
        /// <summary>
        /// ��������� �����
        /// </summary>
        PT_INVERSE
    }
    /// <summary>
    /// ������������ ������
    /// </summary>
    public enum PinOrientation
    {
        /// <summary>
        /// ������������ �����
        /// </summary>
        PO_LEFT,
        /// <summary>
        /// ������������ ������
        /// </summary>
        PO_RIGHT
    }
    /// <summary>
    /// ����� ������� ������
    /// </summary>
    public class PinDesc
    {
        private string _pinName;
        private PinType _pinType;
        private PinOrientation _orientation;
        private int _position;

        public PinDesc()
        {
            _pinName = "";
            _pinType = PinType.PT_DIRECT;
            _orientation = PinOrientation.PO_LEFT;
            _position = 1;
        }

        public PinDesc(PinDesc pin)
        {
            this._pinName = pin._pinName;
            this._pinType = pin._pinType;
            this._orientation = pin._orientation;
            this._position = pin._position;
        }

        public PinDesc(string pinName, PinType pinType, PinOrientation orientation, int position)
        {
            _pinName = pinName;
            _pinType = pinType;
            _orientation = orientation;
            _position = position;
        }
        /// <summary>
        /// �������� ������
        /// </summary>
        public string PinName
        {
            get { return _pinName; }
            set { _pinName = value; }
        }
        /// <summary>
        /// ��� ������
        /// </summary>
        public PinType PinType
        {
            get { return _pinType; }
            set { _pinType = value; }
        }
        /// <summary>
        /// ������������ ������
        /// </summary>
        public PinOrientation Orientation
        {
            get { return _orientation; }
            set { _orientation = value; }
        }
        /// <summary>
        /// �������
        /// </summary>
        public int Position
        {
            get { return _position; }
            set { _position = value; }
        }
    }
}
