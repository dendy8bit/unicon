using System;
using System.Collections.Generic;
using System.Text;
using SchemeEditorSystem.BlockS;

namespace SchemeEditorSystem.descriptors
{
    public class PropDesc
    {
        public string TypeElem="";
        public string group="";
        public string propType = "";
        public string description = "";

        public Block blockref = null;

        public bool _updateNeeded = false;

        virtual public bool IsValid()
        {
            if (TypeElem.Length > 10)
                return false;
            return true;
        }
    }
}
