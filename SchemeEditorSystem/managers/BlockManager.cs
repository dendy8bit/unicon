using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Xml;
using BEMN.Devices;
using Crownwood.Magic.Docking;
using SchemeEditorSystem.BlockS;
using SchemeEditorSystem.descriptors;
using SchemeEditorSystem.LineS;
using SchemeEditorSystem.ResourceLibs;
using SchemeEditorSystem.ResourceLibs.PropForms;

namespace SchemeEditorSystem.managers
{
    public class BlockManager
    {

        public event Action OpenPropFormAction;
        public event Action ClosePropFormAction;

        private InvalidBlockList _invalidBlockListForm;
        private PropertiesManager propManager;
        private BlockDesc currentProps;
        private Graphics grBack;
        private List<Block> blockList;
        private int numOneBlock = 0;
        private int allign;
        private float scale;
        private Content _content;
        private DockingManager _dockingManager;

        private bool _checkOutOfRangeBlock;

        public bool CheckOutBlock { get; set; }

        public List<Block> GetBlockList()
        {
            List<Block> list = new List<Block>();
            foreach (Block i in this.blockList)
            {
                if (i.GetDescription().propType != "TextBlock")
                {
                    list.Add(i);
                }
            }
            return list;
        }

        #region Init - create methods
        public BlockManager(int inpAllign, float inpScale)
        {
            this.allign = inpAllign;
            this.scale = inpScale;
            this.currentProps = new BlockDesc();
            this.blockList = new List<Block>();
        }

        public void SetDeviceToPropManager(string device)
        {
            this.propManager = new PropertiesManager(device);
        }
        public void SetDeviceToPropManager(Device deviceObj)
        {
            this.propManager = new PropertiesManager(deviceObj);
        }

        public void AddBlock(BlockDesc descriptor, bool newBlock)
        {
            string[] name = descriptor.name.Split(' ');

            //if (newBlock && (name[0] != "���" && name[0] != "�������"))
            //{
            //    descriptor.name = "Block" + (this.blockList.Count + 1);
            //}
            this.blockList.Add(new Block(descriptor, this.allign, this.scale));
            this.blockList[this.blockList.Count - 1].Draw(this.grBack);
        }

        public void AddBlock(BlockDesc descriptor)
        {
            this.blockList.Add(new Block(descriptor, this.allign, this.scale));
            this.blockList[this.blockList.Count - 1].Draw(this.grBack);
        }
        /// <summary>
        /// ��������� �� ����� ������� ������� � �������� �������
        /// </summary>
        /// <param name="descriptor">�������� �����</param>
        public void AddInOutBlock(BlockDesc descriptor)
        {
            string nameBlock = string.Format("Block{0}[{1}]", this.blockList.Count + 1, descriptor.name);
            descriptor.name = nameBlock;
            this.blockList.Add(new Block(descriptor, this.allign, this.scale));
            this.blockList[this.blockList.Count - 1].Draw(this.grBack);
        }

        public void SetScale(float inpScale)
        {
            this.scale = inpScale;
            foreach (Block i in this.blockList)
            {
                i.SetScale(inpScale);
            }
        }
        #endregion

        #region Draw methods


        public void Draw(System.Drawing.Graphics gr, SchematicSystem.AppState state)
        {
            this.grBack = gr;
            if (SchematicSystem.AppState.ST_MOVEONE == state)
            {
                for (int i = 0; i < this.blockList.Count; i++)
                {
                    if (i != this.numOneBlock)
                        this.blockList[i].Draw(gr);
                    else
                        this.blockList[i].DrawTransparent(gr);
                }
            }
            else if (SchematicSystem.AppState.ST_MOVESELECTED == state)
            {
                foreach (Block i in this.blockList)
                {
                    if (i.Selected)
                    {
                        i.DrawTransparent(gr);
                    }
                    else
                        i.Draw(gr);
                }
            }
            else
            {
                foreach (Block i in this.blockList)
                {
                    i.Draw(gr);
                }
            }
        }

        public void DrawPhantoms(System.Drawing.Graphics gr, int relX, int relY)
        {
            foreach (Block i in this.blockList)
            {
                if (i.Selected)
                {
                    i.DrawPhantom(gr, relX, relY);
                }
            }
        }
        #endregion

        #region Action methods
        public bool IsOnBlock(int x, int y)
        {
            return this.blockList.Any(b => b.IsOnBlock(x, y));
        }

        public bool IsOnSelectedBlock(int x, int y)
        {
            var block = this.blockList.FirstOrDefault(b => b.IsOnBlock(x,y));
            return block != null && block.Selected;
        }

        public int CountSelected()
        {
            return this.blockList.Count(block => block.Selected);
        }
        
        public void Select(int x, int y)
        {
            foreach (Block i in this.blockList.Where(i => i.IsOnBlock(x, y)))
            {
                i.Selected = true;
                break;
            }
        }

        public void DeselectAll()
        {
            foreach (Block i in this.blockList)
            {
                i.Selected = false;
            }
        }
        public void SelectAll()
        {
            foreach (Block i in this.blockList)
            {
                i.Selected = true;
            }
        }

        public void RefreshPinMoved()
        {
            foreach (var block in this.blockList)
            {
                block.RefreshPinMoved();
            }
        }

        public void MoveOne(int x, int y, int sizeX, int sizeY)
        {
            var block = this.blockList.FirstOrDefault(b => b.Selected);
            if (block != null) block.Move(x, y, sizeX, sizeY);
        }

        public void DrawOneFantom(System.Drawing.Graphics gr, int relX, int relY)
        {
            var block = this.blockList.FirstOrDefault(b => b.Selected);
            if (block != null) block.DrawPhantom(gr, relX, relY);
        }

        public void MoveSelected(int x, int y, int sizeX, int sizeY)
        {
            foreach (Block i in this.blockList)
            {
                if (i.Selected)
                    i.Move(x, y, sizeX, sizeY);
            }
        }

        public void CheckSelectedBlocks(int x, int y, int sizeX, int sizeY)
        {
            foreach (Block i in this.blockList)
            {
                if (i.Selected)
                {
                    i.CheckOutOfRangeBlocks(x, y, sizeX, sizeY, out _checkOutOfRangeBlock);
                    if (!_checkOutOfRangeBlock) CheckOutBlock = false;
                } 
            }
        }


        public void DeleteSelected(List<Connection> connections, Device device)
        {
            List<Block> delList = new List<Block>();
            foreach (Block block in this.blockList.Where(block => block.Selected))
            {
                delList.Add(block);
                block.RemovePinConnections(connections);
            }
            foreach (Block delBlock in delList)
            {
                this.blockList.Remove(delBlock);
                if (device != null && (delBlock.GetDescription().TypeElem == "journal" || delBlock.GetDescription().TypeElem == "journalA"))
                {
                    PropFormsStrings.RemoveSign(device, delBlock.GetDescription());
                }
            }
        }

        public void RemoveDeletedBlocksFromLists(Device device)
        {
            foreach (Block block in this.blockList.Where(b => b.GetDescription().TypeElem == "journal" || b.GetDescription().TypeElem == "journalA"))
            {
                PropFormsStrings.RemoveSign(device, block.GetDescription());
            }
        }

        public void UpdateLists(Device device)
        {
            foreach (Block block in this.blockList.Where(b => b.GetDescription().TypeElem == "journal" || b.GetDescription().TypeElem == "journalA"))
            {
                PropFormsStrings.UpdateLists(device, block.GetDescription());
            }
        }

        /// <summary>
        /// ��������� ���� ��� ������� �����
        /// </summary>
        /// <param name="x">���������� �</param>
        /// <param name="y">���������� Y</param>
        /// <returns></returns>
        public bool OpenProperties(int x, int y)
        {
            foreach (Block block in this.blockList.Where(i => i.IsOnBlock(x, y)))
            {
                this.currentProps = null;
                this.currentProps = block.GetDescription();

                OpenPropFormAction?.Invoke();

                bool ret = this.propManager.OpenPropertyFormWithAnswer(this.currentProps);

                ClosePropFormAction?.Invoke();

                if (ret)
                {
                    block.SetDescCopy(this.currentProps);
                    if (this._invalidBlockListForm != null && !this._invalidBlockListForm.BlockListIsEmpty)
                    {
                        this._invalidBlockListForm.RemoveBlockFromList(block);
                    }
                }
                return ret;
            }
            return false;
        }

        /// <summary>
        /// ��������� ���� ��� ������� ����� ���
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool OpenPropTez(int x, int y)
        {
            foreach (Block block in this.blockList)
            {
                if (block.IsOnBlock(x, y))
                {
                    this.currentProps = null;
                    this.currentProps = block.GetDescription();
                    if (this.currentProps.description != "����")
                    {
                        bool ret = this.propManager.OpenPropertyForm(this.currentProps);
                        if (ret) block.SetDescCopy(this.currentProps);
                        return ret;
                    }                    
                }
            }
            return false;
        }

        public PropertiesManager GetpropertyManager
        {
            get { return propManager; }
        }
        public void DeleteAll()
        {
            this.blockList.Clear();
        }

        public void AreaSelection(int ltX, int ltY, int rlX, int rlY)
        {
            foreach (Block i in this.blockList)
            {
                i.Selected = i.IsInArea(ltX, ltY, rlX, rlY);
            }
        }

        public void CopyFromXML(XmlTextWriter writer)
        {
            writer.WriteStartElement("BlockList");

            foreach (Block i in this.blockList)
            {
                if (i.Selected)
                    i.ExportToXML(writer);
            }
            writer.WriteEndElement();
        }

        public void PasteFromXML(XmlTextReader reader)
        {
            foreach (Block i in this.blockList)
            {
                i.Selected = false;
            }
            string[] str;
            BlockDesc descriptor;
            reader.Read();
            while ((reader.Name != "BlockList") || (reader.NodeType != XmlNodeType.EndElement))
            {
                descriptor = new BlockDesc();
                descriptor.PropertiesFromXML(reader);
                descriptor.posX += 2 *this.allign;
                descriptor.posY += 2 *this.allign;
                str = descriptor.name.Split('[');
                if (str.GetLength(0) == 1)
                    descriptor.name += "";
                else
                    descriptor.name = str[0] + "[" + str[1];
                Block temp = new Block(descriptor, this.allign, this.scale);
                temp.Selected = true;
                this.blockList.Add(temp);

                reader.Read();
            }
            reader.Read();

        }

        #endregion

        #region save/load
        public void ExportToXML(XmlTextWriter writer)
        {
            writer.WriteStartElement("BlockList");

            foreach (Block i in this.blockList)
            {
                i.ExportToXML(writer);
            }
            writer.WriteEndElement();
        }

        public void ImportFromXML(XmlTextReader reader)
        {
            reader.Read(); // ���� � <Block>
            while ((reader.Name != "BlockList") || (reader.NodeType != XmlNodeType.EndElement))
            {
                Block block = new Block(this.allign, this.scale);
                block.ImportFromXml(reader);
                this.blockList.Add(block);
                reader.Read();
                if (reader.NodeType == XmlNodeType.None) break;
            }
            reader.Read(); // ����� �� <BlockList>
        }

        public bool CheckInputsOutputs(Action redraw, DockingManager manager)
        {
            List<Block> invalidBlocks = (from block in this.blockList
                where block.GetDescription().@group == "�������"
                let form = this.propManager.GetPropForm(block.GetDescription()) as InOutProps
                where form != null && !form.CheckBlockList()
                select block).ToList();
            if (invalidBlocks.Count != 0)
            {
                if(this._dockingManager == null || this._dockingManager != manager) 
                    this._dockingManager = manager;
                if (this._invalidBlockListForm == null || !this._invalidBlockListForm.IsHandleCreated)
                {
                    this._invalidBlockListForm = new InvalidBlockList(invalidBlocks, this.propManager, redraw);
                    if (this._content == null)
                    {
                        this._content = this._dockingManager.Contents.Add(this._invalidBlockListForm, "������������ �����");
                        this._content.CaptionBar = true;
                        this._content.CloseButton = true;
                        this._content.FloatingSize = new Size(370, 470);
                        this._dockingManager.AddContentWithState(_content, State.Floating);
                        this._dockingManager.ShowContent(this._content);
                    }
                    else
                    {
                        this._dockingManager.ShowContent(this._content);
                    }
                }
                else
                {
                    this._dockingManager.ShowContent(this._content);
                }
                return false;
            }
            return true;
        }

        /// <summary>
        /// ��������� ����� �� ������� ������������ ������
        /// </summary>
        public void CloseInvalidListForm()
        {
            if (this._invalidBlockListForm == null) return;
            if (this._dockingManager != null && this._content != null)
            {
                this._dockingManager.HideContent(this._content);
            }
            else
            {
                this._invalidBlockListForm.Close();
                this._invalidBlockListForm = null;
            }
        }


        #endregion
    }
}
