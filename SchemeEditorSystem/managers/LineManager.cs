using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using SchemeEditorSystem.BlockS;
using SchemeEditorSystem.descriptors;
using SchemeEditorSystem.LineS;

namespace SchemeEditorSystem.managers
{
    public class LineManager
    {
        private int _numOneLine;
        private List<Connection> _connList = new List<Connection>();
        private Connection _phantomConn;
        private bool _lineCreation;
        public event Action UndoAction;
        private int _allign;
        private float _scale;
        public bool CheckOutOfRangeConnection { get; set; }

        /// <summary>
        /// ���������� ��������������� ������ ���� ����� �����
        /// </summary>
        /// <returns></returns>
        public List<Connection> GetLineList()
        {
            for (int i = 0; i < this._connList.Count;i++)
            {
                this._connList[i].VarName = "var" + i;
            }

            return this._connList;
        }


        #region Init - create methods
        public LineManager(int inpAllign, float inpScale)
        {
            this._allign = inpAllign;
            this._scale = inpScale;
            this._phantomConn = new Connection(this._allign, this._scale, "temp");
            this._phantomConn.AddNextDot(0, 0, false);
            this._phantomConn.AddNextDot(1, 1, false);
            this._phantomConn.AddNextDot(2, 2, false);
        }

        public void AddLine()
        {
            this._connList.Add(new Connection(this._allign, this._scale,"v"+(this._connList.Count+1).ToString()));
            this._lineCreation = true;
        }
        /// <summary>
        /// ��������� ����� ����� � ��������� � ������ ����� �����
        /// </summary>
        /// <param name="inpX">���������� �� �</param>
        /// <param name="inpY">���������� �� �</param>
        public void AddPointToLastLine(int inpX, int inpY)
        {
            this._connList[this._connList.Count - 1].AddNextPoint(inpX, inpY);
        }
        


        private LnkPoint AddNextPointTree(LnkPoint nodePoint, Pin pin, Connection con)
        {
            LnkPoint newPoint = new LnkPoint(nodePoint.X, pin.Y, this._allign, this._scale){AddingPoint = false};
            newPoint.AddNearDot(nodePoint);
            newPoint.AddNearDot(pin);
            nodePoint.AddNearDot(newPoint);
            pin.AddNearDot(newPoint);
            con.LnkPointList.AddRange(new[]{newPoint, pin});
            return newPoint;
        }

        private LnkPoint CreateNodePoint(Pin inpPin, Pin pin, Connection c, List<int> random)
        {
            int i = inpPin.X < pin.X
                ? this.GetNextRandomValue(inpPin.X+ this._allign, pin.X, random)
                : this.GetNextRandomValue(inpPin.X+ this._allign, inpPin.X + 150, random);
            LnkPoint nodePoint = new LnkPoint(i, inpPin.Y, this._allign, this._scale){AddingPoint = false};
            nodePoint.AddNearDot(inpPin);
            inpPin.AddNearDot(nodePoint);
            c.LnkPointList.Add(nodePoint);
            return nodePoint;
        }

        // TODO do while �������������
        private int GetNextRandomValue(int x1, int x2, List<int> randVal)
        {
            Random random = new Random();
            int ret;
            int counter = 0;
            do
            {
                ret = random.Next(x1, x2);
                ret = (int)((this._allign * Math.Round((double)(ret) / (double)(this._allign))));
                counter++;
            } 
            while (randVal.Contains(ret)&&counter<10);   //randVal.FindIndex(val => val == ret) != -1
            randVal.Add(ret);
            return ret;
        }

        public void CloseLine()
        {
            if (this._connList[this._connList.Count - 1].LnkPointList.Count < 2)
            {
                this._connList.RemoveAt(this._connList.Count - 1);
            }
            this._lineCreation = false;
        }

        public bool IsPointAdding()
        {
            return this._lineCreation;
        }

        public void SetScale(float inScale)
        {
            this._phantomConn.SetScale(inScale);
            this._scale = inScale;
            foreach (Connection i in this._connList)
            {
                i.SetScale(inScale);
            }
        }

        public void SetVarValue(string name,int value)
        {
            foreach (Connection i in this._connList)
            {
                if(i.VarName == name)
                {
                    i.VarValue = value;
                }
            }
        }

        #endregion

        #region Action methods
        public bool IsOnLine(int x, int y)
        {
            return this._connList.Any(i => i.IsOnLine(x, y));
        }
        /// <summary>
        /// �������� �� ������� �� ��� ���������� �����
        /// </summary>
        /// <param name="x">���������� ���� �</param>
        /// <param name="y">���������� ���� �</param>
        /// <returns></returns>
        public bool IsOnSelectedLine(int x, int y)
        {
            Connection line = this._connList.FirstOrDefault(b => b.IsOnLine(x, y));
            return line != null && line.IsSelected(x, y);
        }
        /// <summary>
        /// ����������� ���������� ���������� ���������
        /// </summary>
        /// <param name="x">����������� �� �</param>
        /// <param name="y">����������� �� �</param>
        public void MoveSelected(int x, int y, int sizeX, int sizeY)
        {
            foreach (Connection i in this._connList)
            {
                i.MoveSelected(x, y, sizeX, sizeY);
            }
        }

        public void CheckSelectedLine(int x, int y, int sizeX, int sizeY)
        {
            foreach (Connection i in this._connList)
            {
                i.CheckedPoint(x, y, sizeX, sizeY);
                if (!i.Check) CheckOutOfRangeConnection = false;
            }
        }

        public void AreaSelection(int ltX, int ltY, int rlX, int rlY)
        {
            foreach (Connection i in this._connList)
            {
                i.AreaSelection(ltX, ltY, rlX, rlY);
            }
        }

        public void Select(int x, int y)
        {
            Connection connection = this._connList.FirstOrDefault(c => c.IsOnLine(x, y));
            if(connection == null)return;
            connection.Select(x, y);
        }

        public void DeselectAll()
        {
            foreach (Connection i in this._connList)
            {
                i.SetSelected(false);
            }
        }
        public void SelectAll()
        {
            foreach (Connection i in _connList)
            {
                i.SetSelected(true);
            }
        }

        public int CountSelected()
        {
            return this._connList.Count(con => con.IsSelected());
        }
        /// <summary>
        /// ����������� ����������� ������ �������� �����
        /// </summary>
        /// <param name="x">����������� �� �</param>
        /// <param name="y">����������� �� �</param>
        public void MoveOne(int x, int y, int sizeX, int sizeY)
        {
            var connection = this._connList.FirstOrDefault(c => c.IsSelected());
            if(connection!=null)
                connection.MoveSelected(x, y, sizeX, sizeY);
        }

        /// <summary>
        /// ������� ���������� �����, ����� ����� ��� �����
        /// </summary>
        public void DeleteSelected()
        {
            var selectedCon = this._connList.Where(con => con.IsSelected()).Select(con => con).ToList();
            foreach (var connection in selectedCon)
            {
                List<Connection> newCon = connection.DeleteSelected();
                this._connList.Remove(connection);
                this._connList.AddRange(newCon);
            }
        }

        public void DeleteAll()
        {
            this._connList.Clear();
        }
        #endregion
        
        #region Draw methods
        public void Draw(System.Drawing.Graphics gr, SchematicSystem.AppState state)
        {
            int j = 0;
            while (j < this._connList.Count)
            {
                if ((this._connList[j].LnkPointList.Count < 2) && (!this._lineCreation))
                    this._connList.Remove(this._connList[j]);
                else
                {
                    j++;
                }
            }
            if (SchematicSystem.AppState.ST_MOVEONE == state)
            {
                for (int i = 0; i < this._connList.Count; i++)
                {
                    if (i != this._numOneLine)
                        this._connList[i].Draw(gr);
                    else
                        this._connList[i].DrawTransparent(gr);
                }
            }
            else if (SchematicSystem.AppState.ST_MOVESELECTED == state)
            {
                foreach (Connection i in this._connList)
                {
                    if (i.IsSelected())
                    {
                        i.DrawTransparentSelOnly(gr);
                    }
                    else
                        i.Draw(gr);
                }
            }
            else
            {
                foreach (Connection i in this._connList)
                {
                    i.Draw(gr);
                }
            }     

        }

        public void DrawDebug(System.Drawing.Graphics gr)
        {
            foreach (Connection connection in this._connList)
            {
                if ((connection.LnkPointList.Count < 2) && !this._lineCreation)
                    this._connList.Remove(connection);
                else
                    connection.DrawDebug(gr);
            }
        }

        public void DrawOneFantom(System.Drawing.Graphics gr, int relX, int relY)
        {
            this._connList[this._numOneLine].DrawOnePhantom(gr, relX, relY);
        }

        public void DrawPhantoms(System.Drawing.Graphics gr, int relX, int relY)
        {
            foreach (Connection i in this._connList)
            {
                i.DrawPhantom(gr, relX, relY);
            }
        }

        public void DrawAddPointPhantom(System.Drawing.Graphics gr, int x, int y)
        {
            Connection lastConn = this._connList[(this._connList.Count) - 1];
            lastConn.DrawAddPointPhantom(gr, x, y);

            this._phantomConn.LnkPointList[0].SetPosition(lastConn.LnkPointList[lastConn.LnkPointList.Count - 1].X,
                lastConn.LnkPointList[lastConn.LnkPointList.Count - 1].Y);

            if ((lastConn.LnkPointList.Count > 1)
                && (this._phantomConn.LnkPointList[0].Y == lastConn.LnkPointList[lastConn.LnkPointList.Count - 2].Y))
            {
                this._phantomConn.LnkPointList[1].SetPosition(this._phantomConn.LnkPointList[0].X, y);
            }
            else
            {
                this._phantomConn.LnkPointList[1].SetPosition(x, this._phantomConn.LnkPointList[0].Y);
            }

            this._phantomConn.LnkPointList[2].SetPosition(x, y);
            this._phantomConn.DrawTransparent(gr);
        }
        /// <summary>
        /// ��������� ����� � ��������� �� ����� �������
        /// </summary>
        /// <param name="points">��������� ����������� �����</param>
        public void AddLineOnPoints(List<Pin> points)
        {
            if (points.Count == 2)
            {
                this._connList.Add(new Connection(this._allign, this._scale, "var" + (this._connList.Count + 1)));
                var point = points.First(p => p.Orientation == PinOrientation.PO_RIGHT);
                LnkPoint firstPoint = new LnkPoint(point.X, point.Y, this._allign, this._scale) { AddingPoint = false };
                point = points.First(p => p.Orientation == PinOrientation.PO_LEFT);
                LnkPoint secondPoint = new LnkPoint(point.X, point.Y, this._allign, this._scale) { AddingPoint = false };
                //��������� ����� � ����������
                this._connList[this._connList.Count - 1].AddNextDot(firstPoint.X, firstPoint.Y, false, false);
                this._connList[this._connList.Count - 1].AddNextDot(secondPoint.X, secondPoint.Y, false, false);
            }
            else if (points.Count > 2)
            {
                Pin firstPoint = points.FirstOrDefault(p => p.Orientation == PinOrientation.PO_RIGHT);
                List<Pin> otherPoints =
                    points.Select(p => p).Where(p => p.Orientation == PinOrientation.PO_LEFT).ToList();
                Connection con = new Connection(this._allign, this._scale, "var" + (this._connList.Count + 1));
                con.AddNextDot(firstPoint.X, firstPoint.Y, false, false);
                foreach (LnkPoint nextPoint in otherPoints.Select(point =>
                    new LnkPoint(point.X, point.Y, this._allign, this._scale) { AddingPoint = false }))
                {
                    //��������� � ������� ����� � ������ �������� � ��������
                    firstPoint.NearDotsList.Add(nextPoint);
                    nextPoint.NearDotsList.Add(firstPoint);
                    //��������� � ���������� ������� �����
                    con.LnkPointList.Add(nextPoint);
                }
                this._connList.Add(con);
            }
        }


        #region ���������� � ������ ����,��� ����� ����� ���� � ������ ������, ������ ����
        
        //public void AddLineOnPoints(List<Pin> points, List<int> randomX, List<int> randomY)
        //{
        //    Connection c = new Connection(_allign, _scale, "v");
        //    Pin inpPin = points.Find(p => p.Orientation == PinOrientation.PO_RIGHT);
        //    inpPin.AddingPoint = false;
        //    c.LnkPointList.Add(inpPin);
        //    var pins = points.Where(p => p.Orientation == PinOrientation.PO_LEFT).Select(p => p).ToList();
        //    LnkPoint nodePoint = null;
        //    foreach (var pin in pins)
        //    {
        //        pin.AddingPoint = false;
        //        if (pin.Y == inpPin.Y) //���� ����� �� ����� �����
        //        {
        //            if (pins.Count == 1)//���� ����� ������ ��� � �����
        //            {
        //                inpPin.AddNearDot(pin);
        //                pin.AddNearDot(inpPin);
        //            }
        //            else   // ������ ����, �� �� ����� �����, ������ ��� ����
        //            {
        //                nodePoint = CreateNodePoint(inpPin, pin, c, randomX);
        //                nodePoint.AddNearDot(pin);
        //                pin.AddNearDot(nodePoint);
        //            }
        //            c.LnkPointList.Add(pin);
        //        }
        //        else // ����� �� �� ����� �����
        //        {
        //            if (nodePoint == null)
        //            {
        //                nodePoint = CreateNodePoint(inpPin, pin, c, randomX);
        //            }
        //            nodePoint = AddNextPointTree(nodePoint, pin, c);
        //        }
        //    }
        //    c.LnkPointList[c.LnkPointList.Count - 1].LastPoint = true;
        //    connList.Add(c);
        //}

        
        /// <summary>
        /// ������� ����� �� �������� ������
        /// </summary>
        /// <param name="points">������ �����</param>
        /// <param name="blocks">������ ������</param>
        /// <param name="randomX">������ ��������� ����� �� �</param>
        /// <param name="randomY">������ ��������� ����� �� �</param>
        /*public void AddLineOnPoints(List<Pin> points, List<Block> blocks, List<int> randomX, List<int> randomY)
        {
            Connection con = new Connection(_allign, _scale, "v");
            Pin inpPin = points.Find(p => p.Orientation == PinOrientation.PO_RIGHT);
            inpPin.AddingPoint = false;
            con.LnkPointList.Add(inpPin);
            var pins = points.Where(p => p.Orientation == PinOrientation.PO_LEFT).Select(p => p).ToList();
            LnkPoint nodePoint = null;
            foreach (var pin in pins)
            {
                pin.AddingPoint = false;
                // ���������, ����� �������� ����� ������� ��� ��������
                // ���������, ����� �� �����-���� ���� ����� �������
                // ���� ��, ������ ������ ������ �������� �����
                bool rightToLeft = (pin.X - inpPin.X) < 0;
                if (pin.Y == inpPin.Y) //���� ����� �� ����� �����
                {
                    // ���� 2 �����, ������ ���������
                    // ���� 2 ����� � ���� ����, ������ ����
                    // ���� ����� ������ 2�, ������� ����. ���� ���� ����, ������ ���
                    bool needNode = pins.Count > 1;
                    if (rightToLeft)
                    {

                    }
                    else
                    {
                        if (needNode)// ������ ����, �� �� ����� �����, ������ ��� ����
                        {
                            nodePoint = CreateNodePoint(inpPin, pin, con, randomX);
                            nodePoint.AddNearDot(pin);
                            pin.AddNearDot(nodePoint);
                        }
                        else//���� ����� ������ ��� � �����
                        {
                            Block blockOnWay = blocks.FirstOrDefault(block =>
                                block.X < pin.X && pin.Y >= block.Y && pin.Y <= block.Y + block.Heigth);
                            if (blockOnWay == null)
                            {
                                inpPin.AddNearDot(pin);
                                pin.AddNearDot(inpPin);
                            }
                            else
                            {

                            }
                        }
                        con.LnkPointList.Add(pin);
                    }
                }
                else // ����� �� �� ����� �����
                {
                    // �������� ���� ��� �������������
                    if (nodePoint == null)
                    {
                        nodePoint = CreateNodePoint(inpPin, pin, con, randomX);
                    }
                    nodePoint = AddNextPointTree(nodePoint, pin, con);
                }
            }
            con.LnkPointList[con.LnkPointList.Count - 1].LastPoint = true;
            connList.Add(con);
        }*/
        #endregion

        #endregion

        /// <summary>
        /// ��������� �� ����������� ������
        /// </summary>
        public void CheckForUnion(XmlTextReader reader)
        {
            try
            {
                int i = 0;
                while (i < this._connList.Count)
                {
                    int j = 0;
                    while (j < this._connList.Count)
                    {
                        if (this._connList[i] != this._connList[j])
                            if (this._connList[i].UnionConnectionIfPossible(this._connList[j]))
                            {
                                this._connList.Remove(this._connList[j]);
                                this.CheckForUnion(reader);  // �������� ��� ����,����� ����������� �� ���� ������ ������
                            }
                        j++;
                    }
                    i++;
                }
            }
            catch (IndexOutOfRangeException)
            {
                MessageBox.Show("��� ���������� �������� �������� ������.");
                if (this.UndoAction != null)
                {
                    this.UndoAction();
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("��� ���������� �������� �������� ������.");
                if (this.UndoAction != null)
                {
                    this.UndoAction();
                }
            }
            
         }
        

        #region save/load
        public void CopyFromXML(XmlTextWriter writer)
        {
            writer.WriteStartElement("ConnList");
            foreach (Connection i in this._connList)
            {
                if (i.IsSelected()) i.ExportToXML(writer);
            }
            writer.WriteEndElement();
        }

        public void PasteFromXML(XmlTextReader reader)
        {
            foreach (Connection i in this._connList)
            {
                i.SetSelected(false);
            }

            reader.Read();
            while ((reader.Name != "ConnList") || (reader.NodeType != XmlNodeType.EndElement) ||
                   (reader.NodeType == XmlNodeType.None))
            {
                if (reader.Name == "Connection")
                {
                    Connection conn = new Connection(this._allign, this._scale, reader.GetAttribute("varName") + "_copy");
                    conn.PasteFromXML(reader);
                    conn.SetSelected(true);
                    this._connList.Add(conn);
                }
                reader.Read();
            }
        }

        public void ExportToXML(XmlTextWriter writer)
        {
            writer.WriteStartElement("ConnList");
            foreach (Connection con in this._connList)
            {
                if (con.LnkPointList.Count > 1)
                    con.ExportToXML(writer);
            }
            writer.WriteEndElement();

        }

        public void ImportFromXML(XmlTextReader reader)
        {
            reader.Read();
            while ((reader.Name != "ConnList") || (reader.NodeType != XmlNodeType.EndElement))
            {
                if (reader.Name == "Connection")
                {
                    Connection conn = new Connection(this._allign, this._scale, reader.GetAttribute("varName"));
                    conn.ImportFromXML(reader);
                    conn.RemoveDublicatePoint();    // ���� � ����� ���� ������������� �����
                    this._connList.Add(conn);
                }
                reader.Read();
                if (reader.NodeType == XmlNodeType.None) break;
            }
        }
        #endregion
   }
}
