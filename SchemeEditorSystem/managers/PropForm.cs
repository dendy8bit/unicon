using System;
using System.Windows.Forms;
using SchemeEditorSystem.descriptors;

namespace SchemeEditorSystem.managers
{
    /// <summary>
    /// ������� ����� ��� ������� ������� ��������
    /// </summary>
    public class PropForm: Form
    {
        protected bool okClicked;
        /// <summary>
        /// ����, ������� ������ ��� �������� ��������
        /// </summary>
        protected BlockDesc PropFormDescriptor;
        /// <summary>
        /// ������������� ������� ����������� ������� ��� ����������� ��������� � ����� �������
        /// </summary>
        /// <param name="inpPropFormDescriptor"></param>
        public virtual void OpenWithDesc(BlockDesc inpPropFormDescriptor)
        {
            this.PropFormDescriptor = inpPropFormDescriptor;
            this.ShowDialog();
        }

        /// <summary>
        /// ������������� ������� ����������� ������� ��� ����������� ��������� � ����� �������
        /// </summary>
        /// <param name="inpPropFormDescriptor">�������� �����</param>
        public bool Open(BlockDesc inpPropFormDescriptor)
        {
            this.PropFormDescriptor = inpPropFormDescriptor;
            ShowDialog();
            return this.okClicked;
        }

        public void SetPropDesc(BlockDesc desc)
        {
            this.PropFormDescriptor = desc;
        }
    }
}
