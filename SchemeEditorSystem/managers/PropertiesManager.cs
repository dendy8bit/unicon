using System;
using System.Collections.Generic;
using BEMN.Devices;
using SchemeEditorSystem.descriptors;
using SchemeEditorSystem.ResourceLibs.PropForms;

namespace SchemeEditorSystem.managers
{
    public class PropertiesManager
    {
        PropForm _propWindow;

        Dictionary<string, PropForm> propertiesList;
        private string _device = string.Empty;

        public PropertiesManager(string device)
        {
            this.propertiesList = new Dictionary<string, PropForm>();
            this._device = device;
            this.InitForms();
        }
        public PropertiesManager(Device deviceObj)
        {
            this.propertiesList = new Dictionary<string, PropForm>();
            this._device = deviceObj.DeviceType;
            this.InitForms(deviceObj);
        }

        private void InitForms()
        {
            this.AddPropertyForm("MoreLes", new MoreLesProps());
            this.AddPropertyForm("MulDiv", new MulDivProps());
            this.AddPropertyForm("MultInp2Out", new SimpleProps());
            this.AddPropertyForm("TimeInverseInOut", new TimerProps());
            this.AddPropertyForm("Fixed", new FixedProps());
            this.AddPropertyForm("InOutElements", new InOutProps(this._device));
            this.AddPropertyForm("InOut16Elements", new InOut16Props(this._device));
            this.AddPropertyForm("TextBlock", new TextProps());
            this.AddPropertyForm("MS", new MsProps());
            this.AddPropertyForm("MS16", new MS16Props());
            this.AddPropertyForm("D", new DProps());
        }
        private void InitForms(Device deviceObj)
        {
            this.AddPropertyForm("MoreLes", new MoreLesProps());
            this.AddPropertyForm("MulDiv", new MulDivProps());
            this.AddPropertyForm("MultInp2Out", new SimpleProps());
            this.AddPropertyForm("TimeInverseInOut", new TimerProps());
            this.AddPropertyForm("Fixed", new FixedProps());
            this.AddPropertyForm("InOutElements", new InOutProps(deviceObj));
            this.AddPropertyForm("InOut16Elements", new InOut16Props(deviceObj));
            this.AddPropertyForm("TextBlock", new TextProps());
            this.AddPropertyForm("MS", new MsProps());
            this.AddPropertyForm("MS16", new MS16Props());
            this.AddPropertyForm("D", new DProps());
        }
        public void AddPropertyForm(string objType, PropForm editForm)
        {
            this.propertiesList.Add(objType, editForm);
        }

        public bool OpenPropertyForm(BlockDesc inpDesc)
        {
            if(this.propertiesList.ContainsKey(inpDesc.propType))
            {
                this._propWindow = this.propertiesList[inpDesc.propType];
                if (!this._propWindow.IsDisposed && !this._propWindow.Visible)
                {                   
                    this._propWindow.OpenWithDesc(inpDesc);
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// ��������� ����� ������� ����� �� ��������. ���������� ���������, ������ �� ���� ������ "�������"
        /// </summary>
        /// <param name="inpDesc">�������� �����</param>
        /// <returns>���� �� ������ ������� ��</returns>
        public bool OpenPropertyFormWithAnswer(BlockDesc inpDesc)
        {
            if (this.propertiesList.ContainsKey(inpDesc.propType))
            {
                this._propWindow = this.propertiesList[inpDesc.propType];

                if (this._propWindow.IsDisposed || this._propWindow.Visible) return false;
                return this._propWindow.Open(inpDesc);
            }
            return false;
        }

        /// <summary>
        /// ���������� ����� ������� ����� �� ��������
        /// </summary>
        /// <param name="desc">�������� �����</param>
        /// <returns>Form</returns>
        public PropForm GetPropForm(BlockDesc desc)
        {
            PropForm form = null;
            if (this.propertiesList.ContainsKey(desc.propType))
            {
                form = this.propertiesList[desc.propType];
                form.SetPropDesc(desc);
            }
            return form;
        }
    }
}
