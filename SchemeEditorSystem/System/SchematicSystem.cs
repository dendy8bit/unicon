using System;
using System.Globalization;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Windows.Forms;
using System.Drawing;
using System.Linq;
using BEMN.Devices;
using Crownwood.Magic.Docking;
using SchemeEditorSystem.BlockS;
using SchemeEditorSystem.compiler;
using SchemeEditorSystem.descriptors;
using SchemeEditorSystem.managers;

public enum SheetFormat
{
    A4_P,
    A4_L,
    A3_P,
    A3_L,
    A2_P,
    A2_L,
    A1_P,
    A1_L,
    A0_P,
    A0_L
}

public class SchematicSystem : Panel
{
    #region Fields
    private BlockManager _blockManager;
    private LineManager _lineManager;
    private Analyzer _analyzer;
    private bool _isOneBlockMove;
    private string _deviceName;
    private Device _deviceObj;
    private int _sizeX;
    private int _sizeY;
    private float _drawScale;
    private int _drawAllign;
    private string _panelName;
    private bool _addLineMode;
    private bool _debugMode;
    private bool _createLineMode;
    private Action RedrawMethod;
    private DockingManager _dockingManager;
    private int _autoBackupCounter;
    private List<byte[]> _memList;
    private List<string> _varNames;
    private int _numOfDrawn;
    private BufferedGraphicsContext _context;
    private BufferedGraphics _grafx;
    private BufferedGraphics _back;
    private bool _dragCondition;
    private bool _addPhantom;
    private int _dragEnterX;
    private int _dragEnterY;
    private int _currX;
    private int _currY;
    private AppState _state;
    private XmlTextReader _reader;

    private bool _checked;
    #endregion Fields

    #region Properties

    public int SizeX
    {
        get { return this._sizeX; }
        set { this._sizeX = value; }
    }

    public int SizeY
    {
        get { return this._sizeY; }
        set { this._sizeY = value; }
    }

    public bool LinesIsLoaded { get; private set; }

    public bool IsDrawnLoadedLines
    {
        get { return this._numOfDrawn < this._autoBackupCounter && this._numOfDrawn != -1; }
        set
        {
            if (value)
            {
                this._numOfDrawn = this._autoBackupCounter - 1;
            }
            else
            {
                this._numOfDrawn = -1;
            }
        }
    }

    public BlockManager BlockManager
    {
        get { return this._blockManager; }
    }
    #endregion

    #region AppStateEnum

    public enum AppState
    {
        ST_AREASELECT,
        ST_SELECT,
        ST_MOVESELECTED,
        ST_MOVEONE,
        ST_INACTIVE
    }

    #endregion

    #region initialization

    public SchematicSystem(string inpName, SheetFormat sFormat, string device, DockingManager manager)
    {
        this._deviceName = device;
        this._dockingManager = manager;
        this.InitSchematicSystem(inpName, sFormat);
        this.SetDeviceToPropManager(this._deviceName);
    }

    public SchematicSystem(string inpName, SheetFormat sFormat, Device deviceObj, DockingManager manager)
    {
        this._deviceObj = deviceObj;
        this._dockingManager = manager;
        this.InitSchematicSystem(inpName, sFormat);
        this.SetDeviceToPropManager(deviceObj);
    }

    private void InitSchematicSystem(string inpName, SheetFormat sFormat)
    {
        this._panelName = inpName;
        switch (sFormat)
        {
            case SheetFormat.A4_L:
                this._sizeX = 297;
                this._sizeY = 210;
                break;
            case SheetFormat.A4_P:
                this._sizeX = 210;
                this._sizeY = 297;
                break;
            case SheetFormat.A3_L:
                this._sizeX = 420;
                this._sizeY = 297;
                break;
            case SheetFormat.A3_P:
                this._sizeX = 297;
                this._sizeY = 420;
                break;
            case SheetFormat.A2_L:
                this._sizeX = 594;
                this._sizeY = 420;
                break;
            case SheetFormat.A2_P:
                this._sizeX = 420;
                this._sizeY = 594;
                break;
            case SheetFormat.A1_L:
                this._sizeX = 841;
                this._sizeY = 594;
                break;
            case SheetFormat.A1_P:
                this._sizeX = 594;
                this._sizeY = 841;
                break;
            case SheetFormat.A0_L:
                this._sizeX = 1189;
                this._sizeY = 841;
                break;
            case SheetFormat.A0_P:
                this._sizeX = 841;
                this._sizeY = 1189;
                break;
        }
        BackColor = Color.White;
        BorderStyle = BorderStyle.Fixed3D;
        Cursor = Cursors.Cross;
        ImeMode = ImeMode.Close;
        Location = new Point(0, 0);
        Name = "DrawSheetPanel";
        Size = new Size(381, 318);
        TabIndex = 0;
        MouseDown += new MouseEventHandler(this.DrawSheetPanel_MouseDown);
        MouseMove += new MouseEventHandler(this.DrawSheetPanel_MouseMove);
        MouseDoubleClick += new MouseEventHandler(this.DrawSheetPanel_MouseDoubleClick);
        Paint += new PaintEventHandler(this.DrawSheetPanel_Paint);
        MouseUp += new MouseEventHandler(this.DrawSheetPanel_MouseUp);
        AllowDrop = true;
        this._memList = new List<byte[]>();
        this._varNames = new List<string>();
        this._numOfDrawn = -1;
        this.InitVariables();
        this.RedrawMethod = this.Redraw;
    }

    private void InitVariables()
    {
        this._drawScale = 1;
        this._drawAllign = 5;
        this._analyzer = new Analyzer();
        this._blockManager = new BlockManager(this._drawAllign, this._drawScale);
        this._lineManager = new LineManager(this._drawAllign, this._drawScale);
        this._lineManager.UndoAction += this.Undo;
        this.ResizeDrawSheet();
    }

    private void SetDeviceToPropManager(string device)
    {
        this._blockManager.SetDeviceToPropManager(device);
    }

    private void SetDeviceToPropManager(Device deviceObj)
    {
        this._blockManager.SetDeviceToPropManager(deviceObj);
    }

    #endregion

    #region InterfaceEvents

    protected override void OnDragEnter(DragEventArgs drgevent)
    {
        if (this._debugMode) return;
        // everything is fine, allow the user to move the items
        drgevent.Effect = DragDropEffects.Move;
        // call the base OnDragEnter event
        base.OnDragEnter(drgevent);
    }

    protected override void OnDragDrop(DragEventArgs drgevent)
    {
        if (this._debugMode) return;
        // get the currently hovered row that the items will be dragged to
        Point clientPoint = PointToClient(new Point(drgevent.X, drgevent.Y));
        // retrieve the drag item data
        BlockDesc data = (BlockDesc) drgevent.Data.GetData(typeof (BlockDesc).ToString());
        data.posX = (int) ((clientPoint.X - 10)/this._drawScale);
        data.posY = (int) ((clientPoint.Y - 10)/this._drawScale);
        this.AddBlock(data);
        this.RedrawBack();
    }

    private void DrawSheetPanel_MouseDoubleClick(object sender, MouseEventArgs e)
    {
        if (this._debugMode && !this._addLineMode) return;
        int x = (int) (e.X/this._drawScale);
        int y = (int) (e.Y/this._drawScale);
        if (this.OpenProperties(x, y))
        {
            this.Analyse();
            this.RedrawBack();
        }
        this.RedrawSchematic();
        this.SaveBackup();
    }

    private void DrawSheetPanel_MouseDown(object sender, MouseEventArgs e)
    {
        if (this._debugMode) return;
        int x = (int) (e.X/this._drawScale);
        int y = (int) (e.Y/this._drawScale);

        if (this._addLineMode)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (this._createLineMode)
                {
                    this._lineManager.AddLine();
                    this._createLineMode = false;
                }
                this._lineManager.AddPointToLastLine(x, y);
                this.RedrawSchematic();
            }
            if (e.Button == MouseButtons.Right)
            {
                if (this._createLineMode)
                {
                    this._createLineMode = false;
                    this._addLineMode = false;
                }
                else
                {
                    this._createLineMode = true;
                    this._lineManager.CloseLine();
                    this.CheckForUnion();
                    this.Analyse();
                    this.SaveBackup();
                    this.RedrawSchematic();
                }
            }
        }
        else
        {
            if (e.Button == MouseButtons.Left)
            {
                this._dragEnterX = x;
                this._dragEnterY = y;
                if (this.IsOnObject(this._dragEnterX, this._dragEnterY))
                {
                    this._state = AppState.ST_SELECT;
                    this.RedrawBack();
                }
                else
                {
                    this._state = AppState.ST_AREASELECT;
                    this.DeselectAll();
                }
                this.RedrawSchematic();
            }
        }
        Focus();
    }

    private void DrawSheetPanel_MouseUp(object sender, MouseEventArgs e)
    {
        if (this._debugMode) return;
        int x = (int) (e.X/this._drawScale);
        int y = (int) (e.Y/this._drawScale);
        _blockManager.CheckOutBlock = true;
        _lineManager.CheckOutOfRangeConnection = true;
        _blockManager.CheckSelectedBlocks(x - this._dragEnterX, y - this._dragEnterY, SizeX, SizeY);
        _lineManager.CheckSelectedLine(x - this._dragEnterX, y - this._dragEnterY, SizeX, SizeY);
        switch (this._state)
        {
            case AppState.ST_MOVESELECTED:
                if (!_blockManager.CheckOutBlock || !_lineManager.CheckOutOfRangeConnection) break;
                this.MoveSelected(x - this._dragEnterX, y - this._dragEnterY, SizeX, SizeY);
                this.Analyse();
                this.SaveBackup();
                break;
            case AppState.ST_MOVEONE:
                if (!_blockManager.CheckOutBlock) break;
                this.MoveOne(x - this._dragEnterX, y - this._dragEnterY, SizeX, SizeY);
                bool ret = this.Analyse();
                this.SaveBackup();
                break;
            case AppState.ST_AREASELECT:
                int ltX, ltY, rbX, rbY;
                if (this._dragEnterX < x)
                {
                    ltX = this._dragEnterX;
                    rbX = x;
                }
                else
                {
                    ltX = x;
                    rbX = this._dragEnterX;
                }
                if (this._dragEnterY < y)
                {
                    ltY = this._dragEnterY;
                    rbY = y;
                }
                else
                {
                    ltY = y;
                    rbY = this._dragEnterY;
                }
                this.AreaSelection(ltX, ltY, rbX, rbY);
                break;
            case AppState.ST_INACTIVE:
                break;
        }
        this._state = AppState.ST_INACTIVE;
        this._dragCondition = false;
        this._addPhantom = false;
        this.RedrawBack();
        this.RedrawSchematic();
    }

    private void DrawSheetPanel_MouseMove(object sender, MouseEventArgs e)
    {
        if (this._debugMode) return;
        this._currX = (int) (e.X/this._drawScale);
        this._currY = (int) (e.Y/this._drawScale);
        if (this._addLineMode)
        {
            this._addPhantom = true;
            this.RedrawSchematic();
        }
        else if (e.Button == MouseButtons.Left)
        {
            if (this._state == AppState.ST_SELECT)
            {
                this._state = this._isOneBlockMove ? AppState.ST_MOVEONE : AppState.ST_MOVESELECTED;
            }
            this._dragCondition = true;
            this.RedrawSchematic();
        }
    }

    #endregion

    //Grafics initialization and list redraw

    #region Graphic Functions

    private void RecalCulateBuffer()
    {
        // Re-create the graphics buffer for a new window size.
        this._context = BufferedGraphicsManager.Current;
        this._context.MaximumBuffer = new Size(Width, Height);
        if (this._grafx != null)
        {
            this._grafx.Dispose();
            this._grafx = null;
        }
        this._grafx = this._context.Allocate(CreateGraphics(),
            new Rectangle(0, 0, Width, Height));
        if (this._back != null)
        {
            this._back.Dispose();
            this._back = null;
        }
        this._back = this._context.Allocate(CreateGraphics(),
            new Rectangle(0, 0, Width, Height));

        this.RedrawBack();
    }

    public void RedrawBack()
    {
        this.DrawGrid(this._back.Graphics);
        this.DrawBack(this._back.Graphics, this._state);
    }

    private void Redraw()
    {
        this.RedrawBack();
        this._back.Render();
        Invalidate();
    }

    public void UpdateSchematic()
    {
        this._back.Render(this._grafx.Graphics);
        if (this._createLineMode)
        {
            this._grafx.Graphics.DrawLine(Pens.Red, this._currX*this._drawScale - 5, this._currY*this._drawScale - 5, this._currX*this._drawScale + 5, this._currY*this._drawScale + 5);
            this._grafx.Graphics.DrawLine(Pens.Red, this._currX*this._drawScale + 5, this._currY*this._drawScale - 5, this._currX*this._drawScale - 5, this._currY*this._drawScale + 5);
        }
        this.DrawAction(this._grafx.Graphics, this._dragCondition, this._addPhantom, this._state, this._dragEnterX, this._currX, this._dragEnterY, this._currY);
    }

    public void RedrawSchematic()
    {
        this.UpdateSchematic();
        Graphics gr = CreateGraphics();
       this._grafx.Render(gr);
    }

    private void DrawGrid(Graphics g)
    {
        int tempCntr = (int) (this._drawAllign*this._drawScale);
        // Clear the graphics buffer every five updates.
        g.FillRectangle(Brushes.White, 0, 0, Width, Height);
        for (int i = 0; i < (int) (Width); i ++)
        {
            g.DrawLine(new Pen(Color.FromArgb(220, 220, 220)), (int) (i*this._drawAllign*this._drawScale), 0,
                (int) (i*this._drawAllign*this._drawScale), Height);
        }
        for (int i = 0; i < (int) (Height); i ++)
        {
            g.DrawLine(new Pen(Color.FromArgb(220, 220, 220)), 0, (int) (i*this._drawAllign*this._drawScale), Width,
                (int) (i*this._drawAllign*this._drawScale));
        }
        g.DrawRectangle(new Pen(Color.FromArgb(0, 0, 0), this._drawScale),
            tempCntr,
            tempCntr,
            Width - (int) (tempCntr*2),
            Height - (int) (tempCntr*2));
    }

    private void DrawSheetPanel_Paint(object sender, PaintEventArgs e)
    {
        this.UpdateSchematic();
        this._grafx.Render(e.Graphics);
    }

    private void ResizeDrawSheet()
    {
        Width = (int) (this._drawAllign*Math.Ceiling((decimal) (this._sizeX/this._drawAllign)));
        Height = (int) (this._drawAllign*Math.Ceiling((decimal) (this._sizeY/this._drawAllign)));
        Width = (int) (Width*this._drawScale);
        Height = (int) (Height*this._drawScale);
        this.RecalCulateBuffer();
    }

    public void DrawBack(Graphics gr, AppState state)
    {
        if (this._debugMode)
        {
            this._blockManager.Draw(gr, state);
        }
        else
        {
            if (state == AppState.ST_MOVEONE)
            {
                if (this._isOneBlockMove)
                {
                    this._blockManager.Draw(gr, state);
                    this._lineManager.Draw(gr, AppState.ST_INACTIVE);
                }
                else
                {
                    this._blockManager.Draw(gr, AppState.ST_INACTIVE);
                    this._lineManager.Draw(gr, state);
                }
            }
            else
            {
                this._blockManager.Draw(gr, state);
                this._lineManager.Draw(gr, state);
            }
        }
    }

    public void DrawAction(Graphics gr, bool dragCondition, bool addPhantom, AppState state,
               int dragEnter_X, int curr_X, int dragEnter_Y, int curr_Y)
    {
        if (this._debugMode)
        {
            this._lineManager.DrawDebug(gr);
        }

        if (dragCondition)
            this.DrawDrag(gr, state, dragEnter_X, curr_X, dragEnter_Y, curr_Y);
        if (this._lineManager.IsPointAdding())
            this._lineManager.DrawAddPointPhantom(gr, curr_X, curr_Y);
    }

    public void DrawSchematic(Graphics gr, bool dragCondition, bool addPhantom, AppState state,
                int dragEnter_X, int curr_X, int dragEnter_Y, int curr_Y)
    {
        if (this._debugMode)
        {
            this._blockManager.Draw(gr, state);
            this._lineManager.DrawDebug(gr);
        }
        else
        {
            this._blockManager.Draw(gr, state);
            this._lineManager.Draw(gr, state);
        }

        if (dragCondition)
            this.DrawDrag(gr, state, dragEnter_X, curr_X, dragEnter_Y, curr_Y);
        if (this._lineManager.IsPointAdding())
            this._lineManager.DrawAddPointPhantom(gr, curr_X, curr_Y);
    }

    public void DrawDrag(Graphics gr, AppState state,
        int dragEnterX, int currX, int dragEnterY, int currY)
    {
        switch (state)
        {
            case AppState.ST_MOVESELECTED:
                this._blockManager.DrawPhantoms(gr, currX - dragEnterX, currY - dragEnterY);
                this._lineManager.DrawPhantoms(gr, currX - dragEnterX, currY - dragEnterY);
                break;
            case AppState.ST_MOVEONE:
                if (this._isOneBlockMove)
                    this._blockManager.DrawOneFantom(gr, currX - dragEnterX, currY - dragEnterY);
                else
                    this._lineManager.DrawOneFantom(gr, currX - dragEnterX, currY - dragEnterY);
                break;
            case AppState.ST_AREASELECT:
                float tempX1, tempX2, tempY1, tempY2;
                if (currX < dragEnterX)
                {
                    tempX1 = currX;
                    tempX2 = dragEnterX - currX;
                }
                else
                {
                    tempX1 = dragEnterX;
                    tempX2 = currX - dragEnterX;
                }
                if (currY < dragEnterY)
                {
                    tempY1 = currY;
                    tempY2 = dragEnterY - currY;
                }
                else
                {
                    tempY1 = dragEnterY;
                    tempY2 = currY - dragEnterY;
                }
                gr.DrawRectangle(new Pen(Color.LightSkyBlue), (int)(tempX1 *this._drawScale),
                    (int)(tempY1 * this._drawScale), (int)(tempX2 *this._drawScale), (int)(tempY2 *this._drawScale)); // ������
                break;
        }
    }

    private void SaveBackup()
    {
        if (this._autoBackupCounter < this._memList.Count - 1)
        {
            for (int i = this._memList.Count - 1; i > this._autoBackupCounter; i--)
            {
                this._memList.RemoveAt(i);
            }
        }
        MemoryStream tempStream = new MemoryStream();
        XmlTextWriter memwriter = new XmlTextWriter(tempStream, System.Text.Encoding.UTF8);
        memwriter.Formatting = Formatting.Indented;
        memwriter.WriteStartDocument();
        memwriter.WriteStartElement("SchematicData");
        memwriter.WriteAttributeString("drawScale", this._drawScale.ToString());
        memwriter.WriteAttributeString("sizeX", this._sizeX.ToString());
        memwriter.WriteAttributeString("sizeY", this._sizeY.ToString());
        this.ExportToXML(memwriter); // the export...
        memwriter.WriteEndElement();
        memwriter.WriteEndDocument();
        memwriter.Close();
        this._memList.Add(tempStream.ToArray());
        this._autoBackupCounter = this._memList.Count - 1;
    }

    #endregion

    #region Check methods
    public bool IsOnObject(int x, int y)
    {
        if (this._blockManager.IsOnBlock(x, y))          //���� ������ �� �����
        {
            this.ClickOnBlock(x, y);
            return true;
        }
        if (this._lineManager.IsOnLine(x, y))
        {
            this.ClickOnLine(x, y);
            return true;
        }
        return false;
    }

    private void ClickOnBlock(int x, int y)
    {
        switch (this._blockManager.CountSelected())  //������� ���������� ���������� ������
        {
            case 0:                             //���� 0, �� ��������
                this.DeselectAll();
                this._blockManager.Select(x, y);
                this._isOneBlockMove = true;
                break;
            case 1:
                if (!this._blockManager.IsOnSelectedBlock(x, y)) //���� ������� ������ ����, �� �������� �����
                {
                    this.DeselectAll();            //������ ��������� ����������
                    this._blockManager.Select(x, y);
                    this._isOneBlockMove = true;
                }
                else if (this._lineManager.CountSelected() != 0)//���� ����� ����, ������� �������, ��� ���� ����
                {                                          //���������� �����, �� ������� ���
                    this._isOneBlockMove = false;
                }
                else
                {
                    this._isOneBlockMove = true;                //����� ������ ���� ����
                }
                break;
            default:
                if (this._blockManager.IsOnSelectedBlock(x, y))
                {
                    this._isOneBlockMove = false;
                }
                else
                {
                    this.DeselectAll();            //������ ��������� ����������
                    this._blockManager.Select(x, y);
                    this._isOneBlockMove = true;
                }
                break;
        }
    }

    private void ClickOnLine(int x, int y)
    {
        this._isOneBlockMove = false;
        switch (this._lineManager.CountSelected())  //������� ���������� ���������� �����
        {
            case 0:                             //���� 0, �� ��������
                this.DeselectAll();
                this._lineManager.Select(x, y);
                break;
            default:
                if (!this._lineManager.IsOnSelectedLine(x, y)) //���� ������� ������, �� �������� �����
                {
                    this.DeselectAll();            //������ ��������� ����������
                    this._lineManager.Select(x, y);
                }
                break;
        }
    }

    /// <summary>
    /// �������� �� ����������� ���������� ������ � ����
    /// </summary>
    public void CheckForUnion()
    {
        this._lineManager.CheckForUnion(this._reader);
    }
    #endregion

    #region Move, select methods

    public void MoveOne(int dx, int dy, int sizeX = 0, int sizeY = 0)
    {
        this._blockManager.RefreshPinMoved();
        if (this._isOneBlockMove)
            this._blockManager.MoveOne(dx, dy, sizeX, sizeY);
        else
            this._lineManager.MoveOne(dx, dy, sizeX, sizeY);
        this._lineManager.CheckForUnion(this._reader);
    }

    public void MoveSelected(int dx, int dy, int sizeX, int sizeY)
    {
        this._blockManager.RefreshPinMoved();
        this._blockManager.MoveSelected(dx, dy, sizeX, sizeY);
        this._lineManager.MoveSelected(dx, dy, sizeX, sizeY);
        this._lineManager.CheckForUnion(this._reader);
    }
    
    public void DeselectAll()
    {
        this._blockManager.DeselectAll();
        this._lineManager.DeselectAll();
    }

    public void SelectAll()
    {
        this._blockManager.SelectAll();
        this._lineManager.SelectAll();
        this.Analyse();
        this.RedrawBack();
        this.RedrawSchematic();
    }

    public void AreaSelection(int ltX, int ltY, int rbX, int rbY)
    {
        this._blockManager.AreaSelection(ltX, ltY, rbX, rbY);
        this._lineManager.AreaSelection(ltX, ltY, rbX, rbY);
    }
    #endregion

    #region Action methods

    public void SetVarValue(string name, int value)
    {
        this._lineManager.SetVarValue(name, value);
    }

    public void SetScale(float inpScale)
    {
        this._drawScale = inpScale;
        this._lineManager.SetScale(inpScale);
        this._blockManager.SetScale(inpScale);
    }

    public bool OpenProperties(int x, int y)
    {
        return this._blockManager.OpenProperties(x, y);
    }

    public void DeleteSelected(Device device)
    {
        this._blockManager.DeleteSelected(this._lineManager.GetLineList(), device);
        this._lineManager.DeleteSelected();
    }

    public void RemoveDeletedBlocksFromLists(Device device)
    {
        this._blockManager.RemoveDeletedBlocksFromLists(device);
    }

    public void UpdateLists(Device device)
    {
        this._blockManager.UpdateLists(device);
    }

    public void DeleteAll()
    {
        this._blockManager.DeleteAll();
        this._lineManager.DeleteAll();
    }

    public void CopyFromXML(XmlTextWriter writer)
    {
        this._blockManager.CopyFromXML(writer);
        this._lineManager.CopyFromXML(writer);
    }
    public void CutFromXML(XmlTextWriter writer)
    {
        this._blockManager.CopyFromXML(writer);
        this._lineManager.CopyFromXML(writer);
    }

    public void PasteFromXML(XmlTextReader reader)
    {
        reader.Read();//entering BlockManager tag
        if ((reader.Name == "BlockList") && (!reader.IsEmptyElement))
        {
            this._blockManager.PasteFromXML(reader);
        }
        else
            reader.Read();

        if ((reader.Name == "ConnList") && (!reader.IsEmptyElement))
        {
            this._lineManager.PasteFromXML(reader);
        }
        reader.Read();

    }

    #endregion

    #region External interface

    public void Undo()
    {
        if (this._addLineMode) return;
        if (this._autoBackupCounter == 0) return;
        this.DeleteAll();
        --this._autoBackupCounter;
        MemoryStream tempStream = new MemoryStream();
        tempStream.Write(this._memList[this._autoBackupCounter], 0, this._memList[this._autoBackupCounter].GetLength(0));
        tempStream.Seek(0, 0);
        XmlTextReader reader = new XmlTextReader(tempStream);
        //XmlReader reader = new XmlTextReader(tempStream);
        reader.WhitespaceHandling = WhitespaceHandling.None;
        reader.Read();
        reader.Read();
        this._sizeX = Convert.ToInt32(reader.GetAttribute("sizeX"));
        this._sizeY = Convert.ToInt32(reader.GetAttribute("sizeY"));
        this.SetScale(this._drawScale);
        this.ImportFromXML(reader, this.RedrawMethod, this._dockingManager);
        reader.Read(); //exit from SchematicData tag
        reader.Close();
        this.Analyse();
        this.RedrawBack();
        this.RedrawSchematic();
    }

    public void Redo()
    {
        if (this._addLineMode) return;
        if (this._autoBackupCounter > this._memList.Count - 2) return;
        ++this._autoBackupCounter;
        this.DeleteAll();
        MemoryStream tempStream = new MemoryStream();
        tempStream.Write(this._memList[this._autoBackupCounter], 0, this._memList[this._autoBackupCounter].GetLength(0));
        tempStream.Seek(0, 0);
        XmlTextReader reader = new XmlTextReader(tempStream);
        reader.WhitespaceHandling = WhitespaceHandling.None;
        reader.Read();
        reader.Read(); // entering  SchematicData tag
        this._sizeX = Convert.ToInt32(reader.GetAttribute("sizeX"));
        this._sizeY = Convert.ToInt32(reader.GetAttribute("sizeY"));
        this.SetScale(this._drawScale);
        this.ImportFromXML(reader, this.RedrawMethod, this._dockingManager);
        reader.Read(); //exit from SchematicData tag
        reader.Close();
        this.Analyse();
        this.RedrawBack();
        this.RedrawSchematic();
    }

    public void SetVarableValue(string varName, int value)
    {
        this.SetVarValue(varName, value);
    }

    public void GetCompileData(XmlWriter writer)
    {
        if (this.Analyse())
        {
            throw new CompileLogicException(this._analyzer.ErrorFlags, this._panelName);
        }
        if (this.CheckInputsOutputs(this.RedrawMethod, this._dockingManager))
        {
            this.WriteCompileData(writer);
        }
        else
        {
            throw new Exception("������� ������������ ������ � ������ �� ����� " + this._panelName);
        }
    }
    
    /// <summary>
    /// �������������, ��� ����� ��������� � ������ ��������
    /// </summary>
    public void StartDebugMode()
    {
        this.DeselectAll();
        this.SetDebugMode(true);
        this._debugMode = true;
    }

    public void DrawIntoGraphic(Graphics gr, float scl)
    {
        this.SetScale(scl);
        this.DrawSchematic(gr, false, false, AppState.ST_INACTIVE, 0, 0, 0, 0);
        this.SetScale(this._drawScale);
    }

    public void StopDebugEvent()
    {
        this.SetDebugMode(false);
        this._debugMode = false;
        this.RedrawBack();
        this.RedrawSchematic();
    }

    public void AddBlock(BlockDesc descriptor)
    {
        if (descriptor.IsValid())
        {
            this._blockManager.AddBlock(descriptor, true);
        }
        this.Analyse();
        this.RedrawSchematic();
        this.SaveBackup();
    }

    public void AddBlockDescList(List<BlockDesc> descList, List<string> varNames)
    {
        this._varNames.AddRange(varNames);
        this.LinesIsLoaded = true;
        foreach (var blockDesc in descList.Where(blockDesc => blockDesc.IsValid()))
        {
            if (blockDesc.group == "�������")
            {
                this._blockManager.AddInOutBlock(blockDesc);
            }
            else
            {
                this._blockManager.AddBlock(blockDesc);
            }
        }
        this.RedrawSchematic();
        this.SaveBackup();
    }

    /// <summary>
    /// ������ ����� �� ����� �� ������
    /// </summary>
    public void DrawLines()
    {
        if (!this.IsDrawnLoadedLines)
        {
            List<Block> blocks = this._blockManager.GetBlockList();
            //List<int> randomX = new List<int>();
            //List<int> randomY = new List<int>();
            foreach (var vName in this._varNames)
            {
                List<Pin> points = new List<Pin>();
                string name = vName;
                foreach (var p in blocks.Select(block => block.GetPoint(name)).Where(p => p != null))
                {
                    points.AddRange(p);
                }
                if (points.Count > 1) this._lineManager.AddLineOnPoints(points/*, randomX, randomY*/);
            }

            this.CheckForUnion();
            this.Analyse();
            this.RedrawBack();
            this.RedrawSchematic();
            this.SaveBackup();
            this.IsDrawnLoadedLines = true;
        }
    }

    public void AddLine()
    {
        if (!this._addLineMode)
        {
            this._addLineMode = true;
            this._createLineMode = true;
        }
        else
        {
            this._addLineMode = false;
            this._createLineMode = false;
        }
    }

    public void AddLineEvent(object sender, EventArgs e)
    {
        this.AddLine();
    }

    public void ZoomInEvent()
    {
        if (this._drawScale < 5)
        {
            this._drawScale *= 1.25f;
            this.SetScale(this._drawScale);
            this.ResizeDrawSheet();
            this.RedrawSchematic();
        }
    }

    public void ZoomInEvent(object sender, EventArgs e)
    {
        this.ZoomInEvent();
    }

    public void ZoomOutEvent()
    {
        if (this._drawScale > 0.3f)
        {
            this._drawScale /= 1.25f;
            this.SetScale(this._drawScale);
            this.ResizeDrawSheet();
            this.RedrawSchematic();
        }
    }

    public void ZoomOutEvent(object sender, EventArgs e)
    {
        this.ZoomOutEvent();
    }

    public void DeleteEvent(object sender, EventArgs e)
    {
        this.DeleteEvent();
    }

    public void DeleteEvent()
    {
        if(this._addLineMode) return;
        this.DeleteSelected(this._deviceObj);
        this.Analyse();
        this.SaveBackup();
        this.RedrawBack();
        this.RedrawSchematic();
    }

    public void RemoveDeletedBlocksFromLists()
    {
        if(this._deviceObj == null) return;
        this.RemoveDeletedBlocksFromLists(this._deviceObj);
    }

    public void UpdateLists()
    {
        if (this._deviceObj == null) return;
        this.UpdateLists(this._deviceObj);
    }
    
    public void CopyFromXML()
    {
        if (this._addLineMode) return;
        XmlTextWriter memwriter = new XmlTextWriter("bsbglbeb.buf", System.Text.Encoding.UTF8);
        memwriter.Formatting = Formatting.Indented;
        memwriter.WriteStartDocument();
        memwriter.WriteStartElement("SchematicData");
        memwriter.WriteAttributeString("drawScale", this._drawScale.ToString());
        memwriter.WriteAttributeString("sizeX", this._sizeX.ToString());
        memwriter.WriteAttributeString("sizeY", this._sizeY.ToString());
        this.CopyFromXML(memwriter); // the export...
        memwriter.WriteEndElement();

        memwriter.WriteEndDocument();
        memwriter.Close();
    }
    public void CutFromXML()
    {
        if (this._addLineMode) return;
        XmlTextWriter memwriter = new XmlTextWriter("bsbglbeb.buf", System.Text.Encoding.UTF8);
        memwriter.Formatting = Formatting.Indented;
        memwriter.WriteStartDocument();
        memwriter.WriteStartElement("SchematicData");
        memwriter.WriteAttributeString("drawScale", this._drawScale.ToString());
        memwriter.WriteAttributeString("sizeX", this._sizeX.ToString());
        memwriter.WriteAttributeString("sizeY", this._sizeY.ToString());
        this.CutFromXML(memwriter); // the export...
        memwriter.WriteEndElement();

        memwriter.WriteEndDocument();
        memwriter.Close();
        this.DeleteSelected(this._deviceObj);
        this.Analyse();
        this.RedrawBack();
        this.RedrawSchematic();
    }

    public void PasteFromXML()
    {
        if (this._addLineMode) return;
        XmlTextReader reader = new XmlTextReader("bsbglbeb.buf");
        reader.WhitespaceHandling = WhitespaceHandling.None;
        reader.Read();
        reader.Read(); // entering  SchematicData tag
        this.PasteFromXML(reader);
        reader.Read(); //exit from SchematicData tag
        reader.Close();
        this.SaveBackup();
        this.Analyse();
        this.RedrawBack();
        this.RedrawSchematic();
    }

    public string GetPanelName()
    {
        return this._panelName;
    }

    #region Compilation

    public void SetDebugMode(bool inpDebugMode)
    {
        this._debugMode = inpDebugMode;
    }
    /// <summary>
    /// ����������� ����� �� ������ ������, � ��� �� ����������� ������� ���������� ����������.
    /// ���� ����� �� ����� ������, �� ����� ���������� false
    /// </summary>
    /// <returns>���� ����� �� ����� ������, �� false</returns>
    public bool Analyse()
    {
        try
        {
            return this._analyzer.Analyse(this._lineManager.GetLineList(), this._blockManager.GetBlockList());
        }
        catch (Exception e)
        {
            MessageBox.Show(string.Format("� ����� �������� ������:{0}. �������� ������ ��������", e.Message),
                "��������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            return false;
        }
    }
    /// <summary>
    /// ������� XML �� ����������������� �����
    /// </summary>
    /// <param name="writer"></param>
    public void WriteCompileData(XmlWriter writer)
    {
        this._analyzer.WriteAnalysedData(writer);
    }

    #endregion

    #region save/load

    /// <summary>
    /// ���������� ������� � ����
    /// </summary>
    /// <param name="writer">XmlTextWriter ��� ������</param>
    /// <param name="device">�������� ����������</param>
    public void WriteXml(XmlTextWriter writer, string device)
    {
        writer.WriteStartElement("SchematicData");
        writer.WriteAttributeString("name", this._panelName);
        if (device != string.Empty) writer.WriteAttributeString("device", device);
        writer.WriteAttributeString("drawScale", this._drawScale.ToString(CultureInfo.CurrentCulture));
        writer.WriteAttributeString("sizeX", this._sizeX.ToString(CultureInfo.CurrentCulture));
        writer.WriteAttributeString("sizeY", this._sizeY.ToString(CultureInfo.CurrentCulture));
        this.ExportToXML(writer); // the export...
        writer.WriteEndElement();
    }

    /// <summary>
    /// ���������� ������� � ����
    /// </summary>
    /// <param name="writer">XmlTextWriter ��� ������</param>
    public void WriteXml(XmlTextWriter writer)
    {
        this.WriteXml(writer, string.Empty);
    }

    /// <summary>
    /// ������ ������� �� �����
    /// </summary>
    /// <param name="reader">XmlTextReader ��� ������</param>
    public void ReadXml(XmlTextReader reader)
    {
        reader.Read(); // entering  SchematicData tag
        this.ReadOtherXmlData(reader);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="reader"></param>
    /// <param name="device"></param>
    /// <returns></returns>
    public bool ReadXml(XmlTextReader reader, string device)
    {
        reader.Read();
        string deviceAttr = reader.GetAttribute("device");
        if (deviceAttr != device) return false;
        try
        {
            this.ReadOtherXmlData(reader);
        }
        catch
        {
            return false;
        }
        return true;
    }

    private void ReadOtherXmlData(XmlTextReader reader)
    {
        string drawScale = reader.GetAttribute("drawScale");
        if (drawScale != null)
        {
            drawScale = drawScale.Replace(",", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                .Replace(".", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator);
            this._drawScale = Convert.ToSingle(drawScale);
        }
        else
        {
            MessageBox.Show("���� ��� ���������� �������. �� ��������� ��� �������", "������", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            return;
        }
        this._panelName = reader.GetAttribute("name");
        this._sizeX = Convert.ToInt32(reader.GetAttribute("sizeX"));
        this._sizeY = Convert.ToInt32(reader.GetAttribute("sizeY"));
        this.SetScale(this._drawScale);
        this.ImportFromXML(reader, this.RedrawMethod, this._dockingManager);
        reader.Read(); //exit from SchematicData tag
        this.ResizeDrawSheet();
        this.Analyse();
        this.RedrawSchematic();
        this.RedrawBack();
        this.SaveBackup();
    }
    public void ExportToXML(XmlTextWriter writer)
    {
        this._blockManager.ExportToXML(writer);
        this._lineManager.ExportToXML(writer);
    }

    public void ImportFromXML(XmlTextReader reader, Action redraw, DockingManager manager)
    {
        this._reader = reader;
        reader.Read();//entering BlockManager tag
        if ((reader.Name == "BlockList") && (!reader.IsEmptyElement))
        {
            this._blockManager.ImportFromXML(reader);
        }

        if ((reader.Name == "ConnList") && (!reader.IsEmptyElement))
        {
            this._lineManager.ImportFromXML(reader);
        }
        this.CheckInputsOutputs(redraw, manager);
        reader.Read();
    }

    public bool CheckInputsOutputs(Action redraw, DockingManager manager)
    {
        return this._blockManager.CheckInputsOutputs(redraw, manager);
    }

    /// <summary>
    /// ��������� ����� �� ������� ������������ ������
    /// </summary>
    public void CloseInvalidListForm()
    {
        this._blockManager.CloseInvalidListForm();
    }
    #endregion

    #endregion
}
