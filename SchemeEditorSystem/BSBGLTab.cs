using System;
using System.Windows.Forms;
using System.Drawing;
using BEMN.Devices;
using Crownwood.Magic.Docking;

namespace SchemeEditorSystem
{
    public class BSBGLTab : Crownwood.Magic.Controls.TabPage
    {
        private SchematicSystem _schematic;
        private Panel _panel;
        private ToolStrip _editorToolStrip;
        //ScrollMemory
        private int _hScrollMemory;
        private int _vScrollMemory;

        //ScrollMemory
        private int _hScrollMemoryOpenPropform;
        private int _vScrollMemoryOpenPropform;
        //Pan
        private bool isOnPan;
        private Point panStart;

        private bool isCopy;

        public void InitializeBSBGLSheet(string name, SheetFormat sFormat, string device, DockingManager manager)
        {
            this._schematic = new SchematicSystem(name, sFormat, device, manager);
            this.InitControls(name);
        }
        /// <summary>
        /// ����������� ��� ����,����� ���������������� ��� ��600
        /// </summary>
        /// <param name="name">�������� �����</param>
        /// <param name="sFormat">������ �����</param>
        /// <param name="device">����������</param>
        public void InitializeBSBGLSheet(string name, SheetFormat sFormat, Device device, DockingManager manager)
        {
            this._schematic = new SchematicSystem(name, sFormat, device, manager);
            this.InitControls(name);
        }

        private void InitControls(string name)
        {
            this._panel = new Panel();
            this._panel.AutoScroll = true;
            this._panel.Controls.Add(this._schematic);
            this._panel.Dock = DockStyle.Fill;
            this._panel.Location = new Point(0, 0);
            this._panel.Name = name;
            this._panel.TabIndex = 0;
            this._panel.Scroll += new ScrollEventHandler(this._schematic_Scroll);
            this._panel.MouseWheel += new MouseEventHandler(this._panel_MouseWheel);

            this._schematic.PreviewKeyDown += new PreviewKeyDownEventHandler(this.OnPreviewKeyDown);
            this._schematic.MouseWheel += new MouseEventHandler(this._schematic_MouseWheel);
            this._schematic.MouseDown += new MouseEventHandler(this._schematic_MouseDown);
            //this._schematic.MouseMove += this.SchematicOnMouseMove;
            this._schematic.MouseUp += new MouseEventHandler(this._schematic_MouseUp);
            this._schematic.GotFocus+= new EventHandler(this._BSBGLTab_Enter);
            this._schematic.BlockManager.OpenPropFormAction += this._openPropertyForm;
            this._schematic.BlockManager.ClosePropFormAction += this._closePropertyForm;

            ToolStripContainer mainToolStripContainer = new ToolStripContainer();
            mainToolStripContainer.BottomToolStripPanelVisible = false;
            // 
            // MainToolStripContainer.ContentPanel
            // 
            mainToolStripContainer.ContentPanel.Controls.Add(this._panel);
            mainToolStripContainer.ContentPanel.Size = new Size(347, 436);
            mainToolStripContainer.Dock = DockStyle.Fill;
            // 
            // MainToolStripContainer.LeftToolStripPanel
            // 
            this._editorToolStrip = this.CreateToolStrip();
            mainToolStripContainer.LeftToolStripPanel.Controls.Add(this._editorToolStrip);
            mainToolStripContainer.Location = new Point(0, 3);
            mainToolStripContainer.Name = "MainToolStripContainer";
            mainToolStripContainer.RightToolStripPanelVisible = false;
            mainToolStripContainer.Size = new Size(371, 436);
            mainToolStripContainer.TabIndex = 6;
            mainToolStripContainer.Text = "toolStripContainer1";
            mainToolStripContainer.TopToolStripPanelVisible = false;
            Title = name;
            Control = mainToolStripContainer;
            Disposed += (sender, args) =>
            {
                this._schematic.CloseInvalidListForm();
            };
        }
        
        /// <summary>
        /// ���������� �����
        /// </summary>
        public SchematicSystem Schematic
        {
            get { return this._schematic; }
        }
        /// <summary>
        /// �������� ��������� (�����)
        /// </summary>
        public string TabName
        {
            get { return _title; }
        }

        public void UpdateTitle()
        {
            Title = this._schematic.GetPanelName();
            this._schematic.Focus();
        }


        private void _schematic_MouseUp(object sender, MouseEventArgs e)
        {
            if (this.isOnPan)
            {
                this.isOnPan = false;
                this._schematic.Cursor = Cursors.Cross;
                if (this.panStart.X - e.X + this._panel.HorizontalScroll.Value < 0)
                {
                    this._panel.HorizontalScroll.Value = 0;

                }
                else if (this.panStart.X - e.X + this._panel.HorizontalScroll.Value > this._panel.HorizontalScroll.Maximum)
                {
                    this._panel.HorizontalScroll.Value = this._panel.HorizontalScroll.Maximum;
                }
                else
                {
                    this._panel.HorizontalScroll.Value += this.panStart.X - e.X;
                }

                if (this.panStart.Y - e.Y + this._panel.VerticalScroll.Value < 0)
                {
                    this._panel.VerticalScroll.Value = 0;
                }
                else if (this.panStart.Y - e.Y + this._panel.VerticalScroll.Value > this._panel.VerticalScroll.Maximum)
                {
                    this._panel.VerticalScroll.Value = this._panel.VerticalScroll.Maximum;
                }
                else
                {
                    this._panel.VerticalScroll.Value += this.panStart.Y - e.Y;
                }
                this._hScrollMemory = this._panel.HorizontalScroll.Value;
                this._vScrollMemory = this._panel.VerticalScroll.Value;
            }
        }

        //private void SchematicOnMouseMove(object sender, MouseEventArgs e)
        //{
        //    if(!this.isOnPan) return;
            
        //    if (this.panStart.X - e.X + this._panel.HorizontalScroll.Value < 0)
        //    {
        //        this._panel.HorizontalScroll.Value = 0;

        //    }
        //    else if (this.panStart.X - e.X + this._panel.HorizontalScroll.Value > this._panel.HorizontalScroll.Maximum)
        //    {
        //        this._panel.HorizontalScroll.Value = this._panel.HorizontalScroll.Maximum;
        //    }
        //    else
        //    {
        //        this._panel.HorizontalScroll.Value += this.panStart.X - e.X;
        //    }

        //    if (this.panStart.Y - e.Y + this._panel.VerticalScroll.Value < 0)
        //    {
        //        this._panel.VerticalScroll.Value = 0;
        //    }
        //    else if (this.panStart.Y - e.Y + this._panel.VerticalScroll.Value > this._panel.VerticalScroll.Maximum)
        //    {
        //        this._panel.VerticalScroll.Value = this._panel.VerticalScroll.Maximum;
        //    }
        //    else
        //    {
        //        this._panel.VerticalScroll.Value += this.panStart.Y - e.Y;
        //    }
        //}

        private void _schematic_MouseDown(object sender, MouseEventArgs e)
        {
            if ((e.Button == MouseButtons.Right) && (this.isOnPan == false))
            {
                this.isOnPan = true;
                this._schematic.Cursor = Cursors.NoMove2D;
                this.panStart = e.Location;
            }
        }

        private void _openPropertyForm()
        {
            this._hScrollMemoryOpenPropform = this._panel.HorizontalScroll.Value;
            this._vScrollMemoryOpenPropform = this._panel.VerticalScroll.Value;
        }

        private void _closePropertyForm()
        {
            this._panel.AutoScrollPosition = new Point(this._hScrollMemoryOpenPropform, this._vScrollMemoryOpenPropform);
        }
        
        public void _BSBGLTab_Enter(object sender, EventArgs e)
        {
            this._panel.AutoScrollPosition = new Point(this._hScrollMemory, this._vScrollMemory);
            this._panel.Invalidate();
        }
        
        private void _schematic_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollOrientation == ScrollOrientation.HorizontalScroll)
                this._hScrollMemory = e.NewValue;
            else
                this._vScrollMemory = e.NewValue;
        }

        private void _schematic_MouseWheel(object sender, MouseEventArgs e)
        {
            if ((ModifierKeys & Keys.Control) == Keys.Control)
            {
                if (e.Delta > 0)
                {
                    this._schematic.ZoomInEvent();
                }
                else
                {
                    this._schematic.ZoomOutEvent();
                }
            }
        }

        private void _panel_MouseWheel(object sender, MouseEventArgs e)
        {
            this._hScrollMemory = this._panel.HorizontalScroll.Value;
            this._vScrollMemory = this._panel.VerticalScroll.Value;

        }

        public void RemoveDeletedBlocksFromLists()
        {
            this._schematic.RemoveDeletedBlocksFromLists();
        }

        public void UpdateLists()
        {
            this._schematic.UpdateLists();
        }

        private void OnPreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Delete:

                    this._schematic.DeleteEvent();

                    break;
                case Keys.C:
                    if (e.Control)
                    {
                        this._schematic.CopyFromXML();
                    }
                    break;
                case Keys.V:
                    if (e.Control)
                    {
                        if (this.isCopy)
                        {
                            this._schematic.PasteFromXML();
                            this.isCopy = false;
                            break;
                        }
                        this._schematic.CopyFromXML();
                        this._schematic.PasteFromXML();
                    }
                    break;
                case Keys.D:
                    if (e.Control)
                    {
                        this._schematic.CopyFromXML();
                        this._schematic.PasteFromXML();
                    } break;                  
                case Keys.Z:
                    if (e.Control)
                    {
                        this._schematic.Undo();
                    }
                    break;
                case Keys.Y:
                    if (e.Control)
                    {
                        this._schematic.Redo();
                    }
                    break;
                case Keys.A:
                    if (e.Control)
                    {
                        this._schematic.SelectAll();
                    }
                    break;

                case Keys.X:
                    if (e.Control)
                    {
                        this.isCopy = true;
                        this._schematic.CutFromXML();
                    }
                    break;
            }
        }

        protected ToolStrip CreateToolStrip()
        {
            ToolStrip mainToolStrip = new ToolStrip();
            ToolStripButton zoominToolStripButton = new ToolStripButton();
            ToolStripButton zoomoutToolStripButton = new ToolStripButton();
            ToolStripButton wireToolStripButton = new ToolStripButton();
            ToolStripButton deleteToolStripButton = new ToolStripButton();

            mainToolStrip.Items.AddRange(new ToolStripItem[]
            {
                zoominToolStripButton,
                zoomoutToolStripButton,
                wireToolStripButton,
                deleteToolStripButton
            });
            mainToolStrip.Location = new Point(0, 0);
            mainToolStrip.Name = "MainToolStrip";
            mainToolStrip.Size = new Size(816, 25);
            mainToolStrip.TabIndex = 0;
            mainToolStrip.Text = "toolStrip1";

            zoominToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            zoominToolStripButton.Image = AssemblyResource.Resource1.zoomin;
            zoominToolStripButton.ImageTransparentColor = Color.Magenta;
            zoominToolStripButton.Name = "zoominToolStripButton";
            zoominToolStripButton.Size = new Size(23, 22);
            zoominToolStripButton.Text = "����������";
            zoominToolStripButton.Click += new EventHandler(_schematic.ZoomInEvent);

            zoomoutToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            zoomoutToolStripButton.Image = AssemblyResource.Resource1.zoomout;
            zoomoutToolStripButton.ImageTransparentColor = Color.Magenta;
            zoomoutToolStripButton.Name = "zoomoutToolStripButton";
            zoomoutToolStripButton.Size = new Size(23, 22);
            zoomoutToolStripButton.Text = "��������";
            zoomoutToolStripButton.Click += new EventHandler(_schematic.ZoomOutEvent);

            wireToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            wireToolStripButton.Image = AssemblyResource.Resource1.Wire;
            wireToolStripButton.ImageTransparentColor = Color.Magenta;
            wireToolStripButton.Name = "wireToolStripButton";
            wireToolStripButton.Size = new Size(23, 22);
            wireToolStripButton.Text = "�������� �����";
            wireToolStripButton.Click += new EventHandler(_schematic.AddLineEvent);
            
            deleteToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            deleteToolStripButton.Image = AssemblyResource.Resource1.delete;
            deleteToolStripButton.ImageTransparentColor = Color.Magenta;
            deleteToolStripButton.Name = "wireToolStripButton";
            deleteToolStripButton.Size = new Size(23, 22);
            deleteToolStripButton.Text = "�������";
            deleteToolStripButton.Click += new EventHandler(this._schematic.DeleteEvent);

            mainToolStrip.Dock = DockStyle.Left;
            return mainToolStrip;
        }

    }
}
