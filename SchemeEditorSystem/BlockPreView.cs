using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using SchemeEditorSystem.descriptors;
using SchemeEditorSystem.BlockS;

//namespace SchemeEditorSystem
//{
    public class BlockPreView : Panel
    {
        bool draw;

        private float DrawScale;
        private int DrawAllign;

        //private Form parentForm;
        private BufferedGraphicsContext context;
        private BufferedGraphics grafx;
        private BufferedGraphics Back;


        BlockDesc desc;
        //Type and name of the block
        string name;
        string type;

        //the list of contacts
        List<Pin> pinList = new List<Pin>();

        //the max contact number. Is used for calculating the height of the block
        int pinMaxNum;

        //the width and heigth of the block
        int width;
        int heigth;

        //the allign and scale of the scheme


        //the size and longitudal koef of type(print)
        float typeSize;
        float typeKoef;

        //the position of element
        int posX;
        int posY;

        //the flag set to true if element is selected
      

        #region initialization

        public BlockPreView()
        {
            InitSchematicSystem();
        }

        private void InitSchematicSystem()
        {
            this.BackColor = Color.White;
            this.BorderStyle = BorderStyle.Fixed3D;
            this.Cursor = Cursors.Cross;
            this.ImeMode = ImeMode.Close;
            this.Location = new Point(8, 10);
            this.Name = "DisplayWindow";
            this.Size = new Size(381, 318);
            this.TabIndex = 0;
            this.InitVariables();
            this.Paint += new PaintEventHandler(this.DrawSheetPanel_Paint);
            this.Resize += new EventHandler(this.ResizeSheet);
        }
        
        private void InitVariables()
        {
            DrawScale = 1;
            DrawAllign = 10;
            width = DrawAllign*10;
            heigth = DrawAllign*10;
            ResizeDrawSheet();
            RecalCulateBuffer();
            this.UpdateSchematic();
        }

        #endregion

        #region Graphic Functions
        private void ResizeSheet(object sender, EventArgs e)
        {
            // System.Drawing.Graphics gr = this.CreateGraphics(); // ������ ������ ��� �������        
            RedrawSchematic();
        }

        private void RecalCulateBuffer()
        {
            // Re-create the graphics buffer for a new window size.
            context = BufferedGraphicsManager.Current;
            context.MaximumBuffer = new Size(this.Width , this.Height );
            if (grafx != null)
            {
                grafx.Dispose();
                grafx = null;
            }
            grafx = context.Allocate(this.CreateGraphics(),
                new Rectangle(0, 0, this.Width, this.Height));
            
            context.MaximumBuffer = new Size(this.Width , this.Height);
            if (Back != null)
            {
                Back.Dispose();
                Back = null;
            }
            Back = context.Allocate(this.CreateGraphics(),
                new Rectangle(0, 0, this.Width , this.Height));
            DrawGrid(Back.Graphics);
        }

        public void UpdateSchematic()
        {
            Back.Render(grafx.Graphics);
            if (draw)
            {
                if (desc.group != "�������")
                    this.Draw(grafx.Graphics, System.Drawing.Brushes.Black, 0, 0);
                else
                    InternalDrawInOut(grafx.Graphics, System.Drawing.Brushes.Black, 0, 0);
            }
            
        }
        public void RedrawSchematic()
        {
         
            System.Drawing.Graphics gr = this.CreateGraphics();
            ResizeDrawSheet();
            RecalCulateBuffer();

            UpdateSchematic();
            grafx.Render(gr);
        }

        private void DrawGrid(Graphics g)
        {
            g.FillRectangle(Brushes.White, 0, 0, this.Width, this.Height);
        }

        private void DrawSheetPanel_Paint(object sender, PaintEventArgs e)
        {
            RedrawSchematic();
            grafx.Render(e.Graphics);
        }

        private void ResizeDrawSheet()
        {
            if (((float)(this.Height) / (float)heigth) > ((float)(this.Width) / (float)width))
            {
                DrawScale = ((float)(this.Width) / (float)(width + 2 * DrawAllign));
                posX = (int)(((float)((float)(this.Width) / DrawScale) - (float)(width + 2 * DrawAllign)) / 2);
                posY = (int)(((float)((float)(this.Height) / DrawScale) - (float)(heigth + 2 * DrawAllign)) / 2);
 //               AllignNumber(ref posY);
            }
            else
            {
                DrawScale = ((float)(this.Height) / (float)(heigth + 2 * DrawAllign));
                posX = (int)(((float)((float)(this.Width) / DrawScale) - (float)(width + 2 * DrawAllign)) / 2);
                posY = (int)(((float)((float)(this.Height) / DrawScale) - (float)(heigth + 2 * DrawAllign)) / 2);
 //               AllignNumber(ref posX);
            } 
            RecalCulateBuffer();
        }

        private void Draw(System.Drawing.Graphics gr, System.Drawing.Brush color, int relX, int relY)
        {
                System.Drawing.StringFormat tempStrFormat = new System.Drawing.StringFormat();
                // Rectangle Drawing
                gr.DrawRectangle(new System.Drawing.Pen(color),
                                        (posX + relX + DrawAllign) * DrawScale,
                                        (posY + relY + DrawAllign) * DrawScale,
                                        (width) * DrawScale,
                                        (heigth) * DrawScale); // ������
                //Pin Drawing
                foreach (Pin i in pinList)
                {
                    InternalDrawPin(gr, i, color, relX, relY);
                }

                tempStrFormat.Alignment = System.Drawing.StringAlignment.Center;
                /*** Name Drawing ***/
               //gr.DrawString(name, new System.Drawing.Font(System.Drawing.FontFamily.GenericSansSerif,
                 //   typeSize * DrawScale), color, 0.5f * (2 * (posX + relX) + width + 2 * DrawAllign) * DrawScale, (posY + relY) * DrawScale, tempStrFormat);

                tempStrFormat.Alignment = System.Drawing.StringAlignment.Center;
                /*** Type Drawing ***/
                gr.DrawString(type, new System.Drawing.Font(System.Drawing.FontFamily.GenericSansSerif,
                    typeSize * DrawScale), color, 0.5f * (2 * (posX + relX) + width + 2 * DrawAllign) * DrawScale, (posY + relY + DrawAllign) * DrawScale, tempStrFormat);

           
        }

        private void InternalDrawInOut(System.Drawing.Graphics gr, System.Drawing.Brush color, int relX, int relY)
        {
            foreach (Pin i in pinList)
            {
                InternalDrawPinInOut(gr, i, color, relX, relY);
            }
            System.Drawing.StringFormat tempStrFormat = new System.Drawing.StringFormat();
            tempStrFormat.Alignment = System.Drawing.StringAlignment.Center;
            /*** Name Drawing ***/
            gr.DrawString(name, new System.Drawing.Font(System.Drawing.FontFamily.GenericSansSerif,
                typeSize * DrawScale), color, 0.5f * (2 * (posX + relX) + width + 2 * DrawAllign) * DrawScale, (posY + relY + 0.5f * DrawAllign) * DrawScale, tempStrFormat);

        }

        private void InternalDrawPinInOut(System.Drawing.Graphics gr, Pin inpPin, System.Drawing.Brush color, int relX, int relY)
        {
            System.Drawing.StringFormat tempStrFormat = new System.Drawing.StringFormat();

            //calculating local position of Pin
            int tempPinPoseX = 0;
            int tempPinPoseY = posY + DrawAllign * (inpPin.Position + 1);

            if (inpPin.Orientation == PinOrientation.PO_LEFT)
            {
                //calculating local position of Pin
                tempPinPoseX = posX;


                /*** Rectangle Drawing with pin***/

                gr.DrawLine(new System.Drawing.Pen(color), DrawScale * (posX + relX + 4 * DrawAllign), DrawScale * (tempPinPoseY + relY - 0.5f * DrawAllign),
                                 DrawScale * (posX + relX + 4 * DrawAllign), DrawScale * (tempPinPoseY + relY + 0.5f * DrawAllign));
                gr.DrawLine(new System.Drawing.Pen(color), DrawScale * (posX + relX + 4 * DrawAllign), DrawScale * (tempPinPoseY + relY - 0.5f * DrawAllign),
                                DrawScale * (posX + relX + 2 * DrawAllign), DrawScale * (tempPinPoseY + relY - 0.5f * DrawAllign));
                gr.DrawLine(new System.Drawing.Pen(color), DrawScale * (posX + relX + 4 * DrawAllign), DrawScale * (tempPinPoseY + relY + 0.5f * DrawAllign),
                                DrawScale * (posX + relX + 2 * DrawAllign), DrawScale * (tempPinPoseY + relY + 0.5f * DrawAllign));
                gr.DrawLine(new System.Drawing.Pen(color), DrawScale * (posX + relX + 2 * DrawAllign), DrawScale * (tempPinPoseY + relY - 0.5f * DrawAllign),
                                DrawScale * (posX + relX + DrawAllign), DrawScale * (tempPinPoseY + relY));
                gr.DrawLine(new System.Drawing.Pen(color), DrawScale * (posX + relX + 2 * DrawAllign), DrawScale * (tempPinPoseY + relY + 0.5f * DrawAllign),
                                DrawScale * (posX + relX + DrawAllign), DrawScale * (tempPinPoseY + relY));

                //Drawing Pin name
                tempStrFormat.Alignment = System.Drawing.StringAlignment.Near;
                try
                {
                    gr.DrawString(desc.userData["inOutName"], new System.Drawing.Font(System.Drawing.FontFamily.GenericSansSerif, typeSize * DrawScale),
                        color, (tempPinPoseX + relX + 2 * DrawAllign) * DrawScale + 10, (tempPinPoseY - (0.3f * DrawAllign) + relY) * DrawScale - 5, tempStrFormat);
                }
                catch { }

            }
            else
            {
                //calculating local position of Pin
                tempPinPoseX = posX + width + DrawAllign;

                /*** Rectangle Drawing with pin***/

                gr.DrawLine(new System.Drawing.Pen(color), DrawScale * (posX + relX + DrawAllign), DrawScale * (tempPinPoseY + relY - 0.5f * DrawAllign),
                                 DrawScale * (posX + relX + DrawAllign), DrawScale * (tempPinPoseY + relY + 0.5f * DrawAllign));
                gr.DrawLine(new System.Drawing.Pen(color), DrawScale * (posX + relX + DrawAllign), DrawScale * (tempPinPoseY + relY - 0.5f * DrawAllign),
                                DrawScale * (posX + relX + 3 * DrawAllign), DrawScale * (tempPinPoseY + relY - 0.5f * DrawAllign));
                gr.DrawLine(new System.Drawing.Pen(color), DrawScale * (posX + relX + DrawAllign), DrawScale * (tempPinPoseY + relY + 0.5f * DrawAllign),
                                DrawScale * (posX + relX + 3 * DrawAllign), DrawScale * (tempPinPoseY + relY + 0.5f * DrawAllign));
                gr.DrawLine(new System.Drawing.Pen(color), DrawScale * (posX + relX + 3 * DrawAllign), DrawScale * (tempPinPoseY + relY - 0.5f * DrawAllign),
                                DrawScale * (posX + relX + 4 * DrawAllign), DrawScale * (tempPinPoseY + relY));
                gr.DrawLine(new System.Drawing.Pen(color), DrawScale * (posX + relX + 3 * DrawAllign), DrawScale * (tempPinPoseY + relY + 0.5f * DrawAllign),
                                DrawScale * (posX + relX + 4 * DrawAllign), DrawScale * (tempPinPoseY + relY));
                //Drawing Pin name
                tempStrFormat.Alignment = System.Drawing.StringAlignment.Far;
                try
                {
                    gr.DrawString(desc.userData["inOutName"], new System.Drawing.Font(System.Drawing.FontFamily.GenericSansSerif, typeSize * DrawScale),
                        color, (tempPinPoseX + relX - DrawAllign) * DrawScale - 10, (tempPinPoseY - (0.3f * DrawAllign) + relY) * DrawScale - 5, tempStrFormat);
                }
                catch { }
            }

            //Drawing Pin Line             
            gr.DrawLine(new System.Drawing.Pen(color), (tempPinPoseX + relX) * DrawScale, (tempPinPoseY + relY) * DrawScale, (tempPinPoseX + relX + DrawAllign) * DrawScale, (tempPinPoseY + relY) * DrawScale);

        }

        private void InternalDrawPin(System.Drawing.Graphics gr, Pin inpPin, System.Drawing.Brush color, int relX, int relY)
        {
            System.Drawing.StringFormat tempStrFormat = new System.Drawing.StringFormat();

            //calculating local position of Pin
            int tempPinPoseX = 0;
            int tempPinPoseY = posY + DrawAllign * (inpPin.Position + 1);

            if (inpPin.Orientation == PinOrientation.PO_LEFT)
            {
                //calculating local position of Pin
                tempPinPoseX = posX;

                //Drawing inverse circle
                if (inpPin.PinType == PinType.PT_INVERSE)
                    gr.DrawEllipse(new System.Drawing.Pen(color), (tempPinPoseX + relX + 0.8f * DrawAllign) * DrawScale,
                        (tempPinPoseY + relY - 0.2f * DrawAllign) * DrawScale, 0.4f * DrawAllign * DrawScale, 0.4f * DrawAllign * DrawScale);

                //Drawing Pin name
                tempStrFormat.Alignment = System.Drawing.StringAlignment.Near;
                gr.DrawString(inpPin.PinName, new System.Drawing.Font(System.Drawing.FontFamily.GenericSansSerif, typeSize * DrawScale),
                    color, (tempPinPoseX + relX + DrawAllign) * DrawScale, (tempPinPoseY - (0.3f * DrawAllign) + relY) * DrawScale, tempStrFormat);

            }
            else
            {
                //calculating local position of Pin
                tempPinPoseX = posX + width + DrawAllign;

                //Drawing inverse circle
                if (inpPin.PinType == PinType.PT_INVERSE)
                    gr.DrawEllipse(new System.Drawing.Pen(color), (tempPinPoseX + relX - 0.2f * DrawAllign) * DrawScale,
                        (tempPinPoseY + relY - 0.2f * DrawAllign) * DrawScale, 0.4f * DrawAllign * DrawScale, 0.4f * DrawAllign * DrawScale);
                //Drawing Pin name
                tempStrFormat.Alignment = System.Drawing.StringAlignment.Far;
                gr.DrawString(inpPin.PinName, new System.Drawing.Font(System.Drawing.FontFamily.GenericSansSerif, typeSize * DrawScale),
                    color, (tempPinPoseX + relX) * DrawScale, (tempPinPoseY - (0.3f * DrawAllign) + relY) * DrawScale, tempStrFormat);
            }

            //Drawing Pin Line             
            gr.DrawLine(new System.Drawing.Pen(color), (tempPinPoseX + relX) * DrawScale, (tempPinPoseY + relY) * DrawScale, (tempPinPoseX + relX + DrawAllign) * DrawScale, (tempPinPoseY + relY) * DrawScale);

        }

        private void AllignNumber(ref int x)
        {
            int tempX = x;
            x = (int)((DrawAllign * Math.Round((double)(tempX) / (double)(DrawAllign))));
        }

        private void UpdateFromDesc()
        {
            type = desc.TypeElem;
            name = desc.name;

            typeSize = 0.5f * DrawAllign;
            typeKoef = 0.7f;
            pinMaxNum = 0;

            //position setting

            AllignNumber(ref desc.posX);
            AllignNumber(ref desc.posY);

            //posX = desc.posX;
            //posY = desc.posY;
            pinList.Clear();
            //Pins creation and calculating max Pin Number
            foreach (PinDesc i in desc.listPinDesc)
            {
                if (i.Position > pinMaxNum) pinMaxNum = i.Position;

                pinList.Add(new Pin(i,desc.posX, desc.posY));
            }

            //heigth calc
            heigth = (int)((pinMaxNum + 1) * DrawAllign);


            //Calculating max pin name length (for width calc)
            int maxPinNameLength = 0;
            foreach (Pin i in pinList)
            {
                if (i.PinName.Length > maxPinNameLength) maxPinNameLength = i.PinName.Length;
            }

            //width calc
            if (2 * DrawAllign < type.Length * typeSize * typeKoef)
            {
                width = (int)((DrawAllign * Math.Ceiling((Double)(type.Length * typeSize * typeKoef / DrawAllign))));
            }
            else
            {
                width = 2 * DrawAllign;
            }

            if (2 * maxPinNameLength * typeSize * typeKoef > width)
            {
                width = (int)((DrawAllign * Math.Ceiling((Double)((2 * maxPinNameLength * typeSize * typeKoef) / DrawAllign))));
            }
            if (desc.group == "�������")
            {
                width = 3 * DrawAllign;
            }
        }

        #endregion

        #region External interface

        public void SetViewElement(BlockDesc descriptor)
        {
            draw = true;
            desc = descriptor;
            UpdateFromDesc();
            RedrawSchematic();
        }

        #endregion
    }
//}
