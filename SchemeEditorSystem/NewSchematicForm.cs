using System;
using System.Windows.Forms;

namespace SchemeEditorSystem
{

    public partial class NewSchematicForm : Form
    {
        private string _nameOfSchema; 
        public SheetFormat sheetFormat;   
        public NewSchematicForm()
        {
            InitializeComponent();
            OkButton.DialogResult = DialogResult.OK;
            CancelButton.DialogResult = DialogResult.Cancel;
            sheetFormatCombo.SelectedIndex = 0;
        }

        public string NameOfSchema
        {
            get { return _nameOfSchema; }
            set { _nameOfSchema = value; }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            _nameOfSchema = this.SourceNameTextBox.Text;
            if (_nameOfSchema.Trim() == "")
            {
                _nameOfSchema = "�����";
            }
            switch(sheetFormatCombo.SelectedIndex)
            {
                case 0: sheetFormat = SheetFormat.A4_L; break;
                case 1: sheetFormat = SheetFormat.A4_P; break;
                case 2: sheetFormat = SheetFormat.A3_L; break;
                case 3: sheetFormat = SheetFormat.A3_P; break;
                case 4: sheetFormat = SheetFormat.A2_L; break;
                case 5: sheetFormat = SheetFormat.A2_P; break;
                case 6: sheetFormat = SheetFormat.A1_L; break;
                case 7: sheetFormat = SheetFormat.A1_P; break;
                case 8: sheetFormat = SheetFormat.A0_L; break;
                case 9: sheetFormat = SheetFormat.A0_P; break;
                default: sheetFormat = SheetFormat.A4_L; break;
            }

            this.Hide();
        }
    }
}