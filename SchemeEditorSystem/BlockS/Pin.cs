using System.Xml;
using SchemeEditorSystem.descriptors;
using SchemeEditorSystem.LineS;

namespace SchemeEditorSystem.BlockS
{
    /// <summary>
    /// ����� ����� ������
    /// </summary>
    public class Pin : LnkPoint
    {
        /// <summary>
        /// ����������� ������ �������� ������
        /// </summary>
        /// <param name="descriptor">�������� �������</param>
        /// <param name="x">���������� �</param>
        /// <param name="y">���������� �</param>
        /// <param name="allign">������</param>
        /// <param name="scale">�������</param>
        public Pin(PinDesc descriptor, int x, int y, int allign = 5, float scale = 1) : base(x, y, allign, scale)
        {
            this.VarName = "";
            this.PinType = descriptor.PinType;
            this.Orientation = descriptor.Orientation;
            this.Position = descriptor.Position;
            this.PinName = descriptor.PinName;
            this.Priority = 0;
            AddingPoint = false;
        }

        #region Properties

        /// <summary>
        /// �������� ������
        /// </summary>
        public string PinName { get; set; }

        /// <summary>
        /// ������� ������
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// ��� ������
        /// </summary>
        public PinType PinType { get; set; }

        /// <summary>
        /// ��������� ������
        /// </summary>
        public PinOrientation Orientation { get; set; }
        /// <summary>
        /// ����������� �� ��� ����� �� ������ ������
        /// </summary>
        public bool Moved { get; set; }
        /************************************************************************/
        /* compile/analyse parameters                                           */
        /************************************************************************/
        /// <summary>
        /// ��������� ����������
        /// </summary>
        public int Priority { get; set; }
        /// <summary>
        /// ��� ���������� �����
        /// </summary>
        public string VarName { get; set; }
        /// <summary>
        /// �������� ����������
        /// </summary>
        public int Value { get; set; }

        #endregion

        /// <summary>
        /// ��������� �������� �����
        /// </summary>
        /// <param name="pinDesc">�������� �������</param>
        public void SetNewDescription(PinDesc pinDesc)
        {
            PinType = pinDesc.PinType;
            Orientation = pinDesc.Orientation;
            Position = pinDesc.Position;
            PinName = pinDesc.PinName;
        }
        
        /// <summary>
        /// ���������� ������� ������ � ���� XML
        /// </summary>
        /// <param name="writer">XML-������</param>
        public void PropertiesToXml(XmlTextWriter writer)
        {
            writer.WriteStartElement("Pin");
            writer.WriteAttributeString("position", Position.ToString());
            writer.WriteAttributeString("name", PinName);
            writer.WriteAttributeString("type", PinType == PinType.PT_DIRECT ? "direct" : "inverse");
            writer.WriteAttributeString("orient", Orientation == PinOrientation.PO_LEFT ? "left" : "right");
            writer.WriteAttributeString("addingPoint", AddingPoint.ToString());
            writer.WriteEndElement();
        }

        public static PinType GetType(string type)
        {
            switch (type)
            {
                case "direct": return PinType.PT_DIRECT;
                case "inverse": return PinType.PT_INVERSE;
                default: return PinType.PT_DIRECT;
            }
        }

        public static PinOrientation GetPinOrientation(string orientation)
        {
            switch (orientation)
            {
                case "left": return PinOrientation.PO_LEFT;
                case "right": return PinOrientation.PO_RIGHT;
                default: return PinOrientation.PO_LEFT;
            }
        }
    }
}
