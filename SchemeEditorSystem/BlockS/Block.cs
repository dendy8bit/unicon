using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Xml;
using SchemeEditorSystem.descriptors;
using SchemeEditorSystem.LineS;

namespace SchemeEditorSystem.BlockS
{
    /// <summary>
    /// �����, ���������� �� ����� ���������, �� ���������� � ����������
    /// </summary>
    public class Block
    {
        #region Variable declaration

        private BlockDesc _desc;
        //the list of contacts
        private List<Pin> _pinList;
        //the max contact number. Is used for calculating the height of the block
        private int _pinMaxNum;
        //the width and heigth of the block
        private int _width;
        private int _heigth;
        //the allign and scale of the scheme
        private int _allign;
        private float _scale;
        //the size and longitudal koef of type(print)
        private float _typeSize;
        private float _typeKoef;
        //the position of element
        private int _posX;
        private int _posY;
        //the flag set to true if element is selected
        private bool _selected;
        private int _priority;
        
        #endregion

        #region Property
        /// <summary>
        /// ���� ��������� �������
        /// </summary>
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }
        /// <summary>
        /// ������� ����� �� �
        /// </summary>
        public int X
        {
            get { return _posX; }
        }
        /// <summary>
        /// ������� ����� �� �
        /// </summary>
        public int Y
        {
            get { return _posY; }
        }
        /// <summary>
        /// ������ �����
        /// </summary>
        public int Heigth
        {
            get { return _heigth; }
        }
        /// <summary>
        /// ������ �����
        /// </summary>
        public int Width
        {
            get { return _width; }
        }

        public List<Pin> PinList
        {
            get { return this._pinList; }
        }

        public bool Error {  get; set; }

        /// <summary>
        /// ��������� ��������� ����� ��� ����������
        /// </summary>
        public int Priority
        {
            get { return this._priority; }
            set
            {
                if (this._priority < value)
                {
                    this._priority = value;
                    foreach (Pin pin in this._pinList)
                    {
                        if (pin.Priority < value)
                        {
                            pin.Priority = value;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// ��� ���������� ���� ����, ��� ���� ���������������
        /// </summary>
        public bool Analyzed { get; set; }
        #endregion

        #region InitMethods

        public Block(BlockDesc descriptor,int inpAllign,float inpScale)
        {
            this._allign = inpAllign;
            this._scale = inpScale;
            this._pinList = new List<Pin>();
            this._desc = descriptor;
            this._priority = 0;
            this.UpdateFromDescriptor();
        }

        public Block(int allign, float scale)
        {
            this._allign = allign;
            this._scale = scale;
            this._pinList = new List<Pin>();
            this._desc = new BlockDesc();
            this._priority = 0;
        }

        private int AllignNumber(int x)
        {
            return (int)(this._allign * Math.Round(x / (double)(this._allign)));
        }

        public void SetDescCopy(BlockDesc descriptor)
        {
            this._desc = descriptor;
            this.UpdateFromDescriptor();
        }

        public BlockDesc GetDescription()
        {
            return _desc;
        }

        public void UpdateFromDescriptor()
        {
            _typeSize = 0.5f * _allign;
            _typeKoef = 0.7f;
            string[] str = _desc.TypeElem.Split('\n');
            int rowsCount = str.GetLength(0);
            int maxTypeLength = str.Select(i => i.Length).Concat(new[] {0}).Max();
            //position setting
            _posX = AllignNumber(_desc.posX);
            _posY = AllignNumber(_desc.posY);
            _desc.posX = _posX;
            _desc.posY = _posY;
            //Calculating max pin name length (for width calc)
            int maxPinNameLength = _desc.listPinDesc.Select(i => i.PinName.Length).Concat(new[] { 0 }).Max();
            //width calc
            if (2 * _allign < maxTypeLength * _typeSize * _typeKoef)
            {
                _width = (int)((_allign * Math.Ceiling(maxTypeLength * this._typeSize * this._typeKoef / this._allign)));
            }
            else
            {
                _width = 2 * _allign;
            }

            if (2 * maxPinNameLength * _typeSize * _typeKoef > _width)
            {
                _width = (int)(this._allign * Math.Ceiling(2 * maxPinNameLength * this._typeSize * this._typeKoef / this._allign));
            }
            if (_desc.group == "�������")
            {
                _width = 3 * _allign;
            }
            //Pins creation and calculating max Pin Number
            if (_desc.listPinDesc.Count != 0)
            {
                _pinMaxNum = _desc.listPinDesc.Max(p => p.Position);
                List<Pin> inputs = _pinList.Where(p => p.Orientation == PinOrientation.PO_LEFT).Select(p => p).ToList();
                List<Pin> outputs = _pinList.Where(p => p.Orientation == PinOrientation.PO_RIGHT).Select(p => p).ToList();
                List<PinDesc> descInputs =
                    _desc.listPinDesc.Where(p => p.Orientation == PinOrientation.PO_LEFT).Select(p => p).ToList();
                List<PinDesc> descOutputs =
                    _desc.listPinDesc.Where(p => p.Orientation == PinOrientation.PO_RIGHT).Select(p => p).ToList();
                _pinList.Clear();
                SetPins(inputs, descInputs);
                SetPins(outputs, descOutputs);
                _pinList.AddRange(outputs);
                _pinList.AddRange(inputs);
            }
            //heigth calc
            float temp = rowsCount*0.81f;
            if ((temp-1) > _pinMaxNum)
            {
                _heigth = AllignNumber((int)((temp) * _allign));
            }
            else
            {
                _heigth = (this._pinMaxNum + 1) * this._allign;
            }
        }

        private void SetPins(List<Pin> pinsList, List<PinDesc> descList)
        {
            int countPinsDesc = descList.Count;
            int countPins = pinsList.Count;
            if (countPins == 0) // ������� ����� ����
            {
                pinsList.AddRange(descList.Select(desc => desc.Orientation == PinOrientation.PO_LEFT 
                    ? new Pin(desc, _posX, _posY + _allign*(desc.Position + 1), _allign, _scale) 
                    : new Pin(desc, _posX + _width + 2*_allign, _posY + _allign*(desc.Position + 1), _allign, _scale)));
            }
            else if (countPins < countPinsDesc) // �������� ����� �����
            {
                RefreshPin(pinsList, descList, countPins);    // ��������� ������ ������
                for (int i = countPins; i < countPinsDesc;i++) // ��������� �����
                {
                    pinsList.Add(descList[i].Orientation == PinOrientation.PO_LEFT
                        ? new Pin(descList[i], _posX, _posY + _allign * (descList[i].Position + 1), _allign, _scale)
                        : new Pin(descList[i], _posX + _width + 2 * _allign,
                            _posY + _allign * (descList[i].Position + 1), _allign, _scale));
                }
            }
            else if (countPins > countPinsDesc) // ������� �����
            {
                RefreshPin(pinsList, descList, countPinsDesc); // ��������� ������ �� ���������� ��������
                for (int i = countPins - 1; i >= countPinsDesc; i--) //������� � �����, ������� ������ �� ������� ���-��
                {
                    pinsList.Remove(pinsList[i]);
                }
            }
            else if (countPins == countPinsDesc) // ������ �������� ��������
            {
                RefreshPin(pinsList, descList, countPinsDesc); // ��������� ������ �� ���������� ��������               
            }
        }

        private void RefreshPin(List<Pin> pinsList,List<PinDesc> desc, int count)
        {
            for (int i = 0; i < count; i++) 
            {
                pinsList[i].SetNewDescription(desc[i]);
                int x, y;
                if (desc[i].Orientation == PinOrientation.PO_LEFT)
                {
                    x = _posX;
                    y = _posY + _allign * (desc[i].Position + 1);
                }
                else
                {
                    x = _posX + _width + 2 * _allign;
                    y = _posY + _allign * (desc[i].Position + 1);
                }
                pinsList[i].SetPosition(x, y);
            }
        }

        public void SetScale(float inpScale)
        {
            _scale = inpScale;
        }
        /// <summary>
        /// ��������� �� ����� ����� ������ ��������. ���� ������ ����� ���, �� ���������� null
        /// </summary>
        /// <param name="name">��� ������</param>
        /// <returns>����� ������</returns>
        public List<Pin> GetPoint(string name)
        {
            Func<Pin,bool> func = (p => p.PinName.Contains(name) && (p.PinName.IndexOf(name, StringComparison.Ordinal) == 0)
                && (p.PinName.Length == name.Length || p.PinName.Contains(name + " ")));
            var pins = _pinList.Where(func).Select(p => p).ToList();
            return pins.Count == 0 ? null : pins;
        }


        #endregion

        #region Draw methods

        public void Draw(Graphics gr)
        {
            if (_selected)
            {
                gr.DrawRectangle(new Pen(Brushes.Red),
                                  (_posX + 0.5f*_allign)* _scale,
                                  (_posY + 0.5f*_allign) * _scale,
                                  (_width + _allign)* _scale,
                                  (_heigth + _allign)* _scale); // ������
            }
            
            if (_desc.group != "�������")
                if(Error) InternalDraw(gr, Brushes.Red, 0, 0);
                else InternalDraw(gr, Brushes.Black, 0, 0);
            else if (Error) InternalDrawInOut(gr, Brushes.Red, 0, 0);
                else InternalDrawInOut(gr, Brushes.Black, 0, 0);
                
        }

        public void DrawPhantom(Graphics gr,int relX,int relY)
        {
            if (_desc.group != "�������")
                InternalDraw(gr, Brushes.LightBlue, relX, relY);
            else
                InternalDrawInOut(gr, Brushes.LightBlue, relX, relY);
        }

        public void DrawTransparent(Graphics gr)
        {
            if (_desc.group != "�������")
                InternalDraw(gr, Brushes.LightSlateGray, 0, 0);
            else
                InternalDrawInOut(gr, Brushes.LightSlateGray, 0, 0);
        }
        
        private void InternalDraw(Graphics gr,Brush color,int relX,int relY)
        {
            StringFormat tempStrFormat = new StringFormat();
            // Rectangle Drawing
            gr.DrawRectangle(new Pen(color),
                                    (_posX + relX + _allign)*_scale ,
                                    (_posY + relY + _allign) * _scale,
                                    (_width) * _scale,
                                    (_heigth) * _scale); // ������
            //Pin Drawing
            foreach (Pin i in _pinList)
            {
                InternalDrawPin(gr,i, color,relX,relY);
            }
            tempStrFormat.Alignment = StringAlignment.Center;
            /*** Name Drawing ***/
            gr.DrawString(_desc.name, new Font(FontFamily.GenericSansSerif,
                _typeSize*_scale), color, 0.5f*(2*(_posX + relX) + _width + 2*_allign)*_scale, (_posY + relY)*_scale,
                tempStrFormat);

            tempStrFormat.Alignment = StringAlignment.Center;
            /*** Type Drawing ***/
            gr.DrawString(_desc.TypeElem, new Font(FontFamily.GenericSansSerif,
                _typeSize*_scale), color, 0.5f*(2*(_posX + relX) + _width + 2*_allign)*_scale, (_posY + relY + _allign)*_scale,
                tempStrFormat);
        }

        private void InternalDrawInOut(Graphics gr, Brush color, int relX, int relY)
        {
            foreach (Pin i in _pinList)
            {
                InternalDrawPinInOut(gr, i, color, relX, relY);
            }
            StringFormat tempStrFormat = new StringFormat();
            tempStrFormat.Alignment = StringAlignment.Center;
            /*** Name Drawing ***/
            gr.DrawString(_desc.name, new Font(FontFamily.GenericSansSerif,
                _typeSize*_scale), color, 0.5f*(2*(_posX + relX) + _width + 2*_allign)*_scale,
                (_posY + relY + 0.5f*_allign)*_scale, tempStrFormat);      
        }

        private void InternalDrawPinInOut(Graphics gr, Pin inpPin, Brush color, int relX, int relY)
        {
            StringFormat tempStrFormat = new StringFormat();

            //calculating local position of Pin
            int tempPinPoseX = 0;
            int tempPinPoseY = _posY + _allign * (inpPin.Position + 1);

            if (inpPin.Orientation == PinOrientation.PO_LEFT)
            {
                //calculating local position of Pin
                tempPinPoseX = _posX;
                /*** Rectangle Drawing with pin***/
                gr.DrawLine(new Pen(color), _scale*(_posX + relX + 4*_allign),
                    _scale*(tempPinPoseY + relY - 0.5f*_allign),
                    _scale*(_posX + relX + 4*_allign), _scale*(tempPinPoseY + relY + 0.5f*_allign));
                gr.DrawLine(new Pen(color), _scale*(_posX + relX + 4*_allign),
                    _scale*(tempPinPoseY + relY - 0.5f*_allign),
                    _scale*(_posX + relX + 2*_allign), _scale*(tempPinPoseY + relY - 0.5f*_allign));
                gr.DrawLine(new Pen(color), _scale*(_posX + relX + 4*_allign),
                    _scale*(tempPinPoseY + relY + 0.5f*_allign),
                    _scale*(_posX + relX + 2*_allign), _scale*(tempPinPoseY + relY + 0.5f*_allign));
                gr.DrawLine(new Pen(color), _scale*(_posX + relX + 2*_allign),
                    _scale*(tempPinPoseY + relY - 0.5f*_allign),
                    _scale*(_posX + relX + _allign), _scale*(tempPinPoseY + relY));
                gr.DrawLine(new Pen(color), _scale*(_posX + relX + 2*_allign),
                    _scale*(tempPinPoseY + relY + 0.5f*_allign),
                    _scale*(_posX + relX + _allign), _scale*(tempPinPoseY + relY));
                //Drawing Pin name
                tempStrFormat.Alignment = StringAlignment.Near;
                if (inpPin.VarName == "")
                    DrawCross(gr, (tempPinPoseX + relX), tempPinPoseY + relY, 3);
                if(_desc.userData.ContainsKey("inOutName"))
                {
                    gr.DrawString(_desc.userData["inOutName"],
                        new Font(FontFamily.GenericSansSerif, _typeSize*_scale),
                        color, (tempPinPoseX + relX + 2*_allign)*_scale, (tempPinPoseY - (0.3f*_allign) + relY)*_scale,
                        tempStrFormat);
                }
            }
            else
            {
                //calculating local position of Pin
                tempPinPoseX = _posX + _width + _allign;
                /*** Rectangle Drawing with pin***/
                gr.DrawLine(new Pen(color), _scale*(_posX + relX + _allign),
                    _scale*(tempPinPoseY + relY - 0.5f*_allign),
                    _scale*(_posX + relX + _allign), _scale*(tempPinPoseY + relY + 0.5f*_allign));
                gr.DrawLine(new Pen(color), _scale*(_posX + relX + _allign),
                    _scale*(tempPinPoseY + relY - 0.5f*_allign),
                    _scale*(_posX + relX + 3*_allign), _scale*(tempPinPoseY + relY - 0.5f*_allign));
                gr.DrawLine(new Pen(color), _scale*(_posX + relX + _allign),
                    _scale*(tempPinPoseY + relY + 0.5f*_allign),
                    _scale*(_posX + relX + 3*_allign), _scale*(tempPinPoseY + relY + 0.5f*_allign));
                gr.DrawLine(new Pen(color), _scale*(_posX + relX + 3*_allign),
                    _scale*(tempPinPoseY + relY - 0.5f*_allign),
                    _scale*(_posX + relX + 4*_allign), _scale*(tempPinPoseY + relY));
                gr.DrawLine(new Pen(color), _scale*(_posX + relX + 3*_allign),
                    _scale*(tempPinPoseY + relY + 0.5f*_allign),
                    _scale*(_posX + relX + 4*_allign), _scale*(tempPinPoseY + relY));
                //Drawing Pin name
                tempStrFormat.Alignment = StringAlignment.Far;
                if (inpPin.VarName == "")
                    DrawCross(gr, (tempPinPoseX + relX+_allign), tempPinPoseY + relY, 3);
                if (_desc.userData.ContainsKey("inOutName"))
                {
                    gr.DrawString(_desc.userData["inOutName"],
                        new Font(FontFamily.GenericSansSerif, _typeSize*_scale),
                        color, (tempPinPoseX + relX - _allign + 1)*_scale, (tempPinPoseY - (0.3f*_allign) + relY)*_scale,
                        tempStrFormat);
                }
            }
            //Drawing Pin Line             
            gr.DrawLine(new Pen(color), (tempPinPoseX + relX)*_scale, (tempPinPoseY + relY)*_scale,
                (tempPinPoseX + relX + _allign)*_scale, (tempPinPoseY + relY)*_scale);
        }

        private void DrawCross(Graphics gr, int inpX, int inpY, int size)
        {
            gr.DrawLine(Pens.Red,
                (inpX)*_scale - size,
                (inpY)*_scale - size,
                (inpX)*_scale + size,
                (inpY)*_scale + size);
            gr.DrawLine(Pens.Red,
                (inpX)*_scale + size,
                (inpY)*_scale - size,
                (inpX)*_scale - size,
                (inpY)*_scale + size);
        }
        
        private void InternalDrawPin(Graphics gr, Pin inpPin, Brush color, int relX,
            int relY)
        {
            StringFormat tempStrFormat = new StringFormat();
            if (inpPin.Orientation == PinOrientation.PO_LEFT)
            {
                //Drawing inverse circle
                if (inpPin.PinType == PinType.PT_INVERSE)
                    gr.DrawEllipse(new Pen(color), (inpPin.X + relX + 0.8f*_allign)*_scale,
                        (inpPin.Y + relY - 0.2f*_allign)*_scale, 0.4f*_allign*_scale, 0.4f*_allign*_scale);
                //Drawing Pin name
                tempStrFormat.Alignment = StringAlignment.Near;
                gr.DrawString(inpPin.PinName,
                    new Font(FontFamily.GenericSansSerif, _typeSize*_scale),
                    color, (inpPin.X + relX + _allign) * _scale, (inpPin.Y - (0.3f * _allign) + relY) * _scale,
                    tempStrFormat);
                if (inpPin.VarName == "")
                    DrawCross(gr, (inpPin.X + relX), inpPin.Y + relY, 3);
                //Drawing Pin Line             
                gr.DrawLine(new Pen(color), (inpPin.X + relX) * _scale, (inpPin.Y + relY) * _scale,
                    (inpPin.X + relX + _allign) * _scale, (inpPin.Y + relY) * _scale);
            }
            else
            {
                //Drawing inverse circle
                if (inpPin.PinType == PinType.PT_INVERSE)
                    gr.DrawEllipse(new Pen(color), (inpPin.X + relX - 1.2f * _allign) * _scale,
                        (inpPin.Y + relY - 0.2f * _allign) * _scale, 0.4f * _allign * _scale, 0.4f * _allign * _scale);
                //Drawing Pin name
                tempStrFormat.Alignment = StringAlignment.Far;
                gr.DrawString(inpPin.PinName,
                    new Font(FontFamily.GenericSansSerif, _typeSize*_scale),
                    color, (inpPin.X + relX - _allign) * _scale, (inpPin.Y - (0.3f * _allign) + relY) * _scale, tempStrFormat);
                if (inpPin.VarName == "")
                    DrawCross(gr, (inpPin.X + relX), inpPin.Y + relY, 3);
                //Drawing Pin Line             
                gr.DrawLine(new Pen(color), (inpPin.X + relX) * _scale, (inpPin.Y + relY) * _scale,
                    (inpPin.X + relX - _allign) * _scale, (inpPin.Y + relY) * _scale);
            }      
        }

        #endregion

        #region Action Methods

        public bool IsOnBlock(int x, int y)
        {
            return x > (_posX + _allign) && y > (_posY + _allign) &&
                   x < (_posX + _allign + _width) && y < (_posY + _allign + _heigth);
        }

        public bool IsInArea(int ltX, int ltY, int rlX, int rlY)
        {
            if (ltX < (_posX + _allign) &&
                ltY < (_posY + _allign) &&
                rlX > (_posX + _allign + _width) &&
                rlY > (_posY + _allign + _heigth)
                ) return true;
            return false;
        }

        public void RefreshPinMoved()
        {
            foreach (var pin in _pinList)
            {
                pin.Moved = false;
            }
        }

        public void CheckOutOfRangeBlocks(int relX, int relY, int sizeX, int sizeY, out bool check)
        {
            int x = AllignNumber(_posX + relX);
            int y = AllignNumber(_posY + relY);
            if (y >= sizeY - 15 || x >= sizeX - 15 || y < 0 || x < 0)
            {
                check = false;
            }
            else
            {
                check = true;
            }
        }

        public void Move(int relX, int relY, int sizeX, int sizeY)
        {
            _posX = AllignNumber(_posX + relX);
            _posY = AllignNumber(_posY + relY);
           
            //if (_posY > sizeY - 20 || _posX >= sizeX - 30 || _posY < 0 || _posX < 0)
            //{
            //    _posX = _desc.posX;
            //    _posY = _desc.posY;

            //    foreach (Pin p in _pinList)
            //    {
            //        p.X = AllignNumber(p.X);
            //        p.Y = AllignNumber(p.Y);
            //        p.Moved = true;
            //    }

            //    _desc.posX = _posX;
            //    _desc.posY = _posY;
                
            //    return;
            //}

            _desc.posX = _posX;
            _desc.posY = _posY;

            foreach (Pin p in _pinList)
            {
                p.X = AllignNumber(p.X + relX);
                p.Y = AllignNumber(p.Y + relY);
                p.Moved = true;
            }
        }
        /// <summary>
        /// ������� ����� ���������� ����� � ������ �����, �������� ����� ���� ����� ������� �����
        /// </summary>
        /// <param name="connections">������ ����� �����</param>
        public void RemovePinConnections(List<Connection> connections)
        {
            foreach (var connection in connections)
            {
                foreach (var pin in _pinList)
                {
                    var delPin = connection.LnkPointList.FirstOrDefault(p => p == pin);
                    if (delPin == null) continue;
                    LnkPoint newPoint = new LnkPoint(delPin.X, delPin.Y, this._allign, this._scale);
                    newPoint.LastPoint = delPin.LastPoint;
                    newPoint.Selected = delPin.Selected;
                    newPoint.NearDotsList.AddRange(delPin.NearDotsList);
                    foreach (var near in newPoint.NearDotsList)
                    {
                        near.NearDotsList.Remove(delPin);
                        near.NearDotsList.Add(newPoint);
                    }
                    connection.LnkPointList.Add(newPoint);
                    connection.LnkPointList.Remove(delPin);
                }
            }
        }

        public override string ToString()
        {
            return this._desc.name;
        }

        #endregion

        #region Compile methods

        /// <summary>
        /// ������������� ��� ������ ���������� ���������� � ������������
        /// ������� ������ � ������� ���������� ������ �����
        /// </summary>
        /// <param name="line">����� �����</param>
        /// <param name="inOut">���������� ������ � ������� �����</param>
        /// <returns>���� ������ 2� ������ ��� ��� ����� ������, ���������� true</returns>
        public void SetVarNames(Connection line, int[] inOut)
        {
            foreach (Pin pin in _pinList)
            {
                List<LnkPoint> points = line.LnkPointList.Where(lp => lp.X == pin.X && lp.Y == pin.Y).Select(lp => lp).ToList();
                if (points.Count == 0) continue;
                if(points.Contains(pin))
                {
                    pin.VarName = line.VarName;
                }
                else
                {
                    LnkPoint point = points[0];
                    pin.VarName = line.VarName;
                    pin.LastPoint = point.LastPoint;
                    pin.Scale = point.Scale;
                    pin.NearDotsList.Clear();
                    pin.NearDotsList.AddRange(point.NearDotsList);
                    foreach (LnkPoint near in pin.NearDotsList)
                    {
                        near.NearDotsList.Add(pin);
                        near.NearDotsList.Remove(point);
                    }
                    line.LnkPointList.Remove(point);
                    line.LnkPointList.Add(pin);
                }
                if (pin.Orientation == PinOrientation.PO_RIGHT) inOut[0]++;
                if (pin.Orientation == PinOrientation.PO_LEFT) inOut[1]++;
            }
        }

        public void ResetPinNames()
        {
            foreach (Pin i in _pinList)
            {
                i.VarName = string.Empty;
            }
        }

        public void ResetPriority()
        {
            this._priority = 0;
            foreach (Pin pin in this._pinList)
            {
                pin.Priority = 0;
            }
        }
        
        public void WriteCompileData(XmlWriter writer)
        {
            writer.WriteStartElement("block");
            writer.WriteAttributeString("type", _desc.TypeElem);
            writer.WriteAttributeString("name", _desc.name);
            writer.WriteStartElement("PinData");
            foreach (Pin i in _pinList)
            {
                writer.WriteStartElement("Pin");
                writer.WriteAttributeString("varName", i.VarName);
                writer.WriteAttributeString("position", i.Position.ToString());
                writer.WriteAttributeString("orientation", i.Orientation == PinOrientation.PO_LEFT ? "left" : "right");
                writer.WriteAttributeString("type", i.PinType == PinType.PT_DIRECT ? "direct" : "inverse");
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteStartElement("UserData");
            writer.WriteAttributeString("count", _desc.userData.Count.ToString());
            foreach (KeyValuePair<string, string> i in _desc.userData)
            {
                writer.WriteStartElement("Prop");
                writer.WriteAttributeString("key", i.Key);
                writer.WriteAttributeString("value", i.Value);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        #endregion
        
        #region Save/Load
        public void ExportToXML(XmlTextWriter writer)
        {
            writer.WriteStartElement("Block");
            writer.WriteAttributeString("name", _desc.name);
            writer.WriteAttributeString("type", _desc.TypeElem);
            writer.WriteAttributeString("group", _desc.group);
            writer.WriteAttributeString("propType", _desc.propType);
            writer.WriteAttributeString("description", _desc.description);
            writer.WriteAttributeString("posX", _posX.ToString());
            writer.WriteAttributeString("posY", _posY.ToString());

            writer.WriteStartElement("PinData");
            writer.WriteAttributeString("count", _pinList.Count.ToString());
            foreach (Pin i in _pinList)
                i.PropertiesToXml(writer);
            writer.WriteEndElement();

            writer.WriteStartElement("UserData");
            writer.WriteAttributeString("count", _desc.userData.Count.ToString());
            foreach (KeyValuePair<string, string> i in _desc.userData)
            {
                writer.WriteStartElement("Prop");
                writer.WriteAttributeString("key", i.Key);
                writer.WriteAttributeString("value", i.Value);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        public void ImportFromXml(XmlTextReader reader)
        {
            this._desc = new BlockDesc
            {
                name = reader.GetAttribute("name"),
                TypeElem = reader.GetAttribute("type"),
                group = reader.GetAttribute("group"),
                propType = reader.GetAttribute("propType"),
                description = reader.GetAttribute("description"),
                posX = Convert.ToInt32(reader.GetAttribute("posX")),
                posY = Convert.ToInt32(reader.GetAttribute("posY"))
            };
            
            reader.Read(); //���� � <PinData>
            int pinCount = Convert.ToInt32(reader.GetAttribute("count"));
            for (int i = 0; i < pinCount; i++)
            {
                reader.Read(); // <Pin>
                string pName = reader.GetAttribute("name");
                PinType pType = Pin.GetType(reader.GetAttribute("type"));
                PinOrientation pOrientation = Pin.GetPinOrientation(reader.GetAttribute("orient"));
                int pPosition = Convert.ToInt32(reader.GetAttribute("position"));
                PinDesc pDesc = new PinDesc(pName, pType, pOrientation, pPosition);
                _desc.listPinDesc.Add(pDesc);
                bool adding = Convert.ToBoolean(reader.GetAttribute("addingPoint"));
                Pin pin = new Pin(pDesc, _posX, _posY, _allign, _scale){AddingPoint = adding};
                this._pinList.Add(pin);
            }
            if (pinCount != 0) reader.Read();//����� �� <PinData>

            reader.Read(); // ���� � ���� <UserData>
            int userDataCount = Convert.ToInt32(reader.GetAttribute("count"));
            for (int i = 0; i < userDataCount; i++)
            {
                reader.Read();
                string key = reader.GetAttribute("key");
                string value = reader.GetAttribute("value");
                if (key != null) _desc.userData.Add(key, value);
            }
            if (userDataCount != 0) reader.Read(); //����� �� ���� <UserData>
            UpdateFromDescriptor(); //��������� ��������� � �������� �����
            reader.Read();          //����� � <Block>
        }
        #endregion
    }// end class block
}// end namespace SchemeEditorSystem.BlockS 
