using System;
using SchemeEditorSystem.managers;


namespace SchemeEditorSystem.ResourceLibs.PropForms
{
    /// <summary>
    /// ����� ������� ��� �������� "���������"
    /// </summary>
    public partial class MulDivProps : PropForm
    {
        private string _preShiftR;
        private string _preName;
        /// <summary>
        /// �����������
        /// </summary>
        public MulDivProps()
        {
            InitializeComponent();
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            PropFormDescriptor.userData["shiftR"] = this.shiftR.Text;
            okClicked = true;
            this.Close();
        }

        private void BlockAndProp_Load(object sender, EventArgs e)
        {
            this.TypeBox.Text = PropFormDescriptor.TypeElem;
            this.Text = PropFormDescriptor.description + " - ��������";
            this.shiftR.Text = PropFormDescriptor.userData["shiftR"];
            this.textName.Text = PropFormDescriptor.name;

            this._preName = PropFormDescriptor.name;
            this._preShiftR = PropFormDescriptor.userData["shiftR"];
        }
        //*******************************

        private void textName_TextChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this.textName.Text;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this._preName;
            PropFormDescriptor.userData["shiftR"] = this._preShiftR;
            okClicked = false;
            this.Close();
        }
    }
}