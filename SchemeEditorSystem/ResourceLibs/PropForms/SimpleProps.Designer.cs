namespace SchemeEditorSystem.ResourceLibs.PropForms
{
    partial class SimpleProps
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TypeBox = new System.Windows.Forms.TextBox();
            this.textInputNum = new System.Windows.Forms.TextBox();
            this.InputGroup = new System.Windows.Forms.GroupBox();
            this.AddInButtonI = new System.Windows.Forms.Button();
            this.DelInButton = new System.Windows.Forms.Button();
            this.AddInbuttonD = new System.Windows.Forms.Button();
            this.Ok = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textName = new System.Windows.Forms.TextBox();
            this.Cancel = new System.Windows.Forms.Button();
            this.InputGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(63, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "��� ��������";
            // 
            // TypeBox
            // 
            this.TypeBox.Location = new System.Drawing.Point(187, 15);
            this.TypeBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TypeBox.Name = "TypeBox";
            this.TypeBox.ReadOnly = true;
            this.TypeBox.Size = new System.Drawing.Size(112, 22);
            this.TypeBox.TabIndex = 1;
            this.TypeBox.Text = "���������� �������";
            // 
            // textInputNum
            // 
            this.textInputNum.Location = new System.Drawing.Point(187, 23);
            this.textInputNum.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textInputNum.Name = "textInputNum";
            this.textInputNum.ReadOnly = true;
            this.textInputNum.Size = new System.Drawing.Size(100, 22);
            this.textInputNum.TabIndex = 2;
            // 
            // InputGroup
            // 
            this.InputGroup.Controls.Add(this.AddInButtonI);
            this.InputGroup.Controls.Add(this.DelInButton);
            this.InputGroup.Controls.Add(this.AddInbuttonD);
            this.InputGroup.Controls.Add(this.textInputNum);
            this.InputGroup.Location = new System.Drawing.Point(4, 79);
            this.InputGroup.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.InputGroup.Name = "InputGroup";
            this.InputGroup.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.InputGroup.Size = new System.Drawing.Size(296, 92);
            this.InputGroup.TabIndex = 3;
            this.InputGroup.TabStop = false;
            this.InputGroup.Text = "�����";
            // 
            // AddInButtonI
            // 
            this.AddInButtonI.Location = new System.Drawing.Point(8, 55);
            this.AddInButtonI.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AddInButtonI.Name = "AddInButtonI";
            this.AddInButtonI.Size = new System.Drawing.Size(171, 27);
            this.AddInButtonI.TabIndex = 6;
            this.AddInButtonI.Text = "�������� ���������";
            this.AddInButtonI.UseVisualStyleBackColor = true;
            this.AddInButtonI.Click += new System.EventHandler(this.AddInButtonI_Click);
            // 
            // DelInButton
            // 
            this.DelInButton.Location = new System.Drawing.Point(189, 55);
            this.DelInButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DelInButton.Name = "DelInButton";
            this.DelInButton.Size = new System.Drawing.Size(99, 27);
            this.DelInButton.TabIndex = 5;
            this.DelInButton.Text = "�������";
            this.DelInButton.UseVisualStyleBackColor = true;
            this.DelInButton.Click += new System.EventHandler(this.DelInButton_Click);
            // 
            // AddInbuttonD
            // 
            this.AddInbuttonD.Location = new System.Drawing.Point(8, 21);
            this.AddInbuttonD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AddInbuttonD.Name = "AddInbuttonD";
            this.AddInbuttonD.Size = new System.Drawing.Size(171, 27);
            this.AddInbuttonD.TabIndex = 4;
            this.AddInbuttonD.Text = "�������� ������";
            this.AddInbuttonD.UseVisualStyleBackColor = true;
            this.AddInbuttonD.Click += new System.EventHandler(this.AddInbutton_Click);
            // 
            // Ok
            // 
            this.Ok.Location = new System.Drawing.Point(19, 178);
            this.Ok.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Ok.Name = "Ok";
            this.Ok.Size = new System.Drawing.Size(107, 30);
            this.Ok.TabIndex = 5;
            this.Ok.Text = "�������";
            this.Ok.UseVisualStyleBackColor = true;
            this.Ok.Click += new System.EventHandler(this.Ok_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(63, 50);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "��� ��������";
            // 
            // textName
            // 
            this.textName.Location = new System.Drawing.Point(187, 47);
            this.textName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(112, 22);
            this.textName.TabIndex = 7;
            this.textName.TextChanged += new System.EventHandler(this.textName_TextChanged);
            // 
            // Cancel
            // 
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.Location = new System.Drawing.Point(185, 178);
            this.Cancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(107, 30);
            this.Cancel.TabIndex = 8;
            this.Cancel.Text = "������";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // SimpleProps
            // 
            this.AcceptButton = this.Ok;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.CancelButton = this.Cancel;
            this.ClientSize = new System.Drawing.Size(311, 215);
            this.ControlBox = false;
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.textName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Ok);
            this.Controls.Add(this.InputGroup);
            this.Controls.Add(this.TypeBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SimpleProps";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "���������� �������  - ��������";
            this.Load += new System.EventHandler(this.BlockAndProp_Load);
            this.InputGroup.ResumeLayout(false);
            this.InputGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TypeBox;
        private System.Windows.Forms.TextBox textInputNum;
        private System.Windows.Forms.GroupBox InputGroup;
        private System.Windows.Forms.Button Ok;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.Button DelInButton;
        private System.Windows.Forms.Button AddInbuttonD;
        private System.Windows.Forms.Button AddInButtonI;
        private System.Windows.Forms.Button Cancel;
    }
}