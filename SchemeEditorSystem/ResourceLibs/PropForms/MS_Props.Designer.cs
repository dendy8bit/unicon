namespace SchemeEditorSystem.ResourceLibs.PropForms
{
    partial class MsProps
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param pinName="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TypeBox = new System.Windows.Forms.TextBox();
            this.InputGroup = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rbDirectIn2 = new System.Windows.Forms.RadioButton();
            this.rbInverseIn2 = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbDirectIn1 = new System.Windows.Forms.RadioButton();
            this.rbInverseIn1 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbDirectQ = new System.Windows.Forms.RadioButton();
            this.rbInverseQ = new System.Windows.Forms.RadioButton();
            this.Ok = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textName = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbInverseOut = new System.Windows.Forms.RadioButton();
            this.rbDirectOut = new System.Windows.Forms.RadioButton();
            this.Cancel = new System.Windows.Forms.Button();
            this.InputGroup.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(63, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "��� ��������";
            // 
            // TypeBox
            // 
            this.TypeBox.Location = new System.Drawing.Point(187, 15);
            this.TypeBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TypeBox.Name = "TypeBox";
            this.TypeBox.ReadOnly = true;
            this.TypeBox.Size = new System.Drawing.Size(112, 22);
            this.TypeBox.TabIndex = 1;
            this.TypeBox.Text = "���������� �������";
            // 
            // InputGroup
            // 
            this.InputGroup.Controls.Add(this.groupBox4);
            this.InputGroup.Controls.Add(this.groupBox3);
            this.InputGroup.Controls.Add(this.groupBox2);
            this.InputGroup.Location = new System.Drawing.Point(4, 79);
            this.InputGroup.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.InputGroup.Name = "InputGroup";
            this.InputGroup.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.InputGroup.Size = new System.Drawing.Size(296, 310);
            this.InputGroup.TabIndex = 3;
            this.InputGroup.TabStop = false;
            this.InputGroup.Text = "�����";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rbDirectIn2);
            this.groupBox4.Controls.Add(this.rbInverseIn2);
            this.groupBox4.Location = new System.Drawing.Point(8, 117);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Size = new System.Drawing.Size(280, 86);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "in2 (���� 2)";
            // 
            // rbDirectIn2
            // 
            this.rbDirectIn2.AutoSize = true;
            this.rbDirectIn2.Checked = true;
            this.rbDirectIn2.Location = new System.Drawing.Point(8, 23);
            this.rbDirectIn2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbDirectIn2.Name = "rbDirectIn2";
            this.rbDirectIn2.Size = new System.Drawing.Size(78, 21);
            this.rbDirectIn2.TabIndex = 2;
            this.rbDirectIn2.TabStop = true;
            this.rbDirectIn2.Text = "������";
            this.rbDirectIn2.UseVisualStyleBackColor = true;
            this.rbDirectIn2.CheckedChanged += new System.EventHandler(this.rbDirectIn2_CheckedChanged);
            // 
            // rbInverseIn2
            // 
            this.rbInverseIn2.AutoSize = true;
            this.rbInverseIn2.Location = new System.Drawing.Point(8, 54);
            this.rbInverseIn2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbInverseIn2.Name = "rbInverseIn2";
            this.rbInverseIn2.Size = new System.Drawing.Size(101, 21);
            this.rbInverseIn2.TabIndex = 3;
            this.rbInverseIn2.Text = "���������";
            this.rbInverseIn2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbDirectIn1);
            this.groupBox3.Controls.Add(this.rbInverseIn1);
            this.groupBox3.Location = new System.Drawing.Point(8, 23);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(280, 86);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "in1 (���� 1)";
            // 
            // rbDirectIn1
            // 
            this.rbDirectIn1.AutoSize = true;
            this.rbDirectIn1.Checked = true;
            this.rbDirectIn1.Location = new System.Drawing.Point(8, 23);
            this.rbDirectIn1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbDirectIn1.Name = "rbDirectIn1";
            this.rbDirectIn1.Size = new System.Drawing.Size(78, 21);
            this.rbDirectIn1.TabIndex = 2;
            this.rbDirectIn1.TabStop = true;
            this.rbDirectIn1.Text = "������";
            this.rbDirectIn1.UseVisualStyleBackColor = true;
            this.rbDirectIn1.CheckedChanged += new System.EventHandler(this.rbDirectIn1_CheckedChanged);
            // 
            // rbInverseIn1
            // 
            this.rbInverseIn1.AutoSize = true;
            this.rbInverseIn1.Location = new System.Drawing.Point(8, 54);
            this.rbInverseIn1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbInverseIn1.Name = "rbInverseIn1";
            this.rbInverseIn1.Size = new System.Drawing.Size(101, 21);
            this.rbInverseIn1.TabIndex = 3;
            this.rbInverseIn1.Text = "���������";
            this.rbInverseIn1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbDirectQ);
            this.groupBox2.Controls.Add(this.rbInverseQ);
            this.groupBox2.Location = new System.Drawing.Point(8, 210);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(280, 86);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Y (�������� ����)";
            // 
            // rbDirectQ
            // 
            this.rbDirectQ.AutoSize = true;
            this.rbDirectQ.Checked = true;
            this.rbDirectQ.Location = new System.Drawing.Point(8, 23);
            this.rbDirectQ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbDirectQ.Name = "rbDirectQ";
            this.rbDirectQ.Size = new System.Drawing.Size(78, 21);
            this.rbDirectQ.TabIndex = 2;
            this.rbDirectQ.TabStop = true;
            this.rbDirectQ.Text = "������";
            this.rbDirectQ.UseVisualStyleBackColor = true;
            this.rbDirectQ.CheckedChanged += new System.EventHandler(this.rbDirectQ_CheckedChanged);
            // 
            // rbInverseQ
            // 
            this.rbInverseQ.AutoSize = true;
            this.rbInverseQ.Location = new System.Drawing.Point(8, 54);
            this.rbInverseQ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbInverseQ.Name = "rbInverseQ";
            this.rbInverseQ.Size = new System.Drawing.Size(101, 21);
            this.rbInverseQ.TabIndex = 3;
            this.rbInverseQ.Text = "���������";
            this.rbInverseQ.UseVisualStyleBackColor = true;
            // 
            // Ok
            // 
            this.Ok.Location = new System.Drawing.Point(12, 496);
            this.Ok.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Ok.Name = "Ok";
            this.Ok.Size = new System.Drawing.Size(113, 30);
            this.Ok.TabIndex = 5;
            this.Ok.Text = "�������";
            this.Ok.UseVisualStyleBackColor = true;
            this.Ok.Click += new System.EventHandler(this.Ok_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(63, 50);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "��� ��������";
            // 
            // textName
            // 
            this.textName.Location = new System.Drawing.Point(187, 47);
            this.textName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(112, 22);
            this.textName.TabIndex = 7;
            this.textName.TextChanged += new System.EventHandler(this.textName_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbInverseOut);
            this.groupBox1.Controls.Add(this.rbDirectOut);
            this.groupBox1.Location = new System.Drawing.Point(4, 396);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(296, 92);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "�����";
            // 
            // rbInverseOut
            // 
            this.rbInverseOut.AutoSize = true;
            this.rbInverseOut.Location = new System.Drawing.Point(13, 54);
            this.rbInverseOut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbInverseOut.Name = "rbInverseOut";
            this.rbInverseOut.Size = new System.Drawing.Size(101, 21);
            this.rbInverseOut.TabIndex = 1;
            this.rbInverseOut.Text = "���������";
            this.rbInverseOut.UseVisualStyleBackColor = true;
            // 
            // rbDirectOut
            // 
            this.rbDirectOut.AutoSize = true;
            this.rbDirectOut.Checked = true;
            this.rbDirectOut.Location = new System.Drawing.Point(13, 23);
            this.rbDirectOut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbDirectOut.Name = "rbDirectOut";
            this.rbDirectOut.Size = new System.Drawing.Size(78, 21);
            this.rbDirectOut.TabIndex = 0;
            this.rbDirectOut.TabStop = true;
            this.rbDirectOut.Text = "������";
            this.rbDirectOut.UseVisualStyleBackColor = true;
            this.rbDirectOut.CheckedChanged += new System.EventHandler(this.rbDirect_CheckedChanged);
            // 
            // Cancel
            // 
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.Location = new System.Drawing.Point(187, 496);
            this.Cancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(113, 30);
            this.Cancel.TabIndex = 8;
            this.Cancel.Text = "������";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // MsProps
            // 
            this.AcceptButton = this.Ok;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.CancelButton = this.Cancel;
            this.ClientSize = new System.Drawing.Size(311, 534);
            this.ControlBox = false;
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Ok);
            this.Controls.Add(this.InputGroup);
            this.Controls.Add(this.TypeBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MsProps";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "���������� �������  - ��������";
            this.Load += new System.EventHandler(this.BlockAndProp_Load);
            this.InputGroup.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TypeBox;
        private System.Windows.Forms.GroupBox InputGroup;
        private System.Windows.Forms.Button Ok;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbInverseOut;
        private System.Windows.Forms.RadioButton rbDirectOut;
        private System.Windows.Forms.RadioButton rbInverseQ;
        private System.Windows.Forms.RadioButton rbDirectQ;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rbDirectIn2;
        private System.Windows.Forms.RadioButton rbInverseIn2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbDirectIn1;
        private System.Windows.Forms.RadioButton rbInverseIn1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button Cancel;
    }
}