using System;
using System.Collections.Generic;
using SchemeEditorSystem.managers;
using SchemeEditorSystem.descriptors;
using System.Collections;
using System.Linq;


namespace SchemeEditorSystem.ResourceLibs.PropForms
{
    /// <summary>
    /// ����� ��� ������� �����������
    /// </summary>
    public partial class DProps : PropForm
    {
        private List<PinDesc> _pins; 
        private List<PinDesc> _prePins; 
        private int _preBitCount ;
        private int _preBitAddr;
        private string _preMask;
        private string _preName;

        /// <summary>
        /// �����������
        /// </summary>
        public DProps()
        {
            InitializeComponent();
            this.bitAddr.SelectedIndex = 0;
            this.bitCount.SelectedIndex = 0;
            this._outs.SelectedIndex = 0;
            this._pins = new List<PinDesc>();
            this._prePins = new List<PinDesc>();
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            PropFormDescriptor.userData["count"] = this.bitCount.SelectedItem.ToString();
            PropFormDescriptor.userData["addr"] = this.bitAddr.SelectedItem.ToString();

            int countBits = Convert.ToInt32(this.bitCount.SelectedItem);
            BitArray maskBits = new BitArray(countBits);
            maskBits.SetAll(true);
            ushort mask = BitsToUshort(maskBits);
            PropFormDescriptor.userData["mask"] = mask.ToString();

            PropFormDescriptor.listPinDesc.Clear();
            PinDesc temp = new PinDesc();
            temp.Orientation = PinOrientation.PO_LEFT;
            temp.PinType = PinType.PT_DIRECT;
            temp.PinName = "";
            temp.Position = PropFormDescriptor.listPinDesc.Count + 1;
            PropFormDescriptor.listPinDesc.Add(temp);
            for (int i = 0; i < this._pins.Count; i++)
            {
                this._pins[i].Position = PropFormDescriptor.listPinDesc.Count;
                PropFormDescriptor.listPinDesc.Add(this._pins[i]);
            }
            okClicked = true;
            this.Close();
        }

        #region �������� � ������
        /// <summary>
        /// ������������ ��������� ����� � �����
        /// </summary>
        /// <param name="bits">�������� �����</param>
        /// <returns>�����</returns>
        public ushort BitsToUshort(BitArray bits)
        {
            ushort temp = 0;
            for (int i = 0; i < bits.Count; i++)
            {
                temp += bits[i] ? (ushort)Math.Pow(2, i) : (ushort)0;
            }
            return temp;
        }

        /// <summary>
        /// ���������� �������� ����� � ����� ������ 
        /// �������� : ���� ����� �����, � �� �����, ����� �������� ��������� �� n ���, ��� n = ����� ���������� ����.
        /// ������ : int number = GetBits(value,8,9) >> 8
        /// </summary>
        /// <param name="value">�����</param>
        /// <param name="indexes">������ �����</param>
        /// <returns>�������� ����� - �������� �������� �����, ������������ � indexes</returns>
        public ushort GetBits(ushort value, params int[] indexes)
        {
            ushort mask = 0;
            for (int i = 0; i < indexes.Length; i++)
            {
                mask |= (ushort)(1 << indexes[i]);
            }
            return (ushort)(value & mask);
        }
        #endregion

        private void BlockAndProp_Load(object sender, EventArgs e)
        {
            this.TypeBox.Text = PropFormDescriptor.TypeElem;
            this.Text = PropFormDescriptor.description + " - ��������";
            this.textName.Text = PropFormDescriptor.name;
            this.bitCount.SelectedItem = PropFormDescriptor.userData["count"];
            this.bitAddr.SelectedItem = PropFormDescriptor.userData["addr"];
            //���������� ������� ���������� ������� �����
            this._preName = PropFormDescriptor.name;
            this._preBitAddr = Convert.ToInt32(PropFormDescriptor.userData["addr"]);
            this._preBitCount = Convert.ToInt32(PropFormDescriptor.userData["count"]);
            this._preMask = PropFormDescriptor.userData["mask"];
            this._prePins.Clear();

            foreach (PinDesc pin in PropFormDescriptor.listPinDesc.Select(t => new PinDesc(t)))
            {
                this._prePins.Add(pin);
            }

            this._pins.Clear();
            foreach (PinDesc item in PropFormDescriptor.listPinDesc.Where(item => item.Orientation == PinOrientation.PO_RIGHT))
            {
                this._pins.Add(item);
            } 
            this._outName.Text = this._pins[this._outs.SelectedIndex].PinName;
        }

        //*************************************************************

        private void textName_TextChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this.textName.Text;
        }

        private void _bitCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PropFormDescriptor == null) return;
            int count = Convert.ToInt32(this.bitCount.SelectedItem.ToString());
            int addr = Convert.ToInt32(this.bitAddr.SelectedItem.ToString());
            int[] _params = new int[count];
            int paramsIndex = 0;

            BitArray maskBits = new BitArray(16);
            BitArray maskBitsToDevice = new BitArray(count);
            for (int i = 0; i < 16; i++)
            {
                if (i >= addr && i < addr + count)
                {
                    _params[paramsIndex] = i;
                    maskBits[i] = true;
                    paramsIndex++;
                }
                else
                {
                    maskBits[i] = false;
                }
            }
            for (int i = 0; i < count; i++)
            {
                maskBitsToDevice[i] = true;
            }
            ushort mask = BitsToUshort(maskBits);
            ushort maskDev = BitsToUshort(maskBitsToDevice);
            PropFormDescriptor.userData["mask"] = maskDev.ToString();

            int outCount = GetBits(mask, _params) >> _params[0];
            this._outs.Items.Clear();
            this._pins.Clear();
            for (int i = 0; i < outCount + 1; i++)
            {
                _outs.Items.Add("����� " + (i + 1));
                PinDesc _temp = new PinDesc();
                _temp.Orientation = PinOrientation.PO_RIGHT;
                _temp.PinType = PinType.PT_DIRECT;
                _temp.PinName = "Q" + (i + 1);
                this._pins.Add(_temp);
            }
            this._outs.SelectedIndex = 0;
        }

        private void _bitAddr_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PropFormDescriptor == null) return;
            int count = Convert.ToInt32(this.bitCount.SelectedItem.ToString());
            int addr = Convert.ToInt32(this.bitAddr.SelectedItem.ToString());
            int[] _params = new int[count];
            int _paramsIndex = 0;

            BitArray maskBits = new BitArray(16);
            for (int i = 0; i < 16; i++)
            {
                if (i >= addr && i < addr + count)
                {
                    _params[_paramsIndex] = i;
                    maskBits[i] = true;
                    _paramsIndex++;
                }
                else
                {
                    maskBits[i] = false;
                }
            }
            ushort mask = BitsToUshort(maskBits);
            int outCount = GetBits(mask, _params) >> _params[0];
            this._outs.Items.Clear();
            this._pins.Clear();
            for (int i = 0; i < outCount + 1; i++)
            {
                _outs.Items.Add("����� " + (i + 1));
                PinDesc _temp = new PinDesc();
                _temp.Orientation = PinOrientation.PO_RIGHT;
                _temp.PinType = PinType.PT_DIRECT;
                _temp.PinName = "Q" + (i + 1);
                this._pins.Add(_temp);
            }
            this._outs.SelectedIndex = 0;
        }

        private void _outs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PropFormDescriptor == null) return;
            this._outName.Text = this._pins[_outs.SelectedIndex].PinName;
        }

        private void _outName_TextChanged(object sender, EventArgs e)
        {
            this._pins[_outs.SelectedIndex].PinName = this._outName.Text;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this._preName;
            PropFormDescriptor.userData["addr"] = this._preBitAddr.ToString();
            PropFormDescriptor.userData["count"] = this._preBitCount.ToString();
            PropFormDescriptor.userData["mask"] = this._preMask;
            PropFormDescriptor.listPinDesc.Clear();
            for (int i = 0;i<_prePins.Count;i++)
            {
                var pin = new PinDesc(this._prePins[i]);
                PropFormDescriptor.listPinDesc.Add(pin);
            }
            okClicked = false;
            this.Close();
            
        }           
    }
}