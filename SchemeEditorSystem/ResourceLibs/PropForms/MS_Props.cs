using System;
using SchemeEditorSystem.managers;
using SchemeEditorSystem.descriptors;


namespace SchemeEditorSystem.ResourceLibs.PropForms
{
    /// <summary>
    /// ����� ������� ��� ����������� �������� "�������������"
    /// </summary>
    public partial class MsProps : PropForm
    {
        private string _preName;
        private PinType[] _preTypes;

        /// <summary>
        /// �����������
        /// </summary>
        public MsProps()
        {
            InitializeComponent();
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            okClicked = true;
            this.Close();
        }

        private void BlockAndProp_Load(object sender, EventArgs e)
        {
            this.TypeBox.Text = PropFormDescriptor.TypeElem;
            this.Text = PropFormDescriptor.description + " - ��������";
            this.textName.Text = PropFormDescriptor.name;

            this._preName = PropFormDescriptor.name;
            this._preTypes = new PinType[PropFormDescriptor.listPinDesc.Count];
            for (int i = 0; i < PropFormDescriptor.listPinDesc.Count; i++)
            {
                this._preTypes[i] = PropFormDescriptor.listPinDesc[i].PinType;
            }

            if (PropFormDescriptor.listPinDesc[0].PinType == PinType.PT_DIRECT)
            {
                rbDirectOut.Checked = true;
            }
            else
            {
                rbInverseOut.Checked = true;
            }

            if (PropFormDescriptor.listPinDesc[1].PinType == PinType.PT_DIRECT)
            {
                rbDirectIn1.Checked = true;
            }
            else
            {
                rbInverseIn1.Checked = true;
            }

            if (PropFormDescriptor.listPinDesc[2].PinType == PinType.PT_DIRECT)
            {
                rbDirectIn2.Checked = true;
            }
            else
            {
                rbInverseIn2.Checked = true;
            }

            if (PropFormDescriptor.listPinDesc[3].PinType == PinType.PT_DIRECT)
            {
                rbDirectQ.Checked = true;
            }
            else
            {
                rbInverseQ.Checked = true;
            }
        }
        //*******************************

        private void textName_TextChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this.textName.Text;
        }

        private void rbDirect_CheckedChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.listPinDesc[0].PinType = this.rbDirectOut.Checked == false ? PinType.PT_INVERSE : PinType.PT_DIRECT;
        }

        private void rbDirectQ_CheckedChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.listPinDesc[3].PinType = this.rbDirectQ.Checked == false ? PinType.PT_INVERSE : PinType.PT_DIRECT;
        }

        private void rbDirectIn1_CheckedChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.listPinDesc[1].PinType = this.rbDirectIn1.Checked == false ? PinType.PT_INVERSE : PinType.PT_DIRECT;
        }

        private void rbDirectIn2_CheckedChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.listPinDesc[2].PinType = this.rbDirectIn2.Checked == false ? PinType.PT_INVERSE : PinType.PT_DIRECT;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            okClicked = false;
            PropFormDescriptor.name = this._preName;
            for (int i = 0; i < this._preTypes.Length; i++)
            {
                PropFormDescriptor.listPinDesc[i].PinType = this._preTypes[i];
            }
            this.Close();
        }            
    }
}