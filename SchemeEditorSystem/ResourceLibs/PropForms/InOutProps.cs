using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Interfaces;
using BEMN.MBServer;
using SchemeEditorSystem.managers;


namespace SchemeEditorSystem.ResourceLibs.PropForms
{
    /// <summary>
    /// ����� ������� ����� ��� ������ ��� ���������� ��������, � ��� �� ��� �������� ������ � �������
    /// </summary>
    public partial class InOutProps : PropForm
    {
        private List<string> _typeList;
        private string _deviceName = string.Empty;
        private int _preIndBase;
        private int _preIndType;
        private string _preName;
        private string _preSign;
        private double _deviceVers;
        private Device _deviceObj;

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="device">�������� �������</param>
        public InOutProps(string device)
        {
            this.InitializeComponent();
            this._deviceName = device;
            this._typeList = new List<string>();
        }
        /// <summary>
        /// ����������� ��� ���������, � ������� ���������� ������ � ����������� �� ������
        /// </summary>
        /// <param name="deviceObj">������ ����������, ���������� ��� ���������� �� ����� ����������</param>
        public InOutProps(Device deviceObj)
        {
            this.InitializeComponent();
            this._deviceObj = deviceObj;
            this._deviceName = string.IsNullOrEmpty(deviceObj.DeviceType)
                ? this.GetEnDevName((deviceObj as INodeView).NodeName)
                : deviceObj.DeviceType;
            this._deviceVers = Common.VersionConverter(this._deviceObj.DeviceVersion);
            this._typeList = new List<string>();
        }

        private string GetEnDevName(string ruName)
        {
            switch (ruName)
            {
                case "��600":
                    return "MR600";
                case "��761":
                    return "MR761";
                case "��762":
                    return "MR762";
                case "��763":
                    return "MR763";
                case "��901 (�� �.2.13)":
                case "��901 (�.3.xx)":
                    return "MR901";
                case "��902 (�� �.2.13)":
                case "��902 (�.3.xx)":
                    return "MR902";
                case "��771":
                    return "MR771";
                case "��5":
                    return "MR5";
                case "��801���":
                    return "MR801DVG";
                case "��741":
                    return "TZL";
                case "��801 (�� �.2.08)":
                    return "MR801";                   
                case "��801":
                case "��801 (�.3.xx)":
                    return "MR801";
                default: return ruName;
            }
        }

        private string[] BaseStrings
        {
            get
            {
                if ((this._deviceName == "MR771" && this._deviceVers >= 1.02 && this._deviceVers < 1.07) ||
                    (this._deviceName == "MR761" && this._deviceVers >= 2.01 && this._deviceVers < 3.03) || 
                    (this._deviceName == "MR762" && this._deviceVers >= 2.01 && this._deviceVers < 3.03) ||
                    (this._deviceName == "MR763" && this._deviceVers >= 2.01 && this._deviceVers < 3.03))
                {
                    return new[]
                    {
                        "������� �������",
                        "��������������",
                        "����������",
                        "����������"
                    };
                }

                if ((this._deviceName == "MR771" && this._deviceVers >= 1.07) ||
                    (this._deviceName == "MR761" && this._deviceVers >= 3.03) ||
                    (this._deviceName == "MR762" && this._deviceVers >= 3.03) ||
                    (this._deviceName == "MR763" && this._deviceVers >= 3.03))
                {
                    return new[]
                    {
                        "������� �������",
                        "��������������",
                        "����������",
                        "����������",
                        "������� GOOSE"
                    };
                }
                if (this._deviceName == "MR801" && this._deviceVers >= 3.0)
                {
                    return new[]
                    {
                        "������� �������",
                        "��������������",
                        "����������",
                        "����������",
                        "������� GOOSE"
                    };
                }

                if (this._deviceName == "MR901" && this._deviceVers >= 3.0)
                {
                    return new[]
                    {
                        "������� �������",
                        "��������������",
                        "����������",
                        "����������",
                        "������� GOOSE"
                    };
                }

                if (this._deviceName == "MR902" && this._deviceVers >= 3.0)
                {
                    return new[]
                    {
                        "������� �������",
                        "��������������",
                        "����������",
                        "����������",
                        "������� GOOSE"
                    };
                }

                if (this._deviceName == "��761���" && this._deviceVers < 3.00)
                {
                    return new[]
                    {
                        "������� �������",
                        "��������������"
                    };
                }
                
                if ((this._deviceName == "MR761OBR" || this._deviceName == "��761���") && this._deviceVers >= 3.00)
                {
                    return new[]
                    {
                        "������� �������",
                        "��������������",
                        "����������",
                        "������� GOOSE"
                    };
                }

                if (this._deviceName == "MR5")
                {
                    return new[]
                    {
                        "���� ������ 1",
                        "���� ������ 2"
                    };
                }
                return new[]
                {
                    "������� �������",
                    "��������������",
                    "����������"
                };
            }
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            PropFormDescriptor.userData["inOutType"] = this.comboBoxType.SelectedIndex.ToString(CultureInfo.InvariantCulture);
            if (((this._deviceName == "MR771" && this._deviceVers >= 1.04)  
                || this._deviceName == "MR761"  && this._deviceVers >= 3.0
                || this._deviceName == "MR762"  && this._deviceVers >= 3.0
                || this._deviceName == "MR763" && this._deviceVers >= 3.0
                || this._deviceName == "MR801" && this._deviceVers >= 3.0
                || this._deviceName == "MR901" && this._deviceVers >= 3.0
                || this._deviceName == "MR902" && this._deviceVers >= 3.0
                || (this._deviceName == "MR761OBR" || this._deviceName == "��761���")) &&
                (PropFormDescriptor.TypeElem == "journal" || PropFormDescriptor.TypeElem == "journalA"))
            {
                this.SavePropJaJs();
            }
            else
            {
                PropFormDescriptor.name = this.textName.Text + "[ " + this._typeList[this.comboBoxType.SelectedIndex] + "]";
                if (PropFormDescriptor.TypeElem == "in" && PropFormDescriptor.userData.ContainsKey("baseNum"))
                {
                    int baseNum = this.baseComboBox.SelectedIndex;
                    PropFormDescriptor.userData["baseNum"] = baseNum.ToString(CultureInfo.InvariantCulture);
                    
                    if (baseNum == 0)
                    {
                        if (this._deviceName == "MR771" && this._deviceVers >= 1.14)
                        {
                            string value = this.comboBoxType.SelectedItem.ToString();
                            int key =
                                PropFormsStrings.MRUniversalSignals(_deviceObj.DeviceType,this._deviceObj.DevicePlant)
                                    .First(d => d.Value.Equals(value))
                                    .Key;
                            PropFormDescriptor.userData["inOutType"] = key.ToString(CultureInfo.InvariantCulture);
                        }

                        if (this._deviceName == "MR761" && this._deviceVers >= 3.09)
                        {
                            string value = this.comboBoxType.SelectedItem.ToString();
                            int key =
                                PropFormsStrings.MRUniversalSignals(_deviceObj.DeviceType, this._deviceObj.DevicePlant)
                                    .First(d => d.Value.Equals(value))
                                    .Key;
                            PropFormDescriptor.userData["inOutType"] = key.ToString(CultureInfo.InvariantCulture);
                        }

                        if ((this._deviceName == "MR901" || this._deviceName == "��901") && this._deviceVers >= 2.10 && this._deviceVers < 3.00)
                        {
                            string value = this.comboBoxType.SelectedItem.ToString();
                            int key =
                                PropFormsStrings.MR901SignalsBig(this._deviceObj.DevicePlant)
                                    .First(d => d.Value.Equals(value))
                                    .Key;
                            PropFormDescriptor.userData["inOutType"] = key.ToString(CultureInfo.InvariantCulture);
                        }

                        if ((this._deviceName == "MR901" || this._deviceName == "��901") && this._deviceVers >= 3.00)
                        {
                            string value = this.comboBoxType.SelectedItem.ToString();
                            int key =
                                PropFormsStrings.Mr901NewSignals(this._deviceObj.DevicePlant)
                                    .First(d => d.Value.Equals(value))
                                    .Key;
                            PropFormDescriptor.userData["inOutType"] = key.ToString(CultureInfo.InvariantCulture);
                        }
                        else if ((this._deviceName == "MR902" || this._deviceName == "��902") && this._deviceVers >= 2.10 && this._deviceVers < 3.00)
                        {
                            string value = this.comboBoxType.SelectedItem.ToString();
                            int key =
                                PropFormsStrings.MR902SignalsBig(this._deviceObj.DevicePlant)
                                    .First(d => d.Value.Equals(value))
                                    .Key;
                            PropFormDescriptor.userData["inOutType"] = key.ToString(CultureInfo.InvariantCulture);
                        }
                        else if ((this._deviceName == "MR902" || this._deviceName == "��902") && this._deviceVers >= 3.00)
                        {
                            string value = this.comboBoxType.SelectedItem.ToString();
                            int key =
                                PropFormsStrings.Mr902NewSignals(this._deviceObj.DevicePlant)
                                    .First(d => d.Value.Equals(value))
                                    .Key;
                            PropFormDescriptor.userData["inOutType"] = key.ToString(CultureInfo.InvariantCulture);
                        }
                        else if ((this._deviceName == "MR801" || this._deviceName == "��801") && this._deviceVers >= 3.0)
                        {
                            string value = this.comboBoxType.SelectedItem.ToString();
                            int key =
                                PropFormsStrings.MR801BitSignals(this._deviceObj.Info.DeviceConfiguration)
                                    .First(d => d.Value.Equals(value))
                                    .Key;
                            PropFormDescriptor.userData["inOutType"] = key.ToString(CultureInfo.InvariantCulture);
                        }
                        else if ((this._deviceName == "MR761OBR" || this._deviceName == "��761���") && this._deviceVers >= 3.0)
                        {
                            string value = this.comboBoxType.SelectedItem.ToString();
                            int key =
                                PropFormsStrings.MR761OBRNewBits(this._deviceObj.Info.DeviceConfiguration)
                                    .First(d => d.Value.Equals(value))
                                    .Key;
                            PropFormDescriptor.userData["inOutType"] = key.ToString(CultureInfo.InvariantCulture);
                        }
                    }

                    if (baseNum == 3)
                    {
                        if ((this._deviceName == "MR901" || this._deviceName == "��901") && this._deviceVers >= 3.00)
                        {
                            string value = this.comboBoxType.SelectedItem.ToString();
                            int key =
                                PropFormsStrings.MR90xControlSignalsNEW()
                                    .First(d => d.Value.Equals(value))
                                    .Key;
                            PropFormDescriptor.userData["inOutType"] = key.ToString(CultureInfo.InvariantCulture);
                        }

                        if ((this._deviceName == "MR902" || this._deviceName == "��902") && this._deviceVers >= 3.00)
                        {
                            string value = this.comboBoxType.SelectedItem.ToString();
                            int key =
                                PropFormsStrings.MR90xControlSignalsNEW()
                                    .First(d => d.Value.Equals(value))
                                    .Key;
                            PropFormDescriptor.userData["inOutType"] = key.ToString(CultureInfo.InvariantCulture);
                        }
                    }
                }
            }
            okClicked = true;
            Close();
        }

        private void SavePropJaJs()
        {
            PropFormDescriptor.name = this.textName.Text + "[ " + this.captionOfSygnal.Text + "]";
            string str = this.captionOfSygnal.Text;
            if (str.Length < 20)
            {
                int count = 20 - str.Length;
                for (int i = 0; i < count; i++)
                {
                    str += " ";
                }
            }
            int ind = this.comboBoxType.SelectedIndex;
            if (PropFormDescriptor.TypeElem == "journal")
            {
                List<string> journalList = PropFormsStrings.JournalDictionary[this._deviceObj];
                journalList.Insert(ind, str);
                journalList.RemoveAt(ind + 1);
            }
            else
            {
                List<string> alarmList = PropFormsStrings.AlarmDictionary[this._deviceObj];
                alarmList.Insert(ind, str);
                alarmList.RemoveAt(ind + 1);
            }
            PropFormDescriptor.userData["sign"] = str;
        }
        /// <summary>
        /// ��� ������ ������ �� �������� ��90� ��� ����������� ������� ������� ����� �����
        /// </summary>
        /// <param name="device">���������� (��901 ��� ��902)</param>
        /// <param name="deviceConfig">���������� ������������ ����������</param>
        /// <returns></returns>
        public static Dictionary<int, string> InitDictionary(string device, string deviceConfig)
        {
            switch (device)
            {
                case "��901":
                case "MR901":
                    return PropFormsStrings.MR901SignalsBig(deviceConfig);
                case "��902":
                case "MR902":
                    return PropFormsStrings.MR902SignalsBig(deviceConfig);
                case "MR761OBR":
                        return PropFormsStrings.MR761OBRNewBits(deviceConfig);
                case "��761���":
                    return PropFormsStrings.MR761OBRNewBits(deviceConfig);
                case "��801":
                case "MR801":
                    return PropFormsStrings.MR801BitSignals(deviceConfig);
                default:
                    return new Dictionary<int, string>();
            }
        }

        /// <summary>
        /// ������ ������� �������� � ����������� �� �������, ���� �������� � ������ �������(�� �����������)
        /// </summary>
        /// <param name="device">�������� ����������</param>
        /// <param name="baseNum">���� ��������</param>
        /// <param name="deviceVersion">������ ����������</param>
        /// <param name="devicePlant">������������ ���������� (��� ��� ������� 90�)</param>
        /// <returns>������</returns>
        public static List<string> InitInList(string device, int baseNum = 0, double deviceVersion = 1.0, string devicePlant = "")
        {
            switch (device)
            {
                case "��5":
                case "MR5":
                {
                    if (deviceVersion >= 50 && deviceVersion < 55)
                    {
                        return baseNum == 0 ? PropFormsStrings.Mr5SignalsV50(deviceVersion).Values.ToList() : PropFormsStrings.Mr5SignalsBase2V50(deviceVersion).Values.ToList();
                    }
                    if (deviceVersion >= 55 && deviceVersion < 60)
                    {
                        return baseNum == 0 ? PropFormsStrings.Mr5SignalsBase1V55 : PropFormsStrings.Mr5SignalsBase2V55;
                    }
                    if (deviceVersion >= 60 && deviceVersion < 70)
                    {
                        return baseNum == 0 ? PropFormsStrings.Mr5SignalsV60 : PropFormsStrings.Mr5SignalsBase2V60;
                    }
                    if (deviceVersion >= 70 && deviceVersion < 75)
                    {
                        return baseNum == 0 ? PropFormsStrings.Mr5SygnalsV70 : PropFormsStrings.Mr5SignalsBase2V70;
                    }
                    return baseNum == 0
                        ? deviceVersion > 75 ? PropFormsStrings.Mr5SignalsBase1V75_01 : PropFormsStrings.Mr5SignalsBase1V75
                        : PropFormsStrings.Mr5SignalsBase2V75;
                }
                case "MR550":
                    return PropFormsStrings.Mr550Signals;
                case "MR500":
                    return PropFormsStrings.MR500Signals;
                case "MR700":
                    return PropFormsStrings.MR700Signals;
                case "MR750":
                    return PropFormsStrings.MR750Signals;
                case "MR741":
                case "TZL":
                    List<string> list = PropFormsStrings.TZLSignals;
                    if (deviceVersion >= 3.03)
                    {
                        list[4] = " ��1:4   ���� �1";
                        list[11] = " ��1:11  ���� �2";
                    }
                    return list;
                case "MR600":
                    return deviceVersion < 3.02
                        ? PropFormsStrings.MR600SignalsBeforeV3_02
                        : PropFormsStrings.MR600SignalsAfterV3_02;
                case "MR801":
                    if (deviceVersion >= 3.0)
                    {
                        switch (baseNum)
                        {
                            case 0:
                                return PropFormsStrings.MR801BitSignals(devicePlant).Values.ToList();
                            case 1:
                                return PropFormsStrings.MR801Neispr;
                            case 2:
                                return PropFormsStrings.MR801Params;
                            case 3:
                                return PropFormsStrings.MR801ControlSignalsNEW().Values.ToList();
                            case 4:
                                return PropFormsStrings.MR771GooseSignals;
                        }
                    }
                    else
                    {
                        switch (baseNum)
                        {
                            case 0:
                                return PropFormsStrings.UDZTBits(deviceVersion);
                            case 1:
                                return PropFormsStrings.UDZTNeispr;
                            case 2:
                                return PropFormsStrings.UDZTParams;
                        }
                    }
                    break;
                case "MR801DVG":
                    switch (baseNum)
                    {
                        case 0:
                            return PropFormsStrings.Mr801DvgSignals;
                        case 1:
                            return PropFormsStrings.MR801DvgAlarm;
                        case 2:
                            return PropFormsStrings.MR801DvgParams;
                    }
                    break;
                case "MR771":
                    switch (baseNum)
                    {
                        case 0:
                            if (deviceVersion >= 1.14)
                            {
                                return PropFormsStrings.MRUniversalSignals(device, devicePlant).Values.ToList();
                            }

                            if (deviceVersion >= 1.12)
                            {
                                return PropFormsStrings.MR771BitsSignatures != null ? PropFormsStrings.MR771BitsSignatures : PropFormsStrings.MR771Bits(deviceVersion);
                            }

                            return PropFormsStrings.MR771Bits(deviceVersion);
                        case 1:
                            if (deviceVersion >= 1.14)
                            {
                                return PropFormsStrings.MRUniversalNeispr;
                            }
                            return PropFormsStrings.MR771Neispr;
                        case 2:
                            return PropFormsStrings.MR771Params(deviceVersion);
                        case 3:
                            if (deviceVersion >= 1.14)
                            {
                                return PropFormsStrings.MRUniversalControlSignals(device);
                            }
                            return PropFormsStrings.MR771ControlSignals;
                        case 4:
                            return PropFormsStrings.MR771GooseSignals;
                    }
                    break;
                case "MR761":
                    switch (baseNum)
                    {
                        case 0:
                            if (deviceVersion > 3.08)
                            {
                                return PropFormsStrings.MRUniversalSignals(device, devicePlant).Values.ToList();
                            }
                            if (deviceVersion >= 3)
                            {
                                return PropFormsStrings.MR761BitsV3(deviceVersion);
                            }
                            if (deviceVersion >= 2.04 && deviceVersion < 3)
                            {
                                return PropFormsStrings.MR761BitsV204;
                            }
                            if (deviceVersion >= 2.0 && deviceVersion < 2.04)
                            {
                                return PropFormsStrings.MR761BitsV2;
                            }
                            return PropFormsStrings.MR761Bits;
                        case 1:
                            if (deviceVersion >= 3.09)
                            {
                                return PropFormsStrings.MRUniversalNeispr;
                            }
                            else if (deviceVersion >= 3.0 && devicePlant == "T4N4D42R19")
                            {
                                return PropFormsStrings.MR761Foult4T4H(devicePlant);
                            }
                            else if (deviceVersion >= 3.0)
                            {
                                return PropFormsStrings.MR76xNeispr;
                            }
                            else
                            {
                                return PropFormsStrings.MR761Neispr;
                            }
                        case 2:
                            return PropFormsStrings.MR761Params;
                        case 3:
                            if (deviceVersion > 3.08)
                            {
                                return PropFormsStrings.MRUniversalControlSignals(device);
                            }
                            return PropFormsStrings.MR761ControlSignals;
                        case 4:
                            return PropFormsStrings.MR76xGooseSignals;
                    }
                    break; 
                case "MR762":
                    switch (baseNum)
                    {
                        case 0:
                            if (deviceVersion >= 2.04 && deviceVersion<3.02)
                            {
                                return PropFormsStrings.MR761BitsV204;
                            }
                            if (deviceVersion >= 2.0 && deviceVersion < 2.04)
                            {
                                return PropFormsStrings.MR761BitsV2;
                            }
                            if (deviceVersion >= 3.02)
                            {
                                return PropFormsStrings.MR761BitsV3(deviceVersion);
                            }
                            return PropFormsStrings.MR761Bits;
                        case 1:
                            return deviceVersion >= 3.0 ? PropFormsStrings.MR76xNeispr : PropFormsStrings.MR761Neispr;
                        case 2:
                            return PropFormsStrings.MR762Params;
                        case 3:
                            return PropFormsStrings.MR761ControlSignals;
                        case 4:
                            return PropFormsStrings.MR76xGooseSignals;
                    }
                    break;
                case "MR763":
                    switch (baseNum)
                    {
                        case 0:
                            if (deviceVersion >= 2.04 && deviceVersion < 3.02)
                            {
                                return PropFormsStrings.MR761BitsV204;
                            }
                            if (deviceVersion >= 2.0 && deviceVersion < 2.04)
                            {
                                return PropFormsStrings.MR761BitsV2;
                            }
                            if (deviceVersion >= 3.02)
                            {
                                return PropFormsStrings.MR761BitsV3(deviceVersion);
                            }
                            return PropFormsStrings.MR761Bits;
                        case 1:
                            return deviceVersion >= 3.0 ? PropFormsStrings.MR76xNeispr : PropFormsStrings.MR761Neispr;
                        case 2:
                            return PropFormsStrings.MR763Params;
                        case 3:
                            return PropFormsStrings.MR761ControlSignals;
                        case 4:
                            return PropFormsStrings.MR76xGooseSignals;
                    }
                    break;
                case "��901":
                case "MR901":
                    switch (baseNum)
                    {
                        case 0:
                            if (deviceVersion >= 2.10 && deviceVersion < 3.00)
                            {
                                return PropFormsStrings.MR901SignalsBig(devicePlant).Values.ToList();
                            }
                            else if (deviceVersion >= 3.00)
                            {
                                return PropFormsStrings.Mr901NewSignals(devicePlant).Values.ToList();
                            }
                            return PropFormsStrings.DZHBits(deviceVersion);
                        case 1:
                            if (deviceVersion >= 2.10 && deviceVersion < 3.00)
                            {
                                return PropFormsStrings.DZHNeisprV203(devicePlant);
                            }
                            else if (deviceVersion >= 3.00)
                            {
                                return PropFormsStrings.Mr901NewNeispr(devicePlant);
                            }
                            return PropFormsStrings.DZHNeispr;
                        case 2:
                            return PropFormsStrings.DZHParams;
                        case 3:
                            return PropFormsStrings.MR90xControlSignalsNEW().Values.ToList();
                        case 4:
                            return PropFormsStrings.MR771GooseSignals;
                    }
                    break;
                case "��902":
                case "MR902":
                    switch (baseNum)
                    {
                        case 0:
                            if (deviceVersion >= 2.10 && deviceVersion < 3.00)
                            {
                                return PropFormsStrings.MR902SignalsBig(devicePlant).Values.ToList();
                            }
                            else if (deviceVersion >= 3.00)
                            {
                                return PropFormsStrings.Mr902NewSignals(devicePlant).Values.ToList();
                            }
                            return PropFormsStrings.MR902Bits(deviceVersion);
                        case 1:
                            if (deviceVersion >= 2.03 && deviceVersion < 3.00)
                            {
                                return PropFormsStrings.MR902NeisprV203;
                            }
                            else if (deviceVersion >= 3.00)
                            {
                                return PropFormsStrings.Mr902NewNeispr(devicePlant);
                            }
                            return PropFormsStrings.MR902Neispr;

                        case 2:
                            if (deviceVersion >= 3.00)
                            {
                                return PropFormsStrings.Mr901NewParams;
                            }
                            return PropFormsStrings.MR902Params;
                        case 3:
                            return PropFormsStrings.MR90xControlSignalsNEW().Values.ToList();
                        case 4:
                            return PropFormsStrings.MR771GooseSignals;
                    }
                    break;
                case "MR761OBR":
                case "��761���":
                    if (deviceVersion < 3.00)
                    {
                        return baseNum == 0 ? PropFormsStrings.MR761OBRBits : PropFormsStrings.MR761OBRNeispr;
                    }
                    else
                    {
                        switch (baseNum)
                        {
                            case 0:
                                return PropFormsStrings.MR761OBRNewBits(devicePlant).Values.ToList();
                            case 1:
                                return PropFormsStrings.MR761ObrNewFault.Values.ToList();
                            case 2:
                                return PropFormsStrings.MR761ObrNewParams;
                            case 3:
                                return PropFormsStrings.MR771GooseSignals;
                        }
                    }
                    break;
                //return baseNum == 0 ? PropFormsStrings.MR761OBRBits : PropFormsStrings.MR761OBRNeispr;
                case "KL27":
                case "��27":
                    return PropFormsStrings.KL27Signals;
            }
            return new List<string>();
        }

        /// <summary>
        /// ������ ����������� �������
        /// </summary>
        /// <param name="count">���������� �������</param>
        /// <returns>������</returns>
        public static List<string> InitOutList(int count = 32)
        {
            List<string> typeList = new List<string>(count);
            for (int i = 0; i < count; i++)
            {
                typeList.Add("��� " + (i + 1));
            }
            return typeList;
        }
        /// <summary>
        /// ������ ����������� �������
        /// </summary>
        /// <param name="deviceName">�������� ����������</param>
        /// <param name="deviceVers">������ ����������</param>
        /// <returns></returns>
        public static List<string> InitOutList(string deviceName, double deviceVers)
        {
            if ((deviceName == "MR901" || deviceName == "MR902" ) && deviceVers >= 2.10)
            {
                return InitOutList(48);
            }

            if (deviceName == "MR801" && deviceVers >= 3.00)
            {
                return InitOutList(48);
            }

            if(deviceName == "MR761OBR" || deviceName == "��761���" && deviceVers >= 3.00)
            {
                return InitOutList(48);
            }
            return InitOutList();
        }

        /// <summary>
        /// ������ ��� �� ��� ��
        /// </summary>
        /// <param name="journal">������ ������� "��" ��� ������ "��"</param>
        /// <param name="count">���������� ��������� �������</param>
        /// <returns>������</returns>
        public static List<string> InitJournalList(string journal, int count)
        {
            List<string> typeList = new List<string>(count);
            for (int i = 0; i < count; i++)
            {
                typeList.Add("������ � " + journal + (i + 1));
            }
            return typeList;
        }

        private void BlockAndProp_Load(object sender, EventArgs e)
        {
            this.BlockAndProp();
        }

        private void BlockAndProp()
        {
            this.TypeBox.Text = PropFormDescriptor.TypeElem;
            Text = PropFormDescriptor.description + " - ��������";
            this._preName = PropFormDescriptor.name;
            if (PropFormDescriptor.userData.ContainsKey("sign")) this._preSign = PropFormDescriptor.userData["sign"];
            this._preIndType = Convert.ToInt32(PropFormDescriptor.userData["inOutType"]);
            this.textName.Text = PropFormDescriptor.name.Split('[')[0];
            this.ChooseList();
            this.comboBoxType.Items.Clear();
            this.comboBoxType.Items.AddRange(this._typeList.ToArray());
            try
            {
                if (this.baseComboBox.Visible)
                {
                    this.baseComboBox.SelectedIndex = this._preIndBase;
                }

                int index = this.CorrectTypeIndex(this._preIndType);
                this.comboBoxType.SelectedIndex = index;
            }
            catch
            {
                if (this.baseComboBox.Visible)
                {
                    this._preIndBase = 0;
                    this.baseComboBox.SelectedIndex = 0;
                }

                this._preIndType = 0;
                this.comboBoxType.SelectedIndex = 0;
            }
        }

        private int CorrectTypeIndex(int index)
        {
            int retIndex = index;
            if (this._preIndBase == 0 && PropFormDescriptor.TypeElem == "in")
            {
                if (this._deviceName == "MR771" && this._deviceVers >= 1.14)
                {
                    string value = PropFormsStrings.MRUniversalSignals(this._deviceObj.DeviceType, this._deviceObj.DevicePlant)[index];
                    retIndex = this.comboBoxType.Items.IndexOf(value);
                }

                if (this._deviceName == "MR771" && this._deviceVers >= 3.09)
                {
                    string value = PropFormsStrings.MRUniversalSignals(this._deviceObj.DeviceType, this._deviceObj.DevicePlant)[index];
                    retIndex = this.comboBoxType.Items.IndexOf(value);
                }

                if ((this._deviceName == "MR901" || this._deviceName == "��901") && this._deviceVers >= 2.10 &&
                    this._deviceVers < 3.00)
                {
                    string value = PropFormsStrings.MR901SignalsBig(this._deviceObj.DevicePlant)[index];
                    retIndex = this.comboBoxType.Items.IndexOf(value);
                }
                else if ((this._deviceName == "MR901" || this._deviceName == "��901" && this._deviceVers >= 3.00))
                {
                    string value = PropFormsStrings.Mr901NewSignals(this._deviceObj.DevicePlant)[index];
                    retIndex = this.comboBoxType.Items.IndexOf(value);
                }
                else if ((this._deviceName == "MR902" || this._deviceName == "��902") && this._deviceVers >= 2.10 &&
                         this._deviceVers < 3.00)
                {
                    string value = PropFormsStrings.MR902SignalsBig(this._deviceObj.DevicePlant)[index];
                    retIndex = this.comboBoxType.Items.IndexOf(value);
                }
                else if ((this._deviceName == "MR902" || this._deviceName == "��902" && this._deviceVers >= 3.00))
                {
                    string value = PropFormsStrings.Mr902NewSignals(this._deviceObj.DevicePlant)[index];
                    retIndex = this.comboBoxType.Items.IndexOf(value);
                }
                else if ((this._deviceName == "MR801" || this._deviceName == "��801") && this._deviceVers >= 3.0)
                {
                    string value =
                        PropFormsStrings.MR801BitSignals(this._deviceObj.Info.DeviceConfiguration)[index];
                    retIndex = this.comboBoxType.Items.IndexOf(value);
                }
                else if ((this._deviceName == "MR761OBR" || this._deviceName == "��761���") && this._deviceVers >= 3.0)
                {
                    string value =
                        PropFormsStrings.MR761OBRNewBits(this._deviceObj.Info.DeviceConfiguration)[index];
                    retIndex = this.comboBoxType.Items.IndexOf(value);
                }
            }

            if (_preIndBase == 3 && PropFormDescriptor.TypeElem == "in")
            {
                if ((this._deviceName == "MR901" || this._deviceName == "��901" && this._deviceVers >= 3.00))
                {
                    string value = PropFormsStrings.MR90xControlSignalsNEW()[index];
                    retIndex = this.comboBoxType.Items.IndexOf(value);
                }

                if ((this._deviceName == "MR902" || this._deviceName == "��902" && this._deviceVers >= 3.00))
                {
                    string value = PropFormsStrings.MR90xControlSignalsNEW()[index];
                    retIndex = this.comboBoxType.Items.IndexOf(value);
                }
            }
            return retIndex;
        }

        private void ChooseList()
        {
            switch (PropFormDescriptor.TypeElem)
            {
                case "in":
                    {
                        this.InitInput();
                        break;
                    }
                case "out":
                    {
                        this.InitOutput();
                        break;
                    }
                case "journal":
                    {
                        this.InitJournalOutput("�� ");
                        break;
                    }
                case "journalA":
                    {
                        this.InitJournalOutput("�� ");
                        break;
                    }
            }
            if(PropFormDescriptor.TypeElem == "in")
                this.CheckBaseIndex();
        }

        private void CheckBaseIndex()
        {
            int index = this.CorrectTypeIndex(this._preIndType);
            if (this._typeList.Count <= index)
            {
                bool check761Obr = this._deviceName == "MR761OBR" || this._deviceName == "��761���";

                this._preIndType = 0;
                if (this._deviceName == "MR5" && this._deviceName == "MR801" && check761Obr && !PropFormDescriptor.userData.ContainsKey("baseNum"))
                {
                    this._preIndBase = 0;
                    PropFormDescriptor.userData.Add("baseNum", this._preIndType.ToString(CultureInfo.CurrentCulture));
                }


                //if (PropFormDescriptor.userData.ContainsKey("baseNum"))
                //{
                //    this._preIndBase = 0;
                //    PropFormDescriptor.userData.Add("baseNum", this._preIndType.ToString(CultureInfo.CurrentCulture));
                //}

                this.ChooseList();
            }
        }

        private void InitInput()
        {
            this.captionOfSygnal.Visible = false;
            this.captionLabel.Visible = false;
            if (PropFormDescriptor.userData.ContainsKey("baseNum"))
            {
                this._baseLabel.Visible = true;
                this.baseComboBox.Visible = true;
                this.baseComboBox.Items.Clear();
                this.baseComboBox.Items.AddRange(this.BaseStrings);
                this._preIndBase = Convert.ToInt32(PropFormDescriptor.userData["baseNum"]);
            }
            else
            {
                this._baseLabel.Visible = false;
                this.baseComboBox.Visible = false;
            }
            this._typeList = InitInList(this._deviceName, this._preIndBase, this._deviceVers, this.GetDeviceConfig(this._deviceObj));
            this.labelSignal.Text = "������� ������";
            this.comboBoxType.MaxDropDownItems = 30;
        }

        private string GetDeviceConfig(Device device)
        {
            if(device == null) return string.Empty;
            
            if ((device.DeviceType == "MR801" || device.DeviceType == "��801") && this._deviceVers >= 3.0)
            {
                return device.Info.DeviceConfiguration;
            }

            if ((device.DeviceType == "MR761OBR" || device.DeviceType == "��761���") && this._deviceVers >= 3.0)
            {
                return device.Info.DeviceConfiguration;
            }

            return device.DevicePlant;
        }

        private void InitOutput()
        {
            this.captionOfSygnal.Visible = false;
            this.captionLabel.Visible = false;
            this._baseLabel.Visible = false;
            this.baseComboBox.Visible = false;
            if (this._deviceName == "MR600" || this._deviceName == "MR550" || this._deviceName == "TZL" || this._deviceName == "MR741"
                || this._deviceName == "MR500" || this._deviceName == "MR700" || this._deviceName == "MR750" || this._deviceName == "MR5")
            {
                this._typeList = InitOutList(24);
            }
            else if ((this._deviceName == "MR901" || this._deviceName == "MR902") && this._deviceVers >= 2.10
                || this._deviceName == "MR771" && this._deviceVers >= 1.07 
                || this._deviceName == "MR761" && this._deviceVers >= 3.03
                || this._deviceName == "MR762" && this._deviceVers >= 3.03
                || this._deviceName == "MR763" && this._deviceVers >= 3.03
                || this._deviceName == "MR801" && this._deviceVers >= 3.0
                || (this._deviceName == "MR761OBR" || this._deviceName == "��761���")
                || this._deviceName == "MR901" && this._deviceVers >= 3.00 ||
                     this._deviceName == "MR902" && this._deviceVers >= 3.00)
            {
                if (this._deviceName == "MR771" && this._deviceVers >= 1.12 && this._deviceVers < 1.14)
                {
                    this._typeList = PropFormsStrings.MR771SSLList != null ? PropFormsStrings.MR771SSLList : InitOutList(48);
                }
                else
                { 
                    this._typeList = InitOutList(48);
                }
            }
            else
            {
                this._typeList = InitOutList();
            }
            this.labelSignal.Text = "�������� ������";
            this.comboBoxType.MaxDropDownItems = 10;
        }

        private void InitJournalOutput(string jn)
        {
            if ((this._deviceName == "MR771" && this._deviceVers >= 1.04)
                || this._deviceName == "MR761" && this._deviceVers >= 3.0 
                || this._deviceName == "MR762" && this._deviceVers >= 3.0 
                || this._deviceName == "MR763" && this._deviceVers >= 3.0
                || this._deviceName == "MR801" && this._deviceVers >= 3.0
                || (this._deviceName == "MR761OBR" || this._deviceName == "��761���") ||
                this._deviceName == "MR901" && this._deviceVers >= 3.0 || 
                this._deviceName == "MR902" && this._deviceVers >= 3.0)
            {
                this.captionOfSygnal.Visible = true;
                this.captionLabel.Visible = true;
            }
            this._baseLabel.Visible = false;
            this.baseComboBox.Visible = false;
            bool flag = this._deviceName == "MR761" || this._deviceName == "MR762" ||
                        this._deviceName == "MR763" || this._deviceName == "MR801" ||
                        this._deviceName == "MR901" || this._deviceName == "MR902" || 
                        this._deviceName == "MR771" || this._deviceName == "MR801" ||
                        this._deviceName == "MR761OBR" || this._deviceName == "��761���" ||
                        this._deviceName == "MR901" || this._deviceName == "MR902";
            int count = flag ? 100 : 64;
            this._typeList = InitJournalList(jn, count);
            this.labelSignal.Text = "������ � ������";
            this.comboBoxType.MaxDropDownItems = 10;
        }

        private void textName_TextChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this.textName.Text;
        }
        
        private void baseCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.baseComboBox.Visible) return;
            this._typeList = InitInList(this._deviceName, this.baseComboBox.SelectedIndex, this._deviceVers, this.GetDeviceConfig(this._deviceObj));
            this.comboBoxType.Items.Clear();
            this.comboBoxType.Items.AddRange(this._typeList.ToArray());
            this.comboBoxType.SelectedIndex = 0;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            if (PropFormDescriptor.TypeElem == "in" && PropFormDescriptor.userData.ContainsKey("baseNum"))
            {
                PropFormDescriptor.userData["baseNum"] = this._preIndBase.ToString();
            }
            if (PropFormDescriptor.userData.ContainsKey("sign"))
                PropFormDescriptor.userData["sign"] = this._preSign;
            PropFormDescriptor.userData["inOutType"] = this._preIndType.ToString();
            PropFormDescriptor.name = this._preName;
            okClicked = false;
            Close();
        }

        public bool CheckBlockList()
        {
            this._preIndBase = PropFormDescriptor.userData.ContainsKey("baseNum")
                ? Convert.ToInt32(PropFormDescriptor.userData["baseNum"])
                : 0;
            switch (PropFormDescriptor.TypeElem)
            {
                case "in":
                    {
                        this.InitInput();
                        break;
                    }
                case "out":
                    {
                        this.InitOutput();
                        break;
                    }
                case "journal":
                    {
                        this.InitJournalOutput("�� ");
                        break;
                    }
                case "journalA":
                    {
                        this.InitJournalOutput("�� ");
                        break;
                    }
            }
            
            int ind = this.CorrectTypeIndex(Convert.ToInt32(PropFormDescriptor.userData["inOutType"]));
            return this._typeList.Count > ind;

        }

        private void comboBoxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.captionOfSygnal.Visible) return;
            
            int ind = this.comboBoxType.SelectedIndex;
          
            if (this._deviceName == "MR771" && this._deviceVers >= 1.04 ||
                 this._deviceName == "MR761" || this._deviceName == "MR762" ||
                 this._deviceName == "MR763" && this._deviceVers >= 3.0 ||
                 this._deviceName == "MR801" && this._deviceVers >= 3.0 ||
                 (this._deviceName == "MR761OBR" || this._deviceName == "��761���") ||
                this._deviceName == "MR901" && this._deviceVers >= 3.0 ||
                  this._deviceName == "MR902" && this._deviceVers >= 3.0)
            {
                if (PropFormDescriptor.TypeElem == "journal")
                {
                    this.captionOfSygnal.Text = PropFormsStrings.JournalDictionary[this._deviceObj][ind];
                    PropFormDescriptor.userData["sign"] = PropFormsStrings.JournalDictionary[this._deviceObj][ind];
                }
                if (PropFormDescriptor.TypeElem == "journalA")
                {
                    this.captionOfSygnal.Text = PropFormsStrings.AlarmDictionary[this._deviceObj][ind];
                    PropFormDescriptor.userData["sign"] = PropFormsStrings.AlarmDictionary[this._deviceObj][ind];
                }
            }

            this._preIndType = ind;
        }
        
        private bool _comandFlag;
        private void textBox_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Control)
            {
                if (e.KeyCode == Keys.Z || e.KeyCode == Keys.X ||
                    e.KeyCode == Keys.C || e.KeyCode == Keys.V || 
                    e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    _comandFlag = true;
                    return;
                }
            }
            _comandFlag = false;
        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (_comandFlag)
            {
                _comandFlag = false;
                return;
            }
            e.Handled = IsValidKey(e.KeyChar);
        }

        private bool IsValidKey(char c)
        {
            if (!Regex.Match(c.ToString(), @"[�-��-�]|[A-Za-z]|[0-9]").Success)
            {
                if (c == ' ' || c == '.') return true;
                SendKeys.Send("{BS}");
                return false;
            }
            return true;
        }
    }
}