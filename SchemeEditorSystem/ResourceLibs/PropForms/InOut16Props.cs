using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Interfaces;
using BEMN.MBServer;
using SchemeEditorSystem.managers;


namespace SchemeEditorSystem.ResourceLibs.PropForms
{
    /// <summary>
    /// ����� ������� ��� 16-��������� ������, ������� � ��������
    /// </summary>
    public partial class InOut16Props : PropForm
    {
        private List<string> _typeList;
        private string _device;
        private Device _deviceObj;
        private string _preName;
        private string _preIndBase;
        private string _preIndType;
        private string _preConst;
        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="device">�������� ����������</param>
        public InOut16Props(string device)
        {
            InitializeComponent();
            this._device = device;
            this._preIndBase = _preIndType = "0";
            this._typeList = new List<string>();
        }

        public InOut16Props(Device deviceObj)
        {
            InitializeComponent();
            this._device = string.IsNullOrEmpty(deviceObj.DeviceType)
                ? this.GetEnDevName((deviceObj as INodeView).NodeName)
                : deviceObj.DeviceType;
            this._deviceObj = deviceObj;
            this._preIndBase = _preIndType = "0";
            this._typeList = new List<string>();
        }

        private string GetEnDevName(string ruName)
        {
            switch (ruName)
            {
                case "��600": return "MR600";
                case "��761": return "MR761";
                case "��762": return "MR762";
                case "��763": return "MR763";
                case "��901 (�� �.2.13)":
                case "��901 (�.3.xx)":
                    return "MR901";
                case "��902 (�� �.2.13)":
                case "��902 (�.3.xx)":
                    return "MR902";
                case "��771": return "MR771";
                case "��5": return "MR5";
                case "��801���": return "MR801DVG";
                case "��801 (�� �.2.08)":
                case "��801": return "MR801OLD";
                case "��801 (�.3.xx)": return "MR801NEW";
                default: return ruName;
            }
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            if (PropFormDescriptor.TypeElem == "in16" && PropFormDescriptor.userData.ContainsKey("baseNum"))
            {
                if (this.baseComboBox.SelectedIndex == 2)
                {
                    ushort res;
                    if (!ushort.TryParse(this.ConstVal.Text, out res))
                    {
                        MessageBox.Show(string.Format("���������� ������ �������� � �������� [{0},{1}]", ushort.MinValue,
                                ushort.MaxValue), "��������", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    PropFormDescriptor.userData["const"] = this.ConstVal.Text;
                    PropFormDescriptor.name = string.Format("{0} [Const = {1}]", this.textName.Text, this.ConstVal.Text);
                }
                else
                {
                    PropFormDescriptor.name = this.textName.Text + "[ " + this._typeList[this.comboBoxType.SelectedIndex] + "]";
                }
                PropFormDescriptor.userData["baseNum"] = this.baseComboBox.SelectedIndex.ToString();
            }
            else
            {
                PropFormDescriptor.name = this.textName.Text + "[ " + this._typeList[this.comboBoxType.SelectedIndex] + "]";
            }

            if (_device == "MR902" && Common.VersionConverter(_deviceObj.DeviceVersion) >= 3.00)
            {
                string value = this.comboBoxType.SelectedItem.ToString();
                int key =
                    PropFormsStrings.MR902Signals16New(this._deviceObj.DevicePlant)
                        .First(d => d.Value.Equals(value))
                        .Key;
                PropFormDescriptor.userData["inOutType"] = key.ToString(CultureInfo.InvariantCulture);
            }

            else
            {
                PropFormDescriptor.userData["inOutType"] = this.comboBoxType.SelectedIndex.ToString();
            }
            okClicked = true;
            this.Close();
        }

        /// <summary>
        /// ������ ������� ��������, � ����������� �� �������� ������� � ���� ��������
        /// </summary>
        /// <param name="device">�������� �������</param>
        /// <param name="baseNum">���� ��������</param>
        /// <param name="deviceType">���������� �����</param>
        /// <returns>������</returns>
        public static List<string> InitInList(string device, double version, int baseNum = 0, string deviceType = "")
        {
            switch (baseNum)
            {
                case 0:
                {
                    switch (device)
                    {
                        case "MR500":
                        case "TZL":
                        case "��801 (�� �.2.08)":
                        case "MR801OLD":
                            return PropFormsStrings.UDZTSignals16;
                        case "MR801":
                        case "MR801NEW":
                            return PropFormsStrings.MR801Analog;
                        case "��761":
                        case "MR761":
                            if (version >= 3.09)
                            {
                                return PropFormsStrings.MRUniversalSignals16(deviceType);
                            }
                            return PropFormsStrings.MR761Signals16;
                        case "��762":
                        case "MR762":
                            return PropFormsStrings.MR762Signals16(deviceType);
                        case "��763":
                        case "MR763":
                            return PropFormsStrings.MR763Signals16;
                        case "MR771":
                        case "��771":
                            if (version >= 1.14)
                            {
                                return PropFormsStrings.MRUniversalSignals16(deviceType);
                            }
                            return PropFormsStrings.MR771Signals16;
                        case "MR901":
                            return PropFormsStrings.DZHSignals16(deviceType);
                        case "MR902":
                            return version < 3.00 ? PropFormsStrings.MR902Signals16(deviceType) : PropFormsStrings.MR902Signals16New(deviceType).Values.ToList();
                        case "MR801DVG":
                        return PropFormsStrings.Mr801DvgSignals16;
                    }
                    break;
                }
                case 1:
                {
                    List<string> typeList = new List<string>();
                    for (int i = 0; i < 510; i++)
                    {
                        typeList.Add(i.ToString());
                    }
                    return typeList;
                }
            }
            return null;
        }
        /// <summary>
        /// ���������� ������ �������� ��������
        /// </summary>
        /// <returns>������</returns>
        public static List<string> InitOutList()
        {
            List<string> typeList = new List<string>();
            for (int i = 0; i < 8; i++)
            {
                typeList.Add("��� " + (i + 1));
            }
            return typeList;
        }

        private void BlockAndProp_Load(object sender, EventArgs e)
        {
            this.TypeBox.Text = PropFormDescriptor.TypeElem;
            this.Text = PropFormDescriptor.description + " - ��������";
            this._preName = PropFormDescriptor.name;
            this._preIndType = PropFormDescriptor.userData["inOutType"];
            this.textName.Text = PropFormDescriptor.name.Split('[')[0];
            int baseNum = 0;
            switch (PropFormDescriptor.TypeElem)
            {
                case "in16":
                {
                    this.baseComboBox.Visible = true;
                    this._preIndBase = PropFormDescriptor.userData["baseNum"];
                    baseNum = Convert.ToInt32(PropFormDescriptor.userData["baseNum"]);
                    this._typeList = InitInList(this._device, Common.VersionConverter(_deviceObj.DeviceVersion), baseNum, this._deviceObj?.DevicePlant);
                    if (this._typeList == null && this._preIndBase != "2")
                    {
                        //����������� ��,� �� ���������� �� ������������ 16-��������� �����
                        Close();
                        return;
                    }
                    this.baseComboBox.SelectedIndex = baseNum;
                    if (baseNum == 2)
                    {
                        this.comboBoxType.Visible = false;
                        this.label2.Visible = false;
                        this.labelSignal.Visible = false;
                        this.ConstVal.Visible = true;
                        try
                        {
                            this.ConstVal.Text = PropFormDescriptor.userData["const"];
                            this._preConst = PropFormDescriptor.userData["const"];
                        }
                        catch
                        {
                            this.ConstVal.Text = this._preConst = "0";
                        }
                    }
                    else
                    {
                        this.comboBoxType.Visible = true;
                        this.labelSignal.Visible = true;
                        this.label2.Visible = true;
                        this.ConstVal.Visible = false;
                    }
                    break;
                }
                case "out16":
                {
                    this.label2.Visible = false;
                    this.baseComboBox.Visible = false;
                    this.ConstVal.Visible = false;
                    this.comboBoxType.MaxDropDownItems = 10;
                    this.comboBoxType.Visible = true;
                    this.labelSignal.Text = "�������� ������";
                    this._typeList = InitOutList();
                    break;
                }
            }
            if (PropFormDescriptor.userData.ContainsKey("baseNum") && baseNum == 2) return;
            this.comboBoxType.Items.Clear();
            this.comboBoxType.Items.AddRange(_typeList.ToArray());
            this.comboBoxType.SelectedIndex = int.Parse(this._preIndType);
        }

        private void textName_TextChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this.textName.Text;
        }

        private void baseComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.baseComboBox.SelectedIndex != 2)
            {
                this._typeList = InitInList(this._device, Common.VersionConverter(_deviceObj.DeviceVersion), this.baseComboBox.SelectedIndex, this._deviceObj?.DevicePlant);
                this.comboBoxType.MaxDropDownItems = 30;
                this.comboBoxType.Visible = true;
                this.comboBoxType.Items.Clear();
                this.comboBoxType.Items.AddRange(_typeList.ToArray());
                this.comboBoxType.SelectedIndex = 0;
                this.labelSignal.Text = "������� ������";
                this.labelSignal.Visible = true;
                this.label2.Visible = true;
                this.ConstVal.Visible = false;
            }
            else 
            {
                this.comboBoxType.Visible = false;
                this.label2.Visible = false;
                this.labelSignal.Visible = false;
                this.ConstVal.Visible = true;
            }
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this._preName;
            PropFormDescriptor.userData["inOutType"] = _preIndType.ToString(CultureInfo.InvariantCulture);
            if (PropFormDescriptor.TypeElem != "in16") return;

            if (PropFormDescriptor.userData.ContainsKey("baseNum"))
            {
                PropFormDescriptor.userData["baseNum"] = _preIndBase;
                if (int.Parse(_preIndBase) == 2)
                {
                    PropFormDescriptor.userData["const"] = _preConst;
                }
            }
            okClicked = false;
            this.Close();
        }      
    }
}