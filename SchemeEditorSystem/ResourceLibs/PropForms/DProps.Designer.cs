namespace SchemeEditorSystem.ResourceLibs.PropForms
{
    partial class DProps
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TypeBox = new System.Windows.Forms.TextBox();
            this.InputGroup = new System.Windows.Forms.GroupBox();
            this.bitAddr = new System.Windows.Forms.ComboBox();
            this.bitCount = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Ok = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textName = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this._outName = new System.Windows.Forms.MaskedTextBox();
            this._outs = new System.Windows.Forms.ComboBox();
            this.Cancel = new System.Windows.Forms.Button();
            this.InputGroup.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "��� ��������";
            // 
            // TypeBox
            // 
            this.TypeBox.Location = new System.Drawing.Point(157, 11);
            this.TypeBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TypeBox.Name = "TypeBox";
            this.TypeBox.ReadOnly = true;
            this.TypeBox.Size = new System.Drawing.Size(112, 22);
            this.TypeBox.TabIndex = 1;
            this.TypeBox.Text = "���������� �������";
            // 
            // InputGroup
            // 
            this.InputGroup.Controls.Add(this.bitAddr);
            this.InputGroup.Controls.Add(this.bitCount);
            this.InputGroup.Controls.Add(this.label4);
            this.InputGroup.Controls.Add(this.label2);
            this.InputGroup.Location = new System.Drawing.Point(4, 79);
            this.InputGroup.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.InputGroup.Name = "InputGroup";
            this.InputGroup.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.InputGroup.Size = new System.Drawing.Size(296, 84);
            this.InputGroup.TabIndex = 3;
            this.InputGroup.TabStop = false;
            this.InputGroup.Text = "������";
            // 
            // bitAddr
            // 
            this.bitAddr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bitAddr.FormattingEnabled = true;
            this.bitAddr.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15"});
            this.bitAddr.Location = new System.Drawing.Point(212, 49);
            this.bitAddr.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bitAddr.Name = "bitAddr";
            this.bitAddr.Size = new System.Drawing.Size(75, 24);
            this.bitAddr.TabIndex = 11;
            this.bitAddr.SelectedIndexChanged += new System.EventHandler(this._bitAddr_SelectedIndexChanged);
            // 
            // bitCount
            // 
            this.bitCount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bitCount.FormattingEnabled = true;
            this.bitCount.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.bitCount.Location = new System.Drawing.Point(212, 17);
            this.bitCount.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bitCount.Name = "bitCount";
            this.bitCount.Size = new System.Drawing.Size(75, 24);
            this.bitCount.TabIndex = 10;
            this.bitCount.SelectedIndexChanged += new System.EventHandler(this._bitCount_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 54);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "����� ������� ����";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 21);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(186, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "���-�� ����������� �����";
            // 
            // Ok
            // 
            this.Ok.Location = new System.Drawing.Point(4, 250);
            this.Ok.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Ok.Name = "Ok";
            this.Ok.Size = new System.Drawing.Size(133, 30);
            this.Ok.TabIndex = 5;
            this.Ok.Text = "�������";
            this.Ok.UseVisualStyleBackColor = true;
            this.Ok.Click += new System.EventHandler(this.Ok_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 47);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "��� ��������";
            // 
            // textName
            // 
            this.textName.Location = new System.Drawing.Point(157, 43);
            this.textName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(112, 22);
            this.textName.TabIndex = 7;
            this.textName.TextChanged += new System.EventHandler(this.textName_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this._outName);
            this.groupBox1.Controls.Add(this._outs);
            this.groupBox1.Location = new System.Drawing.Point(4, 170);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(215, 73);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "���������";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 43);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "���";
            // 
            // _outName
            // 
            this._outName.Location = new System.Drawing.Point(61, 39);
            this._outName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._outName.Name = "_outName";
            this._outName.Size = new System.Drawing.Size(140, 22);
            this._outName.TabIndex = 2;
            this._outName.Text = "Q1";
            this._outName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._outName.TextChanged += new System.EventHandler(this._outName_TextChanged);
            // 
            // _outs
            // 
            this._outs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._outs.FormattingEnabled = true;
            this._outs.Items.AddRange(new object[] {
            "����� 1",
            "����� 2"});
            this._outs.Location = new System.Drawing.Point(8, 0);
            this._outs.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._outs.Name = "_outs";
            this._outs.Size = new System.Drawing.Size(108, 24);
            this._outs.TabIndex = 0;
            this._outs.SelectedIndexChanged += new System.EventHandler(this._outs_SelectedIndexChanged);
            // 
            // Cancel
            // 
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.Location = new System.Drawing.Point(173, 250);
            this.Cancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(133, 30);
            this.Cancel.TabIndex = 9;
            this.Cancel.Text = "������";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // DProps
            // 
            this.AcceptButton = this.Ok;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.CancelButton = this.Cancel;
            this.ClientSize = new System.Drawing.Size(311, 287);
            this.ControlBox = false;
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Ok);
            this.Controls.Add(this.InputGroup);
            this.Controls.Add(this.TypeBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DProps";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "���������� �������  - ��������";
            this.Load += new System.EventHandler(this.BlockAndProp_Load);
            this.InputGroup.ResumeLayout(false);
            this.InputGroup.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TypeBox;
        private System.Windows.Forms.GroupBox InputGroup;
        private System.Windows.Forms.Button Ok;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox bitAddr;
        private System.Windows.Forms.ComboBox bitCount;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox _outs;
        private System.Windows.Forms.MaskedTextBox _outName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button Cancel;
    }
}