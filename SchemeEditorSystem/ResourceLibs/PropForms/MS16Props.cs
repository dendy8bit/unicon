using System;
using SchemeEditorSystem.managers;
using SchemeEditorSystem.descriptors;
using System.Collections;


namespace SchemeEditorSystem.ResourceLibs.PropForms
{
    /// <summary>
    /// ����� ������� 16-���������� ��������������
    /// </summary>
    public partial class MS16Props : PropForm
    {
        private string _preName;
        private int _preCount;
        private int _preAddr;
        private int _preMask;
        /// <summary>
        /// �����������
        /// </summary>
        public MS16Props()
        {
            InitializeComponent();
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            PropFormDescriptor.userData["count"] = this.bitCount.SelectedItem.ToString();// ���������� ����������� ���
            PropFormDescriptor.userData["addr"] = this.bitAddr.SelectedItem.ToString();// ����� ���������� ����(��������)
            
            int countBits = Convert.ToInt32(this.bitCount.SelectedItem);
            BitArray maskBits = new BitArray(countBits);
            maskBits.SetAll(true);
            ushort mask = BitsToUshort(maskBits);
            PropFormDescriptor.userData["mask"] = mask.ToString();

            PropFormDescriptor.listPinDesc.Clear();
            PinDesc temp = new PinDesc();
            temp.Orientation = PinOrientation.PO_RIGHT;
            temp.PinType = PinType.PT_DIRECT;
            temp.PinName = "Q";
            temp.Position = PropFormDescriptor.listPinDesc.Count + 1;
            PropFormDescriptor.listPinDesc.Add(temp);
            for (int i = 0; i < mask + 1; i++)
            {
                PinDesc pin = new PinDesc();
                pin.Orientation = PinOrientation.PO_LEFT;
                pin.PinType = PinType.PT_DIRECT;
                pin.Position = PropFormDescriptor.listPinDesc.Count;
                pin.PinName = "In" + (i+1);
                PropFormDescriptor.listPinDesc.Add(pin);
            }
            temp = new PinDesc();
            temp.Orientation = PinOrientation.PO_LEFT;
            temp.PinType = PinType.PT_DIRECT;
            temp.Position = PropFormDescriptor.listPinDesc.Count + 1;
            temp.PinName = "Y";
            PropFormDescriptor.listPinDesc.Add(temp);
            okClicked = true;
            Close();
        }
        /// <summary>
        /// ������������ ������ ���� '010001' � BitArray
        /// </summary>
        /// <param name="str">��������� �������������</param>
        /// <returns>������� �������������</returns>
        public BitArray StringToBits(string str)
        {
            bool[] bMas = new bool[str.Length];

            for (int i = 0; i < str.Length; i++)
            {
                if ('1' == str[i])
                {
                    bMas[i] = true;
                }
                else if ('0' == str[i])
                {
                    bMas[i] = false;
                }
                else
                {
                    throw new ApplicationException("������ " + str + " ���������� ��������������� � ����. ������ " + bMas[i]);
                }
            }

            return new BitArray(bMas);
        }

        /// <summary>
        /// ������������ ��������� ����� � �����
        /// </summary>
        /// <param name="bits"></param>
        /// <returns>�����</returns>
        public ushort BitsToUshort(BitArray bits)
        {
            ushort temp = 0;
            for (int i = 0; i < bits.Count; i++)
            {
                temp += bits[i] ? (ushort)(Math.Pow(2, i)) : (ushort)0;
            }
            return temp;
        }
        /// <summary>
        /// ���������� �������� ����� � ����� ������ 
        /// �������� : ���� ����� �����, � �� �����, ����� �������� ���-��� �� n ���, ��� n = ����� ���������� ����.
        /// ������ : int number = GetBits(value,8,9) >> 8
        /// </summary>
        /// <param name="value">�����</param>
        /// <param name="indexes">������ �����</param>
        /// <returns>�������� ����� - �������� �������� �����, ������������ � indexes</returns>
        public ushort GetBits(ushort value, params int[] indexes)
        {
            ushort mask = 0;
            for (int i = 0; i < indexes.Length; i++)
            {
                mask |= (ushort)(1 << indexes[i]);
            }
            return (ushort)(value & mask);
        }

        private void BlockAndProp_Load(object sender, EventArgs e)
        {
            this.TypeBox.Text = PropFormDescriptor.TypeElem;
            this.Text = PropFormDescriptor.description + " - ��������";
            this.textName.Text = PropFormDescriptor.name;
            this.bitCount.SelectedItem = PropFormDescriptor.userData["count"];
            this.bitAddr.SelectedItem = PropFormDescriptor.userData["addr"];

            this._preName = PropFormDescriptor.name;
            this._preCount = int.Parse(PropFormDescriptor.userData["count"]);
            this._preAddr = int.Parse(PropFormDescriptor.userData["addr"]);
            this._preMask = ushort.Parse(PropFormDescriptor.userData["mask"]);
        }
        //*******************************

        private void textName_TextChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this.textName.Text;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this._preName;
            PropFormDescriptor.userData["count"] = this._preCount.ToString();
            PropFormDescriptor.userData["addr"] = this._preAddr.ToString();
            PropFormDescriptor.userData["mask"] = this._preMask.ToString();
            okClicked = false;
            this.Close();
        }            
    }
}