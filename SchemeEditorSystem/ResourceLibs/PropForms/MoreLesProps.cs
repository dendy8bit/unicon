using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using SchemeEditorSystem.managers;

namespace SchemeEditorSystem.ResourceLibs.PropForms
{
    /// <summary>
    ////����� ������� ��� ���������� ��������� "������" ��� "������"
    /// </summary>
    public partial class MoreLesProps : PropForm
    {
        private string _preKoeff;
        private string _preShiftR;
        private string _preUstavkaVozvr;
        private string _preName;

        /// <summary>
        /// �����������
        /// </summary>
        public MoreLesProps()
        {
            InitializeComponent();
        }

        Dictionary<int, Diapazon> diapazon = new Dictionary<int, Diapazon>
        {
            {0, new Diapazon(32768, 65535)}, //0
            {1, new Diapazon(16384, 32768)}, //1
            {2, new Diapazon(8192, 16384)}, //2
            {3, new Diapazon(4096, 8192)}, //3
            {4, new Diapazon(2048, 4096)}, //4
            {5, new Diapazon(1024, 2048)}, //5
            {6, new Diapazon(512, 1024)}, //6
            {7, new Diapazon(256, 512)}, //7
            {8, new Diapazon(128, 256)}, //8
            {9, new Diapazon(64, 128)}, //9
            {10, new Diapazon(32, 64)}, //10
            {11, new Diapazon(16, 32)}, //11
            {12, new Diapazon(8, 16)}, //12
            {13, new Diapazon(4, 8)}, //13
            {14, new Diapazon(2, 4)}, //14
            {15, new Diapazon(1, 2)}, //15
            {16, new Diapazon(0, 1)} //16
        };

        private void Ok_Click(object sender, EventArgs e)
        {
            try
            {
                string tKoef = this._koeff.Text.Replace(".", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator)
                    .Replace(",", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                double koeff = Convert.ToDouble(tKoef);
                KeyValuePair<int, Diapazon> diap = diapazon.Where(pair => koeff > pair.Value.min && koeff <= pair.Value.max)
                    .Select(pairs => pairs)
                    .ToList()[0];
                int ustavkaVozvr = Convert.ToInt32((koeff*65535/diap.Value.max).ToString("0"));
                int shiftR = diap.Key;
                PropFormDescriptor.userData["koeff"] = this._koeff.Text;
                PropFormDescriptor.userData["shiftR"] = shiftR.ToString();
                PropFormDescriptor.userData["ustavkaVozvr"] = ustavkaVozvr.ToString();
                okClicked = true;
                this.Close();
            }
            catch (Exception exc)
            {
                if (exc is FormatException)
                {
                    MessageBox.Show("���� ������������ ��������� �����������", "������ ����� ������",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show(exc.Message, "������ ����� ������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                okClicked = false;
            }
        }

        private void BlockAndProp_Load(object sender, EventArgs e)
        {
            this.TypeBox.Text = PropFormDescriptor.TypeElem;
            this.Text = PropFormDescriptor.description + " - ��������";
            this._koeff.Text = PropFormDescriptor.userData["koeff"];
            this.textName.Text = PropFormDescriptor.name;

            this._preName = PropFormDescriptor.name;
            this._preKoeff = PropFormDescriptor.userData["koeff"];
            this._preShiftR = PropFormDescriptor.userData["shiftR"];
            this._preUstavkaVozvr = PropFormDescriptor.userData["ustavkaVozvr"];
        }
        //*******************************

        private void textName_TextChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this.textName.Text;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this._preName;
            PropFormDescriptor.userData["koeff"] = this._preKoeff;
            PropFormDescriptor.userData["shiftr"] = this._preShiftR;
            PropFormDescriptor.userData["ustavkaVozvr"] = this._preUstavkaVozvr;
            okClicked = false;
            this.Close();
        }
    }

    public class Diapazon
    {
        public Diapazon(ushort min, ushort max)
        {
            this.min = min;
            this.max = max;
        }

        public ushort min;
        public ushort max;
    }
}