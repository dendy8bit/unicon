namespace SchemeEditorSystem.ResourceLibs.PropForms
{
    partial class TimerProps
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TypeBox = new System.Windows.Forms.TextBox();
            this.InputGroup = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbTime = new System.Windows.Forms.TextBox();
            this.Ok = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textName = new System.Windows.Forms.TextBox();
            this.rbDirect = new System.Windows.Forms.RadioButton();
            this.rbInverse = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbInverseInp = new System.Windows.Forms.RadioButton();
            this.rbDirectInp = new System.Windows.Forms.RadioButton();
            this.Cancel = new System.Windows.Forms.Button();
            this.InputGroup.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "��� ��������";
            // 
            // TypeBox
            // 
            this.TypeBox.Location = new System.Drawing.Point(175, 15);
            this.TypeBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TypeBox.Name = "TypeBox";
            this.TypeBox.ReadOnly = true;
            this.TypeBox.Size = new System.Drawing.Size(112, 22);
            this.TypeBox.TabIndex = 1;
            this.TypeBox.Text = "���������� �������";
            // 
            // InputGroup
            // 
            this.InputGroup.Controls.Add(this.label4);
            this.InputGroup.Controls.Add(this.comboBoxType);
            this.InputGroup.Controls.Add(this.label2);
            this.InputGroup.Controls.Add(this.tbTime);
            this.InputGroup.Location = new System.Drawing.Point(9, 79);
            this.InputGroup.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.InputGroup.Name = "InputGroup";
            this.InputGroup.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.InputGroup.Size = new System.Drawing.Size(291, 107);
            this.InputGroup.TabIndex = 3;
            this.InputGroup.TabStop = false;
            this.InputGroup.Text = "����� �������";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 59);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "���";
            // 
            // comboBoxType
            // 
            this.comboBoxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Location = new System.Drawing.Point(107, 55);
            this.comboBoxType.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(171, 24);
            this.comboBoxType.TabIndex = 12;
            this.comboBoxType.SelectedIndexChanged += new System.EventHandler(this.comboBoxType_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 27);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "�����, �";
            // 
            // tbTime
            // 
            this.tbTime.Location = new System.Drawing.Point(107, 23);
            this.tbTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbTime.Name = "tbTime";
            this.tbTime.Size = new System.Drawing.Size(171, 22);
            this.tbTime.TabIndex = 8;
            this.tbTime.TextChanged += new System.EventHandler(this.tbTime_TextChanged);
            // 
            // Ok
            // 
            this.Ok.Location = new System.Drawing.Point(9, 293);
            this.Ok.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Ok.Name = "Ok";
            this.Ok.Size = new System.Drawing.Size(133, 30);
            this.Ok.TabIndex = 5;
            this.Ok.Text = "�������";
            this.Ok.UseVisualStyleBackColor = true;
            this.Ok.Click += new System.EventHandler(this.Ok_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 50);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "��� ��������";
            // 
            // textName
            // 
            this.textName.Location = new System.Drawing.Point(175, 47);
            this.textName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(112, 22);
            this.textName.TabIndex = 7;
            this.textName.TextChanged += new System.EventHandler(this.textname_TextChanged);
            // 
            // rbDirect
            // 
            this.rbDirect.AutoSize = true;
            this.rbDirect.Checked = true;
            this.rbDirect.Location = new System.Drawing.Point(13, 23);
            this.rbDirect.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbDirect.Name = "rbDirect";
            this.rbDirect.Size = new System.Drawing.Size(78, 21);
            this.rbDirect.TabIndex = 0;
            this.rbDirect.TabStop = true;
            this.rbDirect.Text = "������";
            this.rbDirect.UseVisualStyleBackColor = true;
            this.rbDirect.CheckedChanged += new System.EventHandler(this.rbDirect_CheckedChanged);
            // 
            // rbInverse
            // 
            this.rbInverse.AutoSize = true;
            this.rbInverse.Location = new System.Drawing.Point(13, 54);
            this.rbInverse.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbInverse.Name = "rbInverse";
            this.rbInverse.Size = new System.Drawing.Size(101, 21);
            this.rbInverse.TabIndex = 1;
            this.rbInverse.Text = "���������";
            this.rbInverse.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbInverse);
            this.groupBox1.Controls.Add(this.rbDirect);
            this.groupBox1.Location = new System.Drawing.Point(161, 193);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(139, 92);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "�����";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbInverseInp);
            this.groupBox2.Controls.Add(this.rbDirectInp);
            this.groupBox2.Location = new System.Drawing.Point(9, 193);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(136, 92);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "����";
            // 
            // rbInverseInp
            // 
            this.rbInverseInp.AutoSize = true;
            this.rbInverseInp.Location = new System.Drawing.Point(13, 54);
            this.rbInverseInp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbInverseInp.Name = "rbInverseInp";
            this.rbInverseInp.Size = new System.Drawing.Size(101, 21);
            this.rbInverseInp.TabIndex = 1;
            this.rbInverseInp.Text = "���������";
            this.rbInverseInp.UseVisualStyleBackColor = true;
            // 
            // rbDirectInp
            // 
            this.rbDirectInp.AutoSize = true;
            this.rbDirectInp.Checked = true;
            this.rbDirectInp.Location = new System.Drawing.Point(13, 23);
            this.rbDirectInp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbDirectInp.Name = "rbDirectInp";
            this.rbDirectInp.Size = new System.Drawing.Size(78, 21);
            this.rbDirectInp.TabIndex = 0;
            this.rbDirectInp.TabStop = true;
            this.rbDirectInp.Text = "������";
            this.rbDirectInp.UseVisualStyleBackColor = true;
            this.rbDirectInp.CheckedChanged += new System.EventHandler(this.rbDirectInp_CheckedChanged);
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(167, 293);
            this.Cancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(133, 30);
            this.Cancel.TabIndex = 9;
            this.Cancel.Text = "������";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // TimerProps
            // 
            this.AcceptButton = this.Ok;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(311, 337);
            this.ControlBox = false;
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Ok);
            this.Controls.Add(this.InputGroup);
            this.Controls.Add(this.TypeBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TimerProps";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "���������� �������  - ��������";
            this.Load += new System.EventHandler(this.BlockAndProp_Load);
            this.InputGroup.ResumeLayout(false);
            this.InputGroup.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TypeBox;
        private System.Windows.Forms.GroupBox InputGroup;
        private System.Windows.Forms.Button Ok;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbTime;
        private System.Windows.Forms.RadioButton rbDirect;
        private System.Windows.Forms.RadioButton rbInverse;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbInverseInp;
        private System.Windows.Forms.RadioButton rbDirectInp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.Button Cancel;
    }
}