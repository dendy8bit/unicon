using System;
using SchemeEditorSystem.managers;


namespace SchemeEditorSystem.ResourceLibs.PropForms
{
    /// <summary>
    /// ��������� �������
    /// </summary>
    public partial class TextProps : PropForm
    {
        private string _oldText;
        /// <summary>
        /// �����������
        /// </summary>
        public TextProps()
        {
            InitializeComponent();
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            okClicked = true;
            this.Close();
        }

        private void BlockAndProp_Load(object sender, EventArgs e)
        {
            this.richTextBox1.Text = PropFormDescriptor.TypeElem;
            _oldText = PropFormDescriptor.TypeElem;
            this.Text = PropFormDescriptor.description + " - ��������";
            this.textName.Text = PropFormDescriptor.name;
        }
        //*******************************

        private void textName_TextChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this.textName.Text;
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.TypeElem = this.richTextBox1.Text;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            PropFormDescriptor.TypeElem = _oldText;
            okClicked = false;
            this.Close();
        }          
    }
}