using System;
using System.Globalization;
using SchemeEditorSystem.managers;
using SchemeEditorSystem.descriptors;


namespace SchemeEditorSystem.ResourceLibs.PropForms
{
    /// <summary>
    /// ����� ������� ��� �������
    /// </summary>
    public partial class TimerProps : PropForm
    {
        private string _prename;
        private PinType _pretype1;
        private PinType _pretype2;
        private string _preTime;
        private string[] _typeList;
        /// <summary>
        /// �����������
        /// </summary>
        public TimerProps()
        {
            InitializeComponent();
        }

        public static string[] InitList()
        {
            return new[]
            {
                "�� ����.",
                "�� �����.",
                "���. �� ��.1",
                "���. �� ��.1",
                "���. �� ��.2",
                "���. �� ��.2"
            };
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            PropFormDescriptor.name =
                this.textName.Text + "[ " + _typeList[this.comboBoxType.SelectedIndex] + "; " + tbTime.Text + " �]";
            okClicked = true;
            this.Close();
        }

        private void BlockAndProp_Load(object sender, EventArgs e)
        {
            this.TypeBox.Text = PropFormDescriptor.TypeElem;
            this.Text = PropFormDescriptor.description + " - ��������";
            this.tbTime.Text = PropFormDescriptor.userData["time"]
                .Replace(",", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator)
                .Replace(".", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
            this.textName.Text = PropFormDescriptor.name.Split('[')[0];
            
            _typeList = InitList();

            this.comboBoxType.MaxDropDownItems = 10;
            this.comboBoxType.Items.Clear();
            this.comboBoxType.Items.AddRange(_typeList);
            this.comboBoxType.SelectedIndex = Convert.ToInt32(PropFormDescriptor.userData["timerType"]);

            if (PropFormDescriptor.listPinDesc[0].PinType == PinType.PT_DIRECT)
            {
                rbDirect.Checked = true;
            }
            else
            {
                rbInverse.Checked = true;
            }

            if (PropFormDescriptor.listPinDesc[1].PinType == PinType.PT_DIRECT)
            {
                rbDirectInp.Checked = true;
            }
            else
            {
                rbInverseInp.Checked = true;
            }
            _prename = PropFormDescriptor.name;
            _preTime = PropFormDescriptor.userData["time"];
            _pretype1 = PropFormDescriptor.listPinDesc[0].PinType;
            _pretype2 = PropFormDescriptor.listPinDesc[1].PinType;
        }
        //*******************************

        private void textname_TextChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this.textName.Text;
        }

        private void rbDirect_CheckedChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.listPinDesc[0].PinType = rbDirect.Checked == false ? PinType.PT_INVERSE : PinType.PT_DIRECT;
        }

        private void rbDirectInp_CheckedChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.listPinDesc[1].PinType = rbDirectInp.Checked == false ? PinType.PT_INVERSE : PinType.PT_DIRECT;
        }

        private void tbTime_TextChanged(object sender, EventArgs e)
        {
            string str = tbTime.Text;
            bool correct = true;
            foreach(char c in str)
            {
                if ((c == CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToCharArray()[0]) || (c == '0') || (c == '1') || (c == '2') || (c == '3')
                    || (c == '4') || (c == '5') || (c == '6') || (c == '7') || (c == '8')
                    || (c == '9')) continue;
                correct = false;
                break;
            }
            if (correct)
            {
                PropFormDescriptor.userData["time"] = tbTime.Text;
            }
            else
            {
                tbTime.Text = PropFormDescriptor.userData["time"];
            }
        }

        private void comboBoxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.userData["timerType"] = this.comboBoxType.SelectedIndex.ToString();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this._prename;
            PropFormDescriptor.userData["time"] = this._preTime;
            PropFormDescriptor.listPinDesc[0].PinType = this._pretype1;
            PropFormDescriptor.listPinDesc[1].PinType = this._pretype2;
            okClicked = false;
            this.Close();
        }
    }
}