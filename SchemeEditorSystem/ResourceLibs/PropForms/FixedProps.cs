using System;
using SchemeEditorSystem.managers;


namespace SchemeEditorSystem.ResourceLibs.PropForms
{
    /// <summary>
    /// ����� ������� ��� ��������� ���������, ����� ���: ��������, �������
    /// </summary>
    public partial class FixedProps : PropForm
    {
        private string _preName;
        /// <summary>
        /// �����������
        /// </summary>
        public FixedProps()
        {
            InitializeComponent();
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            okClicked = true;
            this.Close();
        }

        private void BlockAndProp_Load(object sender, EventArgs e)
        {
            this.TypeBox.Text = PropFormDescriptor.TypeElem;
            this.Text = PropFormDescriptor.description + " - ��������";
            this.textName.Text = PropFormDescriptor.name;
            this._preName = PropFormDescriptor.name;
        }
        //*******************************

        private void textName_TextChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this.textName.Text;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this._preName;
            okClicked = false;
            this.Close();
        }    
    }
}