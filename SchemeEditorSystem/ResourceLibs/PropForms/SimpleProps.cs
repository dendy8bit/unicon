using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SchemeEditorSystem.managers;
using SchemeEditorSystem.descriptors;


namespace SchemeEditorSystem.ResourceLibs.PropForms
{
    /// <summary>
    /// ����� ������� ��� ���������� ���������� � ����������� ������� � ����� �������
    /// ���� "+", "-", "���", "����" � �.�.
    /// </summary>
    public partial class SimpleProps : PropForm
    {
        private List<PinDesc> _preList;
        private string _preName;
        /// <summary>
        /// �����������
        /// </summary>
        public SimpleProps()
        {
            InitializeComponent();
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            int count = PropFormDescriptor.listPinDesc.Count(p => p.Orientation == PinOrientation.PO_LEFT);
            if (count >= 2)
            {
                okClicked = true;
                this.Close();
            }
            else
            {
                okClicked = false;
                MessageBox.Show("���������� ������� �� ������������ ������ 2 ������", "��������", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
        }

        private void BlockAndProp_Load(object sender, EventArgs e)
        {
            this.TypeBox.Text = PropFormDescriptor.TypeElem;
            this.Text = PropFormDescriptor.description + " - ��������";
            this.textInputNum.Text = (PropFormDescriptor.listPinDesc.Count-1).ToString();
            this.textName.Text = PropFormDescriptor.name;
            this.AddInButtonI.Visible = PropFormDescriptor.TypeElem == "&" || PropFormDescriptor.TypeElem == "|"
                                        || PropFormDescriptor.TypeElem == "^";

            this._preName = PropFormDescriptor.name;
            this._preList = new List<PinDesc>(PropFormDescriptor.listPinDesc);
        }
        //*******************************

        private void textName_TextChanged(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this.textName.Text;
        }

        private void AddInbutton_Click(object sender, EventArgs e)
        {

            if (PropFormDescriptor.listPinDesc.Count(p => p.Orientation == PinOrientation.PO_LEFT) < 8)
            {
                PinDesc temp = new PinDesc();
                temp.Orientation = PinOrientation.PO_LEFT;
                temp.PinType = PinType.PT_DIRECT;
                temp.Position = PropFormDescriptor.listPinDesc.Count;
                PropFormDescriptor.listPinDesc.Add(temp);
                this.textInputNum.Text = (PropFormDescriptor.listPinDesc.Count - 1).ToString();
            }
            else
            {
                MessageBox.Show("���������� ������� �� ������������ ������ 8 ������", "��������", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
        }

        private void AddInButtonI_Click(object sender, EventArgs e)
        {
            if (PropFormDescriptor.listPinDesc.Count(p => p.Orientation == PinOrientation.PO_LEFT) < 8)
            {
                PinDesc temp = new PinDesc();
                temp.Orientation = PinOrientation.PO_LEFT;
                temp.PinType = PinType.PT_INVERSE;
                temp.Position = PropFormDescriptor.listPinDesc.Count;
                PropFormDescriptor.listPinDesc.Add(temp);
                this.textInputNum.Text = (PropFormDescriptor.listPinDesc.Count - 1).ToString();
            }
            else
            {
                MessageBox.Show("���������� ������� �� ������������ ������ 8 ������", "��������", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
        }

        private void DelInButton_Click(object sender, EventArgs e)
        {
            int count = PropFormDescriptor.listPinDesc.Count(p => p.Orientation == PinOrientation.PO_LEFT);
            if (count > 0)
            {
                PropFormDescriptor.listPinDesc.RemoveAt(PropFormDescriptor.listPinDesc.Count - 1);
                this.textInputNum.Text = (count-1).ToString();
            }
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            PropFormDescriptor.name = this._preName;
            PropFormDescriptor.listPinDesc = this._preList;
            okClicked = false;
            this.Close();
        }
    }
}