namespace SchemeEditorSystem.ResourceLibs.PropForms
{
    partial class InOutProps
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param pinName="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TypeBox = new System.Windows.Forms.TextBox();
            this.Ok = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textName = new System.Windows.Forms.TextBox();
            this.labelSignal = new System.Windows.Forms.Label();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.baseComboBox = new System.Windows.Forms.ComboBox();
            this._baseLabel = new System.Windows.Forms.Label();
            this.Cancel = new System.Windows.Forms.Button();
            this.captionOfSygnal = new System.Windows.Forms.MaskedTextBox();
            this.captionLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "��� ��������";
            // 
            // TypeBox
            // 
            this.TypeBox.Location = new System.Drawing.Point(92, 9);
            this.TypeBox.Name = "TypeBox";
            this.TypeBox.ReadOnly = true;
            this.TypeBox.Size = new System.Drawing.Size(118, 20);
            this.TypeBox.TabIndex = 1;
            this.TypeBox.Text = "���������� �������";
            // 
            // Ok
            // 
            this.Ok.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Ok.Location = new System.Drawing.Point(75, 123);
            this.Ok.Name = "Ok";
            this.Ok.Size = new System.Drawing.Size(111, 24);
            this.Ok.TabIndex = 5;
            this.Ok.Text = "�������";
            this.Ok.UseVisualStyleBackColor = true;
            this.Ok.Click += new System.EventHandler(this.Ok_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(222, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "��� ��������";
            // 
            // textName
            // 
            this.textName.Location = new System.Drawing.Point(309, 9);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(85, 20);
            this.textName.TabIndex = 7;
            this.textName.TextChanged += new System.EventHandler(this.textName_TextChanged);
            // 
            // labelSignal
            // 
            this.labelSignal.AutoSize = true;
            this.labelSignal.Location = new System.Drawing.Point(12, 95);
            this.labelSignal.Name = "labelSignal";
            this.labelSignal.Size = new System.Drawing.Size(42, 13);
            this.labelSignal.TabIndex = 12;
            this.labelSignal.Text = "������";
            // 
            // comboBoxType
            // 
            this.comboBoxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxType.DropDownWidth = 330;
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Location = new System.Drawing.Point(114, 92);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(280, 21);
            this.comboBoxType.TabIndex = 11;
            this.comboBoxType.SelectedIndexChanged += new System.EventHandler(this.comboBoxType_SelectedIndexChanged);
            // 
            // baseComboBox
            // 
            this.baseComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.baseComboBox.FormattingEnabled = true;
            this.baseComboBox.Location = new System.Drawing.Point(273, 65);
            this.baseComboBox.Name = "baseComboBox";
            this.baseComboBox.Size = new System.Drawing.Size(121, 21);
            this.baseComboBox.TabIndex = 13;
            this.baseComboBox.SelectedIndexChanged += new System.EventHandler(this.baseCombo_SelectedIndexChanged);
            // 
            // _baseLabel
            // 
            this._baseLabel.AutoSize = true;
            this._baseLabel.Location = new System.Drawing.Point(216, 68);
            this._baseLabel.Name = "_baseLabel";
            this._baseLabel.Size = new System.Drawing.Size(32, 13);
            this._baseLabel.TabIndex = 14;
            this._baseLabel.Text = "����";
            // 
            // Cancel
            // 
            this.Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.Location = new System.Drawing.Point(219, 123);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(111, 24);
            this.Cancel.TabIndex = 15;
            this.Cancel.Text = "������";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // captionOfSygnal
            // 
            this.captionOfSygnal.Location = new System.Drawing.Point(114, 39);
            this.captionOfSygnal.Mask = "CCCCCCCCCCCCCCCCCCCC";
            this.captionOfSygnal.Name = "captionOfSygnal";
            this.captionOfSygnal.PromptChar = ' ';
            this.captionOfSygnal.Size = new System.Drawing.Size(280, 20);
            this.captionOfSygnal.TabIndex = 16;
            this.captionOfSygnal.Visible = false;
            this.captionOfSygnal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            this.captionOfSygnal.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.textBox_PreviewKeyDown);
            // 
            // captionLabel
            // 
            this.captionLabel.AutoSize = true;
            this.captionLabel.Location = new System.Drawing.Point(12, 42);
            this.captionLabel.Name = "captionLabel";
            this.captionLabel.Size = new System.Drawing.Size(95, 13);
            this.captionLabel.TabIndex = 14;
            this.captionLabel.Text = "������� �������";
            this.captionLabel.Visible = false;
            // 
            // InOutProps
            // 
            this.AcceptButton = this.Ok;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.CancelButton = this.Cancel;
            this.ClientSize = new System.Drawing.Size(406, 159);
            this.ControlBox = false;
            this.Controls.Add(this.captionOfSygnal);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.captionLabel);
            this.Controls.Add(this._baseLabel);
            this.Controls.Add(this.baseComboBox);
            this.Controls.Add(this.labelSignal);
            this.Controls.Add(this.comboBoxType);
            this.Controls.Add(this.textName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Ok);
            this.Controls.Add(this.TypeBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InOutProps";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "���������� �������  - ��������";
            this.Shown += new System.EventHandler(this.BlockAndProp_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TypeBox;
        private System.Windows.Forms.Button Ok;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.Label labelSignal;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.ComboBox baseComboBox;
        private System.Windows.Forms.Label _baseLabel;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.MaskedTextBox captionOfSygnal;
        private System.Windows.Forms.Label captionLabel;
    }
}