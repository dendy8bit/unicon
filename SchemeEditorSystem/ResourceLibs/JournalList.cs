﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using BEMN.MBServer;

namespace SchemeEditorSystem.ResourceLibs
{
    /// <summary>
    /// Класс, содержащий в сее подписи сигналов ЖС и ЖА в СПЛ
    /// </summary>
    public class ListsOfJournals
    {
        private string _deviceName;


        /// <summary>
        /// Список подписей ЖА и ЖС для устройства. Сериализует и десериализует файл
        /// </summary>
        /// <param name="device"></param>
        public ListsOfJournals(string device) : this()
        {
            this._deviceName = device;
        }

        private ListsOfJournals()
        {
            this.Version = 0;
            this.SysJournal = new JournalList();
            this.AlarmJournal = new JournalList();
        }
        /// <summary>
        /// Версия устройства
        /// </summary>
        [XmlAttribute("version")]
        public double Version { get; set; }

        /// <summary>
        /// Подписи списка системного журнала
        /// </summary>
        [XmlElement("jsys_list")]
        public JournalList SysJournal { get; set; }

        /// <summary>
        /// Подписи списка журнала аварий
        /// </summary>
        [XmlElement("jalm_list")]
        public JournalList AlarmJournal { get; set; }

        /// <summary>
        /// Сериализует объект класса для сохранения в файл
        /// </summary>
        /// <param name="writer">XmlTextWriter, с помощью которого записываем</param>
        public void XmlToFile(XmlWriter writer)
        {
            writer.WriteStartDocument();
            writer.WriteStartElement(this._deviceName);
            writer.WriteAttributeString("version", Common.ChangedVersionForZerooo(this.Version.ToString(CultureInfo.InvariantCulture), _deviceName, Version));

            writer.WriteStartElement("jsys_list");
            this.SysJournal.XmlToFile(writer);
            writer.WriteEndElement();
            writer.WriteStartElement("jalm_list");
            this.AlarmJournal.XmlToFile(writer);
            writer.WriteEndElement();

            writer.WriteEndElement();
            writer.Flush();
        }

        /// <summary>
        /// Десериализация объекта из файла
        /// </summary>
        /// <param name="reader">XmlTextReader, с помощью которого читаем</param>
        public void XmlFromFile(XmlReader reader)
        {
            do
            {
                reader.Read();
            }
            while (reader.Name != this._deviceName || reader.EOF);
            if(reader.EOF) throw new FileNotFoundException("Данный файл подписей не является файлом подписей устройства "+this._deviceName);
            this.Version = Convert.ToDouble(reader.GetAttribute("version"), CultureInfo.InvariantCulture);
            reader.Read();//пробел
            reader.Read();//вход в тег jsys_list
            this.SysJournal.XmlFromFile(reader);
            reader.Read();//выход из тега jsys_list
            reader.Read();//пробел
            reader.Read();//вход в тег jalm_list
            reader.Read();//пробел
            this.AlarmJournal.XmlFromFile(reader);
        }
    }

    public class JournalList
    {
        public JournalList()
        {
            this.MessagesList = new List<string>();
            this.Len = 20;
            this.Count = 100;
        }
        /// <summary>
        /// Длина одной подписи
        /// </summary>
        [XmlAttribute("lenStr")]
        public int Len { get; set; }
        /// <summary>
        /// Количество всех подписей
        /// </summary>
        [XmlAttribute("cntStr")]
        public int Count { get; set; }
        /// <summary>
        /// Список самих подписей для сериализации
        /// </summary>
        [XmlElement("n")]
        public List<string> MessagesList { get; set; }
        
        /// <summary>
        /// Сериализует объект класса для сохранения в файл
        /// </summary>
        /// <param name="writer">XmlTextWriter, с помощью которого записываем</param>
        public void XmlToFile(XmlWriter writer)
        {
            writer.WriteAttributeString("lenStr", this.Len.ToString());
            writer.WriteAttributeString("cntStr", this.Count.ToString());
            foreach (string message in this.MessagesList)
            {
                writer.WriteStartElement("n");
                writer.WriteCData(message);
                writer.WriteEndElement();
            }
        }
        /// <summary>
        /// Десериализация объекта из файла
        /// </summary>
        /// <param name="reader">XmlTextReader, с помощью которого читаем</param>
        public void XmlFromFile(XmlReader reader)
        {
            this.Len = Convert.ToInt32(reader.GetAttribute("lenStr"));
            this.Count = Convert.ToInt32(reader.GetAttribute("cntStr"));
            this.MessagesList.Clear();
            try
            {
                for (int i = 0; i < this.Count; i++)
                {
                    reader.Read();//пробел
                    reader.Read();//вход в тег
                    string cdatamessage = reader.ReadString();
                    this.MessagesList.Add(cdatamessage);
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
