using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;
using SchemeEditorSystem.descriptors;

namespace SchemeEditorSystem
{
    public partial class LibraryBox : Form
    {
        private List<BlockDesc> descriptorsList;
        private BlockDesc selectedBlock;

        private string _commonGroup = "���";
        private string _blockLib;
        public LibraryBox(string blockLib)
        {
            InitializeComponent();
            descriptorsList = new List<BlockDesc>();
            _blockLib = blockLib;
        }

        private LibraryBox()
        {
            InitializeComponent();
            descriptorsList = new List<BlockDesc>();
        }
        private void UpdateListView()
        {
            LibListView.Items.Clear();

            if(GroupComboBox.SelectedItem.ToString() == _commonGroup)
            {
                foreach (BlockDesc bd in descriptorsList)
                {
                    LibListView.Items.Add(bd.description, 0);
                }
            }
            else
            {
                foreach (BlockDesc bd in descriptorsList)
                {
                    if (GroupComboBox.SelectedItem.ToString() == bd.group)
                    {
                        LibListView.Items.Add(bd.description, 0);
                    }
                }
            }


        }
        private void UpdateComboGroups()
        {
            GroupComboBox.Items.Clear();
            foreach (BlockDesc bd in descriptorsList)
            {
                if (bd.group!=null)
                {
                    if (!GroupComboBox.Items.Contains(bd.group))
                    {
                        GroupComboBox.Items.Add(bd.group);
                    }
                }
            }

            GroupComboBox.Items.Add(_commonGroup);

            if (GroupComboBox.Items.Count > 0)
            {
                GroupComboBox.Select(1, 1);
            }
        }
        private void GroupComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateListView();
        }

        private void LibListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection elements = this.LibListView.SelectedItems;
            foreach (BlockDesc bd in descriptorsList)
            {
                foreach (ListViewItem item in elements)
                {
                    if (item.Text == bd.description)
                    {
                        blockPreView1.SetViewElement(bd);
                        selectedBlock = bd;
                    }
                }
            }
        }

        private void LibraryBox_Load(object sender, EventArgs e)
        {
            ImageList imageListSmall = new ImageList();
            imageListSmall.Images.Add(AssemblyResource.Resource1.element);
            LibListView.SmallImageList = imageListSmall;
            LibListView.Columns.Add("�������",500);
            XmlTextReader reader = new XmlTextReader(new System.IO.StringReader(_blockLib)); ;

            reader.WhitespaceHandling = WhitespaceHandling.None;
            reader.Read();//Root
            reader.Read();
            int Count = Convert.ToInt32(reader.GetAttribute("Count"));
            for (int i = 0; i < Count; i++)
            {
                reader.Read();
                BlockDesc bd = new BlockDesc();
                bd.PropertiesFromXML(reader);
                descriptorsList.Add(bd);
            }
            reader.Close();
            UpdateComboGroups();

        }

        private void LibListView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            if (selectedBlock != null)
            {
                BlockDesc bd = new BlockDesc(selectedBlock);
                DoDragDrop(bd, DragDropEffects.Move);
            }
        }

        private void blockPreView1_MouseMove(object sender, MouseEventArgs e)
        {
            if (selectedBlock != null)
            {
                BlockDesc bd = new BlockDesc(selectedBlock);
                DoDragDrop(bd, DragDropEffects.Move);
            }
        }
    }
}