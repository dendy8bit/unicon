namespace SchemeEditorSystem
{
    partial class NewSchematicForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SourceNameTextBox = new System.Windows.Forms.TextBox();
            this.OkButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.labelSchematicName = new System.Windows.Forms.Label();
            this.labelFormat = new System.Windows.Forms.Label();
            this.sheetFormatCombo = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // SourceNameTextBox
            // 
            this.SourceNameTextBox.Location = new System.Drawing.Point(149, 6);
            this.SourceNameTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.SourceNameTextBox.Name = "SourceNameTextBox";
            this.SourceNameTextBox.Size = new System.Drawing.Size(209, 22);
            this.SourceNameTextBox.TabIndex = 0;
            // 
            // OkButton
            // 
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.Location = new System.Drawing.Point(13, 72);
            this.OkButton.Margin = new System.Windows.Forms.Padding(4);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(167, 28);
            this.OkButton.TabIndex = 1;
            this.OkButton.Text = "�������";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(194, 72);
            this.CancelButton.Margin = new System.Windows.Forms.Padding(4);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(169, 28);
            this.CancelButton.TabIndex = 2;
            this.CancelButton.Text = "������";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // labelSchematicName
            // 
            this.labelSchematicName.AutoSize = true;
            this.labelSchematicName.Location = new System.Drawing.Point(21, 11);
            this.labelSchematicName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSchematicName.Name = "labelSchematicName";
            this.labelSchematicName.Size = new System.Drawing.Size(116, 17);
            this.labelSchematicName.TabIndex = 3;
            this.labelSchematicName.Text = "�������� �����";
            // 
            // labelFormat
            // 
            this.labelFormat.AutoSize = true;
            this.labelFormat.Location = new System.Drawing.Point(21, 40);
            this.labelFormat.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelFormat.Name = "labelFormat";
            this.labelFormat.Size = new System.Drawing.Size(107, 17);
            this.labelFormat.TabIndex = 4;
            this.labelFormat.Text = "������ ����� ";
            // 
            // sheetFormatCombo
            // 
            this.sheetFormatCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sheetFormatCombo.FormattingEnabled = true;
            this.sheetFormatCombo.Items.AddRange(new object[] {
            "A4 - ���������",
            "A4 - ����������",
            "A3 - ���������",
            "A3 - ����������",
            "A2 - ���������",
            "A2 - ����������",
            "A1 - ���������",
            "A1 - ����������",
            "A0 - ���������",
            "A0 - ����������"});
            this.sheetFormatCombo.Location = new System.Drawing.Point(149, 40);
            this.sheetFormatCombo.Margin = new System.Windows.Forms.Padding(4);
            this.sheetFormatCombo.Name = "sheetFormatCombo";
            this.sheetFormatCombo.Size = new System.Drawing.Size(209, 24);
            this.sheetFormatCombo.TabIndex = 5;
            // 
            // NewSchematicForm
            // 
            this.AcceptButton = this.OkButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(376, 108);
            this.ControlBox = false;
            this.Controls.Add(this.sheetFormatCombo);
            this.Controls.Add(this.labelFormat);
            this.Controls.Add(this.labelSchematicName);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.SourceNameTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "NewSchematicForm";
            this.Text = "����� ��������";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox SourceNameTextBox;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Label labelSchematicName;
        private System.Windows.Forms.Label labelFormat;
        private System.Windows.Forms.ComboBox sheetFormatCombo;
    }
}