using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


using BEMN.Devices;
using BEMN.Interfaces;
using BEMN.MR74X;
using BEMN.Forms;

namespace BEMN.MR74X.Forms
{
    public partial class AlarmJournalForm : Form, IFormView
    {
        private MR74X _device;

        public AlarmJournalForm()
        {
            InitializeComponent();
        }

        public AlarmJournalForm(MR74X device)
        {
            InitializeComponent();
            _device = device;
            _journalProgress.Maximum = MR74X.ALARMJOURNAL_RECORD_CNT;
            _device.AlarmJournalRecordLoadOk += new IndexHandler(_device_AlarmJournalRecordLoadOk);
            _device.AlarmJournalRecordLoadFail += new IndexHandler(_device_AlarmJournalRecordLoadFail);
        }

        static void _device_AlarmJournalRecordLoadFail(object sender, int index)
        {
            MessageBox.Show("���������� ��������� ������ ������. ��������� �����.", "������ - ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        void _device_AlarmJournalRecordLoadOk(object sender, int index)
        {
            try
            {
                Invoke(new OnDeviceIndexEventHandler(OnAlarmJournalRecordLoadOk), new object[] { index });
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnAlarmJournalRecordLoadOk(int i)
        {
            _journalProgress.Increment(1);
            _journalCntLabel.Text = (i + 1).ToString();
            _journalGrid.Rows.Add(new object[] { i + 1,
                                                 _device.AlarmJournal[i].time, 
                                                 _device.AlarmJournal[i].msg,
                                                 _device.AlarmJournal[i].code,
                                                 _device.AlarmJournal[i].type_value,
                                                 _device.AlarmJournal[i].Ia,
                                                 _device.AlarmJournal[i].Ib,
                                                 _device.AlarmJournal[i].Ic,
                                                 _device.AlarmJournal[i].I0,
                                                 _device.AlarmJournal[i].I1,
                                                 _device.AlarmJournal[i].I2,
                                                 _device.AlarmJournal[i].In,
                                                 _device.AlarmJournal[i].Ig,
                                                 _device.AlarmJournal[i].F,
                                                 _device.AlarmJournal[i].Uab,
                                                 _device.AlarmJournal[i].Ubc,
                                                 _device.AlarmJournal[i].Uca,
                                                 _device.AlarmJournal[i].U0,
                                                 _device.AlarmJournal[i].U1,
                                                 _device.AlarmJournal[i].U2,
                                                 _device.AlarmJournal[i].Un,
                                                 _device.AlarmJournal[i].inSignals1,
                                                 _device.AlarmJournal[i].inSignals2});
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR74X); }
        }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(AlarmJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return (Image)BEMN.MR74X.Properties.Resources.ja;
            }
        }

        public string NodeName
        {
            get { return "������ ������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _readBut_Click(object sender, EventArgs e)
        {
            _device.RemoveAlarmJournal();
            _journalProgress.Value = 0;
            _journalGrid.Rows.Clear();
            _device.LoadAlarmJournal();
        }

        private void AlarmJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _device.RemoveAlarmJournal();
        }

        private void AlarmJournalForm_Activated(object sender, EventArgs e)
        {
            _device.SuspendAlarmJournal(false);
        }

        private void AlarmJournalForm_Deactivate(object sender, EventArgs e)
        {
            _device.SuspendAlarmJournal(true);
        }

        private void _serializeBut_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable("��700_������_������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");
            table.Columns.Add("��� �����������");
            table.Columns.Add("��� �����������");
            table.Columns.Add("Ia");
            table.Columns.Add("Ib");
            table.Columns.Add("Ic");
            table.Columns.Add("I0");
            table.Columns.Add("I1");
            table.Columns.Add("I2");
            table.Columns.Add("Ig");
            table.Columns.Add("In");
            table.Columns.Add("F");
            table.Columns.Add("Uab");
            table.Columns.Add("Ubc");
            table.Columns.Add("Uca");
            table.Columns.Add("U0");
            table.Columns.Add("U1");
            table.Columns.Add("U2");
            table.Columns.Add("Un");
            table.Columns.Add("��.������� 8-1");
            table.Columns.Add("��.������� 9-16");


            List<object> row = new List<object>();

            for (int i = 0; i < _journalGrid.Rows.Count; i++)
            {
                row.Clear();
                for (int j = 0; j < _journalGrid.Columns.Count; j++)
                {
                    row.Add(_journalGrid[j, i].Value);
                }
                table.Rows.Add(row.ToArray());
            }
            if (DialogResult.OK == _saveSysJournalDlg.ShowDialog())
            {
                table.WriteXml(_saveSysJournalDlg.FileName);
            }

        }

        private void _deserializeBut_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable("��700_������_������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");
            table.Columns.Add("��� �����������");
            table.Columns.Add("��� �����������");
            table.Columns.Add("Ia");
            table.Columns.Add("Ib");
            table.Columns.Add("Ic");
            table.Columns.Add("I0");
            table.Columns.Add("I1");
            table.Columns.Add("I2");
            table.Columns.Add("Ig");
            table.Columns.Add("In");
            table.Columns.Add("F");
            table.Columns.Add("Uab");
            table.Columns.Add("Ubc");
            table.Columns.Add("Uca");
            table.Columns.Add("U0");
            table.Columns.Add("U1");
            table.Columns.Add("U2");
            table.Columns.Add("Un");
            table.Columns.Add("��.������� 8-1");
            table.Columns.Add("��.������� 9-16");

            if (DialogResult.OK == _openSysJounralDlg.ShowDialog())
            {
                _journalGrid.Rows.Clear();
                table.ReadXml(_openSysJounralDlg.FileName);
            }

            List<object> row = new List<object>();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                row.Clear();
                row.AddRange(table.Rows[i].ItemArray);
                _journalGrid.Rows.Add(row.ToArray());

            }

        }
    }
}

//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Text;
//using System.Windows.Forms;


//using BEMN.Devices;
//using BEMN.Interfaces;
//using BEMN.MR74X;
//using BEMN.Forms;

//namespace BEMN.MR74X.Forms
//{
//    public partial class AlarmJournalForm : Form, IFormView
//    {
//        private MR74X _device;

//        public AlarmJournalForm()
//        {
//            InitializeComponent();
//        }

//        public AlarmJournalForm(MR74X device)
//        {
//            InitializeComponent();
//            _device = device;
//            _journalProgress.Maximum = MR74X.ALARMJOURNAL_RECORD_CNT;
//            _device.AlarmJournalRecordLoadOk += new IndexHandler(_device_AlarmJournalRecordLoadOk);
//            _device.AlarmJournalRecordLoadFail += new IndexHandler(_device_AlarmJournalRecordLoadFail);
//        }

//        void _device_AlarmJournalRecordLoadFail(object sender, int index)
//        {
//            MessageBox.Show("���������� ��������� ������ ������. ��������� �����.", "������ - ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
//        }

//        void _device_AlarmJournalRecordLoadOk(object sender, int index)
//        {
//            try
//            {
//                Invoke(new OnDeviceIndexEventHandler(OnAlarmJournalRecordLoadOk), new object[] { index });
//            }
//            catch (InvalidOperationException)
//            { }
//        }

//        private void OnAlarmJournalRecordLoadOk(int i)
//        {
//            _journalProgress.Increment(1);
//            _journalCntLabel.Text = (i + 1).ToString();
//            _journalGrid.Rows.Add(new object[] { i + 1,
//                                                 _device.AlarmJournal[i].time, 
//                                                 _device.AlarmJournal[i].msg,
//                                                 _device.AlarmJournal[i].code,
//                                                 _device.AlarmJournal[i].type_value,
//                                                 _device.AlarmJournal[i].Ia,
//                                                 _device.AlarmJournal[i].Ib,
//                                                 _device.AlarmJournal[i].Ic,
//                                                 _device.AlarmJournal[i].I0,
//                                                 _device.AlarmJournal[i].I1,
//                                                 _device.AlarmJournal[i].I2,
//                                                 _device.AlarmJournal[i].In,
//                                                 _device.AlarmJournal[i].Ig,
//                                                 _device.AlarmJournal[i].F,
//                                                 _device.AlarmJournal[i].Uab,
//                                                 _device.AlarmJournal[i].Ubc,
//                                                 _device.AlarmJournal[i].Uca,
//                                                 _device.AlarmJournal[i].U0,
//                                                 _device.AlarmJournal[i].U1,
//                                                 _device.AlarmJournal[i].U2,
//                                                 _device.AlarmJournal[i].Un,
//                                                 _device.AlarmJournal[i].inSignals1,
//                                                 _device.AlarmJournal[i].inSignals2});
//        }

//        #region IFormView Members

//        public Type FormDevice
//        {
//            get { return typeof(MR74X); }
//        }

//        #endregion

//        #region INodeView Members

//        public Type ClassType
//        {
//            get { return typeof(AlarmJournalForm); }
//        }

//        public bool ForceShow
//        {
//            get { return false; }
//        }

//        public Image NodeImage
//        {
//            get
//            {
//                return (Image)BEMN.MR74X.Properties.Resources.ja;
//            }
//        }

//        public string NodeName
//        {
//            get { return "������ ������"; }
//        }

//        public INodeView[] ChildNodes
//        {
//            get { return new INodeView[] { }; }
//        }

//        public bool Deletable
//        {
//            get { return false; }
//        }

//        #endregion

//        private void _readBut_Click(object sender, EventArgs e)
//        {
//            _device.RemoveAlarmJournal();
//            _journalProgress.Value = 0;
//            _journalGrid.Rows.Clear();
//            _device.LoadAlarmJournal();
//        }

//        private void AlarmJournalForm_FormClosing(object sender, FormClosingEventArgs e)
//        {
//            _device.RemoveAlarmJournal();
//        }

//        private void AlarmJournalForm_Activated(object sender, EventArgs e)
//        {
//            _device.SuspendAlarmJournal(false);
//        }

//        private void AlarmJournalForm_Deactivate(object sender, EventArgs e)
//        {
//            _device.SuspendAlarmJournal(true);
//        }

//        private void _serializeBut_Click(object sender, EventArgs e)
//        {
//            DataTable table = new DataTable("TZL_������_������");
//            table.Columns.Add("�����");
//            table.Columns.Add("�����");
//            table.Columns.Add("���������");
//            table.Columns.Add("��� �����������");
//            table.Columns.Add("��� �����������");
//            table.Columns.Add("Ia");
//            table.Columns.Add("Ib");
//            table.Columns.Add("Ic");
//            table.Columns.Add("I0");
//            table.Columns.Add("I1");
//            table.Columns.Add("I2");
//            table.Columns.Add("Ig");
//            table.Columns.Add("In");
//            table.Columns.Add("F");
//            table.Columns.Add("Uab");
//            table.Columns.Add("Ubc");
//            table.Columns.Add("Uca");
//            table.Columns.Add("U0");
//            table.Columns.Add("U1");
//            table.Columns.Add("U2");
//            table.Columns.Add("Un");
//            table.Columns.Add("��.������� 8-1");
//            table.Columns.Add("��.������� 9-16");


//            List<object> row = new List<object>();

//            for (int i = 0; i < _journalGrid.Rows.Count; i++)
//            {
//                row.Clear();
//                for (int j = 0; j < _journalGrid.Columns.Count; j++)
//                {
//                    row.Add(_journalGrid[j, i].Value);
//                }
//                table.Rows.Add(row.ToArray());
//            }
//            if (DialogResult.OK == _saveSysJournalDlg.ShowDialog())
//            {
//                table.WriteXml(_saveSysJournalDlg.FileName);
//            }

//        }

//        private void _deserializeBut_Click(object sender, EventArgs e)
//        {
//            DataTable table = new DataTable("TZL_������_������");
//            table.Columns.Add("�����");
//            table.Columns.Add("�����");
//            table.Columns.Add("���������");
//            table.Columns.Add("��� �����������");
//            table.Columns.Add("��� �����������");
//            table.Columns.Add("Ia");
//            table.Columns.Add("Ib");
//            table.Columns.Add("Ic");
//            table.Columns.Add("I0");
//            table.Columns.Add("I1");
//            table.Columns.Add("I2");
//            table.Columns.Add("Ig");
//            table.Columns.Add("In");
//            table.Columns.Add("F");
//            table.Columns.Add("Uab");
//            table.Columns.Add("Ubc");
//            table.Columns.Add("Uca");
//            table.Columns.Add("U0");
//            table.Columns.Add("U1");
//            table.Columns.Add("U2");
//            table.Columns.Add("Un");
//            table.Columns.Add("��.������� 8-1");
//            table.Columns.Add("��.������� 9-16");

//            if (DialogResult.OK == _openSysJounralDlg.ShowDialog())
//            {
//                _journalGrid.Rows.Clear();
//                table.ReadXml(_openSysJounralDlg.FileName);
//            }

//            List<object> row = new List<object>();
//            for (int i = 0; i < table.Rows.Count; i++)
//            {
//                row.Clear();
//                row.AddRange(table.Rows[i].ItemArray);
//                _journalGrid.Rows.Add(row.ToArray());

//            }

//        }

//    }
//}