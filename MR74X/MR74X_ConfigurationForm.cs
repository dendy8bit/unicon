using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using BEMN.Devices;
using BEMN.Interfaces;
using BEMN.Forms;
using System.Collections;
using BEMN.MR74X.Utils;
using BEMN.MR74X.Data;

namespace BEMN.MR74X.Forms
{
    public partial class ConfigurationForm : Form, IFormView
    {
        private MR74X _device;
        private bool _validatingOk = true;
        private bool _connectingErrors = false;

        public ConfigurationForm()
        {
            InitializeComponent();
        }

        public ConfigurationForm(MR74X device)
        {
            _device = device;
            InitializeComponent();
        }

        void Grid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                DataGridViewComboBoxEditingControl combo = e.Control as DataGridViewComboBoxEditingControl;
                combo.SelectedIndex = 0;
                combo.Items.RemoveAt(combo.Items.Count - 1);
            }
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR74X); }
        }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(ConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return (Image)BEMN.MR74X.Properties.Resources.config.ToBitmap();
            }
        }

        public string NodeName
        {
            get { return "������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            _exchangeProgressBar.Value = 0;
            if (DialogResult.OK == _openConfigurationDlg.ShowDialog())
            {
                try
                {
                    _device.Deserialize(_openConfigurationDlg.FileName);
                }
                catch (System.IO.FileLoadException exc)
                {
                    _processLabel.Text = "���� " + System.IO.Path.GetFileName(exc.FileName) + " �� �������� ������ ������� MR74X ��� ���������";
                    return;
                }


                if (_tokDefenseMainConstraintRadio.Checked)
                {
                    ShowTokDefenses(_device.TokDefensesMain);
                }
                else
                {
                    ShowTokDefenses(_device.TokDefensesReserve);
                }
                if (_voltageDefenseMainConstraintRadio.Checked)
                {
                    ShowVoltageDefenses(_device.VoltageDefensesMain);
                }
                else
                {
                    ShowVoltageDefenses(_device.VoltageDefensesReserve);
                }
                if (_frequenceDefenseMainConstraintRadio.Checked)
                {
                    ShowFrequenceDefenses(_device.FrequenceDefensesMain);
                }
                else
                {
                    ShowFrequenceDefenses(_device.FrequenceDefensesReserve);
                }
                ReadInputSignals();
                ReadExternalDefenses();
                ReadOutputSignals();

                _exchangeProgressBar.Value = 0;
                _processLabel.Text = "���� " + _openConfigurationDlg.FileName + " ��������";
            }
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            ValidateAll();
            if (_validatingOk)
            {
                if (DialogResult.OK == _saveConfigurationDlg.ShowDialog())
                {
                    _exchangeProgressBar.Value = 0;
                    _device.Serialize(_saveConfigurationDlg.FileName);
                    _processLabel.Text = "���� " + _openConfigurationDlg.FileName + " ��������";
                }
            }


        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            _connectingErrors = false;
            _exchangeProgressBar.Value = 0;
            _validatingOk = true;

            ValidateAll();
            if (_validatingOk)
            {
                if (DialogResult.Yes == MessageBox.Show("�������� ������������ MR74X �" + _device.DeviceNumber + " ?", "��������", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                {
                    _processLabel.Text = "���� ������";
                    _device.SaveOutputSignals();
                    _device.SaveInputSignals();
                    _device.SaveExternalDefenses();
                    _device.SaveTokDefenses();
                    _device.SaveVoltageDefenses();

                    _device.SaveAutomaticsPage();
                    _device.SaveFrequenceDefenses();
                    _device.ConfirmConstraint();

                }
            }
        }

        private void ValidateAll()
        {
            _validatingOk = true;
            //�������� MaskedTextBox
            ValidateInputSignals();
            ValidateTokDefenses();

            ValidateAutomatics();
            //�������� DataGrid
            if (_validatingOk)
            {
                _validatingOk = _validatingOk && WriteOutputSignals()
                                          && WriteExternalDefenses()
                                          && WriteInputSignals()
                                          && WriteAutomaticsPage();

                _validatingOk &= _tokDefenseMainConstraintRadio.Checked ? WriteTokDefenses(_device.TokDefensesMain) : WriteTokDefenses(_device.TokDefensesReserve);
                _validatingOk &= _voltageDefenseMainConstraintRadio.Checked ? WriteVoltageDefenses(_device.VoltageDefensesMain) : WriteVoltageDefenses(_device.VoltageDefensesReserve);
                _validatingOk &= _frequenceDefenseMainConstraintRadio.Checked ? WriteFrequenceDefenses(_device.FrequenceDefensesMain) : WriteFrequenceDefenses(_device.FrequenceDefensesReserve);
            }

        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            _exchangeProgressBar.Value = 0;
            _processLabel.Text = "���� ������...";
            _connectingErrors = false;
            _device.LoadLogic();
            _device.LoadInputSignals();
            _device.LoadOutputSignals();
            _device.LoadExternalDefenses();
            _device.LoadAutomaticsPage();
            _device.LoadTokDefenses();
            _device.LoadVoltageDefenses();
            _device.LoadFrequenceDefenses();
            
        }

        private void OnSaveFail()
        {
            if (!_connectingErrors)
            {
                MessageBox.Show("���������� �������� ������������. ��������� �����", "������-MR74X. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            _connectingErrors = true;
        }

        private void OnLoadFail()
        {
            if (!_connectingErrors)
            {
                MessageBox.Show("���������� ��������� ������������. ��������� �����", "������-MR74X. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            _connectingErrors = true;
        }

        private void OnSaveOk()
        {
            _exchangeProgressBar.PerformStep();
        }

        void OnLoadComplete()
        {
            if (_connectingErrors)
            {
                _processLabel.Text = "������ ��������� ���������";
            }
            else
            {
                _processLabel.Text = "������ ������� ���������";
            }
        }

        void OnSaveComplete()
        {
            if (_connectingErrors)
            {
                _processLabel.Text = "������ ��������� ���������";
            }
            else
            {
                _exchangeProgressBar.PerformStep();
                _processLabel.Text = "������ ������� ���������";
            }
        }

        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            _exchangeProgressBar.Value = 0;
            _device.InputSignalsLoadOK += new Handler(_device_InputSignalsLoadOK);
            _device.InputSignalsLoadFail += new Handler(_device_InputSignalsLoadFail);
            _device.OutputSignalsLoadOK += new Handler(_device_OutputSignalsLoadOK);
            _device.OutputSignalsLoadFail += new Handler(_device_OutputSignalsLoadFail);
            _device.ExternalDefensesLoadOK += new Handler(_device_ExternalDefensesLoadOK);
            _device.ExternalDefensesLoadFail += new Handler(_device_ExternalDefensesLoadFail);
            _device.AutomaticsPageLoadOK += new Handler(_device_AutomaticsPageLoadOK);
            _device.AutomaticsPageLoadFail += new Handler(_device_AutomaticsPageLoadFail);
            _device.TokDefensesLoadOK += new Handler(_device_TokDefensesMainLoadOK);
            _device.TokDefensesLoadFail += new Handler(_device_TokDefensesMainLoadFail);
            _device.VoltageDefensesLoadOk += new Handler(_device_VoltageDefensesLoadOk);
            _device.VoltageDefensesLoadFail += new Handler(_device_VoltageDefensesLoadFail);
            _device.FrequenceDefensesLoadOk += new Handler(_device_FrequenceDefensesLoadOk);
            _device.FrequenceDefensesLoadFail += new Handler(_device_FrequenceDefensesLoadFail);
            _device.LogicLoadOK += new Handler(_device_LogicLoadOK);
            _device.LogicLoadFail += new Handler(_device_LogicLoadFail);

            SubscriptOnSaveHandlers();

            PrepareInputSignals();
            PrepareOutputSignals();
            PrepareExternalDefenses();
            PrepareAutomaticsSignals();
            PrepareTokDefenses();
            PrepareVoltageDefenses();
            PrepareFrequenceDefenses();

            _exchangeProgressBar.Value = 0;
            _processLabel.Text = "���� ������...";
            _connectingErrors = false;
            _device.LoadLogic();
            _device.LoadInputSignals();
            _device.LoadOutputSignals();
            _device.LoadExternalDefenses();
            _device.LoadAutomaticsPage();
            _device.LoadTokDefenses();
            _device.LoadVoltageDefenses();
            _device.LoadFrequenceDefenses();
            _device.LoadLogic();

        }

        private void ReadLogic()
        {

        }

        void _device_LogicLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_LogicLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(ReadLogic));
            }
            catch (InvalidOperationException)
            { }
        }

        private void SubscriptOnSaveHandlers()
        {
            _device.OutputSignalsSaveOK += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            _device.OutputSignalsSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            _device.InputSignalsSaveOK += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            _device.InputSignalsSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            _device.ExternalDefensesSaveOK += delegate(object o)
           {
               try
               {
                   Invoke(new OnDeviceEventHandler(OnSaveOk));
               }
               catch (InvalidOperationException)
               { }
           };
            _device.ExternalDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            _device.AutomaticsPageSaveOK += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            _device.AutomaticsPageSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            _device.TokDefensesSaveOK += delegate(object o)
           {
               try
               {
                   Invoke(new OnDeviceEventHandler(OnSaveOk));
               }
               catch (InvalidOperationException)
               { }
           };
            _device.TokDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            _device.VoltageDefensesSaveOk += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            _device.VoltageDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };

            _device.FrequenceDefensesSaveOk += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveComplete));
                }
                catch (InvalidOperationException)
                { }
            };
            _device.FrequenceDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveComplete));
                }
                catch (InvalidOperationException)
                { }
            };
        }

        void TypeValidation(object sender, TypeValidationEventArgs e)
        {
            if (sender is MaskedTextBox)
            {
                MaskedTextBox box = sender as MaskedTextBox;

                if (!e.IsValidInput)
                {
                    ShowToolTip(box);

                }
                else
                {
                    if (box.ValidatingType == typeof(ulong) && ulong.Parse(box.Text) > ulong.Parse(box.Tag.ToString()))
                    {
                        ShowToolTip(box);
                    }
                    if (box.ValidatingType == typeof(double) && (double.Parse(box.Text) > double.Parse(box.Tag.ToString())) || (double.Parse(box.Text) < 0))
                    {
                        ShowToolTip(box);
                    }
                }

            }
        }

        private void ShowToolTip(MaskedTextBox box)
        {
            if (_validatingOk)
            {
                if (box.Parent.Parent is TabPage)
                {
                    _tabControl.SelectedTab = box.Parent.Parent as TabPage;
                }
                _toolTip.Show("������� ����� ����� � ��������� [0-" + box.Tag + "]", box, 2000);
                box.Focus();
                box.SelectAll();
                _validatingOk = false;
            }
        }

        private void ShowToolTip(string msg, DataGridViewCell cell, TabPage page)
        {
            //cell.OwningColumn.DataGridView.CurrentCell.Selected = false;
            if (_validatingOk)
            {
                _tabControl.SelectedTab = page;
                _toolTip.Show(msg, this, this.Location, 2000);
                cell.Selected = true;
                _validatingOk = false;
            }

        }

        void Combo_DropDown(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            combo.DropDown -= new EventHandler(Combo_DropDown);
            if (combo.SelectedIndex == combo.Items.Count - 1)
            {
                combo.SelectedIndex = 0;
            }
            if (combo.Items.Contains("XXXXX"))
            {
                combo.Items.Remove("XXXXX");
            }

        }

        private void ClearCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].Items.Clear();
            }
        }

        private void SubscriptCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].DropDown += new EventHandler(Combo_DropDown);
                combos[i].SelectedIndex = 0;
            }
        }

        private void PrepareMaskedBoxes(MaskedTextBox[] boxes, Type validateType)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].TypeValidationCompleted += new TypeValidationEventHandler(TypeValidation);
                boxes[i].ValidatingType = validateType;
            }
        }

        private void ValidateMaskedBoxes(MaskedTextBox[] boxes)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].ValidateText();
            }
        }

        #region �������� �������
        void _device_OutputSignalsLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_OutputSignalsLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(ReadOutputSignals));
            }
            catch (InvalidOperationException)
            { }
        }

        private void PrepareOutputSignals()
        {
            _releSignalCol.Items.AddRange(Strings.All.ToArray());
            _outIndSignalCol.Items.AddRange(Strings.All.ToArray());
            _outputLogicCheckList.Items.AddRange(Strings.OutputSignals.ToArray());
            _outputLogicCombo.SelectedIndex = 0;
            for (int i = 0; i < MR74X.COutputRele.COUNT; i++)
            {
                _outputReleGrid.Rows.Add(new object[] { i + 1, "�����������", "���", 0 });
            }
            _outputIndicatorsGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);
            _outputReleGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);
            _outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < MR74X.COutputIndicator.COUNT; i++)
            {
                _outputIndicatorsGrid.Rows.Add(new object[]{i + 1,
                                                            "�����������",
                                                            "���",
                                                            false,
                                                            false,
                                                            false});
            }


        }

        private void ReadOutputSignals()
        {
            _exchangeProgressBar.PerformStep();
            _outputReleGrid.Rows.Clear();
            for (int i = 0; i < MR74X.COutputRele.COUNT; i++)
            {
                _outputReleGrid.Rows.Add(new object[] { i + 1, _device.OutputRele[i].Type, _device.OutputRele[i].SignalString, _device.OutputRele[i].ImpulseUlong });
            }
            _outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < MR74X.COutputIndicator.COUNT; i++)
            {
                _outputIndicatorsGrid.Rows.Add(new object[]{i + 1,
                                                            _device.OutputIndicator[i].Type,
                                                            _device.OutputIndicator[i].SignalString,
                                                            _device.OutputIndicator[i].Indication,
                                                            _device.OutputIndicator[i].Alarm,
                                                            _device.OutputIndicator[i].System});
            }
            _outputLogicCombo.SelectedIndex = 0;
            ShowOutputLogicSignals(0);


        }

        private void ShowOutputLogicSignals(int channel)
        {
            BitArray bits = _device.GetOutputLogicSignals(channel);
            for (int i = 0; i < bits.Count; i++)
            {
                _outputLogicCheckList.SetItemChecked(i, bits[i]);
            }
        }

        private bool WriteOutputSignals()
        {
            bool ret = true;
            if (_device.OutputRele.Count == MR74X.COutputRele.COUNT && _outputReleGrid.Rows.Count <= MR74X.COutputRele.COUNT)
            {
                for (int i = 0; i < _outputReleGrid.Rows.Count; i++)
                {
                    _device.OutputRele[i].Type = _outputReleGrid["_releTypeCol", i].Value.ToString();
                    _device.OutputRele[i].SignalString = _outputReleGrid["_releSignalCol", i].Value.ToString();
                    try
                    {
                        ulong value = ulong.Parse(_outputReleGrid["_releImpulseCol", i].Value.ToString());
                        if (value > MR74X.TIMELIMIT)
                        {
                            ShowToolTip(MR74X.TIMELIMIT_ERROR_MSG, _outputReleGrid["_releImpulseCol", i], _outputSignalsPage);
                            ret = false;
                        }
                        else
                        {
                            _device.OutputRele[i].ImpulseUlong = value;
                        }

                    }
                    catch (Exception)
                    {
                        ShowToolTip(MR74X.TIMELIMIT_ERROR_MSG, _outputReleGrid["_releImpulseCol", i], _outputSignalsPage);
                        ret = false;

                    }
                }
            }

            if (_device.OutputIndicator.Count == MR74X.COutputIndicator.COUNT && _outputIndicatorsGrid.Rows.Count <= MR74X.COutputIndicator.COUNT)
            {
                for (int i = 0; i < _outputIndicatorsGrid.Rows.Count; i++)
                {
                    _device.OutputIndicator[i].Type = _outputIndicatorsGrid["_outIndTypeCol", i].Value.ToString();
                    _device.OutputIndicator[i].SignalString = _outputIndicatorsGrid["_outIndSignalCol", i].Value.ToString();
                    _device.OutputIndicator[i].Indication = (bool)_outputIndicatorsGrid["_outIndResetCol", i].Value;
                    _device.OutputIndicator[i].Alarm = (bool)_outputIndicatorsGrid["_outIndAlarmCol", i].Value;
                    _device.OutputIndicator[i].System = (bool)_outputIndicatorsGrid["_outIndSystemCol", i].Value;
                    
                }

            }
            return ret;
        }

        private void _outputLogicCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowOutputLogicSignals(_outputLogicCombo.SelectedIndex);
        }

        private void _outputLogicAcceptBut_Click(object sender, EventArgs e)
        {
            BitArray bits = new BitArray(_outputLogicCheckList.Items.Count);
            for (int i = 0; i < _outputLogicCheckList.Items.Count; i++)
            {
                bits[i] = _outputLogicCheckList.GetItemChecked(i);

            }
            _device.SetOutputLogicSignals(_outputLogicCombo.SelectedIndex, bits);
        }
        #endregion

        #region ������� ������

        void _device_ExternalDefensesLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_ExternalDefensesLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(ReadExternalDefenses));
            }
            catch (InvalidOperationException)
            { }
        }

        private void PrepareExternalDefenses()
        {
            _externalDefenseGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);
            _exDefBlockingCol.Items.AddRange(Strings.ExternalDefense.ToArray());
            _exDefModeCol.Items.AddRange(Strings.ModesExternal.ToArray());
            _exDefReturnNumberCol.Items.AddRange(Strings.ExternalDefense.ToArray());
            _exDefWorkingCol.Items.AddRange(Strings.ExternalDefense.ToArray());
            for (int i = 0; i < MR74X.CExternalDefenses.COUNT; i++)
            {
                _externalDefenseGrid.Rows.Add(new object[]{i + 1,
                                                           "��������",
                                                           "���",
                                                           "���",
                                                           0,
                                                           false,
                                                           false,
                                                           "���",
                                                           0,
                                                           false,
                                                           false,
                                                           false});
                OnExternalDefenseGridModeChanged(i);
                GridManager.ChangeCellDisabling(_externalDefenseGrid, i, false, 5, new int[] { 6, 7, 8 });

            }
            _externalDefenseGrid.Height = _externalDefenseGrid.ColumnHeadersHeight + _externalDefenseGrid.Rows.Count * _externalDefenseGrid.RowTemplate.Height;
            _externalDefenseGrid.CellStateChanged += new DataGridViewCellStateChangedEventHandler(_externalDefenseGrid_CellStateChanged);
        }

        private bool OnExternalDefenseGridModeChanged(int row)
        {
            int[] columns = new int[_externalDefenseGrid.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }
            return GridManager.ChangeCellDisabling(_externalDefenseGrid, row, "��������", 1, columns);
        }

        void _externalDefenseGrid_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            //���������� ����� ��� ������� 
            if (_exDefModeCol == e.Cell.OwningColumn || _exDefReturnCol == e.Cell.OwningColumn)
            {
                if (!OnExternalDefenseGridModeChanged(e.Cell.RowIndex))
                {
                    GridManager.ChangeCellDisabling(_externalDefenseGrid, e.Cell.RowIndex, false, 5, new int[] { 6, 7, 8 });
                }
            }
        }

        private void ReadExternalDefenses()
        {
            _exchangeProgressBar.PerformStep();
            _externalDefenseGrid.Rows.Clear();

            for (int i = 0; i < MR74X.CExternalDefenses.COUNT; i++)
            {
                _externalDefenseGrid.Rows.Add(new object[]{i + 1,
                                                           _device.ExternalDefenses[i].Mode,
                                                           _device.ExternalDefenses[i].BlockingNumber,
                                                           _device.ExternalDefenses[i].WorkingNumber,
                                                           _device.ExternalDefenses[i].WorkingTime,
                                                           _device.ExternalDefenses[i].Return,
                                                           _device.ExternalDefenses[i].APV_Return,
                                                           _device.ExternalDefenses[i].ReturnNumber,
                                                           _device.ExternalDefenses[i].ReturnTime,
                                                           _device.ExternalDefenses[i].UROV,
                                                           _device.ExternalDefenses[i].APV,
                                                           _device.ExternalDefenses[i].AVR});
                if (!OnExternalDefenseGridModeChanged(i))
                {
                    GridManager.ChangeCellDisabling(_externalDefenseGrid, i, false, 5, new int[] { 6, 7, 8 });
                }
            }
        }



        private bool WriteExternalDefenses()
        {
            bool ret = true;
            if (_device.ExternalDefenses.Count == MR74X.CExternalDefenses.COUNT && _externalDefenseGrid.Rows.Count <= MR74X.CExternalDefenses.COUNT)
            {
                for (int i = 0; i < _externalDefenseGrid.Rows.Count; i++)
                {
                    _device.ExternalDefenses[i].Mode = _externalDefenseGrid["_exDefModeCol", i].Value.ToString();
                    _device.ExternalDefenses[i].BlockingNumber = _externalDefenseGrid["_exDefBlockingCol", i].Value.ToString();
                    _device.ExternalDefenses[i].WorkingNumber = _externalDefenseGrid["_exDefWorkingCol", i].Value.ToString();
                    _device.ExternalDefenses[i].Return = (bool)_externalDefenseGrid["_exDefReturnCol", i].Value;
                    _device.ExternalDefenses[i].APV_Return = (bool)_externalDefenseGrid["_exDefAPVreturnCol", i].Value;
                    _device.ExternalDefenses[i].ReturnNumber = _externalDefenseGrid["_exDefReturnNumberCol", i].Value.ToString();
                    _device.ExternalDefenses[i].UROV = (bool)_externalDefenseGrid["_exDefUROVcol", i].Value;
                    _device.ExternalDefenses[i].APV = (bool)_externalDefenseGrid["_exDefAPVcol", i].Value;
                    _device.ExternalDefenses[i].AVR = (bool)_externalDefenseGrid["_exDefAVRcol", i].Value;
                    try
                    {
                        ulong value = ulong.Parse(_externalDefenseGrid["_exDefReturnTimeCol", i].Value.ToString());
                        if (value > MR74X.TIMELIMIT)
                        {
                            throw new OverflowException(MR74X.TIMELIMIT_ERROR_MSG);
                        }
                        else
                        {
                            _device.ExternalDefenses[i].ReturnTime = value;
                        }

                    }
                    catch (Exception)
                    {
                        ShowToolTip(MR74X.TIMELIMIT_ERROR_MSG, _externalDefenseGrid["_exDefReturnTimeCol", i], _externalDefensePage);
                        ret = false;
                    }
                    try
                    {
                        ulong value = ulong.Parse(_externalDefenseGrid["_exDefWorkingTimeCol", i].Value.ToString());
                        if (value > MR74X.TIMELIMIT)
                        {
                            throw new OverflowException(MR74X.TIMELIMIT_ERROR_MSG);
                        }
                        else
                        {
                            _device.ExternalDefenses[i].WorkingTime = ulong.Parse(_externalDefenseGrid["_exDefWorkingTimeCol", i].Value.ToString());
                        }
                        _device.ExternalDefenses[i].WorkingTime = ulong.Parse(_externalDefenseGrid["_exDefWorkingTimeCol", i].Value.ToString());
                    }
                    catch (Exception)
                    {
                        ShowToolTip(MR74X.TIMELIMIT_ERROR_MSG, _externalDefenseGrid["_exDefWorkingTimeCol", i], _externalDefensePage);
                        ret = false;
                    }

                }

            }
            return ret;
        }

        #endregion

        #region ������� �������

        private void ReadInputSignals()
        {
            ClearCombos(_inputSignalsCombos);
            FillInputSignalsCombos();
            SubscriptCombos(_inputSignalsCombos);
            _exchangeProgressBar.PerformStep();
            _TT_Box.Text = _device.TT.ToString();
            _TTNP_Box.Text = _device.TTNP.ToString();
            _TN_Box.Text = _device.TN.ToString();
            _TNNP_Box.Text = _device.TNNP.ToString();
            double res;
            if (_device.MaxTok > 16) { res = _device.MaxTok - 0.01; } else { res = _device.MaxTok; }
            _maxTok_Box.Text = res.ToString();
            //_maxTok_Box.Text = _device.MaxTok.ToString();
            _switcherDurationBox.Text = _device.SwitcherDuration.ToString();
            _switcherImpulseBox.Text = _device.SwitcherImpulse.ToString();
            _switcherTimeBox.Text = _device.SwitcherTimeUROV.ToString();
            if (_device.SwitcherTokUROV > 24) { res = _device.SwitcherTokUROV - 0.01; } else { res = _device.SwitcherTokUROV; }
            _switcherTokBox.Text = res.ToString();
            //_switcherTokBox.Text = _device.SwitcherTokUROV.ToString();

            _extOffCombo.SelectedItem = _device.ExternalOff;
            _extOnCombo.SelectedItem = _device.ExternalOn;
            _constraintGroupCombo.SelectedItem = _device.ConstraintGroup;
            //_oscilloscopeCombo.SelectedItem = _device.OscilloscopeStart;
            _keyOffCombo.SelectedItem = _device.KeyOff;
            _keyOnCombo.SelectedItem = _device.KeyOn;
            _signalizationCombo.SelectedItem = _device.SignalizationReset;
            _switcherBlockCombo.SelectedItem = _device.SwitcherBlock;
            _switcherErrorCombo.SelectedItem = _device.SwitcherError;
            _switcherStateOffCombo.SelectedItem = _device.SwitcherOff;
            _switcherStateOnCombo.SelectedItem = _device.SwitcherOn;

            _manageSignalsSDTU_Combo.SelectedItem = _device.ManageSignalSDTU;
            _manageSignalsKeyCombo.SelectedItem = _device.ManageSignalKey;
            _manageSignalsExternalCombo.SelectedItem = _device.ManageSignalExternal;
            _manageSignalsButtonCombo.SelectedItem = _device.ManageSignalButton;

            _TNNP_dispepairCombo.SelectedItem = _device.TNNP_Dispepair;
            _TN_dispepairCombo.SelectedItem = _device.TN_Dispepair;
            _TN_typeCombo.SelectedItem = _device.TN_Type;
            _OMP_TypeBox.SelectedItem = _device.OMP_Type;
            _HUD_Box.Text = (_device.HUD / 1000).ToString();
            _releDispepairBox.Text = _device.DispepairImpulse.ToString();

            for (int i = 0; i < _device.DispepairSignal.Count; i++)
            {
                _dispepairCheckList.SetItemChecked(i, _device.DispepairSignal[i]);
            }

            ShowInputLogicSignals(_logicChannelsCombo.SelectedIndex);
        }

        void _device_InputSignalsLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_InputSignalsLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(ReadInputSignals));
            }
            catch (InvalidOperationException)
            { }
        }

        private ComboBox[] _inputSignalsCombos;
        private ComboBox[] _logicCombos;
        private MaskedTextBox[] _inputSignalsDoubleMaskBoxes;
        private MaskedTextBox[] _inputSignalsUlongMaskBoxes;

        private void PrepareInputSignals()
        {
            _inputSignalsCombos = new ComboBox[] { _OMP_TypeBox, _extOffCombo,_extOnCombo,_constraintGroupCombo,
                                                  _keyOffCombo,_keyOnCombo,/*_oscilloscopeCombo,*/_signalizationCombo,
                                                  _switcherBlockCombo,_switcherErrorCombo,_switcherStateOffCombo,
                                                  _switcherStateOnCombo,_TN_dispepairCombo,_TN_typeCombo,_TNNP_dispepairCombo,
                                                  _manageSignalsButtonCombo,_manageSignalsExternalCombo,_manageSignalsSDTU_Combo,_manageSignalsKeyCombo, };
            _inputSignalsDoubleMaskBoxes = new MaskedTextBox[] { _HUD_Box, _maxTok_Box, _TN_Box, _TNNP_Box, _switcherTokBox };
            _inputSignalsUlongMaskBoxes = new MaskedTextBox[] {  _TTNP_Box, _TT_Box, _switcherTimeBox, _switcherImpulseBox, _switcherDurationBox, _releDispepairBox };


            FillInputSignalsCombos();

            _logicChannelsCombo.SelectedIndex = 0;
            SubscriptCombos(_inputSignalsCombos);

            PrepareMaskedBoxes(_inputSignalsDoubleMaskBoxes, typeof(double));
            PrepareMaskedBoxes(_inputSignalsUlongMaskBoxes, typeof(ulong));
        }

        private void FillInputSignalsCombos()
        {
            _extOffCombo.Items.AddRange(Strings.Logic.ToArray());
            _extOnCombo.Items.AddRange(Strings.Logic.ToArray());
            _constraintGroupCombo.Items.AddRange(Strings.Logic.ToArray());
            _keyOffCombo.Items.AddRange(Strings.Logic.ToArray());
            _keyOnCombo.Items.AddRange(Strings.Logic.ToArray());
            //_oscilloscopeCombo.Items.AddRange(Strings.Logic.ToArray());
            _signalizationCombo.Items.AddRange(Strings.Logic.ToArray());
            _switcherBlockCombo.Items.AddRange(Strings.Logic.ToArray());
            _switcherErrorCombo.Items.AddRange(Strings.Logic.ToArray());
            _switcherStateOffCombo.Items.AddRange(Strings.Logic.ToArray());
            _switcherStateOnCombo.Items.AddRange(Strings.Logic.ToArray());
            _TNNP_dispepairCombo.Items.AddRange(Strings.Logic.ToArray());
            _TN_dispepairCombo.Items.AddRange(Strings.Logic.ToArray());
            _TN_typeCombo.Items.AddRange(Strings.TN_Type.ToArray());
            _OMP_TypeBox.Items.AddRange(Strings.ModesLight.ToArray());
            _manageSignalsButtonCombo.Items.AddRange(Strings.Forbidden.ToArray());
            _manageSignalsKeyCombo.Items.AddRange(Strings.Control.ToArray());
            _manageSignalsExternalCombo.Items.AddRange(Strings.Control.ToArray());
            _manageSignalsSDTU_Combo.Items.AddRange(Strings.Forbidden.ToArray());
        }

        private void _logicChannelsCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowInputLogicSignals(_logicChannelsCombo.SelectedIndex);
        }

        private void ShowInputLogicSignals(int channel)
        {
            _logicSignalsDataGrid.Rows.Clear();
            LogicState[] logicSignals = _device.GetInputLogicSignals(channel);
            for (int i = 0; i < 16; i++)
            {
                _logicSignalsDataGrid.Rows.Add(new object[] { i + 1, logicSignals[i].ToString() });
            }

        }

        private void ValidateInputSignals()
        {
            ValidateMaskedBoxes(_inputSignalsUlongMaskBoxes);
            ValidateMaskedBoxes(_inputSignalsDoubleMaskBoxes);
        }

        private bool WriteInputSignals()
        {
            _device.TT = UInt16.Parse(_TT_Box.Text);
            _device.TTNP = UInt16.Parse(_TTNP_Box.Text);
            _device.TNNP = Double.Parse(_TNNP_Box.Text);
            _device.TN = Double.Parse(_TN_Box.Text);
            if (Double.Parse(_maxTok_Box.Text) > 16)
            { _device.MaxTok = Double.Parse(_maxTok_Box.Text) + 0.01; }
            else
            { _device.MaxTok = Double.Parse(_maxTok_Box.Text); }
            //_device.MaxTok = Double.Parse(_maxTok_Box.Text);
            _device.SwitcherDuration = UInt64.Parse(_switcherDurationBox.Text);
            _device.SwitcherImpulse = UInt64.Parse(_switcherImpulseBox.Text);
            _device.SwitcherTimeUROV = UInt64.Parse(_switcherTimeBox.Text);
            if (Double.Parse(_switcherTokBox.Text) > 24)
            { _device.SwitcherTokUROV = Double.Parse(_switcherTokBox.Text) + 0.01; }
            else { _device.SwitcherTokUROV = Double.Parse(_switcherTokBox.Text); }
            //_device.SwitcherTokUROV = Double.Parse(_switcherTokBox.Text);

            _device.ExternalOff = _extOffCombo.SelectedItem.ToString();
            _device.ExternalOn = _extOnCombo.SelectedItem.ToString();
            _device.ConstraintGroup = _constraintGroupCombo.SelectedItem.ToString();
            _device.KeyOff = _keyOffCombo.SelectedItem.ToString();
            _device.KeyOn = _keyOnCombo.SelectedItem.ToString();
            //_device.OscilloscopeStart = _oscilloscopeCombo.SelectedItem.ToString();
            _device.SignalizationReset = _signalizationCombo.SelectedItem.ToString();
            _device.SwitcherBlock = _switcherBlockCombo.SelectedItem.ToString();
            _device.SwitcherError = _switcherErrorCombo.SelectedItem.ToString();
            _device.SwitcherOff = _switcherStateOffCombo.SelectedItem.ToString();
            _device.SwitcherOn = _switcherStateOnCombo.SelectedItem.ToString();

            CheckedListBoxManager checkManager = new CheckedListBoxManager();
            checkManager.CheckList = _dispepairCheckList;
            _device.DispepairSignal = checkManager.ToBitArray();
            _device.DispepairImpulse = ulong.Parse(_releDispepairBox.Text);

            _device.TNNP_Dispepair = _TNNP_dispepairCombo.SelectedItem.ToString();
            _device.TN_Dispepair = _TN_dispepairCombo.SelectedItem.ToString();
            _device.TN_Type = _TN_typeCombo.SelectedItem.ToString();
            _device.OMP_Type = _OMP_TypeBox.SelectedItem.ToString();
            _device.HUD = Double.Parse(_HUD_Box.Text);
            _device.ManageSignalButton = _manageSignalsButtonCombo.SelectedItem.ToString();
            _device.ManageSignalExternal = _manageSignalsExternalCombo.SelectedItem.ToString();
            _device.ManageSignalKey = _manageSignalsKeyCombo.SelectedItem.ToString();
            _device.ManageSignalSDTU = _manageSignalsSDTU_Combo.SelectedItem.ToString();
            return true;

        }

        private void _applyLogicSignalsBut_Click(object sender, EventArgs e)
        {
            LogicState[] logicSignals = new LogicState[16];
            for (int i = 0; i < _logicSignalsDataGrid.Rows.Count; i++)
            {
                object o = Enum.Parse(typeof(LogicState), _logicSignalsDataGrid["_diskretValueCol", i].Value.ToString());
                logicSignals[i] = (LogicState)(o);
            }
            _device.SetInputLogicSignals(_logicChannelsCombo.SelectedIndex, logicSignals);
        }
        #endregion

        #region �������� ����������
        void _device_AutomaticsPageLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_AutomaticsPageLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnAutomaticsPageLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }

        private ComboBox[] _automaticCombos;
        private MaskedTextBox[] _automaticsMaskedBoxes;

        private void ValidateAutomatics()
        {
            ValidateMaskedBoxes(_automaticsMaskedBoxes);
            ValidateMaskedBoxes(new MaskedTextBox[] { lzsh_constraint });
        }

        private void PrepareAutomaticsSignals()
        {
            _automaticCombos = new ComboBox[] {apv_self_off,apv_conf,apv_blocking,
                                               avr_abrasion,avr_blocking,avr_reset_blocking,
                                               avr_return,avr_start,lzsh_conf};
            _automaticsMaskedBoxes = new MaskedTextBox[]{apv_time_1krat,apv_time_2krat,apv_time_3krat,apv_time_4krat,
                                                         apv_time_blocking,apv_time_ready,avr_disconnection,
                                                         avr_time_abrasion,avr_time_return,
                                                         };
            PrepareMaskedBoxes(_automaticsMaskedBoxes, typeof(ulong));
            PrepareMaskedBoxes(new MaskedTextBox[] { lzsh_constraint }, typeof(double));
            FillAutomaticCombo();
            SubscriptCombos(_automaticCombos);



        }

        private void FillAutomaticCombo()
        {
            apv_blocking.Items.AddRange(Strings.Logic.ToArray());
            avr_start.Items.AddRange(Strings.Logic.ToArray());
            avr_blocking.Items.AddRange(Strings.Logic.ToArray());
            avr_reset_blocking.Items.AddRange(Strings.Logic.ToArray());
            avr_abrasion.Items.AddRange(Strings.Logic.ToArray());
            avr_return.Items.AddRange(Strings.Logic.ToArray());
            apv_conf.Items.AddRange(Strings.Crat.ToArray());
            lzsh_conf.Items.AddRange(Strings.ModesLight.ToArray());
            apv_self_off.Items.AddRange(Strings.YesNo.ToArray());


        }

        private void OnAutomaticsPageLoadOk()
        {
            _exchangeProgressBar.PerformStep();
            ClearCombos(_automaticCombos);
            FillAutomaticCombo();
            SubscriptCombos(_automaticCombos);
            apv_conf.Text = _device.APV_Cnf;
            apv_blocking.Text = _device.APV_Blocking;
            apv_self_off.Text = _device.APV_Start;
            avr_start.Text = _device.AVR_Start;
            avr_blocking.Text = _device.AVR_Blocking;
            avr_reset_blocking.Text = _device.AVR_Reset;
            avr_abrasion.Text = _device.AVR_Abrasion;
            avr_return.Text = _device.AVR_Return;
            lzsh_conf.Text = _device.LZSH_Cnf;

            apv_time_blocking.Text = _device.APV_Time_Blocking.ToString();
            apv_time_ready.Text = _device.APV_Time_Ready.ToString();
            apv_time_1krat.Text = _device.APV_Time_1Krat.ToString();
            apv_time_2krat.Text = _device.APV_Time_2Krat.ToString();
            apv_time_3krat.Text = _device.APV_Time_3Krat.ToString();
            apv_time_4krat.Text = _device.APV_Time_4Krat.ToString();

            avr_supply_off.Checked = _device.AVR_Supply_Off;
            avr_self_off.Checked = _device.AVR_Self_Off;
            avr_switch_off.Checked = _device.AVR_Switch_Off;
            avr_abrasion_switch.Checked = _device.AVR_Abrasion_Switch;
            avr_permit_reset_switch.Checked = _device.AVR_Reset_Switch;
            avr_time_abrasion.Text = _device.AVR_Time_Abrasion.ToString();
            avr_time_return.Text = _device.AVR_Time_Return.ToString();
            avr_disconnection.Text = _device.AVR_Time_Off.ToString();
            double res;
            if (_device.LZSH_Constraint > 24)
            { res = _device.LZSH_Constraint - 0.01; }
            else { res = _device.LZSH_Constraint; }
            lzsh_constraint.Text = res.ToString();
            //lzsh_constraint.Text = _device.LZSH_Constraint.ToString();

        }

        private bool WriteAutomaticsPage()
        {
            _device.APV_Cnf = apv_conf.Text;
            _device.APV_Blocking = apv_blocking.Text;
            _device.APV_Time_Blocking = UInt64.Parse(apv_time_blocking.Text);
            _device.APV_Time_Ready = UInt64.Parse(apv_time_ready.Text);
            _device.APV_Time_1Krat = UInt64.Parse(apv_time_1krat.Text);
            _device.APV_Time_2Krat = UInt64.Parse(apv_time_2krat.Text);
            _device.APV_Time_3Krat = UInt64.Parse(apv_time_3krat.Text);
            _device.APV_Time_4Krat = UInt64.Parse(apv_time_4krat.Text);
            _device.APV_Start = apv_self_off.Text;

            _device.AVR_Supply_Off = avr_supply_off.Checked;
            _device.AVR_Self_Off = avr_self_off.Checked;
            _device.AVR_Switch_Off = avr_switch_off.Checked;
            _device.AVR_Abrasion_Switch = avr_abrasion_switch.Checked;
            _device.AVR_Reset_Switch = avr_permit_reset_switch.Checked;
            _device.AVR_Start = avr_start.Text;
            _device.AVR_Blocking = avr_blocking.Text;
            _device.AVR_Reset = avr_reset_blocking.Text;
            _device.AVR_Abrasion = avr_abrasion.Text;
            _device.AVR_Time_Abrasion = UInt64.Parse(avr_time_abrasion.Text);
            _device.AVR_Return = avr_return.Text;
            _device.AVR_Time_Return = UInt64.Parse(avr_time_return.Text);
            _device.AVR_Time_Off = UInt64.Parse(avr_disconnection.Text);

            _device.LZSH_Cnf = lzsh_conf.Text;
            if (Double.Parse(lzsh_constraint.Text) > 24)
            { _device.LZSH_Constraint = double.Parse(lzsh_constraint.Text) + 0.01; }
            else { _device.LZSH_Constraint = double.Parse(lzsh_constraint.Text); }
            //_device.LZSH_Constraint = double.Parse(lzsh_constraint.Text);
            return true;
        }
        #endregion

        #region ������� ������

        void ChangeTokDefenseCellDisabling1(DataGridView grid, int row)
        {
            if (!OnTokDefenseGridModeChanged(grid, row))
            {
                GridManager.ChangeCellDisabling(grid, row, false, 3, 4);
                GridManager.ChangeCellDisabling(grid, row, "���", 5, 6);
                GridManager.ChangeCellDisabling(grid, row, false, 11, 12);
            }
        }
        void ChangeTokDefenseCellDisabling2(DataGridView grid, int row)
        {
            if (!OnTokDefenseGridModeChanged(grid, row))
            {
                GridManager.ChangeCellDisabling(grid, row, false, 3, 4);
                GridManager.ChangeCellDisabling(grid, row, "���", 5, 6);
            }
        }
        void ChangeTokDefenseCellDisabling3(DataGridView grid, int row)
        {
            if (!OnTokDefenseGridModeChanged(grid, row))
            {
                if (0 == row)
                {
                    GridManager.ChangeCellDisabling(grid, row, false, 3, 4);
                    GridManager.ChangeCellDisabling(grid, row, false, 7, 8);
                }

            }
        }

        void ShowTokDefenses(MR74X.CTokDefenses tokDefenses)
        {
            _tokDefenseGrid1.Rows.Clear();
            _tokDefenseGrid3.Rows.Clear();
            //_tokDefenseGrid2.Rows.Clear();
            _tokDefenseGrid4.Rows.Clear();

            _tokDefenseIbox.Text = tokDefenses.I.ToString();
            _tokDefenseInbox.Text = tokDefenses.In.ToString();
            _tokDefenseI0box.Text = tokDefenses.I0.ToString();
            _tokDefenseI2box.Text = tokDefenses.I2.ToString();

            for (int i = 0; i < 4; i++)
            {
                if (tokDefenses[i].WorkConstraint>24)
                {
                    _tokDefenseGrid1.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       (tokDefenses[i].WorkConstraint - 0.01),
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                }else
                { 
                    _tokDefenseGrid1.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                }

                ChangeTokDefenseCellDisabling1(_tokDefenseGrid1, i);
            }
            //for (int i = 2; i < 4; i++)
            //{

            //    _tokDefenseGrid2.Rows.Add(new object[]{tokDefenses[i].Name,
            //                                           tokDefenses[i].Mode,     
            //                                           tokDefenses[i].BlockingNumber,
            //                                           tokDefenses[i].PuskU,
            //                                           tokDefenses[i].PuskU_Constraint,
            //                                           tokDefenses[i].Direction,
            //                                           tokDefenses[i].BlockingExist,
            //                                           tokDefenses[i].Parameter,
            //                                           tokDefenses[i].WorkConstraint,
            //                                           tokDefenses[i].Feature,
            //                                           tokDefenses[i].WorkTime,
            //                                           tokDefenses[i].Engine,
            //                                           tokDefenses[i].UROV,
            //                                           tokDefenses[i].APV,
            //                                           tokDefenses[i].AVR});
            //    ChangeTokDefenseCellDisabling2(_tokDefenseGrid2, i - 2);
            //}
            for (int i = 4; i < 10; i++)
            {

                _tokDefenseGrid3.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                ChangeTokDefenseCellDisabling1(_tokDefenseGrid3, i - 4);

            }

            const int Ig_index = 10;
            _tokDefenseGrid4.Rows.Add(new object[]{ tokDefenses[Ig_index].Name,
                                                       tokDefenses[Ig_index].Mode,     
                                                       tokDefenses[Ig_index].BlockingNumber,
                                                       tokDefenses[Ig_index].PuskU,
                                                       tokDefenses[Ig_index].PuskU_Constraint,
                                                       tokDefenses[Ig_index].WorkConstraint,
                                                       tokDefenses[Ig_index].WorkTime,
                                                       tokDefenses[Ig_index].Speedup,
                                                       tokDefenses[Ig_index].SpeedupTime,
                                                       tokDefenses[Ig_index].UROV,
                                                       tokDefenses[Ig_index].APV,
                                                       tokDefenses[Ig_index].AVR                                                    

            });
            ChangeTokDefenseCellDisabling3(_tokDefenseGrid4, 0);
            const int I12_index = 11;
            _tokDefenseGrid4.Rows.Add(new object[]{ tokDefenses[I12_index].Name,
                                                       tokDefenses[I12_index].Mode,     
                                                       tokDefenses[I12_index].BlockingNumber,
                                                       false,
                                                       0.0,
                                                       tokDefenses[I12_index].WorkConstraint,
                                                       tokDefenses[I12_index].WorkTime,
                                                       false,
                                                       0.0,
                                                       tokDefenses[I12_index].UROV,
                                                       tokDefenses[I12_index].APV,
                                                       tokDefenses[I12_index].AVR  });
            ChangeTokDefenseCellDisabling3(_tokDefenseGrid4, 1);

            _tokDefenseGrid4["_tokDefense4UpuskCol", 1].ReadOnly = true;
            _tokDefenseGrid4["_tokDefense4UpuskCol", 1].Style.BackColor = SystemColors.Control;
            _tokDefenseGrid4["_tokDefense4PuskConstraintCol", 1].ReadOnly = true;
            _tokDefenseGrid4["_tokDefense4PuskConstraintCol", 1].Style.BackColor = SystemColors.Control;
            _tokDefenseGrid4["_tokDefense4SpeedupCol", 1].ReadOnly = true;
            _tokDefenseGrid4["_tokDefense4SpeedupCol", 1].Style.BackColor = SystemColors.Control;
            _tokDefenseGrid4["_tokDefense4SpeedupTimeCol", 1].ReadOnly = true;
            _tokDefenseGrid4["_tokDefense4SpeedupTimeCol", 1].Style.BackColor = SystemColors.Control;

            for (int i = 0; i < _tokDefenseGrid1.Rows.Count; i++)
            {
                _tokDefenseGrid1[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            //for (int i = 0; i < _tokDefenseGrid2.Rows.Count; i++)
            //{
            //    _tokDefenseGrid2[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //}
            for (int i = 0; i < _tokDefenseGrid3.Rows.Count; i++)
            {
                _tokDefenseGrid3[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            for (int i = 0; i < _tokDefenseGrid4.Rows.Count; i++)
            {
                _tokDefenseGrid4[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
        }

        void PrepareTokDefenses()
        {
            _tokDefenseModeCol.Items.AddRange(Strings.Modes.ToArray());
            _tokDefenseParameterCol.Items.AddRange(Strings.TokParameter.ToArray());
            _tokDefenseBlockExistCol.Items.AddRange(Strings.Blocking.ToArray());
            _tokDefenseDirectionCol.Items.AddRange(Strings.BusDirection.ToArray());
            _tokDefenseFeatureCol.Items.AddRange(Strings.Feature.ToArray());
            _tokDefenseBlockNumberCol.Items.AddRange(Strings.Logic.ToArray());

            _tokDefense2ModeCol.Items.AddRange(Strings.Modes.ToArray());
            _tokDefense2ParameterCol.Items.AddRange(Strings.TokParameter.ToArray());
            _tokDefense2BlockExistCol.Items.AddRange(Strings.Blocking.ToArray());
            _tokDefense2DirectionCol.Items.AddRange(Strings.BusDirection.ToArray());
            _tokDefense2FeatureCol.Items.AddRange(Strings.Feature.ToArray());
            _tokDefense2BlockNumberCol.Items.AddRange(Strings.Logic.ToArray());
            _tokDefense2EngineCol.Items.AddRange(Strings.EngineMode.ToArray());

            _tokDefense3ModeCol.Items.AddRange(Strings.Modes.ToArray());
            _tokDefense3ParameterCol.Items.AddRange(Strings.EngineParameter.ToArray());
            _tokDefense3BlockingExistCol.Items.AddRange(Strings.Blocking.ToArray());
            _tokDefense3DirectionCol.Items.AddRange(Strings.BusDirection.ToArray());
            _tokDefense3FeatureCol.Items.AddRange(Strings.FeatureLight.ToArray());
            _tokDefense3BlockingNumberCol.Items.AddRange(Strings.Logic.ToArray());

            _tokDefense4ModeCol.Items.AddRange(Strings.Modes.ToArray());
            _tokDefense4BlockNumberCol.Items.AddRange(Strings.Logic.ToArray());

            _tokDefenseI0box.ValidatingType = _tokDefenseI2box.ValidatingType =
            _tokDefenseInbox.ValidatingType = _tokDefenseIbox.ValidatingType = typeof(ulong);

            _tokDefenseIbox.TypeValidationCompleted += new TypeValidationEventHandler(TypeValidation);
            _tokDefenseI2box.TypeValidationCompleted += new TypeValidationEventHandler(TypeValidation);
            _tokDefenseI0box.TypeValidationCompleted += new TypeValidationEventHandler(TypeValidation);
            _tokDefenseInbox.TypeValidationCompleted += new TypeValidationEventHandler(TypeValidation);


            ShowTokDefenses(_device.TokDefensesMain);

            _tokDefenseGrid1.CellStateChanged += new DataGridViewCellStateChangedEventHandler(_tokDefenseGrid_CellStateChanged);
            //_tokDefenseGrid2.CellStateChanged += new DataGridViewCellStateChangedEventHandler(_tokDefenseGrid_CellStateChanged);
            _tokDefenseGrid3.CellStateChanged += new DataGridViewCellStateChangedEventHandler(_tokDefenseGrid_CellStateChanged);
            _tokDefenseGrid4.CellStateChanged += new DataGridViewCellStateChangedEventHandler(_tokDefenseGrid_CellStateChanged);

            _tokDefenseGrid1.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);
            //_tokDefenseGrid2.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);
            _tokDefenseGrid3.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);
            _tokDefenseGrid4.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);


        }

        void _tokDefenseGrid_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (_tokDefenseModeCol == e.Cell.OwningColumn ||
                _tokDefenseU_PuskCol == e.Cell.OwningColumn ||
                _tokDefenseSpeedUpCol == e.Cell.OwningColumn ||
                _tokDefenseDirectionCol == e.Cell.OwningColumn ||

                _tokDefense3ModeCol == e.Cell.OwningColumn ||
                _tokDefense3UpuskCol == e.Cell.OwningColumn ||
                _tokDefense3SpeedupCol == e.Cell.OwningColumn ||
                _tokDefense3DirectionCol == e.Cell.OwningColumn
                )
            {
                ChangeTokDefenseCellDisabling1(sender as DataGridView, e.Cell.RowIndex);
            }
            if (_tokDefense2ModeCol == e.Cell.OwningColumn ||
                _tokDefense2PuskUCol == e.Cell.OwningColumn ||
                _tokDefense2DirectionCol == e.Cell.OwningColumn
                )
            {
                ChangeTokDefenseCellDisabling2(sender as DataGridView, e.Cell.RowIndex);
            }
            if (_tokDefense4ModeCol == e.Cell.OwningColumn ||
                (_tokDefense4SpeedupCol == e.Cell.OwningColumn && 1 != e.Cell.RowIndex) ||
                _tokDefense4UpuskCol == e.Cell.OwningColumn && 1 != e.Cell.RowIndex)
            {
                ChangeTokDefenseCellDisabling3(sender as DataGridView, e.Cell.RowIndex);
            }

        }

        void _device_TokDefensesMainLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnTokDefenseLoadOk()
        {
            _exchangeProgressBar.PerformStep();

            if (_tokDefenseMainConstraintRadio.Checked)
            {
                ShowTokDefenses(_device.TokDefensesMain);
            }
            else
            {
                ShowTokDefenses(_device.TokDefensesReserve);
            }
        }
        void _device_TokDefensesMainLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnTokDefenseLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }

        //private void _tokDefenseGrid3_Scroll(object sender, ScrollEventArgs e)
        //{
        //    if (ScrollOrientation.HorizontalScroll == e.ScrollOrientation)
        //    {
        //        //_tokDefenseGrid2.HorizontalScrollingOffset = e.NewValue;
        //        _tokDefenseGrid1.HorizontalScrollingOffset = (int)(e.NewValue * 1.1);
        //    }
        //}

        private bool OnTokDefenseGridModeChanged(DataGridView grid, int row)
        {
            int[] columns = new int[grid.Columns.Count - 2];
            if (grid == _tokDefenseGrid4 && 1 == row)
            {
                columns = new int[6] { 2, 5, 6, 9, 10, 11 };
            }
            else
            {
                for (int i = 0; i < columns.Length; i++)
                {
                    columns[i] = i + 2;
                }
            }

            return GridManager.ChangeCellDisabling(grid, row, "��������", 1, columns);
        }

        private bool WriteTokDefenseItem(MR74X.CTokDefenses tokDefenses, DataGridView grid, int rowIndex, int itemIndex)
        {
            bool ret = true;
            tokDefenses[itemIndex].Mode = grid[1, rowIndex].Value.ToString();
            bool enabled = tokDefenses[itemIndex].Mode != "��������";
            if (enabled)
            {
                tokDefenses[itemIndex].BlockingNumber = grid[2, rowIndex].Value.ToString();
                tokDefenses[itemIndex].PuskU = (bool)grid[3, rowIndex].Value;
                try
                {
                    double value = double.Parse(grid[4, rowIndex].Value.ToString());
                    if (value < 0 || value > 256)
                    {
                        ShowToolTip("������� ����� � ��������� [0 - 256.0]", grid[4, rowIndex], _tokDefensesPage);
                        ret = false;
                    }
                    else
                    {
                        tokDefenses[itemIndex].PuskU_Constraint = value;
                    }

                }
                catch (Exception)
                {
                    ShowToolTip("������� ����� � ��������� [0 - 256.0]", grid[4, rowIndex], _tokDefensesPage);
                    ret = false;
                }

                if (grid != _tokDefenseGrid4)
                {
                    tokDefenses[itemIndex].Direction = grid[5, rowIndex].Value.ToString();
                    tokDefenses[itemIndex].BlockingExist = grid[6, rowIndex].Value.ToString();
                    tokDefenses[itemIndex].Parameter = grid[7, rowIndex].Value.ToString();
                }

                int workConstraintIndex = (grid == _tokDefenseGrid4) ? 5 : 8;
                int workTimeIndex = (grid == _tokDefenseGrid4) ? 6 : 10;
                int speedupIndex = (grid == _tokDefenseGrid4) ? 7 : 11;
                int speedupTimeIndex = (grid == _tokDefenseGrid4) ? 8 : 12;
                int UROVIndex = (grid == _tokDefenseGrid4) ? 9 : 13;
                int APVIndex = (grid == _tokDefenseGrid4) ? 10 : 14;
                int AVRIndex = (grid == _tokDefenseGrid4) ? 11 : 15;

                double limit = 40;
                if (grid == _tokDefenseGrid1)
                {
                    limit = 40;
                }
                if (grid == _tokDefenseGrid4)
                {
                    if (0 == rowIndex)
                    {
                        limit = 5;
                    }
                    else
                    {
                        limit = 100;
                    }
                }
                if (grid == _tokDefenseGrid3)
                {
                    if (4 == rowIndex || 5 == rowIndex)
                    {
                        limit = 5;
                    }
                }

                try
                {
                    double value;
                    if (double.Parse(grid[workConstraintIndex, rowIndex].Value.ToString()) > 24)
                    {value = double.Parse(grid[workConstraintIndex, rowIndex].Value.ToString()) - 0.01;
                    }else{ value = double.Parse(grid[workConstraintIndex, rowIndex].Value.ToString());}
                    if (value < 0 || value > limit)
                    {
                        ShowToolTip("������� �����  � ��������� [0 - " + limit + ".0]", grid[workConstraintIndex, rowIndex], _tokDefensesPage);
                        ret = false;
                    }
                    else
                    {
                        if (value > 24)
                        {tokDefenses[itemIndex].WorkConstraint = (value + 0.01);
                        }else
                        {tokDefenses[itemIndex].WorkConstraint = value;}
                    }
                }
                catch (Exception)
                {
                    ShowToolTip("������� �����  � ��������� [0 - " + limit + ".0]", grid[workConstraintIndex, rowIndex], _tokDefensesPage);
                    ret = false;
                }
                tokDefenses[itemIndex].Feature = grid[9, rowIndex].Value.ToString();
                try
                {
                    ulong value = ulong.Parse(grid[workTimeIndex, rowIndex].Value.ToString());
                    if (value > MR74X.TIMELIMIT)
                    {
                        ShowToolTip(MR74X.TIMELIMIT_ERROR_MSG, grid[workTimeIndex, rowIndex], _tokDefensesPage);
                        ret = false;
                    }
                    else
                    {
                        tokDefenses[itemIndex].WorkTime = value;
                    }

                }
                catch (Exception)
                {
                    ShowToolTip(MR74X.TIMELIMIT_ERROR_MSG, grid[workTimeIndex, rowIndex], _tokDefensesPage);
                    ret = false;
                }
                //if (grid == _tokDefenseGrid2)
                //{
                //    tokDefenses[itemIndex].Engine = grid[11, rowIndex].Value.ToString();
                //    tokDefenses[itemIndex].UROV = (bool)grid[12, rowIndex].Value;
                //    tokDefenses[itemIndex].APV = (bool)grid[13, rowIndex].Value;
                //    tokDefenses[itemIndex].AVR = (bool)grid[14, rowIndex].Value;
                //}
                //else
                //{
                    tokDefenses[itemIndex].Speedup = (bool)grid[speedupIndex, rowIndex].Value;
                    try
                    {
                        ulong value = ulong.Parse(grid[speedupTimeIndex, rowIndex].Value.ToString());
                        if (value > MR74X.TIMELIMIT)
                        {
                            ShowToolTip(MR74X.TIMELIMIT_ERROR_MSG, grid[speedupTimeIndex, rowIndex], _tokDefensesPage);
                            ret = false;
                        }
                        else
                        {
                            tokDefenses[itemIndex].SpeedupTime = value;
                        }

                    }
                    catch (Exception)
                    {
                        ShowToolTip(MR74X.TIMELIMIT_ERROR_MSG, grid[speedupTimeIndex, rowIndex], _tokDefensesPage);
                        ret = false;
                    }
                    tokDefenses[itemIndex].UROV = (bool)grid[UROVIndex, rowIndex].Value;
                    tokDefenses[itemIndex].APV = (bool)grid[APVIndex, rowIndex].Value;
                    tokDefenses[itemIndex].AVR = (bool)grid[AVRIndex, rowIndex].Value;
                //}
            }
            return ret;
        }

        private bool WriteTokDefenses(MR74X.CTokDefenses tokDefenses)
        {
            bool ret = true;
            tokDefenses.I = ushort.Parse(_tokDefenseIbox.Text);
            tokDefenses.I0 = ushort.Parse(_tokDefenseI0box.Text);
            tokDefenses.I2 = ushort.Parse(_tokDefenseI2box.Text);
            tokDefenses.In = ushort.Parse(_tokDefenseInbox.Text);
            for (int i = 0; i < 12; i++)
            {
                if (i >= 0 && i < 4)
                {
                    ret &= WriteTokDefenseItem(tokDefenses, _tokDefenseGrid1, i, i);
                }
                //if (i >= 2 && i < 4)
                //{
                //    ret &= WriteTokDefenseItem(tokDefenses, _tokDefenseGrid2, i - 2, i);
                //}
                if (i >= 4 && i < 10)
                {
                    ret &= WriteTokDefenseItem(tokDefenses, _tokDefenseGrid3, i - 4, i);
                }
                if (i >= 10 && i < 12)
                {
                    ret &= WriteTokDefenseItem(tokDefenses, _tokDefenseGrid4, i - 10, i);
                }
            }

            return ret;
        }

        private void ValidateTokDefenses()
        {
            _tokDefenseIbox.ValidateText();
            _tokDefenseI2box.ValidateText();
            _tokDefenseI0box.ValidateText();
            _tokDefenseInbox.ValidateText();
        }

        private void _tokDefenseMainConstraintRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (_tokDefenseMainConstraintRadio.Checked)
            {
                WriteTokDefenses(_device.TokDefensesReserve);
                ShowTokDefenses(_device.TokDefensesMain);
            }
            else
            {
                WriteTokDefenses(_device.TokDefensesMain);
                ShowTokDefenses(_device.TokDefensesReserve);
            }
        }

        #endregion

        #region ������ �� ����������
        void _device_VoltageDefensesLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_VoltageDefensesLoadOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnVoltageDefensesLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnVoltageDefensesLoadOk()
        {
            _exchangeProgressBar.PerformStep();
            if (_voltageDefenseMainConstraintRadio.Checked)
            {
                ShowVoltageDefenses(_device.VoltageDefensesMain);
            }
            else
            {
                ShowVoltageDefenses(_device.VoltageDefensesReserve);
            }

        }

        private void PrepareVoltageDefenses()
        {
            _voltageDefensesGrid1.Rows.Add(4);
            _voltageDefensesGrid1[0, 0].Value = "U>";
            _voltageDefensesGrid1[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            _voltageDefensesGrid1[0, 1].Value = "U>>";
            _voltageDefensesGrid1[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            _voltageDefensesGrid1[0, 2].Value = "U<";
            _voltageDefensesGrid1[0, 2].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            _voltageDefensesGrid1[0, 3].Value = "U<<";
            _voltageDefensesGrid1[0, 3].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            _voltageDefensesGrid2.Rows.Add(2);
            _voltageDefensesGrid2[0, 0].Value = "U2>";
            _voltageDefensesGrid2[0, 1].Value = "U2>>";
            _voltageDefensesGrid2[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            _voltageDefensesGrid2[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            _voltageDefensesGrid3.Rows.Add(2);
            _voltageDefensesGrid3[0, 0].Value = "U0>";
            _voltageDefensesGrid3[0, 1].Value = "U0>>";
            _voltageDefensesGrid3[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            _voltageDefensesGrid3[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;

            _voltageDefensesGrid1.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);
            _voltageDefensesGrid2.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);
            _voltageDefensesGrid3.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);

            _voltageDefensesModeCol1.Items.AddRange(Strings.Modes.ToArray());
            _voltageDefensesBlockingNumberCol1.Items.AddRange(Strings.Logic.ToArray());
            _voltageDefensesParameterCol1.Items.AddRange(Strings.VoltageParameterU.ToArray());

            _voltageDefensesModeCol2.Items.AddRange(Strings.Modes.ToArray());
            _voltageDefensesBlockingNumberCol2.Items.AddRange(Strings.Logic.ToArray());
            _voltageDefensesParameterCol2.Items.AddRange(Strings.VoltageParameterU2.ToArray());

            _voltageDefensesModeCol3.Items.AddRange(Strings.Modes.ToArray());
            _voltageDefensesBlockingNumberCol3.Items.AddRange(Strings.Logic.ToArray());
            _voltageDefensesParameterCol3.Items.AddRange(Strings.VoltageParameterU0.ToArray());

            _voltageDefensesGrid1.CellStateChanged += new DataGridViewCellStateChangedEventHandler(OnVoltageDefenseCellStateChanged);
            _voltageDefensesGrid2.CellStateChanged += new DataGridViewCellStateChangedEventHandler(OnVoltageDefenseCellStateChanged);
            _voltageDefensesGrid3.CellStateChanged += new DataGridViewCellStateChangedEventHandler(OnVoltageDefenseCellStateChanged);

            ShowVoltageDefenses(_device.VoltageDefensesMain);
        }

        void OnVoltageDefenseCellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (e.Cell.OwningColumn == _voltageDefensesModeCol1 ||
                e.Cell.OwningColumn == _voltageDefensesModeCol2 ||
                e.Cell.OwningColumn == _voltageDefensesModeCol3 ||
                e.Cell.OwningColumn == _voltageDefensesReturnCol1 ||
                e.Cell.OwningColumn == _voltageDefensesReturnCol2 ||
                e.Cell.OwningColumn == _voltageDefensesReturnCol3
                )
            {
                DataGridView voltageGrid = sender as DataGridView;
                int rowIndex = e.Cell.RowIndex;
                
                OnVoltageDefenseModeChange(voltageGrid, rowIndex);
            }
        }

        private static void OnVoltageDefenseModeChange(DataGridView voltageGrid, int rowIndex)
        {
            int[] columns = new int[voltageGrid.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }
            if (!GridManager.ChangeCellDisabling(voltageGrid, rowIndex, "��������", 1, columns))
            {
                GridManager.ChangeCellDisabling(voltageGrid, rowIndex, false, 6, 7, 8, 9);
            }
        }

        private void SetVoltageDefenseGridItem(DataGridView grid, MR74X.CVoltageDefenses voltageDefenses, int rowIndex, int defenseItemIndex)
        {
            grid[1, rowIndex].Value = voltageDefenses[defenseItemIndex].Mode;
            grid[2, rowIndex].Value = voltageDefenses[defenseItemIndex].BlockingNumber;
            grid[3, rowIndex].Value = voltageDefenses[defenseItemIndex].Parameter;
            if (voltageDefenses[defenseItemIndex].WorkConstraint > 24)
            {
                grid[4, rowIndex].Value = (voltageDefenses[defenseItemIndex].WorkConstraint - 0.01);
            }
            else
            {
                grid[4, rowIndex].Value = voltageDefenses[defenseItemIndex].WorkConstraint;
            }
            grid[5, rowIndex].Value = voltageDefenses[defenseItemIndex].WorkTime;
            grid[6, rowIndex].Value = voltageDefenses[defenseItemIndex].Return;
            grid[7, rowIndex].Value = voltageDefenses[defenseItemIndex].APV_Return;
            grid[8, rowIndex].Value = voltageDefenses[defenseItemIndex].ReturnConstraint;
            grid[9, rowIndex].Value = voltageDefenses[defenseItemIndex].ReturnTime;
            grid[10, rowIndex].Value = voltageDefenses[defenseItemIndex].UROV;
            grid[11, rowIndex].Value = voltageDefenses[defenseItemIndex].APV;
            grid[12, rowIndex].Value = voltageDefenses[defenseItemIndex].AVR;
        }

        private void ShowVoltageDefenses(MR74X.CVoltageDefenses voltageDefenses)
        {
            for (int i = 0; i < 4; i++)
            {
                SetVoltageDefenseGridItem(_voltageDefensesGrid1, voltageDefenses, i, i);
                OnVoltageDefenseModeChange(_voltageDefensesGrid1, i);
            }
            for (int i = 0; i < 2; i++)
            {
                SetVoltageDefenseGridItem(_voltageDefensesGrid2, voltageDefenses, i, i + 4);
                OnVoltageDefenseModeChange(_voltageDefensesGrid2, i);
            }
            for (int i = 0; i < 2; i++)
            {
                SetVoltageDefenseGridItem(_voltageDefensesGrid3, voltageDefenses, i, i + 6);
                OnVoltageDefenseModeChange(_voltageDefensesGrid3, i);
            }

        }

        private void _voltageDefenseMainConstraintRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (_voltageDefenseMainConstraintRadio.Checked)
            {
                WriteVoltageDefenses(_device.VoltageDefensesReserve);
                ShowVoltageDefenses(_device.VoltageDefensesMain);
            }
            else
            {
                WriteVoltageDefenses(_device.VoltageDefensesMain);
                ShowVoltageDefenses(_device.VoltageDefensesReserve);
            }
        }

        private bool WriteVoltageDefenseGridItem(DataGridView grid, MR74X.CVoltageDefenses voltageDefenses, int rowIndex, int defenseItemIndex)
        {
            bool ret = true;
            voltageDefenses[defenseItemIndex].Mode = grid[1, rowIndex].Value.ToString();
            bool enabled = voltageDefenses[defenseItemIndex].Mode != "��������";
            if (enabled)
            {


                voltageDefenses[defenseItemIndex].BlockingNumber = grid[2, rowIndex].Value.ToString();
                voltageDefenses[defenseItemIndex].Parameter = grid[3, rowIndex].Value.ToString();
                try
                {
                    double value = double.Parse(grid[4, rowIndex].Value.ToString());
                    if (value < 0 || value > 256)
                    {
                        ShowToolTip("������� ����� � ��������� [0 - 256.0]", grid[4, rowIndex], _voltageDefensesPage);
                        ret = false;
                    }
                    else
                    {
                        voltageDefenses[defenseItemIndex].WorkConstraint = double.Parse(grid[4, rowIndex].Value.ToString());
                    }

                }
                catch (Exception)
                {
                    ShowToolTip("������� ����� � ��������� [0 - 256.0]", grid[4, rowIndex], _voltageDefensesPage);
                    ret = false;
                }
                try
                {
                    ulong value = ulong.Parse(grid[5, rowIndex].Value.ToString());
                    if (value > MR74X.TIMELIMIT)
                    {
                        ShowToolTip(MR74X.TIMELIMIT_ERROR_MSG, grid[5, rowIndex], _voltageDefensesPage);
                        ret = false;
                    }
                    else
                    {
                        voltageDefenses[defenseItemIndex].WorkTime = value;
                    }

                }
                catch (Exception)
                {
                    ShowToolTip(MR74X.TIMELIMIT_ERROR_MSG, grid[5, rowIndex], _voltageDefensesPage);
                    ret = false;
                }
                voltageDefenses[defenseItemIndex].Return = (bool)grid[6, rowIndex].Value;
                voltageDefenses[defenseItemIndex].APV_Return = (bool)grid[7, rowIndex].Value;

                try
                {
                    double value = double.Parse(grid[8, rowIndex].Value.ToString());
                    if (value < 0 || value > 256)
                    {
                        ShowToolTip("������� ����� � ��������� [0 - 256.0]", grid[8, rowIndex], _voltageDefensesPage);
                        ret = false;
                    }
                    else
                    {
                        voltageDefenses[defenseItemIndex].ReturnConstraint = value;
                    }

                }
                catch (Exception)
                {
                    ShowToolTip("������� ����� � ��������� [0 - 256.0]", grid[8, rowIndex], _voltageDefensesPage);
                    ret = false;
                }

                try
                {
                    ulong value = ulong.Parse(grid[9, rowIndex].Value.ToString());
                    if (value > MR74X.TIMELIMIT)
                    {
                        ShowToolTip(MR74X.TIMELIMIT_ERROR_MSG, grid[9, rowIndex], _voltageDefensesPage);
                        ret = false;
                    }
                    else
                    {
                        voltageDefenses[defenseItemIndex].ReturnTime = value;
                    }

                }
                catch (Exception)
                {
                    ShowToolTip(MR74X.TIMELIMIT_ERROR_MSG, grid[9, rowIndex], _voltageDefensesPage);
                    ret = false;
                }


                voltageDefenses[defenseItemIndex].UROV = (bool)grid[10, rowIndex].Value;
                voltageDefenses[defenseItemIndex].APV = (bool)grid[11, rowIndex].Value;
                voltageDefenses[defenseItemIndex].AVR = (bool)grid[12, rowIndex].Value;
            }
            return ret;
        }

        public bool WriteVoltageDefenses(MR74X.CVoltageDefenses voltageDefenses)
        {
            bool ret = true;
            for (int i = 0; i < 4; i++)
            {
                ret &= WriteVoltageDefenseGridItem(_voltageDefensesGrid1, voltageDefenses, i, i);
            }
            for (int i = 0; i < 2; i++)
            {
                ret &= WriteVoltageDefenseGridItem(_voltageDefensesGrid2, voltageDefenses, i, i + 4);
            }
            for (int i = 0; i < 2; i++)
            {
                ret &= WriteVoltageDefenseGridItem(_voltageDefensesGrid3, voltageDefenses, i, i + 6);
            }
            return ret;
        }

        #endregion

        #region �������� ����� �� �������

        void _device_FrequenceDefensesLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadComplete));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_FrequenceDefensesLoadOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnFrequenceDefensesLoadOk));
            }
            catch (InvalidOperationException)
            { }
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadComplete));
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnFrequenceDefensesLoadOk()
        {
            _exchangeProgressBar.PerformStep();
            if (_frequenceDefenseMainConstraintRadio.Checked)
            {
                ShowFrequenceDefenses(_device.FrequenceDefensesMain);
            }
            else
            {
                ShowFrequenceDefenses(_device.FrequenceDefensesReserve);
            }

        }

        private void PrepareFrequenceDefenses()
        {
            _frequenceDefensesGrid.Rows.Add(4);

            _frequenceDefensesMode.Items.AddRange(Strings.Modes.ToArray());
            _frequenceDefensesBlockingNumber.Items.AddRange(Strings.Logic.ToArray());
            _frequenceDefensesGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);
            _frequenceDefensesGrid.CellStateChanged += new DataGridViewCellStateChangedEventHandler(OnFrequenceDefenseCellStateChanged);
            _frequenceDefensesGrid.Height = _frequenceDefensesGrid.ColumnHeadersHeight + (_frequenceDefensesGrid.Rows.Count + 1) * _frequenceDefensesGrid.RowTemplate.Height;
            ShowFrequenceDefenses(_device.FrequenceDefensesMain);
        }

        void OnFrequenceDefenseCellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (e.Cell.OwningColumn == _frequenceDefensesMode || e.Cell.OwningColumn == _frequenceDefensesReturn)
            {
                DataGridView frequenceGrid = sender as DataGridView;
                int rowIndex = e.Cell.RowIndex;

                OnFrequenceDefenseModeChanged(frequenceGrid, rowIndex);
            }
        }

        private static void OnFrequenceDefenseModeChanged(DataGridView frequenceGrid, int rowIndex)
        {
            int[] columns = new int[frequenceGrid.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }

            if (!GridManager.ChangeCellDisabling(frequenceGrid, rowIndex, "��������", 1, columns))
            {
                GridManager.ChangeCellDisabling(frequenceGrid, rowIndex, false, 5, 6, 7, 8);
            }
        }

        private void SetFrequenceDefenseGridItem(DataGridView grid, MR74X.CFrequenceDefenses frequenceDefenses, int rowIndex, int defenseItemIndex)
        {
            grid[0, rowIndex].Value = frequenceDefenses[defenseItemIndex].Name;
            grid[1, rowIndex].Value = frequenceDefenses[defenseItemIndex].Mode;
            grid[2, rowIndex].Value = frequenceDefenses[defenseItemIndex].BlockingNumber;
            if (frequenceDefenses[defenseItemIndex].WorkConstraint > 24)
            {
                grid[3, rowIndex].Value = (frequenceDefenses[defenseItemIndex].WorkConstraint - 0.01);
            }
            else
            {
                grid[3, rowIndex].Value = frequenceDefenses[defenseItemIndex].WorkConstraint;
            }
            
            grid[4, rowIndex].Value = frequenceDefenses[defenseItemIndex].WorkTime;
            grid[5, rowIndex].Value = frequenceDefenses[defenseItemIndex].Return;
            grid[6, rowIndex].Value = frequenceDefenses[defenseItemIndex].APV_Return;
            grid[7, rowIndex].Value = frequenceDefenses[defenseItemIndex].ReturnTime;
            grid[8, rowIndex].Value = frequenceDefenses[defenseItemIndex].ConstraintAPV;
            grid[9, rowIndex].Value = frequenceDefenses[defenseItemIndex].UROV;
            grid[10, rowIndex].Value = frequenceDefenses[defenseItemIndex].APV;
            grid[11, rowIndex].Value = frequenceDefenses[defenseItemIndex].AVR;
        }

        private void ShowFrequenceDefenses(MR74X.CFrequenceDefenses frequenceDefenses)
        {
            for (int i = 0; i < 4; i++)
            {
                SetFrequenceDefenseGridItem(_frequenceDefensesGrid, frequenceDefenses, i, i);
                OnFrequenceDefenseModeChanged(_frequenceDefensesGrid, i);
            }
        }

        private void _frequenceDefenseMainConstraintRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (_frequenceDefenseMainConstraintRadio.Checked)
            {
                WriteFrequenceDefenses(_device.FrequenceDefensesReserve);
                ShowFrequenceDefenses(_device.FrequenceDefensesMain);
            }
            else
            {
                WriteFrequenceDefenses(_device.FrequenceDefensesMain);
                ShowFrequenceDefenses(_device.FrequenceDefensesReserve);
            }
        }

        private bool WriteFrequenceDefenseGridItem(DataGridView grid, MR74X.CFrequenceDefenses frequenceDefenses, int rowIndex, int defenseItemIndex)
        {
            bool ret = true;
            frequenceDefenses[defenseItemIndex].Mode = grid[1, rowIndex].Value.ToString();
            bool rowEnabled = ("��������" != frequenceDefenses[defenseItemIndex].Mode);
            if (rowEnabled)
            {
                frequenceDefenses[defenseItemIndex].BlockingNumber = grid[2, rowIndex].Value.ToString();
                try
                {

                    double value = double.Parse(grid[3, rowIndex].Value.ToString());

                    if (value < 48 || value > 52)
                    {
                        ShowToolTip("������� ����� � ��������� [48 - 52]", grid[3, rowIndex], _frequencePage);
                        ret = false;
                    }
                    else
                    {
                        frequenceDefenses[defenseItemIndex].WorkConstraint = value;
                    }

                }
                catch (Exception)
                {
                    ShowToolTip("������� ����� � ��������� [48 - 52]", grid[3, rowIndex], _frequencePage);
                    ret = false;
                }
                try
                {
                    ulong value = ulong.Parse(grid[4, rowIndex].Value.ToString());
                    if (value > MR74X.TIMELIMIT)
                    {
                        throw new OverflowException(MR74X.TIMELIMIT_ERROR_MSG);
                    }
                    else
                    {
                        frequenceDefenses[defenseItemIndex].WorkTime = value;
                    }

                }
                catch (Exception)
                {
                    ShowToolTip(MR74X.TIMELIMIT_ERROR_MSG, grid[4, rowIndex], _frequencePage);
                    ret = false;
                }
                frequenceDefenses[defenseItemIndex].Return = (bool)grid[5, rowIndex].Value;
                frequenceDefenses[defenseItemIndex].APV_Return = (bool)grid[6, rowIndex].Value;

                try
                {
                    ulong value = ulong.Parse(grid[7, rowIndex].Value.ToString());
                    if (value > MR74X.TIMELIMIT)
                    {
                        throw new OverflowException(MR74X.TIMELIMIT_ERROR_MSG);
                    }
                    else
                    {
                        frequenceDefenses[defenseItemIndex].ReturnTime = value;
                    }
                }
                catch (Exception)
                {
                    ShowToolTip(MR74X.TIMELIMIT_ERROR_MSG, grid[7, rowIndex], _frequencePage);
                    ret = false;
                }
                try
                {
                    if (frequenceDefenses[defenseItemIndex].Return)
                    {
                        double value = double.Parse(grid[8, rowIndex].Value.ToString());

                        if (value < 48 || value > 52)
                        {
                            ShowToolTip("������� ����� � ��������� [48 - 52]", grid[8, rowIndex], _frequencePage);
                            ret = false;
                        }
                        else
                        {
                            frequenceDefenses[defenseItemIndex].ConstraintAPV = value;
                        }
                    }

                }
                catch (Exception)
                {
                    ShowToolTip("������� ����� � ��������� [48 - 52]", grid[8, rowIndex], _frequencePage);
                    ret = false;
                }
                frequenceDefenses[defenseItemIndex].UROV = (bool)grid[9, rowIndex].Value;
                frequenceDefenses[defenseItemIndex].APV = (bool)grid[10, rowIndex].Value;
                frequenceDefenses[defenseItemIndex].AVR = (bool)grid[11, rowIndex].Value;
            }
            return ret;
        }

        public bool WriteFrequenceDefenses(MR74X.CFrequenceDefenses frequenceDefenses)
        {
            bool ret = true;
            for (int i = 0; i < 4; i++)
            {
                ret &= WriteFrequenceDefenseGridItem(_frequenceDefensesGrid, frequenceDefenses, i, i);
            }

            return ret;
        }

        #endregion

        private void _OMP_TypeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_OMP_TypeBox.Text == "�������")
            {
                _HUD_Box.ReadOnly = false;
            }
            else
            {
                _HUD_Box.Text = "0";
                _HUD_Box.ReadOnly = true;
            }
            
        }

    }
}