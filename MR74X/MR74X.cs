﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Collections;

using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.Devices;
using BEMN.Forms;
using BEMN.MR74X.Utils;

using System.ComponentModel;
using System.Collections.Specialized;
using BEMN.MR74X.Utils;
using NUnit.Framework;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Globalization;
using System.Drawing.Design;
using BEMN.Utils;
using BEMN.MR74X.Data;
using DescriptionAttribute=System.ComponentModel.DescriptionAttribute;

namespace BEMN.MR74X
{
   
    public enum LogicState { Нет, Да, Инверс };

    public enum ConstraintKoefficient
    {
        K_25600 = 25600,
        K_10000 = 10000,
        K_4000 = 4002,
        K_500 = 500
    };
    
    public class Measuring
    {
       public static double GetI(ushort value,ushort KoeffNPTT,bool phaseTok)
        {
            int b;
            b = phaseTok ? 40 : 5;
            return (double)((double)b * (double)value / (double)0x10000 * (double)KoeffNPTT);
        }

        public static double GetU(ushort value, double Koeff)
        {
            return (double)((double)value / (double)0x100 * (double)Koeff);
        }

        public static double GetF(ushort value)
        {
            return (double)((double)value / (double)0x100);
        }

        public static ulong GetTime(ushort value)
        {
            return value < 32768 ? (ulong)value * 10 : ((ulong)value - 32768) * 100;
        }

        public static double GetConstraint(ushort value, ConstraintKoefficient koeff)
        {
            double temp = (double)value * (int)koeff / 65535;
            return Math.Round(temp, 0, MidpointRounding.ToEven) / 100;
            //return Math.Floor(temp + 0.5) / 100;
        }

        public static ushort SetConstraint(double value,ConstraintKoefficient koeff)
        {
            return (ushort)(value * 65535 * 100 / (int)koeff);
        }

        public static ushort SetTime(ulong value)
        {
            return value < 327680 ? (ushort)(value / 10) : (ushort)(value / 100 + 32768);
        }
    }

    public class MR74X : Device, IDeviceView, ISlave, IDeviceLog
    {
        public const ulong TIMELIMIT = 3000000;
        public const string TIMELIMIT_ERROR_MSG = "Введите число в диапазоне [0 - 3000000]";

        #region Системный журнал

        public const int SYSTEMJOURNAL_RECORD_CNT = 128;
        [Browsable(false)]
        [XmlIgnore]
        public CSystemJournal SystemJournal
        {
            get
            {
                return _systemJournalRecords;
            }
        }

        public struct SystemRecord
        {
            public string msg;
            public string time;

        };
                
        public class CSystemJournal : ICollection
        {

            private List<SystemRecord> _messages = new List<SystemRecord>(SYSTEMJOURNAL_RECORD_CNT);

            public int Count
            {
                get
                {
                    return _messages.Count;
                }
            }

            public SystemRecord this[int i]
            {
                get
                {
                    return (SystemRecord)(_messages[i]);
                }

            }

            public bool IsMessageEmpty(int i)
            {
                return "Пусто" == _messages[i].msg;
            }
            
            public bool AddMessage(ushort[] value)
            {
                bool ret = true;
                //Common.SwapArrayItems(ref value);
                SystemRecord msg = CreateMessage(value);
                if ("Пусто" == msg.msg)
                {
                    ret = false;
                }
                else
                {
                    ret = true;
                    _messages.Add(msg);
                }
                return ret;
                
            }

            private SystemRecord CreateMessage(ushort[] value)
            {
                SystemRecord msg = new SystemRecord();

                switch (value[0])
                {
                    case 0:
                        msg.msg = "Пусто";
                        break;
                    case 1:
                    case 2:
                        msg.msg = "Ошибка хранения данных";
                        break;
                    case 3:
                        msg.msg = "Неисправность вн. шины";
                        break;
                    case 4:
                        msg.msg = "Вн. шина исправна";
                        break;
                    case 5:
                        msg.msg = "Температура выше нормы";
                        break;
                    case 6:
                        msg.msg = "Температура в норме";
                        break;
                    case 7:
                        msg.msg = "МСА 1 неисправен (МСАТ)";
                        break;
                    case 8:
                        msg.msg = "МСА 1 исправен (МСАТ)";
                        break;
                    case 9:
                        msg.msg = "МСА 2 неисправен (МСАН)";
                        break;
                    case 10:
                        msg.msg = "МСА 2 исправен (МСАН)";
                        break;
                    case 11:
                        msg.msg = "МРВ неисправен";
                        break;
                    case 12:
                        msg.msg = "МРВ исправен";
                        break;
                    case 13:
                        msg.msg = "МСД1 неисправен";
                        break;
                    case 14:
                        msg.msg = "МСД1 исправен";
                        break;
                    case 15:
                        msg.msg = "МСД2 неисправен";
                        break;
                    case 16:
                        msg.msg = "МСД2 исправен";
                        break;
                    case 17:
                        msg.msg = "Ошибка контрольной суммы уставок";
                        break;
                    case 18:
                    case 19:
                        msg.msg = "Ошибка контрольной суммы данных";
                        break;
                    case 20:
                        msg.msg = "Ошибка журнала системы";
                        break;
                    case 21:
                        msg.msg = "Ошибка журнала аварий";
                        break;
                    case 22:
                    case 25:
                        msg.msg = "Меню - уставки изменены";
                        break;
                    case 26:
                        msg.msg = "Пароль изменен";
                        break;
                    case 27:
                        msg.msg = "Сброс журнала системы";
                        break;
                    case 28:
                        msg.msg = "Сброс журнала аварий";
                        break;
                    case 29:
                        msg.msg = "Сброс ресурса выключателя";
                        break;
                    case 32:
                        msg.msg = "СДТУ – уставки изменены";
                        break;
                    case 33:
                        msg.msg = "Ошибка задающего генератора";
                        break;
                    case 34:
                        msg.msg = "Рестарт устройства";
                        break;
                    case 35:
                        msg.msg = "Устройство выключено";
                        break;
                    case 36:
                        msg.msg = "Устройство включено";
                        break;
                    case 50:
                        msg.msg = "ТН внеш. неисправность";
                        break;
                    case 51:
                        msg.msg = "ТН исправен";
                        break;
                    case 56:
                        msg.msg = "Uавс < 5В";
                        break;
                    case 57:
                        msg.msg = "Uавс > 5В";
                        break;
                    case 58:
                        msg.msg = "ТННП внеш. неисправность";
                        break;
                    case 59:
                        msg.msg = "ТННП исправен";
                        break;
                    case 60:
                        msg.msg = "Частота вне диапазона";
                        break;
                    case 61:
                        msg.msg = "Частота в норме";
                        break;
                    case 62:
                        msg.msg = "Выключатель отключен";
                        break;
                    case 63:
                        msg.msg = "Выключатель включен";
                        break;
                    case 64:
                        msg.msg = "Блокировка выключателя";
                        break;
                    case 65:
                        msg.msg = "Отказ выключателя";
                        break;
                    case 66:
                        msg.msg = "Неисправность выключателя";
                        break;
                    case 67:
                        msg.msg = "Внеш.неиспр. выключателя";
                        break;
                    case 68:
                        msg.msg = "Неиспр.управ. выключателя";
                        break;
                    case 69:
                        msg.msg = "Работа УРОВ";
                        break;
                    case 71:
                        msg.msg = "Защита отключить";
                        break;
                    case 72:
                        msg.msg = "АПВ блокировано";
                        break;
                    case 73:
                        msg.msg = "АПВ вн.блокировка";
                        break;
                    case 74:
                        msg.msg = "Запуск АПВ 1 крат";
                        break;
                    case 75:
                        msg.msg = "Запуск АПВ 2 крат";
                        break;
                    case 76:
                        msg.msg = "Запуск АПВ 3 крат";
                        break;
                    case 77:
                        msg.msg = "Запуск АПВ 4 крат";
                        break;
                    case 78:
                        msg.msg = "АПВ включить";
                        break;
                    case 85:
                        msg.msg = "AВР блокирован";
                        break;
                    case 86:
                        msg.msg = "АВР внеш. блокировка";
                        break;
                    case 87:
                        msg.msg = "АВР готовность";
                        break;
                    case 88:
                        msg.msg = "АВР отключить";
                        break;
                    case 89:
                        msg.msg = "АВР включить";
                        break;
                    case 90:
                        msg.msg = "АВР вкл. резерв";
                        break;
                    case 91:
                        msg.msg = "АВР откл. резерв";
                        break;
                    case 92:
                        msg.msg = "АВР запуск от защиты";
                        break;
                    case 93:
                        msg.msg = "АВР запуск команда откл.";
                        break;
                    case 94:
                        msg.msg = "АВР запуск по питанию";
                        break;
                    case 95:
                        msg.msg = "АВР запуск самооткл.";
                        break;
                    case 96:
                        msg.msg = "Кнопка отключить";
                        break;
                    case 97:
                        msg.msg = "Кнопка включить";
                        break;
                    case 98:
                        msg.msg = "Ключ отключить";
                        break;
                    case 99:
                        msg.msg = "Ключ включить";
                        break;
                    case 100:
                        msg.msg = "Внешнее отключить";
                        break;
                    case 101:
                        msg.msg = "Внешнее включить";
                        break;
                    case 102:
                        msg.msg = "СДТУ отключить";
                        break;
                    case 103:
                        msg.msg = "СДТУ включить";
                        break;
                    case 104:
                        msg.msg = "Основные уставки";
                        break;
                    case 105:
                        msg.msg = "Резервные уставки";
                        break;
                    case 106:
                        msg.msg = "Внеш.резерв. уставки";
                        break;
                    case 108:
                        msg.msg = "Меню-основные уставки";
                        break;
                    case 109:
                        msg.msg = "Меню-резервные уставки";
                        break;
                    case 110:
                        msg.msg = "СДТУ-основные уставки";
                        break;
                    case 111:
                        msg.msg = "СДТУ-резервные уставки";
                        break;
                    case 112:
                        msg.msg = "АПВ возврат";
                        break;
                    case 113:
                        msg.msg = "АПВ возврат F>";
                        break;
                    case 114:
                        msg.msg = "АПВ возврат F>>";
                        break;
                    case 115:
                        msg.msg = "АПВ возврат F<";
                        break;
                    case 116:
                        msg.msg = "АПВ возврат F<<";
                        break;
                    case 117:
                        msg.msg = "АПВ возврат U>";
                        break;
                    case 118:
                        msg.msg = "АПВ возврат U>>";
                        break;
                    case 119:
                    case 120:
                        msg.msg = "АПВ возврат U<<";
                        break;
                    case 121:
                        msg.msg = "АПВ возврат U2>";
                        break;
                    case 122:
                        msg.msg = "АПВ возврат U2>>";
                        break;
                    case 123:
                        msg.msg = "АПВ возврат Uo>";
                        break;
                    case 124:
                        msg.msg = "АПВ возврат Uo>";
                        break;
                    case 125:
                        msg.msg = "АПВ возврат ВЗ-1";
                        break;
                    case 126:
                        msg.msg = "АПВ возврат ВЗ-2";
                        break;
                    case 127:
                        msg.msg = "АПВ возврат ВЗ-3";
                        break;
                    case 128:
                        msg.msg = "АПВ возврат ВЗ-4";
                        break;
                    case 129:
                        msg.msg = "АПВ возврат ВЗ-5";
                        break;
                    case 130:
                        msg.msg = "АПВ возврат ВЗ-6";
                        break;
                    case 131:
                        msg.msg = "АПВ возврат ВЗ-7";
                        break;
                    case 132:
                        msg.msg = "АПВ возврат ВЗ-8";
                        break;
                    case 133:
                        msg.msg = "U<10B Частота недостоверна";
                        break;
                    case 134:
                        msg.msg = "U>10B Частота достоверна";
                        break;
                    case 135:
                        msg.msg = "АВР Меню блокировка";
                        break;
                    case 136:
                        msg.msg = "АВР СДТУ блокировка";
                        break;
                    case 137:
                        msg.msg = "Блокировка по перегреву";
                        break;
                    case 138:
                        msg.msg = "Ошибка логики по старту";
                        break;
                    case 139:
                        msg.msg = "Вн. – сброс теплового состояния";
                        break;
                    case 140:
                        msg.msg = "Вн. – сброс числа пусков";
                        break;
                    case 141:
                        msg.msg = "Меню – сброс теплового состояния";
                        break;
                    case 142:
                        msg.msg = "Меню – сброс числа пусков";
                        break;
                    case 143:
                        msg.msg = "СДТУ – сброс теплового состояния";
                        break;
                    case 144:
                        msg.msg = "СДТУ – сброс числа пусков";
                        break;

                }
                msg.time = value[3].ToString("d2") + "-" + value[2].ToString("d2") + "-" + value[1].ToString("d2") + " " +
                           value[4].ToString("d2") + ":" + value[5].ToString("d2") + ":" + value[6].ToString("d2") + ":" + value[7].ToString("d2");

                return msg;
            }

            
            #region ICollection Members

            public void CopyTo(Array array, int index)
            {
                
            }

            public bool IsSynchronized
            {
                get {return false; }
            }

            public object SyncRoot
            {
                get {return _messages; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _messages.GetEnumerator();
            }

            #endregion
        }
        #endregion

        #region Журнал аварий
        public const int ALARMJOURNAL_RECORD_CNT = 32;

        [Browsable(false)]
        public CAlarmJournal AlarmJournal
        {
            get { return _alarmJournalRecords; }
        }

        public struct AlarmRecord
        {
            public string time;
            public string msg;
            public string code;
            public string type_value;
            public string Ia;
            public string Ib;
            public string Ic;
            public string I0;
            public string I1;
            public string I2;
            public string In;
            public string Ig;
            public string F;
            public string Uab;
            public string Ubc;
            public string Uca;
            public string U0;
            public string U1;
            public string U2;
            public string Un;
            public string inSignals1;
            public string inSignals2;
        } ;

        public class CAlarmJournal
        {
            private Dictionary<ushort, string> _msgDictionary;
            private Dictionary<byte, string> _codeDictionary;
            private Dictionary<byte, string> _typeDictionary;
            private MR74X _device;

            public CAlarmJournal(MR74X device)
            {
                _device = device;
                _msgDictionary = new Dictionary<ushort, string>(7);
                _codeDictionary = new Dictionary<byte, string>(32);
                _typeDictionary = new Dictionary<byte, string>(28);

                _msgDictionary.Add(0, "Журнал пуст");
                _msgDictionary.Add(1, "Сигнализация");
                _msgDictionary.Add(2, "Отключение");
                _msgDictionary.Add(3, "Работа");
                _msgDictionary.Add(4, "Неуспешное АПВ");
                _msgDictionary.Add(5, "Возврат");
                _msgDictionary.Add(6, "Включение");
                _msgDictionary.Add(7, "ОМП");

                _codeDictionary.Add(1, "I>");
                _codeDictionary.Add(2, "I>>");
                _codeDictionary.Add(3, "I>>>");
                _codeDictionary.Add(4, "I>>>>");
                _codeDictionary.Add(5, "I2>");
                _codeDictionary.Add(6, "I2>>");
                _codeDictionary.Add(7, "I0>");
                _codeDictionary.Add(8, "I0>>");
                _codeDictionary.Add(9, "In>");
                _codeDictionary.Add(10, "In>>");
                _codeDictionary.Add(11, "Ig>");
                _codeDictionary.Add(12, "I1/I2");
                _codeDictionary.Add(13, "F>");
                _codeDictionary.Add(14, "F>>");
                _codeDictionary.Add(15, "F<");
                _codeDictionary.Add(16, "F<<");
                _codeDictionary.Add(17, "U>");
                _codeDictionary.Add(18, "U>>");
                _codeDictionary.Add(19, "U>");
                _codeDictionary.Add(20, "U<<");
                _codeDictionary.Add(21, "U2>");
                _codeDictionary.Add(22, "U2>>");
                _codeDictionary.Add(23, "U0>");
                _codeDictionary.Add(24, "U0>>");
                _codeDictionary.Add(25, "ВЗ-1");
                _codeDictionary.Add(26, "ВЗ-2");
                _codeDictionary.Add(27, "ВЗ-3");
                _codeDictionary.Add(28, "ВЗ-4");
                _codeDictionary.Add(29, "ВЗ-5");
                _codeDictionary.Add(30, "ВЗ-6");
                _codeDictionary.Add(31, "ВЗ-7");
                _codeDictionary.Add(32, "ВЗ-8");
                _codeDictionary.Add(33, "ОМП");

                _typeDictionary.Add(1, "Ig");
                _typeDictionary.Add(2, "In");
                _typeDictionary.Add(3, "Ia");
                _typeDictionary.Add(4, "Ib");
                _typeDictionary.Add(5, "Ic");
                _typeDictionary.Add(6, "I0");
                _typeDictionary.Add(7, "I1");
                _typeDictionary.Add(8, "I2");
                _typeDictionary.Add(9, "Pn");
                _typeDictionary.Add(10, "Pa");
                _typeDictionary.Add(11, "Pb");
                _typeDictionary.Add(12, "Pc");
                _typeDictionary.Add(13, "P0");
                _typeDictionary.Add(14, "P1");
                _typeDictionary.Add(15, "P2");
                _typeDictionary.Add(16, "F");
                _typeDictionary.Add(17, "Un");
                _typeDictionary.Add(18, "Ua");
                _typeDictionary.Add(19, "Ub");
                _typeDictionary.Add(20, "Uc");
                _typeDictionary.Add(21, "Uab");
                _typeDictionary.Add(22, "Ubc");
                _typeDictionary.Add(23, "Uca");
                _typeDictionary.Add(24, "U0");
                _typeDictionary.Add(25, "U1");
                _typeDictionary.Add(26, "U2");
                _typeDictionary.Add(27, "Обрыв провода");
                _typeDictionary.Add(28, "Lкз");
            }

            private List<AlarmRecord> _messages = new List<AlarmRecord>(ALARMJOURNAL_RECORD_CNT);

            public int Count
            {
                get { return _messages.Count; }
            }

            public AlarmRecord this[int i]
            {
                get { return _messages[i]; }
            }

            public bool IsMessageEmpty(int i)
            {
                return "Пусто" == _messages[i].msg;
            }

            public bool AddMessage(byte[] value)
            {
                bool ret;
                Common.SwapArrayItems(ref value);
                AlarmRecord msg = CreateMessage(value);
                if ("Пусто" == msg.msg)
                {
                    ret = false;
                }
                else
                {
                    ret = true;
                    _messages.Add(msg);
                }
                return ret;
            }

            public string GetMessage(byte b)
            {
                return _msgDictionary[b];
            }

            public string GetDateTime(byte[] datetime)
            {
                return datetime[4].ToString("d2") + "-" + datetime[2].ToString("d2") + "-" + datetime[0].ToString("d2") +
                           " " +
                           datetime[6].ToString("d2") + ":" + datetime[8].ToString("d2") + ":" + datetime[10].ToString("d2") +
                           ":" + datetime[12].ToString("d2");
            }

            public string GetCode(byte codeByte, byte phaseByte)
            {

                string group = (0 == (codeByte & 0x80)) ? "основная" : "резерв.";
                string phase = "";
                phase += (0 == (phaseByte & 0x08)) ? " " : "_";
                if (phaseByte == 4 )
                {
                    phase += (0 == (phaseByte & 0x01)) ? " " : "A";
                    phase += (0 == (phaseByte & 0x02)) ? " " : "B";
                    phase += (0 == (phaseByte & 0x04)) ? " " : "C";
                }else
                {
                    phase += (0 == (phaseByte & 0x04)) ? " " : "A";
                    phase += (0 == (phaseByte & 0x02)) ? " " : "B";
                    phase += (0 == (phaseByte & 0x01)) ? " " : "C";
                }


                byte code = (byte)(codeByte & 0x7F);
                try
                {
                    return _codeDictionary[code] + " " + group + " " + phase;
                }
                catch (KeyNotFoundException)
                {
                    return "";
                }

            }

            public string GetTypeValue(ushort damageWord, byte typeByte)
            {
                double damageValue = Measuring.GetI(damageWord, _device.TT, true);
                try
                {
                    return _typeDictionary[typeByte] + String.Format(" = {0:F2}", damageValue);
                }
                catch (KeyNotFoundException)
                {
                    return "";
                }
            }


            private AlarmRecord CreateMessage(byte[] buffer)
            {
                AlarmRecord rec = new AlarmRecord();
                rec.msg = GetMessage(buffer[0]);
                byte[] datetime = new byte[14];
                Array.ConstrainedCopy(buffer, 2, datetime, 0, 14);
                rec.time = GetDateTime(datetime);


                rec.code = GetCode(buffer[16], buffer[18]);
                rec.type_value = GetTypeValue(Common.TOWORD(buffer[13], buffer[12]), buffer[19]);


                double Ia = Measuring.GetI(Common.TOWORD(buffer[23], buffer[22]), _device.TT, true);
                double Ib = Measuring.GetI(Common.TOWORD(buffer[25], buffer[24]), _device.TT, true);
                double Ic = Measuring.GetI(Common.TOWORD(buffer[27], buffer[26]), _device.TT, true);
                double I0 = Measuring.GetI(Common.TOWORD(buffer[29], buffer[28]), _device.TT, true);
                double I1 = Measuring.GetI(Common.TOWORD(buffer[31], buffer[30]), _device.TT, true);
                double I2 = Measuring.GetI(Common.TOWORD(buffer[33], buffer[32]), _device.TT, true);
                double In = Measuring.GetI(Common.TOWORD(buffer[35], buffer[34]), _device.TTNP, false);
                double Ig = Measuring.GetI(Common.TOWORD(buffer[37], buffer[36]), _device.TTNP, false);
                double F = Measuring.GetF(Common.TOWORD(buffer[39], buffer[38]));
                double Uab = Measuring.GetU(Common.TOWORD(buffer[41], buffer[40]), _device.TN);
                double Ubc = Measuring.GetU(Common.TOWORD(buffer[43], buffer[42]), _device.TN);
                double Uca = Measuring.GetU(Common.TOWORD(buffer[45], buffer[44]), _device.TN);
                double U0 = Measuring.GetU(Common.TOWORD(buffer[47], buffer[46]), _device.TN);
                double U1 = Measuring.GetU(Common.TOWORD(buffer[49], buffer[48]), _device.TN);
                double U2 = Measuring.GetU(Common.TOWORD(buffer[51], buffer[50]), _device.TN);
                double Un = Measuring.GetU(Common.TOWORD(buffer[53], buffer[52]), _device.TNNP);

                rec.Ia = String.Format("{0:F2}", Ia);
                rec.Ib = String.Format("{0:F2}", Ib);
                rec.Ic = String.Format("{0:F2}", Ic);
                rec.I0 = String.Format("{0:F2}", I0);
                rec.I1 = String.Format("{0:F2}", I1);
                rec.I2 = String.Format("{0:F2}", I2);
                rec.In = String.Format("{0:F2}", In);
                rec.Ig = String.Format("{0:F2}", Ig);
                rec.F = String.Format("{0:F2}", F);
                rec.Uab = String.Format("{0:F2}", Uab);
                rec.Ubc = String.Format("{0:F2}", Ubc);
                rec.Uca = String.Format("{0:F2}", Uca);
                rec.U0 = String.Format("{0:F2}", U0);
                rec.U1 = String.Format("{0:F2}", U1);
                rec.U2 = String.Format("{0:F2}", U2);
                rec.Un = String.Format("{0:F2}", Un);
                byte[] b1 = new byte[] { buffer[54] };
                byte[] b2 = new byte[] { buffer[55] };
                rec.inSignals1 = Common.BitsToString(new BitArray(b1));
                rec.inSignals2 = Common.BitsToString(new BitArray(b2));
                return rec;
            }
        }


        //public const int ALARMJOURNAL_RECORD_CNT = 32;
        //[Browsable(false)]
        //public CAlarmJournal AlarmJournal
        //{
        //    get
        //    {
        //        return _alarmJournalRecords;
        //    }
        //}

        //public struct AlarmRecord
        //{           
        //    public string time;
        //    public string msg;
        //    public string code;
        //    public string type_value;
        //    public string Ia;
        //    public string Ib;
        //    public string Ic;
        //    public string I0;
        //    public string I1;
        //    public string I2;
        //    public string In;
        //    public string Ig;
        //    public string F;
        //    public string Uab;
        //    public string Ubc;
        //    public string Uca;
        //    public string U0;
        //    public string U1;
        //    public string U2;
        //    public string Un;
        //    public string inSignals1;
        //    public string inSignals2;
        //};

        //public class CAlarmJournal
        //{
        //    private Dictionary<ushort, string> _msgDictionary;
        //    private Dictionary<byte, string> _codeDictionary;
        //    private Dictionary<byte, string> _typeDictionary;
        //    MR74X _device;
            
        //    public CAlarmJournal(MR74X device)
        //    {
        //        _device = device;
        //        _msgDictionary = new Dictionary<ushort, string>(6);
        //        _codeDictionary = new Dictionary<byte, string>(31);
        //        _typeDictionary = new Dictionary<byte, string>(26);

        //        _msgDictionary.Add(0, "Журнал пуст");
        //        _msgDictionary.Add(1, "Сигнализация");
        //        _msgDictionary.Add(2, "Отключение");
        //        _msgDictionary.Add(3, "Работа");
        //        _msgDictionary.Add(4, "Неуспешное АПВ");
        //        _msgDictionary.Add(5, "Возврат");
        //        _msgDictionary.Add(6, "Включение");

        //        _codeDictionary.Add(1, "I>");
        //        _codeDictionary.Add(2, "I>>");
        //        _codeDictionary.Add(3, "I>>>");
        //        _codeDictionary.Add(4, "I>>>>");
        //        _codeDictionary.Add(5, "I2>");
        //        _codeDictionary.Add(6, "I2>>");
        //        _codeDictionary.Add(7, "I0>");
        //        _codeDictionary.Add(8, "I0>>");
        //        _codeDictionary.Add(9, "In>");
        //        _codeDictionary.Add(10, "In>>");
        //        _codeDictionary.Add(11, "Ig>");
        //        _codeDictionary.Add(12, "I1/I2");
        //        _codeDictionary.Add(13, "F>");
        //        _codeDictionary.Add(14, "F>>");
        //        _codeDictionary.Add(15, "F<");
        //        _codeDictionary.Add(16, "F<<");
        //        _codeDictionary.Add(17, "U>");
        //        _codeDictionary.Add(18, "U>>");
        //        _codeDictionary.Add(19, "U>");
        //        _codeDictionary.Add(20, "U<<");
        //        _codeDictionary.Add(21, "U2>");
        //        _codeDictionary.Add(22, "U2>>");
        //        _codeDictionary.Add(23, "U0>");
        //        _codeDictionary.Add(24, "U0>>");
        //        _codeDictionary.Add(25, "ВЗ-1");
        //        _codeDictionary.Add(26, "ВЗ-2");
        //        _codeDictionary.Add(27, "ВЗ-3");
        //        _codeDictionary.Add(28, "ВЗ-4");
        //        _codeDictionary.Add(29, "ВЗ-5");
        //        _codeDictionary.Add(30, "ВЗ-6");
        //        _codeDictionary.Add(31, "ВЗ-7");
        //        _codeDictionary.Add(32, "ВЗ-8");

        //        _typeDictionary.Add(1, "Ig");
        //        _typeDictionary.Add(2, "In");
        //        _typeDictionary.Add(3, "Ia");
        //        _typeDictionary.Add(4, "Ib");
        //        _typeDictionary.Add(5, "Ic");
        //        _typeDictionary.Add(6, "I0");
        //        _typeDictionary.Add(7, "I1");
        //        _typeDictionary.Add(8, "I2");
        //        _typeDictionary.Add(9, "Pn");
        //        _typeDictionary.Add(10, "Pa");
        //        _typeDictionary.Add(11, "Pb");
        //        _typeDictionary.Add(12, "Pc");
        //        _typeDictionary.Add(13, "P0");
        //        _typeDictionary.Add(14, "P1");
        //        _typeDictionary.Add(15, "P2");
        //        _typeDictionary.Add(16, "F");
        //        _typeDictionary.Add(17, "Un");
        //        _typeDictionary.Add(18, "Ua");
        //        _typeDictionary.Add(19, "Ub");
        //        _typeDictionary.Add(20, "Uc");
        //        _typeDictionary.Add(21, "Uab");
        //        _typeDictionary.Add(22, "Ubc");
        //        _typeDictionary.Add(23, "Uca");
        //        _typeDictionary.Add(24, "U0");
        //        _typeDictionary.Add(25, "U1");
        //        _typeDictionary.Add(26, "U2");
        //        _typeDictionary.Add(27, "Обрыв провода");


        //    }

        //    private List<AlarmRecord> _messages = new List<AlarmRecord>(ALARMJOURNAL_RECORD_CNT);
            
        //    public int Count
        //    {
        //        get
        //        {
        //            return _messages.Count;
        //        }
        //    }

        //    public AlarmRecord this[int i]
        //    {
        //        get
        //        {
        //            return (AlarmRecord)(_messages[i]);
        //        }

        //    }

        //    public bool IsMessageEmpty(int i)
        //    {
        //        return "Пусто" == _messages[i].msg;
        //    }

        //    public bool AddMessage(byte[] value)
        //    {
        //        bool ret = true;
        //        Common.SwapArrayItems(ref value);
        //        AlarmRecord msg = CreateMessage(value);
        //        if ("Пусто" == msg.msg)
        //        {
        //            ret = false;
        //        }
        //        else
        //        {
        //            ret = true;
        //            _messages.Add(msg);
        //        }
        //        return ret;

        //    }

        //    public string GetMessage(byte b)
        //    {
        //        return _msgDictionary[b];
        //    }

        //    public string GetDateTime(byte[] datetime)
        //    {
        //        return datetime[4].ToString("d2") + "-" + datetime[2].ToString("d2") + "-" + datetime[0].ToString("d2") +
        //                   " " +
        //                   datetime[6].ToString("d2") + ":" + datetime[8].ToString("d2") + ":" + datetime[10].ToString("d2") +
        //                   ":" + datetime[12].ToString("d2");
        //    }

        //    public string GetCode(byte codeByte, byte phaseByte)
        //    {

        //        string group = (0 == (codeByte & 0x80)) ? "основная" : "резерв.";
        //        string phase = "";
        //        phase += (0 == (phaseByte & 0x08)) ? " " : "_";
        //        phase += (0 == (phaseByte & 0x04)) ? " " : "A";
        //        phase += (0 == (phaseByte & 0x02)) ? " " : "B";
        //        phase += (0 == (phaseByte & 0x01)) ? " " : "C";

        //        byte code = (byte)(codeByte & 0x7F);
        //        try
        //        {
        //            return _codeDictionary[code] + " " + group + " " + phase;
        //        }
        //        catch (KeyNotFoundException)
        //        {
        //            return "";
        //        }

        //    }

        //    public string GetTypeValue(ushort damageWord, byte typeByte)
        //    {
        //        double damageValue = Measuring.GetI(damageWord, _device.TT, true);
        //        try
        //        {
        //            return _typeDictionary[typeByte] + String.Format(" = {0:F2}", damageValue);
        //        }
        //        catch (KeyNotFoundException)
        //        {
        //            return "";
        //        }
        //    }


        //    private AlarmRecord CreateMessage(byte[] buffer)
        //    {                
        //        AlarmRecord rec = new AlarmRecord();
        //       // rec.msg = _msgDictionary[buffer[0]];
        //        rec.time = buffer[6].ToString("d2") + "-" + buffer[4].ToString("d2") + "-" + buffer[2].ToString("d2") + " " +
        //                   buffer[8].ToString("d2") + ":" + buffer[10].ToString("d2") + ":" + buffer[12].ToString("d2") + ":" + buffer[14].ToString("d2");
        //        try
        //        {
        //            string group = (0 == (buffer[16] & 0x80)) ? "основная" : "резерв.";
        //            string phase = "";
        //            phase += (0 == (buffer[18] & 0x08)) ? " " : "_";
        //            phase += (0 == (buffer[18] & 0x04)) ? " " : "A";
        //            phase += (0 == (buffer[18] & 0x02)) ? " " : "B";
        //            phase += (0 == (buffer[18] & 0x01)) ? " " : "C";

        //            byte codeByte = (byte)(buffer[16] & 0x7F);
        //            rec.code = _codeDictionary[codeByte] + " "  + group + " " + phase;
        //            double damageValue = Measuring.GetI(Common.TOWORD(buffer[13], buffer[12]), _device.TT, true);
        //            rec.type_value = _typeDictionary[buffer[19]] + String.Format(" = {0:F2}",damageValue);
        //        }
        //        catch (KeyNotFoundException)
        //        { }
               
        //        double Ia = Measuring.GetI(Common.TOWORD(buffer[23],buffer[22]),_device.TT,true);
        //        double Ib = Measuring.GetI(Common.TOWORD(buffer[25],buffer[24]),_device.TT,true);
        //        double Ic = Measuring.GetI(Common.TOWORD(buffer[27],buffer[26]),_device.TT,true);
        //        double I0 = Measuring.GetI(Common.TOWORD(buffer[29],buffer[28]),_device.TT,true);
        //        double I1 = Measuring.GetI(Common.TOWORD(buffer[31],buffer[30]),_device.TT,true);
        //        double I2 = Measuring.GetI(Common.TOWORD(buffer[33],buffer[32]),_device.TT,true);
        //        double In = Measuring.GetI(Common.TOWORD(buffer[35],buffer[34]),_device.TTNP,false);
        //        double Ig = Measuring.GetI(Common.TOWORD(buffer[37],buffer[36]),_device.TTNP,false);
        //        double F  = Measuring.GetF(Common.TOWORD(buffer[39], buffer[38]));
        //        double Uab = Measuring.GetU(Common.TOWORD(buffer[41], buffer[40]), _device.TN);
        //        double Ubc = Measuring.GetU(Common.TOWORD(buffer[43], buffer[42]), _device.TN);
        //        double Uca = Measuring.GetU(Common.TOWORD(buffer[45], buffer[44]), _device.TN);
        //        double U0  = Measuring.GetU(Common.TOWORD(buffer[47], buffer[46]), _device.TN);
        //        double U1  = Measuring.GetU(Common.TOWORD(buffer[49], buffer[48]), _device.TN);
        //        double U2  = Measuring.GetU(Common.TOWORD(buffer[51], buffer[50]), _device.TN);
        //        double Un  = Measuring.GetU(Common.TOWORD(buffer[53], buffer[52]), _device.TNNP);
                
        //        rec.Ia = String.Format("{0:F2}", Ia);
        //        rec.Ib = String.Format("{0:F2}", Ib);
        //        rec.Ic = String.Format("{0:F2}", Ic);
        //        rec.I0 = String.Format("{0:F2}", I0);
        //        rec.I1 = String.Format("{0:F2}", I1);
        //        rec.I2 = String.Format("{0:F2}", I2);
        //        rec.In = String.Format("{0:F2}", In);
        //        rec.Ig = String.Format("{0:F2}", Ig);
        //        rec.F =  String.Format("{0:F2}", F);
        //        rec.Uab = String.Format("{0:F2}", Uab);
        //        rec.Ubc = String.Format("{0:F2}", Ubc);
        //        rec.Uca = String.Format("{0:F2}", Uca);
        //        rec.U0  = String.Format("{0:F2}", U0);
        //        rec.U1  = String.Format("{0:F2}", U1);
        //        rec.U2  = String.Format("{0:F2}", U2);
        //        rec.Un  = String.Format("{0:F2}", Un);
        //        byte[] b1 = new byte[]{ buffer[54] };
        //        byte[] b2 = new byte[] { buffer[55] };
        //        rec.inSignals1 = Common.BitsToString(new BitArray(b1));
        //        rec.inSignals2 = Common.BitsToString(new BitArray(b2));
        //        return rec;
        //    }

          

        //}
        #endregion
        
        #region Поля
        private slot _diagnostic = new slot(0x1800, 0x1900);
        private slot _inputSignals = new slot(0x1000, 0x103C);
        private slot _outputSignals = new slot(0x1200, 0x1270);
        private slot _externalDefenses = new slot(0x1050,0x1080);
        private slot _logic = new slot(0x3F00, 0x3F03);//(0x1000, 0x103C);

        private slot[] _systemJournal = new slot[SYSTEMJOURNAL_RECORD_CNT];
        private slot[] _alarmJournal = new slot[ALARMJOURNAL_RECORD_CNT];
        private bool _stopAlarmJournal = false;
        private bool _stopSystemJournal = false;
        private CSystemJournal _systemJournalRecords = new CSystemJournal();
        private CAlarmJournal _alarmJournalRecords;
        private StringCollection _outputSignalsList = new StringCollection();
        private StringCollection _releList = new StringCollection();
        private COutputRele _outputRele = new COutputRele();
        #endregion

        #region События 
        public event Handler DiagnosticLoadOk;
        public event Handler DiagnosticLoadFail;
        public event Handler DateTimeLoadOk;
        public event Handler DateTimeLoadFail;
        public event Handler InputSignalsLoadOK;
        public event Handler InputSignalsLoadFail;
        public event Handler InputSignalsSaveOK;
        public event Handler InputSignalsSaveFail;
        public event Handler ExternalDefensesLoadOK;
        public event Handler ExternalDefensesLoadFail;
        public event Handler ExternalDefensesSaveOK;
        public event Handler ExternalDefensesSaveFail;
        public event Handler TokDefensesLoadOK;
        public event Handler TokDefensesLoadFail;
        public event Handler TokDefensesSaveOK;
        public event Handler TokDefensesSaveFail;
        public event Handler LogicLoadOK;
        public event Handler LogicLoadFail;


        public event Handler AnalogSignalsLoadOK;
        public event Handler AnalogSignalsLoadFail;
        public event Handler SystemJournalLoadOk;
        public event IndexHandler SystemJournalRecordLoadOk;
        public event IndexHandler SystemJournalRecordLoadFail;
        public event Handler AlarmJournalLoadOk;
        public event IndexHandler AlarmJournalRecordLoadOk;
        public event IndexHandler AlarmJournalRecordLoadFail;
        public event Handler OutputSignalsLoadOK;
        public event Handler OutputSignalsLoadFail;
        public event Handler OutputSignalsSaveOK;
        public event Handler OutputSignalsSaveFail;
        
        #endregion

        #region Конструкторы инициализация
        public MR74X() { Init(); }

        public MR74X(Modbus mb)
        {
            Init();
            MB = mb;
        }

        [XmlIgnore]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        public override Modbus MB
        {
            get
            {
                return mb;
            }
            set
            {
                mb = value;
                if (null != mb)
                {
                    mb.CompleteExchange += new BEMN.MBServer.CompleteExchangeHandler(mb_CompleteExchange);
                }
            }
        }
          

        private void Init()
        {
            HaveVersion = true;
            CreateLogHash();
            _alarmJournalRecords = new CAlarmJournal(this);
            ushort start = 0x2000;
            ushort size = 0x8;
            for (int i = 0; i < SYSTEMJOURNAL_RECORD_CNT; i++)
            {
                _systemJournal[i] = new slot((ushort)start,(ushort)(start + size));
                start += 0x10;
            }
            start = 0x2800;
            size = 0x1C;
            for (int i = 0; i < ALARMJOURNAL_RECORD_CNT; i++)
            {
                _alarmJournal[i] = new slot(start, (ushort)(start + size));
                start += 0x40;
            }
            _oscilloscope = new Oscilloscope(this);
            _oscilloscope.Initialize();

            this.DeviceNumberChanged += new DeviceNumberChangedHandler(MR700_DeviceNumberChanged);
        }

        void MR700_DeviceNumberChanged(object sender, byte oldNumber, byte newNumber)
        {
            CreateLogHash();
        }
        #endregion

        #region Выходные реле
        public class COutputRele : ICollection
        {
            public const int LENGTH = 16;
            public const int COUNT = 8;

            public COutputRele()
            {
                SetBuffer(new ushort[LENGTH]);
            }
            private List<OutputReleItem> _releList = new List<OutputReleItem>(COUNT);

            [DisplayName("Количество")]
            public int Count
            {
                get
                {
                    return _releList.Count;
                }
            }

            public void SetBuffer(ushort[] values)
            {
                _releList.Clear();
                if (values.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Rele values", (object)LENGTH, "Output rele length must be 16");
                }
                for (int i = 0; i < LENGTH; i += 2)
                {
                    _releList.Add(new OutputReleItem(values[i], values[i + 1]));
                }
            }
            
            public ushort[] ToUshort()
            {
                ushort[] ret = new ushort[LENGTH];
                for (int i = 0; i < COUNT; i++)
                {
                    ret[i * 2] = _releList[i].Value[0];
                    ret[i * 2 + 1] = _releList[i].Value[1];
                }
                return ret;
            }

            public OutputReleItem this[int i]
            {
                get
                {
                    return _releList[i];
                }
                set
                {
                    _releList[i] = value;
                }
            }

            #region ICollection Members

            public void Add(OutputReleItem item)
            {
                _releList.Add(item);
            }
 
            public void CopyTo(Array array, int index)
            {

            }
            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }
            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _releList.GetEnumerator();
            }

            #endregion
        };

        public class OutputReleItem
        {

            private ushort _hiWord;
            private ushort _loWord;

            public override string ToString()
            {
                return "Выходное реле";
            }

            [XmlAttribute("Значения")]
            [DisplayName("Значения")]
            [Description("Данные в словном виде")]
            [System.ComponentModel.Category("Словный вид")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            public ushort[] Value
            {
                get
                {
                    return new ushort[] { _hiWord, _loWord };
                }
                set
                {
                    _hiWord = value[0];
                    _loWord = value[1];
                }
            }

            public OutputReleItem() { }

            public OutputReleItem(ushort hiWord, ushort loWord)
            {
                SetItem(hiWord, loWord);
            }

            public void SetItem(ushort hiWord, ushort loWord)
            {
                _hiWord = hiWord;
                _loWord = loWord;
            }

            [XmlIgnore]
            [Browsable(false)]
            public bool IsBlinker
            {
                get { return 0 != (_hiWord & 256); }
                set
                {
                    if (value)
                    {
                        _hiWord = (ushort)(_hiWord | 256);
                    }
                    else
                    {
                        _hiWord = (ushort)(_hiWord | 256);
                        _hiWord -= 256;
                    }
                }
            }

            [XmlAttribute("Тип")]
            [DisplayName("Тип")]
            [Description("Тип сигнала реле")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BlinkerTypeConverter))]
            public String Type
            {
                get
                {
                    string ret = "";
                    ret = IsBlinker ? "Блинкер" : "Повторитель";
                    return ret;
                }
                set
                {
                    if ("Блинкер" == value)
                    {
                        IsBlinker = true;
                    }
                    else if ("Повторитель" == value)
                    {
                        IsBlinker = false;
                    }
                    else
                    {
                        throw new ArgumentException("Rele type undefined", "Type");
                    }
                }
            }

            [XmlIgnore]
            [Browsable(false)]
            public byte SignalByte
            {
                get { return Common.LOBYTE(_hiWord); }
                set { _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), value); }
            }

            [XmlAttribute("Сигнал")]
            [DisplayName("Вид")]
            [Description("Вид сигнала реле")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(AllSignalsTypeConverter))]
            public string SignalString
            {
                get {
                    string ret = Strings.All[Strings.All.Count - 1];
                    if (Common.LOBYTE(_hiWord) <= Strings.All.Count)
                    {
                        ret = Strings.All[Common.LOBYTE(_hiWord)];
                    }
                    return ret;
                }
                set
                {
                    byte index = (byte)Strings.All.IndexOf(value);
                    _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), index);
                }
            }

            [XmlAttribute("Импульс")]
            [DisplayName("Импульс")]
            [Description("Длительность импульса реле")]
            [System.ComponentModel.Category("Текстовый вид")]
            public ulong ImpulseUlong
            {
                get { return Measuring.GetTime(_loWord); }
                set { _loWord = Measuring.SetTime(value); }
            }

            [XmlIgnore]
            [Browsable(false)]
            public ushort ImpulseUshort
            {
                get { return _loWord; }
                set { _loWord = value; }
            }

        };

        [TestFixture]
        public class TestCoutputRele
        {
            [Test]
            public void COutputReleItem_IsRepeater()
            {
                OutputReleItem item = new OutputReleItem();
                item.SetItem(0xFFFF, 0xFFFF);
                Assert.IsTrue(item.IsBlinker);
                item.IsBlinker = false;
                Assert.AreEqual(item.Value[0], 0xFEFF);
                item.IsBlinker = true;
                Assert.AreEqual(item.Value[0], 0xFFFF);
                item.SetItem(0, 0);
                Assert.IsFalse(item.IsBlinker);
                item.IsBlinker = true;
                Assert.IsTrue(item.IsBlinker);
                Assert.AreEqual(item.Value[0], 256);

            }

            [Test]
            public void Bits()
            {
                Assert.AreEqual(0x1 | 0x2, 0x3);
                Assert.AreEqual(0xF7, 0xFF & ~0x8);
            }

            [Test]
            public void COutputReleItem_Signal()
            {
                OutputReleItem item = new OutputReleItem();
                item.SetItem(0xFFAA, 0);
                Assert.AreEqual(item.SignalByte, 0xAA);
                item.SetItem(0x0011, 0);
                Assert.AreEqual(item.SignalByte, 0x11);
                item.SignalByte = 2;
                Assert.AreEqual(item.Value[0], 0x02);

                item.SetItem(0xFF02, 0);
                Assert.AreEqual(item.SignalString, "Откл. выкл.");
                item.SetItem(0xFFEF, 0);
                Assert.AreEqual(item.SignalString, "U0>> Возврат Инв.");
                item.SignalString = "Откл. выкл.";
                Assert.AreEqual(item.SignalByte, 2);
                Assert.AreEqual(item.SignalString, "Откл. выкл.");
            }
        };

        
        [DisplayName("Выходные реле")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("Выходные сигналы")]
        [XmlElement("Выходные_реле")]       
        public COutputRele OutputRele
        {
            get
            {
                return _outputRele;
            }
        }

        #endregion

        #region Выходные индикаторы
        public class COutputIndicator : ICollection
        {
            #region ICollection Members

            public void Add(OutputIndicatorItem item)
            {
                _indList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }
            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _indList.GetEnumerator();
            }

            #endregion

            [DisplayName("Количество")]
            public int Count
            {
                get
                {
                    return _indList.Count;
                }
            }


            public const int LENGTH = 16;
            public const int COUNT = 8;

            private List<OutputIndicatorItem> _indList = new List<OutputIndicatorItem>(COUNT);

            public COutputIndicator()
            {
                SetBuffer(new ushort[LENGTH]);
            }

            public void SetBuffer(ushort[] values)
            {
                _indList.Clear();
                if (values.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Output Indicator values", (object)LENGTH, "Output indicator length must be 16");
                }
                for (int i = 0; i < LENGTH; i += 2)
                {
                    _indList.Add(new OutputIndicatorItem(values[i], values[i + 1]));
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] ret = new ushort[LENGTH];
                for (int i = 0; i < COUNT; i++)
                {
                    ret[i * 2] = _indList[i].Value[0];
                    ret[i * 2 + 1] = _indList[i].Value[1];
                }
                return ret;
            }

            public OutputIndicatorItem this[int i]
            {
                get
                {
                    return _indList[i];
                }
                set
                {
                    _indList[i] = value;
                }
            }

        };

        public class OutputIndicatorItem
        {
            private ushort _hiWord;
            private ushort _loWord;

            public override string ToString()
            {
                return "Выходной индикатор";
            }

            [XmlAttribute("Значения")]
            [DisplayName("Значения")]
            [Description("Данные в словном виде")]
            [System.ComponentModel.Category("Словный вид")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            public ushort[] Value
            {
                get
                {
                    return new ushort[] { _hiWord, _loWord };
                }
            }

            //public OutputIndicatorItem() { }

            //public OutputIndicatorItem(ushort hiWord, ushort loWord)
            //{
            //    SetItem(hiWord, loWord);
            //}

            //public void SetItem(ushort hiWord, ushort loWord)
            //{
            //    _hiWord = hiWord;
            //    _loWord = loWord;
            //}
            //[XmlIgnore]
            //[Browsable(false)]
            //public bool IsBlinker
            //{
            //    get { return 0 != (_hiWord & 256); }
            //    set
            //    {
            //        if (value)
            //        {
            //            _hiWord = (ushort)(_hiWord | 256);
            //        }
            //        else
            //        {
            //            _hiWord = (ushort)(_hiWord | 256);
            //            _hiWord -= 256;
            //        }
            //    }
            //}

            //[XmlAttribute("Тип")]
            //[DisplayName("Тип")]
            //[Description("Тип сигнала индикатора")]
            //[System.ComponentModel.Category("Текстовый вид")]
            //[TypeConverter(typeof(BlinkerTypeConverter))]
            //public String Type
            //{
            //    get
            //    {
            //        string ret = "";
            //        ret = IsBlinker ? "Блинкер" : "Повторитель";
            //        return ret;
            //    }
            //    set
            //    {
            //        if ("Блинкер" == value)
            //        {
            //            IsBlinker = true;
            //        }
            //        else if ("Повторитель" == value)
            //        {
            //            IsBlinker = false;
            //        }
            //        else
            //        {
            //            throw new ArgumentException("Rele type undefined", "Type");
            //        }
            //    }
            //}

            public OutputIndicatorItem() { }

            public OutputIndicatorItem(ushort hiWord, ushort loWord)
            {
                SetItem(hiWord, loWord);
            }

            public void SetItem(ushort hiWord, ushort loWord)
            {
                _hiWord = hiWord;
                _loWord = loWord;
            }
            [XmlIgnore]
            [Browsable(false)]
            public bool IsBlinker
            {
                get
                {
                    return 0 != (_hiWord & 0x8000);
                }
                set
                {
                    if (value)
                    {
                        _hiWord = (ushort)(_hiWord | 0x8000);
                    }
                    else
                    {
                        _hiWord = (ushort)(_hiWord | 0x8000);
                        _hiWord -= 0x8000;
                    }
                }
            }

            [XmlAttribute("Тип")]
            [DisplayName("Тип")]
            [Description("Тип сигнала индикатора")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BlinkerTypeConverter))]
            public String Type
            {
                get
                {
                    string ret = "";
                    ret = IsBlinker ? "Блинкер" : "Повторитель";
                    return ret;
                }
                set
                {
                    if ("Блинкер" == value)
                    {
                        IsBlinker = true;
                    }
                    else if ("Повторитель" == value)
                    {
                        IsBlinker = false;
                    }
                    else
                    {
                        throw new ArgumentException("Rele type undefined", "Type");
                    }
                }
            }

            [XmlIgnore]
            [Browsable(false)]
            public byte SignalByte
            {
                get { return Common.LOBYTE(_hiWord); }
                set { _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), value); }
            }

            [XmlAttribute("Сигнал")]
            [DisplayName("Вид")]
            [Description("Вид сигнала индикатора")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(AllSignalsTypeConverter))]
            public string SignalString
            {
                get
                {
                    string ret = Strings.All[Strings.All.Count - 1];
                    if (Common.LOBYTE(_hiWord) <= Strings.All.Count)
                    {
                        //ret = Strings.All[Common.LOBYTE(_hiWord)];
                        ret = Strings.All[_hiWord];
                    }
                    return ret;
                }
                set
                {
                    //  byte index = (byte)Strings.All.IndexOf(value) ;
                    //_hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), index);
                    //_hiWord = (ushort)(((ushort)Strings.All.IndexOf(value) & 0x7fff) | (_hiWord & 0x8000));
                  _hiWord = (ushort)((ushort)Strings.All.IndexOf(value)& 0x7fff) ;

                   // _hiWord = (ushort)Strings.All.IndexOf(value);
                }
            }

            [XmlAttribute("Индикация")]
            [DisplayName("Сигнал индикации")]
            [Description("Сигнал 'сброс индикации'")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool Indication
            {
                get
                {
                    return 0 != (_loWord & 0x1);
                }
                set
                {
                    _loWord = value ? (ushort)(_loWord | 0x1) : (ushort)(_loWord & ~0x1);
                }
            }
            [XmlAttribute("Авария")]
            [DisplayName("Ж. аварий")]
            [Description("Сигнал 'сброс журнала аварий'")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool Alarm
            {
                get
                {
                    return 0 != (_loWord & 0x2);
                }
                set
                {
                    _loWord = value ? (ushort)(_loWord | 0x2) : (ushort)(_loWord & ~0x2);
                }
            }
            [XmlAttribute("Система")]
            [DisplayName("Ж. системы")]
            [Description("Сигнал 'сброс журнала системы'")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool System
            {
                get
                {
                    return 0 != (_loWord & 0x4);
                }
                set
                {
                    _loWord = value ? (ushort)(_loWord | 0x4) : (ushort)(_loWord & ~0x4);
                }
            }
        };

        private COutputIndicator _outputIndicator = new COutputIndicator();

        [XmlElement("Выходные_индикаторы")]
        [DisplayName("Выходные индикаторы")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("Выходные сигналы")]
        public COutputIndicator OutputIndicator
        {
            get
            {
                return _outputIndicator;
            }
        }

        #endregion

        #region Аналоговые сигналы

        private slot _analog = new slot(0x1900, 0x1918);

        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double In
        {
            get
            {
                return Measuring.GetI(_analog.Value[0], TT, true);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double Ia
        {
            get
            {
                return Measuring.GetI(_analog.Value[1], TT, true);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double Ib
        {
            get
            {
                return Measuring.GetI(_analog.Value[2], TT, true);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double Ic
        {
            get
            {
                return Measuring.GetI(_analog.Value[3], TT, true);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double I0
        {
            get
            {
                return Measuring.GetI(_analog.Value[4], TT, true);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double I1
        {
            get
            {
                return Measuring.GetI(_analog.Value[5], TT, true);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double I2
        {
            get
            {
                return Measuring.GetI(_analog.Value[6], TT, true);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double Ig
        {
            get
            {
                return Measuring.GetI(_analog.Value[7], TTNP, false);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double Un
        {
            get
            {
                return Measuring.GetU(_analog.Value[8], TNNP);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double Ua
        {
            get
            {
                return Measuring.GetU(_analog.Value[9], TN);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double Ub
        {
            get
            {
                return Measuring.GetU(_analog.Value[10], TN);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double Uc
        {
            get
            {
                return Measuring.GetU(_analog.Value[11], TN);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double Uab
        {
            get
            {
                return Measuring.GetU(_analog.Value[12], TN);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double Ubc
        {
            get
            {
                return Measuring.GetU(_analog.Value[13], TN);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double Uca
        {
            get
            {
                return Measuring.GetU(_analog.Value[14], TN);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double U0
        {
            get
            {
                return Measuring.GetU(_analog.Value[15], TN);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double U1
        {
            get
            {
                return Measuring.GetU(_analog.Value[16], TN);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double U2
        {
            get
            {
                return Measuring.GetU(_analog.Value[17], TN);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double F
        {
            get
            {
                return Measuring.GetF(_analog.Value[18]);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public double Q
        {
            get
            {
                return Measuring.GetConstraint(_analog.Value[19], ConstraintKoefficient.K_25600);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public int Npusk
        {
            get
            {
                return (int)Common.HIBYTE(_analog.Value[20] & 255);
            }
        }
        [System.ComponentModel.Category("Аналоговые сигналы")]
        public int Ngor
        {
            get
            {
                return (int)Common.HIBYTE(_analog.Value[21] & 255);
            }
        }

        #endregion

        #region Дискретные сигналы
        [XmlIgnore]
        [DisplayName("Управляющие сигналы")]
        [Description("Управляющие сигналы СДТУ")]
        [System.ComponentModel.Category("Дискретные сигналы")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray ManageSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[0]), Common.HIBYTE(_diagnostic.Value[0]) });
            }
        }
        [XmlIgnore]
        [DisplayName("Дополнительные сигналы")]
        [System.ComponentModel.Category("Дискретные сигналы")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray AdditionalSignals
        {
            get
            {
                BitArray temp = new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[2]) });
                BitArray ret = new BitArray(4);
                for (int i = 0; i < 4; i++)
                {
                    ret[i] = temp[i + 4];
                }
                return ret;
            }
        }
        [XmlIgnore]
        [DisplayName("Индикаторы")]
        [System.ComponentModel.Category("Дискретные сигналы")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray Indicators
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(_diagnostic.Value[2]) });
            }
        }
        [XmlIgnore]
        [DisplayName("Входные сигналы")]
        [System.ComponentModel.Category("Дискретные сигналы")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray InputSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[0x9]),
                                                 Common.HIBYTE(_diagnostic.Value[0x9]),
                                                 Common.LOBYTE(_diagnostic.Value[0xA])});
            }
        }
        [XmlIgnore]
        [DisplayName("Реле")]
        [System.ComponentModel.Category("Дискретные сигналы")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray Rele
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[3]) });
            }
        }
        [XmlIgnore]
        [DisplayName("Выходные сигналы")]
        [System.ComponentModel.Category("Дискретные сигналы")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray OutputSignals
        {
            get
            {
                //return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[0x9]),
                //                                 Common.HIBYTE(_diagnostic.Value[0x9]),
                //                                 Common.LOBYTE(_diagnostic.Value[0xA])});
                return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[0xE]) });
            }
        }
        [XmlIgnore]
        [DisplayName("Сигналы пределов")]
        [System.ComponentModel.Category("Дискретные сигналы")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray LimitSignals
        {
            get
            {
                return new BitArray(new byte[]{Common.HIBYTE(_diagnostic.Value[0xA]),
                                               Common.LOBYTE(_diagnostic.Value[0xB]),
                                               Common.HIBYTE(_diagnostic.Value[0xB]),
                                               Common.LOBYTE(_diagnostic.Value[0xC]),
                                               Common.HIBYTE(_diagnostic.Value[0xC]),
                                               Common.LOBYTE(_diagnostic.Value[0xD]), 
                                               Common.HIBYTE(_diagnostic.Value[0xD])});
            }
        }
        [XmlIgnore]
        [DisplayName("Состояние неисправности")]
        [System.ComponentModel.Category("Дискретные сигналы")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray FaultState
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[4]) });
            }
        }
        [XmlIgnore]
        [DisplayName("Сигналы неисправности")]
        [System.ComponentModel.Category("Дискретные сигналы")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray FaultSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[5]), Common.HIBYTE(_diagnostic.Value[5]) });
            }
        }
        [XmlIgnore]
        [DisplayName("Автоматика")]
        [System.ComponentModel.Category("Дискретные сигналы")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray Automation
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(_diagnostic.Value[8]) });
            }
        }
        #endregion
        
        #region Параметры тока и напряжений
        [XmlElement("ТТ")]
        [DisplayName("ТТ")]
        [Description("Первичный ток трансформатора")]
        [System.ComponentModel.Category("Параметры тока и напряжений")]
        public ushort TT
        {
            get
            {
                return _inputSignals.Value[1];
            }
            set
            {
                _inputSignals.Value[1] = value;
            }
        }
        [XmlElement("ТТНП")]
        [DisplayName("ТТПН")]
        [Description("Первичный ток трансформатора нулевой последовательности")]
        [System.ComponentModel.Category("Параметры тока и напряжений")]
        public ushort TTNP
        {
            get
            {
                return _inputSignals.Value[2];
            }
            set
            {
                _inputSignals.Value[2] = value;
            }
        }
        [XmlElement("Тип_ОМП")]
        [DisplayName("Тип ОМП")]
        [System.ComponentModel.Category("Определение места повреждения")]
        [TypeConverter(typeof(TN_TypeConverter))]
        public string OMP_Type
        {
            get
            {
                return Strings.ModesLight[_inputSignals.Value[13]];
            }
            set
            {
                _inputSignals.Value[13] = (ushort)Strings.ModesLight.IndexOf(value);
            }
        }
        [XmlElement("ХУД")]
        [DisplayName("ХУД")]
        [System.ComponentModel.Category("Определение места повреждения")]
        public double HUD
        {
            get
            {
                return _inputSignals.Value[14];
            }
            set
            {
                _inputSignals.Value[14] = (ushort)(value * 1000);
            }
        }
        [XmlElement("Тип_ТН")]
        [DisplayName("Тип ТН")]
        [System.ComponentModel.Category("Параметры тока и напряжений")]
        [TypeConverter(typeof(TN_TypeConverter))]
        public string TN_Type
        {
            get
            {
                return Strings.TN_Type[_inputSignals.Value[8]];
            }
            set
            {
                _inputSignals.Value[8] = (ushort)Strings.TN_Type.IndexOf(value);
            }
        }
        [XmlElement("ТН")]
        [DisplayName("ТН")]
        [Description("Коэффициент ТН")]
        [System.ComponentModel.Category("Параметры тока и напряжений")]
        public double TN
        {
            get
            {
                return Measuring.GetConstraint(_inputSignals.Value[9], ConstraintKoefficient.K_25600);
            }
            set
            {
                _inputSignals.Value[9] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
            }
        }
        [XmlElement("Неисправность_ТН")]
        [DisplayName("Неисправность ТН")]
        [Description("Внешняя неисправность ТН")]
        [System.ComponentModel.Category("Параметры тока и напряжений")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string TN_Dispepair
        {
            get
            {
                try
                {
                    return Strings.Logic[_inputSignals.Value[10]];
                }
                catch (ArgumentOutOfRangeException)
                {
                    return Strings.Logic[Strings.Logic.Count - 1];
                }
                
            }
            set
            {
                _inputSignals.Value[10] = (ushort)Strings.Logic.IndexOf(value);
            }
        }
        [XmlElement("Неисправность_ТННП")]
        [DisplayName("Неисправность ТННП")]
        [Description("Внешняя неисправность ТННП")]
        [System.ComponentModel.Category("Параметры тока и напряжений")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string TNNP_Dispepair
        {
            get
            {
                try
                {
                    return Strings.Logic[_inputSignals.Value[12]];
                }
                catch (ArgumentOutOfRangeException)
                {
                    return Strings.Logic[Strings.Logic.Count - 1];
                }
                
            }
            set
            {
                _inputSignals.Value[12] = (ushort)Strings.Logic.IndexOf(value);
            }
        }
        [XmlElement("ТННП")]
        [DisplayName("ТННП")]
        [Description("Коэффициент ТННП")]
        [System.ComponentModel.Category("Параметры тока и напряжений")]
        public double TNNP
        {
            get
            {
                return Measuring.GetConstraint(_inputSignals.Value[0xB],ConstraintKoefficient.K_25600);
            }
            set
            {
                _inputSignals.Value[0xB] = Measuring.SetConstraint(value,ConstraintKoefficient.K_25600);
            }
        }
        [XmlElement("Максимальный_ток")]
        [DisplayName("Максимальный ток")]
        [Description("Максимальный ток нагрузки")]
        [System.ComponentModel.Category("Параметры тока и напряжений")]
        public double MaxTok
        {
            get
            {
                return Measuring.GetConstraint(_inputSignals.Value[3],ConstraintKoefficient.K_4000);
            }
            set
            {
                _inputSignals.Value[3] = Measuring.SetConstraint(value,ConstraintKoefficient.K_4000);
            }
        }
        #endregion

        #region Входные сигналы
        [XmlIgnore]
        [DisplayName("Сигналы неисправности")]
        [Description("Сигналы неисправности")]
        [System.ComponentModel.Category("Входные сигналы")]
        [Editor(typeof(BitsEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray DispepairSignal
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_inputSignals.Value[0x1D]) });
            }
            set
            {
                _inputSignals.Value[0x1D] = Common.BitsToUshort(value);
            }
        }

        [XmlElement("Импульс_неисправности")]
        [DisplayName("Импульс неисправности")]
        [System.ComponentModel.Category("Входные сигналы")]
        [Editor(typeof(BitsEditor), typeof(UITypeEditor))]
        public ulong DispepairImpulse
        {
            get
            {
                return Measuring.GetTime(_inputSignals.Value[0x1E]);
            }
            set
            {
                _inputSignals.Value[0x1E] = Measuring.SetTime(value);
            }
        }



        [XmlElement("Ключ_включить")]
        [DisplayName("Ключ включить")]
        [Description("Номер входа ключа включить")]
        [System.ComponentModel.Category("Входные сигналы")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string KeyOn
        {
            get
            {
                ushort index = _inputSignals.Value[0x10];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }
                
                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[0x10] = (ushort)(Strings.Logic.IndexOf(value));
            }

        }
        [XmlElement("Ключ_выключить")]
        [DisplayName("Ключ выключить")]
        [Description("Номер входа ключа выключить")]
        [System.ComponentModel.Category("Входные сигналы")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string KeyOff
        {
            get
            {
                ushort index = _inputSignals.Value[0x11];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }

                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[0x11] = (ushort)(Strings.Logic.IndexOf(value));
            }


        }
        [XmlElement("Внешние_включить")]
        [DisplayName("Внешние включить")]
        [Description("Номер входа внешние включить")]
        [System.ComponentModel.Category("Входные сигналы")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string ExternalOn
        {
            get
            {
                ushort index = _inputSignals.Value[0x12];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }

                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[0x12] = (ushort)(Strings.Logic.IndexOf(value));
            }


        }
        [XmlElement("Внешние_выключить")]
        [DisplayName("Внешние выключить")]
        [Description("Номер входа внешние выключить")]
        [System.ComponentModel.Category("Входные сигналы")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string ExternalOff
        {          
            get
            {
                ushort index = _inputSignals.Value[0x13];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }
                
                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[0x13] = (ushort)(Strings.Logic.IndexOf(value));
            }

        }
        [XmlElement("Переключение_уставок")]
        [DisplayName("Переключение уставок")]
        [Description("Внешний сигнал группы уставок")]
        [System.ComponentModel.Category("Входные сигналы")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string ConstraintGroup
        {
            get
            {
                ushort index = _inputSignals.Value[0x15];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }

                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[0x15] = (ushort)(Strings.Logic.IndexOf(value));
            }


        }
        //[XmlElement("Пуск_осциллографа")]
        //[DisplayName("Пуск осциллографа")]
        //[Description("Внешний сигнал пуск осциллографа")]
        //[System.ComponentModel.Category("Входные сигналы")]
        //public string OscilloscopeStart
        //{
        //    get
        //    {
        //        ushort index = _inputSignals.Value[0x16];

        //        if (Strings.Logic.Count <= index)
        //        {
        //            index = (ushort)(Strings.Logic.Count - 1);
        //        }

        //        return Strings.Logic[(int)index];
        //    }
        //    set
        //    {
        //        _inputSignals.Value[0x16] = (ushort)(Strings.Logic.IndexOf(value));
        //    }


        //}
        [XmlElement("Сброс_сигнализации")]
        [DisplayName("Сброс сигнализации")]
        [Description("Внешний сигнал сброс сигнализации")]
        [System.ComponentModel.Category("Входные сигналы")]
        public string SignalizationReset
        {
            get
            {
                ushort index = _inputSignals.Value[0x14];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }

                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[0x14] = (ushort)(Strings.Logic.IndexOf(value));
            }
        }

        #endregion

        #region Сигналы управления

        [XmlElement("Сигнал_от_кнопок")]
        [DisplayName("Сигнал от кнопок")]
        [System.ComponentModel.Category("Входные сигналы")]
        [Description("Управляющий сигнал от кнопок")]     
        [TypeConverter(typeof(ForbiddenTypeConverter))]
        public string ManageSignalButton
        {
            get
            {

                string ret = Common.GetBit(_inputSignals.Value[0x3B], 0) ? Strings.Forbidden[0] : Strings.Forbidden[1];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Forbidden[0]) ? true : false;
                _inputSignals.Value[0x3B] = Common.SetBit(_inputSignals.Value[0x3B],0, bit);
            }
        }
        [XmlElement("Сигнал_от_ключа")]
        [DisplayName("Сигнал от ключа")]
        [System.ComponentModel.Category("Входные сигналы")]
        [Description("Управляющий сигнал от ключа")]
        [TypeConverter(typeof(ControlTypeConverter))]
        public string ManageSignalKey
        {
            get
            {

                string ret = Common.GetBit(_inputSignals.Value[0x3B], 1) ? Strings.Control[0] : Strings.Control[1];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Control[0]) ? true : false;
                _inputSignals.Value[0x3B] = Common.SetBit(_inputSignals.Value[0x3B], 1, bit);
            }
        }
        [XmlElement("Сигнал_от_внешнего")]
        [DisplayName("Сигнал от внешнего")]
        [System.ComponentModel.Category("Входные сигналы")]
        [Description("Управляющий сигнал от внешнего устройства")]
        [TypeConverter(typeof(ControlTypeConverter))]
        public string ManageSignalExternal
        {
            get
            {

                string ret = Common.GetBit(_inputSignals.Value[0x3B], 2) ? Strings.Control[0] : Strings.Control[1];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Control[0]) ? true : false;
                _inputSignals.Value[0x3B] = Common.SetBit(_inputSignals.Value[0x3B], 2, bit);
            }
        }
        [XmlElement("Сигнал_от_СДТУ")]
        [DisplayName("Сигнал от СДТУ")]
        [System.ComponentModel.Category("Входные сигналы")]
        [Description("Управляющий сигнал от СДТУ")]
        [TypeConverter(typeof(ForbiddenTypeConverter))]
        public string ManageSignalSDTU
        {
            get
            {

                string ret = Common.GetBit(_inputSignals.Value[0x3B], 3) ? Strings.Forbidden[0] : Strings.Forbidden[1];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Forbidden[0]) ? true : false;
                _inputSignals.Value[0x3B] = Common.SetBit(_inputSignals.Value[0x3B], 3, bit);
            }
        }
             
        #endregion

        #region Выключатель
        [XmlElement("Выключатель_отключить")]
        [DisplayName("Выключатель - отключение")]
        [Description("Выключатель - состояние отключения")]
        [System.ComponentModel.Category("Входные сигналы")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string SwitcherOff
        {
            
           get
            {
                ushort index = _inputSignals.Value[50];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }
                
                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[50] = (ushort)(Strings.Logic.IndexOf(value));
            }


        }
        [XmlElement("Выключатель_включить")]
        [DisplayName("Выключатель - включение")]
        [System.ComponentModel.Category("Входные сигналы")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string SwitcherOn
        {
          get
            {
                ushort index = _inputSignals.Value[51];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }
                
                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[51] = (ushort)(Strings.Logic.IndexOf(value));
            }


        }
        [XmlElement("Выключатель_ошибка")]
        [DisplayName("Выключатель - неисправность")]
        [System.ComponentModel.Category("Входные сигналы")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string SwitcherError
        {          
            get
            {
                ushort index = _inputSignals.Value[52];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }
                
                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[52] = (ushort)(Strings.Logic.IndexOf(value));
            }


        }
        [XmlElement("Выключатель_блокировка")]
        [DisplayName("Выключатель - блокировка")]
        [System.ComponentModel.Category("Входные сигналы")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string SwitcherBlock
        {
            get
            {
                ushort index = _inputSignals.Value[53];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }
                
                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[53] = (ushort)(Strings.Logic.IndexOf(value));
            }

        }
        [XmlElement("Выключатель_время_УРОВ")]
        [DisplayName("Выключатель - время УРОВ")]
        [System.ComponentModel.Category("Входные сигналы")]
        public ulong SwitcherTimeUROV
        {
            get
            {
                return Measuring.GetTime(_inputSignals.Value[54]);
            }
            set
            {
                _inputSignals.Value[54] = Measuring.SetTime(value);
            }
        }
        [XmlElement("Выключатель_ток_УРОВ")]
        [DisplayName("Выключатель - ток УРОВ")]
        [System.ComponentModel.Category("Входные сигналы")]
        public double SwitcherTokUROV
        {
            get
            {
                return Measuring.GetConstraint(_inputSignals.Value[55],ConstraintKoefficient.K_4000);
            }
            set
            {
                _inputSignals.Value[55] = Measuring.SetConstraint(value,ConstraintKoefficient.K_4000);
            }

        }
        [XmlElement("Выключатель_импульс")]
        [DisplayName("Выключатель - импульс")]
        [System.ComponentModel.Category("Входные сигналы")]
        public ulong SwitcherImpulse
        {
            get
            {
                return Measuring.GetTime(_inputSignals.Value[56]);
            }
            set
            {
                _inputSignals.Value[56] = Measuring.SetTime(value);
            }
          
        }
        [XmlElement("Выключатель_длительность")]
        [DisplayName("Выключатель - длительность")]
        [Description("Выключатель -   длительность ускорения режима")]
        [System.ComponentModel.Category("Входные сигналы")]
        public ulong SwitcherDuration
        {
            get
            {
                return Measuring.GetTime(_inputSignals.Value[57]);
            }
            set
            {
                _inputSignals.Value[57] = Measuring.SetTime(value);
            }
           
        }
    
        #endregion

        #region Выходные логические сигналы
               
        //[TypeConverter(typeof(RussianCollectionEditor))]
        //[Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        //public List<BitArray> OutputLogicSignals
        //{
        //    get
        //    {
        //       List<BitArray> _outputLogicSignals = new List<BitArray>(8);
        //        for (int i = 0; i < 8; i++)
        //        {
        //            _outputLogicSignals.Add(GetOutputLogicSignals(i));
        //        }
        //        return _outputLogicSignals;
        //    }
        //    set
        //    {
        //        for (int i = 0; i < 8; i++)
        //        {
        //            SetOutputLogicSignals(i, value[i]);
        //        }
        //    }
        //}

        private const int OUTPUTLOGIC_BITS_CNT = 120;
        public BitArray GetOutputLogicSignals(int channel)
        {
            if ( channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            
            ushort[] logicBuffer = new ushort[8];
            Array.ConstrainedCopy(_outputSignals.Value,channel * 8,logicBuffer,0,8);
            BitArray buffer = new BitArray(Common.TOBYTES(logicBuffer, false));
            BitArray ret = new BitArray(OUTPUTLOGIC_BITS_CNT);
            for (int i = 0; i < ret.Count; i++)
            {
                ret[i] = buffer[i];
            }
            return ret;

        }

        public void SetOutputLogicSignals(int channel,BitArray values)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            if (120 != values.Count)
            {
                throw new ArgumentOutOfRangeException("OutputLogic bits count");
            }

            ushort[] logicBuffer = new ushort[8];
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    if (i * 16 + j < 120)
                    {
                        if (values[i * 16 + j])
                        {
                            logicBuffer[i] += (ushort)Math.Pow(2, j);
                        }
                    }
                }
            }
            Array.ConstrainedCopy(logicBuffer, 0, _outputSignals.Value, channel * 8, 8);

        }
        #endregion
                  
        #region Входные логические сигналы
        
        public LogicState[] GetInputLogicSignals(int channel)
        {
            if ( channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            LogicState[] ret = new LogicState[16];
            SetLogicStates(_inputSignals.Value[0x22 + channel * 2], true, ref ret);
            SetLogicStates(_inputSignals.Value[0x22 + channel * 2 + 1], false, ref ret);
            return ret;
        }

        public void SetInputLogicSignals(int channel,LogicState[] logicSignals)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            ushort firstWord = 0;
            ushort secondWord = 0;

            for (int i = 0; i < logicSignals.Length; i++)
            {
                if (i < 8)
                {
                    if (LogicState.Да == logicSignals[i])
                    {
                        firstWord += (ushort)Math.Pow(2, i);
                    }
                    if (LogicState.Инверс == logicSignals[i])
                    {
                        firstWord += (ushort)Math.Pow(2, i);
                        firstWord += (ushort)Math.Pow(2,i + 8);
                    }
                }
                else
                {
                    if (LogicState.Да == logicSignals[i])
                    {
                        secondWord += (ushort)Math.Pow(2, i - 8);
                    }
                    if (LogicState.Инверс == logicSignals[i])
                    {
                        secondWord += (ushort)Math.Pow(2, i - 8);
                        secondWord += (ushort)Math.Pow(2, i);
                    }
                }
            }
            _inputSignals.Value[0x22 + channel * 2] = firstWord;
            _inputSignals.Value[0x22 + channel * 2 + 1] = secondWord;
        }

        void SetLogicStates(ushort value,bool firstWord,ref  LogicState[] logicArray)
        {
            byte lowByte = Common.LOBYTE(value);
            byte hiByte = Common.HIBYTE(value);

            int start = firstWord ? 0 : 8;
            int end = firstWord ? 8 : 16;

            for (int i = start; i < end; i++)
            {
                int pow  =  firstWord ? i : i - 8;
                logicArray[i] = (byte)(Math.Pow(2, pow)) != ((lowByte) & (byte)(Math.Pow(2, pow))) ? logicArray[i] : LogicState.Да;
                //logicArray[i] = (byte)(Math.Pow(2, pow)) != ((hiByte) & (byte)(Math.Pow(2, pow))) ? logicArray[i] : LogicState.Инверс;
                logicArray[i] = IsLogicEquals(hiByte, pow) && (logicArray[i] == LogicState.Да) ? LogicState.Инверс : logicArray[i];
            }
        }
        private bool IsLogicEquals(byte hiByte, int pow)
        {
            return (byte)(Math.Pow(2, pow)) == ((hiByte) & (byte)(Math.Pow(2, pow)));
        }
        #endregion

        #region Страница автоматики
        //Поле автоматики
        private slot _automaticsPage = new slot(0x103c, 0x1050);
        private BitArray _bits = new BitArray(8);
        //События страницы автоматики
        public event Handler AutomaticsPageLoadOK;
        public event Handler AutomaticsPageLoadFail;
        public event Handler AutomaticsPageSaveOK;
        public event Handler AutomaticsPageSaveFail;

        public void LoadAutomaticsPage()
        {
            LoadSlot(DeviceNumber, _automaticsPage, "LoadAutomaticsPage" + DeviceNumber);
        }

        public void SaveAutomaticsPage()
        {
            SaveSlot(DeviceNumber, _automaticsPage, "SaveAutomaticsPage" + DeviceNumber);
        }

        [XmlElement("Конфигурация_АПВ")]
        [DisplayName("Конфигурация АПВ")]
        [System.ComponentModel.Category("Автоматика")]
        [TypeConverter(typeof(ModeTypeConverter))]
        public string APV_Cnf
        {
            get
            {
                byte index = (byte)(Common.LOBYTE(_automaticsPage.Value[0]));
                if (index >= Strings.Crat.Count)
                {
                    index = (byte)(Strings.Crat.Count - 1);
                }
                return Strings.Crat[index];
            }
            set { _automaticsPage.Value[0] = Common.TOWORD(Common.HIBYTE(_automaticsPage.Value[0]), (byte)Strings.Crat.IndexOf(value)); }
        }

        [XmlElement("Блокировка_АПВ")]
        [DisplayName("Блокировка АПВ")]
        [Description("Номер входа блокировки АПВ")]
        [System.ComponentModel.Category("Автоматика")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string APV_Blocking
        {
            get
            {
                byte index = (byte)(Common.LOBYTE(_automaticsPage.Value[1]));
                if (index >= Strings.Logic.Count)
                {
                    index = (byte)(Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set
            {
                _automaticsPage.Value[1] = (ushort)Strings.Logic.IndexOf(value);
            }
        }

        [XmlElement("Время_блокировки_АПВ")]
        [DisplayName("Время АПВ")]
        [Description("Время блокировки АПВ")]
        [System.ComponentModel.Category("Автоматика")]
        public ulong APV_Time_Blocking
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[2]);
            }
            set
            {
                _automaticsPage.Value[2] = Measuring.SetTime(value);
            }
        }

        [XmlElement("Время_готовности_АПВ")]
        [DisplayName("Готовность АПВ")]
        [Description("Время готовности АПВ")]
        [System.ComponentModel.Category("Автоматика")]
        public ulong APV_Time_Ready
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[3]);
            }
            set
            {
                _automaticsPage.Value[3] = Measuring.SetTime(value);
            }
        }

        [XmlElement("Время_1_крата_АПВ")]
        [DisplayName("Крат 1 АПВ")]
        [Description("Время 1крата АПВ")]
        [System.ComponentModel.Category("Автоматика")]

        public ulong APV_Time_1Krat
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[4]);
            }
            set
            {
                _automaticsPage.Value[4] = Measuring.SetTime(value);
            }
        }

        [XmlElement("Время_2_крата_АПВ")]
        [DisplayName("Крат 2 АПВ")]
        [Description("Время 2крата АПВ")]
        [System.ComponentModel.Category("Автоматика")]

        public ulong APV_Time_2Krat
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[5]);
            }
            set
            {
                _automaticsPage.Value[5] = Measuring.SetTime(value);
            }
        }

        [XmlElement("Время_3_крата_АПВ")]
        [DisplayName("Крат 3 АПВ")]
        [Description("Время 3крата АПВ")]
        [System.ComponentModel.Category("Автоматика")]

        public ulong APV_Time_3Krat
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[6]);
            }
            set
            {
                _automaticsPage.Value[6] = Measuring.SetTime(value);
            }
        }

        [XmlElement("Время_4_крата_АПВ")]
        [DisplayName("Крат 4 АПВ")]
        [Description("Время 4крата АПВ")]
        [System.ComponentModel.Category("Автоматика")]

        public ulong APV_Time_4Krat
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[7]);
            }
            set
            {
                _automaticsPage.Value[7] = Measuring.SetTime(value);
            }
        }

        [XmlElement("Запуск_АПВ_по_самоотключению")]
        [DisplayName("Самоотключение АПВ")]
        [Description("Запуск АПВ по самоотключению")]
        [TypeConverter(typeof(YesNoTypeConverter))]
        [System.ComponentModel.Category("Автоматика")]

        public string APV_Start
        {
            get
            {
                byte index = (byte)(Common.HIBYTE(_automaticsPage.Value[0]));
                if (index >= Strings.YesNo.Count)
                {
                    index = (byte)(Strings.YesNo.Count - 1);
                }
                return Strings.YesNo[index];

            }
            set { _automaticsPage.Value[0] = Common.TOWORD((byte)Strings.YesNo.IndexOf(value), Common.LOBYTE(_automaticsPage.Value[0])); }
        }

        [XmlElement("Пропадание_питания_АВР")]
        [DisplayName("Питание АВР")]
        [Description("Пропадание питания АВР")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        [System.ComponentModel.Category("Автоматика")]

        public bool AVR_Supply_Off
        {
            get
            {
                return AVR_Add_Signals[0];
            }
            set
            {
                SetCurrentBit(8, 0, value);
            }
        }

        private void SetCurrentBit(byte num_word, byte num_bit, bool val)
        {
            if (val)
            {
                if (((_automaticsPage.Value[num_word] >> num_bit) & 1) == 0)
                {
                    _automaticsPage.Value[num_word] += (ushort)Math.Pow(2, num_bit);
                }

            }
            else
            {
                if (((_automaticsPage.Value[num_word] >> num_bit) & 1) == 1)
                {
                    _automaticsPage.Value[num_word] -= (ushort)Math.Pow(2, num_bit);
                }
            }
        }

        [XmlElement("Команда_отключения_выключателя_АВР")]
        [DisplayName("Отключение АВР")]
        [Description("Команда отключения выключателя АВР")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        [System.ComponentModel.Category("Автоматика")]
        public bool AVR_Switch_Off
        {
            get
            {
                return AVR_Add_Signals[1];
            }
            set
            {
                SetCurrentBit(8, 1, value);
            }

        }

        [XmlElement("Самоотключение_АВР")]
        [DisplayName("Самоотключение АВР")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        [System.ComponentModel.Category("Автоматика")]        
        public bool AVR_Self_Off
        {
            get
            {
                return AVR_Add_Signals[2];
            }
            set
            {
                SetCurrentBit(8, 2, value);
            }
        }

        [XmlElement("Срабатывание_защиты_АВР")]
        [DisplayName("Защита АВР")]
        [Description("Срабатывание защиты АВР")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        [System.ComponentModel.Category("Автоматика")]
        public bool AVR_Abrasion_Switch
        {
            get
            {
                return AVR_Add_Signals[3];
            }
            set
            {
                SetCurrentBit(8, 3, value);
            }
        }

        [XmlElement("Разрешение_сброса_АВР_по_включению_выключателя")]
        [DisplayName("Сброс АВР")]
        [Description("Разрешение сброса АВР по включению выключателя")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        [System.ComponentModel.Category("Автоматика")]
        
        public bool AVR_Reset_Switch
        {
            get
            {
                return AVR_Add_Signals[7];
            }
            set
            {
                SetCurrentBit(8, 7, value);
            }
        }

        [XmlIgnore]
        [DisplayName("Сигналы АВР")]
        [Editor(typeof(BitsEditor),typeof(UITypeEditor))]
        [System.ComponentModel.Category("Автоматика")]
        private BitArray AVR_Add_Signals
        {
            get
            {
                return _bits;
            }
            set
            {
                _bits = value;
            }

        }

        [XmlElement("Блокировка_АВР")]
        [DisplayName("Блокировка АВР")]
        [Description("Номер входа блокировки АВР")]
        [System.ComponentModel.Category("Автоматика")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string AVR_Blocking
        {
            get
            {
                ushort index = _automaticsPage.Value[9];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte)(Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set
            {
                _automaticsPage.Value[9] = (ushort)Strings.Logic.IndexOf(value);
            }
        }

        [XmlElement("Сброс_блокировки_АВР")]
        [DisplayName("Сброс АВР")]
        [Description("Сброс блокировки АВР")]
        [System.ComponentModel.Category("Автоматика")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string AVR_Reset
        {
            get
            {
                ushort index = _automaticsPage.Value[10];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte)(Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set
            {
                _automaticsPage.Value[10] = (ushort)Strings.Logic.IndexOf(value);
            }

        }

        [XmlElement("Запуск_АВР")]
        [DisplayName("Запуск АВР")]
        [System.ComponentModel.Category("Автоматика")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string AVR_Start
        {
            get
            {
                ushort index = _automaticsPage.Value[11];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte)(Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set
            {
                _automaticsPage.Value[11] = (ushort)Strings.Logic.IndexOf(value);
            }

        }

        [XmlElement("Срабатывание_АВР")]
        [DisplayName("Срабатывание АВР")]
        [System.ComponentModel.Category("Автоматика")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string AVR_Abrasion
        {
            get
            {
                ushort index = _automaticsPage.Value[12];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte)(Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set
            {
                _automaticsPage.Value[12] = (ushort)Strings.Logic.IndexOf(value);
            }

        }

        [XmlElement("Время_срабатывания_АВР")]
        [DisplayName("Время cрабатывания АВР")]
        [System.ComponentModel.Category("Автоматика")]
        public ulong AVR_Time_Abrasion
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[13]);
            }
            set
            {
                _automaticsPage.Value[13] = Measuring.SetTime(value);
            }
        }

        [XmlElement("Возврат_АВР")]
        [DisplayName("Возврат АВР")]
        [System.ComponentModel.Category("Автоматика")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string AVR_Return
        {
            get
            {
                ushort index = _automaticsPage.Value[14];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte)(Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set
            {
                _automaticsPage.Value[14] = (ushort)Strings.Logic.IndexOf(value);
            }
        }

        [XmlElement("Время_возврата_АВР")]
        [DisplayName("Время возврата АВР")]
        [System.ComponentModel.Category("Автоматика")]
        public ulong AVR_Time_Return
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[15]);
            }
            set
            {
                _automaticsPage.Value[15] = Measuring.SetTime(value);
            }
        }

        [XmlElement("Время_отключения_АВР")]
        [DisplayName("Время отключения АВР")]
        [System.ComponentModel.Category("Автоматика")]
        public ulong AVR_Time_Off
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[16]);
            }
            set
            {
                _automaticsPage.Value[16] = Measuring.SetTime(value);
            }
        }

        [XmlElement("Конфигурация_ЛЗШ")]
        [DisplayName("Конфигурация ЛЗШ")]
        [System.ComponentModel.Category("Автоматика")]
        [TypeConverter(typeof(ModeLightTypeConverter))]
        public string LZSH_Cnf
        {
            get
            {
                ushort index = _automaticsPage.Value[18];
                if (index >= Strings.ModesLight.Count)
                {
                    index = (byte)(Strings.ModesLight.Count - 1);
                }
                return Strings.ModesLight[index];
            }
            set
            {
                _automaticsPage.Value[18] = (ushort)Strings.ModesLight.IndexOf(value);
            }
        }

        [XmlElement("Уставка_ЛЗШ")]
        [DisplayName("Уставка ЛЗШ")]
        [System.ComponentModel.Category("Автоматика")]
        public double LZSH_Constraint
        {
            get
            {
                return Measuring.GetConstraint(_automaticsPage.Value[19], ConstraintKoefficient.K_4000);
            }
            set
            {
                _automaticsPage.Value[19] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        #endregion

        #region Внешние защиты
        
        public class CExternalDefenses : ICollection
        {
            public const int LENGTH = 48;
            public const int COUNT = 8;

            #region ICollection Members

            public void Add(ExternalDefenseItem item)
            {
                _defenseList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }
            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }
            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _defenseList.GetEnumerator();
            }

            #endregion

            public CExternalDefenses()
            {
                SetBuffer(new ushort[LENGTH]);
            }
            
            private List<ExternalDefenseItem> _defenseList = new List<ExternalDefenseItem>(COUNT);

            public void SetBuffer(ushort[] buffer)
            {
                _defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("External defense values", (object)LENGTH, "External defense length must be 48");
                }
                for (int i = 0; i < LENGTH; i += ExternalDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[ExternalDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, ExternalDefenseItem.LENGTH);
                    _defenseList.Add(new ExternalDefenseItem(temp));
                }
            }
            [DisplayName("Количество")]
            public int Count
            {
                get
                {
                    return _defenseList.Count;
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                for (int i = 0; i < Count; i++)
                {
                    Array.ConstrainedCopy(_defenseList[i].Values, 0, buffer, i * ExternalDefenseItem.LENGTH, ExternalDefenseItem.LENGTH);
                }
                return buffer;
            }
            
            public ExternalDefenseItem this[int i]
            {
                get
                {
                    return _defenseList[i];
                }
                set
                {
                    _defenseList[i] = value;
                }
            }
        };
        
        public class ExternalDefenseItem
        {
            public const int LENGTH = 6;
            private ushort[] _values = new ushort[LENGTH];

            public ExternalDefenseItem() { }
            public ExternalDefenseItem(ushort[] buffer)
            {
                SetItem(buffer);
            }
            public override string ToString()
            {
                return "Внешняя защита";
            }
            [XmlAttribute("Значения")]
            [DisplayName("Значения")]
            [Description("Данные в словном виде")]
            [System.ComponentModel.Category("Словный вид")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            public ushort[] Values
            {
                get
                {
                    return _values;
                }
                set
                {
                    SetItem(value);
                }
            }
            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("External defense item value", (object)LENGTH, "External defense item length must be 6");
                }
                _values = buffer;
            }
            [XmlAttribute("Режим")]
            [DisplayName("Режим")]
            [Description("Режим работы защиты")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    byte index = (byte)(Common.LOBYTE(_values[0]) & 0x0F);
                    if (index >= Strings.ModesExternal.Count)
                    {
                        index = (byte)(Strings.ModesExternal.Count - 1);
                    }
                    return Strings.ModesExternal[index];
                }
                set
                {
                    _values[0] = Common.SetBits(_values[0], (ushort)Strings.ModesExternal.IndexOf(value), 0, 1, 2);
                }
            }


            [XmlAttribute("АВР")]
            [DisplayName("АВР")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool AVR
            {
                get
                {
                    return 0 != (_values[0] & 32);
                }
                set
                {
                    _values[0] = value ? (ushort)(_values[0] |= 32) : (ushort)(_values[0] & ~32);
                }
            }

            [XmlAttribute("АПВ")]
            [DisplayName("АПВ")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool APV
            {
                get
                {
                    return 0 != (_values[0] & 64);
                }
                set
                {
                    _values[0] = value ? (ushort)(_values[0] |= 64) : (ushort)(_values[0] & ~64);
                }
            }
            [XmlAttribute("УРОВ")]
            [DisplayName("УРОВ")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool UROV
            {
                get
                {
                    return 0 != (_values[0] & 128);
                }
                set
                {
                    _values[0] = value ? (ushort)(_values[0] |= 128) : (ushort)(_values[0] & ~128);
                }
            }

            [XmlAttribute("Возврат_АПВ")]
            [DisplayName("АПВ возврата")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool APV_Return
            {
                get
                {
                    return 0 != (_values[0] & 0x8000);
                }
                set
                {
                    _values[0] = value ? (ushort)(_values[0] |= 0x8000) : (ushort)(_values[0] & ~0x8000);
                }
            }

            [XmlAttribute("Возврат")]
            [DisplayName("Возврат")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool Return
            {
                get
                {
                    return 0 != (_values[0] & 0x4000);
                }
                set
                {
                    _values[0] = value ? (ushort)(_values[0] |= 0x4000) : (ushort)(_values[0] & ~0x4000);
                }
            }


            [XmlAttribute("Блокировка_номер")]
            [DisplayName("Номер блокировки")]
            [Description("Номер входа блокировки ВЗ")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(ExternalDefensesTypeConverter))]
            public string BlockingNumber
            {
                get
                {
                    ushort value = _values[1];
                    if (value >= Strings.ExternalDefense.Count)
                    {
                        value = (ushort)(Strings.ExternalDefense.Count - 1);
                    }
                    return Strings.ExternalDefense[value];
                }
                set
                {                    
                    _values[1] = (ushort)Strings.ExternalDefense.IndexOf(value);
                }
            }
            [XmlAttribute("Срабатывание_номер")]
            [DisplayName("Номер срабатывания")]
            [Description("Номер входа срабатывания ВЗ")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(ExternalDefensesTypeConverter))]
            public string WorkingNumber
            {
                get
                {
                    ushort value = _values[2];
                    if (value >= Strings.ExternalDefense.Count)
                    {
                        value = (ushort)(Strings.ExternalDefense.Count - 1);
                    }
                    return Strings.ExternalDefense[value];
                }
                set
                {
                    _values[2] = (ushort)Strings.ExternalDefense.IndexOf(value);
                }
            }

            [XmlAttribute("Срабатывание_время")]
            [DisplayName("Время срабатывания")]
            [Description("Выдержка времени срабатывания ВЗ")]
            [System.ComponentModel.Category("Текстовый вид")]
            public ulong WorkingTime
            {
                get
                {
                    return Measuring.GetTime(_values[3]);
                }
                set
                {
                    _values[3] = Measuring.SetTime(value);
                }
            }
            [XmlAttribute("Возврат_номер")]
            [DisplayName("Номер возврата")]
            [Description("Номер входа возврата ВЗ")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(ExternalDefensesTypeConverter))]
            public string ReturnNumber
            {
                get
                {
                    ushort value = _values[4];
                    if (value >= Strings.ExternalDefense.Count)
                    {
                        value = (ushort)(Strings.ExternalDefense.Count - 1);
                    }
                    return Strings.ExternalDefense[value];
                }
                set
                {
                    _values[4] = (ushort)Strings.ExternalDefense.IndexOf(value);
                }
            }
            [XmlAttribute("Возврат_время")]
            [DisplayName("Время возврата")]
            [Description("Выдержка времени возврата ВЗ")]
            [System.ComponentModel.Category("Текстовый вид")]
            public ulong ReturnTime
            {
                get
                {
                    return Measuring.GetTime(_values[5]);
                }
                set
                {
                    _values[5] = Measuring.SetTime(value);
                }
            }
        }

        [TestFixture]
        public class TestExternalDefense
        {
            [Test]
            public void TestFirstWord()
            {
                ExternalDefenseItem item = new ExternalDefenseItem();
                ushort[] buffer = new ushort[] {0xFFFF,0,0,0,0,0};
                item.SetItem(buffer);
                Assert.IsTrue(item.AVR);
                Assert.IsTrue(item.Return);
                Assert.IsTrue(item.APV);
                Assert.IsTrue(item.APV_Return);
                Assert.IsTrue(item.UROV);
                Assert.AreEqual(15, item.Mode);

                buffer = new ushort[] { 0, 0, 0, 0, 0, 0 };
                item.SetItem(buffer);
                Assert.IsFalse(item.AVR);
                Assert.IsFalse(item.Return);
                Assert.IsFalse(item.APV);
                Assert.IsFalse(item.APV_Return);
                Assert.IsFalse(item.UROV);
                Assert.AreEqual(0, item.Mode);

                item.Return = true;
                Assert.AreEqual(item.Values[0], 512);
                item.APV = true;
                Assert.AreEqual(item.Values[0], 512 + 64);
            }
        };

        private CExternalDefenses _cexternalDefenses  = new CExternalDefenses();

        [XmlElement("Внешние_защиты")]
        [DisplayName("Внешние защиты")]
        [Description("Внешние защиты")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("Внешние защиты")]
        public CExternalDefenses ExternalDefenses
        {
            get { return _cexternalDefenses; }
            set { _cexternalDefenses = value; }
        }
	

        #endregion
        
        #region Токовые защиты

        private slot _tokDefensesMain = new slot(0x1080, 0x10C0);
        private slot _tokDefenses2Main = new slot(0x1100, 0x1110);
        private slot _tokDefensesReserve = new slot(0x10C0, 0x1100);
        private slot _tokDefenses2Reserve = new slot(0x1110, 0x1120);

        public void LoadTokDefenses()
        {
            LoadSlot(DeviceNumber, _tokDefenses2Reserve, "LoadTokDefensesReserve2" + DeviceNumber);
            LoadSlot(DeviceNumber, _tokDefensesReserve, "LoadTokDefensesReserve" + DeviceNumber);
            LoadSlot(DeviceNumber, _tokDefenses2Main, "LoadTokDefensesMain2" + DeviceNumber);
            LoadSlot(DeviceNumber, _tokDefensesMain, "LoadTokDefensesMain" + DeviceNumber);
        }

        public void SaveTokDefenses()
        {
            Array.ConstrainedCopy(TokDefensesMain.ToUshort1(), 0, _tokDefensesMain.Value, 0, CTokDefenses.LENGTH1);
            Array.ConstrainedCopy(TokDefensesMain.ToUshort2(), 0, _tokDefenses2Main.Value, 0, CTokDefenses.LENGTH2);
            Array.ConstrainedCopy(TokDefensesReserve.ToUshort1(), 0, _tokDefensesReserve.Value, 0, CTokDefenses.LENGTH1);
            Array.ConstrainedCopy(TokDefensesReserve.ToUshort2(), 0, _tokDefenses2Reserve.Value, 0, CTokDefenses.LENGTH2);

            SaveSlot(DeviceNumber, _tokDefenses2Main, "SaveTokDefensesMain2" + DeviceNumber);
            SaveSlot(DeviceNumber, _tokDefenses2Reserve, "SaveTokDefensesReserve2" + DeviceNumber);
            SaveSlot(DeviceNumber, _tokDefensesReserve, "SaveTokDefensesReserve" + DeviceNumber);
            SaveSlot(DeviceNumber, _tokDefensesMain, "SaveTokDefensesMain" + DeviceNumber);
        }

        private CTokDefenses _ctokDefensesMain = new CTokDefenses();
        private CTokDefenses _ctokDefensesReserve = new CTokDefenses();

        [XmlElement("Токовые_защиты_основные")]
        [DisplayName("Основная группа")]
        [Description("Внешние защиты - основная группа уставок")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("Токовые защиты")]
        public CTokDefenses TokDefensesMain
        {
            get
            {
                return _ctokDefensesMain;
            }
        }
        [XmlElement("Токовые_защиты_резервные")]
        [DisplayName("Резервная группа")]
        [Description("Внешние защиты - резервная группа уставок")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("Токовые защиты")]
        public CTokDefenses TokDefensesReserve
        {
            get
            {
                return _ctokDefensesReserve;
            }
        }
        
      
        public class CTokDefenses : ICollection
        {
            public const int LENGTH1 = 64;
            public const int COUNT1 = 10;
            public const int LENGTH2 = 16;
            public const int COUNT2 = 2;
            private ushort[] _buffer;


            #region ICollection Members

           [DisplayName("Количество")]
           public int Count
           {
               get
               {
                   return _items.Count;
               }
           }
 
            public void Add(TokDefenseItem item)
            {
                _items.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }
            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }
            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _items.GetEnumerator();
            }

            #endregion
                        
            public ushort I
            {
                get { return _buffer[0]; }
                set { _buffer[0] = value; }
            }
            public ushort I0
            {
                get { return _buffer[1]; }
                set { _buffer[1] = value; }
            }
            public ushort In
            {
                get { return _buffer[2]; }
                set { _buffer[2] = value; }
            }
            public ushort I2
            {
                get { return _buffer[3]; }
                set { _buffer[3] = value; }
            }
            
            private List<TokDefenseItem> _items = new List<TokDefenseItem>(12);

            public CTokDefenses()
            {
                SetBuffer(new ushort[160]);
            }
            public TokDefenseItem this[int i]
            {
                get
                {
                    return _items[i];
                }
                set
                {
                    _items[i] = value;
                }
            }
                 
            public void SetBuffer(ushort[] buffer)
            {                
                _buffer = buffer;
                _items.Clear();
                for (int i = 0; i < TokDefenseItem.LENGTH * 12; i += TokDefenseItem.LENGTH)
                {
                    if (i == TokDefenseItem.LENGTH * 11)
                    {
                        i += 2;
                    }
                    ushort[] item = new ushort[TokDefenseItem.LENGTH];
                    Array.ConstrainedCopy(_buffer, i + 4, item, 0, TokDefenseItem.LENGTH);
                    _items.Add(new TokDefenseItem(item));
                }

                for (int i = 0; i < 12; i++)
                {   
                    _items[i].IsIncrease = i < 4 ? true : false;
                    _items[i].IsKoeff500 = i < 8 ? false : true;
                    _items[i].IsI12 = false;

                }

                _items[11].IsI12 = true;
                
                _items[0].Name = "I>";
                _items[1].Name = "I>>";
                _items[2].Name = "I>>>";
                _items[3].Name = "I>>>>";
                _items[4].Name = "I2>";
                _items[5].Name = "I2>>";
                _items[6].Name = "I0>";
                _items[7].Name = "I0>>";
                _items[8].Name = "In>";
                _items[9].Name = "In>>";
                _items[10].Name = "Ig";
                _items[11].Name = "I1/I2";
            }

            public ushort[] ToUshort1()
            {
                ushort[] buffer = new ushort[LENGTH1];
                buffer[0] = I;
                buffer[1] = I0;
                buffer[2] = In;
                buffer[3] = I2;

                for (int i = 0; i < COUNT1; i++)
                {
                    Array.ConstrainedCopy(_items[i].Values, 0, buffer, 4 + i * TokDefenseItem.LENGTH, TokDefenseItem.LENGTH);
                }
                return buffer;
            }
            public ushort[] ToUshort2()
            {
                ushort[] buffer = new ushort[LENGTH2];

                Array.ConstrainedCopy(_items[10].Values, 0, buffer, 0, TokDefenseItem.LENGTH);
                Array.ConstrainedCopy(_items[11].Values, 0,buffer, 2 + TokDefenseItem.LENGTH, TokDefenseItem.LENGTH);
                return buffer;
            }
        };

        public class TokDefenseItem
        {
            public const int LENGTH = 6;
            private bool _isKoeff500;

            [XmlIgnore]
            [Browsable(false)]
            public bool IsKoeff500
            {
                get { return _isKoeff500; }
                set { _isKoeff500 = value; }
            }
	
            private bool _isIncrease;
            private String _name;
            private ushort[] _values = new ushort[LENGTH];

            public TokDefenseItem() { }
            public TokDefenseItem(ushort[] buffer)
            {
                SetItem(buffer);
            }

            private bool _isI12 = false;

            public bool IsI12
            {
                get { return _isI12; }
                set { _isI12 = value; }
            }
	

            [XmlIgnore]
            [Browsable(false)]
            public bool IsIncrease
            {
                get { return _isIncrease; }
                set { _isIncrease = value; }
            }
            [XmlIgnore]
            [Browsable(false)]
            public String Name
            {
                get { return _name; }
                set { _name = value; }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Tok defense increase1 item value", (object)LENGTH, "Tok defense increase1 item length must be 6");
                }
                _values = buffer;
            }
            [XmlAttribute("Значения")]
            [DisplayName("Значения")]
            [Description("Данные в словном виде")]
            [System.ComponentModel.Category("Словный вид")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            public ushort[] Values
            {
                get
                {
                    return _values;
                }
                set
                {
                    SetItem(value);
                }
            }
            [XmlAttribute("Режим")]
            [DisplayName("Режим")]
            [Description("Режим работы защиты")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    byte index = (byte)(Common.LOBYTE(_values[0]) & 0x07);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte)(Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set
                {
                    _values[0] = Common.SetBits(_values[0], (ushort)Strings.Modes.IndexOf(value), 0, 1, 2);
                }
            }
            [XmlAttribute("АВР")]
            [DisplayName("АВР")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool AVR
            {
                get
                {
                    return Common.GetBit(_values[0], 5);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 5, value);
                }
            }
            [XmlAttribute("АПВ")]
            [DisplayName("АПВ")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool APV
            {
                get
                {
                    return Common.GetBit(_values[0], 6);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 6, value);
                }
            }
            [XmlAttribute("УРОВ")]
            [DisplayName("УРОВ")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool UROV
            {
                get
                {
                    return Common.GetBit(_values[0], 7);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 7, value);
                }
            }
            [XmlAttribute("Блокировка_введение")]
            [DisplayName("Блокировка тип")]
            [Description("Тип блокировки")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BlockingTypeConverter))]
            public string BlockingExist
            {
                get
                {
                    string ret = "";
                    ret = Common.GetBit(_values[0], 9) ? Strings.Blocking[1] : Strings.Blocking[0];
                    return ret;
                }
                set
                {
                    bool b = true;
                    if (Strings.Blocking[0] == value)
                    {
                        b = false;
                    }
                    _values[0] = Common.SetBit(_values[0], 9, b);
                }
            }
            [XmlAttribute("Блокировка_номер")]
            [DisplayName("Блокировка номер")]
            [Description("Номер входа блокировки")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(LogicTypeConverter))]
            public string BlockingNumber
            {
                get
                {
                    ushort value = _values[1];
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort)(Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set
                {
                    _values[1] = (ushort)Strings.Logic.IndexOf(value);
                }
            }
            [XmlAttribute("Пуск_по_U")]
            [DisplayName("Пуск по U")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool PuskU
            {
                get
                {
                    return Common.GetBit(_values[0], 14);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 14, value);
                }
            }
            [XmlAttribute("Ускорение")]
            [DisplayName("Ускорение")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool Speedup
            {
                get
                {
                    return Common.GetBit(_values[0], 15);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 15, value);
                }
            }
            [XmlAttribute("Направление")]
            [DisplayName("Направление")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BusDirectionTypeConverter))]
            public string Direction
            {
                get
                {
                    ushort temp = Common.GetBits(_values[0], 10, 11);
                    byte value = (byte)(Common.HIBYTE(temp) >> 2);
                    value = value >= Strings.BusDirection.Count ? (byte)(Strings.BusDirection.Count - 1) : value;
                    return Strings.BusDirection[value];
                }
                set
                {
                    ushort index = (ushort)Strings.BusDirection.IndexOf(value);
                    _values[0] = Common.SetBits(_values[0], index, 10, 11);
                }
            }
            [XmlAttribute("Параметр")]
            [Browsable(false)]
            public string Parameter
            {
                get
                {
                    string ret = "";
                    if (IsIncrease)
                    {
                        ret = Common.GetBit(_values[0], 8) ? Strings.TokParameter[1] : Strings.TokParameter[0];
                    }
                    else
                    {
                        ret = Common.GetBit(_values[0], 8) ? Strings.EngineParameter[1] : Strings.EngineParameter[0];
                    }

                    return ret;
                }
                set
                {
                    bool b = true;
                    if (IsIncrease)
                    {
                        if (Strings.TokParameter[0] == value)
                        {
                            b = false;
                        }
                    }
                    else
                    {
                        if (Strings.EngineParameter[0] == value)
                        {
                            b = false;
                        }
                    }

                    _values[0] = Common.SetBit(_values[0], 8, b);
                }
            }
            [XmlAttribute("Двигатель")]
            [Browsable(false)]
            public string Engine
            {
                get
                {
                    string ret = "";
                    ret = Common.GetBit(_values[0], 15) ? Strings.EngineMode[1] : Strings.EngineMode[0];
                    return ret;
                }
                set
                {
                    bool b = true;
                    if (Strings.EngineMode[0] == value)
                    {
                        b = false;
                    }
                    _values[0] = Common.SetBit(_values[0], 15, b);
                }
            }
            [XmlAttribute("Характеристика")]
            [DisplayName("Характеристика")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(FeatureTypeConverter))]
            public string Feature
            {
                get
                {
                    string ret = "";
                    if (IsIncrease)
                    {
                        ret = Common.GetBit(_values[0], 12) ? Strings.FeatureLight[1] : Strings.FeatureLight[0];    
                    }
                    else
                    {
                        ret = Common.GetBit(_values[0], 12) ? Strings.Feature[1] : Strings.Feature[0];                            
                    }
                    
                    return ret;
                }
                set
                {
                    bool b = true;
                    if (IsIncrease)
                    {
                        if (Strings.FeatureLight[0] == value)
                        {
                            b = false;
                        }   
                    }else
                    {
                        if (Strings.Feature[0] == value)
                        {
                            b = false;
                        }
                    }
                    
                    _values[0] = Common.SetBit(_values[0], 12, b);
                }
            }
            [XmlAttribute("Срабатывание_уставка")]
            [DisplayName("Срабатывание уставка")]
            [Description("Уставка срабатывания")]
            [System.ComponentModel.Category("Текстовый вид")]
            public double WorkConstraint
            {
                get
                {
                    double ret = 0.0;
                    ConstraintKoefficient koeff = ConstraintKoefficient.K_4000;
                    if (IsKoeff500)
                    {
                        koeff = ConstraintKoefficient.K_500;
                    }
                    if (IsI12)
                    {
                        koeff = ConstraintKoefficient.K_10000;
                    }
                    ret = Measuring.GetConstraint(_values[2], koeff);
                    return ret;
                }
                set
                {
                    ConstraintKoefficient koeff = ConstraintKoefficient.K_4000;
                    if (IsKoeff500)
                    {
                        koeff = ConstraintKoefficient.K_500;
                    }
                    if (IsI12)
                    {
                        koeff = ConstraintKoefficient.K_10000;
                    }
                    _values[2] = Measuring.SetConstraint(value, koeff);
                }
            }
            [XmlAttribute("Срабатывание_время")]
            [DisplayName("Срабатывание время")]
            [Description("Выдержка времени срабатывания")]
            [System.ComponentModel.Category("Текстовый вид")]
            public ulong WorkTime
            {
                get
                {
                    return Measuring.GetTime(_values[3]);
                }
                set
                {
                    _values[3] = Measuring.SetTime(value);
                }
            }
            [XmlAttribute("Пуск_по_U_уставка")]
            [DisplayName("Пуск по U уставка")]
            [Description("Уставка пуска по U")]
            [System.ComponentModel.Category("Текстовый вид")]
            public double PuskU_Constraint
            {
                get
                {
                    return Measuring.GetConstraint(_values[4],ConstraintKoefficient.K_25600);
                }
                set
                {
                    _values[4] = Measuring.SetConstraint(value,ConstraintKoefficient.K_25600);
                }
            }
            [XmlAttribute("Ускорение_время")]
            [DisplayName("Ускорение время")]
            [Description("Выдержка времени ускорения")]
            [System.ComponentModel.Category("Текстовый вид")]
            public ulong SpeedupTime
            {
                get
                {
                    return Measuring.GetTime(_values[5]);
                }
                set
                {
                    _values[5] = Measuring.SetTime(value);
                }
            }

        };
              
        #endregion

        #region Защиты по напряжению

        private slot _voltageDefensesMain = new slot(0x1180, 0x11C0);
        private slot _voltageDefensesReserve = new slot(0x11C0, 0x1200);

        public void LoadVoltageDefenses()
        {
            LoadSlot(DeviceNumber, _voltageDefensesReserve, "LoadVoltageDefensesReserve" + DeviceNumber);
            LoadSlot(DeviceNumber, _voltageDefensesMain, "LoadVoltageDefensesMain" + DeviceNumber);
        }

        public void SaveVoltageDefenses()
        {
            Array.ConstrainedCopy(VoltageDefensesMain.ToUshort(), 0, _voltageDefensesMain.Value, 0, CVoltageDefenses.LENGTH);
            Array.ConstrainedCopy(VoltageDefensesReserve.ToUshort(), 0, _voltageDefensesReserve.Value, 0, CVoltageDefenses.LENGTH);
            SaveSlot(DeviceNumber, _voltageDefensesReserve, "SaveVoltageDefensesReserve" + DeviceNumber);
            SaveSlot(DeviceNumber, _voltageDefensesMain, "SaveVoltageDefensesMain" + DeviceNumber);
        }

        public event Handler VoltageDefensesLoadOk;
        public event Handler VoltageDefensesLoadFail;
        public event Handler VoltageDefensesSaveOk;
        public event Handler VoltageDefensesSaveFail;

        private CVoltageDefenses _cvoltageDefensesMain = new CVoltageDefenses();
        private CVoltageDefenses _cvoltageDefensesReserve = new CVoltageDefenses();

        [XmlElement("Защиты_напряжения_основные")]
        [DisplayName("Основная группа")]
        [Description("Защиты по напряжению - основная группа уставок")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("Защиты по напряжению")]
        public CVoltageDefenses VoltageDefensesMain
        {
            get
            {
                return _cvoltageDefensesMain;
            }
        }
        [XmlElement("Защиты_напряжения_резервные")]
        [DisplayName("Резервная группа")]
        [Description("Защиты по напряжению - резервная группа уставок")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("Защиты по напряжению")]
        public CVoltageDefenses VoltageDefensesReserve
        {
            get
            {
                return _cvoltageDefensesReserve;
            }
        }

        public enum VoltageDefenseType { U, U2, U0 };

        public class CVoltageDefenses : ICollection
        {
            public const int LENGTH = 64;
            public const int COUNT = 8;

            #region ICollection Members

            public void Add(VoltageDefenseItem item)
            {
                _defenseList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }
            [Browsable(false)]
            [XmlIgnore]
            public bool IsSynchronized
            {
                get { return false; }
            }
            [Browsable(false)]
            [XmlIgnore]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _defenseList.GetEnumerator();
            }

            #endregion

            private List<VoltageDefenseItem> _defenseList = new List<VoltageDefenseItem>(COUNT);
            public CVoltageDefenses()
            {
                SetBuffer(new ushort[LENGTH]);
            }
            public void SetBuffer(ushort[] buffer)
            {
                _defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Защиты по напряжению", (object)LENGTH, "Буфер для защит по напряжению должен быть " + LENGTH);
                }
                for (int i = 0; i < LENGTH; i += VoltageDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[VoltageDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, VoltageDefenseItem.LENGTH);
                    _defenseList.Add(new VoltageDefenseItem(temp));
                }
                _defenseList[4].DefenseType = _defenseList[5].DefenseType =  VoltageDefenseType.U2;
                _defenseList[6].DefenseType = _defenseList[7].DefenseType = VoltageDefenseType.U0;

                _defenseList[0].Name = "U>";
                _defenseList[1].Name = "U>>";
                _defenseList[2].Name = "U<";
                _defenseList[3].Name = "U<<";
                _defenseList[4].Name = "U2>";
                _defenseList[5].Name = "U2>>";
                _defenseList[6].Name = "U0>";
                _defenseList[7].Name = "U0>>";
            }

            [DisplayName("Количество")]
            public int Count
            {
                get
                {
                    return _defenseList.Count;
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                for (int i = 0; i < Count; i++)
                {
                    Array.ConstrainedCopy(_defenseList[i].Values, 0, buffer, i * VoltageDefenseItem.LENGTH, VoltageDefenseItem.LENGTH);
                }
                return buffer;
            }
            public VoltageDefenseItem this[int i]
            {
                get
                {
                    return _defenseList[i];
                }
                set
                {
                    _defenseList[i] = value;
                }
            }
        };

        public class VoltageDefenseItem
        {
            public const int LENGTH = 8;
            private ushort[] _values = new ushort[LENGTH];
            public override string ToString()
            {
                return "Защита по напряжению";
            }

            public VoltageDefenseItem() { }
            public VoltageDefenseItem(ushort[] buffer)
            {
                SetItem(buffer);
            }
            [XmlAttribute("Значения")]
            [DisplayName("Значения")]
            [Description("Данные в словном виде")]
            [System.ComponentModel.Category("Словный вид")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            public ushort[] Values
            {
                get
                {
                    return _values;
                }
                set
                {
                    SetItem(value);
                }
            }
            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Буфер защиты по напряжению", (object)LENGTH, "Буфер защит должен быть длиной" + LENGTH);
                }
                _values = buffer;
            }
            private VoltageDefenseType _type = VoltageDefenseType.U;
            [XmlAttribute("Тип")]
            [Browsable(false)]
            public VoltageDefenseType DefenseType 
            {
                get
                {
                    return _type;
                }
                set
                {
                    _type = value;
                }
            }
            [XmlAttribute("Режим")]
            [DisplayName("Режим")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    ushort index = Common.GetBits(_values[0], 0, 1, 2, 3);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte)(Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set
                {
                    _values[0] = Common.SetBits(_values[0], (ushort)Strings.Modes.IndexOf(value), 0, 1, 2);
                }
            }
            [XmlAttribute("АВР")]
            [DisplayName("АВР")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool AVR
            {
                get
                {
                    return Common.GetBit(_values[0], 5);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 5, value);
                }
            }
            [XmlAttribute("АПВ")]
            [DisplayName("АПВ")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool APV
            {
                get
                {
                    return Common.GetBit(_values[0], 6);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 6, value);
                }
            }
            [XmlAttribute("УРОВ")]
            [DisplayName("УРОВ")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool UROV
            {
                get
                {
                    return Common.GetBit(_values[0], 7);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 7, value);
                }
            }
            [XmlAttribute("АПВ_возврат")]
            [DisplayName("АПВ возврата")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool APV_Return
            {
                get
                {
                    return Common.GetBit(_values[0], 15);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 15, value);
                }
            }
            [XmlAttribute("Возврат")]
            [DisplayName("Возврат")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool Return
            {
                get
                {
                    return Common.GetBit(_values[0], 14);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 14, value);
                }
            }
            [XmlAttribute("Параметр")]
            [DisplayName("Параметр")]
            [System.ComponentModel.Category("Текстовый вид")]
            public string Parameter
            {
                get
                {
                    string ret = "";
                    int index = (Common.GetBits(_values[0],8,9,10) >> 8);
                    switch (_type)
                    {
                        case VoltageDefenseType.U:
                            if (index >= Strings.VoltageParameterU.Count)
                            {
                                index = Strings.VoltageParameterU.Count - 1;    
                            }
                            ret = Strings.VoltageParameterU[index];
                            break;
                        case VoltageDefenseType.U0:
                            if (index >= Strings.VoltageParameterU0.Count)
                            {
                                index = Strings.VoltageParameterU0.Count - 1;
                            }
                            ret = Strings.VoltageParameterU0[index];
                            break;
                        case VoltageDefenseType.U2:
                            if (index >= Strings.VoltageParameterU2.Count)
                            {
                                index = Strings.VoltageParameterU2.Count - 1;
                            }
                            ret = Strings.VoltageParameterU2[index];
                            break;
                        default:
                            break;
                    }
                    return ret;
                }
                set
                {
                    switch (_type)
                    {
                        case VoltageDefenseType.U:
                            _values[0] = Common.SetBits(_values[0], (ushort)Strings.VoltageParameterU.IndexOf(value), 8, 9, 10);
                            break;
                        case VoltageDefenseType.U2:
                            _values[0] = Common.SetBits(_values[0],(ushort) Strings.VoltageParameterU2.IndexOf(value), 8, 9, 10);
                            break;
                        case VoltageDefenseType.U0:
                            _values[0] = Common.SetBits(_values[0], (ushort)Strings.VoltageParameterU0.IndexOf(value), 8, 9, 10);
                            break;
                        default:
                            break;
                    }             
                   
                }
            }

            [XmlAttribute("Блокировка_номер")]
            [DisplayName("Блокировка номер")]
            [Description("Номер входа блокировки")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(LogicTypeConverter))]
            public string BlockingNumber
            {
                get
                {
                    ushort value = (ushort)(_values[1] & 0xFF);
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort)(Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set
                {
                    _values[1] = (ushort)Strings.Logic.IndexOf(value);
                }
            }
            [XmlAttribute("Срабатывание_уставка")]
            [DisplayName("Срабатывание уставка")]
            [Description("Уставка срабатывания")]
            [System.ComponentModel.Category("Текстовый вид")]
            public double WorkConstraint
            {
                get
                {
                    double ret = 0.0;
                    ret = Measuring.GetConstraint(_values[2], ConstraintKoefficient.K_25600);
                    return ret;
                }
                set
                {
                    _values[2] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
                }
            }
            [XmlAttribute("Срабатывание_время")]
            [DisplayName("Срабатывание время")]
            [Description("Выдержка времени срабатывания")]
            [System.ComponentModel.Category("Текстовый вид")]
            public ulong WorkTime
            {
                get
                {
                    return Measuring.GetTime(_values[3]);
                }
                set
                {
                    _values[3] = Measuring.SetTime(value);
                }
            }
            [XmlAttribute("Возврат_уставка")]
            [DisplayName("Возврат уставка")]
            [System.ComponentModel.Category("Текстовый вид")]
            public double ReturnConstraint
            {
                get
                {
                    double ret = 0.0;
                    ret = Measuring.GetConstraint(_values[4], ConstraintKoefficient.K_25600);
                    return ret;
                }
                set
                {
                    _values[4] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
                }
            }
            [XmlAttribute("Возврат_время")]
            [DisplayName("Возврат время")]
            [Description("Выдержка времени возврата")]
            [System.ComponentModel.Category("Текстовый вид")]
            public ulong ReturnTime
            {
                get
                {
                    return Measuring.GetTime(_values[5]);
                }
                set
                {
                    _values[5] = Measuring.SetTime(value);
                }
            }
            private string _name;
            [Browsable(false)]
            [XmlIgnore]
            public string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    _name = value;
                }
            }
        }
        #endregion

        #region Страница защит по частоте

        private slot _frequenceDefensesMain = new slot(0x1140, 0x1160);
        private slot _frequenceDefensesReserve = new slot(0x1160, 0x1180);

        public void LoadFrequenceDefenses()
        {
            LoadSlot(DeviceNumber, _frequenceDefensesReserve, "LoadFrequenceDefensesReserve" + DeviceNumber);
            LoadSlot(DeviceNumber, _frequenceDefensesMain, "LoadFrequenceDefensesMain" + DeviceNumber);
        }

        public void SaveFrequenceDefenses()
        {
            Array.ConstrainedCopy(FrequenceDefensesMain.ToUshort(), 0, _frequenceDefensesMain.Value, 0, CFrequenceDefenses.LENGTH);
            Array.ConstrainedCopy(FrequenceDefensesReserve.ToUshort(), 0, _frequenceDefensesReserve.Value, 0, CFrequenceDefenses.LENGTH);
            SaveSlot(DeviceNumber, _frequenceDefensesReserve, "SaveFrequenceDefensesReserve" + DeviceNumber);
            SaveSlot(DeviceNumber, _frequenceDefensesMain, "SaveFrequenceDefensesMain" + DeviceNumber);
        }

        public event Handler FrequenceDefensesLoadOk;
        public event Handler FrequenceDefensesLoadFail;
        public event Handler FrequenceDefensesSaveOk;
        public event Handler FrequenceDefensesSaveFail;

        private CFrequenceDefenses _cFrequenceDefensesMain = new CFrequenceDefenses();
        private CFrequenceDefenses _cFrequenceDefensesReserve = new CFrequenceDefenses();

        [XmlElement("Защиты_по_частоте_основные")]
        [DisplayName("Основная группа")]
        [Description("Защиты по частоте - основная группа уставок")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("Защиты по частоте")]
        public CFrequenceDefenses FrequenceDefensesMain
        {
            get
            {
                return _cFrequenceDefensesMain;
            }
        }
        [XmlElement("Защиты_по_частоте_резервные")]
        [DisplayName("Резервная группа")]
        [Description("Защиты по частоте - резервная группа уставок")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("Защиты по частоте")]
        public CFrequenceDefenses FrequenceDefensesReserve
        {
            get
            {
                return _cFrequenceDefensesReserve;
            }
        }

        public class CFrequenceDefenses : ICollection
        {
            public const int LENGTH = 0x20;
            public const int COUNT = 4;


            #region ICollection Members

            public void Add(FrequenceDefenseItem item)
            {
                _defenseList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }
            [Browsable(false)]
            [XmlIgnore]
            public bool IsSynchronized
            {
                get { return false; }
            }
            [Browsable(false)]
            [XmlIgnore]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _defenseList.GetEnumerator();
            }

            #endregion

            private List<FrequenceDefenseItem> _defenseList = new List<FrequenceDefenseItem>(COUNT);
            
            public CFrequenceDefenses()
            {
                SetBuffer(new ushort[LENGTH]);
            }
            public void SetBuffer(ushort[] buffer)
            {
                _defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Защиты по частоте", (object)LENGTH, "Буфер для защит по частоте должен быть " + LENGTH);
                }
                for (int i = 0; i < LENGTH; i += FrequenceDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[FrequenceDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, FrequenceDefenseItem.LENGTH);
                    _defenseList.Add(new FrequenceDefenseItem(temp));
                }
                _defenseList[0].Name = "F>";
                _defenseList[1].Name = "F>>";
                _defenseList[2].Name = "F<";
                _defenseList[3].Name = "F<<";
               

            }
            [DisplayName("Количество")]
            public int Count
            {
                get
                {
                    return _defenseList.Count;
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                for (int i = 0; i < Count; i++)
                {
                    Array.ConstrainedCopy(_defenseList[i].Values, 0, buffer, i * FrequenceDefenseItem.LENGTH, FrequenceDefenseItem.LENGTH);
                }
                return buffer;
            }
            public FrequenceDefenseItem this[int i]
            {
                get
                {
                    return _defenseList[i];
                }
                set
                {
                    _defenseList[i] = value;
                }
            }

        };

        public class FrequenceDefenseItem
        {
            public const int LENGTH = 8;
            private ushort[] _values = new ushort[LENGTH];

            private string _name;
            [Browsable(false)]
            [XmlIgnore]
            public string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    _name = value;
                }
            }

            public FrequenceDefenseItem() { }
            public FrequenceDefenseItem(ushort[] buffer)
            {
                SetItem(buffer);
            }

            public override string ToString()
            {
                return "Защита по частоте";
            }
            [XmlAttribute("Значения")]
            [DisplayName("Значения")]
            [Description("Данные в словном виде")]
            [System.ComponentModel.Category("Словный вид")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            public ushort[] Values
            {
                get
                {
                    return _values;
                }
                set
                {
                    SetItem(value);
                }
            }
            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Буфер защиты по частоте", (object)LENGTH, "Буфер частоте должен быть длиной" + LENGTH);
                }
                _values = buffer;
            }

            [XmlAttribute("Режим")]
            [DisplayName("Режим")]
            [System.ComponentModel.Category("Текстовый вид")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            [TypeConverter(typeof(ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    ushort index = Common.GetBits(_values[0], 0, 1, 2, 3);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte)(Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set
                {
                    _values[0] = Common.SetBits(_values[0], (ushort)Strings.Modes.IndexOf(value), 0, 1, 2);
                }
            }
            [XmlAttribute("АВР")]
            [DisplayName("АВР")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool AVR
            {
                get
                {
                    return Common.GetBit(_values[0], 5);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 5, value);
                }
            }
            [XmlAttribute("АПВ")]
            [DisplayName("АПВ")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool APV
            {
                get
                {
                    return Common.GetBit(_values[0], 6);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 6, value);
                }
            }
            [XmlAttribute("УРОВ")]
            [DisplayName("УРОВ")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool UROV
            {
                get
                {
                    return Common.GetBit(_values[0], 7);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 7, value);
                }
            }
            [XmlAttribute("Возврат_АПВ")]
            [DisplayName("АПВ возврата")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool APV_Return
            {
                get
                {
                    return Common.GetBit(_values[0], 15);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 15, value);
                }
            }
            [XmlAttribute("Возврат")]
            [DisplayName("Возврат")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool Return
            {
                get
                {
                    return Common.GetBit(_values[0], 14);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 14, value);
                }
            }
            [XmlAttribute("Блокировка_номер")]
            [DisplayName("Блокировка номер")]
            [Description("Номер входа блокировки")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(ModeTypeConverter))]
            public string BlockingNumber
            {
                get
                {
                    ushort value = (ushort)(_values[1] & 0xFF);
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort)(Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set
                {
                    _values[1] = (ushort)Strings.Logic.IndexOf(value);
                }
            }
            [XmlAttribute("Срабатывание_уставка")]
            [DisplayName("Уставка срабатывания")]
            [System.ComponentModel.Category("Текстовый вид")]
            public double WorkConstraint
            {
                get
                {
                    double ret = 0.0;
                    ret = Measuring.GetConstraint(_values[2], ConstraintKoefficient.K_25600);
                    return ret;
                }
                set
                {
                    _values[2] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
                }
            }
            [XmlAttribute("Срабатывание_время")]
            [DisplayName("Время срабатывания")]
            [Description("Выдержка времени срабатывания")]
            [System.ComponentModel.Category("Текстовый вид")]
            public ulong WorkTime
            {
                get
                {
                    return Measuring.GetTime(_values[3]);
                }
                set
                {
                    _values[3] = Measuring.SetTime(value);
                }
            }

            [XmlAttribute("Срабатывание_АПВ")]
            [DisplayName("Уставка АПВ")]
            [Description("Уставка срабатывания АПВ")]
            [System.ComponentModel.Category("Текстовый вид")]
            public double ConstraintAPV
            {
                get
                {
                    double ret = 0.0;
                    ret = Measuring.GetConstraint(_values[4], ConstraintKoefficient.K_25600);
                    return ret;
                }
                set
                {
                    _values[4] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
                }
            }
            [XmlAttribute("Возврат_время")]
            [DisplayName("Время возврата")]
            [Description("Выдержка времени возврата")]
            [System.ComponentModel.Category("Текстовый вид")]
            public ulong ReturnTime
            {
                get
                {
                    return Measuring.GetTime(_values[5]);
                }
                set
                {
                    _values[5] = Measuring.SetTime(value);
                }
            }
        }

        #endregion
        
        #region Защита двигателя

        private slot _engineDefenses = new slot(0x1120, 0x1133);
        
        public void LoadEngineDefenses()
        {            
            LoadSlot(DeviceNumber, _engineDefenses, "LoadEngineDefenses" + DeviceNumber);
        }

        public void SaveEngineDefenses()
        {
            Array.ConstrainedCopy(EngineDefenses.ToUshort(), 0, _engineDefenses.Value, 0, CEngingeDefenses.LENGTH);
            SaveSlot(DeviceNumber, _engineDefenses, "SaveEngineDefenses" + DeviceNumber);
        }

        public event Handler EngineDefensesLoadOk;
        public event Handler EngineDefensesLoadFail;
        public event Handler EngineDefensesSaveOk;
        public event Handler EngineDefensesSaveFail;


        private CEngingeDefenses _cengineDefenses = new CEngingeDefenses();

        [XmlElement("Защита_двигателя")]
        [DisplayName("Защита двигателя")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("Защиты двигателя")]
        public CEngingeDefenses EngineDefenses
        {
            get
            {
                return _cengineDefenses;
            }
        }

        [XmlElement("Время_нагрева_двигателя")]
        [DisplayName("Время нагрева")]
        [Description("Время нагрева двигателя")]
        [System.ComponentModel.Category("Защиты двигателя")]
        public ushort EngineHeatingTime
        {
            get { return _engineDefenses.Value[0]; }
            set { _engineDefenses.Value[0] = value; }
        }
        [XmlElement("Время_охлаждения_двигателя")]
        [DisplayName("Время охлаждения")]
        [Description("Время охлаждения двигателя")]
        [System.ComponentModel.Category("Защиты двигателя")]
        public ushort EngineCoolingTime
        {
            get { return _engineDefenses.Value[1]; }
            set { _engineDefenses.Value[1] = value; }
        }
        [XmlElement("Ток_двигателя")]
        [DisplayName("Ток двигателя")]
        [Description("Ток двигателя,In")]
        [System.ComponentModel.Category("Защиты двигателя")]
        public double EngineIn
        {
            get { return Measuring.GetConstraint(_engineDefenses.Value[2], ConstraintKoefficient.K_4000); }
            set { _engineDefenses.Value[2] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000); }
        }
        [XmlElement("Уставка_срабатывания_двигателя")]
        [DisplayName("Уставка срабатывания")]
        [Description("Уставка срабатывания,Iп")]
        [System.ComponentModel.Category("Защиты двигателя")]
        public double EngineWorkConstraint
        {
            get { return Measuring.GetConstraint(_engineDefenses.Value[3], ConstraintKoefficient.K_4000); }
            set { _engineDefenses.Value[3] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000); }
        }
        [XmlElement("Время_пуска_двигателя")]
        [DisplayName("Время пуска")]
        [System.ComponentModel.Category("Защиты двигателя")]
        public ulong EnginePuskTime
        {
            get { return Measuring.GetTime(_engineDefenses.Value[4]); }
            set { _engineDefenses.Value[4] = Measuring.SetTime(value); }
        }
        [XmlElement("Гор_состояние_двигателя")]
        [DisplayName("Состояние двигателя")]
        [Description("Состояние двигателя,%")]
        [System.ComponentModel.Category("Защиты двигателя")]
        public double EngineHeatPuskConstraint
        {
            get { return Measuring.GetConstraint(_engineDefenses.Value[5], ConstraintKoefficient.K_25600); }
            set { _engineDefenses.Value[5] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
        }
        [XmlElement("Вход_Q_сброс")]
        [DisplayName("Вход Q сброс")]
        [TypeConverter(typeof(LogicTypeConverter))]
        [System.ComponentModel.Category("Защиты двигателя")]
        public string EngineResetQ
        {
            get
            {
                ushort index = (ushort)(_engineDefenses.Value[6] & 0xFF);
                if (index >= Strings.Logic.Count)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set
            {
                _engineDefenses.Value[6] = (ushort)Strings.Logic.IndexOf(value);
            }
        }
        [XmlElement("Вход_N_пуск")]
        [DisplayName("Вход N пуск")]
        [TypeConverter(typeof(LogicTypeConverter))]
        [System.ComponentModel.Category("Защиты двигателя")]
        public string EnginePuskN
        {
            get
            {
                ushort index = (ushort)(_engineDefenses.Value[7] & 0xFF);
                if (index >= Strings.Logic.Count)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set
            {
                _engineDefenses.Value[7] = (ushort)Strings.Logic.IndexOf(value);
            }
        }
        [XmlElement("Режим_Q")]
        [DisplayName("Режим Q")]
        [TypeConverter(typeof(ModeLightTypeConverter))]
        [System.ComponentModel.Category("Защиты двигателя")]
        public string QMode
        {
            get
            {
                string ret = "";
                bool bit = Common.GetBit(_engineDefenses.Value[12], 0);
                ret = bit ? "Введено" : "Выведено";
                return ret;
            }
            set
            {
                int index = Strings.ModesLight.IndexOf(value);
                _engineDefenses.Value[12] = (0 == index) ? Common.SetBit(_engineDefenses.Value[12], 0, false) : Common.SetBit(_engineDefenses.Value[12], 0, true);
            }
        }
        [XmlElement("Уставка_Q")]
        [DisplayName("Уставка Q")]
        [System.ComponentModel.Category("Защиты двигателя")]
        public double QConstraint
        {
            get { return Measuring.GetConstraint(_engineDefenses.Value[13], ConstraintKoefficient.K_25600); }
            set { _engineDefenses.Value[13] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
        }
        [XmlElement("Время_Q")]
        [DisplayName("Время Q")]
        [System.ComponentModel.Category("Защиты двигателя")]
        public ushort QTime
        {
            get { return _engineDefenses.Value[14]; }
            set { _engineDefenses.Value[14] = value; }
        }
        [XmlElement("N_пуск")]
        [DisplayName("Число пусков")]
        [System.ComponentModel.Category("Защиты двигателя")]
        public ushort BlockingPuskCount
        {
            get { return _engineDefenses.Value[16]; }
            set { _engineDefenses.Value[16] = value; }
        }
        [XmlElement("N_гор")]
        [DisplayName("Число гор.пусков")]
        [System.ComponentModel.Category("Защиты двигателя")]
        public ushort BlockingHeatCount
        {
            get { return _engineDefenses.Value[15]; }
            set { _engineDefenses.Value[15] = value; }
        }
        [XmlElement("T_длит")]
        [DisplayName("Длительность блокировки")]
        [System.ComponentModel.Category("Защиты двигателя")]
        public ushort BlockingDuration
        {
            get { return _engineDefenses.Value[17]; }
            set { _engineDefenses.Value[17] = value; }
        }
        [XmlElement("T_блок")]
        [DisplayName("Время блокировки")]
        [System.ComponentModel.Category("Защиты двигателя")]
        public ushort BlockingTime
        {
            get { return _engineDefenses.Value[18]; }
            set { _engineDefenses.Value[18] = value; }
        }

        public class CEngingeDefenses : ICollection
        {
            public const int LENGTH = 19;
            public const int COUNT = 2;

            private ushort[] _buffer = new ushort[LENGTH];
            private List<EngineDefenseItem> _defenseList = new List<EngineDefenseItem>(COUNT);
                     
            public CEngingeDefenses()
            {
                SetBuffer(new ushort[LENGTH]);
            }
            public void SetBuffer(ushort[] buffer)
            {
                _defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Защиты двигателя", (object)LENGTH, "Буфер для защит двигателя должен быть " + LENGTH);
                }
                _buffer = buffer;
                for (int i = 8; i < 12; i += EngineDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[EngineDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, EngineDefenseItem.LENGTH);
                    _defenseList.Add(new EngineDefenseItem(temp));
                }
                _defenseList[0].Name = "Q>";
                _defenseList[1].Name = "Q>>";
            }
            [DisplayName("Количество")]
            public int Count
            {
                get
                {
                    return _defenseList.Count;
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                Array.ConstrainedCopy(_buffer, 0, buffer, 0, LENGTH);

                for (int i = 0; i < CEngingeDefenses.COUNT; i++)
                {
                    Array.ConstrainedCopy(_defenseList[i].Values, 0, buffer, 8 + EngineDefenseItem.LENGTH * i, EngineDefenseItem.LENGTH);
                }
                return buffer;
            }
            
            public EngineDefenseItem this[int i]
            {
                get
                {
                    return _defenseList[i];
                }
                set
                {
                    _defenseList[i] = value;
                }
            }

            #region ICollection Members

            public void CopyTo(Array array, int index)
            {
                
            }
            
            public void Add(EngineDefenseItem item)
            {
                _defenseList.Add(item);
            }
            [Browsable(false)]
            public bool IsSynchronized
            {
                get {return false; }
            }
            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members
                        
            public IEnumerator GetEnumerator()
            {
                 return _defenseList.GetEnumerator();
            }

            #endregion
        };
        
        public class EngineDefenseItem
        {
            public const int LENGTH = 2;
            private ushort[] _values = new ushort[LENGTH];

            public override string ToString()
            {
                return "Защита двигателя";
            }
            public EngineDefenseItem() { }
            public EngineDefenseItem(ushort[] buffer)
            {
                SetItem(buffer);
            }

            private string _name;
            [Browsable(false)]
            [XmlIgnore]
            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }
	        
       
            [XmlAttribute("Значения")]
            [DisplayName("Значения")]
            [Description("Данные в словном виде")]
            [System.ComponentModel.Category("Словный вид")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            public ushort[] Values
            {
                get
                {
                    return _values;
                }
                set
                {
                    SetItem(value);
                }
            }
            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Буфер защиты по напряжению", (object)LENGTH, "Буфер защит должен быть длиной" + LENGTH);
                }
                _values = buffer;
            }
            [XmlAttribute("Режим")]
            [DisplayName("Режим")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    ushort index = Common.GetBits(_values[0], 0, 1, 2, 3);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte)(Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set
                {
                    _values[0] = Common.SetBits(_values[0], (ushort)Strings.Modes.IndexOf(value), 0, 1, 2);
                }
            }
            [XmlAttribute("АВР")]
            [DisplayName("АВР")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool AVR
            {
                get
                {
                    return Common.GetBit(_values[0], 5);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 5, value);
                }
            }
            [XmlAttribute("АПВ")]
            [DisplayName("АПВ")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool APV
            {
                get
                {
                    return Common.GetBit(_values[0], 6);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 6, value);
                }
            }
            [XmlAttribute("УРОВ")]
            [DisplayName("УРОВ")]
            [System.ComponentModel.Category("Текстовый вид")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool UROV
            {
                get
                {
                    return Common.GetBit(_values[0], 7);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 7, value);
                }
            }
            [XmlAttribute("Уставка")]
            [DisplayName("Уставка срабатывания")]
            [System.ComponentModel.Category("Текстовый вид")]
            public double WorkConstraint
            {
                get
                {
                    double ret = 0.0;
                    ret = Measuring.GetConstraint(_values[1], ConstraintKoefficient.K_25600);
                    return ret;
                }
                set
                {
                    _values[1] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
                }
            }
        }
   
        #endregion

        #region Параметры логики

        

        public void LoadLogic()
        {
            LoadSlot(DeviceNumber, _logic, "LoadLogic" + DeviceNumber);
        }

        #endregion

        #region Дата - время
        private slot _datetime = new slot(0x200, 0x207);
        public void LoadTimeCycle()
        {
            LoadSlotCycle(DeviceNumber, _datetime, "LoadDateTime" + DeviceNumber, new TimeSpan(1000), 10);
        }
        [Browsable(false)]
        public byte[] DateTime
        {
            get
            {
                return Common.TOBYTES(_datetime.Value, true);
            }
            set
            {
                _datetime.Value = Common.TOWORDS(value, true);
            }
        }
#endregion

        #region Функции загрузки/сохранения

        public void LoadDiagnostic()
        {
            LoadBitSlot(DeviceNumber, _diagnostic, "LoadDiagnostic" + DeviceNumber);
        }
                
        public void LoadDiagnosticCycle()
        {
            LoadBitSlotCycle(DeviceNumber, _diagnostic, "LoadDiagnostic" + DeviceNumber, new TimeSpan(1000), 10);
        }

        public void LoadAnalogSignalsCycle()
        {
            LoadSlotCycle(DeviceNumber, _analog, "LoadAnalogSignals" + DeviceNumber, new TimeSpan(1000), 10);
        }

        public void LoadAnalogSignals()
        {
            LoadSlot(DeviceNumber, _analog, "LoadAnalogSignals" + DeviceNumber);
        }

        public void LoadInputSignals()
        {
            LoadSlot(DeviceNumber, _inputSignals, "LoadInputSignals" + DeviceNumber);
        }

        public void LoadOutputSignals()
        {
            LoadSlot(DeviceNumber, _outputSignals, "LoadOutputSignals" + DeviceNumber);
        }

        public void LoadExternalDefenses()
        {
            LoadSlot(DeviceNumber, _externalDefenses, "LoadExternalDefenses" + DeviceNumber);
        }
        
        public void SaveExternalDefenses()
        {
            if (CExternalDefenses.COUNT == ExternalDefenses.Count)
            {
                Array.ConstrainedCopy(ExternalDefenses.ToUshort(), 0, _externalDefenses.Value, 0, CExternalDefenses.LENGTH);
            }
            SaveSlot(DeviceNumber, _externalDefenses, "SaveExternalDefenses" + DeviceNumber);
        }
               
        public void SaveOutputSignals()
        {
            if (COutputRele.COUNT == OutputRele.Count)
            {
                Array.ConstrainedCopy(OutputRele.ToUshort(), 0, _outputSignals.Value, 0x40, COutputRele.LENGTH);    
            }
            if (COutputIndicator.COUNT == OutputIndicator.Count)
            {
                Array.ConstrainedCopy(OutputIndicator.ToUshort(), 0, _outputSignals.Value, 0x60, COutputIndicator.LENGTH);    
            }
            SaveSlot(DeviceNumber, _outputSignals, "SaveOutputSignals" + DeviceNumber);
        }

        public void SaveInputSignals()
        {
            SaveSlot(DeviceNumber, _inputSignals, "SaveInputSignals" + DeviceNumber);
        }
                
        public void RemoveDiagnostic()
        {
            MB.RemoveQuery("LoadDiagnostic" + DeviceNumber);
        }

        public void RemoveAnalogSignals()
        {
            MB.RemoveQuery("LoadAnalogSignals" + DeviceNumber);
        }

        public void MakeDiagnosticQuick()
        {
            MakeQueryQuick("LoadDiagnostic" + DeviceNumber);
        }

        public void MakeAnalogSignalsQuick()
        {
            MakeQueryQuick("LoadAnalogSignals" + DeviceNumber);
        }

        public void MakeDiagnosticSlow()
        {
            MakeQuerySlow("LoadDiagnostic" + DeviceNumber);
        }

        public void MakeAnalogSignalsSlow()
        {
            MakeQuerySlow("LoadAnalogSignals");
        }

        public void RemoveDateTime()
        {
            MB.RemoveQuery("LoadDateTime" + DeviceNumber);
        }
        
        public void MakeDateTimeQuick()
        {
            MakeQueryQuick("LoadDateTime" + DeviceNumber);
        }

        public void MakeDateTimeSlow()
        {
            MakeQuerySlow("LoadDateTime" + DeviceNumber);
        }

        public void SuspendDateTime(bool suspend)
        {
            MB.SuspendQuery("LoadDateTime" + DeviceNumber, suspend);
        }

        public void SuspendSystemJournal(bool suspend)
        {
            List<Query> q = MB.GetQueryByExpression("LoadSJRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.SuspendQuery(q[i].name, suspend);
            }
        }

        public void SuspendAlarmJournal(bool suspend)
        {
            List<Query> q = MB.GetQueryByExpression("LoadALRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.SuspendQuery(q[i].name, suspend);
            }
        }

        public void RemoveAlarmJournal()
        {
            List<Query> q = MB.GetQueryByExpression("LoadALRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.RemoveQuery(q[i].name);
            }
            //_stopAlarmJournal = true;

        }

        public void RemoveSystemJournal()
        {
            List<Query> q = MB.GetQueryByExpression("LoadSJRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.RemoveQuery(q[i].name);
            }
            _stopSystemJournal = true;
        }

        public void SaveDateTime()
        {
            SaveSlot(DeviceNumber, _datetime, "SaveDateTime" + DeviceNumber);
        }

        public void LoadSystemJournal()
        {
            LoadSlot(DeviceNumber, _systemJournal[0], "LoadSJRecord0_" + DeviceNumber);
            _stopSystemJournal = false;
        }

        public void LoadAlarmJournal()
        {
            LoadInputSignals();
            //if (!_inputSignals.Loaded)
            //{
            //    LoadInputSignals();
            //}
            _stopAlarmJournal = false;
            LoadSlot(DeviceNumber, _alarmJournal[0], "LoadALRecord0_" + DeviceNumber);
        }
        #endregion

        #region Сериализация
        public void Deserialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(binFileName);
            }
            catch (XmlException)
            {
               throw new FileLoadException("Файл уставок MR74X поврежден", binFileName);
            }
            
            try
            {
                DeserializeSlot(doc, "/TZL_уставки/Дата_время", _datetime);
                DeserializeSlot(doc, "/TZL_уставки/Входные_сигналы", _inputSignals);
                DeserializeSlot(doc, "/TZL_уставки/Выходные_сигналы", _outputSignals);
                DeserializeSlot(doc, "/TZL_уставки/Диагностика", _diagnostic);
                DeserializeSlot(doc, "/TZL_уставки/Защиты_двигателя", _engineDefenses);
                DeserializeSlot(doc, "/TZL_уставки/Защиты_внешние", _externalDefenses);
                DeserializeSlot(doc, "/TZL_уставки/Защиты_токовые_основные", _tokDefensesMain);
                DeserializeSlot(doc, "/TZL_уставки/Защиты_токовые_резервные", _tokDefensesReserve);
                DeserializeSlot(doc, "/TZL_уставки/Защиты_токовые2_основные", _tokDefenses2Main);
                DeserializeSlot(doc, "/TZL_уставки/Защиты_токовые2_резервные", _tokDefenses2Reserve);
                DeserializeSlot(doc, "/TZL_уставки/Защиты_напряжения_основные", _voltageDefensesMain);
                DeserializeSlot(doc, "/TZL_уставки/Защиты_напряжения_резервные", _voltageDefensesReserve);
                DeserializeSlot(doc, "/TZL_уставки/Автоматика", _automaticsPage);
                DeserializeSlot(doc, "/TZL_уставки/Защиты_частоты_основные", _frequenceDefensesMain);
                DeserializeSlot(doc, "/TZL_уставки/Защиты_частоты_резервные", _frequenceDefensesReserve);
            }
            catch (NullReferenceException)
            {
                throw new FileLoadException("Файл уставок MR74X поврежден", binFileName);
            }
            

            ExternalDefenses.SetBuffer(_externalDefenses.Value);
            ushort[] buffer = new ushort[_tokDefensesReserve.Size + _tokDefenses2Reserve.Size];
            Array.ConstrainedCopy(_tokDefensesReserve.Value, 0, buffer, 0, _tokDefensesReserve.Size);
            Array.ConstrainedCopy(_tokDefenses2Reserve.Value, 0, buffer, _tokDefensesReserve.Size, _tokDefenses2Reserve.Size);
            TokDefensesReserve.SetBuffer(buffer);
            Array.ConstrainedCopy(_tokDefensesMain.Value, 0, buffer, 0, _tokDefensesMain.Size);
            Array.ConstrainedCopy(_tokDefenses2Main.Value, 0, buffer, _tokDefensesMain.Size, _tokDefenses2Main.Size);
            TokDefensesMain.SetBuffer(buffer);
            VoltageDefensesMain.SetBuffer(_voltageDefensesMain.Value);
            VoltageDefensesReserve.SetBuffer(_voltageDefensesReserve.Value);
            EngineDefenses.SetBuffer(_engineDefenses.Value);
            ushort[] releBuffer = new ushort[COutputRele.LENGTH];
            Array.ConstrainedCopy(_outputSignals.Value, 0x40, releBuffer, 0, COutputRele.LENGTH);
            OutputRele.SetBuffer(releBuffer);
            ushort[] outputIndicator = new ushort[COutputIndicator.LENGTH];
            Array.ConstrainedCopy(_outputSignals.Value, 0x60, outputIndicator, 0, COutputIndicator.LENGTH);
            OutputIndicator.SetBuffer(outputIndicator);
            FrequenceDefensesMain.SetBuffer(_frequenceDefensesMain.Value);
            FrequenceDefensesReserve.SetBuffer(_frequenceDefensesReserve.Value);



        }

        void DeserializeSlot(XmlDocument doc, string nodePath, slot slot)
        {
            slot.Value = Common.TOWORDS(Convert.FromBase64String(doc.SelectSingleNode(nodePath).InnerText), true);
        }


        public void Serialize(string binFileName)
        {
            string xmlFileName = System.IO.Path.ChangeExtension(binFileName, ".xml");

            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(this.GetType());
            System.IO.TextWriter writer = new System.IO.StreamWriter(xmlFileName, false, Encoding.UTF8);
            ser.Serialize(writer, this);
            writer.Close();
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("TZL_уставки"));

            Array.ConstrainedCopy(EngineDefenses.ToUshort(), 0, _engineDefenses.Value, 0, CEngingeDefenses.LENGTH);
            Array.ConstrainedCopy(ExternalDefenses.ToUshort(), 0, _externalDefenses.Value, 0, CExternalDefenses.LENGTH);
            Array.ConstrainedCopy(FrequenceDefensesMain.ToUshort(), 0, _frequenceDefensesMain.Value, 0, CFrequenceDefenses.LENGTH);
            Array.ConstrainedCopy(FrequenceDefensesReserve.ToUshort(), 0, _frequenceDefensesReserve.Value, 0, CFrequenceDefenses.LENGTH);
            Array.ConstrainedCopy(OutputRele.ToUshort(), 0, _outputSignals.Value, 0x40, COutputRele.LENGTH);
            Array.ConstrainedCopy(OutputIndicator.ToUshort(), 0, _outputSignals.Value, 0x60, COutputIndicator.LENGTH);
            Array.ConstrainedCopy(TokDefensesMain.ToUshort1(), 0, _tokDefensesMain.Value, 0, CTokDefenses.LENGTH1);
            Array.ConstrainedCopy(TokDefensesMain.ToUshort2(), 0, _tokDefenses2Main.Value, 0, CTokDefenses.LENGTH2);
            Array.ConstrainedCopy(TokDefensesReserve.ToUshort1(), 0, _tokDefensesReserve.Value, 0, CTokDefenses.LENGTH1);
            Array.ConstrainedCopy(TokDefensesReserve.ToUshort2(), 0, _tokDefenses2Reserve.Value, 0, CTokDefenses.LENGTH2);
            Array.ConstrainedCopy(VoltageDefensesMain.ToUshort(), 0, _voltageDefensesMain.Value, 0, CVoltageDefenses.LENGTH);
            Array.ConstrainedCopy(VoltageDefensesReserve.ToUshort(), 0, _voltageDefensesReserve.Value, 0, CVoltageDefenses.LENGTH);
          

            SerializeSlot(doc, "Дата_время", _datetime);
            SerializeSlot(doc, "Входные_сигналы", _inputSignals);
            SerializeSlot(doc, "Выходные_сигналы", _outputSignals);
            SerializeSlot(doc, "Диагностика", _diagnostic);
            SerializeSlot(doc, "Защиты_двигателя", _engineDefenses);
            SerializeSlot(doc, "Защиты_внешние", _externalDefenses);
            SerializeSlot(doc, "Защиты_токовые_основные", _tokDefensesMain);
            SerializeSlot(doc, "Защиты_токовые_резервные", _tokDefensesReserve);
            SerializeSlot(doc, "Защиты_токовые2_основные", _tokDefenses2Main);
            SerializeSlot(doc, "Защиты_токовые2_резервные", _tokDefenses2Reserve);
            SerializeSlot(doc, "Защиты_напряжения_основные", _voltageDefensesMain);
            SerializeSlot(doc, "Защиты_напряжения_резервные", _voltageDefensesReserve);
            SerializeSlot(doc, "Автоматика", _automaticsPage);
            SerializeSlot(doc, "Защиты_частоты_основные", _frequenceDefensesMain);
            SerializeSlot(doc,"Защиты_частоты_резервные",_frequenceDefensesReserve);

            doc.Save(binFileName);
        }

        void SerializeSlot(XmlDocument doc, string nodeName, slot slot)
        {
            XmlElement element = doc.CreateElement(nodeName);
            element.InnerText = Convert.ToBase64String(Common.TOBYTES(slot.Value, true));
            doc.DocumentElement.AppendChild(element);
        }
        #endregion
                     
        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(MR74X); }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Image.FromStream(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("MR74X.Resources.mr100.ico")); }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "MR74X"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion

        #region ISlave Members

        public void CreateRequests()
        {
            byte type = 0;
            byte period = 0;
            byte paramCnt = 0x1;
            byte command = 0x0;

            if (null != ParentDevice)
            {
                MemoryBlocks.Clear();
                Requests.Clear();
                //Запрос на версию
                MemoryManager.Block block1 = (ParentRAM as MemoryManager).CreateBlock(0x10);
                MemoryBlocks.Add(block1);
                ushort addrDB = (ushort)block1.Start;
                Requests.Add(new Request(period, DeviceNumber, type, command, 0x500, addrDB, paramCnt, "Версия(тест)"));
                
            }

        }



        public void RemoveRequests()
        {
            this.Requests.Clear();

            if (null != ParentDevice)
            {
                for (int i = 0; i < MemoryBlocks.Count; i++)
                {
                    MemoryManager.Block block = MemoryBlocks[i] as MemoryManager.Block;
                    (ParentRAM as MemoryManager).DeleteBlock(ref block);
                }
            }
        }



        private ArrayList _requests = new ArrayList();

        [Browsable(false)]
        [XmlIgnore]
        public ArrayList Requests
        {
            get
            {
                return _requests;
            }
            set
            {
                _requests = value;
            }
        }

        ArrayList _memory = new ArrayList();
        [Browsable(false)]
        [XmlIgnore]
        public ArrayList MemoryBlocks
        {
            get
            {
                return _memory;
            }
            set
            {
                _memory = value;
            }
        }

        [Browsable(false)]
        [XmlIgnore]
        public object ParentRAM
        {
            get
            {
                if (null == ParentDevice)
                {
                    return null;
                }
                if (ParentDevice is IPicon)
                {
                    return ((IPicon)ParentDevice).RAM as MemoryManager;
                }
                else
                {
                    throw new DeviceNotSupportedException(ParentDevice.ToString() + " не поддерживает модули");
                }

            }
        }

        [Browsable(false)]
        [XmlIgnore]
        public bool IsManualAddressDB
        {
            get
            {
                return _isManualAddressDB;
            }
            set
            {
                _isManualAddressDB = value;

                if (IsManualAddressDB && 0 != MemoryBlocks.Count)
                {
                    RemoveRequests();
                }
                else
                {
                    CreateRequests();
                }
            }
        }

        [Browsable(false)]
        [XmlIgnore]
        public byte ModuleType
        {
            get
            {
                return 0x0;
            }

        }


        private bool _isManualAddressDB = true;

        [Browsable(false)]
        [XmlIgnore]
        public Image FailImage
        {
            get
            {
                return NodeImage;
            }
        }

        [Browsable(false)]
        [XmlIgnore]
        public Image StatErrorImage
        {
            get
            {
                return NodeImage;
            }
        }

        private Device _parentDevice;

        [Browsable(false)]
        [XmlIgnore]
        public object ParentDevice
        {
            get { return (object)_parentDevice; }
            set { _parentDevice = (Device)value; }
        }

        [Browsable(false)]
        [XmlIgnore]
        public System.Data.DataTable Data
        {
            get
            {
                return new System.Data.DataTable();
            }
        }


        #endregion
        
        #region IDeviceLog Members

        private Hashtable _logHash = new Hashtable();
        private void CreateLogHash()
        {
            _logHash = new Hashtable();
            _logHash.Add("version" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение версии");
            _logHash.Add("LoadFrequenceDefensesReserve" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение резервных защит по частоте");
            _logHash.Add("LoadFrequenceDefensesMain" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение основных защит по частоте");
            _logHash.Add("LoadEngineDefenses" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение защит двигателя");
            _logHash.Add("LoadVoltageDefensesReserve" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение резервных защит по напряжению");
            _logHash.Add("LoadVoltageDefensesMain" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение основных защит по напряжению");
            _logHash.Add("LoadTokDefensesReserve" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение резервных токовых защит №1");
            _logHash.Add("LoadTokDefensesMain" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение основных токовых защит №1");
            _logHash.Add("LoadTokDefensesReserve2" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение резервных токовых защит №2");
            _logHash.Add("LoadTokDefensesMain2" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение основных токовых защит №2");
            _logHash.Add("LoadDiagnostic" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение диагностики");
            _logHash.Add("LoadDateTime" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение даты/времени");
            _logHash.Add("LoadOutputSignals" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение выходных сигналов");
            _logHash.Add("LoadInputSignals" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение входных сигналов");
            _logHash.Add("LoadAnalogSignals" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение аналоговых сигналов");
            _logHash.Add("LoadDiskretSignals" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение дискретных сигналов");
            _logHash.Add("LoadAutomaticsPage" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение автоматики");
            _logHash.Add("LoadExternalDefenses" + DeviceNumber, "MR74X №" + DeviceNumber + " - чтение внешних защит");

            _logHash.Add("SaveFrequenceDefensesReserve" + DeviceNumber, "MR74X №" + DeviceNumber + " - запись резервных защит по частоте");
            _logHash.Add("SaveFrequenceDefensesMain" + DeviceNumber, "MR74X №" + DeviceNumber + " - запись основных защит по частоте");
            _logHash.Add("SaveEngineDefenses" + DeviceNumber, "MR74X №" + DeviceNumber + " - запись защит двигателя");
            _logHash.Add("SaveVoltageDefensesReserve" + DeviceNumber, "MR74X №" + DeviceNumber + " - запись резервных защит по напряжению");
            _logHash.Add("SaveVoltageDefensesMain" + DeviceNumber, "MR74X №" + DeviceNumber + " - запись основных защит по напряжению");
            _logHash.Add("SaveTokDefensesReserve" + DeviceNumber, "MR74X №" + DeviceNumber + " - запись резервных токовых защит №1");
            _logHash.Add("SaveTokDefensesMain" + DeviceNumber, "MR74X №" + DeviceNumber + " - запись основных токовых защит №1");
            _logHash.Add("SaveTokDefensesReserve2" + DeviceNumber, "MR74X №" + DeviceNumber + " - запись резервных токовых защит №2");
            _logHash.Add("SaveTokDefensesMain2" + DeviceNumber, "MR74X №" + DeviceNumber + " - запись основных токовых защит №2");
            _logHash.Add("SaveDateTime" + DeviceNumber, "MR74X №" + DeviceNumber + " - запись даты/времени");
            _logHash.Add("SaveOutputSignals" + DeviceNumber, "MR74X №" + DeviceNumber + " - запись выходных сигналов");
            _logHash.Add("SaveInputSignals" + DeviceNumber, "MR74X №" + DeviceNumber + " - запись входных сигналов");
            _logHash.Add("SaveAutomaticsPage" + DeviceNumber, "MR74X №" + DeviceNumber + " - запись автоматики");
            _logHash.Add("SaveExternalDefenses" + DeviceNumber, "MR74X №" + DeviceNumber + " - запись внешних защит");
        }

        [Browsable(false)]
        [XmlIgnore]
        public Hashtable LogHash
        {
            get {
                //CreateLogHash();
                return _logHash;
            }
        }

        public event LoadHandler PropertiesLoadOk;

        public event LoadHandler PropertiesLoadFail;
        private bool _loadProperties = false;
        public void LoadProperties()
        {
            _loadProperties = true;
            LoadVersion();
            LoadDiagnostic();
            LoadAnalogSignals();
            LoadAutomaticsPage();
            LoadExternalDefenses();
            LoadFrequenceDefenses();
            LoadInputSignals();
            LoadOutputSignals();
            LoadTokDefenses();
            LoadAnalogSignals();
            LoadVoltageDefenses();
         
        }

        public void SaveProperties()
        {            
            SaveDateTime();
            SaveEngineDefenses();
            SaveExternalDefenses();
            SaveFrequenceDefenses();
            SaveInputSignals();
            SaveOutputSignals();
            SaveTokDefenses();
            SaveVoltageDefenses();
            ConfirmConstraint();
        }

        #endregion

        private Oscilloscope _oscilloscope;

        public Oscilloscope Oscilloscope
        {
            get { return _oscilloscope; }
        }

        public void ConfirmConstraint()
        {
            this.SetBit(DeviceNumber, 0, true, "MR74X запрос подтверждения");
        }

        private slot _constraintSelector = new slot(0x400, 0x401);

        public void SelectConstraintGroup(bool mainGroup)
        {
            _constraintSelector.Value[0] = mainGroup ? (ushort)0 : (ushort)1;
            SaveSlot(DeviceNumber, _constraintSelector, "MR74X №" + DeviceNumber + " переключение группы уставок");
        }

        private new void mb_CompleteExchange(object sender, Query query)
        {
            _oscilloscope.OnMbCompleteExchange(sender, query);

            #region Защиты по частоте
            if ("LoadFrequenceDefensesReserve" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    _frequenceDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);
                    FrequenceDefensesReserve.SetBuffer(_frequenceDefensesReserve.Value);
                }
            }
            if ("LoadFrequenceDefensesMain" + DeviceNumber == query.name)
            {
                _frequenceDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
                FrequenceDefensesMain.SetBuffer(_frequenceDefensesMain.Value);
                if (0 == query.fail)
                {
                    if (null != FrequenceDefensesLoadOk)
                    {
                        FrequenceDefensesLoadOk(this);
                    }
                }
                else
                {
                    if (null != FrequenceDefensesLoadFail)
                    {
                        FrequenceDefensesLoadFail(this);
                    }
                }
            }
            if ("SaveFrequenceDefensesMain" + DeviceNumber == query.name)
            {
                Raise(query, FrequenceDefensesSaveOk, FrequenceDefensesSaveFail);
            }
            #endregion
                 
            #region Защиты напряжения
            if ("LoadVoltageDefensesReserve" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    _voltageDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);
                    VoltageDefensesReserve.SetBuffer(_voltageDefensesReserve.Value);
                }
            }
            if ("LoadVoltageDefensesMain" + DeviceNumber == query.name)
            {
                _voltageDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
                VoltageDefensesMain.SetBuffer(_voltageDefensesMain.Value);
                if (0 == query.fail)
                {
                    if (null != VoltageDefensesLoadOk)
                    {
                        VoltageDefensesLoadOk(this);
                    }
                }
                else
                {
                    if (null != VoltageDefensesLoadFail)
                    {
                        VoltageDefensesLoadFail(this);
                    }
                }
                if (_loadProperties)
                {
                    if (null != PropertiesLoadOk)
                    {
                        PropertiesLoadOk(this);
                    }
                    _loadProperties = false;
                }
            }
            if ("SaveVoltageDefensesMain" + DeviceNumber == query.name)
            {
                Raise(query, VoltageDefensesSaveOk, VoltageDefensesSaveFail);
            }
            #endregion

            #region Токовые защиты
            if ("LoadTokDefensesMain2" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    _tokDefenses2Main.Value = Common.TOWORDS(query.readBuffer, true);
                }
            }
            if ("LoadTokDefensesReserve2" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    _tokDefenses2Reserve.Value = Common.TOWORDS(query.readBuffer, true);
                }
            }
            if ("LoadTokDefensesReserve" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    ushort[] buffer = new ushort[_tokDefensesReserve.Size + _tokDefenses2Reserve.Size];
                    _tokDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);
                    Array.ConstrainedCopy(_tokDefensesReserve.Value, 0, buffer, 0, _tokDefensesReserve.Size);
                    Array.ConstrainedCopy(_tokDefenses2Reserve.Value, 0, buffer, _tokDefensesReserve.Size, _tokDefenses2Reserve.Size);
                    _ctokDefensesReserve.SetBuffer(buffer);
                }
            }
            if ("LoadTokDefensesMain" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    ushort[] buffer = new ushort[_tokDefensesMain.Size + _tokDefenses2Main.Size];
                    _tokDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
                    Array.ConstrainedCopy(_tokDefensesMain.Value, 0, buffer, 0, _tokDefensesMain.Size);
                    Array.ConstrainedCopy(_tokDefenses2Main.Value, 0, buffer, _tokDefensesMain.Size, _tokDefenses2Main.Size);
                    _ctokDefensesMain.SetBuffer(buffer);
                    if (null != TokDefensesLoadOK)
                    {
                        TokDefensesLoadOK(this);
                    }
                }
                else
                {
                    if (null != TokDefensesLoadFail)
                    {
                        TokDefensesLoadFail(this);
                    }
                }
            }
            if ("SaveTokDefensesMain" + DeviceNumber == query.name)
            {
                Raise(query, TokDefensesSaveOK, TokDefensesSaveFail);
            }
            #endregion

            #region Журнал аварий

            if (IsIndexQuery(query.name, "LoadALRecord"))
            {
                int index = GetIndex(query.name, "LoadALRecord");
                if (0 == query.fail)
                {
                    _alarmJournalRecords.AddMessage(query.readBuffer);
                    if (_alarmJournalRecords.IsMessageEmpty(index))
                    {
                        if (null != AlarmJournalLoadOk)
                        {
                            AlarmJournalLoadOk(this);
                        }
                    }
                    else
                    {
                        if (null != AlarmJournalRecordLoadOk)
                        {
                            AlarmJournalRecordLoadOk(this, index);
                        }
                        index += 1;
                        if (index < ALARMJOURNAL_RECORD_CNT && !_stopAlarmJournal)
                        {
                            LoadSlot(DeviceNumber, _alarmJournal[index], "LoadALRecord" + index + "_" + DeviceNumber);
                        }
                        else
                        {
                            if (null != AlarmJournalLoadOk)
                            {
                                AlarmJournalLoadOk(this);
                            }
                        }

                    }
                }
                else
                {
                    if (null != AlarmJournalRecordLoadFail)
                    {
                        AlarmJournalRecordLoadFail(this, index);
                    }
                }

            }
            #endregion

            #region Журнал системы
            if (IsIndexQuery(query.name, "LoadSJRecord"))
            {
                int index = GetIndex(query.name, "LoadSJRecord");
                if (0 == query.fail)
                {
                    if (false == _systemJournalRecords.AddMessage(Common.TOWORDS(query.readBuffer, true)))
                    {
                        if (null != SystemJournalLoadOk)
                        {
                            SystemJournalLoadOk(this);
                        }
                    }
                    else
                    {
                        if (null != SystemJournalRecordLoadOk)
                        {
                            SystemJournalRecordLoadOk(this, index);
                        }
                        index += 1;
                        if (index < SYSTEMJOURNAL_RECORD_CNT && !_stopSystemJournal)
                        {
                            LoadSlot(DeviceNumber, _systemJournal[index], "LoadSJRecord" + index + "_" + DeviceNumber);
                        }
                        else
                        {
                            if (null != SystemJournalLoadOk)
                            {
                                SystemJournalLoadOk(this);
                            }
                        }

                    }
                }
                else
                {
                    if (null != SystemJournalRecordLoadFail)
                    {
                        SystemJournalRecordLoadFail(this, index);
                    }
                }

            }
            #endregion

            #region Выходные сигналы
            if ("LoadOutputSignals" + DeviceNumber == query.name)
            {
                _outputSignals.Value = Common.TOWORDS(query.readBuffer, true);
                ushort[] releBuffer = new ushort[COutputRele.LENGTH];
                Array.ConstrainedCopy(_outputSignals.Value, 0x40, releBuffer, 0, COutputRele.LENGTH);
                _outputRele.SetBuffer(releBuffer);

                ushort[] outputIndicator = new ushort[COutputIndicator.LENGTH];
                Array.ConstrainedCopy(_outputSignals.Value, 0x60, outputIndicator, 0, COutputIndicator.LENGTH);
                _outputIndicator.SetBuffer(outputIndicator);

                if (0 == query.fail)
                {
                    if (null != OutputSignalsLoadOK)
                    {
                        OutputSignalsLoadOK(this);
                    }
                }
                else
                {
                    if (null != OutputSignalsLoadFail)
                    {
                        OutputSignalsLoadFail(this);
                    }
                }
            }
            if ("SaveOutputSignals" + DeviceNumber == query.name)
            {
                Raise(query, OutputSignalsSaveOK, OutputSignalsSaveFail);
            }
            #endregion

            #region Внешние защиты
            if ("LoadExternalDefenses" + DeviceNumber == query.name)
            {
                _externalDefenses.Value = Common.TOWORDS(query.readBuffer, true);
                ExternalDefenses.SetBuffer(_externalDefenses.Value);
                if (0 == query.fail)
                {
                    if (null != ExternalDefensesLoadOK)
                    {
                        ExternalDefensesLoadOK(this);
                    }
                }
                else
                {
                    if (null != ExternalDefensesLoadFail)
                    {
                        ExternalDefensesLoadFail(this);
                    }
                }
            }
            if ("SaveExternalDefenses" + DeviceNumber == query.name)
            {
                Raise(query, ExternalDefensesSaveOK, ExternalDefensesSaveFail);
            }
            #endregion

            #region Входные сигналы
            if ("LoadInputSignals" + DeviceNumber == query.name)
            {
                Raise(query, InputSignalsLoadOK, InputSignalsLoadFail, ref _inputSignals);
            }
            if ("SaveInputSignals" + DeviceNumber == query.name)
            {
                Raise(query, InputSignalsSaveOK, InputSignalsSaveFail);
            }
            #endregion 

            #region Ключи
            if ("LoadLogic" + DeviceNumber == query.name)
            {
                Raise(query, LogicLoadOK, LogicLoadFail, ref _logic);
            }
            #endregion

            #region Для страницы автоматики
            if ("LoadAutomaticsPage" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    _automaticsPage.Value = Common.TOWORDS(query.readBuffer, true);
                    AVR_Add_Signals = new BitArray(new byte[] { Common.LOBYTE(_automaticsPage.Value[8]) });
                    if (null != AutomaticsPageLoadOK)
                    {
                        AutomaticsPageLoadOK(this);
                    }
                }
                else
                {
                    if (null != AutomaticsPageLoadFail)
                    {
                        AutomaticsPageLoadFail(this);
                    }
                }
                
            }
            if ("SaveAutomaticsPage" + DeviceNumber == query.name)
            {
                Raise(query, AutomaticsPageSaveOK, AutomaticsPageSaveFail);
            }
            #endregion

            if ("LoadDiagnostic" + DeviceNumber == query.name)
            {
                Raise(query, DiagnosticLoadOk, DiagnosticLoadFail, ref _diagnostic);
            }
            if ("LoadDateTime" + DeviceNumber == query.name)
            {
                Raise(query, DateTimeLoadOk, DateTimeLoadFail, ref _datetime);
            }


            if ("LoadAnalogSignals" + DeviceNumber == query.name)
            {
                if (!_inputSignals.Loaded)
                {
                    LoadInputSignals();
                }
                else
                {
                    Raise(query, AnalogSignalsLoadOK, AnalogSignalsLoadFail, ref _analog);
                }
            }

            base.mb_CompleteExchange(sender, query);
        }
    }
}
