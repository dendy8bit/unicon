using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using BEMN.Devices;
using BEMN.Interfaces;
using BEMN.MR74X;
using BEMN.Forms;

namespace BEMN.MR74X.Forms
{
    public partial class MeasuringForm : Form, IFormView
    {
        private MR74X _device;
        private LedControl[] _manageLeds;
        private LedControl[] _additionalLeds;
        private LedControl[] _indicatorLeds;
        private LedControl[] _inputLeds;
        private LedControl[] _outputLeds;
        private LedControl[] _releLeds;
        private LedControl[] _limitLeds;
        private LedControl[] _faultStateLeds;
        private LedControl[] _faultSignalsLeds;
        private LedControl[] _automationLeds;

        Timer _timer = new Timer();

        public MeasuringForm()
        {
            InitializeComponent();
        }

        public MeasuringForm(MR74X device)
        {
            InitializeComponent();
            _device = device;
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR74X); }
        }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(MeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return (Image)BEMN.MR74X.Properties.Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "���������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void MeasuringForm_Activated(object sender, EventArgs e)
        {
            _device.MakeDiagnosticQuick();
            _device.MakeDateTimeQuick();
            _device.MakeAnalogSignalsQuick();
        }

        private void MeasuringForm_Deactivate(object sender, EventArgs e)
        {
            _device.MakeDiagnosticSlow();
            _device.MakeDateTimeSlow();
            _device.MakeAnalogSignalsSlow();
        }

        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _device.RemoveDiagnostic();
            _device.RemoveDateTime();
            _device.RemoveAnalogSignals();
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            _dateTimeBox.TZLFlag = true;
            _manageLeds = new LedControl[]{_manageLed1,_manageLed2,_manageLed3,_manageLed4,
                                           _manageLed5,_manageLed6,_manageLed7,_manageLed8};
            _additionalLeds = new LedControl[] { _addIndLed1, _addIndLed2, _addIndLed3, _addIndLed4 };
            _indicatorLeds = new LedControl[]{_indLed1,_indLed2,_indLed3,_indLed4,
                                              _indLed5,_indLed6,_indLed7,_indLed8};
            _inputLeds = new LedControl[] { _inD_led1,_inD_led2,_inD_led3,_inD_led4,
                                            _inD_led5,_inD_led6,_inD_led7,_inD_led8,
                                            _inD_led9,_inD_led10,_inD_led11,_inD_led12,
                                            _inD_led13,_inD_led14,_inD_led15,_inD_led16,
                                            _inL_led1,_inL_led2,_inL_led3,_inL_led4,
                                            _inL_led5,_inL_led6,_inL_led7,_inL_led8};
            _outputLeds = new LedControl[]{_outLed1,_outLed2,_outLed3,_outLed4,
                                            _outLed5,_outLed6,_outLed7,_outLed8};
            _releLeds = new LedControl[]{_releLed1,_releLed2,_releLed3,_releLed4,
                                        _releLed5,_releLed6,_releLed7,_releLed8};
            _limitLeds = new LedControl[]{_ImaxLed1,_ImaxLed2,_ImaxLed3,_ImaxLed4,
                                          _ImaxLed5,_ImaxLed6,_ImaxLed7,_ImaxLed8,
                                          _UmaxLed1,_UmaxLed2,_UmaxLed3,_UmaxLed4,
                                          _UmaxLed5,_UmaxLed6,_UmaxLed7,_UmaxLed8,
                                          _I0maxLed1,_I0maxLed2,_I0maxLed3,_I0maxLed4,
                                          _I0maxLed5,_I0maxLed6,_I0maxLed7,_I0maxLed8,
                                          _U0maxLed1,_U0maxLed2,_U0maxLed3,_U0maxLed4,
                                          _U0maxLed5,_U0maxLed6,_U0maxLed7,_U0maxLed8,
                                          _InmaxLed1,_InmaxLed2,_InmaxLed3,_InmaxLed4,
                                          _InmaxLed5,_InmaxLed6,_InmaxLed7,_InmaxLed8,
                                          _FmaxLed1,_FmaxLed2,_FmaxLed3,_FmaxLed4,
                                          _FmaxLed5,_FmaxLed6,_FmaxLed7,_FmaxLed8,
                                          _extDefenseLed1,_extDefenseLed2,_extDefenseLed3,_extDefenseLed4,
                                          _extDefenseLed5,_extDefenseLed6,_extDefenseLed7,_extDefenseLed8};
            _faultStateLeds = new LedControl[]{_faultStateLed1,_faultStateLed2,_faultStateLed3,_faultStateLed4,
                                               _faultStateLed5,_faultStateLed6,_faultStateLed7,_faultStateLed8};
            _faultSignalsLeds = new LedControl[]{_faultSignalLed1,_faultSignalLed2,_faultSignalLed3,_faultSignalLed4,
                                                 _faultSignalLed5,_faultSignalLed6,_faultSignalLed7,_faultSignalLed8,
                                                 _faultSignalLed9,_faultSignalLed10,_faultSignalLed11,_faultSignalLed12,
                                                 _faultSignalLed13,_faultSignalLed14,_faultSignalLed15,_faultSignalLed16};
            _automationLeds = new LedControl[]{_autoLed1,_autoLed2,_autoLed3,_autoLed4,
                                               _autoLed5,_autoLed6,_autoLed7,_autoLed8};


            _device.DiagnosticLoadOk += new Handler(_device_DiagnosticLoadOk);
            _device.DiagnosticLoadFail += new Handler(_device_DiagnosticLoadFail);
            _device.DateTimeLoadOk += new Handler(_device_DateTimeLoadOk);
            _device.DateTimeLoadFail += new Handler(_device_DateTimeLoadFail);
            _device.AnalogSignalsLoadOK += new Handler(_device_AnalogSignalsLoadOK);
            _device.AnalogSignalsLoadFail += new Handler(_device_AnalogSignalsLoadFail);

            _device.LoadAnalogSignalsCycle();
            _device.LoadDiagnosticCycle();
            _device.LoadTimeCycle();

            _timer.Interval = 100;
            _timer.Tick += new EventHandler(_timer_Tick);
        }

        void _device_AnalogSignalsLoadFail(object sender)
        {
            // throw new Exception("The method or operation is not implemented.");
        }

        void _device_AnalogSignalsLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnAnalogSignalsLoadOk));
            }
            catch (InvalidOperationException)
            { }

        }

        private void OnAnalogSignalsLoadOk()
        {
            _InBox.Text = String.Format("In = {0:F2} �", _device.In);
            _IaBox.Text = String.Format("Ia = {0:F2} �", _device.Ia);
            _IbBox.Text = String.Format("Ib = {0:F2} �", _device.Ib);
            _IcBox.Text = String.Format("Ic = {0:F2} �", _device.Ic);
            _I0Box.Text = String.Format("I0 = {0:F2} �", _device.I0);
            _I1Box.Text = String.Format("I1 = {0:F2} �", _device.I1);
            _I2Box.Text = String.Format("I2 = {0:F2} �", _device.I2);
            _IgBox.Text = String.Format("Ig = {0:F2} �", _device.Ig);
            _UnBox.Text = String.Format("Un = {0:F2} �", _device.Un);
            _UaBox.Text = String.Format("Ua = {0:F2} �", _device.Ua);
            _UbBox.Text = String.Format("Ub = {0:F2} �", _device.Ub);
            _UcBox.Text = String.Format("Uc = {0:F2} �", _device.Uc);
            _UabBox.Text = String.Format("Uab = {0:F2} �", _device.Uab);
            _UbcBox.Text = String.Format("Ubc = {0:F2} �", _device.Ubc);
            _UcaBox.Text = String.Format("Uca = {0:F2} �", _device.Uca);
            _U0Box.Text = String.Format("U0 = {0:F2} �", _device.U0);
            _U1Box.Text = String.Format("U1 = {0:F2} �", _device.U1);
            _U2Box.Text = String.Format("U2 = {0:F2} �", _device.U2);
            _F_Box.Text = String.Format("F = {0:F2} ��", _device.F);


        }

        void _device_DateTimeLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnDateTimeLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnDateTimeLoadFail()
        {
            _dateTimeBox.Text = "";
        }

        void _device_DateTimeLoadOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnDateTimeLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnDateTimeLoadOk()
        {
            _dateTimeBox.DateTime = _device.DateTime;
        }

        void _device_DiagnosticLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnDiagnosticLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnDiagnosticLoadFail()
        {
            LedManager.TurnOffLeds(_manageLeds);
            LedManager.TurnOffLeds(_additionalLeds);
            LedManager.TurnOffLeds(_indicatorLeds);
            LedManager.TurnOffLeds(_inputLeds);
            LedManager.TurnOffLeds(_outputLeds);
            LedManager.TurnOffLeds(_releLeds);
            LedManager.TurnOffLeds(_limitLeds);
            LedManager.TurnOffLeds(_automationLeds);
            LedManager.TurnOffLeds(_faultSignalsLeds);
            LedManager.TurnOffLeds(_faultStateLeds);

        }

        void _device_DiagnosticLoadOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnDiagnosticLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnDiagnosticLoadOk()
        {
            LedManager.SetLeds(_manageLeds, _device.ManageSignals);
            LedManager.SetLeds(_additionalLeds, _device.AdditionalSignals);
            LedManager.SetLeds(_indicatorLeds, _device.Indicators);
            LedManager.SetLeds(_inputLeds, _device.InputSignals);
            LedManager.SetLeds(_outputLeds, _device.OutputSignals);
            LedManager.SetLeds(_releLeds, _device.Rele);
            LedManager.SetLeds(_limitLeds, _device.LimitSignals);
            LedManager.SetLeds(_automationLeds, _device.Automation);
            LedManager.SetLeds(_faultSignalsLeds, _device.FaultSignals);
            LedManager.SetLeds(_faultStateLeds, _device.FaultState);


        }

        private void _readTimeCheck_CheckedChanged(object sender, EventArgs e)
        {
            _device.SuspendDateTime(!_readTimeCheck.Checked);
            if (_readTimeCheck.Checked)
            {
                _systemTimeCheck.Checked = false;
            }
        }

        private void _writeTimeBut_Click(object sender, EventArgs e)
        {
            if ((_dateTimeBox.DateTime[3] < 13 && _dateTimeBox.DateTime[3] > 0) &&(_dateTimeBox.DateTime[5] < 32 && _dateTimeBox.DateTime[5] > 0) &&(_dateTimeBox.DateTime[7] < 24 && _dateTimeBox.DateTime[7] >= 0) &&
                (_dateTimeBox.DateTime[9] < 60 && _dateTimeBox.DateTime[9] >= 0)&&(_dateTimeBox.DateTime[11] < 60 && _dateTimeBox.DateTime[11] >= 0))
            {
                _device.DateTime = _dateTimeBox.DateTime;
                _device.SaveDateTime();
                _readTimeCheck.Checked = _systemTimeCheck.Checked = false;
            }
            else
            {
                MessageBox.Show("���������� ������� ����");
            }
        }

        private void _systemTimeCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (_systemTimeCheck.Checked)
            {
                _readTimeCheck.Checked = false;
                _timer.Start();
            }
            else
            {
                _timer.Stop();
            }


        }

        void _timer_Tick(object sender, EventArgs e)
        {
            string[] dates = System.Text.RegularExpressions.Regex.Split(DateTime.Now.ToShortDateString(), "\\.");
            if (null != dates[2]) //Year
            {
                dates[2] = dates[2].Remove(0, 2);
            }


            _dateTimeBox.Text = String.Concat(dates[0], dates[1], dates[2]) + DateTime.Now.ToLongTimeString() + DateTime.Now.Millisecond.ToString();
        }

        private void _breakerOnBut_Click(object sender, EventArgs e)
        {
            ConfirmSDTU(0x1801, "�������� �����������");
        }

        private void ConfirmSDTU(ushort address, string msg)
        {
            if (DialogResult.Yes == MessageBox.Show(msg + " ?", "������-MR74X", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                _device.SetBit(_device.DeviceNumber, address, true, msg + _device.DeviceNumber);
            }
        }

        private void _breakerOffBut_Click(object sender, EventArgs e)
        {
            ConfirmSDTU(0x1800, "��������� �����������");
        }

        private void _resetFaultBut_Click(object sender, EventArgs e)
        {
            ConfirmSDTU(0x1805, "�������� �������������");
        }

        private void _resetJS_But_Click(object sender, EventArgs e)
        {
            ConfirmSDTU(0x1806, "������������� ������� �������");
        }

        private void _resetJA_But_Click(object sender, EventArgs e)
        {
            ConfirmSDTU(0x1807, "������������� ������� ������");
        }

        private void _resetIndicatBut_Click(object sender, EventArgs e)
        {
            ConfirmSDTU(0x1804, "�������� ���������");
        }

        private void _selectConstraintGroupBut_Click(object sender, EventArgs e)
        {
            string msg = "";
            bool constraintGroup = false;
            if (_manageLed4.State == LedState.NoSignaled)
            {
                constraintGroup = true;
                msg = "����������� �� �������� ������";
            }
            else if (_manageLed4.State == LedState.Signaled)
            {
                constraintGroup = false;
                msg = "����������� �� ��������� ������";
            }
            else
            {
                return;
            }
            if (DialogResult.Yes == MessageBox.Show(msg + " ?", "������-MR74X", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                _device.SelectConstraintGroup(constraintGroup);
            }
        }

    }
}