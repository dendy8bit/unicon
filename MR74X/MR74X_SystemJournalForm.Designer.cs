namespace BEMN.TZL.Forms
{
    partial class SystemJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._saveSysJournalDlg = new System.Windows.Forms.SaveFileDialog();
            this._openSysJounralDlg = new System.Windows.Forms.OpenFileDialog();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this._journalCntLabel = new System.Windows.Forms.Label();
            this._journalProgress = new System.Windows.Forms.ProgressBar();
            this._deserializeSysJournalBut = new System.Windows.Forms.Button();
            this._serializeSysJournalBut = new System.Windows.Forms.Button();
            this._readSysJournalBut = new System.Windows.Forms.Button();
            this._sysJournalGrid = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this._sysJournalGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // _saveSysJournalDlg
            // 
            this._saveSysJournalDlg.DefaultExt = "xml";
            this._saveSysJournalDlg.FileName = "TZL_�������������";
            this._saveSysJournalDlg.Filter = "TZL- ������ ������� | *.xml";
            this._saveSysJournalDlg.Title = "���������  ������ ������� ��� TZL";
            // 
            // _openSysJounralDlg
            // 
            this._openSysJounralDlg.DefaultExt = "xml";
            this._openSysJounralDlg.Filter = "(*.xml) | *.xml";
            this._openSysJounralDlg.RestoreDirectory = true;
            this._openSysJounralDlg.Title = "������� ������ ������� ��� TZL";
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.HeaderText = "�����";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.Width = 65;
            // 
            // _indexCol
            // 
            this._indexCol.HeaderText = "�";
            this._indexCol.Name = "_indexCol";
            this._indexCol.Width = 30;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._msgCol.HeaderText = "���������";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "xml";
            this.saveFileDialog1.FileName = "TZL_�������������";
            this.saveFileDialog1.Filter = "TZL- ������ ������� | *.xml";
            this.saveFileDialog1.Title = "���������  ������ ������� ��� TZL";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "xml";
            this.openFileDialog1.Filter = "(*.xml) | *.xml";
            this.openFileDialog1.RestoreDirectory = true;
            this.openFileDialog1.Title = "������� ������ ������� ��� TZL";
            // 
            // _journalCntLabel
            // 
            this._journalCntLabel.AutoSize = true;
            this._journalCntLabel.Location = new System.Drawing.Point(111, 345);
            this._journalCntLabel.Name = "_journalCntLabel";
            this._journalCntLabel.Size = new System.Drawing.Size(13, 13);
            this._journalCntLabel.TabIndex = 17;
            this._journalCntLabel.Text = "0";
            // 
            // _journalProgress
            // 
            this._journalProgress.Location = new System.Drawing.Point(6, 343);
            this._journalProgress.Name = "_journalProgress";
            this._journalProgress.Size = new System.Drawing.Size(100, 17);
            this._journalProgress.TabIndex = 16;
            // 
            // _deserializeSysJournalBut
            // 
            this._deserializeSysJournalBut.Location = new System.Drawing.Point(236, 311);
            this._deserializeSysJournalBut.Name = "_deserializeSysJournalBut";
            this._deserializeSysJournalBut.Size = new System.Drawing.Size(126, 23);
            this._deserializeSysJournalBut.TabIndex = 15;
            this._deserializeSysJournalBut.Text = "��������� �� �����";
            this._deserializeSysJournalBut.UseVisualStyleBackColor = true;
            this._deserializeSysJournalBut.Click += new System.EventHandler(this._deserializeSysJournalBut_Click);
            // 
            // _serializeSysJournalBut
            // 
            this._serializeSysJournalBut.Location = new System.Drawing.Point(103, 311);
            this._serializeSysJournalBut.Name = "_serializeSysJournalBut";
            this._serializeSysJournalBut.Size = new System.Drawing.Size(111, 23);
            this._serializeSysJournalBut.TabIndex = 14;
            this._serializeSysJournalBut.Text = "��������� � ����";
            this._serializeSysJournalBut.UseVisualStyleBackColor = true;
            this._serializeSysJournalBut.Click += new System.EventHandler(this._serializeSysJournalBut_Click);
            // 
            // _readSysJournalBut
            // 
            this._readSysJournalBut.Location = new System.Drawing.Point(5, 311);
            this._readSysJournalBut.Name = "_readSysJournalBut";
            this._readSysJournalBut.Size = new System.Drawing.Size(75, 23);
            this._readSysJournalBut.TabIndex = 13;
            this._readSysJournalBut.Text = "���������";
            this._readSysJournalBut.UseVisualStyleBackColor = true;
            this._readSysJournalBut.Click += new System.EventHandler(this._readSysJournalBut_Click);
            // 
            // _sysJournalGrid
            // 
            this._sysJournalGrid.AllowUserToAddRows = false;
            this._sysJournalGrid.AllowUserToDeleteRows = false;
            this._sysJournalGrid.BackgroundColor = System.Drawing.Color.White;
            this._sysJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._sysJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msgCol});
            this._sysJournalGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this._sysJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._sysJournalGrid.Name = "_sysJournalGrid";
            this._sysJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._sysJournalGrid.Size = new System.Drawing.Size(372, 303);
            this._sysJournalGrid.TabIndex = 12;
            // 
            // SystemJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 362);
            this.Controls.Add(this._journalCntLabel);
            this.Controls.Add(this._journalProgress);
            this.Controls.Add(this._deserializeSysJournalBut);
            this.Controls.Add(this._serializeSysJournalBut);
            this.Controls.Add(this._readSysJournalBut);
            this.Controls.Add(this._sysJournalGrid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "SystemJournalForm";
            this.Text = "SystemJournalForm";
            this.Deactivate += new System.EventHandler(this.SystemJournalForm_Deactivate);
            this.Activated += new System.EventHandler(this.SystemJournalForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SystemJournalForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this._sysJournalGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog _saveSysJournalDlg;
        private System.Windows.Forms.OpenFileDialog _openSysJounralDlg;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label _journalCntLabel;
        private System.Windows.Forms.ProgressBar _journalProgress;
        private System.Windows.Forms.Button _deserializeSysJournalBut;
        private System.Windows.Forms.Button _serializeSysJournalBut;
        private System.Windows.Forms.Button _readSysJournalBut;
        private System.Windows.Forms.DataGridView _sysJournalGrid;
    }
}