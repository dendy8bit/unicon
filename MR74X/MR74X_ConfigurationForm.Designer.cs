namespace BEMN.TZL.Forms
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            this._tokDefense3SpeedupCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3SpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3FeatureCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3WorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3UROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseGrid2 = new System.Windows.Forms.DataGridView();
            this._tokDefense2NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense2ModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense2BlockNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense2PuskUCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense2PuskConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense2DirectionCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense2BlockExistCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense2ParameterCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense2WorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense2FeatureCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense2WorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense2EngineCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense2UROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense2APVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense2AVRCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3APVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3AVRCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseBlockExistCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseParameterCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefensePuskConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseDirectionCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseWorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseSpeedUpCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseSpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseFeatureCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseWorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseGrid1 = new System.Windows.Forms.DataGridView();
            this._tokDefenseNameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseBlockNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseU_PuskCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseUROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseAPVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseAVRCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3WorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseI2box = new System.Windows.Forms.MaskedTextBox();
            this.label23 = new System.Windows.Forms.Label();
            this._tokDefenseI0box = new System.Windows.Forms.MaskedTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this._tokDefenseIbox = new System.Windows.Forms.MaskedTextBox();
            this._tokDefense4NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4ModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.label21 = new System.Windows.Forms.Label();
            this._tokDefenseGrid4 = new System.Windows.Forms.DataGridView();
            this._tokDefense4BlockNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense4UpuskCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4PuskConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4WorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4WorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4SpeedupCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4SpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4UROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4APVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4AVRCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefensesPage = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this._tokDefenseMainConstraintRadio = new System.Windows.Forms.RadioButton();
            this._tokDefenseReserveConstraintRadio = new System.Windows.Forms.RadioButton();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this._tokDefenseInbox = new System.Windows.Forms.MaskedTextBox();
            this.label24 = new System.Windows.Forms.Label();
            this._tokDefenseGrid3 = new System.Windows.Forms.DataGridView();
            this._tokDefense3NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3ModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3BlockingNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3UpuskCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3PuskConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3DirectionCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3BlockingExistCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3ParameterCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.apv_blocking = new System.Windows.Forms.ComboBox();
            this.apv_conf = new System.Windows.Forms.ComboBox();
            this._frequenceDefensesGrid = new System.Windows.Forms.DataGridView();
            this._frequenceDefensesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesMode = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesBlockingNumber = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesWorkConstraint = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesWorkTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesReturn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesAPVReturn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesReturnTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesConstraintAPV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesUROV = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesAPV = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesAVR = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAVRcol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequencePage = new System.Windows.Forms.TabPage();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this._frequenceDefenseMainConstraintRadio = new System.Windows.Forms.RadioButton();
            this._frequenceDefenseReserveConstraintRadio = new System.Windows.Forms.RadioButton();
            this._voltageDefensesTimeConstraintCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnCol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesParameterCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesWorkConstraintCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesAPVreturnCol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesUROVcol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVcol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesReturnConstraintCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnTimeCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._TT_Box = new System.Windows.Forms.MaskedTextBox();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._processLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._exchangeProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._voltageDefensesBlockingNumberCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesWorkConstraintCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesWorkTimeCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesBlockingNumberCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesParameterCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesReturnCol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesReturnTimeCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesUROVcol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVreturnCol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesReturnConstraintCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesPage = new System.Windows.Forms.TabPage();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this._voltageDefenseMainConstraintRadio = new System.Windows.Forms.RadioButton();
            this._voltageDefenseReservConstraintRadio = new System.Windows.Forms.RadioButton();
            this._voltageDefensesGrid2 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesAPVcol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAVRcol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesGrid1 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesBlockingNumberCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesParameterCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesWorkConstraintCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesWorkTimeCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVReturlCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesReturnConstraintCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnTimeCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesUROVCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAVRCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesGrid3 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._constraintGroupCombo = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._signalizationCombo = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this._extOffCombo = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this._extOnCombo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this._keyOffCombo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this._keyOnCombo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this._switcherImpulseBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherBlockCombo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this._switcherTimeBox = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this._switcherErrorCombo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this._switcherStateOnCombo = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this._switcherStateOffCombo = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this._TNNP_dispepairCombo = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this._TN_dispepairCombo = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._maxTok_Box = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._TTNP_Box = new System.Windows.Forms.MaskedTextBox();
            this._TNNP_Box = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this._TN_Box = new System.Windows.Forms.MaskedTextBox();
            this._TN_typeCombo = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label42 = new System.Windows.Forms.Label();
            this._switcherTokBox = new System.Windows.Forms.MaskedTextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.apv_time_4krat = new System.Windows.Forms.MaskedTextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.apv_time_3krat = new System.Windows.Forms.MaskedTextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._manageSignalsSDTU_Combo = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this._manageSignalsExternalCombo = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this._manageSignalsKeyCombo = new System.Windows.Forms.ComboBox();
            this.label45 = new System.Windows.Forms.Label();
            this._manageSignalsButtonCombo = new System.Windows.Forms.ComboBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this._inSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._HUD_Box = new System.Windows.Forms.MaskedTextBox();
            this._OMP_TypeBox = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this._releDispepairBox = new System.Windows.Forms.MaskedTextBox();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._dispepairCheckList = new System.Windows.Forms.CheckedListBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._applyLogicSignalsBut = new System.Windows.Forms.Button();
            this._logicChannelsCombo = new System.Windows.Forms.ComboBox();
            this._logicSignalsDataGrid = new System.Windows.Forms.DataGridView();
            this._diskretChannelCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._diskretValueCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this._switcherDurationBox = new System.Windows.Forms.MaskedTextBox();
            this.apv_time_2krat = new System.Windows.Forms.MaskedTextBox();
            this._tabControl = new System.Windows.Forms.TabControl();
            this._outputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._outputLogicCheckList = new System.Windows.Forms.CheckedListBox();
            this._outputLogicAcceptBut = new System.Windows.Forms.Button();
            this._outputLogicCombo = new System.Windows.Forms.ComboBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._outputIndicatorsGrid = new System.Windows.Forms.DataGridView();
            this._outIndNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outIndTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndResetCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndAlarmCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndSystemCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releImpulseCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDefensePage = new System.Windows.Forms.TabPage();
            this._externalDefenseGrid = new System.Windows.Forms.DataGridView();
            this._extDefNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefBlockingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefWorkingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefWorkingTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefReturnCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAPVreturnCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefReturnNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefReturnTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefUROVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAPVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAVRcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._automaticPage = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.avr_disconnection = new System.Windows.Forms.MaskedTextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.avr_time_return = new System.Windows.Forms.MaskedTextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.avr_time_abrasion = new System.Windows.Forms.MaskedTextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.avr_return = new System.Windows.Forms.ComboBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.avr_abrasion = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this.avr_reset_blocking = new System.Windows.Forms.ComboBox();
            this.avr_permit_reset_switch = new System.Windows.Forms.CheckBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.avr_abrasion_switch = new System.Windows.Forms.CheckBox();
            this.avr_supply_off = new System.Windows.Forms.CheckBox();
            this.avr_self_off = new System.Windows.Forms.CheckBox();
            this.avr_switch_off = new System.Windows.Forms.CheckBox();
            this.avr_blocking = new System.Windows.Forms.ComboBox();
            this.avr_start = new System.Windows.Forms.ComboBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label57 = new System.Windows.Forms.Label();
            this.lzsh_constraint = new System.Windows.Forms.MaskedTextBox();
            this.lzsh_conf = new System.Windows.Forms.ComboBox();
            this.label48 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.label54 = new System.Windows.Forms.Label();
            this.apv_time_1krat = new System.Windows.Forms.MaskedTextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.apv_time_ready = new System.Windows.Forms.MaskedTextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.apv_time_blocking = new System.Windows.Forms.MaskedTextBox();
            this.apv_self_off = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid4)).BeginInit();
            this._tokDefensesPage.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._frequenceDefensesGrid)).BeginInit();
            this._frequencePage.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this._statusStrip.SuspendLayout();
            this._voltageDefensesPage.SuspendLayout();
            this.groupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid3)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this._inSignalsPage.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid)).BeginInit();
            this.groupBox4.SuspendLayout();
            this._tabControl.SuspendLayout();
            this._outputSignalsPage.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this._externalDefensePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).BeginInit();
            this._automaticPage.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tokDefense3SpeedupCol
            // 
            this._tokDefense3SpeedupCol.HeaderText = "���.";
            this._tokDefense3SpeedupCol.Name = "_tokDefense3SpeedupCol";
            this._tokDefense3SpeedupCol.Width = 36;
            // 
            // _tokDefense3SpeedupTimeCol
            // 
            dataGridViewCellStyle1.NullValue = null;
            this._tokDefense3SpeedupTimeCol.DefaultCellStyle = dataGridViewCellStyle1;
            this._tokDefense3SpeedupTimeCol.HeaderText = "t �����.";
            this._tokDefense3SpeedupTimeCol.Name = "_tokDefense3SpeedupTimeCol";
            this._tokDefense3SpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3SpeedupTimeCol.Width = 51;
            // 
            // _tokDefense3FeatureCol
            // 
            this._tokDefense3FeatureCol.HeaderText = "���-��";
            this._tokDefense3FeatureCol.Name = "_tokDefense3FeatureCol";
            this._tokDefense3FeatureCol.Width = 47;
            // 
            // _tokDefense3WorkTimeCol
            // 
            dataGridViewCellStyle2.NullValue = null;
            this._tokDefense3WorkTimeCol.DefaultCellStyle = dataGridViewCellStyle2;
            this._tokDefense3WorkTimeCol.HeaderText = "t ����.";
            this._tokDefense3WorkTimeCol.Name = "_tokDefense3WorkTimeCol";
            this._tokDefense3WorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3WorkTimeCol.Width = 46;
            // 
            // _tokDefense3UROVCol
            // 
            this._tokDefense3UROVCol.HeaderText = "����";
            this._tokDefense3UROVCol.Name = "_tokDefense3UROVCol";
            this._tokDefense3UROVCol.Width = 43;
            // 
            // _tokDefenseGrid2
            // 
            this._tokDefenseGrid2.AllowUserToAddRows = false;
            this._tokDefenseGrid2.AllowUserToDeleteRows = false;
            this._tokDefenseGrid2.AllowUserToResizeColumns = false;
            this._tokDefenseGrid2.AllowUserToResizeRows = false;
            this._tokDefenseGrid2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._tokDefenseGrid2.BackgroundColor = System.Drawing.Color.White;
            this._tokDefenseGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefense2NameCol,
            this._tokDefense2ModeCol,
            this._tokDefense2BlockNumberCol,
            this._tokDefense2PuskUCol,
            this._tokDefense2PuskConstraintCol,
            this._tokDefense2DirectionCol,
            this._tokDefense2BlockExistCol,
            this._tokDefense2ParameterCol,
            this._tokDefense2WorkConstraintCol,
            this._tokDefense2FeatureCol,
            this._tokDefense2WorkTimeCol,
            this._tokDefense2EngineCol,
            this._tokDefense2UROVCol,
            this._tokDefense2APVCol,
            this._tokDefense2AVRCol});
            this._tokDefenseGrid2.Location = new System.Drawing.Point(8, 245);
            this._tokDefenseGrid2.Name = "_tokDefenseGrid2";
            this._tokDefenseGrid2.RowHeadersVisible = false;
            this._tokDefenseGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid2.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this._tokDefenseGrid2.RowTemplate.Height = 24;
            this._tokDefenseGrid2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this._tokDefenseGrid2.Size = new System.Drawing.Size(709, 79);
            this._tokDefenseGrid2.TabIndex = 1;
            // 
            // _tokDefense2NameCol
            // 
            this._tokDefense2NameCol.Frozen = true;
            this._tokDefense2NameCol.HeaderText = "";
            this._tokDefense2NameCol.Name = "_tokDefense2NameCol";
            this._tokDefense2NameCol.ReadOnly = true;
            this._tokDefense2NameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense2NameCol.Width = 5;
            // 
            // _tokDefense2ModeCol
            // 
            this._tokDefense2ModeCol.HeaderText = "�����";
            this._tokDefense2ModeCol.Name = "_tokDefense2ModeCol";
            this._tokDefense2ModeCol.Width = 48;
            // 
            // _tokDefense2BlockNumberCol
            // 
            this._tokDefense2BlockNumberCol.HeaderText = "����������";
            this._tokDefense2BlockNumberCol.Name = "_tokDefense2BlockNumberCol";
            this._tokDefense2BlockNumberCol.Width = 74;
            // 
            // _tokDefense2PuskUCol
            // 
            this._tokDefense2PuskUCol.HeaderText = "U ����";
            this._tokDefense2PuskUCol.Name = "_tokDefense2PuskUCol";
            this._tokDefense2PuskUCol.Width = 47;
            // 
            // _tokDefense2PuskConstraintCol
            // 
            dataGridViewCellStyle3.NullValue = null;
            this._tokDefense2PuskConstraintCol.DefaultCellStyle = dataGridViewCellStyle3;
            this._tokDefense2PuskConstraintCol.HeaderText = "���. �����";
            this._tokDefense2PuskConstraintCol.Name = "_tokDefense2PuskConstraintCol";
            this._tokDefense2PuskConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense2PuskConstraintCol.Width = 67;
            // 
            // _tokDefense2DirectionCol
            // 
            this._tokDefense2DirectionCol.HeaderText = "�����������";
            this._tokDefense2DirectionCol.Name = "_tokDefense2DirectionCol";
            this._tokDefense2DirectionCol.Width = 81;
            // 
            // _tokDefense2BlockExistCol
            // 
            this._tokDefense2BlockExistCol.HeaderText = "����������";
            this._tokDefense2BlockExistCol.Name = "_tokDefense2BlockExistCol";
            this._tokDefense2BlockExistCol.Width = 74;
            // 
            // _tokDefense2ParameterCol
            // 
            this._tokDefense2ParameterCol.HeaderText = "��������";
            this._tokDefense2ParameterCol.Name = "_tokDefense2ParameterCol";
            this._tokDefense2ParameterCol.Width = 64;
            // 
            // _tokDefense2WorkConstraintCol
            // 
            dataGridViewCellStyle4.NullValue = null;
            this._tokDefense2WorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle4;
            this._tokDefense2WorkConstraintCol.HeaderText = "���.����.";
            this._tokDefense2WorkConstraintCol.Name = "_tokDefense2WorkConstraintCol";
            this._tokDefense2WorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense2WorkConstraintCol.Width = 62;
            // 
            // _tokDefense2FeatureCol
            // 
            this._tokDefense2FeatureCol.HeaderText = "���-��";
            this._tokDefense2FeatureCol.Name = "_tokDefense2FeatureCol";
            this._tokDefense2FeatureCol.Width = 47;
            // 
            // _tokDefense2WorkTimeCol
            // 
            dataGridViewCellStyle5.NullValue = null;
            this._tokDefense2WorkTimeCol.DefaultCellStyle = dataGridViewCellStyle5;
            this._tokDefense2WorkTimeCol.HeaderText = "t ����.";
            this._tokDefense2WorkTimeCol.Name = "_tokDefense2WorkTimeCol";
            this._tokDefense2WorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense2WorkTimeCol.Width = 46;
            // 
            // _tokDefense2EngineCol
            // 
            this._tokDefense2EngineCol.HeaderText = "���������";
            this._tokDefense2EngineCol.Name = "_tokDefense2EngineCol";
            this._tokDefense2EngineCol.Width = 68;
            // 
            // _tokDefense2UROVCol
            // 
            this._tokDefense2UROVCol.HeaderText = "����";
            this._tokDefense2UROVCol.Name = "_tokDefense2UROVCol";
            this._tokDefense2UROVCol.Width = 43;
            // 
            // _tokDefense2APVCol
            // 
            this._tokDefense2APVCol.HeaderText = "���";
            this._tokDefense2APVCol.Name = "_tokDefense2APVCol";
            this._tokDefense2APVCol.Width = 35;
            // 
            // _tokDefense2AVRCol
            // 
            this._tokDefense2AVRCol.HeaderText = "���";
            this._tokDefense2AVRCol.Name = "_tokDefense2AVRCol";
            this._tokDefense2AVRCol.Width = 34;
            // 
            // _tokDefense3APVCol
            // 
            this._tokDefense3APVCol.HeaderText = "���";
            this._tokDefense3APVCol.Name = "_tokDefense3APVCol";
            this._tokDefense3APVCol.Width = 35;
            // 
            // _tokDefense3AVRCol
            // 
            this._tokDefense3AVRCol.HeaderText = "���";
            this._tokDefense3AVRCol.Name = "_tokDefense3AVRCol";
            this._tokDefense3AVRCol.Width = 34;
            // 
            // _tokDefenseBlockExistCol
            // 
            this._tokDefenseBlockExistCol.HeaderText = "����������";
            this._tokDefenseBlockExistCol.Name = "_tokDefenseBlockExistCol";
            this._tokDefenseBlockExistCol.Width = 74;
            // 
            // _tokDefenseParameterCol
            // 
            this._tokDefenseParameterCol.HeaderText = "��������";
            this._tokDefenseParameterCol.Name = "_tokDefenseParameterCol";
            this._tokDefenseParameterCol.Width = 64;
            // 
            // _tokDefensePuskConstraintCol
            // 
            dataGridViewCellStyle7.NullValue = null;
            this._tokDefensePuskConstraintCol.DefaultCellStyle = dataGridViewCellStyle7;
            this._tokDefensePuskConstraintCol.HeaderText = "���. �����";
            this._tokDefensePuskConstraintCol.Name = "_tokDefensePuskConstraintCol";
            this._tokDefensePuskConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefensePuskConstraintCol.Width = 67;
            // 
            // _tokDefenseDirectionCol
            // 
            this._tokDefenseDirectionCol.HeaderText = "�����������";
            this._tokDefenseDirectionCol.Name = "_tokDefenseDirectionCol";
            this._tokDefenseDirectionCol.Width = 81;
            // 
            // _tokDefenseWorkConstraintCol
            // 
            dataGridViewCellStyle8.NullValue = null;
            this._tokDefenseWorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle8;
            this._tokDefenseWorkConstraintCol.HeaderText = "���.����.";
            this._tokDefenseWorkConstraintCol.Name = "_tokDefenseWorkConstraintCol";
            this._tokDefenseWorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseWorkConstraintCol.Width = 62;
            // 
            // _tokDefenseSpeedUpCol
            // 
            this._tokDefenseSpeedUpCol.HeaderText = "���.";
            this._tokDefenseSpeedUpCol.Name = "_tokDefenseSpeedUpCol";
            this._tokDefenseSpeedUpCol.Width = 36;
            // 
            // _tokDefenseSpeedupTimeCol
            // 
            dataGridViewCellStyle9.NullValue = null;
            this._tokDefenseSpeedupTimeCol.DefaultCellStyle = dataGridViewCellStyle9;
            this._tokDefenseSpeedupTimeCol.HeaderText = "t �����.";
            this._tokDefenseSpeedupTimeCol.Name = "_tokDefenseSpeedupTimeCol";
            this._tokDefenseSpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseSpeedupTimeCol.Width = 51;
            // 
            // _tokDefenseFeatureCol
            // 
            this._tokDefenseFeatureCol.HeaderText = "���-��";
            this._tokDefenseFeatureCol.Name = "_tokDefenseFeatureCol";
            this._tokDefenseFeatureCol.Width = 47;
            // 
            // _tokDefenseWorkTimeCol
            // 
            dataGridViewCellStyle10.NullValue = null;
            this._tokDefenseWorkTimeCol.DefaultCellStyle = dataGridViewCellStyle10;
            this._tokDefenseWorkTimeCol.HeaderText = "t ����.";
            this._tokDefenseWorkTimeCol.Name = "_tokDefenseWorkTimeCol";
            this._tokDefenseWorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseWorkTimeCol.Width = 46;
            // 
            // _tokDefenseGrid1
            // 
            this._tokDefenseGrid1.AllowUserToAddRows = false;
            this._tokDefenseGrid1.AllowUserToDeleteRows = false;
            this._tokDefenseGrid1.AllowUserToResizeColumns = false;
            this._tokDefenseGrid1.AllowUserToResizeRows = false;
            this._tokDefenseGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._tokDefenseGrid1.BackgroundColor = System.Drawing.Color.White;
            this._tokDefenseGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefenseNameCol,
            this._tokDefenseModeCol,
            this._tokDefenseBlockNumberCol,
            this._tokDefenseU_PuskCol,
            this._tokDefensePuskConstraintCol,
            this._tokDefenseDirectionCol,
            this._tokDefenseBlockExistCol,
            this._tokDefenseParameterCol,
            this._tokDefenseWorkConstraintCol,
            this._tokDefenseFeatureCol,
            this._tokDefenseWorkTimeCol,
            this._tokDefenseSpeedUpCol,
            this._tokDefenseSpeedupTimeCol,
            this._tokDefenseUROVCol,
            this._tokDefenseAPVCol,
            this._tokDefenseAVRCol});
            this._tokDefenseGrid1.Location = new System.Drawing.Point(8, 84);
            this._tokDefenseGrid1.Name = "_tokDefenseGrid1";
            this._tokDefenseGrid1.RowHeadersVisible = false;
            this._tokDefenseGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid1.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this._tokDefenseGrid1.RowTemplate.Height = 24;
            this._tokDefenseGrid1.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._tokDefenseGrid1.Size = new System.Drawing.Size(709, 136);
            this._tokDefenseGrid1.TabIndex = 0;
            // 
            // _tokDefenseNameCol
            // 
            this._tokDefenseNameCol.Frozen = true;
            this._tokDefenseNameCol.HeaderText = "";
            this._tokDefenseNameCol.Name = "_tokDefenseNameCol";
            this._tokDefenseNameCol.ReadOnly = true;
            this._tokDefenseNameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseNameCol.Width = 5;
            // 
            // _tokDefenseModeCol
            // 
            this._tokDefenseModeCol.HeaderText = "�����";
            this._tokDefenseModeCol.Name = "_tokDefenseModeCol";
            this._tokDefenseModeCol.Width = 48;
            // 
            // _tokDefenseBlockNumberCol
            // 
            this._tokDefenseBlockNumberCol.HeaderText = "����������";
            this._tokDefenseBlockNumberCol.Name = "_tokDefenseBlockNumberCol";
            this._tokDefenseBlockNumberCol.Width = 74;
            // 
            // _tokDefenseU_PuskCol
            // 
            this._tokDefenseU_PuskCol.HeaderText = "U ����";
            this._tokDefenseU_PuskCol.Name = "_tokDefenseU_PuskCol";
            this._tokDefenseU_PuskCol.Width = 47;
            // 
            // _tokDefenseUROVCol
            // 
            this._tokDefenseUROVCol.HeaderText = "����";
            this._tokDefenseUROVCol.Name = "_tokDefenseUROVCol";
            this._tokDefenseUROVCol.Width = 43;
            // 
            // _tokDefenseAPVCol
            // 
            this._tokDefenseAPVCol.HeaderText = "���";
            this._tokDefenseAPVCol.Name = "_tokDefenseAPVCol";
            this._tokDefenseAPVCol.Width = 35;
            // 
            // _tokDefenseAVRCol
            // 
            this._tokDefenseAVRCol.HeaderText = "���";
            this._tokDefenseAVRCol.Name = "_tokDefenseAVRCol";
            this._tokDefenseAVRCol.Width = 34;
            // 
            // _tokDefense3WorkConstraintCol
            // 
            dataGridViewCellStyle12.NullValue = null;
            this._tokDefense3WorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle12;
            this._tokDefense3WorkConstraintCol.HeaderText = "���.����.";
            this._tokDefense3WorkConstraintCol.Name = "_tokDefense3WorkConstraintCol";
            this._tokDefense3WorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3WorkConstraintCol.Width = 62;
            // 
            // _tokDefenseI2box
            // 
            this._tokDefenseI2box.Location = new System.Drawing.Point(152, 15);
            this._tokDefenseI2box.Name = "_tokDefenseI2box";
            this._tokDefenseI2box.Size = new System.Drawing.Size(37, 20);
            this._tokDefenseI2box.TabIndex = 5;
            this._tokDefenseI2box.Tag = "360";
            this._tokDefenseI2box.Text = "0";
            this._tokDefenseI2box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(136, 18);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(16, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "I2";
            // 
            // _tokDefenseI0box
            // 
            this._tokDefenseI0box.Location = new System.Drawing.Point(91, 15);
            this._tokDefenseI0box.Name = "_tokDefenseI0box";
            this._tokDefenseI0box.Size = new System.Drawing.Size(37, 20);
            this._tokDefenseI0box.TabIndex = 3;
            this._tokDefenseI0box.Tag = "360";
            this._tokDefenseI0box.Text = "0";
            this._tokDefenseI0box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(75, 18);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(16, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "I0";
            // 
            // _tokDefenseIbox
            // 
            this._tokDefenseIbox.Location = new System.Drawing.Point(31, 15);
            this._tokDefenseIbox.Name = "_tokDefenseIbox";
            this._tokDefenseIbox.Size = new System.Drawing.Size(37, 20);
            this._tokDefenseIbox.TabIndex = 1;
            this._tokDefenseIbox.Tag = "360";
            this._tokDefenseIbox.Text = "0";
            this._tokDefenseIbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _tokDefense4NameCol
            // 
            this._tokDefense4NameCol.Frozen = true;
            this._tokDefense4NameCol.HeaderText = "";
            this._tokDefense4NameCol.Name = "_tokDefense4NameCol";
            this._tokDefense4NameCol.ReadOnly = true;
            this._tokDefense4NameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4NameCol.Width = 5;
            // 
            // _tokDefense4ModeCol
            // 
            this._tokDefense4ModeCol.HeaderText = "�����";
            this._tokDefense4ModeCol.Name = "_tokDefense4ModeCol";
            this._tokDefense4ModeCol.Width = 48;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(15, 18);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(10, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "I";
            // 
            // _tokDefenseGrid4
            // 
            this._tokDefenseGrid4.AllowUserToAddRows = false;
            this._tokDefenseGrid4.AllowUserToDeleteRows = false;
            this._tokDefenseGrid4.AllowUserToResizeColumns = false;
            this._tokDefenseGrid4.AllowUserToResizeRows = false;
            this._tokDefenseGrid4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid4.BackgroundColor = System.Drawing.Color.White;
            this._tokDefenseGrid4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefense4NameCol,
            this._tokDefense4ModeCol,
            this._tokDefense4BlockNumberCol,
            this._tokDefense4UpuskCol,
            this._tokDefense4PuskConstraintCol,
            this._tokDefense4WorkConstraintCol,
            this._tokDefense4WorkTimeCol,
            this._tokDefense4SpeedupCol,
            this._tokDefense4SpeedupTimeCol,
            this._tokDefense4UROVCol,
            this._tokDefense4APVCol,
            this._tokDefense4AVRCol});
            this._tokDefenseGrid4.Location = new System.Drawing.Point(8, 410);
            this._tokDefenseGrid4.Name = "_tokDefenseGrid4";
            this._tokDefenseGrid4.RowHeadersVisible = false;
            this._tokDefenseGrid4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid4.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this._tokDefenseGrid4.RowTemplate.Height = 24;
            this._tokDefenseGrid4.Size = new System.Drawing.Size(709, 116);
            this._tokDefenseGrid4.TabIndex = 3;
            // 
            // _tokDefense4BlockNumberCol
            // 
            this._tokDefense4BlockNumberCol.HeaderText = "����������";
            this._tokDefense4BlockNumberCol.Name = "_tokDefense4BlockNumberCol";
            this._tokDefense4BlockNumberCol.Width = 74;
            // 
            // _tokDefense4UpuskCol
            // 
            this._tokDefense4UpuskCol.HeaderText = "U ����";
            this._tokDefense4UpuskCol.Name = "_tokDefense4UpuskCol";
            this._tokDefense4UpuskCol.Width = 42;
            // 
            // _tokDefense4PuskConstraintCol
            // 
            dataGridViewCellStyle13.NullValue = null;
            this._tokDefense4PuskConstraintCol.DefaultCellStyle = dataGridViewCellStyle13;
            this._tokDefense4PuskConstraintCol.HeaderText = "������� �����";
            this._tokDefense4PuskConstraintCol.Name = "_tokDefense4PuskConstraintCol";
            this._tokDefense4PuskConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4PuskConstraintCol.Width = 79;
            // 
            // _tokDefense4WorkConstraintCol
            // 
            dataGridViewCellStyle14.NullValue = null;
            this._tokDefense4WorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle14;
            this._tokDefense4WorkConstraintCol.HeaderText = "���.����.";
            this._tokDefense4WorkConstraintCol.Name = "_tokDefense4WorkConstraintCol";
            this._tokDefense4WorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4WorkConstraintCol.Width = 62;
            // 
            // _tokDefense4WorkTimeCol
            // 
            dataGridViewCellStyle15.NullValue = null;
            this._tokDefense4WorkTimeCol.DefaultCellStyle = dataGridViewCellStyle15;
            this._tokDefense4WorkTimeCol.HeaderText = "t ����.";
            this._tokDefense4WorkTimeCol.Name = "_tokDefense4WorkTimeCol";
            this._tokDefense4WorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4WorkTimeCol.Width = 42;
            // 
            // _tokDefense4SpeedupCol
            // 
            this._tokDefense4SpeedupCol.HeaderText = "���.";
            this._tokDefense4SpeedupCol.Name = "_tokDefense4SpeedupCol";
            this._tokDefense4SpeedupCol.Width = 36;
            // 
            // _tokDefense4SpeedupTimeCol
            // 
            this._tokDefense4SpeedupTimeCol.HeaderText = "������� ���������";
            this._tokDefense4SpeedupTimeCol.Name = "_tokDefense4SpeedupTimeCol";
            this._tokDefense4SpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4SpeedupTimeCol.Width = 101;
            // 
            // _tokDefense4UROVCol
            // 
            this._tokDefense4UROVCol.HeaderText = "����";
            this._tokDefense4UROVCol.Name = "_tokDefense4UROVCol";
            this._tokDefense4UROVCol.Width = 43;
            // 
            // _tokDefense4APVCol
            // 
            this._tokDefense4APVCol.HeaderText = "���";
            this._tokDefense4APVCol.Name = "_tokDefense4APVCol";
            this._tokDefense4APVCol.Width = 35;
            // 
            // _tokDefense4AVRCol
            // 
            this._tokDefense4AVRCol.HeaderText = "���";
            this._tokDefense4AVRCol.Name = "_tokDefense4AVRCol";
            this._tokDefense4AVRCol.Width = 34;
            // 
            // _tokDefensesPage
            // 
            this._tokDefensesPage.Controls.Add(this.groupBox16);
            this._tokDefensesPage.Controls.Add(this.groupBox15);
            this._tokDefensesPage.Controls.Add(this._tokDefenseGrid4);
            this._tokDefensesPage.Controls.Add(this._tokDefenseGrid3);
            this._tokDefensesPage.Controls.Add(this._tokDefenseGrid2);
            this._tokDefensesPage.Controls.Add(this._tokDefenseGrid1);
            this._tokDefensesPage.Location = new System.Drawing.Point(4, 22);
            this._tokDefensesPage.Name = "_tokDefensesPage";
            this._tokDefensesPage.Size = new System.Drawing.Size(725, 528);
            this._tokDefensesPage.TabIndex = 4;
            this._tokDefensesPage.Text = "������� ������";
            this._tokDefensesPage.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this._tokDefenseMainConstraintRadio);
            this.groupBox16.Controls.Add(this._tokDefenseReserveConstraintRadio);
            this.groupBox16.Location = new System.Drawing.Point(13, 6);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(184, 35);
            this.groupBox16.TabIndex = 8;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "������ �������";
            // 
            // _tokDefenseMainConstraintRadio
            // 
            this._tokDefenseMainConstraintRadio.AutoSize = true;
            this._tokDefenseMainConstraintRadio.Checked = true;
            this._tokDefenseMainConstraintRadio.Location = new System.Drawing.Point(17, 13);
            this._tokDefenseMainConstraintRadio.Name = "_tokDefenseMainConstraintRadio";
            this._tokDefenseMainConstraintRadio.Size = new System.Drawing.Size(75, 17);
            this._tokDefenseMainConstraintRadio.TabIndex = 6;
            this._tokDefenseMainConstraintRadio.TabStop = true;
            this._tokDefenseMainConstraintRadio.Text = "��������";
            this._tokDefenseMainConstraintRadio.UseVisualStyleBackColor = true;
            this._tokDefenseMainConstraintRadio.CheckedChanged += new System.EventHandler(this._tokDefenseMainConstraintRadio_CheckedChanged);
            // 
            // _tokDefenseReserveConstraintRadio
            // 
            this._tokDefenseReserveConstraintRadio.AutoSize = true;
            this._tokDefenseReserveConstraintRadio.Location = new System.Drawing.Point(98, 14);
            this._tokDefenseReserveConstraintRadio.Name = "_tokDefenseReserveConstraintRadio";
            this._tokDefenseReserveConstraintRadio.Size = new System.Drawing.Size(80, 17);
            this._tokDefenseReserveConstraintRadio.TabIndex = 7;
            this._tokDefenseReserveConstraintRadio.Text = "���������";
            this._tokDefenseReserveConstraintRadio.UseVisualStyleBackColor = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this._tokDefenseInbox);
            this.groupBox15.Controls.Add(this.label24);
            this.groupBox15.Controls.Add(this._tokDefenseI2box);
            this.groupBox15.Controls.Add(this.label23);
            this.groupBox15.Controls.Add(this._tokDefenseI0box);
            this.groupBox15.Controls.Add(this.label22);
            this.groupBox15.Controls.Add(this._tokDefenseIbox);
            this.groupBox15.Controls.Add(this.label21);
            this.groupBox15.Location = new System.Drawing.Point(13, 42);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(261, 41);
            this.groupBox15.TabIndex = 4;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "������������ ���� (�����������)";
            // 
            // _tokDefenseInbox
            // 
            this._tokDefenseInbox.Location = new System.Drawing.Point(216, 15);
            this._tokDefenseInbox.Name = "_tokDefenseInbox";
            this._tokDefenseInbox.Size = new System.Drawing.Size(37, 20);
            this._tokDefenseInbox.TabIndex = 7;
            this._tokDefenseInbox.Tag = "360";
            this._tokDefenseInbox.Text = "0";
            this._tokDefenseInbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(200, 18);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(16, 13);
            this.label24.TabIndex = 6;
            this.label24.Text = "In";
            // 
            // _tokDefenseGrid3
            // 
            this._tokDefenseGrid3.AllowUserToAddRows = false;
            this._tokDefenseGrid3.AllowUserToDeleteRows = false;
            this._tokDefenseGrid3.AllowUserToResizeColumns = false;
            this._tokDefenseGrid3.AllowUserToResizeRows = false;
            this._tokDefenseGrid3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid3.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._tokDefenseGrid3.BackgroundColor = System.Drawing.Color.White;
            this._tokDefenseGrid3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefense3NameCol,
            this._tokDefense3ModeCol,
            this._tokDefense3BlockingNumberCol,
            this._tokDefense3UpuskCol,
            this._tokDefense3PuskConstraintCol,
            this._tokDefense3DirectionCol,
            this._tokDefense3BlockingExistCol,
            this._tokDefense3ParameterCol,
            this._tokDefense3WorkConstraintCol,
            this._tokDefense3FeatureCol,
            this._tokDefense3WorkTimeCol,
            this._tokDefense3SpeedupCol,
            this._tokDefense3SpeedupTimeCol,
            this._tokDefense3UROVCol,
            this._tokDefense3APVCol,
            this._tokDefense3AVRCol});
            this._tokDefenseGrid3.Location = new System.Drawing.Point(8, 226);
            this._tokDefenseGrid3.Name = "_tokDefenseGrid3";
            this._tokDefenseGrid3.RowHeadersVisible = false;
            this._tokDefenseGrid3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid3.RowsDefaultCellStyle = dataGridViewCellStyle18;
            this._tokDefenseGrid3.RowTemplate.Height = 24;
            this._tokDefenseGrid3.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._tokDefenseGrid3.Size = new System.Drawing.Size(709, 178);
            this._tokDefenseGrid3.TabIndex = 2;
            // 
            // _tokDefense3NameCol
            // 
            this._tokDefense3NameCol.Frozen = true;
            this._tokDefense3NameCol.HeaderText = "";
            this._tokDefense3NameCol.Name = "_tokDefense3NameCol";
            this._tokDefense3NameCol.ReadOnly = true;
            this._tokDefense3NameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3NameCol.Width = 5;
            // 
            // _tokDefense3ModeCol
            // 
            this._tokDefense3ModeCol.HeaderText = "�����";
            this._tokDefense3ModeCol.Name = "_tokDefense3ModeCol";
            this._tokDefense3ModeCol.Width = 48;
            // 
            // _tokDefense3BlockingNumberCol
            // 
            this._tokDefense3BlockingNumberCol.HeaderText = "����������";
            this._tokDefense3BlockingNumberCol.Name = "_tokDefense3BlockingNumberCol";
            this._tokDefense3BlockingNumberCol.Width = 74;
            // 
            // _tokDefense3UpuskCol
            // 
            this._tokDefense3UpuskCol.HeaderText = "U ����";
            this._tokDefense3UpuskCol.Name = "_tokDefense3UpuskCol";
            this._tokDefense3UpuskCol.Width = 47;
            // 
            // _tokDefense3PuskConstraintCol
            // 
            dataGridViewCellStyle17.NullValue = null;
            this._tokDefense3PuskConstraintCol.DefaultCellStyle = dataGridViewCellStyle17;
            this._tokDefense3PuskConstraintCol.HeaderText = "���. �����";
            this._tokDefense3PuskConstraintCol.Name = "_tokDefense3PuskConstraintCol";
            this._tokDefense3PuskConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3PuskConstraintCol.Width = 67;
            // 
            // _tokDefense3DirectionCol
            // 
            this._tokDefense3DirectionCol.HeaderText = "�����������";
            this._tokDefense3DirectionCol.Name = "_tokDefense3DirectionCol";
            this._tokDefense3DirectionCol.Width = 81;
            // 
            // _tokDefense3BlockingExistCol
            // 
            this._tokDefense3BlockingExistCol.HeaderText = "����������";
            this._tokDefense3BlockingExistCol.Name = "_tokDefense3BlockingExistCol";
            this._tokDefense3BlockingExistCol.Width = 74;
            // 
            // _tokDefense3ParameterCol
            // 
            this._tokDefense3ParameterCol.HeaderText = "��������";
            this._tokDefense3ParameterCol.Name = "_tokDefense3ParameterCol";
            this._tokDefense3ParameterCol.Width = 64;
            // 
            // apv_blocking
            // 
            this.apv_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apv_blocking.FormattingEnabled = true;
            this.apv_blocking.Location = new System.Drawing.Point(146, 40);
            this.apv_blocking.Name = "apv_blocking";
            this.apv_blocking.Size = new System.Drawing.Size(87, 21);
            this.apv_blocking.TabIndex = 3;
            // 
            // apv_conf
            // 
            this.apv_conf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apv_conf.FormattingEnabled = true;
            this.apv_conf.Location = new System.Drawing.Point(146, 19);
            this.apv_conf.Name = "apv_conf";
            this.apv_conf.Size = new System.Drawing.Size(87, 21);
            this.apv_conf.TabIndex = 1;
            // 
            // _frequenceDefensesGrid
            // 
            this._frequenceDefensesGrid.AllowUserToAddRows = false;
            this._frequenceDefensesGrid.AllowUserToDeleteRows = false;
            this._frequenceDefensesGrid.AllowUserToResizeColumns = false;
            this._frequenceDefensesGrid.AllowUserToResizeRows = false;
            this._frequenceDefensesGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this._frequenceDefensesGrid.BackgroundColor = System.Drawing.Color.White;
            this._frequenceDefensesGrid.ColumnHeadersHeight = 30;
            this._frequenceDefensesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._frequenceDefensesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._frequenceDefensesName,
            this._frequenceDefensesMode,
            this._frequenceDefensesBlockingNumber,
            this._frequenceDefensesWorkConstraint,
            this._frequenceDefensesWorkTime,
            this._frequenceDefensesReturn,
            this._frequenceDefensesAPVReturn,
            this._frequenceDefensesReturnTime,
            this._frequenceDefensesConstraintAPV,
            this._frequenceDefensesUROV,
            this._frequenceDefensesAPV,
            this._frequenceDefensesAVR});
            this._frequenceDefensesGrid.Location = new System.Drawing.Point(8, 49);
            this._frequenceDefensesGrid.Name = "_frequenceDefensesGrid";
            this._frequenceDefensesGrid.RowHeadersVisible = false;
            this._frequenceDefensesGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._frequenceDefensesGrid.RowTemplate.Height = 24;
            this._frequenceDefensesGrid.Size = new System.Drawing.Size(676, 272);
            this._frequenceDefensesGrid.TabIndex = 5;
            // 
            // _frequenceDefensesName
            // 
            this._frequenceDefensesName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._frequenceDefensesName.DefaultCellStyle = dataGridViewCellStyle19;
            this._frequenceDefensesName.HeaderText = "";
            this._frequenceDefensesName.MaxInputLength = 1;
            this._frequenceDefensesName.Name = "_frequenceDefensesName";
            this._frequenceDefensesName.ReadOnly = true;
            this._frequenceDefensesName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesName.Width = 45;
            // 
            // _frequenceDefensesMode
            // 
            this._frequenceDefensesMode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._frequenceDefensesMode.HeaderText = "�����";
            this._frequenceDefensesMode.Name = "_frequenceDefensesMode";
            this._frequenceDefensesMode.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _frequenceDefensesBlockingNumber
            // 
            this._frequenceDefensesBlockingNumber.HeaderText = "����������";
            this._frequenceDefensesBlockingNumber.Name = "_frequenceDefensesBlockingNumber";
            this._frequenceDefensesBlockingNumber.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesBlockingNumber.Width = 74;
            // 
            // _frequenceDefensesWorkConstraint
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesWorkConstraint.DefaultCellStyle = dataGridViewCellStyle20;
            this._frequenceDefensesWorkConstraint.HeaderText = "������� ����.";
            this._frequenceDefensesWorkConstraint.MaxInputLength = 8;
            this._frequenceDefensesWorkConstraint.Name = "_frequenceDefensesWorkConstraint";
            this._frequenceDefensesWorkConstraint.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesWorkConstraint.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesWorkConstraint.Width = 86;
            // 
            // _frequenceDefensesWorkTime
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle21.NullValue = "0";
            this._frequenceDefensesWorkTime.DefaultCellStyle = dataGridViewCellStyle21;
            this._frequenceDefensesWorkTime.HeaderText = "����� ����.";
            this._frequenceDefensesWorkTime.MaxInputLength = 8;
            this._frequenceDefensesWorkTime.Name = "_frequenceDefensesWorkTime";
            this._frequenceDefensesWorkTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesWorkTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesWorkTime.Width = 76;
            // 
            // _frequenceDefensesReturn
            // 
            this._frequenceDefensesReturn.HeaderText = "����.";
            this._frequenceDefensesReturn.Name = "_frequenceDefensesReturn";
            this._frequenceDefensesReturn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesReturn.Width = 41;
            // 
            // _frequenceDefensesAPVReturn
            // 
            this._frequenceDefensesAPVReturn.HeaderText = "��� ��";
            this._frequenceDefensesAPVReturn.Name = "_frequenceDefensesAPVReturn";
            this._frequenceDefensesAPVReturn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesAPVReturn.Width = 52;
            // 
            // _frequenceDefensesReturnTime
            // 
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesReturnTime.DefaultCellStyle = dataGridViewCellStyle22;
            this._frequenceDefensesReturnTime.HeaderText = "����� ��������";
            this._frequenceDefensesReturnTime.Name = "_frequenceDefensesReturnTime";
            this._frequenceDefensesReturnTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesReturnTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesReturnTime.Width = 96;
            // 
            // _frequenceDefensesConstraintAPV
            // 
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesConstraintAPV.DefaultCellStyle = dataGridViewCellStyle23;
            this._frequenceDefensesConstraintAPV.HeaderText = "������� ��";
            this._frequenceDefensesConstraintAPV.Name = "_frequenceDefensesConstraintAPV";
            this._frequenceDefensesConstraintAPV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesConstraintAPV.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesConstraintAPV.Width = 73;
            // 
            // _frequenceDefensesUROV
            // 
            this._frequenceDefensesUROV.HeaderText = "����";
            this._frequenceDefensesUROV.Name = "_frequenceDefensesUROV";
            this._frequenceDefensesUROV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesUROV.Width = 43;
            // 
            // _frequenceDefensesAPV
            // 
            this._frequenceDefensesAPV.HeaderText = "���";
            this._frequenceDefensesAPV.Name = "_frequenceDefensesAPV";
            this._frequenceDefensesAPV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesAPV.Width = 35;
            // 
            // _frequenceDefensesAVR
            // 
            this._frequenceDefensesAVR.HeaderText = "���";
            this._frequenceDefensesAVR.Name = "_frequenceDefensesAVR";
            this._frequenceDefensesAVR.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesAVR.Width = 34;
            // 
            // _voltageDefensesAVRcol3
            // 
            this._voltageDefensesAVRcol3.HeaderText = "���";
            this._voltageDefensesAVRcol3.Name = "_voltageDefensesAVRcol3";
            this._voltageDefensesAVRcol3.Width = 34;
            // 
            // _frequencePage
            // 
            this._frequencePage.Controls.Add(this._frequenceDefensesGrid);
            this._frequencePage.Controls.Add(this.groupBox20);
            this._frequencePage.Location = new System.Drawing.Point(4, 22);
            this._frequencePage.Name = "_frequencePage";
            this._frequencePage.Size = new System.Drawing.Size(725, 528);
            this._frequencePage.TabIndex = 7;
            this._frequencePage.Text = "������ �� �������";
            this._frequencePage.UseVisualStyleBackColor = true;
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this._frequenceDefenseMainConstraintRadio);
            this.groupBox20.Controls.Add(this._frequenceDefenseReserveConstraintRadio);
            this.groupBox20.Location = new System.Drawing.Point(10, 4);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(187, 39);
            this.groupBox20.TabIndex = 4;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "������ �������";
            // 
            // _frequenceDefenseMainConstraintRadio
            // 
            this._frequenceDefenseMainConstraintRadio.AutoSize = true;
            this._frequenceDefenseMainConstraintRadio.Checked = true;
            this._frequenceDefenseMainConstraintRadio.Location = new System.Drawing.Point(6, 14);
            this._frequenceDefenseMainConstraintRadio.Name = "_frequenceDefenseMainConstraintRadio";
            this._frequenceDefenseMainConstraintRadio.Size = new System.Drawing.Size(77, 17);
            this._frequenceDefenseMainConstraintRadio.TabIndex = 0;
            this._frequenceDefenseMainConstraintRadio.TabStop = true;
            this._frequenceDefenseMainConstraintRadio.Text = "��������";
            this._frequenceDefenseMainConstraintRadio.UseVisualStyleBackColor = true;
            this._frequenceDefenseMainConstraintRadio.CheckedChanged += new System.EventHandler(this._frequenceDefenseMainConstraintRadio_CheckedChanged);
            // 
            // _frequenceDefenseReserveConstraintRadio
            // 
            this._frequenceDefenseReserveConstraintRadio.AutoSize = true;
            this._frequenceDefenseReserveConstraintRadio.Location = new System.Drawing.Point(97, 14);
            this._frequenceDefenseReserveConstraintRadio.Name = "_frequenceDefenseReserveConstraintRadio";
            this._frequenceDefenseReserveConstraintRadio.Size = new System.Drawing.Size(82, 17);
            this._frequenceDefenseReserveConstraintRadio.TabIndex = 1;
            this._frequenceDefenseReserveConstraintRadio.Text = "���������";
            this._frequenceDefenseReserveConstraintRadio.UseVisualStyleBackColor = true;
            // 
            // _voltageDefensesTimeConstraintCol3
            // 
            dataGridViewCellStyle24.NullValue = "0";
            this._voltageDefensesTimeConstraintCol3.DefaultCellStyle = dataGridViewCellStyle24;
            this._voltageDefensesTimeConstraintCol3.HeaderText = "����� ����.";
            this._voltageDefensesTimeConstraintCol3.Name = "_voltageDefensesTimeConstraintCol3";
            this._voltageDefensesTimeConstraintCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesTimeConstraintCol3.Width = 76;
            // 
            // _voltageDefensesReturnCol3
            // 
            this._voltageDefensesReturnCol3.HeaderText = "����.";
            this._voltageDefensesReturnCol3.Name = "_voltageDefensesReturnCol3";
            this._voltageDefensesReturnCol3.Width = 41;
            // 
            // _voltageDefensesParameterCol3
            // 
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesParameterCol3.DefaultCellStyle = dataGridViewCellStyle25;
            this._voltageDefensesParameterCol3.HeaderText = "��������";
            this._voltageDefensesParameterCol3.Name = "_voltageDefensesParameterCol3";
            this._voltageDefensesParameterCol3.Width = 64;
            // 
            // _voltageDefensesWorkConstraintCol3
            // 
            this._voltageDefensesWorkConstraintCol3.HeaderText = "������� ����.";
            this._voltageDefensesWorkConstraintCol3.Name = "_voltageDefensesWorkConstraintCol3";
            this._voltageDefensesWorkConstraintCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkConstraintCol3.Width = 86;
            // 
            // _voltageDefensesAPVreturnCol3
            // 
            this._voltageDefensesAPVreturnCol3.HeaderText = "��� ��";
            this._voltageDefensesAPVreturnCol3.Name = "_voltageDefensesAPVreturnCol3";
            this._voltageDefensesAPVreturnCol3.Width = 52;
            // 
            // _voltageDefensesUROVcol3
            // 
            this._voltageDefensesUROVcol3.HeaderText = "����";
            this._voltageDefensesUROVcol3.Name = "_voltageDefensesUROVcol3";
            this._voltageDefensesUROVcol3.Width = 43;
            // 
            // _voltageDefensesAPVcol3
            // 
            this._voltageDefensesAPVcol3.HeaderText = "���";
            this._voltageDefensesAPVcol3.Name = "_voltageDefensesAPVcol3";
            this._voltageDefensesAPVcol3.Width = 35;
            // 
            // _voltageDefensesReturnConstraintCol3
            // 
            this._voltageDefensesReturnConstraintCol3.HeaderText = "������� ��������";
            this._voltageDefensesReturnConstraintCol3.Name = "_voltageDefensesReturnConstraintCol3";
            this._voltageDefensesReturnConstraintCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnConstraintCol3.Width = 106;
            // 
            // _voltageDefensesReturnTimeCol3
            // 
            this._voltageDefensesReturnTimeCol3.HeaderText = "����� ��������";
            this._voltageDefensesReturnTimeCol3.Name = "_voltageDefensesReturnTimeCol3";
            this._voltageDefensesReturnTimeCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnTimeCol3.Width = 96;
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.Location = new System.Drawing.Point(476, 560);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(125, 23);
            this._saveConfigBut.TabIndex = 10;
            this._saveConfigBut.Text = "��������� � ����";
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Location = new System.Drawing.Point(337, 560);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(125, 23);
            this._loadConfigBut.TabIndex = 9;
            this._loadConfigBut.Text = "��������� �� �����";
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _toolTip
            // 
            this._toolTip.IsBalloon = true;
            this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // _TT_Box
            // 
            this._TT_Box.Location = new System.Drawing.Point(124, 12);
            this._TT_Box.Name = "_TT_Box";
            this._TT_Box.Size = new System.Drawing.Size(71, 20);
            this._TT_Box.TabIndex = 0;
            this._TT_Box.Tag = "1500";
            this._TT_Box.Text = "0";
            this._TT_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._toolTip.SetToolTip(this._TT_Box, "��������� ��� ��������������");
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.Location = new System.Drawing.Point(165, 560);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(149, 23);
            this._writeConfigBut.TabIndex = 8;
            this._writeConfigBut.Text = "�������� � ����������";
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "bin";
            this._saveConfigurationDlg.FileName = "��700_�������";
            this._saveConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._saveConfigurationDlg.Title = "���������  ������� ��� ��700";
            // 
            // _processLabel
            // 
            this._processLabel.Name = "_processLabel";
            this._processLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _exchangeProgressBar
            // 
            this._exchangeProgressBar.Maximum = 80;
            this._exchangeProgressBar.Name = "_exchangeProgressBar";
            this._exchangeProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "bin";
            this._openConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "������� ������� ��� ��700";
            // 
            // _statusStrip
            // 
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._processLabel,
            this._exchangeProgressBar});
            this._statusStrip.Location = new System.Drawing.Point(0, 591);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this._statusStrip.Size = new System.Drawing.Size(733, 22);
            this._statusStrip.TabIndex = 11;
            this._statusStrip.Text = "statusStrip1";
            // 
            // _readConfigBut
            // 
            this._readConfigBut.Location = new System.Drawing.Point(10, 560);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(149, 23);
            this._readConfigBut.TabIndex = 7;
            this._readConfigBut.Text = "��������� �� ����������";
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _voltageDefensesBlockingNumberCol3
            // 
            this._voltageDefensesBlockingNumberCol3.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol3.Name = "_voltageDefensesBlockingNumberCol3";
            this._voltageDefensesBlockingNumberCol3.Width = 74;
            // 
            // _voltageDefensesWorkConstraintCol2
            // 
            this._voltageDefensesWorkConstraintCol2.HeaderText = "������� ����.";
            this._voltageDefensesWorkConstraintCol2.Name = "_voltageDefensesWorkConstraintCol2";
            this._voltageDefensesWorkConstraintCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkConstraintCol2.Width = 86;
            // 
            // _voltageDefensesWorkTimeCol2
            // 
            dataGridViewCellStyle26.NullValue = "0";
            this._voltageDefensesWorkTimeCol2.DefaultCellStyle = dataGridViewCellStyle26;
            this._voltageDefensesWorkTimeCol2.HeaderText = "����� ����.";
            this._voltageDefensesWorkTimeCol2.Name = "_voltageDefensesWorkTimeCol2";
            this._voltageDefensesWorkTimeCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkTimeCol2.Width = 76;
            // 
            // _voltageDefensesBlockingNumberCol2
            // 
            this._voltageDefensesBlockingNumberCol2.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol2.Name = "_voltageDefensesBlockingNumberCol2";
            this._voltageDefensesBlockingNumberCol2.Width = 74;
            // 
            // _voltageDefensesParameterCol2
            // 
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesParameterCol2.DefaultCellStyle = dataGridViewCellStyle27;
            this._voltageDefensesParameterCol2.HeaderText = "��������";
            this._voltageDefensesParameterCol2.Name = "_voltageDefensesParameterCol2";
            this._voltageDefensesParameterCol2.Width = 64;
            // 
            // _voltageDefensesReturnCol2
            // 
            this._voltageDefensesReturnCol2.HeaderText = "����.";
            this._voltageDefensesReturnCol2.Name = "_voltageDefensesReturnCol2";
            this._voltageDefensesReturnCol2.Width = 41;
            // 
            // _voltageDefensesReturnTimeCol2
            // 
            this._voltageDefensesReturnTimeCol2.HeaderText = "����� ��������";
            this._voltageDefensesReturnTimeCol2.Name = "_voltageDefensesReturnTimeCol2";
            this._voltageDefensesReturnTimeCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnTimeCol2.Width = 96;
            // 
            // _voltageDefensesUROVcol2
            // 
            this._voltageDefensesUROVcol2.HeaderText = "����";
            this._voltageDefensesUROVcol2.Name = "_voltageDefensesUROVcol2";
            this._voltageDefensesUROVcol2.Width = 43;
            // 
            // _voltageDefensesAPVreturnCol2
            // 
            this._voltageDefensesAPVreturnCol2.HeaderText = "��� ��";
            this._voltageDefensesAPVreturnCol2.Name = "_voltageDefensesAPVreturnCol2";
            this._voltageDefensesAPVreturnCol2.Width = 52;
            // 
            // _voltageDefensesReturnConstraintCol2
            // 
            this._voltageDefensesReturnConstraintCol2.HeaderText = "������� ��������";
            this._voltageDefensesReturnConstraintCol2.Name = "_voltageDefensesReturnConstraintCol2";
            this._voltageDefensesReturnConstraintCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnConstraintCol2.Width = 106;
            // 
            // _voltageDefensesPage
            // 
            this._voltageDefensesPage.Controls.Add(this.groupBox17);
            this._voltageDefensesPage.Controls.Add(this._voltageDefensesGrid2);
            this._voltageDefensesPage.Controls.Add(this._voltageDefensesGrid1);
            this._voltageDefensesPage.Controls.Add(this._voltageDefensesGrid3);
            this._voltageDefensesPage.Location = new System.Drawing.Point(4, 22);
            this._voltageDefensesPage.Name = "_voltageDefensesPage";
            this._voltageDefensesPage.Size = new System.Drawing.Size(725, 528);
            this._voltageDefensesPage.TabIndex = 5;
            this._voltageDefensesPage.Text = "������ �� ����������";
            this._voltageDefensesPage.UseVisualStyleBackColor = true;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this._voltageDefenseMainConstraintRadio);
            this.groupBox17.Controls.Add(this._voltageDefenseReservConstraintRadio);
            this.groupBox17.Location = new System.Drawing.Point(17, 5);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(184, 40);
            this.groupBox17.TabIndex = 9;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "������ �������";
            // 
            // _voltageDefenseMainConstraintRadio
            // 
            this._voltageDefenseMainConstraintRadio.AutoSize = true;
            this._voltageDefenseMainConstraintRadio.Checked = true;
            this._voltageDefenseMainConstraintRadio.Location = new System.Drawing.Point(17, 15);
            this._voltageDefenseMainConstraintRadio.Name = "_voltageDefenseMainConstraintRadio";
            this._voltageDefenseMainConstraintRadio.Size = new System.Drawing.Size(75, 17);
            this._voltageDefenseMainConstraintRadio.TabIndex = 6;
            this._voltageDefenseMainConstraintRadio.TabStop = true;
            this._voltageDefenseMainConstraintRadio.Text = "��������";
            this._voltageDefenseMainConstraintRadio.UseVisualStyleBackColor = true;
            this._voltageDefenseMainConstraintRadio.CheckedChanged += new System.EventHandler(this._voltageDefenseMainConstraintRadio_CheckedChanged);
            // 
            // _voltageDefenseReservConstraintRadio
            // 
            this._voltageDefenseReservConstraintRadio.AutoSize = true;
            this._voltageDefenseReservConstraintRadio.Location = new System.Drawing.Point(98, 16);
            this._voltageDefenseReservConstraintRadio.Name = "_voltageDefenseReservConstraintRadio";
            this._voltageDefenseReservConstraintRadio.Size = new System.Drawing.Size(80, 17);
            this._voltageDefenseReservConstraintRadio.TabIndex = 7;
            this._voltageDefenseReservConstraintRadio.Text = "���������";
            this._voltageDefenseReservConstraintRadio.UseVisualStyleBackColor = true;
            // 
            // _voltageDefensesGrid2
            // 
            this._voltageDefensesGrid2.AllowUserToAddRows = false;
            this._voltageDefensesGrid2.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid2.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid2.AllowUserToResizeRows = false;
            this._voltageDefensesGrid2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._voltageDefensesGrid2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._voltageDefensesGrid2.BackgroundColor = System.Drawing.Color.White;
            this._voltageDefensesGrid2.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol2,
            this._voltageDefensesModeCol2,
            this._voltageDefensesBlockingNumberCol2,
            this._voltageDefensesParameterCol2,
            this._voltageDefensesWorkConstraintCol2,
            this._voltageDefensesWorkTimeCol2,
            this._voltageDefensesReturnCol2,
            this._voltageDefensesAPVreturnCol2,
            this._voltageDefensesReturnConstraintCol2,
            this._voltageDefensesReturnTimeCol2,
            this._voltageDefensesUROVcol2,
            this._voltageDefensesAPVcol2,
            this._voltageDefensesAVRcol2});
            this._voltageDefensesGrid2.Location = new System.Drawing.Point(9, 213);
            this._voltageDefensesGrid2.Name = "_voltageDefensesGrid2";
            this._voltageDefensesGrid2.RowHeadersVisible = false;
            this._voltageDefensesGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid2.RowsDefaultCellStyle = dataGridViewCellStyle29;
            this._voltageDefensesGrid2.RowTemplate.Height = 24;
            this._voltageDefensesGrid2.Size = new System.Drawing.Size(708, 129);
            this._voltageDefensesGrid2.TabIndex = 2;
            // 
            // _voltageDefensesNameCol2
            // 
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._voltageDefensesNameCol2.DefaultCellStyle = dataGridViewCellStyle28;
            this._voltageDefensesNameCol2.HeaderText = "";
            this._voltageDefensesNameCol2.Name = "_voltageDefensesNameCol2";
            this._voltageDefensesNameCol2.ReadOnly = true;
            this._voltageDefensesNameCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol2.Width = 5;
            // 
            // _voltageDefensesModeCol2
            // 
            this._voltageDefensesModeCol2.HeaderText = "�����";
            this._voltageDefensesModeCol2.Name = "_voltageDefensesModeCol2";
            this._voltageDefensesModeCol2.Width = 48;
            // 
            // _voltageDefensesAPVcol2
            // 
            this._voltageDefensesAPVcol2.HeaderText = "���";
            this._voltageDefensesAPVcol2.Name = "_voltageDefensesAPVcol2";
            this._voltageDefensesAPVcol2.Width = 35;
            // 
            // _voltageDefensesAVRcol2
            // 
            this._voltageDefensesAVRcol2.HeaderText = "���";
            this._voltageDefensesAVRcol2.Name = "_voltageDefensesAVRcol2";
            this._voltageDefensesAVRcol2.Width = 34;
            // 
            // _voltageDefensesGrid1
            // 
            this._voltageDefensesGrid1.AllowUserToAddRows = false;
            this._voltageDefensesGrid1.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid1.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid1.AllowUserToResizeRows = false;
            this._voltageDefensesGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._voltageDefensesGrid1.BackgroundColor = System.Drawing.Color.White;
            this._voltageDefensesGrid1.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol1,
            this._voltageDefensesModeCol1,
            this._voltageDefensesBlockingNumberCol1,
            this._voltageDefensesParameterCol1,
            this._voltageDefensesWorkConstraintCol1,
            this._voltageDefensesWorkTimeCol1,
            this._voltageDefensesReturnCol1,
            this._voltageDefensesAPVReturlCol1,
            this._voltageDefensesReturnConstraintCol1,
            this._voltageDefensesReturnTimeCol1,
            this._voltageDefensesUROVCol1,
            this._voltageDefensesAPVCol1,
            this._voltageDefensesAVRCol1});
            this._voltageDefensesGrid1.Location = new System.Drawing.Point(8, 51);
            this._voltageDefensesGrid1.Name = "_voltageDefensesGrid1";
            this._voltageDefensesGrid1.RowHeadersVisible = false;
            this._voltageDefensesGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid1.RowsDefaultCellStyle = dataGridViewCellStyle33;
            this._voltageDefensesGrid1.RowTemplate.Height = 24;
            this._voltageDefensesGrid1.Size = new System.Drawing.Size(708, 156);
            this._voltageDefensesGrid1.TabIndex = 1;
            // 
            // _voltageDefensesNameCol1
            // 
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesNameCol1.DefaultCellStyle = dataGridViewCellStyle30;
            this._voltageDefensesNameCol1.HeaderText = "";
            this._voltageDefensesNameCol1.Name = "_voltageDefensesNameCol1";
            this._voltageDefensesNameCol1.ReadOnly = true;
            this._voltageDefensesNameCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol1.Width = 5;
            // 
            // _voltageDefensesModeCol1
            // 
            this._voltageDefensesModeCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._voltageDefensesModeCol1.HeaderText = "�����";
            this._voltageDefensesModeCol1.Name = "_voltageDefensesModeCol1";
            // 
            // _voltageDefensesBlockingNumberCol1
            // 
            this._voltageDefensesBlockingNumberCol1.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol1.Name = "_voltageDefensesBlockingNumberCol1";
            this._voltageDefensesBlockingNumberCol1.Width = 74;
            // 
            // _voltageDefensesParameterCol1
            // 
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesParameterCol1.DefaultCellStyle = dataGridViewCellStyle31;
            this._voltageDefensesParameterCol1.HeaderText = "��������";
            this._voltageDefensesParameterCol1.Name = "_voltageDefensesParameterCol1";
            this._voltageDefensesParameterCol1.Width = 64;
            // 
            // _voltageDefensesWorkConstraintCol1
            // 
            this._voltageDefensesWorkConstraintCol1.HeaderText = "������� ����.";
            this._voltageDefensesWorkConstraintCol1.Name = "_voltageDefensesWorkConstraintCol1";
            this._voltageDefensesWorkConstraintCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkConstraintCol1.Width = 86;
            // 
            // _voltageDefensesWorkTimeCol1
            // 
            dataGridViewCellStyle32.NullValue = "0";
            this._voltageDefensesWorkTimeCol1.DefaultCellStyle = dataGridViewCellStyle32;
            this._voltageDefensesWorkTimeCol1.HeaderText = "����� ����.";
            this._voltageDefensesWorkTimeCol1.Name = "_voltageDefensesWorkTimeCol1";
            this._voltageDefensesWorkTimeCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkTimeCol1.Width = 76;
            // 
            // _voltageDefensesReturnCol1
            // 
            this._voltageDefensesReturnCol1.HeaderText = "����.";
            this._voltageDefensesReturnCol1.Name = "_voltageDefensesReturnCol1";
            this._voltageDefensesReturnCol1.Width = 41;
            // 
            // _voltageDefensesAPVReturlCol1
            // 
            this._voltageDefensesAPVReturlCol1.HeaderText = "��� ��";
            this._voltageDefensesAPVReturlCol1.Name = "_voltageDefensesAPVReturlCol1";
            this._voltageDefensesAPVReturlCol1.Width = 52;
            // 
            // _voltageDefensesReturnConstraintCol1
            // 
            this._voltageDefensesReturnConstraintCol1.HeaderText = "������� ��������";
            this._voltageDefensesReturnConstraintCol1.Name = "_voltageDefensesReturnConstraintCol1";
            this._voltageDefensesReturnConstraintCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnConstraintCol1.Width = 106;
            // 
            // _voltageDefensesReturnTimeCol1
            // 
            this._voltageDefensesReturnTimeCol1.HeaderText = "����� ��������";
            this._voltageDefensesReturnTimeCol1.Name = "_voltageDefensesReturnTimeCol1";
            this._voltageDefensesReturnTimeCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnTimeCol1.Width = 96;
            // 
            // _voltageDefensesUROVCol1
            // 
            this._voltageDefensesUROVCol1.HeaderText = "����";
            this._voltageDefensesUROVCol1.Name = "_voltageDefensesUROVCol1";
            this._voltageDefensesUROVCol1.Width = 43;
            // 
            // _voltageDefensesAPVCol1
            // 
            this._voltageDefensesAPVCol1.HeaderText = "���";
            this._voltageDefensesAPVCol1.Name = "_voltageDefensesAPVCol1";
            this._voltageDefensesAPVCol1.Width = 35;
            // 
            // _voltageDefensesAVRCol1
            // 
            this._voltageDefensesAVRCol1.HeaderText = "���";
            this._voltageDefensesAVRCol1.Name = "_voltageDefensesAVRCol1";
            this._voltageDefensesAVRCol1.Width = 34;
            // 
            // _voltageDefensesGrid3
            // 
            this._voltageDefensesGrid3.AllowUserToAddRows = false;
            this._voltageDefensesGrid3.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid3.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid3.AllowUserToResizeRows = false;
            this._voltageDefensesGrid3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._voltageDefensesGrid3.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._voltageDefensesGrid3.BackgroundColor = System.Drawing.Color.White;
            this._voltageDefensesGrid3.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol3,
            this._voltageDefensesModeCol3,
            this._voltageDefensesBlockingNumberCol3,
            this._voltageDefensesParameterCol3,
            this._voltageDefensesWorkConstraintCol3,
            this._voltageDefensesTimeConstraintCol3,
            this._voltageDefensesReturnCol3,
            this._voltageDefensesAPVreturnCol3,
            this._voltageDefensesReturnConstraintCol3,
            this._voltageDefensesReturnTimeCol3,
            this._voltageDefensesUROVcol3,
            this._voltageDefensesAPVcol3,
            this._voltageDefensesAVRcol3});
            this._voltageDefensesGrid3.Location = new System.Drawing.Point(9, 340);
            this._voltageDefensesGrid3.Name = "_voltageDefensesGrid3";
            this._voltageDefensesGrid3.RowHeadersVisible = false;
            this._voltageDefensesGrid3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid3.RowsDefaultCellStyle = dataGridViewCellStyle35;
            this._voltageDefensesGrid3.RowTemplate.Height = 24;
            this._voltageDefensesGrid3.Size = new System.Drawing.Size(708, 118);
            this._voltageDefensesGrid3.TabIndex = 3;
            // 
            // _voltageDefensesNameCol3
            // 
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._voltageDefensesNameCol3.DefaultCellStyle = dataGridViewCellStyle34;
            this._voltageDefensesNameCol3.HeaderText = "";
            this._voltageDefensesNameCol3.Name = "_voltageDefensesNameCol3";
            this._voltageDefensesNameCol3.ReadOnly = true;
            this._voltageDefensesNameCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol3.Width = 5;
            // 
            // _voltageDefensesModeCol3
            // 
            this._voltageDefensesModeCol3.HeaderText = "�����";
            this._voltageDefensesModeCol3.Name = "_voltageDefensesModeCol3";
            this._voltageDefensesModeCol3.Width = 48;
            // 
            // _constraintGroupCombo
            // 
            this._constraintGroupCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._constraintGroupCombo.FormattingEnabled = true;
            this._constraintGroupCombo.Location = new System.Drawing.Point(124, 107);
            this._constraintGroupCombo.Name = "_constraintGroupCombo";
            this._constraintGroupCombo.Size = new System.Drawing.Size(71, 21);
            this._constraintGroupCombo.TabIndex = 11;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 110);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "������ �������";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._constraintGroupCombo);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this._signalizationCombo);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this._extOffCombo);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this._extOnCombo);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this._keyOffCombo);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this._keyOnCombo);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(8, 226);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(203, 134);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "������� �������";
            // 
            // _signalizationCombo
            // 
            this._signalizationCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._signalizationCombo.FormattingEnabled = true;
            this._signalizationCombo.Location = new System.Drawing.Point(124, 88);
            this._signalizationCombo.Name = "_signalizationCombo";
            this._signalizationCombo.Size = new System.Drawing.Size(71, 21);
            this._signalizationCombo.TabIndex = 9;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 91);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(112, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "����� ������������";
            // 
            // _extOffCombo
            // 
            this._extOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._extOffCombo.FormattingEnabled = true;
            this._extOffCombo.Location = new System.Drawing.Point(124, 69);
            this._extOffCombo.Name = "_extOffCombo";
            this._extOffCombo.Size = new System.Drawing.Size(71, 21);
            this._extOffCombo.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "����. ���������";
            // 
            // _extOnCombo
            // 
            this._extOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._extOnCombo.FormattingEnabled = true;
            this._extOnCombo.Location = new System.Drawing.Point(124, 50);
            this._extOnCombo.Name = "_extOnCombo";
            this._extOnCombo.Size = new System.Drawing.Size(71, 21);
            this._extOnCombo.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 72);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "����. ��������";
            // 
            // _keyOffCombo
            // 
            this._keyOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._keyOffCombo.FormattingEnabled = true;
            this._keyOffCombo.Location = new System.Drawing.Point(124, 31);
            this._keyOffCombo.Name = "_keyOffCombo";
            this._keyOffCombo.Size = new System.Drawing.Size(71, 21);
            this._keyOffCombo.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "���� ���������";
            // 
            // _keyOnCombo
            // 
            this._keyOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._keyOnCombo.FormattingEnabled = true;
            this._keyOnCombo.Location = new System.Drawing.Point(124, 12);
            this._keyOnCombo.Name = "_keyOnCombo";
            this._keyOnCombo.Size = new System.Drawing.Size(71, 21);
            this._keyOnCombo.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "���� ��������";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(5, 129);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(90, 13);
            this.label20.TabIndex = 21;
            this.label20.Text = "������� ��, ��";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(5, 91);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(93, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "����� ����, ��";
            // 
            // _switcherImpulseBox
            // 
            this._switcherImpulseBox.Location = new System.Drawing.Point(125, 127);
            this._switcherImpulseBox.Name = "_switcherImpulseBox";
            this._switcherImpulseBox.Size = new System.Drawing.Size(71, 20);
            this._switcherImpulseBox.TabIndex = 20;
            this._switcherImpulseBox.Tag = "3000000";
            this._switcherImpulseBox.Text = "0";
            this._switcherImpulseBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherBlockCombo
            // 
            this._switcherBlockCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherBlockCombo.FormattingEnabled = true;
            this._switcherBlockCombo.Location = new System.Drawing.Point(125, 68);
            this._switcherBlockCombo.Name = "_switcherBlockCombo";
            this._switcherBlockCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherBlockCombo.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "����. ��� ��������";
            // 
            // _switcherTimeBox
            // 
            this._switcherTimeBox.Location = new System.Drawing.Point(125, 87);
            this._switcherTimeBox.Name = "_switcherTimeBox";
            this._switcherTimeBox.Size = new System.Drawing.Size(71, 20);
            this._switcherTimeBox.TabIndex = 16;
            this._switcherTimeBox.Tag = "3000000";
            this._switcherTimeBox.Text = "0";
            this._switcherTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "����������";
            // 
            // _switcherErrorCombo
            // 
            this._switcherErrorCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherErrorCombo.FormattingEnabled = true;
            this._switcherErrorCombo.Location = new System.Drawing.Point(125, 49);
            this._switcherErrorCombo.Name = "_switcherErrorCombo";
            this._switcherErrorCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherErrorCombo.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(5, 53);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "�������������";
            // 
            // _switcherStateOnCombo
            // 
            this._switcherStateOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherStateOnCombo.FormattingEnabled = true;
            this._switcherStateOnCombo.Location = new System.Drawing.Point(125, 30);
            this._switcherStateOnCombo.Name = "_switcherStateOnCombo";
            this._switcherStateOnCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherStateOnCombo.TabIndex = 11;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(5, 34);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(113, 13);
            this.label15.TabIndex = 10;
            this.label15.Text = "��������� ��������";
            // 
            // _switcherStateOffCombo
            // 
            this._switcherStateOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherStateOffCombo.FormattingEnabled = true;
            this._switcherStateOffCombo.Location = new System.Drawing.Point(125, 11);
            this._switcherStateOffCombo.Name = "_switcherStateOffCombo";
            this._switcherStateOffCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherStateOffCombo.TabIndex = 9;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(5, 15);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(118, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "��������� ���������";
            // 
            // _TNNP_dispepairCombo
            // 
            this._TNNP_dispepairCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TNNP_dispepairCombo.FormattingEnabled = true;
            this._TNNP_dispepairCombo.Location = new System.Drawing.Point(94, 77);
            this._TNNP_dispepairCombo.Name = "_TNNP_dispepairCombo";
            this._TNNP_dispepairCombo.Size = new System.Drawing.Size(101, 21);
            this._TNNP_dispepairCombo.TabIndex = 10;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(6, 81);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(82, 13);
            this.label41.TabIndex = 9;
            this.label41.Text = "������. ����";
            // 
            // _TN_dispepairCombo
            // 
            this._TN_dispepairCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TN_dispepairCombo.FormattingEnabled = true;
            this._TN_dispepairCombo.Location = new System.Drawing.Point(94, 56);
            this._TN_dispepairCombo.Name = "_TN_dispepairCombo";
            this._TN_dispepairCombo.Size = new System.Drawing.Size(101, 21);
            this._TN_dispepairCombo.TabIndex = 8;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 60);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(66, 13);
            this.label39.TabIndex = 7;
            this.label39.Text = "������. ��";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "����������� ����";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this._maxTok_Box);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._TTNP_Box);
            this.groupBox1.Controls.Add(this._TT_Box);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(203, 82);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "��������� ����";
            // 
            // _maxTok_Box
            // 
            this._maxTok_Box.Location = new System.Drawing.Point(124, 52);
            this._maxTok_Box.Name = "_maxTok_Box";
            this._maxTok_Box.Size = new System.Drawing.Size(71, 20);
            this._maxTok_Box.TabIndex = 5;
            this._maxTok_Box.Tag = "40";
            this._maxTok_Box.Text = "0";
            this._maxTok_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "��������� ��� ��";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "��������� ��� ����";
            // 
            // _TTNP_Box
            // 
            this._TTNP_Box.Location = new System.Drawing.Point(124, 32);
            this._TTNP_Box.Name = "_TTNP_Box";
            this._TTNP_Box.Size = new System.Drawing.Size(71, 20);
            this._TTNP_Box.TabIndex = 2;
            this._TTNP_Box.Tag = "100";
            this._TTNP_Box.Text = "0";
            this._TTNP_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _TNNP_Box
            // 
            this._TNNP_Box.Location = new System.Drawing.Point(125, 36);
            this._TNNP_Box.Name = "_TNNP_Box";
            this._TNNP_Box.Size = new System.Drawing.Size(70, 20);
            this._TNNP_Box.TabIndex = 5;
            this._TNNP_Box.Tag = "256";
            this._TNNP_Box.Text = "0";
            this._TNNP_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "����������� ��";
            // 
            // _TN_Box
            // 
            this._TN_Box.Location = new System.Drawing.Point(125, 16);
            this._TN_Box.Name = "_TN_Box";
            this._TN_Box.Size = new System.Drawing.Size(70, 20);
            this._TN_Box.TabIndex = 2;
            this._TN_Box.Tag = "256";
            this._TN_Box.Text = "0";
            this._TN_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _TN_typeCombo
            // 
            this._TN_typeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TN_typeCombo.FormattingEnabled = true;
            this._TN_typeCombo.Location = new System.Drawing.Point(56, 98);
            this._TN_typeCombo.Name = "_TN_typeCombo";
            this._TN_typeCombo.Size = new System.Drawing.Size(139, 21);
            this._TN_typeCombo.TabIndex = 12;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._TN_typeCombo);
            this.groupBox2.Controls.Add(this.label42);
            this.groupBox2.Controls.Add(this._TNNP_dispepairCombo);
            this.groupBox2.Controls.Add(this.label41);
            this.groupBox2.Controls.Add(this._TN_dispepairCombo);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this._TNNP_Box);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this._TN_Box);
            this.groupBox2.Location = new System.Drawing.Point(8, 94);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(203, 126);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "��� ��";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(6, 101);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(44, 13);
            this.label42.TabIndex = 11;
            this.label42.Text = "��� ��";
            // 
            // _switcherTokBox
            // 
            this._switcherTokBox.Location = new System.Drawing.Point(125, 107);
            this._switcherTokBox.Name = "_switcherTokBox";
            this._switcherTokBox.Size = new System.Drawing.Size(71, 20);
            this._switcherTokBox.TabIndex = 18;
            this._switcherTokBox.Tag = "40";
            this._switcherTokBox.Text = "0";
            this._switcherTokBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(8, 166);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(81, 13);
            this.label56.TabIndex = 33;
            this.label56.Text = "����� 4 �����";
            // 
            // apv_time_4krat
            // 
            this.apv_time_4krat.Location = new System.Drawing.Point(146, 161);
            this.apv_time_4krat.Name = "apv_time_4krat";
            this.apv_time_4krat.Size = new System.Drawing.Size(85, 20);
            this.apv_time_4krat.TabIndex = 23;
            this.apv_time_4krat.Tag = "3000000";
            this.apv_time_4krat.Text = "0";
            this.apv_time_4krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(8, 146);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(81, 13);
            this.label52.TabIndex = 32;
            this.label52.Text = "����� 3 �����";
            // 
            // apv_time_3krat
            // 
            this.apv_time_3krat.Location = new System.Drawing.Point(146, 141);
            this.apv_time_3krat.Name = "apv_time_3krat";
            this.apv_time_3krat.Size = new System.Drawing.Size(85, 20);
            this.apv_time_3krat.TabIndex = 22;
            this.apv_time_3krat.Tag = "3000000";
            this.apv_time_3krat.Text = "0";
            this.apv_time_3krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(8, 126);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(81, 13);
            this.label53.TabIndex = 31;
            this.label53.Text = "����� 2 �����";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._manageSignalsSDTU_Combo);
            this.groupBox7.Controls.Add(this.label43);
            this.groupBox7.Controls.Add(this._manageSignalsExternalCombo);
            this.groupBox7.Controls.Add(this.label44);
            this.groupBox7.Controls.Add(this._manageSignalsKeyCombo);
            this.groupBox7.Controls.Add(this.label45);
            this.groupBox7.Controls.Add(this._manageSignalsButtonCombo);
            this.groupBox7.Controls.Add(this.label46);
            this.groupBox7.Location = new System.Drawing.Point(217, 180);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(203, 105);
            this.groupBox7.TabIndex = 7;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "������� ����������";
            // 
            // _manageSignalsSDTU_Combo
            // 
            this._manageSignalsSDTU_Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsSDTU_Combo.FormattingEnabled = true;
            this._manageSignalsSDTU_Combo.Location = new System.Drawing.Point(71, 75);
            this._manageSignalsSDTU_Combo.Name = "_manageSignalsSDTU_Combo";
            this._manageSignalsSDTU_Combo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsSDTU_Combo.TabIndex = 23;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(6, 79);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(54, 13);
            this.label43.TabIndex = 22;
            this.label43.Text = "�� ����";
            // 
            // _manageSignalsExternalCombo
            // 
            this._manageSignalsExternalCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsExternalCombo.FormattingEnabled = true;
            this._manageSignalsExternalCombo.Location = new System.Drawing.Point(71, 56);
            this._manageSignalsExternalCombo.Name = "_manageSignalsExternalCombo";
            this._manageSignalsExternalCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsExternalCombo.TabIndex = 21;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 60);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(52, 13);
            this.label44.TabIndex = 20;
            this.label44.Text = "�������";
            // 
            // _manageSignalsKeyCombo
            // 
            this._manageSignalsKeyCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsKeyCombo.FormattingEnabled = true;
            this._manageSignalsKeyCombo.Location = new System.Drawing.Point(71, 37);
            this._manageSignalsKeyCombo.Name = "_manageSignalsKeyCombo";
            this._manageSignalsKeyCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsKeyCombo.TabIndex = 19;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(6, 41);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(54, 13);
            this.label45.TabIndex = 18;
            this.label45.Text = "�� �����";
            // 
            // _manageSignalsButtonCombo
            // 
            this._manageSignalsButtonCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsButtonCombo.FormattingEnabled = true;
            this._manageSignalsButtonCombo.Location = new System.Drawing.Point(71, 17);
            this._manageSignalsButtonCombo.Name = "_manageSignalsButtonCombo";
            this._manageSignalsButtonCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsButtonCombo.TabIndex = 17;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(6, 22);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(59, 13);
            this.label46.TabIndex = 16;
            this.label46.Text = "�� ������";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(8, 186);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(132, 13);
            this.label55.TabIndex = 34;
            this.label55.Text = "������ ��� �� �������.";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(8, 26);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(80, 13);
            this.label47.TabIndex = 25;
            this.label47.Text = "������������";
            // 
            // _inSignalsPage
            // 
            this._inSignalsPage.Controls.Add(this.groupBox18);
            this._inSignalsPage.Controls.Add(this.groupBox7);
            this._inSignalsPage.Controls.Add(this.groupBox6);
            this._inSignalsPage.Controls.Add(this.groupBox5);
            this._inSignalsPage.Controls.Add(this.groupBox4);
            this._inSignalsPage.Controls.Add(this.groupBox3);
            this._inSignalsPage.Controls.Add(this.groupBox2);
            this._inSignalsPage.Controls.Add(this.groupBox1);
            this._inSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._inSignalsPage.Name = "_inSignalsPage";
            this._inSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._inSignalsPage.Size = new System.Drawing.Size(725, 528);
            this._inSignalsPage.TabIndex = 0;
            this._inSignalsPage.Text = "������� �������";
            this._inSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this._HUD_Box);
            this.groupBox18.Controls.Add(this._OMP_TypeBox);
            this.groupBox18.Controls.Add(this.label26);
            this.groupBox18.Controls.Add(this.label27);
            this.groupBox18.Location = new System.Drawing.Point(217, 291);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(203, 69);
            this.groupBox18.TabIndex = 8;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "����������� ����� �����������";
            // 
            // _HUD_Box
            // 
            this._HUD_Box.Location = new System.Drawing.Point(116, 42);
            this._HUD_Box.Name = "_HUD_Box";
            this._HUD_Box.Size = new System.Drawing.Size(81, 20);
            this._HUD_Box.TabIndex = 20;
            this._HUD_Box.Tag = "3000000";
            this._HUD_Box.Text = "0";
            this._HUD_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _OMP_TypeBox
            // 
            this._OMP_TypeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._OMP_TypeBox.FormattingEnabled = true;
            this._OMP_TypeBox.Location = new System.Drawing.Point(116, 20);
            this._OMP_TypeBox.Name = "_OMP_TypeBox";
            this._OMP_TypeBox.Size = new System.Drawing.Size(81, 21);
            this._OMP_TypeBox.TabIndex = 19;
            this._OMP_TypeBox.SelectedIndexChanged += new System.EventHandler(this._OMP_TypeBox_SelectedIndexChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 45);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(70, 13);
            this.label26.TabIndex = 18;
            this.label26.Text = "��� (��/��)";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 23);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(108, 13);
            this.label27.TabIndex = 16;
            this.label27.Text = "������������ ���";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox22);
            this.groupBox6.Controls.Add(this.groupBox21);
            this.groupBox6.Location = new System.Drawing.Point(583, 10);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(141, 224);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "���� �������������";
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this._releDispepairBox);
            this.groupBox22.Location = new System.Drawing.Point(6, 15);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(123, 46);
            this.groupBox22.TabIndex = 8;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "�������";
            // 
            // _releDispepairBox
            // 
            this._releDispepairBox.Location = new System.Drawing.Point(10, 17);
            this._releDispepairBox.Name = "_releDispepairBox";
            this._releDispepairBox.Size = new System.Drawing.Size(107, 20);
            this._releDispepairBox.TabIndex = 9;
            this._releDispepairBox.Tag = "3000000";
            this._releDispepairBox.Text = "0";
            this._releDispepairBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._dispepairCheckList);
            this.groupBox21.Location = new System.Drawing.Point(6, 67);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(123, 144);
            this.groupBox21.TabIndex = 8;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "������";
            // 
            // _dispepairCheckList
            // 
            this._dispepairCheckList.CheckOnClick = true;
            this._dispepairCheckList.FormattingEnabled = true;
            this._dispepairCheckList.Items.AddRange(new object[] {
            "������������� 1",
            "������������� 2",
            "������������� 3",
            "������������� 4",
            "������������� 5",
            "������������� 6",
            "������������� 7",
            "������������� 8"});
            this._dispepairCheckList.Location = new System.Drawing.Point(4, 13);
            this._dispepairCheckList.Name = "_dispepairCheckList";
            this._dispepairCheckList.Size = new System.Drawing.Size(116, 124);
            this._dispepairCheckList.TabIndex = 5;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._applyLogicSignalsBut);
            this.groupBox5.Controls.Add(this._logicChannelsCombo);
            this.groupBox5.Controls.Add(this._logicSignalsDataGrid);
            this.groupBox5.Location = new System.Drawing.Point(426, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(151, 426);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "���������� �������";
            // 
            // _applyLogicSignalsBut
            // 
            this._applyLogicSignalsBut.Location = new System.Drawing.Point(46, 397);
            this._applyLogicSignalsBut.Name = "_applyLogicSignalsBut";
            this._applyLogicSignalsBut.Size = new System.Drawing.Size(62, 23);
            this._applyLogicSignalsBut.TabIndex = 3;
            this._applyLogicSignalsBut.Text = "�������";
            this._applyLogicSignalsBut.UseVisualStyleBackColor = true;
            this._applyLogicSignalsBut.Click += new System.EventHandler(this._applyLogicSignalsBut_Click);
            // 
            // _logicChannelsCombo
            // 
            this._logicChannelsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._logicChannelsCombo.FormattingEnabled = true;
            this._logicChannelsCombo.Items.AddRange(new object[] {
            "1 �",
            "2 �",
            "3 �",
            "4 �",
            "5 ���",
            "6 ���",
            "7 ���",
            "8 ���"});
            this._logicChannelsCombo.Location = new System.Drawing.Point(34, 19);
            this._logicChannelsCombo.Name = "_logicChannelsCombo";
            this._logicChannelsCombo.Size = new System.Drawing.Size(64, 21);
            this._logicChannelsCombo.TabIndex = 1;
            this._logicChannelsCombo.SelectedIndexChanged += new System.EventHandler(this._logicChannelsCombo_SelectedIndexChanged);
            // 
            // _logicSignalsDataGrid
            // 
            this._logicSignalsDataGrid.AllowUserToAddRows = false;
            this._logicSignalsDataGrid.AllowUserToDeleteRows = false;
            this._logicSignalsDataGrid.AllowUserToResizeColumns = false;
            this._logicSignalsDataGrid.AllowUserToResizeRows = false;
            this._logicSignalsDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._logicSignalsDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._logicSignalsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._logicSignalsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._diskretChannelCol,
            this._diskretValueCol});
            this._logicSignalsDataGrid.Location = new System.Drawing.Point(11, 45);
            this._logicSignalsDataGrid.MultiSelect = false;
            this._logicSignalsDataGrid.Name = "_logicSignalsDataGrid";
            this._logicSignalsDataGrid.RowHeadersVisible = false;
            this._logicSignalsDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._logicSignalsDataGrid.RowsDefaultCellStyle = dataGridViewCellStyle36;
            this._logicSignalsDataGrid.RowTemplate.Height = 20;
            this._logicSignalsDataGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._logicSignalsDataGrid.ShowCellErrors = false;
            this._logicSignalsDataGrid.ShowCellToolTips = false;
            this._logicSignalsDataGrid.ShowEditingIcon = false;
            this._logicSignalsDataGrid.ShowRowErrors = false;
            this._logicSignalsDataGrid.Size = new System.Drawing.Size(132, 346);
            this._logicSignalsDataGrid.TabIndex = 0;
            // 
            // _diskretChannelCol
            // 
            this._diskretChannelCol.HeaderText = "�";
            this._diskretChannelCol.Name = "_diskretChannelCol";
            this._diskretChannelCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._diskretChannelCol.Width = 24;
            // 
            // _diskretValueCol
            // 
            this._diskretValueCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._diskretValueCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._diskretValueCol.HeaderText = "��������";
            this._diskretValueCol.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this._diskretValueCol.Name = "_diskretValueCol";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this._switcherDurationBox);
            this.groupBox4.Controls.Add(this._switcherTokBox);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this._switcherTimeBox);
            this.groupBox4.Controls.Add(this._switcherImpulseBox);
            this.groupBox4.Controls.Add(this._switcherBlockCombo);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this._switcherErrorCombo);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this._switcherStateOnCombo);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this._switcherStateOffCombo);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Location = new System.Drawing.Point(217, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(203, 172);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "�����������";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(5, 148);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(94, 13);
            this.label19.TabIndex = 23;
            this.label19.Text = "����. ���������";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(5, 110);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 13);
            this.label17.TabIndex = 19;
            this.label17.Text = "��� ����, In";
            // 
            // _switcherDurationBox
            // 
            this._switcherDurationBox.Location = new System.Drawing.Point(125, 147);
            this._switcherDurationBox.Name = "_switcherDurationBox";
            this._switcherDurationBox.Size = new System.Drawing.Size(71, 20);
            this._switcherDurationBox.TabIndex = 22;
            this._switcherDurationBox.Tag = "3000000";
            this._switcherDurationBox.Text = "0";
            this._switcherDurationBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // apv_time_2krat
            // 
            this.apv_time_2krat.Location = new System.Drawing.Point(146, 121);
            this.apv_time_2krat.Name = "apv_time_2krat";
            this.apv_time_2krat.Size = new System.Drawing.Size(85, 20);
            this.apv_time_2krat.TabIndex = 21;
            this.apv_time_2krat.Tag = "3000000";
            this.apv_time_2krat.Text = "0";
            this.apv_time_2krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _tabControl
            // 
            this._tabControl.Controls.Add(this._inSignalsPage);
            this._tabControl.Controls.Add(this._outputSignalsPage);
            this._tabControl.Controls.Add(this._externalDefensePage);
            this._tabControl.Controls.Add(this._automaticPage);
            this._tabControl.Controls.Add(this._tokDefensesPage);
            this._tabControl.Controls.Add(this._voltageDefensesPage);
            this._tabControl.Controls.Add(this._frequencePage);
            this._tabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._tabControl.Location = new System.Drawing.Point(0, 0);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(733, 554);
            this._tabControl.TabIndex = 6;
            // 
            // _outputSignalsPage
            // 
            this._outputSignalsPage.Controls.Add(this.groupBox10);
            this._outputSignalsPage.Controls.Add(this.groupBox9);
            this._outputSignalsPage.Controls.Add(this.groupBox8);
            this._outputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._outputSignalsPage.Name = "_outputSignalsPage";
            this._outputSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._outputSignalsPage.Size = new System.Drawing.Size(725, 528);
            this._outputSignalsPage.TabIndex = 1;
            this._outputSignalsPage.Text = "�������� �������";
            this._outputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._outputLogicCheckList);
            this.groupBox10.Controls.Add(this._outputLogicAcceptBut);
            this.groupBox10.Controls.Add(this._outputLogicCombo);
            this.groupBox10.Location = new System.Drawing.Point(438, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(141, 453);
            this.groupBox10.TabIndex = 5;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "���������� �������";
            // 
            // _outputLogicCheckList
            // 
            this._outputLogicCheckList.CheckOnClick = true;
            this._outputLogicCheckList.FormattingEnabled = true;
            this._outputLogicCheckList.Location = new System.Drawing.Point(3, 48);
            this._outputLogicCheckList.Name = "_outputLogicCheckList";
            this._outputLogicCheckList.ScrollAlwaysVisible = true;
            this._outputLogicCheckList.Size = new System.Drawing.Size(135, 364);
            this._outputLogicCheckList.TabIndex = 4;
            // 
            // _outputLogicAcceptBut
            // 
            this._outputLogicAcceptBut.Location = new System.Drawing.Point(40, 422);
            this._outputLogicAcceptBut.Name = "_outputLogicAcceptBut";
            this._outputLogicAcceptBut.Size = new System.Drawing.Size(59, 23);
            this._outputLogicAcceptBut.TabIndex = 3;
            this._outputLogicAcceptBut.Text = "�������";
            this._outputLogicAcceptBut.UseVisualStyleBackColor = true;
            this._outputLogicAcceptBut.Click += new System.EventHandler(this._outputLogicAcceptBut_Click);
            // 
            // _outputLogicCombo
            // 
            this._outputLogicCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._outputLogicCombo.FormattingEnabled = true;
            this._outputLogicCombo.Items.AddRange(new object[] {
            "��� 1",
            "��� 2 ",
            "��� 3 ",
            "��� 4 ",
            "��� 5",
            "��� 6",
            "��� 7 ",
            "��� 8"});
            this._outputLogicCombo.Location = new System.Drawing.Point(37, 19);
            this._outputLogicCombo.Name = "_outputLogicCombo";
            this._outputLogicCombo.Size = new System.Drawing.Size(66, 21);
            this._outputLogicCombo.TabIndex = 1;
            this._outputLogicCombo.SelectedIndexChanged += new System.EventHandler(this._outputLogicCombo_SelectedIndexChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._outputIndicatorsGrid);
            this.groupBox9.Location = new System.Drawing.Point(11, 245);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(424, 250);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "����������";
            // 
            // _outputIndicatorsGrid
            // 
            this._outputIndicatorsGrid.AllowUserToAddRows = false;
            this._outputIndicatorsGrid.AllowUserToDeleteRows = false;
            this._outputIndicatorsGrid.AllowUserToResizeColumns = false;
            this._outputIndicatorsGrid.AllowUserToResizeRows = false;
            this._outputIndicatorsGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputIndicatorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputIndicatorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._outIndNumberCol,
            this._outIndTypeCol,
            this._outIndSignalCol,
            this._outIndResetCol,
            this._outIndAlarmCol,
            this._outIndSystemCol});
            this._outputIndicatorsGrid.Location = new System.Drawing.Point(8, 14);
            this._outputIndicatorsGrid.Name = "_outputIndicatorsGrid";
            this._outputIndicatorsGrid.RowHeadersVisible = false;
            this._outputIndicatorsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputIndicatorsGrid.RowsDefaultCellStyle = dataGridViewCellStyle37;
            this._outputIndicatorsGrid.RowTemplate.Height = 24;
            this._outputIndicatorsGrid.ShowCellErrors = false;
            this._outputIndicatorsGrid.ShowRowErrors = false;
            this._outputIndicatorsGrid.Size = new System.Drawing.Size(410, 230);
            this._outputIndicatorsGrid.TabIndex = 0;
            // 
            // _outIndNumberCol
            // 
            this._outIndNumberCol.HeaderText = "�";
            this._outIndNumberCol.Name = "_outIndNumberCol";
            this._outIndNumberCol.ReadOnly = true;
            this._outIndNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndNumberCol.Width = 20;
            // 
            // _outIndTypeCol
            // 
            this._outIndTypeCol.HeaderText = "���";
            this._outIndTypeCol.Items.AddRange(new object[] {
            "�����������",
            "�������",
            "XXXXX"});
            this._outIndTypeCol.Name = "_outIndTypeCol";
            this._outIndTypeCol.Width = 120;
            // 
            // _outIndSignalCol
            // 
            this._outIndSignalCol.HeaderText = "������";
            this._outIndSignalCol.Name = "_outIndSignalCol";
            this._outIndSignalCol.Width = 140;
            // 
            // _outIndResetCol
            // 
            this._outIndResetCol.HeaderText = "����� ���.";
            this._outIndResetCol.Name = "_outIndResetCol";
            this._outIndResetCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._outIndResetCol.Width = 40;
            // 
            // _outIndAlarmCol
            // 
            this._outIndAlarmCol.HeaderText = "����� ��";
            this._outIndAlarmCol.Name = "_outIndAlarmCol";
            this._outIndAlarmCol.Width = 40;
            // 
            // _outIndSystemCol
            // 
            this._outIndSystemCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._outIndSystemCol.HeaderText = "����� ��";
            this._outIndSystemCol.Name = "_outIndSystemCol";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._outputReleGrid);
            this.groupBox8.Location = new System.Drawing.Point(8, 5);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(424, 234);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "�������� ����";
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this._releSignalCol,
            this._releImpulseCol});
            this._outputReleGrid.Location = new System.Drawing.Point(9, 13);
            this._outputReleGrid.Name = "_outputReleGrid";
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle38;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(410, 215);
            this._outputReleGrid.TabIndex = 0;
            // 
            // _releNumberCol
            // 
            this._releNumberCol.HeaderText = "�";
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 20;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releTypeCol.HeaderText = "���";
            this._releTypeCol.Items.AddRange(new object[] {
            "�����������",
            "�������",
            "XXXXX"});
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 120;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releSignalCol.HeaderText = "������";
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 140;
            // 
            // _releImpulseCol
            // 
            this._releImpulseCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._releImpulseCol.HeaderText = "�������";
            this._releImpulseCol.Name = "_releImpulseCol";
            this._releImpulseCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _externalDefensePage
            // 
            this._externalDefensePage.Controls.Add(this._externalDefenseGrid);
            this._externalDefensePage.Location = new System.Drawing.Point(4, 22);
            this._externalDefensePage.Name = "_externalDefensePage";
            this._externalDefensePage.Size = new System.Drawing.Size(725, 528);
            this._externalDefensePage.TabIndex = 2;
            this._externalDefensePage.Text = "������� ������";
            this._externalDefensePage.UseVisualStyleBackColor = true;
            // 
            // _externalDefenseGrid
            // 
            this._externalDefenseGrid.AllowUserToAddRows = false;
            this._externalDefenseGrid.AllowUserToDeleteRows = false;
            this._externalDefenseGrid.AllowUserToResizeColumns = false;
            this._externalDefenseGrid.AllowUserToResizeRows = false;
            this._externalDefenseGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._externalDefenseGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._externalDefenseGrid.BackgroundColor = System.Drawing.Color.White;
            this._externalDefenseGrid.ColumnHeadersHeight = 30;
            this._externalDefenseGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._externalDefenseGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extDefNumberCol,
            this._exDefModeCol,
            this._exDefBlockingCol,
            this._exDefWorkingCol,
            this._exDefWorkingTimeCol,
            this._exDefReturnCol,
            this._exDefAPVreturnCol,
            this._exDefReturnNumberCol,
            this._exDefReturnTimeCol,
            this._exDefUROVcol,
            this._exDefAPVcol,
            this._exDefAVRcol});
            this._externalDefenseGrid.Location = new System.Drawing.Point(11, 18);
            this._externalDefenseGrid.Name = "_externalDefenseGrid";
            this._externalDefenseGrid.RowHeadersVisible = false;
            this._externalDefenseGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDefenseGrid.RowsDefaultCellStyle = dataGridViewCellStyle40;
            this._externalDefenseGrid.RowTemplate.Height = 24;
            this._externalDefenseGrid.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._externalDefenseGrid.Size = new System.Drawing.Size(702, 366);
            this._externalDefenseGrid.TabIndex = 0;
            // 
            // _extDefNumberCol
            // 
            this._extDefNumberCol.HeaderText = "�";
            this._extDefNumberCol.Name = "_extDefNumberCol";
            this._extDefNumberCol.ReadOnly = true;
            this._extDefNumberCol.Width = 43;
            // 
            // _exDefModeCol
            // 
            this._exDefModeCol.HeaderText = "�����";
            this._exDefModeCol.Name = "_exDefModeCol";
            this._exDefModeCol.Width = 48;
            // 
            // _exDefBlockingCol
            // 
            this._exDefBlockingCol.HeaderText = "����������";
            this._exDefBlockingCol.Name = "_exDefBlockingCol";
            this._exDefBlockingCol.Width = 74;
            // 
            // _exDefWorkingCol
            // 
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._exDefWorkingCol.DefaultCellStyle = dataGridViewCellStyle39;
            this._exDefWorkingCol.HeaderText = "������������";
            this._exDefWorkingCol.Name = "_exDefWorkingCol";
            this._exDefWorkingCol.Width = 87;
            // 
            // _exDefWorkingTimeCol
            // 
            this._exDefWorkingTimeCol.HeaderText = "����� ������.";
            this._exDefWorkingTimeCol.Name = "_exDefWorkingTimeCol";
            this._exDefWorkingTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._exDefWorkingTimeCol.Width = 87;
            // 
            // _exDefReturnCol
            // 
            this._exDefReturnCol.HeaderText = "����.";
            this._exDefReturnCol.Name = "_exDefReturnCol";
            this._exDefReturnCol.Width = 41;
            // 
            // _exDefAPVreturnCol
            // 
            this._exDefAPVreturnCol.HeaderText = "��� ��";
            this._exDefAPVreturnCol.Name = "_exDefAPVreturnCol";
            this._exDefAPVreturnCol.Width = 52;
            // 
            // _exDefReturnNumberCol
            // 
            this._exDefReturnNumberCol.HeaderText = "���� ��������";
            this._exDefReturnNumberCol.Name = "_exDefReturnNumberCol";
            this._exDefReturnNumberCol.Width = 87;
            // 
            // _exDefReturnTimeCol
            // 
            this._exDefReturnTimeCol.HeaderText = "����� ��������";
            this._exDefReturnTimeCol.Name = "_exDefReturnTimeCol";
            this._exDefReturnTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._exDefReturnTimeCol.Width = 96;
            // 
            // _exDefUROVcol
            // 
            this._exDefUROVcol.HeaderText = "����";
            this._exDefUROVcol.Name = "_exDefUROVcol";
            this._exDefUROVcol.Width = 43;
            // 
            // _exDefAPVcol
            // 
            this._exDefAPVcol.HeaderText = "���";
            this._exDefAPVcol.Name = "_exDefAPVcol";
            this._exDefAPVcol.Width = 35;
            // 
            // _exDefAVRcol
            // 
            this._exDefAVRcol.HeaderText = "���";
            this._exDefAVRcol.Name = "_exDefAVRcol";
            this._exDefAVRcol.Width = 34;
            // 
            // _automaticPage
            // 
            this._automaticPage.Controls.Add(this.groupBox11);
            this._automaticPage.Controls.Add(this.groupBox13);
            this._automaticPage.Controls.Add(this.groupBox14);
            this._automaticPage.Location = new System.Drawing.Point(4, 22);
            this._automaticPage.Name = "_automaticPage";
            this._automaticPage.Size = new System.Drawing.Size(725, 528);
            this._automaticPage.TabIndex = 3;
            this._automaticPage.Text = "����������";
            this._automaticPage.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label58);
            this.groupBox11.Controls.Add(this.label59);
            this.groupBox11.Controls.Add(this.avr_disconnection);
            this.groupBox11.Controls.Add(this.label61);
            this.groupBox11.Controls.Add(this.avr_time_return);
            this.groupBox11.Controls.Add(this.label62);
            this.groupBox11.Controls.Add(this.avr_time_abrasion);
            this.groupBox11.Controls.Add(this.label63);
            this.groupBox11.Controls.Add(this.avr_return);
            this.groupBox11.Controls.Add(this.label64);
            this.groupBox11.Controls.Add(this.label65);
            this.groupBox11.Controls.Add(this.avr_abrasion);
            this.groupBox11.Controls.Add(this.label66);
            this.groupBox11.Controls.Add(this.avr_reset_blocking);
            this.groupBox11.Controls.Add(this.avr_permit_reset_switch);
            this.groupBox11.Controls.Add(this.groupBox12);
            this.groupBox11.Controls.Add(this.avr_blocking);
            this.groupBox11.Controls.Add(this.avr_start);
            this.groupBox11.Location = new System.Drawing.Point(265, 1);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(256, 313);
            this.groupBox11.TabIndex = 24;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "������������ ���";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(9, 288);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(103, 13);
            this.label58.TabIndex = 42;
            this.label58.Text = "����� ����������";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(9, 148);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(67, 13);
            this.label59.TabIndex = 35;
            this.label59.Text = "������ ���";
            // 
            // avr_disconnection
            // 
            this.avr_disconnection.Location = new System.Drawing.Point(152, 286);
            this.avr_disconnection.Name = "avr_disconnection";
            this.avr_disconnection.Size = new System.Drawing.Size(85, 20);
            this.avr_disconnection.TabIndex = 31;
            this.avr_disconnection.Tag = "3000000";
            this.avr_disconnection.Text = "0";
            this.avr_disconnection.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(9, 268);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(90, 13);
            this.label61.TabIndex = 41;
            this.label61.Text = "����� ��������";
            // 
            // avr_time_return
            // 
            this.avr_time_return.Location = new System.Drawing.Point(152, 266);
            this.avr_time_return.Name = "avr_time_return";
            this.avr_time_return.Size = new System.Drawing.Size(85, 20);
            this.avr_time_return.TabIndex = 30;
            this.avr_time_return.Tag = "3000000";
            this.avr_time_return.Text = "0";
            this.avr_time_return.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(9, 248);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(49, 13);
            this.label62.TabIndex = 40;
            this.label62.Text = "�������";
            // 
            // avr_time_abrasion
            // 
            this.avr_time_abrasion.Location = new System.Drawing.Point(152, 225);
            this.avr_time_abrasion.Name = "avr_time_abrasion";
            this.avr_time_abrasion.Size = new System.Drawing.Size(85, 20);
            this.avr_time_abrasion.TabIndex = 29;
            this.avr_time_abrasion.Tag = "3000000";
            this.avr_time_abrasion.Text = "0";
            this.avr_time_abrasion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(9, 228);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(116, 13);
            this.label63.TabIndex = 39;
            this.label63.Text = "����� ������������";
            // 
            // avr_return
            // 
            this.avr_return.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_return.FormattingEnabled = true;
            this.avr_return.Location = new System.Drawing.Point(152, 245);
            this.avr_return.Name = "avr_return";
            this.avr_return.Size = new System.Drawing.Size(87, 21);
            this.avr_return.TabIndex = 28;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(9, 208);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(81, 13);
            this.label64.TabIndex = 38;
            this.label64.Text = "������������";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(9, 188);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(101, 13);
            this.label65.TabIndex = 37;
            this.label65.Text = "����� ����������";
            // 
            // avr_abrasion
            // 
            this.avr_abrasion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_abrasion.FormattingEnabled = true;
            this.avr_abrasion.Location = new System.Drawing.Point(152, 204);
            this.avr_abrasion.Name = "avr_abrasion";
            this.avr_abrasion.Size = new System.Drawing.Size(87, 21);
            this.avr_abrasion.TabIndex = 26;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(9, 168);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(68, 13);
            this.label66.TabIndex = 36;
            this.label66.Text = "����������";
            // 
            // avr_reset_blocking
            // 
            this.avr_reset_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_reset_blocking.FormattingEnabled = true;
            this.avr_reset_blocking.Location = new System.Drawing.Point(152, 183);
            this.avr_reset_blocking.Name = "avr_reset_blocking";
            this.avr_reset_blocking.Size = new System.Drawing.Size(87, 21);
            this.avr_reset_blocking.TabIndex = 24;
            // 
            // avr_permit_reset_switch
            // 
            this.avr_permit_reset_switch.Location = new System.Drawing.Point(12, 113);
            this.avr_permit_reset_switch.Name = "avr_permit_reset_switch";
            this.avr_permit_reset_switch.Size = new System.Drawing.Size(238, 24);
            this.avr_permit_reset_switch.TabIndex = 22;
            this.avr_permit_reset_switch.Text = "���������� ������ �� ���. �����������";
            this.avr_permit_reset_switch.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.avr_abrasion_switch);
            this.groupBox12.Controls.Add(this.avr_supply_off);
            this.groupBox12.Controls.Add(this.avr_self_off);
            this.groupBox12.Controls.Add(this.avr_switch_off);
            this.groupBox12.Location = new System.Drawing.Point(6, 13);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(233, 100);
            this.groupBox12.TabIndex = 19;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "���������� �������";
            // 
            // avr_abrasion_switch
            // 
            this.avr_abrasion_switch.Location = new System.Drawing.Point(6, 72);
            this.avr_abrasion_switch.Name = "avr_abrasion_switch";
            this.avr_abrasion_switch.Size = new System.Drawing.Size(186, 24);
            this.avr_abrasion_switch.TabIndex = 21;
            this.avr_abrasion_switch.Text = "�� ������";
            this.avr_abrasion_switch.UseVisualStyleBackColor = true;
            // 
            // avr_supply_off
            // 
            this.avr_supply_off.Location = new System.Drawing.Point(6, 15);
            this.avr_supply_off.Name = "avr_supply_off";
            this.avr_supply_off.Size = new System.Drawing.Size(139, 24);
            this.avr_supply_off.TabIndex = 18;
            this.avr_supply_off.Text = "�� �������";
            this.avr_supply_off.UseVisualStyleBackColor = true;
            // 
            // avr_self_off
            // 
            this.avr_self_off.Location = new System.Drawing.Point(6, 53);
            this.avr_self_off.Name = "avr_self_off";
            this.avr_self_off.Size = new System.Drawing.Size(186, 24);
            this.avr_self_off.TabIndex = 20;
            this.avr_self_off.Text = "��������������";
            this.avr_self_off.UseVisualStyleBackColor = true;
            // 
            // avr_switch_off
            // 
            this.avr_switch_off.Location = new System.Drawing.Point(6, 34);
            this.avr_switch_off.Name = "avr_switch_off";
            this.avr_switch_off.Size = new System.Drawing.Size(221, 24);
            this.avr_switch_off.TabIndex = 19;
            this.avr_switch_off.Text = "�� ����������";
            this.avr_switch_off.UseVisualStyleBackColor = true;
            // 
            // avr_blocking
            // 
            this.avr_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_blocking.FormattingEnabled = true;
            this.avr_blocking.Location = new System.Drawing.Point(152, 162);
            this.avr_blocking.Name = "avr_blocking";
            this.avr_blocking.Size = new System.Drawing.Size(87, 21);
            this.avr_blocking.TabIndex = 3;
            // 
            // avr_start
            // 
            this.avr_start.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_start.FormattingEnabled = true;
            this.avr_start.Location = new System.Drawing.Point(152, 141);
            this.avr_start.Name = "avr_start";
            this.avr_start.Size = new System.Drawing.Size(87, 21);
            this.avr_start.TabIndex = 1;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label57);
            this.groupBox13.Controls.Add(this.lzsh_constraint);
            this.groupBox13.Controls.Add(this.lzsh_conf);
            this.groupBox13.Controls.Add(this.label48);
            this.groupBox13.Location = new System.Drawing.Point(3, 228);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(256, 86);
            this.groupBox13.TabIndex = 23;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "��������� ���";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(164, 15);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(50, 13);
            this.label57.TabIndex = 27;
            this.label57.Text = "�������";
            // 
            // lzsh_constraint
            // 
            this.lzsh_constraint.Location = new System.Drawing.Point(146, 36);
            this.lzsh_constraint.Name = "lzsh_constraint";
            this.lzsh_constraint.Size = new System.Drawing.Size(85, 20);
            this.lzsh_constraint.TabIndex = 24;
            this.lzsh_constraint.Tag = "40";
            this.lzsh_constraint.Text = "0";
            this.lzsh_constraint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lzsh_conf
            // 
            this.lzsh_conf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lzsh_conf.FormattingEnabled = true;
            this.lzsh_conf.Location = new System.Drawing.Point(6, 36);
            this.lzsh_conf.Name = "lzsh_conf";
            this.lzsh_conf.Size = new System.Drawing.Size(87, 21);
            this.lzsh_conf.TabIndex = 18;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(9, 20);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(80, 13);
            this.label48.TabIndex = 26;
            this.label48.Text = "������������";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.label55);
            this.groupBox14.Controls.Add(this.label47);
            this.groupBox14.Controls.Add(this.label56);
            this.groupBox14.Controls.Add(this.apv_time_4krat);
            this.groupBox14.Controls.Add(this.label52);
            this.groupBox14.Controls.Add(this.apv_time_3krat);
            this.groupBox14.Controls.Add(this.label53);
            this.groupBox14.Controls.Add(this.apv_time_2krat);
            this.groupBox14.Controls.Add(this.label54);
            this.groupBox14.Controls.Add(this.apv_time_1krat);
            this.groupBox14.Controls.Add(this.label50);
            this.groupBox14.Controls.Add(this.label51);
            this.groupBox14.Controls.Add(this.apv_time_ready);
            this.groupBox14.Controls.Add(this.label49);
            this.groupBox14.Controls.Add(this.apv_time_blocking);
            this.groupBox14.Controls.Add(this.apv_self_off);
            this.groupBox14.Controls.Add(this.apv_blocking);
            this.groupBox14.Controls.Add(this.apv_conf);
            this.groupBox14.Location = new System.Drawing.Point(3, 1);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(256, 221);
            this.groupBox14.TabIndex = 22;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "������������ ���";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(8, 106);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(81, 13);
            this.label54.TabIndex = 30;
            this.label54.Text = "����� 1 �����";
            // 
            // apv_time_1krat
            // 
            this.apv_time_1krat.Location = new System.Drawing.Point(146, 101);
            this.apv_time_1krat.Name = "apv_time_1krat";
            this.apv_time_1krat.Size = new System.Drawing.Size(85, 20);
            this.apv_time_1krat.TabIndex = 20;
            this.apv_time_1krat.Tag = "3000000";
            this.apv_time_1krat.Text = "0";
            this.apv_time_1krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(8, 86);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(100, 13);
            this.label50.TabIndex = 29;
            this.label50.Text = "����� ����������";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(8, 66);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(103, 13);
            this.label51.TabIndex = 28;
            this.label51.Text = "����� ����������";
            // 
            // apv_time_ready
            // 
            this.apv_time_ready.Location = new System.Drawing.Point(146, 81);
            this.apv_time_ready.Name = "apv_time_ready";
            this.apv_time_ready.Size = new System.Drawing.Size(85, 20);
            this.apv_time_ready.TabIndex = 19;
            this.apv_time_ready.Tag = "3000000";
            this.apv_time_ready.Text = "0";
            this.apv_time_ready.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(8, 46);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(68, 13);
            this.label49.TabIndex = 27;
            this.label49.Text = "����������";
            // 
            // apv_time_blocking
            // 
            this.apv_time_blocking.Location = new System.Drawing.Point(146, 61);
            this.apv_time_blocking.Name = "apv_time_blocking";
            this.apv_time_blocking.Size = new System.Drawing.Size(85, 20);
            this.apv_time_blocking.TabIndex = 18;
            this.apv_time_blocking.Tag = "3000000";
            this.apv_time_blocking.Text = "0";
            this.apv_time_blocking.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // apv_self_off
            // 
            this.apv_self_off.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apv_self_off.FormattingEnabled = true;
            this.apv_self_off.Location = new System.Drawing.Point(146, 181);
            this.apv_self_off.Name = "apv_self_off";
            this.apv_self_off.Size = new System.Drawing.Size(87, 21);
            this.apv_self_off.TabIndex = 17;
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(733, 613);
            this.Controls.Add(this._saveConfigBut);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this._writeConfigBut);
            this.Controls.Add(this._statusStrip);
            this.Controls.Add(this._readConfigBut);
            this.Controls.Add(this._tabControl);
            this.MaximizeBox = false;
            this.Name = "ConfigurationForm";
            this.Text = "ConfigurationForm";
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid4)).EndInit();
            this._tokDefensesPage.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._frequenceDefensesGrid)).EndInit();
            this._frequencePage.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this._voltageDefensesPage.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid3)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this._inSignalsPage.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this._tabControl.ResumeLayout(false);
            this._outputSignalsPage.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).EndInit();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this._externalDefensePage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).EndInit();
            this._automaticPage.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3SpeedupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3SpeedupTimeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3FeatureCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3WorkTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3UROVCol;
        private System.Windows.Forms.DataGridView _tokDefenseGrid2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3APVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3AVRCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseBlockExistCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseParameterCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefensePuskConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseDirectionCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseWorkConstraintCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseSpeedUpCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseSpeedupTimeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseFeatureCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseWorkTimeCol;
        private System.Windows.Forms.DataGridView _tokDefenseGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseNameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseBlockNumberCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseU_PuskCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseUROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseAPVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseAVRCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3WorkConstraintCol;
        private System.Windows.Forms.MaskedTextBox _tokDefenseI2box;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.MaskedTextBox _tokDefenseI0box;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.MaskedTextBox _tokDefenseIbox;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4NameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense4ModeCol;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataGridView _tokDefenseGrid4;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense4BlockNumberCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4UpuskCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4PuskConstraintCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4WorkConstraintCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4WorkTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4SpeedupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4SpeedupTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4UROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4APVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4AVRCol;
        private System.Windows.Forms.TabPage _tokDefensesPage;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.RadioButton _tokDefenseMainConstraintRadio;
        private System.Windows.Forms.RadioButton _tokDefenseReserveConstraintRadio;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.MaskedTextBox _tokDefenseInbox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.DataGridView _tokDefenseGrid3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3NameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3ModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3BlockingNumberCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3UpuskCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3PuskConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3DirectionCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3BlockingExistCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3ParameterCol;
        private System.Windows.Forms.ComboBox apv_blocking;
        private System.Windows.Forms.ComboBox apv_conf;
        private System.Windows.Forms.DataGridView _frequenceDefensesGrid;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAVRcol3;
        private System.Windows.Forms.TabPage _frequencePage;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.RadioButton _frequenceDefenseMainConstraintRadio;
        private System.Windows.Forms.RadioButton _frequenceDefenseReserveConstraintRadio;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesTimeConstraintCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesReturnCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesParameterCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkConstraintCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVreturnCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesUROVcol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVcol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnConstraintCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnTimeCol3;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.MaskedTextBox _TT_Box;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.ToolStripStatusLabel _processLabel;
        private System.Windows.Forms.ToolStripProgressBar _exchangeProgressBar;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.StatusStrip _statusStrip;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkConstraintCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkTimeCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesParameterCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesReturnCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnTimeCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesUROVcol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVreturnCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnConstraintCol2;
        private System.Windows.Forms.TabPage _voltageDefensesPage;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.RadioButton _voltageDefenseMainConstraintRadio;
        private System.Windows.Forms.RadioButton _voltageDefenseReservConstraintRadio;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVcol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAVRcol2;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesParameterCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkConstraintCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkTimeCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesReturnCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVReturlCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnConstraintCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnTimeCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesUROVCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAVRCol1;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol3;
        private System.Windows.Forms.ComboBox _constraintGroupCombo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox _signalizationCombo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox _extOffCombo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox _extOnCombo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox _keyOffCombo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox _keyOnCombo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.MaskedTextBox _switcherImpulseBox;
        private System.Windows.Forms.ComboBox _switcherBlockCombo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox _switcherTimeBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox _switcherErrorCombo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox _switcherStateOnCombo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox _switcherStateOffCombo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox _TNNP_dispepairCombo;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox _TN_dispepairCombo;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MaskedTextBox _maxTok_Box;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox _TTNP_Box;
        private System.Windows.Forms.MaskedTextBox _TNNP_Box;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox _TN_Box;
        private System.Windows.Forms.ComboBox _TN_typeCombo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.MaskedTextBox _switcherTokBox;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.MaskedTextBox apv_time_4krat;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.MaskedTextBox apv_time_3krat;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox _manageSignalsSDTU_Combo;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox _manageSignalsExternalCombo;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox _manageSignalsKeyCombo;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.ComboBox _manageSignalsButtonCombo;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TabPage _inSignalsPage;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.MaskedTextBox _releDispepairBox;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.CheckedListBox _dispepairCheckList;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button _applyLogicSignalsBut;
        private System.Windows.Forms.ComboBox _logicChannelsCombo;
        private System.Windows.Forms.DataGridView _logicSignalsDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _diskretChannelCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _diskretValueCol;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.MaskedTextBox _switcherDurationBox;
        private System.Windows.Forms.MaskedTextBox apv_time_2krat;
        private System.Windows.Forms.TabControl _tabControl;
        private System.Windows.Forms.TabPage _outputSignalsPage;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.CheckedListBox _outputLogicCheckList;
        private System.Windows.Forms.Button _outputLogicAcceptBut;
        private System.Windows.Forms.ComboBox _outputLogicCombo;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView _outputIndicatorsGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignalCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndResetCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndAlarmCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndSystemCol;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView _outputReleGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releImpulseCol;
        private System.Windows.Forms.TabPage _externalDefensePage;
        private System.Windows.Forms.DataGridView _externalDefenseGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extDefNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefBlockingCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefWorkingCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefWorkingTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefReturnCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAPVreturnCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefReturnNumberCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefReturnTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefUROVcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAPVcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAVRcol;
        private System.Windows.Forms.TabPage _automaticPage;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.MaskedTextBox avr_disconnection;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.MaskedTextBox avr_time_return;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.MaskedTextBox avr_time_abrasion;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.ComboBox avr_return;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.ComboBox avr_abrasion;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.ComboBox avr_reset_blocking;
        private System.Windows.Forms.CheckBox avr_permit_reset_switch;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.CheckBox avr_abrasion_switch;
        private System.Windows.Forms.CheckBox avr_supply_off;
        private System.Windows.Forms.CheckBox avr_self_off;
        private System.Windows.Forms.CheckBox avr_switch_off;
        private System.Windows.Forms.ComboBox avr_blocking;
        private System.Windows.Forms.ComboBox avr_start;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.MaskedTextBox lzsh_constraint;
        private System.Windows.Forms.ComboBox lzsh_conf;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.MaskedTextBox apv_time_1krat;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.MaskedTextBox apv_time_ready;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.MaskedTextBox apv_time_blocking;
        private System.Windows.Forms.ComboBox apv_self_off;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense2NameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense2ModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense2BlockNumberCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense2PuskUCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense2PuskConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense2DirectionCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense2BlockExistCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense2ParameterCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense2WorkConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense2FeatureCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense2WorkTimeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense2EngineCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense2UROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense2APVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense2AVRCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesName;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesMode;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesBlockingNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesWorkConstraint;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesWorkTime;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesReturn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesAPVReturn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesReturnTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesConstraintAPV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesUROV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesAPV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesAVR;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.MaskedTextBox _HUD_Box;
        private System.Windows.Forms.ComboBox _OMP_TypeBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
    }
}