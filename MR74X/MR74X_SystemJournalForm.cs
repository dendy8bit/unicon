using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


using BEMN.Devices;
using BEMN.Interfaces;
using BEMN.MR74X;
using BEMN.Forms;

namespace BEMN.MR74X.Forms
{
    public partial class SystemJournalForm : Form, IFormView
    {
        private MR74X _device;

        public SystemJournalForm()
        {
            InitializeComponent();
        }

        public SystemJournalForm(MR74X device)
        {
            InitializeComponent();
            _device = device;

            _device.SystemJournalRecordLoadOk += new IndexHandler(_device_SystemJournalRecordLoadOk);
        }

        void _device_SystemJournalRecordLoadOk(object sender, int index)
        {
            try
            {
                Invoke(new OnDeviceIndexEventHandler(OnSysJournalIndexLoadOk), new object[] { index });
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnSysJournalIndexLoadOk(int i)
        {
            _journalProgress.Increment(1);
            _journalCntLabel.Text = (i + 1).ToString();
            _sysJournalGrid.Rows.Add(new object[] { i + 1, _device.SystemJournal[i].time, _device.SystemJournal[i].msg });
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR74X); }
        }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(SystemJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return (Image)BEMN.MR74X.Properties.Resources.js;
            }
        }

        public string NodeName
        {
            get { return "������ �������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
        
        private void _serializeSysJournalBut_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable("��700_������_�������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");
            for (int i = 0; i < _sysJournalGrid.Rows.Count; i++)
            {
                table.Rows.Add(new object[]{_sysJournalGrid["_indexCol", i].Value,
                                            _sysJournalGrid["_timeCol", i].Value,
                                            _sysJournalGrid["_msgCol", i].Value});
            }

            if (DialogResult.OK == _saveSysJournalDlg.ShowDialog())
            {
                table.WriteXml(_saveSysJournalDlg.FileName);
            }
        }

        private void _deserializeSysJournalBut_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable("TZL_������_�������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");

            if (DialogResult.OK == _openSysJounralDlg.ShowDialog())
            {
                _sysJournalGrid.Rows.Clear();
                table.ReadXml(_openSysJounralDlg.FileName);
            }

            for (int i = 0; i < table.Rows.Count; i++)
            {
                _sysJournalGrid.Rows.Add(new object[]{table.Rows[i].ItemArray[0],
                                                      table.Rows[i].ItemArray[1],
                                                      table.Rows[i].ItemArray[2]});
            }
        }

        private void SystemJournalForm_Activated(object sender, EventArgs e)
        {
            _device.SuspendSystemJournal(false);
        }

        private void SystemJournalForm_Deactivate(object sender, EventArgs e)
        {
            _device.SuspendSystemJournal(true);
        }

        private void SystemJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _device.RemoveSystemJournal();
        }

        private void _readSysJournalBut_Click(object sender, EventArgs e)
        {
            _device.RemoveSystemJournal();
            _journalProgress.Value = 0;
            _journalProgress.Maximum = MR74X.SYSTEMJOURNAL_RECORD_CNT;
            _sysJournalGrid.Rows.Clear();
            _device.LoadSystemJournal();
        }
    }
}