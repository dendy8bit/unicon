using System;
using System.Drawing;
using System.Reflection;
using BEMN.Interfaces;

namespace BEMN.MR74X.Forms
{
    public class OscilloscopeForm : BEMN.Forms.Oscilloscope.OscilloscopeForm, IFormView
    {
        private MR74X _device;
        public OscilloscopeForm()
        {

        }

        public OscilloscopeForm(MR74X device)
        {
            _device = device;
            Oscilloscope = _device.Oscilloscope;
        }



        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR74X); }
        }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(BEMN.Forms.Oscilloscope.OscilloscopeForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return
                  Image.FromStream(
                      Assembly.GetExecutingAssembly().GetManifestResourceStream("MR74X.Resources.oscilloscope.ico"));
            }
        }

        public string NodeName
        {
            get { return "�������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}