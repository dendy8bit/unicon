using System;
using System.Collections;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.MBServer;


namespace BEMN.MR74X
{
    public class Oscilloscope : BEMN.Utils.Oscilloscope
    {
        private MR74X _device;

        public Oscilloscope(Device device)
            : base(device)
        {
            _device = (MR74X)device;
        }

        public override void Initialize()
        {
            base.Initialize();
            _analogChannelsCnt = 4;
            _diskretChannelsCnt = 16;
            _exchangeCnt = 256;
            _oscilloscopeSize = 0x8000;
            _pointCount = 3276;
            _oscJournal = new Device.slot(0x3800, 0x3818);
            _journalLength = 0x18;

            AnalogChannels.Add("Ia");
            AnalogChannels.Add("Ib");
            AnalogChannels.Add("Ic");
            AnalogChannels.Add("Io");

            for (int i = 0; i < 16; i++)
            {
                DiskretChannels.Add("�" + (i + 1));
            }

        }

        public override void LoadOscilloscopeSettings()
        {
            _device.LoadInputSignals();
            base.LoadOscilloscopeSettings();
        }

        public override void CreateJournal()
        {
            Dictionary<string, string> ret = new Dictionary<string, string>(0xE);
            MR74X.CAlarmJournal helper = new MR74X.CAlarmJournal(_device);
            byte[] buffer = Common.TOBYTES(_journalBuffer, false);
            byte[] datetime = new byte[14];
            Array.ConstrainedCopy(buffer, 2, datetime, 0, 14);

            double Ia = Measuring.GetI(Common.TOWORD(buffer[23], buffer[22]), _device.TT, true);
            double Ib = Measuring.GetI(Common.TOWORD(buffer[25], buffer[24]), _device.TT, true);
            double Ic = Measuring.GetI(Common.TOWORD(buffer[27], buffer[26]), _device.TT, true);
            double Io = Measuring.GetI(Common.TOWORD(buffer[29], buffer[28]), _device.TTNP, false);
            double Ig = Measuring.GetI(Common.TOWORD(buffer[31], buffer[30]), _device.TTNP, false);
            double I0 = Measuring.GetI(Common.TOWORD(buffer[33], buffer[32]), _device.TT, true);
            double I1 = Measuring.GetI(Common.TOWORD(buffer[35], buffer[34]), _device.TT, true);
            double I2 = Measuring.GetI(Common.TOWORD(buffer[37], buffer[36]), _device.TT, true);

            ret.Add("���������", helper.GetMessage(buffer[0]));
            ret.Add("����-�����", helper.GetDateTime(datetime));
            ret.Add("���", helper.GetCode(buffer[16], buffer[18]));
            ret.Add("��������", helper.GetTypeValue(Common.TOWORD(buffer[13], buffer[12]), buffer[19]));


            ret.Add("Ia", String.Format("{0:F2}", Ia));
            ret.Add("Ib", String.Format("{0:F2}", Ib));
            ret.Add("Ic", String.Format("{0:F2}", Ic));
            ret.Add("Io", String.Format("{0:F2}", Io));
            ret.Add("Ig", String.Format("{0:F2}", Ig));
            ret.Add("I0", String.Format("{0:F2}", I0));
            ret.Add("I1", String.Format("{0:F2}", I1));
            ret.Add("I2", String.Format("{0:F2}", I2));

            byte[] b1 = new byte[] { buffer[34] };
            byte[] b2 = new byte[] { buffer[35] };
            ret.Add("���.1-8", Common.BitsToString(new BitArray(b1)));
            ret.Add("���.8-16", Common.BitsToString(new BitArray(b2)));
            ret.Add("TT", _device.TT.ToString());
            _journal = ret;

        }

        protected override void PrepareData(byte[] data)
        {
            if (_journal.ContainsKey("TT"))
            {
                _device.TT = ushort.Parse(_journal["TT"]);
            }
            int kTT = _device.TT;
            int kTTNP = _device.TTNP;
            double k = 0.15625;

            for (int i = 0; i < 16; i++)
            {
                _diskretData.Add(new BitArray(_pointCount));
            }
            for (int i = 0; i < _pointCount - 1; i++)
            {
                int channel = 0;
                //�������
                double value = BitConverter.ToUInt16(data, i * 10 + 0);
                _analogData[channel++][i] = ((value - 0x8000) / 0x8000) * 40 * kTT * 1.41;
                value = BitConverter.ToUInt16(data, i * 10 + 2);
                _analogData[channel++][i] = ((value - 0x8000) / 0x8000) * 40 * kTT * 1.41;
                value = BitConverter.ToUInt16(data, i * 10 + 4);
                _analogData[channel++][i] = ((value - 0x8000) / 0x8000) * 40 * kTT * 1.41;
                value = BitConverter.ToUInt16(data, i * 10 + 6);
                _analogData[channel][i] = ((value - 0x8000) / 0x8000) * 5 * kTTNP * 1.41;

                //��������
                double high = BitConverter.ToInt16(data, i * 10 + 8);
                for (int j = 0; j < 8; j++)
                {
                    _diskretData[j][i] = (0 == ((int)high >> j & 1)) ? false : true;
                }
                double low = BitConverter.ToInt16(data, i * 10 + 9);
                for (int j = 0; j < 8; j++)
                {
                    _diskretData[j + 8][i] = (0 == ((int)low >> j & 1)) ? false : true;
                }
            }
        }
    }
}

