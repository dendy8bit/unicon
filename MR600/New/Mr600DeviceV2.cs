﻿using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.MR600.New.Configuration.ConfigStruct;
using BEMN.MR600.New.Structs.JournalStructs;
using BEMN.MR600.New.Structs.MeasuringStructs;
using BEMN.MR600.Old.Osc.Structures;
using OldOscJournalStruct = BEMN.MR600.Old.Osc.Structures.OscJournalStruct;
using OscJournalStruct = BEMN.MR600.New.Structs.JournalStructs.OscJournalStruct;

namespace BEMN.MR600.New
{
    public class Mr600DeviceV2
    {
        #region [Fields]
        private MemoryEntity<RamMemoryStruct> _ramMemory;
        private MemoryEntity<DateTimeStruct> _dateTime;
        private MemoryEntity<DiscretBdStruct> _discretBd;
        private MemoryEntity<OneWordStruct> _changeGroup;
        private MemoryEntity<RomMeasuringStruct> _measuringChannel;
        private MemoryEntity<AnalogBdStruct> _analogBd;
        private MemoryEntity<SystemJournalStruct> _systemJournal;
        private MemoryEntity<AlarmJournalStruct> _alarmRecord;
        private MemoryEntity<OldOscJournalStruct> _oldOscJournal;
        private MemoryEntity<OscDataStruct> _oscData;
        /// <summary>
        /// Записи журнала
        /// </summary>
        private MemoryEntity<OscJournalStruct> _oscJournal;
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private  MemoryEntity<OneWordStruct> _refreshOscJournal;
        /// <summary>
        /// Установка начальной страницы осциллограммы
        /// </summary>
        private MemoryEntity<OneWordStruct> _setStartPage;
        /// <summary>
        /// Страница осциллограммы
        /// </summary>
        private MemoryEntity<OscPage> _oscPage;
        private MemoryEntity<RomMeasuringStruct> _voltageConfiguration;
        #endregion

        public Mr600DeviceV2(MR600 device)
        {
            InitMemEntity(device);
        }

        #region [Programming]
        private MemoryEntity<SourceProgramStruct> _sourceProgramStruct;
        private MemoryEntity<StartStruct> _programStartStruct;
        private MemoryEntity<ProgramStorageStruct> _programStorageStruct;
        private MemoryEntity<ProgramSignalsStruct> _programSignalsStruct;
        private MemoryEntity<OneWordStruct> _stopSpl;
        private MemoryEntity<OneWordStruct> _startSpl;
        private MemoryEntity<OneWordStruct> _stateSpl;

        public MemoryEntity<OneWordStruct> StopSpl
        {
            get { return _stopSpl; }
        }

        public MemoryEntity<OneWordStruct> StartSpl
        {
            get { return _startSpl; }
        }

        public MemoryEntity<OneWordStruct> StateSpl
        {
            get { return _stateSpl; }
        }

        public MemoryEntity<ProgramStorageStruct> ProgramStorageStruct
        {
            get { return _programStorageStruct; }
        }
        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return _programStartStruct; }
        }
        public MemoryEntity<ProgramSignalsStruct> ProgramSignalsStruct
        {
            get { return _programSignalsStruct; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return _sourceProgramStruct; }
        }
        #endregion

        #region [Properties]
        public MemoryEntity<SystemJournalStruct> SystemJournal
        {
            get { return this._systemJournal; }
        }

        public MemoryEntity<OneWordStruct> ChangeGroup
        {
            get { return this._changeGroup; }
        }
        public MemoryEntity<RamMemoryStruct> RamMemory
        {
            get { return this._ramMemory; }
        }

        public MemoryEntity<DateTimeStruct> DateTime
        {
            get { return this._dateTime; }
        }

        public MemoryEntity<DiscretBdStruct> DiscretBd
        {
            get { return _discretBd; }
        }

        public MemoryEntity<RomMeasuringStruct> MeasuringChannel
        {
            get { return _measuringChannel; }
        }

        public MemoryEntity<AnalogBdStruct> AnalogBd
        {
            get { return _analogBd; }
        }

        public MemoryEntity<AlarmJournalStruct> AlarmRecord
        {
            get { return _alarmRecord; }
        }

        public MemoryEntity<OldOscJournalStruct> OldOscJournal
        {
            get { return this._oldOscJournal; }
        }
        /// <summary>
        /// Страница осциллограммы
        /// </summary>
        public MemoryEntity<OscDataStruct> OscData
        {
            get { return this._oscData; }
        }
        /// <summary>
        /// Записи журнала
        /// </summary>
        public MemoryEntity<OscJournalStruct> OscJournal
        {
            get { return _oscJournal; }
        }
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        public MemoryEntity<OneWordStruct> RefreshOscJournal
        {
            get { return _refreshOscJournal; }
        }
        /// <summary>
        /// Установка начальной страницы осциллограммы
        /// </summary>
        public MemoryEntity<OneWordStruct> SetStartPage
        {
            get { return _setStartPage; }
        }
        /// <summary>
        /// Страница осциллограммы
        /// </summary>
        public MemoryEntity<OscPage> OscPage
        {
            get { return _oscPage; }
        }

        public MemoryEntity<RomMeasuringStruct> VoltageConfiguration
        {
            get { return _voltageConfiguration; }
        }
        #endregion [Properties]

        private void InitMemEntity(MR600 device)
        {
            this._ramMemory = new MemoryEntity<RamMemoryStruct>("Конфигурация", device, 0x1000);
            this._dateTime = new MemoryEntity<DateTimeStruct>("Дата и время", device, 0x200);
            this._discretBd = new MemoryEntity<DiscretBdStruct>("Дискретные сигналы", device, 0x1800);
            this._changeGroup = new MemoryEntity<OneWordStruct>("Переключение групп уставок", device, 0x400);
            this._measuringChannel = new MemoryEntity<RomMeasuringStruct>("Измерительный канал", device, 0x1000);
            this._analogBd = new MemoryEntity<AnalogBdStruct>("Аналоговые сигналы", device, 0x1900);
            this._systemJournal = new MemoryEntity<SystemJournalStruct>("ЖС", device, 0x2000);
            this._alarmRecord = new MemoryEntity<AlarmJournalStruct>("ЖА", device, 0x2800);
            this._oldOscJournal = new MemoryEntity<OldOscJournalStruct>("Чтение журнала осц", device, 0x3800);
            this._oscJournal = new MemoryEntity<OscJournalStruct>("Журнал Осц", device, 0x800);
            this._oscData = new MemoryEntity<OscDataStruct>("Чтение данных осц из устройства", device, 0x4000);
            this._refreshOscJournal = new MemoryEntity<OneWordStruct>("Сброс Журнала Осц", device, 0x800);
            this._setStartPage = new MemoryEntity<OneWordStruct>("Установка страницы осциллографа", device, 0x900);
            this._oscPage = new MemoryEntity<OscPage>("Чтение страницы осциллографа", device, 0x900);
            this._voltageConfiguration = new MemoryEntity<RomMeasuringStruct>("Параметры напряжений", device, 0x1000);
            this._sourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", device, 0xB000);
            this._programStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", device, 0x0001);
            this._programStorageStruct = new MemoryEntity<ProgramStorageStruct>("SaveLoadProgramStorage", device, 0xC000);
            this._programSignalsStruct = new MemoryEntity<ProgramSignalsStruct>("LoadProgramSignals_", device, 0xA000);
            this._stopSpl = new MemoryEntity<OneWordStruct>("StopSpl", device, 0x1808);
            this._startSpl = new MemoryEntity<OneWordStruct>("StartSpl", device, 0x1809);
            this._stateSpl = new MemoryEntity<OneWordStruct>("StateSpl", device, 0x1805);
        }
    }
}
