﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BEMN.MBServer;
using BEMN.MR600.New.Configuration.ConfigStruct;
using BEMN.MR600.New.Structs.JournalStructs;

namespace BEMN.MR600.New.HelpClasses.Osc
{
    /// <summary>
    /// Коллекция отсчётов осцилограммы
    /// </summary>
    public class CountingList
    {
        #region [Constants]
        /// <summary>
        /// Размер одного отсчёта(в словах)
        /// </summary>
        private const int COUNTING_SIZE = 5;

        private const int VOLTAGE_COUNT = 4;

        #endregion [Constants]

        #region [Private fields]
        /// <summary>
        /// Массив отсчётов разбитый на слова
        /// </summary>
        private readonly ushort[][] _countingArray;
        /// <summary>
        /// Массив дискрет 1-8
        /// </summary>
        private ushort[][] _discrets;

        /// <summary>
        /// Общее количество отсчётов
        /// </summary>
        private int _count;

        /// <summary>
        /// Токи
        /// </summary>
        private double[][] _voltage;
        /// <summary>
        /// Авария("Пуск осцилографа") в отсчётах
        /// </summary>
        private int _alarm;
        /// <summary>
        /// Минимальный ток
        /// </summary>
        private double _minU;
        /// <summary>
        /// Максимальный ток
        /// </summary>
        private double _maxU;

        private string[] _uNames = {"Ua", "Ub", "Uc", "Un"};

        private OscJournalStruct _oscJournalStruct;
        private readonly RomMeasuringStruct _romMeasuringStruct;

        private ushort[][] _baseVoltage;

        #endregion [Private fields]
        /// <summary>
        /// Дискреты
        /// </summary>
        public ushort[][] Discrets
        {
            get
            {
                return new[]
                    {
                        this._discrets[0],
                        this._discrets[1],
                        this._discrets[2],
                        this._discrets[3],
                        this._discrets[4],
                        this._discrets[5],
                        this._discrets[6],
                        this._discrets[7],


                    };
            }
            set
            {
                this._discrets[0] = value[0];
                this._discrets[1] = value[1];
                this._discrets[2] = value[2];
                this._discrets[3] = value[3];
                this._discrets[4] = value[4];
                this._discrets[5] = value[5];
                this._discrets[6] = value[6];
                this._discrets[7] = value[7];
            }
        }

        /// <summary>
        /// Перещитанные
        /// </summary>
        public double[][] Voltage
        {
            get { return this._voltage; }
            set { this._voltage = value; }
        }

        /// <summary>
        /// Общее количество отсчётов
        /// </summary>
        public int Count
        {
            get { return _count; }
        }

        /// <summary>
        /// Авария("Пуск осцилографа") в отсчётах
        /// </summary>
        public int Alarm
        {
            get { return _alarm; }
        }

        /// <summary>
        /// Минимальный ток
        /// </summary>
        public double MinU
        {
            get { return this._minU; }
        }

        /// <summary>
        /// Максимальный ток
        /// </summary>
        public double MaxU
        {
            get { return this._maxU; }
        }

        public bool IsLoad { get; private set; }
        public string FilePath { get; private set; }

        private string _dateTime;
        public string DateAndTime
        {
            get { return this._dateTime; }
        }

        private string _stage;
        public string Stage
        {
            get { return this._stage; }
        }
        #region [Ctor's]
        
        public CountingList(ushort[] pageValue, OscJournalStruct oscJournalStruct, RomMeasuringStruct romMeasuringStruct)
        {
            this._oscJournalStruct = oscJournalStruct;
            this._romMeasuringStruct = romMeasuringStruct;
            this._dateTime = oscJournalStruct.GetFormattedDateTime;
            this._alarm = this._oscJournalStruct.Len - this._oscJournalStruct.After;
            this._stage = oscJournalStruct.Stage;
            this._countingArray = new ushort[CountingList.COUNTING_SIZE][];
            //Общее количество отсчётов
            this._count = pageValue.Length / CountingList.COUNTING_SIZE;
            //Инициализация массива
            for (int i = 0; i < CountingList.COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this.Count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n == CountingList.COUNTING_SIZE)
                {
                    m++;
                    n = 0;
                }
            }
   
            this._discrets = this.DiscretArrayInit(this._countingArray[VOLTAGE_COUNT]);
            this._voltage = new double[VOLTAGE_COUNT][];
            this._baseVoltage = new ushort[VOLTAGE_COUNT][];
            
            for (int i = 0; i < VOLTAGE_COUNT; i++)
            {
                this._baseVoltage[i] = this._countingArray[i];

                double factor = i == 3 ? this._romMeasuringStruct.RomFactorUo : this._romMeasuringStruct.RomFactorU;

                this._voltage[i] = this._baseVoltage[i].Select(a => (a - 32768) / (double)32768 * factor * 256 * Math.Sqrt(2)).ToArray();
                this._maxU = Math.Max(this.MaxU, this._voltage[i].Max());
                this._minU = Math.Min(this.MinU, this._voltage[i].Min());
            }
        }

        #endregion [Ctor's]

        #region [Help members]
        /// <summary>
        /// Превращает каждое слово в инвертированный массив бит(значения 0/1) 
        /// </summary>
        /// <param name="sourseArray">Массив массивов бит</param>
        /// <returns></returns>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {

            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        }
        #endregion [Help members]

        public void Save(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine("МР 600 {0} {1} ступень - {2}", this._oscJournalStruct.GetDate, this._oscJournalStruct.GetTime, this._oscJournalStruct.Stage);
                hdrFile.WriteLine("Размер осциллограммы = {0}", this._oscJournalStruct.Len);
                hdrFile.WriteLine("Alarm = {0}", this._alarm);
                hdrFile.WriteLine("Stage = {0}", this._stage);
                hdrFile.WriteLine(1251);
            }
            
            string cgfPath = Path.ChangeExtension(filePath, "cfg");

            using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
            {
                cgfFile.WriteLine("MP600,1");
                cgfFile.WriteLine("12,4A,8D");
                int index = 1;
                for (int i = 0; i < this.Voltage.Length; i++)
                {
                    NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                    double factor = i == 3 ? this._romMeasuringStruct.RomFactorUo : this._romMeasuringStruct.RomFactorU;
                    double factorA = 1/(double) 32768 * factor * 256 * Math.Sqrt(2);
                    double factorB = (-1) * 32768/(double) 32768 * factor * 256 * Math.Sqrt(2);
                    cgfFile.WriteLine("{0},{1},,,V,{2},{3},0,-32768,32767,1,1,P", index, this._uNames[i], factorA.ToString(format), factorB.ToString(format));
                    index++;
                }

                for (int i = 0; i < this.Discrets.Length; i++)
                {
                    cgfFile.WriteLine("{0},D{1},0", index, i + 1);
                    index++;
                }
                
                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this._oscJournalStruct.Len);

                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);
                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeAlarm(this.Alarm));
                cgfFile.WriteLine("ASCII");
            }
            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < this._count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}", i, i * 1000);
                    foreach (ushort[] voltage in this._baseVoltage)
                    {
                        datFile.Write(",{0}", voltage[i]);
                    }
                    foreach (ushort[] discret in this.Discrets)
                    {
                        datFile.Write(",{0}", discret[i]);
                    }
                    datFile.WriteLine();
                }
            }
        }

        public static CountingList Load(string filePath)
        {

            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string timeString;
            if (hdrStrings[0].StartsWith("Время срабатывания : "))
            {
                timeString = hdrStrings[0].Replace("Время срабатывания :  ", string.Empty);
            }
            else
            {
                string[] buff = hdrStrings[0].Split(' ');
                timeString = string.Format("{0} {1}", buff[2], buff[3]);
            }
            int alarm = int.Parse(hdrStrings[2].Replace("Alarm = ", string.Empty));
            string stage = hdrStrings[3];
            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);
            double[,] factors = new double[4,2];
            //Regex factorRegex = new Regex(@"\d+\,U\w\,\,\,V\,(?<factorA>[0-9\.]+)\,(?<factorB>[0-9\.\-]+)");
            Regex factorRegex = new Regex(@"\d+\,[U]\w+\,\,\,[V]\,(?<valueA>[0-9\.-]+)\,(?<valueB>[0-9\.-]+)");
            for (int i = 2; i < 2 + 4; i++)
            {
                string fA = factorRegex.Match(cfgStrings[i]).Groups["valueA"].Value;
                string fB = factorRegex.Match(cfgStrings[i]).Groups["valueB"].Value;
                NumberFormatInfo format = new System.Globalization.NumberFormatInfo { NumberDecimalSeparator = "." };
                factors[i - 2, 0] = double.Parse(fA, format);
                factors[i - 2, 1] = double.Parse(fB, format);
            }

            int counts = int.Parse(cfgStrings[16].Replace("1000,", string.Empty));
            CountingList result = new CountingList(counts) {_alarm = alarm};

            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);

            const string dataPattern = @"^\d+\,\d+,(?<I1>\-?\d+),(?<I2>\-?\d+),(?<I3>\-?\d+),(?<I4>\-?\d+),(?<D1>\d+),(?<D2>\d+),(?<D3>\d+),(?<D4>\d+),(?<D5>\d+),(?<D6>\d+),(?<D7>\d+),(?<D8>\d+)";
            Regex dataRegex = new Regex(dataPattern);
            double[][] voltage = new double[4][];
            ushort[][] discrets = new ushort[8][];
            for (int i = 0; i < voltage.Length; i++)
            {
                voltage[i] = new double[counts];
            }

            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[counts];
            }
            for (int i = 0; i < datStrings.Length; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    voltage[j][i] = double.Parse(dataRegex.Match(datStrings[i]).Groups["I" + (j + 1)].Value) * factors[j, 0] + factors[j, 1];
                }

                for (int j = 0; j < 8; j++)
                {
                    discrets[j][i] = ushort.Parse(dataRegex.Match(datStrings[i]).Groups["D" + (j + 1)].Value);
                }
            }
            for (int i = 0; i < 4; i++)
            {
                result._maxU = Math.Max(result.MaxU, voltage[i].Max());
                result._minU = Math.Min(result.MinU, voltage[i].Min());
            }
            result.Voltage = voltage;
            result.Discrets = discrets;
            result.FilePath = filePath;
            result.IsLoad = true;
            result._dateTime = timeString;
            result._stage = stage;
            return result;
        }
        private CountingList(int count)
        {
            this._discrets = new ushort[8][];
            this._voltage = new double[4][];
            this._baseVoltage = new ushort[4][];
            this._count = count;
        }

    }
}
