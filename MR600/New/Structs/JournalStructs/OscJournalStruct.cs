﻿using System;
using System.Globalization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR600.New.HelpClasses;

namespace BEMN.MR600.New.Structs.JournalStructs
{
    public class OscJournalStruct : StructBase
    {
        #region [Constants]
        private const string DATE_PATTERN = "{0:d2}.{1:d2}.{2:d2}";
        private const string TIME_PATTERN = "{0:d2}:{1:d2}:{2:d2},{3:d2}";
        private const string NUMBER_PATTERN = "{0}";

        /// <summary>
        /// Размер одной страницы 
        /// </summary>
        private const int OSCLEN = 1024;

        #endregion [Constants]

        public static int RecordIndex;

        #region [Private fields]
        [Layout(0)] private ushort _reserv;
        [Layout(1)] private ushort _year;
        [Layout(2)] private ushort _month;
        [Layout(3)] private ushort _date;
        [Layout(4)] private ushort _hour;
        [Layout(5)] private ushort _minute;
        [Layout(6)] private ushort _second;
        [Layout(7)] private ushort _millisecond;
        /// <summary>
        /// "READY" если 0 - осциллограмма готова 
        /// </summary>
        [Layout(8)] private int _ready;
        /// <summary>
        /// "POINT" Адрес начала блока текущей осциллограммы в массиве данных (в словах)
        /// </summary>
        [Layout(9)] private int _point;
        /// <summary>
        /// "BEGIN" Адрес аварии в массиве данных (в словах)
        /// </summary>
        [Layout(10)] private int _begin;
        /// <summary>
        /// "LEN" Размер осциллограммы (в отсчётах)
        /// </summary>
        [Layout(11)] private int _len;
        /// <summary>
        /// "AFTER" Размер после аварии (в отсчётах)
        /// </summary>
        [Layout(12)] private int _after;
        /// <summary>
        /// "ALM" Номер (последней) сработавшей защиты 
        /// </summary>
        [Layout(13)] private ushort _numberDefence;
        /// <summary>
        /// "REZ" Размер одного отсчёта (в словах)
        /// </summary>
        [Layout(14)] private ushort _sizeReference;

        #endregion [Private fields]


        //Размер хранилища данных в массиве данных осциллограмм (в словах)– OSCSIZE =1032192   

        #region [Properties]
        /// <summary>
        /// Сработанная ступень
        /// </summary>
        public string Stage
        {
            get
            {
                return StringsV2.AlarmJournaleCode.ContainsKey(this._numberDefence)
                    ? StringsV2.AlarmJournaleCode[this._numberDefence]
                    : string.Format("Числовое значение - {0}", this._numberDefence);
                   
            }
        }

        /// <summary>
        /// Запись журнала осциллографа
        /// </summary>
        public object[] GetRecord
        {
            get
            {
                return new object[]
                    {
                        this.GetNumber, //Номер
                        this.GetDate,
                        this.GetTime, //Время
                        this.Stage,
                        this._ready, //Готовность
                        this.Point, //Адрес начала
                        this.GetEnd, //Адрес конца
                        this.Begin, //Смещение
                        this.Len, //Размер
                        this.After, //№ сработавшей защиты
                        
                        this.SizeReference
                    };
            }
        }

        public bool IsEmpty
        {
            get
            {
                //пустая запись
                return this.SizeReference == 0 |
                    //осциллограмма не готова, тоже признак конца журнала
                       this._ready != 0;
            }
        }
        /// <summary>
        /// Начальная страница осциллограммы
        /// </summary>
        public ushort OscStartIndex
        {
            get { return (ushort)(this.Point / OscJournalStruct.OSCLEN); }
        }
        /// <summary>
        /// Номер соощения
        /// </summary>
        private string GetNumber
        {
            get
            {
                var result = string.Format(NUMBER_PATTERN, RecordIndex + 1);
                if (RecordIndex == 0)
                {
                    result += "(посл.)";
                }
                return result;
            }
        }

        /// <summary>
        /// Время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                return string.Format
                    (
                        TIME_PATTERN,
                        this._hour,
                        this._minute,
                        this._second,
                        this._millisecond
                    );
            }
        }

        /// <summary>
        /// Дата сообщения
        /// </summary>
        public string GetDate
        {
            get
            {
                return string.Format
                    (
                        DATE_PATTERN,
                        this._date,
                        this._month,
                        this._year
                    );
            }
        }

        public string GetFormattedDateTimeAlarm(int alarm)
        {
            try
            {
                var a = new DateTime(this._year, this._month, this._date, this._hour, this._minute, this._second, this._millisecond < 100 ? this._millisecond * 10 : this._millisecond);
                var result = a.AddMilliseconds(alarm);
                return string.Format("{0:D2}/{1:D2}/{2:D2},{3:D2}:{4:D2}:{5:D2}.{6:D3}",
                    result.Month,
                    result.Day,
                    result.Year,
                    result.Hour,
                    result.Minute,
                    result.Second,
                    result.Millisecond);
            }
            catch (Exception)
            {
                return this.GetFormattedDateTime;
            }
        }

        public string GetFormattedDateTime
        {
            get
            {
                return string.Format("{0:D2}/{1:D2}/{2:D2},{3:D2}:{4:D2}:{5:D2}.{6:D3}",
                                     this._month,
                                     this._date,
                                     this._year,
                                     this._hour,
                                     this._minute,
                                     this._second,
                    this._millisecond < 100 ? this._millisecond * 10 : this._millisecond);
            }
        }

        private static int OscSize = 0x77FF4;

        /// <summary>
        /// Адрес конца
        /// </summary>
        private string GetEnd
        {
            get
            {
                int num = OscSize;
                int num2 = this.Point + (this.Len * this.SizeReference);
                return num2 > num ? string.Format("{0} [{1}]", num2, num2 - num) : num2.ToString(CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// "LEN" Размер осциллограммы (в отсчётах)
        /// </summary>
        public int Len
        {
            get { return _len; }
        }

        /// <summary>
        /// "AFTER" Размер после аварии (в отсчётах)
        /// </summary>
        public int After
        {
            get { return _after; }
        }

        /// <summary>
        /// "BEGIN" Адрес аварии в массиве данных (в словах)
        /// </summary>
        public int Begin
        {
            get { return _begin; }
        }

        /// <summary>
        /// "POINT" Адрес начала блока текущей осциллограммы в массиве данных (в словах)
        /// </summary>
        public int Point
        {
            get { return _point; }
        }

        /// <summary>
        /// "REZ" Размер одного отсчёта (в словах)
        /// </summary>
        public ushort SizeReference
        {
            get { return _sizeReference; }
        }

        #endregion [Private Properties]
    }
}
