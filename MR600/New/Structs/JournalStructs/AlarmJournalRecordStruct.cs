﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;
using BEMN.MR600.New.HelpClasses;

namespace BEMN.MR600.New.Structs.JournalStructs
{
    public class AlarmJournalRecordStruct : StructBase
    {
        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2}.{6:d2}";

        #endregion [Constants]

        #region [Private fields]

        [Layout(0)] private ushort _message; //1
        [Layout(1)] private ushort _year; //2
        [Layout(2)] private ushort _month; //3
        [Layout(3)] private ushort _date; //4
        [Layout(4)] private ushort _hour; //5
        [Layout(5)] private ushort _minute; //6
        [Layout(6)] private ushort _second; //7
        [Layout(7)] private ushort _millisecond; //8
        [Layout(8)] private ushort _code; // 2 Код повреждения** 9
        [Layout(9)] private ushort _type; //3 Тип повреждения*** 10
        [Layout(10)] private ushort _value; //4 Значение повреждения //Знач. сраб пар 11
        [Layout(11)] private ushort _f; //5 Значение  //F 12
        [Layout(12)] private ushort _ua; //6 Значение   //Ua 13 
        [Layout(13)] private ushort _ub; //7 Значение   //Ub 14 
        [Layout(14)] private ushort _uc; //8 Значение //Uc 15
        [Layout(15)] private ushort _uab; //9 Значение //Uab 16
        [Layout(16)] private ushort _ubc; //10 Значение  //Ubc 17 
        [Layout(17)] private ushort _uca; //11 Значение //Uca 18
        [Layout(18)] private ushort _u0; //12 Значение //U0 19
        [Layout(19)] private ushort _u1; //13 Значение //U1 20
        [Layout(20)] private ushort _u2; //14 Значение //U2 21
        [Layout(21)] private ushort _un; //15 Значение //Un 22
        [Layout(22)] private ushort reserv1; //17 рез 24
        [Layout(23)] private ushort _discret; //16 Значение //Дискрет 23
        [Layout(24)] private ushort reserv2; //18 рез 25
        [Layout(25)] private ushort reserv3; //19 рез 26
        [Layout(26)] private ushort reserv4; //20 рез 27
        [Layout(27)] private ushort reserv5; //21 рез 28 

        #endregion [Private fields]

        #region [Properties]
        public bool IsEmpty
        {
            get
            {
                return (this._date + this._month + this._year + this._hour + this._minute + this._second +
                        this._millisecond + this._code + this._type + this._value) == 0;
            }
        }

        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                return string.Format
                    (
                        DATE_TIME_PATTERN,
                        this._date,
                        this._month,
                        this._year,
                        this._hour,
                        this._minute,
                        this._second,
                        this._millisecond
                    );
            }
        }
        public string Message
        {
            get
            {
                return StringsV2.AlarmJournaleMessages.ContainsKey(_message)
                    ? StringsV2.AlarmJournaleMessages[_message]
                    : _message.ToString();
            }
        }
        public string Code
        {
            get
            {
                if (_code < 128)
                {
                    return StringsV2.AlarmJournaleCode.ContainsKey(_code)
                        ? string.Format("{0} ОСН",StringsV2.AlarmJournaleCode[_code])
                        : _code.ToString();
                }
                else
                {
                    return StringsV2.AlarmJournaleCode.ContainsKey(_code-128)
                        ? string.Format("{0} РЕЗ", StringsV2.AlarmJournaleCode[_code-128])
                        : _code.ToString();
                }
            }
        }

        public string Type
        {
            get
            {
                int t = _type >> 8;
                try
                {
                    StringsV2.AlarmJournaleType.Contains(StringsV2.AlarmJournaleType[t]);//проверка на то, есть ли такой индекс в списке
                    {                                                                    //если нет, то вылетит исключение,которое отработается
                        if (t == 0)
                        {
                            return string.Format("{0}", StringsV2.AlarmJournaleType[t]);
                        }
                        else
                        {
                            return string.Format("{0} = ", StringsV2.AlarmJournaleType[t]);
                        }
                    }
                }
                catch (Exception)
                {
                    return t.ToString();
                }
                return null;
            }
        }

        public ushort Value
        {
            get { return _value; }
        }

        public ushort F
        {
            get { return _f; }
        }

        public ushort Ua
        {
            get { return _ua; }
        }

        public ushort Ub
        {
            get { return _ub; }
        }

        public ushort Uc
        {
            get { return _uc; }
        }

        public ushort Uab
        {
            get { return _uab; }
        }

        public ushort Ubc
        {
            get { return _ubc; }
        }

        public ushort Uca
        {
            get { return _uca; }
        }

        public ushort U0
        {
            get { return _u0; }
        }

        public ushort U1
        {
            get { return _u1; }
        }

        public ushort U2
        {
            get { return _u2; }
        }

        public ushort Un
        {
            get { return _un; }
        }

        public string Discret
        {
            get { return   GetMask(Common.LOBYTE(this._discret)); }
        }

        /// <summary>
        /// Ивертирует двоичное представление числа
        /// </summary>
        /// <param name="value">Число</param>
        /// <returns>Инвертированое двоичное представление</returns>
        private string GetMask(ushort value)
        {
            var chars = Convert.ToString(value, 2).PadLeft(8, '0').ToCharArray();
            Array.Reverse(chars);
            return new string(chars);
        }

        #endregion [Properties]
    }
}
