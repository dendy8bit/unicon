﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.MR600.New.Structs.ConfigurationStructs.Primary;

namespace BEMN.MR600.New.Structs.ConfigurationStructs
{
    /// <summary>
    /// Вся конфигурация
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct RamMemoryStruct : IStruct, IStructInit
    {
        public RomMeasuringStruct RomMeasuring;
        public RomExternalSignalsStruct RomExternalSignals;
        public InputLogicParametresStruct InputLogicParametres;
        public OutputAddedRelaysStruct OutputAddedRelays;
        public AllExternalDefensesConfigurationStruct AllExternalDefensesConfiguration;
        public AllVoltageDefenseConfigurationStruct AllVoltageDefenseConfiguration;
        public AllFrequencyDefenseConfigurationStruct AllFrequencyDefenseConfiguration;
        public AllOutputLogicSignalStruct AllOutputLogicSignal;
        public OutputRelaysStruct OutputRelays;
        public AllIndicatorsStruct AllIndicators;
        public SystemConfigurationStruct SystemConfiguration;


        #region [Ctor's]
        public RamMemoryStruct(bool a)
        {
            this.RomMeasuring = new RomMeasuringStruct();
            this.RomExternalSignals = new RomExternalSignalsStruct();
            this.InputLogicParametres = new InputLogicParametresStruct(a);
            this.OutputAddedRelays = new OutputAddedRelaysStruct(a);
            this.AllExternalDefensesConfiguration = new AllExternalDefensesConfigurationStruct(a);
            this.AllVoltageDefenseConfiguration = new AllVoltageDefenseConfigurationStruct(a);
            this.AllFrequencyDefenseConfiguration = new AllFrequencyDefenseConfigurationStruct(a);
            this.AllOutputLogicSignal = new AllOutputLogicSignalStruct(a);
            this.OutputRelays = new OutputRelaysStruct(a);
            this.AllIndicators = new AllIndicatorsStruct(a);
            this.SystemConfiguration = new SystemConfigurationStruct();
        } 
        #endregion [Ctor's]

        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]


        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            int index = 0;


            this.RomMeasuring = new RomMeasuringStruct();
            var oneStruct = new byte[Marshal.SizeOf(typeof(RomMeasuringStruct))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.RomMeasuring.InitStruct(oneStruct);
            index += Marshal.SizeOf(typeof(RomMeasuringStruct));


            this.RomExternalSignals = new RomExternalSignalsStruct();
            oneStruct = new byte[Marshal.SizeOf(typeof(RomExternalSignalsStruct))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.RomExternalSignals.InitStruct(oneStruct);
            index += Marshal.SizeOf(typeof(RomExternalSignalsStruct));

            this.InputLogicParametres = new InputLogicParametresStruct();
            oneStruct = new byte[Marshal.SizeOf(typeof(InputLogicParametresStruct))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.InputLogicParametres.InitStruct(oneStruct);
            index += Marshal.SizeOf(typeof(InputLogicParametresStruct));

            this.OutputAddedRelays = new OutputAddedRelaysStruct();
            oneStruct = new byte[Marshal.SizeOf(typeof(OutputAddedRelaysStruct))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.OutputAddedRelays.InitStruct(oneStruct);
            index += Marshal.SizeOf(typeof(OutputAddedRelaysStruct));

            this.AllExternalDefensesConfiguration = new AllExternalDefensesConfigurationStruct();
            oneStruct = new byte[Marshal.SizeOf(typeof(AllExternalDefensesConfigurationStruct))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.AllExternalDefensesConfiguration.InitStruct(oneStruct);
            index += Marshal.SizeOf(typeof(AllExternalDefensesConfigurationStruct));

            this.AllVoltageDefenseConfiguration = new AllVoltageDefenseConfigurationStruct();
            oneStruct = new byte[Marshal.SizeOf(typeof(AllVoltageDefenseConfigurationStruct))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.AllVoltageDefenseConfiguration.InitStruct(oneStruct);
            index += Marshal.SizeOf(typeof(AllVoltageDefenseConfigurationStruct));


            this.AllFrequencyDefenseConfiguration = new AllFrequencyDefenseConfigurationStruct();
            oneStruct = new byte[Marshal.SizeOf(typeof(AllFrequencyDefenseConfigurationStruct))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.AllFrequencyDefenseConfiguration.InitStruct(oneStruct);
            index += Marshal.SizeOf(typeof(AllFrequencyDefenseConfigurationStruct));


            this.AllOutputLogicSignal = new AllOutputLogicSignalStruct();
            oneStruct = new byte[Marshal.SizeOf(typeof(AllOutputLogicSignalStruct))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.AllOutputLogicSignal.InitStruct(oneStruct);
            index += Marshal.SizeOf(typeof(AllOutputLogicSignalStruct));

            this.OutputRelays = new OutputRelaysStruct();
            oneStruct = new byte[Marshal.SizeOf(typeof(OutputRelaysStruct))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.OutputRelays.InitStruct(oneStruct);
            index += Marshal.SizeOf(typeof(OutputRelaysStruct));

            this.AllIndicators = new AllIndicatorsStruct();
            oneStruct = new byte[Marshal.SizeOf(typeof(AllIndicatorsStruct))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.AllIndicators.InitStruct(oneStruct);
            index += Marshal.SizeOf(typeof(AllIndicatorsStruct));

            this.SystemConfiguration = new SystemConfigurationStruct();
            oneStruct = new byte[Marshal.SizeOf(typeof(SystemConfigurationStruct))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.SystemConfiguration.InitStruct(oneStruct);
           
        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>();

            result.AddRange(this.RomMeasuring.GetValues());                     //1
            result.AddRange(this.RomExternalSignals.GetValues());               //2
            result.AddRange(this.InputLogicParametres.GetValues());             //3
            result.AddRange(this.OutputAddedRelays.GetValues());                //4
            result.AddRange(this.AllExternalDefensesConfiguration.GetValues()); //5
            result.AddRange(this.AllVoltageDefenseConfiguration.GetValues());   //6
            result.AddRange(this.AllFrequencyDefenseConfiguration.GetValues()); //7
            result.AddRange(this.AllOutputLogicSignal.GetValues());             //8
            result.AddRange(this.OutputRelays.GetValues());                     //9
            result.AddRange(this.AllIndicators.GetValues());                    //10
            result.AddRange(this.SystemConfiguration.GetValues());              //11
            return result.ToArray();
        }

        #endregion [IStructInit Members]
    }
}
