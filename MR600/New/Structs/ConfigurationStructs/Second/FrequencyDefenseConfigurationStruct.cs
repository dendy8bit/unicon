﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Second
{
    /// <summary>
    /// конфигурации защит частоты
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct FrequencyDefenseConfigurationStruct : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public FrequencyDefenseStruct[] FrequencyDefenses;

        public FrequencyDefenseConfigurationStruct(bool a)
        {
            this.FrequencyDefenses = new FrequencyDefenseStruct[8];
        }

        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]


        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.FrequencyDefenses = new FrequencyDefenseStruct[8];
            for (int i = 0; i < this.FrequencyDefenses.Length; i++)
            {
                var oneStruct = new byte[Marshal.SizeOf(typeof(FrequencyDefenseStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.FrequencyDefenses[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(FrequencyDefenseStruct));
            }

        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>();
            foreach (var defense in FrequencyDefenses)
            {
                result.AddRange(defense.GetValues());
            }
            return result.ToArray();
        }

        #endregion [IStructInit Members]
    }
}