﻿using System;
using BEMN.Devices;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;
using BEMN.MR600.New.HelpClasses;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Second
{
    /// <summary>
    /// Выходное реле
    /// </summary>
    public struct RelayStruct : IStruct, IStructInit
    {
        #region [Private fields]
        private ushort _type;
        private ushort _waitTime; 
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// Тип реле
        /// </summary>
        public string RelayType
        {
            get
            {
                var type = Common.GetBit(this._type, 15) ? 1 : 0;
                return StringsV2.RelayType[type];
            }
            set
            {
                var index = StringsV2.RelayType.IndexOf(value);
                this._type = Common.SetBit(this._type, 15, index == 1);
            }
        }
        /// <summary>
        /// Сигнал реле
        /// </summary>
        public string Signal
        {
            get
            {
                var index = Common.GetBits(this._type, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
                return StringsV2.OutputSignals[index];
            }
            set
            {
                var index = StringsV2.OutputSignals.IndexOf(value);
                this._type = Common.SetBits(this._type, (ushort)index, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            }
        }
        /// <summary>
        /// Импульс
        /// </summary>
        public int WaitTime
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._waitTime);
            }
            set
            {
                this._waitTime = ValuesConverterCommon.SetWaitTime(value);
            }
        } 
        #endregion [Properties]


        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]


        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            int index = 0;

            this._type = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._waitTime = Common.TOWORD(array[index + 1], array[index]);
       
        }

        public ushort[] GetValues()
        {
            return new[]
                {
                    this._type,
                    this._waitTime
                };


        }

        #endregion [IStructInit Members]
    }
}