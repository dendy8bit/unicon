﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Second
{
    /// <summary>
    /// конфигурации внешних защит 96 байт
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct ExternalDefensesConfigurationStruct : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public ExternalDefenseStruct[] ExternalDefenses;

        public ExternalDefensesConfigurationStruct(bool a)
        {
            this.ExternalDefenses = new ExternalDefenseStruct[8];
        }

        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]


        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.ExternalDefenses = new ExternalDefenseStruct[8];
            for (int i = 0; i < this.ExternalDefenses.Length; i++)
            {
                var oneStruct = new byte[Marshal.SizeOf(typeof(ExternalDefenseStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.ExternalDefenses[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(ExternalDefenseStruct));
            }

        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>();
            foreach (var defense in ExternalDefenses)
            {
                result.AddRange(defense.GetValues());
            }
            return result.ToArray();
        }

        #endregion [IStructInit Members]
    }
}