﻿using System;
using BEMN.Devices;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;
using BEMN.MR600.New.HelpClasses;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Second
{
    /// <summary>
    /// внешние защиты 12 байт
    /// </summary>
    public struct ExternalDefenseStruct : IStruct, IStructInit
    {
        #region [Private fields]
        private ushort _romExtConfig;       //конфигурация                                   2 байта
        private ushort _romExtBlock;        //номер входа блокировки                         2 байта
        private ushort _romExtInput;        //номер входа срабатывания                       2 байта
        private ushort _romExtWait;         //выдержка времени срабатывания                  2 байта
        private ushort _romExtInputAdd;     //номер входа возврата                           2 байта
        private ushort _romExtWaitAdd;      //выдержка времени возврвта                      2 байта 
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// Режим
        /// </summary>
        public string Mode
        {
            get
            {
                var index = Common.GetBits(this._romExtConfig, 0, 1);
                return StringsV2.VoltageDefenseMode[index];
            }
            set
            {
                var index = (ushort)StringsV2.VoltageDefenseMode.IndexOf(value);
               this._romExtConfig = Common.SetBits(this._romExtConfig, index, 0, 1);
            }
        }
        /// <summary>
        /// Блокировка
        /// </summary>
        public string RomExtBlock
        {
            get { return  StringsV2.ExternalDefensesSignals[this._romExtBlock] ; }
            set { this._romExtBlock =(ushort) StringsV2.ExternalDefensesSignals.IndexOf(value) ; }
        }
        /// <summary>
        /// Срабатывание
        /// </summary>
        public string RomExtInput
        {
            get { return StringsV2.ExternalDefensesSignals[this._romExtInput]; }
            set { _romExtInput = (ushort)StringsV2.ExternalDefensesSignals.IndexOf(value); }
        }
        /// <summary>
        /// Время срабатывания
        /// </summary>
        public int RomExtWait
        {
            get { return ValuesConverterCommon.GetWaitTime(this._romExtWait); }
            set { this._romExtWait = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// Возврат
        /// </summary>
        public bool Recovery
        {
            get { return Common.GetBit(this._romExtConfig, 14); }
            set { this._romExtConfig = Common.SetBit(this._romExtConfig, 14, value); }
        }
        /// <summary>
        /// Вход возврата
        /// </summary>
        public string RomExtInputAdd
        {
            get { return StringsV2.ExternalDefensesSignals[this._romExtInputAdd]; }
            set { _romExtInputAdd = (ushort)StringsV2.ExternalDefensesSignals.IndexOf(value); }
        }
        /// <summary>
        /// Время возврата
        /// </summary>
        public int RomExtWaitAdd
        {
            get { return ValuesConverterCommon.GetWaitTime(_romExtWaitAdd); }
            set { this._romExtWaitAdd = ValuesConverterCommon.SetWaitTime(value); }
        } 
        /// <summary>
        /// Осциллограф
        /// </summary>
        public bool Osc
        {
            get { return Common.GetBit(this._romExtConfig, 4); }
            set { this._romExtConfig = Common.SetBit(this._romExtConfig, 4, value); }
        }

        /// <summary>
        /// Сброс
        /// </summary>
        public bool Reset
        {
            get { return Common.GetBit(this._romExtConfig, 13); }
            set { this._romExtConfig = Common.SetBit(this._romExtConfig, 13, value); }
        }
        #endregion [Properties]


        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]


        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            int index = 0;

            this._romExtConfig = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romExtBlock = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romExtInput = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romExtWait = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romExtInputAdd = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romExtWaitAdd = Common.TOWORD(array[index + 1], array[index]);


        }

        public ushort[] GetValues()
        {
            return new[]
                {
                    this._romExtConfig,
                    this._romExtBlock,
                    this._romExtInput,
                    this._romExtWait,
                    this._romExtInputAdd,
                    this._romExtWaitAdd
                };


        }

        #endregion [IStructInit Members]
    }
}