﻿using System;
using BEMN.Devices;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;
using BEMN.MR600.New.HelpClasses;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Second
{
    /// <summary>
    /// параметры сигнала НЕИСПРАВНОСТЬ
    /// </summary>
    public struct FaultSygnalStruct : IStruct, IStructInit
    {
        #region [Private fields]

        private ushort _config;
        private ushort _wait;

        #endregion [Private fields]

        public bool[] Config
        {
            get
            {
                var result = new bool[8];
                for (int i = 0; i < 8; i++)
                {
                    result[i] = Common.GetBit(this._config, i);
                }
                return result;
            }

            set
            {
                for (int i = 0; i < 8; i++)
                {
                    this._config = Common.SetBit(this._config, i, value[i]);
                }
            }
        }

        /// <summary>
        /// Импульс неисправности
        /// </summary>
        public int WaitTime
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._wait);
            }
            set
            {
                this._wait = ValuesConverterCommon.SetWaitTime(value);
            }
        } 


        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]


        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            int index = 0;

            this._config = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof (UInt16);

            this._wait = Common.TOWORD(array[index + 1], array[index]);

        }

        public ushort[] GetValues()
        {
            return new[]
                {
                    this._config,
                    this._wait
                };
        }
        #endregion [IStructInit Members]
    }
}