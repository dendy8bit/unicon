﻿using System;
using BEMN.Devices;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;
using BEMN.MR600.New.HelpClasses;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Second
{
    /// <summary>
    /// защиты напряжения  16 байт
    /// </summary>
    public struct VoltageDefenseStruct : IStruct, IStructInit
    {
        #region [Private fields]
        private ushort _romUConfig; //конфигурация                                  2 байта
        private ushort _romUBlock; //номер входа блокировки                       2 байта
        private ushort _romUValue; //уставка срабатывания                         2 байта
        private ushort _romUWait; //выдержка времени срабатывания                 2 байта
        private ushort _romUValueAdd; //уставка возврата                          2 байта
        private ushort _romUWaitAdd; //выдержка времени возврата                  2 байта 
        private ushort _reserv1;
        private ushort _reserv2;
        #endregion [Private fields]

        #region [Properties]
        /// <summary>
        /// Режим
        /// </summary>
        public string Mode
        {
            get
            {
                var index = Common.GetBits(this._romUConfig, 0, 1);
                return StringsV2.VoltageDefenseMode[index];
            }
            set
            {
                var index = (ushort)StringsV2.VoltageDefenseMode.IndexOf(value);
                this._romUConfig = Common.SetBits(this._romUConfig, index, 0, 1);
            }
        }
        /// <summary>
        /// Блокировка
        /// </summary>
        public string RomExtBlock
        {
            get { return StringsV2.InputSignals[this._romUBlock]; }
            set { this._romUBlock = (ushort)StringsV2.InputSignals.IndexOf(value); }
        }
        /// <summary>
        /// Уставка срабатывания
        /// </summary>
        public double RomExtInput
        {
            get { return Math.Round(this._romUValue/(double) 256, 2); }
            set { _romUValue = (ushort)Math.Round(value*256) ; }
        }
        /// <summary>
        /// Время срабатывания
        /// </summary>
        public int RomExtWait
        {
            get { return ValuesConverterCommon.GetWaitTime(this._romUWait); }
            set { this._romUWait = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// Возврат
        /// </summary>
        public bool Recovery
        {
            get { return Common.GetBit(this._romUConfig, 14); }
            set { this._romUConfig = Common.SetBit(this._romUConfig, 14, value); }
        }
        /// <summary>
        /// Уставка возврата
        /// </summary>
        public double RomExtInputAdd
        {
            get { return Math.Round(this._romUValueAdd / (double)256, 2);}
            set { _romUValueAdd = (ushort)Math.Round(value * 256); }
        }
        /// <summary>
        /// Время возврата
        /// </summary>
        public int RomExtWaitAdd
        {
            get { return ValuesConverterCommon.GetWaitTime(_romUWaitAdd); }
            set { this._romUWaitAdd = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// Осциллограф
        /// </summary>
        public string Osc
        {
            get { return StringsV2.VoltageDefenseOsc[(Common.GetBits(this._romUConfig, 3, 4) >> 3)]; }
            set
            {
                var osc = (ushort) StringsV2.VoltageDefenseOsc.IndexOf(value);
                this._romUConfig = Common.SetBits(this._romUConfig,osc ,3,4);
            }
        }

        /// <summary>
        /// Сброс
        /// </summary>
        public bool Reset
        {
            get { return Common.GetBit(this._romUConfig, 13); }
            set { this._romUConfig = Common.SetBit(this._romUConfig, 13, value); }
        }
        /// <summary>
        /// Параметр
        /// </summary>
        public ushort Parameter
        {
            get { return (ushort) (Common.GetBits(this._romUConfig, 8, 9, 10) >> 8); }
            set { this._romUConfig = Common.SetBits(this._romUConfig, value, 8, 9, 10); }
        }
        /// <summary>
        /// БЛОКИРОВКА ПО U 5В
        /// </summary>
        public int Block5V
        {
            get
            {
               return Common.GetBits(this._romUConfig, 11)>>11;       
            }
            set
            {
                this._romUConfig = Common.SetBits(this._romUConfig, (ushort) value, 11);
            }  
        }
        #endregion [Properties]


        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]


        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            int index = 0;

            this._romUConfig = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romUBlock = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romUValue = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romUWait = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romUValueAdd = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romUWaitAdd = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._reserv1 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._reserv2 = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            return new[]
                {
                    this._romUConfig,
                    this._romUBlock,
                    this._romUValue,
                    this._romUWait,
                    this._romUValueAdd,
                    this._romUWaitAdd,
                    this._reserv1,
                    this._reserv2
                };


        }

        #endregion [IStructInit Members]
    }
}