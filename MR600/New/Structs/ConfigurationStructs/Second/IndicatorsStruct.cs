﻿using BEMN.Devices;
using BEMN.MBServer;
using BEMN.MR600.New.HelpClasses;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Second
{
    /// <summary>
    /// Индикатор
    /// </summary>
    public struct IndicatorsStruct : IStruct, IStructInit
    {
        #region [Private fields]
        private ushort _typeAndSignal;
        private ushort _clear; 
        #endregion [Private fields]


        #region [Properties]
        public string Type
        {
            get
            {
                var type = Common.GetBit(this._typeAndSignal, 15) ? 1 : 0;
                return StringsV2.RelayType[type];
            }
            set
            {
                var flag = StringsV2.RelayType.IndexOf(value) == 1;
                this._typeAndSignal = Common.SetBit(this._typeAndSignal, 15, flag);
            }
        }

        public string Signal
        {
            get
            {
                var signal = Common.SetBit(this._typeAndSignal, 15, false);
                return StringsV2.OutputSignals[signal];
            }

            set
            {
                var signal = StringsV2.OutputSignals.IndexOf(value);
                this._typeAndSignal = Common.SetBits(this._typeAndSignal, (ushort) signal, 0, 1, 2, 3, 4, 5, 6, 7);
            }
        }

        public bool Ind
        {
            get { return Common.GetBit(this._clear, 0); }
            set { this._clear = Common.SetBit(this._clear, 0, value); }
        }

        public bool Sj
        {
            get { return Common.GetBit(this._clear, 2); }
            set { this._clear = Common.SetBit(this._clear, 2, value); }
        }

        public bool Aj
        {
            get { return Common.GetBit(this._clear, 1); }
            set { this._clear = Common.SetBit(this._clear, 1, value); }
        } 
        #endregion [Properties]

        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]


        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            int index = 0;

            this._typeAndSignal  = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this._clear = Common.TOWORD(array[index + 1], array[index]);

        }

        public ushort[] GetValues()
        {
            return new[]
                {
                    this._typeAndSignal,
                    this._clear
                };


        }

        #endregion [IStructInit Members]
    }
}