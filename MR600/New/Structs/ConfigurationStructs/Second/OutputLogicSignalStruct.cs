﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.MBServer;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Second
{
    /// <summary>
    /// выходные логические сигналы 16 байт
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct OutputLogicSignalStruct
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)] private ushort[] _logicSignals;

        public OutputLogicSignalStruct(bool a)
        {
            this._logicSignals = new ushort[8];
        }

        #region [IStruct Members]

        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }

        #endregion [IStruct Members]


        #region [IStructInit Members]

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this._logicSignals = new ushort[8];
            for (int i = 0; i < this._logicSignals.Length; i++)
            {
                this._logicSignals[i] = Common.TOWORD(array[index + 1], array[index]);
                index += sizeof (UInt16);
            }

        }

        public ushort[] GetValues()
        {
            return this._logicSignals;
        }

        #endregion [IStructInit Members]

        public  BitArray Bits
        {
            get
            {
                var mass = Common.TOBYTES(this._logicSignals, false);
                var result = new BitArray(mass);
                return result;
            }

            set
            {
                for (int i = 0; i < value.Count; i++)
                {
                    int x = i/16;
                    int y = i%16;
                    this._logicSignals[x] = Common.SetBit(this._logicSignals[x], y, value[i]);
                }
            }
        }
    }

}