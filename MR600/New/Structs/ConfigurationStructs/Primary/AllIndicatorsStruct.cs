﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.MR600.New.Structs.ConfigurationStructs.Second;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Primary
{
    /// <summary>
    /// индикаторы (10)
    /// </summary>
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public  struct AllIndicatorsStruct : IStruct, IStructInit
    {
        #region [Private fields]
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public IndicatorsStruct[] Indicators; 
        #endregion [Private fields]

       public AllIndicatorsStruct(bool a)
       {
           this.Indicators = new IndicatorsStruct[10];
       }

        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]


        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.Indicators = new IndicatorsStruct[10];

            for (int i = 0; i < this.Indicators.Length; i++)
            {
                var oneStruct = new byte[Marshal.SizeOf(typeof(IndicatorsStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.Indicators[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(IndicatorsStruct));
            }

        }

        public ushort[] GetValues()
        {

            var result = new List<ushort>();
            foreach (var indikator in this.Indicators)
            {
                result.AddRange(indikator.GetValues());
            }
            return result.ToArray();

        }
        #endregion [IStructInit Members]
    }
}
