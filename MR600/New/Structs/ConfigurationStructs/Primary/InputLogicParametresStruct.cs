﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.MR600.New.Structs.ConfigurationStructs.Second;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Primary
{
    /// <summary>
    /// входные логические параметры 32 байта (3)
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct InputLogicParametresStruct
    {
        /// <summary>
        /// 4-И 4-ИЛИ
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public LogicSignalStruct[] LogicSignalsStructs;


        public InputLogicParametresStruct(bool a)
        {
            this.LogicSignalsStructs = new LogicSignalStruct[8];
        }


        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]


        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.LogicSignalsStructs = new LogicSignalStruct[8];
            for (int i = 0; i < this.LogicSignalsStructs.Length; i++)
            {
                var oneStruct = new byte[Marshal.SizeOf(typeof(LogicSignalStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.LogicSignalsStructs[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(LogicSignalStruct));
            }

        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>();
            foreach (var signalStruct in LogicSignalsStructs)
            {
                result.AddRange(signalStruct.GetValues());
            }
            return result.ToArray();
        }

        #endregion [IStructInit Members]

    }
}