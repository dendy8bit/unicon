﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.MR600.New.Structs.ConfigurationStructs.Second;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Primary
{
    /// <summary>
    /// выходные реле(9)
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct OutputRelaysStruct
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)] public RelayStruct[] Relays;

        public OutputRelaysStruct(bool a)
        {
            this.Relays = new RelayStruct[16];
        }

        #region [IStruct Members]

        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }

        #endregion [IStruct Members]


        #region [IStructInit Members]

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.Relays = new RelayStruct[16];
            for (int i = 0; i < this.Relays.Length; i++)
            {
                var oneStruct = new byte[Marshal.SizeOf(typeof (RelayStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.Relays[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof (RelayStruct));
            }
        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>();
            foreach (var relay in this.Relays)
            {
                result.AddRange(relay.GetValues());
            }
            return result.ToArray();
        }

        #endregion [IStructInit Members]
    }
}
