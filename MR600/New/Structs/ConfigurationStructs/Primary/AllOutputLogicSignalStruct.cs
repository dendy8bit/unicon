﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.MR600.New.Structs.ConfigurationStructs.Second;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Primary
{
    /// <summary>
    /// все выходные логические сигналы(8)
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct AllOutputLogicSignalStruct : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public OutputLogicSignalStruct[] LogicSignalsStruct;

      public AllOutputLogicSignalStruct(bool a)
      {
          this.LogicSignalsStruct = new OutputLogicSignalStruct[8];
          for (int i = 0; i < this.LogicSignalsStruct.Length; i++)
          {
              this.LogicSignalsStruct[i] = new OutputLogicSignalStruct(a);
          }
      }


        #region [IStruct Members]

        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }

        #endregion [IStruct Members]


        #region [IStructInit Members]

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.LogicSignalsStruct = new OutputLogicSignalStruct[8];
            for (int i = 0; i < this.LogicSignalsStruct.Length; i++)
            {
                var oneStruct = new byte[Marshal.SizeOf(typeof(OutputLogicSignalStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.LogicSignalsStruct[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(OutputLogicSignalStruct));
            }

        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>();
            foreach (var signalStruct in this.LogicSignalsStruct)
            {
                result.AddRange(signalStruct.GetValues());
            }

            return result.ToArray();
        }

        #endregion [IStructInit Members]
    }
}