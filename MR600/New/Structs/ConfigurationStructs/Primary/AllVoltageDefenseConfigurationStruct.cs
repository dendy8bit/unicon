﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.MR600.New.Structs.ConfigurationStructs.Second;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Primary
{
    /// <summary>
    /// конфигурации защит U обе группы уставок (6)
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct AllVoltageDefenseConfigurationStruct : IStruct, IStructInit
    {
        #region [Private fields]

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)] 
        public VoltageDefenseConfigurationStruct[] VoltageDefenseConfiguration;

        #endregion [Private fields]


        #region [Ctor's]

        public AllVoltageDefenseConfigurationStruct(bool a)
        {
            this.VoltageDefenseConfiguration = new VoltageDefenseConfigurationStruct[2];
            for (int i = 0; i < this.VoltageDefenseConfiguration.Length; i++)
            {
                this.VoltageDefenseConfiguration[i] = new VoltageDefenseConfigurationStruct(a);
            }
        }

        #endregion [Ctor's]


        #region [IStruct Members]

        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }

        #endregion [IStruct Members]


        #region [IStructInit Members]

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.VoltageDefenseConfiguration = new VoltageDefenseConfigurationStruct[2];
            for (int i = 0; i < this.VoltageDefenseConfiguration.Length; i++)
            {
                var oneStruct = new byte[Marshal.SizeOf(typeof (VoltageDefenseConfigurationStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.VoltageDefenseConfiguration[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof (VoltageDefenseConfigurationStruct));
            }

        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>();
            foreach (var defense in VoltageDefenseConfiguration)
            {
                result.AddRange(defense.GetValues());
            }
            return result.ToArray();
        }

        #endregion [IStructInit Members]
    }
}