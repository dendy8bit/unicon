﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.MBServer;
using BEMN.MR600.New.HelpClasses;
using BEMN.MR600.New.Structs.ConfigurationStructs.Second;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Primary
{
    /// <summary>
    /// выходные дополнительные реле 8 байта (4)
    /// </summary>
     [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct OutputAddedRelaysStruct : IStruct, IStructInit
    {
        /// <summary>
        /// тип работы выходного реле 1,2
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)] 
         public RelayStruct[] Relay;

        /// <summary>
        ///сброс ступени
        /// </summary>
        public ushort _reset;

        private ushort _reserv;


         public OutputAddedRelaysStruct(bool a)
         {
             this.Relay = new RelayStruct[2];
             this._reset = 0;
             this._reserv = 0;
         }

        public string Reset
        {
            get { return StringsV2.InputSignals[this._reset]; }
            set { this._reset = (ushort) StringsV2.InputSignals.IndexOf(value); }
        }


        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]


        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.Relay = new RelayStruct[2];
            for (int i = 0; i < this.Relay.Length; i++)
            {
                var oneStruct = new byte[Marshal.SizeOf(typeof(RelayStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.Relay[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(RelayStruct));
            }
            this._reset = Common.TOWORD(array[index + 1], array[index]);
            index += Marshal.SizeOf(typeof(ushort));
            this._reserv = Common.TOWORD(array[index + 1], array[index]);

        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>();
            foreach (var relay in this.Relay)
            {
                result.AddRange(relay.GetValues());
            }
            result.Add(this._reset);
            result.Add(this._reserv);
            return result.ToArray();
        }

        #endregion [IStructInit Members]
    }
}