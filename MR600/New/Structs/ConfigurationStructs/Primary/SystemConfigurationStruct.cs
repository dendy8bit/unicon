﻿using System;
using BEMN.Devices;
using BEMN.MBServer;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Primary
{
    /// <summary>
    /// Параметры системы и осцилографа(11)
    /// </summary>

    public struct SystemConfigurationStruct : IStruct, IStructInit
    {
     //   private ushort _address;
     //   private ushort _speed;
     //   private ushort _delay;
        private ushort _oscConfig;

    /*    public SystemConfigurationStruct(bool a)
        {
            this._address = ushort.MaxValue;
            this._speed = ushort.MaxValue;
            this._delay = ushort.MaxValue;
            this._oscConfig = 0;
        }*/

      /*  #region [Private Properties]
        private ushort Address
        {
            get
            {
                return (ushort)(this._address == ushort.MaxValue ? 1 : _address);
            }
        }

        private ushort Speed
        {
            get
            {
                return (ushort)(this._speed == ushort.MaxValue ? 7 : _speed);
            }
        }

        private ushort Delay
        {
            get
            {
                return (ushort)(this._delay == ushort.MaxValue ? 0 : _delay);
            }
        }

        #endregion [Private Properties]*/

        /// <summary>
        /// Длительность
        /// </summary>
        public ushort Duration
        {
            get { return Common.GetBits(this._oscConfig, 0, 1, 2, 3, 4, 5, 6); }
            set { this._oscConfig =Common.SetBits(this._oscConfig, value, 0, 1, 2, 3, 4, 5, 6); }
        }

        /// <summary>
        /// Фиксация
        /// </summary>
        public ushort Fixation
        {
            get { return (ushort) (Common.GetBits(this._oscConfig, 7) >> 7); }
            set {this._oscConfig= Common.SetBits(this._oscConfig, value, 7); }
        }

        /// <summary>
        /// Длительность предзаписи
        /// </summary>
        public ushort PreRecording
        {
            get { return (ushort) (Common.GetBits(this._oscConfig, 8, 9, 10, 11, 12, 13, 14, 15)>> 8); }
            set {this._oscConfig= Common.SetBits(this._oscConfig, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]


        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            int index = 0;

         /*   this._address = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._speed = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._delay = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);*/

            this._oscConfig = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            return new[]
                {
                //    this.Address,
                //    this.Speed,
                //    this.Delay,
                    this._oscConfig
                };


        }

        #endregion [IStructInit Members]
    }
}
