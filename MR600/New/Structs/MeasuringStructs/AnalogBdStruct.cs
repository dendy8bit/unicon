﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MR600.New.Configuration.ConfigStruct;

namespace BEMN.MR600.New.Structs.MeasuringStructs
{
    public class AnalogBdStruct : StructBase
    {
        [Layout(0, Count = 11)] private ushort[] _analogValues;
        
        private ushort GetMean(List<AnalogBdStruct> list, Func<AnalogBdStruct, ushort> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (ushort)(sum / (double)count);
        }


        public string GetF(List<AnalogBdStruct> list)
        {
            var value = this.GetMean(list, o => o._analogValues[10]);
            return ValuesConverterCommon.Analog.GetF(value);
        }
        public string GetUa(List<AnalogBdStruct> list, RomMeasuringStruct measure)
        {
            var value = this.GetMean(list, o => o._analogValues[1]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }

        public string GetUb(List<AnalogBdStruct> list, RomMeasuringStruct measure)
        {
            var value = this.GetMean(list, o => o._analogValues[2]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }

        public string GetUc(List<AnalogBdStruct> list, RomMeasuringStruct measure)
        {
            var value = this.GetMean(list, o => o._analogValues[3]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }

        public string GetUab(List<AnalogBdStruct> list, RomMeasuringStruct measure)
        {
            var value = this.GetMean(list, o => o._analogValues[4]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }

        public string GetUbc(List<AnalogBdStruct> list, RomMeasuringStruct measure)
        {
            var value = this.GetMean(list, o => o._analogValues[5]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }

        public string GetUca(List<AnalogBdStruct> list, RomMeasuringStruct measure)
        {
            var value = this.GetMean(list, o => o._analogValues[6]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }
        public string GetU0(List<AnalogBdStruct> list, RomMeasuringStruct measure)
        {
            var value = this.GetMean(list, o => o._analogValues[7]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }

        public string GetU1(List<AnalogBdStruct> list, RomMeasuringStruct measure)
        {
            var value = this.GetMean(list, o => o._analogValues[8]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }

        public string GetU2(List<AnalogBdStruct> list, RomMeasuringStruct measure)
        {
            var value = this.GetMean(list, o => o._analogValues[9]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }

        public string GetUn(List<AnalogBdStruct> list, RomMeasuringStruct measure)
        {
            var value = this.GetMean(list, o => o._analogValues[0]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorUo);
        } 
    }
}
