﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Int;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR600.New.Configuration.ConfigStruct;
using BEMN.MR600.New.Configuration.ConfigStruct.External;
using BEMN.MR600.New.Configuration.ConfigStruct.FaultSignals;
using BEMN.MR600.New.Configuration.ConfigStruct.Frequency;
using BEMN.MR600.New.Configuration.ConfigStruct.ILS;
using BEMN.MR600.New.Configuration.ConfigStruct.Indicators;
using BEMN.MR600.New.Configuration.ConfigStruct.Relay;
using BEMN.MR600.New.Configuration.ConfigStruct.VLS;
using BEMN.MR600.New.Configuration.ConfigStruct.Voltage;
using BEMN.MR600.New.HelpClasses;

namespace BEMN.MR600.New.Configuration
{
    public partial class Mr600ConfigurationFormV2 : Form, IFormView
    {
        #region [Constants]
        private const string SETPOINTS_READ_OK = "Конфигурация успешно прочитана";
        private const string SETPOINTS_READ_FAIL = "Уставки невозможно прочитать";
        private const string FILE_SAVE_FAIL = "Невозможно сохранить файл";
        private const string FILE_LOAD_FAIL = "Невозможно загрузить файл";
        private const string XML_HEAD = "MR600V3_SET_POINTS";
        private const string WRITE_FAIL = "Невозможно записать конфигурацию";
        private const string WRITE_OK = "Конфигурация успешно записана";
        private const string WRITTING_CONFIG = "Записать конфигурацию в устройство?";
        private const string WRITE_CONFIG = "Запись конфигурации";
        private const string START_READ = "Идет чтение";
        private const string START_WRITE = "Идет запись";
        private const string MR600_BASE_CONFIG_PATH = "\\MR600\\MR600_BaseConfig_v{0}.xml";
        private const string BASE_CONFIG_LOAD_SUCCESSFULLY = "Базовые уставки успешно загружены";

        #endregion [Constants]


        #region [Private fields]
        private readonly MemoryEntity<RamMemoryStruct> _ramMemory;
        private RamMemoryStruct _currentStruct;
        private readonly MR600 _device;
        private RadioButtonSelector _groupSelector;
        private DataGridView[] _lsBoxes;
        private CheckedListBox[] _vlsBoxes;
        private NewStructValidator<RomMeasuringStruct> _romMeasuringValidator;
        private NewCheckedListBoxValidator<KeysStruct> _keysValidator;
        private NewStructValidator<RomExternalSignalsStruct> _romExternalSignalValidator;
        private NewStructValidator<AllFaultSignalsStruct> _faultStructValidator;
        private NewDgwValidatior<LogicSignalStruct>[] _inputLogicValidator;
        private StructUnion<InputLogicParametresStruct> _inputLogicUnion;
        private NewDgwValidatior<AllRelaysStruct, RelayStruct> _relaysValidator;
        private NewStructValidator<ResetStepStruct> _resetStepValidator;

        private NewDgwValidatior<AllExternalDefensesStruct, ExternalDefenseStruct> _externalDefenseValidatior;
        private NewDgwValidatior<AllVoltageDefensesStruct, VoltageDefensesStruct> _voltageDefenseValidatior;
        private NewDgwValidatior<AllFrequencyDefensesStruct, FrequencyDefensesStruct> _frequencyDefenseValidatior;
        private StructUnion<DefensesSetpointStruct> _defenseUnion;
        private SetpointsValidator<AllSetpointStruct, DefensesSetpointStruct> _defensesValidator;

        private NewCheckedListBoxValidator<OutputLogicSignalStruct>[] _vlsValidator;
        private StructUnion<AllOutputLogicSignalStruct> _vlsUnion;
        private NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct> _indicatorsValidatior;
        private NewStructValidator<OscilloscopeConfigStruct> _oscValidator; 
        private StructUnion<RamMemoryStruct> _configUnion;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr600ConfigurationFormV2()
        {
            this.InitializeComponent();
        }

        public Mr600ConfigurationFormV2(MR600 device)
        {
            this.InitializeComponent();
            this._currentStruct = new RamMemoryStruct();
            this._ramMemory = device.Mr600DeviceV2.RamMemory;
            this._ramMemory.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadSuccessful);
            this._ramMemory.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ReadFail);

            this._ramMemory.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.WriteSuccessful);
            this._ramMemory.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.WriteFail);

            this._ramMemory.ReadOk += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
            this._ramMemory.WriteOk += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
            this.Prepare();
            this._device = device;
        }
        /// <summary>
        /// Подготовка
        /// </summary>
        private void Prepare()
        {
            this._lsBoxes = new[]
            {
                this._inputSignals1, this._inputSignals2, this._inputSignals3, this._inputSignals4, this._inputSignals5, this._inputSignals6, this._inputSignals7, this._inputSignals8
            };

            this._vlsBoxes = new[]
            {
                this.VLScheckedListBox1, this.VLScheckedListBox2, this.VLScheckedListBox3, this.VLScheckedListBox4, this.VLScheckedListBox5, this.VLScheckedListBox6, this.VLScheckedListBox7, this.VLScheckedListBox8
            };

            ToolTip toolTip = new ToolTip();
            //  измерительный канал
            this._romMeasuringValidator = new NewStructValidator<RomMeasuringStruct>(
                toolTip,
                new ControlInfoText(this._TN_Box, new Ustavka128000Rule()),
                new ControlInfoCombo(this._defectTN_CB, StringsV2.InputSignals),
                new ControlInfoText(this._TNNP_Box, new Ustavka128000Rule()),
                new ControlInfoCombo(this._defectTNNP_CB, StringsV2.InputSignals));
            // Ключи
            this._keysValidator = new NewCheckedListBoxValidator<KeysStruct>(this.keysCheckList, StringsV2.KeyNumbers);
            // Внешние сигналы(Дополнительные сигналы)
            this._romExternalSignalValidator = new NewStructValidator<RomExternalSignalsStruct>(
                toolTip,
                new ControlInfoCombo(this._inputSignalsIndicationResetCombo, StringsV2.InputSignals),
                new ControlInfoCombo(this._inputSignalsConstraintResetCombo, StringsV2.InputSignals));
            // Сигнал неисправность
            this._faultStructValidator = new NewStructValidator<AllFaultSignalsStruct>(
                toolTip,
                new ControlInfoValidator(new NewCheckedListBoxValidator<FaultSignalStruct>(this._dispepairCheckList, StringsV2.FaultSignalsDictionary)),
                new ControlInfoText(this._outputSignalsDispepairTimeBox, new TimeRule()));
            
            //Логические сигналы
            this._inputLogicValidator = new NewDgwValidatior<LogicSignalStruct>[InputLogicParametresStruct.LOGIC_COUNT];

            for (int i = 0; i < InputLogicParametresStruct.LOGIC_COUNT; i++)
            {
                this._inputLogicValidator[i] = new NewDgwValidatior<LogicSignalStruct>
                    (
                    this._lsBoxes[i],
                    LogicSignalStruct.DISCRETS_COUNT,
                    toolTip,
                    new ColumnInfoCombo(StringsV2.LsSignals, ColumnsType.NAME),
                    new ColumnInfoCombo(StringsV2.LogicState)
                    );
            }
            this._inputLogicUnion = new StructUnion<InputLogicParametresStruct>(this._inputLogicValidator);
            // Дополнительные и выходные реле
            this._relaysValidator = new NewDgwValidatior<AllRelaysStruct, RelayStruct>
                (this._outputReleGrid,
                18,
                toolTip,
                new ColumnInfoCombo(StringsV2.RelayNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsV2.OutputSignalsType),
                new ColumnInfoCombo(StringsV2.OutputSignals),
                new ColumnInfoText(RulesContainer.IntTo3M)
                );
            // Сброс шага
            this._resetStepValidator = new NewStructValidator<ResetStepStruct>(
                toolTip,
                new ControlInfoCombo(this._inputSignalsTN_DispepairCombo, StringsV2.InputSignals));

            // Внешние защиты
            this._externalDefenseValidatior = new NewDgwValidatior<AllExternalDefensesStruct, ExternalDefenseStruct>(this._externalDefenseGrid,
                8,
                toolTip,
                new ColumnInfoCombo(StringsV2.ExternalDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsV2.ExternalDefenseMode),
                new ColumnInfoCombo(StringsV2.ExternalDefensesSignals),
                new ColumnInfoCombo(StringsV2.ExternalDefensesSignals),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsV2.ExternalDefensesSignals),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCheck())
            {
                TurnOff = new[]
                {
                    new TurnOffDgv(this._externalDefenseGrid, new TurnOffRule(1, StringsV2.ExternalDefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8, 9))
                }
            };
            // Защиты по напряжению
            this._voltageDefenseValidatior = new NewDgwValidatior<AllVoltageDefensesStruct, VoltageDefensesStruct>(
                new[]
                {
                    this._voltageDefensesUBGrid, this._voltageDefensesUMGrid, this._voltageDefensesU0Grid, this._voltageDefensesU2Grid, this._voltageDefensesU1Grid
                },
                new int[] {4, 4, 4, 2, 2},
                toolTip,
                new ColumnInfoCombo(StringsV2.VoltageDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsV2.ExternalDefenseMode),
                new ColumnInfoCombo(StringsV2.ExternalDefensesSignals),
                new ColumnInfoCombo(StringsV2.ParametrU, ColumnsType.COMBO, true, false, false, false, false),
                new ColumnInfoCombo(StringsV2.ParametrU, ColumnsType.COMBO, false, true, false, false, false),
                new ColumnInfoCombo(StringsV2.ParametrU0, ColumnsType.COMBO, false, false, true, false, false),
                new ColumnInfoCombo(StringsV2.ParametrU2, ColumnsType.COMBO, false, false, false, true, false),
                new ColumnInfoCombo(StringsV2.ParametrU1, ColumnsType.COMBO, false, false, false, false, true),
                new ColumnInfoText(RulesContainer.Ustavka256),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.Ustavka256),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsV2.VoltageDefenseOsc),
                new ColumnInfoCheck())
            {
                TurnOff = new[]
                {
                    new TurnOffDgv(this._voltageDefensesUBGrid, new TurnOffRule(1, StringsV2.ExternalDefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)),
                    new TurnOffDgv(this._voltageDefensesUMGrid, new TurnOffRule(1, StringsV2.ExternalDefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)), 
                    new TurnOffDgv(this._voltageDefensesU0Grid, new TurnOffRule(1, StringsV2.ExternalDefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)),
                    new TurnOffDgv(this._voltageDefensesU2Grid, new TurnOffRule(1, StringsV2.ExternalDefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)), 
                    new TurnOffDgv(this._voltageDefensesU1Grid, new TurnOffRule(1, StringsV2.ExternalDefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)) 
                }
            };
            // Защиты по частоте
            this._frequencyDefenseValidatior = new NewDgwValidatior<AllFrequencyDefensesStruct, FrequencyDefensesStruct>(this._frequenceDefensesGrid,
                8,
                toolTip,
                new ColumnInfoCombo(StringsV2.FrequenceDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsV2.ExternalDefenseMode),
                new ColumnInfoCombo(StringsV2.ExternalDefensesSignals),
                new ColumnInfoText(RulesContainer.Ustavka40To60),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.Ustavka40To60),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCombo(StringsV2.VoltageDefenseOsc),
                new ColumnInfoCheck())
            {
                TurnOff = new[]
                {
                    new TurnOffDgv(this._frequenceDefensesGrid,
                        new TurnOffRule(1, StringsV2.ExternalDefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8, 9))
                }
            };
            // Объединение всех защит
            this._defenseUnion = new StructUnion<DefensesSetpointStruct>(this._externalDefenseValidatior, this._voltageDefenseValidatior, this._frequencyDefenseValidatior);
            // Переключатель между вставками
            this._groupSelector = new RadioButtonSelector(this._mainRadioBtnGroup, this._reserveRadioBtnGroup, this._btnGroupChange);
            // Общий для всех защит валидатор
            this._defensesValidator = new SetpointsValidator<AllSetpointStruct, DefensesSetpointStruct>(this._groupSelector, this._defenseUnion);

            // ВЛС
            this._vlsValidator = new NewCheckedListBoxValidator<OutputLogicSignalStruct>[AllOutputLogicSignalStruct.VLS_COUNT];
            for (int i = 0; i < AllOutputLogicSignalStruct.VLS_COUNT; i++)
            {
                this._vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicSignalStruct>(this._vlsBoxes[i], StringsV2.OutputLogicSignals);
            }
            this._vlsUnion = new StructUnion<AllOutputLogicSignalStruct>(this._vlsValidator); 

            // Индикаторы
            this._indicatorsValidatior = new NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct>(this._indicatorsGrid,
                AllIndicatorsStruct.COUNT_INDICATORS,
                toolTip,
                new ColumnInfoCombo(StringsV2.IndicatorNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsV2.OutputSignalsType),
                new ColumnInfoCombo(StringsV2.OutputSignals),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                );

            // Конфигурация осциллографа
            this._oscValidator = new NewStructValidator<OscilloscopeConfigStruct>(
                toolTip,
                new ControlInfoCombo(this._oscLength, StringsV2.OscPeriodDuration),
                new ControlInfoCombo(this._oscFixationCombo, StringsV2.OscFicsation),
                new ControlInfoText(this._oscWriteLength, RulesContainer.Ushort1To100));

            // Вся структура конфигурации
            this._configUnion = new StructUnion<RamMemoryStruct>(
                this._romMeasuringValidator, 
                this._keysValidator, 
                this._romExternalSignalValidator,
                this._faultStructValidator, 
                this._inputLogicUnion, 
                this._resetStepValidator,
                this._defensesValidator, 
                this._vlsUnion,
                this._relaysValidator,
                this._indicatorsValidatior, 
                this._oscValidator);
        }
        #endregion [Ctor's]


        #region [Ввод-вывод уставок]

        private void WriteFail()
        {
            MessageBox.Show(WRITE_FAIL);
        }

        private void WriteSuccessful()
        {
            this._statusLabel.Text = WRITE_OK;
        }
        /// <summary>
        /// Уставки успешно прочитаны
        /// </summary>
        private void ReadSuccessful()
        {
            this._currentStruct = this._ramMemory.Value;
            this._configUnion.Set(this._currentStruct);
            this._statusLabel.Text = SETPOINTS_READ_OK;
        }
        /// <summary>
        /// Уставки невозможно прочитать
        /// </summary>
        private void ReadFail()
        {
            this._statusLabel.Text = SETPOINTS_READ_FAIL;
        }
        
        #endregion


        #region [Чтение/Запись конфигурации]
        /// <summary>
        /// Запуск чтения конфигурации из устройства
        /// </summary>
        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._exchangeProgressBar.Value = 0;
            this._statusLabel.Text = START_READ;
            this._ramMemory.LoadStruct();
        }


        private void SetConfiguration()
        {
            this._currentStruct = this._configUnion.Get();
            this._ramMemory.Value = this._currentStruct;
        }


        /// <summary>
        /// Запись конфигурации в устройство
        /// </summary>
        private void StartWrite()
        {
            this.SetConfiguration();
            this._ramMemory.SaveStruct();
            this._device.SetBit(this._device.DeviceNumber, 0, true, "Save_settings", this._device);
        }


        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            try
            {
                var doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MR600"));
                
                var values = this._currentStruct.GetValues();

                XmlElement element = doc.CreateElement(XML_HEAD);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);
             
                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("Файл {0} успешно сохранён", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }
        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                var doc = new XmlDocument();
                doc.Load(binFileName);
                var a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if(a == null)
                    throw new NullReferenceException();
                var values = Convert.FromBase64String(a.InnerText);
                var ramStruct = new RamMemoryStruct();
                ramStruct.InitStruct(values);
                this._currentStruct = ramStruct;
                this._configUnion.Set(this._currentStruct);
                this._statusLabel.Text = string.Format("Файл {0} успешно загружен", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }
        #endregion [Чтение/Запись конфигурации]


        #region [События взаимодействия]
        /// <summary>
        /// Сохранение в файл
        /// </summary>
        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveinFile();
        }

        private void SaveinFile()
        {
            this._saveConfigurationDlg.FileName = string.Format("МР600_Уставки_версия {0:F1}.xml", this._device.DeviceVersion);
            if (DialogResult.OK != this._saveConfigurationDlg.ShowDialog()) return;
            string message;
            if (this._configUnion.Check(out message, true))
            {
                this.SetConfiguration();
                this.Serialize(this._saveConfigurationDlg.FileName);
            }
            else
            {
                MessageBox.Show("Имеются неверные уставки. Невозможно сохранить в файл", "Ошибка записи", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Загрузить из файла
        /// </summary>
        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();       
        }

        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName);
            }  
        }

        /// <summary>
        /// Прочитать из устройства
        /// </summary>
        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void Mr600ConfigurationFormV2_Shown(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig)
            {
                this.StartRead();
            }
            this._configUnion.Reset();
        }

        /// <summary>
        /// Записать в устройтво
        /// </summary>
        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig(); 
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(WRITTING_CONFIG, WRITE_CONFIG, MessageBoxButtons.YesNo) != DialogResult.Yes) return;
            string message;
            if (this._configUnion.Check(out message, true))
            {
                this._exchangeProgressBar.Value = 0;
                this._statusLabel.Text = START_WRITE;
                this.StartWrite();
            }
            else
            {
                MessageBox.Show("Имеются неверные уставки. Невозможно сохранить в устройство", "Ошибка записи",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            } 
        }
        private void _resetSetpointBtn_Click(object sender, EventArgs e)
        {
            this._configUnion.Reset();
        }
        #endregion [События взаимодействия]


        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR600); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(MR600); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }
        #endregion [IFormView Members]


        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtmlFile();
        }

        private void SaveToHtmlFile()
        {
            string message;
            if (this._configUnion.Check(out message, true))
            {
                this.SetConfiguration();
                try
                {
                    this._currentStruct.DeviceVersion = this._device.DeviceVersion;
                    this._currentStruct.DeviceNumber = this._device.DeviceNumber.ToString();
                    this._statusLabel.Text = HtmlExport.Export(Resources.MR600MainV3, Resources.MR600ResV3, this._currentStruct, this._currentStruct.DeviceType, this._currentStruct.DeviceVersion);
                }
                catch (Exception)
                {
                    MessageBox.Show("Ошибка сохранения");
                }
            }
        }

        private void ResetSetpoints(bool isDialog)   //загрузка базовых уставок из файла, isDialog - показывает будет ли диалог
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                double version = Common.VersionConverter(this._device.DeviceVersion);
                if (version == 3.0)
                {
                    doc.Load(Path.GetDirectoryName(Application.ExecutablePath) +
                             string.Format(MR600_BASE_CONFIG_PATH, "3.00"));
                }
                if (version == 3.01)
                {
                    doc.Load(Path.GetDirectoryName(Application.ExecutablePath) +
                             string.Format(MR600_BASE_CONFIG_PATH, "3.01"));
                }
                if (version == 3.02)
                {
                    doc.Load(Path.GetDirectoryName(Application.ExecutablePath) +
                             string.Format(MR600_BASE_CONFIG_PATH, "3.03"));
                }
                if (version == 3.03)
                {
                    doc.Load(Path.GetDirectoryName(Application.ExecutablePath) +
                             string.Format(MR600_BASE_CONFIG_PATH, "3.03"));
                }
                if (version >= 3.04)
                {
                    doc.Load(Path.GetDirectoryName(Application.ExecutablePath) +
                             string.Format(MR600_BASE_CONFIG_PATH, "3.04"));
                }

                XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);

                    this._currentStruct.InitStruct(values);

                this._configUnion.Set(this._currentStruct);
                this._statusLabel.Text = BASE_CONFIG_LOAD_SUCCESSFULLY;
            }
            catch
            {
                if (isDialog)
                {
                    if (MessageBox.Show("Невозможно загрузить файл стандартной конфигурации. Обнулить уставки?",
                            "Внимание", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        this._configUnion.Reset();
                }
                else
                {
                    this._configUnion.Reset();
                }
            }
        }
        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }

            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtmlFile();
                return;
            }

            if (e.ClickedItem == this.clearSetpointsItem)
            {
                this._configUnion.Reset();
                return;
            }
        }

        private void Mr600ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                   
                    this._exchangeProgressBar.Value = 0;
            this.StartRead();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void _resetSetpointsButton_Click(object sender, EventArgs e)
        {
            ResetSetpoints(true);
        }
    }
}
