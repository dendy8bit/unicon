﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;
using BEMN.MR600.New.HelpClasses;

namespace BEMN.MR600.New.Configuration.ConfigStruct.Frequency
{
    /// <summary>
    /// защиты частоты 16 байт
    /// </summary>
    public class FrequencyDefensesStruct : StructBase
    {
        [Layout(0)] private ushort _config;            //EQU     0       ;конфигурация                          2 байта
        [Layout(1)] private ushort _block;             //EQU     2       ;номер входа блокировки                2 байта
        [Layout(2)] private ushort _srab;              //EQU     4       ;уставка срабатывания                  2 байта
        [Layout(3)] private ushort _tSrab;             //EQU     6       ;выдержка времени срабатывания         2 байта
        [Layout(4)] private ushort _return;            //EQU     8       ;уставка возврата                      2 байта
        [Layout(5)] private ushort _tReturn;           //EQU     10      ;выдержка времени возврата             2 байта
        [Layout(6, Count = 2)] private ushort[] _res;

        [BindingProperty(0)]
        public string Mode
        {
            get { return Validator.Get(this._config, StringsV2.ExternalDefenseMode, 0, 1); }
            set { this._config = Validator.Set(value, StringsV2.ExternalDefenseMode, this._config, 0, 1); }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(1)]
        public string Block
        {
            get { return Validator.Get(this._block, StringsV2.ExternalDefensesSignals); }
            set { this._block = Validator.Set(value, StringsV2.ExternalDefensesSignals); }
        }

        /// <summary>
        /// 
        /// </summary>
        [BindingProperty(2)]
        public double UstavkaSrab
        {
            get { return ValuesConverterCommon.GetU(this._srab); }
            set { this._srab = ValuesConverterCommon.SetU(value); }
        }

        /// <summary>
        /// tср, время срабатывания
        /// </summary>
        [BindingProperty(3)]
        public int TimeSrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tSrab); }
            set { this._tSrab = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// Возврат
        /// </summary>
        [BindingProperty(4)]
        public bool Return
        {
            get { return Common.GetBit(this._config, 14); }
            set { this._config = Common.SetBit(this._config, 14, value); }
        }
        /// <summary>
        /// Возврат
        /// </summary>
        [BindingProperty(5)]
        public double UstavkaReturn
        {
            get { return ValuesConverterCommon.GetU(this._return); }
            set { this._return = ValuesConverterCommon.SetU(value); }
        }
        
        [BindingProperty(6)]
        public int TimeReturn
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tReturn); }
            set { this._tReturn = ValuesConverterCommon.SetWaitTime(value); }
        }
       
        /// <summary>
        /// Осциллограф
        /// </summary>
        [BindingProperty(7)]
        public string Osc
        {
            get { return Validator.Get(this._config, StringsV2.VoltageDefenseOsc, 3, 4); }
            set { this._config = Validator.Set(value, StringsV2.VoltageDefenseOsc, this._config, 3, 4); }
        }
        /// <summary>
        /// Сброс
        /// </summary>
        [BindingProperty(8)]
        public bool Reset
        {
            get { return Common.GetBit(this._config, 13); }
            set { this._config = Common.SetBit(this._config, 13, value); }
        }
    }
}