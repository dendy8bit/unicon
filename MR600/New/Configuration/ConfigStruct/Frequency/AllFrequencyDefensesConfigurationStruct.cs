﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR600.New.Configuration.ConfigStruct.Frequency
{
    /// <summary>
    /// конфигурации защит частоты обе группы уставок (7)
    /// </summary>
    public class AllFrequencyDefensesStruct : StructBase, IDgvRowsContainer<FrequencyDefensesStruct>
    {
        [Layout(0,Count = 8)]
        private FrequencyDefensesStruct[] _frequencyDefenses;
        
        public FrequencyDefensesStruct[] Rows
        {
            get { return _frequencyDefenses; }
            set { _frequencyDefenses = value; }
        }
    }
}
