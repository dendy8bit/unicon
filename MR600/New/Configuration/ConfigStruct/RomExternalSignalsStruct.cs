﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MR600.New.HelpClasses;

namespace BEMN.MR600.New.Configuration.ConfigStruct
{
    /// <summary>
    /// внешние сигналы
    /// </summary>
    public class RomExternalSignalsStruct : StructBase
    {
        [Layout(0)] private ushort _romInpClear; //вн.сигн.сброса сигнализации
        [Layout(1)] private ushort _romInpGroup; //вн.сигн.группы уставок

        /// <summary>
        /// Сброс индикации
        /// </summary>
        [BindingProperty(0)]
        public string ResetIndication
        {
            get { return Validator.Get(_romInpClear, StringsV2.InputSignals); }
            set { _romInpClear = Validator.Set(value, StringsV2.InputSignals); }
        }

        /// <summary>
        /// Переключение на резервные уставки
        /// </summary>
        [BindingProperty(1)]
        public string ReservSetpoints
        {
            get { return Validator.Get(_romInpGroup, StringsV2.InputSignals); }
            set { _romInpGroup = Validator.Set(value, StringsV2.InputSignals); }
        }
    }
}
