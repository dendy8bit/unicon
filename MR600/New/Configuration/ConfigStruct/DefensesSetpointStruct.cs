﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR600.New.Configuration.ConfigStruct.External;
using BEMN.MR600.New.Configuration.ConfigStruct.Frequency;
using BEMN.MR600.New.Configuration.ConfigStruct.Voltage;

namespace BEMN.MR600.New.Configuration.ConfigStruct
{
    public class DefensesSetpointStruct : StructBase
    {
        [Layout(0)] private AllExternalDefensesStruct _externalDefenses;
        [Layout(1)] private AllVoltageDefensesStruct _voltageDefenses;
        [Layout(2)] private AllFrequencyDefensesStruct _frequencyDefenses;

        [BindingProperty(0)]
        public AllExternalDefensesStruct ExternalDefenses
        {
            get { return _externalDefenses; }
            set { _externalDefenses = value; }
        }
        [BindingProperty(1)]
        public AllVoltageDefensesStruct VoltageDefenses
        {
            get { return _voltageDefenses; }
            set { _voltageDefenses = value; }
        }
        [BindingProperty(2)]
        public AllFrequencyDefensesStruct FrequencyDefenses
        {
            get { return _frequencyDefenses; }
            set { _frequencyDefenses = value; }
        }
    }
}
