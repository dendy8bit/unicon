﻿using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR600.New.Configuration.ConfigStruct.Relay
{
    public class AllRelaysStruct : StructBase, IDgvRowsContainer<RelayStruct>
    {
        #region Fields
        private OutputAddedRelaysStruct _addRelays;
        private OutputRelayStruct _relays;
        #endregion

        #region Constructors

        public AllRelaysStruct() : base()
        {
            _addRelays = new OutputAddedRelaysStruct();
            _relays = new OutputRelayStruct();
        }
        public AllRelaysStruct(OutputAddedRelaysStruct aR, OutputRelayStruct r)
        {
            _addRelays = aR.Clone<OutputAddedRelaysStruct>();
            _relays = r.Clone<OutputRelayStruct>();
        }
        #endregion

        #region Properties
        public RelayStruct[] Rows
        {
            get
            {
                var list = new List<RelayStruct>();
                list.AddRange(AddRelays.Relay);
                list.AddRange(Relays.Relays);
                return list.ToArray();
            }
            set
            {
                AddRelays.Relay = new RelayStruct[] {value[0], value[1]};
                Relays.Relays = value.Skip(2).ToArray();
            }
        }

        public OutputAddedRelaysStruct AddRelays
        {
            get { return _addRelays; }
        }

        public OutputRelayStruct Relays
        {
            get { return _relays; }
        }
        #endregion
    }
}
