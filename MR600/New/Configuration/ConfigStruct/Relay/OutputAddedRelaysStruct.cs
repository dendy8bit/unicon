﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR600.New.Configuration.ConfigStruct.Relay
{
    /// <summary>
    /// выходные дополнительные реле
    /// </summary>
    public class OutputAddedRelaysStruct : StructBase
    {
        [Layout(0, Count = 2)]
        private RelayStruct[] _relay;

        public RelayStruct[] Relay
        {
            get { return _relay; }
            set { _relay = value; }
        }
    }
}