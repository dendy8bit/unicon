﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR600.New.Configuration.ConfigStruct.Relay
{
    /// <summary>
    /// Выходные реле
    /// </summary>
    public class OutputRelayStruct : StructBase
    {
        [Layout(0, Count = 16)] private RelayStruct[] _relays;

        public RelayStruct[] Relays
        {
            get { return _relays; }
            set { _relays = value; }
        }
    }
}
