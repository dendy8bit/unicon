﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;
using BEMN.MR600.New.HelpClasses;

namespace BEMN.MR600.New.Configuration.ConfigStruct.External
{
    /// <summary>
    /// внешние защиты 12 байт
    /// </summary>
    public class ExternalDefenseStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _romExtConfig;       //конфигурация                                   2 байта
        [Layout(1)] private ushort _romExtBlock;        //номер входа блокировки                         2 байта
        [Layout(2)] private ushort _romExtInput;        //номер входа срабатывания                       2 байта
        [Layout(3)] private ushort _romExtWait;         //выдержка времени срабатывания                  2 байта
        [Layout(4)] private ushort _romExtInputAdd;     //номер входа возврата                           2 байта
        [Layout(5)] private ushort _romExtWaitAdd;      //выдержка времени возврвта                      2 байта 
        #endregion [Private fields]

        #region [Properties]
        /// <summary>
        /// Режим
        /// </summary>
        [BindingProperty(0)] 
        public string Mode
        {
            get { return Validator.Get(_romExtConfig, StringsV2.ExternalDefenseMode, 0, 1); }
            set { _romExtConfig = Validator.Set(value, StringsV2.ExternalDefenseMode, _romExtConfig, 0, 1); }
        }
        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(1)]
        public string RomExtBlock
        {
            get { return Validator.Get(this._romExtBlock, StringsV2.ExternalDefensesSignals); }
            set { this._romExtBlock = Validator.Set(value, StringsV2.ExternalDefensesSignals); }
        }
        /// <summary>
        /// Срабатывание
        /// </summary>
        [BindingProperty(2)]
        public string RomExtInput
        {
            get { return Validator.Get(this._romExtInput, StringsV2.ExternalDefensesSignals); }
            set { this._romExtInput = Validator.Set(value, StringsV2.ExternalDefensesSignals); }
        }
        /// <summary>
        /// Время срабатывания
        /// </summary>
        [BindingProperty(3)]
        public int RomExtWait
        {
            get { return ValuesConverterCommon.GetWaitTime(this._romExtWait); }
            set { this._romExtWait = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// Возврат
        /// </summary>
        [BindingProperty(4)]
        public bool Recovery
        {
            get { return Common.GetBit(this._romExtConfig, 14); }
            set { this._romExtConfig = Common.SetBit(this._romExtConfig, 14, value); }
        }
        /// <summary>
        /// Вход возврата
        /// </summary>
        [BindingProperty(5)]
        public string RomExtInputAdd
        {
            get { return Validator.Get(this._romExtInputAdd, StringsV2.ExternalDefensesSignals); }
            set { this._romExtInputAdd = Validator.Set(value, StringsV2.ExternalDefensesSignals); }
        }
        /// <summary>
        /// Время возврата
        /// </summary>
        [BindingProperty(6)]
        public int RomExtWaitAdd
        {
            get { return ValuesConverterCommon.GetWaitTime(_romExtWaitAdd); }
            set { this._romExtWaitAdd = ValuesConverterCommon.SetWaitTime(value); }
        } 
        /// <summary>
        /// Осциллограф
        /// </summary>
        [BindingProperty(7)]
        public bool Osc
        {
            get { return Common.GetBit(this._romExtConfig, 4); }
            set { this._romExtConfig = Common.SetBit(this._romExtConfig, 4, value); }
        }
        /// <summary>
        /// Сброс
        /// </summary>
        [BindingProperty(8)]
        public bool Reset
        {
            get { return Common.GetBit(this._romExtConfig, 13); }
            set { this._romExtConfig = Common.SetBit(this._romExtConfig, 13, value); }
        }
        #endregion [Properties]
    }
}