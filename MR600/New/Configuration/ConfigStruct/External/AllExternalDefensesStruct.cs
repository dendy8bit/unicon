﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR600.New.Configuration.ConfigStruct.External
{
    /// <summary>
    /// конфигурации внешних защит
    /// </summary>
    public class AllExternalDefensesStruct : StructBase, IDgvRowsContainer<ExternalDefenseStruct>
    {
        public const int COUNT_EXTERN_DEF = 8;
        [Layout(0, Count = COUNT_EXTERN_DEF)]
        private ExternalDefenseStruct[] _externalDefenses;

        public ExternalDefenseStruct[] Rows
        {
            get { return _externalDefenses; }
            set { _externalDefenses = value; }
        }
    }
}