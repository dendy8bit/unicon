﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MR600.New.HelpClasses;

namespace BEMN.MR600.New.Configuration.ConfigStruct
{
    public class ResetStepStruct:StructBase
    {
        [Layout(0)] private ushort _reset;
        [Layout(1)] private ushort _reserve;

        [BindingProperty(0)]
        public string Reset
        {
            get { return Validator.Get(_reset, StringsV2.InputSignals); }
            set { this._reset = Validator.Set(value, StringsV2.InputSignals); }
        }
    }
}
