﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR600.New.Configuration.ConfigStruct.Voltage
{
    /// <summary>
    /// конфигурации защит U обе группы уставок
    /// </summary>
    public class AllVoltageDefensesStruct : StructBase, IDgvRowsContainer<VoltageDefensesStruct>
    {
        [Layout(0, Count = 16)]
        private VoltageDefensesStruct[] _voltageDefenses;
        
        public VoltageDefensesStruct[] Rows
        {
            get { return _voltageDefenses; }
            set { _voltageDefenses = value; }
        }
    }
}