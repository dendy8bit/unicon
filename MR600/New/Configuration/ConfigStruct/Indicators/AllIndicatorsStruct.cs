﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR600.New.Configuration.ConfigStruct.Indicators
{
    /// <summary>
    /// индикаторы
    /// </summary>
    public class AllIndicatorsStruct : StructBase, IDgvRowsContainer<IndicatorsStruct>
    {
        public const int COUNT_INDICATORS = 10;
        [Layout(0, Count = COUNT_INDICATORS)]
        private IndicatorsStruct[] _indicators;

        public IndicatorsStruct[] Rows
        {
            get { return _indicators; }
            set { _indicators = value; }
        }
    }
}
