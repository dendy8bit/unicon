﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;
using BEMN.MR600.New.HelpClasses;

namespace BEMN.MR600.New.Configuration.ConfigStruct.Indicators
{
    /// <summary>
    /// Индикатор
    /// </summary>
    public class IndicatorsStruct : StructBase
    {
        #region Fields
        [Layout(0)]
        private ushort _typeAndSignal;
        [Layout(1)]
        private ushort _clear;
        #endregion

        #region Properties
        [BindingProperty(0)]
        public string Type
        {
            get
            {
                return Validator.Get(this._typeAndSignal, StringsV2.OutputSignalsType, 15);
            }
            set
            {

                this._typeAndSignal = Validator.Set(value, StringsV2.OutputSignalsType, _typeAndSignal, 15);;
            }
        }
        [BindingProperty(1)]
        public string Signal
        {
            get
            {
                return Validator.Get(this._typeAndSignal, StringsV2.OutputSignals, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
                    12, 13, 14);
            }
            set
            {
                this._typeAndSignal = Validator.Set(value, StringsV2.OutputSignals, _typeAndSignal, 0, 1, 2, 3, 4, 5, 6,
                    7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        [BindingProperty(2)]
        public bool Ind
        {
            get { return Common.GetBit(this._clear, 0); }
            set { this._clear = Common.SetBit(this._clear, 0, value); }
        }

        [BindingProperty(3)]
        public bool Aj
        {
            get { return Common.GetBit(this._clear, 1); }
            set { this._clear = Common.SetBit(this._clear, 1, value); }
        }

        [BindingProperty(4)]
        public bool Sj
        {
            get { return Common.GetBit(this._clear, 2); }
            set { this._clear = Common.SetBit(this._clear, 2, value); }
        }
        #endregion [Properties]
    }
}