﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR600.New.Configuration.ConfigStruct.FaultSignals;
using BEMN.MR600.New.Configuration.ConfigStruct.ILS;
using BEMN.MR600.New.Configuration.ConfigStruct.Indicators;
using BEMN.MR600.New.Configuration.ConfigStruct.Relay;
using BEMN.MR600.New.Configuration.ConfigStruct.VLS;

namespace BEMN.MR600.New.Configuration.ConfigStruct
{
    /// <summary>
    /// Вся конфигурация
    /// </summary>
    public class RamMemoryStruct : StructBase
    {
        [XmlElement(ElementName = "Версия")]
        public string DeviceVersion { get; set; }
        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }
        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType { get { return "МР600"; } set { } }


        [Layout(0)] private RomMeasuringStruct _romMeasuring;
        [Layout(1)] private KeysStruct _keys;
        [Layout(2)] private RomExternalSignalsStruct _romExternalSignals;
        [Layout(3)] private AllFaultSignalsStruct _faultSignal;
        [Layout(4)] private InputLogicParametresStruct _inputLogicParametres;
        [Layout(5)] private OutputAddedRelaysStruct _outputAddedRelays;
        [Layout(6)] private ResetStepStruct _resetStep;
        [Layout(7)] private AllSetpointStruct _allSetpoint;
        [Layout(8)] private AllOutputLogicSignalStruct _allOutputLogicSignal;
        [Layout(9)] private OutputRelayStruct _outputRelays;
        [Layout(10)] private AllIndicatorsStruct _allIndicators;
        [Layout(11)] private OscilloscopeConfigStruct _oscConfiguration;
        
        [BindingProperty(0)]
        public RomMeasuringStruct RomMeasuring
        {
            get { return _romMeasuring; }
            set { _romMeasuring = value; }
        }

        [BindingProperty(1)]
        public KeysStruct Keys
        {
            get { return _keys; }
            set { _keys = value; }
        }

        [BindingProperty(2)]
        public RomExternalSignalsStruct RomExternalSignals
        {
            get { return _romExternalSignals; }
            set { _romExternalSignals = value; }
        }
        [BindingProperty(3)]
        public AllFaultSignalsStruct AllFaultSignal
        {
            get { return _faultSignal; }
            set { _faultSignal = value; }
        }
        [BindingProperty(4)]
        public InputLogicParametresStruct InputLogicParametres
        {
            get { return _inputLogicParametres; }
            set { _inputLogicParametres = value; }
        }

        [BindingProperty(5)]
        public ResetStepStruct ResetStep
        {
            get { return _resetStep; }
            set { _resetStep = value; }
        }
        [BindingProperty(6)]
        public AllSetpointStruct AllSetpoint
        {
            get { return _allSetpoint; }
            set { _allSetpoint = value; }
        }
        [BindingProperty(7)]
        public AllOutputLogicSignalStruct AllOutputLogicSignal
        {
            get { return _allOutputLogicSignal; }
            set { _allOutputLogicSignal = value; }
        }
        [BindingProperty(8)]
        public AllRelaysStruct AllOutputRelays
        {
            get { return new AllRelaysStruct(_outputAddedRelays, _outputRelays); }
            set
            {
                _outputAddedRelays = value.AddRelays;
                _outputRelays = value.Relays;
            }
        }
        [BindingProperty(9)]
        public AllIndicatorsStruct AllIndicators
        {
            get { return _allIndicators; }
            set { _allIndicators = value; }
        }
        [BindingProperty(10)]
        public OscilloscopeConfigStruct OscConfiguration
        {
            get { return _oscConfiguration; }
            set { _oscConfiguration = value; }
        }
    }
}
