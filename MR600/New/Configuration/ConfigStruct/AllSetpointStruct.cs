﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.MR600.New.Configuration.ConfigStruct.External;
using BEMN.MR600.New.Configuration.ConfigStruct.Frequency;
using BEMN.MR600.New.Configuration.ConfigStruct.Voltage;

namespace BEMN.MR600.New.Configuration.ConfigStruct
{
    public class AllSetpointStruct : StructBase, ISetpointContainer<DefensesSetpointStruct>
    {
        [Layout(0)] private AllExternalDefensesStruct _extMain;
        [Layout(1)] private AllExternalDefensesStruct _extRes;
        [Layout(2)] private AllVoltageDefensesStruct _voltageMain;
        [Layout(3)] private AllVoltageDefensesStruct _voltageRes;
        [Layout(4)] private AllFrequencyDefensesStruct _frequencyMain;
        [Layout(5)] private AllFrequencyDefensesStruct _frequencyRes;

        /// <summary>
        /// Основная группа уставок
        /// </summary>
        //[Layout(0)]
        public DefensesSetpointStruct MainSetpoints
        {
            get
            {
                var ret = new DefensesSetpointStruct();
                ret.ExternalDefenses = _extMain;
                ret.VoltageDefenses = _voltageMain;
                ret.FrequencyDefenses = _frequencyMain;
                return ret;
            }
            set
            {
                _extMain = value.ExternalDefenses;
                _voltageMain = value.VoltageDefenses;
                _frequencyMain = value.FrequencyDefenses;
            }
        }

        /// <summary>
        /// Резервная группа уставок
        /// </summary>
        //[Layout(1)]
        public DefensesSetpointStruct ReserveSetpoints
        {
            get
            {
                var ret = new DefensesSetpointStruct();
                ret.ExternalDefenses = _extRes;
                ret.VoltageDefenses = _voltageRes;
                ret.FrequencyDefenses = _frequencyRes;
                return ret;
            }
            set
            {
                _extRes = value.ExternalDefenses;
                _voltageRes = value.VoltageDefenses;
                _frequencyRes = value.FrequencyDefenses;
            }
        }
    [XmlIgnore]
        public DefensesSetpointStruct[] Setpoints
        {
            get
            {
                return new[]
                    {
                        MainSetpoints.Clone<DefensesSetpointStruct>(),
                        ReserveSetpoints.Clone<DefensesSetpointStruct>()
                    };
            }
            set
            {
                MainSetpoints = value[0].Clone<DefensesSetpointStruct>();
                ReserveSetpoints = value[1].Clone<DefensesSetpointStruct>();
            }
        }
    }
}
