﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;
using BEMN.MR600.New.HelpClasses;

namespace BEMN.MR600.New.Configuration.ConfigStruct.ILS
{
    public class LogicSignalStruct : StructBase, IXmlSerializable
    {
        public const int DISCRETS_COUNT = 8;

        #region [Private fields]
        [Layout(0)]
        ushort _a1;
        [Layout(1)]
        ushort _a2;

        #endregion [Private fields]

        [BindingProperty(0)]
        [XmlIgnore]
        public string this[int ls]
        {
            get
            {
                ushort val;
                if (ls < 8)
                {
                    val = this._a1;
                }
                else
                {
                    val = this._a2;
                    ls = ls - 8;
                }
                if (Common.GetBit(val, ls))
                {
                    if (Common.GetBit(val, ls + 8))
                    {
                        return StringsV2.LogicState[2];
                    }
                    else
                    {
                        return StringsV2.LogicState[1];
                    }
                }
                return StringsV2.LogicState[0];
            }
            set
            {
                if (ls < 8)
                {
                    if (value == StringsV2.LogicState[0])
                    {
                        this._a1 = Common.SetBit(this._a1, ls, false);
                        this._a1 = Common.SetBit(this._a1, ls + 8, false);
                    }
                    if (value == StringsV2.LogicState[1])
                    {
                        this._a1 = Common.SetBit(this._a1, ls, true);
                        this._a1 = Common.SetBit(this._a1, ls + 8, false);
                    }
                    if (value == StringsV2.LogicState[2])
                    {
                        this._a1 = Common.SetBit(this._a1, ls, true);
                        this._a1 = Common.SetBit(this._a1, ls + 8, true);
                    }
                }
                else
                {
                    ls = ls - 8;
                    if (value == StringsV2.LogicState[0])
                    {
                        this._a2 = Common.SetBit(this._a2, ls, false);
                        this._a2 = Common.SetBit(this._a2, ls + 8, false);
                    }
                    if (value == StringsV2.LogicState[1])
                    {
                        this._a2 = Common.SetBit(this._a2, ls, true);
                        this._a2 = Common.SetBit(this._a2, ls + 8, false);
                    }
                    if (value == StringsV2.LogicState[2])
                    {
                        this._a2 = Common.SetBit(this._a2, ls, true);
                        this._a2 = Common.SetBit(this._a2, ls + 8, true);
                    }
                }

            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {

        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {

            for (int i = 0; i < DISCRETS_COUNT; i++)
            {
                writer.WriteElementString("Дискрет", this[i]);
            }
        }
    }
}