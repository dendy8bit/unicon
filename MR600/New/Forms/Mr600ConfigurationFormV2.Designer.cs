﻿namespace BEMN.MR600.New.Forms
{
    partial class Mr600ConfigurationFormV2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this._processLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._exchangeProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._defendingTabPage = new System.Windows.Forms.TabPage();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._mainRadioBtnGroup = new System.Windows.Forms.RadioButton();
            this._reserveRadioBtnGroup = new System.Windows.Forms.RadioButton();
            this._btnGroupChange = new System.Windows.Forms.Button();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._TabControlDefending = new System.Windows.Forms.TabControl();
            this._frequenceTanPage = new System.Windows.Forms.TabPage();
            this._frequenceDefensesGrid = new System.Windows.Forms.DataGridView();
            this._frequenceDefensesReset = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesOSCv11 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesConstraintAPV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesReturnTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesReturn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesWorkTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesWorkConstraint = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesBlockingNumber = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesMode = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesTabPage = new System.Windows.Forms.TabPage();
            this._voltageDefensesGrid1 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesResetCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesOSCv11Col1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesReturnTimeCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnConstraintCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesWorkTimeCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesWorkConstraintCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesParameterCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesBlockingNumberCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesModeCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesNameCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._blockDDL = new System.Windows.Forms.GroupBox();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this._U_1_CB = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._U_2_CB = new System.Windows.Forms.ComboBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this._U_3_CB = new System.Windows.Forms.ComboBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this._U_4_CB = new System.Windows.Forms.ComboBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this._U_1_1_CB = new System.Windows.Forms.ComboBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this._U_1_2_CB = new System.Windows.Forms.ComboBox();
            this._externalTabPage = new System.Windows.Forms.TabPage();
            this._externalDefenseGrid = new System.Windows.Forms.DataGridView();
            this._resetExDefenseColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._oscExDefenseColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefReturnTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefReturnNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefReturnCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefWorkingTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefWorkingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefBlockingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._extDefNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this._releImpulseCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._outputIndicatorsGrid = new System.Windows.Forms.DataGridView();
            this._outIndJA_Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndJS_Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._outputLogicCombo = new System.Windows.Forms.ComboBox();
            this._outputLogicAcceptBut = new System.Windows.Forms.Button();
            this._outputLogicCheckList = new System.Windows.Forms.CheckedListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._dispepairCheckList = new System.Windows.Forms.CheckedListBox();
            this._outputSignalsDispepairTimeBox = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this._inSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._TN_Box = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this._TNNP_Box = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this._defectTN_CB = new System.Windows.Forms.ComboBox();
            this._defectTNNP_CB = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._logicSignalsDataGrid = new System.Windows.Forms.DataGridView();
            this._diskretValueCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._diskretChannelCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._logicChannelsCombo = new System.Windows.Forms.ComboBox();
            this._applyLogicSignalsBut = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._inputSignalsConstraintResetCombo = new System.Windows.Forms.ComboBox();
            this._inputSignalsIndicationResetCombo = new System.Windows.Forms.ComboBox();
            this._inputSignalsTN_DispepairCombo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._oscLength = new System.Windows.Forms.ComboBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._oscWriteLength = new System.Windows.Forms.MaskedTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this._keysDataGrid = new System.Windows.Forms.DataGridView();
            this._keyValue = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._applyKeys = new System.Windows.Forms.Button();
            this._tabControl = new System.Windows.Forms.TabControl();
            this._statusStrip.SuspendLayout();
            this._defendingTabPage.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this._TabControlDefending.SuspendLayout();
            this._frequenceTanPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._frequenceDefensesGrid)).BeginInit();
            this._voltageDefensesTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this._blockDDL.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this._externalTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).BeginInit();
            this._outputSignalsPage.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).BeginInit();
            this.groupBox10.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this._inSignalsPage.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._keysDataGrid)).BeginInit();
            this._tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // _statusStrip
            // 
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._processLabel,
            this._exchangeProgressBar,
            this._statusLabel});
            this._statusStrip.Location = new System.Drawing.Point(0, 727);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this._statusStrip.Size = new System.Drawing.Size(938, 22);
            this._statusStrip.TabIndex = 10;
            this._statusStrip.Text = "statusStrip1";
            // 
            // _processLabel
            // 
            this._processLabel.Name = "_processLabel";
            this._processLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _exchangeProgressBar
            // 
            this._exchangeProgressBar.Maximum = 10;
            this._exchangeProgressBar.Name = "_exchangeProgressBar";
            this._exchangeProgressBar.Size = new System.Drawing.Size(100, 16);
            this._exchangeProgressBar.Step = 1;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(16, 17);
            this._statusLabel.Text = "...";
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveConfigBut.Location = new System.Drawing.Point(496, 698);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(134, 23);
            this._saveConfigBut.TabIndex = 9;
            this._saveConfigBut.Text = "Сохранить в файл";
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._loadConfigBut.Location = new System.Drawing.Point(359, 698);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(131, 23);
            this._loadConfigBut.TabIndex = 8;
            this._loadConfigBut.Text = "Загрузить из файла";
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeConfigBut.Location = new System.Drawing.Point(157, 698);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(172, 23);
            this._writeConfigBut.TabIndex = 7;
            this._writeConfigBut.Text = "Записать в устройство";
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readConfigBut.Location = new System.Drawing.Point(4, 698);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(147, 23);
            this._readConfigBut.TabIndex = 6;
            this._readConfigBut.Text = "Прочитать из устройства";
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.FileName = "Уставки МР 600";
            this._saveConfigurationDlg.Filter = "Файл уставок МР 600 (*.xml)|*.xml";
            this._saveConfigurationDlg.Title = "Сохранение файла уставок МР 600";
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.FileName = "Уставки МР 600";
            this._openConfigurationDlg.Filter = "Файл уставок МР 600 (*.xml)|*.xml";
            this._openConfigurationDlg.Title = "Загрузка файла уставок МР 600";
            // 
            // _defendingTabPage
            // 
            this._defendingTabPage.Controls.Add(this.groupBox21);
            this._defendingTabPage.Controls.Add(this.groupBox18);
            this._defendingTabPage.Location = new System.Drawing.Point(4, 22);
            this._defendingTabPage.Name = "_defendingTabPage";
            this._defendingTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._defendingTabPage.Size = new System.Drawing.Size(930, 704);
            this._defendingTabPage.TabIndex = 8;
            this._defendingTabPage.Text = "Защиты";
            this._defendingTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this._btnGroupChange);
            this.groupBox18.Controls.Add(this._reserveRadioBtnGroup);
            this.groupBox18.Controls.Add(this._mainRadioBtnGroup);
            this.groupBox18.Location = new System.Drawing.Point(8, 6);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(433, 57);
            this.groupBox18.TabIndex = 0;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Группа уставок";
            // 
            // _mainRadioBtnGroup
            // 
            this._mainRadioBtnGroup.AutoSize = true;
            this._mainRadioBtnGroup.Checked = true;
            this._mainRadioBtnGroup.Location = new System.Drawing.Point(16, 20);
            this._mainRadioBtnGroup.Name = "_mainRadioBtnGroup";
            this._mainRadioBtnGroup.Size = new System.Drawing.Size(75, 17);
            this._mainRadioBtnGroup.TabIndex = 0;
            this._mainRadioBtnGroup.TabStop = true;
            this._mainRadioBtnGroup.Text = "Основная";
            this._mainRadioBtnGroup.UseVisualStyleBackColor = true;
            this._mainRadioBtnGroup.CheckedChanged += new System.EventHandler(this._mainRadioBtnGroup_CheckedChanged);
            // 
            // _reserveRadioBtnGroup
            // 
            this._reserveRadioBtnGroup.AutoSize = true;
            this._reserveRadioBtnGroup.Location = new System.Drawing.Point(112, 20);
            this._reserveRadioBtnGroup.Name = "_reserveRadioBtnGroup";
            this._reserveRadioBtnGroup.Size = new System.Drawing.Size(80, 17);
            this._reserveRadioBtnGroup.TabIndex = 1;
            this._reserveRadioBtnGroup.Text = "Резервная";
            this._reserveRadioBtnGroup.UseVisualStyleBackColor = true;
            // 
            // _btnGroupChange
            // 
            this._btnGroupChange.Location = new System.Drawing.Point(250, 20);
            this._btnGroupChange.Name = "_btnGroupChange";
            this._btnGroupChange.Size = new System.Drawing.Size(177, 23);
            this._btnGroupChange.TabIndex = 2;
            this._btnGroupChange.Text = "Основные --> Резервные";
            this._btnGroupChange.UseVisualStyleBackColor = true;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._TabControlDefending);
            this.groupBox21.Location = new System.Drawing.Point(9, 69);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(913, 601);
            this.groupBox21.TabIndex = 1;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Защиты";
            // 
            // _TabControlDefending
            // 
            this._TabControlDefending.Controls.Add(this._externalTabPage);
            this._TabControlDefending.Controls.Add(this._voltageDefensesTabPage);
            this._TabControlDefending.Controls.Add(this._frequenceTanPage);
            this._TabControlDefending.Location = new System.Drawing.Point(15, 20);
            this._TabControlDefending.Name = "_TabControlDefending";
            this._TabControlDefending.SelectedIndex = 0;
            this._TabControlDefending.Size = new System.Drawing.Size(894, 581);
            this._TabControlDefending.TabIndex = 0;
            // 
            // _frequenceTanPage
            // 
            this._frequenceTanPage.Controls.Add(this._frequenceDefensesGrid);
            this._frequenceTanPage.Location = new System.Drawing.Point(4, 22);
            this._frequenceTanPage.Name = "_frequenceTanPage";
            this._frequenceTanPage.Padding = new System.Windows.Forms.Padding(3);
            this._frequenceTanPage.Size = new System.Drawing.Size(886, 555);
            this._frequenceTanPage.TabIndex = 2;
            this._frequenceTanPage.Text = "Защиты по частоте";
            this._frequenceTanPage.UseVisualStyleBackColor = true;
            // 
            // _frequenceDefensesGrid
            // 
            this._frequenceDefensesGrid.AllowUserToAddRows = false;
            this._frequenceDefensesGrid.AllowUserToDeleteRows = false;
            this._frequenceDefensesGrid.AllowUserToResizeColumns = false;
            this._frequenceDefensesGrid.AllowUserToResizeRows = false;
            this._frequenceDefensesGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._frequenceDefensesGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this._frequenceDefensesGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._frequenceDefensesGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this._frequenceDefensesGrid.ColumnHeadersHeight = 30;
            this._frequenceDefensesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._frequenceDefensesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._frequenceDefensesName,
            this._frequenceDefensesMode,
            this._frequenceDefensesBlockingNumber,
            this._frequenceDefensesWorkConstraint,
            this._frequenceDefensesWorkTime,
            this._frequenceDefensesReturn,
            this._frequenceDefensesReturnTime,
            this._frequenceDefensesConstraintAPV,
            this._frequenceDefensesOSCv11,
            this._frequenceDefensesReset});
            this._frequenceDefensesGrid.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesGrid.DefaultCellStyle = dataGridViewCellStyle32;
            this._frequenceDefensesGrid.Location = new System.Drawing.Point(21, 16);
            this._frequenceDefensesGrid.Name = "_frequenceDefensesGrid";
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._frequenceDefensesGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle33;
            this._frequenceDefensesGrid.RowHeadersVisible = false;
            this._frequenceDefensesGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._frequenceDefensesGrid.RowTemplate.Height = 24;
            this._frequenceDefensesGrid.Size = new System.Drawing.Size(832, 248);
            this._frequenceDefensesGrid.TabIndex = 7;
            // 
            // _frequenceDefensesReset
            // 
            this._frequenceDefensesReset.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._frequenceDefensesReset.HeaderText = "Сброс";
            this._frequenceDefensesReset.Name = "_frequenceDefensesReset";
            this._frequenceDefensesReset.Width = 44;
            // 
            // _frequenceDefensesOSCv11
            // 
            this._frequenceDefensesOSCv11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._frequenceDefensesOSCv11.HeaderText = "Осциллограф";
            this._frequenceDefensesOSCv11.Name = "_frequenceDefensesOSCv11";
            this._frequenceDefensesOSCv11.Width = 82;
            // 
            // _frequenceDefensesConstraintAPV
            // 
            this._frequenceDefensesConstraintAPV.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesConstraintAPV.DefaultCellStyle = dataGridViewCellStyle31;
            this._frequenceDefensesConstraintAPV.HeaderText = "Уставка ВЗ, Гц";
            this._frequenceDefensesConstraintAPV.Name = "_frequenceDefensesConstraintAPV";
            this._frequenceDefensesConstraintAPV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesConstraintAPV.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesConstraintAPV.Width = 91;
            // 
            // _frequenceDefensesReturnTime
            // 
            this._frequenceDefensesReturnTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesReturnTime.DefaultCellStyle = dataGridViewCellStyle30;
            this._frequenceDefensesReturnTime.HeaderText = "Время возврата, мс";
            this._frequenceDefensesReturnTime.Name = "_frequenceDefensesReturnTime";
            this._frequenceDefensesReturnTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesReturnTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesReturnTime.Width = 116;
            // 
            // _frequenceDefensesReturn
            // 
            this._frequenceDefensesReturn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._frequenceDefensesReturn.HeaderText = "Возв.";
            this._frequenceDefensesReturn.Name = "_frequenceDefensesReturn";
            this._frequenceDefensesReturn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesReturn.Width = 41;
            // 
            // _frequenceDefensesWorkTime
            // 
            this._frequenceDefensesWorkTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle29.NullValue = "0";
            this._frequenceDefensesWorkTime.DefaultCellStyle = dataGridViewCellStyle29;
            this._frequenceDefensesWorkTime.HeaderText = "Время сраб., мс";
            this._frequenceDefensesWorkTime.MaxInputLength = 8;
            this._frequenceDefensesWorkTime.Name = "_frequenceDefensesWorkTime";
            this._frequenceDefensesWorkTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesWorkTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesWorkTime.Width = 96;
            // 
            // _frequenceDefensesWorkConstraint
            // 
            this._frequenceDefensesWorkConstraint.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesWorkConstraint.DefaultCellStyle = dataGridViewCellStyle28;
            this._frequenceDefensesWorkConstraint.HeaderText = "Уставка сраб., Гц";
            this._frequenceDefensesWorkConstraint.MaxInputLength = 8;
            this._frequenceDefensesWorkConstraint.Name = "_frequenceDefensesWorkConstraint";
            this._frequenceDefensesWorkConstraint.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesWorkConstraint.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesWorkConstraint.Width = 104;
            // 
            // _frequenceDefensesBlockingNumber
            // 
            this._frequenceDefensesBlockingNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._frequenceDefensesBlockingNumber.HeaderText = "Блокировка";
            this._frequenceDefensesBlockingNumber.Name = "_frequenceDefensesBlockingNumber";
            this._frequenceDefensesBlockingNumber.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesBlockingNumber.Width = 74;
            // 
            // _frequenceDefensesMode
            // 
            this._frequenceDefensesMode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._frequenceDefensesMode.HeaderText = "Режим";
            this._frequenceDefensesMode.Name = "_frequenceDefensesMode";
            this._frequenceDefensesMode.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesMode.Width = 48;
            // 
            // _frequenceDefensesName
            // 
            this._frequenceDefensesName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._frequenceDefensesName.DefaultCellStyle = dataGridViewCellStyle27;
            this._frequenceDefensesName.HeaderText = "";
            this._frequenceDefensesName.MaxInputLength = 1;
            this._frequenceDefensesName.Name = "_frequenceDefensesName";
            this._frequenceDefensesName.ReadOnly = true;
            this._frequenceDefensesName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesName.Width = 45;
            // 
            // _voltageDefensesTabPage
            // 
            this._voltageDefensesTabPage.Controls.Add(this._blockDDL);
            this._voltageDefensesTabPage.Controls.Add(this.dataGridView2);
            this._voltageDefensesTabPage.Controls.Add(this.dataGridView1);
            this._voltageDefensesTabPage.Controls.Add(this._voltageDefensesGrid1);
            this._voltageDefensesTabPage.Location = new System.Drawing.Point(4, 22);
            this._voltageDefensesTabPage.Name = "_voltageDefensesTabPage";
            this._voltageDefensesTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._voltageDefensesTabPage.Size = new System.Drawing.Size(886, 555);
            this._voltageDefensesTabPage.TabIndex = 1;
            this._voltageDefensesTabPage.Text = "Защиты по напряжению";
            this._voltageDefensesTabPage.UseVisualStyleBackColor = true;
            // 
            // _voltageDefensesGrid1
            // 
            this._voltageDefensesGrid1.AllowUserToAddRows = false;
            this._voltageDefensesGrid1.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid1.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid1.AllowUserToResizeRows = false;
            this._voltageDefensesGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._voltageDefensesGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._voltageDefensesGrid1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this._voltageDefensesGrid1.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol1,
            this._voltageDefensesModeCol1,
            this._voltageDefensesBlockingNumberCol1,
            this._voltageDefensesParameterCol1,
            this._voltageDefensesWorkConstraintCol1,
            this._voltageDefensesWorkTimeCol1,
            this._voltageDefensesReturnCol1,
            this._voltageDefensesReturnConstraintCol1,
            this._voltageDefensesReturnTimeCol1,
            this._voltageDefensesOSCv11Col1,
            this._voltageDefensesResetCol1});
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesGrid1.DefaultCellStyle = dataGridViewCellStyle23;
            this._voltageDefensesGrid1.Location = new System.Drawing.Point(37, 89);
            this._voltageDefensesGrid1.Name = "_voltageDefensesGrid1";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this._voltageDefensesGrid1.RowHeadersVisible = false;
            this._voltageDefensesGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid1.RowsDefaultCellStyle = dataGridViewCellStyle25;
            this._voltageDefensesGrid1.RowTemplate.Height = 24;
            this._voltageDefensesGrid1.Size = new System.Drawing.Size(830, 149);
            this._voltageDefensesGrid1.TabIndex = 14;
            // 
            // _voltageDefensesResetCol1
            // 
            this._voltageDefensesResetCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._voltageDefensesResetCol1.HeaderText = "Сброс";
            this._voltageDefensesResetCol1.Name = "_voltageDefensesResetCol1";
            this._voltageDefensesResetCol1.Width = 44;
            // 
            // _voltageDefensesOSCv11Col1
            // 
            this._voltageDefensesOSCv11Col1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._voltageDefensesOSCv11Col1.HeaderText = "Осциллограф";
            this._voltageDefensesOSCv11Col1.Name = "_voltageDefensesOSCv11Col1";
            this._voltageDefensesOSCv11Col1.Width = 82;
            // 
            // _voltageDefensesReturnTimeCol1
            // 
            this._voltageDefensesReturnTimeCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._voltageDefensesReturnTimeCol1.HeaderText = "Время возврата, мс";
            this._voltageDefensesReturnTimeCol1.Name = "_voltageDefensesReturnTimeCol1";
            this._voltageDefensesReturnTimeCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnTimeCol1.Width = 116;
            // 
            // _voltageDefensesReturnConstraintCol1
            // 
            this._voltageDefensesReturnConstraintCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._voltageDefensesReturnConstraintCol1.HeaderText = "Уставка возврата, В";
            this._voltageDefensesReturnConstraintCol1.Name = "_voltageDefensesReturnConstraintCol1";
            this._voltageDefensesReturnConstraintCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnConstraintCol1.Width = 119;
            // 
            // _voltageDefensesReturnCol1
            // 
            this._voltageDefensesReturnCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._voltageDefensesReturnCol1.HeaderText = "Возв.";
            this._voltageDefensesReturnCol1.Name = "_voltageDefensesReturnCol1";
            this._voltageDefensesReturnCol1.Width = 41;
            // 
            // _voltageDefensesWorkTimeCol1
            // 
            this._voltageDefensesWorkTimeCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle22.NullValue = "0";
            this._voltageDefensesWorkTimeCol1.DefaultCellStyle = dataGridViewCellStyle22;
            this._voltageDefensesWorkTimeCol1.HeaderText = "Время сраб., мс";
            this._voltageDefensesWorkTimeCol1.Name = "_voltageDefensesWorkTimeCol1";
            this._voltageDefensesWorkTimeCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkTimeCol1.Width = 96;
            // 
            // _voltageDefensesWorkConstraintCol1
            // 
            this._voltageDefensesWorkConstraintCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._voltageDefensesWorkConstraintCol1.HeaderText = "Уставка сраб., В";
            this._voltageDefensesWorkConstraintCol1.Name = "_voltageDefensesWorkConstraintCol1";
            this._voltageDefensesWorkConstraintCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkConstraintCol1.Width = 99;
            // 
            // _voltageDefensesParameterCol1
            // 
            this._voltageDefensesParameterCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesParameterCol1.DefaultCellStyle = dataGridViewCellStyle21;
            this._voltageDefensesParameterCol1.HeaderText = "Параметр";
            this._voltageDefensesParameterCol1.Name = "_voltageDefensesParameterCol1";
            this._voltageDefensesParameterCol1.Width = 64;
            // 
            // _voltageDefensesBlockingNumberCol1
            // 
            this._voltageDefensesBlockingNumberCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._voltageDefensesBlockingNumberCol1.HeaderText = "Блокировка";
            this._voltageDefensesBlockingNumberCol1.Name = "_voltageDefensesBlockingNumberCol1";
            this._voltageDefensesBlockingNumberCol1.Width = 74;
            // 
            // _voltageDefensesModeCol1
            // 
            this._voltageDefensesModeCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._voltageDefensesModeCol1.HeaderText = "Режим";
            this._voltageDefensesModeCol1.Name = "_voltageDefensesModeCol1";
            this._voltageDefensesModeCol1.Width = 48;
            // 
            // _voltageDefensesNameCol1
            // 
            this._voltageDefensesNameCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesNameCol1.DefaultCellStyle = dataGridViewCellStyle20;
            this._voltageDefensesNameCol1.HeaderText = "";
            this._voltageDefensesNameCol1.Name = "_voltageDefensesNameCol1";
            this._voltageDefensesNameCol1.ReadOnly = true;
            this._voltageDefensesNameCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol1.Width = 5;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridView1.ColumnHeadersHeight = 30;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewComboBoxColumn1,
            this.dataGridViewComboBoxColumn2,
            this.dataGridViewComboBoxColumn3,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewComboBoxColumn4,
            this.dataGridViewCheckBoxColumn2});
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridView1.Location = new System.Drawing.Point(37, 244);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(830, 149);
            this.dataGridView1.TabIndex = 15;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewCheckBoxColumn2.HeaderText = "Сброс";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Width = 44;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn4.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Width = 82;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn5.HeaderText = "Время возврата, мс";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Width = 116;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn4.HeaderText = "Уставка возврата, В";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 119;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewCheckBoxColumn1.HeaderText = "Возв.";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 41;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle15.NullValue = "0";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn3.HeaderText = "Время сраб., мс";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 96;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn2.HeaderText = "Уставка сраб., В";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 99;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewComboBoxColumn3.HeaderText = "Параметр";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Width = 64;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn2.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Width = 74;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn1.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Width = 48;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn1.HeaderText = "";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 5;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView2.ColumnHeadersHeight = 30;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewComboBoxColumn5,
            this.dataGridViewComboBoxColumn6,
            this.dataGridViewComboBoxColumn7,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewCheckBoxColumn3,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewComboBoxColumn8,
            this.dataGridViewCheckBoxColumn4});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridView2.Location = new System.Drawing.Point(37, 399);
            this.dataGridView2.Name = "dataGridView2";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridView2.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(830, 149);
            this.dataGridView2.TabIndex = 16;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewCheckBoxColumn4.HeaderText = "Сброс";
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.Width = 44;
            // 
            // dataGridViewComboBoxColumn8
            // 
            this.dataGridViewComboBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn8.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn8.Name = "dataGridViewComboBoxColumn8";
            this.dataGridViewComboBoxColumn8.Width = 82;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn10.HeaderText = "Время возврата, мс";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Width = 116;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn9.HeaderText = "Уставка возврата, В";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Width = 119;
            // 
            // dataGridViewCheckBoxColumn3
            // 
            this.dataGridViewCheckBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewCheckBoxColumn3.HeaderText = "Возв.";
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            this.dataGridViewCheckBoxColumn3.Width = 41;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle8.NullValue = "0";
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn8.HeaderText = "Время сраб., мс";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Width = 96;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn7.HeaderText = "Уставка сраб., В";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Width = 99;
            // 
            // dataGridViewComboBoxColumn7
            // 
            this.dataGridViewComboBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.dataGridViewComboBoxColumn7.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewComboBoxColumn7.HeaderText = "Параметр";
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            this.dataGridViewComboBoxColumn7.ReadOnly = true;
            this.dataGridViewComboBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewComboBoxColumn7.Width = 64;
            // 
            // dataGridViewComboBoxColumn6
            // 
            this.dataGridViewComboBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn6.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Width = 74;
            // 
            // dataGridViewComboBoxColumn5
            // 
            this.dataGridViewComboBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn5.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Width = 48;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn6.HeaderText = "";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Width = 5;
            // 
            // _blockDDL
            // 
            this._blockDDL.Controls.Add(this.groupBox16);
            this._blockDDL.Controls.Add(this.groupBox15);
            this._blockDDL.Controls.Add(this.groupBox14);
            this._blockDDL.Controls.Add(this.groupBox13);
            this._blockDDL.Controls.Add(this.groupBox6);
            this._blockDDL.Controls.Add(this.groupBox25);
            this._blockDDL.Location = new System.Drawing.Point(37, 6);
            this._blockDDL.Name = "_blockDDL";
            this._blockDDL.Size = new System.Drawing.Size(696, 77);
            this._blockDDL.TabIndex = 17;
            this._blockDDL.TabStop = false;
            this._blockDDL.Text = "Блокировка по U< 5В";
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this._U_1_CB);
            this.groupBox25.Location = new System.Drawing.Point(6, 15);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(106, 46);
            this.groupBox25.TabIndex = 0;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "U<";
            // 
            // _U_1_CB
            // 
            this._U_1_CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._U_1_CB.Items.AddRange(new object[] {
            "Введено",
            "Выведено"});
            this._U_1_CB.Location = new System.Drawing.Point(6, 15);
            this._U_1_CB.Name = "_U_1_CB";
            this._U_1_CB.Size = new System.Drawing.Size(94, 21);
            this._U_1_CB.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._U_2_CB);
            this.groupBox6.Location = new System.Drawing.Point(118, 15);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(106, 46);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "U<<";
            // 
            // _U_2_CB
            // 
            this._U_2_CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._U_2_CB.Items.AddRange(new object[] {
            "Введено",
            "Выведено"});
            this._U_2_CB.Location = new System.Drawing.Point(6, 15);
            this._U_2_CB.Name = "_U_2_CB";
            this._U_2_CB.Size = new System.Drawing.Size(94, 21);
            this._U_2_CB.TabIndex = 0;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this._U_3_CB);
            this.groupBox13.Location = new System.Drawing.Point(230, 15);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(106, 46);
            this.groupBox13.TabIndex = 2;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "U<<<";
            // 
            // _U_3_CB
            // 
            this._U_3_CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._U_3_CB.Items.AddRange(new object[] {
            "Введено",
            "Выведено"});
            this._U_3_CB.Location = new System.Drawing.Point(6, 15);
            this._U_3_CB.Name = "_U_3_CB";
            this._U_3_CB.Size = new System.Drawing.Size(94, 21);
            this._U_3_CB.TabIndex = 0;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this._U_4_CB);
            this.groupBox14.Location = new System.Drawing.Point(342, 15);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(106, 46);
            this.groupBox14.TabIndex = 1;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "U<<<<";
            // 
            // _U_4_CB
            // 
            this._U_4_CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._U_4_CB.Items.AddRange(new object[] {
            "Введено",
            "Выведено"});
            this._U_4_CB.Location = new System.Drawing.Point(6, 15);
            this._U_4_CB.Name = "_U_4_CB";
            this._U_4_CB.Size = new System.Drawing.Size(94, 21);
            this._U_4_CB.TabIndex = 0;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this._U_1_1_CB);
            this.groupBox15.Location = new System.Drawing.Point(567, 15);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(106, 46);
            this.groupBox15.TabIndex = 3;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "U1<";
            // 
            // _U_1_1_CB
            // 
            this._U_1_1_CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._U_1_1_CB.Items.AddRange(new object[] {
            "Введено",
            "Выведено"});
            this._U_1_1_CB.Location = new System.Drawing.Point(6, 15);
            this._U_1_1_CB.Name = "_U_1_1_CB";
            this._U_1_1_CB.Size = new System.Drawing.Size(94, 21);
            this._U_1_1_CB.TabIndex = 0;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this._U_1_2_CB);
            this.groupBox16.Location = new System.Drawing.Point(455, 15);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(106, 46);
            this.groupBox16.TabIndex = 4;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "U1<<";
            // 
            // _U_1_2_CB
            // 
            this._U_1_2_CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._U_1_2_CB.Items.AddRange(new object[] {
            "Введено",
            "Выведено"});
            this._U_1_2_CB.Location = new System.Drawing.Point(6, 15);
            this._U_1_2_CB.Name = "_U_1_2_CB";
            this._U_1_2_CB.Size = new System.Drawing.Size(94, 21);
            this._U_1_2_CB.TabIndex = 0;
            // 
            // _externalTabPage
            // 
            this._externalTabPage.Controls.Add(this._externalDefenseGrid);
            this._externalTabPage.Location = new System.Drawing.Point(4, 22);
            this._externalTabPage.Name = "_externalTabPage";
            this._externalTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._externalTabPage.Size = new System.Drawing.Size(886, 555);
            this._externalTabPage.TabIndex = 0;
            this._externalTabPage.Text = "Внешние защиты";
            this._externalTabPage.UseVisualStyleBackColor = true;
            // 
            // _externalDefenseGrid
            // 
            this._externalDefenseGrid.AllowUserToAddRows = false;
            this._externalDefenseGrid.AllowUserToDeleteRows = false;
            this._externalDefenseGrid.AllowUserToResizeColumns = false;
            this._externalDefenseGrid.AllowUserToResizeRows = false;
            this._externalDefenseGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._externalDefenseGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._externalDefenseGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._externalDefenseGrid.ColumnHeadersHeight = 30;
            this._externalDefenseGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._externalDefenseGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extDefNumberCol,
            this._exDefModeCol,
            this._exDefBlockingCol,
            this._exDefWorkingCol,
            this._exDefWorkingTimeCol,
            this._exDefReturnCol,
            this._exDefReturnNumberCol,
            this._exDefReturnTimeCol,
            this._oscExDefenseColumn,
            this._resetExDefenseColumn});
            this._externalDefenseGrid.Location = new System.Drawing.Point(6, 26);
            this._externalDefenseGrid.Name = "_externalDefenseGrid";
            this._externalDefenseGrid.RowHeadersVisible = false;
            this._externalDefenseGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDefenseGrid.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this._externalDefenseGrid.RowTemplate.Height = 24;
            this._externalDefenseGrid.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._externalDefenseGrid.Size = new System.Drawing.Size(829, 247);
            this._externalDefenseGrid.TabIndex = 1;
            // 
            // _resetExDefenseColumn
            // 
            this._resetExDefenseColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.NullValue = false;
            this._resetExDefenseColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this._resetExDefenseColumn.HeaderText = "Сброс";
            this._resetExDefenseColumn.MinimumWidth = 50;
            this._resetExDefenseColumn.Name = "_resetExDefenseColumn";
            this._resetExDefenseColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._resetExDefenseColumn.Width = 50;
            // 
            // _oscExDefenseColumn
            // 
            this._oscExDefenseColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._oscExDefenseColumn.HeaderText = "Осц.";
            this._oscExDefenseColumn.MinimumWidth = 50;
            this._oscExDefenseColumn.Name = "_oscExDefenseColumn";
            this._oscExDefenseColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._oscExDefenseColumn.Width = 50;
            // 
            // _exDefReturnTimeCol
            // 
            this._exDefReturnTimeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._exDefReturnTimeCol.HeaderText = "Время возврата, мс";
            this._exDefReturnTimeCol.MinimumWidth = 50;
            this._exDefReturnTimeCol.Name = "_exDefReturnTimeCol";
            this._exDefReturnTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._exDefReturnTimeCol.Width = 116;
            // 
            // _exDefReturnNumberCol
            // 
            this._exDefReturnNumberCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._exDefReturnNumberCol.HeaderText = "Вход возврата";
            this._exDefReturnNumberCol.MinimumWidth = 50;
            this._exDefReturnNumberCol.Name = "_exDefReturnNumberCol";
            this._exDefReturnNumberCol.Width = 87;
            // 
            // _exDefReturnCol
            // 
            this._exDefReturnCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._exDefReturnCol.HeaderText = "Возв.";
            this._exDefReturnCol.MinimumWidth = 50;
            this._exDefReturnCol.Name = "_exDefReturnCol";
            this._exDefReturnCol.Width = 50;
            // 
            // _exDefWorkingTimeCol
            // 
            this._exDefWorkingTimeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._exDefWorkingTimeCol.HeaderText = "Время срабат., мс";
            this._exDefWorkingTimeCol.MinimumWidth = 50;
            this._exDefWorkingTimeCol.Name = "_exDefWorkingTimeCol";
            this._exDefWorkingTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._exDefWorkingTimeCol.Width = 107;
            // 
            // _exDefWorkingCol
            // 
            this._exDefWorkingCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._exDefWorkingCol.DefaultCellStyle = dataGridViewCellStyle2;
            this._exDefWorkingCol.HeaderText = "Срабатывание";
            this._exDefWorkingCol.MinimumWidth = 50;
            this._exDefWorkingCol.Name = "_exDefWorkingCol";
            this._exDefWorkingCol.Width = 87;
            // 
            // _exDefBlockingCol
            // 
            this._exDefBlockingCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._exDefBlockingCol.HeaderText = "Блокировка";
            this._exDefBlockingCol.MinimumWidth = 50;
            this._exDefBlockingCol.Name = "_exDefBlockingCol";
            this._exDefBlockingCol.Width = 74;
            // 
            // _exDefModeCol
            // 
            this._exDefModeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._exDefModeCol.HeaderText = "Режим";
            this._exDefModeCol.MinimumWidth = 50;
            this._exDefModeCol.Name = "_exDefModeCol";
            this._exDefModeCol.Width = 50;
            // 
            // _extDefNumberCol
            // 
            this._extDefNumberCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._extDefNumberCol.DefaultCellStyle = dataGridViewCellStyle1;
            this._extDefNumberCol.HeaderText = "№";
            this._extDefNumberCol.MinimumWidth = 50;
            this._extDefNumberCol.Name = "_extDefNumberCol";
            this._extDefNumberCol.ReadOnly = true;
            this._extDefNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._extDefNumberCol.Width = 50;
            // 
            // _outputSignalsPage
            // 
            this._outputSignalsPage.Controls.Add(this.groupBox3);
            this._outputSignalsPage.Controls.Add(this.groupBox10);
            this._outputSignalsPage.Controls.Add(this.groupBox9);
            this._outputSignalsPage.Controls.Add(this.groupBox8);
            this._outputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._outputSignalsPage.Name = "_outputSignalsPage";
            this._outputSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._outputSignalsPage.Size = new System.Drawing.Size(930, 704);
            this._outputSignalsPage.TabIndex = 1;
            this._outputSignalsPage.Text = "Выходные сигналы";
            this._outputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._outputReleGrid);
            this.groupBox8.Location = new System.Drawing.Point(8, 5);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(441, 277);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Выходные реле";
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this._releSignalCol,
            this._releImpulseCol});
            this._outputReleGrid.Location = new System.Drawing.Point(9, 13);
            this._outputReleGrid.Name = "_outputReleGrid";
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle35;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(418, 258);
            this._outputReleGrid.TabIndex = 0;
            this._outputReleGrid.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this._outputReleGrid_CellValidating);
            // 
            // _releImpulseCol
            // 
            this._releImpulseCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._releImpulseCol.HeaderText = "Импульс, мс";
            this._releImpulseCol.Name = "_releImpulseCol";
            this._releImpulseCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.HeaderText = "Сигнал";
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 140;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releTypeCol.HeaderText = "Тип";
            this._releTypeCol.Items.AddRange(new object[] {
            "Повторитель",
            "Блинкер",
            "XXXXX"});
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 120;
            // 
            // _releNumberCol
            // 
            this._releNumberCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._releNumberCol.HeaderText = "№";
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 24;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._outputIndicatorsGrid);
            this.groupBox9.Location = new System.Drawing.Point(11, 287);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(424, 169);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Индикаторы";
            // 
            // _outputIndicatorsGrid
            // 
            this._outputIndicatorsGrid.AllowUserToAddRows = false;
            this._outputIndicatorsGrid.AllowUserToDeleteRows = false;
            this._outputIndicatorsGrid.AllowUserToResizeColumns = false;
            this._outputIndicatorsGrid.AllowUserToResizeRows = false;
            this._outputIndicatorsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._outputIndicatorsGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._outputIndicatorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputIndicatorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._outIndNumberCol,
            this._outIndTypeCol,
            this._outIndSignalCol,
            this._outIndCol,
            this._outIndJS_Col,
            this._outIndJA_Col});
            this._outputIndicatorsGrid.Location = new System.Drawing.Point(8, 14);
            this._outputIndicatorsGrid.Name = "_outputIndicatorsGrid";
            this._outputIndicatorsGrid.RowHeadersVisible = false;
            this._outputIndicatorsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputIndicatorsGrid.RowsDefaultCellStyle = dataGridViewCellStyle34;
            this._outputIndicatorsGrid.RowTemplate.Height = 24;
            this._outputIndicatorsGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._outputIndicatorsGrid.ShowCellErrors = false;
            this._outputIndicatorsGrid.ShowRowErrors = false;
            this._outputIndicatorsGrid.Size = new System.Drawing.Size(410, 149);
            this._outputIndicatorsGrid.TabIndex = 0;
            // 
            // _outIndJA_Col
            // 
            this._outIndJA_Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._outIndJA_Col.HeaderText = "ЖА";
            this._outIndJA_Col.Name = "_outIndJA_Col";
            // 
            // _outIndJS_Col
            // 
            this._outIndJS_Col.HeaderText = "ЖС";
            this._outIndJS_Col.Name = "_outIndJS_Col";
            this._outIndJS_Col.Width = 31;
            // 
            // _outIndCol
            // 
            this._outIndCol.HeaderText = "Инд.";
            this._outIndCol.Name = "_outIndCol";
            this._outIndCol.Width = 36;
            // 
            // _outIndSignalCol
            // 
            this._outIndSignalCol.HeaderText = "Сигнал";
            this._outIndSignalCol.Name = "_outIndSignalCol";
            this._outIndSignalCol.Width = 49;
            // 
            // _outIndTypeCol
            // 
            this._outIndTypeCol.HeaderText = "Тип";
            this._outIndTypeCol.Items.AddRange(new object[] {
            "Повторитель",
            "Блинкер",
            "XXXXX"});
            this._outIndTypeCol.Name = "_outIndTypeCol";
            this._outIndTypeCol.Width = 32;
            // 
            // _outIndNumberCol
            // 
            this._outIndNumberCol.HeaderText = "№";
            this._outIndNumberCol.Name = "_outIndNumberCol";
            this._outIndNumberCol.ReadOnly = true;
            this._outIndNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndNumberCol.Width = 24;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._outputLogicCheckList);
            this.groupBox10.Controls.Add(this._outputLogicAcceptBut);
            this.groupBox10.Controls.Add(this._outputLogicCombo);
            this.groupBox10.Location = new System.Drawing.Point(438, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(141, 453);
            this.groupBox10.TabIndex = 5;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Логические сигналы";
            // 
            // _outputLogicCombo
            // 
            this._outputLogicCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._outputLogicCombo.FormattingEnabled = true;
            this._outputLogicCombo.Items.AddRange(new object[] {
            "ВЛС 1",
            "ВЛС 2 ",
            "ВЛС 3 ",
            "ВЛС 4 ",
            "ВЛС 5",
            "ВЛС 6",
            "ВЛС 7 ",
            "ВЛС 8"});
            this._outputLogicCombo.Location = new System.Drawing.Point(37, 16);
            this._outputLogicCombo.Name = "_outputLogicCombo";
            this._outputLogicCombo.Size = new System.Drawing.Size(66, 21);
            this._outputLogicCombo.TabIndex = 1;
            this._outputLogicCombo.SelectedIndexChanged += new System.EventHandler(this._outputLogicCombo_SelectedIndexChanged);
            // 
            // _outputLogicAcceptBut
            // 
            this._outputLogicAcceptBut.Location = new System.Drawing.Point(41, 422);
            this._outputLogicAcceptBut.Name = "_outputLogicAcceptBut";
            this._outputLogicAcceptBut.Size = new System.Drawing.Size(59, 23);
            this._outputLogicAcceptBut.TabIndex = 3;
            this._outputLogicAcceptBut.Text = "Принять";
            this._outputLogicAcceptBut.UseVisualStyleBackColor = true;
            this._outputLogicAcceptBut.Click += new System.EventHandler(this._outputLogicAcceptBut_Click);
            // 
            // _outputLogicCheckList
            // 
            this._outputLogicCheckList.CheckOnClick = true;
            this._outputLogicCheckList.FormattingEnabled = true;
            this._outputLogicCheckList.Location = new System.Drawing.Point(3, 52);
            this._outputLogicCheckList.Name = "_outputLogicCheckList";
            this._outputLogicCheckList.ScrollAlwaysVisible = true;
            this._outputLogicCheckList.Size = new System.Drawing.Size(135, 349);
            this._outputLogicCheckList.TabIndex = 4;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this._outputSignalsDispepairTimeBox);
            this.groupBox3.Controls.Add(this._dispepairCheckList);
            this.groupBox3.Location = new System.Drawing.Point(582, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(141, 455);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Сигнальные реле";
            // 
            // _dispepairCheckList
            // 
            this._dispepairCheckList.CheckOnClick = true;
            this._dispepairCheckList.FormattingEnabled = true;
            this._dispepairCheckList.Items.AddRange(new object[] {
            "Неисправность 1",
            "Неисправность 2",
            "Неисправность 3",
            "Неисправность 4",
            "Неисправность 5",
            "Неисправность 6",
            "Неисправность 7",
            "Неисправность 8"});
            this._dispepairCheckList.Location = new System.Drawing.Point(4, 47);
            this._dispepairCheckList.Name = "_dispepairCheckList";
            this._dispepairCheckList.Size = new System.Drawing.Size(131, 124);
            this._dispepairCheckList.TabIndex = 5;
            // 
            // _outputSignalsDispepairTimeBox
            // 
            this._outputSignalsDispepairTimeBox.Location = new System.Drawing.Point(20, 211);
            this._outputSignalsDispepairTimeBox.Name = "_outputSignalsDispepairTimeBox";
            this._outputSignalsDispepairTimeBox.Size = new System.Drawing.Size(100, 20);
            this._outputSignalsDispepairTimeBox.TabIndex = 18;
            this._outputSignalsDispepairTimeBox.Tag = "3000000";
            this._outputSignalsDispepairTimeBox.Text = "0";
            this._outputSignalsDispepairTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._outputSignalsDispepairTimeBox.Validating += new System.ComponentModel.CancelEventHandler(this._outputSignalsDispepairTimeBox_Validating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 190);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(132, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Импульс неисправности";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(112, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "Реле неисправности";
            // 
            // _inSignalsPage
            // 
            this._inSignalsPage.Controls.Add(this.groupBox19);
            this._inSignalsPage.Controls.Add(this.groupBox12);
            this._inSignalsPage.Controls.Add(this.groupBox11);
            this._inSignalsPage.Controls.Add(this.groupBox7);
            this._inSignalsPage.Controls.Add(this.groupBox1);
            this._inSignalsPage.Controls.Add(this.groupBox5);
            this._inSignalsPage.Controls.Add(this.groupBox2);
            this._inSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._inSignalsPage.Name = "_inSignalsPage";
            this._inSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._inSignalsPage.Size = new System.Drawing.Size(930, 704);
            this._inSignalsPage.TabIndex = 0;
            this._inSignalsPage.Text = "Входные сигналы";
            this._inSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._defectTNNP_CB);
            this.groupBox2.Controls.Add(this._defectTN_CB);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this._TNNP_Box);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this._TN_Box);
            this.groupBox2.Location = new System.Drawing.Point(8, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(216, 135);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Измерительный канал";
            // 
            // _TN_Box
            // 
            this._TN_Box.Location = new System.Drawing.Point(136, 25);
            this._TN_Box.Name = "_TN_Box";
            this._TN_Box.Size = new System.Drawing.Size(70, 20);
            this._TN_Box.TabIndex = 2;
            this._TN_Box.Tag = "128000";
            this._TN_Box.Text = "0";
            this._TN_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._TN_Box.Validating += new System.ComponentModel.CancelEventHandler(this._TN_Box_Validating);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Параметры ТН";
            // 
            // _TNNP_Box
            // 
            this._TNNP_Box.Location = new System.Drawing.Point(136, 78);
            this._TNNP_Box.Name = "_TNNP_Box";
            this._TNNP_Box.Size = new System.Drawing.Size(70, 20);
            this._TNNP_Box.TabIndex = 5;
            this._TNNP_Box.Tag = "128000";
            this._TNNP_Box.Text = "0";
            this._TNNP_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._TNNP_Box.Validating += new System.ComponentModel.CancelEventHandler(this._TN_Box_Validating);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Параметр ТННП";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 54);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(104, 13);
            this.label14.TabIndex = 8;
            this.label14.Text = "Неисправность ТН";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 107);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(120, 13);
            this.label15.TabIndex = 9;
            this.label15.Text = "Неисправность ТННП";
            // 
            // _defectTN_CB
            // 
            this._defectTN_CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._defectTN_CB.FormattingEnabled = true;
            this._defectTN_CB.Location = new System.Drawing.Point(136, 51);
            this._defectTN_CB.Name = "_defectTN_CB";
            this._defectTN_CB.Size = new System.Drawing.Size(70, 21);
            this._defectTN_CB.TabIndex = 10;
            // 
            // _defectTNNP_CB
            // 
            this._defectTNNP_CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._defectTNNP_CB.FormattingEnabled = true;
            this._defectTNNP_CB.Location = new System.Drawing.Point(136, 104);
            this._defectTNNP_CB.Name = "_defectTNNP_CB";
            this._defectTNNP_CB.Size = new System.Drawing.Size(70, 21);
            this._defectTNNP_CB.TabIndex = 11;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._applyLogicSignalsBut);
            this.groupBox5.Controls.Add(this._logicChannelsCombo);
            this.groupBox5.Controls.Add(this._logicSignalsDataGrid);
            this.groupBox5.Location = new System.Drawing.Point(336, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(151, 299);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Логические сигналы";
            // 
            // _logicSignalsDataGrid
            // 
            this._logicSignalsDataGrid.AllowUserToAddRows = false;
            this._logicSignalsDataGrid.AllowUserToDeleteRows = false;
            this._logicSignalsDataGrid.AllowUserToResizeColumns = false;
            this._logicSignalsDataGrid.AllowUserToResizeRows = false;
            this._logicSignalsDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._logicSignalsDataGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._logicSignalsDataGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._logicSignalsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._logicSignalsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._diskretChannelCol,
            this._diskretValueCol});
            this._logicSignalsDataGrid.Location = new System.Drawing.Point(11, 45);
            this._logicSignalsDataGrid.MultiSelect = false;
            this._logicSignalsDataGrid.Name = "_logicSignalsDataGrid";
            this._logicSignalsDataGrid.RowHeadersVisible = false;
            this._logicSignalsDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._logicSignalsDataGrid.RowsDefaultCellStyle = dataGridViewCellStyle40;
            this._logicSignalsDataGrid.RowTemplate.Height = 20;
            this._logicSignalsDataGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._logicSignalsDataGrid.ShowCellErrors = false;
            this._logicSignalsDataGrid.ShowCellToolTips = false;
            this._logicSignalsDataGrid.ShowEditingIcon = false;
            this._logicSignalsDataGrid.ShowRowErrors = false;
            this._logicSignalsDataGrid.Size = new System.Drawing.Size(132, 219);
            this._logicSignalsDataGrid.TabIndex = 0;
            // 
            // _diskretValueCol
            // 
            this._diskretValueCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._diskretValueCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._diskretValueCol.HeaderText = "Значение";
            this._diskretValueCol.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this._diskretValueCol.Name = "_diskretValueCol";
            // 
            // _diskretChannelCol
            // 
            this._diskretChannelCol.HeaderText = "№";
            this._diskretChannelCol.Name = "_diskretChannelCol";
            this._diskretChannelCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._diskretChannelCol.Width = 24;
            // 
            // _logicChannelsCombo
            // 
            this._logicChannelsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._logicChannelsCombo.FormattingEnabled = true;
            this._logicChannelsCombo.Items.AddRange(new object[] {
            "1 И",
            "2 И",
            "3 И",
            "4 И",
            "5 ИЛИ",
            "6 ИЛИ",
            "7 ИЛИ",
            "8 ИЛИ"});
            this._logicChannelsCombo.Location = new System.Drawing.Point(43, 18);
            this._logicChannelsCombo.Name = "_logicChannelsCombo";
            this._logicChannelsCombo.Size = new System.Drawing.Size(64, 21);
            this._logicChannelsCombo.TabIndex = 1;
            // 
            // _applyLogicSignalsBut
            // 
            this._applyLogicSignalsBut.Location = new System.Drawing.Point(45, 270);
            this._applyLogicSignalsBut.Name = "_applyLogicSignalsBut";
            this._applyLogicSignalsBut.Size = new System.Drawing.Size(62, 23);
            this._applyLogicSignalsBut.TabIndex = 3;
            this._applyLogicSignalsBut.Text = "Принять";
            this._applyLogicSignalsBut.UseVisualStyleBackColor = true;
            this._applyLogicSignalsBut.Click += new System.EventHandler(this._applyLogicSignalsBut_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._inputSignalsTN_DispepairCombo);
            this.groupBox1.Controls.Add(this._inputSignalsIndicationResetCombo);
            this.groupBox1.Controls.Add(this._inputSignalsConstraintResetCombo);
            this.groupBox1.Location = new System.Drawing.Point(8, 205);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(322, 100);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Дополнительные сигналы";
            // 
            // _inputSignalsConstraintResetCombo
            // 
            this._inputSignalsConstraintResetCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inputSignalsConstraintResetCombo.FormattingEnabled = true;
            this._inputSignalsConstraintResetCombo.Location = new System.Drawing.Point(232, 14);
            this._inputSignalsConstraintResetCombo.Name = "_inputSignalsConstraintResetCombo";
            this._inputSignalsConstraintResetCombo.Size = new System.Drawing.Size(84, 21);
            this._inputSignalsConstraintResetCombo.TabIndex = 2;
            // 
            // _inputSignalsIndicationResetCombo
            // 
            this._inputSignalsIndicationResetCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inputSignalsIndicationResetCombo.FormattingEnabled = true;
            this._inputSignalsIndicationResetCombo.Location = new System.Drawing.Point(232, 41);
            this._inputSignalsIndicationResetCombo.Name = "_inputSignalsIndicationResetCombo";
            this._inputSignalsIndicationResetCombo.Size = new System.Drawing.Size(84, 21);
            this._inputSignalsIndicationResetCombo.TabIndex = 3;
            // 
            // _inputSignalsTN_DispepairCombo
            // 
            this._inputSignalsTN_DispepairCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inputSignalsTN_DispepairCombo.FormattingEnabled = true;
            this._inputSignalsTN_DispepairCombo.Location = new System.Drawing.Point(232, 68);
            this._inputSignalsTN_DispepairCombo.Name = "_inputSignalsTN_DispepairCombo";
            this._inputSignalsTN_DispepairCombo.Size = new System.Drawing.Size(84, 21);
            this._inputSignalsTN_DispepairCombo.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(199, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Переключение на резервные уставки";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Сброс индикации";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(203, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Сброс ступеней, ожидающих возврата";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._oscLength);
            this.groupBox7.Location = new System.Drawing.Point(8, 311);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(146, 43);
            this.groupBox7.TabIndex = 9;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Длит. периода осц.";
            // 
            // _oscLength
            // 
            this._oscLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscLength.FormattingEnabled = true;
            this._oscLength.Location = new System.Drawing.Point(6, 16);
            this._oscLength.Name = "_oscLength";
            this._oscLength.Size = new System.Drawing.Size(134, 21);
            this._oscLength.TabIndex = 12;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.comboBox3);
            this.groupBox11.Location = new System.Drawing.Point(160, 311);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(146, 43);
            this.groupBox11.TabIndex = 10;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Фиксация осц.";
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(6, 16);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(134, 21);
            this.comboBox3.TabIndex = 12;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label22);
            this.groupBox12.Controls.Add(this._oscWriteLength);
            this.groupBox12.Location = new System.Drawing.Point(312, 311);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(146, 43);
            this.groupBox12.TabIndex = 11;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Длит. предзаписа осц";
            // 
            // _oscWriteLength
            // 
            this._oscWriteLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._oscWriteLength.Location = new System.Drawing.Point(6, 17);
            this._oscWriteLength.Name = "_oscWriteLength";
            this._oscWriteLength.Size = new System.Drawing.Size(121, 20);
            this._oscWriteLength.TabIndex = 23;
            this._oscWriteLength.Tag = "3000000";
            this._oscWriteLength.Text = "1";
            this._oscWriteLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._oscWriteLength.Validating += new System.ComponentModel.CancelEventHandler(this._oscWriteLength_Validating);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(129, 20);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(15, 13);
            this.label22.TabIndex = 26;
            this.label22.Text = "%";
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this._applyKeys);
            this.groupBox19.Controls.Add(this._keysDataGrid);
            this.groupBox19.Location = new System.Drawing.Point(493, 6);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(145, 428);
            this.groupBox19.TabIndex = 19;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Ключи";
            // 
            // _keysDataGrid
            // 
            this._keysDataGrid.AllowUserToAddRows = false;
            this._keysDataGrid.AllowUserToDeleteRows = false;
            this._keysDataGrid.AllowUserToResizeColumns = false;
            this._keysDataGrid.AllowUserToResizeRows = false;
            this._keysDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._keysDataGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle36.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._keysDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle36;
            this._keysDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._keysDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn11,
            this._keyValue});
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle37.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._keysDataGrid.DefaultCellStyle = dataGridViewCellStyle37;
            this._keysDataGrid.Location = new System.Drawing.Point(7, 46);
            this._keysDataGrid.MultiSelect = false;
            this._keysDataGrid.Name = "_keysDataGrid";
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._keysDataGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle38;
            this._keysDataGrid.RowHeadersVisible = false;
            this._keysDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._keysDataGrid.RowsDefaultCellStyle = dataGridViewCellStyle39;
            this._keysDataGrid.RowTemplate.Height = 20;
            this._keysDataGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._keysDataGrid.ShowCellErrors = false;
            this._keysDataGrid.ShowCellToolTips = false;
            this._keysDataGrid.ShowEditingIcon = false;
            this._keysDataGrid.ShowRowErrors = false;
            this._keysDataGrid.Size = new System.Drawing.Size(132, 346);
            this._keysDataGrid.TabIndex = 1;
            // 
            // _keyValue
            // 
            this._keyValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._keyValue.HeaderText = "Значение";
            this._keyValue.Items.AddRange(new object[] {
            "Нет",
            "Да"});
            this._keyValue.Name = "_keyValue";
            this._keyValue.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "№";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Width = 24;
            // 
            // _applyKeys
            // 
            this._applyKeys.Location = new System.Drawing.Point(47, 398);
            this._applyKeys.Name = "_applyKeys";
            this._applyKeys.Size = new System.Drawing.Size(62, 23);
            this._applyKeys.TabIndex = 4;
            this._applyKeys.Text = "Принять";
            this._applyKeys.UseVisualStyleBackColor = true;
            this._applyKeys.Click += new System.EventHandler(this._applyKeys_Click);
            // 
            // _tabControl
            // 
            this._tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tabControl.Controls.Add(this._inSignalsPage);
            this._tabControl.Controls.Add(this._outputSignalsPage);
            this._tabControl.Controls.Add(this._defendingTabPage);
            this._tabControl.Location = new System.Drawing.Point(0, 0);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(938, 690);
            this._tabControl.TabIndex = 1;
            // 
            // Mr600ConfigurationFormV2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(938, 749);
            this.Controls.Add(this._statusStrip);
            this.Controls.Add(this._saveConfigBut);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this._writeConfigBut);
            this.Controls.Add(this._readConfigBut);
            this.Controls.Add(this._tabControl);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(786, 709);
            this.Name = "Mr600ConfigurationFormV2";
            this.Text = "Mr600ConfigurationFormV2";
            this.Shown += new System.EventHandler(this.Mr600ConfigurationFormV2_Shown);
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this._defendingTabPage.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this._TabControlDefending.ResumeLayout(false);
            this._frequenceTanPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._frequenceDefensesGrid)).EndInit();
            this._voltageDefensesTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this._blockDDL.ResumeLayout(false);
            this.groupBox25.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this._externalTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).EndInit();
            this._outputSignalsPage.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this._inSignalsPage.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._keysDataGrid)).EndInit();
            this._tabControl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip _statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel _processLabel;
        private System.Windows.Forms.ToolStripProgressBar _exchangeProgressBar;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.TabPage _defendingTabPage;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.TabControl _TabControlDefending;
        private System.Windows.Forms.TabPage _externalTabPage;
        private System.Windows.Forms.DataGridView _externalDefenseGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extDefNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefBlockingCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefWorkingCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefWorkingTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefReturnCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefReturnNumberCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefReturnTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _oscExDefenseColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _resetExDefenseColumn;
        private System.Windows.Forms.TabPage _voltageDefensesTabPage;
        private System.Windows.Forms.GroupBox _blockDDL;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.ComboBox _U_1_2_CB;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.ComboBox _U_1_1_CB;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.ComboBox _U_4_CB;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.ComboBox _U_3_CB;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox _U_2_CB;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.ComboBox _U_1_CB;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewComboBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn8;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesParameterCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkConstraintCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkTimeCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesReturnCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnConstraintCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnTimeCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesOSCv11Col1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesResetCol1;
        private System.Windows.Forms.TabPage _frequenceTanPage;
        private System.Windows.Forms.DataGridView _frequenceDefensesGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesName;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesMode;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesBlockingNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesWorkConstraint;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesWorkTime;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesReturn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesReturnTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesConstraintAPV;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesOSCv11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesReset;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Button _btnGroupChange;
        private System.Windows.Forms.RadioButton _reserveRadioBtnGroup;
        private System.Windows.Forms.RadioButton _mainRadioBtnGroup;
        private System.Windows.Forms.TabPage _outputSignalsPage;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox _outputSignalsDispepairTimeBox;
        private System.Windows.Forms.CheckedListBox _dispepairCheckList;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.CheckedListBox _outputLogicCheckList;
        private System.Windows.Forms.Button _outputLogicAcceptBut;
        private System.Windows.Forms.ComboBox _outputLogicCombo;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView _outputIndicatorsGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignalCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndJS_Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndJA_Col;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView _outputReleGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releImpulseCol;
        private System.Windows.Forms.TabPage _inSignalsPage;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Button _applyKeys;
        private System.Windows.Forms.DataGridView _keysDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewComboBoxColumn _keyValue;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.MaskedTextBox _oscWriteLength;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox _oscLength;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox _inputSignalsTN_DispepairCombo;
        private System.Windows.Forms.ComboBox _inputSignalsIndicationResetCombo;
        private System.Windows.Forms.ComboBox _inputSignalsConstraintResetCombo;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button _applyLogicSignalsBut;
        private System.Windows.Forms.ComboBox _logicChannelsCombo;
        private System.Windows.Forms.DataGridView _logicSignalsDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _diskretChannelCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _diskretValueCol;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox _defectTNNP_CB;
        private System.Windows.Forms.ComboBox _defectTN_CB;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox _TNNP_Box;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox _TN_Box;
        private System.Windows.Forms.TabControl _tabControl;
    }
}