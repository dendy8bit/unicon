﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.MR600.New.Configuration.ConfigStruct;
using BEMN.MR600.New.HelpClasses;
using BEMN.MR600.New.Structs.JournalStructs;
using BEMN.Forms.Export;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Interfaces;


namespace BEMN.MR600.New.Forms
{
    public partial class Mr600AlarmJournalFormV2 : Form, IFormView
    {
        private readonly MemoryEntity<AlarmJournalStruct> _alarmRecord;
        private readonly MemoryEntity<RomMeasuringStruct> _measuringChannel;
        private DataTable _table;
        private MR600 _device;
        private const string READ_AJ = "Чтение журнала аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        private const string RECORDS_IN_JOURNAL_PATTERN = "В журнале {0} сообщений";
        private const string PARAMETERS_LOADED = "Параметры загружены";
        private const string FAULT_LOAD_PARAMETERS = "Невозможно загрузить параметры";
        private const string FAULT_READ_JOURNAL = "Невозможно прочитать журнал";
        private const string JOURNAL_SAVED = "Журнал сохранён";

        public Mr600AlarmJournalFormV2()
        {
            this.InitializeComponent();
        }

        public Mr600AlarmJournalFormV2(MR600 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._alarmRecord = device.Mr600DeviceV2.AlarmRecord;
            this._alarmRecord.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadJournalOk);

            this._alarmRecord.ReadOk += HandlerHelper.CreateHandler(this, this._configProgressBar.PerformStep);
            this._alarmRecord.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.JournalReadFail);

            this._measuringChannel = device.Mr600DeviceV2.MeasuringChannel;
            this._measuringChannel.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringChannelLoad);
            this._measuringChannel.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringChannelLoadFail);
        }

        private void JournalReadFail()
        {
            this._statusLabel.Text = FAULT_READ_JOURNAL;
        }

        private void MeasuringChannelLoadFail()
        {
            MessageBox.Show(FAULT_LOAD_PARAMETERS);
        }

        private void MeasuringChannelLoad()
        {
            MessageBox.Show(PARAMETERS_LOADED);
            this._alarmRecord.LoadStruct();
        }

        private void ReadJournalOk()
        {
            int i = 1;
            var tn = this._measuringChannel.Value.RomFactorU;
            var tnnp = this._measuringChannel.Value.RomFactorUo;
            
            foreach (var record in this._alarmRecord.Value.Records)
            {
                if (record.IsEmpty)
                {
                    continue;
                }
                var tempKoef = this.GetTempKoef(record.Type);
                var type = record.Type == StringsV2.AlarmJournaleType[0] 
                    ? record.Type 
                    : string.Format("{0}{1}", record.Type, Math.Round(record.Value*tempKoef/256, 2));
                this._table.Rows.Add
                    (
                        i,
                        record.GetTime,
                        record.Message,
                        record.Code,
                        type,
                        Math.Round(record.F/(double) 256, 2),
                        Math.Round(record.Ua*tn/256, 2),
                        Math.Round(record.Ub*tn/256, 2),
                        Math.Round(record.Uc*tn/256, 2),
                        Math.Round(record.Uab*tn/256, 2),
                        Math.Round(record.Ubc*tn/256, 2),
                        Math.Round(record.Uca*tn/256, 2),
                        Math.Round(record.U0*tn/256, 2),
                        Math.Round(record.U1*tn/256, 2),
                        Math.Round(record.U2*tn/256, 2),
                        Math.Round(record.Un*tnnp/256, 2),
                        record.Discret
                    ); 
                i++;
            }
            this._journalGrid.DataSource = this._table;
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL_PATTERN, this._table.Rows.Count);
            this._readAlarmJournalButton.Enabled = true;
        }
        /// <summary>
        /// В зависимости от типа повреждения получаем коэффициент пересчета
        /// </summary>
        /// <param name="param">Тип повреждения</param>
        /// <returns>Коэффициент пересчета</returns>
        private double GetTempKoef(string param)
        {
            double ret;
            int key = StringsV2.AlarmJournaleType.IndexOf(param.Replace(" = ",""));
            switch (key)
            {
                case 1:
                    ret = 1;
                    break;
                case 2:
                    ret = this._measuringChannel.Value.RomFactorUo;
                    break;
                default:
                    ret = this._measuringChannel.Value.RomFactorU;
                    break;
            }
            return ret;
        }

        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR600); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr600AlarmJournalFormV2); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return "Журнал аварий"; }
        }
        #endregion [IFormView Members]

        private void Mr600AlarmJournalFormV2_Load(object sender, EventArgs e)
        {
            this._table = this.GetJournalDataTable();
            this._journalGrid.DataSource = this._table;
            this.StartRead();
        }

        private DataTable GetJournalDataTable()
        {
            var table = new DataTable("МР600_журнал_аварий");
            for (int j = 0; j < this._journalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._journalGrid.Columns[j].Name);
            }
            return table;
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._readAlarmJournalButton.Enabled = false;
            this._table.Rows.Clear();
            this._configProgressBar.Value = 0;
            this._statusLabel.Text = READ_AJ;
            this._measuringChannel.LoadStruct();
        }

        private void _readAlarmJournalButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
            }
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._openAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.Clear();
                this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL_PATTERN, this._table.Rows.Count);
            }
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR600AJ_new);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }
    }
}
