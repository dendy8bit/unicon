﻿namespace BEMN.MR600.New.Forms
{
    partial class Mr600MeasuringFormV2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._diskretPage = new System.Windows.Forms.TabPage();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._ssl24 = new BEMN.Forms.LedControl();
            this.label245 = new System.Windows.Forms.Label();
            this._ssl23 = new BEMN.Forms.LedControl();
            this.label246 = new System.Windows.Forms.Label();
            this._ssl22 = new BEMN.Forms.LedControl();
            this.label247 = new System.Windows.Forms.Label();
            this._ssl21 = new BEMN.Forms.LedControl();
            this.label248 = new System.Windows.Forms.Label();
            this._ssl20 = new BEMN.Forms.LedControl();
            this.label249 = new System.Windows.Forms.Label();
            this._ssl19 = new BEMN.Forms.LedControl();
            this.label250 = new System.Windows.Forms.Label();
            this._ssl18 = new BEMN.Forms.LedControl();
            this.label251 = new System.Windows.Forms.Label();
            this._ssl17 = new BEMN.Forms.LedControl();
            this.label252 = new System.Windows.Forms.Label();
            this._ssl16 = new BEMN.Forms.LedControl();
            this.label253 = new System.Windows.Forms.Label();
            this._ssl15 = new BEMN.Forms.LedControl();
            this.label254 = new System.Windows.Forms.Label();
            this._ssl14 = new BEMN.Forms.LedControl();
            this.label255 = new System.Windows.Forms.Label();
            this._ssl13 = new BEMN.Forms.LedControl();
            this.label256 = new System.Windows.Forms.Label();
            this._ssl12 = new BEMN.Forms.LedControl();
            this.label257 = new System.Windows.Forms.Label();
            this._ssl11 = new BEMN.Forms.LedControl();
            this.label258 = new System.Windows.Forms.Label();
            this._ssl10 = new BEMN.Forms.LedControl();
            this.label259 = new System.Windows.Forms.Label();
            this._ssl9 = new BEMN.Forms.LedControl();
            this.label260 = new System.Windows.Forms.Label();
            this._ssl8 = new BEMN.Forms.LedControl();
            this.label261 = new System.Windows.Forms.Label();
            this._ssl7 = new BEMN.Forms.LedControl();
            this.label262 = new System.Windows.Forms.Label();
            this._ssl6 = new BEMN.Forms.LedControl();
            this.label263 = new System.Windows.Forms.Label();
            this._ssl5 = new BEMN.Forms.LedControl();
            this.label264 = new System.Windows.Forms.Label();
            this._ssl4 = new BEMN.Forms.LedControl();
            this.label265 = new System.Windows.Forms.Label();
            this._ssl3 = new BEMN.Forms.LedControl();
            this.label266 = new System.Windows.Forms.Label();
            this._ssl2 = new BEMN.Forms.LedControl();
            this.label267 = new System.Windows.Forms.Label();
            this._ssl1 = new BEMN.Forms.LedControl();
            this.label268 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this._FminLed8 = new BEMN.Forms.LedControl();
            this._FminLed7 = new BEMN.Forms.LedControl();
            this._FminLed6 = new BEMN.Forms.LedControl();
            this._FminLed5 = new BEMN.Forms.LedControl();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this._FminLed4 = new BEMN.Forms.LedControl();
            this._FminLed3 = new BEMN.Forms.LedControl();
            this._FminLed2 = new BEMN.Forms.LedControl();
            this.label92 = new System.Windows.Forms.Label();
            this._FminLed1 = new BEMN.Forms.LedControl();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._FmaxLed8 = new BEMN.Forms.LedControl();
            this._FmaxLed7 = new BEMN.Forms.LedControl();
            this._FmaxLed6 = new BEMN.Forms.LedControl();
            this._FmaxLed5 = new BEMN.Forms.LedControl();
            this.label49 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this._FmaxLed4 = new BEMN.Forms.LedControl();
            this._FmaxLed3 = new BEMN.Forms.LedControl();
            this._FmaxLed2 = new BEMN.Forms.LedControl();
            this.label86 = new System.Windows.Forms.Label();
            this._FmaxLed1 = new BEMN.Forms.LedControl();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.label129 = new System.Windows.Forms.Label();
            this._faultSignalLed15 = new BEMN.Forms.LedControl();
            this.label130 = new System.Windows.Forms.Label();
            this._faultSignalLed14 = new BEMN.Forms.LedControl();
            this.label131 = new System.Windows.Forms.Label();
            this._faultSignalLed13 = new BEMN.Forms.LedControl();
            this.label132 = new System.Windows.Forms.Label();
            this._faultSignalLed12 = new BEMN.Forms.LedControl();
            this.label133 = new System.Windows.Forms.Label();
            this._faultSignalLed11 = new BEMN.Forms.LedControl();
            this.label134 = new System.Windows.Forms.Label();
            this._faultSignalLed10 = new BEMN.Forms.LedControl();
            this.label136 = new System.Windows.Forms.Label();
            this._faultSignalLed9 = new BEMN.Forms.LedControl();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.label121 = new System.Windows.Forms.Label();
            this._faultSignalLed8 = new BEMN.Forms.LedControl();
            this.label122 = new System.Windows.Forms.Label();
            this._faultSignalLed7 = new BEMN.Forms.LedControl();
            this.label123 = new System.Windows.Forms.Label();
            this._faultSignalLed6 = new BEMN.Forms.LedControl();
            this.label124 = new System.Windows.Forms.Label();
            this._faultSignalLed5 = new BEMN.Forms.LedControl();
            this.label125 = new System.Windows.Forms.Label();
            this._faultSignalLed4 = new BEMN.Forms.LedControl();
            this.label126 = new System.Windows.Forms.Label();
            this._faultSignalLed3 = new BEMN.Forms.LedControl();
            this.label127 = new System.Windows.Forms.Label();
            this._faultSignalLed2 = new BEMN.Forms.LedControl();
            this.label128 = new System.Windows.Forms.Label();
            this._faultSignalLed1 = new BEMN.Forms.LedControl();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.label117 = new System.Windows.Forms.Label();
            this._faultStateLed4 = new BEMN.Forms.LedControl();
            this.label118 = new System.Windows.Forms.Label();
            this._faultStateLed3 = new BEMN.Forms.LedControl();
            this.label119 = new System.Windows.Forms.Label();
            this._faultStateLed2 = new BEMN.Forms.LedControl();
            this.label120 = new System.Windows.Forms.Label();
            this._faultStateLed1 = new BEMN.Forms.LedControl();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.label105 = new System.Windows.Forms.Label();
            this._extDefenseLed8 = new BEMN.Forms.LedControl();
            this.label106 = new System.Windows.Forms.Label();
            this._extDefenseLed7 = new BEMN.Forms.LedControl();
            this.label107 = new System.Windows.Forms.Label();
            this._extDefenseLed6 = new BEMN.Forms.LedControl();
            this.label108 = new System.Windows.Forms.Label();
            this._extDefenseLed5 = new BEMN.Forms.LedControl();
            this.label109 = new System.Windows.Forms.Label();
            this._extDefenseLed4 = new BEMN.Forms.LedControl();
            this.label110 = new System.Windows.Forms.Label();
            this._extDefenseLed3 = new BEMN.Forms.LedControl();
            this.label111 = new System.Windows.Forms.Label();
            this._extDefenseLed2 = new BEMN.Forms.LedControl();
            this.label112 = new System.Windows.Forms.Label();
            this._extDefenseLed1 = new BEMN.Forms.LedControl();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._U12Led_4 = new BEMN.Forms.LedControl();
            this._U12Led_3 = new BEMN.Forms.LedControl();
            this._U12Led_2 = new BEMN.Forms.LedControl();
            this._U12Led_1 = new BEMN.Forms.LedControl();
            this.label67 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this._U12Led_8 = new BEMN.Forms.LedControl();
            this._U12Led_7 = new BEMN.Forms.LedControl();
            this._U12Led_6 = new BEMN.Forms.LedControl();
            this.label78 = new System.Windows.Forms.Label();
            this._U12Led_5 = new BEMN.Forms.LedControl();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._U0maxLed8 = new BEMN.Forms.LedControl();
            this._U0maxLed7 = new BEMN.Forms.LedControl();
            this._U0maxLed6 = new BEMN.Forms.LedControl();
            this._U0maxLed5 = new BEMN.Forms.LedControl();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this._U0maxLed4 = new BEMN.Forms.LedControl();
            this._U0maxLed3 = new BEMN.Forms.LedControl();
            this._U0maxLed2 = new BEMN.Forms.LedControl();
            this.label84 = new System.Windows.Forms.Label();
            this._U0maxLed1 = new BEMN.Forms.LedControl();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._UminLed8 = new BEMN.Forms.LedControl();
            this._UminLed7 = new BEMN.Forms.LedControl();
            this._UminLed6 = new BEMN.Forms.LedControl();
            this._UminLed5 = new BEMN.Forms.LedControl();
            this.label55 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this._UminLed4 = new BEMN.Forms.LedControl();
            this._UminLed3 = new BEMN.Forms.LedControl();
            this._UminLed2 = new BEMN.Forms.LedControl();
            this.label66 = new System.Windows.Forms.Label();
            this._UminLed1 = new BEMN.Forms.LedControl();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._UmaxLed8 = new BEMN.Forms.LedControl();
            this._UmaxLed7 = new BEMN.Forms.LedControl();
            this._UmaxLed6 = new BEMN.Forms.LedControl();
            this._UmaxLed5 = new BEMN.Forms.LedControl();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this._UmaxLed4 = new BEMN.Forms.LedControl();
            this._UmaxLed3 = new BEMN.Forms.LedControl();
            this._UmaxLed2 = new BEMN.Forms.LedControl();
            this.label68 = new System.Windows.Forms.Label();
            this._UmaxLed1 = new BEMN.Forms.LedControl();
            this._analogPage = new System.Windows.Forms.TabPage();
            this._dateTimeControl = new BEMN.Forms.DateTimeControl();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._resetInd_But = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label61 = new System.Windows.Forms.Label();
            this._resGroupLed = new BEMN.Forms.LedControl();
            this._uabcLed = new BEMN.Forms.LedControl();
            this.label94 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this._sjLed = new BEMN.Forms.LedControl();
            this.label58 = new System.Windows.Forms.Label();
            this._faultLed = new BEMN.Forms.LedControl();
            this._ajLed = new BEMN.Forms.LedControl();
            this._resetJA_But = new System.Windows.Forms.Button();
            this._resetJS_But = new System.Windows.Forms.Button();
            this._resetFaultBut = new System.Windows.Forms.Button();
            this._constraintToggleButton = new System.Windows.Forms.Button();
            this.label50 = new System.Windows.Forms.Label();
            this._mainGroupLed = new BEMN.Forms.LedControl();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label104 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this._U1 = new System.Windows.Forms.TextBox();
            this._F = new System.Windows.Forms.TextBox();
            this._U2 = new System.Windows.Forms.TextBox();
            this._U0 = new System.Windows.Forms.TextBox();
            this._Ubc = new System.Windows.Forms.TextBox();
            this._Uc = new System.Windows.Forms.TextBox();
            this._Ua = new System.Windows.Forms.TextBox();
            this._Uab = new System.Windows.Forms.TextBox();
            this._Ub = new System.Windows.Forms.TextBox();
            this._Un = new System.Windows.Forms.TextBox();
            this._Uca = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label41 = new System.Windows.Forms.Label();
            this._inL_led8 = new BEMN.Forms.LedControl();
            this.label42 = new System.Windows.Forms.Label();
            this._inL_led7 = new BEMN.Forms.LedControl();
            this.label43 = new System.Windows.Forms.Label();
            this._inL_led6 = new BEMN.Forms.LedControl();
            this.label44 = new System.Windows.Forms.Label();
            this._inL_led5 = new BEMN.Forms.LedControl();
            this.label45 = new System.Windows.Forms.Label();
            this._inL_led4 = new BEMN.Forms.LedControl();
            this.label46 = new System.Windows.Forms.Label();
            this._inL_led3 = new BEMN.Forms.LedControl();
            this.label47 = new System.Windows.Forms.Label();
            this._inL_led2 = new BEMN.Forms.LedControl();
            this.label48 = new System.Windows.Forms.Label();
            this._inL_led1 = new BEMN.Forms.LedControl();
            this.label25 = new System.Windows.Forms.Label();
            this._inD_led8 = new BEMN.Forms.LedControl();
            this.label26 = new System.Windows.Forms.Label();
            this._inD_led7 = new BEMN.Forms.LedControl();
            this.label27 = new System.Windows.Forms.Label();
            this._inD_led6 = new BEMN.Forms.LedControl();
            this.label28 = new System.Windows.Forms.Label();
            this._inD_led5 = new BEMN.Forms.LedControl();
            this.label29 = new System.Windows.Forms.Label();
            this._inD_led4 = new BEMN.Forms.LedControl();
            this.label30 = new System.Windows.Forms.Label();
            this._inD_led3 = new BEMN.Forms.LedControl();
            this.label31 = new System.Windows.Forms.Label();
            this._inD_led2 = new BEMN.Forms.LedControl();
            this.label32 = new System.Windows.Forms.Label();
            this._inD_led1 = new BEMN.Forms.LedControl();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this._outLed8 = new BEMN.Forms.LedControl();
            this.label18 = new System.Windows.Forms.Label();
            this._outLed7 = new BEMN.Forms.LedControl();
            this.label19 = new System.Windows.Forms.Label();
            this._outLed6 = new BEMN.Forms.LedControl();
            this.label20 = new System.Windows.Forms.Label();
            this._outLed5 = new BEMN.Forms.LedControl();
            this.label21 = new System.Windows.Forms.Label();
            this._outLed4 = new BEMN.Forms.LedControl();
            this.label22 = new System.Windows.Forms.Label();
            this._outLed3 = new BEMN.Forms.LedControl();
            this.label23 = new System.Windows.Forms.Label();
            this._outLed2 = new BEMN.Forms.LedControl();
            this.label24 = new System.Windows.Forms.Label();
            this._outLed1 = new BEMN.Forms.LedControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this._releLed16 = new BEMN.Forms.LedControl();
            this._releAlarmLed = new BEMN.Forms.LedControl();
            this.label34 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this._releLed15 = new BEMN.Forms.LedControl();
            this.label35 = new System.Windows.Forms.Label();
            this._releFaultLed = new BEMN.Forms.LedControl();
            this._releLed14 = new BEMN.Forms.LedControl();
            this.label36 = new System.Windows.Forms.Label();
            this._releLed13 = new BEMN.Forms.LedControl();
            this.label37 = new System.Windows.Forms.Label();
            this._releLed12 = new BEMN.Forms.LedControl();
            this.label38 = new System.Windows.Forms.Label();
            this._releLed11 = new BEMN.Forms.LedControl();
            this.label39 = new System.Windows.Forms.Label();
            this._releLed10 = new BEMN.Forms.LedControl();
            this.label40 = new System.Windows.Forms.Label();
            this._releLed9 = new BEMN.Forms.LedControl();
            this.label9 = new System.Windows.Forms.Label();
            this._releLed8 = new BEMN.Forms.LedControl();
            this.label10 = new System.Windows.Forms.Label();
            this._releLed7 = new BEMN.Forms.LedControl();
            this.label11 = new System.Windows.Forms.Label();
            this._releLed6 = new BEMN.Forms.LedControl();
            this.label12 = new System.Windows.Forms.Label();
            this._releLed5 = new BEMN.Forms.LedControl();
            this.label13 = new System.Windows.Forms.Label();
            this._releLed4 = new BEMN.Forms.LedControl();
            this.label14 = new System.Windows.Forms.Label();
            this._releLed3 = new BEMN.Forms.LedControl();
            this.label15 = new System.Windows.Forms.Label();
            this._releLed2 = new BEMN.Forms.LedControl();
            this.label16 = new System.Windows.Forms.Label();
            this._releLed1 = new BEMN.Forms.LedControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this._indLed10 = new BEMN.Forms.LedControl();
            this.label6 = new System.Windows.Forms.Label();
            this._indLed9 = new BEMN.Forms.LedControl();
            this.label95 = new System.Windows.Forms.Label();
            this._indLed8 = new BEMN.Forms.LedControl();
            this.label96 = new System.Windows.Forms.Label();
            this._indLed7 = new BEMN.Forms.LedControl();
            this.label7 = new System.Windows.Forms.Label();
            this._indLed6 = new BEMN.Forms.LedControl();
            this.label8 = new System.Windows.Forms.Label();
            this._indLed5 = new BEMN.Forms.LedControl();
            this.label3 = new System.Windows.Forms.Label();
            this._indLed4 = new BEMN.Forms.LedControl();
            this.label4 = new System.Windows.Forms.Label();
            this._indLed3 = new BEMN.Forms.LedControl();
            this.label2 = new System.Windows.Forms.Label();
            this._indLed2 = new BEMN.Forms.LedControl();
            this.label1 = new System.Windows.Forms.Label();
            this._indLed1 = new BEMN.Forms.LedControl();
            this._diagTab = new System.Windows.Forms.TabControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.slpOnBtn = new System.Windows.Forms.Button();
            this._splLed = new BEMN.Forms.LedControl();
            this.label113 = new System.Windows.Forms.Label();
            this.splOffBtn = new System.Windows.Forms.Button();
            this._diskretPage.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this._analogPage.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this._diagTab.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _diskretPage
            // 
            this._diskretPage.Controls.Add(this.groupBox21);
            this._diskretPage.Controls.Add(this.groupBox13);
            this._diskretPage.Controls.Add(this.groupBox7);
            this._diskretPage.Controls.Add(this.groupBox19);
            this._diskretPage.Controls.Add(this.groupBox18);
            this._diskretPage.Controls.Add(this.groupBox17);
            this._diskretPage.Controls.Add(this.groupBox16);
            this._diskretPage.Controls.Add(this.groupBox11);
            this._diskretPage.Controls.Add(this.groupBox12);
            this._diskretPage.Controls.Add(this.groupBox10);
            this._diskretPage.Controls.Add(this.groupBox9);
            this._diskretPage.ImageIndex = 1;
            this._diskretPage.Location = new System.Drawing.Point(4, 22);
            this._diskretPage.Name = "_diskretPage";
            this._diskretPage.Padding = new System.Windows.Forms.Padding(3);
            this._diskretPage.Size = new System.Drawing.Size(635, 422);
            this._diskretPage.TabIndex = 1;
            this._diskretPage.Text = "Дискретная БД";
            this._diskretPage.UseVisualStyleBackColor = true;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._ssl24);
            this.groupBox21.Controls.Add(this.label245);
            this.groupBox21.Controls.Add(this._ssl23);
            this.groupBox21.Controls.Add(this.label246);
            this.groupBox21.Controls.Add(this._ssl22);
            this.groupBox21.Controls.Add(this.label247);
            this.groupBox21.Controls.Add(this._ssl21);
            this.groupBox21.Controls.Add(this.label248);
            this.groupBox21.Controls.Add(this._ssl20);
            this.groupBox21.Controls.Add(this.label249);
            this.groupBox21.Controls.Add(this._ssl19);
            this.groupBox21.Controls.Add(this.label250);
            this.groupBox21.Controls.Add(this._ssl18);
            this.groupBox21.Controls.Add(this.label251);
            this.groupBox21.Controls.Add(this._ssl17);
            this.groupBox21.Controls.Add(this.label252);
            this.groupBox21.Controls.Add(this._ssl16);
            this.groupBox21.Controls.Add(this.label253);
            this.groupBox21.Controls.Add(this._ssl15);
            this.groupBox21.Controls.Add(this.label254);
            this.groupBox21.Controls.Add(this._ssl14);
            this.groupBox21.Controls.Add(this.label255);
            this.groupBox21.Controls.Add(this._ssl13);
            this.groupBox21.Controls.Add(this.label256);
            this.groupBox21.Controls.Add(this._ssl12);
            this.groupBox21.Controls.Add(this.label257);
            this.groupBox21.Controls.Add(this._ssl11);
            this.groupBox21.Controls.Add(this.label258);
            this.groupBox21.Controls.Add(this._ssl10);
            this.groupBox21.Controls.Add(this.label259);
            this.groupBox21.Controls.Add(this._ssl9);
            this.groupBox21.Controls.Add(this.label260);
            this.groupBox21.Controls.Add(this._ssl8);
            this.groupBox21.Controls.Add(this.label261);
            this.groupBox21.Controls.Add(this._ssl7);
            this.groupBox21.Controls.Add(this.label262);
            this.groupBox21.Controls.Add(this._ssl6);
            this.groupBox21.Controls.Add(this.label263);
            this.groupBox21.Controls.Add(this._ssl5);
            this.groupBox21.Controls.Add(this.label264);
            this.groupBox21.Controls.Add(this._ssl4);
            this.groupBox21.Controls.Add(this.label265);
            this.groupBox21.Controls.Add(this._ssl3);
            this.groupBox21.Controls.Add(this.label266);
            this.groupBox21.Controls.Add(this._ssl2);
            this.groupBox21.Controls.Add(this.label267);
            this.groupBox21.Controls.Add(this._ssl1);
            this.groupBox21.Controls.Add(this.label268);
            this.groupBox21.Location = new System.Drawing.Point(10, 216);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(205, 175);
            this.groupBox21.TabIndex = 15;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Сигналы СП-логики";
            // 
            // _ssl24
            // 
            this._ssl24.Location = new System.Drawing.Point(142, 152);
            this._ssl24.Name = "_ssl24";
            this._ssl24.Size = new System.Drawing.Size(13, 13);
            this._ssl24.State = BEMN.Forms.LedState.Off;
            this._ssl24.TabIndex = 47;
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.Location = new System.Drawing.Point(161, 152);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(41, 13);
            this.label245.TabIndex = 46;
            this.label245.Text = "ССЛ24";
            // 
            // _ssl23
            // 
            this._ssl23.Location = new System.Drawing.Point(142, 133);
            this._ssl23.Name = "_ssl23";
            this._ssl23.Size = new System.Drawing.Size(13, 13);
            this._ssl23.State = BEMN.Forms.LedState.Off;
            this._ssl23.TabIndex = 45;
            // 
            // label246
            // 
            this.label246.AutoSize = true;
            this.label246.Location = new System.Drawing.Point(161, 133);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(41, 13);
            this.label246.TabIndex = 44;
            this.label246.Text = "ССЛ23";
            // 
            // _ssl22
            // 
            this._ssl22.Location = new System.Drawing.Point(142, 114);
            this._ssl22.Name = "_ssl22";
            this._ssl22.Size = new System.Drawing.Size(13, 13);
            this._ssl22.State = BEMN.Forms.LedState.Off;
            this._ssl22.TabIndex = 43;
            // 
            // label247
            // 
            this.label247.AutoSize = true;
            this.label247.Location = new System.Drawing.Point(161, 114);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(41, 13);
            this.label247.TabIndex = 42;
            this.label247.Text = "ССЛ22";
            // 
            // _ssl21
            // 
            this._ssl21.Location = new System.Drawing.Point(142, 95);
            this._ssl21.Name = "_ssl21";
            this._ssl21.Size = new System.Drawing.Size(13, 13);
            this._ssl21.State = BEMN.Forms.LedState.Off;
            this._ssl21.TabIndex = 41;
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.Location = new System.Drawing.Point(161, 95);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(41, 13);
            this.label248.TabIndex = 40;
            this.label248.Text = "ССЛ21";
            // 
            // _ssl20
            // 
            this._ssl20.Location = new System.Drawing.Point(142, 76);
            this._ssl20.Name = "_ssl20";
            this._ssl20.Size = new System.Drawing.Size(13, 13);
            this._ssl20.State = BEMN.Forms.LedState.Off;
            this._ssl20.TabIndex = 39;
            // 
            // label249
            // 
            this.label249.AutoSize = true;
            this.label249.Location = new System.Drawing.Point(162, 76);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(41, 13);
            this.label249.TabIndex = 38;
            this.label249.Text = "ССЛ20";
            // 
            // _ssl19
            // 
            this._ssl19.Location = new System.Drawing.Point(142, 57);
            this._ssl19.Name = "_ssl19";
            this._ssl19.Size = new System.Drawing.Size(13, 13);
            this._ssl19.State = BEMN.Forms.LedState.Off;
            this._ssl19.TabIndex = 37;
            // 
            // label250
            // 
            this.label250.AutoSize = true;
            this.label250.Location = new System.Drawing.Point(161, 57);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(41, 13);
            this.label250.TabIndex = 36;
            this.label250.Text = "ССЛ19";
            // 
            // _ssl18
            // 
            this._ssl18.Location = new System.Drawing.Point(142, 38);
            this._ssl18.Name = "_ssl18";
            this._ssl18.Size = new System.Drawing.Size(13, 13);
            this._ssl18.State = BEMN.Forms.LedState.Off;
            this._ssl18.TabIndex = 35;
            // 
            // label251
            // 
            this.label251.AutoSize = true;
            this.label251.Location = new System.Drawing.Point(161, 38);
            this.label251.Name = "label251";
            this.label251.Size = new System.Drawing.Size(41, 13);
            this.label251.TabIndex = 34;
            this.label251.Text = "ССЛ18";
            // 
            // _ssl17
            // 
            this._ssl17.Location = new System.Drawing.Point(142, 19);
            this._ssl17.Name = "_ssl17";
            this._ssl17.Size = new System.Drawing.Size(13, 13);
            this._ssl17.State = BEMN.Forms.LedState.Off;
            this._ssl17.TabIndex = 33;
            // 
            // label252
            // 
            this.label252.AutoSize = true;
            this.label252.Location = new System.Drawing.Point(161, 19);
            this.label252.Name = "label252";
            this.label252.Size = new System.Drawing.Size(41, 13);
            this.label252.TabIndex = 32;
            this.label252.Text = "ССЛ17";
            // 
            // _ssl16
            // 
            this._ssl16.Location = new System.Drawing.Point(71, 152);
            this._ssl16.Name = "_ssl16";
            this._ssl16.Size = new System.Drawing.Size(13, 13);
            this._ssl16.State = BEMN.Forms.LedState.Off;
            this._ssl16.TabIndex = 31;
            // 
            // label253
            // 
            this.label253.AutoSize = true;
            this.label253.Location = new System.Drawing.Point(90, 152);
            this.label253.Name = "label253";
            this.label253.Size = new System.Drawing.Size(41, 13);
            this.label253.TabIndex = 30;
            this.label253.Text = "ССЛ16";
            // 
            // _ssl15
            // 
            this._ssl15.Location = new System.Drawing.Point(71, 133);
            this._ssl15.Name = "_ssl15";
            this._ssl15.Size = new System.Drawing.Size(13, 13);
            this._ssl15.State = BEMN.Forms.LedState.Off;
            this._ssl15.TabIndex = 29;
            // 
            // label254
            // 
            this.label254.AutoSize = true;
            this.label254.Location = new System.Drawing.Point(90, 133);
            this.label254.Name = "label254";
            this.label254.Size = new System.Drawing.Size(41, 13);
            this.label254.TabIndex = 28;
            this.label254.Text = "ССЛ15";
            // 
            // _ssl14
            // 
            this._ssl14.Location = new System.Drawing.Point(71, 114);
            this._ssl14.Name = "_ssl14";
            this._ssl14.Size = new System.Drawing.Size(13, 13);
            this._ssl14.State = BEMN.Forms.LedState.Off;
            this._ssl14.TabIndex = 27;
            // 
            // label255
            // 
            this.label255.AutoSize = true;
            this.label255.Location = new System.Drawing.Point(90, 114);
            this.label255.Name = "label255";
            this.label255.Size = new System.Drawing.Size(41, 13);
            this.label255.TabIndex = 26;
            this.label255.Text = "ССЛ14";
            // 
            // _ssl13
            // 
            this._ssl13.Location = new System.Drawing.Point(71, 95);
            this._ssl13.Name = "_ssl13";
            this._ssl13.Size = new System.Drawing.Size(13, 13);
            this._ssl13.State = BEMN.Forms.LedState.Off;
            this._ssl13.TabIndex = 25;
            // 
            // label256
            // 
            this.label256.AutoSize = true;
            this.label256.Location = new System.Drawing.Point(90, 95);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(41, 13);
            this.label256.TabIndex = 24;
            this.label256.Text = "ССЛ13";
            // 
            // _ssl12
            // 
            this._ssl12.Location = new System.Drawing.Point(71, 76);
            this._ssl12.Name = "_ssl12";
            this._ssl12.Size = new System.Drawing.Size(13, 13);
            this._ssl12.State = BEMN.Forms.LedState.Off;
            this._ssl12.TabIndex = 23;
            // 
            // label257
            // 
            this.label257.AutoSize = true;
            this.label257.Location = new System.Drawing.Point(90, 76);
            this.label257.Name = "label257";
            this.label257.Size = new System.Drawing.Size(41, 13);
            this.label257.TabIndex = 22;
            this.label257.Text = "ССЛ12";
            // 
            // _ssl11
            // 
            this._ssl11.Location = new System.Drawing.Point(71, 57);
            this._ssl11.Name = "_ssl11";
            this._ssl11.Size = new System.Drawing.Size(13, 13);
            this._ssl11.State = BEMN.Forms.LedState.Off;
            this._ssl11.TabIndex = 21;
            // 
            // label258
            // 
            this.label258.AutoSize = true;
            this.label258.Location = new System.Drawing.Point(90, 57);
            this.label258.Name = "label258";
            this.label258.Size = new System.Drawing.Size(41, 13);
            this.label258.TabIndex = 20;
            this.label258.Text = "ССЛ11";
            // 
            // _ssl10
            // 
            this._ssl10.Location = new System.Drawing.Point(71, 38);
            this._ssl10.Name = "_ssl10";
            this._ssl10.Size = new System.Drawing.Size(13, 13);
            this._ssl10.State = BEMN.Forms.LedState.Off;
            this._ssl10.TabIndex = 19;
            // 
            // label259
            // 
            this.label259.AutoSize = true;
            this.label259.Location = new System.Drawing.Point(90, 38);
            this.label259.Name = "label259";
            this.label259.Size = new System.Drawing.Size(41, 13);
            this.label259.TabIndex = 18;
            this.label259.Text = "ССЛ10";
            // 
            // _ssl9
            // 
            this._ssl9.Location = new System.Drawing.Point(71, 19);
            this._ssl9.Name = "_ssl9";
            this._ssl9.Size = new System.Drawing.Size(13, 13);
            this._ssl9.State = BEMN.Forms.LedState.Off;
            this._ssl9.TabIndex = 17;
            // 
            // label260
            // 
            this.label260.AutoSize = true;
            this.label260.Location = new System.Drawing.Point(90, 19);
            this.label260.Name = "label260";
            this.label260.Size = new System.Drawing.Size(35, 13);
            this.label260.TabIndex = 16;
            this.label260.Text = "ССЛ9";
            // 
            // _ssl8
            // 
            this._ssl8.Location = new System.Drawing.Point(6, 152);
            this._ssl8.Name = "_ssl8";
            this._ssl8.Size = new System.Drawing.Size(13, 13);
            this._ssl8.State = BEMN.Forms.LedState.Off;
            this._ssl8.TabIndex = 15;
            // 
            // label261
            // 
            this.label261.AutoSize = true;
            this.label261.Location = new System.Drawing.Point(25, 152);
            this.label261.Name = "label261";
            this.label261.Size = new System.Drawing.Size(35, 13);
            this.label261.TabIndex = 14;
            this.label261.Text = "ССЛ8";
            // 
            // _ssl7
            // 
            this._ssl7.Location = new System.Drawing.Point(6, 133);
            this._ssl7.Name = "_ssl7";
            this._ssl7.Size = new System.Drawing.Size(13, 13);
            this._ssl7.State = BEMN.Forms.LedState.Off;
            this._ssl7.TabIndex = 13;
            // 
            // label262
            // 
            this.label262.AutoSize = true;
            this.label262.Location = new System.Drawing.Point(25, 133);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(35, 13);
            this.label262.TabIndex = 12;
            this.label262.Text = "ССЛ7";
            // 
            // _ssl6
            // 
            this._ssl6.Location = new System.Drawing.Point(6, 114);
            this._ssl6.Name = "_ssl6";
            this._ssl6.Size = new System.Drawing.Size(13, 13);
            this._ssl6.State = BEMN.Forms.LedState.Off;
            this._ssl6.TabIndex = 11;
            // 
            // label263
            // 
            this.label263.AutoSize = true;
            this.label263.Location = new System.Drawing.Point(25, 114);
            this.label263.Name = "label263";
            this.label263.Size = new System.Drawing.Size(35, 13);
            this.label263.TabIndex = 10;
            this.label263.Text = "ССЛ6";
            // 
            // _ssl5
            // 
            this._ssl5.Location = new System.Drawing.Point(6, 95);
            this._ssl5.Name = "_ssl5";
            this._ssl5.Size = new System.Drawing.Size(13, 13);
            this._ssl5.State = BEMN.Forms.LedState.Off;
            this._ssl5.TabIndex = 9;
            // 
            // label264
            // 
            this.label264.AutoSize = true;
            this.label264.Location = new System.Drawing.Point(25, 95);
            this.label264.Name = "label264";
            this.label264.Size = new System.Drawing.Size(35, 13);
            this.label264.TabIndex = 8;
            this.label264.Text = "ССЛ5";
            // 
            // _ssl4
            // 
            this._ssl4.Location = new System.Drawing.Point(6, 76);
            this._ssl4.Name = "_ssl4";
            this._ssl4.Size = new System.Drawing.Size(13, 13);
            this._ssl4.State = BEMN.Forms.LedState.Off;
            this._ssl4.TabIndex = 7;
            // 
            // label265
            // 
            this.label265.AutoSize = true;
            this.label265.Location = new System.Drawing.Point(26, 76);
            this.label265.Name = "label265";
            this.label265.Size = new System.Drawing.Size(35, 13);
            this.label265.TabIndex = 6;
            this.label265.Text = "ССЛ4";
            // 
            // _ssl3
            // 
            this._ssl3.Location = new System.Drawing.Point(6, 57);
            this._ssl3.Name = "_ssl3";
            this._ssl3.Size = new System.Drawing.Size(13, 13);
            this._ssl3.State = BEMN.Forms.LedState.Off;
            this._ssl3.TabIndex = 5;
            // 
            // label266
            // 
            this.label266.AutoSize = true;
            this.label266.Location = new System.Drawing.Point(25, 57);
            this.label266.Name = "label266";
            this.label266.Size = new System.Drawing.Size(35, 13);
            this.label266.TabIndex = 4;
            this.label266.Text = "ССЛ3";
            // 
            // _ssl2
            // 
            this._ssl2.Location = new System.Drawing.Point(6, 38);
            this._ssl2.Name = "_ssl2";
            this._ssl2.Size = new System.Drawing.Size(13, 13);
            this._ssl2.State = BEMN.Forms.LedState.Off;
            this._ssl2.TabIndex = 3;
            // 
            // label267
            // 
            this.label267.AutoSize = true;
            this.label267.Location = new System.Drawing.Point(25, 38);
            this.label267.Name = "label267";
            this.label267.Size = new System.Drawing.Size(35, 13);
            this.label267.TabIndex = 2;
            this.label267.Text = "ССЛ2";
            // 
            // _ssl1
            // 
            this._ssl1.Location = new System.Drawing.Point(6, 19);
            this._ssl1.Name = "_ssl1";
            this._ssl1.Size = new System.Drawing.Size(13, 13);
            this._ssl1.State = BEMN.Forms.LedState.Off;
            this._ssl1.TabIndex = 1;
            // 
            // label268
            // 
            this.label268.AutoSize = true;
            this.label268.Location = new System.Drawing.Point(25, 19);
            this.label268.Name = "label268";
            this.label268.Size = new System.Drawing.Size(35, 13);
            this.label268.TabIndex = 0;
            this.label268.Text = "ССЛ1";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this._FminLed8);
            this.groupBox13.Controls.Add(this._FminLed7);
            this.groupBox13.Controls.Add(this._FminLed6);
            this.groupBox13.Controls.Add(this._FminLed5);
            this.groupBox13.Controls.Add(this.label87);
            this.groupBox13.Controls.Add(this.label88);
            this.groupBox13.Controls.Add(this.label89);
            this.groupBox13.Controls.Add(this.label90);
            this.groupBox13.Controls.Add(this.label91);
            this.groupBox13.Controls.Add(this._FminLed4);
            this.groupBox13.Controls.Add(this._FminLed3);
            this.groupBox13.Controls.Add(this._FminLed2);
            this.groupBox13.Controls.Add(this.label92);
            this.groupBox13.Controls.Add(this._FminLed1);
            this.groupBox13.Location = new System.Drawing.Point(194, 111);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(89, 99);
            this.groupBox13.TabIndex = 14;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "F min";
            // 
            // _FminLed8
            // 
            this._FminLed8.Location = new System.Drawing.Point(34, 79);
            this._FminLed8.Name = "_FminLed8";
            this._FminLed8.Size = new System.Drawing.Size(13, 13);
            this._FminLed8.State = BEMN.Forms.LedState.Off;
            this._FminLed8.TabIndex = 24;
            // 
            // _FminLed7
            // 
            this._FminLed7.Location = new System.Drawing.Point(8, 79);
            this._FminLed7.Name = "_FminLed7";
            this._FminLed7.Size = new System.Drawing.Size(13, 13);
            this._FminLed7.State = BEMN.Forms.LedState.Off;
            this._FminLed7.TabIndex = 23;
            // 
            // _FminLed6
            // 
            this._FminLed6.Location = new System.Drawing.Point(34, 63);
            this._FminLed6.Name = "_FminLed6";
            this._FminLed6.Size = new System.Drawing.Size(13, 13);
            this._FminLed6.State = BEMN.Forms.LedState.Off;
            this._FminLed6.TabIndex = 22;
            // 
            // _FminLed5
            // 
            this._FminLed5.Location = new System.Drawing.Point(8, 63);
            this._FminLed5.Name = "_FminLed5";
            this._FminLed5.Size = new System.Drawing.Size(13, 13);
            this._FminLed5.State = BEMN.Forms.LedState.Off;
            this._FminLed5.TabIndex = 21;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(48, 79);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(37, 13);
            this.label87.TabIndex = 20;
            this.label87.Text = "F<<<<";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(48, 63);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(31, 13);
            this.label88.TabIndex = 19;
            this.label88.Text = "F<<<";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(48, 47);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(25, 13);
            this.label89.TabIndex = 18;
            this.label89.Text = "F<<";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(48, 31);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(19, 13);
            this.label90.TabIndex = 17;
            this.label90.Text = "F<";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(26, 14);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(35, 13);
            this.label91.TabIndex = 16;
            this.label91.Text = "Сраб.";
            // 
            // _FminLed4
            // 
            this._FminLed4.Location = new System.Drawing.Point(34, 47);
            this._FminLed4.Name = "_FminLed4";
            this._FminLed4.Size = new System.Drawing.Size(13, 13);
            this._FminLed4.State = BEMN.Forms.LedState.Off;
            this._FminLed4.TabIndex = 6;
            // 
            // _FminLed3
            // 
            this._FminLed3.Location = new System.Drawing.Point(8, 47);
            this._FminLed3.Name = "_FminLed3";
            this._FminLed3.Size = new System.Drawing.Size(13, 13);
            this._FminLed3.State = BEMN.Forms.LedState.Off;
            this._FminLed3.TabIndex = 4;
            // 
            // _FminLed2
            // 
            this._FminLed2.Location = new System.Drawing.Point(34, 31);
            this._FminLed2.Name = "_FminLed2";
            this._FminLed2.Size = new System.Drawing.Size(13, 13);
            this._FminLed2.State = BEMN.Forms.LedState.Off;
            this._FminLed2.TabIndex = 2;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(5, 14);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(23, 13);
            this.label92.TabIndex = 1;
            this.label92.Text = "ИО";
            // 
            // _FminLed1
            // 
            this._FminLed1.Location = new System.Drawing.Point(8, 31);
            this._FminLed1.Name = "_FminLed1";
            this._FminLed1.Size = new System.Drawing.Size(13, 13);
            this._FminLed1.State = BEMN.Forms.LedState.Off;
            this._FminLed1.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._FmaxLed8);
            this.groupBox7.Controls.Add(this._FmaxLed7);
            this.groupBox7.Controls.Add(this._FmaxLed6);
            this.groupBox7.Controls.Add(this._FmaxLed5);
            this.groupBox7.Controls.Add(this.label49);
            this.groupBox7.Controls.Add(this.label52);
            this.groupBox7.Controls.Add(this.label54);
            this.groupBox7.Controls.Add(this.label56);
            this.groupBox7.Controls.Add(this.label85);
            this.groupBox7.Controls.Add(this._FmaxLed4);
            this.groupBox7.Controls.Add(this._FmaxLed3);
            this.groupBox7.Controls.Add(this._FmaxLed2);
            this.groupBox7.Controls.Add(this.label86);
            this.groupBox7.Controls.Add(this._FmaxLed1);
            this.groupBox7.Location = new System.Drawing.Point(102, 111);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(89, 99);
            this.groupBox7.TabIndex = 13;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "F max";
            // 
            // _FmaxLed8
            // 
            this._FmaxLed8.Location = new System.Drawing.Point(34, 79);
            this._FmaxLed8.Name = "_FmaxLed8";
            this._FmaxLed8.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed8.State = BEMN.Forms.LedState.Off;
            this._FmaxLed8.TabIndex = 24;
            // 
            // _FmaxLed7
            // 
            this._FmaxLed7.Location = new System.Drawing.Point(8, 79);
            this._FmaxLed7.Name = "_FmaxLed7";
            this._FmaxLed7.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed7.State = BEMN.Forms.LedState.Off;
            this._FmaxLed7.TabIndex = 23;
            // 
            // _FmaxLed6
            // 
            this._FmaxLed6.Location = new System.Drawing.Point(34, 63);
            this._FmaxLed6.Name = "_FmaxLed6";
            this._FmaxLed6.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed6.State = BEMN.Forms.LedState.Off;
            this._FmaxLed6.TabIndex = 22;
            // 
            // _FmaxLed5
            // 
            this._FmaxLed5.Location = new System.Drawing.Point(8, 63);
            this._FmaxLed5.Name = "_FmaxLed5";
            this._FmaxLed5.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed5.State = BEMN.Forms.LedState.Off;
            this._FmaxLed5.TabIndex = 21;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(48, 79);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(37, 13);
            this.label49.TabIndex = 20;
            this.label49.Text = "F>>>>";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(48, 63);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(31, 13);
            this.label52.TabIndex = 19;
            this.label52.Text = "F>>>";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(48, 47);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(25, 13);
            this.label54.TabIndex = 18;
            this.label54.Text = "F>>";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(48, 31);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(19, 13);
            this.label56.TabIndex = 17;
            this.label56.Text = "F>";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(26, 14);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(35, 13);
            this.label85.TabIndex = 16;
            this.label85.Text = "Сраб.";
            // 
            // _FmaxLed4
            // 
            this._FmaxLed4.Location = new System.Drawing.Point(34, 47);
            this._FmaxLed4.Name = "_FmaxLed4";
            this._FmaxLed4.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed4.State = BEMN.Forms.LedState.Off;
            this._FmaxLed4.TabIndex = 6;
            // 
            // _FmaxLed3
            // 
            this._FmaxLed3.Location = new System.Drawing.Point(8, 47);
            this._FmaxLed3.Name = "_FmaxLed3";
            this._FmaxLed3.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed3.State = BEMN.Forms.LedState.Off;
            this._FmaxLed3.TabIndex = 4;
            // 
            // _FmaxLed2
            // 
            this._FmaxLed2.Location = new System.Drawing.Point(34, 31);
            this._FmaxLed2.Name = "_FmaxLed2";
            this._FmaxLed2.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed2.State = BEMN.Forms.LedState.Off;
            this._FmaxLed2.TabIndex = 2;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(5, 14);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(23, 13);
            this.label86.TabIndex = 1;
            this.label86.Text = "ИО";
            // 
            // _FmaxLed1
            // 
            this._FmaxLed1.Location = new System.Drawing.Point(8, 31);
            this._FmaxLed1.Name = "_FmaxLed1";
            this._FmaxLed1.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed1.State = BEMN.Forms.LedState.Off;
            this._FmaxLed1.TabIndex = 0;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.label129);
            this.groupBox19.Controls.Add(this._faultSignalLed15);
            this.groupBox19.Controls.Add(this.label130);
            this.groupBox19.Controls.Add(this._faultSignalLed14);
            this.groupBox19.Controls.Add(this.label131);
            this.groupBox19.Controls.Add(this._faultSignalLed13);
            this.groupBox19.Controls.Add(this.label132);
            this.groupBox19.Controls.Add(this._faultSignalLed12);
            this.groupBox19.Controls.Add(this.label133);
            this.groupBox19.Controls.Add(this._faultSignalLed11);
            this.groupBox19.Controls.Add(this.label134);
            this.groupBox19.Controls.Add(this._faultSignalLed10);
            this.groupBox19.Controls.Add(this.label136);
            this.groupBox19.Controls.Add(this._faultSignalLed9);
            this.groupBox19.Location = new System.Drawing.Point(420, 6);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(119, 243);
            this.groupBox19.TabIndex = 12;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Сигналы неисправности 2";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(21, 192);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(94, 13);
            this.label129.TabIndex = 31;
            this.label129.Text = "О. задачи логики";
            // 
            // _faultSignalLed15
            // 
            this._faultSignalLed15.Location = new System.Drawing.Point(6, 193);
            this._faultSignalLed15.Name = "_faultSignalLed15";
            this._faultSignalLed15.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed15.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed15.TabIndex = 30;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(20, 167);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(91, 13);
            this.label130.TabIndex = 29;
            this.label130.Text = "О.осциллографа";
            // 
            // _faultSignalLed14
            // 
            this._faultSignalLed14.Location = new System.Drawing.Point(6, 167);
            this._faultSignalLed14.Name = "_faultSignalLed14";
            this._faultSignalLed14.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed14.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed14.TabIndex = 28;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(21, 140);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(50, 13);
            this.label131.TabIndex = 27;
            this.label131.Text = "О. часов";
            // 
            // _faultSignalLed13
            // 
            this._faultSignalLed13.Location = new System.Drawing.Point(6, 141);
            this._faultSignalLed13.Name = "_faultSignalLed13";
            this._faultSignalLed13.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed13.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed13.TabIndex = 26;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(21, 114);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(39, 13);
            this.label132.TabIndex = 25;
            this.label132.Text = "О. ЖА";
            // 
            // _faultSignalLed12
            // 
            this._faultSignalLed12.Location = new System.Drawing.Point(6, 115);
            this._faultSignalLed12.Name = "_faultSignalLed12";
            this._faultSignalLed12.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed12.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed12.TabIndex = 24;
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(21, 88);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(39, 13);
            this.label133.TabIndex = 23;
            this.label133.Text = "О. ЖС";
            // 
            // _faultSignalLed11
            // 
            this._faultSignalLed11.Location = new System.Drawing.Point(6, 89);
            this._faultSignalLed11.Name = "_faultSignalLed11";
            this._faultSignalLed11.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed11.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed11.TabIndex = 22;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(21, 63);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(84, 13);
            this.label134.TabIndex = 21;
            this.label134.Text = "О. коэфф. АЦП";
            // 
            // _faultSignalLed10
            // 
            this._faultSignalLed10.Location = new System.Drawing.Point(6, 63);
            this._faultSignalLed10.Name = "_faultSignalLed10";
            this._faultSignalLed10.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed10.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed10.TabIndex = 18;
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(21, 37);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(90, 13);
            this.label136.TabIndex = 17;
            this.label136.Text = "Ошибка уставок";
            // 
            // _faultSignalLed9
            // 
            this._faultSignalLed9.Location = new System.Drawing.Point(6, 37);
            this._faultSignalLed9.Name = "_faultSignalLed9";
            this._faultSignalLed9.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed9.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed9.TabIndex = 16;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.label121);
            this.groupBox18.Controls.Add(this._faultSignalLed8);
            this.groupBox18.Controls.Add(this.label122);
            this.groupBox18.Controls.Add(this._faultSignalLed7);
            this.groupBox18.Controls.Add(this.label123);
            this.groupBox18.Controls.Add(this._faultSignalLed6);
            this.groupBox18.Controls.Add(this.label124);
            this.groupBox18.Controls.Add(this._faultSignalLed5);
            this.groupBox18.Controls.Add(this.label125);
            this.groupBox18.Controls.Add(this._faultSignalLed4);
            this.groupBox18.Controls.Add(this.label126);
            this.groupBox18.Controls.Add(this._faultSignalLed3);
            this.groupBox18.Controls.Add(this.label127);
            this.groupBox18.Controls.Add(this._faultSignalLed2);
            this.groupBox18.Controls.Add(this.label128);
            this.groupBox18.Controls.Add(this._faultSignalLed1);
            this.groupBox18.Location = new System.Drawing.Point(299, 6);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(115, 243);
            this.groupBox18.TabIndex = 11;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Сигналы неисправности 1";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(21, 219);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(32, 13);
            this.label121.TabIndex = 31;
            this.label121.Text = "МСД";
            // 
            // _faultSignalLed8
            // 
            this._faultSignalLed8.Location = new System.Drawing.Point(6, 219);
            this._faultSignalLed8.Name = "_faultSignalLed8";
            this._faultSignalLed8.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed8.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed8.TabIndex = 30;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(21, 193);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(36, 13);
            this.label122.TabIndex = 29;
            this.label122.Text = "МРВ2";
            // 
            // _faultSignalLed7
            // 
            this._faultSignalLed7.Location = new System.Drawing.Point(6, 193);
            this._faultSignalLed7.Name = "_faultSignalLed7";
            this._faultSignalLed7.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed7.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed7.TabIndex = 28;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(21, 166);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(36, 13);
            this.label123.TabIndex = 27;
            this.label123.Text = "МРВ1";
            // 
            // _faultSignalLed6
            // 
            this._faultSignalLed6.Location = new System.Drawing.Point(6, 167);
            this._faultSignalLed6.Name = "_faultSignalLed6";
            this._faultSignalLed6.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed6.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed6.TabIndex = 26;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(21, 140);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(33, 13);
            this.label124.TabIndex = 25;
            this.label124.Text = "МСА ";
            // 
            // _faultSignalLed5
            // 
            this._faultSignalLed5.Location = new System.Drawing.Point(6, 141);
            this._faultSignalLed5.Name = "_faultSignalLed5";
            this._faultSignalLed5.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed5.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed5.TabIndex = 24;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(21, 114);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(44, 13);
            this.label125.TabIndex = 23;
            this.label125.Text = "Резерв";
            // 
            // _faultSignalLed4
            // 
            this._faultSignalLed4.Location = new System.Drawing.Point(6, 115);
            this._faultSignalLed4.Name = "_faultSignalLed4";
            this._faultSignalLed4.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed4.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed4.TabIndex = 22;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(21, 63);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(70, 13);
            this.label126.TabIndex = 21;
            this.label126.Text = "О.  шины I2c";
            // 
            // _faultSignalLed3
            // 
            this._faultSignalLed3.Location = new System.Drawing.Point(6, 89);
            this._faultSignalLed3.Name = "_faultSignalLed3";
            this._faultSignalLed3.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed3.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed3.TabIndex = 20;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(21, 89);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(88, 13);
            this.label127.TabIndex = 19;
            this.label127.Text = "О. температуры";
            // 
            // _faultSignalLed2
            // 
            this._faultSignalLed2.Location = new System.Drawing.Point(6, 63);
            this._faultSignalLed2.Name = "_faultSignalLed2";
            this._faultSignalLed2.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed2.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed2.TabIndex = 18;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(21, 37);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(73, 13);
            this.label128.TabIndex = 17;
            this.label128.Text = "Ошибка ОЗУ";
            // 
            // _faultSignalLed1
            // 
            this._faultSignalLed1.Location = new System.Drawing.Point(6, 37);
            this._faultSignalLed1.Name = "_faultSignalLed1";
            this._faultSignalLed1.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed1.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed1.TabIndex = 16;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.label117);
            this.groupBox17.Controls.Add(this._faultStateLed4);
            this.groupBox17.Controls.Add(this.label118);
            this.groupBox17.Controls.Add(this._faultStateLed3);
            this.groupBox17.Controls.Add(this.label119);
            this.groupBox17.Controls.Add(this._faultStateLed2);
            this.groupBox17.Controls.Add(this.label120);
            this.groupBox17.Controls.Add(this._faultStateLed1);
            this.groupBox17.Location = new System.Drawing.Point(223, 255);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(163, 136);
            this.groupBox17.TabIndex = 10;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Состояние неисправности";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(21, 100);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(121, 13);
            this.label117.TabIndex = 23;
            this.label117.Text = "Н. измерения частоты";
            // 
            // _faultStateLed4
            // 
            this._faultStateLed4.Location = new System.Drawing.Point(6, 101);
            this._faultStateLed4.Name = "_faultStateLed4";
            this._faultStateLed4.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed4.State = BEMN.Forms.LedState.Off;
            this._faultStateLed4.TabIndex = 22;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(21, 49);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(58, 13);
            this.label118.TabIndex = 21;
            this.label118.Text = "Н. данных";
            // 
            // _faultStateLed3
            // 
            this._faultStateLed3.Location = new System.Drawing.Point(6, 75);
            this._faultStateLed3.Name = "_faultStateLed3";
            this._faultStateLed3.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed3.State = BEMN.Forms.LedState.Off;
            this._faultStateLed3.TabIndex = 20;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(21, 75);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(88, 13);
            this.label119.TabIndex = 19;
            this.label119.Text = "Н. измерения U";
            // 
            // _faultStateLed2
            // 
            this._faultStateLed2.Location = new System.Drawing.Point(6, 49);
            this._faultStateLed2.Name = "_faultStateLed2";
            this._faultStateLed2.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed2.State = BEMN.Forms.LedState.Off;
            this._faultStateLed2.TabIndex = 18;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(21, 23);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(78, 13);
            this.label120.TabIndex = 17;
            this.label120.Text = "Н. устройства";
            // 
            // _faultStateLed1
            // 
            this._faultStateLed1.Location = new System.Drawing.Point(6, 23);
            this._faultStateLed1.Name = "_faultStateLed1";
            this._faultStateLed1.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed1.State = BEMN.Forms.LedState.Off;
            this._faultStateLed1.TabIndex = 16;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.label105);
            this.groupBox16.Controls.Add(this._extDefenseLed8);
            this.groupBox16.Controls.Add(this.label106);
            this.groupBox16.Controls.Add(this._extDefenseLed7);
            this.groupBox16.Controls.Add(this.label107);
            this.groupBox16.Controls.Add(this._extDefenseLed6);
            this.groupBox16.Controls.Add(this.label108);
            this.groupBox16.Controls.Add(this._extDefenseLed5);
            this.groupBox16.Controls.Add(this.label109);
            this.groupBox16.Controls.Add(this._extDefenseLed4);
            this.groupBox16.Controls.Add(this.label110);
            this.groupBox16.Controls.Add(this._extDefenseLed3);
            this.groupBox16.Controls.Add(this.label111);
            this.groupBox16.Controls.Add(this._extDefenseLed2);
            this.groupBox16.Controls.Add(this.label112);
            this.groupBox16.Controls.Add(this._extDefenseLed1);
            this.groupBox16.Location = new System.Drawing.Point(392, 255);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(147, 109);
            this.groupBox16.TabIndex = 9;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Внешние защиты";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(98, 87);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(36, 13);
            this.label105.TabIndex = 47;
            this.label105.Text = "ВЗ - 8";
            // 
            // _extDefenseLed8
            // 
            this._extDefenseLed8.Location = new System.Drawing.Point(79, 86);
            this._extDefenseLed8.Name = "_extDefenseLed8";
            this._extDefenseLed8.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed8.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed8.TabIndex = 46;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(98, 65);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(36, 13);
            this.label106.TabIndex = 45;
            this.label106.Text = "ВЗ - 7";
            // 
            // _extDefenseLed7
            // 
            this._extDefenseLed7.Location = new System.Drawing.Point(79, 65);
            this._extDefenseLed7.Name = "_extDefenseLed7";
            this._extDefenseLed7.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed7.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed7.TabIndex = 44;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(98, 44);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(36, 13);
            this.label107.TabIndex = 43;
            this.label107.Text = "ВЗ - 6";
            // 
            // _extDefenseLed6
            // 
            this._extDefenseLed6.Location = new System.Drawing.Point(79, 44);
            this._extDefenseLed6.Name = "_extDefenseLed6";
            this._extDefenseLed6.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed6.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed6.TabIndex = 42;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(98, 23);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(36, 13);
            this.label108.TabIndex = 41;
            this.label108.Text = "ВЗ - 5";
            // 
            // _extDefenseLed5
            // 
            this._extDefenseLed5.Location = new System.Drawing.Point(79, 23);
            this._extDefenseLed5.Name = "_extDefenseLed5";
            this._extDefenseLed5.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed5.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed5.TabIndex = 40;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(28, 87);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(36, 13);
            this.label109.TabIndex = 39;
            this.label109.Text = "ВЗ - 4";
            // 
            // _extDefenseLed4
            // 
            this._extDefenseLed4.Location = new System.Drawing.Point(9, 86);
            this._extDefenseLed4.Name = "_extDefenseLed4";
            this._extDefenseLed4.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed4.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed4.TabIndex = 38;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(28, 65);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(36, 13);
            this.label110.TabIndex = 37;
            this.label110.Text = "ВЗ - 3";
            // 
            // _extDefenseLed3
            // 
            this._extDefenseLed3.Location = new System.Drawing.Point(9, 65);
            this._extDefenseLed3.Name = "_extDefenseLed3";
            this._extDefenseLed3.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed3.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed3.TabIndex = 36;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(28, 44);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(36, 13);
            this.label111.TabIndex = 35;
            this.label111.Text = "ВЗ - 2";
            // 
            // _extDefenseLed2
            // 
            this._extDefenseLed2.Location = new System.Drawing.Point(9, 44);
            this._extDefenseLed2.Name = "_extDefenseLed2";
            this._extDefenseLed2.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed2.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed2.TabIndex = 34;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(28, 23);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(36, 13);
            this.label112.TabIndex = 33;
            this.label112.Text = "ВЗ - 1";
            // 
            // _extDefenseLed1
            // 
            this._extDefenseLed1.Location = new System.Drawing.Point(9, 23);
            this._extDefenseLed1.Name = "_extDefenseLed1";
            this._extDefenseLed1.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed1.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed1.TabIndex = 32;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._U12Led_4);
            this.groupBox11.Controls.Add(this._U12Led_3);
            this.groupBox11.Controls.Add(this._U12Led_2);
            this.groupBox11.Controls.Add(this._U12Led_1);
            this.groupBox11.Controls.Add(this.label67);
            this.groupBox11.Controls.Add(this.label74);
            this.groupBox11.Controls.Add(this.label75);
            this.groupBox11.Controls.Add(this.label76);
            this.groupBox11.Controls.Add(this.label77);
            this.groupBox11.Controls.Add(this._U12Led_8);
            this.groupBox11.Controls.Add(this._U12Led_7);
            this.groupBox11.Controls.Add(this._U12Led_6);
            this.groupBox11.Controls.Add(this.label78);
            this.groupBox11.Controls.Add(this._U12Led_5);
            this.groupBox11.Location = new System.Drawing.Point(10, 111);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(89, 99);
            this.groupBox11.TabIndex = 5;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "U1 и U2 ";
            // 
            // _U12Led_4
            // 
            this._U12Led_4.Location = new System.Drawing.Point(34, 79);
            this._U12Led_4.Name = "_U12Led_4";
            this._U12Led_4.Size = new System.Drawing.Size(13, 13);
            this._U12Led_4.State = BEMN.Forms.LedState.Off;
            this._U12Led_4.TabIndex = 24;
            // 
            // _U12Led_3
            // 
            this._U12Led_3.Location = new System.Drawing.Point(8, 79);
            this._U12Led_3.Name = "_U12Led_3";
            this._U12Led_3.Size = new System.Drawing.Size(13, 13);
            this._U12Led_3.State = BEMN.Forms.LedState.Off;
            this._U12Led_3.TabIndex = 23;
            // 
            // _U12Led_2
            // 
            this._U12Led_2.Location = new System.Drawing.Point(34, 63);
            this._U12Led_2.Name = "_U12Led_2";
            this._U12Led_2.Size = new System.Drawing.Size(13, 13);
            this._U12Led_2.State = BEMN.Forms.LedState.Off;
            this._U12Led_2.TabIndex = 22;
            // 
            // _U12Led_1
            // 
            this._U12Led_1.Location = new System.Drawing.Point(8, 63);
            this._U12Led_1.Name = "_U12Led_1";
            this._U12Led_1.Size = new System.Drawing.Size(13, 13);
            this._U12Led_1.State = BEMN.Forms.LedState.Off;
            this._U12Led_1.TabIndex = 21;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(48, 79);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(33, 13);
            this.label67.TabIndex = 20;
            this.label67.Text = "U2>>";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(48, 63);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(27, 13);
            this.label74.TabIndex = 19;
            this.label74.Text = "U2>";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(48, 47);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(33, 13);
            this.label75.TabIndex = 18;
            this.label75.Text = "U1<<";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(48, 31);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(27, 13);
            this.label76.TabIndex = 17;
            this.label76.Text = "U1<";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(26, 14);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(35, 13);
            this.label77.TabIndex = 16;
            this.label77.Text = "Сраб.";
            // 
            // _U12Led_8
            // 
            this._U12Led_8.Location = new System.Drawing.Point(34, 47);
            this._U12Led_8.Name = "_U12Led_8";
            this._U12Led_8.Size = new System.Drawing.Size(13, 13);
            this._U12Led_8.State = BEMN.Forms.LedState.Off;
            this._U12Led_8.TabIndex = 6;
            // 
            // _U12Led_7
            // 
            this._U12Led_7.Location = new System.Drawing.Point(8, 47);
            this._U12Led_7.Name = "_U12Led_7";
            this._U12Led_7.Size = new System.Drawing.Size(13, 13);
            this._U12Led_7.State = BEMN.Forms.LedState.Off;
            this._U12Led_7.TabIndex = 4;
            // 
            // _U12Led_6
            // 
            this._U12Led_6.Location = new System.Drawing.Point(34, 31);
            this._U12Led_6.Name = "_U12Led_6";
            this._U12Led_6.Size = new System.Drawing.Size(13, 13);
            this._U12Led_6.State = BEMN.Forms.LedState.Off;
            this._U12Led_6.TabIndex = 2;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(5, 14);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(23, 13);
            this.label78.TabIndex = 1;
            this.label78.Text = "ИО";
            // 
            // _U12Led_5
            // 
            this._U12Led_5.Location = new System.Drawing.Point(8, 31);
            this._U12Led_5.Name = "_U12Led_5";
            this._U12Led_5.Size = new System.Drawing.Size(13, 13);
            this._U12Led_5.State = BEMN.Forms.LedState.Off;
            this._U12Led_5.TabIndex = 0;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._U0maxLed8);
            this.groupBox12.Controls.Add(this._U0maxLed7);
            this.groupBox12.Controls.Add(this._U0maxLed6);
            this.groupBox12.Controls.Add(this._U0maxLed5);
            this.groupBox12.Controls.Add(this.label79);
            this.groupBox12.Controls.Add(this.label80);
            this.groupBox12.Controls.Add(this.label81);
            this.groupBox12.Controls.Add(this.label82);
            this.groupBox12.Controls.Add(this.label83);
            this.groupBox12.Controls.Add(this._U0maxLed4);
            this.groupBox12.Controls.Add(this._U0maxLed3);
            this.groupBox12.Controls.Add(this._U0maxLed2);
            this.groupBox12.Controls.Add(this.label84);
            this.groupBox12.Controls.Add(this._U0maxLed1);
            this.groupBox12.Location = new System.Drawing.Point(194, 6);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(99, 99);
            this.groupBox12.TabIndex = 4;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "U0";
            // 
            // _U0maxLed8
            // 
            this._U0maxLed8.Location = new System.Drawing.Point(35, 79);
            this._U0maxLed8.Name = "_U0maxLed8";
            this._U0maxLed8.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed8.State = BEMN.Forms.LedState.Off;
            this._U0maxLed8.TabIndex = 24;
            // 
            // _U0maxLed7
            // 
            this._U0maxLed7.Location = new System.Drawing.Point(8, 79);
            this._U0maxLed7.Name = "_U0maxLed7";
            this._U0maxLed7.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed7.State = BEMN.Forms.LedState.Off;
            this._U0maxLed7.TabIndex = 23;
            // 
            // _U0maxLed6
            // 
            this._U0maxLed6.Location = new System.Drawing.Point(35, 63);
            this._U0maxLed6.Name = "_U0maxLed6";
            this._U0maxLed6.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed6.State = BEMN.Forms.LedState.Off;
            this._U0maxLed6.TabIndex = 22;
            // 
            // _U0maxLed5
            // 
            this._U0maxLed5.Location = new System.Drawing.Point(8, 63);
            this._U0maxLed5.Name = "_U0maxLed5";
            this._U0maxLed5.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed5.State = BEMN.Forms.LedState.Off;
            this._U0maxLed5.TabIndex = 21;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(50, 79);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(39, 13);
            this.label79.TabIndex = 20;
            this.label79.Text = "U0>>>";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(50, 63);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(39, 13);
            this.label80.TabIndex = 19;
            this.label80.Text = "U0>>>";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(50, 47);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(33, 13);
            this.label81.TabIndex = 18;
            this.label81.Text = "U0>>";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(50, 31);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(27, 13);
            this.label82.TabIndex = 17;
            this.label82.Text = "U0>";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(26, 14);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(35, 13);
            this.label83.TabIndex = 16;
            this.label83.Text = "Сраб.";
            // 
            // _U0maxLed4
            // 
            this._U0maxLed4.Location = new System.Drawing.Point(35, 47);
            this._U0maxLed4.Name = "_U0maxLed4";
            this._U0maxLed4.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed4.State = BEMN.Forms.LedState.Off;
            this._U0maxLed4.TabIndex = 6;
            // 
            // _U0maxLed3
            // 
            this._U0maxLed3.Location = new System.Drawing.Point(8, 47);
            this._U0maxLed3.Name = "_U0maxLed3";
            this._U0maxLed3.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed3.State = BEMN.Forms.LedState.Off;
            this._U0maxLed3.TabIndex = 4;
            // 
            // _U0maxLed2
            // 
            this._U0maxLed2.Location = new System.Drawing.Point(35, 31);
            this._U0maxLed2.Name = "_U0maxLed2";
            this._U0maxLed2.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed2.State = BEMN.Forms.LedState.Off;
            this._U0maxLed2.TabIndex = 2;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(5, 14);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(23, 13);
            this.label84.TabIndex = 1;
            this.label84.Text = "ИО";
            // 
            // _U0maxLed1
            // 
            this._U0maxLed1.Location = new System.Drawing.Point(8, 31);
            this._U0maxLed1.Name = "_U0maxLed1";
            this._U0maxLed1.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed1.State = BEMN.Forms.LedState.Off;
            this._U0maxLed1.TabIndex = 0;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._UminLed8);
            this.groupBox10.Controls.Add(this._UminLed7);
            this.groupBox10.Controls.Add(this._UminLed6);
            this.groupBox10.Controls.Add(this._UminLed5);
            this.groupBox10.Controls.Add(this.label55);
            this.groupBox10.Controls.Add(this.label57);
            this.groupBox10.Controls.Add(this.label63);
            this.groupBox10.Controls.Add(this.label64);
            this.groupBox10.Controls.Add(this.label65);
            this.groupBox10.Controls.Add(this._UminLed4);
            this.groupBox10.Controls.Add(this._UminLed3);
            this.groupBox10.Controls.Add(this._UminLed2);
            this.groupBox10.Controls.Add(this.label66);
            this.groupBox10.Controls.Add(this._UminLed1);
            this.groupBox10.Location = new System.Drawing.Point(102, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(89, 99);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "U min";
            // 
            // _UminLed8
            // 
            this._UminLed8.Location = new System.Drawing.Point(34, 79);
            this._UminLed8.Name = "_UminLed8";
            this._UminLed8.Size = new System.Drawing.Size(13, 13);
            this._UminLed8.State = BEMN.Forms.LedState.Off;
            this._UminLed8.TabIndex = 24;
            // 
            // _UminLed7
            // 
            this._UminLed7.Location = new System.Drawing.Point(8, 79);
            this._UminLed7.Name = "_UminLed7";
            this._UminLed7.Size = new System.Drawing.Size(13, 13);
            this._UminLed7.State = BEMN.Forms.LedState.Off;
            this._UminLed7.TabIndex = 23;
            // 
            // _UminLed6
            // 
            this._UminLed6.Location = new System.Drawing.Point(34, 63);
            this._UminLed6.Name = "_UminLed6";
            this._UminLed6.Size = new System.Drawing.Size(13, 13);
            this._UminLed6.State = BEMN.Forms.LedState.Off;
            this._UminLed6.TabIndex = 22;
            // 
            // _UminLed5
            // 
            this._UminLed5.Location = new System.Drawing.Point(8, 63);
            this._UminLed5.Name = "_UminLed5";
            this._UminLed5.Size = new System.Drawing.Size(13, 13);
            this._UminLed5.State = BEMN.Forms.LedState.Off;
            this._UminLed5.TabIndex = 21;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(48, 79);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(39, 13);
            this.label55.TabIndex = 20;
            this.label55.Text = "U<<<<";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(48, 63);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(33, 13);
            this.label57.TabIndex = 19;
            this.label57.Text = "U<<<";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(48, 47);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(27, 13);
            this.label63.TabIndex = 18;
            this.label63.Text = "U<<";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(48, 31);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(21, 13);
            this.label64.TabIndex = 17;
            this.label64.Text = "U<";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(26, 14);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(35, 13);
            this.label65.TabIndex = 16;
            this.label65.Text = "Сраб.";
            // 
            // _UminLed4
            // 
            this._UminLed4.Location = new System.Drawing.Point(34, 47);
            this._UminLed4.Name = "_UminLed4";
            this._UminLed4.Size = new System.Drawing.Size(13, 13);
            this._UminLed4.State = BEMN.Forms.LedState.Off;
            this._UminLed4.TabIndex = 6;
            // 
            // _UminLed3
            // 
            this._UminLed3.Location = new System.Drawing.Point(8, 47);
            this._UminLed3.Name = "_UminLed3";
            this._UminLed3.Size = new System.Drawing.Size(13, 13);
            this._UminLed3.State = BEMN.Forms.LedState.Off;
            this._UminLed3.TabIndex = 4;
            // 
            // _UminLed2
            // 
            this._UminLed2.Location = new System.Drawing.Point(34, 31);
            this._UminLed2.Name = "_UminLed2";
            this._UminLed2.Size = new System.Drawing.Size(13, 13);
            this._UminLed2.State = BEMN.Forms.LedState.Off;
            this._UminLed2.TabIndex = 2;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(5, 14);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(23, 13);
            this.label66.TabIndex = 1;
            this.label66.Text = "ИО";
            // 
            // _UminLed1
            // 
            this._UminLed1.Location = new System.Drawing.Point(8, 31);
            this._UminLed1.Name = "_UminLed1";
            this._UminLed1.Size = new System.Drawing.Size(13, 13);
            this._UminLed1.State = BEMN.Forms.LedState.Off;
            this._UminLed1.TabIndex = 0;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._UmaxLed8);
            this.groupBox9.Controls.Add(this._UmaxLed7);
            this.groupBox9.Controls.Add(this._UmaxLed6);
            this.groupBox9.Controls.Add(this._UmaxLed5);
            this.groupBox9.Controls.Add(this.label73);
            this.groupBox9.Controls.Add(this.label72);
            this.groupBox9.Controls.Add(this.label71);
            this.groupBox9.Controls.Add(this.label70);
            this.groupBox9.Controls.Add(this.label69);
            this.groupBox9.Controls.Add(this._UmaxLed4);
            this.groupBox9.Controls.Add(this._UmaxLed3);
            this.groupBox9.Controls.Add(this._UmaxLed2);
            this.groupBox9.Controls.Add(this.label68);
            this.groupBox9.Controls.Add(this._UmaxLed1);
            this.groupBox9.Location = new System.Drawing.Point(10, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(89, 99);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "U max";
            // 
            // _UmaxLed8
            // 
            this._UmaxLed8.Location = new System.Drawing.Point(32, 79);
            this._UmaxLed8.Name = "_UmaxLed8";
            this._UmaxLed8.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed8.State = BEMN.Forms.LedState.Off;
            this._UmaxLed8.TabIndex = 24;
            // 
            // _UmaxLed7
            // 
            this._UmaxLed7.Location = new System.Drawing.Point(8, 79);
            this._UmaxLed7.Name = "_UmaxLed7";
            this._UmaxLed7.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed7.State = BEMN.Forms.LedState.Off;
            this._UmaxLed7.TabIndex = 23;
            // 
            // _UmaxLed6
            // 
            this._UmaxLed6.Location = new System.Drawing.Point(32, 63);
            this._UmaxLed6.Name = "_UmaxLed6";
            this._UmaxLed6.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed6.State = BEMN.Forms.LedState.Off;
            this._UmaxLed6.TabIndex = 22;
            // 
            // _UmaxLed5
            // 
            this._UmaxLed5.Location = new System.Drawing.Point(8, 63);
            this._UmaxLed5.Name = "_UmaxLed5";
            this._UmaxLed5.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed5.State = BEMN.Forms.LedState.Off;
            this._UmaxLed5.TabIndex = 21;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(47, 79);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(39, 13);
            this.label73.TabIndex = 20;
            this.label73.Text = "U>>>>";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(47, 63);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(33, 13);
            this.label72.TabIndex = 19;
            this.label72.Text = "U>>>";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(47, 47);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(27, 13);
            this.label71.TabIndex = 18;
            this.label71.Text = "U>>";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(47, 31);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(21, 13);
            this.label70.TabIndex = 17;
            this.label70.Text = "U>";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(26, 14);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(35, 13);
            this.label69.TabIndex = 16;
            this.label69.Text = "Сраб.";
            // 
            // _UmaxLed4
            // 
            this._UmaxLed4.Location = new System.Drawing.Point(32, 47);
            this._UmaxLed4.Name = "_UmaxLed4";
            this._UmaxLed4.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed4.State = BEMN.Forms.LedState.Off;
            this._UmaxLed4.TabIndex = 6;
            // 
            // _UmaxLed3
            // 
            this._UmaxLed3.Location = new System.Drawing.Point(8, 47);
            this._UmaxLed3.Name = "_UmaxLed3";
            this._UmaxLed3.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed3.State = BEMN.Forms.LedState.Off;
            this._UmaxLed3.TabIndex = 4;
            // 
            // _UmaxLed2
            // 
            this._UmaxLed2.Location = new System.Drawing.Point(32, 31);
            this._UmaxLed2.Name = "_UmaxLed2";
            this._UmaxLed2.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed2.State = BEMN.Forms.LedState.Off;
            this._UmaxLed2.TabIndex = 2;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(5, 14);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(23, 13);
            this.label68.TabIndex = 1;
            this.label68.Text = "ИО";
            // 
            // _UmaxLed1
            // 
            this._UmaxLed1.Location = new System.Drawing.Point(8, 31);
            this._UmaxLed1.Name = "_UmaxLed1";
            this._UmaxLed1.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed1.State = BEMN.Forms.LedState.Off;
            this._UmaxLed1.TabIndex = 0;
            // 
            // _analogPage
            // 
            this._analogPage.AllowDrop = true;
            this._analogPage.Controls.Add(this.groupBox1);
            this._analogPage.Controls.Add(this._dateTimeControl);
            this._analogPage.Controls.Add(this.groupBox8);
            this._analogPage.Controls.Add(this.groupBox6);
            this._analogPage.Controls.Add(this.groupBox5);
            this._analogPage.Controls.Add(this.groupBox4);
            this._analogPage.Controls.Add(this.groupBox3);
            this._analogPage.Controls.Add(this.groupBox2);
            this._analogPage.ImageIndex = 0;
            this._analogPage.Location = new System.Drawing.Point(4, 22);
            this._analogPage.Name = "_analogPage";
            this._analogPage.Padding = new System.Windows.Forms.Padding(3);
            this._analogPage.Size = new System.Drawing.Size(635, 422);
            this._analogPage.TabIndex = 0;
            this._analogPage.Text = "Аналоговая БД ";
            this._analogPage.UseVisualStyleBackColor = true;
            // 
            // _dateTimeControl
            // 
            this._dateTimeControl.Location = new System.Drawing.Point(360, 230);
            this._dateTimeControl.Name = "_dateTimeControl";
            this._dateTimeControl.Size = new System.Drawing.Size(201, 166);
            this._dateTimeControl.TabIndex = 13;
            this._dateTimeControl.TimeChanged += new System.Action(this._dateTimeControl_TimeChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._resetInd_But);
            this.groupBox8.Controls.Add(this.button1);
            this.groupBox8.Controls.Add(this.label61);
            this.groupBox8.Controls.Add(this._resGroupLed);
            this.groupBox8.Controls.Add(this._uabcLed);
            this.groupBox8.Controls.Add(this.label94);
            this.groupBox8.Controls.Add(this.label137);
            this.groupBox8.Controls.Add(this.label51);
            this.groupBox8.Controls.Add(this._sjLed);
            this.groupBox8.Controls.Add(this.label58);
            this.groupBox8.Controls.Add(this._faultLed);
            this.groupBox8.Controls.Add(this._ajLed);
            this.groupBox8.Controls.Add(this._resetJA_But);
            this.groupBox8.Controls.Add(this._resetJS_But);
            this.groupBox8.Controls.Add(this._resetFaultBut);
            this.groupBox8.Controls.Add(this._constraintToggleButton);
            this.groupBox8.Controls.Add(this.label50);
            this.groupBox8.Controls.Add(this._mainGroupLed);
            this.groupBox8.Location = new System.Drawing.Point(360, 10);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(267, 214);
            this.groupBox8.TabIndex = 12;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Управляющие сигналы(СДТУ)";
            // 
            // _resetInd_But
            // 
            this._resetInd_But.Location = new System.Drawing.Point(11, 160);
            this._resetInd_But.Name = "_resetInd_But";
            this._resetInd_But.Size = new System.Drawing.Size(245, 23);
            this._resetInd_But.TabIndex = 78;
            this._resetInd_But.Text = "Сброс индикации";
            this._resetInd_But.UseVisualStyleBackColor = true;
            this._resetInd_But.Click += new System.EventHandler(this._resetInd_But_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(181, 44);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 77;
            this.button1.Text = "Перекл.";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(30, 49);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(145, 13);
            this.label61.TabIndex = 76;
            this.label61.Text = "Резервная группа уставок ";
            // 
            // _resGroupLed
            // 
            this._resGroupLed.Location = new System.Drawing.Point(11, 49);
            this._resGroupLed.Name = "_resGroupLed";
            this._resGroupLed.Size = new System.Drawing.Size(13, 13);
            this._resGroupLed.State = BEMN.Forms.LedState.Off;
            this._resGroupLed.TabIndex = 75;
            // 
            // _uabcLed
            // 
            this._uabcLed.Location = new System.Drawing.Point(11, 192);
            this._uabcLed.Name = "_uabcLed";
            this._uabcLed.Size = new System.Drawing.Size(13, 13);
            this._uabcLed.State = BEMN.Forms.LedState.Off;
            this._uabcLed.TabIndex = 71;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(30, 192);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(58, 13);
            this.label94.TabIndex = 70;
            this.label94.Text = "Uabc < 5В";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(30, 107);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(99, 13);
            this.label137.TabIndex = 68;
            this.label137.Text = "Новая запись ЖС";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(30, 136);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(99, 13);
            this.label51.TabIndex = 65;
            this.label51.Text = "Новая запись ЖА";
            // 
            // _sjLed
            // 
            this._sjLed.Location = new System.Drawing.Point(11, 107);
            this._sjLed.Name = "_sjLed";
            this._sjLed.Size = new System.Drawing.Size(13, 13);
            this._sjLed.State = BEMN.Forms.LedState.Off;
            this._sjLed.TabIndex = 64;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(30, 78);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(136, 13);
            this.label58.TabIndex = 63;
            this.label58.Text = "Наличие неисправностей";
            // 
            // _faultLed
            // 
            this._faultLed.Location = new System.Drawing.Point(11, 78);
            this._faultLed.Name = "_faultLed";
            this._faultLed.Size = new System.Drawing.Size(13, 13);
            this._faultLed.State = BEMN.Forms.LedState.Off;
            this._faultLed.TabIndex = 62;
            // 
            // _ajLed
            // 
            this._ajLed.Location = new System.Drawing.Point(11, 136);
            this._ajLed.Name = "_ajLed";
            this._ajLed.Size = new System.Drawing.Size(13, 13);
            this._ajLed.State = BEMN.Forms.LedState.Off;
            this._ajLed.TabIndex = 56;
            // 
            // _resetJA_But
            // 
            this._resetJA_But.Location = new System.Drawing.Point(181, 131);
            this._resetJA_But.Name = "_resetJA_But";
            this._resetJA_But.Size = new System.Drawing.Size(75, 23);
            this._resetJA_But.TabIndex = 53;
            this._resetJA_But.Text = "Сбросить";
            this._resetJA_But.UseVisualStyleBackColor = true;
            this._resetJA_But.Click += new System.EventHandler(this._resetJA_But_Click);
            // 
            // _resetJS_But
            // 
            this._resetJS_But.Location = new System.Drawing.Point(181, 102);
            this._resetJS_But.Name = "_resetJS_But";
            this._resetJS_But.Size = new System.Drawing.Size(75, 23);
            this._resetJS_But.TabIndex = 52;
            this._resetJS_But.Text = "Сбросить";
            this._resetJS_But.UseVisualStyleBackColor = true;
            this._resetJS_But.Click += new System.EventHandler(this._resetJS_But_Click);
            // 
            // _resetFaultBut
            // 
            this._resetFaultBut.Location = new System.Drawing.Point(181, 73);
            this._resetFaultBut.Name = "_resetFaultBut";
            this._resetFaultBut.Size = new System.Drawing.Size(75, 23);
            this._resetFaultBut.TabIndex = 51;
            this._resetFaultBut.Text = "Сбросить";
            this._resetFaultBut.UseVisualStyleBackColor = true;
            this._resetFaultBut.Click += new System.EventHandler(this._resetFaultBut_Click);
            // 
            // _constraintToggleButton
            // 
            this._constraintToggleButton.Location = new System.Drawing.Point(181, 15);
            this._constraintToggleButton.Name = "_constraintToggleButton";
            this._constraintToggleButton.Size = new System.Drawing.Size(75, 23);
            this._constraintToggleButton.TabIndex = 47;
            this._constraintToggleButton.Text = "Перекл.";
            this._constraintToggleButton.UseVisualStyleBackColor = true;
            this._constraintToggleButton.Click += new System.EventHandler(this._constraintToggleButton_Click);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(30, 20);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(140, 13);
            this.label50.TabIndex = 46;
            this.label50.Text = "Основная группа уставок ";
            // 
            // _mainGroupLed
            // 
            this._mainGroupLed.Location = new System.Drawing.Point(11, 20);
            this._mainGroupLed.Name = "_mainGroupLed";
            this._mainGroupLed.Size = new System.Drawing.Size(13, 13);
            this._mainGroupLed.State = BEMN.Forms.LedState.Off;
            this._mainGroupLed.TabIndex = 32;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label104);
            this.groupBox6.Controls.Add(this.label103);
            this.groupBox6.Controls.Add(this.label102);
            this.groupBox6.Controls.Add(this.label101);
            this.groupBox6.Controls.Add(this.label100);
            this.groupBox6.Controls.Add(this.label99);
            this.groupBox6.Controls.Add(this.label98);
            this.groupBox6.Controls.Add(this.label97);
            this.groupBox6.Controls.Add(this.label93);
            this.groupBox6.Controls.Add(this.label62);
            this.groupBox6.Controls.Add(this.label59);
            this.groupBox6.Controls.Add(this._U1);
            this.groupBox6.Controls.Add(this._F);
            this.groupBox6.Controls.Add(this._U2);
            this.groupBox6.Controls.Add(this._U0);
            this.groupBox6.Controls.Add(this._Ubc);
            this.groupBox6.Controls.Add(this._Uc);
            this.groupBox6.Controls.Add(this._Ua);
            this.groupBox6.Controls.Add(this._Uab);
            this.groupBox6.Controls.Add(this._Ub);
            this.groupBox6.Controls.Add(this._Un);
            this.groupBox6.Controls.Add(this._Uca);
            this.groupBox6.Location = new System.Drawing.Point(8, 200);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(346, 122);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Измерения";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(251, 27);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(13, 13);
            this.label104.TabIndex = 29;
            this.label104.Text = "F";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(172, 27);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(21, 13);
            this.label103.TabIndex = 28;
            this.label103.Text = "U1";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(172, 51);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(21, 13);
            this.label102.TabIndex = 21;
            this.label102.Text = "U2";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(86, 76);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(27, 13);
            this.label101.TabIndex = 27;
            this.label101.Text = "Uca";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(172, 75);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(21, 13);
            this.label100.TabIndex = 26;
            this.label100.Text = "U0";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(86, 51);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(27, 13);
            this.label99.TabIndex = 25;
            this.label99.Text = "Ubc";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(86, 27);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(27, 13);
            this.label98.TabIndex = 24;
            this.label98.Text = "Uab";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(7, 76);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(21, 13);
            this.label97.TabIndex = 23;
            this.label97.Text = "Uc";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(7, 51);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(21, 13);
            this.label93.TabIndex = 22;
            this.label93.Text = "Ub";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(7, 27);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(21, 13);
            this.label62.TabIndex = 21;
            this.label62.Text = "Ua";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(7, 100);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(21, 13);
            this.label59.TabIndex = 20;
            this.label59.Text = "Un";
            // 
            // _U1
            // 
            this._U1.Enabled = false;
            this._U1.Location = new System.Drawing.Point(199, 24);
            this._U1.Name = "_U1";
            this._U1.Size = new System.Drawing.Size(46, 20);
            this._U1.TabIndex = 19;
            // 
            // _F
            // 
            this._F.Enabled = false;
            this._F.Location = new System.Drawing.Point(270, 24);
            this._F.Name = "_F";
            this._F.Size = new System.Drawing.Size(46, 20);
            this._F.TabIndex = 18;
            // 
            // _U2
            // 
            this._U2.Enabled = false;
            this._U2.Location = new System.Drawing.Point(199, 48);
            this._U2.Name = "_U2";
            this._U2.Size = new System.Drawing.Size(46, 20);
            this._U2.TabIndex = 17;
            // 
            // _U0
            // 
            this._U0.Enabled = false;
            this._U0.Location = new System.Drawing.Point(199, 72);
            this._U0.Name = "_U0";
            this._U0.Size = new System.Drawing.Size(46, 20);
            this._U0.TabIndex = 16;
            // 
            // _Ubc
            // 
            this._Ubc.Enabled = false;
            this._Ubc.Location = new System.Drawing.Point(119, 48);
            this._Ubc.Name = "_Ubc";
            this._Ubc.Size = new System.Drawing.Size(46, 20);
            this._Ubc.TabIndex = 15;
            // 
            // _Uc
            // 
            this._Uc.Enabled = false;
            this._Uc.Location = new System.Drawing.Point(34, 72);
            this._Uc.Name = "_Uc";
            this._Uc.Size = new System.Drawing.Size(46, 20);
            this._Uc.TabIndex = 14;
            // 
            // _Ua
            // 
            this._Ua.Enabled = false;
            this._Ua.Location = new System.Drawing.Point(34, 24);
            this._Ua.Name = "_Ua";
            this._Ua.Size = new System.Drawing.Size(46, 20);
            this._Ua.TabIndex = 13;
            // 
            // _Uab
            // 
            this._Uab.Enabled = false;
            this._Uab.Location = new System.Drawing.Point(119, 24);
            this._Uab.Name = "_Uab";
            this._Uab.Size = new System.Drawing.Size(46, 20);
            this._Uab.TabIndex = 12;
            // 
            // _Ub
            // 
            this._Ub.Enabled = false;
            this._Ub.Location = new System.Drawing.Point(34, 48);
            this._Ub.Name = "_Ub";
            this._Ub.Size = new System.Drawing.Size(46, 20);
            this._Ub.TabIndex = 11;
            // 
            // _Un
            // 
            this._Un.Enabled = false;
            this._Un.Location = new System.Drawing.Point(34, 96);
            this._Un.Name = "_Un";
            this._Un.Size = new System.Drawing.Size(46, 20);
            this._Un.TabIndex = 9;
            // 
            // _Uca
            // 
            this._Uca.Enabled = false;
            this._Uca.Location = new System.Drawing.Point(119, 72);
            this._Uca.Name = "_Uca";
            this._Uca.Size = new System.Drawing.Size(46, 20);
            this._Uca.TabIndex = 6;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label41);
            this.groupBox5.Controls.Add(this._inL_led8);
            this.groupBox5.Controls.Add(this.label42);
            this.groupBox5.Controls.Add(this._inL_led7);
            this.groupBox5.Controls.Add(this.label43);
            this.groupBox5.Controls.Add(this._inL_led6);
            this.groupBox5.Controls.Add(this.label44);
            this.groupBox5.Controls.Add(this._inL_led5);
            this.groupBox5.Controls.Add(this.label45);
            this.groupBox5.Controls.Add(this._inL_led4);
            this.groupBox5.Controls.Add(this.label46);
            this.groupBox5.Controls.Add(this._inL_led3);
            this.groupBox5.Controls.Add(this.label47);
            this.groupBox5.Controls.Add(this._inL_led2);
            this.groupBox5.Controls.Add(this.label48);
            this.groupBox5.Controls.Add(this._inL_led1);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this._inD_led8);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this._inD_led7);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this._inD_led6);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this._inD_led5);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Controls.Add(this._inD_led4);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this._inD_led3);
            this.groupBox5.Controls.Add(this.label31);
            this.groupBox5.Controls.Add(this._inD_led2);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this._inD_led1);
            this.groupBox5.Location = new System.Drawing.Point(265, 8);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(89, 155);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Входные сигналы";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(64, 135);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(21, 13);
            this.label41.TabIndex = 79;
            this.label41.Text = "Л8";
            // 
            // _inL_led8
            // 
            this._inL_led8.Location = new System.Drawing.Point(51, 135);
            this._inL_led8.Name = "_inL_led8";
            this._inL_led8.Size = new System.Drawing.Size(13, 13);
            this._inL_led8.State = BEMN.Forms.LedState.Off;
            this._inL_led8.TabIndex = 78;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(64, 120);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(21, 13);
            this.label42.TabIndex = 77;
            this.label42.Text = "Л7";
            // 
            // _inL_led7
            // 
            this._inL_led7.Location = new System.Drawing.Point(51, 120);
            this._inL_led7.Name = "_inL_led7";
            this._inL_led7.Size = new System.Drawing.Size(13, 13);
            this._inL_led7.State = BEMN.Forms.LedState.Off;
            this._inL_led7.TabIndex = 76;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(64, 105);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(21, 13);
            this.label43.TabIndex = 75;
            this.label43.Text = "Л6";
            // 
            // _inL_led6
            // 
            this._inL_led6.Location = new System.Drawing.Point(51, 105);
            this._inL_led6.Name = "_inL_led6";
            this._inL_led6.Size = new System.Drawing.Size(13, 13);
            this._inL_led6.State = BEMN.Forms.LedState.Off;
            this._inL_led6.TabIndex = 74;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(64, 90);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(21, 13);
            this.label44.TabIndex = 73;
            this.label44.Text = "Л5";
            // 
            // _inL_led5
            // 
            this._inL_led5.Location = new System.Drawing.Point(51, 90);
            this._inL_led5.Name = "_inL_led5";
            this._inL_led5.Size = new System.Drawing.Size(13, 13);
            this._inL_led5.State = BEMN.Forms.LedState.Off;
            this._inL_led5.TabIndex = 72;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(64, 75);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(21, 13);
            this.label45.TabIndex = 71;
            this.label45.Text = "Л4";
            // 
            // _inL_led4
            // 
            this._inL_led4.Location = new System.Drawing.Point(51, 75);
            this._inL_led4.Name = "_inL_led4";
            this._inL_led4.Size = new System.Drawing.Size(13, 13);
            this._inL_led4.State = BEMN.Forms.LedState.Off;
            this._inL_led4.TabIndex = 70;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(64, 60);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(21, 13);
            this.label46.TabIndex = 69;
            this.label46.Text = "Л3";
            // 
            // _inL_led3
            // 
            this._inL_led3.Location = new System.Drawing.Point(51, 60);
            this._inL_led3.Name = "_inL_led3";
            this._inL_led3.Size = new System.Drawing.Size(13, 13);
            this._inL_led3.State = BEMN.Forms.LedState.Off;
            this._inL_led3.TabIndex = 68;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(64, 45);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(21, 13);
            this.label47.TabIndex = 67;
            this.label47.Text = "Л2";
            // 
            // _inL_led2
            // 
            this._inL_led2.Location = new System.Drawing.Point(51, 45);
            this._inL_led2.Name = "_inL_led2";
            this._inL_led2.Size = new System.Drawing.Size(13, 13);
            this._inL_led2.State = BEMN.Forms.LedState.Off;
            this._inL_led2.TabIndex = 66;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(64, 30);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(21, 13);
            this.label48.TabIndex = 65;
            this.label48.Text = "Л1";
            // 
            // _inL_led1
            // 
            this._inL_led1.Location = new System.Drawing.Point(51, 30);
            this._inL_led1.Name = "_inL_led1";
            this._inL_led1.Size = new System.Drawing.Size(13, 13);
            this._inL_led1.State = BEMN.Forms.LedState.Off;
            this._inL_led1.TabIndex = 64;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(23, 135);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 13);
            this.label25.TabIndex = 47;
            this.label25.Text = "Д8";
            // 
            // _inD_led8
            // 
            this._inD_led8.Location = new System.Drawing.Point(9, 135);
            this._inD_led8.Name = "_inD_led8";
            this._inD_led8.Size = new System.Drawing.Size(13, 13);
            this._inD_led8.State = BEMN.Forms.LedState.Off;
            this._inD_led8.TabIndex = 46;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(23, 120);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(22, 13);
            this.label26.TabIndex = 45;
            this.label26.Text = "Д7";
            // 
            // _inD_led7
            // 
            this._inD_led7.Location = new System.Drawing.Point(9, 120);
            this._inD_led7.Name = "_inD_led7";
            this._inD_led7.Size = new System.Drawing.Size(13, 13);
            this._inD_led7.State = BEMN.Forms.LedState.Off;
            this._inD_led7.TabIndex = 44;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(23, 105);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(22, 13);
            this.label27.TabIndex = 43;
            this.label27.Text = "Д6";
            // 
            // _inD_led6
            // 
            this._inD_led6.Location = new System.Drawing.Point(9, 105);
            this._inD_led6.Name = "_inD_led6";
            this._inD_led6.Size = new System.Drawing.Size(13, 13);
            this._inD_led6.State = BEMN.Forms.LedState.Off;
            this._inD_led6.TabIndex = 42;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(23, 90);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(22, 13);
            this.label28.TabIndex = 41;
            this.label28.Text = "Д5";
            // 
            // _inD_led5
            // 
            this._inD_led5.Location = new System.Drawing.Point(9, 90);
            this._inD_led5.Name = "_inD_led5";
            this._inD_led5.Size = new System.Drawing.Size(13, 13);
            this._inD_led5.State = BEMN.Forms.LedState.Off;
            this._inD_led5.TabIndex = 40;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(23, 75);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(22, 13);
            this.label29.TabIndex = 39;
            this.label29.Text = "Д4";
            // 
            // _inD_led4
            // 
            this._inD_led4.Location = new System.Drawing.Point(9, 75);
            this._inD_led4.Name = "_inD_led4";
            this._inD_led4.Size = new System.Drawing.Size(13, 13);
            this._inD_led4.State = BEMN.Forms.LedState.Off;
            this._inD_led4.TabIndex = 38;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(23, 60);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(22, 13);
            this.label30.TabIndex = 37;
            this.label30.Text = "Д3";
            // 
            // _inD_led3
            // 
            this._inD_led3.Location = new System.Drawing.Point(9, 60);
            this._inD_led3.Name = "_inD_led3";
            this._inD_led3.Size = new System.Drawing.Size(13, 13);
            this._inD_led3.State = BEMN.Forms.LedState.Off;
            this._inD_led3.TabIndex = 36;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(23, 45);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(22, 13);
            this.label31.TabIndex = 35;
            this.label31.Text = "Д2";
            // 
            // _inD_led2
            // 
            this._inD_led2.Location = new System.Drawing.Point(9, 45);
            this._inD_led2.Name = "_inD_led2";
            this._inD_led2.Size = new System.Drawing.Size(13, 13);
            this._inD_led2.State = BEMN.Forms.LedState.Off;
            this._inD_led2.TabIndex = 34;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(23, 30);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(22, 13);
            this.label32.TabIndex = 33;
            this.label32.Text = "Д1";
            // 
            // _inD_led1
            // 
            this._inD_led1.Location = new System.Drawing.Point(9, 30);
            this._inD_led1.Name = "_inD_led1";
            this._inD_led1.Size = new System.Drawing.Size(13, 13);
            this._inD_led1.State = BEMN.Forms.LedState.Off;
            this._inD_led1.TabIndex = 32;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this._outLed8);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this._outLed7);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this._outLed6);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this._outLed5);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this._outLed4);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this._outLed3);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this._outLed2);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this._outLed1);
            this.groupBox4.Location = new System.Drawing.Point(191, 8);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(68, 155);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Вых. сигналы";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(28, 135);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 13);
            this.label17.TabIndex = 47;
            this.label17.Text = "ВЛС8";
            // 
            // _outLed8
            // 
            this._outLed8.Location = new System.Drawing.Point(9, 135);
            this._outLed8.Name = "_outLed8";
            this._outLed8.Size = new System.Drawing.Size(13, 13);
            this._outLed8.State = BEMN.Forms.LedState.Off;
            this._outLed8.TabIndex = 46;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(28, 120);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 13);
            this.label18.TabIndex = 45;
            this.label18.Text = "ВЛС7";
            // 
            // _outLed7
            // 
            this._outLed7.Location = new System.Drawing.Point(9, 120);
            this._outLed7.Name = "_outLed7";
            this._outLed7.Size = new System.Drawing.Size(13, 13);
            this._outLed7.State = BEMN.Forms.LedState.Off;
            this._outLed7.TabIndex = 44;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(28, 105);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 13);
            this.label19.TabIndex = 43;
            this.label19.Text = "ВЛС6";
            // 
            // _outLed6
            // 
            this._outLed6.Location = new System.Drawing.Point(9, 105);
            this._outLed6.Name = "_outLed6";
            this._outLed6.Size = new System.Drawing.Size(13, 13);
            this._outLed6.State = BEMN.Forms.LedState.Off;
            this._outLed6.TabIndex = 42;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(28, 90);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 13);
            this.label20.TabIndex = 41;
            this.label20.Text = "ВЛС5";
            // 
            // _outLed5
            // 
            this._outLed5.Location = new System.Drawing.Point(9, 90);
            this._outLed5.Name = "_outLed5";
            this._outLed5.Size = new System.Drawing.Size(13, 13);
            this._outLed5.State = BEMN.Forms.LedState.Off;
            this._outLed5.TabIndex = 40;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(28, 75);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(35, 13);
            this.label21.TabIndex = 39;
            this.label21.Text = "ВЛС4";
            // 
            // _outLed4
            // 
            this._outLed4.Location = new System.Drawing.Point(9, 75);
            this._outLed4.Name = "_outLed4";
            this._outLed4.Size = new System.Drawing.Size(13, 13);
            this._outLed4.State = BEMN.Forms.LedState.Off;
            this._outLed4.TabIndex = 38;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(28, 60);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 13);
            this.label22.TabIndex = 37;
            this.label22.Text = "ВЛС3";
            // 
            // _outLed3
            // 
            this._outLed3.Location = new System.Drawing.Point(9, 60);
            this._outLed3.Name = "_outLed3";
            this._outLed3.Size = new System.Drawing.Size(13, 13);
            this._outLed3.State = BEMN.Forms.LedState.Off;
            this._outLed3.TabIndex = 36;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(28, 45);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 13);
            this.label23.TabIndex = 35;
            this.label23.Text = "ВЛС2";
            // 
            // _outLed2
            // 
            this._outLed2.Location = new System.Drawing.Point(9, 45);
            this._outLed2.Name = "_outLed2";
            this._outLed2.Size = new System.Drawing.Size(13, 13);
            this._outLed2.State = BEMN.Forms.LedState.Off;
            this._outLed2.TabIndex = 34;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(28, 30);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 13);
            this.label24.TabIndex = 33;
            this.label24.Text = "ВЛС1";
            // 
            // _outLed1
            // 
            this._outLed1.Location = new System.Drawing.Point(9, 30);
            this._outLed1.Name = "_outLed1";
            this._outLed1.Size = new System.Drawing.Size(13, 13);
            this._outLed1.State = BEMN.Forms.LedState.Off;
            this._outLed1.TabIndex = 32;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label53);
            this.groupBox3.Controls.Add(this.label33);
            this.groupBox3.Controls.Add(this._releLed16);
            this.groupBox3.Controls.Add(this._releAlarmLed);
            this.groupBox3.Controls.Add(this.label34);
            this.groupBox3.Controls.Add(this.label60);
            this.groupBox3.Controls.Add(this._releLed15);
            this.groupBox3.Controls.Add(this.label35);
            this.groupBox3.Controls.Add(this._releFaultLed);
            this.groupBox3.Controls.Add(this._releLed14);
            this.groupBox3.Controls.Add(this.label36);
            this.groupBox3.Controls.Add(this._releLed13);
            this.groupBox3.Controls.Add(this.label37);
            this.groupBox3.Controls.Add(this._releLed12);
            this.groupBox3.Controls.Add(this.label38);
            this.groupBox3.Controls.Add(this._releLed11);
            this.groupBox3.Controls.Add(this.label39);
            this.groupBox3.Controls.Add(this._releLed10);
            this.groupBox3.Controls.Add(this.label40);
            this.groupBox3.Controls.Add(this._releLed9);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this._releLed8);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this._releLed7);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this._releLed6);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this._releLed5);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this._releLed4);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this._releLed3);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this._releLed2);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this._releLed1);
            this.groupBox3.Location = new System.Drawing.Point(69, 8);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(116, 186);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Реле";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(25, 41);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(79, 13);
            this.label53.TabIndex = 51;
            this.label53.Text = "Сигнализация";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(59, 165);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(19, 13);
            this.label33.TabIndex = 47;
            this.label33.Text = "16";
            // 
            // _releLed16
            // 
            this._releLed16.Location = new System.Drawing.Point(40, 165);
            this._releLed16.Name = "_releLed16";
            this._releLed16.Size = new System.Drawing.Size(13, 13);
            this._releLed16.State = BEMN.Forms.LedState.Off;
            this._releLed16.TabIndex = 46;
            // 
            // _releAlarmLed
            // 
            this._releAlarmLed.Location = new System.Drawing.Point(6, 22);
            this._releAlarmLed.Name = "_releAlarmLed";
            this._releAlarmLed.Size = new System.Drawing.Size(13, 13);
            this._releAlarmLed.State = BEMN.Forms.LedState.Off;
            this._releAlarmLed.TabIndex = 50;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(59, 150);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(19, 13);
            this.label34.TabIndex = 45;
            this.label34.Text = "15";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(25, 22);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(44, 13);
            this.label60.TabIndex = 49;
            this.label60.Text = "Авария";
            // 
            // _releLed15
            // 
            this._releLed15.Location = new System.Drawing.Point(40, 150);
            this._releLed15.Name = "_releLed15";
            this._releLed15.Size = new System.Drawing.Size(13, 13);
            this._releLed15.State = BEMN.Forms.LedState.Off;
            this._releLed15.TabIndex = 44;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(59, 135);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(19, 13);
            this.label35.TabIndex = 43;
            this.label35.Text = "14";
            // 
            // _releFaultLed
            // 
            this._releFaultLed.Location = new System.Drawing.Point(6, 41);
            this._releFaultLed.Name = "_releFaultLed";
            this._releFaultLed.Size = new System.Drawing.Size(13, 13);
            this._releFaultLed.State = BEMN.Forms.LedState.Off;
            this._releFaultLed.TabIndex = 48;
            // 
            // _releLed14
            // 
            this._releLed14.Location = new System.Drawing.Point(40, 135);
            this._releLed14.Name = "_releLed14";
            this._releLed14.Size = new System.Drawing.Size(13, 13);
            this._releLed14.State = BEMN.Forms.LedState.Off;
            this._releLed14.TabIndex = 42;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(59, 120);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(19, 13);
            this.label36.TabIndex = 41;
            this.label36.Text = "13";
            // 
            // _releLed13
            // 
            this._releLed13.Location = new System.Drawing.Point(40, 120);
            this._releLed13.Name = "_releLed13";
            this._releLed13.Size = new System.Drawing.Size(13, 13);
            this._releLed13.State = BEMN.Forms.LedState.Off;
            this._releLed13.TabIndex = 40;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(59, 105);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(19, 13);
            this.label37.TabIndex = 39;
            this.label37.Text = "12";
            // 
            // _releLed12
            // 
            this._releLed12.Location = new System.Drawing.Point(40, 105);
            this._releLed12.Name = "_releLed12";
            this._releLed12.Size = new System.Drawing.Size(13, 13);
            this._releLed12.State = BEMN.Forms.LedState.Off;
            this._releLed12.TabIndex = 38;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(59, 90);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(19, 13);
            this.label38.TabIndex = 37;
            this.label38.Text = "11";
            // 
            // _releLed11
            // 
            this._releLed11.Location = new System.Drawing.Point(40, 90);
            this._releLed11.Name = "_releLed11";
            this._releLed11.Size = new System.Drawing.Size(13, 13);
            this._releLed11.State = BEMN.Forms.LedState.Off;
            this._releLed11.TabIndex = 36;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(59, 75);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(19, 13);
            this.label39.TabIndex = 35;
            this.label39.Text = "10";
            // 
            // _releLed10
            // 
            this._releLed10.Location = new System.Drawing.Point(40, 75);
            this._releLed10.Name = "_releLed10";
            this._releLed10.Size = new System.Drawing.Size(13, 13);
            this._releLed10.State = BEMN.Forms.LedState.Off;
            this._releLed10.TabIndex = 34;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(59, 60);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(13, 13);
            this.label40.TabIndex = 33;
            this.label40.Text = "9";
            // 
            // _releLed9
            // 
            this._releLed9.Location = new System.Drawing.Point(40, 60);
            this._releLed9.Name = "_releLed9";
            this._releLed9.Size = new System.Drawing.Size(13, 13);
            this._releLed9.State = BEMN.Forms.LedState.Off;
            this._releLed9.TabIndex = 32;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 165);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "8";
            // 
            // _releLed8
            // 
            this._releLed8.Location = new System.Drawing.Point(6, 165);
            this._releLed8.Name = "_releLed8";
            this._releLed8.Size = new System.Drawing.Size(13, 13);
            this._releLed8.State = BEMN.Forms.LedState.Off;
            this._releLed8.TabIndex = 30;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(25, 150);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 29;
            this.label10.Text = "7";
            // 
            // _releLed7
            // 
            this._releLed7.Location = new System.Drawing.Point(6, 150);
            this._releLed7.Name = "_releLed7";
            this._releLed7.Size = new System.Drawing.Size(13, 13);
            this._releLed7.State = BEMN.Forms.LedState.Off;
            this._releLed7.TabIndex = 28;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(25, 135);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "6";
            // 
            // _releLed6
            // 
            this._releLed6.Location = new System.Drawing.Point(6, 135);
            this._releLed6.Name = "_releLed6";
            this._releLed6.Size = new System.Drawing.Size(13, 13);
            this._releLed6.State = BEMN.Forms.LedState.Off;
            this._releLed6.TabIndex = 26;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(25, 120);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 13);
            this.label12.TabIndex = 25;
            this.label12.Text = "5";
            // 
            // _releLed5
            // 
            this._releLed5.Location = new System.Drawing.Point(6, 120);
            this._releLed5.Name = "_releLed5";
            this._releLed5.Size = new System.Drawing.Size(13, 13);
            this._releLed5.State = BEMN.Forms.LedState.Off;
            this._releLed5.TabIndex = 24;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(25, 105);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "4";
            // 
            // _releLed4
            // 
            this._releLed4.Location = new System.Drawing.Point(6, 105);
            this._releLed4.Name = "_releLed4";
            this._releLed4.Size = new System.Drawing.Size(13, 13);
            this._releLed4.State = BEMN.Forms.LedState.Off;
            this._releLed4.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(25, 90);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(13, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "3";
            // 
            // _releLed3
            // 
            this._releLed3.Location = new System.Drawing.Point(6, 90);
            this._releLed3.Name = "_releLed3";
            this._releLed3.Size = new System.Drawing.Size(13, 13);
            this._releLed3.State = BEMN.Forms.LedState.Off;
            this._releLed3.TabIndex = 20;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(25, 75);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 13);
            this.label15.TabIndex = 19;
            this.label15.Text = "2";
            // 
            // _releLed2
            // 
            this._releLed2.Location = new System.Drawing.Point(6, 75);
            this._releLed2.Name = "_releLed2";
            this._releLed2.Size = new System.Drawing.Size(13, 13);
            this._releLed2.State = BEMN.Forms.LedState.Off;
            this._releLed2.TabIndex = 18;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(25, 60);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(13, 13);
            this.label16.TabIndex = 17;
            this.label16.Text = "1";
            // 
            // _releLed1
            // 
            this._releLed1.Location = new System.Drawing.Point(6, 60);
            this._releLed1.Name = "_releLed1";
            this._releLed1.Size = new System.Drawing.Size(13, 13);
            this._releLed1.State = BEMN.Forms.LedState.Off;
            this._releLed1.TabIndex = 16;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this._indLed10);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this._indLed9);
            this.groupBox2.Controls.Add(this.label95);
            this.groupBox2.Controls.Add(this._indLed8);
            this.groupBox2.Controls.Add(this.label96);
            this.groupBox2.Controls.Add(this._indLed7);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this._indLed6);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this._indLed5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this._indLed4);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this._indLed3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this._indLed2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this._indLed1);
            this.groupBox2.Location = new System.Drawing.Point(8, 8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(55, 186);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Инд.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 166);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "10";
            // 
            // _indLed10
            // 
            this._indLed10.Location = new System.Drawing.Point(6, 165);
            this._indLed10.Name = "_indLed10";
            this._indLed10.Size = new System.Drawing.Size(13, 13);
            this._indLed10.State = BEMN.Forms.LedState.Off;
            this._indLed10.TabIndex = 18;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "9";
            // 
            // _indLed9
            // 
            this._indLed9.Location = new System.Drawing.Point(6, 150);
            this._indLed9.Name = "_indLed9";
            this._indLed9.Size = new System.Drawing.Size(13, 13);
            this._indLed9.State = BEMN.Forms.LedState.Off;
            this._indLed9.TabIndex = 16;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(25, 136);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(13, 13);
            this.label95.TabIndex = 15;
            this.label95.Text = "8";
            // 
            // _indLed8
            // 
            this._indLed8.Location = new System.Drawing.Point(6, 135);
            this._indLed8.Name = "_indLed8";
            this._indLed8.Size = new System.Drawing.Size(13, 13);
            this._indLed8.State = BEMN.Forms.LedState.Off;
            this._indLed8.TabIndex = 14;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(25, 121);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(13, 13);
            this.label96.TabIndex = 13;
            this.label96.Text = "7";
            // 
            // _indLed7
            // 
            this._indLed7.Location = new System.Drawing.Point(6, 120);
            this._indLed7.Name = "_indLed7";
            this._indLed7.Size = new System.Drawing.Size(13, 13);
            this._indLed7.State = BEMN.Forms.LedState.Off;
            this._indLed7.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 106);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "6";
            // 
            // _indLed6
            // 
            this._indLed6.Location = new System.Drawing.Point(6, 105);
            this._indLed6.Name = "_indLed6";
            this._indLed6.Size = new System.Drawing.Size(13, 13);
            this._indLed6.State = BEMN.Forms.LedState.Off;
            this._indLed6.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "5";
            // 
            // _indLed5
            // 
            this._indLed5.Location = new System.Drawing.Point(6, 90);
            this._indLed5.Name = "_indLed5";
            this._indLed5.Size = new System.Drawing.Size(13, 13);
            this._indLed5.State = BEMN.Forms.LedState.Off;
            this._indLed5.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "4";
            // 
            // _indLed4
            // 
            this._indLed4.Location = new System.Drawing.Point(6, 75);
            this._indLed4.Name = "_indLed4";
            this._indLed4.Size = new System.Drawing.Size(13, 13);
            this._indLed4.State = BEMN.Forms.LedState.Off;
            this._indLed4.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "3";
            // 
            // _indLed3
            // 
            this._indLed3.Location = new System.Drawing.Point(6, 60);
            this._indLed3.Name = "_indLed3";
            this._indLed3.Size = new System.Drawing.Size(13, 13);
            this._indLed3.State = BEMN.Forms.LedState.Off;
            this._indLed3.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "2";
            // 
            // _indLed2
            // 
            this._indLed2.Location = new System.Drawing.Point(6, 45);
            this._indLed2.Name = "_indLed2";
            this._indLed2.Size = new System.Drawing.Size(13, 13);
            this._indLed2.State = BEMN.Forms.LedState.Off;
            this._indLed2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "1";
            // 
            // _indLed1
            // 
            this._indLed1.Location = new System.Drawing.Point(6, 30);
            this._indLed1.Name = "_indLed1";
            this._indLed1.Size = new System.Drawing.Size(13, 13);
            this._indLed1.State = BEMN.Forms.LedState.Off;
            this._indLed1.TabIndex = 0;
            // 
            // _diagTab
            // 
            this._diagTab.Controls.Add(this._analogPage);
            this._diagTab.Controls.Add(this._diskretPage);
            this._diagTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this._diagTab.Location = new System.Drawing.Point(0, 0);
            this._diagTab.Name = "_diagTab";
            this._diagTab.SelectedIndex = 0;
            this._diagTab.Size = new System.Drawing.Size(643, 448);
            this._diagTab.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.splOffBtn);
            this.groupBox1.Controls.Add(this.slpOnBtn);
            this.groupBox1.Controls.Add(this._splLed);
            this.groupBox1.Controls.Add(this.label113);
            this.groupBox1.Location = new System.Drawing.Point(8, 328);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(342, 75);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Свободно программируемая логика";
            // 
            // slpOnBtn
            // 
            this.slpOnBtn.Location = new System.Drawing.Point(261, 19);
            this.slpOnBtn.Name = "slpOnBtn";
            this.slpOnBtn.Size = new System.Drawing.Size(75, 23);
            this.slpOnBtn.TabIndex = 53;
            this.slpOnBtn.Text = "Включить";
            this.slpOnBtn.UseVisualStyleBackColor = true;
            this.slpOnBtn.Click += new System.EventHandler(this.OnSplBtnClock);
            // 
            // _splLed
            // 
            this._splLed.Location = new System.Drawing.Point(13, 33);
            this._splLed.Name = "_splLed";
            this._splLed.Size = new System.Drawing.Size(13, 13);
            this._splLed.State = BEMN.Forms.LedState.Off;
            this._splLed.TabIndex = 56;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(29, 33);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(229, 13);
            this.label113.TabIndex = 65;
            this.label113.Text = "Работа свободно программируемой логики";
            // 
            // splOffBtn
            // 
            this.splOffBtn.Location = new System.Drawing.Point(261, 45);
            this.splOffBtn.Name = "splOffBtn";
            this.splOffBtn.Size = new System.Drawing.Size(75, 23);
            this.splOffBtn.TabIndex = 53;
            this.splOffBtn.Text = "Выключить";
            this.splOffBtn.UseVisualStyleBackColor = true;
            this.splOffBtn.Click += new System.EventHandler(this.OffSplBtnClock);
            // 
            // Mr600MeasuringFormV2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 448);
            this.Controls.Add(this._diagTab);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(659, 486);
            this.MinimumSize = new System.Drawing.Size(659, 486);
            this.Name = "Mr600MeasuringFormV2";
            this.Text = "Mr600MeasuringFormV2";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Mr600MeasuringFormV2_FormClosing);
            this.Load += new System.EventHandler(this.Mr600MeasuringFormV2_Load);
            this._diskretPage.ResumeLayout(false);
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this._analogPage.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this._diagTab.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage _diskretPage;
        private System.Windows.Forms.GroupBox groupBox13;
        private BEMN.Forms.LedControl _FminLed8;
        private BEMN.Forms.LedControl _FminLed7;
        private BEMN.Forms.LedControl _FminLed6;
        private BEMN.Forms.LedControl _FminLed5;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private BEMN.Forms.LedControl _FminLed4;
        private BEMN.Forms.LedControl _FminLed3;
        private BEMN.Forms.LedControl _FminLed2;
        private System.Windows.Forms.Label label92;
        private BEMN.Forms.LedControl _FminLed1;
        private System.Windows.Forms.GroupBox groupBox7;
        private BEMN.Forms.LedControl _FmaxLed8;
        private BEMN.Forms.LedControl _FmaxLed7;
        private BEMN.Forms.LedControl _FmaxLed6;
        private BEMN.Forms.LedControl _FmaxLed5;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label85;
        private BEMN.Forms.LedControl _FmaxLed4;
        private BEMN.Forms.LedControl _FmaxLed3;
        private BEMN.Forms.LedControl _FmaxLed2;
        private System.Windows.Forms.Label label86;
        private BEMN.Forms.LedControl _FmaxLed1;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label129;
        private BEMN.Forms.LedControl _faultSignalLed15;
        private System.Windows.Forms.Label label130;
        private BEMN.Forms.LedControl _faultSignalLed14;
        private System.Windows.Forms.Label label131;
        private BEMN.Forms.LedControl _faultSignalLed13;
        private System.Windows.Forms.Label label132;
        private BEMN.Forms.LedControl _faultSignalLed12;
        private System.Windows.Forms.Label label133;
        private BEMN.Forms.LedControl _faultSignalLed11;
        private System.Windows.Forms.Label label134;
        private BEMN.Forms.LedControl _faultSignalLed10;
        private System.Windows.Forms.Label label136;
        private BEMN.Forms.LedControl _faultSignalLed9;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Label label121;
        private BEMN.Forms.LedControl _faultSignalLed8;
        private System.Windows.Forms.Label label122;
        private BEMN.Forms.LedControl _faultSignalLed7;
        private System.Windows.Forms.Label label123;
        private BEMN.Forms.LedControl _faultSignalLed6;
        private System.Windows.Forms.Label label124;
        private BEMN.Forms.LedControl _faultSignalLed5;
        private System.Windows.Forms.Label label125;
        private BEMN.Forms.LedControl _faultSignalLed4;
        private System.Windows.Forms.Label label126;
        private BEMN.Forms.LedControl _faultSignalLed3;
        private System.Windows.Forms.Label label127;
        private BEMN.Forms.LedControl _faultSignalLed2;
        private System.Windows.Forms.Label label128;
        private BEMN.Forms.LedControl _faultSignalLed1;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Label label117;
        private BEMN.Forms.LedControl _faultStateLed4;
        private System.Windows.Forms.Label label118;
        private BEMN.Forms.LedControl _faultStateLed3;
        private System.Windows.Forms.Label label119;
        private BEMN.Forms.LedControl _faultStateLed2;
        private System.Windows.Forms.Label label120;
        private BEMN.Forms.LedControl _faultStateLed1;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label label105;
        private BEMN.Forms.LedControl _extDefenseLed8;
        private System.Windows.Forms.Label label106;
        private BEMN.Forms.LedControl _extDefenseLed7;
        private System.Windows.Forms.Label label107;
        private BEMN.Forms.LedControl _extDefenseLed6;
        private System.Windows.Forms.Label label108;
        private BEMN.Forms.LedControl _extDefenseLed5;
        private System.Windows.Forms.Label label109;
        private BEMN.Forms.LedControl _extDefenseLed4;
        private System.Windows.Forms.Label label110;
        private BEMN.Forms.LedControl _extDefenseLed3;
        private System.Windows.Forms.Label label111;
        private BEMN.Forms.LedControl _extDefenseLed2;
        private System.Windows.Forms.Label label112;
        private BEMN.Forms.LedControl _extDefenseLed1;
        private System.Windows.Forms.GroupBox groupBox11;
        private BEMN.Forms.LedControl _U12Led_4;
        private BEMN.Forms.LedControl _U12Led_3;
        private BEMN.Forms.LedControl _U12Led_2;
        private BEMN.Forms.LedControl _U12Led_1;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private BEMN.Forms.LedControl _U12Led_8;
        private BEMN.Forms.LedControl _U12Led_7;
        private BEMN.Forms.LedControl _U12Led_6;
        private System.Windows.Forms.Label label78;
        private BEMN.Forms.LedControl _U12Led_5;
        private System.Windows.Forms.GroupBox groupBox12;
        private BEMN.Forms.LedControl _U0maxLed8;
        private BEMN.Forms.LedControl _U0maxLed7;
        private BEMN.Forms.LedControl _U0maxLed6;
        private BEMN.Forms.LedControl _U0maxLed5;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private BEMN.Forms.LedControl _U0maxLed4;
        private BEMN.Forms.LedControl _U0maxLed3;
        private BEMN.Forms.LedControl _U0maxLed2;
        private System.Windows.Forms.Label label84;
        private BEMN.Forms.LedControl _U0maxLed1;
        private System.Windows.Forms.GroupBox groupBox10;
        private BEMN.Forms.LedControl _UminLed8;
        private BEMN.Forms.LedControl _UminLed7;
        private BEMN.Forms.LedControl _UminLed6;
        private BEMN.Forms.LedControl _UminLed5;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private BEMN.Forms.LedControl _UminLed4;
        private BEMN.Forms.LedControl _UminLed3;
        private BEMN.Forms.LedControl _UminLed2;
        private System.Windows.Forms.Label label66;
        private BEMN.Forms.LedControl _UminLed1;
        private System.Windows.Forms.GroupBox groupBox9;
        private BEMN.Forms.LedControl _UmaxLed8;
        private BEMN.Forms.LedControl _UmaxLed7;
        private BEMN.Forms.LedControl _UmaxLed6;
        private BEMN.Forms.LedControl _UmaxLed5;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private BEMN.Forms.LedControl _UmaxLed4;
        private BEMN.Forms.LedControl _UmaxLed3;
        private BEMN.Forms.LedControl _UmaxLed2;
        private System.Windows.Forms.Label label68;
        private BEMN.Forms.LedControl _UmaxLed1;
        private System.Windows.Forms.TabPage _analogPage;
        private BEMN.Forms.DateTimeControl _dateTimeControl;
        private System.Windows.Forms.GroupBox groupBox8;
        private BEMN.Forms.LedControl _uabcLed;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label51;
        private BEMN.Forms.LedControl _sjLed;
        private System.Windows.Forms.Label label58;
        private BEMN.Forms.LedControl _faultLed;
        private BEMN.Forms.LedControl _ajLed;
        private System.Windows.Forms.Button _resetJA_But;
        private System.Windows.Forms.Button _resetJS_But;
        private System.Windows.Forms.Button _resetFaultBut;
        private System.Windows.Forms.Button _constraintToggleButton;
        private System.Windows.Forms.Label label50;
        private BEMN.Forms.LedControl _mainGroupLed;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox _U1;
        private System.Windows.Forms.TextBox _F;
        private System.Windows.Forms.TextBox _U2;
        private System.Windows.Forms.TextBox _U0;
        private System.Windows.Forms.TextBox _Ubc;
        private System.Windows.Forms.TextBox _Uc;
        private System.Windows.Forms.TextBox _Ua;
        private System.Windows.Forms.TextBox _Uab;
        private System.Windows.Forms.TextBox _Ub;
        private System.Windows.Forms.TextBox _Un;
        private System.Windows.Forms.TextBox _Uca;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label41;
        private BEMN.Forms.LedControl _inL_led8;
        private System.Windows.Forms.Label label42;
        private BEMN.Forms.LedControl _inL_led7;
        private System.Windows.Forms.Label label43;
        private BEMN.Forms.LedControl _inL_led6;
        private System.Windows.Forms.Label label44;
        private BEMN.Forms.LedControl _inL_led5;
        private System.Windows.Forms.Label label45;
        private BEMN.Forms.LedControl _inL_led4;
        private System.Windows.Forms.Label label46;
        private BEMN.Forms.LedControl _inL_led3;
        private System.Windows.Forms.Label label47;
        private BEMN.Forms.LedControl _inL_led2;
        private System.Windows.Forms.Label label48;
        private BEMN.Forms.LedControl _inL_led1;
        private System.Windows.Forms.Label label25;
        private BEMN.Forms.LedControl _inD_led8;
        private System.Windows.Forms.Label label26;
        private BEMN.Forms.LedControl _inD_led7;
        private System.Windows.Forms.Label label27;
        private BEMN.Forms.LedControl _inD_led6;
        private System.Windows.Forms.Label label28;
        private BEMN.Forms.LedControl _inD_led5;
        private System.Windows.Forms.Label label29;
        private BEMN.Forms.LedControl _inD_led4;
        private System.Windows.Forms.Label label30;
        private BEMN.Forms.LedControl _inD_led3;
        private System.Windows.Forms.Label label31;
        private BEMN.Forms.LedControl _inD_led2;
        private System.Windows.Forms.Label label32;
        private BEMN.Forms.LedControl _inD_led1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label17;
        private BEMN.Forms.LedControl _outLed8;
        private System.Windows.Forms.Label label18;
        private BEMN.Forms.LedControl _outLed7;
        private System.Windows.Forms.Label label19;
        private BEMN.Forms.LedControl _outLed6;
        private System.Windows.Forms.Label label20;
        private BEMN.Forms.LedControl _outLed5;
        private System.Windows.Forms.Label label21;
        private BEMN.Forms.LedControl _outLed4;
        private System.Windows.Forms.Label label22;
        private BEMN.Forms.LedControl _outLed3;
        private System.Windows.Forms.Label label23;
        private BEMN.Forms.LedControl _outLed2;
        private System.Windows.Forms.Label label24;
        private BEMN.Forms.LedControl _outLed1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label33;
        private BEMN.Forms.LedControl _releLed16;
        private System.Windows.Forms.Label label34;
        private BEMN.Forms.LedControl _releLed15;
        private System.Windows.Forms.Label label35;
        private BEMN.Forms.LedControl _releLed14;
        private System.Windows.Forms.Label label36;
        private BEMN.Forms.LedControl _releLed13;
        private System.Windows.Forms.Label label37;
        private BEMN.Forms.LedControl _releLed12;
        private System.Windows.Forms.Label label38;
        private BEMN.Forms.LedControl _releLed11;
        private System.Windows.Forms.Label label39;
        private BEMN.Forms.LedControl _releLed10;
        private System.Windows.Forms.Label label40;
        private BEMN.Forms.LedControl _releLed9;
        private System.Windows.Forms.Label label9;
        private BEMN.Forms.LedControl _releLed8;
        private System.Windows.Forms.Label label10;
        private BEMN.Forms.LedControl _releLed7;
        private System.Windows.Forms.Label label11;
        private BEMN.Forms.LedControl _releLed6;
        private System.Windows.Forms.Label label12;
        private BEMN.Forms.LedControl _releLed5;
        private System.Windows.Forms.Label label13;
        private BEMN.Forms.LedControl _releLed4;
        private System.Windows.Forms.Label label14;
        private BEMN.Forms.LedControl _releLed3;
        private System.Windows.Forms.Label label15;
        private BEMN.Forms.LedControl _releLed2;
        private System.Windows.Forms.Label label16;
        private BEMN.Forms.LedControl _releLed1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private BEMN.Forms.LedControl _indLed10;
        private System.Windows.Forms.Label label6;
        private BEMN.Forms.LedControl _indLed9;
        private System.Windows.Forms.Label label95;
        private BEMN.Forms.LedControl _indLed8;
        private System.Windows.Forms.Label label96;
        private BEMN.Forms.LedControl _indLed7;
        private System.Windows.Forms.Label label7;
        private BEMN.Forms.LedControl _indLed6;
        private System.Windows.Forms.Label label8;
        private BEMN.Forms.LedControl _indLed5;
        private System.Windows.Forms.Label label3;
        private BEMN.Forms.LedControl _indLed4;
        private System.Windows.Forms.Label label4;
        private BEMN.Forms.LedControl _indLed3;
        private System.Windows.Forms.Label label2;
        private BEMN.Forms.LedControl _indLed2;
        private System.Windows.Forms.Label label1;
        private BEMN.Forms.LedControl _indLed1;
        private System.Windows.Forms.TabControl _diagTab;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label61;
        private BEMN.Forms.LedControl _resGroupLed;
        private System.Windows.Forms.Button _resetInd_But;
        private System.Windows.Forms.Label label53;
        private BEMN.Forms.LedControl _releAlarmLed;
        private System.Windows.Forms.Label label60;
        private BEMN.Forms.LedControl _releFaultLed;
        private System.Windows.Forms.GroupBox groupBox21;
        private BEMN.Forms.LedControl _ssl24;
        private System.Windows.Forms.Label label245;
        private BEMN.Forms.LedControl _ssl23;
        private System.Windows.Forms.Label label246;
        private BEMN.Forms.LedControl _ssl22;
        private System.Windows.Forms.Label label247;
        private BEMN.Forms.LedControl _ssl21;
        private System.Windows.Forms.Label label248;
        private BEMN.Forms.LedControl _ssl20;
        private System.Windows.Forms.Label label249;
        private BEMN.Forms.LedControl _ssl19;
        private System.Windows.Forms.Label label250;
        private BEMN.Forms.LedControl _ssl18;
        private System.Windows.Forms.Label label251;
        private BEMN.Forms.LedControl _ssl17;
        private System.Windows.Forms.Label label252;
        private BEMN.Forms.LedControl _ssl16;
        private System.Windows.Forms.Label label253;
        private BEMN.Forms.LedControl _ssl15;
        private System.Windows.Forms.Label label254;
        private BEMN.Forms.LedControl _ssl14;
        private System.Windows.Forms.Label label255;
        private BEMN.Forms.LedControl _ssl13;
        private System.Windows.Forms.Label label256;
        private BEMN.Forms.LedControl _ssl12;
        private System.Windows.Forms.Label label257;
        private BEMN.Forms.LedControl _ssl11;
        private System.Windows.Forms.Label label258;
        private BEMN.Forms.LedControl _ssl10;
        private System.Windows.Forms.Label label259;
        private BEMN.Forms.LedControl _ssl9;
        private System.Windows.Forms.Label label260;
        private BEMN.Forms.LedControl _ssl8;
        private System.Windows.Forms.Label label261;
        private BEMN.Forms.LedControl _ssl7;
        private System.Windows.Forms.Label label262;
        private BEMN.Forms.LedControl _ssl6;
        private System.Windows.Forms.Label label263;
        private BEMN.Forms.LedControl _ssl5;
        private System.Windows.Forms.Label label264;
        private BEMN.Forms.LedControl _ssl4;
        private System.Windows.Forms.Label label265;
        private BEMN.Forms.LedControl _ssl3;
        private System.Windows.Forms.Label label266;
        private BEMN.Forms.LedControl _ssl2;
        private System.Windows.Forms.Label label267;
        private BEMN.Forms.LedControl _ssl1;
        private System.Windows.Forms.Label label268;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button splOffBtn;
        private System.Windows.Forms.Button slpOnBtn;
        private BEMN.Forms.LedControl _splLed;
        private System.Windows.Forms.Label label113;

    }
}