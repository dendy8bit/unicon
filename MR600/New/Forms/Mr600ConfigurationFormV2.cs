﻿using System;
using System.Collections;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR600.New.HelpClasses;
using BEMN.MR600.New.Structs.ConfigurationStructs;
using BEMN.MR600.New.Structs.ConfigurationStructs.Primary;
using BEMN.MR600.New.Structs.ConfigurationStructs.Second;

namespace BEMN.MR600.New.Forms
{
    public partial class Mr600ConfigurationFormV2 : Form, IFormView
    {
        #region [Constants]
        private const string SETPOINTS_READ_OK = "Конфигурация успешно прочитана";
        private const string SETPOINTS_READ_FAIL = "Уставки невозможно прочитать";
        private const string FILE_SAVE_FAIL = "Невозможно сохранить файл";
        private const string FILE_LOAD_FAIL = "Невозможно загрузить файл";
        private const string XML_HEAD = "MR600V3_SET_POINTS";
        private const string WRITE_FAIL = "Невозможно записать конфигурацию";
        private const string WRITE_OK = "Конфигурация успешно записана";
        private const string PARAMETR_ERROR_PATTERN = "При чтении конфигурации из устройства , в параметр {0} было обнаружено недопустимое значение. В Униконе было востановлено значение по умолчанию.";
        private const string TN_FAULT = "Неисправность ТН";
        private const string TNNP_FAULT = "Неисправность ТННП";
        private const string LOGIC_SIGNALS_FAULT = "Логические сигналы";
        private const string RESET_STAGE_FAULT = "Сброс ступени";
        private const string RESET_INDICATION_FAULT = "Сброс индикации";
        private const string RESET_SETPOINTS_FAULT = "Переключение на резервные уставки";
        private const string DEFENSES_FAULT_PATTERN = "При чтении конфигурации из устройства ,{0} в строке {1} было обнаружено недопустимое значение. В Униконе было востановлено значение по умолчанию.";
        private const string EXTERNAL_DEFENSE = "Внешние защиты";
        private const string VOLTAGE_DEFENCE = "Защиты по напряжению";
        private const string FREQUENCY_DEFENSE = "Защиты по частоте";
        private const string ALARN_RELAY = "Реле Сигнализации";
        private const string FAULT_RELAY = "Реле Авария";
        private const string OSC_LEN = "Длит. периода осц.";
        private const string OSC_FIXATION = "Фиксация осц.";
        private const string WRITTING_CONFIG = "Записать конфигурацию в устройство?";
        private const string WRITE_CONFIG = "Запись конфигурации";
        private const string START_READ = "Идет чтение";
        private const string START_WRITE = "Идет запись";

        #endregion [Constants]
        

        #region [Private fields]
        private readonly MemoryEntity<RamMemoryStruct> _ramMemory;
        private RamMemoryStruct _currentStruct;
        private readonly MR600 _device;
        private RadioButtonSelector _groupSelector;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr600ConfigurationFormV2()
        {
            InitializeComponent();
        }

        public Mr600ConfigurationFormV2(MR600 device)
        {
            InitializeComponent();
            this._currentStruct = new RamMemoryStruct(true);
            this._ramMemory = device.Mr600DeviceV2.RamMemory;
            this._ramMemory.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadSuccessful);
            this._ramMemory.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, ReadFail);

            this._ramMemory.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, WriteSuccessful);
            this._ramMemory.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, WriteFail);

            Action a = HandlerHelper.CreateActionHandler(this,() => _exchangeProgressBar.PerformStep());
            this._ramMemory.ReadOk += o => { if (this.IsHandleCreated) this.Invoke(a); };
            this._ramMemory.WriteOk += o => { if (this.IsHandleCreated) this.Invoke(a); };
            this.Prepare();
            this._device = device;

            _groupSelector = new RadioButtonSelector(_mainRadioBtnGroup, _reserveRadioBtnGroup, _btnGroupChange);
            _groupSelector.NeedCopyGroup += _groupSelector_NeedCopyGroup;
        }

        void _groupSelector_NeedCopyGroup()
        {
            //Основные в резервные, иначе наоборот
            if (_groupSelector.SelectedGroup == 0)
            {
                this.WriteVoltageDefenses(1);
                this.WriteFrequenceDefenses(1);
                this.WriteExternalDefenses(1);
            }
            else
            {
                this.WriteVoltageDefenses(0);
                this.WriteFrequenceDefenses(0);
                this.WriteExternalDefenses(0);
            }
        }

        private void WriteFail()
        {
            MessageBox.Show(WRITE_FAIL);
        }

        private void WriteSuccessful()
        {
            this._statusLabel.Text = WRITE_OK;
        }


        /// <summary>
        /// Подготовка
        /// </summary>
        private void Prepare()
        {
            _diskretValueCol.DataSource = StringsV2.LogicState;
            //Измерительный канал
            this._defectTN_CB.DataSource = StringsV2.InputSignals;
            this._defectTNNP_CB.DataSource = StringsV2.InputSignals;
            //Дополнительные сигналы
            this._inputSignalsTN_DispepairCombo.DataSource = StringsV2.InputSignals;
            this._inputSignalsIndicationResetCombo.DataSource = StringsV2.InputSignals;
            this._inputSignalsConstraintResetCombo.DataSource = StringsV2.InputSignals;

            // Ключи
            for (int i = 0; i < 16; i++)
            {
                this._keysDataGrid.Rows.Add(i + 1, StringsV2.YesNo[0]);
            }
            //Логические сигналы
            this._logicChannelsCombo.SelectedIndex = 0;
            this._logicChannelsCombo.SelectedIndexChanged += this._logicChannelsCombo_SelectedIndexChanged;
            for (int i = 0; i < 8; i++)
            {
                this._logicSignalsDataGrid.Rows.Add(i + 1, StringsV2.LogicState[0]);
            }
            //Выходные сигналы
            this._releSignalCol.DataSource = StringsV2.OutputSignals;
            this._releTypeCol.DataSource = StringsV2.RelayType;
            //Индикаторы
            this._outIndTypeCol.DataSource = StringsV2.RelayType;
            this._outIndSignalCol.DataSource = StringsV2.OutputSignals;


            //Внешние защиты
            this._exDefModeCol.DataSource = StringsV2.VoltageDefenseMode;
            this._exDefBlockingCol.DataSource = StringsV2.ExternalDefensesSignals;
            this._exDefWorkingCol.DataSource = StringsV2.ExternalDefensesSignals;
            this._exDefReturnNumberCol.DataSource = StringsV2.ExternalDefensesSignals;
            this.ShowExternalDefenses(0);
            //Реле
            this.ShowRelays();

            //Защиты по напряжению
            //U
            this._voltageDefensesModeCol1.DataSource = StringsV2.VoltageDefenseMode;
            this._voltageDefensesBlockingNumberCol1.DataSource = StringsV2.InputSignals;
            this._voltageDefensesParameterCol1.DataSource = StringsV2.ParametrU;
            this._voltageDefensesOSCv11Col1.DataSource = StringsV2.VoltageDefenseOsc;
            //U0
            this.dataGridViewComboBoxColumn1.DataSource = StringsV2.VoltageDefenseMode;
            this.dataGridViewComboBoxColumn2.DataSource = StringsV2.InputSignals;
            this.dataGridViewComboBoxColumn3.DataSource = StringsV2.ParametrU0;
            this.dataGridViewComboBoxColumn4.DataSource = StringsV2.VoltageDefenseOsc;
            //U1-U2
            this.dataGridViewComboBoxColumn5.DataSource = StringsV2.VoltageDefenseMode;
            this.dataGridViewComboBoxColumn6.DataSource = StringsV2.InputSignals;
            //НЕТ ПАРАМЕТРА
            this.dataGridViewComboBoxColumn8.DataSource = StringsV2.VoltageDefenseOsc;
            this.ShowVoltageDefenses(0);

            //Блокировка по U
            this._U_1_CB.DataSource = StringsV2.OffOn;
            this._U_2_CB.DataSource = StringsV2.OffOn;
            this._U_3_CB.DataSource = StringsV2.OffOn;
            this._U_4_CB.DataSource = StringsV2.OffOn;
            this._U_1_1_CB.DataSource = StringsV2.OffOn;
            this._U_1_2_CB.DataSource = StringsV2.OffOn;
            this._U_1_CB.SelectedIndex = 0;
            this._U_2_CB.SelectedIndex = 0;
            this._U_3_CB.SelectedIndex = 0;
            this._U_4_CB.SelectedIndex = 0;
            this._U_1_1_CB.SelectedIndex = 0;
            this._U_1_2_CB.SelectedIndex = 0;

            //Защиты по частоте
            this._frequenceDefensesMode.DataSource = StringsV2.VoltageDefenseMode;
            this._frequenceDefensesBlockingNumber.DataSource = StringsV2.ExternalDefensesSignals;
            this._frequenceDefensesOSCv11.DataSource = StringsV2.VoltageDefenseOsc;
            this.ShowFrequenceDefenses(0);

            //Логические сигналы
            this._outputLogicCheckList.DataSource = StringsV2.OutputLogicSignals;
            this._outputLogicCombo.SelectedIndex = 0;

            //Осц
            _oscLength.DataSource = StringsV2.OscPeriodDuration;
            comboBox3.DataSource = StringsV2.OscFicsation;

            this.ShowIndicators();
            ShowOscConfig();

        }
        #endregion [Ctor's]


        #region [Ввод-вывод уставок]

        /// <summary>
        /// Уставки успешно прочитаны
        /// </summary>
        private void ReadSuccessful()
        {

            this._currentStruct = this._ramMemory.Value;
            this.ShowAll();
            this._statusLabel.Text = SETPOINTS_READ_OK;
        }
        /// <summary>
        /// Уставки невозможно прочитать
        /// </summary>
        private void ReadFail()
        {
            this._statusLabel.Text = SETPOINTS_READ_FAIL;
        }
        /// <summary>
        /// Выводит все значения
        /// </summary>
        private void ShowAll()
        {
            // измерительный канал
            this.ShowMeasuringChannel();

            //Ключи
            this.ShowLogicKeys(_currentStruct.RomMeasuring.RomFreeLogicKey);

            //Логические сигналы
            int index = this._logicChannelsCombo.SelectedIndex;
            this.ShowLogicValues(index);

            //Дополнительные сигналы
            this.ShowAddedSignals();

            int externalGroup = this._mainRadioBtnGroup.Checked ? 0 : 1;
            this.ShowExternalDefenses(externalGroup);


            int voltageGroup = this._mainRadioBtnGroup.Checked ? 0 : 1;
            this.ShowVoltageDefenses(voltageGroup);

            int frequenceGroup = this._mainRadioBtnGroup.Checked ? 0 : 1;
            this.ShowFrequenceDefenses(frequenceGroup);

            this.ShowOutputLogicSignals(_outputLogicCombo.SelectedIndex);

            this.ShowRelays();

            this.ShowIndicators();

            this.ShowSignalFault();

            ShowOscConfig();
        }
        /// <summary>
        /// Дополнительные сигналы
        /// </summary>
        private void ShowAddedSignals()
        {
            try
            {
                this._inputSignalsTN_DispepairCombo.SelectedItem = this._currentStruct.OutputAddedRelays.Reset;
            }
            catch (Exception)
            {
                this._inputSignalsTN_DispepairCombo.SelectedItem = 0;
                MessageBox.Show(string.Format(PARAMETR_ERROR_PATTERN, RESET_STAGE_FAULT));
            }

            try
            {
                this._inputSignalsIndicationResetCombo.SelectedIndex =
                    this._currentStruct.RomExternalSignals.ResetIndication;
            }
            catch (Exception)
            {

                this._inputSignalsIndicationResetCombo.SelectedIndex = 0;
                MessageBox.Show(string.Format(PARAMETR_ERROR_PATTERN, RESET_INDICATION_FAULT));
                
            }

            try
            {
                this._inputSignalsConstraintResetCombo.SelectedIndex =
                    this._currentStruct.RomExternalSignals.ReservSetpoints;
            }
            catch (Exception)
            {
                this._inputSignalsConstraintResetCombo.SelectedIndex = 0;
                MessageBox.Show(string.Format(PARAMETR_ERROR_PATTERN, RESET_SETPOINTS_FAULT));
    
            }

            
        }

        /// <summary>
        /// измерительный канал
        /// </summary>
        private void ShowMeasuringChannel()
        {
            try
            {
                this._defectTN_CB.SelectedIndex = this._currentStruct.RomMeasuring.RomErrorsU;
            }
            catch (Exception)
            {
                this._defectTN_CB.SelectedIndex = 0;
                MessageBox.Show(string.Format(PARAMETR_ERROR_PATTERN,TN_FAULT));
            }

            try
            {
                this._defectTNNP_CB.SelectedIndex = this._currentStruct.RomMeasuring.RomErrorsUo;
            }
            catch (Exception)
            {
                this._defectTNNP_CB.SelectedIndex = 0;
                MessageBox.Show(string.Format(PARAMETR_ERROR_PATTERN, TNNP_FAULT));
            }
            
            this._TN_Box.Text = (this._currentStruct.RomMeasuring.RomFactorU).ToString(CultureInfo.CurrentUICulture);
            this._TNNP_Box.Text = this._currentStruct.RomMeasuring.RomFactorUo.ToString(CultureInfo.CurrentUICulture);
        }

        /// <summary>
        /// Запись конфигурации осц
        /// </summary>
        private void WriteOscConfig()
        {
            this._currentStruct.SystemConfiguration.Duration = (ushort) _oscLength.SelectedIndex;
            this._currentStruct.SystemConfiguration.Fixation = (ushort) comboBox3.SelectedIndex;
            this._currentStruct.SystemConfiguration.PreRecording = ushort.Parse(_oscWriteLength.Text);
        }

        /// <summary>
        /// Вывод конфигурации осц
        /// </summary>
        private void ShowOscConfig()
        {
            try
            {
              _oscLength.SelectedIndex = this._currentStruct.SystemConfiguration.Duration;
            }
            catch (Exception)
            {
                _oscLength.SelectedIndex = 0;
                MessageBox.Show(string.Format(PARAMETR_ERROR_PATTERN, OSC_LEN));
            }

            try
            {
                comboBox3.SelectedIndex = this._currentStruct.SystemConfiguration.Fixation;
            }
            catch (Exception)
            {
                comboBox3.SelectedIndex = 0;
                MessageBox.Show(string.Format(PARAMETR_ERROR_PATTERN, OSC_FIXATION));
            }
            
            
            _oscWriteLength.Text = this._currentStruct.SystemConfiguration.PreRecording.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Запись сигнала неисправность
        /// </summary>
        private void WriteSignalFault()
        {
            var bits = new bool[8];
            for (int i = 0; i < 8; i++)
            {
                bits[i] = _dispepairCheckList.GetItemChecked(i);
            }
            this._currentStruct.RomExternalSignals.RomDispepair.Config = bits;
            this._currentStruct.RomExternalSignals.RomDispepair.WaitTime = int.Parse(_outputSignalsDispepairTimeBox.Text);
        }

        /// <summary>
        /// Вывод сигнала неисправность
        /// </summary>
        private void ShowSignalFault()
        {
            _outputSignalsDispepairTimeBox.Text = this._currentStruct.RomExternalSignals.RomDispepair.WaitTime.ToString(CultureInfo.InvariantCulture);

            var bits = this._currentStruct.RomExternalSignals.RomDispepair.Config;

            for (int i = 0; i < _dispepairCheckList.Items.Count; i++)
            {
                _dispepairCheckList.SetItemChecked(i, bits[i]);
            }
        }

        /// <summary>
        /// Вывод индикаторов
        /// </summary>
        private void ShowIndicators()
        {
            _outputIndicatorsGrid.Rows.Clear();

            var indicators = this._currentStruct.AllIndicators.Indicators;
            for (int i = 0; i < indicators.Length; i++)
            {
                try
                {
                    _outputIndicatorsGrid.Rows.Add(
                        i + 1,
                        indicators[i].Type,
                        indicators[i].Signal,
                        indicators[i].Ind,
                        indicators[i].Sj,
                        indicators[i].Aj
                        );
                }
                catch (Exception)
                {
                    indicators[i] = new IndicatorsStruct();
                    this._currentStruct.AllIndicators.Indicators = indicators;
                    MessageBox.Show(string.Format(DEFENSES_FAULT_PATTERN, "Индикаторы", i + 1));
                    i--;
                }
                
            }

        }

        /// <summary>
        /// Запись индикаторов
        /// </summary>
        private void WriteIndicators()
        {
            var allIndicators = new AllIndicatorsStruct(true);
            for (int i = 0; i < allIndicators.Indicators.Length; i++)
            {
                allIndicators.Indicators[i].Type = this._outputIndicatorsGrid[1, i].Value.ToString();
                allIndicators.Indicators[i].Signal = this._outputIndicatorsGrid[2, i].Value.ToString();
                allIndicators.Indicators[i].Ind = Convert.ToBoolean(this._outputIndicatorsGrid[3, i].Value);
                allIndicators.Indicators[i].Sj = Convert.ToBoolean(this._outputIndicatorsGrid[4, i].Value);
                allIndicators.Indicators[i].Aj = Convert.ToBoolean(this._outputIndicatorsGrid[5, i].Value);
            }
            this._currentStruct.AllIndicators = allIndicators;
        }

        /// <summary>
        /// Вывод внешних логических сигналов
        /// </summary>
        private void ShowOutputLogicSignals(int number)
        {
            BitArray bits = this._currentStruct.AllOutputLogicSignal.LogicSignalsStruct[number].Bits;
            for (int i = 0; i < _outputLogicCheckList.Items.Count; i++)
            {
                _outputLogicCheckList.SetItemChecked(i, bits[i]);
            }

        }

        /// <summary>
        /// Запись внешних логических сигналов
        /// </summary>
        private void WriteOutputLogicSignals(int number)
        {
            var count = _outputLogicCheckList.Items.Count;
            var bits = new bool[count];
            for (int i = 0; i < count; i++)
            {
                bits[i] = _outputLogicCheckList.GetItemChecked(i);
            }
            this._currentStruct.AllOutputLogicSignal.LogicSignalsStruct[number].Bits = new BitArray(bits);
        }

        /// <summary>
        /// Вывод защит по частоте
        /// </summary>
        private void ShowFrequenceDefenses(int group)
        {
            _frequenceDefensesGrid.Rows.Clear();

            var frequenceDefenses =
                this._currentStruct.AllFrequencyDefenseConfiguration.FrequencyDefenseConfiguration[group]
                    .FrequencyDefenses;


            for (int i = 0; i < frequenceDefenses.Length; i++)
            {
                try
                {
                    this._frequenceDefensesGrid.Rows.Add(
                        StringsV2.FrequenceDefensesNames[i],
                        frequenceDefenses[i].Mode,
                        frequenceDefenses[i].RomExtBlock,
                        frequenceDefenses[i].RomExtInput,
                        frequenceDefenses[i].RomExtWait,
                        frequenceDefenses[i].Recovery,
                        frequenceDefenses[i].RomExtWaitAdd,
                        frequenceDefenses[i].RomExtInputAdd,
                        frequenceDefenses[i].Osc,
                        frequenceDefenses[i].Reset
                        );
                }
                catch (Exception)
                {
                    frequenceDefenses[i] = new FrequencyDefenseStruct();
                    this._currentStruct.AllFrequencyDefenseConfiguration.FrequencyDefenseConfiguration[group].FrequencyDefenses = frequenceDefenses;
                    MessageBox.Show(string.Format(DEFENSES_FAULT_PATTERN, FREQUENCY_DEFENSE, i+1));
                    i--;
                }

            }
        }
        /// <summary>
        /// Запись защит по частоте
        /// </summary>
        private void WriteFrequenceDefenses(int group)
        {
            var configurationStruct = new FrequencyDefenseConfigurationStruct(true);
            for (int i = 0; i < configurationStruct.FrequencyDefenses.Length; i++)
            {
                configurationStruct.FrequencyDefenses[i].Mode = this._frequenceDefensesGrid[1, i].Value.ToString();
                configurationStruct.FrequencyDefenses[i].RomExtBlock = this._frequenceDefensesGrid[2, i].Value.ToString();
                configurationStruct.FrequencyDefenses[i].RomExtInput = Convert.ToDouble(this._frequenceDefensesGrid[3, i].Value);
                configurationStruct.FrequencyDefenses[i].RomExtWait = Convert.ToInt32(this._frequenceDefensesGrid[4, i].Value);
                configurationStruct.FrequencyDefenses[i].Recovery = Convert.ToBoolean(this._frequenceDefensesGrid[5, i].Value);
                configurationStruct.FrequencyDefenses[i].RomExtWaitAdd = Convert.ToInt32(this._frequenceDefensesGrid[6, i].Value);
                configurationStruct.FrequencyDefenses[i].RomExtInputAdd = Convert.ToDouble(this._frequenceDefensesGrid[7, i].Value);
                configurationStruct.FrequencyDefenses[i].Osc = this._frequenceDefensesGrid[8, i].Value.ToString();
                configurationStruct.FrequencyDefenses[i].Reset = Convert.ToBoolean(this._frequenceDefensesGrid[9, i].Value);
            }

            this._currentStruct.AllFrequencyDefenseConfiguration.FrequencyDefenseConfiguration[group] = configurationStruct;
        }

        /// <summary>
        /// Вывод защит по напряжению
        /// </summary>
        private void ShowVoltageDefenses(int group)
        {
            this._voltageDefensesGrid1.Rows.Clear();
            this.dataGridView1.Rows.Clear();
            this.dataGridView2.Rows.Clear();
            var voltageDefenses =
                this._currentStruct.AllVoltageDefenseConfiguration.VoltageDefenseConfiguration[group].VoltageDefenses;
            //БЛОКИРОВКА ПО U< 5В
            this._U_1_CB.SelectedIndex = voltageDefenses[4].Block5V;
            this._U_2_CB.SelectedIndex = voltageDefenses[5].Block5V;
            this._U_3_CB.SelectedIndex = voltageDefenses[6].Block5V;
            this._U_4_CB.SelectedIndex = voltageDefenses[7].Block5V;
            this._U_1_1_CB.SelectedIndex = voltageDefenses[14].Block5V;
            this._U_1_2_CB.SelectedIndex = voltageDefenses[15].Block5V;

            for (int i = 0; i < voltageDefenses.Length; i++)
            {
                try
                {
                    string parametrString;
                    DataGridView currentGrid;
                    if (i < 8)
                    {
                        parametrString = StringsV2.ParametrU[voltageDefenses[i].Parameter];
                        currentGrid = this._voltageDefensesGrid1;
                    }
                    else
                    {
                        if (i < 12)
                        {
                            parametrString = StringsV2.ParametrU0[voltageDefenses[i].Parameter];
                            currentGrid = this.dataGridView1;
                        }
                        else
                        {
                            parametrString = StringsV2.VoltageDefensesNames[i];
                            currentGrid = this.dataGridView2;
                        }
                    }
                    var rowMass = new object[]
                        {
                            StringsV2.VoltageDefensesNames[i],
                            voltageDefenses[i].Mode,
                            voltageDefenses[i].RomExtBlock,
                            parametrString,
                            voltageDefenses[i].RomExtInput,
                            voltageDefenses[i].RomExtWait,
                            voltageDefenses[i].Recovery,
                            voltageDefenses[i].RomExtInputAdd,
                            voltageDefenses[i].RomExtWaitAdd,
                            voltageDefenses[i].Osc,
                            voltageDefenses[i].Reset
                        };
                    currentGrid.Rows.Add(rowMass);
                }
                catch (Exception)
                {
                    voltageDefenses[i] = new VoltageDefenseStruct();
                    this._currentStruct.AllVoltageDefenseConfiguration.VoltageDefenseConfiguration[group].VoltageDefenses = voltageDefenses;
                    MessageBox.Show(string.Format(DEFENSES_FAULT_PATTERN, VOLTAGE_DEFENCE, StringsV2.VoltageDefensesNames[i])); 
                    i--;
                }

            }
        }

        /// <summary>
        /// Запись защит по напряжению
        /// </summary>
        private void WriteVoltageDefenses(int group)
        {
            var configurationStruct = new VoltageDefenseConfigurationStruct(true);
            
            //БЛОКИРОВКА ПО U< 5В
            configurationStruct.VoltageDefenses[4].Block5V = this._U_1_CB.SelectedIndex;
            configurationStruct.VoltageDefenses[5].Block5V = this._U_2_CB.SelectedIndex;
            configurationStruct.VoltageDefenses[6].Block5V = this._U_3_CB.SelectedIndex;
            configurationStruct.VoltageDefenses[7].Block5V = this._U_4_CB.SelectedIndex;
            configurationStruct.VoltageDefenses[14].Block5V = this._U_1_1_CB.SelectedIndex;
            configurationStruct.VoltageDefenses[15].Block5V = this._U_1_2_CB.SelectedIndex;

            for (int i = 0; i < configurationStruct.VoltageDefenses.Length; i++)
            {
                DataGridView currentGrid;
                int j;
                if (i < 8)
                {

                    currentGrid = this._voltageDefensesGrid1;
                    j = i;
                    configurationStruct.VoltageDefenses[i].Parameter =
                        (ushort)StringsV2.ParametrU.IndexOf(currentGrid[3, j].Value.ToString());

                }
                else
                {
                    if (i < 12)
                    {

                        currentGrid = this.dataGridView1;
                        j = i - 8;
                        configurationStruct.VoltageDefenses[i].Parameter =
                            (ushort)StringsV2.ParametrU0.IndexOf(currentGrid[3, j].Value.ToString());

                    }
                    else
                    {
                        currentGrid = this.dataGridView2;
                        j = i - 12;
                    }
                }

                configurationStruct.VoltageDefenses[i].Mode = currentGrid[1, j].Value.ToString();
                configurationStruct.VoltageDefenses[i].RomExtBlock = currentGrid[2, j].Value.ToString();
                configurationStruct.VoltageDefenses[i].RomExtInput = Convert.ToDouble(currentGrid[4, j].Value);
                configurationStruct.VoltageDefenses[i].RomExtWait = Convert.ToInt32(currentGrid[5, j].Value);
                configurationStruct.VoltageDefenses[i].Recovery = Convert.ToBoolean(currentGrid[6, j].Value);
                configurationStruct.VoltageDefenses[i].RomExtInputAdd = Convert.ToDouble(currentGrid[7, j].Value);
                configurationStruct.VoltageDefenses[i].RomExtWaitAdd = Convert.ToInt32(currentGrid[8, j].Value);
                configurationStruct.VoltageDefenses[i].Osc = currentGrid[9, j].Value.ToString();
                configurationStruct.VoltageDefenses[i].Reset = Convert.ToBoolean(currentGrid[10, j].Value);
            }
            this._currentStruct.AllVoltageDefenseConfiguration.VoltageDefenseConfiguration[group] = configurationStruct;
        }

        /// <summary>
        /// Вывод внешних защит
        /// </summary>
        private void ShowExternalDefenses(int group)
        {
            this._externalDefenseGrid.Rows.Clear();

            var externalDefenses =
                this._currentStruct.AllExternalDefensesConfiguration.ExternalDefensesConfiguration[group].ExternalDefenses;
            for (int i = 0; i < externalDefenses.Length; i++)
            {
                try
                {
                    this._externalDefenseGrid.Rows.Add(
                        i + 1,
                        externalDefenses[i].Mode,
                        externalDefenses[i].RomExtBlock,
                        externalDefenses[i].RomExtInput,
                        externalDefenses[i].RomExtWait,
                        externalDefenses[i].Recovery,
                        externalDefenses[i].RomExtInputAdd,
                        externalDefenses[i].RomExtWaitAdd,
                        externalDefenses[i].Osc,
                        externalDefenses[i].Reset
                        );
                }
                catch (Exception)
                {
                    externalDefenses[i] = new ExternalDefenseStruct();      
                    MessageBox.Show(string.Format(DEFENSES_FAULT_PATTERN,EXTERNAL_DEFENSE,(i + 1)));
                    i--;
                }

            }
        }

        /// <summary>
        /// Запись внешних защит
        /// </summary>
        private void WriteExternalDefenses(int group)
        {
            var configurationStruct = new ExternalDefensesConfigurationStruct(true);



            for (int i = 0; i < configurationStruct.ExternalDefenses.Length; i++)
            {
                configurationStruct.ExternalDefenses[i].Mode = this._externalDefenseGrid[1, i].Value.ToString();
                configurationStruct.ExternalDefenses[i].RomExtBlock = this._externalDefenseGrid[2, i].Value.ToString();
                configurationStruct.ExternalDefenses[i].RomExtInput = this._externalDefenseGrid[3, i].Value.ToString();
                configurationStruct.ExternalDefenses[i].RomExtWait =
                    Convert.ToInt32(this._externalDefenseGrid[4, i].Value);
                configurationStruct.ExternalDefenses[i].Recovery =
                    Convert.ToBoolean(this._externalDefenseGrid[5, i].Value);
                configurationStruct.ExternalDefenses[i].RomExtInputAdd =
                    this._externalDefenseGrid[6, i].Value.ToString();
                configurationStruct.ExternalDefenses[i].RomExtWaitAdd =
                    Convert.ToInt32(this._externalDefenseGrid[7, i].Value);
                configurationStruct.ExternalDefenses[i].Osc = Convert.ToBoolean(this._externalDefenseGrid[8, i].Value);
                configurationStruct.ExternalDefenses[i].Reset = Convert.ToBoolean(this._externalDefenseGrid[9, i].Value);
            }

            this._currentStruct.AllExternalDefensesConfiguration.ExternalDefensesConfiguration[group] =
                configurationStruct;

        }

        /// <summary>
        /// Вывод реле
        /// </summary>
        private void ShowRelays()
        {
            this._outputReleGrid.Rows.Clear();
            try
            {
                this._outputReleGrid.Rows.Add("Сигн",
                                              this._currentStruct.OutputAddedRelays.Relay[0].RelayType,
                                              this._currentStruct.OutputAddedRelays.Relay[0].Signal,
                                              this._currentStruct.OutputAddedRelays.Relay[0].WaitTime);
            }
            catch (Exception)
            {
                this._currentStruct.OutputAddedRelays.Relay[0] = new RelayStruct();
                this.ShowRelays();
                MessageBox.Show(PARAMETR_ERROR_PATTERN, ALARN_RELAY);
              return;
            }

            try
            {
                this._outputReleGrid.Rows.Add("Авария",
                                              this._currentStruct.OutputAddedRelays.Relay[1].RelayType,
                                              this._currentStruct.OutputAddedRelays.Relay[1].Signal,
                                              this._currentStruct.OutputAddedRelays.Relay[1].WaitTime);
            }
            catch (Exception)
            {
                this._currentStruct.OutputAddedRelays.Relay[1] = new RelayStruct();
                this.ShowRelays();
                MessageBox.Show(PARAMETR_ERROR_PATTERN, FAULT_RELAY);
                return;
            }

            var relayArray = this._currentStruct.OutputRelays.Relays;
            for (int i = 0; i < relayArray.Length; i++)
            {
                try
                {
                    this._outputReleGrid.Rows.Add(i + 1,
                                                  relayArray[i].RelayType,
                                                  relayArray[i].Signal,
                                                  relayArray[i].WaitTime);
                }
                catch (Exception)
                {
                    
                  relayArray[i] = new RelayStruct();
                    this._currentStruct.OutputRelays.Relays = relayArray;
                    MessageBox.Show(string.Format(PARAMETR_ERROR_PATTERN, string.Format("Реле {0}", i + 1)));
                    i--;
                }

            }
        }

        /// <summary>
        /// Запись реле
        /// </summary>
        private void WriteRelays()
        {
            this._currentStruct.OutputAddedRelays.Relay[0].RelayType = this._outputReleGrid[1, 0].Value.ToString();
            this._currentStruct.OutputAddedRelays.Relay[0].Signal = this._outputReleGrid[2, 0].Value.ToString();
            this._currentStruct.OutputAddedRelays.Relay[0].WaitTime = Convert.ToInt32(this._outputReleGrid[3, 0].Value);


            this._currentStruct.OutputAddedRelays.Relay[1].RelayType = this._outputReleGrid[1, 1].Value.ToString();
            this._currentStruct.OutputAddedRelays.Relay[1].Signal = this._outputReleGrid[2, 1].Value.ToString();
            this._currentStruct.OutputAddedRelays.Relay[1].WaitTime = Convert.ToInt32(this._outputReleGrid[3, 1].Value);

            var relayStruct = new OutputRelaysStruct(true);

            for (int i = 0; i < relayStruct.Relays.Length; i++)
            {
                relayStruct.Relays[i].RelayType = this._outputReleGrid[1, i + 2].Value.ToString();
                relayStruct.Relays[i].Signal = this._outputReleGrid[2, i + 2].Value.ToString();
                relayStruct.Relays[i].WaitTime = Convert.ToInt32(this._outputReleGrid[3, i + 2].Value);
            }

            this._currentStruct.OutputRelays = relayStruct;
        }
        /// <summary>
        /// Вывод состояния ключей
        /// </summary>
        /// <param name="keys">Строковый битовый массив</param>
        private void ShowLogicKeys(string keys)
        {
            _keysDataGrid.Rows.Clear();
            for (int i = 0; i < 16; i++)
            {
                if (keys[i] == '0')
                {
                    _keysDataGrid.Rows.Add(new object[] { i + 1, StringsV2.YesNo[0] });
                }
                else
                {
                    _keysDataGrid.Rows.Add(new object[] { i + 1, StringsV2.YesNo[1] });
                }
            }
        }
        /// <summary>
        /// Вывод логических сигналов
        /// </summary>
        private void ShowLogicValues(int signalIndex)
        {
            try
            {
                for (int i = 0; i < 8; i++)
                {
                    this._logicSignalsDataGrid[1, i].Value =
                        StringsV2.LogicState[
                            this._currentStruct.InputLogicParametres.LogicSignalsStructs[signalIndex].LogicValues[i]];
                }
            }
            catch (Exception)
            {
                this._currentStruct.InputLogicParametres = new InputLogicParametresStruct(true);
                this.ShowLogicValues(signalIndex);
                MessageBox.Show(PARAMETR_ERROR_PATTERN, LOGIC_SIGNALS_FAULT);
            }
           
        }
        #endregion [Ввод-вывод уставок]


        #region [Чтение/Запись конфигурации]
        /// <summary>
        /// Запуск чтения конфигурации из устройства
        /// </summary>
        private void StartRead()
        {
            this._statusLabel.Text = START_READ;
            this._ramMemory.LoadStruct();
        }


        private void SetConfiguration()
        {
            //Измерительный канал
            this._currentStruct.RomMeasuring.RomErrorsU = (ushort) this._defectTN_CB.SelectedIndex;
            this._currentStruct.RomMeasuring.RomErrorsUo = (ushort) this._defectTNNP_CB.SelectedIndex;
            this._currentStruct.RomMeasuring.RomFactorU = double.Parse(this._TN_Box.Text);
            this._currentStruct.RomMeasuring.RomFactorUo = double.Parse(this._TNNP_Box.Text);


            //Дополнительные сигналы
            this._currentStruct.OutputAddedRelays.Reset = this._inputSignalsTN_DispepairCombo.SelectedItem.ToString();
            this._currentStruct.RomExternalSignals.ResetIndication =
                (ushort) this._inputSignalsIndicationResetCombo.SelectedIndex;
            this._currentStruct.RomExternalSignals.ReservSetpoints =
                (ushort) this._inputSignalsConstraintResetCombo.SelectedIndex;
            //Реле
            this.WriteRelays();
            //Внешние защиты
            int group = this._mainRadioBtnGroup.Checked ? 0 : 1;
            this.WriteExternalDefenses(group);
            //Защиты по напряжению
            int voltageGroup = this._mainRadioBtnGroup.Checked ? 0 : 1;
            this.WriteVoltageDefenses(voltageGroup);
            //Защиты по частоте
            int frequenceGroup = this._mainRadioBtnGroup.Checked ? 0 : 1;
            this.WriteFrequenceDefenses(frequenceGroup);
            //Индикаторы
            this.WriteIndicators();
            //Сигнал неисправность
            this.WriteSignalFault();
            //Конфигурация осц
            this.WriteOscConfig();
        }


        /// <summary>
        /// Запись конфигурации в устройство
        /// </summary>
        private void StartWrite()
        {
            this.SetConfiguration();
            this._ramMemory.Value = this._currentStruct;
            this._ramMemory.SaveStruct();
            this._device.SetBit(this._device.DeviceNumber, 0, true, "Save_settings", this._device);
        }

        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            try
            {
                var doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MR600"));


                var values = this._currentStruct.GetValues();

                XmlElement element = doc.CreateElement(XML_HEAD);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("Файл {0} успешно сохранён", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }
        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                var doc = new XmlDocument();
                doc.Load(binFileName);

                var a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if(a == null)
                    throw new NullReferenceException();

                var values = Convert.FromBase64String(a.InnerText);
                var ramStruct = new RamMemoryStruct(true);
                ramStruct.InitStruct(values);
                this._currentStruct = ramStruct;
                this.ShowAll();
                this._statusLabel.Text = string.Format("Файл {0} успешно загружен", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }
        #endregion [Чтение/Запись конфигурации]


        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR600); }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(MR600); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return  AssemblyResources.Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }
        #endregion [IFormView Members]


        #region [События взаимодействия]
        /// <summary>
        /// Сохранение в файл
        /// </summary>
        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            _saveConfigurationDlg.FileName = string.Format("МР600_Уставки_версия {0:F1}", this._device.DeviceVersion); 
            if (DialogResult.OK == _saveConfigurationDlg.ShowDialog())
            {
                this.SetConfiguration();
                this.Serialize(_saveConfigurationDlg.FileName);
            }
        }

        /// <summary>
        /// Загрузить из файла
        /// </summary>
        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == _openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(_openConfigurationDlg.FileName);
            }          
        }

        /// <summary>
        /// Прочитать из устройства
        /// </summary>
        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this._exchangeProgressBar.Value = 0;
            this.StartRead();
        }

        private void Mr600ConfigurationFormV2_Shown(object sender, EventArgs e)
        {
            this.StartRead();
        }

        /// <summary>
        /// Записать в устройтво
        /// </summary>
        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(WRITTING_CONFIG, WRITE_CONFIG, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this._exchangeProgressBar.Value = 0;
                this._statusLabel.Text = START_WRITE;
                SetConfiguration();
                this.StartWrite();
            }

        }

        /// <summary>
        /// Логические сигналы, применить изменения
        /// </summary>
        private void _applyLogicSignalsBut_Click(object sender, EventArgs e)
        {
            //Входные сигналы
            var result = new int[8];
            for (int i = 0; i < 8; i++)
            {
                result[i] = (ushort)(StringsV2.LogicState.IndexOf(this._logicSignalsDataGrid.Rows[i].Cells[1].Value.ToString()));
            }
            int index = this._logicChannelsCombo.SelectedIndex;
            this._currentStruct.InputLogicParametres.LogicSignalsStructs[index].LogicValues = result;


        }
        /// <summary>
        /// Принять изменения для ключей
        /// </summary>
        private void _applyKeys_Click(object sender, EventArgs e)
        {
            string keys = "";
            for (int i = 0; i < 16; i++)
            {
                if (_keysDataGrid.Rows[i].Cells[1].Value.ToString() == "Да")
                {
                    keys += "1";
                }
                else
                {
                    keys += "0";
                }
            }

            _currentStruct.RomMeasuring.RomFreeLogicKey = keys;
        }
        /// <summary>
        /// Выбран другой логический сигнал
        /// </summary>
        private void _logicChannelsCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ShowLogicValues(this._logicChannelsCombo.SelectedIndex);
        }   
     
        /// <summary>
        /// Переключение выходящих логических сигналов
        /// </summary>
        private void _outputLogicCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowOutputLogicSignals(_outputLogicCombo.SelectedIndex);
        }

        /// <summary>
        /// Сохнанение логического сигнала
        /// </summary>
        private void _outputLogicAcceptBut_Click(object sender, EventArgs e)
        {
            this.WriteOutputLogicSignals(_outputLogicCombo.SelectedIndex);
        } 
        #endregion [События взаимодействия]


        #region [Проверка значений]
        /// <summary>
        /// проверка Выходные реле
        /// </summary>
        private void _outputReleGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            // не импульс
            if (e.ColumnIndex != 3)
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }


        private bool FrequenceValidate(string frequence, out string message)
        {
            message = string.Empty;
            double value;

            var flag = double.TryParse(frequence, out value);

            if ((value < 40) | (value > 60) | (!flag))
            {
                message = "Поле должно содержать вещественное число в диапазоне [40-60], с точностью до сотых";
                return false;
            }

            return true;
        }

        private bool VoltageValidate(string voltage, out string message)
        {
            message = string.Empty;
            double value;

            var flag = double.TryParse(voltage, out value);

            if ((value < 0) | (value > 256) | (!flag))
            {
                message = "Поле должно содержать положительное вещественное число <= 256, с точностью до сотых";
                return false;
            }

            return true;
        }

        private bool TimeValidate(string time, out string message)
        {
            message = string.Empty;
            uint value;

            var flag = uint.TryParse(time, out value);

            if ((value > 3000000) | (!flag))
            {
                message = "Поле должно содержать положительное целое число <= 3.000.000";
                return false;
            }

            if (value == 0)
            {
                return true;
            }

            if (((value >= 1000000) & (value % 100 != 0)) | ((value < 1000000) & (value % 10 != 0)))
            {
                message = "Для значений меньше 1.000.000 значение должно быть кратно 10, для больших кратно 100";
                return false;
            }

            return true;
        }

        private bool PersentValidate(string voltage, out string message)
        {
            message = string.Empty;
            int value;

            var flag = int.TryParse(voltage, out value);

            if ((value < 1) | (value > 99) | (!flag))
            {
                message = "Поле должно содержать целое число от 1 до 99";
                return false;
            }

            return true;
        }

        private bool ParametrValidate(string voltage, out string message)
        {
            message = string.Empty;
            double value;

            var flag = double.TryParse(voltage, out value);

            if ((value < 0) | (value > 128000) | (!flag))
            {
                message = "Поле должно содержать положительное вещественное число <= 128000, с точностью до сотых";
                return false;
            }

            return true;
        }

        /// <summary>
        /// проверка Импульс неисправности
        /// </summary>
        private void _outputSignalsDispepairTimeBox_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string message;
            if (this.TimeValidate(_outputSignalsDispepairTimeBox.Text, out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }
        /// <summary>
        /// проверка Внешние защиты
        /// </summary>
        private void _externalDefenseGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if ((e.ColumnIndex != 4) & (e.ColumnIndex != 7))
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }
        /// <summary>
        /// проверка Защиты по напряжению
        /// </summary>
        private void _voltageDefensesGrid1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            //по времени
            if ((e.ColumnIndex == 5) | (e.ColumnIndex == 8))
            {
                string message;
                if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                    return;

                e.Cancel = true;

                MessageBox.Show(message);

            }

            // по уставке

            if ((e.ColumnIndex == 4) | (e.ColumnIndex == 7))
            {
                string message;
                if (this.VoltageValidate(e.FormattedValue.ToString(), out message))
                    return;

                e.Cancel = true;

                MessageBox.Show(message);

            }


        }
        /// <summary>
        /// проверка Защиты по частоте
        /// </summary>
        private void _frequenceDefensesGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            //по времени
            if ((e.ColumnIndex == 4) | (e.ColumnIndex == 6))
            {
                string message;
                if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                    return;

                e.Cancel = true;

                MessageBox.Show(message);
            }

            // по уставке
            if ((e.ColumnIndex == 3) | (e.ColumnIndex == 7))
            {
                string message;
                if (this.FrequenceValidate(e.FormattedValue.ToString(), out message))
                    return;

                e.Cancel = true;

                MessageBox.Show(message);
            }
        } 
        /// <summary>
        /// Длит. предзаписа осц
        /// </summary>
        private void _oscWriteLength_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string message;
            if (this.PersentValidate(_oscWriteLength.Text, out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }
        #endregion [Проверка значений]   

        private void _TN_Box_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string message;
            var tb = sender as MaskedTextBox;
            if(tb == null)
                return;
            if (this.ParametrValidate(tb.Text, out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }
        /// <summary>
        /// Переключение групп уставок (внешние защиты, по частоте, по напряжению)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _mainRadioBtnGroup_CheckedChanged(object sender, EventArgs e)
        {
            if (this._mainRadioBtnGroup.Checked)
            {

                this.WriteVoltageDefenses(1);
                this.ShowVoltageDefenses(0);
            }
            else
            {
                this.WriteVoltageDefenses(0);
                this.ShowVoltageDefenses(1);
            }

            if (this._mainRadioBtnGroup.Checked)
            {

                this.WriteFrequenceDefenses(1);
                this.ShowFrequenceDefenses(0);
            }
            else
            {
                this.WriteFrequenceDefenses(0);
                this.ShowFrequenceDefenses(1);
            }

            if (this._mainRadioBtnGroup.Checked)
            {
                this.WriteExternalDefenses(1);
                this.ShowExternalDefenses(0);
            }
            else
            {
                this.WriteExternalDefenses(0);
                this.ShowExternalDefenses(1);
            }
        }

        

    }
}
