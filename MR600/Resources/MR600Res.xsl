<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
<head>
 <script type="text/javascript">
	function translateBoolean(value, elementId){
 		var result = "";
 		if(value.toString().toLowerCase() == "true")
  			result = "��";
 		else if(value.toString().toLowerCase() == "false")
  			result = "���";
		else 
			result = "������������ ��������"
 		document.getElementById(elementId).innerHTML = result; 
}
</script>

</head>
  <body>
    <h3>���������� ��600<xsl:value-of select="MR600/DeviceType"/> ������ �� <xsl:value-of select="MR600/DeviceVersion"/></h3>
	<p></p>
	    <h3>���������� ����</h3>
	<table border="1">
      <tr bgcolor="#c1ced5">
         <th>�������, ��</th>
         <th>���� ������������</th>
		 <th>������� ���� ������������</th>
         <th>���� ������</th>
         <th>������� ���� ������</th>
      </tr>

      <tr>
         <td><xsl:value-of select="MR600/�����_�������������"/></td>
         <td><xsl:value-of select="MR600/����_������������"/></td>
         <td><xsl:value-of select="MR600/�����_������������"/></td>
         <td><xsl:value-of select="MR600/����_������"/></td>
		 <td><xsl:value-of select="MR600/�����_������"/></td>
      </tr>

    </table>
	<p></p>
    <h3>������������ �������� ����</h3>
    <table border="1">
      <tr bgcolor="#c1ced5">
         <th>����� ����</th>
         <th>���</th>
         <th>������</th>
         <th>�������, ��</th>
      </tr>
      <xsl:for-each select="MR600/��������_����">
      <tr>
         <td><xsl:value-of select="position()"/></td>
         <td><xsl:value-of select="@���"/></td>
         <td><xsl:value-of select="@������"/></td>
         <td><xsl:value-of select="@�������"/></td>
      </tr>
    </xsl:for-each>
    </table>
<p></p>

    <h3>������������ ������������ �����������</h3> 
    <table border="1">    
      <tr bgcolor="#c1ced5">
         <th>����� ����������</th>
         <th>���</th>
         <th>������</th>
         <th>����� �����. �� ���. ���.</th>
         <th>����� �����. �� ��</th>
         <th>����� �����. �� ��</th>
      </tr>
     <xsl:for-each select="MR600/��������_����������">
        <tr>
         <td><xsl:value-of select="position()"/></td>
         <td><xsl:value-of select="@���"/></td>
         <td><xsl:value-of select="@������"/></td>
         <xsl:element name="td">
        <xsl:attribute name="id">indexing_<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@���������"/>,"indexing_<xsl:value-of select="position()"/>");</script>
      </xsl:element>
      <xsl:element name="td">
        <xsl:attribute name="id">alarm_<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@������"/>,"alarm_<xsl:value-of select="position()"/>");</script>
      </xsl:element>
         <xsl:element name="td">
        <xsl:attribute name="id">sys_<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@�������"/>,"sys_<xsl:value-of select="position()"/>");</script>
      </xsl:element>
        </tr>
      </xsl:for-each>
     </table>
<p></p>

    <h3>������������ ������������� ��������������� � ������� ��������</h3> 
   
     <table border="1">    
      <tr bgcolor="#ced5c1">
         <th>����������� ��</th>
         <th>������������� ��</th>
         <th>����������� ����</th>
         <th>������������ �������</th>
         <th>����� ���������</th>
      </tr>
          <tr>
         <td><xsl:value-of select="MR600/��"/></td>
         <td><xsl:value-of select="MR600/�������������_��"/></td>
         <td><xsl:value-of select="MR600/����"/></td>
         <td><xsl:value-of select="MR600/������������_�������"/></td>
		 <td><xsl:value-of select="MR600/�����_����������"/></td>
      </tr>

    </table>
	

	 <p></p>
    <h3>������������ ������� �����</h3> 
	<p></p>
	<b>�������� ������� ������</b>
     <table border="1">    
      <tr bgcolor="#bcbcbc">
	     <th>����� ��</th>
         <th>�����</th>
         <th>������������</th>
         <th>����������</th>
         <th>����� ������. ��</th>
         <th>�������</th>
         <th>���� �����.</th>
         <th>����� ��������, ��</th>
      </tr>
	  <xsl:for-each select="MR600/�������_������_��������">
      <tr>
         <td><xsl:value-of select="position()"/></td>
         <td><xsl:value-of select="@�����"/></td>
         <td><xsl:value-of select="@������������_�����"/></td>
         <td><xsl:value-of select="@����������_�����"/></td>
         <td><xsl:value-of select="@������������_�����"/></td>
		 <xsl:element name="td">
		 <xsl:attribute name="id">vozvr1_<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@�������"/>,"vozvr1_<xsl:value-of select="position()"/>");</script>
      </xsl:element>
		 <td><xsl:value-of select="@�������_�����"/></td>
		 <td><xsl:value-of select="@�������_�����"/></td>
      </tr>
	  </xsl:for-each>
     </table>
	 <p></p>
	<b>��������� ������� ������</b>
     <table border="1">    
      <tr bgcolor="#bcbcbc">
	     <th>����� ��</th>
         <th>�����</th>
         <th>������������</th>
         <th>����������</th>
         <th>����� ������. ��</th>
         <th>�������</th>
         <th>���� �����.</th>
         <th>����� ��������, ��</th>
      </tr>
	  <xsl:for-each select="MR600/�������_������_���������">
      <tr>
         <td><xsl:value-of select="position()"/></td>
         <td><xsl:value-of select="@�����"/></td>
         <td><xsl:value-of select="@������������_�����"/></td>
         <td><xsl:value-of select="@����������_�����"/></td>
         <td><xsl:value-of select="@������������_�����"/></td>
		  <xsl:element name="td">
		 <xsl:attribute name="id">vozvr2_<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@�������"/>,"vozvr2_<xsl:value-of select="position()"/>");</script>
      </xsl:element>
		 <td><xsl:value-of select="@�������_�����"/></td>
		 <td><xsl:value-of select="@�������_�����"/></td>
      </tr>
	  </xsl:for-each>
     </table>

	 <p></p>
	 <h3>������������ ����� �� ����������</h3> 
	<p></p>
	<b>�������� ������ �� ����������</b>
     <table border="1">    
      <tr bgcolor="#bcbcbc">
	     <th>�����</th>
		 <th>���</th>
         <th>�����</th>
         <th>��� �������</th>
         <th>�������</th>
         <th>����������</th>
         <th>�����</th>
         
      </tr>
	  <xsl:for-each select="MR600/������_����������_��������">
      <tr>
         <td><xsl:value-of select="position()"/></td>
<td><xsl:if test="position() =1">U></xsl:if><xsl:if test="position() =2">U>></xsl:if><xsl:if test="position() =3">U>>></xsl:if><xsl:if test="position() =4">U>>>></xsl:if><xsl:if test="position() =5">Umin1</xsl:if><xsl:if test="position() =6">Umin2</xsl:if><xsl:if test="position() =7">Umin3</xsl:if><xsl:if test="position() =8">Umin4</xsl:if><xsl:if test="position() =9">U0></xsl:if><xsl:if test="position() =10">U0>></xsl:if><xsl:if test="position() =11">U0>>></xsl:if><xsl:if test="position() =12">U0>>>></xsl:if><xsl:if test="position() =13">U1min1</xsl:if><xsl:if test="position() =14">U1min2</xsl:if><xsl:if test="position() =15">U2></xsl:if><xsl:if test="position() =16">U2>></xsl:if></td>
         <td><xsl:value-of select="@�����"/></td>
         <td><xsl:value-of select="@���_�������"/></td>
         <td><xsl:value-of select="@����������_�����"/></td>
		 <td><xsl:value-of select="@������������_�������"/></td>
		 <td><xsl:value-of select="@������������_�����"/></td>
		 
      </tr>
	  </xsl:for-each>
     </table>
	 <p></p>
	<b>��������� ������ �� ����������</b>
     <table border="1">    
      <tr bgcolor="#bcbcbc">
	     <th>�����</th>
		 <th>���</th>
         <th>�����</th>
         <th>��� �������</th>
         <th>�������</th>
         <th>����������</th>
         <th>�����</th>
         
      </tr>
	  <xsl:for-each select="MR600/������_����������_���������">
      <tr>
         <td><xsl:value-of select="position()"/></td>
         <td><xsl:if test="position() =1">U></xsl:if><xsl:if test="position() =2">U>></xsl:if><xsl:if test="position() =3">U>>></xsl:if><xsl:if test="position() =4">U>>>></xsl:if><xsl:if test="position() =5">Umin1</xsl:if><xsl:if test="position() =6">Umin2</xsl:if><xsl:if test="position() =7">Umin3</xsl:if><xsl:if test="position() =8">Umin4</xsl:if><xsl:if test="position() =9">U0></xsl:if><xsl:if test="position() =10">U0>></xsl:if><xsl:if test="position() =11">U0>>></xsl:if><xsl:if test="position() =12">U0>>>></xsl:if><xsl:if test="position() =13">U1min1</xsl:if><xsl:if test="position() =14">U1min2</xsl:if><xsl:if test="position() =15">U2></xsl:if><xsl:if test="position() =16">U2>></xsl:if></td>
         <td><xsl:value-of select="@�����"/></td>
         <td><xsl:value-of select="@���_�������"/></td>
         <td><xsl:value-of select="@����������_�����"/></td>
		 <td><xsl:value-of select="@������������_�������"/></td>
		 <td><xsl:value-of select="@������������_�����"/></td>
		 
      </tr>
	  </xsl:for-each>
     </table>
	  
	  <p></p>
	 <h3>������������ ����� �� �������</h3> 
	<p></p>
	<b>�������� ������ �� �������</b>
     <table border="1">    
      <tr bgcolor="#bcbcbc">
	     <th>�����</th>
		 <th>���</th>
		 <th>�����</th>
         <th>�������</th>
         <th>����������</th>
         <th>t����, ��</th>
         <th>����� ����</th>
		 <th>���������� ����</th>
		 <th>������� ����</th>
		 <th>t����, �� ����</th>
      </tr>
	  <xsl:for-each select="MR600/������_��_�������_��������">
      <tr>
	   <xsl:if test="position() &lt; 5">
         <td><xsl:value-of select="position()"/></td>
		 <td><xsl:if test="position() =5">F></xsl:if><xsl:if test="position() =6">F>></xsl:if><xsl:if test="position() =7">F>>></xsl:if><xsl:if test="position() =8">F>>>></xsl:if><xsl:if test="position() =1">Fmin1</xsl:if><xsl:if test="position() =2">Fmin2</xsl:if><xsl:if test="position() =3">Fmin3</xsl:if><xsl:if test="position() =4">Fmin4</xsl:if></td>
         <td><xsl:value-of select="@�����"/></td>
         <td><xsl:value-of select="@�������"/></td>
		 <td><xsl:value-of select="@����������_�����"/></td>
         <td><xsl:value-of select="@��������"/></td>
		 <td><xsl:value-of select="@����_�����"/></td>
		 <td><xsl:value-of select="@����_����������_�����"/></td>
		 <td><xsl:value-of select="@����_�������"/></td>
		 <td><xsl:value-of select="@����_��������"/></td>
		  </xsl:if>
      </tr>
	  </xsl:for-each>
     </table>
	 <p></p>
	 <table border="1">    
      <tr bgcolor="#bcbcbc">
	     <th>�����</th>
		 <th>���</th>
		 <th>�����</th>
         <th>�������</th>
         <th>����������</th>
         <th>t����, ��</th>
         </tr>
	  <xsl:for-each select="MR600/������_��_�������_��������">
      <tr>
	   <xsl:if test="position() &gt; 4">
         <td><xsl:value-of select="position()"/></td>
		 <td><xsl:if test="position() =5">F></xsl:if><xsl:if test="position() =6">F>></xsl:if><xsl:if test="position() =7">F>>></xsl:if><xsl:if test="position() =8">F>>>></xsl:if><xsl:if test="position() =1">Fmin1</xsl:if><xsl:if test="position() =2">Fmin2</xsl:if><xsl:if test="position() =3">Fmin3</xsl:if><xsl:if test="position() =4">Fmin4</xsl:if></td>
         <td><xsl:value-of select="@�����"/></td>
         <td><xsl:value-of select="@�������"/></td>
		 <td><xsl:value-of select="@����������_�����"/></td>
         <td><xsl:value-of select="@��������"/></td>
		 </xsl:if>
      </tr>
	  </xsl:for-each>
     </table>
	 <p></p>
	<b>��������� ������ �� �������</b>
     <table border="1">    
      <tr bgcolor="#bcbcbc">
	     <th>�����</th>
		 <th>���</th>
		 <th>�����</th>
         <th>�������</th>
         <th>����������</th>
         <th>t����, ��</th>
         <th>����� ����</th>
		 <th>���������� ����</th>
		 <th>������� ����</th>
		 <th>t����, �� ����</th>
      </tr>
	  <xsl:for-each select="MR600/������_��_�������_���������">
      <tr>
	  <xsl:if test="position() &lt; 5">
         <td><xsl:value-of select="position()"/></td>
		 <td><xsl:if test="position() =5">F></xsl:if><xsl:if test="position() =6">F>></xsl:if><xsl:if test="position() =7">F>>></xsl:if><xsl:if test="position() =8">F>>>></xsl:if><xsl:if test="position() =1">Fmin1</xsl:if><xsl:if test="position() =2">Fmin2</xsl:if><xsl:if test="position() =3">Fmin3</xsl:if><xsl:if test="position() =4">Fmin4</xsl:if></td>
         <td><xsl:value-of select="@�����"/></td>
         <td><xsl:value-of select="@�������"/></td>
		 <td><xsl:value-of select="@����������_�����"/></td>
         <td><xsl:value-of select="@��������"/></td>
		 <td><xsl:value-of select="@����_�����"/></td>
		 <td><xsl:value-of select="@����_����������_�����"/></td>
		 <td><xsl:value-of select="@����_�������"/></td>
		 <td><xsl:value-of select="@����_��������"/></td>
		 </xsl:if>
      </tr>
	  </xsl:for-each>
     </table>
	 <p></p>
	 <table border="1">    
      <tr bgcolor="#bcbcbc">
	     <th>�����</th>
		 <th>���</th>
		 <th>�����</th>
         <th>�������</th>
         <th>����������</th>
         <th>t����, ��</th>
         </tr>
	  <xsl:for-each select="MR600/������_��_�������_���������">
      <tr>
	  <xsl:if test="position() &gt; 4">
         <td><xsl:value-of select="position()"/></td>
		 <td><xsl:if test="position() =5">F></xsl:if><xsl:if test="position() =6">F>></xsl:if><xsl:if test="position() =7">F>>></xsl:if><xsl:if test="position() =8">F>>>></xsl:if><xsl:if test="position() =1">Fmin1</xsl:if><xsl:if test="position() =2">Fmin2</xsl:if><xsl:if test="position() =3">Fmin3</xsl:if><xsl:if test="position() =4">Fmin4</xsl:if></td>
         <td><xsl:value-of select="@�����"/></td>
         <td><xsl:value-of select="@�������"/></td>
		 <td><xsl:value-of select="@����������_�����"/></td>
         <td><xsl:value-of select="@��������"/></td>
		 </xsl:if>
      </tr>
	  </xsl:for-each>
     </table>
	 </body>
  </html>
</xsl:template>
</xsl:stylesheet>
