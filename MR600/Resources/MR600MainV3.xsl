<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.1"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
<head>
<script type="text/javascript">
	function translateBoolean(value, elementId){
 		var result = "";
 		if(value.toString().toLowerCase() == "true")
  			result = "��";
 		else if(value.toString().toLowerCase() == "false")
  			result = "���";
		else 
			result = "������������ ��������"
 		document.getElementById(elementId).innerHTML = result; 
}
</script>
</head>
  <body>

 <h3>���������� <xsl:value-of select="RamMemoryStruct/���_����������"/> . ������ �� <xsl:value-of select="RamMemoryStruct/������"/> . ����� <xsl:value-of select="RamMemoryStruct/�����_����������"/></h3>
 <!-- ������� ������� -->
 
  <h3>������� �������</h3>
   <b>������������� �����</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
         <th>����������� ������������� ��</th>
         <th>������������� ��</th>
		 <th>����������� ������������� ����</th>
		 <th>������������� ����</th>
      </tr>
     
      <tr align="center">
         <td><xsl:value-of select="RamMemoryStruct/RomMeasuring/RomFactorU"/></td>
         <td><xsl:value-of select="RamMemoryStruct/RomMeasuring/RomErrorsU"/></td>
		 <td><xsl:value-of select="RamMemoryStruct/RomMeasuring/RomFactorUo"/></td>
		 <td><xsl:value-of select="RamMemoryStruct/RomMeasuring/RomErrorsUo"/></td>
      </tr>
    </table>
	<p></p>
	
	<b>�����</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
         <th>����</th>
         <th>��������</th>
      </tr>

         <xsl:for-each select="RamMemoryStruct/Keys/����">
			<tr align="center">
			<td><xsl:value-of select="position()"/></td>
			<td><xsl:value-of select="current()"/></td>
			</tr>
		 </xsl:for-each>
    </table>
	<p></p>
	
	<b>�������������� �������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
		 <th>������������ �� ��������� �������</th>
         <th>����� ���������</th>
		 <th>����� ��������, ��������� ��������</th>
      </tr>
     
      <tr align="center">
	     <td><xsl:value-of select="RamMemoryStruct/RomExternalSignals/ReservSetpoints"/></td>
         <td><xsl:value-of select="RamMemoryStruct/RomExternalSignals/ResetIndication"/></td>
		 <td><xsl:value-of select="RamMemoryStruct/ResetStep/Reset"/></td>
      </tr>
    </table>
	<p></p>
	<b>������� ���������� �������</b>
	<table border="1" cellspacing="0">
	<td>
	<b>���������� ������� �</b>
	<table border="1" cellspacing="0">

      <tr bgcolor="FFFFCC">
         <th>����� ��</th>
         <th>������������</th>
	  </tr>
 
   <xsl:for-each select="RamMemoryStruct/InputLogicParametres/��">  
     <xsl:if test="position() &lt; 5 ">
   	  <tr>	
         <td><xsl:value-of  select="position()"/></td>
	
			 <td>
			 <xsl:for-each select="�������">
				<xsl:if test="current() !='���    ' ">
					<xsl:if test="current() ='��     '">
					�<xsl:value-of select="position()"/>
					</xsl:if>
					<xsl:if test="current() ='������ '">
					^�<xsl:value-of select="position()"/>
					</xsl:if>
				</xsl:if>
			 </xsl:for-each>
			 </td>
	  </tr>
	</xsl:if>	
    </xsl:for-each>
    </table>
	
		<b>���������� ������� ���</b>
	<table border="1" cellspacing="0">

      <tr bgcolor="FFFFCC">
         <th>����� ��</th>
         <th>������������</th>
	  </tr>
 
   <xsl:for-each select="RamMemoryStruct/InputLogicParametres/��">  
     <xsl:if test="position() &gt; 4 ">
   	  <tr>	
         <td><xsl:value-of  select="position()"/></td>
	
			 <td>
			<xsl:for-each select="�������">
				<xsl:if test="current() !='���    ' ">
					<xsl:if test="current() ='��     '">
					�<xsl:value-of select="position()"/>
					</xsl:if>
					<xsl:if test="current() ='������ '">
					^�<xsl:value-of select="position()"/>
					</xsl:if>
				</xsl:if>
			 </xsl:for-each>
			 </td>
	  </tr>
	</xsl:if>	
    </xsl:for-each>
    </table>
	</td>
	</table>
	<p></p>
	
	<b>��������� ������������</b>
		<table border="1" cellspacing="0">
			<tr bgcolor="FFFFCC">
				<th>����. ������� ���.</th>
				<th>�������� ���.</th>
				<th>����. ���������� ���.</th>
			</tr>
			<tr align="center">
				<td><xsl:value-of select="RamMemoryStruct/OscConfiguration/Code"/></td>
				<td><xsl:value-of select="RamMemoryStruct/OscConfiguration/Fixation"/></td>
				<td><xsl:value-of select="RamMemoryStruct/OscConfiguration/PeriodPercent"/></td>
			</tr>
		</table>
	<p></p>
	
	<!-- �������� ������� -->
	
	<h3>�������� �������</h3>
	
	  <b>���������� ����</b>
	  <table border="1" cellspacing="0">
	  <td>
	  <b>���� �������������</b>
	  <table border="1" cellspacing="0">
		<tr bgcolor="#4682B4">
		 <th>����������</th>
		 <th>���-������</th>
      <th>�����������</th>
      <th>������1</th>
      <th>������2</th>
      <th>������3</th>
      <th>����������</th>
      <th>�������</th>
    </tr>
		<tr align="center">
			<td><xsl:value-of select="RamMemoryStruct/AllFaultSignal/Signals/�������������[1]"/></td>
			<td><xsl:value-of select="RamMemoryStruct/AllFaultSignal/Signals/�������������[2]"/></td>
      <td><xsl:value-of select="RamMemoryStruct/AllFaultSignal/Signals/�������������[3]"/></td>
      <td><xsl:value-of select="RamMemoryStruct/AllFaultSignal/Signals/�������������[4]"/></td>
      <td><xsl:value-of select="RamMemoryStruct/AllFaultSignal/Signals/�������������[5]"/></td>
      <td><xsl:value-of select="RamMemoryStruct/AllFaultSignal/Signals/�������������[6]"/></td>
      <td><xsl:value-of select="RamMemoryStruct/AllFaultSignal/Signals/�������������[7]"/></td>
      <td><xsl:value-of select="RamMemoryStruct/AllFaultSignal/Signals/�������������[8]"/></td>
		</tr>
	  </table>	  

	
	<b>�������, ��</b>
	<table border="1" cellspacing="0">
	<tr bgcolor="#4682B4">
		 <th>��������</th>
    </tr>
	
	  <tr align="center">
		 <td><xsl:value-of select="RamMemoryStruct/AllFaultSignal/Time"/></td>
      </tr>
	</table>
	</td>
	</table>
	<p></p>
	
	<b>�������� ����</b>
	<table border="1" cellspacing="0">
	<tr bgcolor="4682B4">
		 <th>�����</th>
		 <th>���</th>
		 <th>������</th>
		 <th>�������, ��</th>
    </tr>

	<xsl:for-each select="RamMemoryStruct/AllOutputRelays/Rows/RelayStruct">
		 <tr align="center">
			<td>
			<xsl:if test="position() =1">������������</xsl:if>
			<xsl:if test="position() =2">������</xsl:if>
			<xsl:if test="position() !=1 ">
				<xsl:if test="position() !=2 ">
					<center>���� <xsl:value-of select="position()-2"/></center>
					</xsl:if>
				</xsl:if>
			</td>
			<td><xsl:value-of select= "RelayType" /></td>
			<td><xsl:value-of select="Signal"/></td>
			<td><xsl:value-of select="WaitTime"/></td>
		 </tr>
	</xsl:for-each>
	</table>
	<p></p>
	
	<b>����������</b>
	<table border="1" cellspacing="0">
	<tr bgcolor="4682B4">
		 <th>�����</th>
		 <th>���</th>
		 <th>������</th>
		 <th>����������</th>
		 <th>��</th>
		 <th>��</th>
    </tr>
	<xsl:for-each select="RamMemoryStruct/AllIndicators/Rows/IndicatorsStruct">
		 <tr align="center">
			<td>���. <xsl:value-of select="position()"/> </td>
			<td><xsl:value-of select="Type"/></td>
			<td><xsl:value-of select="Signal"/></td>
			<xsl:element name="td">
			<xsl:attribute name="id">Ind_<xsl:value-of select="position()"/></xsl:attribute>            
			<script>translateBoolean(<xsl:value-of select="Ind"/>,"Ind_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
			<xsl:element name="td">
			<xsl:attribute name="id">Aj_<xsl:value-of select="position()"/></xsl:attribute>            
			<script>translateBoolean(<xsl:value-of select="Aj"/>,"Aj_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
			<xsl:element name="td">
			<xsl:attribute name="id">Sj_<xsl:value-of select="position()"/></xsl:attribute>            
			<script>translateBoolean(<xsl:value-of select="Sj"/>,"Sj_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
		 </tr>
	</xsl:for-each>
	</table>
	<p></p>
	
	<b>���</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="4682B4">
         <th>�����</th>
         <th>������������</th>
      </tr>
     
      
		 <xsl:for-each select="RamMemoryStruct/AllOutputLogicSignal/���">
		 <tr>
         <td><xsl:value-of select="position()"/></td>
		 <td>
		 <xsl:for-each select="�������">
         <xsl:value-of select="current()"/>|
         </xsl:for-each></td>
		 </tr>
		 </xsl:for-each>
	 </table>   
	 
	<!-- ������ -->
	
	<h2><b>������. �������� ������ �������</b></h2>
	<b>������� ������</b>
	<table border="1" cellspacing="0">
	<tr bgcolor="32CD32">
		 <th>�����</th>
		 <th>�����</th>
		 <th>����������</th>
		 <th>������������</th>
		 <th>����� ������������, ��</th>
		 <th>����.</th>
		 <th>���� ��������</th>
		 <th>����� ��������, ��</th>
		 <th>���.</th>
		 <th>�����</th>
    </tr>
	<xsl:for-each select="RamMemoryStruct/AllSetpoint/MainSetpoints/ExternalDefenses/Rows/ExternalDefenseStruct"> 
		 <tr align="center">
			<td>��- <xsl:value-of select="position()"/> </td>
			<td><xsl:value-of select="Mode"/></td>
			<td><xsl:value-of select="RomExtBlock"/></td>
			<td><xsl:value-of select="RomExtInput"/></td>
			<td><xsl:value-of select="RomExtWait"/></td>
			<xsl:element name="td">
			<xsl:attribute name="id">Rec_<xsl:value-of select="position()"/></xsl:attribute>            
			<script>translateBoolean(<xsl:value-of select="Recovery"/>,"Rec_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
			<td><xsl:value-of select="RomExtInputAdd"/></td>
			<td><xsl:value-of select="RomExtWaitAdd"/></td>
			<xsl:element name="td">
			<xsl:attribute name="id">Osc_<xsl:value-of select="position()"/></xsl:attribute>            
			<script>translateBoolean(<xsl:value-of select="Osc"/>,"Osc_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
			<xsl:element name="td">
			<xsl:attribute name="id">Res_<xsl:value-of select="position()"/></xsl:attribute>            
			<script>translateBoolean(<xsl:value-of select="Reset"/>,"Res_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
		 </tr>
	</xsl:for-each>
	</table>
	<p></p>
	<b>������ U</b>
	<p></p>
	<table border="1" cellspacing="0">
	<td>
	<table border="1" cellspacing="0">
	<b>������ U></b>
	<tr bgcolor="32CD32">
		 <th>�������</th>
		 <th>�����</th>
		 <th>����������</th>
		 <th>��������</th>
		 <th>��������� ����., �</th>
		 <th>����� ������������, ��</th>
		 <th>����.</th>
		 <th>��������� �������. �</th>
		 <th>����� ��������</th>
		 <th>���.</th>
		 <th>�����</th>
    </tr>
	<xsl:for-each select="RamMemoryStruct/AllSetpoint/MainSetpoints/VoltageDefenses/Rows/VoltageDefensesStruct">
	<xsl:if test="position() &lt; 5">
		<tr align="center">
			<td>
			<xsl:if test="position() =1">U></xsl:if>
			<xsl:if test="position() =2">U>></xsl:if>
			<xsl:if test="position() =3">U>>></xsl:if>
			<xsl:if test="position() =4">U>>>></xsl:if>
			</td>
			<td><xsl:value-of select="Mode"/></td>
			<td><xsl:value-of select="Block"/></td>
			<td><xsl:value-of select="ParametrUB"/></td>
			<td><xsl:value-of select="Srab"/></td>
			<td><xsl:value-of select="TimeSrab"/></td>
			<xsl:element name="td">
			  <xsl:attribute name="id">Ret1_<xsl:value-of select="position()"/></xsl:attribute>            
			  <script>translateBoolean(<xsl:value-of select="Return"/>,"Ret1_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
			<td><xsl:value-of select="UstavkaReturn"/></td>
			<td><xsl:value-of select="TimeReturn"/></td>
			<td><xsl:value-of select="Osc"/></td>
			<xsl:element name="td">
			  <xsl:attribute name="id">Reset1_<xsl:value-of select="position()"/></xsl:attribute>            
			  <script>translateBoolean(<xsl:value-of select="Reset"/>,"Reset1_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
		</tr>
	</xsl:if>
	</xsl:for-each>
	</table>
	<table border="1" cellspacing="0">
	<b>������ U&#60;</b>
	<tr bgcolor="32CD32">
		 <th>�������</th>
		 <th>�����</th>
		 <th>����������</th>
		 <th>��������</th>
		 <th>��������� ����., �</th>
		 <th>����� ������������, ��</th>
		 <th>����.</th>
		 <th>��������� �������. �</th>
		 <th>����� ��������</th>
		 <th>���������� U&#60;5�</th>
		 <th>���.</th>
		 <th>�����</th>
    </tr>
	<xsl:for-each select="RamMemoryStruct/AllSetpoint/MainSetpoints/VoltageDefenses/Rows/VoltageDefensesStruct">
	<xsl:if test="position() &gt; 4 and position() &lt; 9">	
		<tr align="center">
			<td>
			<xsl:if test="position() =5">U&#60;</xsl:if>
			<xsl:if test="position() =6">U&#60;&#60;</xsl:if>
			<xsl:if test="position() =7">U&#60;&#60;&#60;</xsl:if>
			<xsl:if test="position() =8">U&#60;&#60;&#60;&#60;</xsl:if>
			</td>
			<td><xsl:value-of select="Mode"/></td>
			<td><xsl:value-of select="Block"/></td>
			<td><xsl:value-of select="ParametrUM"/></td>
			<td><xsl:value-of select="Srab"/></td>
			<td><xsl:value-of select="TimeSrab"/></td>
			
			<xsl:element name="td">
			  <xsl:attribute name="id">Ret2_<xsl:value-of select="position()"/></xsl:attribute>            
			  <script>translateBoolean(<xsl:value-of select="Return"/>,"Ret2_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
			<td><xsl:value-of select="UstavkaReturn"/></td>
			<td><xsl:value-of select="TimeReturn"/></td>
			<xsl:element name="td">
			  <xsl:attribute name="id">BlockV5_<xsl:value-of select="position()"/></xsl:attribute>            
			  <script>translateBoolean(<xsl:value-of select="BlockV5"/>,"BlockV5_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
			<td><xsl:value-of select="Osc"/></td>
			  <xsl:element name="td">
			  <xsl:attribute name="id">Reset2_<xsl:value-of select="position()"/></xsl:attribute>            
			  <script>translateBoolean(<xsl:value-of select="Reset"/>,"Reset2_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
		 </tr>
	</xsl:if>
	</xsl:for-each>
	</table>
	<table border="1" cellspacing="0">
	<b>������ U0></b>
	<tr bgcolor="32CD32">
		 <th>�������</th>
		 <th>�����</th>
		 <th>����������</th>
		 <th>��������</th>
		 <th>��������� ����., �</th>
		 <th>����� ������������, ��</th>
		 <th>����.</th>
		 <th>��������� �������. �</th>
		 <th>����� ��������</th>
		 <th>���.</th>
		 <th>�����</th>
    </tr>
	<xsl:for-each select="RamMemoryStruct/AllSetpoint/MainSetpoints/VoltageDefenses/Rows/VoltageDefensesStruct">
	<xsl:if test="position() &gt; 8 and position() &lt; 13">
		<tr align="center">
			<td>
			<xsl:if test="position() =9">U0></xsl:if>
			<xsl:if test="position() =10">U0>></xsl:if>
			<xsl:if test="position() =11">U0>>></xsl:if>
			<xsl:if test="position() =12">U0>>>></xsl:if>
			</td>
			<td><xsl:value-of select="Mode"/></td>
			<td><xsl:value-of select="Block"/></td>
			<td><xsl:value-of select="ParametrU0"/></td>
			<td><xsl:value-of select="Srab"/></td>
			<td><xsl:value-of select="TimeSrab"/></td>
			<xsl:element name="td">
			  <xsl:attribute name="id">RetU0_<xsl:value-of select="position()"/></xsl:attribute>            
			  <script>translateBoolean(<xsl:value-of select="Return"/>,"RetU0_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
			<td><xsl:value-of select="UstavkaReturn"/></td>
			<td><xsl:value-of select="TimeReturn"/></td>
			<td><xsl:value-of select="Osc"/></td>
			<xsl:element name="td">
			  <xsl:attribute name="id">ResU0_<xsl:value-of select="position()"/></xsl:attribute>            
			  <script>translateBoolean(<xsl:value-of select="Reset"/>,"ResU0_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
		 </tr>
	</xsl:if>
	</xsl:for-each>
	</table>
	<table border="1" cellspacing="0">
	<b>������ U2></b>
	<tr bgcolor="32CD32">
		 <th>�������</th>
		 <th>�����</th>
		 <th>����������</th>
		 <th>��������</th>
		 <th>��������� ����., �</th>
		 <th>����� ������������, ��</th>
		 <th>����.</th>
		 <th>��������� �������. �</th>
		 <th>����� ��������</th>
		 <th>���.</th>
		 <th>�����</th>
    </tr>
	<xsl:for-each select="RamMemoryStruct/AllSetpoint/MainSetpoints/VoltageDefenses/Rows/VoltageDefensesStruct">
	<xsl:if test="position() &gt; 12 and position() &lt; 15">
		<tr align="center">
			<td>
			<xsl:if test="position() =13">U2></xsl:if>
			<xsl:if test="position() =14">U2>></xsl:if>
			</td>
			<td><xsl:value-of select="Mode"/></td>
			<td><xsl:value-of select="Block"/></td>
			<td><xsl:value-of select="ParametrU2"/></td>
			<td><xsl:value-of select="Srab"/></td>
			<td><xsl:value-of select="TimeSrab"/></td>
			<xsl:element name="td">
			  <xsl:attribute name="id">RetU2_<xsl:value-of select="position()"/></xsl:attribute>            
			  <script>translateBoolean(<xsl:value-of select="Return"/>,"RetU2_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
			<td><xsl:value-of select="UstavkaReturn"/></td>
			<td><xsl:value-of select="TimeReturn"/></td>
			<td><xsl:value-of select="Osc"/></td>
			<xsl:element name="td">
			  <xsl:attribute name="id">ResetU2_<xsl:value-of select="position()"/></xsl:attribute>            
			  <script>translateBoolean(<xsl:value-of select="Reset"/>,"ResetU2_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
		 </tr>
	</xsl:if>
	</xsl:for-each>
	</table>
	<table border="1" cellspacing="0">
	<b>������ U1&#60;</b>
	<tr bgcolor="32CD32">
		 <th>�������</th>
		 <th>�����</th>
		 <th>����������</th>
		 <th>��������</th>
		 <th>��������� ����., �</th>
		 <th>����� ������������, ��</th>
		 <th>����.</th>
		 <th>��������� �������. �</th>
		 <th>����� ��������</th>
		 <th>���.</th>
		 <th>�����</th>
    </tr>
	<xsl:for-each select="RamMemoryStruct/AllSetpoint/MainSetpoints/VoltageDefenses/Rows/VoltageDefensesStruct">
	<xsl:if test="position() &gt; 14">
		<tr align="center">
			<td>
			<xsl:if test="position() =15">U1&#60;</xsl:if>
			<xsl:if test="position() =16">U1&#60;&#60;</xsl:if>
			</td>
			<td><xsl:value-of select="Mode"/></td>
			<td><xsl:value-of select="Block"/></td>
			<td><xsl:value-of select="ParametrU1"/></td>
			<td><xsl:value-of select="Srab"/></td>
			<td><xsl:value-of select="TimeSrab"/></td>
			<xsl:element name="td">
			  <xsl:attribute name="id">RetU1_<xsl:value-of select="position()"/></xsl:attribute>            
			  <script>translateBoolean(<xsl:value-of select="Return"/>,"RetU1_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
			<td><xsl:value-of select="UstavkaReturn"/></td>
			<td><xsl:value-of select="TimeReturn"/></td>
			<td><xsl:value-of select="Osc"/></td>
			<xsl:element name="td">
			  <xsl:attribute name="id">ResU1_<xsl:value-of select="position()"/></xsl:attribute>            
			  <script>translateBoolean(<xsl:value-of select="Reset"/>,"ResU1_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
		 </tr>
	</xsl:if>
	</xsl:for-each>
	</table>
	<p></p>
	</td>
	</table>
	<p></p>
	<b>������ F</b>
	<p></p>
	<table border="1" cellspacing="0">
	<td>
	<b>������ F></b>
	<table border="1" cellspacing="0">
	<tr bgcolor="32CD32">
		 <th>�������</th>
		 <th>�����</th>
		 <th>����������</th>
		 <th>������������</th>
		 <th>����� ������������, ��</th>
		 <th>����.</th>
		 <th>���� ��������</th>
		 <th>����� ��������, ��</th>
		 <th>���.</th>
		 <th>�����</th>
    </tr>
	<xsl:for-each select="RamMemoryStruct/AllSetpoint/MainSetpoints/FrequencyDefenses/Rows/FrequencyDefensesStruct">
		 <xsl:if test="position() &lt; 5">
		 <tr align="center">
			<td>
			<xsl:if test="position() =1">F></xsl:if>
			<xsl:if test="position() =2">F>></xsl:if>
			<xsl:if test="position() =3">F>>></xsl:if>
			<xsl:if test="position() =4">F>>>></xsl:if>
			</td>
			<td><xsl:value-of select="Mode"/></td>
			<td><xsl:value-of select="Block"/></td>
			<td><xsl:value-of select="UstavkaSrab"/></td>
			<td><xsl:value-of select="TimeSrab"/></td>
			<xsl:element name="td">
			  <xsl:attribute name="id">RetF1_<xsl:value-of select="position()"/></xsl:attribute>            
			  <script>translateBoolean(<xsl:value-of select="Return"/>,"RetF1_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
			<td><xsl:value-of select="UstavkaReturn"/></td>
			<td><xsl:value-of select="TimeReturn"/></td>
			<td><xsl:value-of select="Osc"/></td>
			<xsl:element name="td">
			  <xsl:attribute name="id">ResF1_<xsl:value-of select="position()"/></xsl:attribute>            
			  <script>translateBoolean(<xsl:value-of select="Reset"/>,"ResF1_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
		 </tr>
		 </xsl:if>
	</xsl:for-each>
	</table>
	<b>������ F&#60;</b>
	<table border="1" cellspacing="0">
	<tr bgcolor="32CD32">
		 <th>�������</th>
		 <th>�����</th>
		 <th>����������</th>
		 <th>������������</th>
		 <th>����� ������������, ��</th>
		 <th>����.</th>
		 <th>���� ��������</th>
		 <th>����� ��������, ��</th>
		 <th>���.</th>
		 <th>�����</th>
    </tr>
	<xsl:for-each select="RamMemoryStruct/AllSetpoint/MainSetpoints/FrequencyDefenses/Rows/FrequencyDefensesStruct">
		 <xsl:if test="position() &gt; 4">
		 <tr align="center">
			<td>
			<xsl:if test="position() =5">F&#60;</xsl:if>
			<xsl:if test="position() =6">F&#60;&#60;</xsl:if>
			<xsl:if test="position() =7">F&#60;&#60;&#60;</xsl:if>
			<xsl:if test="position() =8">F&#60;&#60;&#60;&#60;</xsl:if>
			</td>
			<td><xsl:value-of select="Mode"/></td>
			<td><xsl:value-of select="Block"/></td>
			<td><xsl:value-of select="UstavkaSrab"/></td>
			<td><xsl:value-of select="TimeSrab"/></td>
			<xsl:element name="td">
			  <xsl:attribute name="id">RetF2_<xsl:value-of select="position()"/></xsl:attribute>            
			  <script>translateBoolean(<xsl:value-of select="Return"/>,"RetF2_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
			<td><xsl:value-of select="UstavkaReturn"/></td>
			<td><xsl:value-of select="TimeReturn"/></td>
			<td><xsl:value-of select="Osc"/></td>
			<xsl:element name="td">
			  <xsl:attribute name="id">ResF2_<xsl:value-of select="position()"/></xsl:attribute>            
			  <script>translateBoolean(<xsl:value-of select="Reset"/>,"ResF2_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
		 </tr>
		 </xsl:if>
	</xsl:for-each>
	</table>
	</td>
	</table>

  </body>
  </html>
</xsl:template>
</xsl:stylesheet>