﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Compressor;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR600.New;
using BMTCD.BMTCDMr600;
using BMTCD.HelperClasses;
using Crownwood.Magic.Common;
using Crownwood.Magic.Docking;
using SchemeEditorSystem;

namespace BEMN.MR600.BSBGL
{
    public partial class BSBGLEF : Form, IFormView
    {
        #region Fields
        private const int COUNT_EXCHANGES_PROGRAM = 16;
        private const int COUNT_EXCHANGES_PROGECT = 128;
        private MemoryEntity<StartStruct> _currentStartProgramStruct;
        private MemoryEntity<ProgramStorageStruct> _currentStorageStruct;
        private MemoryEntity<SourceProgramStruct> _currentSourceProgramStruct;
        private MemoryEntity<ProgramSignalsStruct> _currentSignalsStruct;
        private Mr600DeviceV2 _deviceV2;
        private MR600 _device;
        private NewSchematicForm _newForm;
        private DockingManager _manager;
        private MR600CD _compiller;
        private bool _isOnSimulateMode;
        private MessageBoxForm _formCheck;
        ushort[] _binFile;
        private bool _isRunEmul;
        private byte[] _fromDevice;
        private OutputWindow _outputWindow;
        private LibraryBox _libraryWindow;

        private Content _outputContent;
        private Content _libraryContent;
        
        private VisualStyle _style;
        
        private StatusBar _statusBar;
        private Crownwood.Magic.Controls.TabControl _filler;
        #endregion
        
        #region Constructor
        public BSBGLEF()
        {
            InitForm();
        }
        public BSBGLEF(MR600 device)
        {
            this._deviceV2 = device.Mr600DeviceV2;
            this._device = device;
            this._currentSourceProgramStruct = _deviceV2.SourceProgramStruct;
            this._currentStartProgramStruct = _deviceV2.ProgramStartStruct;
            this._currentSignalsStruct = _deviceV2.ProgramSignalsStruct;
            this._currentStorageStruct = _deviceV2.ProgramStorageStruct;
            //Программа
            this._currentSourceProgramStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, ProgramStructWriteOk);
            this._currentSourceProgramStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStructWriteFail);
            this._currentSourceProgramStruct.WriteOk += HandlerHelper.CreateHandler(this, ExchangeOk);
            this._currentSourceProgramStruct.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._deviceV2.SourceProgramStruct.RemoveStructQueries();
                this.ProgramStructWriteFail();
            });
            //Архив программы
            this._currentStorageStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ProgramStorageReadOk);
            this._currentStorageStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, ProgramStorageReadFail);
            this._currentStorageStruct.ReadOk += HandlerHelper.CreateHandler(this, ExchangeOk);
            this._currentStorageStruct.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._currentStorageStruct.RemoveStructQueries();
                ProgramStorageReadFail();
            });
            this._currentStorageStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, ProgramStorageWriteOk);
            this._currentStorageStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, ProgramStorageWriteFail);
            this._currentStorageStruct.WriteOk += HandlerHelper.CreateHandler(this, ExchangeOk);
            this._currentStorageStruct.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._currentStorageStruct.RemoveStructQueries();
                ProgramStorageWriteFail();
            });
            //Старт программы
            this._currentStartProgramStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, ProgramStartSaveOk);
            this._currentStartProgramStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, ProgramStartSaveFail);
            _deviceV2.StopSpl.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Выполнение свободно программируемой логики остановлено", "Останов СПЛ", MessageBoxButtons.OK, MessageBoxIcon.Information));
            _deviceV2.StopSpl.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Невозможно выполнить команду", "", MessageBoxButtons.OK, MessageBoxIcon.Error));
            _deviceV2.StartSpl.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, _deviceV2.StateSpl.LoadStruct);
            _deviceV2.StartSpl.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Невозможно выполнить команду", "", MessageBoxButtons.OK, MessageBoxIcon.Error));
            _deviceV2.StateSpl.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                if (Common.GetBit(_deviceV2.StateSpl.Value.Word, 15))
                {
                    MessageBox.Show("Состояние логики: ОШИБКА", "Запуск логики", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Состояние логики: ЗАПУЩЕНА", "Запуск логики", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
            });
            this._deviceV2.StateSpl.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Невозможно прочитать состояние логики", "Запуск логики", MessageBoxButtons.OK, MessageBoxIcon.Error));
            //Загрузка сигналов
            this._currentSignalsStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ProgramSignalsLoadOk);
            this._currentSignalsStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, ProgramSignalsLoadFail);
            this.InitForm();
        }
        #endregion

        void InitForm()
        {
            InitializeComponent();
            _style = VisualStyle.IDE;
            _manager = new DockingManager(this, _style);
            _newForm = new NewSchematicForm();
            _compiller = new MR600CD();
            _formCheck = new MessageBoxForm();
        }
        private void BSBGLEF_Load(object sender, EventArgs e)
        {
            _filler = new Crownwood.Magic.Controls.TabControl();
            _filler.Appearance = Crownwood.Magic.Controls.TabControl.VisualAppearance.MultiDocument;
            _filler.Dock = DockStyle.Fill;
            _filler.Style = _style;
            _filler.IDEPixelBorder = true;
            Controls.Add(_filler);
            _filler.ClosePressed += new EventHandler(OnFileClose);

            // Reduce the amount of flicker that occurs when windows are redocked within
            // the container. As this prevents unsightly backcolors being drawn in the
            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);

            // Create the object that manages the docking state
            _manager = new DockingManager(this, _style);
            // Ensure that the RichTextBox is always the innermost control
            _manager.InnerControl = _filler;

            // Create and setup the StatusBar object
            _statusBar = new StatusBar();
            _statusBar.Dock = DockStyle.Bottom;
            _statusBar.ShowPanels = true;

            // Create and setup a single panel for the StatusBar
            StatusBarPanel statusBarPanel = new StatusBarPanel();
            statusBarPanel.AutoSize = StatusBarPanelAutoSize.Spring;
            _statusBar.Panels.Add(statusBarPanel);
            Controls.Add(_statusBar);
            Controls.Add(CreateToolStrip());
            CreateMenus();
            _manager.OuterControl = _statusBar;
            CreateOutputWindow();
            CreateLibraryWindow();
            this.Width = 800;
            this.Height = 600;
        }

        #region [Device events handlers]
        private void ExchangeOk()
        {
            _formCheck.ProgramExchangeOk();
        }
        private void ProgramStorageReadFail()
        {
            _formCheck.Fail = true;
            _formCheck.ShowResultMessage(InformationMessages.ERROR_DOWNLOAD_ARCHIVE_OF_DEVICE);
        }
        
        private void ProgramStructWriteFail()
        {
            OutMessage(InformationMessages.ERROR_LOADING_PROGRAM_IN_DEVICE);
            _formCheck.Fail = true;
            _formCheck.ShowResultMessage(InformationMessages.ERROR_LOADING_PROGRAM_IN_DEVICE);
            OnStop();
        }

        private void ProgramStructWriteOk()
        {
            _formCheck.ShowMessage(InformationMessages.PROGRAM_SAVE_OK);
            var values = new ushort[1];
            values[0] = 0x00FF;
            StartStruct ss = new StartStruct();
            ss.InitStruct(Common.TOBYTES(values, false));
            _currentStartProgramStruct.Value = ss;
            _currentStartProgramStruct.SaveStruct5();

        }

        private void ProgramStorageReadOk()
        {
            if (!this._isRunEmul)
            {
                ushort[] value = this._currentStorageStruct.Values;
                this._formCheck.ShowResultMessage(this.UncompresseProject(value)
                    ? InformationMessages.PROJECT_LOADED_OK_OF_DEVICE
                    : InformationMessages.PROJWCT_STORAGE_IS_EMPTY);
            }
            else
            {
                this.OnEmulArchLoaded();
            }
        }
        
        private void ProgramStorageWriteOk()
        {
            _formCheck.ShowResultMessage(InformationMessages.PROGRAM_ARCHIVE_SAVE_OK_START_PROGRAM);
            ProgramSignalsStruct ps = new ProgramSignalsStruct(_compiller.GetRamRequired());
            ushort[] values = new ushort[_compiller.GetRamRequired()];
            this._currentSignalsStruct.Value = ps;
            this._currentSignalsStruct.Values = values;
            this._currentSignalsStruct.Slots = HelperFunctions.SetSlots(values, 0xA000);
            this._currentSignalsStruct.LoadStructCycle();
        }

        private void ProgramSignalsLoadOk()
        {
            if (_isOnSimulateMode)
            {
                _compiller.UpdateVol(this._currentSignalsStruct.Values);
                foreach (BSBGLTab src in _filler.TabPages)
                {
                    src.Schematic.RedrawSchematic();
                    src.Schematic.RedrawBack();
                }
                OutMessage(InformationMessages.VARIABLES_UPDATED);
            }
        }

        private void ProgramStorageWriteFail()
        {
            OutMessage(InformationMessages.ERROR_ARCHIVE_IS_NOT_SAVE_IN_DEVICE); 
            _formCheck.Fail = true;
            _formCheck.ShowResultMessage(InformationMessages.ERROR_ARCHIVE_IS_NOT_SAVE_IN_DEVICE);
            OnStop();
        }

        private void ProgramStartSaveFail()
        {
            OutMessage(InformationMessages.ERROR_PROGRAM_START);
            _formCheck.Fail = true;
            _formCheck.ShowResultMessage(InformationMessages.ERROR_PROGRAM_START);
            OnStop();
        }

        void ProgramSignalsLoadFail()
        {
            //Останавливаем отрисовку
            foreach (BSBGLTab src in _filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
            if (_isOnSimulateMode)
            {
                OutMessage(InformationMessages.ERROR_VARIABLES_UPDATED);
            }
        }
        
        private void ProgramStartSaveOk()
        {
            OutMessage(InformationMessages.PROGRAM_START_OK);
            _formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGECT);
            ushort[] programStorageValue = CompresseProject();
            ushort[] values = new ushort[8192];
            programStorageValue.CopyTo(values, 0);
            ProgramStorageStruct pss = new ProgramStorageStruct();
            pss.InitStruct(Common.TOBYTES(values, false));
            _currentStorageStruct.Value = pss;
            _currentStorageStruct.SaveStruct();
            _formCheck.ShowMessage(InformationMessages.LOADING_ARCHIVE_IN_DEVICE);
        }

        private void StartLogicProgram(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Запустить свободно программируемую логику в устройстве?", "Запуск СПЛ",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            _deviceV2.StartSpl.Value.Word = 0x00FF;
            _deviceV2.StartSpl.SaveStruct5();
        }

        private void StopLogicProgram(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Остановить свободно программируемую логику в устройстве? ВНИМАНИЕ! Это может привести к выводу из работы важных функций устройства", "Останов СПЛ",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            _deviceV2.StopSpl.Value.Word = 0x00FF;
            _deviceV2.StopSpl.SaveStruct5();
        }
        #endregion [Device events handlers]

        private void OutMessage(String str)
        {
            _outputWindow.AddMessage(str + "\r\n");
        }

        private void DefineContentState(Content c)
        {
            c.CaptionBar = true;
            c.CloseButton = true ;
        }

        #region Docking Forms Code
        private void CreateOutputWindow()
        {
            _outputWindow = new OutputWindow();
            _outputContent = _manager.Contents.Add(_outputWindow, "Сообщения");
            DefineContentState(_outputContent);
            _manager.AddContentWithState(_outputContent, State.DockBottom);
        }

        private void CreateLibraryWindow()
        {
            _libraryWindow = new LibraryBox(Resources.BlockLib);
            _libraryContent = _manager.Contents.Add(_libraryWindow, "Библиотека");
            DefineContentState(_libraryContent);
            _manager.AddContentWithState(_libraryContent, State.DockRight);
        }

        #endregion
        
        private void OnFileNew(object sender, EventArgs e)
        {
            _newForm.ShowDialog();
            if (DialogResult.OK == _newForm.DialogResult)
            {
                BSBGLTab myTab = new BSBGLTab();
                myTab.InitializeBSBGLSheet(_newForm.NameOfSchema, _newForm.sheetFormat, _device, this._manager);
                myTab.Selected = true;
                _filler.TabPages.Add(myTab);
                OutMessage("Создан новый лист схемы: " + _newForm.NameOfSchema);
                myTab.Schematic.Focus();
            }
        }
        void printToolStripButton_Click(object sender, EventArgs e)
        {
            // Initialize the dialog's PrinterSettings property to hold user
            // defined printer settings.
            pageSetupDialog.PageSettings =
                new System.Drawing.Printing.PageSettings();

            // Initialize dialog's PrinterSettings property to hold user
            // set printer settings.
            pageSetupDialog.PrinterSettings =
                new System.Drawing.Printing.PrinterSettings();

            //Do not show the network in the printer dialog.
            pageSetupDialog.ShowNetwork = false;

            //Show the dialog storing the result.
            DialogResult result = pageSetupDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                printDocument.DefaultPageSettings = pageSetupDialog.PageSettings;
                printPreviewDialog.Document = printDocument;
                // Call the ShowDialog method. This will trigger the document's
                //  PrintPage event.
                printPreviewDialog.ShowDialog();
                DialogResult result1 = printDialog.ShowDialog();
                if (result1 == DialogResult.OK)
                {
                    printDocument.Print();
                }
            }
        }
        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {

            if (_filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)_filler.SelectedTab;

                float scale = (float)e.PageBounds.Width /
                              (float)_sTab.Schematic.SizeX;
                _sTab.Schematic.DrawIntoGraphic(e.Graphics, scale);
            }
        }

        private void OnUndo(object sender, EventArgs e)
        {
            if (_filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)_filler.SelectedTab;
                _sTab.Schematic.Undo();
            }
        }
        private void OnRedo(object sender, EventArgs e)
        {
            if (_filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)_filler.SelectedTab;
                _sTab.Schematic.Redo();
            }
        }

        private void OnEditCut(object sender, EventArgs e)
        {
            if (_filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)_filler.SelectedTab;
                _sTab.Schematic.CopyFromXML();
                _sTab.Schematic.DeleteEvent();
            }

        }

        private void OnEditCopy(object sender, EventArgs e)
        {
            if (_filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)_filler.SelectedTab;
                _sTab.Schematic.CopyFromXML();
            }
        }

        private void OnEditPaste(object sender, EventArgs e)
        {
            if (_filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)_filler.SelectedTab;
                _sTab.Schematic.PasteFromXML();
            }
        }

        private void OnFileOpen(object sender, EventArgs e)
        {
            openFileDialog.Filter = "bsbgl файлы(*.bsbgl)|*.bsbgl|Все файлы (*.*)|*.*";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                BSBGLTab myTab = new BSBGLTab();
                myTab.InitializeBSBGLSheet(openFileDialog.FileName, SheetFormat.A0_L, _device, this._manager);
                myTab.Selected = true;
                _filler.TabPages.Add(myTab);

                XmlTextReader reader = new XmlTextReader(openFileDialog.FileName);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                reader.Read();
                myTab.Schematic.ReadXml(reader);
                myTab.UpdateTitle();
                reader.Close();
                OutMessage("Загружен лист схемы.");
                myTab.Schematic.Focus();
            }
        }
        private void OnFileOpenFromDevice(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            try
            {
                if (DialogResult.Cancel == CloseFileProject())
                {
                    return;
                }
                _currentStorageStruct.LoadStruct();
                //_formCheck = new MessageBoxForm();
                _formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGECT);
                _formCheck.ShowDialog(InformationMessages.DOWNLOADING_ARCHIVE_OF_DEVICE);
            }
            catch
            {
                _formCheck.Fail = true;
                _formCheck.ShowResultMessage(InformationMessages.ERROR_DOWNLOAD_ARCHIVE_OF_DEVICE);
                OutMessage(InformationMessages.ERROR_DOWNLOAD_ARCHIVE_OF_DEVICE);
            }
        }

        private DialogResult CloseFileProject()
        {
            if (_filler.SelectedTab == null) return 0;
            switch (MessageBox.Show("Сохранить проект на диске ?"
                                                , "Закрытие проекта", MessageBoxButtons.YesNoCancel))
            {
                case DialogResult.Yes:
                    if (SaveProjectDoc())
                    {
                        _filler.TabPages.Clear();
                    }
                    break;
                case DialogResult.No: _filler.TabPages.Clear();
                    break;
                case DialogResult.Cancel:
                    return DialogResult.Cancel;
            }
            return DialogResult.None;
        }

        private void OnFileOpenProject(object sender, EventArgs e)
        {
            openFileDialog.Filter = "bprj файлы(*.bprj)|*.bprj|Все файлы (*.*)|*.*";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (_filler.SelectedTab != null)
                {
                    switch (MessageBox.Show("Сохранить текущий проект на диске ?"
                                                    , "Закрытие проекта", MessageBoxButtons.YesNoCancel))
                    {
                        case DialogResult.Yes:
                            if (SaveProjectDoc())
                            {
                                _filler.TabPages.Clear();
                            }
                            else
                            {
                                return;
                            }
                            break;
                        case DialogResult.No: _filler.TabPages.Clear();
                            break;
                        case DialogResult.Cancel:
                            return;
                    }
                }

                XmlTextReader reader = new XmlTextReader(openFileDialog.FileName);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                while (reader.Read())
                {
                    if ((reader.Name == "Source") && (reader.NodeType != XmlNodeType.EndElement))
                    {
                        BSBGLTab myTab = new BSBGLTab();
                        string sName = reader.GetAttribute("name");
                        if (string.IsNullOrEmpty(sName)) sName = reader.GetAttribute("pinName");
                        myTab.InitializeBSBGLSheet(sName, SheetFormat.A0_L, _device, this._manager);
                        myTab.Selected = true;
                        _filler.TabPages.Add(myTab);
                        myTab.Schematic.ReadXml(reader);
                        myTab.UpdateTitle();
                    }
                }
                OutMessage("Загружен проект.");
                reader.Close();
                _filler.SelectedTab.Focus();
            }

        }

        private void OnFileClose(object sender, EventArgs e)
        {
            if (_filler.SelectedTab == null) return;
            switch (MessageBox.Show("Сохранить документ на диске ?"
                                                , "Закрытие документа", MessageBoxButtons.YesNoCancel))
            {
                case DialogResult.Yes:
                    if (SaveActiveDoc())
                    {
                        BSBGLTab tab = (BSBGLTab)this._filler.SelectedTab;
                        this._filler.TabPages.Remove(tab);
                        tab.Dispose();
                    }
                    break;
                case DialogResult.No:
                {
                    BSBGLTab tab = (BSBGLTab)this._filler.SelectedTab;
                    this._filler.TabPages.Remove(tab);
                    tab.Dispose();
                }
                    break;
                case DialogResult.Cancel:
                    break;

            }
        }
        private void OnFileCloseProject(object sender, EventArgs e)
        {
            if (_filler.SelectedTab == null) return;
            switch (MessageBox.Show("Сохранить проект на диске ?", "Закрытие проекта", MessageBoxButtons.YesNoCancel))
            {
                case DialogResult.Yes:
                    SaveProjectDoc();                  
                    foreach (BSBGLTab tabPage in this._filler.TabPages)
                    {
                        tabPage.Dispose();
                    }
                    _filler.TabPages.Clear();                    
                    break;
                case DialogResult.No: 
                    foreach (BSBGLTab tabPage in this._filler.TabPages)
                    {
                        tabPage.Dispose();
                    } 
                    _filler.TabPages.Clear();
                    break;
                case DialogResult.Cancel:
                    break;
            }
        }
        private void OnFileSave(object sender, EventArgs e)
        {
            if (this._filler.TabPages.Count == 0)
            {
                MessageBox.Show("Проект пуст", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            SaveActiveDoc();
        }
        private void OnFileSaveProject(object sender, EventArgs e)
        {
            if (this._filler.TabPages.Count == 0)
            {
                MessageBox.Show("Проект пуст", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            SaveProjectDoc();
        }
        private void OnViewToolWindow(object sender, EventArgs e)
        {
            _manager.ShowContent(_libraryContent);
        }
        private void OnViewOutputWindow(object sender, EventArgs e)
        {
            _manager.ShowContent(_outputContent);
        }

        private bool CompileProject()
        {
            this._compiller.ResetCompiller();
            try
            {
                foreach (BSBGLTab src in this._filler.TabPages)
                {
                    this._compiller.AddSource(src.TabName, src.Schematic);
                    this.OutMessage("Компиляция схемы :" + src.TabName);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Ошибка компиляции", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.OnStop();
                return false;
            }
            this._binFile = this._compiller.Make();
            this.OutMessage("Пpоцент заполнения схемы:" + (float)this._compiller.Binarysize * 100 / 1024 + "%.");
            if (this._compiller.Binarysize > 1024)
            {
                MessageBox.Show("Программа слишком велика ! ", "Ошибка компиляции", MessageBoxButtons.OK);
                this.OnStop();
                return false;
            }
            if (this._binFile.Length == 0)
            {
                MessageBox.Show("Невозможно записать логическую программу программу",
                    "Ошибка компилятора", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.OnStop();
                return false;
            }
            foreach (BSBGLTab tabPage in this._filler.TabPages)
            {
                tabPage.Schematic.StartDebugMode();
            }
            return true;
        }

        protected void OnCompileUpload(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Записать файл логической программы в устройство?", "", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question) != DialogResult.OK) return;

            if (!this.CompileProject()) return;

            SourceProgramStruct ps = new SourceProgramStruct();
            ps.InitStruct(Common.TOBYTES(this._binFile, false));
            this._currentSourceProgramStruct.Value = ps;
            this._currentSourceProgramStruct.SaveStruct();
            this._isOnSimulateMode = true;
            this._formCheck = new MessageBoxForm();
            this._formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGRAM);
            this._formCheck.ShowDialog(InformationMessages.LOADING_PROGRAM_IN_DEVICE);
        }

        private byte[] MakeBinFromXml()
        {
            MemoryStream memstream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(memstream, System.Text.Encoding.UTF8);
            writer.Formatting = System.Xml.Formatting.Indented;
            writer.WriteStartDocument();
            writer.WriteStartElement("BSBGL_ProjectFile");
            if (this._filler.TabPages.Count == 0) return new byte[0];
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("name", src.TabName);
                src.Schematic.WriteXml(writer);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();
            return memstream.ToArray();
        }

        private bool CompareScheme()
        {
            byte[] fromScheme = this.MakeBinFromXml();
            if (fromScheme.Length == 0 || fromScheme.Length != this._fromDevice.Length) return false;
            //return !fromScheme.Where((t, i) => t != this._fromDevice[i]).Any();
            bool ret = true;// = !fromScheme.Where((t, i) => t != readCompressedData[i]).Any();
            for (int i = 0; i < fromScheme.Length; i++)
            {
                ret &= fromScheme[i] == this._fromDevice[i];
                if (!ret)
                {
                    return false;
                }
            }
            return true;
        }

        private void OnStartEmul(object sender, EventArgs e)
        {
            if (this._filler.TabPages.Count == 0)
            {
                if (MessageBox.Show("Нет текущей схемы. Загрузить из устройства?", "", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.No) return;
            }
            try
            {
                this._isRunEmul = true;
                this._currentStorageStruct.LoadStruct();
                this._formCheck = new MessageBoxForm();
                this._formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGECT);
                this._formCheck.ShowDialog(InformationMessages.DOWNLOADING_ARCHIVE_OF_DEVICE);
            }
            catch
            {
                this._formCheck.Fail = true;
                this._formCheck.ShowResultMessage(InformationMessages.ERROR_DOWNLOAD_ARCHIVE_OF_DEVICE);
                this.OutMessage(InformationMessages.ERROR_DOWNLOAD_ARCHIVE_OF_DEVICE);
                this._isRunEmul = false;
            }
        }

        private void OnEmulArchLoaded()
        {
            this._fromDevice = this.Uncompress(this._currentStorageStruct.Values);
            if (this._fromDevice.Length == 0)
            {
                this._formCheck.ShowResultMessage(InformationMessages.PROJWCT_STORAGE_IS_EMPTY);
                return;
            }
            if (this._filler.TabPages.Count != 0)
            {
                if (!this.CompareScheme())
                {
                    DialogResult res =
                        MessageBox.Show(
                            "Архив логической программы, загруженной из устройства, не совпадает с проектом, открытым в униконе. Продолжить эмуляцию с загруженным архивом?",
                            "ВНИМАНИЕ", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (res == DialogResult.No)
                    {
                        this._formCheck.ShowResultMessage("Отменено пользователем");
                        return;
                    }
                }
                this._filler.TabPages.Clear();
            }
            this.LoadProjectFromBin(this._fromDevice);

            this._isOnSimulateMode = true;
            this.CompileProject();
            foreach (BSBGLTab tabPage in this._filler.TabPages)
            {
                tabPage.Schematic.StartDebugMode();
            }
            this.StartLoadCurrentSignals();
        }

        private void StartLoadCurrentSignals()
        {
            ProgramSignalsStruct ps = new ProgramSignalsStruct(this._compiller.GetRamRequired());
            ushort[] values = new ushort[this._compiller.GetRamRequired()];
            this._currentSignalsStruct.Value = ps;
            this._currentSignalsStruct.Values = values;
            this._currentSignalsStruct.Slots = HelperFunctions.SetSlots(values, 0xA000);
            this._currentSignalsStruct.LoadStructCycle();
            this._formCheck.ShowResultMessage(InformationMessages.PROGRAM_SAVE_OK_EMULATOR_RUNNING);
        }

        void OnStop(object sender, EventArgs e)
        {
            _isOnSimulateMode = false;
            _currentSignalsStruct.RemoveStructQueries();
            foreach (BSBGLTab src in _filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
        }
        void OnStop()
        {
            _isOnSimulateMode = false;
            _currentSignalsStruct.RemoveStructQueries();
            foreach (BSBGLTab src in _filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
        }
        private bool SaveActiveDoc()
        {
            ZIPCompressor compr = new ZIPCompressor();
            System.Windows.Forms.DialogResult SaveFileRzult;
            saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "bsbgl файлы(*.bsbgl)|*.bsbgl|Все файлы (*.*)|*.*";
            SaveFileRzult = saveFileDialog.ShowDialog();
            if (SaveFileRzult == DialogResult.OK)
            {
                MemoryStream memstream = new MemoryStream();
                XmlTextWriter memwriter = new XmlTextWriter(memstream, System.Text.Encoding.UTF8);
                memwriter.Formatting = System.Xml.Formatting.Indented;
                memwriter.WriteStartDocument();

                if (_filler.SelectedTab is BSBGLTab)
                {
                    BSBGLTab _sTab = (BSBGLTab)_filler.SelectedTab;
                    _sTab.Schematic.WriteXml(memwriter);
                }
                memwriter.WriteEndDocument();
                memwriter.Close();
                byte[] uncompressed = memstream.ToArray();
                FileStream fs = new FileStream(saveFileDialog.FileName,
                                FileMode.Create, FileAccess.Write, FileShare.None, uncompressed.Length,
                                false);
                fs.Write(uncompressed, 0, uncompressed.Length);
                fs.Close();

                byte[] compressed = compr.Compress(uncompressed);
                FileStream fsa = new FileStream(saveFileDialog.FileName + ".Zip",
                                FileMode.Create, FileAccess.Write, FileShare.None, compressed.Length,
                                false);
                fsa.Write(compressed, 0, compressed.Length);
                fsa.Close();
                memstream.Close();
                return true;
            }
            return false;
        }
        private bool SaveProjectDoc()
        {
            ZIPCompressor compr = new ZIPCompressor();
            System.Windows.Forms.DialogResult SaveFileRzult;
            saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "bprj файлы(*.bprj)|*.bprj|Все файлы (*.*)|*.*";
            SaveFileRzult = saveFileDialog.ShowDialog();
            if (SaveFileRzult == DialogResult.OK)
            {
                MemoryStream memstream = new MemoryStream();
                XmlTextWriter writer = new XmlTextWriter(memstream, System.Text.Encoding.UTF8);
                writer.Formatting = System.Xml.Formatting.Indented;
                writer.WriteStartDocument();
                writer.WriteStartElement("BSBGL_ProjectFile");
                foreach (BSBGLTab src in _filler.TabPages)
                {
                    writer.WriteStartElement("Source");
                    writer.WriteAttributeString("name", src.TabName);
                    src.Schematic.WriteXml(writer);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
                byte[] uncompressed = memstream.ToArray();
                FileStream fs = new FileStream(saveFileDialog.FileName,
                                FileMode.Create, FileAccess.Write, FileShare.None, uncompressed.Length,
                                false);
                fs.Write(uncompressed, 0, uncompressed.Length);
                fs.Close();

                byte[] compressed = compr.Compress(uncompressed);
                FileStream fsa = new FileStream(saveFileDialog.FileName + ".Zip",
                                FileMode.Create, FileAccess.Write, FileShare.None, compressed.Length,
                                false);
                fsa.Write(compressed, 0, compressed.Length);
                fsa.Close();
                memstream.Close();
                return true;
            }
            return false;
        }
        private ushort[] CompresseProject()
        {
            ZIPCompressor compr = new ZIPCompressor();
            var buf = this.MakeBinFromXml();
            byte[] compressed = compr.Compress(buf);
            ushort[] compressedWords = new ushort[(compressed.Length + 1) / 2 + 3]; //Размер хранилища
            compressedWords[0] = (ushort)compressed.Length; // размер сжатого проекта (байты)
            compressedWords[1] = 0x0001; // Версия упаковщика
            compressedWords[2] = 0x0000; // СRС архива проекта
            for (int i = 3; i < (compressed.Length + 1) / 2 + 3; i++)
            {
                if ((i - 2) * 2 + 1 < compressed.Length)
                {
                    compressedWords[i] = (ushort)(compressed[(i - 3) * 2 + 1] << 8);
                }
                else
                {
                    compressedWords[i] = 0;
                }
                compressedWords[i] += compressed[(i - 3) * 2];
            }
            return compressedWords;
        }


        private byte[] Uncompress(ushort[] readedData)
        {
            ZIPCompressor compr = new ZIPCompressor();
            byte[] compressed = new byte[readedData[0]];
            for (int i = 3; i < (compressed.Length + 1) / 2 + 3; i++)
            {
                if ((i - 2) * 2 + 1 < compressed.Length)
                {
                    compressed[(i - 3) * 2 + 1] = (byte)(readedData[i] >> 8);
                }
                compressed[(i - 3) * 2] = (byte)readedData[i];
            }
            return compr.Decompress(compressed);
        }

        private void LoadProjectFromBin(byte[] uncompressed)
        {
            MemoryStream memstream = new MemoryStream();
            memstream.Write(uncompressed, 0, uncompressed.Length);
            if (this._filler.SelectedTab != null)
            {
                switch (MessageBox.Show("Сохранить текущий проект на диске ?"
                                                , "Закрытие проекта", MessageBoxButtons.YesNoCancel))
                {
                    case DialogResult.Yes:
                        if (this.SaveProjectDoc())
                        {
                            this._filler.TabPages.Clear();
                        }
                        else
                        {
                            return;
                        }
                        break;
                    case DialogResult.No:
                        this._filler.TabPages.Clear();
                        break;
                    case DialogResult.Cancel:
                        return;
                }
            }
            memstream.Seek(0, SeekOrigin.Begin);
            XmlTextReader reader = new XmlTextReader(memstream);
            reader.WhitespaceHandling = WhitespaceHandling.None;
            while (reader.Read())
            {
                if ((reader.Name == "Source") && (reader.NodeType != XmlNodeType.EndElement))
                {
                    BSBGLTab myTab = new BSBGLTab();
                    myTab.InitializeBSBGLSheet(reader.GetAttribute("name"), SheetFormat.A0_L, this._device.DeviceType, this._manager);
                    myTab.Selected = true;
                    this._filler.TabPages.Add(myTab);
                    myTab.Schematic.ReadXml(reader);
                }
            }
            this.OutMessage("Проект успешно загружен из устройства");
            reader.Close();
        }


        private bool UncompresseProject(ushort[] compressedWords)
        {
            byte[] uncompressed = this.Uncompress(compressedWords);
            if (uncompressed.Length == 0)
            {
                return false;
            }
            this.LoadProjectFromBin(uncompressed);
            return true;
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR600); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(BSBGLEF); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.programming.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Программирование"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void BSBGLEF_FormClosed(object sender, FormClosedEventArgs e)
        {
            _currentSignalsStruct.RemoveStructQueries();
            if (this._filler.TabPages.Count == 0) return;
            switch (MessageBox.Show("Сохранить текущий проект на диске ?", "Закрытие проекта", MessageBoxButtons.YesNo))
            {
                case DialogResult.Yes:
                    if (SaveProjectDoc())
                    {
                        _filler.TabPages.Clear();
                    }
                    break;
            }
        }
    }
}
