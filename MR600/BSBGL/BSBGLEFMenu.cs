﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AssemblyResources;
using Crownwood.Magic.Menus;

namespace BEMN.MR600.BSBGL
{
    partial class BSBGLEF
    {
        private ToolStrip MainToolStrip;
        private ToolStripButton newToolStripButton;
        private ToolStripButton openToolStripButton;
        private ToolStripButton openProjToolStripButton;
        private ToolStripButton saveToolStripButton;
        private ToolStripButton saveProjToolStripButton;
        private ToolStripButton printToolStripButton;
        private ToolStripButton undoToolStripButton;
        private ToolStripButton redoToolStripButton;
        private ToolStripButton cutToolStripButton;
        private ToolStripButton copyToolStripButton;
        private ToolStripButton pasteToolStripButton;
        private ToolStripButton CompilToDeviceToolStripButton;
        private ToolStripButton StopToolStripButton;
        private ToolStripButton openFromDeviceToolStripButton;
        private ToolStripButton StartToolStripButton;

        #region Toolbox
        protected ToolStrip CreateToolStrip()
        {
            List<ToolStripSeparator> toolStripSepList = new List<ToolStripSeparator>();
            for (int i = 0; i < 7; i++)
            {
                ToolStripSeparator tls = new ToolStripSeparator();
                toolStripSepList.Add(tls);
                tls.Name = "toolStripSeparator" + i;
                tls.Size = new System.Drawing.Size(6, 25);
            }

            this.MainToolStrip = new ToolStrip();
            this.newToolStripButton = new ToolStripButton();
            this.openToolStripButton = new ToolStripButton();
            this.openProjToolStripButton = new ToolStripButton();
            this.openFromDeviceToolStripButton = new ToolStripButton();
            this.saveToolStripButton = new ToolStripButton();
            this.saveProjToolStripButton = new ToolStripButton();
            this.printToolStripButton = new ToolStripButton();
            this.undoToolStripButton = new ToolStripButton();
            this.redoToolStripButton = new ToolStripButton();
            this.cutToolStripButton = new ToolStripButton();
            this.copyToolStripButton = new ToolStripButton();
            this.pasteToolStripButton = new ToolStripButton();
            this.CompilToDeviceToolStripButton = new ToolStripButton();
            this.StopToolStripButton = new ToolStripButton();
            this.StartToolStripButton = new ToolStripButton();

            ToolStripButton startLogicBtn = new ToolStripButton
            {
                DisplayStyle = ToolStripItemDisplayStyle.Image,
                Image = Resources.go1,
                ImageTransparentColor = System.Drawing.Color.Magenta,
                Size = new System.Drawing.Size(23, 22),
                Text = "Запустить СПЛ в устройстве",
            };
            startLogicBtn.Click += this.StartLogicProgram;

            ToolStripButton stopLogicBtn = new ToolStripButton
            {
                DisplayStyle = ToolStripItemDisplayStyle.Image,
                Image = Resources.stop1,
                ImageTransparentColor = System.Drawing.Color.Magenta,
                Size = new System.Drawing.Size(23, 22),
                Text = "Остановить выполнение СПЛ в устройстве",
            };
            stopLogicBtn.Click += this.StopLogicProgram;

            this.MainToolStrip.Items.Add(this.newToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[0]);
            this.MainToolStrip.Items.Add(this.openToolStripButton);
            this.MainToolStrip.Items.Add(this.openProjToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[1]);
            this.MainToolStrip.Items.Add(this.saveToolStripButton);
            this.MainToolStrip.Items.Add(this.saveProjToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[2]);
            this.MainToolStrip.Items.Add(this.printToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[3]);
            this.MainToolStrip.Items.Add(this.undoToolStripButton);
            this.MainToolStrip.Items.Add(this.redoToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[4]);
            this.MainToolStrip.Items.Add(this.cutToolStripButton);
            this.MainToolStrip.Items.Add(this.copyToolStripButton);
            this.MainToolStrip.Items.Add(this.pasteToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[5]);
            this.MainToolStrip.Items.Add(this.CompilToDeviceToolStripButton);
            this.MainToolStrip.Items.Add(this.openFromDeviceToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[6]);
            this.MainToolStrip.Items.Add(this.StartToolStripButton);
            this.MainToolStrip.Items.Add(this.StopToolStripButton);
            this.MainToolStrip.Items.Add(new ToolStripSeparator { Margin = new Padding(20, 0, 0, 0) });
            this.MainToolStrip.Items.Add(startLogicBtn);
            this.MainToolStrip.Items.Add(stopLogicBtn);


            this.MainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.MainToolStrip.Name = "MainToolStrip";
            this.MainToolStrip.Size = new System.Drawing.Size(816, 25);
            this.MainToolStrip.TabIndex = 0;
            this.MainToolStrip.Text = "toolStrip1";

            this.newToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = Resources.filenew;
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.newToolStripButton.Text = "Новая схема";
            this.newToolStripButton.Click += new EventHandler(this.OnFileNew);

            this.openToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = Resources.fileopen;
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "Открыть документ";
            this.openToolStripButton.Click += new EventHandler(this.OnFileOpen);

            this.openProjToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.openProjToolStripButton.Image = Resources.fileopenproject;
            this.openProjToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openProjToolStripButton.Name = "openProjToolStripButton";
            this.openProjToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openProjToolStripButton.Text = "Открыть проект";
            this.openProjToolStripButton.Click += new EventHandler(this.OnFileOpenProject);

            this.openFromDeviceToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.openFromDeviceToolStripButton.Image = Resources.OpenFromDeviceBmp;
            this.openFromDeviceToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openFromDeviceToolStripButton.Name = "openFromDeviceToolStripButton";
            this.openFromDeviceToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openFromDeviceToolStripButton.Text = "Загрузить проект из устройства";
            this.openFromDeviceToolStripButton.Click += new EventHandler(this.OnFileOpenFromDevice);

            this.saveToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = Resources.filesave;
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "Сохранить документ";
            this.saveToolStripButton.Click += new EventHandler(this.OnFileSave);

            this.saveProjToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.saveProjToolStripButton.Image = Resources.filesaveproject;
            this.saveProjToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveProjToolStripButton.Name = "saveProjToolStripButton";
            this.saveProjToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveProjToolStripButton.Text = "Сохранить проект";
            this.saveProjToolStripButton.Click += new EventHandler(this.OnFileSaveProject);

            this.printToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.printToolStripButton.Image = Resources.print;
            this.printToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.printToolStripButton.Text = "Печать";
            this.printToolStripButton.Click += new EventHandler(this.printToolStripButton_Click);

            this.undoToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.undoToolStripButton.Image = Resources.undo;
            this.undoToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.undoToolStripButton.Name = "undoToolStripButton";
            this.undoToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.undoToolStripButton.Text = "Отмена";
            this.undoToolStripButton.Click += new EventHandler(this.OnUndo);

            this.redoToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.redoToolStripButton.Image = Resources.redo;
            this.redoToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.redoToolStripButton.Name = "redoToolStripButton";
            this.redoToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.redoToolStripButton.Text = "Восстановить отмененое";
            this.redoToolStripButton.Click += new EventHandler(this.OnRedo);

            this.cutToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.cutToolStripButton.Image = Resources.cut;
            this.cutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripButton.Name = "cutToolStripButton";
            this.cutToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.cutToolStripButton.Text = "Вырезать";
            this.cutToolStripButton.Click += new EventHandler(this.OnEditCut);

            this.copyToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.copyToolStripButton.Image = Resources.copy;
            this.copyToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.copyToolStripButton.Text = "Копировать";
            this.copyToolStripButton.Click += new EventHandler(this.OnEditCopy);

            this.pasteToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.pasteToolStripButton.Image = Resources.paste;
            this.pasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            this.pasteToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.pasteToolStripButton.Text = "Вставить";
            this.pasteToolStripButton.Click += new EventHandler(this.OnEditPaste);

            this.CompilToDeviceToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.CompilToDeviceToolStripButton.Image = Resources.go;
            this.CompilToDeviceToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CompilToDeviceToolStripButton.Name = "CompilToDeviceToolStripButton";
            this.CompilToDeviceToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.CompilToDeviceToolStripButton.Text = "Компиляция, загрузка в устройство и запуск эмулятора";
            this.CompilToDeviceToolStripButton.Click += new EventHandler(this.OnCompileUpload);

            this.StartToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.StartToolStripButton.Image = Resources.startemul;
            this.StartToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.StartToolStripButton.Name = "StartEmulation";
            this.StartToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.StartToolStripButton.Text = "Запуск эмуляции";
            this.StartToolStripButton.Click += new EventHandler(this.OnStartEmul);

            this.StopToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.StopToolStripButton.Image = Resources.stop;
            this.StopToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.StopToolStripButton.Name = "CompilToDeviceToolStripButton";
            this.StopToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.StopToolStripButton.Text = "Остановка эмуляции";
            this.StopToolStripButton.Click += new EventHandler(this.OnStop);

            return this.MainToolStrip;
        }


        #endregion
 
        protected void CreateMenus()
        {
            MenuControl topMenu = new MenuControl();
            topMenu.Style = this._style;
            topMenu.MultiLine = false;

            MenuCommand topFile = new MenuCommand("&Файл");
            MenuCommand topEdit = new MenuCommand("Правка");
            MenuCommand topView = new MenuCommand("Вид");

            topMenu.MenuCommands.AddRange(new MenuCommand[] { topFile, topEdit, topView });

            //File
            MenuCommand topFileNew = new MenuCommand("Новый", new EventHandler(this.OnFileNew));
            MenuCommand topFileOpen = new MenuCommand("Открыть документ", new EventHandler(this.OnFileOpen));
            MenuCommand topFileOpenProject = new MenuCommand("Открыть проект", new EventHandler(this.OnFileOpenProject));
            MenuCommand topFileClose = new MenuCommand("Закрыть документ", new EventHandler(this.OnFileClose));
            MenuCommand topFileCloseProject = new MenuCommand("Закрыть проект", new EventHandler(this.OnFileCloseProject));
            MenuCommand topFileSave = new MenuCommand("Сохранить документ", new EventHandler(this.OnFileSave));
            MenuCommand topFileSaveProject = new MenuCommand("Сохранить проект", new EventHandler(this.OnFileSaveProject));
            topFile.MenuCommands.AddRange(new MenuCommand[] { topFileNew, topFileOpen, topFileOpenProject,
                                                              topFileClose,topFileCloseProject,
                                                              topFileSave, topFileSaveProject });
            //Edit
            MenuCommand topEditCopy = new MenuCommand("Копировать", new EventHandler(this.OnEditCopy));
            MenuCommand topEditPaste = new MenuCommand("Вставить", new EventHandler(this.OnEditPaste));

            topEdit.MenuCommands.AddRange(new MenuCommand[] { topEditCopy, topEditPaste });
            //View
            MenuCommand topViewOutputWindow = new MenuCommand("Сообщения", new EventHandler(this.OnViewOutputWindow));
            MenuCommand topViewToolWindow = new MenuCommand("Библиотека", new EventHandler(this.OnViewToolWindow));
            topView.MenuCommands.AddRange(new MenuCommand[] {topViewOutputWindow, topViewToolWindow});
            
            topMenu.Dock = DockStyle.Top;
            Controls.Add(topMenu);
        }
    }
}
