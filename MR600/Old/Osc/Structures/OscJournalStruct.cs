﻿using System;
using System.Collections;
using System.Collections.Generic;
using BEMN.MBServer;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR600.Old.Osc.Structures
{
    public class OscJournalStruct : StructBase
    {
        #region [Constants]
        private const string DATE_PATTERN = "{0:X2}.{1:X2}.{2:X2}";
        private const string TIME_PATTERN = "{0:X2}:{1:X2}:{2:X2}.{3:X2}";
        public const int OscSize = 0x8000; // байт
        #endregion [Constants]

        
        public static int RecordIndex;

        #region [Private fields]
        [Layout(0)] private ushort _alarmMark;
        [Layout(1)] private ushort _alarm;
        [Layout(2)] private ushort _startOsc;
        [Layout(3)] private ushort _res1;
        [Layout(4, Count = 8)] private byte[] _dateTime;
        [Layout(5)] private ushort _stage;
        [Layout(6)] private ushort _valueStage;
        [Layout(7)] private ushort _f;
        [Layout(8)] private ushort _ua;
        [Layout(9)] private ushort _ub;
        [Layout(10)] private ushort _uc;
        [Layout(11)] private ushort _uo;
        [Layout(12)] private ushort _uab;
        [Layout(13)] private ushort _ubc;
        [Layout(14)] private ushort _uca;
        [Layout(15)] private ushort _u0;
        [Layout(16)] private ushort _u1;
        [Layout(17)] private ushort _u2;
        [Layout(18)] private ushort _res2;
        [Layout(19)] private ushort _discrets;
        #endregion [Private fields]
        
        #region [Properties]
        /// <summary>
        /// Смещение в массиве считанных данных осциллографа 
        /// на начало записи непосредственно самой осциллограммы (в байтах)
        /// </summary>
        public ushort Start => (ushort) (this._startOsc + OscSize);

        public ushort Alarm => (ushort) (this._alarm + OscSize);

        public int AlarmTime => this.Start > this.Alarm
            ? (this.Alarm + (OscSize - this.Start))/2/this.CountingSize
            : (this.Alarm - this.Start)/2/this.CountingSize;

        public string Stage => this.GetCode(this._stage, this._valueStage);

        private static List<string> Stages => new List<string>
        {
            "ВЗ-1",
            "ВЗ-2",
            "ВЗ-3",
            "ВЗ-4",
            "ВЗ-5",
            "ВЗ-6",
            "ВЗ-7",
            "ВЗ-8",
            "U>",
            "U>>",
            "U>>>",
            "U>>>>",
            "U<",
            "U<<",
            "U<<<",
            "U<<<<",
            "U0>",
            "U0>>",
            "U0>>>",
            "U0>>>>",
            "U1<",
            "U1<<",
            "U2>",
            "U2>>",
            "F>",
            "F>>",
            "F>>>",
            "F>>>>",
            "F<",
            "F<<",
            "F<<<",
            "F<<<<"
        };

        public int SizeTime => OscSize / 2 / this.CountingSize;

        public int CountingSize => this.Version >= 2.03 ? 5 : 4; // слов

        public double Version { get; set; } = 2.11;

        private static Dictionary<ushort, string> MsgDictionary => new Dictionary<ushort, string>
        {
            {0, "Журнал пуст"},
            {1, "Сигнализация"},
            {2, "Авария"},
            {118, "Журнал пуст"},
            {255, "Нет сообщения"}
        };

        public string GetCode(ushort code, ushort phase)
        {
            string group = (0 == (code & 0x80)) ? "основная" : "резерв.";
            string phaseStr = string.Empty;
            phaseStr += (0 == (phase & 0x08)) ? " " : "_";
            phaseStr += (0 == (phase & 0x04)) ? " " : "A";
            phaseStr += (0 == (phase & 0x02)) ? " " : "B";
            phaseStr += (0 == (phase & 0x01)) ? " " : "C";

            byte codeByte = (byte)((code & 0x7F)-1);
            try
            {
                return Stages[codeByte] + " " + group + " " + phaseStr;
            }
            catch 
            {
                return "";
            }
        }
            
        /// <summary>
        /// Запись журнала осциллографа
        /// </summary>
        public List<object> GetRecord
        {
            get
            {
                string message = MsgDictionary.Count > this._dateTime[0] ? MsgDictionary[this._dateTime[0]] : "Ошибка";
                byte discr = Common.LOBYTE(this._discrets);
                byte ls = Common.HIBYTE(this._discrets);
                return new List<object>
                    {
                        message, //Номер
                        this.GetDate,
                        this.GetTime, //Время
                        this.Stage,
                        new string(Common.BitsToString(new BitArray(new[] {discr})).Reverse().ToArray()),
                        new string(Common.BitsToString(new BitArray(new[] {ls})).Reverse().ToArray())
                    };
            }
        }

        public bool IsReady => this._alarmMark != 0;

        /// <summary>
        /// Время сообщения
        /// </summary>
        public string GetTime => string.Format
            (
                TIME_PATTERN,
                this._dateTime[4],
                this._dateTime[5],
                this._dateTime[6],
                this._dateTime[7]
            );

        /// <summary>
        /// Дата сообщения
        /// </summary>
        public string GetDate => string.Format
            (
                DATE_PATTERN,
                this._dateTime[3],
                this._dateTime[2],
                this._dateTime[1]
            );

        public string GetFormattedDateTimeAlarm(int alarm)
        {
            try
            {
                int year = Convert.ToInt32($"{this._dateTime[1]:X2}");
                int month = Convert.ToInt32($"{this._dateTime[2]:X2}");
                int day = Convert.ToInt32($"{this._dateTime[3]:X2}");
                int hour = Convert.ToInt32($"{this._dateTime[4]:X2}");
                int min = Convert.ToInt32($"{this._dateTime[5]:X2}");
                int sec = Convert.ToInt32($"{this._dateTime[6]:X2}");
                int msec = Convert.ToInt32($"{this._dateTime[7]:X2}");
                DateTime a = new DateTime(year, month, day, hour, min, sec, msec*10);
                DateTime result = a.AddMilliseconds(alarm);
                return
                    $"{result.Month:D2}/{result.Day:D2}/{result.Year:D2},{result.Hour:D2}:{result.Minute:D2}:{result.Second:D2}.{result.Millisecond:D3}";
            }
            catch (Exception)
            {
                return GetFormattedDateTime;
            }
        }

        public string GetFormattedDateTime
        {
            get
            {
                int year = Convert.ToInt32(string.Format("{0:X2}", this._dateTime[1]));
                int month = Convert.ToInt32(string.Format("{0:X2}", this._dateTime[2]));
                int day = Convert.ToInt32(string.Format("{0:X2}", this._dateTime[3]));
                int hour = Convert.ToInt32(string.Format("{0:X2}", this._dateTime[4]));
                int min = Convert.ToInt32(string.Format("{0:X2}", this._dateTime[5]));
                int sec = Convert.ToInt32(string.Format("{0:X2}", this._dateTime[6]));
                int msec = Convert.ToInt32(string.Format("{0:X3}", this._dateTime[7]));
                return $"{month:D2}/{day:D2}/{year:D2},{hour:D2}:{min:D2}:{sec:D2}.{(msec < 100 ? msec*10 : msec):D3}";
            }
        }
        
        #endregion [Private Properties]
    }
}
