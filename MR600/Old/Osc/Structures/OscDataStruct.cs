﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR600.Old.Osc.Structures
{
    /// <summary>
    /// Страница осциллографа
    /// </summary>
    public class OscDataStruct : StructBase
    {
        #region [Private fields]
        private const int BIG_SLOT_LENGHT = 125;
        [Layout(0, Count = 16384)] private ushort[] _words; 
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// Страница осцилограммы в виде массива слов
        /// </summary>
        public ushort[] Words
        {
            get { return _words; }
        } 
        #endregion [Properties]


        #region [IStruct Members]
        
        public override object GetSlots(ushort start, bool slotArray, int length)
        {
            return base.GetSlots(start, slotArray, length > BIG_SLOT_LENGHT ? length : BIG_SLOT_LENGHT);
        }

        #endregion [IStruct Members]
    }
}
