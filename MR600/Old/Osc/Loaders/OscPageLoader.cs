﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices;
using BEMN.MBServer;
using BEMN.MR600.Old.Osc.Structures;
using BEMN.Devices.MemoryEntityClasses;

namespace BEMN.MR600.Old.Osc.Loaders
{
    /// <summary>
    /// Класс загрузки страниц осциллограммы
    /// </summary>
    public class OscPageLoader
    {
        #region [Private fields]
        /// <summary>
        /// Страница осциллограммы
        /// </summary>
        private readonly MemoryEntity<ArrayStruct> _oscArray;
        /// <summary>
        /// Запись о текущей осцилограмме
        /// </summary>
        private OscJournalStruct _journalStruct;

        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Прочитана одна страница
        /// </summary>
        public event Action PageRead;
        /// <summary>
        /// Осцилограмма загружена успешно
        /// </summary>
        public event Action OscReadSuccessful;
        public event Action OscReadFail;
        private Device _device;
        #endregion [Events]
        

        #region [Ctor's]

        public OscPageLoader(MR600 device)
        {
            this._device = device;
            ArrayStruct.FullSize = 16384; //длинна осцилоги
            this._oscArray = new MemoryEntity<ArrayStruct>("Отсчёты", this._device, 0x4000);
            this._oscArray.AllReadOk += o =>
            {
                this._oscArray.RemoveStructQueries();
                OscReadOk();
            };
            this._oscArray.AllReadFail += o =>
            {
                this._oscArray.RemoveStructQueries();
                this.RaiseOscReadFail();
            };
            this._oscArray.ReadOk += this.OnRaisePageRead;
        }

        private void RaiseOscReadFail()
        {
            if (this.OscReadFail != null)
            {
                this.OscReadFail.Invoke();
            }
        } 
        #endregion [Ctor's]


        #region [Public members]

        /// <summary>
        /// Запускает чтение указанной осцилограммы
        /// </summary>
        /// <param name="journalStruct">Запись журнала о осцилограмме</param>
        public void StartRead(OscJournalStruct journalStruct)
        {
            this._journalStruct = journalStruct;
            this._oscArray.LoadStruct();
        }

        #endregion [Public members]


        #region [Event Raise Members]
        /// <summary>
        /// Вызывает событие "Осцилограмма загружена успешно"
        /// </summary>
        private void OnRaiseOscReadSuccessful()
        {
            if (this.OscReadSuccessful != null)
            {
                this.OscReadSuccessful.Invoke();
            }
        }
        /// <summary>
        /// Вызывает событие "Прочитана одна страница"
        /// </summary>
        private void OnRaisePageRead( object sender)
        {
            if (this.PageRead != null)
            {
                this.PageRead.Invoke();
            }
        } 
        #endregion [Event Raise Members]


        #region [Help members]

        /// <summary>
        /// Осцилограмма прочитана
        /// </summary>
        private void OscReadOk()
        {
            if (Common.VersionConverter(this._device.DeviceVersion) >= 2.03)
            {
                int a = this._journalStruct.Alarm; 
                int b = a + 2048 * 10 + 8; // в байтах
                if (b >= 32768)
                {
                    b = b - 32768;
                }
                b = b / 2;
                List<ushort> read = new List<ushort>(this._oscArray.Value.Data);
                List<ushort> result = new List<ushort>(read.Skip(b));
                result.AddRange(read.Take(b));
                this.ResultArray = result.ToArray();
            }
            else
            {
                int startOsc = this._journalStruct.Start / 2; // в словах
                List<ushort> readMassiv = new List<ushort>(this._oscArray.Value.Data);
                List<ushort> result = new List<ushort>();
                result.AddRange(readMassiv.Skip(startOsc));
                result.AddRange(readMassiv.Take(startOsc));
                this.ResultArray = result.ToArray();
            }

            this.OnRaiseOscReadSuccessful();
        }

        #endregion [Help members]


        #region [Properties]
        /// <summary>
        /// Готовый массив осц
        /// </summary>
        public ushort[] ResultArray { get; private set; }
        #endregion [Properties]

        public int SlotCount
        {
            get { return this._oscArray.Slots.Count; }
        }
    }
}
