﻿namespace BEMN.MR600.Old.Osc.ShowOsc
{
    partial class Mr600OscilloscopeOldResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MAINTABLE = new System.Windows.Forms.TableLayoutPanel();
            this.hScrollBar4 = new System.Windows.Forms.HScrollBar();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MAINPANEL = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this._discretsLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._discrestsChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this._discrete5Button = new System.Windows.Forms.Button();
            this._discrete16Button = new System.Windows.Forms.Button();
            this._discrete15Button = new System.Windows.Forms.Button();
            this._discrete14Button = new System.Windows.Forms.Button();
            this._discrete13Button = new System.Windows.Forms.Button();
            this._discrete12Button = new System.Windows.Forms.Button();
            this._discrete11Button = new System.Windows.Forms.Button();
            this._discrete10Button = new System.Windows.Forms.Button();
            this._discrete9Button = new System.Windows.Forms.Button();
            this._discrete8Button = new System.Windows.Forms.Button();
            this._discrete7Button = new System.Windows.Forms.Button();
            this._discrete6Button = new System.Windows.Forms.Button();
            this._discrete2Button = new System.Windows.Forms.Button();
            this._discrete3Button = new System.Windows.Forms.Button();
            this._discrete4Button = new System.Windows.Forms.Button();
            this._discrete1Button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this._voltageLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._uScroll = new System.Windows.Forms.VScrollBar();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this._u3Button = new System.Windows.Forms.Button();
            this._u4Button = new System.Windows.Forms.Button();
            this._u1Button = new System.Windows.Forms.Button();
            this._u2Button = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this._uChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this._voltageChartDecreaseButton = new System.Windows.Forms.Button();
            this._voltageChartIncreaseButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.MarkersTable = new System.Windows.Forms.TableLayoutPanel();
            this._marker1TrackBar = new System.Windows.Forms.TrackBar();
            this._marker2TrackBar = new System.Windows.Forms.TrackBar();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this._measuringChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._markerScrollPanel = new System.Windows.Forms.Panel();
            this._marker2Box = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._marker2D11 = new System.Windows.Forms.Label();
            this._marker2D10 = new System.Windows.Forms.Label();
            this._marker2D9 = new System.Windows.Forms.Label();
            this._marker2D16 = new System.Windows.Forms.Label();
            this._marker2D15 = new System.Windows.Forms.Label();
            this._marker2D14 = new System.Windows.Forms.Label();
            this._marker2D13 = new System.Windows.Forms.Label();
            this._marker2D12 = new System.Windows.Forms.Label();
            this._marker2D8 = new System.Windows.Forms.Label();
            this._marker2D7 = new System.Windows.Forms.Label();
            this._marker2D6 = new System.Windows.Forms.Label();
            this._marker2D5 = new System.Windows.Forms.Label();
            this._marker2D4 = new System.Windows.Forms.Label();
            this._marker2D3 = new System.Windows.Forms.Label();
            this._marker2D2 = new System.Windows.Forms.Label();
            this._marker2D1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._marker2U4 = new System.Windows.Forms.Label();
            this._marker2U3 = new System.Windows.Forms.Label();
            this._marker2U2 = new System.Windows.Forms.Label();
            this._marker2U1 = new System.Windows.Forms.Label();
            this._marker1Box = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._marker1U4 = new System.Windows.Forms.Label();
            this._marker1U3 = new System.Windows.Forms.Label();
            this._marker1U2 = new System.Windows.Forms.Label();
            this._marker1U1 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._marker1D11 = new System.Windows.Forms.Label();
            this._marker1D10 = new System.Windows.Forms.Label();
            this._marker1D9 = new System.Windows.Forms.Label();
            this._marker1D16 = new System.Windows.Forms.Label();
            this._marker1D15 = new System.Windows.Forms.Label();
            this._marker1D14 = new System.Windows.Forms.Label();
            this._marker1D13 = new System.Windows.Forms.Label();
            this._marker1D12 = new System.Windows.Forms.Label();
            this._marker1D8 = new System.Windows.Forms.Label();
            this._marker1D7 = new System.Windows.Forms.Label();
            this._marker1D6 = new System.Windows.Forms.Label();
            this._marker1D5 = new System.Windows.Forms.Label();
            this._marker1D4 = new System.Windows.Forms.Label();
            this._marker1D3 = new System.Windows.Forms.Label();
            this._marker1D2 = new System.Windows.Forms.Label();
            this._marker1D1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._deltaTimeBox = new System.Windows.Forms.GroupBox();
            this._markerTimeDelta = new System.Windows.Forms.Label();
            this._marker2TimeBox = new System.Windows.Forms.GroupBox();
            this._marker2Time = new System.Windows.Forms.Label();
            this._marker1TimeBox = new System.Windows.Forms.GroupBox();
            this._marker1Time = new System.Windows.Forms.Label();
            this._xDecreaseButton = new System.Windows.Forms.Button();
            this._discrestsСheckBox = new System.Windows.Forms.CheckBox();
            this._markCheckBox = new System.Windows.Forms.CheckBox();
            this._markerCheckBox = new System.Windows.Forms.CheckBox();
            this._newMarkToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._xIncreaseButton = new System.Windows.Forms.Button();
            this._oscRunСheckBox = new System.Windows.Forms.CheckBox();
            this._voltageСheckBox = new System.Windows.Forms.CheckBox();
            this.MAINTABLE.SuspendLayout();
            this.MAINPANEL.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this._discretsLayoutPanel.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this._voltageLayoutPanel.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.MarkersTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._marker1TrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._marker2TrackBar)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._markerScrollPanel.SuspendLayout();
            this._marker2Box.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this._marker1Box.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this._deltaTimeBox.SuspendLayout();
            this._marker2TimeBox.SuspendLayout();
            this._marker1TimeBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // MAINTABLE
            // 
            this.MAINTABLE.ColumnCount = 2;
            this.MAINTABLE.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MAINTABLE.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.MAINTABLE.Controls.Add(this.hScrollBar4, 0, 2);
            this.MAINTABLE.Controls.Add(this.menuStrip1, 0, 0);
            this.MAINTABLE.Controls.Add(this.MAINPANEL, 0, 1);
            this.MAINTABLE.Controls.Add(this.panel3, 1, 1);
            this.MAINTABLE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MAINTABLE.Location = new System.Drawing.Point(0, 0);
            this.MAINTABLE.Margin = new System.Windows.Forms.Padding(0);
            this.MAINTABLE.Name = "MAINTABLE";
            this.MAINTABLE.RowCount = 2;
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MAINTABLE.Size = new System.Drawing.Size(1384, 753);
            this.MAINTABLE.TabIndex = 1;
            // 
            // hScrollBar4
            // 
            this.hScrollBar4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.hScrollBar4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hScrollBar4.LargeChange = 1;
            this.hScrollBar4.Location = new System.Drawing.Point(0, 733);
            this.hScrollBar4.Maximum = 10;
            this.hScrollBar4.Minimum = 10;
            this.hScrollBar4.Name = "hScrollBar4";
            this.hScrollBar4.Size = new System.Drawing.Size(1084, 20);
            this.hScrollBar4.TabIndex = 20;
            this.hScrollBar4.Value = 10;
            this.hScrollBar4.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Silver;
            this.MAINTABLE.SetColumnSpan(this.menuStrip1, 2);
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1384, 24);
            this.menuStrip1.TabIndex = 18;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // MAINPANEL
            // 
            this.MAINPANEL.Controls.Add(this.panel1);
            this.MAINPANEL.Controls.Add(this.MarkersTable);
            this.MAINPANEL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MAINPANEL.Location = new System.Drawing.Point(3, 28);
            this.MAINPANEL.Name = "MAINPANEL";
            this.MAINPANEL.Size = new System.Drawing.Size(1078, 702);
            this.MAINPANEL.TabIndex = 32;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 90);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1078, 612);
            this.panel1.TabIndex = 32;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoSize = true;
            this.panel2.Controls.Add(this.splitter5);
            this.panel2.Controls.Add(this.splitter2);
            this.panel2.Controls.Add(this._discretsLayoutPanel);
            this.panel2.Controls.Add(this.splitter1);
            this.panel2.Controls.Add(this._voltageLayoutPanel);
            this.panel2.Controls.Add(this.splitter3);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1055, 946);
            this.panel2.TabIndex = 0;
            // 
            // splitter5
            // 
            this.splitter5.BackColor = System.Drawing.Color.Black;
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter5.Location = new System.Drawing.Point(0, 933);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(1055, 3);
            this.splitter5.TabIndex = 36;
            this.splitter5.TabStop = false;
            this.splitter5.Visible = false;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.Color.Black;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Enabled = false;
            this.splitter2.Location = new System.Drawing.Point(0, 930);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(1055, 3);
            this.splitter2.TabIndex = 30;
            this.splitter2.TabStop = false;
            // 
            // _discretsLayoutPanel
            // 
            this._discretsLayoutPanel.ColumnCount = 3;
            this._discretsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this._discretsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._discretsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._discretsLayoutPanel.Controls.Add(this._discrestsChart, 1, 1);
            this._discretsLayoutPanel.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this._discretsLayoutPanel.Controls.Add(this.label2, 1, 0);
            this._discretsLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._discretsLayoutPanel.Location = new System.Drawing.Point(0, 544);
            this._discretsLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this._discretsLayoutPanel.Name = "_discretsLayoutPanel";
            this._discretsLayoutPanel.RowCount = 2;
            this._discretsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._discretsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._discretsLayoutPanel.Size = new System.Drawing.Size(1055, 386);
            this._discretsLayoutPanel.TabIndex = 29;
            // 
            // _discrestsChart
            // 
            this._discrestsChart.BkGradient = false;
            this._discrestsChart.BkGradientAngle = 90;
            this._discrestsChart.BkGradientColor = System.Drawing.Color.White;
            this._discrestsChart.BkGradientRate = 0.5F;
            this._discrestsChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._discrestsChart.BkRestrictedToChartPanel = false;
            this._discrestsChart.BkShinePosition = 1F;
            this._discrestsChart.BkTransparency = 0F;
            this._discrestsChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._discrestsChart.BorderExteriorLength = 0;
            this._discrestsChart.BorderGradientAngle = 225;
            this._discrestsChart.BorderGradientLightPos1 = 1F;
            this._discrestsChart.BorderGradientLightPos2 = -1F;
            this._discrestsChart.BorderGradientRate = 0.5F;
            this._discrestsChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._discrestsChart.BorderLightIntermediateBrightness = 0F;
            this._discrestsChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._discrestsChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._discrestsChart.ChartPanelBkTransparency = 0F;
            this._discrestsChart.ControlShadow = false;
            this._discrestsChart.CoordinateAxesVisible = true;
            this._discrestsChart.CoordinateAxisColor = System.Drawing.Color.Transparent;
            this._discrestsChart.CoordinateXOrigin = 0D;
            this._discrestsChart.CoordinateYMax = 100D;
            this._discrestsChart.CoordinateYMin = -100D;
            this._discrestsChart.CoordinateYOrigin = 0D;
            this._discrestsChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._discrestsChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._discrestsChart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._discrestsChart.FooterColor = System.Drawing.Color.Black;
            this._discrestsChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._discrestsChart.FooterVisible = false;
            this._discrestsChart.GridColor = System.Drawing.Color.MistyRose;
            this._discrestsChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._discrestsChart.GridVisible = true;
            this._discrestsChart.GridXSubTicker = 0;
            this._discrestsChart.GridXTicker = 10;
            this._discrestsChart.GridYSubTicker = 0;
            this._discrestsChart.GridYTicker = 2;
            this._discrestsChart.HeaderColor = System.Drawing.Color.Black;
            this._discrestsChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._discrestsChart.HeaderVisible = false;
            this._discrestsChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._discrestsChart.InnerBorderLength = 0;
            this._discrestsChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._discrestsChart.LegendBkColor = System.Drawing.Color.White;
            this._discrestsChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._discrestsChart.LegendVisible = false;
            this._discrestsChart.Location = new System.Drawing.Point(78, 26);
            this._discrestsChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._discrestsChart.MiddleBorderLength = 0;
            this._discrestsChart.Name = "_discrestsChart";
            this._discrestsChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._discrestsChart.OuterBorderLength = 0;
            this._discrestsChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._discrestsChart.Precision = 0;
            this._discrestsChart.RoundRadius = 10;
            this._discrestsChart.ShadowColor = System.Drawing.Color.DimGray;
            this._discrestsChart.ShadowDepth = 8;
            this._discrestsChart.ShadowRate = 0.5F;
            this._discrestsChart.Size = new System.Drawing.Size(954, 357);
            this._discrestsChart.TabIndex = 29;
            this._discrestsChart.Text = "daS_Net_XYChart2";
            this._discrestsChart.XMax = 100D;
            this._discrestsChart.XMin = 0D;
            this._discrestsChart.XScaleColor = System.Drawing.Color.White;
            this._discrestsChart.XScaleVisible = false;
            this._discrestsChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Chart_MouseClick);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.tableLayoutPanel5.Controls.Add(this._discrete5Button, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this._discrete16Button, 0, 15);
            this.tableLayoutPanel5.Controls.Add(this._discrete15Button, 0, 14);
            this.tableLayoutPanel5.Controls.Add(this._discrete14Button, 0, 13);
            this.tableLayoutPanel5.Controls.Add(this._discrete13Button, 0, 12);
            this.tableLayoutPanel5.Controls.Add(this._discrete12Button, 0, 11);
            this.tableLayoutPanel5.Controls.Add(this._discrete11Button, 0, 10);
            this.tableLayoutPanel5.Controls.Add(this._discrete10Button, 0, 9);
            this.tableLayoutPanel5.Controls.Add(this._discrete9Button, 0, 8);
            this.tableLayoutPanel5.Controls.Add(this._discrete8Button, 0, 7);
            this.tableLayoutPanel5.Controls.Add(this._discrete7Button, 0, 6);
            this.tableLayoutPanel5.Controls.Add(this._discrete6Button, 0, 5);
            this.tableLayoutPanel5.Controls.Add(this._discrete2Button, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this._discrete3Button, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this._discrete4Button, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this._discrete1Button, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 26);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 41;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(69, 357);
            this.tableLayoutPanel5.TabIndex = 30;
            // 
            // _discrete5Button
            // 
            this._discrete5Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete5Button.AutoSize = true;
            this._discrete5Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete5Button.ForeColor = System.Drawing.Color.Black;
            this._discrete5Button.Location = new System.Drawing.Point(0, 88);
            this._discrete5Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete5Button.Name = "_discrete5Button";
            this._discrete5Button.Size = new System.Drawing.Size(66, 22);
            this._discrete5Button.TabIndex = 16;
            this._discrete5Button.Text = "Д5";
            this._discrete5Button.UseVisualStyleBackColor = false;
            this._discrete5Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete16Button
            // 
            this._discrete16Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete16Button.AutoSize = true;
            this._discrete16Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete16Button.ForeColor = System.Drawing.Color.Black;
            this._discrete16Button.Location = new System.Drawing.Point(0, 330);
            this._discrete16Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete16Button.Name = "_discrete16Button";
            this._discrete16Button.Size = new System.Drawing.Size(66, 22);
            this._discrete16Button.TabIndex = 27;
            this._discrete16Button.Text = "ЛС8";
            this._discrete16Button.UseVisualStyleBackColor = false;
            this._discrete16Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete15Button
            // 
            this._discrete15Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete15Button.AutoSize = true;
            this._discrete15Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete15Button.ForeColor = System.Drawing.Color.Black;
            this._discrete15Button.Location = new System.Drawing.Point(0, 308);
            this._discrete15Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete15Button.Name = "_discrete15Button";
            this._discrete15Button.Size = new System.Drawing.Size(66, 22);
            this._discrete15Button.TabIndex = 26;
            this._discrete15Button.Text = "ЛС7";
            this._discrete15Button.UseVisualStyleBackColor = false;
            this._discrete15Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete14Button
            // 
            this._discrete14Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete14Button.AutoSize = true;
            this._discrete14Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete14Button.ForeColor = System.Drawing.Color.Black;
            this._discrete14Button.Location = new System.Drawing.Point(0, 286);
            this._discrete14Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete14Button.Name = "_discrete14Button";
            this._discrete14Button.Size = new System.Drawing.Size(66, 22);
            this._discrete14Button.TabIndex = 25;
            this._discrete14Button.Text = "ЛС6";
            this._discrete14Button.UseVisualStyleBackColor = false;
            this._discrete14Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete13Button
            // 
            this._discrete13Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete13Button.AutoSize = true;
            this._discrete13Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete13Button.ForeColor = System.Drawing.Color.Black;
            this._discrete13Button.Location = new System.Drawing.Point(0, 264);
            this._discrete13Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete13Button.Name = "_discrete13Button";
            this._discrete13Button.Size = new System.Drawing.Size(66, 22);
            this._discrete13Button.TabIndex = 24;
            this._discrete13Button.Text = "ЛС5";
            this._discrete13Button.UseVisualStyleBackColor = false;
            this._discrete13Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete12Button
            // 
            this._discrete12Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete12Button.AutoSize = true;
            this._discrete12Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete12Button.ForeColor = System.Drawing.Color.Black;
            this._discrete12Button.Location = new System.Drawing.Point(0, 242);
            this._discrete12Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete12Button.Name = "_discrete12Button";
            this._discrete12Button.Size = new System.Drawing.Size(66, 22);
            this._discrete12Button.TabIndex = 23;
            this._discrete12Button.Text = "ЛС4";
            this._discrete12Button.UseVisualStyleBackColor = false;
            this._discrete12Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete11Button
            // 
            this._discrete11Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete11Button.AutoSize = true;
            this._discrete11Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete11Button.ForeColor = System.Drawing.Color.Black;
            this._discrete11Button.Location = new System.Drawing.Point(0, 220);
            this._discrete11Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete11Button.Name = "_discrete11Button";
            this._discrete11Button.Size = new System.Drawing.Size(66, 22);
            this._discrete11Button.TabIndex = 22;
            this._discrete11Button.Text = "ЛС3";
            this._discrete11Button.UseVisualStyleBackColor = false;
            this._discrete11Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete10Button
            // 
            this._discrete10Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete10Button.AutoSize = true;
            this._discrete10Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete10Button.ForeColor = System.Drawing.Color.Black;
            this._discrete10Button.Location = new System.Drawing.Point(0, 198);
            this._discrete10Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete10Button.Name = "_discrete10Button";
            this._discrete10Button.Size = new System.Drawing.Size(66, 22);
            this._discrete10Button.TabIndex = 21;
            this._discrete10Button.Text = "ЛС2";
            this._discrete10Button.UseVisualStyleBackColor = false;
            this._discrete10Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete9Button
            // 
            this._discrete9Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete9Button.AutoSize = true;
            this._discrete9Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete9Button.ForeColor = System.Drawing.Color.Black;
            this._discrete9Button.Location = new System.Drawing.Point(0, 176);
            this._discrete9Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete9Button.Name = "_discrete9Button";
            this._discrete9Button.Size = new System.Drawing.Size(66, 22);
            this._discrete9Button.TabIndex = 20;
            this._discrete9Button.Text = "ЛС1";
            this._discrete9Button.UseVisualStyleBackColor = false;
            this._discrete9Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete8Button
            // 
            this._discrete8Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete8Button.AutoSize = true;
            this._discrete8Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete8Button.ForeColor = System.Drawing.Color.Black;
            this._discrete8Button.Location = new System.Drawing.Point(0, 154);
            this._discrete8Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete8Button.Name = "_discrete8Button";
            this._discrete8Button.Size = new System.Drawing.Size(66, 22);
            this._discrete8Button.TabIndex = 19;
            this._discrete8Button.Text = "Д8";
            this._discrete8Button.UseVisualStyleBackColor = false;
            this._discrete8Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete7Button
            // 
            this._discrete7Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete7Button.AutoSize = true;
            this._discrete7Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete7Button.ForeColor = System.Drawing.Color.Black;
            this._discrete7Button.Location = new System.Drawing.Point(0, 132);
            this._discrete7Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete7Button.Name = "_discrete7Button";
            this._discrete7Button.Size = new System.Drawing.Size(66, 22);
            this._discrete7Button.TabIndex = 18;
            this._discrete7Button.Text = "Д7";
            this._discrete7Button.UseVisualStyleBackColor = false;
            this._discrete7Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete6Button
            // 
            this._discrete6Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete6Button.AutoSize = true;
            this._discrete6Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete6Button.ForeColor = System.Drawing.Color.Black;
            this._discrete6Button.Location = new System.Drawing.Point(0, 110);
            this._discrete6Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete6Button.Name = "_discrete6Button";
            this._discrete6Button.Size = new System.Drawing.Size(66, 22);
            this._discrete6Button.TabIndex = 17;
            this._discrete6Button.Text = "Д6";
            this._discrete6Button.UseVisualStyleBackColor = false;
            this._discrete6Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete2Button
            // 
            this._discrete2Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete2Button.AutoSize = true;
            this._discrete2Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete2Button.ForeColor = System.Drawing.Color.Black;
            this._discrete2Button.Location = new System.Drawing.Point(0, 22);
            this._discrete2Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete2Button.Name = "_discrete2Button";
            this._discrete2Button.Size = new System.Drawing.Size(66, 22);
            this._discrete2Button.TabIndex = 37;
            this._discrete2Button.Text = "Д2";
            this._discrete2Button.UseVisualStyleBackColor = false;
            this._discrete2Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete3Button
            // 
            this._discrete3Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete3Button.AutoSize = true;
            this._discrete3Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete3Button.ForeColor = System.Drawing.Color.Black;
            this._discrete3Button.Location = new System.Drawing.Point(0, 44);
            this._discrete3Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete3Button.Name = "_discrete3Button";
            this._discrete3Button.Size = new System.Drawing.Size(66, 22);
            this._discrete3Button.TabIndex = 38;
            this._discrete3Button.Text = "Д3";
            this._discrete3Button.UseVisualStyleBackColor = false;
            this._discrete3Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete4Button
            // 
            this._discrete4Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete4Button.AutoSize = true;
            this._discrete4Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete4Button.ForeColor = System.Drawing.Color.Black;
            this._discrete4Button.Location = new System.Drawing.Point(0, 66);
            this._discrete4Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete4Button.Name = "_discrete4Button";
            this._discrete4Button.Size = new System.Drawing.Size(66, 22);
            this._discrete4Button.TabIndex = 39;
            this._discrete4Button.Text = "Д4";
            this._discrete4Button.UseVisualStyleBackColor = false;
            this._discrete4Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete1Button
            // 
            this._discrete1Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete1Button.AutoSize = true;
            this._discrete1Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete1Button.ForeColor = System.Drawing.Color.Black;
            this._discrete1Button.Location = new System.Drawing.Point(0, 0);
            this._discrete1Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete1Button.Name = "_discrete1Button";
            this._discrete1Button.Size = new System.Drawing.Size(66, 22);
            this._discrete1Button.TabIndex = 36;
            this._discrete1Button.Text = "Д1";
            this._discrete1Button.UseVisualStyleBackColor = false;
            this._discrete1Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(78, 5);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Дискреты";
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Black;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Enabled = false;
            this.splitter1.Location = new System.Drawing.Point(0, 541);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1055, 3);
            this.splitter1.TabIndex = 28;
            this.splitter1.TabStop = false;
            // 
            // _voltageLayoutPanel
            // 
            this._voltageLayoutPanel.ColumnCount = 3;
            this._voltageLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._voltageLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._voltageLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._voltageLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._voltageLayoutPanel.Controls.Add(this._uScroll, 2, 1);
            this._voltageLayoutPanel.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this._voltageLayoutPanel.Controls.Add(this._uChart, 1, 1);
            this._voltageLayoutPanel.Controls.Add(this.tableLayoutPanel8, 0, 1);
            this._voltageLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._voltageLayoutPanel.Location = new System.Drawing.Point(0, 3);
            this._voltageLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this._voltageLayoutPanel.Name = "_voltageLayoutPanel";
            this._voltageLayoutPanel.RowCount = 2;
            this._voltageLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this._voltageLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._voltageLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._voltageLayoutPanel.Size = new System.Drawing.Size(1055, 538);
            this._voltageLayoutPanel.TabIndex = 27;
            // 
            // _uScroll
            // 
            this._uScroll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this._uScroll.Cursor = System.Windows.Forms.Cursors.Hand;
            this._uScroll.LargeChange = 1;
            this._uScroll.Location = new System.Drawing.Point(1037, 35);
            this._uScroll.Minimum = 100;
            this._uScroll.Name = "_uScroll";
            this._uScroll.Size = new System.Drawing.Size(15, 503);
            this._uScroll.TabIndex = 32;
            this._uScroll.Value = 100;
            this._uScroll.Visible = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 6;
            this._voltageLayoutPanel.SetColumnSpan(this.tableLayoutPanel3, 3);
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 666F));
            this.tableLayoutPanel3.Controls.Add(this._u3Button, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this._u4Button, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this._u1Button, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this._u2Button, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(986, 29);
            this.tableLayoutPanel3.TabIndex = 23;
            // 
            // _u3Button
            // 
            this._u3Button.BackColor = System.Drawing.Color.Red;
            this._u3Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._u3Button.Location = new System.Drawing.Point(213, 3);
            this._u3Button.Name = "_u3Button";
            this._u3Button.Size = new System.Drawing.Size(49, 23);
            this._u3Button.TabIndex = 13;
            this._u3Button.Text = "Uc";
            this._u3Button.UseVisualStyleBackColor = false;
            this._u3Button.Click += new System.EventHandler(this.VoltageButton_Click);
            // 
            // _u4Button
            // 
            this._u4Button.BackColor = System.Drawing.Color.Indigo;
            this._u4Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._u4Button.Location = new System.Drawing.Point(268, 3);
            this._u4Button.Name = "_u4Button";
            this._u4Button.Size = new System.Drawing.Size(49, 23);
            this._u4Button.TabIndex = 11;
            this._u4Button.Text = "Un";
            this._u4Button.UseVisualStyleBackColor = false;
            this._u4Button.Click += new System.EventHandler(this.VoltageButton_Click);
            // 
            // _u1Button
            // 
            this._u1Button.BackColor = System.Drawing.Color.Yellow;
            this._u1Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._u1Button.Location = new System.Drawing.Point(103, 3);
            this._u1Button.Name = "_u1Button";
            this._u1Button.Size = new System.Drawing.Size(49, 23);
            this._u1Button.TabIndex = 10;
            this._u1Button.Text = "Ua";
            this._u1Button.UseVisualStyleBackColor = false;
            this._u1Button.Click += new System.EventHandler(this.VoltageButton_Click);
            // 
            // _u2Button
            // 
            this._u2Button.BackColor = System.Drawing.Color.Green;
            this._u2Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._u2Button.Location = new System.Drawing.Point(158, 3);
            this._u2Button.Name = "_u2Button";
            this._u2Button.Size = new System.Drawing.Size(49, 23);
            this._u2Button.TabIndex = 9;
            this._u2Button.Text = "Ub";
            this._u2Button.UseVisualStyleBackColor = false;
            this._u2Button.Click += new System.EventHandler(this.VoltageButton_Click);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Напряжения";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _uChart
            // 
            this._uChart.BkGradient = false;
            this._uChart.BkGradientAngle = 90;
            this._uChart.BkGradientColor = System.Drawing.Color.White;
            this._uChart.BkGradientRate = 0.5F;
            this._uChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._uChart.BkRestrictedToChartPanel = false;
            this._uChart.BkShinePosition = 1F;
            this._uChart.BkTransparency = 0F;
            this._uChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._uChart.BorderExteriorLength = 0;
            this._uChart.BorderGradientAngle = 225;
            this._uChart.BorderGradientLightPos1 = 1F;
            this._uChart.BorderGradientLightPos2 = -1F;
            this._uChart.BorderGradientRate = 0.5F;
            this._uChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._uChart.BorderLightIntermediateBrightness = 0F;
            this._uChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._uChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._uChart.ChartPanelBkTransparency = 0F;
            this._uChart.ControlShadow = false;
            this._uChart.CoordinateAxesVisible = true;
            this._uChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._uChart.CoordinateXOrigin = 0D;
            this._uChart.CoordinateYMax = 100D;
            this._uChart.CoordinateYMin = -100D;
            this._uChart.CoordinateYOrigin = 0D;
            this._uChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._uChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._uChart.FooterColor = System.Drawing.Color.Black;
            this._uChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._uChart.FooterVisible = false;
            this._uChart.GridColor = System.Drawing.Color.MistyRose;
            this._uChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._uChart.GridVisible = true;
            this._uChart.GridXSubTicker = 0;
            this._uChart.GridXTicker = 10;
            this._uChart.GridYSubTicker = 0;
            this._uChart.GridYTicker = 10;
            this._uChart.HeaderColor = System.Drawing.Color.Black;
            this._uChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._uChart.HeaderVisible = false;
            this._uChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._uChart.InnerBorderLength = 0;
            this._uChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._uChart.LegendBkColor = System.Drawing.Color.White;
            this._uChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._uChart.LegendVisible = false;
            this._uChart.Location = new System.Drawing.Point(78, 38);
            this._uChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._uChart.MiddleBorderLength = 0;
            this._uChart.Name = "_uChart";
            this._uChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._uChart.OuterBorderLength = 0;
            this._uChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._uChart.Precision = 0;
            this._uChart.RoundRadius = 10;
            this._uChart.ShadowColor = System.Drawing.Color.DimGray;
            this._uChart.ShadowDepth = 8;
            this._uChart.ShadowRate = 0.5F;
            this._uChart.Size = new System.Drawing.Size(954, 497);
            this._uChart.TabIndex = 33;
            this._uChart.Text = "daS_Net_XYChart1";
            this._uChart.XMax = 100D;
            this._uChart.XMin = 0D;
            this._uChart.XScaleColor = System.Drawing.Color.Black;
            this._uChart.XScaleVisible = true;
            this._uChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Chart_MouseClick);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this._voltageChartDecreaseButton, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this._voltageChartIncreaseButton, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 38);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 4;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(69, 497);
            this.tableLayoutPanel8.TabIndex = 31;
            // 
            // _voltageChartDecreaseButton
            // 
            this._voltageChartDecreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._voltageChartDecreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._voltageChartDecreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._voltageChartDecreaseButton.Enabled = false;
            this._voltageChartDecreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._voltageChartDecreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._voltageChartDecreaseButton.Location = new System.Drawing.Point(3, 251);
            this._voltageChartDecreaseButton.Name = "_voltageChartDecreaseButton";
            this._voltageChartDecreaseButton.Size = new System.Drawing.Size(63, 24);
            this._voltageChartDecreaseButton.TabIndex = 1;
            this._voltageChartDecreaseButton.Text = "-";
            this._voltageChartDecreaseButton.UseVisualStyleBackColor = false;
            // 
            // _voltageChartIncreaseButton
            // 
            this._voltageChartIncreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._voltageChartIncreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._voltageChartIncreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._voltageChartIncreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._voltageChartIncreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._voltageChartIncreaseButton.Location = new System.Drawing.Point(3, 221);
            this._voltageChartIncreaseButton.Name = "_voltageChartIncreaseButton";
            this._voltageChartIncreaseButton.Size = new System.Drawing.Size(63, 24);
            this._voltageChartIncreaseButton.TabIndex = 0;
            this._voltageChartIncreaseButton.Text = "+";
            this._voltageChartIncreaseButton.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.Location = new System.Drawing.Point(27, 203);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Y";
            // 
            // splitter3
            // 
            this.splitter3.BackColor = System.Drawing.Color.Black;
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Enabled = false;
            this.splitter3.Location = new System.Drawing.Point(0, 0);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(1055, 3);
            this.splitter3.TabIndex = 37;
            this.splitter3.TabStop = false;
            // 
            // MarkersTable
            // 
            this.MarkersTable.BackColor = System.Drawing.Color.LightGray;
            this.MarkersTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.MarkersTable.ColumnCount = 3;
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.MarkersTable.Controls.Add(this._marker1TrackBar, 1, 0);
            this.MarkersTable.Controls.Add(this._marker2TrackBar, 1, 1);
            this.MarkersTable.Controls.Add(this.tableLayoutPanel4, 1, 2);
            this.MarkersTable.Controls.Add(this.panel4, 0, 2);
            this.MarkersTable.Controls.Add(this.label6, 2, 2);
            this.MarkersTable.Controls.Add(this.label9, 0, 0);
            this.MarkersTable.Controls.Add(this.label10, 0, 1);
            this.MarkersTable.Dock = System.Windows.Forms.DockStyle.Top;
            this.MarkersTable.Location = new System.Drawing.Point(0, 0);
            this.MarkersTable.Margin = new System.Windows.Forms.Padding(0);
            this.MarkersTable.Name = "MarkersTable";
            this.MarkersTable.RowCount = 3;
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.MarkersTable.Size = new System.Drawing.Size(1078, 90);
            this.MarkersTable.TabIndex = 31;
            this.MarkersTable.Visible = false;
            // 
            // _marker1TrackBar
            // 
            this._marker1TrackBar.BackColor = System.Drawing.Color.LightSkyBlue;
            this._marker1TrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this._marker1TrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this._marker1TrackBar.Location = new System.Drawing.Point(66, 4);
            this._marker1TrackBar.Name = "_marker1TrackBar";
            this._marker1TrackBar.Size = new System.Drawing.Size(960, 24);
            this._marker1TrackBar.TabIndex = 2;
            this._marker1TrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this._marker1TrackBar.Scroll += new System.EventHandler(this._marker1TrackBar_Scroll);
            // 
            // _marker2TrackBar
            // 
            this._marker2TrackBar.BackColor = System.Drawing.Color.Violet;
            this._marker2TrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this._marker2TrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this._marker2TrackBar.LargeChange = 0;
            this._marker2TrackBar.Location = new System.Drawing.Point(66, 35);
            this._marker2TrackBar.Maximum = 3400;
            this._marker2TrackBar.Name = "_marker2TrackBar";
            this._marker2TrackBar.Size = new System.Drawing.Size(960, 24);
            this._marker2TrackBar.TabIndex = 3;
            this._marker2TrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this._marker2TrackBar.Scroll += new System.EventHandler(this._marker2TrackBar_Scroll);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel4.Controls.Add(this._measuringChart, 1, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(63, 63);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(966, 30);
            this.tableLayoutPanel4.TabIndex = 35;
            // 
            // _measuringChart
            // 
            this._measuringChart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._measuringChart.BkGradient = false;
            this._measuringChart.BkGradientAngle = 90;
            this._measuringChart.BkGradientColor = System.Drawing.Color.White;
            this._measuringChart.BkGradientRate = 0.5F;
            this._measuringChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._measuringChart.BkRestrictedToChartPanel = false;
            this._measuringChart.BkShinePosition = 1F;
            this._measuringChart.BkTransparency = 0F;
            this._measuringChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._measuringChart.BorderExteriorLength = 0;
            this._measuringChart.BorderGradientAngle = 225;
            this._measuringChart.BorderGradientLightPos1 = 1F;
            this._measuringChart.BorderGradientLightPos2 = -1F;
            this._measuringChart.BorderGradientRate = 0.5F;
            this._measuringChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._measuringChart.BorderLightIntermediateBrightness = 0F;
            this._measuringChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._measuringChart.ChartPanelBackColor = System.Drawing.Color.White;
            this._measuringChart.ChartPanelBkTransparency = 0F;
            this._measuringChart.ControlShadow = false;
            this._measuringChart.CoordinateAxesVisible = true;
            this._measuringChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._measuringChart.CoordinateXOrigin = 0D;
            this._measuringChart.CoordinateYMax = 1D;
            this._measuringChart.CoordinateYMin = -1D;
            this._measuringChart.CoordinateYOrigin = 0D;
            this._measuringChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._measuringChart.FooterColor = System.Drawing.Color.Black;
            this._measuringChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._measuringChart.FooterVisible = false;
            this._measuringChart.GridColor = System.Drawing.Color.Gray;
            this._measuringChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._measuringChart.GridVisible = true;
            this._measuringChart.GridXSubTicker = 0;
            this._measuringChart.GridXTicker = 10;
            this._measuringChart.GridYSubTicker = 0;
            this._measuringChart.GridYTicker = 2;
            this._measuringChart.HeaderColor = System.Drawing.Color.Black;
            this._measuringChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._measuringChart.HeaderVisible = false;
            this._measuringChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._measuringChart.InnerBorderLength = 0;
            this._measuringChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._measuringChart.LegendBkColor = System.Drawing.Color.White;
            this._measuringChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._measuringChart.LegendVisible = false;
            this._measuringChart.Location = new System.Drawing.Point(16, 0);
            this._measuringChart.Margin = new System.Windows.Forms.Padding(0);
            this._measuringChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._measuringChart.MiddleBorderLength = 0;
            this._measuringChart.Name = "_measuringChart";
            this._measuringChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._measuringChart.OuterBorderLength = 0;
            this._measuringChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._measuringChart.Precision = 0;
            this._measuringChart.RoundRadius = 10;
            this._measuringChart.ShadowColor = System.Drawing.Color.DimGray;
            this._measuringChart.ShadowDepth = 8;
            this._measuringChart.ShadowRate = 0.5F;
            this._measuringChart.Size = new System.Drawing.Size(935, 30);
            this._measuringChart.TabIndex = 34;
            this._measuringChart.Text = "daS_Net_XYChart1";
            this._measuringChart.XMax = 100D;
            this._measuringChart.XMin = 0D;
            this._measuringChart.XScaleColor = System.Drawing.Color.Black;
            this._measuringChart.XScaleVisible = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label4);
            this.panel4.Location = new System.Drawing.Point(4, 66);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(44, 24);
            this.panel4.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 2);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Время";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1033, 69);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "мс";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 30);
            this.label9.TabIndex = 37;
            this.label9.Text = "Маркер 1";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 30);
            this.label10.TabIndex = 38;
            this.label10.Text = "Маркер 2";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(1087, 28);
            this.panel3.Name = "panel3";
            this.MAINTABLE.SetRowSpan(this.panel3, 2);
            this.panel3.Size = new System.Drawing.Size(294, 722);
            this.panel3.TabIndex = 33;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.LightGray;
            this.groupBox1.Controls.Add(this._markerScrollPanel);
            this.groupBox1.Location = new System.Drawing.Point(3, -1);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox1.Size = new System.Drawing.Size(293, 720);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Мгновенные значения";
            // 
            // _markerScrollPanel
            // 
            this._markerScrollPanel.AutoScroll = true;
            this._markerScrollPanel.Controls.Add(this._marker2Box);
            this._markerScrollPanel.Controls.Add(this._marker1Box);
            this._markerScrollPanel.Controls.Add(this.groupBox4);
            this._markerScrollPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._markerScrollPanel.Location = new System.Drawing.Point(0, 13);
            this._markerScrollPanel.Name = "_markerScrollPanel";
            this._markerScrollPanel.Size = new System.Drawing.Size(293, 707);
            this._markerScrollPanel.TabIndex = 3;
            // 
            // _marker2Box
            // 
            this._marker2Box.Controls.Add(this.groupBox5);
            this._marker2Box.Controls.Add(this.groupBox3);
            this._marker2Box.Location = new System.Drawing.Point(142, 3);
            this._marker2Box.Name = "_marker2Box";
            this._marker2Box.Size = new System.Drawing.Size(133, 466);
            this._marker2Box.TabIndex = 3;
            this._marker2Box.TabStop = false;
            this._marker2Box.Text = "Маркер 2";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._marker2D11);
            this.groupBox5.Controls.Add(this._marker2D10);
            this.groupBox5.Controls.Add(this._marker2D9);
            this.groupBox5.Controls.Add(this._marker2D16);
            this.groupBox5.Controls.Add(this._marker2D15);
            this.groupBox5.Controls.Add(this._marker2D14);
            this.groupBox5.Controls.Add(this._marker2D13);
            this.groupBox5.Controls.Add(this._marker2D12);
            this.groupBox5.Controls.Add(this._marker2D8);
            this.groupBox5.Controls.Add(this._marker2D7);
            this.groupBox5.Controls.Add(this._marker2D6);
            this.groupBox5.Controls.Add(this._marker2D5);
            this.groupBox5.Controls.Add(this._marker2D4);
            this.groupBox5.Controls.Add(this._marker2D3);
            this.groupBox5.Controls.Add(this._marker2D2);
            this.groupBox5.Controls.Add(this._marker2D1);
            this.groupBox5.Location = new System.Drawing.Point(6, 116);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(119, 341);
            this.groupBox5.TabIndex = 46;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Дискреты";
            // 
            // _marker2D11
            // 
            this._marker2D11.AutoSize = true;
            this._marker2D11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D11.Location = new System.Drawing.Point(6, 216);
            this._marker2D11.Name = "_marker2D11";
            this._marker2D11.Size = new System.Drawing.Size(40, 13);
            this._marker2D11.TabIndex = 34;
            this._marker2D11.Text = "ЛС3 = ";
            // 
            // _marker2D10
            // 
            this._marker2D10.AutoSize = true;
            this._marker2D10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D10.Location = new System.Drawing.Point(6, 196);
            this._marker2D10.Name = "_marker2D10";
            this._marker2D10.Size = new System.Drawing.Size(40, 13);
            this._marker2D10.TabIndex = 33;
            this._marker2D10.Text = "ЛС2 = ";
            // 
            // _marker2D9
            // 
            this._marker2D9.AutoSize = true;
            this._marker2D9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D9.Location = new System.Drawing.Point(6, 176);
            this._marker2D9.Name = "_marker2D9";
            this._marker2D9.Size = new System.Drawing.Size(40, 13);
            this._marker2D9.TabIndex = 32;
            this._marker2D9.Text = "ЛС1 = ";
            // 
            // _marker2D16
            // 
            this._marker2D16.AutoSize = true;
            this._marker2D16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D16.Location = new System.Drawing.Point(5, 316);
            this._marker2D16.Name = "_marker2D16";
            this._marker2D16.Size = new System.Drawing.Size(40, 13);
            this._marker2D16.TabIndex = 28;
            this._marker2D16.Text = "ЛС8 = ";
            // 
            // _marker2D15
            // 
            this._marker2D15.AutoSize = true;
            this._marker2D15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D15.Location = new System.Drawing.Point(5, 296);
            this._marker2D15.Name = "_marker2D15";
            this._marker2D15.Size = new System.Drawing.Size(40, 13);
            this._marker2D15.TabIndex = 27;
            this._marker2D15.Text = "ЛС7 = ";
            // 
            // _marker2D14
            // 
            this._marker2D14.AutoSize = true;
            this._marker2D14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D14.Location = new System.Drawing.Point(5, 276);
            this._marker2D14.Name = "_marker2D14";
            this._marker2D14.Size = new System.Drawing.Size(40, 13);
            this._marker2D14.TabIndex = 26;
            this._marker2D14.Text = "ЛС6 = ";
            // 
            // _marker2D13
            // 
            this._marker2D13.AutoSize = true;
            this._marker2D13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D13.Location = new System.Drawing.Point(5, 256);
            this._marker2D13.Name = "_marker2D13";
            this._marker2D13.Size = new System.Drawing.Size(40, 13);
            this._marker2D13.TabIndex = 25;
            this._marker2D13.Text = "ЛС5 = ";
            // 
            // _marker2D12
            // 
            this._marker2D12.AutoSize = true;
            this._marker2D12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D12.Location = new System.Drawing.Point(5, 236);
            this._marker2D12.Name = "_marker2D12";
            this._marker2D12.Size = new System.Drawing.Size(40, 13);
            this._marker2D12.TabIndex = 24;
            this._marker2D12.Text = "ЛС4 = ";
            // 
            // _marker2D8
            // 
            this._marker2D8.AutoSize = true;
            this._marker2D8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D8.Location = new System.Drawing.Point(6, 156);
            this._marker2D8.Name = "_marker2D8";
            this._marker2D8.Size = new System.Drawing.Size(34, 13);
            this._marker2D8.TabIndex = 23;
            this._marker2D8.Text = "Д8 = ";
            // 
            // _marker2D7
            // 
            this._marker2D7.AutoSize = true;
            this._marker2D7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D7.Location = new System.Drawing.Point(6, 136);
            this._marker2D7.Name = "_marker2D7";
            this._marker2D7.Size = new System.Drawing.Size(34, 13);
            this._marker2D7.TabIndex = 22;
            this._marker2D7.Text = "Д7 = ";
            // 
            // _marker2D6
            // 
            this._marker2D6.AutoSize = true;
            this._marker2D6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D6.Location = new System.Drawing.Point(6, 116);
            this._marker2D6.Name = "_marker2D6";
            this._marker2D6.Size = new System.Drawing.Size(34, 13);
            this._marker2D6.TabIndex = 21;
            this._marker2D6.Text = "Д6 = ";
            // 
            // _marker2D5
            // 
            this._marker2D5.AutoSize = true;
            this._marker2D5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D5.Location = new System.Drawing.Point(6, 96);
            this._marker2D5.Name = "_marker2D5";
            this._marker2D5.Size = new System.Drawing.Size(34, 13);
            this._marker2D5.TabIndex = 20;
            this._marker2D5.Text = "Д5 = ";
            // 
            // _marker2D4
            // 
            this._marker2D4.AutoSize = true;
            this._marker2D4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D4.Location = new System.Drawing.Point(6, 76);
            this._marker2D4.Name = "_marker2D4";
            this._marker2D4.Size = new System.Drawing.Size(34, 13);
            this._marker2D4.TabIndex = 19;
            this._marker2D4.Text = "Д4 = ";
            // 
            // _marker2D3
            // 
            this._marker2D3.AutoSize = true;
            this._marker2D3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D3.Location = new System.Drawing.Point(6, 56);
            this._marker2D3.Name = "_marker2D3";
            this._marker2D3.Size = new System.Drawing.Size(34, 13);
            this._marker2D3.TabIndex = 18;
            this._marker2D3.Text = "Д3 = ";
            // 
            // _marker2D2
            // 
            this._marker2D2.AutoSize = true;
            this._marker2D2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D2.Location = new System.Drawing.Point(6, 36);
            this._marker2D2.Name = "_marker2D2";
            this._marker2D2.Size = new System.Drawing.Size(34, 13);
            this._marker2D2.TabIndex = 17;
            this._marker2D2.Text = "Д2 = ";
            // 
            // _marker2D1
            // 
            this._marker2D1.AutoSize = true;
            this._marker2D1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D1.Location = new System.Drawing.Point(6, 16);
            this._marker2D1.Name = "_marker2D1";
            this._marker2D1.Size = new System.Drawing.Size(34, 13);
            this._marker2D1.TabIndex = 16;
            this._marker2D1.Text = "Д1 = ";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._marker2U4);
            this.groupBox3.Controls.Add(this._marker2U3);
            this.groupBox3.Controls.Add(this._marker2U2);
            this.groupBox3.Controls.Add(this._marker2U1);
            this.groupBox3.Location = new System.Drawing.Point(6, 15);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(119, 95);
            this.groupBox3.TabIndex = 45;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Напряжения";
            // 
            // _marker2U4
            // 
            this._marker2U4.AutoSize = true;
            this._marker2U4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2U4.Location = new System.Drawing.Point(6, 78);
            this._marker2U4.Name = "_marker2U4";
            this._marker2U4.Size = new System.Drawing.Size(33, 13);
            this._marker2U4.TabIndex = 3;
            this._marker2U4.Text = "Un = ";
            // 
            // _marker2U3
            // 
            this._marker2U3.AutoSize = true;
            this._marker2U3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2U3.Location = new System.Drawing.Point(6, 58);
            this._marker2U3.Name = "_marker2U3";
            this._marker2U3.Size = new System.Drawing.Size(33, 13);
            this._marker2U3.TabIndex = 2;
            this._marker2U3.Text = "Uc = ";
            // 
            // _marker2U2
            // 
            this._marker2U2.AutoSize = true;
            this._marker2U2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2U2.Location = new System.Drawing.Point(6, 38);
            this._marker2U2.Name = "_marker2U2";
            this._marker2U2.Size = new System.Drawing.Size(33, 13);
            this._marker2U2.TabIndex = 1;
            this._marker2U2.Text = "Ub = ";
            // 
            // _marker2U1
            // 
            this._marker2U1.AutoSize = true;
            this._marker2U1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2U1.Location = new System.Drawing.Point(6, 18);
            this._marker2U1.Name = "_marker2U1";
            this._marker2U1.Size = new System.Drawing.Size(33, 13);
            this._marker2U1.TabIndex = 0;
            this._marker2U1.Text = "Ua = ";
            // 
            // _marker1Box
            // 
            this._marker1Box.Controls.Add(this.groupBox2);
            this._marker1Box.Controls.Add(this.groupBox11);
            this._marker1Box.Location = new System.Drawing.Point(3, 3);
            this._marker1Box.Name = "_marker1Box";
            this._marker1Box.Size = new System.Drawing.Size(133, 466);
            this._marker1Box.TabIndex = 0;
            this._marker1Box.TabStop = false;
            this._marker1Box.Text = "Маркер 1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._marker1U4);
            this.groupBox2.Controls.Add(this._marker1U3);
            this.groupBox2.Controls.Add(this._marker1U2);
            this.groupBox2.Controls.Add(this._marker1U1);
            this.groupBox2.Location = new System.Drawing.Point(6, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(119, 95);
            this.groupBox2.TabIndex = 45;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Напряжения";
            // 
            // _marker1U4
            // 
            this._marker1U4.AutoSize = true;
            this._marker1U4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1U4.Location = new System.Drawing.Point(6, 78);
            this._marker1U4.Name = "_marker1U4";
            this._marker1U4.Size = new System.Drawing.Size(33, 13);
            this._marker1U4.TabIndex = 3;
            this._marker1U4.Text = "U0 = ";
            // 
            // _marker1U3
            // 
            this._marker1U3.AutoSize = true;
            this._marker1U3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1U3.Location = new System.Drawing.Point(6, 58);
            this._marker1U3.Name = "_marker1U3";
            this._marker1U3.Size = new System.Drawing.Size(33, 13);
            this._marker1U3.TabIndex = 2;
            this._marker1U3.Text = "Uc = ";
            // 
            // _marker1U2
            // 
            this._marker1U2.AutoSize = true;
            this._marker1U2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1U2.Location = new System.Drawing.Point(6, 38);
            this._marker1U2.Name = "_marker1U2";
            this._marker1U2.Size = new System.Drawing.Size(33, 13);
            this._marker1U2.TabIndex = 1;
            this._marker1U2.Text = "Ub = ";
            // 
            // _marker1U1
            // 
            this._marker1U1.AutoSize = true;
            this._marker1U1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1U1.Location = new System.Drawing.Point(6, 18);
            this._marker1U1.Name = "_marker1U1";
            this._marker1U1.Size = new System.Drawing.Size(33, 13);
            this._marker1U1.TabIndex = 0;
            this._marker1U1.Text = "Ua = ";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._marker1D11);
            this.groupBox11.Controls.Add(this._marker1D10);
            this.groupBox11.Controls.Add(this._marker1D9);
            this.groupBox11.Controls.Add(this._marker1D16);
            this.groupBox11.Controls.Add(this._marker1D15);
            this.groupBox11.Controls.Add(this._marker1D14);
            this.groupBox11.Controls.Add(this._marker1D13);
            this.groupBox11.Controls.Add(this._marker1D12);
            this.groupBox11.Controls.Add(this._marker1D8);
            this.groupBox11.Controls.Add(this._marker1D7);
            this.groupBox11.Controls.Add(this._marker1D6);
            this.groupBox11.Controls.Add(this._marker1D5);
            this.groupBox11.Controls.Add(this._marker1D4);
            this.groupBox11.Controls.Add(this._marker1D3);
            this.groupBox11.Controls.Add(this._marker1D2);
            this.groupBox11.Controls.Add(this._marker1D1);
            this.groupBox11.Location = new System.Drawing.Point(6, 116);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(119, 341);
            this.groupBox11.TabIndex = 43;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Дискреты";
            // 
            // _marker1D11
            // 
            this._marker1D11.AutoSize = true;
            this._marker1D11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D11.Location = new System.Drawing.Point(6, 216);
            this._marker1D11.Name = "_marker1D11";
            this._marker1D11.Size = new System.Drawing.Size(40, 13);
            this._marker1D11.TabIndex = 34;
            this._marker1D11.Text = "ЛС3 = ";
            // 
            // _marker1D10
            // 
            this._marker1D10.AutoSize = true;
            this._marker1D10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D10.Location = new System.Drawing.Point(6, 196);
            this._marker1D10.Name = "_marker1D10";
            this._marker1D10.Size = new System.Drawing.Size(40, 13);
            this._marker1D10.TabIndex = 33;
            this._marker1D10.Text = "ЛС2 = ";
            // 
            // _marker1D9
            // 
            this._marker1D9.AutoSize = true;
            this._marker1D9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D9.Location = new System.Drawing.Point(6, 176);
            this._marker1D9.Name = "_marker1D9";
            this._marker1D9.Size = new System.Drawing.Size(40, 13);
            this._marker1D9.TabIndex = 32;
            this._marker1D9.Text = "ЛС1 = ";
            // 
            // _marker1D16
            // 
            this._marker1D16.AutoSize = true;
            this._marker1D16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D16.Location = new System.Drawing.Point(5, 316);
            this._marker1D16.Name = "_marker1D16";
            this._marker1D16.Size = new System.Drawing.Size(40, 13);
            this._marker1D16.TabIndex = 28;
            this._marker1D16.Text = "ЛС8 = ";
            // 
            // _marker1D15
            // 
            this._marker1D15.AutoSize = true;
            this._marker1D15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D15.Location = new System.Drawing.Point(5, 296);
            this._marker1D15.Name = "_marker1D15";
            this._marker1D15.Size = new System.Drawing.Size(40, 13);
            this._marker1D15.TabIndex = 27;
            this._marker1D15.Text = "ЛС7 = ";
            // 
            // _marker1D14
            // 
            this._marker1D14.AutoSize = true;
            this._marker1D14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D14.Location = new System.Drawing.Point(5, 276);
            this._marker1D14.Name = "_marker1D14";
            this._marker1D14.Size = new System.Drawing.Size(40, 13);
            this._marker1D14.TabIndex = 26;
            this._marker1D14.Text = "ЛС6 = ";
            // 
            // _marker1D13
            // 
            this._marker1D13.AutoSize = true;
            this._marker1D13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D13.Location = new System.Drawing.Point(5, 256);
            this._marker1D13.Name = "_marker1D13";
            this._marker1D13.Size = new System.Drawing.Size(40, 13);
            this._marker1D13.TabIndex = 25;
            this._marker1D13.Text = "ЛС5 = ";
            // 
            // _marker1D12
            // 
            this._marker1D12.AutoSize = true;
            this._marker1D12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D12.Location = new System.Drawing.Point(5, 236);
            this._marker1D12.Name = "_marker1D12";
            this._marker1D12.Size = new System.Drawing.Size(40, 13);
            this._marker1D12.TabIndex = 24;
            this._marker1D12.Text = "ЛС4 = ";
            // 
            // _marker1D8
            // 
            this._marker1D8.AutoSize = true;
            this._marker1D8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D8.Location = new System.Drawing.Point(6, 156);
            this._marker1D8.Name = "_marker1D8";
            this._marker1D8.Size = new System.Drawing.Size(34, 13);
            this._marker1D8.TabIndex = 23;
            this._marker1D8.Text = "Д8 = ";
            // 
            // _marker1D7
            // 
            this._marker1D7.AutoSize = true;
            this._marker1D7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D7.Location = new System.Drawing.Point(6, 136);
            this._marker1D7.Name = "_marker1D7";
            this._marker1D7.Size = new System.Drawing.Size(34, 13);
            this._marker1D7.TabIndex = 22;
            this._marker1D7.Text = "Д7 = ";
            // 
            // _marker1D6
            // 
            this._marker1D6.AutoSize = true;
            this._marker1D6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D6.Location = new System.Drawing.Point(6, 116);
            this._marker1D6.Name = "_marker1D6";
            this._marker1D6.Size = new System.Drawing.Size(34, 13);
            this._marker1D6.TabIndex = 21;
            this._marker1D6.Text = "Д6 = ";
            // 
            // _marker1D5
            // 
            this._marker1D5.AutoSize = true;
            this._marker1D5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D5.Location = new System.Drawing.Point(6, 96);
            this._marker1D5.Name = "_marker1D5";
            this._marker1D5.Size = new System.Drawing.Size(34, 13);
            this._marker1D5.TabIndex = 20;
            this._marker1D5.Text = "Д5 = ";
            // 
            // _marker1D4
            // 
            this._marker1D4.AutoSize = true;
            this._marker1D4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D4.Location = new System.Drawing.Point(6, 76);
            this._marker1D4.Name = "_marker1D4";
            this._marker1D4.Size = new System.Drawing.Size(34, 13);
            this._marker1D4.TabIndex = 19;
            this._marker1D4.Text = "Д4 = ";
            // 
            // _marker1D3
            // 
            this._marker1D3.AutoSize = true;
            this._marker1D3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D3.Location = new System.Drawing.Point(6, 56);
            this._marker1D3.Name = "_marker1D3";
            this._marker1D3.Size = new System.Drawing.Size(34, 13);
            this._marker1D3.TabIndex = 18;
            this._marker1D3.Text = "Д3 = ";
            // 
            // _marker1D2
            // 
            this._marker1D2.AutoSize = true;
            this._marker1D2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D2.Location = new System.Drawing.Point(6, 36);
            this._marker1D2.Name = "_marker1D2";
            this._marker1D2.Size = new System.Drawing.Size(34, 13);
            this._marker1D2.TabIndex = 17;
            this._marker1D2.Text = "Д2 = ";
            // 
            // _marker1D1
            // 
            this._marker1D1.AutoSize = true;
            this._marker1D1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D1.Location = new System.Drawing.Point(6, 16);
            this._marker1D1.Name = "_marker1D1";
            this._marker1D1.Size = new System.Drawing.Size(34, 13);
            this._marker1D1.TabIndex = 16;
            this._marker1D1.Text = "Д1 = ";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this._deltaTimeBox);
            this.groupBox4.Controls.Add(this._marker2TimeBox);
            this.groupBox4.Controls.Add(this._marker1TimeBox);
            this.groupBox4.Location = new System.Drawing.Point(3, 616);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(831, 85);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Время";
            // 
            // _deltaTimeBox
            // 
            this._deltaTimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._deltaTimeBox.Controls.Add(this._markerTimeDelta);
            this._deltaTimeBox.Location = new System.Drawing.Point(6, 46);
            this._deltaTimeBox.Name = "_deltaTimeBox";
            this._deltaTimeBox.Size = new System.Drawing.Size(266, 33);
            this._deltaTimeBox.TabIndex = 2;
            this._deltaTimeBox.TabStop = false;
            this._deltaTimeBox.Text = "Дельта";
            // 
            // _markerTimeDelta
            // 
            this._markerTimeDelta.AutoSize = true;
            this._markerTimeDelta.Location = new System.Drawing.Point(118, 16);
            this._markerTimeDelta.Name = "_markerTimeDelta";
            this._markerTimeDelta.Size = new System.Drawing.Size(30, 13);
            this._markerTimeDelta.TabIndex = 2;
            this._markerTimeDelta.Text = "0 мс";
            // 
            // _marker2TimeBox
            // 
            this._marker2TimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._marker2TimeBox.Controls.Add(this._marker2Time);
            this._marker2TimeBox.Location = new System.Drawing.Point(144, 13);
            this._marker2TimeBox.Name = "_marker2TimeBox";
            this._marker2TimeBox.Size = new System.Drawing.Size(119, 33);
            this._marker2TimeBox.TabIndex = 1;
            this._marker2TimeBox.TabStop = false;
            this._marker2TimeBox.Text = "Маркер 2";
            // 
            // _marker2Time
            // 
            this._marker2Time.AutoSize = true;
            this._marker2Time.Location = new System.Drawing.Point(41, 16);
            this._marker2Time.Name = "_marker2Time";
            this._marker2Time.Size = new System.Drawing.Size(30, 13);
            this._marker2Time.TabIndex = 1;
            this._marker2Time.Text = "0 мс";
            // 
            // _marker1TimeBox
            // 
            this._marker1TimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._marker1TimeBox.Controls.Add(this._marker1Time);
            this._marker1TimeBox.Location = new System.Drawing.Point(6, 13);
            this._marker1TimeBox.Name = "_marker1TimeBox";
            this._marker1TimeBox.Size = new System.Drawing.Size(118, 33);
            this._marker1TimeBox.TabIndex = 0;
            this._marker1TimeBox.TabStop = false;
            this._marker1TimeBox.Text = "Маркер 1";
            // 
            // _marker1Time
            // 
            this._marker1Time.AutoSize = true;
            this._marker1Time.Location = new System.Drawing.Point(39, 16);
            this._marker1Time.Name = "_marker1Time";
            this._marker1Time.Size = new System.Drawing.Size(30, 13);
            this._marker1Time.TabIndex = 0;
            this._marker1Time.Text = "0 мс";
            // 
            // _xDecreaseButton
            // 
            this._xDecreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._xDecreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._xDecreaseButton.Location = new System.Drawing.Point(148, 3);
            this._xDecreaseButton.Name = "_xDecreaseButton";
            this._xDecreaseButton.Size = new System.Drawing.Size(33, 20);
            this._xDecreaseButton.TabIndex = 3;
            this._xDecreaseButton.Text = "X -";
            this._xDecreaseButton.UseVisualStyleBackColor = false;
            // 
            // _discrestsСheckBox
            // 
            this._discrestsСheckBox.AutoSize = true;
            this._discrestsСheckBox.BackColor = System.Drawing.Color.Silver;
            this._discrestsСheckBox.Checked = true;
            this._discrestsСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._discrestsСheckBox.Location = new System.Drawing.Point(361, 3);
            this._discrestsСheckBox.Name = "_discrestsСheckBox";
            this._discrestsСheckBox.Size = new System.Drawing.Size(78, 17);
            this._discrestsСheckBox.TabIndex = 5;
            this._discrestsСheckBox.Text = "Дискреты";
            this._discrestsСheckBox.UseVisualStyleBackColor = false;
            this._discrestsСheckBox.CheckedChanged += new System.EventHandler(this._discrestsСheckBox_CheckedChanged);
            // 
            // _markCheckBox
            // 
            this._markCheckBox.AutoSize = true;
            this._markCheckBox.BackColor = System.Drawing.Color.Silver;
            this._markCheckBox.Location = new System.Drawing.Point(691, 3);
            this._markCheckBox.Name = "_markCheckBox";
            this._markCheckBox.Size = new System.Drawing.Size(58, 17);
            this._markCheckBox.TabIndex = 7;
            this._markCheckBox.Text = "Метки";
            this._markCheckBox.UseVisualStyleBackColor = false;
            // 
            // _markerCheckBox
            // 
            this._markerCheckBox.AutoSize = true;
            this._markerCheckBox.BackColor = System.Drawing.Color.Silver;
            this._markerCheckBox.Location = new System.Drawing.Point(761, 3);
            this._markerCheckBox.Name = "_markerCheckBox";
            this._markerCheckBox.Size = new System.Drawing.Size(73, 17);
            this._markerCheckBox.TabIndex = 8;
            this._markerCheckBox.Text = "Маркеры";
            this._markerCheckBox.UseVisualStyleBackColor = false;
            this._markerCheckBox.CheckedChanged += new System.EventHandler(this._markerCheckBox_CheckedChanged);
            // 
            // _xIncreaseButton
            // 
            this._xIncreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._xIncreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._xIncreaseButton.Location = new System.Drawing.Point(108, 3);
            this._xIncreaseButton.Name = "_xIncreaseButton";
            this._xIncreaseButton.Size = new System.Drawing.Size(33, 20);
            this._xIncreaseButton.TabIndex = 2;
            this._xIncreaseButton.Text = "X +";
            this._xIncreaseButton.UseVisualStyleBackColor = false;
            // 
            // _oscRunСheckBox
            // 
            this._oscRunСheckBox.AutoSize = true;
            this._oscRunСheckBox.BackColor = System.Drawing.Color.Silver;
            this._oscRunСheckBox.Location = new System.Drawing.Point(521, 3);
            this._oscRunСheckBox.Name = "_oscRunСheckBox";
            this._oscRunСheckBox.Size = new System.Drawing.Size(127, 17);
            this._oscRunСheckBox.TabIndex = 9;
            this._oscRunСheckBox.Text = "Пуск осциллографа";
            this._oscRunСheckBox.UseVisualStyleBackColor = false;
            this._oscRunСheckBox.CheckedChanged += new System.EventHandler(this._oscRunСheckBox_CheckedChanged);
            // 
            // _voltageСheckBox
            // 
            this._voltageСheckBox.AutoSize = true;
            this._voltageСheckBox.BackColor = System.Drawing.Color.Silver;
            this._voltageСheckBox.Checked = true;
            this._voltageСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._voltageСheckBox.Location = new System.Drawing.Point(256, 3);
            this._voltageСheckBox.Name = "_voltageСheckBox";
            this._voltageСheckBox.Size = new System.Drawing.Size(90, 17);
            this._voltageСheckBox.TabIndex = 10;
            this._voltageСheckBox.Text = "Напряжения";
            this._voltageСheckBox.UseVisualStyleBackColor = false;
            this._voltageСheckBox.CheckedChanged += new System.EventHandler(this._voltageСheckBox_CheckedChanged);
            // 
            // Mr600OscilloscopeOldResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 753);
            this.Controls.Add(this._voltageСheckBox);
            this.Controls.Add(this._oscRunСheckBox);
            this.Controls.Add(this._markerCheckBox);
            this.Controls.Add(this._markCheckBox);
            this.Controls.Add(this._discrestsСheckBox);
            this.Controls.Add(this._xDecreaseButton);
            this.Controls.Add(this._xIncreaseButton);
            this.Controls.Add(this.MAINTABLE);
            this.Name = "Mr600OscilloscopeOldResultForm";
            this.Text = "Mr901OscilloscopeResultForm";
            this.MAINTABLE.ResumeLayout(false);
            this.MAINTABLE.PerformLayout();
            this.MAINPANEL.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this._discretsLayoutPanel.ResumeLayout(false);
            this._discretsLayoutPanel.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this._voltageLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.MarkersTable.ResumeLayout(false);
            this.MarkersTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._marker1TrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._marker2TrackBar)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this._markerScrollPanel.ResumeLayout(false);
            this._marker2Box.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this._marker1Box.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this._deltaTimeBox.ResumeLayout(false);
            this._deltaTimeBox.PerformLayout();
            this._marker2TimeBox.ResumeLayout(false);
            this._marker2TimeBox.PerformLayout();
            this._marker1TimeBox.ResumeLayout(false);
            this._marker1TimeBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel MAINTABLE;
        private System.Windows.Forms.HScrollBar hScrollBar4;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Panel MAINPANEL;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.TableLayoutPanel _discretsLayoutPanel;
        private BEMN_XY_Chart.DAS_Net_XYChart _discrestsChart;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.TableLayoutPanel _voltageLayoutPanel;
        private System.Windows.Forms.VScrollBar _uScroll;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button _voltageChartDecreaseButton;
        private System.Windows.Forms.Button _voltageChartIncreaseButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button _u3Button;
        private System.Windows.Forms.Button _u4Button;
        private System.Windows.Forms.Button _u1Button;
        private System.Windows.Forms.Button _u2Button;
        private System.Windows.Forms.Label label5;
        private BEMN_XY_Chart.DAS_Net_XYChart _uChart;
        private System.Windows.Forms.TableLayoutPanel MarkersTable;
        private System.Windows.Forms.TrackBar _marker1TrackBar;
        private System.Windows.Forms.TrackBar _marker2TrackBar;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox _deltaTimeBox;
        private System.Windows.Forms.GroupBox _marker2TimeBox;
        private System.Windows.Forms.GroupBox _marker1TimeBox;
        private System.Windows.Forms.GroupBox _marker1Box;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button _discrete5Button;
        private System.Windows.Forms.Button _discrete6Button;
        private System.Windows.Forms.Button _discrete7Button;
        private System.Windows.Forms.Button _discrete8Button;
        private System.Windows.Forms.Button _discrete9Button;
        private System.Windows.Forms.Button _discrete10Button;
        private System.Windows.Forms.Button _discrete11Button;
        private System.Windows.Forms.Button _discrete12Button;
        private System.Windows.Forms.Button _discrete13Button;
        private System.Windows.Forms.Button _discrete14Button;
        private System.Windows.Forms.Button _discrete15Button;
        private System.Windows.Forms.Button _discrete16Button;
        private System.Windows.Forms.Button _discrete1Button;
        private System.Windows.Forms.Button _discrete2Button;
        private System.Windows.Forms.Button _discrete3Button;
        private System.Windows.Forms.Button _discrete4Button;
        private System.Windows.Forms.Button _xDecreaseButton;
        private System.Windows.Forms.CheckBox _discrestsСheckBox;
        private System.Windows.Forms.CheckBox _markCheckBox;
        private System.Windows.Forms.CheckBox _markerCheckBox;
        private System.Windows.Forms.ToolTip _newMarkToolTip;
        private System.Windows.Forms.Label _marker1D11;
        private System.Windows.Forms.Label _marker1D10;
        private System.Windows.Forms.Label _marker1D9;
        private System.Windows.Forms.Label _marker1D16;
        private System.Windows.Forms.Label _marker1D15;
        private System.Windows.Forms.Label _marker1D14;
        private System.Windows.Forms.Label _marker1D13;
        private System.Windows.Forms.Label _marker1D12;
        private System.Windows.Forms.Label _marker1D8;
        private System.Windows.Forms.Label _marker1D7;
        private System.Windows.Forms.Label _marker1D6;
        private System.Windows.Forms.Label _marker1D5;
        private System.Windows.Forms.Label _marker1D4;
        private System.Windows.Forms.Label _marker1D3;
        private System.Windows.Forms.Label _marker1D2;
        private System.Windows.Forms.Label _marker1D1;
        private System.Windows.Forms.Panel _markerScrollPanel;
        private System.Windows.Forms.GroupBox _marker2Box;
        private System.Windows.Forms.Button _xIncreaseButton;
        private System.Windows.Forms.Label _markerTimeDelta;
        private System.Windows.Forms.Label _marker2Time;
        private System.Windows.Forms.Label _marker1Time;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private BEMN_XY_Chart.DAS_Net_XYChart _measuringChart;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.CheckBox _oscRunСheckBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label _marker2D11;
        private System.Windows.Forms.Label _marker2D10;
        private System.Windows.Forms.Label _marker2D9;
        private System.Windows.Forms.Label _marker2D16;
        private System.Windows.Forms.Label _marker2D15;
        private System.Windows.Forms.Label _marker2D14;
        private System.Windows.Forms.Label _marker2D13;
        private System.Windows.Forms.Label _marker2D12;
        private System.Windows.Forms.Label _marker2D8;
        private System.Windows.Forms.Label _marker2D7;
        private System.Windows.Forms.Label _marker2D6;
        private System.Windows.Forms.Label _marker2D5;
        private System.Windows.Forms.Label _marker2D4;
        private System.Windows.Forms.Label _marker2D3;
        private System.Windows.Forms.Label _marker2D2;
        private System.Windows.Forms.Label _marker2D1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label _marker2U4;
        private System.Windows.Forms.Label _marker2U3;
        private System.Windows.Forms.Label _marker2U2;
        private System.Windows.Forms.Label _marker2U1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label _marker1U4;
        private System.Windows.Forms.Label _marker1U3;
        private System.Windows.Forms.Label _marker1U2;
        private System.Windows.Forms.Label _marker1U1;
        private System.Windows.Forms.CheckBox _voltageСheckBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;



    }
}