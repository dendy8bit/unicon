using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BEMN.MR600.Old.Osc.HelpClasses;
using BEMN.MR600.Old.Osc.Loaders;
using BEMN.MR600.Old.Osc.Structures;
using System.Reflection;
using System.IO;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.MR600.Old.Osc
{
    public partial class Mr600OscilloscopeForm : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "�������������";
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string RECORDS_IN_JOURNAL = "������������ � ������� - {0}";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string READ_OSC_STOPPED = "������ ������������� ����������";

        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscPageLoader _pageLoader;

        private readonly MemoryEntity<OscJournalStruct> _oscJournal;

        /// <summary>
        /// ������ ���
        /// </summary>
        private CountingListV203 _countingListV203;


        private OscJournalStruct _journalStruct;
        private readonly DataTable _table;
        private MR600 _device;
        private double _tnnp;
        private double _tn;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// ������ ���
        /// </summary>
        public CountingListV203 CountingListV203
        {
            get
            {
                return this._countingListV203 
                    ?? new CountingListV203(new ushort[OscJournalStruct.OscSize], new OscJournalStruct(), 0, 0);
            }
            set
            {
                this._countingListV203 = value;
                this._oscShowButton.Enabled = true;
            }
        }


        #endregion [Properties]


        #region [Ctor's]
        public Mr600OscilloscopeForm()
        {
            InitializeComponent();
        }

        public Mr600OscilloscopeForm(MR600 device)
        {
            this.InitializeComponent();
            this._device = device;
            device.InputSignalsLoadOK += HandlerHelper.CreateHandler(this, ParametersLoadOk);
            device.InputSignalsLoadFail += HandlerHelper.CreateHandler(this, FailReadOscJournal);
            //��������� �������
            this._oscJournal = device.Mr600DeviceV2.OldOscJournal;
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadJournalOk);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, FailReadOscJournal);
            this._pageLoader = new OscPageLoader(device);
            this._pageLoader.PageRead += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);//() => this.Invoke(new Action(() => this._oscProgressBar.PerformStep()));
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, OscReadOk);
            this._pageLoader.OscReadFail += HandlerHelper.CreateActionHandler(this, OscReadFail);

            this._table = this.GetJournalDataTable();
        }

        private void OscReadFail()
        {
            MessageBox.Show("������ ������ �������������");
        }

        private void ParametersLoadOk()
        {
            this._tn = this._device.TN;
            this._tnnp = this._device.TNNP;
            this._oscJournal.LoadStruct();
        }

        private void ReadJournalOk()
        {
            this._table.Clear();
            if (this._oscJournal.Value.IsReady)
            {
                List<object> record = this._oscJournal.Value.GetRecord;
                record.Add(this._tn);
                record.Add(this._tnnp);
                this._table.Rows.Add(record.ToArray());
            }
            this._oscReadButton.Enabled = this._oscJournal.Value.IsReady;
            this.EnableButtons = true;
        }

        #endregion [Ctor's]


        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR600); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(Mr600OscilloscopeForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return Mr600OscilloscopeForm.OSC; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }
        #endregion [IFormView Members]


        #region [Help members]
        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
            this.EnableButtons = true;
        }

        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            try
            {
                this.CountingListV203 = new CountingListV203(this._pageLoader.ResultArray, this._journalStruct, this._tn, this._tnnp);
            }
            catch (Exception e)
            {
                MessageBox.Show("������ ������������� ���������� ��� �������", "��������", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            this._oscReadButton.Enabled = true;
            this._oscJournalReadButton.Enabled = true;
            this._oscSaveButton.Enabled = true;
            this._oscShowButton.Enabled = true;
            this.EnableButtons = true;
        }


        private DataTable GetJournalDataTable()
        {
            var table = new DataTable("��600_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._oscJournalReadButton.Enabled = false;
            this._table.Clear();
            this._oscJournalDataGrid.Refresh();
            this._device.LoadInputSignals();
        }
        #endregion []


        #region [Event Handlers]
        /// <summary>
        /// �������� �����
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            this.StartRead();
        }
        /// <summary>
        /// �������� �������������
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (Validator.GetVersionFromRegistry())
            {
                string fileName;
                if (this._countingListV203.IsLoad)
                {
                    fileName = this._countingListV203.FilePath;
                }
                else
                {
                    fileName = Validator.CreateOscFileNameCfg($"��600 v{this._device.DeviceVersion} �������������");
                    this._countingListV203.Save(fileName);
                }
                System.Diagnostics.Process.Start(
                    Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"),
                    fileName);
            }
            else
            {

            }
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        /// <summary>
        /// ��������� �������������
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            this._journalStruct = this._oscJournal.Value;
            this._journalStruct.Version = Common.VersionConverter(this._device.DeviceVersion);
            this._pageLoader.StartRead(this._journalStruct);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoader.SlotCount;
            this.EnableButtons = false;
            this._oscReadButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this.EnableButtons = false;
        }

        private bool EnableButtons
        {
            set
            {
                this._oscJournalReadButton.Enabled =
                        this._oscLoadButton.Enabled = value;
            }
        }

        /// <summary>
        /// ��������� ������������� � ����
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        {
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                this._countingListV203.Save(this._saveOscilloscopeDlg.FileName);
            }
        }

        /// <summary>
        /// ��������� ������������� �� �����
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                this.CountingListV203 = this.CountingListV203.Load(this._openOscilloscopeDlg.FileName);
                this._statusLabel.Text = string.Format("������������ ��������� �� ����� {0}",
                                                       this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = "���������� ��������� ������������";
            }

        }

        #endregion [Event Handlers]
    }
}