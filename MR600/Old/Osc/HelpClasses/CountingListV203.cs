﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using BEMN.MBServer;
using BEMN.MR600.Old.Osc.Structures;

namespace BEMN.MR600.Old.Osc.HelpClasses
{
    public class CountingListV203
    {
        #region[Constants]
        /// <summary>
        /// Размер одного отсчёта(в словах)
        /// </summary>
        private int COUNTING_SIZE = 5;
        /// <summary>
        /// Кол-во напряжений
        /// </summary>
        private const int VOLTAGES_COUNT = 4;
        /// <summary>
        /// Кол-во дискрет
        /// </summary>
        private int _discret_count= 16;

        private const string SAVE_OSC_FAIL = "Ошибка сохранения файла";

        public static readonly string[] UNames = new[] { "Ua", "Ub", "Uc", "U0" };
        #endregion [Constants]

        #region [Private fields]
        /// <summary>
        /// Массив отсчётов разбитый на слова
        /// </summary>
        private readonly ushort[][] _countingArray;

        /// <summary>
        /// Общее количество отсчётов
        /// </summary>
        private int _count;

        #region [Токи]
        /// <summary>
        /// Токи
        /// </summary>
        private double[][] _currents;
        /// <summary>
        /// Неприведённые значения токов
        /// </summary>
        private readonly short[][] _baseCurrents;
        /// <summary>
        /// Перещитанные
        /// </summary>
        public double[][] Currents
        {
            get { return this._currents; }
            set { this._currents = value; }
        }
        #endregion [Токи]

        #region [Напряжения]
        /// <summary>
        /// Напряжения
        /// </summary>
        private double[][] _voltages;


        /// <summary>
        /// Минимальное напряжение
        /// </summary>
        private double _minU;
        /// <summary>
        /// Максимальное напряжение
        /// </summary>
        private double _maxU;
        /// <summary>
        /// Минимальное напряжение
        /// </summary>
        public double MinU
        {
            get { return this._minU; }
        }
        /// <summary>
        /// Максимальное напряжение
        /// </summary>
        public double MaxU
        {
            get { return this._maxU; }
        }
        /// <summary>
        /// Перещитанные
        /// </summary>
        public double[][] Voltages
        {
            get { return this._voltages; }
            set { this._voltages = value; }
        }

        #endregion [Напряжения]

        /// <summary>
        /// Массив дискрет 1-16
        /// </summary>
        private ushort[][] _discrets;
        /// <summary>
        /// Авария("Пуск осцилографа") в отсчётах
        /// </summary>
        private int _alarm;
        private string _dateTime;

        private OscJournalStruct _oscJournalStruct;

        #endregion [Private fields]
        
        /// <summary>
        /// Общее количество отсчётов
        /// </summary>
        public int Count
        {
            get { return this._count; }
        }

        /// <summary>
        /// Авария("Пуск осцилографа") в отсчётах
        /// </summary>
        public int Alarm
        {
            get { return this._alarm; }
        }

        /// <summary>
        /// Дискреты
        /// </summary>
        public ushort[][] Discrets
        {
            get { return this._discrets; }
            set { this._discrets = value; }
        }

        public bool IsLoad { get; private set; }
        public string FilePath { get; private set; }


        /// <summary>
        /// Коэффициенты перещёта для токов
        /// </summary>
        private double[] CurrentsKoefs { get; set; }

        #region [Ctor's]
        public CountingListV203(ushort[] pageValue, OscJournalStruct oscJournalStruct, double tn, double ttnp)
        {
            this._oscJournalStruct = oscJournalStruct;
            this._dateTime = oscJournalStruct.GetFormattedDateTime;
            this._alarm = this._oscJournalStruct.AlarmTime;
            this.COUNTING_SIZE = this._oscJournalStruct.CountingSize;
            this._countingArray = new ushort[this.COUNTING_SIZE][];
            //Общее количество отсчётов
            this._count = pageValue.Length /this.COUNTING_SIZE;
            //Инициализация массива
            for (int i = 0; i < this.COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this.Count];
            }
            int m = 0;
            int n = 0;

            for (int i = 0; i < this._count *this.COUNTING_SIZE; i++)
            {
                this._countingArray[n][m] = pageValue[i];
                n++;
                if (n == this.COUNTING_SIZE)
                {
                    m++;
                    n = 0;
                }
            }

            if (this._oscJournalStruct.Version >= 2.03)
            {
                this._discrets = this.DiscretArrayInit(this._countingArray[4]);
                this._discret_count = 16;
            }
            else
            {
                this._discret_count = 0;
                this._discrets = new ushort [][] {};
            }

            this._currents = new double[VOLTAGES_COUNT][];
            this._baseCurrents = new short[VOLTAGES_COUNT][];
            double[] currentsKoefs = new double[VOLTAGES_COUNT];
            for (int i = 0; i < VOLTAGES_COUNT; i++)
            {
                    
                this._baseCurrents[i] = this._countingArray[i].Select(a => (short)(a - 32768)).ToArray();

                double koef = i == VOLTAGES_COUNT - 1 ? ttnp * 256 : tn * 256;

                currentsKoefs[i] = koef * Math.Sqrt(2) / 32768;
                this._currents[i] = this._baseCurrents[i].Select(a => currentsKoefs[i] * a).ToArray();
                this._maxU = Math.Max(this.MaxU, this._currents[i].Max());
                this._minU = Math.Min(this.MinU, this._currents[i].Min());
            }
            this.CurrentsKoefs = currentsKoefs;
        }

        #endregion [Ctor's]

        #region [Help members]
        /// <summary>
        /// Превращает каждое слово в инвертированный массив бит(значения 0/1) 
        /// </summary>
        /// <param pinName="sourseArray">Массив массивов бит</param>

        #endregion [Help members]

        public void Save(string filePath)
        {
            try
            {
                if (string.IsNullOrEmpty(filePath))
                {
                    throw new ArgumentException();
                }

                string hdrPath = Path.ChangeExtension(filePath, "hdr");
                
                using (StreamWriter hdrFile = new StreamWriter(hdrPath))
                {
                    hdrFile.WriteLine("МР 600 {0} {1}", this._oscJournalStruct.GetDate, this._oscJournalStruct.GetTime);
                    hdrFile.WriteLine("Size, ms = {0}", this._oscJournalStruct.SizeTime);
                    hdrFile.WriteLine("Alarm = {0}", this._alarm);
                    hdrFile.WriteLine(1251);
                }
                
                string cgfPath = Path.ChangeExtension(filePath, "cfg");

                using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
                {
                    cgfFile.WriteLine("MP600,1");
                    cgfFile.WriteLine(this._oscJournalStruct.Version < 2.03 ? "4,4A,0D" : "20,4A,16D");
                    int index = 1;
                    for (int i = 0; i < this.Currents.Length; i++)
                    {
                        NumberFormatInfo format = new NumberFormatInfo { NumberDecimalSeparator = "." };

                        cgfFile.WriteLine("{0},{1},,,V,{2},0,0,-32768,32767,1,1,P", index, UNames[i],
                            this.CurrentsKoefs[i].ToString(format));
                        index++;
                    }
                    
                    for (int i = 0; i < this._discret_count; i++)
                    {
                        if (i < 8)
                        {
                            cgfFile.WriteLine("{0},Д{1},0", index, i + 1);
                            index++;
                        }
                        else
                        {
                            cgfFile.WriteLine("{0},Л{1},0", index, i -7);
                            index++;
                        }
                        
                    }

                    cgfFile.WriteLine("50");
                    cgfFile.WriteLine("1");
                    cgfFile.WriteLine("1000,{0}", this._count);

                    cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);
                    cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeAlarm(this._alarm));
                    cgfFile.WriteLine("ASCII");
                }

                string datPath = Path.ChangeExtension(filePath, "dat");
                using (StreamWriter datFile = new StreamWriter(datPath))
                {
                    for (int i = 0; i < this._count; i++)
                    {
                        datFile.Write("{0:D6},{1:D6}", i, i * 1000);
                        foreach (short[] current in this._baseCurrents)
                        {
                            datFile.Write(",{0}", current[i]);
                        }
                        foreach (ushort[] discret in this.Discrets)
                        {
                            datFile.Write(",{0}", discret[i]);
                        }

                        datFile.WriteLine();
                    }
                }
            }
            catch
            {
                MessageBox.Show(SAVE_OSC_FAIL);
            }

        }

        public CountingListV203 Load(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string timeString;
            if (hdrStrings[0].StartsWith("Fault date :"))
            {
                timeString = hdrStrings[0].Replace("Fault date : ", string.Empty);
            }
            else
            {
                string[] buff = hdrStrings[0].Split(' ');
                timeString = string.Format("{0} {1}", buff[2], buff[3]);
            }
            int alarm = int.Parse(hdrStrings[2].Replace("Alarm = ", string.Empty));

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);
            string[] cfgCountChannles = cfgStrings[1].Split(',');
            int string1000 = int.Parse(cfgCountChannles[0]) == 20 ? 24 : 8;

            double[,] factors = new double[VOLTAGES_COUNT, 2];
            Regex factorRegex = new Regex(@"\d+\,[U]\w+\,\,\,[V]\,(?<valueA>[0-9\.-]+)\,(?<valueB>[0-9\.-]+)");
            for (int i = 2; i < 2 + VOLTAGES_COUNT; i++)
            {
                string res = factorRegex.Match(cfgStrings[i]).Groups["valueA"].Value;
                NumberFormatInfo format = new NumberFormatInfo { NumberDecimalSeparator = "." };
                factors[i - 2, 0] = double.Parse(res, format);
                res = factorRegex.Match(cfgStrings[i]).Groups["valueB"].Value;
                factors[i - 2, 1] = double.Parse(res, format);
            }
            
            int  counts = int.Parse(cfgStrings[string1000].Replace("1000,", string.Empty));

            CountingListV203 result = new CountingListV203(counts) { _alarm = alarm };
            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);
            
            ushort[][] discrets = new ushort[this._discret_count][];
            double[][] currents = new double[VOLTAGES_COUNT][];
            
            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new double[counts];
            }


            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[counts];
            }

            for (int i = 0; i < datStrings.Length; i++)
            {
                string[] means = datStrings[i].Split(',');
                for (int j = 0; j < VOLTAGES_COUNT; j++)
                {
                    currents[j][i] = Convert.ToDouble(means[j + 2], CultureInfo.InvariantCulture) *factors[j, 0] + factors[j, 1];
                }
                for (int j = 0; j < this._discret_count; j++)
                {
                    discrets[j][i] = Convert.ToUInt16(means[j + 2 + VOLTAGES_COUNT]);
                }
            }
            for (int i = 0; i < VOLTAGES_COUNT; i++)
            {
                result._maxU = Math.Max(result.MaxU, currents[i].Max());
                result._minU = Math.Min(result.MinU, currents[i].Min());
            }
            
            result.Currents = currents;
            result.FilePath = filePath;
            result.IsLoad = true;
            result.Discrets = discrets;

            result._dateTime = timeString;

            return result;
        }

        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {

            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        }
        private CountingListV203(int count)
        {
            this._currents = new double[VOLTAGES_COUNT][];
            this._baseCurrents = new short[VOLTAGES_COUNT][];

            this._count = count;
        }
    }
}