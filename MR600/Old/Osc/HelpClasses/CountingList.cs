using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using BEMN.MBServer;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BEMN.MR600.Old.Osc.Structures;

namespace BEMN.MR600.Old.Osc.HelpClasses
{
    /// <summary>
    /// ��������� �������� ������������
    /// </summary>
    public class CountingList
    {
        #region [Constants]
        /// <summary>
        /// ������ ������ �������(� ������)
        /// </summary>
        public const int COUNTING_SIZE = 4;
        /// <summary>
        /// ���-�� ����������
        /// </summary>
        public const int VOLTAGES_COUNT = 4;

        private const string SAVE_OSC_FAIL = "������ ���������� �����";

        public static readonly string[] UNames = new[] { "Ua", "Ub", "Uc", "U0" };
        #endregion [Constants]



        #region [Private fields]
        /// <summary>
        /// ������ �������� �������� �� �����
        /// </summary>
        private readonly ushort[][] _countingArray;

        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        private int _count;

        #region [����]
        /// <summary>
        /// ����
        /// </summary>
        private double[][] _currents;
        /// <summary>
        /// ������������ �������� �����
        /// </summary>
        private readonly short[][] _baseCurrents;     
        /// <summary>
        /// ������������
        /// </summary>
        public double[][] Currents
        {
            get { return this._currents; }
            set { this._currents = value; }
        }
        #endregion [����]

        #region [����������]
        /// <summary>
        /// ����������
        /// </summary>
        private double[][] _voltages;


        /// <summary>
        /// ����������� ����������
        /// </summary>
        private double _minU;
        /// <summary>
        /// ������������ ����������
        /// </summary>
        private double _maxU;
        /// <summary>
        /// ����������� ����������
        /// </summary>
        public double MinU
        {
            get { return this._minU; }
        }
        /// <summary>
        /// ������������ ����������
        /// </summary>
        public double MaxU
        {
            get { return this._maxU; }
        }
        /// <summary>
        /// ������������
        /// </summary>
        public double[][] Voltages
        {
            get { return this._voltages; }
            set { this._voltages = value; }
        }

        #endregion [����������]

        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        private int _alarm;
        private string _dateTime;

        private OscJournalStruct _oscJournalStruct;

        #endregion [Private fields]


        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        public int Count
        {
            get { return this._count; }
        }

        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        public int Alarm
        {
            get { return this._alarm; }
        }

        public bool IsLoad { get; private set; }
        public string FilePath { get; private set; }

        public string DateAndTime
        {
            get { return this._dateTime; }
        }

        /// <summary>
        /// ������������ �������� ��� �����
        /// </summary>
        private double[] CurrentsKoefs { get; set; }


        #region [Ctor's]
        public CountingList(ushort[] pageValue, OscJournalStruct oscJournalStruct, double tn,double ttnp)
        {
            this._oscJournalStruct = oscJournalStruct;
            this._dateTime = oscJournalStruct.GetFormattedDateTime;
            this._alarm = 1229;//this._oscJournalStruct.FaultTime>32768 ? this._oscJournalStruct.FaultTime-32768:this._oscJournalStruct.FaultTime; ��� ������ ������ ���� ����� ������ ����������

            this._countingArray = new ushort[COUNTING_SIZE][];
            //����� ���������� ��������
            this._count = pageValue.Length / COUNTING_SIZE;
            //������������� �������
            for (int i = 0; i < COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this.Count];
            }
            int m = 0;
            int n = 0;

            for (int i = 0; i < this._count * COUNTING_SIZE; i++)
            {
                this._countingArray[n][m] = pageValue[i];
                n++;
                if (n == COUNTING_SIZE)
                {
                    m++;
                    n = 0;
                }
            }


            this._currents = new double[VOLTAGES_COUNT][];
            this._baseCurrents = new short[VOLTAGES_COUNT][];
            double[] currentsKoefs = new double[VOLTAGES_COUNT];
            for (int i = 0; i < VOLTAGES_COUNT; i++)
            {
                this._baseCurrents[i] = this._countingArray[i].Select(a => (short)(a - 32768)).ToArray();

                double koef = i == VOLTAGES_COUNT - 1 ? ttnp * 256 : tn * 256;

                currentsKoefs[i] = koef * Math.Sqrt(2) / 32768;
                this._currents[i] = this._baseCurrents[i].Select(a => currentsKoefs[i] * a).ToArray();
                this._maxU = Math.Max(this.MaxU, this._currents[i].Max());
                this._minU = Math.Min(this.MinU, this._currents[i].Min());
            }
            this.CurrentsKoefs = currentsKoefs;


        }

        #endregion [Ctor's]

        #region [Help members]
        /// <summary>
        /// ���������� ������ ����� � ��������������� ������ ���(�������� 0/1) 
        /// </summary>
        /// <param pinName="sourseArray">������ �������� ���</param>
        
        #endregion [Help members]

        public void Save(string filePath)
        {
            try
            {
                string directiryPath = Path.GetDirectoryName(filePath);
                string fileName = Path.GetFileNameWithoutExtension(filePath);

                if (string.IsNullOrEmpty(directiryPath) | string.IsNullOrEmpty(fileName))
                {
                    throw new ArgumentException();
                }

                string hdrPath = Path.ChangeExtension(Path.Combine(directiryPath, fileName), "hdr");


                using (StreamWriter hdrFile = new StreamWriter(hdrPath))
                {
                    hdrFile.WriteLine("�� 600 {0} {1}", this._oscJournalStruct.GetDate, this._oscJournalStruct.GetTime);
                    hdrFile.WriteLine("Size, ms = {0}", this._oscJournalStruct.SizeTime);
                    hdrFile.WriteLine("Alarm = {0}", this._alarm);
                    hdrFile.WriteLine(1251);
                }


                string cgfPath = Path.ChangeExtension(Path.Combine(directiryPath, fileName), "cfg");

                using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
                {
                    cgfFile.WriteLine("MP600,1");
                    cgfFile.WriteLine("4,4A,0D");
                    int index = 1;
                    for (int i = 0; i < this.Currents.Length; i++)
                    {
                        NumberFormatInfo format = new System.Globalization.NumberFormatInfo { NumberDecimalSeparator = "." };

                        cgfFile.WriteLine("{0},{1},,,V,{2},0,0,-32768,32767", index, UNames[i],
                            this.CurrentsKoefs[i].ToString(format));
                        index++;
                    }




                    cgfFile.WriteLine("50");
                    cgfFile.WriteLine("1");
                    cgfFile.WriteLine("1000,{0}", this._count);

                    cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);
                    cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeAlarm(this._alarm));
                    cgfFile.WriteLine("ASCII");
                }

                string datPath = Path.ChangeExtension(Path.Combine(directiryPath, fileName), "dat");
                using (StreamWriter datFile = new StreamWriter(datPath))
                {
                    for (int i = 0; i < this._count; i++)
                    {
                        datFile.Write("{0:D6},{1:D6}", i, i * 1000);
                        foreach (short[] current in this._baseCurrents)
                        {
                            datFile.Write(",{0}", current[i]);
                        }


                        datFile.WriteLine();
                    }
                }
                // File.WriteAllBytes(@"D:\MP901Osc.osc",Common.TOBYTES(this._pageValue,true));*/
            }
            catch
            {
                MessageBox.Show(SAVE_OSC_FAIL);
            }

        }

        public CountingList Load(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string timeString;
            if (hdrStrings[0].StartsWith("Fault date :"))
            {
                timeString = hdrStrings[0].Replace("Fault date : ", string.Empty);
            }
            else
            {
                string[] buff = hdrStrings[0].Split(' ');
                timeString = string.Format("{0} {1}", buff[2], buff[3]);
            }
            int alarm = int.Parse(hdrStrings[2].Replace("Alarm = ", string.Empty));

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);
            double[,] factors = new double[VOLTAGES_COUNT, 2];

            Regex factorRegex = new Regex(@"\d+\,[U]\w+\,\,\,[V]\,(?<valueA>[0-9\.-]+)\,(?<valueB>[0-9\.-]+)");
            for (int i = 2; i < 2 + VOLTAGES_COUNT; i++)
            {
                string res = factorRegex.Match(cfgStrings[i]).Groups["valueA"].Value;
                NumberFormatInfo format = new System.Globalization.NumberFormatInfo { NumberDecimalSeparator = "." };
                factors[i - 2, 0] = double.Parse(res, format);
                res = factorRegex.Match(cfgStrings[i]).Groups["valueB"].Value;
                factors[i - 2, 1] = double.Parse(res, format);
            }

            int counts = int.Parse(cfgStrings[8].Replace("1000,", string.Empty));

            CountingList result = new CountingList(counts) { _alarm = alarm };
            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);

            string dataPattern =
                @"^\d+\,\d+,(?<U1>\-?\d+),(?<U2>\-?\d+),(?<U3>\-?\d+),(?<U4>\-?\d+)";
            Regex dataRegex = new Regex(dataPattern);



            double[][] currents = new double[VOLTAGES_COUNT][];


            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new double[counts];
            }



            for (int i = 0; i < datStrings.Length; i++)
            {
                for (int j = 0; j < VOLTAGES_COUNT; j++)
                {
                    currents[j][i] = double.Parse(dataRegex.Match(datStrings[i]).Groups["U" + (j + 1)].Value) * factors[j, 0] + factors[j, 1];
                }


            }
            for (int i = 0; i < VOLTAGES_COUNT; i++)
            {
                result._maxU = Math.Max(result.MaxU, currents[i].Max());
                result._minU = Math.Min(result.MinU, currents[i].Min());
            }


            result.Currents = currents;
            result.FilePath = filePath;
            result.IsLoad = true;


            result._dateTime = timeString;

            return result;


        }
        private CountingList(int count)
        {
            this._currents = new double[VOLTAGES_COUNT][];
            this._baseCurrents = new short[VOLTAGES_COUNT][];

            this._count = count;
        }
    }
}
