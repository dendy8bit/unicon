﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR600.Old.HelpClasses;

namespace BEMN.MR600.Old.Structures
{
    public class MeasuringTransformator : StructBase
    {
        [Layout(0)] private ushort _tn;
        [Layout(1)] private ushort _tnnp;

        public double TN
        {
            get { return Measuring.GetConstraint(this._tn, ConstraintKoefficient.K_Undefine); }
            set { this._tn = Measuring.SetConstraint(value, ConstraintKoefficient.K_Undefine); }
        }

        public double TNNP
        {
            get { return Measuring.GetConstraint(this._tnnp, ConstraintKoefficient.K_Undefine); }
            set { this._tnnp = Measuring.SetConstraint(value, ConstraintKoefficient.K_Undefine); }
        }

    }
}
