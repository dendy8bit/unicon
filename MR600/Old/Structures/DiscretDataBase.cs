﻿using System.Collections;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR600.Old.Structures
{
    public class DiscretDataBase : StructBase
    {
        [Layout(0, Count = 14)] private ushort[] _diagnostic;

        public BitArray ManageSignals
        {
            get
            {
                BitArray ret = new BitArray(10);
                for (int i = 2; i < 8; i++)
                {
                    ret[i - 2] = Common.GetBit(this._diagnostic[0], i);
                }
                ret[6] = Common.GetBit(this._diagnostic[8], 1);
                ret[7] = Common.GetBit(this._diagnostic[8], 5);
                ret[8] = Common.GetBit(this._diagnostic[8], 6);
                ret[9] = Common.GetBit(this._diagnostic[8], 7);
                return ret;
            }
        }

        public BitArray Indicators
        {
            get { return new BitArray(new byte[] { Common.LOBYTE(this._diagnostic[6]) }); }
        }

        public BitArray InputSignals
        {
            get
            {
                return new BitArray(Common.GetBitsArray(this._diagnostic, 136, 152));
            }
        }

        public BitArray Rele
        {
            get
            {
                return new BitArray(Common.GetBitsArray(this._diagnostic, 104, 120));
            }
        }

        public BitArray OutputSignals
        {
            get { return new BitArray(new byte[] { Common.LOBYTE(this._diagnostic[13]) }); }
        }

        public BitArray LimitSignals
        {
            get
            {
                return new BitArray(Common.GetBitsArray(this._diagnostic, 152, 200));
            }
        }

        public BitArray FaultState
        {
            get { return new BitArray(new byte[] { Common.LOBYTE(this._diagnostic[4]) }); }
        }

        public BitArray FaultSignals
        {
            get
            {
                return new BitArray(Common.GetBitsArray(this._diagnostic, 72, 88));
            }
        }

        public BitArray ExternalSignals
        {
            get { return new BitArray(new byte[] { Common.HIBYTE(this._diagnostic[12]) }); }
        }
    }
}
