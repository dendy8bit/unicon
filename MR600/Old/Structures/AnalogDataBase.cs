﻿using System;
using System.ComponentModel;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MR600.Old.HelpClasses;

namespace BEMN.MR600.Old.Structures
{
    public class AnalogDataBase : StructBase
    {
        [Layout(0, Count = 11)] private ushort[] _analogDataBase;
        public  MeasuringTransformator Transformator { get; set; }

        [Category("Аналоговые сигналы")]
        public string F
        {
            get
            {
                double ret = Measuring.GetConstraint(this._analogDataBase[0], ConstraintKoefficient.K_25600);
                return String.Format("{0:F2} Гц", ret);
            }
        }

        public string Un
        {
            get { return ValuesConverterCommon.Analog.GetU(this._analogDataBase[1], this.Transformator.TNNP); }
        }

        public string Ua
        {
            get { return ValuesConverterCommon.Analog.GetU(this._analogDataBase[2], this.Transformator.TN); }
        }

        public string Ub
        {
            get { return ValuesConverterCommon.Analog.GetU(this._analogDataBase[3], this.Transformator.TN); }
        }

        public string Uc
        {
            get { return ValuesConverterCommon.Analog.GetU(this._analogDataBase[4], this.Transformator.TN); }
        }

        public string Uab
        {
            get { return ValuesConverterCommon.Analog.GetU(this._analogDataBase[5], this.Transformator.TN); }
        }

        public string Ubc
        {
            get { return ValuesConverterCommon.Analog.GetU(this._analogDataBase[6], this.Transformator.TN); }
        }

        public string Uca
        {
            get { return ValuesConverterCommon.Analog.GetU(this._analogDataBase[7], this.Transformator.TN); }
        }

        public string U0
        {
            get { return ValuesConverterCommon.Analog.GetU(this._analogDataBase[8], this.Transformator.TN); }
        }

        public string U1
        {
            get { return ValuesConverterCommon.Analog.GetU(this._analogDataBase[9], this.Transformator.TN); }
        }

        public string U2
        {
            get { return ValuesConverterCommon.Analog.GetU(this._analogDataBase[10], this.Transformator.TN); }
        }
    }
}
