﻿using System;
using System.Collections;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.MBServer;

namespace BEMN.MR600.Old.HelpClasses
{
    public class Oscilloscope : Devices.Oscilloscope
    {
        private MR600 _device;

        public Oscilloscope(Device device)
            : base(device)
        {
            this._device = (MR600)device;
        }

        public override void Initialize()
        {
            base.Initialize();

            _analogChannelsCnt = 5;
            _diskretChannelsCnt = 17;
            _exchangeCnt = 256;
            _oscilloscopeSize = 0x8000;
            _pointCount = 3276;
            _oscJournal = new Device.slot(0x3800, 0x3818);
            _journalLength = 0x18;

            AnalogUChannels.Add("Ua");
            AnalogUChannels.Add("Ub");
            AnalogUChannels.Add("Uc");
            AnalogUChannels.Add("Uo");
            AnalogUChannels.Add("Авария");

            DiskretChannels.Add("Авария");
            //for (int i = 0; i < 17; i++)
            //{
            //    if (i == 16)
            //    {
            //        DiskretChannels.Add("Авария");
            //    }
            //    else
            //    {
            //        DiskretChannels.Add("Д" + (i + 1));
            //    }
            //}
        }

        public override void LoadOscilloscopeSettings()
        {
            this._device.LoadInputSignals();
            base.LoadOscilloscopeSettings();
        }

        public string Reverse(string revString)
        {
            string ret = "";
            for (int i = revString.Length; i > 0; i--)
            {
                ret += revString[i - 1];
            }
            return ret;
        }

        public override void CreateJournal()
        {
            Dictionary<string, string> ret = new Dictionary<string, string>(0xE);
            MR600.CAlarmJournal helper = new MR600.CAlarmJournal(this._device);
            byte[] buf = Common.TOBYTES(_oscSettings.Value, true);
            _BUF = Common.TOBYTES(_oscSettings.Value, true);

            #region Дата-Время
            string str = "";
            int i = 7;
            int j = 9;
            do
            {
                if (buf[i] > 9)
                {
                    if (i != 3)
                    {
                        str += buf[i] + "-";
                    }
                    else
                    {
                        str += buf[i];
                    }

                }
                else
                {
                    if (i != 3)
                    {
                        str += "0" + buf[i] + "-";
                    }
                    else
                    {
                        str += "0" + buf[i];
                    }
                }
                i -= 2;
            } while (i >= 3);
            str += "   ";

            do
            {
                if (buf[j] > 9)
                {
                    if (j != 15)
                    {
                        str += buf[j] + ":";
                    }
                    else
                    {
                        str += buf[j];
                    }

                }
                else
                {
                    if (j != 15)
                    {
                        str += "0" + buf[j] + ":";
                    }
                    else
                    {
                        str += "0" + buf[j];
                    }
                }
                j += 2;
            } while (j <= 15);
            #endregion

            DateTime = str;
            DeviceKtn = this._device.TN.ToString();
            DeviceKtnnp = this._device.TNNP.ToString();
            byte[] buffer = Common.TOBYTES(_journalBuffer, false);
            byte[] datetime = new byte[7];
            Array.ConstrainedCopy(buffer, 9, datetime, 0, 7);

            ret.Add("Сообщение", helper.GetMessage(buffer[8]));
            ret.Add("Дата-время", helper.GetDateTime(datetime));
            ret.Add("Код", helper.GetCode(buffer[16], buffer[18]));

            byte[] b1 = new byte[] { buffer[38] };
            byte[] b2 = new byte[] { buffer[39] };
            ret.Add("Дискр.1-8", this.Reverse(Common.BitsToString(new BitArray(b1))));
            ret.Add("Дискр.8-16", this.Reverse(Common.BitsToString(new BitArray(b2))));
            ret.Add("TН", this._device.TN.ToString());
            ret.Add("TННП", this._device.TNNP.ToString());
            ret.Add("Устройство", this._device.DeviceType.ToString());
            _journal = ret;

        }
        protected override void PrepareData_OldVers(byte[] data)
        {


        }

        protected override void PrepareData(byte[] data)
        {
            Dev = "";
            if (_journal.ContainsKey("TН"))
            {
                this._device.TN = double.Parse(_journal["TН"]);
            }
            if (_journal.ContainsKey("TННП"))
            {
                this._device.TNNP = double.Parse(_journal["TННП"]);
            }
            if (_journal.ContainsKey("Устройство")) 
            {
                Dev = _journal["Устройство"];
            }
            DeviceName = Device.DeviceType.ToString();

            int _KoefAvary = 32768 - 2048 * 10;
            int _KoefZashiti = KoefZashiti;

            double ukTN = this._device.TN;
            double ukTNNP = this._device.TNNP;
            KoefU = ukTN / ukTNNP;
            KoefShkaliU = 256 * ukTN * 1.41;

            double k = 0.15625;
            _diskretData = new List<BitArray>();
            for (int i = 0; i < 17; i++)
            {
                _diskretData.Add(new BitArray(_pointCount));
            }
            
            for (int i = 0; i < _pointCount - 1; i++)
            {
                int channelCfg = 0;
                int uChannel = 0;
                int cChannel = 0;
                int channel = 0;
                //Аналоги
                double value = BitConverter.ToUInt16(data, i * 10 + 0);
                ComTradeData[i][cChannel++] = value;
                ComTradeDataCfg[channelCfg][0] = KoefShkaliU / 32768;
                ComTradeDataCfg[channelCfg++][1] = -1 * KoefShkaliU;
                _analogUData[channel++][i] = ((value - 0x8000) / 0x8000) * KoefShkaliU;

                value = BitConverter.ToUInt16(data, i * 10 + 2);
                ComTradeData[i][cChannel++] = value;
                ComTradeDataCfg[channelCfg][0] = KoefShkaliU / 32768;
                ComTradeDataCfg[channelCfg++][1] = -1 * KoefShkaliU;
                _analogUData[channel++][i] = ((value - 0x8000) / 0x8000) * KoefShkaliU;

                value = BitConverter.ToUInt16(data, i * 10 + 4);
                ComTradeData[i][cChannel++] = value;
                ComTradeDataCfg[channelCfg][0] = KoefShkaliU / 32768;
                ComTradeDataCfg[channelCfg++][1] = -1 * KoefShkaliU;
                _analogUData[channel++][i] = ((value - 0x8000) / 0x8000) * KoefShkaliU;

                value = BitConverter.ToUInt16(data, i * 10 + 6);
                ComTradeData[i][cChannel++] = value;
                ComTradeDataCfg[channelCfg][0] = KoefShkaliU / 32768;
                ComTradeDataCfg[channelCfg++][1] = -1 * KoefShkaliU;
                _analogUData[channel][i] = ((value - 0x8000) / 0x8000) * 5 * ukTNNP * 1.41;

               if (i <= (_KoefAvary / _KoefZashiti))
                {
                    _analogUData[4][i] = -KoefShkaliU;
                }
                else
                {
                    _analogUData[4][i] = KoefShkaliU;
                }

                //Дискреты
                double high = BitConverter.ToInt16(data, i * 10 + 8);
                for (int j = 0; j < 8; j++)
                {
                    _diskretData[j][i] = (0 == ((int)high >> j & 1)) ? false : true;
                    ComTradeData[i][cChannel++] = (0 == ((int)high >> j & 1)) ? 0 : 1;  
                }
                double low = BitConverter.ToInt16(data, i * 10 + 9);
                for (int j = 0; j < 8; j++)
                {
                    _diskretData[j + 8][i] = (0 == ((int)low >> j & 1)) ? false : true;
                    ComTradeData[i][cChannel++] = (0 == ((int)high >> j & 1)) ? 0 : 1;  
                }
                if (i <= (_KoefAvary / _KoefZashiti))
                {
                    _diskretData[16][i] = true;
                }
                else
                {
                    _diskretData[16][i] = false;
                }
            }
        }
    }
}

