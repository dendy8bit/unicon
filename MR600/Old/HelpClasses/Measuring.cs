using System;
using BEMN.MBServer;

namespace BEMN.MR600.Old.HelpClasses
{
    public class Measuring
    {
        public static double GetI(ushort value, ushort KoeffNPTT, bool phaseTok)
        {
            int b;
            b = phaseTok ? 40 : 5;
            return (double) ((double) b*(double) value/(double) 0x10000*(double) KoeffNPTT);
        }

        public static double GetU(ushort value, double Koeff)
        {
            return (double)(value / (double)0x100 * Koeff);
        }

        public static double GetConstraintOnly(ushort value, ConstraintKoefficient koeff)
        {
            double temp = ((double)value * (int)koeff / 65535) / 100;
            return Math.Round(temp, 2);
            //return Math.Floor(temp + 0.5) / 100;
        }

        public static double GetF(ushort value)
        {
            return (double) ((double) value/(double) 0x100);
        }

        public static ulong GetTime(ushort value)
        {
            return value < 32768 ? (ulong) value*10 : ((ulong) value - 32768)*100;
        }

        public static double GetConstraint(ushort value, ConstraintKoefficient koeff)
        {
            double ret = 0.0f;

            if (ConstraintKoefficient.K_Undefine == koeff)
            {
                ret = GetConstraint(value, ConstraintKoefficient.K_25600);
                if (Common.HIBYTE(value) >= 0x80)
                {
                    ret -= 0x80;
                    ret = ret > 1 ? Math.Floor(ret*10 + 0.5)/10 : Math.Floor(ret*100 + 0.5)/100;
                    ret *= 1000;
                }
            }
            else
            {
                double temp = (double)value * (int)koeff / 65536;
                ret = Math.Round(temp, 3) / 100;
            }
            return ret;
        }

        public static ushort SetConstraint(double value, ConstraintKoefficient koeff)
        {
            ushort ret = 0;
            if (ConstraintKoefficient.K_Undefine == koeff)
            {
                if (value >= 128)
                {
                    ret = SetConstraint(value/1000, ConstraintKoefficient.K_25600);
                    ret += 0x8000;
                }
                else
                {
                    ret = SetConstraint(value, ConstraintKoefficient.K_25600);
                }
            }
            else
            {
                ret = (ushort) (value*65536*100/(int) koeff);
            }
            return ret;
        }

        public static ushort SetTime(ulong value)
        {
            return value < 327680 ? (ushort) (value/10) : (ushort) (value/100 + 32768);
        }
    }
}