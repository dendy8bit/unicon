using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Forms.Export;
using BEMN.Interfaces;

namespace BEMN.MR600.Old.Forms
{    
    public partial class AlarmJournalForm : Form , IFormView
    {
        private MR600 _device;
        private DataTable _table;

        public AlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public AlarmJournalForm(MR600 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._journalProgress.Maximum = MR600.ALARMJOURNAL_RECORD_CNT;
            this._device.AlarmJournalRecordLoadOk += this._device_AlarmJournalRecordLoadOk;
            this._device.AlarmJournalRecordLoadFail += this._device_AlarmJournalRecordLoadFail;
        }

        void _device_AlarmJournalRecordLoadFail(object sender, int index)
        {
            MessageBox.Show("���������� ��������� ������ ������. ��������� �����.", "������ - ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        void _device_AlarmJournalRecordLoadOk(object sender, int index)
        {
            try
            {
                Invoke(new OnDeviceIndexEventHandler(this.OnAlarmJournalRecordLoadOk), index);
            }
            catch (InvalidOperationException ee)
            { }
        }

        private void OnAlarmJournalRecordLoadOk(int i)
        {
            this._journalProgress.Increment(1);
            if (this._device.AlarmJournal[i].msg == MR600.JURNAL_CLEAR) return;
            this._journalCntLabel.Text = (i + 1).ToString();
            this._journalGrid.Rows.Add(i + 1, this._device.AlarmJournal[i].time, this._device.AlarmJournal[i].msg,
                this._device.AlarmJournal[i].code, this._device.AlarmJournal[i].type_value,
                this._device.AlarmJournal[i].F, this._device.AlarmJournal[i].Ua, this._device.AlarmJournal[i].Ub,
                this._device.AlarmJournal[i].Uc, this._device.AlarmJournal[i].Uo, this._device.AlarmJournal[i].Uab,
                this._device.AlarmJournal[i].Ubc, this._device.AlarmJournal[i].Uca, this._device.AlarmJournal[i].U0,
                this._device.AlarmJournal[i].U1, this._device.AlarmJournal[i].U2,
                this._device.AlarmJournal[i].inSignals1, this._device.AlarmJournal[i].inSignals2);
            if (i == 31)
            {
                this._readBut.Enabled = true;
            }
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR600); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(AlarmJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return "������ ������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _readBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.AlarmJournal = new MR600.CAlarmJournal(this._device);
            this._readBut.Enabled = false;
            this._device.RemoveAlarmJournal();
            this._journalProgress.Value = 0;
            this._journalGrid.Rows.Clear();
            this._device.LoadAlarmJournal();
        }

        private void AlarmJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.RemoveAlarmJournal();
        }

        private void AlarmJournalForm_Activated(object sender, EventArgs e)
        {
            this._device.SuspendAlarmJournal(false);
        }

        private void AlarmJournalForm_Deactivate(object sender, EventArgs e)
        {
            this._device.SuspendAlarmJournal(true);
        }

        private void _serializeBut_Click(object sender, EventArgs e)
        {
            this.FillTable();
            if (DialogResult.OK == this._saveSysJournalDlg.ShowDialog())
            {
                this._table.WriteXml(this._saveSysJournalDlg.FileName);
            }
        }

        private void FillTable()
        {
            this._table = CreateAlarmJournalTable();
            List<object> row = new List<object>();

            for (int i = 0; i < this._journalGrid.Rows.Count; i++)
            {
                row.Clear();
                for (int j = 0; j < this._journalGrid.Columns.Count; j++)
                {
                    row.Add(this._journalGrid[j, i].Value);
                }
                this._table.Rows.Add(row.ToArray());
            }
        }

        private static DataTable CreateAlarmJournalTable()
        {
            DataTable table = new DataTable("��600_������_������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");
            table.Columns.Add("���_�����������");
            table.Columns.Add("���_�����������");
            table.Columns.Add("F");
            table.Columns.Add("Ua");
            table.Columns.Add("Ub");
            table.Columns.Add("Uc");
            table.Columns.Add("Uo");
            table.Columns.Add("Uab");
            table.Columns.Add("Ubc");
            table.Columns.Add("Uca");
            table.Columns.Add("U0");
            table.Columns.Add("U1");
            table.Columns.Add("U2");
            table.Columns.Add("��.�������_8-1");
            table.Columns.Add("��.�������_9-16");
            return table;
        }

        private void _deserializeBut_Click(object sender, EventArgs e)
        {
            DataTable table = CreateAlarmJournalTable();
          
            if (DialogResult.OK == this._openSysJounralDlg.ShowDialog())
            {
                this._journalGrid.Rows.Clear();
                table.ReadXml(this._openSysJounralDlg.FileName);
            }

            List<object> row = new List<object>();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                row.Clear();
                row.AddRange(table.Rows[i].ItemArray);
                this._journalGrid.Rows.Add(row.ToArray());
            }
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                this.FillTable();
                HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR600AJ);
                MessageBox.Show("������ ��������");
            }
        }
    }
}