namespace BEMN.MR600.Old.Forms
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToHtmlItem = new System.Windows.Forms.ToolStripMenuItem();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this._exchangeProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._processLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._saveToXmlButton = new System.Windows.Forms.Button();
            this._saveXmlDialog = new System.Windows.Forms.SaveFileDialog();
            this._defensesPage = new System.Windows.Forms.TabPage();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this._defenseMainConstraintRadio = new System.Windows.Forms.RadioButton();
            this._defenseReserveConstraintRadio = new System.Windows.Forms.RadioButton();
            this._ChangeGroupButton2 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this._externalDefensePage = new System.Windows.Forms.TabPage();
            this._externalDefenseGrid = new System.Windows.Forms.DataGridView();
            this._extDefNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefWorkingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefBlockingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefWorkingTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefReturnCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefReturnNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefReturnTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesPage = new System.Windows.Forms.TabPage();
            this._voltageDefensesGrid3 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesTypeCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesConstraintCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesBlockingNumberCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesTimeCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesGrid2 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesTypeCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesConstraintCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesBlockingNumberCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesTimeCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesGrid1 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesTypeCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesConstraintCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesBlockingNumberCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesTimeCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequencePage = new System.Windows.Forms.TabPage();
            this._frequenceDefensesGrid1 = new System.Windows.Forms.DataGridView();
            this._frequenceDefensesName1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesMode1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesWorkConstraint1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesBlockingNumber1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesWorkTime1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label12 = new System.Windows.Forms.Label();
            this._frequenceDefensesGrid = new System.Windows.Forms.DataGridView();
            this._frequenceDefensesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesMode = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesWorkConstraint = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesBlockingNumber = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesWorkTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._CAPV_ModeCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._CAPV_FrequencyCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._CAPV_BlockingCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._CAPV_TimeCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this._outputSignalsAlarmTimeBox = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this._outputSignalsAlarmReleCombo = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this._outputSignalsSignalizationTimeBox = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this._outputSignalsSignalizationReleCombo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this._outputSignalsDispepairTimeBox = new System.Windows.Forms.MaskedTextBox();
            this._dispepairCheckList = new System.Windows.Forms.CheckedListBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._outputLogicCheckList = new System.Windows.Forms.CheckedListBox();
            this._outputLogicAcceptBut = new System.Windows.Forms.Button();
            this._outputLogicCombo = new System.Windows.Forms.ComboBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._outputIndicatorsGrid = new System.Windows.Forms.DataGridView();
            this._outIndNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outIndTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndJS_Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndJA_Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releImpulseCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._inSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._inputSignalsTN_DispepairCombo = new System.Windows.Forms.ComboBox();
            this._inputSignalsIndicationResetCombo = new System.Windows.Forms.ComboBox();
            this._inputSignalsConstraintResetCombo = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._applyLogicSignalsBut = new System.Windows.Forms.Button();
            this._logicChannelsCombo = new System.Windows.Forms.ComboBox();
            this._logicSignalsDataGrid = new System.Windows.Forms.DataGridView();
            this._diskretChannelCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._diskretValueCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this._TNNP_Box = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this._TN_Box = new System.Windows.Forms.MaskedTextBox();
            this._tabControl = new System.Windows.Forms.TabControl();
            this.button1 = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.contextMenu.SuspendLayout();
            this._statusStrip.SuspendLayout();
            this._defensesPage.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this._externalDefensePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).BeginInit();
            this._voltageDefensesPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid1)).BeginInit();
            this._frequencePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._frequenceDefensesGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._frequenceDefensesGrid)).BeginInit();
            this._outputSignalsPage.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this._inSignalsPage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid)).BeginInit();
            this.groupBox2.SuspendLayout();
            this._tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.readFromFileItem,
            this.writeToFileItem,
            this.writeToHtmlItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 114);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "��������� �� ����������";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "�������� � ����������";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "��������� �� �����";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "��������� � ����";
            // 
            // writeToHtmlItem
            // 
            this.writeToHtmlItem.Name = "writeToHtmlItem";
            this.writeToHtmlItem.Size = new System.Drawing.Size(212, 22);
            this.writeToHtmlItem.Text = "��������� � HTML";
            // 
            // _readConfigBut
            // 
            this._readConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readConfigBut.Location = new System.Drawing.Point(10, 493);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(147, 23);
            this._readConfigBut.TabIndex = 1;
            this._readConfigBut.Text = "��������� �� ����������";
            this.toolTip1.SetToolTip(this._readConfigBut, "��������� ������������ �� ���������� (CTRL+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _toolTip
            // 
            this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeConfigBut.Location = new System.Drawing.Point(163, 493);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(172, 23);
            this._writeConfigBut.TabIndex = 2;
            this._writeConfigBut.Text = "�������� � ����������";
            this.toolTip1.SetToolTip(this._writeConfigBut, "�������� ������������ � ���������� (CTRL+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveConfigBut.Location = new System.Drawing.Point(619, 493);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(134, 23);
            this._saveConfigBut.TabIndex = 4;
            this._saveConfigBut.Text = "��������� � ����";
            this.toolTip1.SetToolTip(this._saveConfigBut, "��������� ������������ � ���� (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._loadConfigBut.Location = new System.Drawing.Point(347, 493);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(131, 23);
            this._loadConfigBut.TabIndex = 3;
            this._loadConfigBut.Text = "��������� �� �����";
            this.toolTip1.SetToolTip(this._loadConfigBut, "��������� ������������ �� ����� (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "bin";
            this._saveConfigurationDlg.FileName = "��600_�������";
            this._saveConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._saveConfigurationDlg.Title = "���������  ������� ��� ��600";
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "bin";
            this._openConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "������� ������� ��� ��600";
            // 
            // _statusStrip
            // 
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._exchangeProgressBar,
            this._processLabel});
            this._statusStrip.Location = new System.Drawing.Point(0, 523);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this._statusStrip.Size = new System.Drawing.Size(907, 22);
            this._statusStrip.TabIndex = 5;
            this._statusStrip.Text = "statusStrip1";
            // 
            // _exchangeProgressBar
            // 
            this._exchangeProgressBar.Maximum = 50;
            this._exchangeProgressBar.Name = "_exchangeProgressBar";
            this._exchangeProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // _processLabel
            // 
            this._processLabel.Name = "_processLabel";
            this._processLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _saveToXmlButton
            // 
            this._saveToXmlButton.AllowDrop = true;
            this._saveToXmlButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveToXmlButton.AutoSize = true;
            this._saveToXmlButton.Location = new System.Drawing.Point(759, 493);
            this._saveToXmlButton.Name = "_saveToXmlButton";
            this._saveToXmlButton.Size = new System.Drawing.Size(136, 23);
            this._saveToXmlButton.TabIndex = 34;
            this._saveToXmlButton.Text = "��������� � HTML";
            this._saveToXmlButton.UseVisualStyleBackColor = true;
            this._saveToXmlButton.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _saveXmlDialog
            // 
            this._saveXmlDialog.Filter = "(*.html) | *.html";
            // 
            // _defensesPage
            // 
            this._defensesPage.Controls.Add(this.groupBox20);
            this._defensesPage.Controls.Add(this.tabControl1);
            this._defensesPage.Location = new System.Drawing.Point(4, 22);
            this._defensesPage.Name = "_defensesPage";
            this._defensesPage.Padding = new System.Windows.Forms.Padding(3);
            this._defensesPage.Size = new System.Drawing.Size(899, 461);
            this._defensesPage.TabIndex = 8;
            this._defensesPage.Text = "������";
            this._defensesPage.UseVisualStyleBackColor = true;
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this._defenseMainConstraintRadio);
            this.groupBox20.Controls.Add(this._defenseReserveConstraintRadio);
            this.groupBox20.Controls.Add(this._ChangeGroupButton2);
            this.groupBox20.Location = new System.Drawing.Point(12, 1);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(414, 39);
            this.groupBox20.TabIndex = 7;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "������ �������";
            // 
            // _defenseMainConstraintRadio
            // 
            this._defenseMainConstraintRadio.AutoSize = true;
            this._defenseMainConstraintRadio.Checked = true;
            this._defenseMainConstraintRadio.Location = new System.Drawing.Point(6, 14);
            this._defenseMainConstraintRadio.Name = "_defenseMainConstraintRadio";
            this._defenseMainConstraintRadio.Size = new System.Drawing.Size(77, 17);
            this._defenseMainConstraintRadio.TabIndex = 0;
            this._defenseMainConstraintRadio.TabStop = true;
            this._defenseMainConstraintRadio.Text = "��������";
            this._defenseMainConstraintRadio.UseVisualStyleBackColor = true;
            this._defenseMainConstraintRadio.CheckedChanged += new System.EventHandler(this._defenseMainConstraintRadio_CheckedChanged);
            // 
            // _defenseReserveConstraintRadio
            // 
            this._defenseReserveConstraintRadio.AutoSize = true;
            this._defenseReserveConstraintRadio.Location = new System.Drawing.Point(97, 14);
            this._defenseReserveConstraintRadio.Name = "_defenseReserveConstraintRadio";
            this._defenseReserveConstraintRadio.Size = new System.Drawing.Size(82, 17);
            this._defenseReserveConstraintRadio.TabIndex = 1;
            this._defenseReserveConstraintRadio.Text = "���������";
            this._defenseReserveConstraintRadio.UseVisualStyleBackColor = true;
            // 
            // _ChangeGroupButton2
            // 
            this._ChangeGroupButton2.Location = new System.Drawing.Point(204, 11);
            this._ChangeGroupButton2.Name = "_ChangeGroupButton2";
            this._ChangeGroupButton2.Size = new System.Drawing.Size(192, 23);
            this._ChangeGroupButton2.TabIndex = 0;
            this._ChangeGroupButton2.Text = "�������� --> ���������";
            this._ChangeGroupButton2.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this._externalDefensePage);
            this.tabControl1.Controls.Add(this._voltageDefensesPage);
            this.tabControl1.Controls.Add(this._frequencePage);
            this.tabControl1.Location = new System.Drawing.Point(8, 46);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(739, 399);
            this.tabControl1.TabIndex = 6;
            // 
            // _externalDefensePage
            // 
            this._externalDefensePage.Controls.Add(this._externalDefenseGrid);
            this._externalDefensePage.Location = new System.Drawing.Point(4, 22);
            this._externalDefensePage.Name = "_externalDefensePage";
            this._externalDefensePage.Padding = new System.Windows.Forms.Padding(3);
            this._externalDefensePage.Size = new System.Drawing.Size(731, 373);
            this._externalDefensePage.TabIndex = 0;
            this._externalDefensePage.Text = "������� ������";
            this._externalDefensePage.UseVisualStyleBackColor = true;
            // 
            // _externalDefenseGrid
            // 
            this._externalDefenseGrid.AllowUserToAddRows = false;
            this._externalDefenseGrid.AllowUserToDeleteRows = false;
            this._externalDefenseGrid.AllowUserToResizeColumns = false;
            this._externalDefenseGrid.AllowUserToResizeRows = false;
            this._externalDefenseGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._externalDefenseGrid.ColumnHeadersHeight = 30;
            this._externalDefenseGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._externalDefenseGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extDefNumberCol,
            this._exDefModeCol,
            this._exDefWorkingCol,
            this._exDefBlockingCol,
            this._exDefWorkingTimeCol,
            this._exDefReturnCol,
            this._exDefReturnNumberCol,
            this._exDefReturnTimeCol});
            this._externalDefenseGrid.Location = new System.Drawing.Point(2, 3);
            this._externalDefenseGrid.Name = "_externalDefenseGrid";
            this._externalDefenseGrid.RowHeadersVisible = false;
            this._externalDefenseGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDefenseGrid.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this._externalDefenseGrid.RowTemplate.Height = 24;
            this._externalDefenseGrid.Size = new System.Drawing.Size(678, 224);
            this._externalDefenseGrid.TabIndex = 1;
            // 
            // _extDefNumberCol
            // 
            this._extDefNumberCol.HeaderText = "�";
            this._extDefNumberCol.Name = "_extDefNumberCol";
            this._extDefNumberCol.ReadOnly = true;
            this._extDefNumberCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._extDefNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._extDefNumberCol.Width = 40;
            // 
            // _exDefModeCol
            // 
            this._exDefModeCol.HeaderText = "�����";
            this._exDefModeCol.Name = "_exDefModeCol";
            this._exDefModeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._exDefModeCol.Width = 110;
            // 
            // _exDefWorkingCol
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._exDefWorkingCol.DefaultCellStyle = dataGridViewCellStyle1;
            this._exDefWorkingCol.HeaderText = "������������";
            this._exDefWorkingCol.Name = "_exDefWorkingCol";
            this._exDefWorkingCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._exDefWorkingCol.Width = 110;
            // 
            // _exDefBlockingCol
            // 
            this._exDefBlockingCol.HeaderText = "����������";
            this._exDefBlockingCol.Name = "_exDefBlockingCol";
            this._exDefBlockingCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._exDefBlockingCol.Width = 75;
            // 
            // _exDefWorkingTimeCol
            // 
            this._exDefWorkingTimeCol.HeaderText = "����� ������.";
            this._exDefWorkingTimeCol.Name = "_exDefWorkingTimeCol";
            this._exDefWorkingTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._exDefWorkingTimeCol.Width = 90;
            // 
            // _exDefReturnCol
            // 
            this._exDefReturnCol.HeaderText = "����.";
            this._exDefReturnCol.Name = "_exDefReturnCol";
            this._exDefReturnCol.Width = 40;
            // 
            // _exDefReturnNumberCol
            // 
            this._exDefReturnNumberCol.HeaderText = "���� ��������";
            this._exDefReturnNumberCol.Name = "_exDefReturnNumberCol";
            this._exDefReturnNumberCol.Width = 110;
            // 
            // _exDefReturnTimeCol
            // 
            this._exDefReturnTimeCol.HeaderText = "����� ��������";
            this._exDefReturnTimeCol.Name = "_exDefReturnTimeCol";
            this._exDefReturnTimeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._exDefReturnTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _voltageDefensesPage
            // 
            this._voltageDefensesPage.AutoScroll = true;
            this._voltageDefensesPage.Controls.Add(this._voltageDefensesGrid3);
            this._voltageDefensesPage.Controls.Add(this._voltageDefensesGrid2);
            this._voltageDefensesPage.Controls.Add(this._voltageDefensesGrid1);
            this._voltageDefensesPage.Location = new System.Drawing.Point(4, 22);
            this._voltageDefensesPage.Name = "_voltageDefensesPage";
            this._voltageDefensesPage.Padding = new System.Windows.Forms.Padding(3);
            this._voltageDefensesPage.Size = new System.Drawing.Size(731, 373);
            this._voltageDefensesPage.TabIndex = 1;
            this._voltageDefensesPage.Text = "������ �� ����������";
            this._voltageDefensesPage.UseVisualStyleBackColor = true;
            // 
            // _voltageDefensesGrid3
            // 
            this._voltageDefensesGrid3.AllowUserToAddRows = false;
            this._voltageDefensesGrid3.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid3.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid3.AllowUserToResizeRows = false;
            this._voltageDefensesGrid3.BackgroundColor = System.Drawing.SystemColors.Control;
            this._voltageDefensesGrid3.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid3.ColumnHeadersVisible = false;
            this._voltageDefensesGrid3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol3,
            this._voltageDefensesModeCol3,
            this._voltageDefensesTypeCol3,
            this._voltageDefensesConstraintCol3,
            this._voltageDefensesBlockingNumberCol3,
            this._voltageDefensesTimeCol3});
            this._voltageDefensesGrid3.Location = new System.Drawing.Point(3, 318);
            this._voltageDefensesGrid3.Name = "_voltageDefensesGrid3";
            this._voltageDefensesGrid3.RowHeadersVisible = false;
            this._voltageDefensesGrid3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid3.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this._voltageDefensesGrid3.RowTemplate.Height = 24;
            this._voltageDefensesGrid3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this._voltageDefensesGrid3.Size = new System.Drawing.Size(538, 99);
            this._voltageDefensesGrid3.TabIndex = 13;
            // 
            // _voltageDefensesNameCol3
            // 
            this._voltageDefensesNameCol3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesNameCol3.DefaultCellStyle = dataGridViewCellStyle3;
            this._voltageDefensesNameCol3.HeaderText = "";
            this._voltageDefensesNameCol3.MinimumWidth = 50;
            this._voltageDefensesNameCol3.Name = "_voltageDefensesNameCol3";
            this._voltageDefensesNameCol3.ReadOnly = true;
            this._voltageDefensesNameCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol3.Width = 50;
            // 
            // _voltageDefensesModeCol3
            // 
            this._voltageDefensesModeCol3.HeaderText = "�����";
            this._voltageDefensesModeCol3.Name = "_voltageDefensesModeCol3";
            this._voltageDefensesModeCol3.Width = 120;
            // 
            // _voltageDefensesTypeCol3
            // 
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesTypeCol3.DefaultCellStyle = dataGridViewCellStyle4;
            this._voltageDefensesTypeCol3.HeaderText = "��� �������";
            this._voltageDefensesTypeCol3.Name = "_voltageDefensesTypeCol3";
            this._voltageDefensesTypeCol3.Width = 120;
            // 
            // _voltageDefensesConstraintCol3
            // 
            this._voltageDefensesConstraintCol3.HeaderText = "�������";
            this._voltageDefensesConstraintCol3.Name = "_voltageDefensesConstraintCol3";
            this._voltageDefensesConstraintCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesConstraintCol3.ToolTipText = "������� ������������";
            this._voltageDefensesConstraintCol3.Width = 60;
            // 
            // _voltageDefensesBlockingNumberCol3
            // 
            this._voltageDefensesBlockingNumberCol3.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol3.Name = "_voltageDefensesBlockingNumberCol3";
            this._voltageDefensesBlockingNumberCol3.Width = 75;
            // 
            // _voltageDefensesTimeCol3
            // 
            this._voltageDefensesTimeCol3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._voltageDefensesTimeCol3.HeaderText = "����� ";
            this._voltageDefensesTimeCol3.Name = "_voltageDefensesTimeCol3";
            this._voltageDefensesTimeCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesTimeCol3.ToolTipText = "�������� ������� ������������";
            // 
            // _voltageDefensesGrid2
            // 
            this._voltageDefensesGrid2.AllowUserToAddRows = false;
            this._voltageDefensesGrid2.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid2.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid2.AllowUserToResizeRows = false;
            this._voltageDefensesGrid2.BackgroundColor = System.Drawing.SystemColors.Control;
            this._voltageDefensesGrid2.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid2.ColumnHeadersVisible = false;
            this._voltageDefensesGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol2,
            this._voltageDefensesModeCol2,
            this._voltageDefensesTypeCol2,
            this._voltageDefensesConstraintCol2,
            this._voltageDefensesBlockingNumberCol2,
            this._voltageDefensesTimeCol2});
            this._voltageDefensesGrid2.Location = new System.Drawing.Point(3, 220);
            this._voltageDefensesGrid2.Name = "_voltageDefensesGrid2";
            this._voltageDefensesGrid2.RowHeadersVisible = false;
            this._voltageDefensesGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid2.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this._voltageDefensesGrid2.RowTemplate.Height = 24;
            this._voltageDefensesGrid2.Size = new System.Drawing.Size(538, 99);
            this._voltageDefensesGrid2.TabIndex = 12;
            // 
            // _voltageDefensesNameCol2
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesNameCol2.DefaultCellStyle = dataGridViewCellStyle6;
            this._voltageDefensesNameCol2.HeaderText = "";
            this._voltageDefensesNameCol2.MinimumWidth = 50;
            this._voltageDefensesNameCol2.Name = "_voltageDefensesNameCol2";
            this._voltageDefensesNameCol2.ReadOnly = true;
            this._voltageDefensesNameCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol2.Width = 50;
            // 
            // _voltageDefensesModeCol2
            // 
            this._voltageDefensesModeCol2.HeaderText = "�����";
            this._voltageDefensesModeCol2.Name = "_voltageDefensesModeCol2";
            this._voltageDefensesModeCol2.Width = 120;
            // 
            // _voltageDefensesTypeCol2
            // 
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesTypeCol2.DefaultCellStyle = dataGridViewCellStyle7;
            this._voltageDefensesTypeCol2.HeaderText = "��� �������";
            this._voltageDefensesTypeCol2.Name = "_voltageDefensesTypeCol2";
            this._voltageDefensesTypeCol2.Width = 120;
            // 
            // _voltageDefensesConstraintCol2
            // 
            this._voltageDefensesConstraintCol2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._voltageDefensesConstraintCol2.HeaderText = "�������";
            this._voltageDefensesConstraintCol2.Name = "_voltageDefensesConstraintCol2";
            this._voltageDefensesConstraintCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesConstraintCol2.ToolTipText = "������� ������������";
            this._voltageDefensesConstraintCol2.Width = 60;
            // 
            // _voltageDefensesBlockingNumberCol2
            // 
            this._voltageDefensesBlockingNumberCol2.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol2.Name = "_voltageDefensesBlockingNumberCol2";
            this._voltageDefensesBlockingNumberCol2.Width = 75;
            // 
            // _voltageDefensesTimeCol2
            // 
            this._voltageDefensesTimeCol2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._voltageDefensesTimeCol2.HeaderText = "����� ";
            this._voltageDefensesTimeCol2.Name = "_voltageDefensesTimeCol2";
            this._voltageDefensesTimeCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesTimeCol2.ToolTipText = "�������� ������� ������������";
            // 
            // _voltageDefensesGrid1
            // 
            this._voltageDefensesGrid1.AllowUserToAddRows = false;
            this._voltageDefensesGrid1.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid1.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid1.AllowUserToResizeRows = false;
            this._voltageDefensesGrid1.BackgroundColor = System.Drawing.SystemColors.Control;
            this._voltageDefensesGrid1.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol1,
            this._voltageDefensesModeCol1,
            this._voltageDefensesTypeCol1,
            this._voltageDefensesConstraintCol1,
            this._voltageDefensesBlockingNumberCol1,
            this._voltageDefensesTimeCol1});
            this._voltageDefensesGrid1.Location = new System.Drawing.Point(2, 0);
            this._voltageDefensesGrid1.Name = "_voltageDefensesGrid1";
            this._voltageDefensesGrid1.RowHeadersVisible = false;
            this._voltageDefensesGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid1.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this._voltageDefensesGrid1.RowTemplate.Height = 24;
            this._voltageDefensesGrid1.Size = new System.Drawing.Size(538, 224);
            this._voltageDefensesGrid1.TabIndex = 2;
            // 
            // _voltageDefensesNameCol1
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesNameCol1.DefaultCellStyle = dataGridViewCellStyle9;
            this._voltageDefensesNameCol1.HeaderText = "";
            this._voltageDefensesNameCol1.MinimumWidth = 50;
            this._voltageDefensesNameCol1.Name = "_voltageDefensesNameCol1";
            this._voltageDefensesNameCol1.ReadOnly = true;
            this._voltageDefensesNameCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol1.Width = 50;
            // 
            // _voltageDefensesModeCol1
            // 
            this._voltageDefensesModeCol1.HeaderText = "�����";
            this._voltageDefensesModeCol1.Name = "_voltageDefensesModeCol1";
            this._voltageDefensesModeCol1.Width = 120;
            // 
            // _voltageDefensesTypeCol1
            // 
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesTypeCol1.DefaultCellStyle = dataGridViewCellStyle10;
            this._voltageDefensesTypeCol1.HeaderText = "��� �������";
            this._voltageDefensesTypeCol1.Name = "_voltageDefensesTypeCol1";
            this._voltageDefensesTypeCol1.Width = 120;
            // 
            // _voltageDefensesConstraintCol1
            // 
            this._voltageDefensesConstraintCol1.HeaderText = "�������";
            this._voltageDefensesConstraintCol1.Name = "_voltageDefensesConstraintCol1";
            this._voltageDefensesConstraintCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesConstraintCol1.ToolTipText = "������� ������������";
            this._voltageDefensesConstraintCol1.Width = 60;
            // 
            // _voltageDefensesBlockingNumberCol1
            // 
            this._voltageDefensesBlockingNumberCol1.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol1.Name = "_voltageDefensesBlockingNumberCol1";
            this._voltageDefensesBlockingNumberCol1.Width = 75;
            // 
            // _voltageDefensesTimeCol1
            // 
            this._voltageDefensesTimeCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._voltageDefensesTimeCol1.HeaderText = "�����";
            this._voltageDefensesTimeCol1.Name = "_voltageDefensesTimeCol1";
            this._voltageDefensesTimeCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesTimeCol1.ToolTipText = "�������� ������� ������������";
            // 
            // _frequencePage
            // 
            this._frequencePage.Controls.Add(this._frequenceDefensesGrid1);
            this._frequencePage.Controls.Add(this.label12);
            this._frequencePage.Controls.Add(this._frequenceDefensesGrid);
            this._frequencePage.Location = new System.Drawing.Point(4, 22);
            this._frequencePage.Name = "_frequencePage";
            this._frequencePage.Padding = new System.Windows.Forms.Padding(3);
            this._frequencePage.Size = new System.Drawing.Size(731, 373);
            this._frequencePage.TabIndex = 2;
            this._frequencePage.Text = "������ �� �������";
            this._frequencePage.UseVisualStyleBackColor = true;
            // 
            // _frequenceDefensesGrid1
            // 
            this._frequenceDefensesGrid1.AllowUserToAddRows = false;
            this._frequenceDefensesGrid1.AllowUserToDeleteRows = false;
            this._frequenceDefensesGrid1.AllowUserToResizeColumns = false;
            this._frequenceDefensesGrid1.AllowUserToResizeRows = false;
            this._frequenceDefensesGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._frequenceDefensesGrid1.BackgroundColor = System.Drawing.SystemColors.Control;
            this._frequenceDefensesGrid1.ColumnHeadersHeight = 30;
            this._frequenceDefensesGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._frequenceDefensesGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._frequenceDefensesName1,
            this._frequenceDefensesMode1,
            this._frequenceDefensesWorkConstraint1,
            this._frequenceDefensesBlockingNumber1,
            this._frequenceDefensesWorkTime1});
            this._frequenceDefensesGrid1.Location = new System.Drawing.Point(9, 6);
            this._frequenceDefensesGrid1.Name = "_frequenceDefensesGrid1";
            this._frequenceDefensesGrid1.RowHeadersVisible = false;
            this._frequenceDefensesGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._frequenceDefensesGrid1.RowTemplate.Height = 24;
            this._frequenceDefensesGrid1.Size = new System.Drawing.Size(383, 128);
            this._frequenceDefensesGrid1.TabIndex = 11;
            // 
            // _frequenceDefensesName1
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._frequenceDefensesName1.DefaultCellStyle = dataGridViewCellStyle12;
            this._frequenceDefensesName1.HeaderText = "";
            this._frequenceDefensesName1.MaxInputLength = 1;
            this._frequenceDefensesName1.Name = "_frequenceDefensesName1";
            this._frequenceDefensesName1.ReadOnly = true;
            this._frequenceDefensesName1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesName1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesName1.Width = 5;
            // 
            // _frequenceDefensesMode1
            // 
            this._frequenceDefensesMode1.HeaderText = "�����";
            this._frequenceDefensesMode1.Name = "_frequenceDefensesMode1";
            this._frequenceDefensesMode1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesMode1.Width = 48;
            // 
            // _frequenceDefensesWorkConstraint1
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesWorkConstraint1.DefaultCellStyle = dataGridViewCellStyle13;
            this._frequenceDefensesWorkConstraint1.HeaderText = "�������";
            this._frequenceDefensesWorkConstraint1.MaxInputLength = 8;
            this._frequenceDefensesWorkConstraint1.Name = "_frequenceDefensesWorkConstraint1";
            this._frequenceDefensesWorkConstraint1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesWorkConstraint1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesWorkConstraint1.Width = 55;
            // 
            // _frequenceDefensesBlockingNumber1
            // 
            this._frequenceDefensesBlockingNumber1.HeaderText = "����������";
            this._frequenceDefensesBlockingNumber1.Name = "_frequenceDefensesBlockingNumber1";
            this._frequenceDefensesBlockingNumber1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesBlockingNumber1.Width = 74;
            // 
            // _frequenceDefensesWorkTime1
            // 
            this._frequenceDefensesWorkTime1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesWorkTime1.DefaultCellStyle = dataGridViewCellStyle14;
            this._frequenceDefensesWorkTime1.HeaderText = "t����, ��/����.";
            this._frequenceDefensesWorkTime1.MaxInputLength = 8;
            this._frequenceDefensesWorkTime1.Name = "_frequenceDefensesWorkTime1";
            this._frequenceDefensesWorkTime1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesWorkTime1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(483, 150);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(113, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "������������ ����";
            // 
            // _frequenceDefensesGrid
            // 
            this._frequenceDefensesGrid.AllowUserToAddRows = false;
            this._frequenceDefensesGrid.AllowUserToDeleteRows = false;
            this._frequenceDefensesGrid.AllowUserToResizeColumns = false;
            this._frequenceDefensesGrid.AllowUserToResizeRows = false;
            this._frequenceDefensesGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._frequenceDefensesGrid.ColumnHeadersHeight = 30;
            this._frequenceDefensesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._frequenceDefensesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._frequenceDefensesName,
            this._frequenceDefensesMode,
            this._frequenceDefensesWorkConstraint,
            this._frequenceDefensesBlockingNumber,
            this._frequenceDefensesWorkTime,
            this._CAPV_ModeCol1,
            this._CAPV_FrequencyCol1,
            this._CAPV_BlockingCol1,
            this._CAPV_TimeCol1});
            this._frequenceDefensesGrid.Location = new System.Drawing.Point(9, 167);
            this._frequenceDefensesGrid.Name = "_frequenceDefensesGrid";
            this._frequenceDefensesGrid.RowHeadersVisible = false;
            this._frequenceDefensesGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._frequenceDefensesGrid.RowTemplate.Height = 24;
            this._frequenceDefensesGrid.Size = new System.Drawing.Size(707, 130);
            this._frequenceDefensesGrid.TabIndex = 9;
            // 
            // _frequenceDefensesName
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._frequenceDefensesName.DefaultCellStyle = dataGridViewCellStyle15;
            this._frequenceDefensesName.HeaderText = "";
            this._frequenceDefensesName.MaxInputLength = 1;
            this._frequenceDefensesName.Name = "_frequenceDefensesName";
            this._frequenceDefensesName.ReadOnly = true;
            this._frequenceDefensesName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesName.Width = 40;
            // 
            // _frequenceDefensesMode
            // 
            this._frequenceDefensesMode.HeaderText = "�����";
            this._frequenceDefensesMode.Name = "_frequenceDefensesMode";
            this._frequenceDefensesMode.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesMode.Width = 110;
            // 
            // _frequenceDefensesWorkConstraint
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesWorkConstraint.DefaultCellStyle = dataGridViewCellStyle16;
            this._frequenceDefensesWorkConstraint.HeaderText = "�������";
            this._frequenceDefensesWorkConstraint.MaxInputLength = 8;
            this._frequenceDefensesWorkConstraint.Name = "_frequenceDefensesWorkConstraint";
            this._frequenceDefensesWorkConstraint.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesWorkConstraint.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesWorkConstraint.Width = 55;
            // 
            // _frequenceDefensesBlockingNumber
            // 
            this._frequenceDefensesBlockingNumber.HeaderText = "����������";
            this._frequenceDefensesBlockingNumber.Name = "_frequenceDefensesBlockingNumber";
            this._frequenceDefensesBlockingNumber.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesBlockingNumber.Width = 75;
            // 
            // _frequenceDefensesWorkTime
            // 
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesWorkTime.DefaultCellStyle = dataGridViewCellStyle17;
            this._frequenceDefensesWorkTime.HeaderText = "t����, ��/����.";
            this._frequenceDefensesWorkTime.MaxInputLength = 8;
            this._frequenceDefensesWorkTime.Name = "_frequenceDefensesWorkTime";
            this._frequenceDefensesWorkTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesWorkTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesWorkTime.Width = 94;
            // 
            // _CAPV_ModeCol1
            // 
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._CAPV_ModeCol1.DefaultCellStyle = dataGridViewCellStyle18;
            this._CAPV_ModeCol1.HeaderText = "�����";
            this._CAPV_ModeCol1.Name = "_CAPV_ModeCol1";
            this._CAPV_ModeCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._CAPV_ModeCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._CAPV_ModeCol1.Width = 80;
            // 
            // _CAPV_FrequencyCol1
            // 
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._CAPV_FrequencyCol1.DefaultCellStyle = dataGridViewCellStyle19;
            this._CAPV_FrequencyCol1.HeaderText = "�������";
            this._CAPV_FrequencyCol1.Name = "_CAPV_FrequencyCol1";
            this._CAPV_FrequencyCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._CAPV_FrequencyCol1.Width = 74;
            // 
            // _CAPV_BlockingCol1
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._CAPV_BlockingCol1.DefaultCellStyle = dataGridViewCellStyle20;
            this._CAPV_BlockingCol1.HeaderText = "����������";
            this._CAPV_BlockingCol1.Name = "_CAPV_BlockingCol1";
            this._CAPV_BlockingCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._CAPV_BlockingCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._CAPV_BlockingCol1.Width = 75;
            // 
            // _CAPV_TimeCol1
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._CAPV_TimeCol1.DefaultCellStyle = dataGridViewCellStyle21;
            this._CAPV_TimeCol1.HeaderText = "t����, ��/����.";
            this._CAPV_TimeCol1.Name = "_CAPV_TimeCol1";
            this._CAPV_TimeCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _outputSignalsPage
            // 
            this._outputSignalsPage.Controls.Add(this.groupBox3);
            this._outputSignalsPage.Controls.Add(this.groupBox10);
            this._outputSignalsPage.Controls.Add(this.groupBox9);
            this._outputSignalsPage.Controls.Add(this.groupBox8);
            this._outputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._outputSignalsPage.Name = "_outputSignalsPage";
            this._outputSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._outputSignalsPage.Size = new System.Drawing.Size(899, 461);
            this._outputSignalsPage.TabIndex = 1;
            this._outputSignalsPage.Text = "�������� �������";
            this._outputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this._outputSignalsAlarmTimeBox);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this._outputSignalsAlarmReleCombo);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this._outputSignalsSignalizationTimeBox);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this._outputSignalsSignalizationReleCombo);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this._outputSignalsDispepairTimeBox);
            this.groupBox3.Controls.Add(this._dispepairCheckList);
            this.groupBox3.Location = new System.Drawing.Point(582, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(167, 450);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "���������� ����";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(112, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "���� �������������";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 388);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "������� ������";
            // 
            // _outputSignalsAlarmTimeBox
            // 
            this._outputSignalsAlarmTimeBox.Location = new System.Drawing.Point(20, 409);
            this._outputSignalsAlarmTimeBox.Name = "_outputSignalsAlarmTimeBox";
            this._outputSignalsAlarmTimeBox.Size = new System.Drawing.Size(100, 20);
            this._outputSignalsAlarmTimeBox.TabIndex = 26;
            this._outputSignalsAlarmTimeBox.Tag = "3000000";
            this._outputSignalsAlarmTimeBox.Text = "0";
            this._outputSignalsAlarmTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(35, 338);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "���� ������";
            // 
            // _outputSignalsAlarmReleCombo
            // 
            this._outputSignalsAlarmReleCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._outputSignalsAlarmReleCombo.FormattingEnabled = true;
            this._outputSignalsAlarmReleCombo.Location = new System.Drawing.Point(10, 359);
            this._outputSignalsAlarmReleCombo.Name = "_outputSignalsAlarmReleCombo";
            this._outputSignalsAlarmReleCombo.Size = new System.Drawing.Size(121, 21);
            this._outputSignalsAlarmReleCombo.TabIndex = 24;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 289);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(126, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "������� ������������";
            // 
            // _outputSignalsSignalizationTimeBox
            // 
            this._outputSignalsSignalizationTimeBox.Location = new System.Drawing.Point(20, 310);
            this._outputSignalsSignalizationTimeBox.Name = "_outputSignalsSignalizationTimeBox";
            this._outputSignalsSignalizationTimeBox.Size = new System.Drawing.Size(100, 20);
            this._outputSignalsSignalizationTimeBox.TabIndex = 22;
            this._outputSignalsSignalizationTimeBox.Tag = "3000000";
            this._outputSignalsSignalizationTimeBox.Text = "0";
            this._outputSignalsSignalizationTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 239);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "���� ������������";
            // 
            // _outputSignalsSignalizationReleCombo
            // 
            this._outputSignalsSignalizationReleCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._outputSignalsSignalizationReleCombo.FormattingEnabled = true;
            this._outputSignalsSignalizationReleCombo.Location = new System.Drawing.Point(10, 260);
            this._outputSignalsSignalizationReleCombo.Name = "_outputSignalsSignalizationReleCombo";
            this._outputSignalsSignalizationReleCombo.Size = new System.Drawing.Size(121, 21);
            this._outputSignalsSignalizationReleCombo.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 190);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "������, ��";
            // 
            // _outputSignalsDispepairTimeBox
            // 
            this._outputSignalsDispepairTimeBox.Location = new System.Drawing.Point(20, 211);
            this._outputSignalsDispepairTimeBox.Name = "_outputSignalsDispepairTimeBox";
            this._outputSignalsDispepairTimeBox.Size = new System.Drawing.Size(100, 20);
            this._outputSignalsDispepairTimeBox.TabIndex = 18;
            this._outputSignalsDispepairTimeBox.Tag = "3000000";
            this._outputSignalsDispepairTimeBox.Text = "0";
            this._outputSignalsDispepairTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _dispepairCheckList
            // 
            this._dispepairCheckList.CheckOnClick = true;
            this._dispepairCheckList.FormattingEnabled = true;
            this._dispepairCheckList.Items.AddRange(new object[] {
            "1.����������",
            "2. ������",
            "3. ������",
            "4. ���������� (U<5B)",
            "5. ������� (U<10B)",
            "6. ������",
            "7. ������",
            "8. ������� ������. ��"});
            this._dispepairCheckList.Location = new System.Drawing.Point(4, 47);
            this._dispepairCheckList.Name = "_dispepairCheckList";
            this._dispepairCheckList.Size = new System.Drawing.Size(163, 94);
            this._dispepairCheckList.TabIndex = 5;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._outputLogicCheckList);
            this.groupBox10.Controls.Add(this._outputLogicAcceptBut);
            this.groupBox10.Controls.Add(this._outputLogicCombo);
            this.groupBox10.Location = new System.Drawing.Point(438, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(141, 450);
            this.groupBox10.TabIndex = 5;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "���������� �������";
            // 
            // _outputLogicCheckList
            // 
            this._outputLogicCheckList.CheckOnClick = true;
            this._outputLogicCheckList.FormattingEnabled = true;
            this._outputLogicCheckList.Location = new System.Drawing.Point(3, 52);
            this._outputLogicCheckList.Name = "_outputLogicCheckList";
            this._outputLogicCheckList.ScrollAlwaysVisible = true;
            this._outputLogicCheckList.Size = new System.Drawing.Size(135, 334);
            this._outputLogicCheckList.TabIndex = 4;
            // 
            // _outputLogicAcceptBut
            // 
            this._outputLogicAcceptBut.Location = new System.Drawing.Point(42, 409);
            this._outputLogicAcceptBut.Name = "_outputLogicAcceptBut";
            this._outputLogicAcceptBut.Size = new System.Drawing.Size(59, 23);
            this._outputLogicAcceptBut.TabIndex = 3;
            this._outputLogicAcceptBut.Text = "�������";
            this._outputLogicAcceptBut.UseVisualStyleBackColor = true;
            this._outputLogicAcceptBut.Click += new System.EventHandler(this._outputLogicAcceptBut_Click);
            // 
            // _outputLogicCombo
            // 
            this._outputLogicCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._outputLogicCombo.FormattingEnabled = true;
            this._outputLogicCombo.Items.AddRange(new object[] {
            "��� 1",
            "��� 2 ",
            "��� 3 ",
            "��� 4 ",
            "��� 5",
            "��� 6",
            "��� 7 ",
            "��� 8"});
            this._outputLogicCombo.Location = new System.Drawing.Point(37, 16);
            this._outputLogicCombo.Name = "_outputLogicCombo";
            this._outputLogicCombo.Size = new System.Drawing.Size(66, 21);
            this._outputLogicCombo.TabIndex = 1;
            this._outputLogicCombo.SelectedIndexChanged += new System.EventHandler(this._outputLogicCombo_SelectedIndexChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._outputIndicatorsGrid);
            this.groupBox9.Location = new System.Drawing.Point(11, 287);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(424, 169);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "����������";
            // 
            // _outputIndicatorsGrid
            // 
            this._outputIndicatorsGrid.AllowUserToAddRows = false;
            this._outputIndicatorsGrid.AllowUserToDeleteRows = false;
            this._outputIndicatorsGrid.AllowUserToResizeColumns = false;
            this._outputIndicatorsGrid.AllowUserToResizeRows = false;
            this._outputIndicatorsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._outputIndicatorsGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._outputIndicatorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputIndicatorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._outIndNumberCol,
            this._outIndTypeCol,
            this._outIndSignalCol,
            this._outIndCol,
            this._outIndJS_Col,
            this._outIndJA_Col});
            this._outputIndicatorsGrid.Location = new System.Drawing.Point(6, 14);
            this._outputIndicatorsGrid.Name = "_outputIndicatorsGrid";
            this._outputIndicatorsGrid.RowHeadersVisible = false;
            this._outputIndicatorsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputIndicatorsGrid.RowsDefaultCellStyle = dataGridViewCellStyle22;
            this._outputIndicatorsGrid.RowTemplate.Height = 24;
            this._outputIndicatorsGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._outputIndicatorsGrid.ShowCellErrors = false;
            this._outputIndicatorsGrid.ShowRowErrors = false;
            this._outputIndicatorsGrid.Size = new System.Drawing.Size(412, 145);
            this._outputIndicatorsGrid.TabIndex = 0;
            // 
            // _outIndNumberCol
            // 
            this._outIndNumberCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._outIndNumberCol.HeaderText = "�";
            this._outIndNumberCol.Name = "_outIndNumberCol";
            this._outIndNumberCol.ReadOnly = true;
            this._outIndNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndNumberCol.Width = 24;
            // 
            // _outIndTypeCol
            // 
            this._outIndTypeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._outIndTypeCol.HeaderText = "���";
            this._outIndTypeCol.Items.AddRange(new object[] {
            "�����������",
            "�������",
            "XXXXX"});
            this._outIndTypeCol.Name = "_outIndTypeCol";
            // 
            // _outIndSignalCol
            // 
            this._outIndSignalCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._outIndSignalCol.HeaderText = "������";
            this._outIndSignalCol.Name = "_outIndSignalCol";
            // 
            // _outIndCol
            // 
            this._outIndCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._outIndCol.HeaderText = "���.";
            this._outIndCol.Name = "_outIndCol";
            this._outIndCol.Width = 36;
            // 
            // _outIndJS_Col
            // 
            this._outIndJS_Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._outIndJS_Col.HeaderText = "��";
            this._outIndJS_Col.Name = "_outIndJS_Col";
            this._outIndJS_Col.Width = 31;
            // 
            // _outIndJA_Col
            // 
            this._outIndJA_Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._outIndJA_Col.HeaderText = "��";
            this._outIndJA_Col.Name = "_outIndJA_Col";
            this._outIndJA_Col.Width = 31;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._outputReleGrid);
            this.groupBox8.Location = new System.Drawing.Point(11, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(424, 279);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "�������� ����";
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this._releSignalCol,
            this._releImpulseCol});
            this._outputReleGrid.Location = new System.Drawing.Point(6, 13);
            this._outputReleGrid.Name = "_outputReleGrid";
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle23;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(413, 260);
            this._outputReleGrid.TabIndex = 0;
            // 
            // _releNumberCol
            // 
            this._releNumberCol.HeaderText = "�";
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 20;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releTypeCol.HeaderText = "���";
            this._releTypeCol.Items.AddRange(new object[] {
            "�����������",
            "�������",
            "XXXXX"});
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 120;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.HeaderText = "������";
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 140;
            // 
            // _releImpulseCol
            // 
            this._releImpulseCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._releImpulseCol.HeaderText = "������, ��";
            this._releImpulseCol.Name = "_releImpulseCol";
            this._releImpulseCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _inSignalsPage
            // 
            this._inSignalsPage.Controls.Add(this.groupBox1);
            this._inSignalsPage.Controls.Add(this.groupBox5);
            this._inSignalsPage.Controls.Add(this.groupBox2);
            this._inSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._inSignalsPage.Name = "_inSignalsPage";
            this._inSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._inSignalsPage.Size = new System.Drawing.Size(899, 461);
            this._inSignalsPage.TabIndex = 0;
            this._inSignalsPage.Text = "������� �������";
            this._inSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._inputSignalsTN_DispepairCombo);
            this.groupBox1.Controls.Add(this._inputSignalsIndicationResetCombo);
            this.groupBox1.Controls.Add(this._inputSignalsConstraintResetCombo);
            this.groupBox1.Location = new System.Drawing.Point(13, 85);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(304, 100);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "�������������� �������";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "������������� ��";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "����� ���������";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(199, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "������������ �� ��������� �������";
            // 
            // _inputSignalsTN_DispepairCombo
            // 
            this._inputSignalsTN_DispepairCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inputSignalsTN_DispepairCombo.FormattingEnabled = true;
            this._inputSignalsTN_DispepairCombo.Location = new System.Drawing.Point(209, 68);
            this._inputSignalsTN_DispepairCombo.Name = "_inputSignalsTN_DispepairCombo";
            this._inputSignalsTN_DispepairCombo.Size = new System.Drawing.Size(84, 21);
            this._inputSignalsTN_DispepairCombo.TabIndex = 4;
            // 
            // _inputSignalsIndicationResetCombo
            // 
            this._inputSignalsIndicationResetCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inputSignalsIndicationResetCombo.FormattingEnabled = true;
            this._inputSignalsIndicationResetCombo.Location = new System.Drawing.Point(209, 41);
            this._inputSignalsIndicationResetCombo.Name = "_inputSignalsIndicationResetCombo";
            this._inputSignalsIndicationResetCombo.Size = new System.Drawing.Size(84, 21);
            this._inputSignalsIndicationResetCombo.TabIndex = 3;
            // 
            // _inputSignalsConstraintResetCombo
            // 
            this._inputSignalsConstraintResetCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inputSignalsConstraintResetCombo.FormattingEnabled = true;
            this._inputSignalsConstraintResetCombo.Location = new System.Drawing.Point(209, 14);
            this._inputSignalsConstraintResetCombo.Name = "_inputSignalsConstraintResetCombo";
            this._inputSignalsConstraintResetCombo.Size = new System.Drawing.Size(84, 21);
            this._inputSignalsConstraintResetCombo.TabIndex = 2;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._applyLogicSignalsBut);
            this.groupBox5.Controls.Add(this._logicChannelsCombo);
            this.groupBox5.Controls.Add(this._logicSignalsDataGrid);
            this.groupBox5.Location = new System.Drawing.Point(323, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(151, 281);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "���������� �������";
            // 
            // _applyLogicSignalsBut
            // 
            this._applyLogicSignalsBut.Location = new System.Drawing.Point(43, 252);
            this._applyLogicSignalsBut.Name = "_applyLogicSignalsBut";
            this._applyLogicSignalsBut.Size = new System.Drawing.Size(64, 23);
            this._applyLogicSignalsBut.TabIndex = 3;
            this._applyLogicSignalsBut.Text = "�������";
            this._applyLogicSignalsBut.UseVisualStyleBackColor = true;
            this._applyLogicSignalsBut.Click += new System.EventHandler(this._applyLogicSignalsBut_Click);
            // 
            // _logicChannelsCombo
            // 
            this._logicChannelsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._logicChannelsCombo.FormattingEnabled = true;
            this._logicChannelsCombo.Items.AddRange(new object[] {
            "1 �",
            "2 �",
            "3 �",
            "4 �",
            "5 ���",
            "6 ���",
            "7 ���",
            "8 ���"});
            this._logicChannelsCombo.Location = new System.Drawing.Point(43, 18);
            this._logicChannelsCombo.Name = "_logicChannelsCombo";
            this._logicChannelsCombo.Size = new System.Drawing.Size(64, 21);
            this._logicChannelsCombo.TabIndex = 1;
            this._logicChannelsCombo.SelectedIndexChanged += new System.EventHandler(this._logicChannelsCombo_SelectedIndexChanged);
            // 
            // _logicSignalsDataGrid
            // 
            this._logicSignalsDataGrid.AllowUserToAddRows = false;
            this._logicSignalsDataGrid.AllowUserToDeleteRows = false;
            this._logicSignalsDataGrid.AllowUserToResizeColumns = false;
            this._logicSignalsDataGrid.AllowUserToResizeRows = false;
            this._logicSignalsDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._logicSignalsDataGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._logicSignalsDataGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._logicSignalsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._logicSignalsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._diskretChannelCol,
            this._diskretValueCol});
            this._logicSignalsDataGrid.Location = new System.Drawing.Point(11, 45);
            this._logicSignalsDataGrid.MultiSelect = false;
            this._logicSignalsDataGrid.Name = "_logicSignalsDataGrid";
            this._logicSignalsDataGrid.RowHeadersVisible = false;
            this._logicSignalsDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._logicSignalsDataGrid.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this._logicSignalsDataGrid.RowTemplate.Height = 20;
            this._logicSignalsDataGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._logicSignalsDataGrid.ShowCellErrors = false;
            this._logicSignalsDataGrid.ShowCellToolTips = false;
            this._logicSignalsDataGrid.ShowEditingIcon = false;
            this._logicSignalsDataGrid.ShowRowErrors = false;
            this._logicSignalsDataGrid.Size = new System.Drawing.Size(132, 201);
            this._logicSignalsDataGrid.TabIndex = 0;
            // 
            // _diskretChannelCol
            // 
            this._diskretChannelCol.HeaderText = "�";
            this._diskretChannelCol.Name = "_diskretChannelCol";
            this._diskretChannelCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._diskretChannelCol.Width = 24;
            // 
            // _diskretValueCol
            // 
            this._diskretValueCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._diskretValueCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._diskretValueCol.HeaderText = "��������";
            this._diskretValueCol.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this._diskretValueCol.Name = "_diskretValueCol";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this._TNNP_Box);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this._TN_Box);
            this.groupBox2.Location = new System.Drawing.Point(13, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(203, 73);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "������������� �����";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "�������� ����";
            // 
            // _TNNP_Box
            // 
            this._TNNP_Box.Location = new System.Drawing.Point(125, 40);
            this._TNNP_Box.Name = "_TNNP_Box";
            this._TNNP_Box.Size = new System.Drawing.Size(70, 20);
            this._TNNP_Box.TabIndex = 5;
            this._TNNP_Box.Tag = "128000";
            this._TNNP_Box.Text = "0";
            this._TNNP_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "��������� ��";
            // 
            // _TN_Box
            // 
            this._TN_Box.Location = new System.Drawing.Point(125, 20);
            this._TN_Box.Name = "_TN_Box";
            this._TN_Box.Size = new System.Drawing.Size(70, 20);
            this._TN_Box.TabIndex = 2;
            this._TN_Box.Tag = "128000";
            this._TN_Box.Text = "0";
            this._TN_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _tabControl
            // 
            this._tabControl.ContextMenuStrip = this.contextMenu;
            this._tabControl.Controls.Add(this._inSignalsPage);
            this._tabControl.Controls.Add(this._outputSignalsPage);
            this._tabControl.Controls.Add(this._defensesPage);
            this._tabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._tabControl.Location = new System.Drawing.Point(0, 0);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(907, 487);
            this._tabControl.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(484, 493);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 23);
            this.button1.TabIndex = 35;
            this.button1.Text = "��������� ���. ���.";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 545);
            this.Controls.Add(this.button1);
            this.Controls.Add(this._saveToXmlButton);
            this.Controls.Add(this._statusStrip);
            this.Controls.Add(this._saveConfigBut);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this._writeConfigBut);
            this.Controls.Add(this._readConfigBut);
            this.Controls.Add(this._tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "ConfigurationForm";
            this.Text = "ConfigurationForm";
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Mr600ConfigurationForm_KeyUp);
            this.contextMenu.ResumeLayout(false);
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this._defensesPage.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this._externalDefensePage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).EndInit();
            this._voltageDefensesPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid1)).EndInit();
            this._frequencePage.ResumeLayout(false);
            this._frequencePage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._frequenceDefensesGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._frequenceDefensesGrid)).EndInit();
            this._outputSignalsPage.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).EndInit();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this._inSignalsPage.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this._tabControl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.StatusStrip _statusStrip;
        private System.Windows.Forms.ToolStripProgressBar _exchangeProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel _processLabel;
        private System.Windows.Forms.Button _saveToXmlButton;
        private System.Windows.Forms.SaveFileDialog _saveXmlDialog;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToHtmlItem;
        private System.Windows.Forms.TabPage _defensesPage;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.RadioButton _defenseMainConstraintRadio;
        private System.Windows.Forms.RadioButton _defenseReserveConstraintRadio;
        private System.Windows.Forms.Button _ChangeGroupButton2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage _externalDefensePage;
        private System.Windows.Forms.DataGridView _externalDefenseGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extDefNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefWorkingCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefBlockingCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefWorkingTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefReturnCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefReturnNumberCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefReturnTimeCol;
        private System.Windows.Forms.TabPage _voltageDefensesPage;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesTypeCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesConstraintCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesTimeCol3;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesTypeCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesConstraintCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesTimeCol2;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesTypeCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesConstraintCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesTimeCol1;
        private System.Windows.Forms.TabPage _frequencePage;
        private System.Windows.Forms.DataGridView _frequenceDefensesGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesName1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesMode1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesWorkConstraint1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesBlockingNumber1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesWorkTime1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView _frequenceDefensesGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesName;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesMode;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesWorkConstraint;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesBlockingNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesWorkTime;
        private System.Windows.Forms.DataGridViewComboBoxColumn _CAPV_ModeCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _CAPV_FrequencyCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _CAPV_BlockingCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _CAPV_TimeCol1;
        private System.Windows.Forms.TabPage _outputSignalsPage;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox _outputSignalsAlarmTimeBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox _outputSignalsAlarmReleCombo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox _outputSignalsSignalizationTimeBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox _outputSignalsSignalizationReleCombo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox _outputSignalsDispepairTimeBox;
        private System.Windows.Forms.CheckedListBox _dispepairCheckList;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.CheckedListBox _outputLogicCheckList;
        private System.Windows.Forms.Button _outputLogicAcceptBut;
        private System.Windows.Forms.ComboBox _outputLogicCombo;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView _outputIndicatorsGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignalCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndJS_Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndJA_Col;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView _outputReleGrid;
        private System.Windows.Forms.TabPage _inSignalsPage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox _inputSignalsTN_DispepairCombo;
        private System.Windows.Forms.ComboBox _inputSignalsIndicationResetCombo;
        private System.Windows.Forms.ComboBox _inputSignalsConstraintResetCombo;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button _applyLogicSignalsBut;
        private System.Windows.Forms.ComboBox _logicChannelsCombo;
        private System.Windows.Forms.DataGridView _logicSignalsDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _diskretChannelCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _diskretValueCol;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox _TNNP_Box;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox _TN_Box;
        private System.Windows.Forms.TabControl _tabControl;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releImpulseCol;
    }
}