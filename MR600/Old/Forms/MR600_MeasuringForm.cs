using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.MR600.Old.Forms
{
    public partial class MeasuringForm : Form , IFormView
    {
        private MR600 _device;
        private LedControl[] _manageLeds;
        private LedControl[] _indicatorLeds;
        private LedControl[] _inputLeds;
        private LedControl[] _outputLeds;
        private LedControl[] _releLeds;
        private LedControl[] _limitLeds;
        private LedControl[] _faultStateLeds;
        private LedControl[] _faultSignalsLeds;
        private LedControl[] _externalSignalsLeds;

        public MeasuringForm()
        {
            this.InitializeComponent();
        }

        public MeasuringForm(MR600 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopRead;
            
            this._device.DiscretDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.OnDiagnosticLoadOk);
            this._device.DiscretDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.OnDiagnosticLoadFail);

            this._device.Transformator.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._device.AnalogDataBase.LoadStruct);
            this._device.Transformator.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.OnAnalogSignalsLoadFail);

            this._device.AnalogDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.OnAnalogSignalsLoadOk);
            this._device.AnalogDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.OnAnalogSignalsLoadFail);

            this._device.DateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.OnDateTimeLoadOk);
            //this._device.DateTimeOld.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            //{
            //    MessageBox.Show("�������� �������");
            //});
            
            this.Initialize();
        }
         
        private void Initialize()
        {
            this._manageLeds = new LedControl[]
            {
                this._manageLed1, this._manageLed2, this._manageLed3, this._manageLed4, this._manageLed5,
                this._manageLed6, this._manageLed7, this._manageLed8, this._manageLed9, this._manageLed10
            };
            this._externalSignalsLeds = new LedControl[]
            {
                this._extDefenseLed1, this._extDefenseLed2, this._extDefenseLed3, this._extDefenseLed4,
                this._extDefenseLed5, this._extDefenseLed6, this._extDefenseLed7, this._extDefenseLed8
            };
            this._indicatorLeds = new LedControl[]
            {
                this._indLed6, this._indLed5, this._indLed4, this._indLed3, this._indLed2, this._indLed1
                /*,_indLed7,_indLed8*/
            };
            this._inputLeds = new LedControl[]
            {
                this._inD_led1, this._inD_led2, this._inD_led3, this._inD_led4, this._inD_led5, this._inD_led6,
                this._inD_led7, this._inD_led8, this._inL_led1, this._inL_led2, this._inL_led3, this._inL_led4,
                this._inL_led5, this._inL_led6, this._inL_led7, this._inL_led8
            };
            this._outputLeds = new LedControl[]
            {
                this._outLed1, this._outLed2, this._outLed3, this._outLed4, this._outLed5, this._outLed6, this._outLed7,
                this._outLed8
            };
            this._releLeds = new LedControl[]
            {
                this._releLed1, this._releLed2, this._releLed3, this._releLed4, this._releLed5, this._releLed6,
                this._releLed7, this._releLed8, this._releLed9, this._releLed10, this._releLed11, this._releLed12,
                this._releLed13, this._releLed14, this._releLed15, this._releLed16
            };
            this._limitLeds = new LedControl[]
            {
                this._UmaxLed1, this._UmaxLed2, this._UmaxLed3, this._UmaxLed4, this._UmaxLed5, this._UmaxLed6,
                this._UmaxLed7, this._UmaxLed8, this._UminLed1, this._UminLed2, this._UminLed3, this._UminLed4,
                this._UminLed5, this._UminLed6, this._UminLed7, this._UminLed8, this._U0maxLed1, this._U0maxLed2,
                this._U0maxLed3, this._U0maxLed4, this._U0maxLed5, this._U0maxLed6, this._U0maxLed7, this._U0maxLed8,
                this._U12Led1, this._U12Led2, this._U12Led3, this._U12Led4, this._U12Led5, this._U12Led6, this._U12Led7,
                this._U12Led8, this._FmaxLed1, this._FmaxLed2, this._FmaxLed3, this._FmaxLed4, this._FmaxLed5,
                this._FmaxLed6, this._FmaxLed7, this._FmaxLed8, this._FminLed1, this._FminLed2, this._FminLed3,
                this._FminLed4, this._FminLed5, this._FminLed6, this._FminLed7, this._FminLed8
            };
            this._faultStateLeds = new LedControl[]
            {
                this._faultStateLed1, this._faultStateLed2, this._faultStateLed3, this._faultStateLed4,
                this._faultStateLed5, this._faultStateLed6, this._faultStateLed7, this._faultStateLed8
            };
            this._faultSignalsLeds = new LedControl[]
            {
                this._faultSignalLed1, this._faultSignalLed2, this._faultSignalLed3, this._faultSignalLed4,
                this._faultSignalLed5, this._faultSignalLed6, this._faultSignalLed7, this._faultSignalLed8,
                this._faultSignalLed9, this._faultSignalLed10, this._faultSignalLed11, this._faultSignalLed12,
                this._faultSignalLed13, this._faultSignalLed14, this._faultSignalLed15, this._faultSignalLed16
            };
        }

        private void MeasuringForm_Activated(object sender, EventArgs e)
        {
            this._device.MakeDiagnosticQuick();
            this._device.MakeDateTimeQuick();
            this._device.MakeAnalogSignalsQuick();
        }

        private void MeasuringForm_Deactivate(object sender, EventArgs e)
        {
            this._device.MakeDiagnosticSlow();
            this._device.MakeDateTimeSlow();
            this._device.MakeAnalogSignalsSlow();
        }

        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.DiscretDataBase.RemoveStructQueries();
            this._device.DateTime.RemoveStructQueries();
            this._device.Transformator.RemoveStructQueries();

            this._device.DiscretDataBase.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this, this.OnDiagnosticLoadOk);
            this._device.DiscretDataBase.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this, this.OnDiagnosticLoadFail);

            this._device.Transformator.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this, this._device.AnalogDataBase.LoadStruct);
            this._device.Transformator.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this, this.OnAnalogSignalsLoadFail);

            this._device.AnalogDataBase.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this, this.OnAnalogSignalsLoadOk);
            this._device.AnalogDataBase.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this, this.OnAnalogSignalsLoadFail);

            this._device.DateTimeOld.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this, this.OnDateTimeLoadOk);
            this._device.ConnectionModeChanged -= this.StartStopRead;
        }

        private void StartStopRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                if (Common.VersionConverter(this._device.DeviceVersion) < 2.06)
                {
                    this._device.DiscretDataBase.LoadBitsCycle();
                }
                else
                    this._device.DiscretDataBase.LoadStructCycle();
                this._device.DateTime.LoadStructCycle();
                this._device.Transformator.LoadStructCycle();
            }
            else
            {
                this._device.DiscretDataBase.RemoveStructQueries();
                this._device.DateTime.RemoveStructQueries();
                this._device.Transformator.RemoveStructQueries();
                this.OnAnalogSignalsLoadFail();
                this.OnDiagnosticLoadFail();
            }
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopRead();
        }

        private void OnAnalogSignalsLoadFail()
        {
            this._UnBox.Text = this._UaBox.Text = this._UbBox.Text = this._UcBox.Text = 
                this._UabBox.Text = this._UbcBox.Text = this._UcaBox.Text = this._U0Box.Text =
                this._U1Box.Text = this._U2Box.Text = this._F_Box.Text = string.Empty;
        }

        private void OnAnalogSignalsLoadOk()
        {
            this._device.AnalogDataBase.Value.Transformator = this._device.Transformator.Value;
            this._UnBox.Text = this._device.AnalogDataBase.Value.Un;
            this._UaBox.Text = this._device.AnalogDataBase.Value.Ua;
            this._UbBox.Text = this._device.AnalogDataBase.Value.Ub;
            this._UcBox.Text = this._device.AnalogDataBase.Value.Uc;
            this._UabBox.Text = this._device.AnalogDataBase.Value.Uab;
            this._UbcBox.Text = this._device.AnalogDataBase.Value.Ubc;
            this._UcaBox.Text = this._device.AnalogDataBase.Value.Uca;
            this._U0Box.Text = this._device.AnalogDataBase.Value.U0;
            this._U1Box.Text = this._device.AnalogDataBase.Value.U1;
            this._U2Box.Text = this._device.AnalogDataBase.Value.U2;
            this._F_Box.Text = this._device.AnalogDataBase.Value.F;
        }
        
        private void OnDateTimeLoadOk()
        {
            this._dateTimeControlOld.DateTime = this._device.DateTime.Value;
        }
        
        private void OnDiagnosticLoadFail()
        {
            LedManager.TurnOffLeds(this._manageLeds);
            LedManager.TurnOffLeds(this._externalSignalsLeds);
            LedManager.TurnOffLeds(this._indicatorLeds);
            LedManager.TurnOffLeds(this._inputLeds);
            LedManager.TurnOffLeds(this._outputLeds);
            LedManager.TurnOffLeds(this._releLeds);
            LedManager.TurnOffLeds(this._limitLeds);
            LedManager.TurnOffLeds(this._faultSignalsLeds);
            LedManager.TurnOffLeds(this._faultStateLeds);
        }
        
        private void OnDiagnosticLoadOk()
        {
            LedManager.SetLeds(this._manageLeds, this._device.DiscretDataBase.Value.ManageSignals);
            LedManager.SetLeds(this._externalSignalsLeds, this._device.DiscretDataBase.Value.ExternalSignals);
            LedManager.SetLeds(this._indicatorLeds, this._device.DiscretDataBase.Value.Indicators);
            LedManager.SetLeds(this._inputLeds, this._device.DiscretDataBase.Value.InputSignals);
            LedManager.SetLeds(this._outputLeds, this._device.DiscretDataBase.Value.OutputSignals);
            LedManager.SetLeds(this._releLeds, this._device.DiscretDataBase.Value.Rele);
            LedManager.SetLeds(this._limitLeds, this._device.DiscretDataBase.Value.LimitSignals);
            LedManager.SetLeds(this._faultSignalsLeds, this._device.DiscretDataBase.Value.FaultSignals);
            LedManager.SetLeds(this._faultStateLeds, this._device.DiscretDataBase.Value.FaultState);
        }
        
       
        private void ConfirmSDTU(ushort address,string msg)
        {
            if (DialogResult.Yes == MessageBox.Show(msg + " ?", "������-��600", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                this._device.SetBit(this._device.DeviceNumber, address, true, msg + this._device.DeviceNumber, this);
            }
        }
             
        private void _resetFaultBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1805, "�������� �������������");
        }

        private void _resetJS_But_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1806, "�������� ������ �������");
        }

        private void _resetJA_But_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1807, "�������� ������ ������");
        }
        
        private void _indicationResetButton_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1802, "�������� ���������");
        }

        private void _constraintToggleButton_Click(object sender, EventArgs e)
        {
            string msg;
            bool constraintGroup;
            switch (this._manageLed2.State)
            {
                case LedState.NoSignaled:
                    constraintGroup = true;
                    msg = "����������� �� �������� ������";
                    break;
                case LedState.Signaled:
                    constraintGroup = false;
                    msg =  "����������� �� ��������� ������";
                    break;
                default:
                    return;
            }
            if (DialogResult.Yes == MessageBox.Show(msg + " ?", "������-��600", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                this._device.SelectConstraintGroup(constraintGroup);
            }
        }

        private void _dateTimeControlOld_TimeChanged()
        {
            DateTimeStructOld buffer = new DateTimeStructOld();
            buffer.SetDateTimeNow(this._dateTimeControlOld.DateTime.GetDateTime());
            this._device.DateTimeOld.Value = buffer;
            this._device.DateTimeOld.SaveStruct();
        }


        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR600); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(MeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "���������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}