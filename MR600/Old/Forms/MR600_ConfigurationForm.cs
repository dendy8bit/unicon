using System;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Forms;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR600.Old.HelpClasses;

namespace BEMN.MR600.Old.Forms
{
    public partial class ConfigurationForm : Form, IFormView
    {
        private MR600 _device;
        private bool _validatingOk = true;
        private bool _connectingErrors;
        private RadioButtonSelector _groupSelector;
        private const string MR600_BASE_CONFIG_PATH = "\\MR600\\MR600_BaseConfig_v{0}.bin";
        private const string BASE_CONFIG_LOAD_SUCCESSFULLY = "������� ������� ������� ���������";

        public ConfigurationForm()
        {
            InitializeComponent();
        }

        public ConfigurationForm(MR600 device)
        {
            InitializeComponent();
            _device = device;
            _device.InputSignalsLoadOK += new Handler(_device_InputSignalsLoadOK);
            _device.InputSignalsLoadFail += (_device_InputSignalsLoadFail);
            _device.OutputSignalsLoadOK += (_device_OutputSignalsLoadOK);
            _device.OutputSignalsLoadFail += (_device_OutputSignalsLoadFail);
            _device.ExternalDefensesLoadOK += (_device_ExternalDefensesLoadOK);
            _device.ExternalDefensesLoadFail += (_device_ExternalDefensesLoadFail);
            _device.VoltageDefensesLoadOk += (_device_VoltageDefensesLoadOk);
            _device.VoltageDefensesLoadFail += (_device_VoltageDefensesLoadFail);
            _device.FrequenceDefensesLoadOk += (_device_FrequenceDefensesLoadOk);
            _device.FrequenceDefensesLoadFail += (_device_FrequenceDefensesLoadFail);
            this._groupSelector = new RadioButtonSelector(this._defenseMainConstraintRadio,
                _defenseReserveConstraintRadio, this._ChangeGroupButton2);
            this._groupSelector.NeedCopyGroup += this._groupSelector_NeedCopyGroup;
            SubscriptOnSaveHandlers();
            if (this._device.DeviceVersion.Contains("2.05"))
            {
                this._dispepairCheckList.Items[5] = "6. ����������� U";
            }
            else
            {
                this._dispepairCheckList.Items[5] = "6. ������";
            }

            PrepareInputSignals();
            PrepareOutputSignals();
            PrepareExternalDefenses();
            PrepareVoltageDefenses();
            PrepareFrequenceDefenses();
        }

        private void _groupSelector_NeedCopyGroup()
        {
            if (this._groupSelector.SelectedGroup == 0)
            {
                this.WriteExternalDefenses(this._device.ExternalDefensesReserve);
                this.WriteVoltageDefenses(this._device.VoltageDefensesReserve);
                this.WriteFrequenceDefenses(this._device.FrequenceDefensesReserve);
            }
            else
            {
                this.WriteExternalDefenses(this._device.ExternalDefensesMain);
                this.WriteVoltageDefenses(this._device.VoltageDefensesMain);
                this.WriteFrequenceDefenses(this._device.FrequenceDefensesMain);

            }
        }

        #region ����������� UI

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            ReadFromFile();
        }

        private void ReadFromFile()
        {
            _exchangeProgressBar.Value = 0;
            if (DialogResult.OK == _openConfigurationDlg.ShowDialog())
            {
                try
                {
                    _device.Deserialize(_openConfigurationDlg.FileName);
                }
                catch (System.IO.FileLoadException exc)
                {
                    _processLabel.Text = "���� " + System.IO.Path.GetFileName(exc.FileName) +
                                         " �� �������� ������ ������� ��600 ��� ���������";
                    return;
                }
                if (_defenseMainConstraintRadio.Checked)
                {
                    ShowVoltageDefenses(_device.VoltageDefensesMain);
                }
                else
                {
                    ShowVoltageDefenses(_device.VoltageDefensesReserve);
                }
                if (_defenseMainConstraintRadio.Checked)
                {
                    ShowFrequenceDefenses(_device.FrequenceDefensesMain);
                }
                else
                {
                    ShowFrequenceDefenses(_device.FrequenceDefensesReserve);
                }
                ReadInputSignals();
                OnExternalDefensesLoadOk();
                ReadOutputSignals();

                _exchangeProgressBar.Value = 0;
                _processLabel.Text = "���� " + _openConfigurationDlg.FileName + " ��������";
            }
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            SaveinFile();
        }

        private void SaveinFile()
        {
            ValidateAll();

            if (_validatingOk)
            {
                _saveConfigurationDlg.FileName =
                    string.Format("��600_�������_������ {0:F1}.bin", this._device.DeviceVersion);
                if (DialogResult.OK == _saveConfigurationDlg.ShowDialog())
                {
                    _exchangeProgressBar.Value = 0;
                    _device.Serialize(_saveConfigurationDlg.FileName);
                    _processLabel.Text = "���� " + _openConfigurationDlg.FileName + " ��������";
                }
            }
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;

            _connectingErrors = false;
            _exchangeProgressBar.Value = 0;
            ValidateAll();
            if (_validatingOk)
            {
                if (DialogResult.Yes == MessageBox.Show("�������� ������������ ��600 �" + _device.DeviceNumber + " ?",
                        "��������", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                {
                    _processLabel.Text = "���� ������";
                    _device.SaveOutputSignals();
                    _device.SaveInputSignals();
                    _device.SaveExternalDefenses();
                    _device.SaveVoltageDefenses();
                    _device.SaveFrequenceDefenses();
                    _device.ConfirmConstraint();
                }
            }
        }

        private void ValidateAll()
        {
            _validatingOk = true;
            //�������� MaskedTextBox
            ValidateInputSignals();
            ValidateOutputSignals();
            //�������� DataGrid
            if (_validatingOk)
            {
                _validatingOk = _validatingOk && WriteOutputSignals() && WriteInputSignals();
                _validatingOk &= _defenseMainConstraintRadio.Checked
                    ? WriteExternalDefenses(_device.ExternalDefensesMain)
                    : WriteExternalDefenses(_device.ExternalDefensesReserve);
                _validatingOk &= _defenseMainConstraintRadio.Checked
                    ? WriteVoltageDefenses(_device.VoltageDefensesMain)
                    : WriteVoltageDefenses(_device.VoltageDefensesReserve);
                _validatingOk &= _defenseMainConstraintRadio.Checked
                    ? WriteFrequenceDefenses(_device.FrequenceDefensesMain)
                    : WriteFrequenceDefenses(_device.FrequenceDefensesReserve);
            }
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            _exchangeProgressBar.Value = 0;
            _processLabel.Text = "���� ������...";
            _connectingErrors = false;
            _device.LoadInputSignals();
            _device.LoadOutputSignals();
            _device.LoadExternalDefenses();
            _device.LoadVoltageDefenses();
            _device.LoadFrequenceDefenses();
        }

        void Grid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                DataGridViewComboBoxEditingControl combo = e.Control as DataGridViewComboBoxEditingControl;
                combo.Items.RemoveAt(combo.Items.Count - 1);
            }
        }

        void Combo_DropDown(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox) sender;
            combo.DropDown -= new EventHandler(Combo_DropDown);
            if (combo.SelectedIndex == combo.Items.Count - 1)
            {
                combo.SelectedIndex = 0;
            }
            if (combo.Items.Contains("XXXXX"))
            {
                combo.Items.Remove("XXXXX");
            }
        }

        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig)
                this.StartRead();
        }

        #endregion

        #region ����������� ����������

        private void OnSaveFail()
        {
            if (!_connectingErrors)
            {
                MessageBox.Show("���������� �������� ������������. ��������� �����", "������-��600. ������",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            _connectingErrors = true;
        }

        private void OnLoadFail()
        {
            if (!_connectingErrors)
            {
                MessageBox.Show("���������� ��������� ������������. ��������� �����", "������-��600. ������",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            _connectingErrors = true;
        }

        private void OnSaveOk()
        {
            _exchangeProgressBar.PerformStep();
        }

        void OnLoadComplete()
        {
            if (_connectingErrors)
            {
                _processLabel.Text = "������ ��������� ���������";
            }
            else
            {
                _processLabel.Text = "������ ������� ���������";
            }
        }

        void OnSaveComplete()
        {
            if (_connectingErrors)
            {
                _processLabel.Text = "������ ��������� ���������";
            }
            else
            {
                _exchangeProgressBar.PerformStep();
                _processLabel.Text = "������ ������� ���������";
            }
        }

        #endregion

        private void SubscriptOnSaveHandlers()
        {
            _device.OutputSignalsSaveOK += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveOk));
                }
                catch (Exception ff)
                {
                }
            };
            _device.OutputSignalsSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveFail));
                }
                catch (Exception ff)
                {
                }
            };
            _device.InputSignalsSaveOK += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveOk));
                }
                catch (Exception ff)
                {
                }
            };
            _device.InputSignalsSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveFail));
                }
                catch (Exception ff)
                {
                }
            };
            _device.ExternalDefensesSaveOK += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveOk));
                }
                catch (Exception ff)
                {
                }
            };
            _device.ExternalDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveFail));
                }
                catch (Exception ff)
                {
                }
            };

            _device.VoltageDefensesSaveOk += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveOk));
                }
                catch (Exception ff)
                {
                }
            };
            _device.VoltageDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveFail));
                }
                catch (Exception ff)
                {
                }
            };

            _device.FrequenceDefensesSaveOk += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveComplete));
                }
                catch (Exception ff)
                {
                }
            };
            _device.FrequenceDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(OnSaveComplete));
                }
                catch (Exception ff)
                {
                }
            };
        }

        void TypeValidation(object sender, TypeValidationEventArgs e)
        {
            if (sender is MaskedTextBox)
            {
                MaskedTextBox box = sender as MaskedTextBox;

                if (!e.IsValidInput)
                {
                    ShowToolTip(box);

                }
                else
                {
                    if (box.ValidatingType == typeof(ulong) && ulong.Parse(box.Text) > ulong.Parse(box.Tag.ToString()))
                    {
                        ShowToolTip(box);
                    }
                    if (box.ValidatingType == typeof(double) &&
                        (double.Parse(box.Text) > double.Parse(box.Tag.ToString())) || (double.Parse(box.Text) < 0))
                    {
                        ShowToolTip(box);
                    }
                }
            }
        }


        private void ShowToolTip(MaskedTextBox box)
        {
            if (_validatingOk)
            {
                if (box.Parent.Parent is TabPage)
                {
                    _tabControl.SelectedTab = box.Parent.Parent as TabPage;
                }
                _toolTip.Show("������� ����� ����� � ��������� [0-" + box.Tag + "]", box, 2000);
                box.Focus();
                box.SelectAll();
                _validatingOk = false;
            }
        }

        private void ShowToolTip(string msg, DataGridView dgv, TabPage page)
        {
            if (!_validatingOk) return;
            _tabControl.SelectedTab = page;
            Point pCell = dgv.GetCellDisplayRectangle(dgv.CurrentCell.ColumnIndex, dgv.CurrentCell.RowIndex, true)
                .Location;
            Point pGrid = dgv.Location;
            _toolTip.Show(msg, page, pCell.X + pGrid.X, pCell.Y + pGrid.Y + dgv.CurrentRow.Height, 2000);
            dgv.CurrentCell.Selected = true;
            _validatingOk = false;
        }

        private void ClearCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].Items.Clear();
            }
        }

        private void SubscriptCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].DropDown += new EventHandler(Combo_DropDown);
                combos[i].SelectedIndexChanged += new EventHandler(Combo_SelectedIndexChanged);
                combos[i].SelectedIndex = 0;
            }
        }

        void Combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox) sender;
            combo.SelectedIndexChanged -= new EventHandler(Combo_SelectedIndexChanged);
            if (combo.SelectedIndex == combo.Items.Count - 1)
            {
                combo.SelectedIndex = 0;
            }
            if (combo.Items.Contains("XXXXX"))
            {
                combo.Items.Remove("XXXXX");
            }
        }

        private void PrepareMaskedBoxes(MaskedTextBox[] boxes, Type validateType)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].TypeValidationCompleted += new TypeValidationEventHandler(TypeValidation);
                boxes[i].ValidatingType = validateType;
            }
        }

        private void ValidateMaskedBoxes(MaskedTextBox[] boxes)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].ValidateText();
            }
        }

        #region ������� �������

        void _device_InputSignalsLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadFail));
            }
            catch (InvalidOperationException)
            {
            }
            catch (Exception ff)
            {
            }
        }

        void _device_InputSignalsLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(ReadInputSignals));
            }
            catch (InvalidOperationException)
            {
            }
            catch (Exception ff)
            {
            }
        }

        private ComboBox[] _inputSignalsCombos;
        private MaskedTextBox[] _inputSignalsDoubleMaskBoxes;

        private void PrepareInputSignals()
        {
            _inputSignalsCombos = new ComboBox[]
                {_inputSignalsConstraintResetCombo, _inputSignalsIndicationResetCombo, _inputSignalsTN_DispepairCombo};

            _inputSignalsDoubleMaskBoxes = new MaskedTextBox[] {_TN_Box, _TNNP_Box};

            FillInputSignalsCombos();

            _logicChannelsCombo.SelectedIndex = _inputSignalsIndicationResetCombo.SelectedIndex =
                _inputSignalsConstraintResetCombo.SelectedIndex = _inputSignalsTN_DispepairCombo.SelectedIndex = 0;
            SubscriptCombos(_inputSignalsCombos);

            PrepareMaskedBoxes(_inputSignalsDoubleMaskBoxes, typeof(double));

        }

        private void FillInputSignalsCombos()
        {
            _inputSignalsTN_DispepairCombo.Items.AddRange(Strings.Diskret.ToArray());
            _inputSignalsConstraintResetCombo.Items.AddRange(Strings.Diskret.ToArray());
            _inputSignalsIndicationResetCombo.Items.AddRange(Strings.Diskret.ToArray());
        }

        private void _logicChannelsCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowInputLogicSignals(_logicChannelsCombo.SelectedIndex);
        }

        private void ShowInputLogicSignals(int channel)
        {
            _logicSignalsDataGrid.Rows.Clear();
            LogicState[] logicSignals = _device.GetInputLogicSignals(channel);
            for (int i = 0; i < 8; i++)
            {
                _logicSignalsDataGrid.Rows.Add(new object[] {i + 1, logicSignals[i].ToString()});
            }
        }

        private void ValidateInputSignals()
        {
            ValidateMaskedBoxes(_inputSignalsDoubleMaskBoxes);
        }

        private void ReadInputSignals()
        {
            ClearCombos(_inputSignalsCombos);
            FillInputSignalsCombos();
            SubscriptCombos(_inputSignalsCombos);
            _exchangeProgressBar.PerformStep();

            _TN_Box.Text = _device.TN.ToString();
            _TNNP_Box.Text = _device.TNNP.ToString();

            _inputSignalsTN_DispepairCombo.SelectedItem = _device.TN_Dispepair;
            _inputSignalsIndicationResetCombo.SelectedItem = _device.IndicationReset;
            _inputSignalsConstraintResetCombo.SelectedItem = _device.ConstraintGroup;
            ShowInputLogicSignals(_logicChannelsCombo.SelectedIndex);
        }

        private bool WriteInputSignals()
        {
            _device.TNNP = Double.Parse(_TNNP_Box.Text);
            _device.TN = Double.Parse(_TN_Box.Text);

            _device.TN_Dispepair = _inputSignalsTN_DispepairCombo.SelectedItem.ToString();
            _device.IndicationReset = _inputSignalsIndicationResetCombo.SelectedItem.ToString();
            _device.ConstraintGroup = _inputSignalsConstraintResetCombo.SelectedItem.ToString();

            return true;

        }

        private void _applyLogicSignalsBut_Click(object sender, EventArgs e)
        {
            LogicState[] logicSignals = new LogicState[16];
            for (int i = 0; i < _logicSignalsDataGrid.Rows.Count; i++)
            {
                object o = Enum.Parse(typeof(LogicState),
                    _logicSignalsDataGrid["_diskretValueCol", i].Value.ToString());
                logicSignals[i] = (LogicState) (o);
            }
            _device.SetInputLogicSignals(_logicChannelsCombo.SelectedIndex, logicSignals);
        }

        #endregion

        #region �������� �������

        private ComboBox[] _outputSignalsCombos;
        private MaskedTextBox[] _outputSignalsBoxes;

        void _device_OutputSignalsLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadFail));
            }
            catch (InvalidOperationException)
            {
            }
            catch (Exception ff)
            {
            }
        }

        void _device_OutputSignalsLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(ReadOutputSignals));
            }
            catch (InvalidOperationException)
            {
            }
            catch (Exception ff)
            {
            }
        }

        private void PrepareOutputSignals()
        {
            _outputSignalsCombos = new ComboBox[] {_outputSignalsSignalizationReleCombo, _outputSignalsAlarmReleCombo};
            _outputSignalsBoxes = new MaskedTextBox[]
                {_outputSignalsAlarmTimeBox, _outputSignalsSignalizationTimeBox, _outputSignalsDispepairTimeBox};

            _outputReleGrid.EditingControlShowing +=
                new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);
            _outputIndicatorsGrid.EditingControlShowing +=
                new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);

            _releSignalCol.Items.AddRange(Strings.All.ToArray());
            _outIndSignalCol.Items.AddRange(Strings.All.ToArray());
            _outputLogicCheckList.Items.AddRange(Strings.Output.ToArray());

            _outputSignalsAlarmReleCombo.Items.AddRange(Strings.All.ToArray());
            _outputSignalsSignalizationReleCombo.Items.AddRange(Strings.All.ToArray());
            _outputSignalsSignalizationReleCombo.SelectedIndex = _outputSignalsAlarmReleCombo.SelectedIndex =
                _outputLogicCombo.SelectedIndex = 0;
            for (int i = 0; i < MR600.COutputRele.COUNT; i++)
            {
                _outputReleGrid.Rows.Add(new object[] {i + 1, "�����������", "���", 0});
            }
            _outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < MR600.COutputIndicator.COUNT; i++)
            {
                _outputIndicatorsGrid.Rows.Add(new object[]
                {
                    i + 1,
                    "�����������",
                    "���",
                    false,
                    false,
                    false
                });
            }
            PrepareMaskedBoxes(_outputSignalsBoxes, typeof(ulong));
            SubscriptCombos(_outputSignalsCombos);
        }

        private void ValidateOutputSignals()
        {
            ValidateMaskedBoxes(_outputSignalsBoxes);
        }

        private void ReadOutputSignals()
        {
            ClearCombos(_outputSignalsCombos);
            _outputSignalsAlarmReleCombo.Items.AddRange(Strings.All.ToArray());
            _outputSignalsSignalizationReleCombo.Items.AddRange(Strings.All.ToArray());
            SubscriptCombos(_outputSignalsCombos);

            _exchangeProgressBar.PerformStep();
            _outputReleGrid.Rows.Clear();

            for (int i = 0; i < _device.OutputDispepairRele.Count; i++)
            {
                _dispepairCheckList.SetItemChecked(i, _device.OutputDispepairRele[i]);
            }
            for (int i = 0; i < MR600.COutputRele.COUNT; i++)
            {
                _outputReleGrid.Rows.Add(new object[]
                {
                    i + 1,
                    _device.OutputRele[i].Type,
                    _device.OutputRele[i].SignalString,
                    _device.OutputRele[i].ImpulseUlong
                });
            }
            _outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < MR600.COutputIndicator.COUNT; i++)
            {
                _outputIndicatorsGrid.Rows.Add(new object[]
                {
                    i + 1,
                    _device.OutputIndicator[i].Type,
                    _device.OutputIndicator[i].SignalString,
                    _device.OutputIndicator[i].Indication,
                    _device.OutputIndicator[i].Alarm,
                    _device.OutputIndicator[i].System
                });
            }
            _outputLogicCombo.SelectedIndex = 0;
            _outputSignalsSignalizationReleCombo.SelectedItem = _device.OutputSignalizationRele;
            _outputSignalsAlarmReleCombo.SelectedItem = _device.OutputAlarmRele;
            _outputSignalsAlarmTimeBox.Text = _device.OutputAlarmTime.ToString();
            _outputSignalsDispepairTimeBox.Text = _device.OutputDispepairTime.ToString();
            _outputSignalsSignalizationTimeBox.Text = _device.OutputSignalizationTime.ToString();
            ShowOutputLogicSignals(0);


        }

        private void ShowOutputLogicSignals(int channel)
        {
            BitArray bits = _device.GetOutputLogicSignals(channel);
            for (int i = 0; i < bits.Count; i++)
            {
                _outputLogicCheckList.SetItemChecked(i, bits[i]);
            }
        }

        private bool WriteOutputSignals()
        {
            bool ret = true;

            CheckedListBoxManager checkManager = new CheckedListBoxManager();
            checkManager.CheckList = _dispepairCheckList;
            _device.OutputDispepairRele = checkManager.ToBitArray();

            if (_device.OutputRele.Count == MR600.COutputRele.COUNT &&
                _outputReleGrid.Rows.Count <= MR600.COutputRele.COUNT)
            {
                for (int i = 0; i < _outputReleGrid.Rows.Count; i++)
                {
                    _device.OutputRele[i].Type = _outputReleGrid["_releTypeCol", i].Value.ToString();
                    _device.OutputRele[i].SignalString = _outputReleGrid["_releSignalCol", i].Value.ToString();
                    try
                    {
                        ulong value = ulong.Parse(_outputReleGrid["_releImpulseCol", i].Value.ToString());
                        if (value > MR600.TIMELIMIT)
                        {
                            _outputReleGrid.CurrentCell = _outputReleGrid["_releImpulseCol", i];
                            ShowToolTip(MR600.TIMELIMIT_ERROR_MSG, _outputReleGrid, _outputSignalsPage);
                            ret = false;
                        }
                        else
                        {
                            _device.OutputRele[i].ImpulseUlong = value;
                        }

                    }
                    catch (Exception)
                    {
                        _outputReleGrid.CurrentCell = _outputReleGrid["_releImpulseCol", i];
                        ShowToolTip(MR600.TIMELIMIT_ERROR_MSG, _outputReleGrid, _outputSignalsPage);
                        ret = false;
                    }
                }
            }

            if (_device.OutputIndicator.Count == MR600.COutputIndicator.COUNT &&
                _outputIndicatorsGrid.Rows.Count <= MR600.COutputIndicator.COUNT)
            {
                for (int i = 0; i < _outputIndicatorsGrid.Rows.Count; i++)
                {
                    _device.OutputIndicator[i].Type = _outputIndicatorsGrid[1, i].Value.ToString();
                    _device.OutputIndicator[i].SignalString = _outputIndicatorsGrid[2, i].Value.ToString();
                    _device.OutputIndicator[i].Indication = (bool) _outputIndicatorsGrid[3, i].Value;
                    _device.OutputIndicator[i].Alarm = (bool) _outputIndicatorsGrid[4, i].Value;
                    _device.OutputIndicator[i].System = (bool) _outputIndicatorsGrid[5, i].Value;
                }

            }
            _device.OutputDispepairTime = ulong.Parse(_outputSignalsDispepairTimeBox.Text);
            _device.OutputSignalizationRele = _outputSignalsSignalizationReleCombo.SelectedItem.ToString();
            _device.OutputSignalizationTime = ulong.Parse(_outputSignalsSignalizationTimeBox.Text);
            _device.OutputAlarmRele = _outputSignalsAlarmReleCombo.SelectedItem.ToString();
            _device.OutputAlarmTime = ulong.Parse(_outputSignalsAlarmTimeBox.Text);

            return ret;
        }

        private void _outputLogicCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowOutputLogicSignals(_outputLogicCombo.SelectedIndex);
        }

        private void _outputLogicAcceptBut_Click(object sender, EventArgs e)
        {
            BitArray bits = new BitArray(72);
            for (int i = 0; i < _outputLogicCheckList.Items.Count; i++)
            {
                bits[i] = _outputLogicCheckList.GetItemChecked(i);
            }
            _device.SetOutputLogicSignals(_outputLogicCombo.SelectedIndex, bits);
        }

        #endregion

        #region ������� ������

        void _device_ExternalDefensesLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadFail));
            }
            catch (InvalidOperationException)
            {
            }
            catch (Exception ff)
            {
            }
        }

        void _device_ExternalDefensesLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnExternalDefensesLoadOk));
            }
            catch (InvalidOperationException)
            {
            }
            catch (Exception ff)
            {
            }
        }

        private void PrepareExternalDefenses()
        {
            _externalDefenseGrid.EditingControlShowing +=
                new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);
            _exDefBlockingCol.Items.AddRange(Strings.Logic.ToArray());
            _exDefModeCol.Items.AddRange(Strings.Modes2.ToArray());
            _exDefReturnNumberCol.Items.AddRange(Strings.External.ToArray());
            _exDefWorkingCol.Items.AddRange(Strings.External.ToArray());
            _externalDefenseGrid.Rows.Add(MR600.CExternalDefenses.COUNT);

            _externalDefenseGrid.CellStateChanged +=
                new DataGridViewCellStateChangedEventHandler(_externalDefenseGrid_CellStateChanged);

            ShowExtrernalDefenses(_device.ExternalDefensesMain);

        }


        private bool OnExternalDefenseGridModeChanged(int row)
        {
            int[] columns = new int[_externalDefenseGrid.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }
            return GridManager.ChangeCellDisabling(_externalDefenseGrid, row, "��������", 1, columns);
        }

        void _externalDefenseGrid_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            //���������� ����� ��� ������� 
            if (_exDefModeCol == e.Cell.OwningColumn || _exDefReturnCol == e.Cell.OwningColumn)
            {
                if (!OnExternalDefenseGridModeChanged(e.Cell.RowIndex))
                {
                    GridManager.ChangeCellDisabling(_externalDefenseGrid, e.Cell.RowIndex, false, 5, new int[] {6, 7});
                }
            }
        }

        private void SetExternalDefenseGridItem(DataGridView grid, MR600.CExternalDefenses externalDefenses,
            int rowIndex, int defenseItemIndex)
        {
            grid[0, rowIndex].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            grid[0, rowIndex].Value = externalDefenses[defenseItemIndex].Name;
            grid[1, rowIndex].Value = externalDefenses[defenseItemIndex].Mode;
            grid[2, rowIndex].Value = externalDefenses[defenseItemIndex].WorkingNumber;
            grid[3, rowIndex].Value = externalDefenses[defenseItemIndex].BlockingNumber;
            grid[4, rowIndex].Value = externalDefenses[defenseItemIndex].WorkingTime;
            grid[5, rowIndex].Value = externalDefenses[defenseItemIndex].Return;
            grid[6, rowIndex].Value = externalDefenses[defenseItemIndex].ReturnNumber;
            grid[7, rowIndex].Value = externalDefenses[defenseItemIndex].ReturnTime;
        }

        private void ShowExtrernalDefenses(MR600.CExternalDefenses externalDefenses)
        {
            for (int i = 0; i < MR600.CExternalDefenses.COUNT; i++)
            {
                SetExternalDefenseGridItem(_externalDefenseGrid, externalDefenses, i, i);
            }
            for (int i = 0; i < MR600.CExternalDefenses.COUNT; i++)
            {
                if (!OnExternalDefenseGridModeChanged(i))
                {
                    GridManager.ChangeCellDisabling(_externalDefenseGrid, i, false, 5, new int[] {6, 7});
                }
            }

        }

        private void OnExternalDefensesLoadOk()
        {
            _exchangeProgressBar.PerformStep();

            if (_defenseMainConstraintRadio.Checked)
            {
                ShowExtrernalDefenses(_device.ExternalDefensesMain);
            }
            else
            {
                ShowExtrernalDefenses(_device.ExternalDefensesReserve);
            }
        }

        private bool WriteExternalDefenseGridItem(DataGridView grid, MR600.CExternalDefenses externalDefenses,
            int rowIndex, int defenseItemIndex)
        {
            bool ret = true;
            externalDefenses[defenseItemIndex].Mode = grid[1, rowIndex].Value.ToString();
            externalDefenses[defenseItemIndex].WorkingNumber = grid[2, rowIndex].Value.ToString();
            externalDefenses[defenseItemIndex].BlockingNumber = grid[3, rowIndex].Value.ToString();
            try
            {
                ulong value = ulong.Parse(grid[4, rowIndex].Value.ToString());
                if (value > MR600.TIMELIMIT)
                {
                    grid.CurrentCell = grid[4, rowIndex];
                    ShowToolTip(MR600.TIMELIMIT_ERROR_MSG, grid, _externalDefensePage);
                    ret = false;
                }
                else
                {
                    externalDefenses[defenseItemIndex].WorkingTime = value;
                }

            }
            catch (Exception)
            {
                grid.CurrentCell = grid[4, rowIndex];
                ShowToolTip(MR600.TIMELIMIT_ERROR_MSG, grid, _externalDefensePage);
                ret = false;
            }
            externalDefenses[defenseItemIndex].Return = bool.Parse(grid[5, rowIndex].Value.ToString());
            externalDefenses[defenseItemIndex].ReturnNumber = grid[6, rowIndex].Value.ToString();
            try
            {
                ulong value = ulong.Parse(grid[7, rowIndex].Value.ToString());
                if (value > MR600.TIMELIMIT)
                {
                    grid.CurrentCell = grid[7, rowIndex];
                    ShowToolTip(MR600.TIMELIMIT_ERROR_MSG, grid, _externalDefensePage);
                    ret = false;
                }
                else
                {
                    externalDefenses[defenseItemIndex].ReturnTime = value;
                }
            }
            catch (Exception)
            {
                grid.CurrentCell = grid[7, rowIndex];
                ShowToolTip(MR600.TIMELIMIT_ERROR_MSG, grid, _externalDefensePage);
                ret = false;
            }

            return ret;
        }


        public bool WriteExternalDefenses(MR600.CExternalDefenses externalDefenses)
        {
            bool ret = true;
            for (int i = 0; i < MR600.CExternalDefenses.COUNT; i++)
            {
                ret &= WriteExternalDefenseGridItem(_externalDefenseGrid, externalDefenses, i, i);
            }

            return ret;
        }


        #endregion

        #region ������ �� ����������

        void _device_VoltageDefensesLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadFail));
            }
            catch (InvalidOperationException)
            {
            }
        }

        void _device_VoltageDefensesLoadOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnVoltageDefensesLoadOk));
            }
            catch (InvalidOperationException)
            {
            }
        }

        private void OnVoltageDefensesLoadOk()
        {
            _exchangeProgressBar.PerformStep();
            if (_defenseMainConstraintRadio.Checked)
            {
                ShowVoltageDefenses(_device.VoltageDefensesMain);
            }
            else
            {
                ShowVoltageDefenses(_device.VoltageDefensesReserve);
            }

        }

        private void PrepareVoltageDefenses()
        {
            _voltageDefensesGrid1.Rows.Add(8);
            _voltageDefensesGrid2.Rows.Add(4);
            _voltageDefensesGrid3.Rows.Add(4);

            _voltageDefensesGrid1.EditingControlShowing +=
                new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);
            _voltageDefensesGrid2.EditingControlShowing +=
                new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);
            _voltageDefensesGrid3.EditingControlShowing +=
                new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);

            _voltageDefensesModeCol1.Items.AddRange(Strings.Modes.ToArray());
            _voltageDefensesBlockingNumberCol1.Items.AddRange(Strings.Logic.ToArray());
            _voltageDefensesTypeCol1.Items.AddRange(Strings.VoltageTypesU.ToArray());

            _voltageDefensesModeCol2.Items.AddRange(Strings.Modes.ToArray());
            _voltageDefensesBlockingNumberCol2.Items.AddRange(Strings.Logic.ToArray());
            _voltageDefensesTypeCol2.Items.AddRange(Strings.VoltageTypesU0.ToArray());

            _voltageDefensesModeCol3.Items.AddRange(Strings.Modes.ToArray());
            _voltageDefensesBlockingNumberCol3.Items.AddRange(Strings.Logic.ToArray());
            _voltageDefensesTypeCol3.Items.AddRange(Strings.VoltageTypesU12.ToArray());

            ShowVoltageDefenses(_device.VoltageDefensesMain);

            _voltageDefensesGrid1.CellStateChanged +=
                new DataGridViewCellStateChangedEventHandler(OnVoltageDefenseCellStateChanged);
            _voltageDefensesGrid2.CellStateChanged +=
                new DataGridViewCellStateChangedEventHandler(OnVoltageDefenseCellStateChanged);
            _voltageDefensesGrid3.CellStateChanged +=
                new DataGridViewCellStateChangedEventHandler(OnVoltageDefenseCellStateChanged);
        }

        private void DisableVoltageItems()
        {
            int[] columns = new int[_voltageDefensesGrid1.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }
            for (int i = 0; i < 8; i++)
            {
                GridManager.ChangeCellDisabling(_voltageDefensesGrid1, i, "��������", 1, columns);
            }
            for (int i = 0; i < 4; i++)
            {
                GridManager.ChangeCellDisabling(_voltageDefensesGrid2, i, "��������", 1, columns);
                GridManager.ChangeCellDisabling(_voltageDefensesGrid3, i, "��������", 1, columns);
            }

        }

        void OnVoltageDefenseCellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (e.Cell.OwningColumn == _voltageDefensesModeCol1 ||
                e.Cell.OwningColumn == _voltageDefensesModeCol2 ||
                e.Cell.OwningColumn == _voltageDefensesModeCol3)
            {
                DataGridView voltageGrid = sender as DataGridView;
                int rowIndex = e.Cell.RowIndex;

                int[] columns = new int[voltageGrid.Columns.Count - 2];
                for (int i = 0; i < columns.Length; i++)
                {
                    columns[i] = i + 2;
                }
                GridManager.ChangeCellDisabling(voltageGrid, rowIndex, "��������", 1, columns);
            }
        }

        private void SetVoltageDefenseGridItem(DataGridView grid, MR600.CVoltageDefenses voltageDefenses, int rowIndex,
            int defenseItemIndex)
        {
            grid[0, rowIndex].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            grid[0, rowIndex].Value = voltageDefenses[defenseItemIndex].Name;
            grid[1, rowIndex].Value = voltageDefenses[defenseItemIndex].Mode;
            grid[2, rowIndex].Value = voltageDefenses[defenseItemIndex].SignalType;
            grid[3, rowIndex].Value = voltageDefenses[defenseItemIndex].WorkConstraint;
            grid[4, rowIndex].Value = voltageDefenses[defenseItemIndex].BlockingNumber;
            grid[5, rowIndex].Value = voltageDefenses[defenseItemIndex].WorkTime;
        }

        private void ShowVoltageDefenses(MR600.CVoltageDefenses voltageDefenses)
        {
            for (int i = 0; i < 8; i++)
            {
                SetVoltageDefenseGridItem(_voltageDefensesGrid1, voltageDefenses, i, i);
            }
            for (int i = 8; i < 12; i++)
            {
                SetVoltageDefenseGridItem(_voltageDefensesGrid2, voltageDefenses, i - 8, i);
            }
            for (int i = 12; i < 16; i++)
            {
                SetVoltageDefenseGridItem(_voltageDefensesGrid3, voltageDefenses, i - 12, i);
            }
            DisableVoltageItems();
        }



        private bool WriteVoltageDefenseGridItem(DataGridView grid, MR600.CVoltageDefenses voltageDefenses,
            int rowIndex, int defenseItemIndex)
        {
            bool ret = true;
            voltageDefenses[defenseItemIndex].Mode = grid[1, rowIndex].Value.ToString();
            voltageDefenses[defenseItemIndex].BlockingNumber = grid[4, rowIndex].Value.ToString();
            voltageDefenses[defenseItemIndex].SignalType = grid[2, rowIndex].Value.ToString();
            try
            {
                double value = double.Parse(grid[3, rowIndex].Value.ToString());
                if (value < 0 || value > 256)
                {
                    grid.CurrentCell = grid[3, rowIndex];
                    ShowToolTip("������� ����� � ��������� [0 - 256]", grid, _voltageDefensesPage);
                    ret = false;
                }
                else
                {
                    voltageDefenses[defenseItemIndex].WorkConstraint = value;
                }

            }
            catch (Exception)
            {
                grid.CurrentCell = grid[3, rowIndex];
                ShowToolTip("������� ����� � ��������� [0 - 256]", grid, _voltageDefensesPage);
                ret = false;
            }
            try
            {
                ulong value = ulong.Parse(grid[5, rowIndex].Value.ToString());
                if (value > MR600.TIMELIMIT)
                {
                    grid.CurrentCell = grid[5, rowIndex];
                    ShowToolTip(MR600.TIMELIMIT_ERROR_MSG, grid, _voltageDefensesPage);
                    ret = false;
                }
                else
                {
                    voltageDefenses[defenseItemIndex].WorkTime = value;
                }

            }
            catch (Exception)
            {
                grid.CurrentCell = grid[5, rowIndex];
                ShowToolTip(MR600.TIMELIMIT_ERROR_MSG, grid, _voltageDefensesPage);
                ret = false;
            }

            return ret;
        }

        public bool WriteVoltageDefenses(MR600.CVoltageDefenses voltageDefenses)
        {
            bool ret = true;
            for (int i = 0; i < 8; i++)
            {
                ret &= WriteVoltageDefenseGridItem(_voltageDefensesGrid1, voltageDefenses, i, i);
            }
            for (int i = 8; i < 12; i++)
            {
                ret &= WriteVoltageDefenseGridItem(_voltageDefensesGrid2, voltageDefenses, i - 8, i);
            }
            for (int i = 12; i < 16; i++)
            {
                ret &= WriteVoltageDefenseGridItem(_voltageDefensesGrid3, voltageDefenses, i - 12, i);
            }
            return ret;
        }

        #endregion

        #region ������ �� �������

        void _device_FrequenceDefensesLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadFail));
            }
            catch (InvalidOperationException)
            {
            }
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadComplete));
            }
            catch (InvalidOperationException)
            {
            }
        }

        void _device_FrequenceDefensesLoadOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnFrequenceDefensesLoadOk));
            }
            catch (InvalidOperationException)
            {
            }
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadComplete));
            }
            catch (InvalidOperationException)
            {
            }
        }

        private void OnFrequenceDefensesLoadOk()
        {
            _exchangeProgressBar.PerformStep();
            if (_defenseMainConstraintRadio.Checked)
            {
                ShowFrequenceDefenses(_device.FrequenceDefensesMain);
            }
            else
            {
                ShowFrequenceDefenses(_device.FrequenceDefensesReserve);
            }

        }

        private void PrepareFrequenceDefenses()
        {
            _frequenceDefensesGrid.Rows.Add(4);
            _frequenceDefensesGrid1.Rows.Add(4);


            _frequenceDefensesMode.Items.AddRange(Strings.Modes.ToArray());
            _frequenceDefensesBlockingNumber.Items.AddRange(Strings.Logic.ToArray());
            _frequenceDefensesMode1.Items.AddRange(Strings.Modes.ToArray());
            _frequenceDefensesBlockingNumber1.Items.AddRange(Strings.Logic.ToArray());
            _CAPV_ModeCol1.Items.AddRange(Strings.ModesLight.ToArray());
            _CAPV_BlockingCol1.Items.AddRange(Strings.Logic.ToArray());

            ShowFrequenceDefenses(_device.FrequenceDefensesMain);

            _frequenceDefensesGrid.EditingControlShowing +=
                new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);
            _frequenceDefensesGrid.CellStateChanged +=
                new DataGridViewCellStateChangedEventHandler(OnFrequenceDefenseCellStateChanged);
            _frequenceDefensesGrid1.EditingControlShowing +=
                new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);
            _frequenceDefensesGrid1.CellStateChanged +=
                new DataGridViewCellStateChangedEventHandler(OnFrequenceDefenseCellStateChanged);
        }

        void OnFrequenceDefenseCellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            int rowIndex = e.Cell.RowIndex;

            if (e.Cell.OwningColumn == _frequenceDefensesMode)
            {
                OnFrequencyModeChanged(grid, rowIndex);
            }
            if (e.Cell.OwningColumn == _frequenceDefensesMode1)
            {
                OnFrequencyModeChanged(grid, rowIndex);
            }
            if (e.Cell.OwningColumn == _CAPV_ModeCol1)
            {
                OnFrequencyModeChanged(grid, rowIndex);
            }
        }

        private void OnCAPV_ModeChanged(DataGridView capvGRID, int rowIndex)
        {
            if (_frequenceDefensesGrid[1, rowIndex].Value.ToString() != "��������")
            {
                int[] columns = new int[capvGRID.Columns.Count - 1];
                for (int i = 1; i < columns.Length + 1; i++)
                {
                    columns[i - 1] = i;
                }
                GridManager.ChangeCellDisabling(capvGRID, rowIndex, "��������", 0, columns);
            }
        }

        private void OnFrequencyModeChanged(DataGridView frequenceGrid, int rowIndex)
        {
            if (frequenceGrid == _frequenceDefensesGrid)
            {
                int[] columns = new int[_frequenceDefensesGrid.Columns.Count - 2];
                for (int i = 0; i < columns.Length; i++)
                {
                    columns[i] = i + 2;
                }
                GridManager.ChangeCellDisabling(_frequenceDefensesGrid, rowIndex, "��������", 1, columns);
                if (_frequenceDefensesGrid[1, rowIndex].Value.ToString() != "��������")
                {
                    int[] columns1 = new int[_frequenceDefensesGrid.Columns.Count - 6];
                    for (int i = 0; i < columns1.Length; i++)
                    {
                        columns1[i] = i + 6;
                    }
                    GridManager.ChangeCellDisabling(_frequenceDefensesGrid, rowIndex, "��������", 5, columns1);

                }
            }
            else
            {
                int[] columns = new int[_frequenceDefensesGrid1.Columns.Count - 2];
                for (int i = 0; i < columns.Length; i++)
                {
                    columns[i] = i + 2;
                }
                GridManager.ChangeCellDisabling(_frequenceDefensesGrid1, rowIndex, "��������", 1, columns);
            }

        }

        private void SetFrequenceDefenseGridItem(DataGridView grid, MR600.CFrequenceDefenses frequenceDefenses,
            int rowIndex, int defenseItemIndex)
        {
            grid[0, rowIndex].Value = frequenceDefenses[defenseItemIndex].Name;
            grid[1, rowIndex].Value = frequenceDefenses[defenseItemIndex].Mode;
            grid[2, rowIndex].Value = frequenceDefenses[defenseItemIndex].Frequency;
            grid[3, rowIndex].Value = frequenceDefenses[defenseItemIndex].BlockingNumber;
            grid[4, rowIndex].Value = frequenceDefenses[defenseItemIndex].WorkTime;
        }

        private void SetFrequenceDefenseGridItemF(DataGridView grid, MR600.CFrequenceDefenses frequenceDefenses,
            int rowIndex, int defenseItemIndex)
        {
            grid[0, rowIndex].Value = frequenceDefenses[defenseItemIndex].Name;
            grid[1, rowIndex].Value = frequenceDefenses[defenseItemIndex].Mode;
            grid[2, rowIndex].Value = frequenceDefenses[defenseItemIndex].Frequency;
            grid[3, rowIndex].Value = frequenceDefenses[defenseItemIndex].BlockingNumber;
            grid[4, rowIndex].Value = frequenceDefenses[defenseItemIndex].WorkTime;
            grid[5, rowIndex].Value = frequenceDefenses[defenseItemIndex].CAPV_Mode;
            grid[7, rowIndex].Value = frequenceDefenses[defenseItemIndex].CAPV_BlockingNumber;
            grid[6, rowIndex].Value = frequenceDefenses[defenseItemIndex].CAPV_Frequency;
            grid[8, rowIndex].Value = frequenceDefenses[defenseItemIndex].CAPV_WorkTime;
        }

        private void ShowFrequenceDefenses(MR600.CFrequenceDefenses frequenceDefenses)
        {
            for (int i = 0; i < MR600.CFrequenceDefenses.COUNT; i++)
            {
                if (i < 4)
                {
                    SetFrequenceDefenseGridItemF(_frequenceDefensesGrid, frequenceDefenses, i, i);
                }
                else
                {
                    SetFrequenceDefenseGridItem(_frequenceDefensesGrid1, frequenceDefenses, i - 4, i);
                }
            }


            for (int i = 0; i < _frequenceDefensesGrid.Rows.Count; i++)
            {
                OnFrequencyModeChanged(_frequenceDefensesGrid, i);
            }
            for (int i = 0; i < _frequenceDefensesGrid1.Rows.Count; i++)
            {
                OnFrequencyModeChanged(_frequenceDefensesGrid1, i);
            }
        }



        private bool WriteFrequenceDefenseGridItem(DataGridView grid, MR600.CFrequenceDefenses frequenceDefenses,
            int rowIndex, int defenseItemIndex)
        {
            bool ret = true;
            frequenceDefenses[defenseItemIndex].Mode = grid[1, rowIndex].Value.ToString();
            frequenceDefenses[defenseItemIndex].BlockingNumber = grid[3, rowIndex].Value.ToString();
            try
            {
                double value = double.Parse(grid[2, rowIndex].Value.ToString());
                if (value < 0 || value > 256)
                {
                    grid.CurrentCell = grid[2, rowIndex];
                    ShowToolTip("������� ����� � ��������� [0 - 256]", grid, _frequencePage);
                    ret = false;
                }
                else
                {
                    frequenceDefenses[defenseItemIndex].Frequency = value;
                }

            }
            catch (Exception)
            {
                grid.CurrentCell = grid[2, rowIndex];
                ShowToolTip("������� ����� � ��������� [0 - 256]", grid, _frequencePage);
                ret = false;
            }
            try
            {
                ulong value = ulong.Parse(grid[4, rowIndex].Value.ToString());
                if (value > MR600.TIMELIMIT)
                {
                    grid.CurrentCell = grid[4, rowIndex];
                    ShowToolTip(MR600.TIMELIMIT_ERROR_MSG, grid, _frequencePage);
                    ret = false;
                }
                else
                {
                    frequenceDefenses[defenseItemIndex].WorkTime = value;
                }

            }
            catch (Exception)
            {
                grid.CurrentCell = grid[4, rowIndex];
                ShowToolTip(MR600.TIMELIMIT_ERROR_MSG, grid, _frequencePage);
                ret = false;
            }

            return ret;
        }

        private bool WriteCAPVGridItem(DataGridView grid, MR600.CFrequenceDefenses frequenceDefenses, int rowIndex,
            int defenseItemIndex)
        {
            bool ret = true;
            frequenceDefenses[defenseItemIndex].CAPV_Mode = grid[5, rowIndex].Value.ToString();
            frequenceDefenses[defenseItemIndex].CAPV_BlockingNumber = grid[7, rowIndex].Value.ToString();
            try
            {
                double value = double.Parse(grid[6, rowIndex].Value.ToString());
                if (value < 0 || value > 256)
                {
                    grid.CurrentCell = grid[6, rowIndex];
                    ShowToolTip("������� ����� � ��������� [0 - 256]", grid, _frequencePage);
                    ret = false;
                }
                else
                {
                    frequenceDefenses[defenseItemIndex].CAPV_Frequency = value;
                }
            }
            catch (Exception)
            {
                grid.CurrentCell = grid[6, rowIndex];
                ShowToolTip("������� ����� � ��������� [0 - 256]", grid, _frequencePage);
                ret = false;
            }
            try
            {
                ulong value = ulong.Parse(grid[8, rowIndex].Value.ToString());
                if (value > MR600.TIMELIMIT)
                {
                    grid.CurrentCell = grid[8, rowIndex];
                    ShowToolTip(MR600.TIMELIMIT_ERROR_MSG, grid, _frequencePage);
                    ret = false;
                }
                else
                {
                    frequenceDefenses[defenseItemIndex].CAPV_WorkTime = value;
                }

            }
            catch (Exception)
            {
                grid.CurrentCell = grid[8, rowIndex];
                ShowToolTip(MR600.TIMELIMIT_ERROR_MSG, grid, _frequencePage);
                ret = false;
            }

            return ret;
        }

        public bool WriteFrequenceDefenses(MR600.CFrequenceDefenses frequenceDefenses)
        {
            bool ret = true;
            for (int i = 0; i < MR600.CFrequenceDefenses.COUNT; i++)
            {
                if (i < 4)
                {
                    ret &= WriteFrequenceDefenseGridItem(_frequenceDefensesGrid, frequenceDefenses, i, i);
                }
                else
                {
                    ret &= WriteFrequenceDefenseGridItem(_frequenceDefensesGrid1, frequenceDefenses, i - 4, i);
                }

            }
            for (int i = 0; i < 4; i++)
            {
                ret &= WriteCAPVGridItem(_frequenceDefensesGrid, frequenceDefenses, i, i);
            }

            return ret;
        }

        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR600); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(ConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtmlFile();
        }

        private void SaveToHtmlFile()
        {
            ValidateAll();
            if (_validatingOk)
            {
                _processLabel.Text = HtmlExport.Export(Resources.MR600Main, Resources.MR600Res, _device, "��600",
                    _device.DeviceVersion);
            }
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }

            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtmlFile();
                return;
            }
        }

        private void Mr600ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void _defenseMainConstraintRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (_defenseMainConstraintRadio.Checked)
            {
                WriteFrequenceDefenses(_device.FrequenceDefensesReserve);
                ShowFrequenceDefenses(_device.FrequenceDefensesMain);

                WriteVoltageDefenses(_device.VoltageDefensesReserve);
                ShowVoltageDefenses(_device.VoltageDefensesMain);

                WriteExternalDefenses(_device.ExternalDefensesReserve);
                ShowExtrernalDefenses(_device.ExternalDefensesMain);
            }
            else
            {
                WriteFrequenceDefenses(_device.FrequenceDefensesMain);
                ShowFrequenceDefenses(_device.FrequenceDefensesReserve);

                WriteVoltageDefenses(_device.VoltageDefensesMain);
                ShowVoltageDefenses(_device.VoltageDefensesReserve);

                WriteExternalDefenses(_device.ExternalDefensesMain);
                ShowExtrernalDefenses(_device.ExternalDefensesReserve);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ResetSetPoints(true);
        }

        private void ResetSetPoints(bool isDialog)
        {
            try
            {
                double version = Common.VersionConverter(this._device.DeviceVersion);
                if (version < 3.0)
                {
                    string path = (Path.GetDirectoryName(Application.ExecutablePath) +
                                   string.Format(MR600_BASE_CONFIG_PATH, "1.0"));
                    this._device.Deserialize(path);
                }
                if (_defenseMainConstraintRadio.Checked)
                {
                    ShowVoltageDefenses(_device.VoltageDefensesMain);
                }
                else
                {
                    ShowVoltageDefenses(_device.VoltageDefensesReserve);
                }
                if (_defenseMainConstraintRadio.Checked)
                {
                    ShowFrequenceDefenses(_device.FrequenceDefensesMain);
                }
                else
                {
                    ShowFrequenceDefenses(_device.FrequenceDefensesReserve);
                }
                ReadInputSignals();
                OnExternalDefensesLoadOk();
                ReadOutputSignals();

                _exchangeProgressBar.Value = 0;
                _processLabel.Text = BASE_CONFIG_LOAD_SUCCESSFULLY;
            }
            catch (System.IO.FileLoadException exc)
            {
                if (isDialog)
                {
                    if (MessageBox.Show("���������� ��������� ���� ����������� ������������. �������� �������?",
                            "��������", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        this.ResetInputSignals();
                        this.ResetOutputSignals();
                        this.ResetFreqDef();
                        this.ResetVoltage();
                        this.ResetExDef();

                    }
                }
                else
                {
                    return;
                }

            }
        }

        private void ResetExDef()
        {
            this._device.ExternalDefensesMain=new MR600.CExternalDefenses();
            for (int i = 0; i < MR600.CExternalDefenses.COUNT; i++)
            {
                SetExternalDefenseGridItem(_externalDefenseGrid, _device.ExternalDefensesMain, i, i);
            }
            for (int i = 0; i < MR600.CExternalDefenses.COUNT; i++)
            {
                if (!OnExternalDefenseGridModeChanged(i))
                {
                    GridManager.ChangeCellDisabling(_externalDefenseGrid, i, false, 5, new int[] { 6, 7 });
                }
            }
            this._device.ExternalDefensesReserve=new MR600.CExternalDefenses();

        }

        private void ResetVoltage()
        {
            this._device.VoltageDefensesMain = new MR600.CVoltageDefenses();

            for (int i = 0; i < 8; i++)
            {
                SetVoltageDefenseGridItem(_voltageDefensesGrid1, _device.VoltageDefensesMain, i, i);
            }
            for (int i = 8; i < 12; i++)
            {
                SetVoltageDefenseGridItem(_voltageDefensesGrid2, _device.VoltageDefensesMain, i - 8, i);
            }
            for (int i = 12; i < 16; i++)
            {
                SetVoltageDefenseGridItem(_voltageDefensesGrid3, _device.VoltageDefensesMain, i - 12, i);
            }

            this._device.VoltageDefensesReserve = new MR600.CVoltageDefenses();
        }

        private void ResetFreqDef()
        {
            this._frequenceDefensesGrid.Rows.Clear();
            this._frequenceDefensesGrid.Rows.Add(4);
            this._device.FrequenceDefensesMain = new MR600.CFrequenceDefenses();

            for (int i = 0; i < MR600.CFrequenceDefenses.COUNT; i++)
            {
                if (i < 4)
                {
                    SetFrequenceDefenseGridItemF(_frequenceDefensesGrid, this._device.FrequenceDefensesMain, i, i);
                }
                else
                {
                    SetFrequenceDefenseGridItem(_frequenceDefensesGrid1, this._device.FrequenceDefensesMain, i - 4, i);
                }
            }


            for (int i = 0; i < _frequenceDefensesGrid.Rows.Count; i++)
            {
                OnFrequencyModeChanged(_frequenceDefensesGrid, i);
            }
            for (int i = 0; i < _frequenceDefensesGrid1.Rows.Count; i++)
            {
                OnFrequencyModeChanged(_frequenceDefensesGrid1, i);
            }

            this._device.FrequenceDefensesReserve = new MR600.CFrequenceDefenses(); ;
        }

        private void ResetInputSignals()
        {
            this._TN_Box.Text = 0.ToString();
            this._TNNP_Box.Text = 0.ToString();


            this._inputSignalsConstraintResetCombo.SelectedIndex = 0;
            this._inputSignalsIndicationResetCombo.SelectedIndex = 0;
            this._inputSignalsTN_DispepairCombo.SelectedIndex = 0;

            //����� ��
            this._outputLogicCheckList.ClearSelected();
            this._outputLogicCombo.SelectedIndex = 0;
            for (int i = 0; i < this._logicChannelsCombo.Items.Count; i++)
            {
                this._device.SetInputLogicSignals(i, new LogicState[8]);
            }
            this._logicSignalsDataGrid.Rows.Clear();
            for (int i = 0; i < 16; i++)
            {
                this._logicSignalsDataGrid.Rows.Add(new object[] {i + 1, Strings.Logic[0]});
            }
            this._logicChannelsCombo.SelectedIndex = 0;
        }

        private void ResetOutputSignals()
        {
            //����� ���
            for (int i = 0; i < this._outputLogicCombo.Items.Count; i++)
            {
                this._device.SetOutputLogicSignals(i, new BitArray(72));
            }
            this._outputLogicCheckList.ClearSelected();
            this._outputLogicCombo.SelectedIndex = 0;

            this._outputLogicCheckList.Items.Clear();
            if (this._outputLogicCheckList.Items.Count == 0)
            {
                this._outputLogicCheckList.Items.AddRange(Strings.Output.ToArray());
            }

            //����� �������� ����
            this._outputReleGrid.Rows.Clear();

            this._releSignalCol.Items.AddRange(Strings.All.ToArray());
            this._outIndSignalCol.Items.AddRange(Strings.All.ToArray());

            for (int i = 0; i < BEMN.MR600.MR600.OUTPUTLOGIC_OFFSET; i++)
            {
                this._outputReleGrid.Rows.Add(new object[] {i + 1, "�����������", "���", 0});
            }

            //����� �����������
            this._outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < BEMN.MR600.MR600.COutputIndicator.COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(new object[]
                {
                    i + 1,
                    "�����������",
                    "���",
                    false,
                    false,
                    false
                });
            }
            //����� ��������������
            this._outputSignalsDispepairTimeBox.Text = 0.ToString();
            this._outputSignalsSignalizationTimeBox.Text = 0.ToString();
            this._outputSignalsAlarmTimeBox.Text = 0.ToString();



            _outputSignalsSignalizationReleCombo.SelectedIndex = 0;
            _outputSignalsAlarmReleCombo.SelectedIndex = 0;

            for (int i = 0; i < this._device.OutputDispepairRele.Count; i++)
            {
                this._dispepairCheckList.SetItemChecked(i, this._device.OutputDispepairRele[i]);
            }
            this._dispepairCheckList.ClearSelected();
        }

    }
}

