using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Forms.Export;
using BEMN.Interfaces;

namespace BEMN.MR600.Old.Forms
{
    public partial class SystemJournalForm : Form , IFormView
    {
        private MR600 _device;
        private DataTable _table;
        public SystemJournalForm()
        {
            InitializeComponent();
        }

        public SystemJournalForm(MR600 device)
        {
            InitializeComponent();
            _device = device;

            _device.SystemJournalRecordLoadOk += this._device_SystemJournalRecordLoadOk;
            _device.SystemJournalRecordLoadFail += this._device_SystemJournalRecordLoadFail;
        }

        void _device_SystemJournalRecordLoadFail(object sender, int index)
        {
            _readSysJournalBut.Enabled = true;
        }

        void _device_SystemJournalRecordLoadOk(object sender, int index)
        {
            try
            {
                Invoke(new OnDeviceIndexEventHandler(OnSysJournalIndexLoadOk), index);
            }
            catch (InvalidOperationException)
            {}
           
        }

        private void OnSysJournalIndexLoadOk(int i)
        {
            _journalProgress.Increment(1);
            if (_device.SystemJournal[i].msg == MR600.JURNAL_CLEAR) return;   
            _journalCntLabel.Text = (i + 1).ToString();         
            _sysJournalGrid.Rows.Add(new object[] { i + 1, _device.SystemJournal[i].time, _device.SystemJournal[i].msg });
            if (i == MR600.SYSTEMJOURNAL_RECORD_CNT - 1)
            {
                _readSysJournalBut.Enabled = true;
            }
        }
        
        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR600); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(SystemJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.js; }
        }

        public string NodeName
        {
            get { return "������ �������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _serializeSysJournalBut_Click(object sender, EventArgs e)
        {
            FillTable();
          

            if (DialogResult.OK == _saveSysJournalDlg.ShowDialog())
            {
                _table.WriteXml(_saveSysJournalDlg.FileName);
            }
        }

        private void FillTable()
        {
            _table = new DataTable("��600_������_�������");
            _table.Columns.Add("�����");
            _table.Columns.Add("�����");
            _table.Columns.Add("���������");
            for (int i = 0; i < _sysJournalGrid.Rows.Count; i++)
            {
                this._table.Rows.Add(this._sysJournalGrid["_indexCol", i].Value, 
                    this._sysJournalGrid["_timeCol", i].Value, this._sysJournalGrid["_msgCol", i].Value);
            } 
        }

        private void _deserializeSysJournalBut_Click(object sender, EventArgs e)
        {            
            DataTable table = new DataTable("��600_������_�������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");

            if (DialogResult.OK == _openSysJounralDlg.ShowDialog())
            {
                _sysJournalGrid.Rows.Clear();
                table.ReadXml(_openSysJounralDlg.FileName);
            }

            for (int i = 0; i < table.Rows.Count; i++)
            {
                this._sysJournalGrid.Rows.Add(table.Rows[i].ItemArray[0], 
                    table.Rows[i].ItemArray[1], table.Rows[i].ItemArray[2]);
            }
        }

        private void SystemJournalForm_Activated(object sender, EventArgs e)
        {
            _device.SuspendSystemJournal(false);
        }

        private void SystemJournalForm_Deactivate(object sender, EventArgs e)
        {
            _device.SuspendSystemJournal(true);
        }

        private void SystemJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _device.RemoveSystemJournal();
        }

        private void _readSysJournalBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            _readSysJournalBut.Enabled = false;
            _device.SystemJournal = new MR600.CSystemJournal();
            _device.RemoveSystemJournal();
            _journalProgress.Value = 0;
            _journalProgress.Maximum = MR600.SYSTEMJOURNAL_RECORD_CNT;
            _sysJournalGrid.Rows.Clear();
            _device.LoadSystemJournal();
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                FillTable();
                HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR600SJ);
                MessageBox.Show("������ ��������");
            }
        }
    }
}