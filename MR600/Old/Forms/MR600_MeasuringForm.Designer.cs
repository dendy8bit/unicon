namespace BEMN.MR600.Old.Forms
{
    partial class MeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BEMN.Devices.Structures.DateTimeStruct dateTimeStructOld1 = new BEMN.Devices.Structures.DateTimeStruct();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MeasuringForm));
            this._diagTab = new System.Windows.Forms.TabControl();
            this._analogPage = new System.Windows.Forms.TabPage();
            this._dateTimeControlOld = new BEMN.Forms.DateTimeControl();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._manageLed10 = new BEMN.Forms.LedControl();
            this.label93 = new System.Windows.Forms.Label();
            this._manageLed9 = new BEMN.Forms.LedControl();
            this.label94 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this._manageLed8 = new BEMN.Forms.LedControl();
            this.label53 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this._manageLed5 = new BEMN.Forms.LedControl();
            this.label58 = new System.Windows.Forms.Label();
            this.ledControl2 = new BEMN.Forms.LedControl();
            this.ledControl1 = new BEMN.Forms.LedControl();
            this._manageLed4 = new BEMN.Forms.LedControl();
            this._manageLed3 = new BEMN.Forms.LedControl();
            this.label61 = new System.Windows.Forms.Label();
            this._manageLed7 = new BEMN.Forms.LedControl();
            this._manageLed6 = new BEMN.Forms.LedControl();
            this._resetJA_But = new System.Windows.Forms.Button();
            this._resetJS_But = new System.Windows.Forms.Button();
            this._resetFaultBut = new System.Windows.Forms.Button();
            this._constraintToggleButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this._indicationResetButton = new System.Windows.Forms.Button();
            this.label60 = new System.Windows.Forms.Label();
            this._manageLed2 = new BEMN.Forms.LedControl();
            this._manageLed1 = new BEMN.Forms.LedControl();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label97 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this._UnBox = new System.Windows.Forms.TextBox();
            this._U2Box = new System.Windows.Forms.TextBox();
            this._U1Box = new System.Windows.Forms.TextBox();
            this._U0Box = new System.Windows.Forms.TextBox();
            this._UcaBox = new System.Windows.Forms.TextBox();
            this._UbcBox = new System.Windows.Forms.TextBox();
            this._UabBox = new System.Windows.Forms.TextBox();
            this._UbBox = new System.Windows.Forms.TextBox();
            this._UaBox = new System.Windows.Forms.TextBox();
            this._F_Box = new System.Windows.Forms.TextBox();
            this._UcBox = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label41 = new System.Windows.Forms.Label();
            this._inL_led8 = new BEMN.Forms.LedControl();
            this.label42 = new System.Windows.Forms.Label();
            this._inL_led7 = new BEMN.Forms.LedControl();
            this.label43 = new System.Windows.Forms.Label();
            this._inL_led6 = new BEMN.Forms.LedControl();
            this.label44 = new System.Windows.Forms.Label();
            this._inL_led5 = new BEMN.Forms.LedControl();
            this.label45 = new System.Windows.Forms.Label();
            this._inL_led4 = new BEMN.Forms.LedControl();
            this.label46 = new System.Windows.Forms.Label();
            this._inL_led3 = new BEMN.Forms.LedControl();
            this.label47 = new System.Windows.Forms.Label();
            this._inL_led2 = new BEMN.Forms.LedControl();
            this.label48 = new System.Windows.Forms.Label();
            this._inL_led1 = new BEMN.Forms.LedControl();
            this.label25 = new System.Windows.Forms.Label();
            this._inD_led8 = new BEMN.Forms.LedControl();
            this.label26 = new System.Windows.Forms.Label();
            this._inD_led7 = new BEMN.Forms.LedControl();
            this.label27 = new System.Windows.Forms.Label();
            this._inD_led6 = new BEMN.Forms.LedControl();
            this.label28 = new System.Windows.Forms.Label();
            this._inD_led5 = new BEMN.Forms.LedControl();
            this.label29 = new System.Windows.Forms.Label();
            this._inD_led4 = new BEMN.Forms.LedControl();
            this.label30 = new System.Windows.Forms.Label();
            this._inD_led3 = new BEMN.Forms.LedControl();
            this.label31 = new System.Windows.Forms.Label();
            this._inD_led2 = new BEMN.Forms.LedControl();
            this.label32 = new System.Windows.Forms.Label();
            this._inD_led1 = new BEMN.Forms.LedControl();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this._outLed8 = new BEMN.Forms.LedControl();
            this.label18 = new System.Windows.Forms.Label();
            this._outLed7 = new BEMN.Forms.LedControl();
            this.label19 = new System.Windows.Forms.Label();
            this._outLed6 = new BEMN.Forms.LedControl();
            this.label20 = new System.Windows.Forms.Label();
            this._outLed5 = new BEMN.Forms.LedControl();
            this.label21 = new System.Windows.Forms.Label();
            this._outLed4 = new BEMN.Forms.LedControl();
            this.label22 = new System.Windows.Forms.Label();
            this._outLed3 = new BEMN.Forms.LedControl();
            this.label23 = new System.Windows.Forms.Label();
            this._outLed2 = new BEMN.Forms.LedControl();
            this.label24 = new System.Windows.Forms.Label();
            this._outLed1 = new BEMN.Forms.LedControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label33 = new System.Windows.Forms.Label();
            this._releLed16 = new BEMN.Forms.LedControl();
            this.label34 = new System.Windows.Forms.Label();
            this._releLed15 = new BEMN.Forms.LedControl();
            this.label35 = new System.Windows.Forms.Label();
            this._releLed14 = new BEMN.Forms.LedControl();
            this.label36 = new System.Windows.Forms.Label();
            this._releLed13 = new BEMN.Forms.LedControl();
            this.label37 = new System.Windows.Forms.Label();
            this._releLed12 = new BEMN.Forms.LedControl();
            this.label38 = new System.Windows.Forms.Label();
            this._releLed11 = new BEMN.Forms.LedControl();
            this.label39 = new System.Windows.Forms.Label();
            this._releLed10 = new BEMN.Forms.LedControl();
            this.label40 = new System.Windows.Forms.Label();
            this._releLed9 = new BEMN.Forms.LedControl();
            this.label9 = new System.Windows.Forms.Label();
            this._releLed8 = new BEMN.Forms.LedControl();
            this.label10 = new System.Windows.Forms.Label();
            this._releLed7 = new BEMN.Forms.LedControl();
            this.label11 = new System.Windows.Forms.Label();
            this._releLed6 = new BEMN.Forms.LedControl();
            this.label12 = new System.Windows.Forms.Label();
            this._releLed5 = new BEMN.Forms.LedControl();
            this.label13 = new System.Windows.Forms.Label();
            this._releLed4 = new BEMN.Forms.LedControl();
            this.label14 = new System.Windows.Forms.Label();
            this._releLed3 = new BEMN.Forms.LedControl();
            this.label15 = new System.Windows.Forms.Label();
            this._releLed2 = new BEMN.Forms.LedControl();
            this.label16 = new System.Windows.Forms.Label();
            this._releLed1 = new BEMN.Forms.LedControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this._indLed6 = new BEMN.Forms.LedControl();
            this.label8 = new System.Windows.Forms.Label();
            this._indLed5 = new BEMN.Forms.LedControl();
            this.label3 = new System.Windows.Forms.Label();
            this._indLed4 = new BEMN.Forms.LedControl();
            this.label4 = new System.Windows.Forms.Label();
            this._indLed3 = new BEMN.Forms.LedControl();
            this.label2 = new System.Windows.Forms.Label();
            this._indLed2 = new BEMN.Forms.LedControl();
            this.label1 = new System.Windows.Forms.Label();
            this._indLed1 = new BEMN.Forms.LedControl();
            this._diskretPage = new System.Windows.Forms.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this._FminLed8 = new BEMN.Forms.LedControl();
            this._FminLed7 = new BEMN.Forms.LedControl();
            this._FminLed6 = new BEMN.Forms.LedControl();
            this._FminLed5 = new BEMN.Forms.LedControl();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this._FminLed4 = new BEMN.Forms.LedControl();
            this._FminLed3 = new BEMN.Forms.LedControl();
            this._FminLed2 = new BEMN.Forms.LedControl();
            this.label92 = new System.Windows.Forms.Label();
            this._FminLed1 = new BEMN.Forms.LedControl();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._FmaxLed8 = new BEMN.Forms.LedControl();
            this._FmaxLed7 = new BEMN.Forms.LedControl();
            this._FmaxLed6 = new BEMN.Forms.LedControl();
            this._FmaxLed5 = new BEMN.Forms.LedControl();
            this.label49 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this._FmaxLed4 = new BEMN.Forms.LedControl();
            this._FmaxLed3 = new BEMN.Forms.LedControl();
            this._FmaxLed2 = new BEMN.Forms.LedControl();
            this.label86 = new System.Windows.Forms.Label();
            this._FmaxLed1 = new BEMN.Forms.LedControl();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this._faultSignalLed16 = new BEMN.Forms.LedControl();
            this.label130 = new System.Windows.Forms.Label();
            this._faultSignalLed15 = new BEMN.Forms.LedControl();
            this.label131 = new System.Windows.Forms.Label();
            this._faultSignalLed14 = new BEMN.Forms.LedControl();
            this.label132 = new System.Windows.Forms.Label();
            this._faultSignalLed13 = new BEMN.Forms.LedControl();
            this.label133 = new System.Windows.Forms.Label();
            this._faultSignalLed12 = new BEMN.Forms.LedControl();
            this.label134 = new System.Windows.Forms.Label();
            this._faultSignalLed11 = new BEMN.Forms.LedControl();
            this.label135 = new System.Windows.Forms.Label();
            this._faultSignalLed10 = new BEMN.Forms.LedControl();
            this.label136 = new System.Windows.Forms.Label();
            this._faultSignalLed9 = new BEMN.Forms.LedControl();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.label121 = new System.Windows.Forms.Label();
            this._faultSignalLed8 = new BEMN.Forms.LedControl();
            this.label122 = new System.Windows.Forms.Label();
            this._faultSignalLed7 = new BEMN.Forms.LedControl();
            this.label123 = new System.Windows.Forms.Label();
            this._faultSignalLed6 = new BEMN.Forms.LedControl();
            this.label124 = new System.Windows.Forms.Label();
            this._faultSignalLed5 = new BEMN.Forms.LedControl();
            this._faultSignalLed4 = new BEMN.Forms.LedControl();
            this.label126 = new System.Windows.Forms.Label();
            this._faultSignalLed3 = new BEMN.Forms.LedControl();
            this.label127 = new System.Windows.Forms.Label();
            this._faultSignalLed2 = new BEMN.Forms.LedControl();
            this.label128 = new System.Windows.Forms.Label();
            this._faultSignalLed1 = new BEMN.Forms.LedControl();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.label113 = new System.Windows.Forms.Label();
            this._faultStateLed8 = new BEMN.Forms.LedControl();
            this.label114 = new System.Windows.Forms.Label();
            this._faultStateLed7 = new BEMN.Forms.LedControl();
            this.label115 = new System.Windows.Forms.Label();
            this._faultStateLed6 = new BEMN.Forms.LedControl();
            this.label116 = new System.Windows.Forms.Label();
            this._faultStateLed5 = new BEMN.Forms.LedControl();
            this.label117 = new System.Windows.Forms.Label();
            this._faultStateLed4 = new BEMN.Forms.LedControl();
            this.label118 = new System.Windows.Forms.Label();
            this._faultStateLed3 = new BEMN.Forms.LedControl();
            this.label119 = new System.Windows.Forms.Label();
            this._faultStateLed2 = new BEMN.Forms.LedControl();
            this.label120 = new System.Windows.Forms.Label();
            this._faultStateLed1 = new BEMN.Forms.LedControl();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.label105 = new System.Windows.Forms.Label();
            this._extDefenseLed8 = new BEMN.Forms.LedControl();
            this.label106 = new System.Windows.Forms.Label();
            this._extDefenseLed7 = new BEMN.Forms.LedControl();
            this.label107 = new System.Windows.Forms.Label();
            this._extDefenseLed6 = new BEMN.Forms.LedControl();
            this.label108 = new System.Windows.Forms.Label();
            this._extDefenseLed5 = new BEMN.Forms.LedControl();
            this.label109 = new System.Windows.Forms.Label();
            this._extDefenseLed4 = new BEMN.Forms.LedControl();
            this.label110 = new System.Windows.Forms.Label();
            this._extDefenseLed3 = new BEMN.Forms.LedControl();
            this.label111 = new System.Windows.Forms.Label();
            this._extDefenseLed2 = new BEMN.Forms.LedControl();
            this.label112 = new System.Windows.Forms.Label();
            this._extDefenseLed1 = new BEMN.Forms.LedControl();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._U12Led8 = new BEMN.Forms.LedControl();
            this._U12Led7 = new BEMN.Forms.LedControl();
            this._U12Led6 = new BEMN.Forms.LedControl();
            this._U12Led5 = new BEMN.Forms.LedControl();
            this.label67 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this._U12Led4 = new BEMN.Forms.LedControl();
            this._U12Led3 = new BEMN.Forms.LedControl();
            this._U12Led2 = new BEMN.Forms.LedControl();
            this.label78 = new System.Windows.Forms.Label();
            this._U12Led1 = new BEMN.Forms.LedControl();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._U0maxLed8 = new BEMN.Forms.LedControl();
            this._U0maxLed7 = new BEMN.Forms.LedControl();
            this._U0maxLed6 = new BEMN.Forms.LedControl();
            this._U0maxLed5 = new BEMN.Forms.LedControl();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this._U0maxLed4 = new BEMN.Forms.LedControl();
            this._U0maxLed3 = new BEMN.Forms.LedControl();
            this._U0maxLed2 = new BEMN.Forms.LedControl();
            this.label84 = new System.Windows.Forms.Label();
            this._U0maxLed1 = new BEMN.Forms.LedControl();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._UminLed8 = new BEMN.Forms.LedControl();
            this._UminLed7 = new BEMN.Forms.LedControl();
            this._UminLed6 = new BEMN.Forms.LedControl();
            this._UminLed5 = new BEMN.Forms.LedControl();
            this.label55 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this._UminLed4 = new BEMN.Forms.LedControl();
            this._UminLed3 = new BEMN.Forms.LedControl();
            this._UminLed2 = new BEMN.Forms.LedControl();
            this.label66 = new System.Windows.Forms.Label();
            this._UminLed1 = new BEMN.Forms.LedControl();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._UmaxLed8 = new BEMN.Forms.LedControl();
            this._UmaxLed7 = new BEMN.Forms.LedControl();
            this._UmaxLed6 = new BEMN.Forms.LedControl();
            this._UmaxLed5 = new BEMN.Forms.LedControl();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this._UmaxLed4 = new BEMN.Forms.LedControl();
            this._UmaxLed3 = new BEMN.Forms.LedControl();
            this._UmaxLed2 = new BEMN.Forms.LedControl();
            this.label68 = new System.Windows.Forms.Label();
            this._UmaxLed1 = new BEMN.Forms.LedControl();
            this._images = new System.Windows.Forms.ImageList(this.components);
            this._diagTab.SuspendLayout();
            this._analogPage.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this._diskretPage.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // _diagTab
            // 
            this._diagTab.Controls.Add(this._analogPage);
            this._diagTab.Controls.Add(this._diskretPage);
            this._diagTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this._diagTab.ImageList = this._images;
            this._diagTab.Location = new System.Drawing.Point(0, 0);
            this._diagTab.Name = "_diagTab";
            this._diagTab.SelectedIndex = 0;
            this._diagTab.Size = new System.Drawing.Size(582, 428);
            this._diagTab.TabIndex = 0;
            // 
            // _analogPage
            // 
            this._analogPage.AllowDrop = true;
            this._analogPage.Controls.Add(this._dateTimeControlOld);
            this._analogPage.Controls.Add(this.groupBox8);
            this._analogPage.Controls.Add(this.groupBox6);
            this._analogPage.Controls.Add(this.groupBox5);
            this._analogPage.Controls.Add(this.groupBox4);
            this._analogPage.Controls.Add(this.groupBox3);
            this._analogPage.Controls.Add(this.groupBox2);
            this._analogPage.ImageIndex = 0;
            this._analogPage.Location = new System.Drawing.Point(4, 23);
            this._analogPage.Name = "_analogPage";
            this._analogPage.Padding = new System.Windows.Forms.Padding(3);
            this._analogPage.Size = new System.Drawing.Size(574, 401);
            this._analogPage.TabIndex = 0;
            this._analogPage.Text = "���������� �� ";
            this._analogPage.UseVisualStyleBackColor = true;
            // 
            // _dateTimeControlOld
            // 
            this._dateTimeControlOld.DateTime = dateTimeStructOld1;
            this._dateTimeControlOld.Location = new System.Drawing.Point(318, 232);
            this._dateTimeControlOld.Name = "_dateTimeControlOld";
            this._dateTimeControlOld.Size = new System.Drawing.Size(202, 162);
            this._dateTimeControlOld.TabIndex = 13;
            this._dateTimeControlOld.TimeChanged += new System.Action(this._dateTimeControlOld_TimeChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._manageLed10);
            this.groupBox8.Controls.Add(this.label93);
            this.groupBox8.Controls.Add(this._manageLed9);
            this.groupBox8.Controls.Add(this.label94);
            this.groupBox8.Controls.Add(this.label137);
            this.groupBox8.Controls.Add(this._manageLed8);
            this.groupBox8.Controls.Add(this.label53);
            this.groupBox8.Controls.Add(this.label51);
            this.groupBox8.Controls.Add(this._manageLed5);
            this.groupBox8.Controls.Add(this.label58);
            this.groupBox8.Controls.Add(this.ledControl2);
            this.groupBox8.Controls.Add(this.ledControl1);
            this.groupBox8.Controls.Add(this._manageLed4);
            this.groupBox8.Controls.Add(this._manageLed3);
            this.groupBox8.Controls.Add(this.label61);
            this.groupBox8.Controls.Add(this._manageLed7);
            this.groupBox8.Controls.Add(this._manageLed6);
            this.groupBox8.Controls.Add(this._resetJA_But);
            this.groupBox8.Controls.Add(this._resetJS_But);
            this.groupBox8.Controls.Add(this._resetFaultBut);
            this.groupBox8.Controls.Add(this._constraintToggleButton);
            this.groupBox8.Controls.Add(this.label6);
            this.groupBox8.Controls.Add(this.label104);
            this.groupBox8.Controls.Add(this.label5);
            this.groupBox8.Controls.Add(this.label50);
            this.groupBox8.Controls.Add(this._indicationResetButton);
            this.groupBox8.Controls.Add(this.label60);
            this.groupBox8.Controls.Add(this._manageLed2);
            this.groupBox8.Controls.Add(this._manageLed1);
            this.groupBox8.Location = new System.Drawing.Point(318, 8);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(248, 218);
            this.groupBox8.TabIndex = 12;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "����������� �������(����)";
            // 
            // _manageLed10
            // 
            this._manageLed10.Location = new System.Drawing.Point(12, 195);
            this._manageLed10.Name = "_manageLed10";
            this._manageLed10.Size = new System.Drawing.Size(13, 13);
            this._manageLed10.State = BEMN.Forms.LedState.Off;
            this._manageLed10.TabIndex = 73;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(28, 195);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(172, 13);
            this.label93.TabIndex = 72;
            this.label93.Text = "������� ��� ��������� 40-70 ��";
            // 
            // _manageLed9
            // 
            this._manageLed9.Location = new System.Drawing.Point(12, 173);
            this._manageLed9.Name = "_manageLed9";
            this._manageLed9.Size = new System.Drawing.Size(13, 13);
            this._manageLed9.State = BEMN.Forms.LedState.Off;
            this._manageLed9.TabIndex = 71;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(28, 173);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(96, 13);
            this.label94.TabIndex = 70;
            this.label94.Text = "���������� < 5�";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(28, 107);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(99, 13);
            this.label137.TabIndex = 68;
            this.label137.Text = "����� ������ ��";
            // 
            // _manageLed8
            // 
            this._manageLed8.Location = new System.Drawing.Point(12, 151);
            this._manageLed8.Name = "_manageLed8";
            this._manageLed8.Size = new System.Drawing.Size(13, 13);
            this._manageLed8.State = BEMN.Forms.LedState.Off;
            this._manageLed8.TabIndex = 67;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(9, 193);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(0, 13);
            this.label53.TabIndex = 66;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(28, 129);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(99, 13);
            this.label51.TabIndex = 65;
            this.label51.Text = "����� ������ ��";
            // 
            // _manageLed5
            // 
            this._manageLed5.Location = new System.Drawing.Point(12, 107);
            this._manageLed5.Name = "_manageLed5";
            this._manageLed5.Size = new System.Drawing.Size(13, 13);
            this._manageLed5.State = BEMN.Forms.LedState.Off;
            this._manageLed5.TabIndex = 64;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(28, 85);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(136, 13);
            this.label58.TabIndex = 63;
            this.label58.Text = "������� ��������������";
            // 
            // ledControl2
            // 
            this.ledControl2.Location = new System.Drawing.Point(103, 62);
            this.ledControl2.Name = "ledControl2";
            this.ledControl2.Size = new System.Drawing.Size(13, 13);
            this.ledControl2.State = BEMN.Forms.LedState.NoSignaled;
            this.ledControl2.TabIndex = 62;
            // 
            // ledControl1
            // 
            this.ledControl1.Location = new System.Drawing.Point(31, 62);
            this.ledControl1.Name = "ledControl1";
            this.ledControl1.Size = new System.Drawing.Size(13, 13);
            this.ledControl1.State = BEMN.Forms.LedState.Signaled;
            this.ledControl1.TabIndex = 62;
            // 
            // _manageLed4
            // 
            this._manageLed4.Location = new System.Drawing.Point(12, 85);
            this._manageLed4.Name = "_manageLed4";
            this._manageLed4.Size = new System.Drawing.Size(13, 13);
            this._manageLed4.State = BEMN.Forms.LedState.Off;
            this._manageLed4.TabIndex = 62;
            // 
            // _manageLed3
            // 
            this._manageLed3.Location = new System.Drawing.Point(229, 195);
            this._manageLed3.Name = "_manageLed3";
            this._manageLed3.Size = new System.Drawing.Size(13, 13);
            this._manageLed3.State = BEMN.Forms.LedState.Off;
            this._manageLed3.TabIndex = 60;
            this._manageLed3.Visible = false;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(28, 151);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(40, 13);
            this.label61.TabIndex = 59;
            this.label61.Text = "�����";
            // 
            // _manageLed7
            // 
            this._manageLed7.Location = new System.Drawing.Point(229, 195);
            this._manageLed7.Name = "_manageLed7";
            this._manageLed7.Size = new System.Drawing.Size(13, 13);
            this._manageLed7.State = BEMN.Forms.LedState.Off;
            this._manageLed7.TabIndex = 58;
            this._manageLed7.Visible = false;
            // 
            // _manageLed6
            // 
            this._manageLed6.Location = new System.Drawing.Point(12, 129);
            this._manageLed6.Name = "_manageLed6";
            this._manageLed6.Size = new System.Drawing.Size(13, 13);
            this._manageLed6.State = BEMN.Forms.LedState.Off;
            this._manageLed6.TabIndex = 56;
            // 
            // _resetJA_But
            // 
            this._resetJA_But.Location = new System.Drawing.Point(167, 127);
            this._resetJA_But.Name = "_resetJA_But";
            this._resetJA_But.Size = new System.Drawing.Size(75, 23);
            this._resetJA_But.TabIndex = 53;
            this._resetJA_But.Text = "��������";
            this._resetJA_But.UseVisualStyleBackColor = true;
            this._resetJA_But.Click += new System.EventHandler(this._resetJA_But_Click);
            // 
            // _resetJS_But
            // 
            this._resetJS_But.Location = new System.Drawing.Point(167, 105);
            this._resetJS_But.Name = "_resetJS_But";
            this._resetJS_But.Size = new System.Drawing.Size(75, 23);
            this._resetJS_But.TabIndex = 52;
            this._resetJS_But.Text = "��������";
            this._resetJS_But.UseVisualStyleBackColor = true;
            this._resetJS_But.Click += new System.EventHandler(this._resetJS_But_Click);
            // 
            // _resetFaultBut
            // 
            this._resetFaultBut.Location = new System.Drawing.Point(167, 83);
            this._resetFaultBut.Name = "_resetFaultBut";
            this._resetFaultBut.Size = new System.Drawing.Size(75, 23);
            this._resetFaultBut.TabIndex = 51;
            this._resetFaultBut.Text = "��������";
            this._resetFaultBut.UseVisualStyleBackColor = true;
            this._resetFaultBut.Click += new System.EventHandler(this._resetFaultBut_Click);
            // 
            // _constraintToggleButton
            // 
            this._constraintToggleButton.Location = new System.Drawing.Point(167, 35);
            this._constraintToggleButton.Name = "_constraintToggleButton";
            this._constraintToggleButton.Size = new System.Drawing.Size(75, 23);
            this._constraintToggleButton.TabIndex = 47;
            this._constraintToggleButton.Text = "������.";
            this._constraintToggleButton.UseVisualStyleBackColor = true;
            this._constraintToggleButton.Click += new System.EventHandler(this._constraintToggleButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(116, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 46;
            this.label6.Text = "- ���������";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(19, 61);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(11, 13);
            this.label104.TabIndex = 46;
            this.label104.Text = "*";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 46;
            this.label5.Text = "- ��������";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(28, 40);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(89, 13);
            this.label50.TabIndex = 46;
            this.label50.Text = "������ �������*";
            // 
            // _indicationResetButton
            // 
            this._indicationResetButton.Location = new System.Drawing.Point(167, 13);
            this._indicationResetButton.Name = "_indicationResetButton";
            this._indicationResetButton.Size = new System.Drawing.Size(75, 23);
            this._indicationResetButton.TabIndex = 45;
            this._indicationResetButton.Text = "��������";
            this._indicationResetButton.UseVisualStyleBackColor = true;
            this._indicationResetButton.Click += new System.EventHandler(this._indicationResetButton_Click);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(28, 18);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(63, 13);
            this.label60.TabIndex = 38;
            this.label60.Text = "���������";
            // 
            // _manageLed2
            // 
            this._manageLed2.Location = new System.Drawing.Point(12, 40);
            this._manageLed2.Name = "_manageLed2";
            this._manageLed2.Size = new System.Drawing.Size(13, 13);
            this._manageLed2.State = BEMN.Forms.LedState.Off;
            this._manageLed2.TabIndex = 32;
            // 
            // _manageLed1
            // 
            this._manageLed1.Location = new System.Drawing.Point(12, 18);
            this._manageLed1.Name = "_manageLed1";
            this._manageLed1.Size = new System.Drawing.Size(13, 13);
            this._manageLed1.State = BEMN.Forms.LedState.Off;
            this._manageLed1.TabIndex = 31;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label97);
            this.groupBox6.Controls.Add(this.label96);
            this.groupBox6.Controls.Add(this.label95);
            this.groupBox6.Controls.Add(this.label103);
            this.groupBox6.Controls.Add(this.label102);
            this.groupBox6.Controls.Add(this.label101);
            this.groupBox6.Controls.Add(this.label100);
            this.groupBox6.Controls.Add(this.label99);
            this.groupBox6.Controls.Add(this.label98);
            this.groupBox6.Controls.Add(this.label62);
            this.groupBox6.Controls.Add(this.label59);
            this.groupBox6.Controls.Add(this._UnBox);
            this.groupBox6.Controls.Add(this._U2Box);
            this.groupBox6.Controls.Add(this._U1Box);
            this.groupBox6.Controls.Add(this._U0Box);
            this.groupBox6.Controls.Add(this._UcaBox);
            this.groupBox6.Controls.Add(this._UbcBox);
            this.groupBox6.Controls.Add(this._UabBox);
            this.groupBox6.Controls.Add(this._UbBox);
            this.groupBox6.Controls.Add(this._UaBox);
            this.groupBox6.Controls.Add(this._F_Box);
            this.groupBox6.Controls.Add(this._UcBox);
            this.groupBox6.Location = new System.Drawing.Point(8, 168);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(174, 158);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "���������";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(6, 113);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(21, 13);
            this.label97.TabIndex = 13;
            this.label97.Text = "Un";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(6, 89);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(21, 13);
            this.label96.TabIndex = 13;
            this.label96.Text = "Uc";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(6, 65);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(21, 13);
            this.label95.TabIndex = 13;
            this.label95.Text = "Ub";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(88, 136);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(21, 13);
            this.label103.TabIndex = 13;
            this.label103.Text = "U2";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(88, 113);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(21, 13);
            this.label102.TabIndex = 13;
            this.label102.Text = "U1";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(88, 89);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(21, 13);
            this.label101.TabIndex = 13;
            this.label101.Text = "U0";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(88, 65);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(27, 13);
            this.label100.TabIndex = 13;
            this.label100.Text = "Uca";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(88, 41);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(27, 13);
            this.label99.TabIndex = 13;
            this.label99.Text = "Ubc";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(88, 17);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(27, 13);
            this.label98.TabIndex = 13;
            this.label98.Text = "Uab";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(6, 41);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(21, 13);
            this.label62.TabIndex = 13;
            this.label62.Text = "Ua";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(6, 17);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(13, 13);
            this.label59.TabIndex = 13;
            this.label59.Text = "F";
            // 
            // _UnBox
            // 
            this._UnBox.Enabled = false;
            this._UnBox.Location = new System.Drawing.Point(33, 110);
            this._UnBox.Name = "_UnBox";
            this._UnBox.Size = new System.Drawing.Size(42, 20);
            this._UnBox.TabIndex = 19;
            // 
            // _U2Box
            // 
            this._U2Box.Enabled = false;
            this._U2Box.Location = new System.Drawing.Point(121, 133);
            this._U2Box.Name = "_U2Box";
            this._U2Box.Size = new System.Drawing.Size(42, 20);
            this._U2Box.TabIndex = 18;
            // 
            // _U1Box
            // 
            this._U1Box.Enabled = false;
            this._U1Box.Location = new System.Drawing.Point(121, 110);
            this._U1Box.Name = "_U1Box";
            this._U1Box.Size = new System.Drawing.Size(42, 20);
            this._U1Box.TabIndex = 17;
            // 
            // _U0Box
            // 
            this._U0Box.Enabled = false;
            this._U0Box.Location = new System.Drawing.Point(121, 86);
            this._U0Box.Name = "_U0Box";
            this._U0Box.Size = new System.Drawing.Size(42, 20);
            this._U0Box.TabIndex = 16;
            // 
            // _UcaBox
            // 
            this._UcaBox.Enabled = false;
            this._UcaBox.Location = new System.Drawing.Point(121, 62);
            this._UcaBox.Name = "_UcaBox";
            this._UcaBox.Size = new System.Drawing.Size(42, 20);
            this._UcaBox.TabIndex = 15;
            // 
            // _UbcBox
            // 
            this._UbcBox.Enabled = false;
            this._UbcBox.Location = new System.Drawing.Point(121, 38);
            this._UbcBox.Name = "_UbcBox";
            this._UbcBox.Size = new System.Drawing.Size(42, 20);
            this._UbcBox.TabIndex = 14;
            // 
            // _UabBox
            // 
            this._UabBox.Enabled = false;
            this._UabBox.Location = new System.Drawing.Point(121, 14);
            this._UabBox.Name = "_UabBox";
            this._UabBox.Size = new System.Drawing.Size(42, 20);
            this._UabBox.TabIndex = 13;
            // 
            // _UbBox
            // 
            this._UbBox.Enabled = false;
            this._UbBox.Location = new System.Drawing.Point(33, 62);
            this._UbBox.Name = "_UbBox";
            this._UbBox.Size = new System.Drawing.Size(42, 20);
            this._UbBox.TabIndex = 12;
            // 
            // _UaBox
            // 
            this._UaBox.Enabled = false;
            this._UaBox.Location = new System.Drawing.Point(33, 38);
            this._UaBox.Name = "_UaBox";
            this._UaBox.Size = new System.Drawing.Size(42, 20);
            this._UaBox.TabIndex = 11;
            // 
            // _F_Box
            // 
            this._F_Box.Enabled = false;
            this._F_Box.Location = new System.Drawing.Point(33, 14);
            this._F_Box.Name = "_F_Box";
            this._F_Box.Size = new System.Drawing.Size(42, 20);
            this._F_Box.TabIndex = 9;
            // 
            // _UcBox
            // 
            this._UcBox.Enabled = false;
            this._UcBox.Location = new System.Drawing.Point(33, 86);
            this._UcBox.Name = "_UcBox";
            this._UcBox.Size = new System.Drawing.Size(42, 20);
            this._UcBox.TabIndex = 6;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label41);
            this.groupBox5.Controls.Add(this._inL_led8);
            this.groupBox5.Controls.Add(this.label42);
            this.groupBox5.Controls.Add(this._inL_led7);
            this.groupBox5.Controls.Add(this.label43);
            this.groupBox5.Controls.Add(this._inL_led6);
            this.groupBox5.Controls.Add(this.label44);
            this.groupBox5.Controls.Add(this._inL_led5);
            this.groupBox5.Controls.Add(this.label45);
            this.groupBox5.Controls.Add(this._inL_led4);
            this.groupBox5.Controls.Add(this.label46);
            this.groupBox5.Controls.Add(this._inL_led3);
            this.groupBox5.Controls.Add(this.label47);
            this.groupBox5.Controls.Add(this._inL_led2);
            this.groupBox5.Controls.Add(this.label48);
            this.groupBox5.Controls.Add(this._inL_led1);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this._inD_led8);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this._inD_led7);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this._inD_led6);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this._inD_led5);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Controls.Add(this._inD_led4);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this._inD_led3);
            this.groupBox5.Controls.Add(this.label31);
            this.groupBox5.Controls.Add(this._inD_led2);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this._inD_led1);
            this.groupBox5.Location = new System.Drawing.Point(223, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(89, 155);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "������� �������";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(64, 135);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(21, 13);
            this.label41.TabIndex = 79;
            this.label41.Text = "�8";
            // 
            // _inL_led8
            // 
            this._inL_led8.Location = new System.Drawing.Point(51, 135);
            this._inL_led8.Name = "_inL_led8";
            this._inL_led8.Size = new System.Drawing.Size(13, 13);
            this._inL_led8.State = BEMN.Forms.LedState.Off;
            this._inL_led8.TabIndex = 78;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(64, 120);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(21, 13);
            this.label42.TabIndex = 77;
            this.label42.Text = "�7";
            // 
            // _inL_led7
            // 
            this._inL_led7.Location = new System.Drawing.Point(51, 120);
            this._inL_led7.Name = "_inL_led7";
            this._inL_led7.Size = new System.Drawing.Size(13, 13);
            this._inL_led7.State = BEMN.Forms.LedState.Off;
            this._inL_led7.TabIndex = 76;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(64, 105);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(21, 13);
            this.label43.TabIndex = 75;
            this.label43.Text = "�6";
            // 
            // _inL_led6
            // 
            this._inL_led6.Location = new System.Drawing.Point(51, 105);
            this._inL_led6.Name = "_inL_led6";
            this._inL_led6.Size = new System.Drawing.Size(13, 13);
            this._inL_led6.State = BEMN.Forms.LedState.Off;
            this._inL_led6.TabIndex = 74;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(64, 90);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(21, 13);
            this.label44.TabIndex = 73;
            this.label44.Text = "�5";
            // 
            // _inL_led5
            // 
            this._inL_led5.Location = new System.Drawing.Point(51, 90);
            this._inL_led5.Name = "_inL_led5";
            this._inL_led5.Size = new System.Drawing.Size(13, 13);
            this._inL_led5.State = BEMN.Forms.LedState.Off;
            this._inL_led5.TabIndex = 72;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(64, 75);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(21, 13);
            this.label45.TabIndex = 71;
            this.label45.Text = "�4";
            // 
            // _inL_led4
            // 
            this._inL_led4.Location = new System.Drawing.Point(51, 75);
            this._inL_led4.Name = "_inL_led4";
            this._inL_led4.Size = new System.Drawing.Size(13, 13);
            this._inL_led4.State = BEMN.Forms.LedState.Off;
            this._inL_led4.TabIndex = 70;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(64, 60);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(21, 13);
            this.label46.TabIndex = 69;
            this.label46.Text = "�3";
            // 
            // _inL_led3
            // 
            this._inL_led3.Location = new System.Drawing.Point(51, 60);
            this._inL_led3.Name = "_inL_led3";
            this._inL_led3.Size = new System.Drawing.Size(13, 13);
            this._inL_led3.State = BEMN.Forms.LedState.Off;
            this._inL_led3.TabIndex = 68;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(64, 45);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(21, 13);
            this.label47.TabIndex = 67;
            this.label47.Text = "�2";
            // 
            // _inL_led2
            // 
            this._inL_led2.Location = new System.Drawing.Point(51, 45);
            this._inL_led2.Name = "_inL_led2";
            this._inL_led2.Size = new System.Drawing.Size(13, 13);
            this._inL_led2.State = BEMN.Forms.LedState.Off;
            this._inL_led2.TabIndex = 66;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(64, 30);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(21, 13);
            this.label48.TabIndex = 65;
            this.label48.Text = "�1";
            // 
            // _inL_led1
            // 
            this._inL_led1.Location = new System.Drawing.Point(51, 30);
            this._inL_led1.Name = "_inL_led1";
            this._inL_led1.Size = new System.Drawing.Size(13, 13);
            this._inL_led1.State = BEMN.Forms.LedState.Off;
            this._inL_led1.TabIndex = 64;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(23, 135);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 13);
            this.label25.TabIndex = 47;
            this.label25.Text = "�8";
            // 
            // _inD_led8
            // 
            this._inD_led8.Location = new System.Drawing.Point(9, 135);
            this._inD_led8.Name = "_inD_led8";
            this._inD_led8.Size = new System.Drawing.Size(13, 13);
            this._inD_led8.State = BEMN.Forms.LedState.Off;
            this._inD_led8.TabIndex = 46;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(23, 120);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(22, 13);
            this.label26.TabIndex = 45;
            this.label26.Text = "�7";
            // 
            // _inD_led7
            // 
            this._inD_led7.Location = new System.Drawing.Point(9, 120);
            this._inD_led7.Name = "_inD_led7";
            this._inD_led7.Size = new System.Drawing.Size(13, 13);
            this._inD_led7.State = BEMN.Forms.LedState.Off;
            this._inD_led7.TabIndex = 44;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(23, 105);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(22, 13);
            this.label27.TabIndex = 43;
            this.label27.Text = "�6";
            // 
            // _inD_led6
            // 
            this._inD_led6.Location = new System.Drawing.Point(9, 105);
            this._inD_led6.Name = "_inD_led6";
            this._inD_led6.Size = new System.Drawing.Size(13, 13);
            this._inD_led6.State = BEMN.Forms.LedState.Off;
            this._inD_led6.TabIndex = 42;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(23, 90);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(22, 13);
            this.label28.TabIndex = 41;
            this.label28.Text = "�5";
            // 
            // _inD_led5
            // 
            this._inD_led5.Location = new System.Drawing.Point(9, 90);
            this._inD_led5.Name = "_inD_led5";
            this._inD_led5.Size = new System.Drawing.Size(13, 13);
            this._inD_led5.State = BEMN.Forms.LedState.Off;
            this._inD_led5.TabIndex = 40;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(23, 75);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(22, 13);
            this.label29.TabIndex = 39;
            this.label29.Text = "�4";
            // 
            // _inD_led4
            // 
            this._inD_led4.Location = new System.Drawing.Point(9, 75);
            this._inD_led4.Name = "_inD_led4";
            this._inD_led4.Size = new System.Drawing.Size(13, 13);
            this._inD_led4.State = BEMN.Forms.LedState.Off;
            this._inD_led4.TabIndex = 38;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(23, 60);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(22, 13);
            this.label30.TabIndex = 37;
            this.label30.Text = "�3";
            // 
            // _inD_led3
            // 
            this._inD_led3.Location = new System.Drawing.Point(9, 60);
            this._inD_led3.Name = "_inD_led3";
            this._inD_led3.Size = new System.Drawing.Size(13, 13);
            this._inD_led3.State = BEMN.Forms.LedState.Off;
            this._inD_led3.TabIndex = 36;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(23, 45);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(22, 13);
            this.label31.TabIndex = 35;
            this.label31.Text = "�2";
            // 
            // _inD_led2
            // 
            this._inD_led2.Location = new System.Drawing.Point(9, 45);
            this._inD_led2.Name = "_inD_led2";
            this._inD_led2.Size = new System.Drawing.Size(13, 13);
            this._inD_led2.State = BEMN.Forms.LedState.Off;
            this._inD_led2.TabIndex = 34;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(23, 30);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(22, 13);
            this.label32.TabIndex = 33;
            this.label32.Text = "�1";
            // 
            // _inD_led1
            // 
            this._inD_led1.Location = new System.Drawing.Point(9, 30);
            this._inD_led1.Name = "_inD_led1";
            this._inD_led1.Size = new System.Drawing.Size(13, 13);
            this._inD_led1.State = BEMN.Forms.LedState.Off;
            this._inD_led1.TabIndex = 32;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this._outLed8);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this._outLed7);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this._outLed6);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this._outLed5);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this._outLed4);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this._outLed3);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this._outLed2);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this._outLed1);
            this.groupBox4.Location = new System.Drawing.Point(149, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(68, 155);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "���. �������";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(28, 135);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 13);
            this.label17.TabIndex = 47;
            this.label17.Text = "���8";
            // 
            // _outLed8
            // 
            this._outLed8.Location = new System.Drawing.Point(9, 135);
            this._outLed8.Name = "_outLed8";
            this._outLed8.Size = new System.Drawing.Size(13, 13);
            this._outLed8.State = BEMN.Forms.LedState.Off;
            this._outLed8.TabIndex = 46;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(28, 120);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 13);
            this.label18.TabIndex = 45;
            this.label18.Text = "���7";
            // 
            // _outLed7
            // 
            this._outLed7.Location = new System.Drawing.Point(9, 120);
            this._outLed7.Name = "_outLed7";
            this._outLed7.Size = new System.Drawing.Size(13, 13);
            this._outLed7.State = BEMN.Forms.LedState.Off;
            this._outLed7.TabIndex = 44;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(28, 105);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 13);
            this.label19.TabIndex = 43;
            this.label19.Text = "���6";
            // 
            // _outLed6
            // 
            this._outLed6.Location = new System.Drawing.Point(9, 105);
            this._outLed6.Name = "_outLed6";
            this._outLed6.Size = new System.Drawing.Size(13, 13);
            this._outLed6.State = BEMN.Forms.LedState.Off;
            this._outLed6.TabIndex = 42;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(28, 90);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 13);
            this.label20.TabIndex = 41;
            this.label20.Text = "���5";
            // 
            // _outLed5
            // 
            this._outLed5.Location = new System.Drawing.Point(9, 90);
            this._outLed5.Name = "_outLed5";
            this._outLed5.Size = new System.Drawing.Size(13, 13);
            this._outLed5.State = BEMN.Forms.LedState.Off;
            this._outLed5.TabIndex = 40;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(28, 75);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(35, 13);
            this.label21.TabIndex = 39;
            this.label21.Text = "���4";
            // 
            // _outLed4
            // 
            this._outLed4.Location = new System.Drawing.Point(9, 75);
            this._outLed4.Name = "_outLed4";
            this._outLed4.Size = new System.Drawing.Size(13, 13);
            this._outLed4.State = BEMN.Forms.LedState.Off;
            this._outLed4.TabIndex = 38;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(28, 60);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 13);
            this.label22.TabIndex = 37;
            this.label22.Text = "���3";
            // 
            // _outLed3
            // 
            this._outLed3.Location = new System.Drawing.Point(9, 60);
            this._outLed3.Name = "_outLed3";
            this._outLed3.Size = new System.Drawing.Size(13, 13);
            this._outLed3.State = BEMN.Forms.LedState.Off;
            this._outLed3.TabIndex = 36;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(28, 45);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 13);
            this.label23.TabIndex = 35;
            this.label23.Text = "���2";
            // 
            // _outLed2
            // 
            this._outLed2.Location = new System.Drawing.Point(9, 45);
            this._outLed2.Name = "_outLed2";
            this._outLed2.Size = new System.Drawing.Size(13, 13);
            this._outLed2.State = BEMN.Forms.LedState.Off;
            this._outLed2.TabIndex = 34;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(28, 30);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 13);
            this.label24.TabIndex = 33;
            this.label24.Text = "���1";
            // 
            // _outLed1
            // 
            this._outLed1.Location = new System.Drawing.Point(9, 30);
            this._outLed1.Name = "_outLed1";
            this._outLed1.Size = new System.Drawing.Size(13, 13);
            this._outLed1.State = BEMN.Forms.LedState.Off;
            this._outLed1.TabIndex = 32;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label33);
            this.groupBox3.Controls.Add(this._releLed16);
            this.groupBox3.Controls.Add(this.label34);
            this.groupBox3.Controls.Add(this._releLed15);
            this.groupBox3.Controls.Add(this.label35);
            this.groupBox3.Controls.Add(this._releLed14);
            this.groupBox3.Controls.Add(this.label36);
            this.groupBox3.Controls.Add(this._releLed13);
            this.groupBox3.Controls.Add(this.label37);
            this.groupBox3.Controls.Add(this._releLed12);
            this.groupBox3.Controls.Add(this.label38);
            this.groupBox3.Controls.Add(this._releLed11);
            this.groupBox3.Controls.Add(this.label39);
            this.groupBox3.Controls.Add(this._releLed10);
            this.groupBox3.Controls.Add(this.label40);
            this.groupBox3.Controls.Add(this._releLed9);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this._releLed8);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this._releLed7);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this._releLed6);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this._releLed5);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this._releLed4);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this._releLed3);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this._releLed2);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this._releLed1);
            this.groupBox3.Location = new System.Drawing.Point(57, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(86, 155);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "����";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(64, 135);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(19, 13);
            this.label33.TabIndex = 47;
            this.label33.Text = "16";
            // 
            // _releLed16
            // 
            this._releLed16.Location = new System.Drawing.Point(45, 135);
            this._releLed16.Name = "_releLed16";
            this._releLed16.Size = new System.Drawing.Size(13, 13);
            this._releLed16.State = BEMN.Forms.LedState.Off;
            this._releLed16.TabIndex = 46;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(64, 120);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(19, 13);
            this.label34.TabIndex = 45;
            this.label34.Text = "15";
            // 
            // _releLed15
            // 
            this._releLed15.Location = new System.Drawing.Point(45, 120);
            this._releLed15.Name = "_releLed15";
            this._releLed15.Size = new System.Drawing.Size(13, 13);
            this._releLed15.State = BEMN.Forms.LedState.Off;
            this._releLed15.TabIndex = 44;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(64, 105);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(19, 13);
            this.label35.TabIndex = 43;
            this.label35.Text = "14";
            // 
            // _releLed14
            // 
            this._releLed14.Location = new System.Drawing.Point(45, 105);
            this._releLed14.Name = "_releLed14";
            this._releLed14.Size = new System.Drawing.Size(13, 13);
            this._releLed14.State = BEMN.Forms.LedState.Off;
            this._releLed14.TabIndex = 42;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(64, 90);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(19, 13);
            this.label36.TabIndex = 41;
            this.label36.Text = "13";
            // 
            // _releLed13
            // 
            this._releLed13.Location = new System.Drawing.Point(45, 90);
            this._releLed13.Name = "_releLed13";
            this._releLed13.Size = new System.Drawing.Size(13, 13);
            this._releLed13.State = BEMN.Forms.LedState.Off;
            this._releLed13.TabIndex = 40;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(64, 75);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(19, 13);
            this.label37.TabIndex = 39;
            this.label37.Text = "12";
            // 
            // _releLed12
            // 
            this._releLed12.Location = new System.Drawing.Point(45, 75);
            this._releLed12.Name = "_releLed12";
            this._releLed12.Size = new System.Drawing.Size(13, 13);
            this._releLed12.State = BEMN.Forms.LedState.Off;
            this._releLed12.TabIndex = 38;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(64, 60);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(19, 13);
            this.label38.TabIndex = 37;
            this.label38.Text = "11";
            // 
            // _releLed11
            // 
            this._releLed11.Location = new System.Drawing.Point(45, 60);
            this._releLed11.Name = "_releLed11";
            this._releLed11.Size = new System.Drawing.Size(13, 13);
            this._releLed11.State = BEMN.Forms.LedState.Off;
            this._releLed11.TabIndex = 36;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(64, 45);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(19, 13);
            this.label39.TabIndex = 35;
            this.label39.Text = "10";
            // 
            // _releLed10
            // 
            this._releLed10.Location = new System.Drawing.Point(45, 45);
            this._releLed10.Name = "_releLed10";
            this._releLed10.Size = new System.Drawing.Size(13, 13);
            this._releLed10.State = BEMN.Forms.LedState.Off;
            this._releLed10.TabIndex = 34;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(64, 30);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(13, 13);
            this.label40.TabIndex = 33;
            this.label40.Text = "9";
            // 
            // _releLed9
            // 
            this._releLed9.Location = new System.Drawing.Point(45, 30);
            this._releLed9.Name = "_releLed9";
            this._releLed9.Size = new System.Drawing.Size(13, 13);
            this._releLed9.State = BEMN.Forms.LedState.Off;
            this._releLed9.TabIndex = 32;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(30, 135);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "8";
            // 
            // _releLed8
            // 
            this._releLed8.Location = new System.Drawing.Point(11, 135);
            this._releLed8.Name = "_releLed8";
            this._releLed8.Size = new System.Drawing.Size(13, 13);
            this._releLed8.State = BEMN.Forms.LedState.Off;
            this._releLed8.TabIndex = 30;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(30, 120);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 29;
            this.label10.Text = "7";
            // 
            // _releLed7
            // 
            this._releLed7.Location = new System.Drawing.Point(11, 120);
            this._releLed7.Name = "_releLed7";
            this._releLed7.Size = new System.Drawing.Size(13, 13);
            this._releLed7.State = BEMN.Forms.LedState.Off;
            this._releLed7.TabIndex = 28;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(30, 105);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "6";
            // 
            // _releLed6
            // 
            this._releLed6.Location = new System.Drawing.Point(11, 105);
            this._releLed6.Name = "_releLed6";
            this._releLed6.Size = new System.Drawing.Size(13, 13);
            this._releLed6.State = BEMN.Forms.LedState.Off;
            this._releLed6.TabIndex = 26;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(30, 90);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 13);
            this.label12.TabIndex = 25;
            this.label12.Text = "5";
            // 
            // _releLed5
            // 
            this._releLed5.Location = new System.Drawing.Point(11, 90);
            this._releLed5.Name = "_releLed5";
            this._releLed5.Size = new System.Drawing.Size(13, 13);
            this._releLed5.State = BEMN.Forms.LedState.Off;
            this._releLed5.TabIndex = 24;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(30, 75);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "4";
            // 
            // _releLed4
            // 
            this._releLed4.Location = new System.Drawing.Point(11, 75);
            this._releLed4.Name = "_releLed4";
            this._releLed4.Size = new System.Drawing.Size(13, 13);
            this._releLed4.State = BEMN.Forms.LedState.Off;
            this._releLed4.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(30, 60);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(13, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "3";
            // 
            // _releLed3
            // 
            this._releLed3.Location = new System.Drawing.Point(11, 60);
            this._releLed3.Name = "_releLed3";
            this._releLed3.Size = new System.Drawing.Size(13, 13);
            this._releLed3.State = BEMN.Forms.LedState.Off;
            this._releLed3.TabIndex = 20;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(30, 45);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 13);
            this.label15.TabIndex = 19;
            this.label15.Text = "2";
            // 
            // _releLed2
            // 
            this._releLed2.Location = new System.Drawing.Point(11, 45);
            this._releLed2.Name = "_releLed2";
            this._releLed2.Size = new System.Drawing.Size(13, 13);
            this._releLed2.State = BEMN.Forms.LedState.Off;
            this._releLed2.TabIndex = 18;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(30, 30);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(13, 13);
            this.label16.TabIndex = 17;
            this.label16.Text = "1";
            // 
            // _releLed1
            // 
            this._releLed1.Location = new System.Drawing.Point(11, 30);
            this._releLed1.Name = "_releLed1";
            this._releLed1.Size = new System.Drawing.Size(13, 13);
            this._releLed1.State = BEMN.Forms.LedState.Off;
            this._releLed1.TabIndex = 16;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this._indLed6);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this._indLed5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this._indLed4);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this._indLed3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this._indLed2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this._indLed1);
            this.groupBox2.Location = new System.Drawing.Point(8, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(43, 155);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "���.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 131);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "6";
            // 
            // _indLed6
            // 
            this._indLed6.Location = new System.Drawing.Point(6, 130);
            this._indLed6.Name = "_indLed6";
            this._indLed6.Size = new System.Drawing.Size(13, 13);
            this._indLed6.State = BEMN.Forms.LedState.Off;
            this._indLed6.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 111);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "5";
            // 
            // _indLed5
            // 
            this._indLed5.Location = new System.Drawing.Point(6, 110);
            this._indLed5.Name = "_indLed5";
            this._indLed5.Size = new System.Drawing.Size(13, 13);
            this._indLed5.State = BEMN.Forms.LedState.Off;
            this._indLed5.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "4";
            // 
            // _indLed4
            // 
            this._indLed4.Location = new System.Drawing.Point(6, 90);
            this._indLed4.Name = "_indLed4";
            this._indLed4.Size = new System.Drawing.Size(13, 13);
            this._indLed4.State = BEMN.Forms.LedState.Off;
            this._indLed4.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "3";
            // 
            // _indLed3
            // 
            this._indLed3.Location = new System.Drawing.Point(6, 70);
            this._indLed3.Name = "_indLed3";
            this._indLed3.Size = new System.Drawing.Size(13, 13);
            this._indLed3.State = BEMN.Forms.LedState.Off;
            this._indLed3.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "2";
            // 
            // _indLed2
            // 
            this._indLed2.Location = new System.Drawing.Point(6, 50);
            this._indLed2.Name = "_indLed2";
            this._indLed2.Size = new System.Drawing.Size(13, 13);
            this._indLed2.State = BEMN.Forms.LedState.Off;
            this._indLed2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "1";
            // 
            // _indLed1
            // 
            this._indLed1.Location = new System.Drawing.Point(6, 30);
            this._indLed1.Name = "_indLed1";
            this._indLed1.Size = new System.Drawing.Size(13, 13);
            this._indLed1.State = BEMN.Forms.LedState.Off;
            this._indLed1.TabIndex = 0;
            // 
            // _diskretPage
            // 
            this._diskretPage.Controls.Add(this.groupBox13);
            this._diskretPage.Controls.Add(this.groupBox7);
            this._diskretPage.Controls.Add(this.groupBox19);
            this._diskretPage.Controls.Add(this.groupBox18);
            this._diskretPage.Controls.Add(this.groupBox17);
            this._diskretPage.Controls.Add(this.groupBox16);
            this._diskretPage.Controls.Add(this.groupBox11);
            this._diskretPage.Controls.Add(this.groupBox12);
            this._diskretPage.Controls.Add(this.groupBox10);
            this._diskretPage.Controls.Add(this.groupBox9);
            this._diskretPage.ImageIndex = 1;
            this._diskretPage.Location = new System.Drawing.Point(4, 23);
            this._diskretPage.Name = "_diskretPage";
            this._diskretPage.Padding = new System.Windows.Forms.Padding(3);
            this._diskretPage.Size = new System.Drawing.Size(574, 401);
            this._diskretPage.TabIndex = 1;
            this._diskretPage.Text = "���������� ��";
            this._diskretPage.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this._FminLed8);
            this.groupBox13.Controls.Add(this._FminLed7);
            this.groupBox13.Controls.Add(this._FminLed6);
            this.groupBox13.Controls.Add(this._FminLed5);
            this.groupBox13.Controls.Add(this.label87);
            this.groupBox13.Controls.Add(this.label88);
            this.groupBox13.Controls.Add(this.label89);
            this.groupBox13.Controls.Add(this.label90);
            this.groupBox13.Controls.Add(this.label91);
            this.groupBox13.Controls.Add(this._FminLed4);
            this.groupBox13.Controls.Add(this._FminLed3);
            this.groupBox13.Controls.Add(this._FminLed2);
            this.groupBox13.Controls.Add(this.label92);
            this.groupBox13.Controls.Add(this._FminLed1);
            this.groupBox13.Location = new System.Drawing.Point(480, 7);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(89, 99);
            this.groupBox13.TabIndex = 14;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "F min";
            // 
            // _FminLed8
            // 
            this._FminLed8.Location = new System.Drawing.Point(34, 79);
            this._FminLed8.Name = "_FminLed8";
            this._FminLed8.Size = new System.Drawing.Size(13, 13);
            this._FminLed8.State = BEMN.Forms.LedState.Off;
            this._FminLed8.TabIndex = 24;
            // 
            // _FminLed7
            // 
            this._FminLed7.Location = new System.Drawing.Point(8, 79);
            this._FminLed7.Name = "_FminLed7";
            this._FminLed7.Size = new System.Drawing.Size(13, 13);
            this._FminLed7.State = BEMN.Forms.LedState.Off;
            this._FminLed7.TabIndex = 23;
            // 
            // _FminLed6
            // 
            this._FminLed6.Location = new System.Drawing.Point(34, 63);
            this._FminLed6.Name = "_FminLed6";
            this._FminLed6.Size = new System.Drawing.Size(13, 13);
            this._FminLed6.State = BEMN.Forms.LedState.Off;
            this._FminLed6.TabIndex = 22;
            // 
            // _FminLed5
            // 
            this._FminLed5.Location = new System.Drawing.Point(8, 63);
            this._FminLed5.Name = "_FminLed5";
            this._FminLed5.Size = new System.Drawing.Size(13, 13);
            this._FminLed5.State = BEMN.Forms.LedState.Off;
            this._FminLed5.TabIndex = 21;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(48, 79);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(37, 13);
            this.label87.TabIndex = 20;
            this.label87.Text = "F<<<<";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(48, 63);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(31, 13);
            this.label88.TabIndex = 19;
            this.label88.Text = "F<<<";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(48, 47);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(25, 13);
            this.label89.TabIndex = 18;
            this.label89.Text = "F<<";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(48, 31);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(19, 13);
            this.label90.TabIndex = 17;
            this.label90.Text = "F<";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(26, 14);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(35, 13);
            this.label91.TabIndex = 16;
            this.label91.Text = "����.";
            // 
            // _FminLed4
            // 
            this._FminLed4.Location = new System.Drawing.Point(34, 47);
            this._FminLed4.Name = "_FminLed4";
            this._FminLed4.Size = new System.Drawing.Size(13, 13);
            this._FminLed4.State = BEMN.Forms.LedState.Off;
            this._FminLed4.TabIndex = 6;
            // 
            // _FminLed3
            // 
            this._FminLed3.Location = new System.Drawing.Point(8, 47);
            this._FminLed3.Name = "_FminLed3";
            this._FminLed3.Size = new System.Drawing.Size(13, 13);
            this._FminLed3.State = BEMN.Forms.LedState.Off;
            this._FminLed3.TabIndex = 4;
            // 
            // _FminLed2
            // 
            this._FminLed2.Location = new System.Drawing.Point(34, 31);
            this._FminLed2.Name = "_FminLed2";
            this._FminLed2.Size = new System.Drawing.Size(13, 13);
            this._FminLed2.State = BEMN.Forms.LedState.Off;
            this._FminLed2.TabIndex = 2;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(5, 14);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(23, 13);
            this.label92.TabIndex = 1;
            this.label92.Text = "��";
            // 
            // _FminLed1
            // 
            this._FminLed1.Location = new System.Drawing.Point(8, 31);
            this._FminLed1.Name = "_FminLed1";
            this._FminLed1.Size = new System.Drawing.Size(13, 13);
            this._FminLed1.State = BEMN.Forms.LedState.Off;
            this._FminLed1.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._FmaxLed8);
            this.groupBox7.Controls.Add(this._FmaxLed7);
            this.groupBox7.Controls.Add(this._FmaxLed6);
            this.groupBox7.Controls.Add(this._FmaxLed5);
            this.groupBox7.Controls.Add(this.label49);
            this.groupBox7.Controls.Add(this.label52);
            this.groupBox7.Controls.Add(this.label54);
            this.groupBox7.Controls.Add(this.label56);
            this.groupBox7.Controls.Add(this.label85);
            this.groupBox7.Controls.Add(this._FmaxLed4);
            this.groupBox7.Controls.Add(this._FmaxLed3);
            this.groupBox7.Controls.Add(this._FmaxLed2);
            this.groupBox7.Controls.Add(this.label86);
            this.groupBox7.Controls.Add(this._FmaxLed1);
            this.groupBox7.Location = new System.Drawing.Point(388, 7);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(89, 99);
            this.groupBox7.TabIndex = 13;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "F max";
            // 
            // _FmaxLed8
            // 
            this._FmaxLed8.Location = new System.Drawing.Point(34, 79);
            this._FmaxLed8.Name = "_FmaxLed8";
            this._FmaxLed8.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed8.State = BEMN.Forms.LedState.Off;
            this._FmaxLed8.TabIndex = 24;
            // 
            // _FmaxLed7
            // 
            this._FmaxLed7.Location = new System.Drawing.Point(8, 79);
            this._FmaxLed7.Name = "_FmaxLed7";
            this._FmaxLed7.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed7.State = BEMN.Forms.LedState.Off;
            this._FmaxLed7.TabIndex = 23;
            // 
            // _FmaxLed6
            // 
            this._FmaxLed6.Location = new System.Drawing.Point(34, 63);
            this._FmaxLed6.Name = "_FmaxLed6";
            this._FmaxLed6.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed6.State = BEMN.Forms.LedState.Off;
            this._FmaxLed6.TabIndex = 22;
            // 
            // _FmaxLed5
            // 
            this._FmaxLed5.Location = new System.Drawing.Point(8, 63);
            this._FmaxLed5.Name = "_FmaxLed5";
            this._FmaxLed5.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed5.State = BEMN.Forms.LedState.Off;
            this._FmaxLed5.TabIndex = 21;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(48, 79);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(37, 13);
            this.label49.TabIndex = 20;
            this.label49.Text = "F>>>>";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(48, 63);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(31, 13);
            this.label52.TabIndex = 19;
            this.label52.Text = "F>>>";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(48, 47);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(25, 13);
            this.label54.TabIndex = 18;
            this.label54.Text = "F>>";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(48, 31);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(19, 13);
            this.label56.TabIndex = 17;
            this.label56.Text = "F>";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(26, 14);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(35, 13);
            this.label85.TabIndex = 16;
            this.label85.Text = "����.";
            // 
            // _FmaxLed4
            // 
            this._FmaxLed4.Location = new System.Drawing.Point(34, 47);
            this._FmaxLed4.Name = "_FmaxLed4";
            this._FmaxLed4.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed4.State = BEMN.Forms.LedState.Off;
            this._FmaxLed4.TabIndex = 6;
            // 
            // _FmaxLed3
            // 
            this._FmaxLed3.Location = new System.Drawing.Point(8, 47);
            this._FmaxLed3.Name = "_FmaxLed3";
            this._FmaxLed3.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed3.State = BEMN.Forms.LedState.Off;
            this._FmaxLed3.TabIndex = 4;
            // 
            // _FmaxLed2
            // 
            this._FmaxLed2.Location = new System.Drawing.Point(34, 31);
            this._FmaxLed2.Name = "_FmaxLed2";
            this._FmaxLed2.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed2.State = BEMN.Forms.LedState.Off;
            this._FmaxLed2.TabIndex = 2;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(5, 14);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(23, 13);
            this.label86.TabIndex = 1;
            this.label86.Text = "��";
            // 
            // _FmaxLed1
            // 
            this._FmaxLed1.Location = new System.Drawing.Point(8, 31);
            this._FmaxLed1.Name = "_FmaxLed1";
            this._FmaxLed1.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed1.State = BEMN.Forms.LedState.Off;
            this._FmaxLed1.TabIndex = 0;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this._faultSignalLed16);
            this.groupBox19.Controls.Add(this.label130);
            this.groupBox19.Controls.Add(this._faultSignalLed15);
            this.groupBox19.Controls.Add(this.label131);
            this.groupBox19.Controls.Add(this._faultSignalLed14);
            this.groupBox19.Controls.Add(this.label132);
            this.groupBox19.Controls.Add(this._faultSignalLed13);
            this.groupBox19.Controls.Add(this.label133);
            this.groupBox19.Controls.Add(this._faultSignalLed12);
            this.groupBox19.Controls.Add(this.label134);
            this.groupBox19.Controls.Add(this._faultSignalLed11);
            this.groupBox19.Controls.Add(this.label135);
            this.groupBox19.Controls.Add(this._faultSignalLed10);
            this.groupBox19.Controls.Add(this.label136);
            this.groupBox19.Controls.Add(this._faultSignalLed9);
            this.groupBox19.Location = new System.Drawing.Point(454, 112);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(115, 243);
            this.groupBox19.TabIndex = 12;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "������� ������������� 2";
            // 
            // _faultSignalLed16
            // 
            this._faultSignalLed16.Location = new System.Drawing.Point(6, 219);
            this._faultSignalLed16.Name = "_faultSignalLed16";
            this._faultSignalLed16.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed16.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed16.TabIndex = 30;
            this._faultSignalLed16.Visible = false;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(20, 193);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(91, 13);
            this.label130.TabIndex = 29;
            this.label130.Text = "�.������������";
            // 
            // _faultSignalLed15
            // 
            this._faultSignalLed15.Location = new System.Drawing.Point(6, 193);
            this._faultSignalLed15.Name = "_faultSignalLed15";
            this._faultSignalLed15.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed15.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed15.TabIndex = 28;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(21, 166);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(50, 13);
            this.label131.TabIndex = 27;
            this.label131.Text = "�. �����";
            // 
            // _faultSignalLed14
            // 
            this._faultSignalLed14.Location = new System.Drawing.Point(6, 167);
            this._faultSignalLed14.Name = "_faultSignalLed14";
            this._faultSignalLed14.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed14.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed14.TabIndex = 26;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(21, 140);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(39, 13);
            this.label132.TabIndex = 25;
            this.label132.Text = "�. ��";
            // 
            // _faultSignalLed13
            // 
            this._faultSignalLed13.Location = new System.Drawing.Point(6, 141);
            this._faultSignalLed13.Name = "_faultSignalLed13";
            this._faultSignalLed13.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed13.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed13.TabIndex = 24;
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(21, 114);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(39, 13);
            this.label133.TabIndex = 23;
            this.label133.Text = "�. ��";
            // 
            // _faultSignalLed12
            // 
            this._faultSignalLed12.Location = new System.Drawing.Point(6, 115);
            this._faultSignalLed12.Name = "_faultSignalLed12";
            this._faultSignalLed12.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed12.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed12.TabIndex = 22;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(21, 63);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(84, 13);
            this.label134.TabIndex = 21;
            this.label134.Text = "�. �����. ���";
            // 
            // _faultSignalLed11
            // 
            this._faultSignalLed11.Location = new System.Drawing.Point(6, 89);
            this._faultSignalLed11.Name = "_faultSignalLed11";
            this._faultSignalLed11.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed11.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed11.TabIndex = 20;
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(21, 89);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(78, 13);
            this.label135.TabIndex = 19;
            this.label135.Text = "�. ��. �������";
            // 
            // _faultSignalLed10
            // 
            this._faultSignalLed10.Location = new System.Drawing.Point(6, 63);
            this._faultSignalLed10.Name = "_faultSignalLed10";
            this._faultSignalLed10.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed10.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed10.TabIndex = 18;
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(21, 37);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(90, 13);
            this.label136.TabIndex = 17;
            this.label136.Text = "������ �������";
            // 
            // _faultSignalLed9
            // 
            this._faultSignalLed9.Location = new System.Drawing.Point(6, 37);
            this._faultSignalLed9.Name = "_faultSignalLed9";
            this._faultSignalLed9.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed9.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed9.TabIndex = 16;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.label121);
            this.groupBox18.Controls.Add(this._faultSignalLed8);
            this.groupBox18.Controls.Add(this.label122);
            this.groupBox18.Controls.Add(this._faultSignalLed7);
            this.groupBox18.Controls.Add(this.label123);
            this.groupBox18.Controls.Add(this._faultSignalLed6);
            this.groupBox18.Controls.Add(this.label124);
            this.groupBox18.Controls.Add(this._faultSignalLed5);
            this.groupBox18.Controls.Add(this._faultSignalLed4);
            this.groupBox18.Controls.Add(this.label126);
            this.groupBox18.Controls.Add(this._faultSignalLed3);
            this.groupBox18.Controls.Add(this.label127);
            this.groupBox18.Controls.Add(this._faultSignalLed2);
            this.groupBox18.Controls.Add(this.label128);
            this.groupBox18.Controls.Add(this._faultSignalLed1);
            this.groupBox18.Location = new System.Drawing.Point(322, 112);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(115, 243);
            this.groupBox18.TabIndex = 11;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "������� ������������� 1";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(21, 193);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(32, 13);
            this.label121.TabIndex = 31;
            this.label121.Text = "���";
            // 
            // _faultSignalLed8
            // 
            this._faultSignalLed8.Location = new System.Drawing.Point(6, 193);
            this._faultSignalLed8.Name = "_faultSignalLed8";
            this._faultSignalLed8.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed8.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed8.TabIndex = 30;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(21, 167);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(36, 13);
            this.label122.TabIndex = 29;
            this.label122.Text = "���2";
            // 
            // _faultSignalLed7
            // 
            this._faultSignalLed7.Location = new System.Drawing.Point(6, 167);
            this._faultSignalLed7.Name = "_faultSignalLed7";
            this._faultSignalLed7.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed7.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed7.TabIndex = 28;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(21, 140);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(36, 13);
            this.label123.TabIndex = 27;
            this.label123.Text = "���1";
            // 
            // _faultSignalLed6
            // 
            this._faultSignalLed6.Location = new System.Drawing.Point(6, 141);
            this._faultSignalLed6.Name = "_faultSignalLed6";
            this._faultSignalLed6.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed6.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed6.TabIndex = 26;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(21, 114);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(33, 13);
            this.label124.TabIndex = 25;
            this.label124.Text = "��� ";
            // 
            // _faultSignalLed5
            // 
            this._faultSignalLed5.Location = new System.Drawing.Point(6, 115);
            this._faultSignalLed5.Name = "_faultSignalLed5";
            this._faultSignalLed5.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed5.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed5.TabIndex = 24;
            // 
            // _faultSignalLed4
            // 
            this._faultSignalLed4.Location = new System.Drawing.Point(6, 218);
            this._faultSignalLed4.Name = "_faultSignalLed4";
            this._faultSignalLed4.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed4.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed4.TabIndex = 22;
            this._faultSignalLed4.Visible = false;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(21, 63);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(70, 13);
            this.label126.TabIndex = 21;
            this.label126.Text = "�.  ���� I2c";
            // 
            // _faultSignalLed3
            // 
            this._faultSignalLed3.Location = new System.Drawing.Point(6, 89);
            this._faultSignalLed3.Name = "_faultSignalLed3";
            this._faultSignalLed3.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed3.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed3.TabIndex = 20;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(21, 89);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(88, 13);
            this.label127.TabIndex = 19;
            this.label127.Text = "�. �����������";
            // 
            // _faultSignalLed2
            // 
            this._faultSignalLed2.Location = new System.Drawing.Point(6, 63);
            this._faultSignalLed2.Name = "_faultSignalLed2";
            this._faultSignalLed2.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed2.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed2.TabIndex = 18;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(21, 37);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(73, 13);
            this.label128.TabIndex = 17;
            this.label128.Text = "������ ���";
            // 
            // _faultSignalLed1
            // 
            this._faultSignalLed1.Location = new System.Drawing.Point(6, 37);
            this._faultSignalLed1.Name = "_faultSignalLed1";
            this._faultSignalLed1.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed1.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed1.TabIndex = 16;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.label113);
            this.groupBox17.Controls.Add(this._faultStateLed8);
            this.groupBox17.Controls.Add(this.label114);
            this.groupBox17.Controls.Add(this._faultStateLed7);
            this.groupBox17.Controls.Add(this.label115);
            this.groupBox17.Controls.Add(this._faultStateLed6);
            this.groupBox17.Controls.Add(this.label116);
            this.groupBox17.Controls.Add(this._faultStateLed5);
            this.groupBox17.Controls.Add(this.label117);
            this.groupBox17.Controls.Add(this._faultStateLed4);
            this.groupBox17.Controls.Add(this.label118);
            this.groupBox17.Controls.Add(this._faultStateLed3);
            this.groupBox17.Controls.Add(this.label119);
            this.groupBox17.Controls.Add(this._faultStateLed2);
            this.groupBox17.Controls.Add(this.label120);
            this.groupBox17.Controls.Add(this._faultStateLed1);
            this.groupBox17.Location = new System.Drawing.Point(194, 112);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(115, 243);
            this.groupBox17.TabIndex = 10;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "��������� �������������";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(21, 218);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(82, 13);
            this.label113.TabIndex = 31;
            this.label113.Text = "������� �. ��";
            // 
            // _faultStateLed8
            // 
            this._faultStateLed8.Location = new System.Drawing.Point(6, 219);
            this._faultStateLed8.Name = "_faultStateLed8";
            this._faultStateLed8.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed8.State = BEMN.Forms.LedState.Off;
            this._faultStateLed8.TabIndex = 30;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(21, 191);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(86, 13);
            this.label114.TabIndex = 29;
            this.label114.Text = "�������������";
            // 
            // _faultStateLed7
            // 
            this._faultStateLed7.Location = new System.Drawing.Point(6, 193);
            this._faultStateLed7.Name = "_faultStateLed7";
            this._faultStateLed7.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed7.State = BEMN.Forms.LedState.Off;
            this._faultStateLed7.TabIndex = 28;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(21, 166);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(87, 13);
            this.label115.TabIndex = 27;
            this.label115.Text = "����������� U";
            // 
            // _faultStateLed6
            // 
            this._faultStateLed6.Location = new System.Drawing.Point(6, 167);
            this._faultStateLed6.Name = "_faultStateLed6";
            this._faultStateLed6.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed6.State = BEMN.Forms.LedState.Off;
            this._faultStateLed6.TabIndex = 26;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(21, 140);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(91, 13);
            this.label116.TabIndex = 25;
            this.label116.Text = "F ��� ���������";
            // 
            // _faultStateLed5
            // 
            this._faultStateLed5.Location = new System.Drawing.Point(6, 141);
            this._faultStateLed5.Name = "_faultStateLed5";
            this._faultStateLed5.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed5.State = BEMN.Forms.LedState.Off;
            this._faultStateLed5.TabIndex = 24;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(21, 114);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(40, 13);
            this.label117.TabIndex = 23;
            this.label117.Text = "U < 5�";
            // 
            // _faultStateLed4
            // 
            this._faultStateLed4.Location = new System.Drawing.Point(6, 115);
            this._faultStateLed4.Name = "_faultStateLed4";
            this._faultStateLed4.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed4.State = BEMN.Forms.LedState.Off;
            this._faultStateLed4.TabIndex = 22;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(21, 63);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(58, 13);
            this.label118.TabIndex = 21;
            this.label118.Text = "�. ������";
            // 
            // _faultStateLed3
            // 
            this._faultStateLed3.Location = new System.Drawing.Point(6, 89);
            this._faultStateLed3.Name = "_faultStateLed3";
            this._faultStateLed3.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed3.State = BEMN.Forms.LedState.Off;
            this._faultStateLed3.TabIndex = 20;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(21, 89);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(83, 13);
            this.label119.TabIndex = 19;
            this.label119.Text = "�������� ���";
            // 
            // _faultStateLed2
            // 
            this._faultStateLed2.Location = new System.Drawing.Point(6, 63);
            this._faultStateLed2.Name = "_faultStateLed2";
            this._faultStateLed2.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed2.State = BEMN.Forms.LedState.Off;
            this._faultStateLed2.TabIndex = 18;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(21, 37);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(78, 13);
            this.label120.TabIndex = 17;
            this.label120.Text = "�. ����������";
            // 
            // _faultStateLed1
            // 
            this._faultStateLed1.Location = new System.Drawing.Point(6, 37);
            this._faultStateLed1.Name = "_faultStateLed1";
            this._faultStateLed1.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed1.State = BEMN.Forms.LedState.Off;
            this._faultStateLed1.TabIndex = 16;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.label105);
            this.groupBox16.Controls.Add(this._extDefenseLed8);
            this.groupBox16.Controls.Add(this.label106);
            this.groupBox16.Controls.Add(this._extDefenseLed7);
            this.groupBox16.Controls.Add(this.label107);
            this.groupBox16.Controls.Add(this._extDefenseLed6);
            this.groupBox16.Controls.Add(this.label108);
            this.groupBox16.Controls.Add(this._extDefenseLed5);
            this.groupBox16.Controls.Add(this.label109);
            this.groupBox16.Controls.Add(this._extDefenseLed4);
            this.groupBox16.Controls.Add(this.label110);
            this.groupBox16.Controls.Add(this._extDefenseLed3);
            this.groupBox16.Controls.Add(this.label111);
            this.groupBox16.Controls.Add(this._extDefenseLed2);
            this.groupBox16.Controls.Add(this.label112);
            this.groupBox16.Controls.Add(this._extDefenseLed1);
            this.groupBox16.Location = new System.Drawing.Point(10, 112);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(180, 109);
            this.groupBox16.TabIndex = 9;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "������� ������";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(125, 87);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(36, 13);
            this.label105.TabIndex = 47;
            this.label105.Text = "�� - 8";
            // 
            // _extDefenseLed8
            // 
            this._extDefenseLed8.Location = new System.Drawing.Point(106, 86);
            this._extDefenseLed8.Name = "_extDefenseLed8";
            this._extDefenseLed8.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed8.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed8.TabIndex = 46;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(125, 65);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(36, 13);
            this.label106.TabIndex = 45;
            this.label106.Text = "�� - 7";
            // 
            // _extDefenseLed7
            // 
            this._extDefenseLed7.Location = new System.Drawing.Point(106, 65);
            this._extDefenseLed7.Name = "_extDefenseLed7";
            this._extDefenseLed7.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed7.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed7.TabIndex = 44;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(125, 44);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(36, 13);
            this.label107.TabIndex = 43;
            this.label107.Text = "�� - 6";
            // 
            // _extDefenseLed6
            // 
            this._extDefenseLed6.Location = new System.Drawing.Point(106, 44);
            this._extDefenseLed6.Name = "_extDefenseLed6";
            this._extDefenseLed6.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed6.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed6.TabIndex = 42;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(125, 23);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(36, 13);
            this.label108.TabIndex = 41;
            this.label108.Text = "�� - 5";
            // 
            // _extDefenseLed5
            // 
            this._extDefenseLed5.Location = new System.Drawing.Point(106, 23);
            this._extDefenseLed5.Name = "_extDefenseLed5";
            this._extDefenseLed5.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed5.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed5.TabIndex = 40;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(28, 87);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(36, 13);
            this.label109.TabIndex = 39;
            this.label109.Text = "�� - 4";
            // 
            // _extDefenseLed4
            // 
            this._extDefenseLed4.Location = new System.Drawing.Point(9, 86);
            this._extDefenseLed4.Name = "_extDefenseLed4";
            this._extDefenseLed4.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed4.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed4.TabIndex = 38;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(28, 65);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(36, 13);
            this.label110.TabIndex = 37;
            this.label110.Text = "�� - 3";
            // 
            // _extDefenseLed3
            // 
            this._extDefenseLed3.Location = new System.Drawing.Point(9, 65);
            this._extDefenseLed3.Name = "_extDefenseLed3";
            this._extDefenseLed3.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed3.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed3.TabIndex = 36;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(28, 44);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(36, 13);
            this.label111.TabIndex = 35;
            this.label111.Text = "�� - 2";
            // 
            // _extDefenseLed2
            // 
            this._extDefenseLed2.Location = new System.Drawing.Point(9, 44);
            this._extDefenseLed2.Name = "_extDefenseLed2";
            this._extDefenseLed2.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed2.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed2.TabIndex = 34;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(28, 23);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(36, 13);
            this.label112.TabIndex = 33;
            this.label112.Text = "�� - 1";
            // 
            // _extDefenseLed1
            // 
            this._extDefenseLed1.Location = new System.Drawing.Point(9, 23);
            this._extDefenseLed1.Name = "_extDefenseLed1";
            this._extDefenseLed1.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed1.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed1.TabIndex = 32;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._U12Led8);
            this.groupBox11.Controls.Add(this._U12Led7);
            this.groupBox11.Controls.Add(this._U12Led6);
            this.groupBox11.Controls.Add(this._U12Led5);
            this.groupBox11.Controls.Add(this.label67);
            this.groupBox11.Controls.Add(this.label74);
            this.groupBox11.Controls.Add(this.label75);
            this.groupBox11.Controls.Add(this.label76);
            this.groupBox11.Controls.Add(this.label77);
            this.groupBox11.Controls.Add(this._U12Led4);
            this.groupBox11.Controls.Add(this._U12Led3);
            this.groupBox11.Controls.Add(this._U12Led2);
            this.groupBox11.Controls.Add(this.label78);
            this.groupBox11.Controls.Add(this._U12Led1);
            this.groupBox11.Location = new System.Drawing.Point(296, 7);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(89, 99);
            this.groupBox11.TabIndex = 5;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "U1 � U2 ";
            // 
            // _U12Led8
            // 
            this._U12Led8.Location = new System.Drawing.Point(34, 79);
            this._U12Led8.Name = "_U12Led8";
            this._U12Led8.Size = new System.Drawing.Size(13, 13);
            this._U12Led8.State = BEMN.Forms.LedState.Off;
            this._U12Led8.TabIndex = 24;
            // 
            // _U12Led7
            // 
            this._U12Led7.Location = new System.Drawing.Point(8, 79);
            this._U12Led7.Name = "_U12Led7";
            this._U12Led7.Size = new System.Drawing.Size(13, 13);
            this._U12Led7.State = BEMN.Forms.LedState.Off;
            this._U12Led7.TabIndex = 23;
            // 
            // _U12Led6
            // 
            this._U12Led6.Location = new System.Drawing.Point(34, 63);
            this._U12Led6.Name = "_U12Led6";
            this._U12Led6.Size = new System.Drawing.Size(13, 13);
            this._U12Led6.State = BEMN.Forms.LedState.Off;
            this._U12Led6.TabIndex = 22;
            // 
            // _U12Led5
            // 
            this._U12Led5.Location = new System.Drawing.Point(8, 63);
            this._U12Led5.Name = "_U12Led5";
            this._U12Led5.Size = new System.Drawing.Size(13, 13);
            this._U12Led5.State = BEMN.Forms.LedState.Off;
            this._U12Led5.TabIndex = 21;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(48, 79);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(33, 13);
            this.label67.TabIndex = 20;
            this.label67.Text = "U2>>";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(48, 63);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(27, 13);
            this.label74.TabIndex = 19;
            this.label74.Text = "U2>";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(48, 47);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(33, 13);
            this.label75.TabIndex = 18;
            this.label75.Text = "U1<<";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(48, 31);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(27, 13);
            this.label76.TabIndex = 17;
            this.label76.Text = "U1<";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(26, 14);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(35, 13);
            this.label77.TabIndex = 16;
            this.label77.Text = "����.";
            // 
            // _U12Led4
            // 
            this._U12Led4.Location = new System.Drawing.Point(34, 47);
            this._U12Led4.Name = "_U12Led4";
            this._U12Led4.Size = new System.Drawing.Size(13, 13);
            this._U12Led4.State = BEMN.Forms.LedState.Off;
            this._U12Led4.TabIndex = 6;
            // 
            // _U12Led3
            // 
            this._U12Led3.Location = new System.Drawing.Point(8, 47);
            this._U12Led3.Name = "_U12Led3";
            this._U12Led3.Size = new System.Drawing.Size(13, 13);
            this._U12Led3.State = BEMN.Forms.LedState.Off;
            this._U12Led3.TabIndex = 4;
            // 
            // _U12Led2
            // 
            this._U12Led2.Location = new System.Drawing.Point(34, 31);
            this._U12Led2.Name = "_U12Led2";
            this._U12Led2.Size = new System.Drawing.Size(13, 13);
            this._U12Led2.State = BEMN.Forms.LedState.Off;
            this._U12Led2.TabIndex = 2;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(5, 14);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(23, 13);
            this.label78.TabIndex = 1;
            this.label78.Text = "��";
            // 
            // _U12Led1
            // 
            this._U12Led1.Location = new System.Drawing.Point(8, 31);
            this._U12Led1.Name = "_U12Led1";
            this._U12Led1.Size = new System.Drawing.Size(13, 13);
            this._U12Led1.State = BEMN.Forms.LedState.Off;
            this._U12Led1.TabIndex = 0;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._U0maxLed8);
            this.groupBox12.Controls.Add(this._U0maxLed7);
            this.groupBox12.Controls.Add(this._U0maxLed6);
            this.groupBox12.Controls.Add(this._U0maxLed5);
            this.groupBox12.Controls.Add(this.label79);
            this.groupBox12.Controls.Add(this.label80);
            this.groupBox12.Controls.Add(this.label81);
            this.groupBox12.Controls.Add(this.label82);
            this.groupBox12.Controls.Add(this.label83);
            this.groupBox12.Controls.Add(this._U0maxLed4);
            this.groupBox12.Controls.Add(this._U0maxLed3);
            this.groupBox12.Controls.Add(this._U0maxLed2);
            this.groupBox12.Controls.Add(this.label84);
            this.groupBox12.Controls.Add(this._U0maxLed1);
            this.groupBox12.Location = new System.Drawing.Point(194, 6);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(99, 99);
            this.groupBox12.TabIndex = 4;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "U0";
            // 
            // _U0maxLed8
            // 
            this._U0maxLed8.Location = new System.Drawing.Point(35, 79);
            this._U0maxLed8.Name = "_U0maxLed8";
            this._U0maxLed8.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed8.State = BEMN.Forms.LedState.Off;
            this._U0maxLed8.TabIndex = 24;
            // 
            // _U0maxLed7
            // 
            this._U0maxLed7.Location = new System.Drawing.Point(8, 79);
            this._U0maxLed7.Name = "_U0maxLed7";
            this._U0maxLed7.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed7.State = BEMN.Forms.LedState.Off;
            this._U0maxLed7.TabIndex = 23;
            // 
            // _U0maxLed6
            // 
            this._U0maxLed6.Location = new System.Drawing.Point(35, 63);
            this._U0maxLed6.Name = "_U0maxLed6";
            this._U0maxLed6.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed6.State = BEMN.Forms.LedState.Off;
            this._U0maxLed6.TabIndex = 22;
            // 
            // _U0maxLed5
            // 
            this._U0maxLed5.Location = new System.Drawing.Point(8, 63);
            this._U0maxLed5.Name = "_U0maxLed5";
            this._U0maxLed5.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed5.State = BEMN.Forms.LedState.Off;
            this._U0maxLed5.TabIndex = 21;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(50, 79);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(39, 13);
            this.label79.TabIndex = 20;
            this.label79.Text = "U0>>>";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(50, 63);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(39, 13);
            this.label80.TabIndex = 19;
            this.label80.Text = "U0>>>";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(50, 47);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(33, 13);
            this.label81.TabIndex = 18;
            this.label81.Text = "U0>>";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(50, 31);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(27, 13);
            this.label82.TabIndex = 17;
            this.label82.Text = "U0>";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(26, 14);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(35, 13);
            this.label83.TabIndex = 16;
            this.label83.Text = "����.";
            // 
            // _U0maxLed4
            // 
            this._U0maxLed4.Location = new System.Drawing.Point(35, 47);
            this._U0maxLed4.Name = "_U0maxLed4";
            this._U0maxLed4.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed4.State = BEMN.Forms.LedState.Off;
            this._U0maxLed4.TabIndex = 6;
            // 
            // _U0maxLed3
            // 
            this._U0maxLed3.Location = new System.Drawing.Point(8, 47);
            this._U0maxLed3.Name = "_U0maxLed3";
            this._U0maxLed3.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed3.State = BEMN.Forms.LedState.Off;
            this._U0maxLed3.TabIndex = 4;
            // 
            // _U0maxLed2
            // 
            this._U0maxLed2.Location = new System.Drawing.Point(35, 31);
            this._U0maxLed2.Name = "_U0maxLed2";
            this._U0maxLed2.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed2.State = BEMN.Forms.LedState.Off;
            this._U0maxLed2.TabIndex = 2;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(5, 14);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(23, 13);
            this.label84.TabIndex = 1;
            this.label84.Text = "��";
            // 
            // _U0maxLed1
            // 
            this._U0maxLed1.Location = new System.Drawing.Point(8, 31);
            this._U0maxLed1.Name = "_U0maxLed1";
            this._U0maxLed1.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed1.State = BEMN.Forms.LedState.Off;
            this._U0maxLed1.TabIndex = 0;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._UminLed8);
            this.groupBox10.Controls.Add(this._UminLed7);
            this.groupBox10.Controls.Add(this._UminLed6);
            this.groupBox10.Controls.Add(this._UminLed5);
            this.groupBox10.Controls.Add(this.label55);
            this.groupBox10.Controls.Add(this.label57);
            this.groupBox10.Controls.Add(this.label63);
            this.groupBox10.Controls.Add(this.label64);
            this.groupBox10.Controls.Add(this.label65);
            this.groupBox10.Controls.Add(this._UminLed4);
            this.groupBox10.Controls.Add(this._UminLed3);
            this.groupBox10.Controls.Add(this._UminLed2);
            this.groupBox10.Controls.Add(this.label66);
            this.groupBox10.Controls.Add(this._UminLed1);
            this.groupBox10.Location = new System.Drawing.Point(102, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(89, 99);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "U min";
            // 
            // _UminLed8
            // 
            this._UminLed8.Location = new System.Drawing.Point(34, 79);
            this._UminLed8.Name = "_UminLed8";
            this._UminLed8.Size = new System.Drawing.Size(13, 13);
            this._UminLed8.State = BEMN.Forms.LedState.Off;
            this._UminLed8.TabIndex = 24;
            // 
            // _UminLed7
            // 
            this._UminLed7.Location = new System.Drawing.Point(8, 79);
            this._UminLed7.Name = "_UminLed7";
            this._UminLed7.Size = new System.Drawing.Size(13, 13);
            this._UminLed7.State = BEMN.Forms.LedState.Off;
            this._UminLed7.TabIndex = 23;
            // 
            // _UminLed6
            // 
            this._UminLed6.Location = new System.Drawing.Point(34, 63);
            this._UminLed6.Name = "_UminLed6";
            this._UminLed6.Size = new System.Drawing.Size(13, 13);
            this._UminLed6.State = BEMN.Forms.LedState.Off;
            this._UminLed6.TabIndex = 22;
            // 
            // _UminLed5
            // 
            this._UminLed5.Location = new System.Drawing.Point(8, 63);
            this._UminLed5.Name = "_UminLed5";
            this._UminLed5.Size = new System.Drawing.Size(13, 13);
            this._UminLed5.State = BEMN.Forms.LedState.Off;
            this._UminLed5.TabIndex = 21;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(48, 79);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(39, 13);
            this.label55.TabIndex = 20;
            this.label55.Text = "U<<<<";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(48, 63);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(33, 13);
            this.label57.TabIndex = 19;
            this.label57.Text = "U<<<";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(48, 47);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(27, 13);
            this.label63.TabIndex = 18;
            this.label63.Text = "U<<";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(48, 31);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(21, 13);
            this.label64.TabIndex = 17;
            this.label64.Text = "U<";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(26, 14);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(35, 13);
            this.label65.TabIndex = 16;
            this.label65.Text = "����.";
            // 
            // _UminLed4
            // 
            this._UminLed4.Location = new System.Drawing.Point(34, 47);
            this._UminLed4.Name = "_UminLed4";
            this._UminLed4.Size = new System.Drawing.Size(13, 13);
            this._UminLed4.State = BEMN.Forms.LedState.Off;
            this._UminLed4.TabIndex = 6;
            // 
            // _UminLed3
            // 
            this._UminLed3.Location = new System.Drawing.Point(8, 47);
            this._UminLed3.Name = "_UminLed3";
            this._UminLed3.Size = new System.Drawing.Size(13, 13);
            this._UminLed3.State = BEMN.Forms.LedState.Off;
            this._UminLed3.TabIndex = 4;
            // 
            // _UminLed2
            // 
            this._UminLed2.Location = new System.Drawing.Point(34, 31);
            this._UminLed2.Name = "_UminLed2";
            this._UminLed2.Size = new System.Drawing.Size(13, 13);
            this._UminLed2.State = BEMN.Forms.LedState.Off;
            this._UminLed2.TabIndex = 2;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(5, 14);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(23, 13);
            this.label66.TabIndex = 1;
            this.label66.Text = "��";
            // 
            // _UminLed1
            // 
            this._UminLed1.Location = new System.Drawing.Point(8, 31);
            this._UminLed1.Name = "_UminLed1";
            this._UminLed1.Size = new System.Drawing.Size(13, 13);
            this._UminLed1.State = BEMN.Forms.LedState.Off;
            this._UminLed1.TabIndex = 0;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._UmaxLed8);
            this.groupBox9.Controls.Add(this._UmaxLed7);
            this.groupBox9.Controls.Add(this._UmaxLed6);
            this.groupBox9.Controls.Add(this._UmaxLed5);
            this.groupBox9.Controls.Add(this.label73);
            this.groupBox9.Controls.Add(this.label72);
            this.groupBox9.Controls.Add(this.label71);
            this.groupBox9.Controls.Add(this.label70);
            this.groupBox9.Controls.Add(this.label69);
            this.groupBox9.Controls.Add(this._UmaxLed4);
            this.groupBox9.Controls.Add(this._UmaxLed3);
            this.groupBox9.Controls.Add(this._UmaxLed2);
            this.groupBox9.Controls.Add(this.label68);
            this.groupBox9.Controls.Add(this._UmaxLed1);
            this.groupBox9.Location = new System.Drawing.Point(10, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(89, 99);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "U max";
            // 
            // _UmaxLed8
            // 
            this._UmaxLed8.Location = new System.Drawing.Point(32, 79);
            this._UmaxLed8.Name = "_UmaxLed8";
            this._UmaxLed8.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed8.State = BEMN.Forms.LedState.Off;
            this._UmaxLed8.TabIndex = 24;
            // 
            // _UmaxLed7
            // 
            this._UmaxLed7.Location = new System.Drawing.Point(8, 79);
            this._UmaxLed7.Name = "_UmaxLed7";
            this._UmaxLed7.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed7.State = BEMN.Forms.LedState.Off;
            this._UmaxLed7.TabIndex = 23;
            // 
            // _UmaxLed6
            // 
            this._UmaxLed6.Location = new System.Drawing.Point(32, 63);
            this._UmaxLed6.Name = "_UmaxLed6";
            this._UmaxLed6.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed6.State = BEMN.Forms.LedState.Off;
            this._UmaxLed6.TabIndex = 22;
            // 
            // _UmaxLed5
            // 
            this._UmaxLed5.Location = new System.Drawing.Point(8, 63);
            this._UmaxLed5.Name = "_UmaxLed5";
            this._UmaxLed5.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed5.State = BEMN.Forms.LedState.Off;
            this._UmaxLed5.TabIndex = 21;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(47, 79);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(39, 13);
            this.label73.TabIndex = 20;
            this.label73.Text = "U>>>>";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(47, 63);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(33, 13);
            this.label72.TabIndex = 19;
            this.label72.Text = "U>>>";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(47, 47);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(27, 13);
            this.label71.TabIndex = 18;
            this.label71.Text = "U>>";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(47, 31);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(21, 13);
            this.label70.TabIndex = 17;
            this.label70.Text = "U>";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(26, 14);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(35, 13);
            this.label69.TabIndex = 16;
            this.label69.Text = "����.";
            // 
            // _UmaxLed4
            // 
            this._UmaxLed4.Location = new System.Drawing.Point(32, 47);
            this._UmaxLed4.Name = "_UmaxLed4";
            this._UmaxLed4.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed4.State = BEMN.Forms.LedState.Off;
            this._UmaxLed4.TabIndex = 6;
            // 
            // _UmaxLed3
            // 
            this._UmaxLed3.Location = new System.Drawing.Point(8, 47);
            this._UmaxLed3.Name = "_UmaxLed3";
            this._UmaxLed3.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed3.State = BEMN.Forms.LedState.Off;
            this._UmaxLed3.TabIndex = 4;
            // 
            // _UmaxLed2
            // 
            this._UmaxLed2.Location = new System.Drawing.Point(32, 31);
            this._UmaxLed2.Name = "_UmaxLed2";
            this._UmaxLed2.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed2.State = BEMN.Forms.LedState.Off;
            this._UmaxLed2.TabIndex = 2;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(5, 14);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(23, 13);
            this.label68.TabIndex = 1;
            this.label68.Text = "��";
            // 
            // _UmaxLed1
            // 
            this._UmaxLed1.Location = new System.Drawing.Point(8, 31);
            this._UmaxLed1.Name = "_UmaxLed1";
            this._UmaxLed1.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed1.State = BEMN.Forms.LedState.Off;
            this._UmaxLed1.TabIndex = 0;
            // 
            // _images
            // 
            this._images.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_images.ImageStream")));
            this._images.TransparentColor = System.Drawing.Color.Transparent;
            this._images.Images.SetKeyName(0, "analog.bmp");
            this._images.Images.SetKeyName(1, "diskret.bmp");
            // 
            // MeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 428);
            this.Controls.Add(this._diagTab);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MeasuringForm";
            this.Text = "MR600_MeasuringForm";
            this.Activated += new System.EventHandler(this.MeasuringForm_Activated);
            this.Deactivate += new System.EventHandler(this.MeasuringForm_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.MeasuringForm_Load);
            this._diagTab.ResumeLayout(false);
            this._analogPage.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this._diskretPage.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl _diagTab;
        private System.Windows.Forms.TabPage _analogPage;
        private System.Windows.Forms.TabPage _diskretPage;
        private System.Windows.Forms.ImageList _images;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private BEMN.Forms.LedControl _indLed1;
        private System.Windows.Forms.Label label9;
        private BEMN.Forms.LedControl _releLed8;
        private System.Windows.Forms.Label label10;
        private BEMN.Forms.LedControl _releLed7;
        private System.Windows.Forms.Label label11;
        private BEMN.Forms.LedControl _releLed6;
        private System.Windows.Forms.Label label12;
        private BEMN.Forms.LedControl _releLed5;
        private System.Windows.Forms.Label label13;
        private BEMN.Forms.LedControl _releLed4;
        private System.Windows.Forms.Label label14;
        private BEMN.Forms.LedControl _releLed3;
        private System.Windows.Forms.Label label15;
        private BEMN.Forms.LedControl _releLed2;
        private System.Windows.Forms.Label label16;
        private BEMN.Forms.LedControl _releLed1;
        private System.Windows.Forms.Label label7;
        private BEMN.Forms.LedControl _indLed6;
        private System.Windows.Forms.Label label8;
        private BEMN.Forms.LedControl _indLed5;
        private System.Windows.Forms.Label label3;
        private BEMN.Forms.LedControl _indLed4;
        private System.Windows.Forms.Label label4;
        private BEMN.Forms.LedControl _indLed3;
        private System.Windows.Forms.Label label2;
        private BEMN.Forms.LedControl _indLed2;
        private System.Windows.Forms.Label label41;
        private BEMN.Forms.LedControl _inL_led8;
        private System.Windows.Forms.Label label42;
        private BEMN.Forms.LedControl _inL_led7;
        private System.Windows.Forms.Label label43;
        private BEMN.Forms.LedControl _inL_led6;
        private System.Windows.Forms.Label label44;
        private BEMN.Forms.LedControl _inL_led5;
        private System.Windows.Forms.Label label45;
        private BEMN.Forms.LedControl _inL_led4;
        private System.Windows.Forms.Label label46;
        private BEMN.Forms.LedControl _inL_led3;
        private System.Windows.Forms.Label label47;
        private BEMN.Forms.LedControl _inL_led2;
        private System.Windows.Forms.Label label48;
        private BEMN.Forms.LedControl _inL_led1;
        private System.Windows.Forms.Label label25;
        private BEMN.Forms.LedControl _inD_led8;
        private System.Windows.Forms.Label label26;
        private BEMN.Forms.LedControl _inD_led7;
        private System.Windows.Forms.Label label27;
        private BEMN.Forms.LedControl _inD_led6;
        private System.Windows.Forms.Label label28;
        private BEMN.Forms.LedControl _inD_led5;
        private System.Windows.Forms.Label label29;
        private BEMN.Forms.LedControl _inD_led4;
        private System.Windows.Forms.Label label30;
        private BEMN.Forms.LedControl _inD_led3;
        private System.Windows.Forms.Label label31;
        private BEMN.Forms.LedControl _inD_led2;
        private System.Windows.Forms.Label label32;
        private BEMN.Forms.LedControl _inD_led1;
        private System.Windows.Forms.Label label17;
        private BEMN.Forms.LedControl _outLed8;
        private System.Windows.Forms.Label label18;
        private BEMN.Forms.LedControl _outLed7;
        private System.Windows.Forms.Label label19;
        private BEMN.Forms.LedControl _outLed6;
        private System.Windows.Forms.Label label20;
        private BEMN.Forms.LedControl _outLed5;
        private System.Windows.Forms.Label label21;
        private BEMN.Forms.LedControl _outLed4;
        private System.Windows.Forms.Label label22;
        private BEMN.Forms.LedControl _outLed3;
        private System.Windows.Forms.Label label23;
        private BEMN.Forms.LedControl _outLed2;
        private System.Windows.Forms.Label label24;
        private BEMN.Forms.LedControl _outLed1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox _UbBox;
        private System.Windows.Forms.TextBox _UaBox;
        private System.Windows.Forms.TextBox _F_Box;
        private System.Windows.Forms.TextBox _UcBox;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Button _indicationResetButton;
        private System.Windows.Forms.Label label60;
        private BEMN.Forms.LedControl _manageLed2;
        private BEMN.Forms.LedControl _manageLed1;
        private System.Windows.Forms.Button _constraintToggleButton;
        private System.Windows.Forms.Button _resetJA_But;
        private System.Windows.Forms.Button _resetJS_But;
        private System.Windows.Forms.Button _resetFaultBut;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label51;
        private BEMN.Forms.LedControl _manageLed5;
        private System.Windows.Forms.Label label58;
        private BEMN.Forms.LedControl _manageLed4;
        private BEMN.Forms.LedControl _manageLed3;
        private System.Windows.Forms.Label label61;
        private BEMN.Forms.LedControl _manageLed7;
        private BEMN.Forms.LedControl _manageLed6;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private BEMN.Forms.LedControl _UmaxLed4;
        private BEMN.Forms.LedControl _UmaxLed3;
        private BEMN.Forms.LedControl _UmaxLed2;
        private System.Windows.Forms.Label label68;
        private BEMN.Forms.LedControl _UmaxLed1;
        private System.Windows.Forms.GroupBox groupBox10;
        private BEMN.Forms.LedControl _UminLed8;
        private BEMN.Forms.LedControl _UminLed7;
        private BEMN.Forms.LedControl _UminLed6;
        private BEMN.Forms.LedControl _UminLed5;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private BEMN.Forms.LedControl _UminLed4;
        private BEMN.Forms.LedControl _UminLed3;
        private BEMN.Forms.LedControl _UminLed2;
        private System.Windows.Forms.Label label66;
        private BEMN.Forms.LedControl _UminLed1;
        private BEMN.Forms.LedControl _UmaxLed8;
        private BEMN.Forms.LedControl _UmaxLed7;
        private BEMN.Forms.LedControl _UmaxLed6;
        private BEMN.Forms.LedControl _UmaxLed5;
        private System.Windows.Forms.GroupBox groupBox11;
        private BEMN.Forms.LedControl _U12Led8;
        private BEMN.Forms.LedControl _U12Led7;
        private BEMN.Forms.LedControl _U12Led6;
        private BEMN.Forms.LedControl _U12Led5;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private BEMN.Forms.LedControl _U12Led4;
        private BEMN.Forms.LedControl _U12Led3;
        private BEMN.Forms.LedControl _U12Led2;
        private System.Windows.Forms.Label label78;
        private BEMN.Forms.LedControl _U12Led1;
        private System.Windows.Forms.GroupBox groupBox12;
        private BEMN.Forms.LedControl _U0maxLed8;
        private BEMN.Forms.LedControl _U0maxLed7;
        private BEMN.Forms.LedControl _U0maxLed6;
        private BEMN.Forms.LedControl _U0maxLed5;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private BEMN.Forms.LedControl _U0maxLed4;
        private BEMN.Forms.LedControl _U0maxLed3;
        private BEMN.Forms.LedControl _U0maxLed2;
        private System.Windows.Forms.Label label84;
        private BEMN.Forms.LedControl _U0maxLed1;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label label109;
        private BEMN.Forms.LedControl _extDefenseLed4;
        private System.Windows.Forms.Label label110;
        private BEMN.Forms.LedControl _extDefenseLed3;
        private System.Windows.Forms.Label label111;
        private BEMN.Forms.LedControl _extDefenseLed2;
        private System.Windows.Forms.Label label112;
        private BEMN.Forms.LedControl _extDefenseLed1;
        private System.Windows.Forms.Label label105;
        private BEMN.Forms.LedControl _extDefenseLed8;
        private System.Windows.Forms.Label label106;
        private BEMN.Forms.LedControl _extDefenseLed7;
        private System.Windows.Forms.Label label107;
        private BEMN.Forms.LedControl _extDefenseLed6;
        private System.Windows.Forms.Label label108;
        private BEMN.Forms.LedControl _extDefenseLed5;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Label label113;
        private BEMN.Forms.LedControl _faultStateLed8;
        private System.Windows.Forms.Label label114;
        private BEMN.Forms.LedControl _faultStateLed7;
        private System.Windows.Forms.Label label115;
        private BEMN.Forms.LedControl _faultStateLed6;
        private System.Windows.Forms.Label label116;
        private BEMN.Forms.LedControl _faultStateLed5;
        private System.Windows.Forms.Label label117;
        private BEMN.Forms.LedControl _faultStateLed4;
        private System.Windows.Forms.Label label118;
        private BEMN.Forms.LedControl _faultStateLed3;
        private System.Windows.Forms.Label label119;
        private BEMN.Forms.LedControl _faultStateLed2;
        private System.Windows.Forms.Label label120;
        private BEMN.Forms.LedControl _faultStateLed1;
        private System.Windows.Forms.GroupBox groupBox19;
        private BEMN.Forms.LedControl _faultSignalLed16;
        private System.Windows.Forms.Label label130;
        private BEMN.Forms.LedControl _faultSignalLed15;
        private System.Windows.Forms.Label label131;
        private BEMN.Forms.LedControl _faultSignalLed14;
        private System.Windows.Forms.Label label132;
        private BEMN.Forms.LedControl _faultSignalLed13;
        private System.Windows.Forms.Label label133;
        private BEMN.Forms.LedControl _faultSignalLed12;
        private System.Windows.Forms.Label label134;
        private BEMN.Forms.LedControl _faultSignalLed11;
        private System.Windows.Forms.Label label135;
        private BEMN.Forms.LedControl _faultSignalLed10;
        private System.Windows.Forms.Label label136;
        private BEMN.Forms.LedControl _faultSignalLed9;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Label label121;
        private BEMN.Forms.LedControl _faultSignalLed8;
        private System.Windows.Forms.Label label122;
        private BEMN.Forms.LedControl _faultSignalLed7;
        private System.Windows.Forms.Label label123;
        private BEMN.Forms.LedControl _faultSignalLed6;
        private System.Windows.Forms.Label label124;
        private BEMN.Forms.LedControl _faultSignalLed5;
        private BEMN.Forms.LedControl _faultSignalLed4;
        private System.Windows.Forms.Label label126;
        private BEMN.Forms.LedControl _faultSignalLed3;
        private System.Windows.Forms.Label label127;
        private BEMN.Forms.LedControl _faultSignalLed2;
        private System.Windows.Forms.Label label128;
        private BEMN.Forms.LedControl _faultSignalLed1;
        private System.Windows.Forms.Label label137;
        private BEMN.Forms.LedControl _manageLed8;
        private System.Windows.Forms.TextBox _U2Box;
        private System.Windows.Forms.TextBox _U1Box;
        private System.Windows.Forms.TextBox _U0Box;
        private System.Windows.Forms.TextBox _UcaBox;
        private System.Windows.Forms.TextBox _UbcBox;
        private System.Windows.Forms.TextBox _UabBox;
        private System.Windows.Forms.TextBox _UnBox;
        private System.Windows.Forms.Label label33;
        private BEMN.Forms.LedControl _releLed16;
        private System.Windows.Forms.Label label34;
        private BEMN.Forms.LedControl _releLed15;
        private System.Windows.Forms.Label label35;
        private BEMN.Forms.LedControl _releLed14;
        private System.Windows.Forms.Label label36;
        private BEMN.Forms.LedControl _releLed13;
        private System.Windows.Forms.Label label37;
        private BEMN.Forms.LedControl _releLed12;
        private System.Windows.Forms.Label label38;
        private BEMN.Forms.LedControl _releLed11;
        private System.Windows.Forms.Label label39;
        private BEMN.Forms.LedControl _releLed10;
        private System.Windows.Forms.Label label40;
        private BEMN.Forms.LedControl _releLed9;
        private System.Windows.Forms.GroupBox groupBox13;
        private BEMN.Forms.LedControl _FminLed8;
        private BEMN.Forms.LedControl _FminLed7;
        private BEMN.Forms.LedControl _FminLed6;
        private BEMN.Forms.LedControl _FminLed5;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private BEMN.Forms.LedControl _FminLed4;
        private BEMN.Forms.LedControl _FminLed3;
        private BEMN.Forms.LedControl _FminLed2;
        private System.Windows.Forms.Label label92;
        private BEMN.Forms.LedControl _FminLed1;
        private System.Windows.Forms.GroupBox groupBox7;
        private BEMN.Forms.LedControl _FmaxLed8;
        private BEMN.Forms.LedControl _FmaxLed7;
        private BEMN.Forms.LedControl _FmaxLed6;
        private BEMN.Forms.LedControl _FmaxLed5;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label85;
        private BEMN.Forms.LedControl _FmaxLed4;
        private BEMN.Forms.LedControl _FmaxLed3;
        private BEMN.Forms.LedControl _FmaxLed2;
        private System.Windows.Forms.Label label86;
        private BEMN.Forms.LedControl _FmaxLed1;
        private BEMN.Forms.LedControl _manageLed10;
        private System.Windows.Forms.Label label93;
        private BEMN.Forms.LedControl _manageLed9;
        private System.Windows.Forms.Label label94;
        private BEMN.Forms.LedControl ledControl1;
        private System.Windows.Forms.Label label5;
        private BEMN.Forms.LedControl ledControl2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label104;
        private BEMN.Forms.DateTimeControl _dateTimeControlOld;
    }
}