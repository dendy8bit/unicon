using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Old;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MBServer.Queries;
using BEMN.MR600.BSBGL;
using BEMN.MR600.New;
using BEMN.MR600.New.Configuration;
using BEMN.MR600.New.Forms;
using BEMN.MR600.Old.Forms;
using BEMN.MR600.Old.HelpClasses;
using BEMN.MR600.Old.Osc;
using BEMN.MR600.Old.Structures;

namespace BEMN.MR600
{
    public class MR600 : Device, IDeviceView, IDeviceVersion
    {
        public const ulong TIMELIMIT = 3000000;
        public const string TIMELIMIT_ERROR_MSG = "������� ����� � ��������� [0 - 3000000]";

        #region ��������� ������

        public const int SYSTEMJOURNAL_RECORD_CNT = 128;
        public const string JURNAL_CLEAR = "������ ����";

        [Browsable(false)]
        [XmlIgnore]
        public CSystemJournal SystemJournal
        {
            get { return _systemJournalRecords; }
            set { _systemJournalRecords = value; }
        }

        public struct SystemRecord
        {
            public string msg;
            public string time;
        } ;

        public class CSystemJournal : ICollection
        {
            private List<SystemRecord> _messages = new List<SystemRecord>(SYSTEMJOURNAL_RECORD_CNT);

            public int Count
            {
                get { return _messages.Count; }
            }

            public SystemRecord this[int i]
            {
                get { return (SystemRecord) (_messages[i]); }
            }

            public bool IsMessageEmpty(int i)
            {
                return "�����" == _messages[i].msg;
            }

            public bool AddMessage(ushort[] value)
            {
                bool ret;
                SystemRecord msg = CreateMessage(value);
                if ("�����" == msg.msg)
                {
                    ret = false;
                }
                else
                {
                    ret = true;
                    _messages.Add(msg);
                }
                return ret;
            }

            private SystemRecord CreateMessage(ushort[] value)
            {
                SystemRecord msg = new SystemRecord();
                int index = value[0];
                if (value[0] > Strings.SystemJournal.Count - 1)
                {
                    index = Strings.SystemJournal.Count - 1;
                }
                if (value[0] < Strings.SystemJournal.Count)
                {
                    msg.msg = Strings.SystemJournal[value[0]];
                }
                else 
                {
                    msg.msg = "����-��������� �������";
                }

                msg.time = value[3].ToString("d2") + "-" + value[2].ToString("d2") + "-" + value[1].ToString("d2") + " " +
                           value[4].ToString("d2") + ":" + value[5].ToString("d2") + ":" + value[6].ToString("d2") + ":" +
                           value[7].ToString("d2");

                return msg;
            }

            #region ICollection Members

            public void CopyTo(Array array, int index)
            {
            }

            public bool IsSynchronized
            {
                get { return false; }
            }

            public object SyncRoot
            {
                get { return _messages; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _messages.GetEnumerator();
            }

            #endregion
        }

        #endregion

        #region ������ ������

        public const int ALARMJOURNAL_RECORD_CNT = 32;

        [Browsable(false)]
        [XmlIgnore]
        public CAlarmJournal AlarmJournal
        {
            get { return _alarmJournalRecords; }
            set { _alarmJournalRecords = value; }
        }

        public struct AlarmRecord
        {
            public string time;
            public string msg;
            public string code;
            public string type_value;
            public string Ua;
            public string Ub;
            public string Uc;
            public string Uo;
            public string F;
            public string Uab;
            public string Ubc;
            public string Uca;
            public string U0;
            public string U1;
            public string U2;
            public string inSignals1;
            public string inSignals2;
        } 

        public class CAlarmJournal
        {
            private Dictionary<ushort, string> _msgDictionary;
            private Dictionary<byte, string> _codeDictionary;
            private Dictionary<byte, string> _typeDictionary;
            private MR600 _device;

            public CAlarmJournal()
            {

            }

            public CAlarmJournal(MR600 device)
            {
                _device = device;
                _msgDictionary = new Dictionary<ushort, string>(6);
                _codeDictionary = new Dictionary<byte, string>(25);
                _typeDictionary = new Dictionary<byte, string>(9);

                _msgDictionary.Add(0, "������ ����");
                _msgDictionary.Add(1, "������������");
                _msgDictionary.Add(2, "������");
                _msgDictionary.Add(118, "������ ����");
                _msgDictionary.Add(255, "��� ���������");

                _codeDictionary.Add(1, "��-1");
                _codeDictionary.Add(2, "��-2");
                _codeDictionary.Add(3, "��-3");
                _codeDictionary.Add(4, "��-4");
                _codeDictionary.Add(5, "��-5");
                _codeDictionary.Add(6, "��-6");
                _codeDictionary.Add(7, "��-7");
                _codeDictionary.Add(8, "��-8");
                _codeDictionary.Add(9, "U>");
                _codeDictionary.Add(10, "U>>");
                _codeDictionary.Add(11, "U>>>");
                _codeDictionary.Add(12, "U>>>>");
                _codeDictionary.Add(13, "U<");
                _codeDictionary.Add(14, "U<<");
                _codeDictionary.Add(15, "U<<<");
                _codeDictionary.Add(16, "U<<<<");
                _codeDictionary.Add(17, "U0>");
                _codeDictionary.Add(18, "U0>>");
                _codeDictionary.Add(19, "U0>>>");
                _codeDictionary.Add(20, "U0>>>>");
                _codeDictionary.Add(21, "U1<");
                _codeDictionary.Add(22, "U1<<");
                _codeDictionary.Add(23, "U2>");
                _codeDictionary.Add(24, "U2>>");
                _codeDictionary.Add(25, "F>");
                _codeDictionary.Add(26, "F>>");
                _codeDictionary.Add(27, "F>>>");
                _codeDictionary.Add(28, "F>>>>");
                _codeDictionary.Add(29, "F<");
                _codeDictionary.Add(30, "F<<");
                _codeDictionary.Add(31, "F<<<");
                _codeDictionary.Add(32, "F<<<<");

                _typeDictionary.Add(0, "��");
                _typeDictionary.Add(1, "F");
                _typeDictionary.Add(2, "Uo");
                _typeDictionary.Add(3, "Ua");
                _typeDictionary.Add(4, "Ub");
                _typeDictionary.Add(5, "Uc");
                _typeDictionary.Add(6, "Uab");
                _typeDictionary.Add(7, "Ubc");
                _typeDictionary.Add(8, "Uca");
                _typeDictionary.Add(9, "U0");
                _typeDictionary.Add(10, "U1");
                _typeDictionary.Add(11, "U2");
            }

            private List<AlarmRecord> _messages = new List<AlarmRecord>(ALARMJOURNAL_RECORD_CNT);

            public int Count
            {
                get { return _messages.Count; }
            }

            public AlarmRecord this[int i]
            {
                get { return _messages[i]; }
            }

            public bool IsMessageEmpty(int i)
            {
                return "�����" == _messages[i].msg;
            }

            public bool AddMessage(byte[] value)
            {
                bool ret;
                Common.SwapArrayItems(ref value);
                AlarmRecord msg = CreateMessage(value);
                if ("�����" == msg.msg)
                {
                    ret = false;
                }
                else
                {
                    ret = true;
                    _messages.Add(msg);
                }
                return ret;
            }

            public string GetMessage(byte b)
            {
                try
                {
                    return _msgDictionary[b];
                }
                catch(KeyNotFoundException) 
                {
                    return _msgDictionary[0];
                }
            }

            public string GetDateTime(byte[] datetime)
            {
                return
                    datetime[2].ToString("x") + "-" + datetime[1].ToString("x") + "-" + datetime[0].ToString("x") +
                    " " +
                    datetime[3].ToString("x") + ":" + datetime[4].ToString("x") + ":" + datetime[5].ToString("x") +
                           ":" + datetime[6].ToString("x");
            }

            public string GetCode(byte codeByte, byte phaseByte)
            {

                string group = (0 == (codeByte & 0x80)) ? "��������" : "������.";
                string phase = "";
                phase += (0 == (phaseByte & 0x08)) ? " " : "_";
                phase += (0 == (phaseByte & 0x04)) ? " " : "A";
                phase += (0 == (phaseByte & 0x02)) ? " " : "B";
                phase += (0 == (phaseByte & 0x01)) ? " " : "C";

                byte code = (byte)(codeByte & 0x7F);
                try
                {
                    return _codeDictionary[code] + " " + group + " " + phase;
                }
                catch (KeyNotFoundException)
                {
                    return "";
                }
            }


            private AlarmRecord CreateMessage(byte[] buffer)
            {
                AlarmRecord rec = new AlarmRecord();
                rec.msg = _msgDictionary[buffer[0]];
                rec.time = buffer[6].ToString("d2") + "-" + buffer[4].ToString("d2") + "-" + buffer[2].ToString("d2") +
                           " " +
                           buffer[8].ToString("d2") + ":" + buffer[10].ToString("d2") + ":" + buffer[12].ToString("d2") +
                           ":" + buffer[14].ToString("d2");
                try
                {
                    string group = (0 == (buffer[16] & 0x80)) ? "��������" : "������.";
                    string phase = "";
                    phase += (0 == (buffer[18] & 0x08)) ? " " : "_";
                    phase += (0 == (buffer[18] & 0x04)) ? " " : "A";
                    phase += (0 == (buffer[18] & 0x02)) ? " " : "B";
                    phase += (0 == (buffer[18] & 0x01)) ? " " : "C";

                    byte codeByte = (byte) (buffer[16] & 0x7F);
                    rec.code = _codeDictionary[codeByte] + " " + group + " " + phase;
                    double damageValue =
                    Measuring.GetConstraintOnly(Common.TOWORD(buffer[21], buffer[20]), ConstraintKoefficient.K_25600);
                    if (buffer[19] == 0)
                    {
                        rec.type_value = "";
                    }
                    else 
                    {
                        rec.type_value = _typeDictionary[buffer[19]] + String.Format(" = {0:F2}", damageValue);
                    }

 
                }
                catch (KeyNotFoundException)
                {
                }


                double F = Measuring.GetF(Common.TOWORD(buffer[23], buffer[22]));
                double Ua =
                    Measuring.GetConstraintOnly(Common.TOWORD(buffer[25], buffer[24]), ConstraintKoefficient.K_25600);
                double Ub =
                    Measuring.GetConstraintOnly(Common.TOWORD(buffer[27], buffer[26]), ConstraintKoefficient.K_25600);
                double Uc =
                    Measuring.GetConstraintOnly(Common.TOWORD(buffer[29], buffer[28]), ConstraintKoefficient.K_25600);
                double Uo =
                    Measuring.GetConstraintOnly(Common.TOWORD(buffer[31], buffer[30]), ConstraintKoefficient.K_25600);
                double Uab =
                    Measuring.GetConstraintOnly(Common.TOWORD(buffer[33], buffer[32]), ConstraintKoefficient.K_25600);
                double Ubc =
                    Measuring.GetConstraintOnly(Common.TOWORD(buffer[35], buffer[34]), ConstraintKoefficient.K_25600);
                double Uca =
                    Measuring.GetConstraintOnly(Common.TOWORD(buffer[37], buffer[36]), ConstraintKoefficient.K_25600);
                double U0 =
                    Measuring.GetConstraintOnly(Common.TOWORD(buffer[39], buffer[38]), ConstraintKoefficient.K_25600);
                double U1 =
                    Measuring.GetConstraintOnly(Common.TOWORD(buffer[41], buffer[40]), ConstraintKoefficient.K_25600);
                double U2 =
                    Measuring.GetConstraintOnly(Common.TOWORD(buffer[43], buffer[42]), ConstraintKoefficient.K_25600);

                rec.F = String.Format("{0:F2}", F);
                rec.Ua = String.Format("{0:F2}", Ua);
                rec.Ub = String.Format("{0:F2}", Ub);
                rec.Uc = String.Format("{0:F2}", Uc);
                rec.Uo = String.Format("{0:F2}", Uo);
                rec.Uab = String.Format("{0:F2}", Uab);
                rec.Ubc = String.Format("{0:F2}", Ubc);
                rec.Uca = String.Format("{0:F2}", Uca);
                rec.U0 = String.Format("{0:F2}", U0);
                rec.U1 = String.Format("{0:F2}", U1);
                rec.U2 = String.Format("{0:F2}", U2);

                byte[] b1 = new byte[] {buffer[44]};
                byte[] b2 = new byte[] {buffer[45]};
                rec.inSignals1 = Common.BitsToString(new BitArray(b1));
                rec.inSignals2 = Common.BitsToString(new BitArray(b2));
                return rec;
            }
        }

        #endregion

        #region ����
        private Mr600DeviceV2 _mr600DeviceV2;
        private MemoryEntity<DiscretDataBase> _discretDataBase;
        private MemoryEntity<DateTimeStruct> _dateTime;
        private MemoryEntity<DateTimeStructOld> _dateTimeOld;
        private MemoryEntity<AnalogDataBase> _analogDataBase;
        private MemoryEntity<MeasuringTransformator> _transformator; 

        private slot _inputSignals = new slot(0x1180, 0x1198);
        private slot _outputSignals = new slot(0x1198, 0x11F8);

        private slot[] _systemJournal = new slot[SYSTEMJOURNAL_RECORD_CNT];
        private slot[] _alarmJournal = new slot[32];
        private bool _stopAlarmJournal = false;
        private bool _stopSystemJournal = false;
        private CSystemJournal _systemJournalRecords = new CSystemJournal();
        private CAlarmJournal _alarmJournalRecords;

        private COutputRele _outputRele = new COutputRele();

        #endregion

        #region �������

        public event Handler InputSignalsLoadOK;
        public event Handler InputSignalsLoadFail;
        public event Handler InputSignalsSaveOK;
        public event Handler InputSignalsSaveFail;
        public event Handler ExternalDefensesLoadOK;
        public event Handler ExternalDefensesLoadFail;
        public event Handler ExternalDefensesSaveOK;
        public event Handler ExternalDefensesSaveFail;
        public event Handler SystemJournalLoadOk;
        public event IndexHandler SystemJournalRecordLoadOk;
        public event IndexHandler SystemJournalRecordLoadFail;
        public event Handler AlarmJournalLoadOk;
        public event IndexHandler AlarmJournalRecordLoadOk;
        public event IndexHandler AlarmJournalRecordLoadFail;
        public event Handler OutputSignalsLoadOK;
        public event Handler OutputSignalsLoadFail;
        public event Handler OutputSignalsSaveOK;
        public event Handler OutputSignalsSaveFail;

        #endregion

        #region ������������ �������������

        public MR600()
        {
            Init();
        }

        public MR600(Modbus mb)
        {
            MB = mb;
            this._mr600DeviceV2 = new Mr600DeviceV2(this);
            this._discretDataBase = new MemoryEntity<DiscretDataBase>("���������� ���� ������", this, 0x1900);
            this._dateTimeOld = new MemoryEntity<DateTimeStructOld>("���� � ����� ������", this, 0x200);
            this._dateTime = new MemoryEntity<DateTimeStruct>("���� � ����� ������", this, 0x200);
            this._analogDataBase = new MemoryEntity<AnalogDataBase>("���������� ���� ������", this, 0x1800);
            this._transformator = new MemoryEntity<MeasuringTransformator>("������������� �������������", this, 0x1180);
        }

        [XmlIgnore]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        public override Modbus MB
        {
            get { return mb; }
            set
            {
                if (value == null) return;
                mb = value;
                this.Init();
                mb.CompleteExchange += this.mb_CompleteExchange;
            }
        }


        private void Init()
        {
            HaveVersion = true;
            _alarmJournalRecords = new CAlarmJournal(this);
            ushort start = 0x2000;
            ushort size = 0x8;
            for (int i = 0; i < SYSTEMJOURNAL_RECORD_CNT; i++)
            {
                _systemJournal[i] = new slot((ushort) start, (ushort) (start + size));
                start += 0x10;
            }
            start = 0x2800;
            size = 24;
            for (int i = 0; i < ALARMJOURNAL_RECORD_CNT; i++)
            {
                _alarmJournal[i] = new slot(start, (ushort) (start + size));
                start += 0x40;
            }
        }

        #endregion

        #region Properties
        public Mr600DeviceV2 Mr600DeviceV2
        {
            get { return _mr600DeviceV2; }
        }

        public MemoryEntity<DiscretDataBase> DiscretDataBase
        {
            get { return this._discretDataBase; }
        }

        public MemoryEntity<DateTimeStruct> DateTime
        {
            get { return this._dateTime; }
        }
        public MemoryEntity<DateTimeStructOld> DateTimeOld
        {
            get { return this._dateTimeOld; }
        }

        public MemoryEntity<AnalogDataBase> AnalogDataBase
        {
            get { return this._analogDataBase; }
        }

        public MemoryEntity<MeasuringTransformator> Transformator
        {
            get { return this._transformator; }
        }
        #endregion

        #region �������� �������

        #region ���������� ����

        [XmlIgnore]
        [DisplayName("���� �������������")]
        [Description("���� �������������")]
        [CategoryAttribute("�������� �������")]
        [Editor(typeof (BitsEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray OutputDispepairRele
        {
            get { return new BitArray(new byte[] {Common.LOBYTE(_outputSignals.Value[0])}); }
            set { _outputSignals.Value[0] = Common.BitsToUshort(value); }
        }

        [XmlElement("�����_�������������")]
        [DisplayName("����� �������������")]
        [Description("����� ���� �������������")]
        [Category("�������� �������")]
        public ulong OutputDispepairTime
        {
            get { return Measuring.GetTime(_outputSignals.Value[1]); }
            set { _outputSignals.Value[1] = Measuring.SetTime(value); }
        }

        [XmlElement("����_������������")]
        [DisplayName("���� ������������")]
        [Description("���� ������������")]
        [Category("�������� �������")]
        [TypeConverter(typeof (RussianObjectConverter))]
        public string OutputSignalizationRele
        {
            get
            {
                ushort index = _outputSignals.Value[2];

                if (Strings.All.Count <= index)
                {
                    index = (ushort) (Strings.All.Count - 1);
                }

                return Strings.All[(int) index];
            }
            set { _outputSignals.Value[2] = (ushort) (Strings.All.IndexOf(value)); }
        }

        [XmlElement("�����_������������")]
        [DisplayName("����� ������������")]
        [Description("����� ���� ������������")]
        [Category("�������� �������")]
        public ulong OutputSignalizationTime
        {
            get { return Measuring.GetTime(_outputSignals.Value[3]); }
            set { _outputSignals.Value[3] = Measuring.SetTime(value); }
        }

        [XmlElement("����_������")]
        [DisplayName("���� ������")]
        [Description("���� ������")]
        [Category("�������� �������")]
        [TypeConverter(typeof (RussianObjectConverter))]
        public string OutputAlarmRele
        {
            get
            {
                ushort index = _outputSignals.Value[4];

                if (Strings.All.Count <= index)
                {
                    index = (ushort) (Strings.All.Count - 1);
                }

                return Strings.All[(int) index];
            }
            set { _outputSignals.Value[4] = (ushort) (Strings.All.IndexOf(value)); }
        }

        [XmlElement("�����_������")]
        [DisplayName("����� ������")]
        [Description("����� ���� ������")]
        [Category("�������� �������")]
        public ulong OutputAlarmTime
        {
            get { return Measuring.GetTime(_outputSignals.Value[5]); }
            set { _outputSignals.Value[5] = Measuring.SetTime(value); }
        }

        #endregion

        #region �������� ����

        public class COutputRele : ICollection
        {
            public const int LENGTH = 32;
            public const int COUNT = 16;
            public const int OFFSET = 24;

            public COutputRele()
            {
                SetBuffer(new ushort[LENGTH]);
            }

            private List<OutputReleItem> _releList = new List<OutputReleItem>(COUNT);

            [DisplayName("����������")]
            public int Count
            {
                get { return _releList.Count; }
            }

            public void SetBuffer(ushort[] values)
            {
                _releList.Clear();
                if (values.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Rele values", (object) LENGTH,
                                                          "Output rele length must be 16");
                }
                for (int i = 0; i < LENGTH; i += 2)
                {
                    _releList.Add(new OutputReleItem(values[i], values[i + 1]));
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] ret = new ushort[LENGTH];
                for (int i = 0; i < COUNT; i++)
                {
                    ret[i*2] = _releList[i].Value[0];
                    ret[i*2 + 1] = _releList[i].Value[1];
                }
                return ret;
            }

            public OutputReleItem this[int i]
            {
                get { return _releList[i]; }
                set { _releList[i] = value; }
            }

            #region ICollection Members

            public void Add(OutputReleItem item)
            {
                _releList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _releList.GetEnumerator();
            }

            #endregion
        } ;

        public class OutputReleItem
        {
            private ushort _hiWord;
            private ushort _loWord;

            public override string ToString()
            {
                return "�������� ����";
            }

            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [Category("������� ���")]
            [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            public ushort[] Value
            {
                get { return new ushort[] {_hiWord, _loWord}; }
                set
                {
                    _hiWord = value[0];
                    _loWord = value[1];
                }
            }

            public OutputReleItem()
            {
            }

            public OutputReleItem(ushort hiWord, ushort loWord)
            {
                SetItem(hiWord, loWord);
            }

            public void SetItem(ushort hiWord, ushort loWord)
            {
                _hiWord = hiWord;
                _loWord = loWord;
            }

            [XmlIgnore]
            [Browsable(false)]
            public bool IsBlinker
            {
                get { return 0 != (_hiWord & 256); }
                set
                {
                    if (value)
                    {
                        _hiWord = (ushort) (_hiWord | 256);
                    }
                    else
                    {
                        _hiWord = (ushort) (_hiWord | 256);
                        _hiWord -= 256;
                    }
                }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Description("��� ������� ����")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BlinkerTypeConverter))]
            public String Type
            {
                get
                {
                    string ret = "";
                    ret = IsBlinker ? "�������" : "�����������";
                    return ret;
                }
                set
                {
                    if ("�������" == value)
                    {
                        IsBlinker = true;
                    }
                    else if ("�����������" == value)
                    {
                        IsBlinker = false;
                    }
                    else
                    {
                        throw new ArgumentException("Rele type undefined", "Type");
                    }
                }
            }

            [XmlIgnore]
            [Browsable(false)]
            public byte SignalByte
            {
                get { return Common.LOBYTE(_hiWord); }
                set { _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), value); }
            }

            [XmlAttribute("������")]
            [DisplayName("���")]
            [Description("��� ������� ����")]
            [Category("��������� ���")]
            [TypeConverter(typeof (AllSignalsTypeConverter))]
            public string SignalString
            {
                get
                {
                    string ret = Strings.All[Strings.All.Count - 1];
                    if (Common.LOBYTE(_hiWord) <= Strings.All.Count)
                    {
                        ret = Strings.All[Common.LOBYTE(_hiWord)];
                    }
                    return ret;
                }
                set
                {
                    byte index = (byte) Strings.All.IndexOf(value);
                    _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), index);
                }
            }

            [XmlAttribute("�������")]
            [DisplayName("�������")]
            [Description("������������ �������� ����")]
            [Category("��������� ���")]
            public ulong ImpulseUlong
            {
                get { return Measuring.GetTime(_loWord); }
                set { _loWord = Measuring.SetTime(value); }
            }

            [XmlIgnore]
            [Browsable(false)]
            public ushort ImpulseUshort
            {
                get { return _loWord; }
                set { _loWord = value; }
            }
        } ;

        //[TestFixture]
        //public class TestCoutputRele
        //{
        //    [Test]
        //    public void COutputReleItem_IsRepeater()
        //    {
        //        OutputReleItem item = new OutputReleItem();
        //        item.SetItem(0xFFFF, 0xFFFF);
        //        Assert.IsTrue(item.IsBlinker);
        //        item.IsBlinker = false;
        //        Assert.AreEqual(item.Value[0], 0xFEFF);
        //        item.IsBlinker = true;
        //        Assert.AreEqual(item.Value[0], 0xFFFF);
        //        item.SetItem(0, 0);
        //        Assert.IsFalse(item.IsBlinker);
        //        item.IsBlinker = true;
        //        Assert.IsTrue(item.IsBlinker);
        //        Assert.AreEqual(item.Value[0], 256);
        //    }

        //    [Test]
        //    public void Bits()
        //    {
        //        Assert.AreEqual(0x1 | 0x2, 0x3);
        //        Assert.AreEqual(0xF7, 0xFF & ~0x8);
        //    }

        //    [Test]
        //    public void COutputReleItem_Signal()
        //    {
        //        OutputReleItem item = new OutputReleItem();
        //        item.SetItem(0xFFAA, 0);
        //        Assert.AreEqual(item.SignalByte, 0xAA);
        //        item.SetItem(0x0011, 0);
        //        Assert.AreEqual(item.SignalByte, 0x11);
        //        item.SignalByte = 2;
        //        Assert.AreEqual(item.Value[0], 0x02);

        //        item.SetItem(0xFF02, 0);
        //        Assert.AreEqual(item.SignalString, "����. ����.");
        //        item.SetItem(0xFFEF, 0);
        //        Assert.AreEqual(item.SignalString, "U0>> ������� ���.");
        //        item.SignalString = "����. ����.";
        //        Assert.AreEqual(item.SignalByte, 2);
        //        Assert.AreEqual(item.SignalString, "����. ����.");
        //    }
        //} ;


        [DisplayName("�������� ����")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("�������� �������")]
        [XmlElement("��������_����")]
        public COutputRele OutputRele
        {
            get { return _outputRele; }
        }

        #endregion

        #region �������� ����������

        public class COutputIndicator : ICollection
        {
            #region ICollection Members

            public void Add(OutputIndicatorItem item)
            {
                _indList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _indList.GetEnumerator();
            }

            #endregion

            [DisplayName("����������")]
            public int Count
            {
                get { return _indList.Count; }
            }


            public const int LENGTH = 12;
            public const int COUNT = 6;
            public const int OFFSET = 8;

            private List<OutputIndicatorItem> _indList = new List<OutputIndicatorItem>(COUNT);

            public COutputIndicator()
            {
                SetBuffer(new ushort[LENGTH]);
            }

            public void SetBuffer(ushort[] values)
            {
                _indList.Clear();
                if (values.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Output Indicator values", (object) LENGTH,
                                                          "Output indicator length must be 16");
                }
                for (int i = 0; i < LENGTH; i += 2)
                {
                    _indList.Add(new OutputIndicatorItem(values[i], values[i + 1]));
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] ret = new ushort[LENGTH];
                for (int i = 0; i < COUNT; i++)
                {
                    ret[i*2] = _indList[i].Value[0];
                    ret[i*2 + 1] = _indList[i].Value[1];
                }
                return ret;
            }

            public OutputIndicatorItem this[int i]
            {
                get { return _indList[i]; }
                set { _indList[i] = value; }
            }
        } ;

        public class OutputIndicatorItem
        {
            private ushort _hiWord;
            private ushort _loWord;

            public override string ToString()
            {
                return "�������� ���������";
            }

            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [Category("������� ���")]
            [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            public ushort[] Value
            {
                get { return new ushort[] {_hiWord, _loWord}; }
            }

            public OutputIndicatorItem()
            {
            }

            public OutputIndicatorItem(ushort hiWord, ushort loWord)
            {
                SetItem(hiWord, loWord);
            }

            public void SetItem(ushort hiWord, ushort loWord)
            {
                _hiWord = hiWord;
                _loWord = loWord;
            }

            [XmlIgnore]
            [Browsable(false)]
            public bool IsBlinker
            {
                get 
                {
                    //return 0 != (_hiWord & 256); 
                    return 0 != Common.HIBYTE(_hiWord);
                }
                set
                {
                    //if (value)
                    //{
                    //    _hiWord = (ushort) (_hiWord | 256);
                    //}
                    //else
                    //{
                    //    _hiWord = (ushort) (_hiWord | 256);
                    //    _hiWord -= 256;
                    //}

                    if (value)
                    {
                        _hiWord = Common.TOWORD(0xff, Common.LOBYTE(_hiWord));
                    }
                    else
                    {
                        _hiWord = Common.TOWORD(0x0, Common.LOBYTE(_hiWord));
                    }
                }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Description("��� ������� ����������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BlinkerTypeConverter))]
            public String Type
            {
                get
                {
                    string ret = "";
                    ret = IsBlinker ? "�������" : "�����������";
                    return ret;
                }
                set
                {
                    if ("�������" == value)
                    {
                        IsBlinker = true;
                    }
                    else if ("�����������" == value)
                    {
                        IsBlinker = false;
                    }
                    else
                    {
                        throw new ArgumentException("Rele type undefined", "Type");
                    }
                }
            }

            [XmlIgnore]
            [Browsable(false)]
            public byte SignalByte
            {
                get { return Common.LOBYTE(_hiWord); }
                set { _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), value); }
            }

            [XmlAttribute("������")]
            [DisplayName("���")]
            [Description("��� ������� ����������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (AllSignalsTypeConverter))]
            public string SignalString
            {
                get
                {
                    string ret = Strings.All[Strings.All.Count - 1];
                    if (Common.LOBYTE(_hiWord) <= Strings.All.Count)
                    {
                        ret = Strings.All[Common.LOBYTE(_hiWord)];
                    }
                    return ret;
                }
                set
                {
                    byte index = (byte) Strings.All.IndexOf(value);
                    _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), index);
                }
            }

            [XmlAttribute("���������")]
            [DisplayName("������ ���������")]
            [Description("������ '����� ���������'")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool Indication
            {
                get { return 0 != (_loWord & 0x1); }
                set { _loWord = value ? (ushort) (_loWord | 0x1) : (ushort) (_loWord & ~0x1); }
            }

            [XmlAttribute("������")]
            [DisplayName("�. ������")]
            [Description("������ '����� ������� ������'")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool Alarm
            {
                get { return 0 != (_loWord & 0x2); }
                set { _loWord = value ? (ushort) (_loWord | 0x2) : (ushort) (_loWord & ~0x2); }
            }

            [XmlAttribute("�������")]
            [DisplayName("�. �������")]
            [Description("������ '����� ������� �������'")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool System
            {
                get { return 0 != (_loWord & 0x4); }
                set { _loWord = value ? (ushort) (_loWord | 0x4) : (ushort) (_loWord & ~0x4); }
            }
        } ;

        private COutputIndicator _outputIndicator = new COutputIndicator();

        [XmlElement("��������_����������")]
        [DisplayName("�������� ����������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("�������� �������")]
        public COutputIndicator OutputIndicator
        {
            get { return _outputIndicator; }
        }

        #endregion

        #region �������� ���������� �������

        public const int OUTPUTLOGIC_OFFSET = 0x38;

        public BitArray GetOutputLogicSignals(int channel)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }

            ushort[] logicBuffer = new ushort[5];
            Array.ConstrainedCopy(_outputSignals.Value, OUTPUTLOGIC_OFFSET + channel*5, logicBuffer, 0, 5);
            BitArray buffer = new BitArray(Common.TOBYTES(logicBuffer, false));
            BitArray ret = new BitArray(72);
            for (int i = 0; i < ret.Count; i++)
            {
                ret[i] = buffer[i];
            }
            return ret;
        }

        public void SetOutputLogicSignals(int channel, BitArray values)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            if (72 != values.Count)
            {
                throw new ArgumentOutOfRangeException("OutputLogic bits count");
            }

            ushort[] logicBuffer = new ushort[5];
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    if (i*16 + j < 72)
                    {
                        if (values[i*16 + j])
                        {
                            logicBuffer[i] += (ushort) Math.Pow(2, j);
                        }
                    }
                }
            }
            Array.ConstrainedCopy(logicBuffer, 0, _outputSignals.Value, OUTPUTLOGIC_OFFSET + channel*5, 5);
        }

        #endregion

        #endregion
        
        #region ������� �������

        #region ��������� ���� � ����������

        [XmlElement("��")]
        [DisplayName("��")]
        [Description("����������� ��")]
        [Category("��������� ���� � ����������")]
        public double TN
        {
            get { return Measuring.GetConstraint(_inputSignals.Value[0], ConstraintKoefficient.K_Undefine); }
            set { _inputSignals.Value[0] = Measuring.SetConstraint(value, ConstraintKoefficient.K_Undefine); }
        }


        [XmlElement("����")]
        [DisplayName("����")]
        [Description("����������� ����")]
        [Category("��������� ���� � ����������")]
        public double TNNP
        {
            get { return Measuring.GetConstraint(_inputSignals.Value[1], ConstraintKoefficient.K_Undefine); }
            set { _inputSignals.Value[1] = Measuring.SetConstraint(value, ConstraintKoefficient.K_Undefine); }
        }

        #endregion

        #region �������������� �������

        [XmlElement("������������_�������")]
        [DisplayName("������������ �������")]
        [Description("������������ �� ��������� ������ �������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string ConstraintGroup
        {
            get
            {
                ushort index = _inputSignals.Value[4];

                if (Strings.Diskret.Count <= index)
                {
                    index = (ushort) (Strings.Diskret.Count - 1);
                }

                return Strings.Diskret[(int) index];
            }
            set { _inputSignals.Value[4] = (ushort) (Strings.Diskret.IndexOf(value)); }
        }

        [XmlElement("�����_����������")]
        [DisplayName("����� ����������")]
        [Description("������ ������ ����������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string IndicationReset
        {
            get
            {
                ushort index = _inputSignals.Value[5];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[(int) index];
            }
            set { _inputSignals.Value[5] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�������������_��")]
        [DisplayName("������������� ��")]
        [Description("������� ������ ������ ��")]
        [Category("������� �������")]
        public string TN_Dispepair
        {
            get
            {
                ushort index = _inputSignals.Value[7];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Diskret.Count - 1);
                }

                return Strings.Diskret[(int) index];
            }
            set { _inputSignals.Value[7] = (ushort) (Strings.Diskret.IndexOf(value)); }
        }

        #endregion

        #region ������� ���������� �������

        private const int INPUT_LOGIC_START = 0x8;
        private const int INPUT_LOGIC_COUNT = 0x8;

        public LogicState[] GetInputLogicSignals(int channel)
        {
            if (channel < 0 || channel >= INPUT_LOGIC_COUNT)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            LogicState[] ret = new LogicState[INPUT_LOGIC_COUNT];
            SetLogicStates(_inputSignals.Value[INPUT_LOGIC_START + channel], ref ret);
            return ret;
        }

        public void SetInputLogicSignals(int channel, LogicState[] logicSignals)
        {
            if (channel < 0 || channel >= INPUT_LOGIC_COUNT)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            ushort firstWord = 0;
            ushort secondWord = 0;

            for (int i = 0; i < INPUT_LOGIC_COUNT; i++)
            {
                if (LogicState.�� == logicSignals[i])
                {
                    firstWord += (ushort) Math.Pow(2, i);
                }
                if (LogicState.������ == logicSignals[i])
                {
                    firstWord += (ushort) Math.Pow(2, i);
                    firstWord += (ushort) Math.Pow(2, i + 8);
                }
            }
            _inputSignals.Value[INPUT_LOGIC_START + channel] = firstWord;
            _inputSignals.Value[INPUT_LOGIC_START + channel + 1] = secondWord;
        }

        private void SetLogicStates(ushort value, ref LogicState[] logicArray)
        {
            byte lowByte = Common.LOBYTE(value);
            byte hiByte = Common.HIBYTE(value);

            int start = 0;
            int end = INPUT_LOGIC_COUNT;

            for (int i = start; i < end; i++)
            {
                int pow = i;
                logicArray[i] = 0 == ((lowByte) & (byte) (Math.Pow(2, pow))) ? logicArray[i] : LogicState.��;
                logicArray[i] = IsLogicEquals(hiByte, pow) && (logicArray[i] == LogicState.��)
                                    ? LogicState.������
                                    : logicArray[i];
            }
        }

        private bool IsLogicEquals(byte hiByte, int pow)
        {
            return (byte) (Math.Pow(2, pow)) == ((hiByte) & (byte) (Math.Pow(2, pow)));
        }

        #endregion

        #endregion

        public class SignalsListAttribute : Attribute
        {
            private String _listType;

            public String ListType
            {
                get { return _listType; }
                set { _listType = value; }
            }

            public SignalsListAttribute(string listType)
            {
                _listType = listType;
            }
        } ;

        #region ������� ������

        private slot _externalDefenses1 = new slot(0x1100, 0x1140);
        private slot _externalDefenses2 = new slot(0x1140, 0x1180);

        public class CExternalDefenses : ICollection
        {
            public const int LENGTH = 0x40;
            public const int COUNT = 8;

            #region ICollection Members

            public void Add(ExternalDefenseItem item)
            {
                _defenseList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _defenseList.GetEnumerator();
            }

            #endregion

            public CExternalDefenses()
            {
                SetBuffer(new ushort[LENGTH]);
            }

            private List<ExternalDefenseItem> _defenseList = new List<ExternalDefenseItem>(COUNT);

            public void SetBuffer(ushort[] buffer)
            {
                _defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("External defense values", (object) LENGTH,
                                                          "External defense length must be 48");
                }
                for (int i = 0; i < 0x20; i += 4)
                {
                    ushort[] temp = new ushort[ExternalDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, 4);
                    Array.ConstrainedCopy(buffer, i + 0x20, temp, 4, 4);
                    _defenseList.Add(new ExternalDefenseItem(temp));
                }
                for (int i = 0; i < COUNT; i++)
                {
                    _defenseList[i].Name = "��-" + (i + 1);
                }
            }

            [DisplayName("����������")]
            public int Count
            {
                get { return _defenseList.Count; }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];

                for (int i = 0; i < Count; i++)
                {
                    Array.ConstrainedCopy(_defenseList[i].Values, 0, buffer, i*ExternalDefenseItem.LENGTH,
                                          ExternalDefenseItem.LENGTH);
                }
                return buffer;
            }

            public ExternalDefenseItem this[int i]
            {
                get { return _defenseList[i]; }
                set { _defenseList[i] = value; }
            }
        } ;

        public class ExternalDefenseItem
        {
            public const int LENGTH = 8;
            private ushort[] _values = new ushort[LENGTH];

            public ExternalDefenseItem()
            {
            }

            public ExternalDefenseItem(ushort[] buffer)
            {
                SetItem(buffer);
            }

            private string _name;

            [DisplayName("���")]
            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }

            public override string ToString()
            {
                return "������� ������";
            }

            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [Category("������� ���")]
            [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            public ushort[] Values
            {
                get { return _values; }
                set { SetItem(value); }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("External defense item value", (object) LENGTH,
                                                          "External defense item length must be 6");
                }
                _values = buffer;
            }

            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [Description("����� ������ ������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ModeTypeConverter))]
            [SignalsList("Modes2")]
            public string Mode
            {
                get
                {
                    byte index = (byte) (Common.LOBYTE(_values[0]) & 0x0F);
                    if (index >= Strings.Modes2.Count)
                    {
                        index = (byte) (Strings.Modes2.Count - 1);
                    }
                    return Strings.Modes2[index];
                }
                set { _values[0] = Common.SetBits(_values[0], (ushort) Strings.Modes2.IndexOf(value), 0, 1, 2); }
            }

            [XmlAttribute("�������")]
            [DisplayName("�������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool Return
            {
                get { return Common.GetBit(_values[0], 8); }
                set { _values[0] = Common.SetBit(_values[0], 8, value); }
            }


            [XmlAttribute("����������_�����")]
            [DisplayName("����� ����������")]
            [Description("����� ����� ���������� ��")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ExternalDefensesTypeConverter))]
            [SignalsList("Logic")]
            public string BlockingNumber
            {
                get
                {
                    ushort value = (ushort) (_values[2] & 0xFF);
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set { _values[2] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlAttribute("������������_�����")]
            [DisplayName("����� ������������")]
            [Description("����� ����� ������������ ��")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ExternalDefensesTypeConverter))]
            [SignalsList("External")]
            public string WorkingNumber
            {
                get
                {
                    ushort value = _values[1];
                    if (value >= Strings.External.Count)
                    {
                        value = (ushort) (Strings.External.Count - 1);
                    }
                    return Strings.External[value];
                }
                set 
                { 
                    _values[1] = (ushort) Strings.External.IndexOf(value); 
                }
            }

            [XmlAttribute("������������_�����")]
            [DisplayName("����� ������������")]
            [Description("�������� ������� ������������ ��")]
            [Category("��������� ���")]
            public ulong WorkingTime
            {
                get { return Measuring.GetTime(_values[3]); }
                set { _values[3] = Measuring.SetTime(value); }
            }

            [XmlAttribute("�������_�����")]
            [DisplayName("����� ��������")]
            [Description("����� ����� �������� ��")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ExternalDefensesTypeConverter))]
            [SignalsList("External")]
            public string ReturnNumber
            {
                get
                {
                    ushort value = _values[4];
                    if (value >= Strings.External.Count)
                    {
                        value = (ushort) (Strings.External.Count - 1);
                    }
                    return Strings.External[value];
                }
                set { _values[4] = (ushort) Strings.External.IndexOf(value); }
            }

            [XmlAttribute("�������_�����")]
            [DisplayName("����� ��������")]
            [Description("�������� ������� �������� ��")]
            [Category("��������� ���")]
            public ulong ReturnTime
            {
                get { return Measuring.GetTime(_values[5]); }
                set { _values[5] = Measuring.SetTime(value); }
            }
        }

        private CExternalDefenses _cexternalDefensesMain = new CExternalDefenses();
        private CExternalDefenses _cexternalDefensesResereve = new CExternalDefenses();

        [XmlElement("�������_������_��������")]
        [DisplayName("������� ������ ��������")]
        [Description("������� ������ - �������� ������ �������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("������� ������")]
        public CExternalDefenses ExternalDefensesMain
        {
            get { return _cexternalDefensesMain; }
            set { _cexternalDefensesMain = value; }
        }

        [XmlElement("�������_������_���������")]
        [DisplayName("������� ������ ���������")]
        [Description("������� ������ - ��������� ������ �������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("������� ������")]
        public CExternalDefenses ExternalDefensesReserve
        {
            get { return _cexternalDefensesResereve; }
            set { _cexternalDefensesResereve = value; }
        }

        #endregion

        #region ������ �� ����������

        private slot _voltageDefensesMain = new slot(0x1000, 0x1040);
        private slot _voltageDefensesReserve = new slot(0x1040, 0x1080);

        public void LoadVoltageDefenses()
        {
            LoadSlot(DeviceNumber, _voltageDefensesReserve, "LoadVoltageDefensesReserve" + DeviceNumber, this);
            LoadSlot(DeviceNumber, _voltageDefensesMain, "LoadVoltageDefensesMain" + DeviceNumber, this);
        }

        public void SaveVoltageDefenses()
        {
            Array.ConstrainedCopy(VoltageDefensesMain.ToUshort(), 0, _voltageDefensesMain.Value, 0,
                                  CVoltageDefenses.LENGTH);
            Array.ConstrainedCopy(VoltageDefensesReserve.ToUshort(), 0, _voltageDefensesReserve.Value, 0,
                                  CVoltageDefenses.LENGTH);
            SaveSlot(DeviceNumber, _voltageDefensesReserve, "SaveVoltageDefensesReserve" + DeviceNumber, this);
            SaveSlot(DeviceNumber, _voltageDefensesMain, "SaveVoltageDefensesMain" + DeviceNumber, this);
        }

        public event Handler VoltageDefensesLoadOk;
        public event Handler VoltageDefensesLoadFail;
        public event Handler VoltageDefensesSaveOk;
        public event Handler VoltageDefensesSaveFail;

        private CVoltageDefenses _cvoltageDefensesMain = new CVoltageDefenses();
        private CVoltageDefenses _cvoltageDefensesReserve = new CVoltageDefenses();

        [XmlElement("������_����������_��������")]
        [DisplayName("�������� ������")]
        [Description("������ �� ���������� - �������� ������ �������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("������ �� ����������")]
        public CVoltageDefenses VoltageDefensesMain
        {
            get { return _cvoltageDefensesMain; }
            set { this._cvoltageDefensesMain = value; }
        }

        [XmlElement("������_����������_���������")]
        [DisplayName("��������� ������")]
        [Description("������ �� ���������� - ��������� ������ �������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("������ �� ����������")]
        public CVoltageDefenses VoltageDefensesReserve
        {
            get { return _cvoltageDefensesReserve; }
            set { this._cvoltageDefensesReserve = value; }
        }

        public class CVoltageDefenses : ICollection
        {
            public const int LENGTH = 64;
            public const int COUNT = 16;

            #region ICollection Members

            public void Add(VoltageDefenseItem item)
            {
                _defenseList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            [XmlIgnore]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            [XmlIgnore]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _defenseList.GetEnumerator();
            }

            #endregion

            private List<VoltageDefenseItem> _defenseList = new List<VoltageDefenseItem>(COUNT);

            public CVoltageDefenses()
            {
                SetBuffer(new ushort[LENGTH]);
            }

            public void SetBuffer(ushort[] buffer)
            {
                _defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("������ �� ����������", (object) LENGTH,
                                                          "����� ��� ����� �� ���������� ������ ���� " + LENGTH);
                }
                for (int i = 0; i < LENGTH; i += VoltageDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[VoltageDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, VoltageDefenseItem.LENGTH);
                    _defenseList.Add(new VoltageDefenseItem(temp));
                }

                _defenseList[0].Name = "U>";
                _defenseList[1].Name = "U>>";
                _defenseList[2].Name = "U>>>";
                _defenseList[3].Name = "U>>>>";
                _defenseList[4].Name = "U<";
                _defenseList[5].Name = "U<<";
                _defenseList[6].Name = "U<<<";
                _defenseList[7].Name = "U<<<<";
                _defenseList[8].Name = "U0>";
                _defenseList[9].Name = "U0>>";
                _defenseList[10].Name = "U0>>>";
                _defenseList[11].Name = "U0>>>>";
                _defenseList[12].Name = "U1<";
                _defenseList[13].Name = "U1<<";
                _defenseList[14].Name = "U2>";
                _defenseList[15].Name = "U2>>";

                for (int i = 0; i < 8; i++)
                {
                    _defenseList[i].DefenseType = VoltageDefenseType.U;
                }
                for (int i = 8; i < 12; i++)
                {
                    _defenseList[i].DefenseType = VoltageDefenseType.U0;
                }
                for (int i = 12; i < 16; i++)
                {
                    _defenseList[i].DefenseType = VoltageDefenseType.U12;
                }
            }

            [DisplayName("����������")]
            public int Count
            {
                get { return _defenseList.Count; }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                for (int i = 0; i < Count; i++)
                {
                    Array.ConstrainedCopy(_defenseList[i].Values, 0, buffer, i*VoltageDefenseItem.LENGTH,
                                          VoltageDefenseItem.LENGTH);
                }
                return buffer;
            }

            public VoltageDefenseItem this[int i]
            {
                get { return _defenseList[i]; }
                set { _defenseList[i] = value; }
            }
        } ;

        public enum VoltageDefenseType
        {
            U,
            U12,
            U0
        } ;

        public class VoltageDefenseItem
        {
            public const int LENGTH = 4;
            private ushort[] _values = new ushort[LENGTH];

            public override string ToString()
            {
                return "������ �� ����������";
            }

            public VoltageDefenseItem()
            {
            }

            public VoltageDefenseItem(ushort[] buffer)
            {
                SetItem(buffer);
            }

            private VoltageDefenseType _type = VoltageDefenseType.U;

            [XmlAttribute("���")]
            [Browsable(false)]
            public VoltageDefenseType DefenseType
            {
                get { return _type; }
                set { _type = value; }
            }

            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [Category("������� ���")]
            [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            public ushort[] Values
            {
                get { return _values; }
                set { SetItem(value); }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("����� ������ �� ����������", (object) LENGTH,
                                                          "����� ����� ������ ���� ������" + LENGTH);
                }
                _values = buffer;
            }

            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    ushort index = Common.GetBits(_values[0], 0, 1, 2);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte) (Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set { _values[0] = Common.SetBits(_values[0], (ushort) Strings.Modes.IndexOf(value), 0, 1, 2); }
            }

            [XmlAttribute("���_�������")]
            [DisplayName("��� �������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ModeTypeConverter))]
            public string SignalType
            {
                get
                {
                    List<string> signalsList = null;
                    switch (DefenseType)
                    {
                        case VoltageDefenseType.U:
                            signalsList = Strings.VoltageTypesU;
                            break;
                        case VoltageDefenseType.U12:
                            signalsList = Strings.VoltageTypesU12;
                            break;
                        case VoltageDefenseType.U0:
                            signalsList = Strings.VoltageTypesU0;
                            break;
                        default:
                            break;
                    }
                    ushort index = (ushort) (Common.GetBits(_values[0], 8, 9) >> 8);
                    if (index >= signalsList.Count)
                    {
                        index = (byte) (signalsList.Count - 1);
                    }
                    return signalsList[index];
                }
                set
                {
                    List<string> signalsList = null;
                    switch (DefenseType)
                    {
                        case VoltageDefenseType.U:
                            signalsList = Strings.VoltageTypesU;
                            break;
                        case VoltageDefenseType.U12:
                            signalsList = Strings.VoltageTypesU12;
                            break;
                        case VoltageDefenseType.U0:
                            signalsList = Strings.VoltageTypesU0;
                            break;
                        default:
                            break;
                    }
                    _values[0] = Common.SetBits(_values[0], (ushort) signalsList.IndexOf(value), 8, 9);
                }
            }

            [XmlAttribute("����������_�����")]
            [DisplayName("���������� �����")]
            [Description("����� ����� ����������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (LogicTypeConverter))]
            public string BlockingNumber
            {
                get
                {
                    ushort value = (ushort) (_values[2] & 0xFF);
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set { _values[2] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlAttribute("������������_�������")]
            [DisplayName("������������ �������")]
            [Description("������� ������������")]
            [Category("��������� ���")]
            public double WorkConstraint
            {
                get
                {
                    double ret = 0.0;
                    ret = Measuring.GetConstraint(_values[1], ConstraintKoefficient.K_25600);
                    return ret;
                }
                set { _values[1] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
            }

            [XmlAttribute("������������_�����")]
            [DisplayName("������������ �����")]
            [Description("�������� ������� ������������")]
            [Category("��������� ���")]
            public ulong WorkTime
            {
                get { return Measuring.GetTime(_values[3]); }
                set { _values[3] = Measuring.SetTime(value); }
            }

            private string _name;

            [Browsable(false)]
            [XmlIgnore]
            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }
        }

        #endregion

        #region �������� ����� �� �������

        private slot _frequenceDefensesMain = new slot(0x1080, 0x10C0);
        private slot _frequenceDefensesReserve = new slot(0x10C0, 0x1100);

        public void LoadFrequenceDefenses()
        {
            LoadSlot(DeviceNumber, _frequenceDefensesReserve, "LoadFrequenceDefensesReserve" + DeviceNumber, this);
            LoadSlot(DeviceNumber, _frequenceDefensesMain, "LoadFrequenceDefensesMain" + DeviceNumber, this);
        }

        public void SaveFrequenceDefenses()
        {
            Array.ConstrainedCopy(FrequenceDefensesMain.ToUshort(), 0, _frequenceDefensesMain.Value, 0,
                                  CFrequenceDefenses.LENGTH);
            Array.ConstrainedCopy(FrequenceDefensesReserve.ToUshort(), 0, _frequenceDefensesReserve.Value, 0,
                                  CFrequenceDefenses.LENGTH);
            SaveSlot(DeviceNumber, _frequenceDefensesReserve, "SaveFrequenceDefensesReserve" + DeviceNumber, this);
            SaveSlot(DeviceNumber, _frequenceDefensesMain, "SaveFrequenceDefensesMain" + DeviceNumber, this);
        }

        public event Handler FrequenceDefensesLoadOk;
        public event Handler FrequenceDefensesLoadFail;
        public event Handler FrequenceDefensesSaveOk;
        public event Handler FrequenceDefensesSaveFail;

        private CFrequenceDefenses _cFrequenceDefensesMain = new CFrequenceDefenses();
        private CFrequenceDefenses _cFrequenceDefensesReserve = new CFrequenceDefenses();

        [XmlElement("������_��_�������_��������")]
        [DisplayName("�������� ������")]
        [Description("������ �� ������� - �������� ������ �������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("������ �� �������")]
        public CFrequenceDefenses FrequenceDefensesMain
        {
            get { return _cFrequenceDefensesMain; }
            set { this._cFrequenceDefensesMain= value; }
        }

        [XmlElement("������_��_�������_���������")]
        [DisplayName("��������� ������")]
        [Description("������ �� ������� - ��������� ������ �������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("������ �� �������")]
        public CFrequenceDefenses FrequenceDefensesReserve
        {
            get { return _cFrequenceDefensesReserve; }
            set { this._cFrequenceDefensesReserve = value; }

        }

        public class CFrequenceDefenses : ICollection
        {
            public const int LENGTH = 0x40;
            public const int COUNT = 8;

            #region ICollection Members

            public void Add(FrequenceDefenseItem item)
            {
                _defenseList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            [XmlIgnore]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            [XmlIgnore]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _defenseList.GetEnumerator();
            }

            #endregion

            private List<FrequenceDefenseItem> _defenseList = new List<FrequenceDefenseItem>(COUNT);

            public CFrequenceDefenses()
            {
                SetBuffer(new ushort[LENGTH]);
            }

            public void SetBuffer(ushort[] buffer)
            {
                _defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("������ �� �������", (object) LENGTH,
                                                          "����� ��� ����� �� ������� ������ ���� " + LENGTH);
                }
                for (int i = 0; i < LENGTH; i += FrequenceDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[FrequenceDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, FrequenceDefenseItem.LENGTH);
                    _defenseList.Add(new FrequenceDefenseItem(temp));
                }
                _defenseList[0].Name = "F<";
                _defenseList[1].Name = "F<<";
                _defenseList[2].Name = "F<<<";
                _defenseList[3].Name = "F<<<<";
                _defenseList[4].Name = "F>";
                _defenseList[5].Name = "F>>";
                _defenseList[6].Name = "F>>>";
                _defenseList[7].Name = "F>>>>";

                
                for (int i = 0; i < 4; i++)
                {
                    _defenseList[i].HaveCAPV = false;
                }
            }

            [DisplayName("����������")]
            public int Count
            {
                get { return _defenseList.Count; }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                for (int i = 0; i < Count; i++)
                {
                    Array.ConstrainedCopy(_defenseList[i].Values, 0, buffer, i*FrequenceDefenseItem.LENGTH,
                                          FrequenceDefenseItem.LENGTH);
                }
                return buffer;
            }

            public FrequenceDefenseItem this[int i]
            {
                get { return _defenseList[i]; }
                set { _defenseList[i] = value; }
            }
        } ;

        public class FrequenceDefenseItem
        {
            public const int LENGTH = 8;
            private ushort[] _values = new ushort[LENGTH];

            private Boolean _haveCAPV;

            public Boolean HaveCAPV
            {
                get { return _haveCAPV; }
                set { _haveCAPV = value; }
            }


            private string _name;

            [Browsable(false)]
            [XmlIgnore]
            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }

            public FrequenceDefenseItem()
            {
            }

            public FrequenceDefenseItem(ushort[] buffer)
            {
                SetItem(buffer);
            }

            public override string ToString()
            {
                return "������ �� �������";
            }

            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [Category("������� ���")]
            [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            public ushort[] Values
            {
                get { return _values; }
                set { SetItem(value); }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("����� ������ �� �������", (object) LENGTH,
                                                          "����� ������� ������ ���� ������" + LENGTH);
                }
                _values = buffer;
            }

            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [Category("��������� ���")]
            [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            [TypeConverter(typeof (ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    ushort index = Common.GetBits(_values[0], 0, 1, 2);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte) (Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set { _values[0] = Common.SetBits(_values[0], (ushort) Strings.Modes.IndexOf(value), 0, 1, 2); }
            }

            [XmlAttribute("����������_�����")]
            [DisplayName("���������� �����")]
            [Description("����� ����� ����������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ModeTypeConverter))]
            public string BlockingNumber
            {
                get
                {
                    ushort value = (ushort) (_values[2] & 0xFF);
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set { _values[2] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlAttribute("�������")]
            [DisplayName("�������")]
            [Category("��������� ���")]
            public double Frequency
            {
                get
                {
                    double ret = 0.0;
                    ret = Measuring.GetConstraint(_values[1], ConstraintKoefficient.K_25600);
                    return ret;
                }
                set { _values[1] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
            }

            [XmlAttribute("��������")]
            [DisplayName("�������� ������������")]
            [Description("�������� ������� ������������")]
            [Category("��������� ���")]
            public ulong WorkTime
            {
                get { return Measuring.GetTime(_values[3]); }
                set { _values[3] = Measuring.SetTime(value); }
            }

            [XmlAttribute("����_�����")]
            [DisplayName("���� �����")]
            [Category("��������� ���")]
            [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            [TypeConverter(typeof (ModeTypeConverter))]
            public string CAPV_Mode
            {
                get
                {
                    ushort index = Common.GetBits(_values[4], 0, 1, 2, 3);
                    if (index >= Strings.ModesLight.Count)
                    {
                        index = (byte) (Strings.ModesLight.Count - 1);
                    }
                    return Strings.ModesLight[index];
                }
                set { _values[4] = Common.SetBits(_values[4], (ushort) Strings.ModesLight.IndexOf(value), 0, 1, 2); }
            }

            [XmlAttribute("����_����������_�����")]
            [DisplayName("���� ���������� �����")]
            [Description("����� ����� ���������� ����")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ModeTypeConverter))]
            public string CAPV_BlockingNumber
            {
                get
                {
                    ushort value = (ushort) (_values[6] & 0xFF);
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set { _values[6] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlAttribute("����_�������")]
            [DisplayName("���� �������")]
            [Category("��������� ���")]
            public double CAPV_Frequency
            {
                get
                {
                    double ret = 0.0;
                    ret = Measuring.GetConstraint(_values[5], ConstraintKoefficient.K_25600);
                    return ret;
                }
                set { _values[5] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
            }

            [XmlAttribute("����_��������")]
            [DisplayName("���� �������� ������������")]
            [Description("�������� ������� ������������ ����")]
            [Category("��������� ���")]
            public ulong CAPV_WorkTime
            {
                get { return Measuring.GetTime(_values[7]); }
                set { _values[7] = Measuring.SetTime(value); }
            }
        }

        #endregion
        
        #region ������� ��������/����������
        
        public void LoadInputSignals()
        {
            LoadSlot(DeviceNumber, _inputSignals, "LoadInputSignals" + DeviceNumber, this);
        }

        public void LoadOutputSignals()
        {
            LoadSlot(DeviceNumber, _outputSignals, "LoadOutputSignals" + DeviceNumber, this);
        }

        public void LoadExternalDefenses()
        {
            LoadSlot(DeviceNumber, _externalDefenses1, "LoadExternalDefenses1" + DeviceNumber, this);
            LoadSlot(DeviceNumber, _externalDefenses2, "LoadExternalDefenses2" + DeviceNumber, this);
        }

        public void SaveExternalDefenses()
        {
            if (CExternalDefenses.COUNT == ExternalDefensesMain.Count &&
                CExternalDefenses.COUNT == ExternalDefensesReserve.Count)
            {
                ushort[] bufferMain = ExternalDefensesMain.ToUshort();
                ushort[] bufferReserve = ExternalDefensesReserve.ToUshort();
                for (int i = 0; i < 0x40; i += ExternalDefenseItem.LENGTH)
                {
                    Array.ConstrainedCopy(bufferMain, i, _externalDefenses1.Value, i/2, 4);
                    Array.ConstrainedCopy(bufferMain, i + 4, _externalDefenses2.Value, i/2, 4);

                    Array.ConstrainedCopy(bufferReserve, i, _externalDefenses1.Value, i/2 + 0x20, 4);
                    Array.ConstrainedCopy(bufferReserve, i + 4, _externalDefenses2.Value, i/2 + 0x20, 4);
                }
                SaveSlot(DeviceNumber, _externalDefenses1, "SaveExternalDefenses1" + DeviceNumber, this);
                SaveSlot(DeviceNumber, _externalDefenses2, "SaveExternalDefenses2" + DeviceNumber, this);
            }
        }


        public void SaveOutputSignals()
        {
            if (COutputRele.COUNT == OutputRele.Count)
            {
                Array.ConstrainedCopy(OutputRele.ToUshort(), 0, _outputSignals.Value, COutputRele.OFFSET,
                                      COutputRele.LENGTH);
            }
            if (COutputIndicator.COUNT == OutputIndicator.Count)
            {
                Array.ConstrainedCopy(OutputIndicator.ToUshort(), 0, _outputSignals.Value, COutputIndicator.OFFSET,
                                      COutputIndicator.LENGTH);
            }
            SaveSlot(DeviceNumber, _outputSignals, "SaveOutputSignals" + DeviceNumber, this);
        }

        public void SaveInputSignals()
        {
            SaveSlot(DeviceNumber, _inputSignals, "SaveInputSignals" + DeviceNumber, this);
        }
        
        public void MakeDiagnosticQuick()
        {
            MakeQueryQuick("LoadDiagnostic" + DeviceNumber);
        }

        public void MakeAnalogSignalsQuick()
        {
            MakeQueryQuick("LoadAnalogSignals" + DeviceNumber);
        }

        public void MakeDiagnosticSlow()
        {
            MakeQuerySlow("LoadDiagnostic" + DeviceNumber);
        }

        public void MakeAnalogSignalsSlow()
        {
            MakeQuerySlow("LoadAnalogSignals");
        }

        public void MakeDateTimeQuick()
        {
            MakeQueryQuick("LoadDateTime" + DeviceNumber);
        }

        public void MakeDateTimeSlow()
        {
            MakeQuerySlow("LoadDateTime" + DeviceNumber);
        }

        public void SuspendDateTime(bool suspend)
        {
            MB.SuspendQuery("LoadDateTime" + DeviceNumber, suspend);
        }

        private slot _constraintSelector = new slot(0x400, 0x401);

        public void SelectConstraintGroup(bool mainGroup)
        {
            _constraintSelector.Value[0] = mainGroup ? (ushort) 0 : (ushort) 1;
            SaveSlot(DeviceNumber, _constraintSelector, "��600 �" + DeviceNumber + " ������������ ������ �������", this);
        }

        public void SuspendSystemJournal(bool suspend)
        {
            List<Query> q = MB.GetQueryByExpression("LoadSJRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.SuspendQuery(q[i].name, suspend);
            }
        }

        public void SuspendAlarmJournal(bool suspend)
        {
            List<Query> q = MB.GetQueryByExpression("LoadALRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.SuspendQuery(q[i].name, suspend);
            }
        }

        public void RemoveAlarmJournal()
        {
            List<Query> q = MB.GetQueryByExpression("LoadALRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.RemoveQuery(q[i].name);
            }
            _stopAlarmJournal = true;
        }

        public void RemoveSystemJournal()
        {
            List<Query> q = MB.GetQueryByExpression("LoadSJRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.RemoveQuery(q[i].name);
            }
            _stopSystemJournal = true;
        }
        
        public void LoadSystemJournal()
        {
            LoadSlot(DeviceNumber, _systemJournal[0], "LoadSJRecord0_" + DeviceNumber, this);
            _stopSystemJournal = false;
        }

        public void LoadAlarmJournal()
        {
            if (!_inputSignals.Loaded)
            {
                LoadInputSignals();
            }
            _stopAlarmJournal = false;
            LoadSlot(DeviceNumber, _alarmJournal[0], "LoadALRecord0_" + DeviceNumber, this);
        }

        public void ConfirmConstraint()
        {
            SetBit(DeviceNumber, 0, true, "��600 ������ �������������", this);
        }

        #endregion

        #region ������������

        public void Deserialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(binFileName);
            }
            catch (XmlException)
            {
                throw new FileLoadException("���� ������� ��600 ���������", binFileName);
            }
            try
            {
                DeserializeSlot(doc, "/��600_�������/�������_�������", _inputSignals);
                DeserializeSlot(doc, "/��600_�������/��������_�������", _outputSignals);
                DeserializeSlot(doc, "/��600_�������/������_�������1", _externalDefenses1);
                DeserializeSlot(doc, "/��600_�������/������_�������2", _externalDefenses2);
                DeserializeSlot(doc, "/��600_�������/������_����������_��������", _voltageDefensesMain);
                DeserializeSlot(doc, "/��600_�������/������_����������_���������", _voltageDefensesReserve);
                DeserializeSlot(doc, "/��600_�������/������_�������_��������", _frequenceDefensesMain);
                DeserializeSlot(doc, "/��600_�������/������_�������_���������", _frequenceDefensesReserve);
            }
            catch (NullReferenceException)
            {
                throw new FileLoadException("���� ������� ��600 ���������", binFileName);
            }

            ushort[] bufferMain = new ushort[CExternalDefenses.LENGTH];
            ushort[] bufferReserve = new ushort[CExternalDefenses.LENGTH];
            Array.ConstrainedCopy(_externalDefenses1.Value, 0, bufferMain, 0, 0x20);
            Array.ConstrainedCopy(_externalDefenses2.Value, 0, bufferMain, 0x20, 0x20);
            Array.ConstrainedCopy(_externalDefenses1.Value, 0x20, bufferReserve, 0, 0x20);
            Array.ConstrainedCopy(_externalDefenses2.Value, 0x20, bufferReserve, 0x20, 0x20);
            ExternalDefensesMain.SetBuffer(bufferMain);
            ExternalDefensesReserve.SetBuffer(bufferReserve);

            VoltageDefensesMain.SetBuffer(_voltageDefensesMain.Value);
            VoltageDefensesReserve.SetBuffer(_voltageDefensesReserve.Value);
            ushort[] releBuffer = new ushort[COutputRele.LENGTH];
            Array.ConstrainedCopy(_outputSignals.Value, COutputRele.OFFSET, releBuffer, 0, COutputRele.LENGTH);
            OutputRele.SetBuffer(releBuffer);
            ushort[] outputIndicator = new ushort[COutputIndicator.LENGTH];
            Array.ConstrainedCopy(_outputSignals.Value, COutputIndicator.OFFSET, outputIndicator, 0,
                                  COutputIndicator.LENGTH);
            OutputIndicator.SetBuffer(outputIndicator);
            FrequenceDefensesMain.SetBuffer(_frequenceDefensesMain.Value);
            FrequenceDefensesReserve.SetBuffer(_frequenceDefensesReserve.Value);
        }

        private void DeserializeSlot(XmlDocument doc, string nodePath, slot slot)
        {
            slot.Value = Common.TOWORDS(Convert.FromBase64String(doc.SelectSingleNode(nodePath).InnerText), true);
        }


        public void Serialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("��600_�������"));

            if (COutputRele.COUNT == OutputRele.Count)
            {
                Array.ConstrainedCopy(OutputRele.ToUshort(), 0, _outputSignals.Value, COutputRele.OFFSET,
                    COutputRele.LENGTH);
            }
            if (COutputIndicator.COUNT == OutputIndicator.Count)
            {
                Array.ConstrainedCopy(OutputIndicator.ToUshort(), 0, _outputSignals.Value, COutputIndicator.OFFSET,
                    COutputIndicator.LENGTH);
            }
            Array.ConstrainedCopy(VoltageDefensesMain.ToUshort(), 0, _voltageDefensesMain.Value, 0,
                CVoltageDefenses.LENGTH);
            Array.ConstrainedCopy(VoltageDefensesReserve.ToUshort(), 0, _voltageDefensesReserve.Value, 0,
                CVoltageDefenses.LENGTH);
            if (CExternalDefenses.COUNT == ExternalDefensesMain.Count &&
                CExternalDefenses.COUNT == ExternalDefensesReserve.Count)
            {
                ushort[] bufferMain = ExternalDefensesMain.ToUshort();
                ushort[] bufferReserve = ExternalDefensesReserve.ToUshort();

                for (int i = 0; i < 0x40; i += ExternalDefenseItem.LENGTH)
                {
                    Array.ConstrainedCopy(bufferMain, i, _externalDefenses1.Value, i/2, 4);
                    Array.ConstrainedCopy(bufferMain, i + 4, _externalDefenses2.Value, i/2, 4);

                    Array.ConstrainedCopy(bufferReserve, i, _externalDefenses1.Value, i/2 + 0x20, 4);
                    Array.ConstrainedCopy(bufferReserve, i + 4, _externalDefenses2.Value, i/2 + 0x20, 4);
                }
            }
            Array.ConstrainedCopy(FrequenceDefensesMain.ToUshort(), 0, _frequenceDefensesMain.Value, 0,
                CFrequenceDefenses.LENGTH);
            Array.ConstrainedCopy(FrequenceDefensesReserve.ToUshort(), 0, _frequenceDefensesReserve.Value, 0,
                CFrequenceDefenses.LENGTH);

            SerializeSlot(doc, "�������_�������", _inputSignals);
            SerializeSlot(doc, "��������_�������", _outputSignals);
            SerializeSlot(doc, "������_�������1", _externalDefenses1);
            SerializeSlot(doc, "������_�������2", _externalDefenses2);
            SerializeSlot(doc, "������_����������_��������", _voltageDefensesMain);
            SerializeSlot(doc, "������_����������_���������", _voltageDefensesReserve);
            SerializeSlot(doc, "������_�������_��������", _frequenceDefensesMain);
            SerializeSlot(doc, "������_�������_���������", _frequenceDefensesReserve);

            doc.Save(binFileName);
        }

        private void SerializeSlot(XmlDocument doc, string nodeName, slot slot)
        {
            XmlElement element = doc.CreateElement(nodeName);
            element.InnerText = Convert.ToBase64String(Common.TOBYTES(slot.Value, true));
            doc.DocumentElement.AppendChild(element);
        }

        #endregion

        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof (MR600); }
        }

        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mr600; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "��600"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion

        private new void mb_CompleteExchange(object sender, Query query)
        {
            #region ������ �� �������

            
            if ("LoadFrequenceDefensesReserve" + DeviceNumber == query.name)
            {
                if (query.fail == 0)
                {
                    _frequenceDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);
                    FrequenceDefensesReserve.SetBuffer(_frequenceDefensesReserve.Value);
                }
            }
            if ("LoadFrequenceDefensesMain" + DeviceNumber == query.name)
            {
                _frequenceDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
                FrequenceDefensesMain.SetBuffer(_frequenceDefensesMain.Value);
                if (query.fail == 0)
                {
                    if (null != FrequenceDefensesLoadOk)
                    {
                        FrequenceDefensesLoadOk(this);
                    }
                }
                else
                {
                    if (null != FrequenceDefensesLoadFail)
                    {
                        FrequenceDefensesLoadFail(this);
                    }
                }
            }
            if ("SaveFrequenceDefensesMain" + DeviceNumber == query.name)
            {
                Raise(query, FrequenceDefensesSaveOk, FrequenceDefensesSaveFail);
            }

            #endregion

            #region ������ ����������

            if ("LoadVoltageDefensesReserve" + DeviceNumber == query.name)
            {
                if (query.fail == 0)
                {
                    _voltageDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);
                    VoltageDefensesReserve.SetBuffer(_voltageDefensesReserve.Value);
                }
            }
            if ("LoadVoltageDefensesMain" + DeviceNumber == query.name)
            {
                _voltageDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
                VoltageDefensesMain.SetBuffer(_voltageDefensesMain.Value);
                if (query.fail == 0)
                {
                    if (null != VoltageDefensesLoadOk)
                    {
                        VoltageDefensesLoadOk(this);
                    }
                }
                else
                {
                    if (null != VoltageDefensesLoadFail)
                    {
                        VoltageDefensesLoadFail(this);
                    }
                }
            }
            if ("SaveVoltageDefensesMain" + DeviceNumber == query.name)
            {
                Raise(query, VoltageDefensesSaveOk, VoltageDefensesSaveFail);
            }

            #endregion

            #region �������������

            //if ("��600 �" + DeviceNumber + " ������ �������������" == query.QueryName)
            //{
            //    _osciloscopeDataIndex++;

            //    if (!query.Error)
            //    {
            //        if (null != OscilloscopeDataIndexLoadOk)
            //        {
            //            _oscilloscopeData[_osciloscopeDataIndex - 1].Value = Common.TOWORDS(query.Buffer,true);
            //            OscilloscopeDataIndexLoadOk(this, _osciloscopeDataIndex);
            //        }
            //    }
            //    else
            //    {
            //        _oscilloscopeDataHaveErrors = true;
            //        if (null != OscilloscopeDataIndexLoadFail)
            //        {
            //            OscilloscopeDataIndexLoadFail(this, _osciloscopeDataIndex);
            //        }
            //    }
            //    if (COscilloscope._exchangeCnt == _osciloscopeDataIndex)
            //    {
            //        if (_oscilloscopeDataHaveErrors)
            //        {
            //            if (null != OscilloscopeDataLoadFail)
            //            {
            //                OscilloscopeDataLoadFail(this);
            //            }
            //        }
            //        else
            //        {
            //            if (null != OscilloscopeDataLoadOk)
            //            {
            //                Oscilloscope.SetData(_oscilloscopeData);
            //                OscilloscopeDataLoadOk(this);
            //            }

            //        }
            //    }
            //}
            //if ("��600 �" + DeviceNumber + " ��������� ��������� ������������" == query.QueryName)
            //{
            //    if (!query.Error)
            //    {
            //        _oscSettings.Value = Common.TOWORDS(query.Buffer, true);
            //    }
            //}

            //if ("��600 �" + DeviceNumber + " ��������� ������ ������������" == query.QueryName)
            //{
            //    if (!query.Error)
            //    {
            //        _oscJournal.Value = Common.TOWORDS(query.Buffer, true);
            //        Oscilloscope.SetBuffers(_oscSettings.Value, _oscJournal.Value);
            //        if (null != OscilloscopeSettingsLoadOk)
            //        {
            //            OscilloscopeSettingsLoadOk(this);
            //        }
            //    }
            //    else
            //    {
            //        if (null != OscilloscopeSettingsLoadFail)
            //        {
            //            OscilloscopeSettingsLoadFail(this);
            //        }
            //    }
            //}

            #endregion
            
            #region ������ ������

            if (IsIndexQuery(query.name, "LoadALRecord"))
            {
                int index = GetIndex(query.name, "LoadALRecord");
                if (query.fail == 0)

                {
                    _alarmJournalRecords.AddMessage(query.readBuffer);
                    if (_alarmJournalRecords.IsMessageEmpty(index))
                    {
                        if (null != AlarmJournalLoadOk)
                        {
                            AlarmJournalLoadOk(this);
                        }
                    }
                    else
                    {
                        if (null != AlarmJournalRecordLoadOk)
                        {
                            AlarmJournalRecordLoadOk(this, index);
                        }
                        index += 1;
                        if (index < ALARMJOURNAL_RECORD_CNT && !_stopAlarmJournal)
                        {
                            LoadSlot(DeviceNumber, _alarmJournal[index], "LoadALRecord" + index + "_" + DeviceNumber, this);
                        }
                        else
                        {
                            if (null != AlarmJournalLoadOk)
                            {
                                AlarmJournalLoadOk(this);
                            }
                        }
                    }
                }
                else
                {
                    if (null != AlarmJournalRecordLoadFail)
                    {
                        AlarmJournalRecordLoadFail(this, index);
                    }
                }
            }

            #endregion

            #region ������ �������

            if (IsIndexQuery(query.name, "LoadSJRecord"))
            {
                int index = GetIndex(query.name, "LoadSJRecord");
                if (query.fail == 0)
                {
                    if (false == _systemJournalRecords.AddMessage(Common.TOWORDS(query.readBuffer, true)))
                    {
                        if (null != SystemJournalLoadOk)
                        {
                            SystemJournalLoadOk(this);
                        }
                    }
                    else
                    {
                        if (null != SystemJournalRecordLoadOk)
                        {
                            SystemJournalRecordLoadOk(this, index);
                        }
                        index += 1;
                        if (index < SYSTEMJOURNAL_RECORD_CNT && !_stopSystemJournal)
                        {
                            LoadSlot(DeviceNumber, _systemJournal[index], "LoadSJRecord" + index + "_" + DeviceNumber, this);
                        }
                        else
                        {
                            if (null != SystemJournalLoadOk)
                            {
                                SystemJournalLoadOk(this);
                            }
                        }
                    }
                }
                else
                {
                    if (null != SystemJournalRecordLoadFail)
                    {
                        SystemJournalRecordLoadFail(this, index);
                    }
                }
            }

            #endregion

            #region �������� �������

            if ("LoadOutputSignals" + DeviceNumber == query.name)
            {
                _outputSignals.Value = Common.TOWORDS(query.readBuffer, true);
                ushort[] releBuffer = new ushort[COutputRele.LENGTH];
                Array.ConstrainedCopy(_outputSignals.Value, COutputRele.OFFSET, releBuffer, 0, COutputRele.LENGTH);
                _outputRele.SetBuffer(releBuffer);

                ushort[] outputIndicator = new ushort[COutputIndicator.LENGTH];
                Array.ConstrainedCopy(_outputSignals.Value, COutputIndicator.OFFSET, outputIndicator, 0,
                                      COutputIndicator.LENGTH);
                _outputIndicator.SetBuffer(outputIndicator);

                if (query.fail == 0)
                {
                    if (null != OutputSignalsLoadOK)
                    {
                        OutputSignalsLoadOK(this);
                    }
                }
                else
                {
                    if (null != OutputSignalsLoadFail)
                    {
                        OutputSignalsLoadFail(this);
                    }
                }
            }
            if ("SaveOutputSignals" + DeviceNumber == query.name)
            {
                Raise(query, OutputSignalsSaveOK, OutputSignalsSaveFail);
            }

            #endregion

            #region ������� ������

            if ("LoadExternalDefenses1" + DeviceNumber == query.name)
            {
                _externalDefenses1.Value = Common.TOWORDS(query.readBuffer, true);
            }
            if ("LoadExternalDefenses2" + DeviceNumber == query.name)
            {
                _externalDefenses2.Value = Common.TOWORDS(query.readBuffer, true);
                ushort[] bufferMain = new ushort[CExternalDefenses.LENGTH];
                ushort[] bufferReserve = new ushort[CExternalDefenses.LENGTH];
                Array.ConstrainedCopy(_externalDefenses1.Value, 0, bufferMain, 0, 0x20);
                Array.ConstrainedCopy(_externalDefenses2.Value, 0, bufferMain, 0x20, 0x20);
                Array.ConstrainedCopy(_externalDefenses1.Value, 0x20, bufferReserve, 0, 0x20);
                Array.ConstrainedCopy(_externalDefenses2.Value, 0x20, bufferReserve, 0x20, 0x20);
                ExternalDefensesMain.SetBuffer(bufferMain);
                ExternalDefensesReserve.SetBuffer(bufferReserve);
                if (query.fail == 0)
                {
                    if (null != ExternalDefensesLoadOK)
                    {
                        ExternalDefensesLoadOK(this);
                    }
                }
                else
                {
                    if (null != ExternalDefensesLoadFail)
                    {
                        ExternalDefensesLoadFail(this);
                    }
                }
            }
            if ("SaveExternalDefenses2" + DeviceNumber == query.name)
            {
                Raise(query, ExternalDefensesSaveOK, ExternalDefensesSaveFail);
            }

            #endregion

            #region ������� �������

            if ("LoadInputSignals" + DeviceNumber == query.name)
            {
                Raise(query, InputSignalsLoadOK, InputSignalsLoadFail, ref _inputSignals);
            }
            if ("SaveInputSignals" + DeviceNumber == query.name)
            {
                Raise(query, InputSignalsSaveOK, InputSignalsSaveFail);
            }

            #endregion
            
            base.mb_CompleteExchange(sender, query);
        }

        public Type[] Forms
        {
            get
            {
                if (this.DeviceVersion == "�� 2.03")
                {
                    this.DeviceVersion = "2.02";
                }      
                else if (this.DeviceVersion == "� 2.03 �� 2.11")
                {
                    this.DeviceVersion = "2.11";
                }
                double ver = Common.VersionConverter(this.DeviceVersion);
                if (ver < 2.06)
                {
                    this._discretDataBase.StartAddress = 0x1800;
                }
                if (ver >= 3.0)
                {
                    return new Type[]
                    {
                        typeof (BSBGLEF),
                        typeof (Mr600AlarmJournalFormV2),
                        typeof (Mr600ConfigurationFormV2),
                        typeof (Mr600MeasuringFormV2),
                        typeof (Mr600OscilloscopeFormV2),
                        typeof (Mr600SystemJournalFormV2)
                    };
                }
                return new Type[]
                {
                    typeof (AlarmJournalForm),
                    typeof (ConfigurationForm),
                    typeof (MeasuringForm),
                    typeof (Mr600OscilloscopeForm),
                    typeof (SystemJournalForm)
                };
            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                    {
                        "�� 2.03",
                        "� 2.03 �� 2.11",
                        "3.00",
                        "3.01",
                        "3.02",
                        "3.03",
                        "3.04",
                        "3.05"
                    };
            }
        }   
    }
}