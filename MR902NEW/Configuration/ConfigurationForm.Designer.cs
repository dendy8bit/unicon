﻿namespace BEMN.MR902NEW.Configuration
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle76 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle77 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle78 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle79 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle80 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle85 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle91 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle92 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle93 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle94 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle95 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle96 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle97 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle98 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle99 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle100 = new System.Windows.Forms.DataGridViewCellStyle();
            this.VLScheckedListBox16 = new System.Windows.Forms.CheckedListBox();
            this.VLScheckedListBox13 = new System.Windows.Forms.CheckedListBox();
            this.VLScheckedListBox15 = new System.Windows.Forms.CheckedListBox();
            this.VLScheckedListBox14 = new System.Windows.Forms.CheckedListBox();
            this.VLScheckedListBox12 = new System.Windows.Forms.CheckedListBox();
            this.VLScheckedListBox11 = new System.Windows.Forms.CheckedListBox();
            this.VLS2 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox2 = new System.Windows.Forms.CheckedListBox();
            this.VLS3 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox3 = new System.Windows.Forms.CheckedListBox();
            this.VLSTabControl = new System.Windows.Forms.TabControl();
            this.VLS1 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.VLS4 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox4 = new System.Windows.Forms.CheckedListBox();
            this.VLS5 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox5 = new System.Windows.Forms.CheckedListBox();
            this.VLS6 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox6 = new System.Windows.Forms.CheckedListBox();
            this.VLS7 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox7 = new System.Windows.Forms.CheckedListBox();
            this.VLS8 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox8 = new System.Windows.Forms.CheckedListBox();
            this.VLS9 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox9 = new System.Windows.Forms.CheckedListBox();
            this.VLS10 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox10 = new System.Windows.Forms.CheckedListBox();
            this.VLS11 = new System.Windows.Forms.TabPage();
            this.VLS12 = new System.Windows.Forms.TabPage();
            this.VLS13 = new System.Windows.Forms.TabPage();
            this.VLS14 = new System.Windows.Forms.TabPage();
            this.VLS15 = new System.Windows.Forms.TabPage();
            this.VLS16 = new System.Windows.Forms.TabPage();
            this._allDefensesPage = new System.Windows.Forms.TabPage();
            this.groupBox36 = new System.Windows.Forms.GroupBox();
            this._inpAddCombo = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._applyCopySetpoinsButton = new System.Windows.Forms.Button();
            this._copySetpoinsGroupComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this._setpointsComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._difensesTC = new System.Windows.Forms.TabControl();
            this.tabPage21 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._difMDataGrid = new System.Windows.Forms.DataGridView();
            this._difMStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difMModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._difMBlockColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._difMIcpColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difMIdoColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difMIbColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difMfColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._difMOchColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._difMIochColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iOchMgColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difMtochColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difMEnterOchColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._difMOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._difMUrovColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this._difDDataGrid = new System.Windows.Forms.DataGridView();
            this._difDStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._difDBlockColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._difDIcpColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDIdoColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDtcpColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDIbColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDfColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDBlockGColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._difDI2gColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDBlock5GColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._difDI5gColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDOprNasColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._difDOchColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._difDIochColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iOchColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDtochColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDEnterOchColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._difDOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._difDUrovColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage22 = new System.Windows.Forms.TabPage();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._MTZDifensesDataGrid = new System.Windows.Forms.DataGridView();
            this._mtzStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._mtzModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._mtzBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._LogicCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._mtzMeasureColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._mtzICPColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._mtzCharColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._mtzTColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._mtzkColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._mtzOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._mtzUROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage19 = new System.Windows.Forms.TabPage();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this._externalDifensesDataGrid = new System.Windows.Forms.DataGridView();
            this._externalDifStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifOtklColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifSrabColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifTsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifTvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifVozvrColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifVozvrYNColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifUROVColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this._inputSygnalsPage = new System.Windows.Forms.TabPage();
            this.groupBox34 = new System.Windows.Forms.GroupBox();
            this._antiBounceDataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this._indComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this._grUst2ComboBox = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this._grUst1ComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this._inpResetTtcomboBox = new System.Windows.Forms.ComboBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this._inputSignals9 = new System.Windows.Forms.DataGridView();
            this._signalValueNumILI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signalValueColILI = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this._inputSignals10 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this._inputSignals11 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn9 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this._inputSignals12 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn10 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this._inputSignals13 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn11 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this._inputSignals14 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn12 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this._inputSignals15 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn13 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this._inputSignals16 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn14 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._inputSignals1 = new System.Windows.Forms.DataGridView();
            this._lsChannelCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signalValueCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this._inputSignals2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this._inputSignals3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this._inputSignals4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this._inputSignals5 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this._inputSignals6 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this._inputSignals7 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this._inputSignals8 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._configurationTabControl = new System.Windows.Forms.TabControl();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetSetpointsItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearSetpointsItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToHtmlItem = new System.Windows.Forms.ToolStripMenuItem();
            this._joinPage = new System.Windows.Forms.TabPage();
            this._joinData = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._joinITT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._joinSwitchOFF = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._joinSwitchOn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._joinJoin = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._joinEnter = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._joinResetColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._timeResetJoinColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage20 = new System.Windows.Forms.TabPage();
            this._configTtDgv = new System.Windows.Forms.DataGridView();
            this._nameTtColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._idTtColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeSrabTtColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._faultTtColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._resetTtCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._urovPage = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._DZHUROVself = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.urovGroup = new System.Windows.Forms.GroupBox();
            this._UROVJoinData = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._JoinIUROV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._JoinTUROV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._DZHUROVblock = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._tOtklTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.groupBox33 = new System.Windows.Forms.GroupBox();
            this._DZHSHBLOCK1 = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this._DZHSHBLOCK2 = new System.Windows.Forms.ComboBox();
            this._DZHPOBLOCK1 = new System.Windows.Forms.ComboBox();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._DZHSH1 = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this._DZHSH2 = new System.Windows.Forms.ComboBox();
            this._DZHPO = new System.Windows.Forms.ComboBox();
            this._DZHTUrov3 = new System.Windows.Forms.MaskedTextBox();
            this._DZHTUrov2 = new System.Windows.Forms.MaskedTextBox();
            this._DZHUrov3 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this._DZHUrov2 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this._DZHTUrov1 = new System.Windows.Forms.MaskedTextBox();
            this._DZHSelf = new System.Windows.Forms.ComboBox();
            this._DZHConn = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._DZHKontr = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this._relayIndPage = new System.Windows.Forms.TabPage();
            this._confIndGB = new System.Windows.Forms.GroupBox();
            this._configIgSshComboBox = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.groupBox175 = new System.Windows.Forms.GroupBox();
            this._outputIndicatorsGrid = new System.Windows.Forms.DataGridView();
            this._outIndNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outIndTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.baseGreenColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.baseRedColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._out1IndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignal2Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox35 = new System.Windows.Forms.GroupBox();
            this._resetAlarmCheckBox = new System.Windows.Forms.CheckBox();
            this._resetSystemCheckBox = new System.Windows.Forms.CheckBox();
            this.label110 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this._fault5CheckBox = new System.Windows.Forms.CheckBox();
            this._fault5Label = new System.Windows.Forms.Label();
            this._fault4CheckBox = new System.Windows.Forms.CheckBox();
            this._fault3CheckBox = new System.Windows.Forms.CheckBox();
            this._fault2CheckBox = new System.Windows.Forms.CheckBox();
            this._fault1CheckBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this._impTB = new System.Windows.Forms.MaskedTextBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.baseColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releWaitCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage23 = new System.Windows.Forms.TabPage();
            this.groupBox55 = new System.Windows.Forms.GroupBox();
            this._virtualReleDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn23 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn24 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn25 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox53 = new System.Windows.Forms.GroupBox();
            this._rsTriggersDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn65 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn66 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn67 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this._wrapBtn = new System.Windows.Forms.Button();
            this.treeViewForVLS = new System.Windows.Forms.TreeView();
            this._systemPage = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this._oscChannelsWithBaseGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._baseCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn22 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._poCheckBox = new System.Windows.Forms.CheckBox();
            this._sh2CheckBox = new System.Windows.Forms.CheckBox();
            this._sh1CheckBox = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this._oscLength = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this._inpOscComboBox = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this._oscSizeTextBox = new System.Windows.Forms.MaskedTextBox();
            this._oscFix = new System.Windows.Forms.ComboBox();
            this._oscWriteLength = new System.Windows.Forms.MaskedTextBox();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.goin64 = new System.Windows.Forms.ComboBox();
            this.goin48 = new System.Windows.Forms.ComboBox();
            this.goin32 = new System.Windows.Forms.ComboBox();
            this.label281 = new System.Windows.Forms.Label();
            this.label282 = new System.Windows.Forms.Label();
            this.label283 = new System.Windows.Forms.Label();
            this.goin63 = new System.Windows.Forms.ComboBox();
            this.goin47 = new System.Windows.Forms.ComboBox();
            this.goin31 = new System.Windows.Forms.ComboBox();
            this.label284 = new System.Windows.Forms.Label();
            this.label285 = new System.Windows.Forms.Label();
            this.label286 = new System.Windows.Forms.Label();
            this.goin62 = new System.Windows.Forms.ComboBox();
            this.goin46 = new System.Windows.Forms.ComboBox();
            this.goin30 = new System.Windows.Forms.ComboBox();
            this.label287 = new System.Windows.Forms.Label();
            this.label288 = new System.Windows.Forms.Label();
            this.label289 = new System.Windows.Forms.Label();
            this.goin61 = new System.Windows.Forms.ComboBox();
            this.goin45 = new System.Windows.Forms.ComboBox();
            this.goin29 = new System.Windows.Forms.ComboBox();
            this.label290 = new System.Windows.Forms.Label();
            this.label291 = new System.Windows.Forms.Label();
            this.label292 = new System.Windows.Forms.Label();
            this.goin60 = new System.Windows.Forms.ComboBox();
            this.goin44 = new System.Windows.Forms.ComboBox();
            this.goin28 = new System.Windows.Forms.ComboBox();
            this.label293 = new System.Windows.Forms.Label();
            this.label294 = new System.Windows.Forms.Label();
            this.label295 = new System.Windows.Forms.Label();
            this.goin59 = new System.Windows.Forms.ComboBox();
            this.goin43 = new System.Windows.Forms.ComboBox();
            this.goin27 = new System.Windows.Forms.ComboBox();
            this.label296 = new System.Windows.Forms.Label();
            this.label297 = new System.Windows.Forms.Label();
            this.label298 = new System.Windows.Forms.Label();
            this.goin58 = new System.Windows.Forms.ComboBox();
            this.goin42 = new System.Windows.Forms.ComboBox();
            this.goin26 = new System.Windows.Forms.ComboBox();
            this.label299 = new System.Windows.Forms.Label();
            this.label300 = new System.Windows.Forms.Label();
            this.label301 = new System.Windows.Forms.Label();
            this.goin57 = new System.Windows.Forms.ComboBox();
            this.goin41 = new System.Windows.Forms.ComboBox();
            this.goin25 = new System.Windows.Forms.ComboBox();
            this.label302 = new System.Windows.Forms.Label();
            this.label303 = new System.Windows.Forms.Label();
            this.label304 = new System.Windows.Forms.Label();
            this.goin56 = new System.Windows.Forms.ComboBox();
            this.goin40 = new System.Windows.Forms.ComboBox();
            this.goin24 = new System.Windows.Forms.ComboBox();
            this.label305 = new System.Windows.Forms.Label();
            this.label306 = new System.Windows.Forms.Label();
            this.label307 = new System.Windows.Forms.Label();
            this.goin55 = new System.Windows.Forms.ComboBox();
            this.goin39 = new System.Windows.Forms.ComboBox();
            this.goin23 = new System.Windows.Forms.ComboBox();
            this.label308 = new System.Windows.Forms.Label();
            this.label309 = new System.Windows.Forms.Label();
            this.label310 = new System.Windows.Forms.Label();
            this.goin54 = new System.Windows.Forms.ComboBox();
            this.goin38 = new System.Windows.Forms.ComboBox();
            this.goin22 = new System.Windows.Forms.ComboBox();
            this.label311 = new System.Windows.Forms.Label();
            this.label312 = new System.Windows.Forms.Label();
            this.label313 = new System.Windows.Forms.Label();
            this.goin53 = new System.Windows.Forms.ComboBox();
            this.goin37 = new System.Windows.Forms.ComboBox();
            this.goin21 = new System.Windows.Forms.ComboBox();
            this.label314 = new System.Windows.Forms.Label();
            this.label315 = new System.Windows.Forms.Label();
            this.label316 = new System.Windows.Forms.Label();
            this.goin52 = new System.Windows.Forms.ComboBox();
            this.goin36 = new System.Windows.Forms.ComboBox();
            this.goin20 = new System.Windows.Forms.ComboBox();
            this.label317 = new System.Windows.Forms.Label();
            this.label318 = new System.Windows.Forms.Label();
            this.label319 = new System.Windows.Forms.Label();
            this.goin51 = new System.Windows.Forms.ComboBox();
            this.goin35 = new System.Windows.Forms.ComboBox();
            this.goin19 = new System.Windows.Forms.ComboBox();
            this.label320 = new System.Windows.Forms.Label();
            this.label321 = new System.Windows.Forms.Label();
            this.label322 = new System.Windows.Forms.Label();
            this.goin50 = new System.Windows.Forms.ComboBox();
            this.goin34 = new System.Windows.Forms.ComboBox();
            this.goin18 = new System.Windows.Forms.ComboBox();
            this.label323 = new System.Windows.Forms.Label();
            this.label324 = new System.Windows.Forms.Label();
            this.label325 = new System.Windows.Forms.Label();
            this.goin49 = new System.Windows.Forms.ComboBox();
            this.label326 = new System.Windows.Forms.Label();
            this.goin33 = new System.Windows.Forms.ComboBox();
            this.label327 = new System.Windows.Forms.Label();
            this.goin17 = new System.Windows.Forms.ComboBox();
            this.label328 = new System.Windows.Forms.Label();
            this.goin16 = new System.Windows.Forms.ComboBox();
            this.label329 = new System.Windows.Forms.Label();
            this.goin15 = new System.Windows.Forms.ComboBox();
            this.label330 = new System.Windows.Forms.Label();
            this.goin14 = new System.Windows.Forms.ComboBox();
            this.label331 = new System.Windows.Forms.Label();
            this.goin13 = new System.Windows.Forms.ComboBox();
            this.label332 = new System.Windows.Forms.Label();
            this.goin12 = new System.Windows.Forms.ComboBox();
            this.label333 = new System.Windows.Forms.Label();
            this.goin11 = new System.Windows.Forms.ComboBox();
            this.label334 = new System.Windows.Forms.Label();
            this.goin10 = new System.Windows.Forms.ComboBox();
            this.label335 = new System.Windows.Forms.Label();
            this.goin9 = new System.Windows.Forms.ComboBox();
            this.label336 = new System.Windows.Forms.Label();
            this.goin8 = new System.Windows.Forms.ComboBox();
            this.label337 = new System.Windows.Forms.Label();
            this.goin7 = new System.Windows.Forms.ComboBox();
            this.label338 = new System.Windows.Forms.Label();
            this.goin6 = new System.Windows.Forms.ComboBox();
            this.label339 = new System.Windows.Forms.Label();
            this.goin5 = new System.Windows.Forms.ComboBox();
            this.label340 = new System.Windows.Forms.Label();
            this.goin4 = new System.Windows.Forms.ComboBox();
            this.label341 = new System.Windows.Forms.Label();
            this.goin3 = new System.Windows.Forms.ComboBox();
            this.label342 = new System.Windows.Forms.Label();
            this.goin2 = new System.Windows.Forms.ComboBox();
            this.label343 = new System.Windows.Forms.Label();
            this.goin1 = new System.Windows.Forms.ComboBox();
            this.label344 = new System.Windows.Forms.Label();
            this.label345 = new System.Windows.Forms.Label();
            this.operationBGS = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.currentBGS = new System.Windows.Forms.ComboBox();
            this._ethernetPage = new System.Windows.Forms.TabPage();
            this._reserveGroupBox = new System.Windows.Forms.GroupBox();
            this._reserveCB = new System.Windows.Forms.ComboBox();
            this.groupBox51 = new System.Windows.Forms.GroupBox();
            this._ipLo1 = new System.Windows.Forms.MaskedTextBox();
            this._ipLo2 = new System.Windows.Forms.MaskedTextBox();
            this._ipHi1 = new System.Windows.Forms.MaskedTextBox();
            this._ipHi2 = new System.Windows.Forms.MaskedTextBox();
            this.label214 = new System.Windows.Forms.Label();
            this.label213 = new System.Windows.Forms.Label();
            this.label212 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this._dif0DataGreed = new System.Windows.Forms.DataGridView();
            this._dif0AVRColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0APVColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0UrovColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0OscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0Intg2Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0Ib2Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0Intg1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0Ib1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0TdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0InColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0BlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0ModeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0StageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this._modeDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this._stepOnInstantValuesDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this._UROVDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this._oscDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this.label54 = new System.Windows.Forms.Label();
            this._blockingDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this.label51 = new System.Windows.Forms.Label();
            this._constraintDTOBTTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label55 = new System.Windows.Forms.Label();
            this._timeEnduranceDTOBTTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label128 = new System.Windows.Forms.Label();
            this._APVDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this.label129 = new System.Windows.Forms.Label();
            this._AVRDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this.label56 = new System.Windows.Forms.Label();
            this._modeDTZComboBox = new System.Windows.Forms.ComboBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this._Ib1BeginTextBox = new System.Windows.Forms.MaskedTextBox();
            this._K1AngleOfSlopeTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label66 = new System.Windows.Forms.Label();
            this._Ib2BeginTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label69 = new System.Windows.Forms.Label();
            this._K2TangensTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this._constraintDTZTextBox = new System.Windows.Forms.MaskedTextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this._I2I1TextBox = new System.Windows.Forms.MaskedTextBox();
            this.label23 = new System.Windows.Forms.Label();
            this._perBlockI2I1 = new System.Windows.Forms.ComboBox();
            this._timeEnduranceDTZTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this._blockingDTZComboBox = new System.Windows.Forms.ComboBox();
            this._UROVDTZComboBox = new System.Windows.Forms.ComboBox();
            this._oscDTZComboBox = new System.Windows.Forms.ComboBox();
            this._modeI2I1CB = new System.Windows.Forms.ComboBox();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this._I5I1TextBox = new System.Windows.Forms.MaskedTextBox();
            this.label24 = new System.Windows.Forms.Label();
            this._perBlockI5I1 = new System.Windows.Forms.ComboBox();
            this._modeI5I1CB = new System.Windows.Forms.ComboBox();
            this.label130 = new System.Windows.Forms.Label();
            this._APVDTZComboBox = new System.Windows.Forms.ComboBox();
            this.label131 = new System.Windows.Forms.Label();
            this._AVRDTZComboBox = new System.Windows.Forms.ComboBox();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this._saveToXmlButton = new System.Windows.Forms.Button();
            this._resetSetpointsButton = new System.Windows.Forms.Button();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._saveXmlDialog = new System.Windows.Forms.SaveFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn26 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn28 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.VLS2.SuspendLayout();
            this.VLS3.SuspendLayout();
            this.VLSTabControl.SuspendLayout();
            this.VLS1.SuspendLayout();
            this.VLS4.SuspendLayout();
            this.VLS5.SuspendLayout();
            this.VLS6.SuspendLayout();
            this.VLS7.SuspendLayout();
            this.VLS8.SuspendLayout();
            this.VLS9.SuspendLayout();
            this.VLS10.SuspendLayout();
            this.VLS11.SuspendLayout();
            this.VLS12.SuspendLayout();
            this.VLS13.SuspendLayout();
            this.VLS14.SuspendLayout();
            this.VLS15.SuspendLayout();
            this.VLS16.SuspendLayout();
            this._allDefensesPage.SuspendLayout();
            this.groupBox36.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this._difensesTC.SuspendLayout();
            this.tabPage21.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difMDataGrid)).BeginInit();
            this.groupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difDDataGrid)).BeginInit();
            this.tabPage22.SuspendLayout();
            this.groupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._MTZDifensesDataGrid)).BeginInit();
            this.tabPage19.SuspendLayout();
            this.groupBox24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._externalDifensesDataGrid)).BeginInit();
            this.groupBox13.SuspendLayout();
            this._inputSygnalsPage.SuspendLayout();
            this.groupBox34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._antiBounceDataGridView)).BeginInit();
            this.groupBox15.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals9)).BeginInit();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals10)).BeginInit();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals11)).BeginInit();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals12)).BeginInit();
            this.tabPage13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals13)).BeginInit();
            this.tabPage14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals14)).BeginInit();
            this.tabPage15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals15)).BeginInit();
            this.tabPage16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals16)).BeginInit();
            this.groupBox14.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals3)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals4)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals5)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals6)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals7)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals8)).BeginInit();
            this._configurationTabControl.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this._joinPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._joinData)).BeginInit();
            this.tabPage20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._configTtDgv)).BeginInit();
            this._urovPage.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.urovGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._UROVJoinData)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox33.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this._relayIndPage.SuspendLayout();
            this._confIndGB.SuspendLayout();
            this.groupBox175.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).BeginInit();
            this.groupBox35.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this.tabPage23.SuspendLayout();
            this.groupBox55.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._virtualReleDataGrid)).BeginInit();
            this.groupBox53.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rsTriggersDataGrid)).BeginInit();
            this._outputSignalsPage.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this._systemPage.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._oscChannelsWithBaseGrid)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.tabPage17.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this._ethernetPage.SuspendLayout();
            this._reserveGroupBox.SuspendLayout();
            this.groupBox51.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dif0DataGreed)).BeginInit();
            this._statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // VLScheckedListBox16
            // 
            this.VLScheckedListBox16.CheckOnClick = true;
            this.VLScheckedListBox16.FormattingEnabled = true;
            this.VLScheckedListBox16.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox16.Name = "VLScheckedListBox16";
            this.VLScheckedListBox16.ScrollAlwaysVisible = true;
            this.VLScheckedListBox16.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox16.TabIndex = 6;
            this.VLScheckedListBox16.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLScheckedListBox13
            // 
            this.VLScheckedListBox13.CheckOnClick = true;
            this.VLScheckedListBox13.FormattingEnabled = true;
            this.VLScheckedListBox13.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox13.Name = "VLScheckedListBox13";
            this.VLScheckedListBox13.ScrollAlwaysVisible = true;
            this.VLScheckedListBox13.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox13.TabIndex = 6;
            this.VLScheckedListBox13.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLScheckedListBox15
            // 
            this.VLScheckedListBox15.CheckOnClick = true;
            this.VLScheckedListBox15.FormattingEnabled = true;
            this.VLScheckedListBox15.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox15.Name = "VLScheckedListBox15";
            this.VLScheckedListBox15.ScrollAlwaysVisible = true;
            this.VLScheckedListBox15.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox15.TabIndex = 6;
            this.VLScheckedListBox15.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLScheckedListBox14
            // 
            this.VLScheckedListBox14.CheckOnClick = true;
            this.VLScheckedListBox14.FormattingEnabled = true;
            this.VLScheckedListBox14.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox14.Name = "VLScheckedListBox14";
            this.VLScheckedListBox14.ScrollAlwaysVisible = true;
            this.VLScheckedListBox14.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox14.TabIndex = 6;
            this.VLScheckedListBox14.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLScheckedListBox12
            // 
            this.VLScheckedListBox12.CheckOnClick = true;
            this.VLScheckedListBox12.FormattingEnabled = true;
            this.VLScheckedListBox12.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox12.Name = "VLScheckedListBox12";
            this.VLScheckedListBox12.ScrollAlwaysVisible = true;
            this.VLScheckedListBox12.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox12.TabIndex = 6;
            this.VLScheckedListBox12.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLScheckedListBox11
            // 
            this.VLScheckedListBox11.CheckOnClick = true;
            this.VLScheckedListBox11.FormattingEnabled = true;
            this.VLScheckedListBox11.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox11.Name = "VLScheckedListBox11";
            this.VLScheckedListBox11.ScrollAlwaysVisible = true;
            this.VLScheckedListBox11.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox11.TabIndex = 6;
            this.VLScheckedListBox11.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS2
            // 
            this.VLS2.Controls.Add(this.VLScheckedListBox2);
            this.VLS2.Location = new System.Drawing.Point(4, 49);
            this.VLS2.Name = "VLS2";
            this.VLS2.Padding = new System.Windows.Forms.Padding(3);
            this.VLS2.Size = new System.Drawing.Size(388, 468);
            this.VLS2.TabIndex = 1;
            this.VLS2.Text = "ВЛС 2";
            this.VLS2.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox2
            // 
            this.VLScheckedListBox2.CheckOnClick = true;
            this.VLScheckedListBox2.FormattingEnabled = true;
            this.VLScheckedListBox2.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox2.Name = "VLScheckedListBox2";
            this.VLScheckedListBox2.ScrollAlwaysVisible = true;
            this.VLScheckedListBox2.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox2.TabIndex = 6;
            this.VLScheckedListBox2.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS3
            // 
            this.VLS3.Controls.Add(this.VLScheckedListBox3);
            this.VLS3.Location = new System.Drawing.Point(4, 49);
            this.VLS3.Name = "VLS3";
            this.VLS3.Size = new System.Drawing.Size(388, 468);
            this.VLS3.TabIndex = 2;
            this.VLS3.Text = "ВЛС   3";
            this.VLS3.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox3
            // 
            this.VLScheckedListBox3.CheckOnClick = true;
            this.VLScheckedListBox3.FormattingEnabled = true;
            this.VLScheckedListBox3.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox3.Name = "VLScheckedListBox3";
            this.VLScheckedListBox3.ScrollAlwaysVisible = true;
            this.VLScheckedListBox3.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox3.TabIndex = 6;
            this.VLScheckedListBox3.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLSTabControl
            // 
            this.VLSTabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.VLSTabControl.Controls.Add(this.VLS1);
            this.VLSTabControl.Controls.Add(this.VLS2);
            this.VLSTabControl.Controls.Add(this.VLS3);
            this.VLSTabControl.Controls.Add(this.VLS4);
            this.VLSTabControl.Controls.Add(this.VLS5);
            this.VLSTabControl.Controls.Add(this.VLS6);
            this.VLSTabControl.Controls.Add(this.VLS7);
            this.VLSTabControl.Controls.Add(this.VLS8);
            this.VLSTabControl.Controls.Add(this.VLS9);
            this.VLSTabControl.Controls.Add(this.VLS10);
            this.VLSTabControl.Controls.Add(this.VLS11);
            this.VLSTabControl.Controls.Add(this.VLS12);
            this.VLSTabControl.Controls.Add(this.VLS13);
            this.VLSTabControl.Controls.Add(this.VLS14);
            this.VLSTabControl.Controls.Add(this.VLS15);
            this.VLSTabControl.Controls.Add(this.VLS16);
            this.VLSTabControl.Location = new System.Drawing.Point(6, 19);
            this.VLSTabControl.Multiline = true;
            this.VLSTabControl.Name = "VLSTabControl";
            this.VLSTabControl.SelectedIndex = 0;
            this.VLSTabControl.Size = new System.Drawing.Size(396, 521);
            this.VLSTabControl.TabIndex = 0;
            // 
            // VLS1
            // 
            this.VLS1.Controls.Add(this.VLScheckedListBox1);
            this.VLS1.Location = new System.Drawing.Point(4, 49);
            this.VLS1.Name = "VLS1";
            this.VLS1.Padding = new System.Windows.Forms.Padding(3);
            this.VLS1.Size = new System.Drawing.Size(388, 468);
            this.VLS1.TabIndex = 0;
            this.VLS1.Text = "ВЛС 1";
            this.VLS1.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox1
            // 
            this.VLScheckedListBox1.CheckOnClick = true;
            this.VLScheckedListBox1.FormattingEnabled = true;
            this.VLScheckedListBox1.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox1.Name = "VLScheckedListBox1";
            this.VLScheckedListBox1.ScrollAlwaysVisible = true;
            this.VLScheckedListBox1.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox1.TabIndex = 6;
            this.VLScheckedListBox1.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS4
            // 
            this.VLS4.Controls.Add(this.VLScheckedListBox4);
            this.VLS4.Location = new System.Drawing.Point(4, 49);
            this.VLS4.Name = "VLS4";
            this.VLS4.Size = new System.Drawing.Size(388, 468);
            this.VLS4.TabIndex = 3;
            this.VLS4.Text = "ВЛС 4";
            this.VLS4.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox4
            // 
            this.VLScheckedListBox4.CheckOnClick = true;
            this.VLScheckedListBox4.FormattingEnabled = true;
            this.VLScheckedListBox4.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox4.Name = "VLScheckedListBox4";
            this.VLScheckedListBox4.ScrollAlwaysVisible = true;
            this.VLScheckedListBox4.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox4.TabIndex = 6;
            this.VLScheckedListBox4.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS5
            // 
            this.VLS5.Controls.Add(this.VLScheckedListBox5);
            this.VLS5.Location = new System.Drawing.Point(4, 49);
            this.VLS5.Name = "VLS5";
            this.VLS5.Size = new System.Drawing.Size(388, 468);
            this.VLS5.TabIndex = 4;
            this.VLS5.Text = "ВЛС   5";
            this.VLS5.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox5
            // 
            this.VLScheckedListBox5.CheckOnClick = true;
            this.VLScheckedListBox5.FormattingEnabled = true;
            this.VLScheckedListBox5.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox5.Name = "VLScheckedListBox5";
            this.VLScheckedListBox5.ScrollAlwaysVisible = true;
            this.VLScheckedListBox5.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox5.TabIndex = 6;
            this.VLScheckedListBox5.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS6
            // 
            this.VLS6.Controls.Add(this.VLScheckedListBox6);
            this.VLS6.Location = new System.Drawing.Point(4, 49);
            this.VLS6.Name = "VLS6";
            this.VLS6.Size = new System.Drawing.Size(388, 468);
            this.VLS6.TabIndex = 5;
            this.VLS6.Text = "ВЛС  6";
            this.VLS6.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox6
            // 
            this.VLScheckedListBox6.CheckOnClick = true;
            this.VLScheckedListBox6.FormattingEnabled = true;
            this.VLScheckedListBox6.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox6.Name = "VLScheckedListBox6";
            this.VLScheckedListBox6.ScrollAlwaysVisible = true;
            this.VLScheckedListBox6.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox6.TabIndex = 6;
            this.VLScheckedListBox6.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS7
            // 
            this.VLS7.Controls.Add(this.VLScheckedListBox7);
            this.VLS7.Location = new System.Drawing.Point(4, 49);
            this.VLS7.Name = "VLS7";
            this.VLS7.Size = new System.Drawing.Size(388, 468);
            this.VLS7.TabIndex = 6;
            this.VLS7.Text = "ВЛС 7";
            this.VLS7.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox7
            // 
            this.VLScheckedListBox7.CheckOnClick = true;
            this.VLScheckedListBox7.FormattingEnabled = true;
            this.VLScheckedListBox7.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox7.Name = "VLScheckedListBox7";
            this.VLScheckedListBox7.ScrollAlwaysVisible = true;
            this.VLScheckedListBox7.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox7.TabIndex = 6;
            this.VLScheckedListBox7.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS8
            // 
            this.VLS8.Controls.Add(this.VLScheckedListBox8);
            this.VLS8.Location = new System.Drawing.Point(4, 49);
            this.VLS8.Name = "VLS8";
            this.VLS8.Size = new System.Drawing.Size(388, 468);
            this.VLS8.TabIndex = 7;
            this.VLS8.Text = "ВЛС   8";
            this.VLS8.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox8
            // 
            this.VLScheckedListBox8.CheckOnClick = true;
            this.VLScheckedListBox8.FormattingEnabled = true;
            this.VLScheckedListBox8.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox8.Name = "VLScheckedListBox8";
            this.VLScheckedListBox8.ScrollAlwaysVisible = true;
            this.VLScheckedListBox8.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox8.TabIndex = 6;
            this.VLScheckedListBox8.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS9
            // 
            this.VLS9.Controls.Add(this.VLScheckedListBox9);
            this.VLS9.Location = new System.Drawing.Point(4, 49);
            this.VLS9.Name = "VLS9";
            this.VLS9.Size = new System.Drawing.Size(388, 468);
            this.VLS9.TabIndex = 8;
            this.VLS9.Text = "ВЛС9";
            this.VLS9.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox9
            // 
            this.VLScheckedListBox9.CheckOnClick = true;
            this.VLScheckedListBox9.FormattingEnabled = true;
            this.VLScheckedListBox9.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox9.Name = "VLScheckedListBox9";
            this.VLScheckedListBox9.ScrollAlwaysVisible = true;
            this.VLScheckedListBox9.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox9.TabIndex = 6;
            this.VLScheckedListBox9.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS10
            // 
            this.VLS10.Controls.Add(this.VLScheckedListBox10);
            this.VLS10.Location = new System.Drawing.Point(4, 49);
            this.VLS10.Name = "VLS10";
            this.VLS10.Size = new System.Drawing.Size(388, 468);
            this.VLS10.TabIndex = 9;
            this.VLS10.Text = "ВЛС10";
            this.VLS10.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox10
            // 
            this.VLScheckedListBox10.CheckOnClick = true;
            this.VLScheckedListBox10.FormattingEnabled = true;
            this.VLScheckedListBox10.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox10.Name = "VLScheckedListBox10";
            this.VLScheckedListBox10.ScrollAlwaysVisible = true;
            this.VLScheckedListBox10.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox10.TabIndex = 6;
            this.VLScheckedListBox10.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS11
            // 
            this.VLS11.Controls.Add(this.VLScheckedListBox11);
            this.VLS11.Location = new System.Drawing.Point(4, 49);
            this.VLS11.Name = "VLS11";
            this.VLS11.Size = new System.Drawing.Size(388, 468);
            this.VLS11.TabIndex = 10;
            this.VLS11.Text = "ВЛС11";
            this.VLS11.UseVisualStyleBackColor = true;
            // 
            // VLS12
            // 
            this.VLS12.Controls.Add(this.VLScheckedListBox12);
            this.VLS12.Location = new System.Drawing.Point(4, 49);
            this.VLS12.Name = "VLS12";
            this.VLS12.Size = new System.Drawing.Size(388, 468);
            this.VLS12.TabIndex = 11;
            this.VLS12.Text = "ВЛС12";
            this.VLS12.UseVisualStyleBackColor = true;
            // 
            // VLS13
            // 
            this.VLS13.Controls.Add(this.VLScheckedListBox13);
            this.VLS13.Location = new System.Drawing.Point(4, 49);
            this.VLS13.Name = "VLS13";
            this.VLS13.Size = new System.Drawing.Size(388, 468);
            this.VLS13.TabIndex = 12;
            this.VLS13.Text = "ВЛС13";
            this.VLS13.UseVisualStyleBackColor = true;
            // 
            // VLS14
            // 
            this.VLS14.Controls.Add(this.VLScheckedListBox14);
            this.VLS14.Location = new System.Drawing.Point(4, 49);
            this.VLS14.Name = "VLS14";
            this.VLS14.Size = new System.Drawing.Size(388, 468);
            this.VLS14.TabIndex = 13;
            this.VLS14.Text = "ВЛС14";
            this.VLS14.UseVisualStyleBackColor = true;
            // 
            // VLS15
            // 
            this.VLS15.Controls.Add(this.VLScheckedListBox15);
            this.VLS15.Location = new System.Drawing.Point(4, 49);
            this.VLS15.Name = "VLS15";
            this.VLS15.Size = new System.Drawing.Size(388, 468);
            this.VLS15.TabIndex = 14;
            this.VLS15.Text = "ВЛС15";
            this.VLS15.UseVisualStyleBackColor = true;
            // 
            // VLS16
            // 
            this.VLS16.Controls.Add(this.VLScheckedListBox16);
            this.VLS16.Location = new System.Drawing.Point(4, 49);
            this.VLS16.Name = "VLS16";
            this.VLS16.Size = new System.Drawing.Size(388, 468);
            this.VLS16.TabIndex = 15;
            this.VLS16.Text = "ВЛС16";
            this.VLS16.UseVisualStyleBackColor = true;
            // 
            // _allDefensesPage
            // 
            this._allDefensesPage.Controls.Add(this.groupBox36);
            this._allDefensesPage.Controls.Add(this.groupBox5);
            this._allDefensesPage.Controls.Add(this.groupBox31);
            this._allDefensesPage.Controls.Add(this.groupBox10);
            this._allDefensesPage.Location = new System.Drawing.Point(4, 22);
            this._allDefensesPage.Name = "_allDefensesPage";
            this._allDefensesPage.Size = new System.Drawing.Size(1024, 553);
            this._allDefensesPage.TabIndex = 4;
            this._allDefensesPage.Text = "Защиты";
            this._allDefensesPage.UseVisualStyleBackColor = true;
            // 
            // groupBox36
            // 
            this.groupBox36.Controls.Add(this._inpAddCombo);
            this.groupBox36.Location = new System.Drawing.Point(297, 3);
            this.groupBox36.Name = "groupBox36";
            this.groupBox36.Size = new System.Drawing.Size(139, 54);
            this.groupBox36.TabIndex = 12;
            this.groupBox36.TabStop = false;
            this.groupBox36.Text = "Вход опорного сигнала";
            // 
            // _inpAddCombo
            // 
            this._inpAddCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inpAddCombo.FormattingEnabled = true;
            this._inpAddCombo.Location = new System.Drawing.Point(6, 21);
            this._inpAddCombo.Name = "_inpAddCombo";
            this._inpAddCombo.Size = new System.Drawing.Size(127, 21);
            this._inpAddCombo.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._applyCopySetpoinsButton);
            this.groupBox5.Controls.Add(this._copySetpoinsGroupComboBox);
            this.groupBox5.Location = new System.Drawing.Point(117, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(174, 54);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Копировать в";
            // 
            // _applyCopySetpoinsButton
            // 
            this._applyCopySetpoinsButton.Location = new System.Drawing.Point(95, 17);
            this._applyCopySetpoinsButton.Name = "_applyCopySetpoinsButton";
            this._applyCopySetpoinsButton.Size = new System.Drawing.Size(75, 23);
            this._applyCopySetpoinsButton.TabIndex = 1;
            this._applyCopySetpoinsButton.Text = "Применить";
            this._applyCopySetpoinsButton.UseVisualStyleBackColor = true;
            this._applyCopySetpoinsButton.Click += new System.EventHandler(this._applyCopySetpoinsButton_Click);
            // 
            // _copySetpoinsGroupComboBox
            // 
            this._copySetpoinsGroupComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._copySetpoinsGroupComboBox.FormattingEnabled = true;
            this._copySetpoinsGroupComboBox.Location = new System.Drawing.Point(6, 19);
            this._copySetpoinsGroupComboBox.Name = "_copySetpoinsGroupComboBox";
            this._copySetpoinsGroupComboBox.Size = new System.Drawing.Size(83, 21);
            this._copySetpoinsGroupComboBox.TabIndex = 0;
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this._setpointsComboBox);
            this.groupBox31.Location = new System.Drawing.Point(8, 3);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(103, 54);
            this.groupBox31.TabIndex = 10;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "Группы уставок";
            // 
            // _setpointsComboBox
            // 
            this._setpointsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._setpointsComboBox.FormattingEnabled = true;
            this._setpointsComboBox.Location = new System.Drawing.Point(6, 19);
            this._setpointsComboBox.Name = "_setpointsComboBox";
            this._setpointsComboBox.Size = new System.Drawing.Size(88, 21);
            this._setpointsComboBox.TabIndex = 0;
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this._difensesTC);
            this.groupBox10.Location = new System.Drawing.Point(8, 63);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(973, 478);
            this.groupBox10.TabIndex = 9;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Защиты";
            // 
            // _difensesTC
            // 
            this._difensesTC.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this._difensesTC.Controls.Add(this.tabPage21);
            this._difensesTC.Controls.Add(this.tabPage22);
            this._difensesTC.Controls.Add(this.tabPage19);
            this._difensesTC.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesTC.Location = new System.Drawing.Point(3, 16);
            this._difensesTC.Name = "_difensesTC";
            this._difensesTC.SelectedIndex = 0;
            this._difensesTC.Size = new System.Drawing.Size(967, 459);
            this._difensesTC.TabIndex = 0;
            // 
            // tabPage21
            // 
            this.tabPage21.Controls.Add(this.groupBox1);
            this.tabPage21.Controls.Add(this.groupBox20);
            this.tabPage21.Location = new System.Drawing.Point(4, 25);
            this.tabPage21.Name = "tabPage21";
            this.tabPage21.Size = new System.Drawing.Size(959, 430);
            this.tabPage21.TabIndex = 4;
            this.tabPage21.Text = "Дифференциальные";
            this.tabPage21.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this._difMDataGrid);
            this.groupBox1.Location = new System.Drawing.Point(3, 151);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(950, 142);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Дифференциальные защиты по мгновенным значениям";
            // 
            // _difMDataGrid
            // 
            this._difMDataGrid.AllowUserToAddRows = false;
            this._difMDataGrid.AllowUserToDeleteRows = false;
            this._difMDataGrid.AllowUserToResizeColumns = false;
            this._difMDataGrid.AllowUserToResizeRows = false;
            this._difMDataGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._difMDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this._difMDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difMDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._difMStageColumn,
            this._difMModesColumn,
            this._difMBlockColumn,
            this.Column2,
            this._difMIcpColumn,
            this._difMIdoColumn,
            this.Column3,
            this._difMIbColumn,
            this._difMfColumn,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this._difMOchColumn,
            this._difMIochColumn,
            this._iOchMgColumn,
            this._difMtochColumn,
            this._difMEnterOchColumn,
            this._difMOscColumn,
            this._difMUrovColumn});
            this._difMDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difMDataGrid.Location = new System.Drawing.Point(3, 16);
            this._difMDataGrid.MultiSelect = false;
            this._difMDataGrid.Name = "_difMDataGrid";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._difMDataGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this._difMDataGrid.RowHeadersVisible = false;
            this._difMDataGrid.RowHeadersWidth = 51;
            this._difMDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difMDataGrid.RowTemplate.Height = 24;
            this._difMDataGrid.ShowCellErrors = false;
            this._difMDataGrid.ShowRowErrors = false;
            this._difMDataGrid.Size = new System.Drawing.Size(944, 123);
            this._difMDataGrid.TabIndex = 4;
            // 
            // _difMStageColumn
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            this._difMStageColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this._difMStageColumn.Frozen = true;
            this._difMStageColumn.HeaderText = "Ступень";
            this._difMStageColumn.MinimumWidth = 6;
            this._difMStageColumn.Name = "_difMStageColumn";
            this._difMStageColumn.ReadOnly = true;
            this._difMStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difMStageColumn.Width = 125;
            // 
            // _difMModesColumn
            // 
            this._difMModesColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMModesColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this._difMModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._difMModesColumn.HeaderText = "Режим";
            this._difMModesColumn.MinimumWidth = 6;
            this._difMModesColumn.Name = "_difMModesColumn";
            this._difMModesColumn.Width = 48;
            // 
            // _difMBlockColumn
            // 
            this._difMBlockColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMBlockColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this._difMBlockColumn.HeaderText = "Блокировка";
            this._difMBlockColumn.MinimumWidth = 6;
            this._difMBlockColumn.Name = "_difMBlockColumn";
            this._difMBlockColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difMBlockColumn.Width = 74;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Пуск от Iд3м ПО";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.Width = 125;
            // 
            // _difMIcpColumn
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMIcpColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this._difMIcpColumn.HeaderText = "Iд>, Iн";
            this._difMIcpColumn.MinimumWidth = 6;
            this._difMIcpColumn.Name = "_difMIcpColumn";
            this._difMIcpColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difMIcpColumn.Width = 60;
            // 
            // _difMIdoColumn
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMIdoColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this._difMIdoColumn.HeaderText = "Iд>>, Iн";
            this._difMIdoColumn.MinimumWidth = 6;
            this._difMIdoColumn.Name = "_difMIdoColumn";
            this._difMIdoColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difMIdoColumn.Width = 60;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.Visible = false;
            this.Column3.Width = 125;
            // 
            // _difMIbColumn
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMIbColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this._difMIbColumn.HeaderText = "Iб1, Iн";
            this._difMIbColumn.MinimumWidth = 6;
            this._difMIbColumn.Name = "_difMIbColumn";
            this._difMIbColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difMIbColumn.Width = 60;
            // 
            // _difMfColumn
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMfColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this._difMfColumn.HeaderText = "f";
            this._difMfColumn.MinimumWidth = 6;
            this._difMfColumn.Name = "_difMfColumn";
            this._difMfColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difMfColumn.Width = 40;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Column4";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.Visible = false;
            this.Column4.Width = 125;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Column5";
            this.Column5.MinimumWidth = 6;
            this.Column5.Name = "Column5";
            this.Column5.Visible = false;
            this.Column5.Width = 65;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Column6";
            this.Column6.MinimumWidth = 6;
            this.Column6.Name = "Column6";
            this.Column6.Visible = false;
            this.Column6.Width = 125;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Column7";
            this.Column7.MinimumWidth = 6;
            this.Column7.Name = "Column7";
            this.Column7.Visible = false;
            this.Column7.Width = 125;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Column8";
            this.Column8.MinimumWidth = 6;
            this.Column8.Name = "Column8";
            this.Column8.Visible = false;
            this.Column8.Width = 125;
            // 
            // _difMOchColumn
            // 
            this._difMOchColumn.HeaderText = "Очувствление *";
            this._difMOchColumn.MinimumWidth = 6;
            this._difMOchColumn.Name = "_difMOchColumn";
            this._difMOchColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difMOchColumn.Width = 105;
            // 
            // _difMIochColumn
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMIochColumn.DefaultCellStyle = dataGridViewCellStyle9;
            this._difMIochColumn.HeaderText = "Iд*, Iн";
            this._difMIochColumn.MinimumWidth = 6;
            this._difMIochColumn.Name = "_difMIochColumn";
            this._difMIochColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difMIochColumn.Width = 60;
            // 
            // _iOchMgColumn
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iOchMgColumn.DefaultCellStyle = dataGridViewCellStyle10;
            this._iOchMgColumn.HeaderText = "Iб*";
            this._iOchMgColumn.MinimumWidth = 6;
            this._iOchMgColumn.Name = "_iOchMgColumn";
            this._iOchMgColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iOchMgColumn.Visible = false;
            this._iOchMgColumn.Width = 60;
            // 
            // _difMtochColumn
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMtochColumn.DefaultCellStyle = dataGridViewCellStyle11;
            this._difMtochColumn.HeaderText = "tоч, мс";
            this._difMtochColumn.MinimumWidth = 6;
            this._difMtochColumn.Name = "_difMtochColumn";
            this._difMtochColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difMtochColumn.Width = 60;
            // 
            // _difMEnterOchColumn
            // 
            this._difMEnterOchColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMEnterOchColumn.DefaultCellStyle = dataGridViewCellStyle12;
            this._difMEnterOchColumn.HeaderText = "Вход очувств.";
            this._difMEnterOchColumn.MinimumWidth = 6;
            this._difMEnterOchColumn.Name = "_difMEnterOchColumn";
            this._difMEnterOchColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difMEnterOchColumn.Width = 74;
            // 
            // _difMOscColumn
            // 
            this._difMOscColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMOscColumn.DefaultCellStyle = dataGridViewCellStyle13;
            this._difMOscColumn.HeaderText = "Осц.";
            this._difMOscColumn.MinimumWidth = 6;
            this._difMOscColumn.Name = "_difMOscColumn";
            this._difMOscColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difMOscColumn.Width = 36;
            // 
            // _difMUrovColumn
            // 
            this._difMUrovColumn.HeaderText = "УРОВ";
            this._difMUrovColumn.MinimumWidth = 6;
            this._difMUrovColumn.Name = "_difMUrovColumn";
            this._difMUrovColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difMUrovColumn.Width = 60;
            // 
            // groupBox20
            // 
            this.groupBox20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox20.Controls.Add(this._difDDataGrid);
            this.groupBox20.Location = new System.Drawing.Point(3, 3);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(953, 142);
            this.groupBox20.TabIndex = 6;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Дифференциальные защиты по действующим значениям";
            // 
            // _difDDataGrid
            // 
            this._difDDataGrid.AllowUserToAddRows = false;
            this._difDDataGrid.AllowUserToDeleteRows = false;
            this._difDDataGrid.AllowUserToResizeColumns = false;
            this._difDDataGrid.AllowUserToResizeRows = false;
            this._difDDataGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._difDDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this._difDDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difDDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._difDStageColumn,
            this._difDModesColumn,
            this._difDBlockColumn,
            this.Column1,
            this._difDIcpColumn,
            this._difDIdoColumn,
            this._difDtcpColumn,
            this._difDIbColumn,
            this._difDfColumn,
            this._difDBlockGColumn,
            this._difDI2gColumn,
            this._difDBlock5GColumn,
            this._difDI5gColumn,
            this._difDOprNasColumn,
            this._difDOchColumn,
            this._difDIochColumn,
            this._iOchColumn,
            this._difDtochColumn,
            this._difDEnterOchColumn,
            this._difDOscColumn,
            this._difDUrovColumn});
            this._difDDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difDDataGrid.Location = new System.Drawing.Point(3, 16);
            this._difDDataGrid.MultiSelect = false;
            this._difDDataGrid.Name = "_difDDataGrid";
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._difDDataGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this._difDDataGrid.RowHeadersVisible = false;
            this._difDDataGrid.RowHeadersWidth = 51;
            this._difDDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difDDataGrid.RowTemplate.Height = 24;
            this._difDDataGrid.ShowCellErrors = false;
            this._difDDataGrid.ShowRowErrors = false;
            this._difDDataGrid.Size = new System.Drawing.Size(947, 123);
            this._difDDataGrid.TabIndex = 3;
            // 
            // _difDStageColumn
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.White;
            this._difDStageColumn.DefaultCellStyle = dataGridViewCellStyle16;
            this._difDStageColumn.HeaderText = "Ступень";
            this._difDStageColumn.MinimumWidth = 6;
            this._difDStageColumn.Name = "_difDStageColumn";
            this._difDStageColumn.ReadOnly = true;
            this._difDStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difDStageColumn.Width = 125;
            // 
            // _difDModesColumn
            // 
            this._difDModesColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDModesColumn.DefaultCellStyle = dataGridViewCellStyle17;
            this._difDModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._difDModesColumn.HeaderText = "Режим";
            this._difDModesColumn.MinimumWidth = 6;
            this._difDModesColumn.Name = "_difDModesColumn";
            this._difDModesColumn.Width = 48;
            // 
            // _difDBlockColumn
            // 
            this._difDBlockColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDBlockColumn.DefaultCellStyle = dataGridViewCellStyle18;
            this._difDBlockColumn.HeaderText = "Блокировка";
            this._difDBlockColumn.MinimumWidth = 6;
            this._difDBlockColumn.Name = "_difDBlockColumn";
            this._difDBlockColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difDBlockColumn.Width = 74;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Пуск от Iд3 ПО";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.Width = 125;
            // 
            // _difDIcpColumn
            // 
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDIcpColumn.DefaultCellStyle = dataGridViewCellStyle19;
            this._difDIcpColumn.HeaderText = "Iд>, Iн";
            this._difDIcpColumn.MinimumWidth = 6;
            this._difDIcpColumn.Name = "_difDIcpColumn";
            this._difDIcpColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difDIcpColumn.Width = 60;
            // 
            // _difDIdoColumn
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDIdoColumn.DefaultCellStyle = dataGridViewCellStyle20;
            this._difDIdoColumn.HeaderText = "Iд>>, Iн";
            this._difDIdoColumn.MinimumWidth = 6;
            this._difDIdoColumn.Name = "_difDIdoColumn";
            this._difDIdoColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difDIdoColumn.Width = 60;
            // 
            // _difDtcpColumn
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDtcpColumn.DefaultCellStyle = dataGridViewCellStyle21;
            this._difDtcpColumn.HeaderText = "tср, мс";
            this._difDtcpColumn.MinimumWidth = 6;
            this._difDtcpColumn.Name = "_difDtcpColumn";
            this._difDtcpColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difDtcpColumn.Width = 60;
            // 
            // _difDIbColumn
            // 
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDIbColumn.DefaultCellStyle = dataGridViewCellStyle22;
            this._difDIbColumn.HeaderText = "Iб, Iн";
            this._difDIbColumn.MinimumWidth = 6;
            this._difDIbColumn.Name = "_difDIbColumn";
            this._difDIbColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difDIbColumn.Width = 60;
            // 
            // _difDfColumn
            // 
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDfColumn.DefaultCellStyle = dataGridViewCellStyle23;
            this._difDfColumn.HeaderText = "f";
            this._difDfColumn.MinimumWidth = 6;
            this._difDfColumn.Name = "_difDfColumn";
            this._difDfColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difDfColumn.Width = 40;
            // 
            // _difDBlockGColumn
            // 
            this._difDBlockGColumn.HeaderText = "Блок. гармон. 2";
            this._difDBlockGColumn.MinimumWidth = 6;
            this._difDBlockGColumn.Name = "_difDBlockGColumn";
            this._difDBlockGColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difDBlockGColumn.Width = 110;
            // 
            // _difDI2gColumn
            // 
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDI2gColumn.DefaultCellStyle = dataGridViewCellStyle24;
            this._difDI2gColumn.HeaderText = "I2г, %";
            this._difDI2gColumn.MinimumWidth = 6;
            this._difDI2gColumn.Name = "_difDI2gColumn";
            this._difDI2gColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difDI2gColumn.Width = 50;
            // 
            // _difDBlock5GColumn
            // 
            this._difDBlock5GColumn.HeaderText = "Блок. гармон. 5";
            this._difDBlock5GColumn.MinimumWidth = 6;
            this._difDBlock5GColumn.Name = "_difDBlock5GColumn";
            this._difDBlock5GColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difDBlock5GColumn.Width = 110;
            // 
            // _difDI5gColumn
            // 
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._difDI5gColumn.DefaultCellStyle = dataGridViewCellStyle25;
            this._difDI5gColumn.HeaderText = "I5г, %";
            this._difDI5gColumn.MinimumWidth = 6;
            this._difDI5gColumn.Name = "_difDI5gColumn";
            this._difDI5gColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difDI5gColumn.Width = 50;
            // 
            // _difDOprNasColumn
            // 
            this._difDOprNasColumn.HeaderText = "Опред. насыщ.";
            this._difDOprNasColumn.MinimumWidth = 6;
            this._difDOprNasColumn.Name = "_difDOprNasColumn";
            this._difDOprNasColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difDOprNasColumn.Width = 105;
            // 
            // _difDOchColumn
            // 
            this._difDOchColumn.HeaderText = "Очувствление *";
            this._difDOchColumn.MinimumWidth = 6;
            this._difDOchColumn.Name = "_difDOchColumn";
            this._difDOchColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difDOchColumn.Width = 105;
            // 
            // _difDIochColumn
            // 
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDIochColumn.DefaultCellStyle = dataGridViewCellStyle26;
            this._difDIochColumn.HeaderText = "Iд*, Iн";
            this._difDIochColumn.MinimumWidth = 6;
            this._difDIochColumn.Name = "_difDIochColumn";
            this._difDIochColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difDIochColumn.Width = 60;
            // 
            // _iOchColumn
            // 
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iOchColumn.DefaultCellStyle = dataGridViewCellStyle27;
            this._iOchColumn.HeaderText = "Iб*";
            this._iOchColumn.MinimumWidth = 6;
            this._iOchColumn.Name = "_iOchColumn";
            this._iOchColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iOchColumn.Visible = false;
            this._iOchColumn.Width = 60;
            // 
            // _difDtochColumn
            // 
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDtochColumn.DefaultCellStyle = dataGridViewCellStyle28;
            this._difDtochColumn.HeaderText = "tоч, мс";
            this._difDtochColumn.MinimumWidth = 6;
            this._difDtochColumn.Name = "_difDtochColumn";
            this._difDtochColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difDtochColumn.Width = 60;
            // 
            // _difDEnterOchColumn
            // 
            this._difDEnterOchColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDEnterOchColumn.DefaultCellStyle = dataGridViewCellStyle29;
            this._difDEnterOchColumn.HeaderText = "Вход очувств.";
            this._difDEnterOchColumn.MinimumWidth = 6;
            this._difDEnterOchColumn.Name = "_difDEnterOchColumn";
            this._difDEnterOchColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difDEnterOchColumn.Width = 74;
            // 
            // _difDOscColumn
            // 
            this._difDOscColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDOscColumn.DefaultCellStyle = dataGridViewCellStyle30;
            this._difDOscColumn.HeaderText = "Осц.";
            this._difDOscColumn.MinimumWidth = 6;
            this._difDOscColumn.Name = "_difDOscColumn";
            this._difDOscColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difDOscColumn.Width = 36;
            // 
            // _difDUrovColumn
            // 
            this._difDUrovColumn.HeaderText = "УРОВ";
            this._difDUrovColumn.MinimumWidth = 6;
            this._difDUrovColumn.Name = "_difDUrovColumn";
            this._difDUrovColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difDUrovColumn.Width = 60;
            // 
            // tabPage22
            // 
            this.tabPage22.Controls.Add(this.groupBox21);
            this.tabPage22.Location = new System.Drawing.Point(4, 25);
            this.tabPage22.Name = "tabPage22";
            this.tabPage22.Size = new System.Drawing.Size(959, 430);
            this.tabPage22.TabIndex = 5;
            this.tabPage22.Text = "Защиты I>";
            this.tabPage22.UseVisualStyleBackColor = true;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._MTZDifensesDataGrid);
            this.groupBox21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox21.Location = new System.Drawing.Point(0, 0);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(959, 430);
            this.groupBox21.TabIndex = 4;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Защиты I>";
            // 
            // _MTZDifensesDataGrid
            // 
            this._MTZDifensesDataGrid.AllowUserToAddRows = false;
            this._MTZDifensesDataGrid.AllowUserToDeleteRows = false;
            this._MTZDifensesDataGrid.AllowUserToResizeColumns = false;
            this._MTZDifensesDataGrid.AllowUserToResizeRows = false;
            this._MTZDifensesDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._MTZDifensesDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._MTZDifensesDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._mtzStageColumn,
            this._mtzModesColumn,
            this._mtzBlockingColumn,
            this._LogicCol,
            this._mtzMeasureColumn,
            this._mtzICPColumn,
            this._mtzCharColumn,
            this._mtzTColumn,
            this._mtzkColumn,
            this._mtzOscColumn,
            this._mtzUROVColumn});
            this._MTZDifensesDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._MTZDifensesDataGrid.Location = new System.Drawing.Point(3, 16);
            this._MTZDifensesDataGrid.MultiSelect = false;
            this._MTZDifensesDataGrid.Name = "_MTZDifensesDataGrid";
            this._MTZDifensesDataGrid.RowHeadersVisible = false;
            this._MTZDifensesDataGrid.RowHeadersWidth = 51;
            this._MTZDifensesDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._MTZDifensesDataGrid.RowTemplate.Height = 24;
            this._MTZDifensesDataGrid.ShowCellErrors = false;
            this._MTZDifensesDataGrid.ShowRowErrors = false;
            this._MTZDifensesDataGrid.Size = new System.Drawing.Size(953, 411);
            this._MTZDifensesDataGrid.TabIndex = 3;
            // 
            // _mtzStageColumn
            // 
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle32.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.Color.White;
            this._mtzStageColumn.DefaultCellStyle = dataGridViewCellStyle32;
            this._mtzStageColumn.Frozen = true;
            this._mtzStageColumn.HeaderText = "Ступень";
            this._mtzStageColumn.MinimumWidth = 6;
            this._mtzStageColumn.Name = "_mtzStageColumn";
            this._mtzStageColumn.ReadOnly = true;
            this._mtzStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._mtzStageColumn.Width = 60;
            // 
            // _mtzModesColumn
            // 
            this._mtzModesColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._mtzModesColumn.DefaultCellStyle = dataGridViewCellStyle33;
            this._mtzModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._mtzModesColumn.HeaderText = "Режим";
            this._mtzModesColumn.MinimumWidth = 6;
            this._mtzModesColumn.Name = "_mtzModesColumn";
            this._mtzModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._mtzModesColumn.Width = 48;
            // 
            // _mtzBlockingColumn
            // 
            this._mtzBlockingColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._mtzBlockingColumn.HeaderText = "Блокировка";
            this._mtzBlockingColumn.MinimumWidth = 6;
            this._mtzBlockingColumn.Name = "_mtzBlockingColumn";
            this._mtzBlockingColumn.Width = 74;
            // 
            // _LogicCol
            // 
            this._LogicCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._LogicCol.HeaderText = "Логика";
            this._LogicCol.MinimumWidth = 6;
            this._LogicCol.Name = "_LogicCol";
            this._LogicCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._LogicCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._LogicCol.Width = 69;
            // 
            // _mtzMeasureColumn
            // 
            this._mtzMeasureColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._mtzMeasureColumn.DefaultCellStyle = dataGridViewCellStyle34;
            this._mtzMeasureColumn.HeaderText = "Измерение";
            this._mtzMeasureColumn.MinimumWidth = 6;
            this._mtzMeasureColumn.Name = "_mtzMeasureColumn";
            this._mtzMeasureColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._mtzMeasureColumn.Width = 71;
            // 
            // _mtzICPColumn
            // 
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._mtzICPColumn.DefaultCellStyle = dataGridViewCellStyle35;
            this._mtzICPColumn.HeaderText = "Iср, Iн";
            this._mtzICPColumn.MinimumWidth = 6;
            this._mtzICPColumn.Name = "_mtzICPColumn";
            this._mtzICPColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._mtzICPColumn.Width = 65;
            // 
            // _mtzCharColumn
            // 
            this._mtzCharColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._mtzCharColumn.DefaultCellStyle = dataGridViewCellStyle36;
            this._mtzCharColumn.HeaderText = "Характ-ка";
            this._mtzCharColumn.MinimumWidth = 6;
            this._mtzCharColumn.Name = "_mtzCharColumn";
            this._mtzCharColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._mtzCharColumn.Width = 64;
            // 
            // _mtzTColumn
            // 
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._mtzTColumn.DefaultCellStyle = dataGridViewCellStyle37;
            this._mtzTColumn.HeaderText = "t, мс";
            this._mtzTColumn.MinimumWidth = 6;
            this._mtzTColumn.Name = "_mtzTColumn";
            this._mtzTColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._mtzTColumn.Width = 65;
            // 
            // _mtzkColumn
            // 
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._mtzkColumn.DefaultCellStyle = dataGridViewCellStyle38;
            this._mtzkColumn.HeaderText = "k завис. хар-ки";
            this._mtzkColumn.MinimumWidth = 6;
            this._mtzkColumn.Name = "_mtzkColumn";
            this._mtzkColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._mtzkColumn.Width = 110;
            // 
            // _mtzOscColumn
            // 
            this._mtzOscColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._mtzOscColumn.DefaultCellStyle = dataGridViewCellStyle39;
            this._mtzOscColumn.HeaderText = "Осциллограф";
            this._mtzOscColumn.MinimumWidth = 6;
            this._mtzOscColumn.Name = "_mtzOscColumn";
            this._mtzOscColumn.Width = 82;
            // 
            // _mtzUROVColumn
            // 
            this._mtzUROVColumn.HeaderText = "УРОВ";
            this._mtzUROVColumn.MinimumWidth = 6;
            this._mtzUROVColumn.Name = "_mtzUROVColumn";
            this._mtzUROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._mtzUROVColumn.Width = 125;
            // 
            // tabPage19
            // 
            this.tabPage19.Controls.Add(this.groupBox24);
            this.tabPage19.Location = new System.Drawing.Point(4, 25);
            this.tabPage19.Name = "tabPage19";
            this.tabPage19.Size = new System.Drawing.Size(959, 430);
            this.tabPage19.TabIndex = 2;
            this.tabPage19.Text = "Внешние";
            this.tabPage19.UseVisualStyleBackColor = true;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this._externalDifensesDataGrid);
            this.groupBox24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox24.Location = new System.Drawing.Point(0, 0);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(959, 430);
            this.groupBox24.TabIndex = 5;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Внешние защиты";
            // 
            // _externalDifensesDataGrid
            // 
            this._externalDifensesDataGrid.AllowUserToAddRows = false;
            this._externalDifensesDataGrid.AllowUserToDeleteRows = false;
            this._externalDifensesDataGrid.AllowUserToResizeColumns = false;
            this._externalDifensesDataGrid.AllowUserToResizeRows = false;
            this._externalDifensesDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._externalDifensesDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._externalDifensesDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._externalDifStageColumn,
            this._externalDifModesColumn,
            this._externalDifOtklColumn,
            this._externalDifBlockingColumn,
            this._externalDifSrabColumn,
            this._externalDifTsrColumn,
            this._externalDifTvzColumn,
            this._externalDifVozvrColumn,
            this._externalDifVozvrYNColumn,
            this._externalDifOscColumn,
            this._externalDifUROVColumn});
            this._externalDifensesDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._externalDifensesDataGrid.Location = new System.Drawing.Point(3, 16);
            this._externalDifensesDataGrid.MultiSelect = false;
            this._externalDifensesDataGrid.Name = "_externalDifensesDataGrid";
            this._externalDifensesDataGrid.RowHeadersVisible = false;
            this._externalDifensesDataGrid.RowHeadersWidth = 51;
            this._externalDifensesDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._externalDifensesDataGrid.RowTemplate.Height = 24;
            this._externalDifensesDataGrid.ShowCellErrors = false;
            this._externalDifensesDataGrid.ShowRowErrors = false;
            this._externalDifensesDataGrid.Size = new System.Drawing.Size(953, 411);
            this._externalDifensesDataGrid.TabIndex = 3;
            // 
            // _externalDifStageColumn
            // 
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle40.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle40.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.Color.White;
            this._externalDifStageColumn.DefaultCellStyle = dataGridViewCellStyle40;
            this._externalDifStageColumn.Frozen = true;
            this._externalDifStageColumn.HeaderText = "Ступень";
            this._externalDifStageColumn.MinimumWidth = 6;
            this._externalDifStageColumn.Name = "_externalDifStageColumn";
            this._externalDifStageColumn.ReadOnly = true;
            this._externalDifStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifStageColumn.Width = 125;
            // 
            // _externalDifModesColumn
            // 
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifModesColumn.DefaultCellStyle = dataGridViewCellStyle41;
            this._externalDifModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._externalDifModesColumn.HeaderText = "Режим";
            this._externalDifModesColumn.MinimumWidth = 6;
            this._externalDifModesColumn.Name = "_externalDifModesColumn";
            this._externalDifModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDifModesColumn.Width = 95;
            // 
            // _externalDifOtklColumn
            // 
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifOtklColumn.DefaultCellStyle = dataGridViewCellStyle42;
            this._externalDifOtklColumn.HeaderText = "Отключение";
            this._externalDifOtklColumn.MinimumWidth = 6;
            this._externalDifOtklColumn.Name = "_externalDifOtklColumn";
            this._externalDifOtklColumn.Width = 125;
            // 
            // _externalDifBlockingColumn
            // 
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifBlockingColumn.DefaultCellStyle = dataGridViewCellStyle43;
            this._externalDifBlockingColumn.HeaderText = "Сигнал блокировки";
            this._externalDifBlockingColumn.MinimumWidth = 6;
            this._externalDifBlockingColumn.Name = "_externalDifBlockingColumn";
            this._externalDifBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDifBlockingColumn.Width = 120;
            // 
            // _externalDifSrabColumn
            // 
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifSrabColumn.DefaultCellStyle = dataGridViewCellStyle44;
            this._externalDifSrabColumn.HeaderText = "Сигнал срабатывания";
            this._externalDifSrabColumn.MinimumWidth = 6;
            this._externalDifSrabColumn.Name = "_externalDifSrabColumn";
            this._externalDifSrabColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDifSrabColumn.Width = 130;
            // 
            // _externalDifTsrColumn
            // 
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifTsrColumn.DefaultCellStyle = dataGridViewCellStyle45;
            this._externalDifTsrColumn.HeaderText = "tср, мс";
            this._externalDifTsrColumn.MinimumWidth = 6;
            this._externalDifTsrColumn.Name = "_externalDifTsrColumn";
            this._externalDifTsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifTsrColumn.Width = 70;
            // 
            // _externalDifTvzColumn
            // 
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifTvzColumn.DefaultCellStyle = dataGridViewCellStyle46;
            this._externalDifTvzColumn.HeaderText = "tвз, мс";
            this._externalDifTvzColumn.MinimumWidth = 6;
            this._externalDifTvzColumn.Name = "_externalDifTvzColumn";
            this._externalDifTvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifTvzColumn.Width = 70;
            // 
            // _externalDifVozvrColumn
            // 
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifVozvrColumn.DefaultCellStyle = dataGridViewCellStyle47;
            this._externalDifVozvrColumn.HeaderText = "Сигнал возврата";
            this._externalDifVozvrColumn.MinimumWidth = 6;
            this._externalDifVozvrColumn.Name = "_externalDifVozvrColumn";
            this._externalDifVozvrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDifVozvrColumn.Width = 110;
            // 
            // _externalDifVozvrYNColumn
            // 
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifVozvrYNColumn.DefaultCellStyle = dataGridViewCellStyle48;
            this._externalDifVozvrYNColumn.HeaderText = "Возврат";
            this._externalDifVozvrYNColumn.MinimumWidth = 6;
            this._externalDifVozvrYNColumn.Name = "_externalDifVozvrYNColumn";
            this._externalDifVozvrYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDifVozvrYNColumn.Width = 80;
            // 
            // _externalDifOscColumn
            // 
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifOscColumn.DefaultCellStyle = dataGridViewCellStyle49;
            this._externalDifOscColumn.HeaderText = "Осциллограф";
            this._externalDifOscColumn.MinimumWidth = 6;
            this._externalDifOscColumn.Name = "_externalDifOscColumn";
            this._externalDifOscColumn.Width = 110;
            // 
            // _externalDifUROVColumn
            // 
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifUROVColumn.DefaultCellStyle = dataGridViewCellStyle50;
            this._externalDifUROVColumn.HeaderText = "УРОВ";
            this._externalDifUROVColumn.MinimumWidth = 6;
            this._externalDifUROVColumn.Name = "_externalDifUROVColumn";
            this._externalDifUROVColumn.Width = 125;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.VLSTabControl);
            this.groupBox13.Location = new System.Drawing.Point(8, 3);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(409, 546);
            this.groupBox13.TabIndex = 5;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "ВЛС";
            // 
            // _inputSygnalsPage
            // 
            this._inputSygnalsPage.Controls.Add(this.groupBox34);
            this._inputSygnalsPage.Controls.Add(this.groupBox15);
            this._inputSygnalsPage.Controls.Add(this.groupBox32);
            this._inputSygnalsPage.Controls.Add(this.groupBox16);
            this._inputSygnalsPage.Controls.Add(this.groupBox17);
            this._inputSygnalsPage.Controls.Add(this.groupBox14);
            this._inputSygnalsPage.Location = new System.Drawing.Point(4, 22);
            this._inputSygnalsPage.Name = "_inputSygnalsPage";
            this._inputSygnalsPage.Size = new System.Drawing.Size(1024, 553);
            this._inputSygnalsPage.TabIndex = 7;
            this._inputSygnalsPage.Text = "Входные сигналы";
            this._inputSygnalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox34
            // 
            this.groupBox34.Controls.Add(this._antiBounceDataGridView);
            this.groupBox34.Location = new System.Drawing.Point(666, 3);
            this.groupBox34.Name = "groupBox34";
            this.groupBox34.Size = new System.Drawing.Size(251, 539);
            this.groupBox34.TabIndex = 8;
            this.groupBox34.TabStop = false;
            this.groupBox34.Text = "Антидребезговая задержка";
            // 
            // _antiBounceDataGridView
            // 
            this._antiBounceDataGridView.AllowUserToAddRows = false;
            this._antiBounceDataGridView.AllowUserToDeleteRows = false;
            this._antiBounceDataGridView.AllowUserToResizeColumns = false;
            this._antiBounceDataGridView.AllowUserToResizeRows = false;
            this._antiBounceDataGridView.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle51.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle51.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle51.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle51.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle51.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._antiBounceDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle51;
            this._antiBounceDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._antiBounceDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewComboBoxColumn26,
            this.dataGridViewComboBoxColumn28});
            this._antiBounceDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._antiBounceDataGridView.Location = new System.Drawing.Point(3, 16);
            this._antiBounceDataGridView.Name = "_antiBounceDataGridView";
            this._antiBounceDataGridView.RowHeadersVisible = false;
            this._antiBounceDataGridView.RowHeadersWidth = 51;
            this._antiBounceDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._antiBounceDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle52;
            this._antiBounceDataGridView.RowTemplate.Height = 24;
            this._antiBounceDataGridView.ShowCellErrors = false;
            this._antiBounceDataGridView.ShowRowErrors = false;
            this._antiBounceDataGridView.Size = new System.Drawing.Size(245, 520);
            this._antiBounceDataGridView.TabIndex = 1;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this._indComboBox);
            this.groupBox15.Location = new System.Drawing.Point(406, 86);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(254, 52);
            this.groupBox15.TabIndex = 7;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Сброс блинкеров";
            // 
            // _indComboBox
            // 
            this._indComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._indComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._indComboBox.FormattingEnabled = true;
            this._indComboBox.Location = new System.Drawing.Point(9, 19);
            this._indComboBox.Name = "_indComboBox";
            this._indComboBox.Size = new System.Drawing.Size(167, 21);
            this._indComboBox.TabIndex = 0;
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this.label31);
            this.groupBox32.Controls.Add(this._grUst2ComboBox);
            this.groupBox32.Controls.Add(this.label32);
            this.groupBox32.Controls.Add(this._grUst1ComboBox);
            this.groupBox32.Location = new System.Drawing.Point(406, 3);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(254, 77);
            this.groupBox32.TabIndex = 6;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "Группы уставок";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 48);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(51, 13);
            this.label31.TabIndex = 3;
            this.label31.Text = "Группа 2";
            // 
            // _grUst2ComboBox
            // 
            this._grUst2ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst2ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst2ComboBox.FormattingEnabled = true;
            this._grUst2ComboBox.Location = new System.Drawing.Point(76, 45);
            this._grUst2ComboBox.Name = "_grUst2ComboBox";
            this._grUst2ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst2ComboBox.TabIndex = 2;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 21);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(51, 13);
            this.label32.TabIndex = 1;
            this.label32.Text = "Группа 1";
            // 
            // _grUst1ComboBox
            // 
            this._grUst1ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst1ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst1ComboBox.FormattingEnabled = true;
            this._grUst1ComboBox.Location = new System.Drawing.Point(76, 18);
            this._grUst1ComboBox.Name = "_grUst1ComboBox";
            this._grUst1ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst1ComboBox.TabIndex = 0;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this._inpResetTtcomboBox);
            this.groupBox16.Location = new System.Drawing.Point(406, 144);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(254, 52);
            this.groupBox16.TabIndex = 5;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Сброс неисправности ТТ";
            // 
            // _inpResetTtcomboBox
            // 
            this._inpResetTtcomboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._inpResetTtcomboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._inpResetTtcomboBox.FormattingEnabled = true;
            this._inpResetTtcomboBox.Location = new System.Drawing.Point(9, 19);
            this._inpResetTtcomboBox.Name = "_inpResetTtcomboBox";
            this._inpResetTtcomboBox.Size = new System.Drawing.Size(167, 21);
            this._inpResetTtcomboBox.TabIndex = 0;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.tabControl2);
            this.groupBox17.Location = new System.Drawing.Point(207, 3);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(193, 547);
            this.groupBox17.TabIndex = 2;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Логические сигналы ИЛИ";
            // 
            // tabControl2
            // 
            this.tabControl2.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl2.Controls.Add(this.tabPage9);
            this.tabControl2.Controls.Add(this.tabPage10);
            this.tabControl2.Controls.Add(this.tabPage11);
            this.tabControl2.Controls.Add(this.tabPage12);
            this.tabControl2.Controls.Add(this.tabPage13);
            this.tabControl2.Controls.Add(this.tabPage14);
            this.tabControl2.Controls.Add(this.tabPage15);
            this.tabControl2.Controls.Add(this.tabPage16);
            this.tabControl2.Location = new System.Drawing.Point(6, 18);
            this.tabControl2.Multiline = true;
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(181, 528);
            this.tabControl2.TabIndex = 2;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this._inputSignals9);
            this.tabPage9.Location = new System.Drawing.Point(4, 49);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(173, 475);
            this.tabPage9.TabIndex = 0;
            this.tabPage9.Text = "ЛС9";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // _inputSignals9
            // 
            this._inputSignals9.AllowUserToAddRows = false;
            this._inputSignals9.AllowUserToDeleteRows = false;
            this._inputSignals9.AllowUserToResizeColumns = false;
            this._inputSignals9.AllowUserToResizeRows = false;
            this._inputSignals9.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals9.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals9.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._signalValueNumILI,
            this._signalValueColILI});
            this._inputSignals9.Location = new System.Drawing.Point(3, 3);
            this._inputSignals9.MultiSelect = false;
            this._inputSignals9.Name = "_inputSignals9";
            this._inputSignals9.RowHeadersVisible = false;
            this._inputSignals9.RowHeadersWidth = 51;
            this._inputSignals9.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals9.RowsDefaultCellStyle = dataGridViewCellStyle53;
            this._inputSignals9.RowTemplate.Height = 20;
            this._inputSignals9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals9.ShowCellErrors = false;
            this._inputSignals9.ShowCellToolTips = false;
            this._inputSignals9.ShowEditingIcon = false;
            this._inputSignals9.ShowRowErrors = false;
            this._inputSignals9.Size = new System.Drawing.Size(167, 469);
            this._inputSignals9.TabIndex = 2;
            // 
            // _signalValueNumILI
            // 
            this._signalValueNumILI.HeaderText = "№";
            this._signalValueNumILI.MinimumWidth = 6;
            this._signalValueNumILI.Name = "_signalValueNumILI";
            this._signalValueNumILI.ReadOnly = true;
            this._signalValueNumILI.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._signalValueNumILI.Width = 24;
            // 
            // _signalValueColILI
            // 
            this._signalValueColILI.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._signalValueColILI.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._signalValueColILI.HeaderText = "Значение";
            this._signalValueColILI.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this._signalValueColILI.MinimumWidth = 6;
            this._signalValueColILI.Name = "_signalValueColILI";
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this._inputSignals10);
            this.tabPage10.Location = new System.Drawing.Point(4, 49);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(173, 475);
            this.tabPage10.TabIndex = 1;
            this.tabPage10.Text = "ЛС10";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // _inputSignals10
            // 
            this._inputSignals10.AllowUserToAddRows = false;
            this._inputSignals10.AllowUserToDeleteRows = false;
            this._inputSignals10.AllowUserToResizeColumns = false;
            this._inputSignals10.AllowUserToResizeRows = false;
            this._inputSignals10.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals10.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals10.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals10.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewComboBoxColumn8});
            this._inputSignals10.Location = new System.Drawing.Point(3, 3);
            this._inputSignals10.MultiSelect = false;
            this._inputSignals10.Name = "_inputSignals10";
            this._inputSignals10.RowHeadersVisible = false;
            this._inputSignals10.RowHeadersWidth = 51;
            this._inputSignals10.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals10.RowsDefaultCellStyle = dataGridViewCellStyle54;
            this._inputSignals10.RowTemplate.Height = 20;
            this._inputSignals10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals10.ShowCellErrors = false;
            this._inputSignals10.ShowCellToolTips = false;
            this._inputSignals10.ShowEditingIcon = false;
            this._inputSignals10.ShowRowErrors = false;
            this._inputSignals10.Size = new System.Drawing.Size(167, 469);
            this._inputSignals10.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "№";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Width = 24;
            // 
            // dataGridViewComboBoxColumn8
            // 
            this.dataGridViewComboBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn8.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn8.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn8.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn8.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn8.Name = "dataGridViewComboBoxColumn8";
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this._inputSignals11);
            this.tabPage11.Location = new System.Drawing.Point(4, 49);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(173, 475);
            this.tabPage11.TabIndex = 2;
            this.tabPage11.Text = "ЛС11";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // _inputSignals11
            // 
            this._inputSignals11.AllowUserToAddRows = false;
            this._inputSignals11.AllowUserToDeleteRows = false;
            this._inputSignals11.AllowUserToResizeColumns = false;
            this._inputSignals11.AllowUserToResizeRows = false;
            this._inputSignals11.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals11.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals11.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals11.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewComboBoxColumn9});
            this._inputSignals11.Location = new System.Drawing.Point(3, 3);
            this._inputSignals11.MultiSelect = false;
            this._inputSignals11.Name = "_inputSignals11";
            this._inputSignals11.RowHeadersVisible = false;
            this._inputSignals11.RowHeadersWidth = 51;
            this._inputSignals11.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals11.RowsDefaultCellStyle = dataGridViewCellStyle55;
            this._inputSignals11.RowTemplate.Height = 20;
            this._inputSignals11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals11.ShowCellErrors = false;
            this._inputSignals11.ShowCellToolTips = false;
            this._inputSignals11.ShowEditingIcon = false;
            this._inputSignals11.ShowRowErrors = false;
            this._inputSignals11.Size = new System.Drawing.Size(167, 469);
            this._inputSignals11.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "№";
            this.dataGridViewTextBoxColumn9.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Width = 24;
            // 
            // dataGridViewComboBoxColumn9
            // 
            this.dataGridViewComboBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn9.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn9.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn9.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn9.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn9.Name = "dataGridViewComboBoxColumn9";
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this._inputSignals12);
            this.tabPage12.Location = new System.Drawing.Point(4, 49);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(173, 475);
            this.tabPage12.TabIndex = 3;
            this.tabPage12.Text = "ЛС12";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // _inputSignals12
            // 
            this._inputSignals12.AllowUserToAddRows = false;
            this._inputSignals12.AllowUserToDeleteRows = false;
            this._inputSignals12.AllowUserToResizeColumns = false;
            this._inputSignals12.AllowUserToResizeRows = false;
            this._inputSignals12.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals12.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals12.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals12.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewComboBoxColumn10});
            this._inputSignals12.Location = new System.Drawing.Point(3, 3);
            this._inputSignals12.MultiSelect = false;
            this._inputSignals12.Name = "_inputSignals12";
            this._inputSignals12.RowHeadersVisible = false;
            this._inputSignals12.RowHeadersWidth = 51;
            this._inputSignals12.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals12.RowsDefaultCellStyle = dataGridViewCellStyle56;
            this._inputSignals12.RowTemplate.Height = 20;
            this._inputSignals12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals12.ShowCellErrors = false;
            this._inputSignals12.ShowCellToolTips = false;
            this._inputSignals12.ShowEditingIcon = false;
            this._inputSignals12.ShowRowErrors = false;
            this._inputSignals12.Size = new System.Drawing.Size(167, 469);
            this._inputSignals12.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "№";
            this.dataGridViewTextBoxColumn10.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Width = 24;
            // 
            // dataGridViewComboBoxColumn10
            // 
            this.dataGridViewComboBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn10.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn10.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn10.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn10.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn10.Name = "dataGridViewComboBoxColumn10";
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this._inputSignals13);
            this.tabPage13.Location = new System.Drawing.Point(4, 49);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Size = new System.Drawing.Size(173, 475);
            this.tabPage13.TabIndex = 4;
            this.tabPage13.Text = "ЛС13";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // _inputSignals13
            // 
            this._inputSignals13.AllowUserToAddRows = false;
            this._inputSignals13.AllowUserToDeleteRows = false;
            this._inputSignals13.AllowUserToResizeColumns = false;
            this._inputSignals13.AllowUserToResizeRows = false;
            this._inputSignals13.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals13.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals13.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals13.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewComboBoxColumn11});
            this._inputSignals13.Location = new System.Drawing.Point(3, 3);
            this._inputSignals13.MultiSelect = false;
            this._inputSignals13.Name = "_inputSignals13";
            this._inputSignals13.RowHeadersVisible = false;
            this._inputSignals13.RowHeadersWidth = 51;
            this._inputSignals13.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals13.RowsDefaultCellStyle = dataGridViewCellStyle57;
            this._inputSignals13.RowTemplate.Height = 20;
            this._inputSignals13.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals13.ShowCellErrors = false;
            this._inputSignals13.ShowCellToolTips = false;
            this._inputSignals13.ShowEditingIcon = false;
            this._inputSignals13.ShowRowErrors = false;
            this._inputSignals13.Size = new System.Drawing.Size(167, 469);
            this._inputSignals13.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "№";
            this.dataGridViewTextBoxColumn11.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Width = 24;
            // 
            // dataGridViewComboBoxColumn11
            // 
            this.dataGridViewComboBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn11.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn11.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn11.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn11.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn11.Name = "dataGridViewComboBoxColumn11";
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this._inputSignals14);
            this.tabPage14.Location = new System.Drawing.Point(4, 49);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Size = new System.Drawing.Size(173, 475);
            this.tabPage14.TabIndex = 5;
            this.tabPage14.Text = "ЛС14";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // _inputSignals14
            // 
            this._inputSignals14.AllowUserToAddRows = false;
            this._inputSignals14.AllowUserToDeleteRows = false;
            this._inputSignals14.AllowUserToResizeColumns = false;
            this._inputSignals14.AllowUserToResizeRows = false;
            this._inputSignals14.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals14.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals14.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals14.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewComboBoxColumn12});
            this._inputSignals14.Location = new System.Drawing.Point(3, 3);
            this._inputSignals14.MultiSelect = false;
            this._inputSignals14.Name = "_inputSignals14";
            this._inputSignals14.RowHeadersVisible = false;
            this._inputSignals14.RowHeadersWidth = 51;
            this._inputSignals14.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals14.RowsDefaultCellStyle = dataGridViewCellStyle58;
            this._inputSignals14.RowTemplate.Height = 20;
            this._inputSignals14.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals14.ShowCellErrors = false;
            this._inputSignals14.ShowCellToolTips = false;
            this._inputSignals14.ShowEditingIcon = false;
            this._inputSignals14.ShowRowErrors = false;
            this._inputSignals14.Size = new System.Drawing.Size(167, 469);
            this._inputSignals14.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "№";
            this.dataGridViewTextBoxColumn12.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Width = 24;
            // 
            // dataGridViewComboBoxColumn12
            // 
            this.dataGridViewComboBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn12.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn12.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn12.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn12.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn12.Name = "dataGridViewComboBoxColumn12";
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this._inputSignals15);
            this.tabPage15.Location = new System.Drawing.Point(4, 49);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Size = new System.Drawing.Size(173, 475);
            this.tabPage15.TabIndex = 6;
            this.tabPage15.Text = "ЛС15";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // _inputSignals15
            // 
            this._inputSignals15.AllowUserToAddRows = false;
            this._inputSignals15.AllowUserToDeleteRows = false;
            this._inputSignals15.AllowUserToResizeColumns = false;
            this._inputSignals15.AllowUserToResizeRows = false;
            this._inputSignals15.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals15.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals15.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals15.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewComboBoxColumn13});
            this._inputSignals15.Location = new System.Drawing.Point(3, 3);
            this._inputSignals15.MultiSelect = false;
            this._inputSignals15.Name = "_inputSignals15";
            this._inputSignals15.RowHeadersVisible = false;
            this._inputSignals15.RowHeadersWidth = 51;
            this._inputSignals15.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals15.RowsDefaultCellStyle = dataGridViewCellStyle59;
            this._inputSignals15.RowTemplate.Height = 20;
            this._inputSignals15.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals15.ShowCellErrors = false;
            this._inputSignals15.ShowCellToolTips = false;
            this._inputSignals15.ShowEditingIcon = false;
            this._inputSignals15.ShowRowErrors = false;
            this._inputSignals15.Size = new System.Drawing.Size(167, 469);
            this._inputSignals15.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "№";
            this.dataGridViewTextBoxColumn13.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Width = 24;
            // 
            // dataGridViewComboBoxColumn13
            // 
            this.dataGridViewComboBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn13.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn13.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn13.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn13.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn13.Name = "dataGridViewComboBoxColumn13";
            // 
            // tabPage16
            // 
            this.tabPage16.Controls.Add(this._inputSignals16);
            this.tabPage16.Location = new System.Drawing.Point(4, 49);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Size = new System.Drawing.Size(173, 475);
            this.tabPage16.TabIndex = 7;
            this.tabPage16.Text = "ЛС16";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // _inputSignals16
            // 
            this._inputSignals16.AllowUserToAddRows = false;
            this._inputSignals16.AllowUserToDeleteRows = false;
            this._inputSignals16.AllowUserToResizeColumns = false;
            this._inputSignals16.AllowUserToResizeRows = false;
            this._inputSignals16.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals16.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals16.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals16.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewComboBoxColumn14});
            this._inputSignals16.Location = new System.Drawing.Point(3, 3);
            this._inputSignals16.MultiSelect = false;
            this._inputSignals16.Name = "_inputSignals16";
            this._inputSignals16.RowHeadersVisible = false;
            this._inputSignals16.RowHeadersWidth = 51;
            this._inputSignals16.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals16.RowsDefaultCellStyle = dataGridViewCellStyle60;
            this._inputSignals16.RowTemplate.Height = 20;
            this._inputSignals16.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals16.ShowCellErrors = false;
            this._inputSignals16.ShowCellToolTips = false;
            this._inputSignals16.ShowEditingIcon = false;
            this._inputSignals16.ShowRowErrors = false;
            this._inputSignals16.Size = new System.Drawing.Size(167, 469);
            this._inputSignals16.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "№";
            this.dataGridViewTextBoxColumn14.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Width = 24;
            // 
            // dataGridViewComboBoxColumn14
            // 
            this.dataGridViewComboBoxColumn14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn14.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn14.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn14.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn14.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn14.Name = "dataGridViewComboBoxColumn14";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.tabControl1);
            this.groupBox14.Location = new System.Drawing.Point(8, 3);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(193, 547);
            this.groupBox14.TabIndex = 0;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Логические сигналы И";
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Location = new System.Drawing.Point(6, 18);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(181, 528);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._inputSignals1);
            this.tabPage1.Location = new System.Drawing.Point(4, 49);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(173, 475);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "ЛС1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // _inputSignals1
            // 
            this._inputSignals1.AllowUserToAddRows = false;
            this._inputSignals1.AllowUserToDeleteRows = false;
            this._inputSignals1.AllowUserToResizeColumns = false;
            this._inputSignals1.AllowUserToResizeRows = false;
            this._inputSignals1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals1.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._lsChannelCol,
            this._signalValueCol});
            this._inputSignals1.Location = new System.Drawing.Point(3, 3);
            this._inputSignals1.MultiSelect = false;
            this._inputSignals1.Name = "_inputSignals1";
            this._inputSignals1.RowHeadersVisible = false;
            this._inputSignals1.RowHeadersWidth = 51;
            this._inputSignals1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle61.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals1.RowsDefaultCellStyle = dataGridViewCellStyle61;
            this._inputSignals1.RowTemplate.Height = 20;
            this._inputSignals1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals1.ShowCellErrors = false;
            this._inputSignals1.ShowCellToolTips = false;
            this._inputSignals1.ShowEditingIcon = false;
            this._inputSignals1.ShowRowErrors = false;
            this._inputSignals1.Size = new System.Drawing.Size(167, 469);
            this._inputSignals1.TabIndex = 2;
            // 
            // _lsChannelCol
            // 
            this._lsChannelCol.HeaderText = "№";
            this._lsChannelCol.MinimumWidth = 6;
            this._lsChannelCol.Name = "_lsChannelCol";
            this._lsChannelCol.ReadOnly = true;
            this._lsChannelCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._lsChannelCol.Width = 24;
            // 
            // _signalValueCol
            // 
            this._signalValueCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._signalValueCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._signalValueCol.HeaderText = "Значение";
            this._signalValueCol.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this._signalValueCol.MinimumWidth = 6;
            this._signalValueCol.Name = "_signalValueCol";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this._inputSignals2);
            this.tabPage2.Location = new System.Drawing.Point(4, 49);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(173, 475);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "ЛС2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // _inputSignals2
            // 
            this._inputSignals2.AllowUserToAddRows = false;
            this._inputSignals2.AllowUserToDeleteRows = false;
            this._inputSignals2.AllowUserToResizeColumns = false;
            this._inputSignals2.AllowUserToResizeRows = false;
            this._inputSignals2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals2.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewComboBoxColumn1});
            this._inputSignals2.Location = new System.Drawing.Point(3, 3);
            this._inputSignals2.MultiSelect = false;
            this._inputSignals2.Name = "_inputSignals2";
            this._inputSignals2.RowHeadersVisible = false;
            this._inputSignals2.RowHeadersWidth = 51;
            this._inputSignals2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle62.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals2.RowsDefaultCellStyle = dataGridViewCellStyle62;
            this._inputSignals2.RowTemplate.Height = 20;
            this._inputSignals2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals2.ShowCellErrors = false;
            this._inputSignals2.ShowCellToolTips = false;
            this._inputSignals2.ShowEditingIcon = false;
            this._inputSignals2.ShowRowErrors = false;
            this._inputSignals2.Size = new System.Drawing.Size(167, 469);
            this._inputSignals2.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "№";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 24;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn1.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn1.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this._inputSignals3);
            this.tabPage3.Location = new System.Drawing.Point(4, 49);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(173, 475);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "ЛС3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // _inputSignals3
            // 
            this._inputSignals3.AllowUserToAddRows = false;
            this._inputSignals3.AllowUserToDeleteRows = false;
            this._inputSignals3.AllowUserToResizeColumns = false;
            this._inputSignals3.AllowUserToResizeRows = false;
            this._inputSignals3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals3.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewComboBoxColumn2});
            this._inputSignals3.Location = new System.Drawing.Point(3, 3);
            this._inputSignals3.MultiSelect = false;
            this._inputSignals3.Name = "_inputSignals3";
            this._inputSignals3.RowHeadersVisible = false;
            this._inputSignals3.RowHeadersWidth = 51;
            this._inputSignals3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals3.RowsDefaultCellStyle = dataGridViewCellStyle63;
            this._inputSignals3.RowTemplate.Height = 20;
            this._inputSignals3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals3.ShowCellErrors = false;
            this._inputSignals3.ShowCellToolTips = false;
            this._inputSignals3.ShowEditingIcon = false;
            this._inputSignals3.ShowRowErrors = false;
            this._inputSignals3.Size = new System.Drawing.Size(167, 469);
            this._inputSignals3.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "№";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 24;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn2.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn2.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this._inputSignals4);
            this.tabPage4.Location = new System.Drawing.Point(4, 49);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(173, 475);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "ЛС4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // _inputSignals4
            // 
            this._inputSignals4.AllowUserToAddRows = false;
            this._inputSignals4.AllowUserToDeleteRows = false;
            this._inputSignals4.AllowUserToResizeColumns = false;
            this._inputSignals4.AllowUserToResizeRows = false;
            this._inputSignals4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals4.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewComboBoxColumn3});
            this._inputSignals4.Location = new System.Drawing.Point(3, 3);
            this._inputSignals4.MultiSelect = false;
            this._inputSignals4.Name = "_inputSignals4";
            this._inputSignals4.RowHeadersVisible = false;
            this._inputSignals4.RowHeadersWidth = 51;
            this._inputSignals4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals4.RowsDefaultCellStyle = dataGridViewCellStyle64;
            this._inputSignals4.RowTemplate.Height = 20;
            this._inputSignals4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals4.ShowCellErrors = false;
            this._inputSignals4.ShowCellToolTips = false;
            this._inputSignals4.ShowEditingIcon = false;
            this._inputSignals4.ShowRowErrors = false;
            this._inputSignals4.Size = new System.Drawing.Size(167, 469);
            this._inputSignals4.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "№";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 24;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn3.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn3.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this._inputSignals5);
            this.tabPage5.Location = new System.Drawing.Point(4, 49);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(173, 475);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "ЛС5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // _inputSignals5
            // 
            this._inputSignals5.AllowUserToAddRows = false;
            this._inputSignals5.AllowUserToDeleteRows = false;
            this._inputSignals5.AllowUserToResizeColumns = false;
            this._inputSignals5.AllowUserToResizeRows = false;
            this._inputSignals5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals5.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewComboBoxColumn4});
            this._inputSignals5.Location = new System.Drawing.Point(3, 3);
            this._inputSignals5.MultiSelect = false;
            this._inputSignals5.Name = "_inputSignals5";
            this._inputSignals5.RowHeadersVisible = false;
            this._inputSignals5.RowHeadersWidth = 51;
            this._inputSignals5.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle65.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals5.RowsDefaultCellStyle = dataGridViewCellStyle65;
            this._inputSignals5.RowTemplate.Height = 20;
            this._inputSignals5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals5.ShowCellErrors = false;
            this._inputSignals5.ShowCellToolTips = false;
            this._inputSignals5.ShowEditingIcon = false;
            this._inputSignals5.ShowRowErrors = false;
            this._inputSignals5.Size = new System.Drawing.Size(167, 469);
            this._inputSignals5.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "№";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 24;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn4.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn4.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this._inputSignals6);
            this.tabPage6.Location = new System.Drawing.Point(4, 49);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(173, 475);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "ЛС6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // _inputSignals6
            // 
            this._inputSignals6.AllowUserToAddRows = false;
            this._inputSignals6.AllowUserToDeleteRows = false;
            this._inputSignals6.AllowUserToResizeColumns = false;
            this._inputSignals6.AllowUserToResizeRows = false;
            this._inputSignals6.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals6.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewComboBoxColumn5});
            this._inputSignals6.Location = new System.Drawing.Point(3, 3);
            this._inputSignals6.MultiSelect = false;
            this._inputSignals6.Name = "_inputSignals6";
            this._inputSignals6.RowHeadersVisible = false;
            this._inputSignals6.RowHeadersWidth = 51;
            this._inputSignals6.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle66.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals6.RowsDefaultCellStyle = dataGridViewCellStyle66;
            this._inputSignals6.RowTemplate.Height = 20;
            this._inputSignals6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals6.ShowCellErrors = false;
            this._inputSignals6.ShowCellToolTips = false;
            this._inputSignals6.ShowEditingIcon = false;
            this._inputSignals6.ShowRowErrors = false;
            this._inputSignals6.Size = new System.Drawing.Size(167, 469);
            this._inputSignals6.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "№";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Width = 24;
            // 
            // dataGridViewComboBoxColumn5
            // 
            this.dataGridViewComboBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn5.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn5.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this._inputSignals7);
            this.tabPage7.Location = new System.Drawing.Point(4, 49);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(173, 475);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "ЛС7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // _inputSignals7
            // 
            this._inputSignals7.AllowUserToAddRows = false;
            this._inputSignals7.AllowUserToDeleteRows = false;
            this._inputSignals7.AllowUserToResizeColumns = false;
            this._inputSignals7.AllowUserToResizeRows = false;
            this._inputSignals7.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals7.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals7.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewComboBoxColumn6});
            this._inputSignals7.Location = new System.Drawing.Point(3, 3);
            this._inputSignals7.MultiSelect = false;
            this._inputSignals7.Name = "_inputSignals7";
            this._inputSignals7.RowHeadersVisible = false;
            this._inputSignals7.RowHeadersWidth = 51;
            this._inputSignals7.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle67.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals7.RowsDefaultCellStyle = dataGridViewCellStyle67;
            this._inputSignals7.RowTemplate.Height = 20;
            this._inputSignals7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals7.ShowCellErrors = false;
            this._inputSignals7.ShowCellToolTips = false;
            this._inputSignals7.ShowEditingIcon = false;
            this._inputSignals7.ShowRowErrors = false;
            this._inputSignals7.Size = new System.Drawing.Size(167, 469);
            this._inputSignals7.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "№";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Width = 24;
            // 
            // dataGridViewComboBoxColumn6
            // 
            this.dataGridViewComboBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn6.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn6.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this._inputSignals8);
            this.tabPage8.Location = new System.Drawing.Point(4, 49);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(173, 475);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "ЛС8";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // _inputSignals8
            // 
            this._inputSignals8.AllowUserToAddRows = false;
            this._inputSignals8.AllowUserToDeleteRows = false;
            this._inputSignals8.AllowUserToResizeColumns = false;
            this._inputSignals8.AllowUserToResizeRows = false;
            this._inputSignals8.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals8.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals8.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewComboBoxColumn7});
            this._inputSignals8.Location = new System.Drawing.Point(3, 3);
            this._inputSignals8.MultiSelect = false;
            this._inputSignals8.Name = "_inputSignals8";
            this._inputSignals8.RowHeadersVisible = false;
            this._inputSignals8.RowHeadersWidth = 51;
            this._inputSignals8.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle68.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals8.RowsDefaultCellStyle = dataGridViewCellStyle68;
            this._inputSignals8.RowTemplate.Height = 20;
            this._inputSignals8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals8.ShowCellErrors = false;
            this._inputSignals8.ShowCellToolTips = false;
            this._inputSignals8.ShowEditingIcon = false;
            this._inputSignals8.ShowRowErrors = false;
            this._inputSignals8.Size = new System.Drawing.Size(167, 469);
            this._inputSignals8.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "№";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Width = 24;
            // 
            // dataGridViewComboBoxColumn7
            // 
            this.dataGridViewComboBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn7.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn7.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn7.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn7.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            // 
            // _toolTip
            // 
            this._toolTip.ShowAlways = true;
            this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // _configurationTabControl
            // 
            this._configurationTabControl.ContextMenuStrip = this.contextMenu;
            this._configurationTabControl.Controls.Add(this._joinPage);
            this._configurationTabControl.Controls.Add(this._inputSygnalsPage);
            this._configurationTabControl.Controls.Add(this.tabPage20);
            this._configurationTabControl.Controls.Add(this._allDefensesPage);
            this._configurationTabControl.Controls.Add(this._urovPage);
            this._configurationTabControl.Controls.Add(this._relayIndPage);
            this._configurationTabControl.Controls.Add(this.tabPage23);
            this._configurationTabControl.Controls.Add(this._outputSignalsPage);
            this._configurationTabControl.Controls.Add(this._systemPage);
            this._configurationTabControl.Controls.Add(this.tabPage17);
            this._configurationTabControl.Controls.Add(this._ethernetPage);
            this._configurationTabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._configurationTabControl.Location = new System.Drawing.Point(0, 0);
            this._configurationTabControl.MinimumSize = new System.Drawing.Size(820, 579);
            this._configurationTabControl.Name = "_configurationTabControl";
            this._configurationTabControl.SelectedIndex = 0;
            this._configurationTabControl.Size = new System.Drawing.Size(1032, 579);
            this._configurationTabControl.TabIndex = 30;
            // 
            // contextMenu
            // 
            this.contextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.resetSetpointsItem,
            this.clearSetpointsItem,
            this.readFromFileItem,
            this.writeToFileItem,
            this.writeToHtmlItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(223, 158);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(222, 22);
            this.readFromDeviceItem.Text = "Прочитать из устройства";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(222, 22);
            this.writeToDeviceItem.Text = "Записать в устройство";
            // 
            // resetSetpointsItem
            // 
            this.resetSetpointsItem.Name = "resetSetpointsItem";
            this.resetSetpointsItem.Size = new System.Drawing.Size(222, 22);
            this.resetSetpointsItem.Text = "Загрузить базовые уставки";
            // 
            // clearSetpointsItem
            // 
            this.clearSetpointsItem.Name = "clearSetpointsItem";
            this.clearSetpointsItem.Size = new System.Drawing.Size(222, 22);
            this.clearSetpointsItem.Text = "Обнулить уставки";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(222, 22);
            this.readFromFileItem.Text = "Загрузить из файла";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(222, 22);
            this.writeToFileItem.Text = "Сохранить в файл";
            // 
            // writeToHtmlItem
            // 
            this.writeToHtmlItem.Name = "writeToHtmlItem";
            this.writeToHtmlItem.Size = new System.Drawing.Size(222, 22);
            this.writeToHtmlItem.Text = "Сохранить в HTML";
            // 
            // _joinPage
            // 
            this._joinPage.Controls.Add(this._joinData);
            this._joinPage.Location = new System.Drawing.Point(4, 22);
            this._joinPage.Name = "_joinPage";
            this._joinPage.Size = new System.Drawing.Size(1024, 553);
            this._joinPage.TabIndex = 2;
            this._joinPage.Text = "Присоединения";
            this._joinPage.UseVisualStyleBackColor = true;
            // 
            // _joinData
            // 
            this._joinData.AllowUserToAddRows = false;
            this._joinData.AllowUserToDeleteRows = false;
            this._joinData.AllowUserToResizeColumns = false;
            this._joinData.AllowUserToResizeRows = false;
            this._joinData.BackgroundColor = System.Drawing.Color.White;
            this._joinData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._joinData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn15,
            this._joinITT,
            this._joinSwitchOFF,
            this._joinSwitchOn,
            this._joinJoin,
            this._joinEnter,
            this._joinResetColumn,
            this._timeResetJoinColumn});
            this._joinData.Dock = System.Windows.Forms.DockStyle.Fill;
            this._joinData.Location = new System.Drawing.Point(0, 0);
            this._joinData.MultiSelect = false;
            this._joinData.Name = "_joinData";
            this._joinData.RowHeadersVisible = false;
            this._joinData.RowHeadersWidth = 51;
            this._joinData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._joinData.RowTemplate.Height = 24;
            this._joinData.ShowCellErrors = false;
            this._joinData.ShowRowErrors = false;
            this._joinData.Size = new System.Drawing.Size(1024, 553);
            this._joinData.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn15.DataPropertyName = "Stage";
            dataGridViewCellStyle69.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle69.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle69.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle69.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle69.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle69;
            this.dataGridViewTextBoxColumn15.Frozen = true;
            this.dataGridViewTextBoxColumn15.HeaderText = "Ступень";
            this.dataGridViewTextBoxColumn15.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Width = 54;
            // 
            // _joinITT
            // 
            this._joinITT.DataPropertyName = "Inom";
            dataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._joinITT.DefaultCellStyle = dataGridViewCellStyle70;
            this._joinITT.Frozen = true;
            this._joinITT.HeaderText = "Iтт, A";
            this._joinITT.MinimumWidth = 6;
            this._joinITT.Name = "_joinITT";
            this._joinITT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._joinITT.Width = 70;
            // 
            // _joinSwitchOFF
            // 
            this._joinSwitchOFF.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._joinSwitchOFF.DataPropertyName = "JoinSwitchoff";
            dataGridViewCellStyle71.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._joinSwitchOFF.DefaultCellStyle = dataGridViewCellStyle71;
            this._joinSwitchOFF.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._joinSwitchOFF.Frozen = true;
            this._joinSwitchOFF.HeaderText = "Отключ.";
            this._joinSwitchOFF.MinimumWidth = 6;
            this._joinSwitchOFF.Name = "_joinSwitchOFF";
            this._joinSwitchOFF.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._joinSwitchOFF.Width = 54;
            // 
            // _joinSwitchOn
            // 
            this._joinSwitchOn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._joinSwitchOn.DataPropertyName = "JoinSwitchon";
            dataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._joinSwitchOn.DefaultCellStyle = dataGridViewCellStyle72;
            this._joinSwitchOn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._joinSwitchOn.Frozen = true;
            this._joinSwitchOn.HeaderText = "Включ.";
            this._joinSwitchOn.MinimumWidth = 6;
            this._joinSwitchOn.Name = "_joinSwitchOn";
            this._joinSwitchOn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._joinSwitchOn.Width = 48;
            // 
            // _joinJoin
            // 
            this._joinJoin.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._joinJoin.DataPropertyName = "JoinJoin";
            dataGridViewCellStyle73.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._joinJoin.DefaultCellStyle = dataGridViewCellStyle73;
            this._joinJoin.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._joinJoin.Frozen = true;
            this._joinJoin.HeaderText = "Привязка";
            this._joinJoin.MinimumWidth = 6;
            this._joinJoin.Name = "_joinJoin";
            this._joinJoin.Width = 63;
            // 
            // _joinEnter
            // 
            this._joinEnter.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._joinEnter.DataPropertyName = "JoinEnter";
            dataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._joinEnter.DefaultCellStyle = dataGridViewCellStyle74;
            this._joinEnter.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._joinEnter.Frozen = true;
            this._joinEnter.HeaderText = "Вход";
            this._joinEnter.MinimumWidth = 6;
            this._joinEnter.Name = "_joinEnter";
            this._joinEnter.Width = 37;
            // 
            // _joinResetColumn
            // 
            this._joinResetColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._joinResetColumn.DataPropertyName = "ResetJoin";
            this._joinResetColumn.Frozen = true;
            this._joinResetColumn.HeaderText = "Обнуление";
            this._joinResetColumn.MinimumWidth = 6;
            this._joinResetColumn.Name = "_joinResetColumn";
            this._joinResetColumn.Width = 68;
            // 
            // _timeResetJoinColumn
            // 
            this._timeResetJoinColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._timeResetJoinColumn.DataPropertyName = "ResetDeley";
            this._timeResetJoinColumn.Frozen = true;
            this._timeResetJoinColumn.HeaderText = "tобнул, задержка обнуления, мс ";
            this._timeResetJoinColumn.MinimumWidth = 6;
            this._timeResetJoinColumn.Name = "_timeResetJoinColumn";
            this._timeResetJoinColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeResetJoinColumn.Width = 144;
            // 
            // tabPage20
            // 
            this.tabPage20.Controls.Add(this._configTtDgv);
            this.tabPage20.Location = new System.Drawing.Point(4, 22);
            this.tabPage20.Name = "tabPage20";
            this.tabPage20.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage20.Size = new System.Drawing.Size(1024, 553);
            this.tabPage20.TabIndex = 11;
            this.tabPage20.Text = "Контроль цепей ТТ";
            this.tabPage20.UseVisualStyleBackColor = true;
            // 
            // _configTtDgv
            // 
            this._configTtDgv.AllowUserToAddRows = false;
            this._configTtDgv.AllowUserToDeleteRows = false;
            this._configTtDgv.AllowUserToResizeColumns = false;
            this._configTtDgv.AllowUserToResizeRows = false;
            this._configTtDgv.BackgroundColor = System.Drawing.Color.White;
            this._configTtDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._configTtDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._nameTtColumn,
            this._idTtColumn,
            this._timeSrabTtColumn,
            this._faultTtColumn,
            this._resetTtCol});
            this._configTtDgv.Location = new System.Drawing.Point(6, 6);
            this._configTtDgv.MultiSelect = false;
            this._configTtDgv.Name = "_configTtDgv";
            this._configTtDgv.RowHeadersVisible = false;
            this._configTtDgv.RowHeadersWidth = 51;
            this._configTtDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._configTtDgv.RowTemplate.Height = 24;
            this._configTtDgv.ShowCellErrors = false;
            this._configTtDgv.ShowRowErrors = false;
            this._configTtDgv.Size = new System.Drawing.Size(548, 99);
            this._configTtDgv.TabIndex = 6;
            // 
            // _nameTtColumn
            // 
            dataGridViewCellStyle75.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle75.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle75.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle75.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle75.SelectionForeColor = System.Drawing.Color.White;
            this._nameTtColumn.DefaultCellStyle = dataGridViewCellStyle75;
            this._nameTtColumn.Frozen = true;
            this._nameTtColumn.HeaderText = "Контроль цепей ТТ";
            this._nameTtColumn.MinimumWidth = 6;
            this._nameTtColumn.Name = "_nameTtColumn";
            this._nameTtColumn.ReadOnly = true;
            this._nameTtColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._nameTtColumn.Width = 120;
            // 
            // _idTtColumn
            // 
            dataGridViewCellStyle76.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._idTtColumn.DefaultCellStyle = dataGridViewCellStyle76;
            this._idTtColumn.HeaderText = "Iдmin, Iн";
            this._idTtColumn.MinimumWidth = 6;
            this._idTtColumn.Name = "_idTtColumn";
            this._idTtColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._idTtColumn.Width = 75;
            // 
            // _timeSrabTtColumn
            // 
            dataGridViewCellStyle77.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._timeSrabTtColumn.DefaultCellStyle = dataGridViewCellStyle77;
            this._timeSrabTtColumn.HeaderText = "Tср, мс";
            this._timeSrabTtColumn.MinimumWidth = 6;
            this._timeSrabTtColumn.Name = "_timeSrabTtColumn";
            this._timeSrabTtColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeSrabTtColumn.Width = 75;
            // 
            // _faultTtColumn
            // 
            this._faultTtColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._faultTtColumn.HeaderText = "Неисправность";
            this._faultTtColumn.MinimumWidth = 6;
            this._faultTtColumn.Name = "_faultTtColumn";
            this._faultTtColumn.Width = 135;
            // 
            // _resetTtCol
            // 
            this._resetTtCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._resetTtCol.HeaderText = "Сброс";
            this._resetTtCol.MinimumWidth = 6;
            this._resetTtCol.Name = "_resetTtCol";
            // 
            // _urovPage
            // 
            this._urovPage.Controls.Add(this.groupBox11);
            this._urovPage.Controls.Add(this.groupBox2);
            this._urovPage.Controls.Add(this._DZHKontr);
            this._urovPage.Controls.Add(this.label3);
            this._urovPage.Location = new System.Drawing.Point(4, 22);
            this._urovPage.Name = "_urovPage";
            this._urovPage.Size = new System.Drawing.Size(1024, 553);
            this._urovPage.TabIndex = 9;
            this._urovPage.Text = "УРОВ";
            this._urovPage.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._DZHUROVself);
            this.groupBox11.Controls.Add(this.label37);
            this.groupBox11.Controls.Add(this.label38);
            this.groupBox11.Controls.Add(this.urovGroup);
            this.groupBox11.Controls.Add(this._DZHUROVblock);
            this.groupBox11.Location = new System.Drawing.Point(277, 3);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(739, 547);
            this.groupBox11.TabIndex = 2;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "УРОВ присоед.";
            // 
            // _DZHUROVself
            // 
            this._DZHUROVself.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHUROVself.FormattingEnabled = true;
            this._DZHUROVself.Location = new System.Drawing.Point(94, 46);
            this._DZHUROVself.Name = "_DZHUROVself";
            this._DZHUROVself.Size = new System.Drawing.Size(121, 21);
            this._DZHUROVself.TabIndex = 19;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(10, 49);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(48, 13);
            this.label37.TabIndex = 8;
            this.label37.Text = "На себя";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(10, 29);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(32, 13);
            this.label38.TabIndex = 9;
            this.label38.Text = "Блок";
            // 
            // urovGroup
            // 
            this.urovGroup.Controls.Add(this._UROVJoinData);
            this.urovGroup.Location = new System.Drawing.Point(234, 7);
            this.urovGroup.Name = "urovGroup";
            this.urovGroup.Size = new System.Drawing.Size(499, 534);
            this.urovGroup.TabIndex = 1;
            this.urovGroup.TabStop = false;
            this.urovGroup.Text = "Присоединений";
            // 
            // _UROVJoinData
            // 
            this._UROVJoinData.AllowUserToAddRows = false;
            this._UROVJoinData.AllowUserToDeleteRows = false;
            this._UROVJoinData.AllowUserToResizeColumns = false;
            this._UROVJoinData.AllowUserToResizeRows = false;
            this._UROVJoinData.BackgroundColor = System.Drawing.Color.White;
            this._UROVJoinData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._UROVJoinData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._UROVJoinData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn16,
            this.Column21,
            this._JoinIUROV,
            this._JoinTUROV,
            this.Column22});
            this._UROVJoinData.Dock = System.Windows.Forms.DockStyle.Fill;
            this._UROVJoinData.Location = new System.Drawing.Point(3, 16);
            this._UROVJoinData.MultiSelect = false;
            this._UROVJoinData.Name = "_UROVJoinData";
            this._UROVJoinData.RowHeadersVisible = false;
            this._UROVJoinData.RowHeadersWidth = 51;
            this._UROVJoinData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._UROVJoinData.RowTemplate.Height = 24;
            this._UROVJoinData.ShowCellErrors = false;
            this._UROVJoinData.ShowRowErrors = false;
            this._UROVJoinData.Size = new System.Drawing.Size(493, 515);
            this._UROVJoinData.TabIndex = 5;
            // 
            // dataGridViewTextBoxColumn16
            // 
            dataGridViewCellStyle78.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle78.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle78.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle78.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle78.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle78;
            this.dataGridViewTextBoxColumn16.Frozen = true;
            this.dataGridViewTextBoxColumn16.HeaderText = "Ступень";
            this.dataGridViewTextBoxColumn16.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.Width = 120;
            // 
            // Column21
            // 
            this.Column21.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column21.HeaderText = "Вх. Пуска";
            this.Column21.MinimumWidth = 6;
            this.Column21.Name = "Column21";
            this.Column21.Width = 56;
            // 
            // _JoinIUROV
            // 
            dataGridViewCellStyle79.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._JoinIUROV.DefaultCellStyle = dataGridViewCellStyle79;
            this._JoinIUROV.HeaderText = "Iуров, Iн";
            this._JoinIUROV.MinimumWidth = 6;
            this._JoinIUROV.Name = "_JoinIUROV";
            this._JoinIUROV.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._JoinIUROV.Width = 70;
            // 
            // _JoinTUROV
            // 
            this._JoinTUROV.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle80.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._JoinTUROV.DefaultCellStyle = dataGridViewCellStyle80;
            this._JoinTUROV.HeaderText = "t уров1 пр., мс (На себя)";
            this._JoinTUROV.MinimumWidth = 6;
            this._JoinTUROV.Name = "_JoinTUROV";
            this._JoinTUROV.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column22
            // 
            this.Column22.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle81.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column22.DefaultCellStyle = dataGridViewCellStyle81;
            this.Column22.HeaderText = "t уров2 пр., мс (На смеж. вых.)";
            this.Column22.MinimumWidth = 6;
            this.Column22.Name = "Column22";
            this.Column22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _DZHUROVblock
            // 
            this._DZHUROVblock.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._DZHUROVblock.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._DZHUROVblock.FormattingEnabled = true;
            this._DZHUROVblock.Location = new System.Drawing.Point(94, 26);
            this._DZHUROVblock.Name = "_DZHUROVblock";
            this._DZHUROVblock.Size = new System.Drawing.Size(121, 21);
            this._DZHUROVblock.TabIndex = 20;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._tOtklTextBox);
            this.groupBox2.Controls.Add(this.label41);
            this.groupBox2.Controls.Add(this.groupBox33);
            this.groupBox2.Controls.Add(this.groupBox18);
            this.groupBox2.Controls.Add(this._DZHTUrov3);
            this.groupBox2.Controls.Add(this._DZHTUrov2);
            this.groupBox2.Controls.Add(this._DZHUrov3);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this._DZHUrov2);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this._DZHTUrov1);
            this.groupBox2.Controls.Add(this._DZHSelf);
            this.groupBox2.Controls.Add(this._DZHConn);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(8, 37);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(263, 513);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "УРОВ СШ";
            // 
            // _tOtklTextBox
            // 
            this._tOtklTextBox.Location = new System.Drawing.Point(101, 252);
            this._tOtklTextBox.Name = "_tOtklTextBox";
            this._tOtklTextBox.Size = new System.Drawing.Size(121, 20);
            this._tOtklTextBox.TabIndex = 30;
            this._tOtklTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(17, 255);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(53, 13);
            this.label41.TabIndex = 29;
            this.label41.Text = "tоткл, мс";
            // 
            // groupBox33
            // 
            this.groupBox33.Controls.Add(this._DZHSHBLOCK1);
            this.groupBox33.Controls.Add(this.label33);
            this.groupBox33.Controls.Add(this.label34);
            this.groupBox33.Controls.Add(this.label35);
            this.groupBox33.Controls.Add(this._DZHSHBLOCK2);
            this.groupBox33.Controls.Add(this._DZHPOBLOCK1);
            this.groupBox33.Location = new System.Drawing.Point(6, 381);
            this.groupBox33.Name = "groupBox33";
            this.groupBox33.Size = new System.Drawing.Size(251, 97);
            this.groupBox33.TabIndex = 26;
            this.groupBox33.TabStop = false;
            this.groupBox33.Text = "Блок. УРОВ";
            // 
            // _DZHSHBLOCK1
            // 
            this._DZHSHBLOCK1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHSHBLOCK1.FormattingEnabled = true;
            this._DZHSHBLOCK1.Location = new System.Drawing.Point(95, 19);
            this._DZHSHBLOCK1.Name = "_DZHSHBLOCK1";
            this._DZHSHBLOCK1.Size = new System.Drawing.Size(121, 21);
            this._DZHSHBLOCK1.TabIndex = 19;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(11, 22);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(29, 13);
            this.label33.TabIndex = 8;
            this.label33.Text = "СШ1";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(11, 43);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 13);
            this.label34.TabIndex = 9;
            this.label34.Text = "СШ2";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(11, 64);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(23, 13);
            this.label35.TabIndex = 10;
            this.label35.Text = "ПО";
            // 
            // _DZHSHBLOCK2
            // 
            this._DZHSHBLOCK2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHSHBLOCK2.FormattingEnabled = true;
            this._DZHSHBLOCK2.Location = new System.Drawing.Point(95, 40);
            this._DZHSHBLOCK2.Name = "_DZHSHBLOCK2";
            this._DZHSHBLOCK2.Size = new System.Drawing.Size(121, 21);
            this._DZHSHBLOCK2.TabIndex = 20;
            // 
            // _DZHPOBLOCK1
            // 
            this._DZHPOBLOCK1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHPOBLOCK1.FormattingEnabled = true;
            this._DZHPOBLOCK1.Location = new System.Drawing.Point(95, 61);
            this._DZHPOBLOCK1.Name = "_DZHPOBLOCK1";
            this._DZHPOBLOCK1.Size = new System.Drawing.Size(121, 21);
            this._DZHPOBLOCK1.TabIndex = 21;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this._DZHSH1);
            this.groupBox18.Controls.Add(this.label13);
            this.groupBox18.Controls.Add(this.label12);
            this.groupBox18.Controls.Add(this.label11);
            this.groupBox18.Controls.Add(this._DZHSH2);
            this.groupBox18.Controls.Add(this._DZHPO);
            this.groupBox18.Location = new System.Drawing.Point(6, 278);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(251, 97);
            this.groupBox18.TabIndex = 25;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Сигнал пуска";
            // 
            // _DZHSH1
            // 
            this._DZHSH1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHSH1.FormattingEnabled = true;
            this._DZHSH1.Location = new System.Drawing.Point(95, 19);
            this._DZHSH1.Name = "_DZHSH1";
            this._DZHSH1.Size = new System.Drawing.Size(121, 21);
            this._DZHSH1.TabIndex = 19;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "СШ1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 43);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "СШ2";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 64);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(23, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "ПО";
            // 
            // _DZHSH2
            // 
            this._DZHSH2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHSH2.FormattingEnabled = true;
            this._DZHSH2.Location = new System.Drawing.Point(95, 40);
            this._DZHSH2.Name = "_DZHSH2";
            this._DZHSH2.Size = new System.Drawing.Size(121, 21);
            this._DZHSH2.TabIndex = 20;
            // 
            // _DZHPO
            // 
            this._DZHPO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHPO.FormattingEnabled = true;
            this._DZHPO.Location = new System.Drawing.Point(95, 61);
            this._DZHPO.Name = "_DZHPO";
            this._DZHPO.Size = new System.Drawing.Size(121, 21);
            this._DZHPO.TabIndex = 21;
            // 
            // _DZHTUrov3
            // 
            this._DZHTUrov3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._DZHTUrov3.Location = new System.Drawing.Point(101, 226);
            this._DZHTUrov3.Name = "_DZHTUrov3";
            this._DZHTUrov3.Size = new System.Drawing.Size(121, 20);
            this._DZHTUrov3.TabIndex = 24;
            this._DZHTUrov3.Tag = "3000000";
            this._DZHTUrov3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _DZHTUrov2
            // 
            this._DZHTUrov2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._DZHTUrov2.Location = new System.Drawing.Point(101, 149);
            this._DZHTUrov2.Name = "_DZHTUrov2";
            this._DZHTUrov2.Size = new System.Drawing.Size(121, 20);
            this._DZHTUrov2.TabIndex = 23;
            this._DZHTUrov2.Tag = "3000000";
            this._DZHTUrov2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _DZHUrov3
            // 
            this._DZHUrov3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHUrov3.FormattingEnabled = true;
            this._DZHUrov3.Location = new System.Drawing.Point(101, 199);
            this._DZHUrov3.Name = "_DZHUrov3";
            this._DZHUrov3.Size = new System.Drawing.Size(121, 21);
            this._DZHUrov3.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 202);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "УРОВ 3";
            // 
            // _DZHUrov2
            // 
            this._DZHUrov2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHUrov2.FormattingEnabled = true;
            this._DZHUrov2.Location = new System.Drawing.Point(101, 122);
            this._DZHUrov2.Name = "_DZHUrov2";
            this._DZHUrov2.Size = new System.Drawing.Size(121, 21);
            this._DZHUrov2.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "УРОВ 2";
            // 
            // _DZHTUrov1
            // 
            this._DZHTUrov1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._DZHTUrov1.Location = new System.Drawing.Point(101, 78);
            this._DZHTUrov1.Name = "_DZHTUrov1";
            this._DZHTUrov1.Size = new System.Drawing.Size(121, 20);
            this._DZHTUrov1.TabIndex = 22;
            this._DZHTUrov1.Tag = "3000000";
            this._DZHTUrov1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _DZHSelf
            // 
            this._DZHSelf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHSelf.FormattingEnabled = true;
            this._DZHSelf.Location = new System.Drawing.Point(101, 51);
            this._DZHSelf.Name = "_DZHSelf";
            this._DZHSelf.Size = new System.Drawing.Size(121, 21);
            this._DZHSelf.TabIndex = 14;
            // 
            // _DZHConn
            // 
            this._DZHConn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHConn.FormattingEnabled = true;
            this._DZHConn.Location = new System.Drawing.Point(101, 19);
            this._DZHConn.Name = "_DZHConn";
            this._DZHConn.Size = new System.Drawing.Size(121, 21);
            this._DZHConn.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 218);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 26);
            this.label7.TabIndex = 6;
            this.label7.Text = "tуров3, мс \r\n(пит. присоед.)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 147);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 26);
            this.label8.TabIndex = 5;
            this.label8.Text = "tуров2, мс\r\n(на смеж.)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 78);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 26);
            this.label9.TabIndex = 4;
            this.label9.Text = "tуров1, мс \r\n(на себя)";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(16, 22);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(68, 26);
            this.label39.TabIndex = 1;
            this.label39.Text = "Пуск УРОВ \r\nСШ от пр.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "На себя";
            // 
            // _DZHKontr
            // 
            this._DZHKontr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHKontr.FormattingEnabled = true;
            this._DZHKontr.Location = new System.Drawing.Point(109, 10);
            this._DZHKontr.Name = "_DZHKontr";
            this._DZHKontr.Size = new System.Drawing.Size(121, 21);
            this._DZHKontr.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Кон. прис.";
            // 
            // _relayIndPage
            // 
            this._relayIndPage.Controls.Add(this._confIndGB);
            this._relayIndPage.Controls.Add(this.groupBox175);
            this._relayIndPage.Controls.Add(this.groupBox35);
            this._relayIndPage.Controls.Add(this.groupBox26);
            this._relayIndPage.Controls.Add(this.groupBox12);
            this._relayIndPage.Location = new System.Drawing.Point(4, 22);
            this._relayIndPage.Name = "_relayIndPage";
            this._relayIndPage.Padding = new System.Windows.Forms.Padding(3);
            this._relayIndPage.Size = new System.Drawing.Size(1024, 553);
            this._relayIndPage.TabIndex = 12;
            this._relayIndPage.Text = "Выходные сигналы";
            this._relayIndPage.UseVisualStyleBackColor = true;
            // 
            // _confIndGB
            // 
            this._confIndGB.Controls.Add(this._configIgSshComboBox);
            this._confIndGB.Controls.Add(this.label40);
            this._confIndGB.Location = new System.Drawing.Point(772, 419);
            this._confIndGB.Name = "_confIndGB";
            this._confIndGB.Size = new System.Drawing.Size(184, 43);
            this._confIndGB.TabIndex = 26;
            this._confIndGB.TabStop = false;
            this._confIndGB.Text = "Конфигурация индикатора";
            // 
            // _configIgSshComboBox
            // 
            this._configIgSshComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._configIgSshComboBox.FormattingEnabled = true;
            this._configIgSshComboBox.Location = new System.Drawing.Point(85, 16);
            this._configIgSshComboBox.Name = "_configIgSshComboBox";
            this._configIgSshComboBox.Size = new System.Drawing.Size(90, 21);
            this._configIgSshComboBox.TabIndex = 24;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(9, 19);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(70, 13);
            this.label40.TabIndex = 23;
            this.label40.Text = "Инд. Iд2СШ2";
            // 
            // groupBox175
            // 
            this.groupBox175.Controls.Add(this._outputIndicatorsGrid);
            this.groupBox175.Location = new System.Drawing.Point(404, 6);
            this.groupBox175.Name = "groupBox175";
            this.groupBox175.Size = new System.Drawing.Size(614, 330);
            this.groupBox175.TabIndex = 24;
            this.groupBox175.TabStop = false;
            this.groupBox175.Text = "Индикаторы";
            // 
            // _outputIndicatorsGrid
            // 
            this._outputIndicatorsGrid.AllowUserToAddRows = false;
            this._outputIndicatorsGrid.AllowUserToDeleteRows = false;
            this._outputIndicatorsGrid.AllowUserToResizeColumns = false;
            this._outputIndicatorsGrid.AllowUserToResizeRows = false;
            this._outputIndicatorsGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputIndicatorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputIndicatorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._outIndNumberCol,
            this._outIndTypeCol,
            this.baseGreenColumn,
            this._outIndSignalCol,
            this.baseRedColumn,
            this._out1IndSignalCol,
            this._outIndSignal2Col});
            this._outputIndicatorsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._outputIndicatorsGrid.Location = new System.Drawing.Point(3, 16);
            this._outputIndicatorsGrid.Name = "_outputIndicatorsGrid";
            this._outputIndicatorsGrid.RowHeadersVisible = false;
            this._outputIndicatorsGrid.RowHeadersWidth = 51;
            this._outputIndicatorsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle82.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._outputIndicatorsGrid.RowsDefaultCellStyle = dataGridViewCellStyle82;
            this._outputIndicatorsGrid.RowTemplate.Height = 24;
            this._outputIndicatorsGrid.ShowCellErrors = false;
            this._outputIndicatorsGrid.ShowRowErrors = false;
            this._outputIndicatorsGrid.Size = new System.Drawing.Size(608, 311);
            this._outputIndicatorsGrid.TabIndex = 0;
            this._outputIndicatorsGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataErrorGrid);
            // 
            // _outIndNumberCol
            // 
            this._outIndNumberCol.HeaderText = "№";
            this._outIndNumberCol.MinimumWidth = 6;
            this._outIndNumberCol.Name = "_outIndNumberCol";
            this._outIndNumberCol.ReadOnly = true;
            this._outIndNumberCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndNumberCol.Width = 25;
            // 
            // _outIndTypeCol
            // 
            this._outIndTypeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._outIndTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndTypeCol.HeaderText = "Тип";
            this._outIndTypeCol.MinimumWidth = 6;
            this._outIndTypeCol.Name = "_outIndTypeCol";
            this._outIndTypeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndTypeCol.Width = 32;
            // 
            // baseGreenColumn
            // 
            this.baseGreenColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.baseGreenColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.baseGreenColumn.HeaderText = "База";
            this.baseGreenColumn.MinimumWidth = 6;
            this.baseGreenColumn.Name = "baseGreenColumn";
            this.baseGreenColumn.Width = 38;
            // 
            // _outIndSignalCol
            // 
            this._outIndSignalCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._outIndSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndSignalCol.HeaderText = "Сигнал \"зеленый\"";
            this._outIndSignalCol.MinimumWidth = 6;
            this._outIndSignalCol.Name = "_outIndSignalCol";
            this._outIndSignalCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndSignalCol.Width = 95;
            // 
            // baseRedColumn
            // 
            this.baseRedColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.baseRedColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.baseRedColumn.HeaderText = "База";
            this.baseRedColumn.MinimumWidth = 6;
            this.baseRedColumn.Name = "baseRedColumn";
            this.baseRedColumn.Width = 38;
            // 
            // _out1IndSignalCol
            // 
            this._out1IndSignalCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._out1IndSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._out1IndSignalCol.HeaderText = "Сигнал \"красный\"";
            this._out1IndSignalCol.MinimumWidth = 6;
            this._out1IndSignalCol.Name = "_out1IndSignalCol";
            this._out1IndSignalCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._out1IndSignalCol.Width = 95;
            // 
            // _outIndSignal2Col
            // 
            this._outIndSignal2Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._outIndSignal2Col.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndSignal2Col.HeaderText = "Режим работы";
            this._outIndSignal2Col.MinimumWidth = 6;
            this._outIndSignal2Col.Name = "_outIndSignal2Col";
            this._outIndSignal2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndSignal2Col.Width = 79;
            // 
            // groupBox35
            // 
            this.groupBox35.Controls.Add(this._resetAlarmCheckBox);
            this.groupBox35.Controls.Add(this._resetSystemCheckBox);
            this.groupBox35.Controls.Add(this.label110);
            this.groupBox35.Controls.Add(this.label6);
            this.groupBox35.Location = new System.Drawing.Point(772, 342);
            this.groupBox35.Name = "groupBox35";
            this.groupBox35.Size = new System.Drawing.Size(215, 71);
            this.groupBox35.TabIndex = 24;
            this.groupBox35.TabStop = false;
            this.groupBox35.Text = "Сброс индикаторов";
            // 
            // _resetAlarmCheckBox
            // 
            this._resetAlarmCheckBox.AutoSize = true;
            this._resetAlarmCheckBox.Location = new System.Drawing.Point(185, 46);
            this._resetAlarmCheckBox.Name = "_resetAlarmCheckBox";
            this._resetAlarmCheckBox.Size = new System.Drawing.Size(15, 14);
            this._resetAlarmCheckBox.TabIndex = 21;
            this._resetAlarmCheckBox.UseVisualStyleBackColor = true;
            // 
            // _resetSystemCheckBox
            // 
            this._resetSystemCheckBox.AutoSize = true;
            this._resetSystemCheckBox.Location = new System.Drawing.Point(185, 21);
            this._resetSystemCheckBox.Name = "_resetSystemCheckBox";
            this._resetSystemCheckBox.Size = new System.Drawing.Size(15, 14);
            this._resetSystemCheckBox.TabIndex = 19;
            this._resetSystemCheckBox.UseVisualStyleBackColor = true;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(6, 45);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(152, 13);
            this.label110.TabIndex = 20;
            this.label110.Text = "2. По входу в журнал аварий";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(161, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "1. По входу в журнал системы";
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this._fault5CheckBox);
            this.groupBox26.Controls.Add(this._fault5Label);
            this.groupBox26.Controls.Add(this._fault4CheckBox);
            this.groupBox26.Controls.Add(this._fault3CheckBox);
            this.groupBox26.Controls.Add(this._fault2CheckBox);
            this.groupBox26.Controls.Add(this._fault1CheckBox);
            this.groupBox26.Controls.Add(this.label1);
            this.groupBox26.Controls.Add(this.label84);
            this.groupBox26.Controls.Add(this.label83);
            this.groupBox26.Controls.Add(this.label82);
            this.groupBox26.Controls.Add(this.label81);
            this.groupBox26.Controls.Add(this._impTB);
            this.groupBox26.Location = new System.Drawing.Point(404, 342);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(362, 149);
            this.groupBox26.TabIndex = 9;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Реле неисправность";
            // 
            // _fault5CheckBox
            // 
            this._fault5CheckBox.AutoSize = true;
            this._fault5CheckBox.Location = new System.Drawing.Point(182, 100);
            this._fault5CheckBox.Name = "_fault5CheckBox";
            this._fault5CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault5CheckBox.TabIndex = 24;
            this._fault5CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault5Label
            // 
            this._fault5Label.AutoSize = true;
            this._fault5Label.Location = new System.Drawing.Point(6, 100);
            this._fault5Label.Name = "_fault5Label";
            this._fault5Label.Size = new System.Drawing.Size(56, 13);
            this._fault5Label.TabIndex = 23;
            this._fault5Label.Text = "5. Логики";
            // 
            // _fault4CheckBox
            // 
            this._fault4CheckBox.AutoSize = true;
            this._fault4CheckBox.Location = new System.Drawing.Point(182, 82);
            this._fault4CheckBox.Name = "_fault4CheckBox";
            this._fault4CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault4CheckBox.TabIndex = 22;
            this._fault4CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault3CheckBox
            // 
            this._fault3CheckBox.AutoSize = true;
            this._fault3CheckBox.Location = new System.Drawing.Point(182, 62);
            this._fault3CheckBox.Name = "_fault3CheckBox";
            this._fault3CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault3CheckBox.TabIndex = 21;
            this._fault3CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault2CheckBox
            // 
            this._fault2CheckBox.AutoSize = true;
            this._fault2CheckBox.Location = new System.Drawing.Point(182, 42);
            this._fault2CheckBox.Name = "_fault2CheckBox";
            this._fault2CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault2CheckBox.TabIndex = 20;
            this._fault2CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault1CheckBox
            // 
            this._fault1CheckBox.AutoSize = true;
            this._fault1CheckBox.Location = new System.Drawing.Point(182, 22);
            this._fault1CheckBox.Name = "_fault1CheckBox";
            this._fault1CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault1CheckBox.TabIndex = 19;
            this._fault1CheckBox.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "4. Отказ выключателя (УРОВ)";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(6, 122);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(64, 13);
            this.label84.TabIndex = 14;
            this.label84.Text = "Твозвр, мс";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(6, 62);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(148, 13);
            this.label83.TabIndex = 13;
            this.label83.Text = "3. Неисправность цепей ТТ";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(6, 42);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(170, 13);
            this.label82.TabIndex = 12;
            this.label82.Text = "2. Программная неисправность";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(6, 22);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(159, 13);
            this.label81.TabIndex = 11;
            this.label81.Text = "1. Аппаратная неисправность";
            // 
            // _impTB
            // 
            this._impTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._impTB.Location = new System.Drawing.Point(182, 120);
            this._impTB.Name = "_impTB";
            this._impTB.Size = new System.Drawing.Size(121, 20);
            this._impTB.TabIndex = 7;
            this._impTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._outputReleGrid);
            this.groupBox12.Location = new System.Drawing.Point(3, 6);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(398, 541);
            this.groupBox12.TabIndex = 7;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Выходные реле";
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this.baseColumn,
            this._releSignalCol,
            this._releWaitCol});
            this._outputReleGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._outputReleGrid.Location = new System.Drawing.Point(3, 16);
            this._outputReleGrid.Name = "_outputReleGrid";
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidth = 51;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle83.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle83;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(392, 522);
            this._outputReleGrid.TabIndex = 0;
            this._outputReleGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataErrorGrid);
            // 
            // _releNumberCol
            // 
            this._releNumberCol.HeaderText = "№";
            this._releNumberCol.MinimumWidth = 6;
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 25;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._releTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releTypeCol.HeaderText = "Тип";
            this._releTypeCol.MinimumWidth = 6;
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 32;
            // 
            // baseColumn
            // 
            this.baseColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.baseColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.baseColumn.HeaderText = "База";
            this.baseColumn.MinimumWidth = 6;
            this.baseColumn.Name = "baseColumn";
            this.baseColumn.Width = 38;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._releSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releSignalCol.HeaderText = "Сигнал";
            this._releSignalCol.MinimumWidth = 6;
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 49;
            // 
            // _releWaitCol
            // 
            this._releWaitCol.HeaderText = "Твозвр, мс";
            this._releWaitCol.MinimumWidth = 6;
            this._releWaitCol.Name = "_releWaitCol";
            this._releWaitCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releWaitCol.Width = 70;
            // 
            // tabPage23
            // 
            this.tabPage23.Controls.Add(this.groupBox55);
            this.tabPage23.Controls.Add(this.groupBox53);
            this.tabPage23.Location = new System.Drawing.Point(4, 22);
            this.tabPage23.Name = "tabPage23";
            this.tabPage23.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage23.Size = new System.Drawing.Size(1024, 553);
            this.tabPage23.TabIndex = 17;
            this.tabPage23.Text = "Логические элементы";
            this.tabPage23.UseVisualStyleBackColor = true;
            // 
            // groupBox55
            // 
            this.groupBox55.Controls.Add(this._virtualReleDataGrid);
            this.groupBox55.Location = new System.Drawing.Point(3, 3);
            this.groupBox55.Name = "groupBox55";
            this.groupBox55.Size = new System.Drawing.Size(419, 541);
            this.groupBox55.TabIndex = 24;
            this.groupBox55.TabStop = false;
            this.groupBox55.Text = "Виртуальные  реле";
            // 
            // _virtualReleDataGrid
            // 
            this._virtualReleDataGrid.AllowUserToAddRows = false;
            this._virtualReleDataGrid.AllowUserToDeleteRows = false;
            this._virtualReleDataGrid.AllowUserToResizeColumns = false;
            this._virtualReleDataGrid.AllowUserToResizeRows = false;
            this._virtualReleDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._virtualReleDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._virtualReleDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewComboBoxColumn23,
            this.dataGridViewComboBoxColumn24,
            this.dataGridViewComboBoxColumn25,
            this.dataGridViewTextBoxColumn24});
            this._virtualReleDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._virtualReleDataGrid.Location = new System.Drawing.Point(3, 16);
            this._virtualReleDataGrid.Name = "_virtualReleDataGrid";
            this._virtualReleDataGrid.RowHeadersVisible = false;
            this._virtualReleDataGrid.RowHeadersWidth = 51;
            this._virtualReleDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle84.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._virtualReleDataGrid.RowsDefaultCellStyle = dataGridViewCellStyle84;
            this._virtualReleDataGrid.RowTemplate.Height = 24;
            this._virtualReleDataGrid.ShowCellErrors = false;
            this._virtualReleDataGrid.ShowRowErrors = false;
            this._virtualReleDataGrid.Size = new System.Drawing.Size(413, 522);
            this._virtualReleDataGrid.TabIndex = 1;
            this._virtualReleDataGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataErrorGrid);
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.HeaderText = "№";
            this.dataGridViewTextBoxColumn17.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn17.Width = 25;
            // 
            // dataGridViewComboBoxColumn23
            // 
            this.dataGridViewComboBoxColumn23.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn23.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn23.HeaderText = "Тип";
            this.dataGridViewComboBoxColumn23.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn23.Name = "dataGridViewComboBoxColumn23";
            this.dataGridViewComboBoxColumn23.Width = 32;
            // 
            // dataGridViewComboBoxColumn24
            // 
            this.dataGridViewComboBoxColumn24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn24.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn24.HeaderText = "База";
            this.dataGridViewComboBoxColumn24.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn24.Name = "dataGridViewComboBoxColumn24";
            this.dataGridViewComboBoxColumn24.Width = 38;
            // 
            // dataGridViewComboBoxColumn25
            // 
            this.dataGridViewComboBoxColumn25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn25.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn25.HeaderText = "Сигнал";
            this.dataGridViewComboBoxColumn25.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn25.Name = "dataGridViewComboBoxColumn25";
            this.dataGridViewComboBoxColumn25.Width = 49;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.HeaderText = "Твозвр, мс";
            this.dataGridViewTextBoxColumn24.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn24.Width = 70;
            // 
            // groupBox53
            // 
            this.groupBox53.Controls.Add(this._rsTriggersDataGrid);
            this.groupBox53.Location = new System.Drawing.Point(441, 3);
            this.groupBox53.Name = "groupBox53";
            this.groupBox53.Size = new System.Drawing.Size(292, 427);
            this.groupBox53.TabIndex = 23;
            this.groupBox53.TabStop = false;
            this.groupBox53.Text = "Энергонезависимые RS-триггеры";
            // 
            // _rsTriggersDataGrid
            // 
            this._rsTriggersDataGrid.AllowUserToAddRows = false;
            this._rsTriggersDataGrid.AllowUserToDeleteRows = false;
            this._rsTriggersDataGrid.AllowUserToResizeColumns = false;
            this._rsTriggersDataGrid.AllowUserToResizeRows = false;
            this._rsTriggersDataGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle85.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle85.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle85.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle85.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle85.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle85.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle85.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._rsTriggersDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle85;
            this._rsTriggersDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._rsTriggersDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewComboBoxColumn65,
            this.dataGridViewComboBoxColumn66,
            this.dataGridViewComboBoxColumn67});
            this._rsTriggersDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._rsTriggersDataGrid.Location = new System.Drawing.Point(3, 16);
            this._rsTriggersDataGrid.Name = "_rsTriggersDataGrid";
            this._rsTriggersDataGrid.RowHeadersVisible = false;
            this._rsTriggersDataGrid.RowHeadersWidth = 51;
            this._rsTriggersDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle86.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._rsTriggersDataGrid.RowsDefaultCellStyle = dataGridViewCellStyle86;
            this._rsTriggersDataGrid.RowTemplate.Height = 24;
            this._rsTriggersDataGrid.ShowCellErrors = false;
            this._rsTriggersDataGrid.ShowRowErrors = false;
            this._rsTriggersDataGrid.Size = new System.Drawing.Size(286, 408);
            this._rsTriggersDataGrid.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn51
            // 
            this.dataGridViewTextBoxColumn51.HeaderText = "№";
            this.dataGridViewTextBoxColumn51.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.ReadOnly = true;
            this.dataGridViewTextBoxColumn51.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn51.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn51.Width = 25;
            // 
            // dataGridViewComboBoxColumn65
            // 
            this.dataGridViewComboBoxColumn65.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn65.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn65.HeaderText = "Тип";
            this.dataGridViewComboBoxColumn65.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn65.Name = "dataGridViewComboBoxColumn65";
            this.dataGridViewComboBoxColumn65.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn65.Width = 32;
            // 
            // dataGridViewComboBoxColumn66
            // 
            this.dataGridViewComboBoxColumn66.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn66.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn66.HeaderText = "Вход R";
            this.dataGridViewComboBoxColumn66.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn66.Name = "dataGridViewComboBoxColumn66";
            this.dataGridViewComboBoxColumn66.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn66.Width = 48;
            // 
            // dataGridViewComboBoxColumn67
            // 
            this.dataGridViewComboBoxColumn67.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn67.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn67.HeaderText = "Вход S";
            this.dataGridViewComboBoxColumn67.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn67.Name = "dataGridViewComboBoxColumn67";
            this.dataGridViewComboBoxColumn67.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn67.Width = 47;
            // 
            // _outputSignalsPage
            // 
            this._outputSignalsPage.Controls.Add(this.groupBox28);
            this._outputSignalsPage.Controls.Add(this.groupBox13);
            this._outputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._outputSignalsPage.Name = "_outputSignalsPage";
            this._outputSignalsPage.Size = new System.Drawing.Size(1024, 553);
            this._outputSignalsPage.TabIndex = 6;
            this._outputSignalsPage.Text = "ВЛС";
            this._outputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this._wrapBtn);
            this.groupBox28.Controls.Add(this.treeViewForVLS);
            this.groupBox28.Location = new System.Drawing.Point(423, 3);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(218, 546);
            this.groupBox28.TabIndex = 7;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Список всех ВЛС";
            // 
            // _wrapBtn
            // 
            this._wrapBtn.Location = new System.Drawing.Point(6, 517);
            this._wrapBtn.Name = "_wrapBtn";
            this._wrapBtn.Size = new System.Drawing.Size(206, 23);
            this._wrapBtn.TabIndex = 8;
            this._wrapBtn.Text = "Свернуть";
            this._wrapBtn.UseVisualStyleBackColor = true;
            this._wrapBtn.Click += new System.EventHandler(this._wrapBtn_Click);
            // 
            // treeViewForVLS
            // 
            this.treeViewForVLS.Location = new System.Drawing.Point(6, 19);
            this.treeViewForVLS.Name = "treeViewForVLS";
            this.treeViewForVLS.Size = new System.Drawing.Size(206, 495);
            this.treeViewForVLS.TabIndex = 6;
            this.treeViewForVLS.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView_NodeMouseClick);
            // 
            // _systemPage
            // 
            this._systemPage.Controls.Add(this.groupBox6);
            this._systemPage.Location = new System.Drawing.Point(4, 22);
            this._systemPage.Name = "_systemPage";
            this._systemPage.Size = new System.Drawing.Size(1024, 553);
            this._systemPage.TabIndex = 10;
            this._systemPage.Text = "Осциллограф";
            this._systemPage.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox27);
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Location = new System.Drawing.Point(8, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(625, 547);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Осцилограф";
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this._oscChannelsWithBaseGrid);
            this.groupBox27.Location = new System.Drawing.Point(291, 19);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(328, 522);
            this.groupBox27.TabIndex = 5;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Программируемые дискретные каналы";
            // 
            // _oscChannelsWithBaseGrid
            // 
            this._oscChannelsWithBaseGrid.AllowUserToAddRows = false;
            this._oscChannelsWithBaseGrid.AllowUserToDeleteRows = false;
            this._oscChannelsWithBaseGrid.AllowUserToResizeColumns = false;
            this._oscChannelsWithBaseGrid.AllowUserToResizeRows = false;
            this._oscChannelsWithBaseGrid.BackgroundColor = System.Drawing.Color.White;
            this._oscChannelsWithBaseGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._oscChannelsWithBaseGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn23,
            this._baseCol,
            this.dataGridViewComboBoxColumn22});
            this._oscChannelsWithBaseGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._oscChannelsWithBaseGrid.Location = new System.Drawing.Point(3, 16);
            this._oscChannelsWithBaseGrid.Name = "_oscChannelsWithBaseGrid";
            this._oscChannelsWithBaseGrid.RowHeadersVisible = false;
            this._oscChannelsWithBaseGrid.RowHeadersWidth = 51;
            this._oscChannelsWithBaseGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._oscChannelsWithBaseGrid.RowTemplate.Height = 24;
            this._oscChannelsWithBaseGrid.ShowCellErrors = false;
            this._oscChannelsWithBaseGrid.ShowRowErrors = false;
            this._oscChannelsWithBaseGrid.Size = new System.Drawing.Size(322, 503);
            this._oscChannelsWithBaseGrid.TabIndex = 27;
            this._oscChannelsWithBaseGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataErrorGrid);
            // 
            // dataGridViewTextBoxColumn23
            // 
            dataGridViewCellStyle87.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle87.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle87.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle87.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle87.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn23.DefaultCellStyle = dataGridViewCellStyle87;
            this.dataGridViewTextBoxColumn23.Frozen = true;
            this.dataGridViewTextBoxColumn23.HeaderText = "Канал";
            this.dataGridViewTextBoxColumn23.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn23.Width = 70;
            // 
            // _baseCol
            // 
            this._baseCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle88.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._baseCol.DefaultCellStyle = dataGridViewCellStyle88;
            this._baseCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._baseCol.HeaderText = "База";
            this._baseCol.MinimumWidth = 6;
            this._baseCol.Name = "_baseCol";
            this._baseCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._baseCol.Width = 38;
            // 
            // dataGridViewComboBoxColumn22
            // 
            this.dataGridViewComboBoxColumn22.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle89.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewComboBoxColumn22.DefaultCellStyle = dataGridViewCellStyle89;
            this.dataGridViewComboBoxColumn22.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn22.HeaderText = "Сигнал";
            this.dataGridViewComboBoxColumn22.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn22.Name = "dataGridViewComboBoxColumn22";
            this.dataGridViewComboBoxColumn22.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn22.Width = 49;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._poCheckBox);
            this.groupBox7.Controls.Add(this._sh2CheckBox);
            this.groupBox7.Controls.Add(this._sh1CheckBox);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Controls.Add(this._oscLength);
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Controls.Add(this._inpOscComboBox);
            this.groupBox7.Controls.Add(this.label20);
            this.groupBox7.Controls.Add(this.label10);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this._oscSizeTextBox);
            this.groupBox7.Controls.Add(this._oscFix);
            this.groupBox7.Controls.Add(this._oscWriteLength);
            this.groupBox7.Location = new System.Drawing.Point(6, 19);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(279, 138);
            this.groupBox7.TabIndex = 34;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Параметры осциллографа";
            // 
            // _poCheckBox
            // 
            this._poCheckBox.AutoSize = true;
            this._poCheckBox.Location = new System.Drawing.Point(125, 86);
            this._poCheckBox.Name = "_poCheckBox";
            this._poCheckBox.Size = new System.Drawing.Size(42, 17);
            this._poCheckBox.TabIndex = 35;
            this._poCheckBox.Text = "ПО";
            this._poCheckBox.UseVisualStyleBackColor = true;
            this._poCheckBox.CheckedChanged += new System.EventHandler(this._oscLength_Changed);
            // 
            // _sh2CheckBox
            // 
            this._sh2CheckBox.AutoSize = true;
            this._sh2CheckBox.Location = new System.Drawing.Point(71, 86);
            this._sh2CheckBox.Name = "_sh2CheckBox";
            this._sh2CheckBox.Size = new System.Drawing.Size(48, 17);
            this._sh2CheckBox.TabIndex = 34;
            this._sh2CheckBox.Text = "СШ2";
            this._sh2CheckBox.UseVisualStyleBackColor = true;
            this._sh2CheckBox.CheckedChanged += new System.EventHandler(this._oscLength_Changed);
            // 
            // _sh1CheckBox
            // 
            this._sh1CheckBox.AutoSize = true;
            this._sh1CheckBox.Location = new System.Drawing.Point(17, 86);
            this._sh1CheckBox.Name = "_sh1CheckBox";
            this._sh1CheckBox.Size = new System.Drawing.Size(48, 17);
            this._sh1CheckBox.TabIndex = 33;
            this._sh1CheckBox.Text = "СШ1";
            this._sh1CheckBox.UseVisualStyleBackColor = true;
            this._sh1CheckBox.CheckedChanged += new System.EventHandler(this._oscLength_Changed);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(249, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(21, 13);
            this.label16.TabIndex = 31;
            this.label16.Text = "мс";
            // 
            // _oscLength
            // 
            this._oscLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscLength.FormattingEnabled = true;
            this._oscLength.Location = new System.Drawing.Point(125, 18);
            this._oscLength.Name = "_oscLength";
            this._oscLength.Size = new System.Drawing.Size(44, 21);
            this._oscLength.TabIndex = 12;
            this._oscLength.SelectedIndexChanged += new System.EventHandler(this._oscLength_Changed);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 21);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(66, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Количество";
            // 
            // _inpOscComboBox
            // 
            this._inpOscComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inpOscComboBox.FormattingEnabled = true;
            this._inpOscComboBox.Location = new System.Drawing.Point(125, 107);
            this._inpOscComboBox.Name = "_inpOscComboBox";
            this._inpOscComboBox.Size = new System.Drawing.Size(121, 21);
            this._inpOscComboBox.TabIndex = 32;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 41);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(113, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "Длит. предзаписи, %";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 110);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 13);
            this.label10.TabIndex = 31;
            this.label10.Text = "Вход пуска";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 62);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(66, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "Фиксац. по";
            // 
            // _oscSizeTextBox
            // 
            this._oscSizeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._oscSizeTextBox.Location = new System.Drawing.Point(175, 19);
            this._oscSizeTextBox.Name = "_oscSizeTextBox";
            this._oscSizeTextBox.ReadOnly = true;
            this._oscSizeTextBox.Size = new System.Drawing.Size(71, 20);
            this._oscSizeTextBox.TabIndex = 30;
            this._oscSizeTextBox.Tag = "3000000";
            this._oscSizeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _oscFix
            // 
            this._oscFix.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscFix.FormattingEnabled = true;
            this._oscFix.Location = new System.Drawing.Point(125, 59);
            this._oscFix.Name = "_oscFix";
            this._oscFix.Size = new System.Drawing.Size(121, 21);
            this._oscFix.TabIndex = 13;
            // 
            // _oscWriteLength
            // 
            this._oscWriteLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._oscWriteLength.Location = new System.Drawing.Point(125, 39);
            this._oscWriteLength.Name = "_oscWriteLength";
            this._oscWriteLength.Size = new System.Drawing.Size(121, 20);
            this._oscWriteLength.TabIndex = 23;
            this._oscWriteLength.Tag = "3000000";
            this._oscWriteLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tabPage17
            // 
            this.tabPage17.Controls.Add(this.groupBox29);
            this.tabPage17.Controls.Add(this.label36);
            this.tabPage17.Controls.Add(this.currentBGS);
            this.tabPage17.Location = new System.Drawing.Point(4, 22);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage17.Size = new System.Drawing.Size(1024, 553);
            this.tabPage17.TabIndex = 15;
            this.tabPage17.Text = "БГС";
            this.tabPage17.UseVisualStyleBackColor = true;
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.groupBox30);
            this.groupBox29.Controls.Add(this.label345);
            this.groupBox29.Controls.Add(this.operationBGS);
            this.groupBox29.Location = new System.Drawing.Point(6, 33);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(682, 511);
            this.groupBox29.TabIndex = 12;
            this.groupBox29.TabStop = false;
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.goin64);
            this.groupBox30.Controls.Add(this.goin48);
            this.groupBox30.Controls.Add(this.goin32);
            this.groupBox30.Controls.Add(this.label281);
            this.groupBox30.Controls.Add(this.label282);
            this.groupBox30.Controls.Add(this.label283);
            this.groupBox30.Controls.Add(this.goin63);
            this.groupBox30.Controls.Add(this.goin47);
            this.groupBox30.Controls.Add(this.goin31);
            this.groupBox30.Controls.Add(this.label284);
            this.groupBox30.Controls.Add(this.label285);
            this.groupBox30.Controls.Add(this.label286);
            this.groupBox30.Controls.Add(this.goin62);
            this.groupBox30.Controls.Add(this.goin46);
            this.groupBox30.Controls.Add(this.goin30);
            this.groupBox30.Controls.Add(this.label287);
            this.groupBox30.Controls.Add(this.label288);
            this.groupBox30.Controls.Add(this.label289);
            this.groupBox30.Controls.Add(this.goin61);
            this.groupBox30.Controls.Add(this.goin45);
            this.groupBox30.Controls.Add(this.goin29);
            this.groupBox30.Controls.Add(this.label290);
            this.groupBox30.Controls.Add(this.label291);
            this.groupBox30.Controls.Add(this.label292);
            this.groupBox30.Controls.Add(this.goin60);
            this.groupBox30.Controls.Add(this.goin44);
            this.groupBox30.Controls.Add(this.goin28);
            this.groupBox30.Controls.Add(this.label293);
            this.groupBox30.Controls.Add(this.label294);
            this.groupBox30.Controls.Add(this.label295);
            this.groupBox30.Controls.Add(this.goin59);
            this.groupBox30.Controls.Add(this.goin43);
            this.groupBox30.Controls.Add(this.goin27);
            this.groupBox30.Controls.Add(this.label296);
            this.groupBox30.Controls.Add(this.label297);
            this.groupBox30.Controls.Add(this.label298);
            this.groupBox30.Controls.Add(this.goin58);
            this.groupBox30.Controls.Add(this.goin42);
            this.groupBox30.Controls.Add(this.goin26);
            this.groupBox30.Controls.Add(this.label299);
            this.groupBox30.Controls.Add(this.label300);
            this.groupBox30.Controls.Add(this.label301);
            this.groupBox30.Controls.Add(this.goin57);
            this.groupBox30.Controls.Add(this.goin41);
            this.groupBox30.Controls.Add(this.goin25);
            this.groupBox30.Controls.Add(this.label302);
            this.groupBox30.Controls.Add(this.label303);
            this.groupBox30.Controls.Add(this.label304);
            this.groupBox30.Controls.Add(this.goin56);
            this.groupBox30.Controls.Add(this.goin40);
            this.groupBox30.Controls.Add(this.goin24);
            this.groupBox30.Controls.Add(this.label305);
            this.groupBox30.Controls.Add(this.label306);
            this.groupBox30.Controls.Add(this.label307);
            this.groupBox30.Controls.Add(this.goin55);
            this.groupBox30.Controls.Add(this.goin39);
            this.groupBox30.Controls.Add(this.goin23);
            this.groupBox30.Controls.Add(this.label308);
            this.groupBox30.Controls.Add(this.label309);
            this.groupBox30.Controls.Add(this.label310);
            this.groupBox30.Controls.Add(this.goin54);
            this.groupBox30.Controls.Add(this.goin38);
            this.groupBox30.Controls.Add(this.goin22);
            this.groupBox30.Controls.Add(this.label311);
            this.groupBox30.Controls.Add(this.label312);
            this.groupBox30.Controls.Add(this.label313);
            this.groupBox30.Controls.Add(this.goin53);
            this.groupBox30.Controls.Add(this.goin37);
            this.groupBox30.Controls.Add(this.goin21);
            this.groupBox30.Controls.Add(this.label314);
            this.groupBox30.Controls.Add(this.label315);
            this.groupBox30.Controls.Add(this.label316);
            this.groupBox30.Controls.Add(this.goin52);
            this.groupBox30.Controls.Add(this.goin36);
            this.groupBox30.Controls.Add(this.goin20);
            this.groupBox30.Controls.Add(this.label317);
            this.groupBox30.Controls.Add(this.label318);
            this.groupBox30.Controls.Add(this.label319);
            this.groupBox30.Controls.Add(this.goin51);
            this.groupBox30.Controls.Add(this.goin35);
            this.groupBox30.Controls.Add(this.goin19);
            this.groupBox30.Controls.Add(this.label320);
            this.groupBox30.Controls.Add(this.label321);
            this.groupBox30.Controls.Add(this.label322);
            this.groupBox30.Controls.Add(this.goin50);
            this.groupBox30.Controls.Add(this.goin34);
            this.groupBox30.Controls.Add(this.goin18);
            this.groupBox30.Controls.Add(this.label323);
            this.groupBox30.Controls.Add(this.label324);
            this.groupBox30.Controls.Add(this.label325);
            this.groupBox30.Controls.Add(this.goin49);
            this.groupBox30.Controls.Add(this.label326);
            this.groupBox30.Controls.Add(this.goin33);
            this.groupBox30.Controls.Add(this.label327);
            this.groupBox30.Controls.Add(this.goin17);
            this.groupBox30.Controls.Add(this.label328);
            this.groupBox30.Controls.Add(this.goin16);
            this.groupBox30.Controls.Add(this.label329);
            this.groupBox30.Controls.Add(this.goin15);
            this.groupBox30.Controls.Add(this.label330);
            this.groupBox30.Controls.Add(this.goin14);
            this.groupBox30.Controls.Add(this.label331);
            this.groupBox30.Controls.Add(this.goin13);
            this.groupBox30.Controls.Add(this.label332);
            this.groupBox30.Controls.Add(this.goin12);
            this.groupBox30.Controls.Add(this.label333);
            this.groupBox30.Controls.Add(this.goin11);
            this.groupBox30.Controls.Add(this.label334);
            this.groupBox30.Controls.Add(this.goin10);
            this.groupBox30.Controls.Add(this.label335);
            this.groupBox30.Controls.Add(this.goin9);
            this.groupBox30.Controls.Add(this.label336);
            this.groupBox30.Controls.Add(this.goin8);
            this.groupBox30.Controls.Add(this.label337);
            this.groupBox30.Controls.Add(this.goin7);
            this.groupBox30.Controls.Add(this.label338);
            this.groupBox30.Controls.Add(this.goin6);
            this.groupBox30.Controls.Add(this.label339);
            this.groupBox30.Controls.Add(this.goin5);
            this.groupBox30.Controls.Add(this.label340);
            this.groupBox30.Controls.Add(this.goin4);
            this.groupBox30.Controls.Add(this.label341);
            this.groupBox30.Controls.Add(this.goin3);
            this.groupBox30.Controls.Add(this.label342);
            this.groupBox30.Controls.Add(this.goin2);
            this.groupBox30.Controls.Add(this.label343);
            this.groupBox30.Controls.Add(this.goin1);
            this.groupBox30.Controls.Add(this.label344);
            this.groupBox30.Location = new System.Drawing.Point(6, 50);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(645, 455);
            this.groupBox30.TabIndex = 8;
            this.groupBox30.TabStop = false;
            // 
            // goin64
            // 
            this.goin64.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin64.FormattingEnabled = true;
            this.goin64.Location = new System.Drawing.Point(553, 424);
            this.goin64.Name = "goin64";
            this.goin64.Size = new System.Drawing.Size(75, 21);
            this.goin64.TabIndex = 33;
            // 
            // goin48
            // 
            this.goin48.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin48.FormattingEnabled = true;
            this.goin48.Location = new System.Drawing.Point(384, 424);
            this.goin48.Name = "goin48";
            this.goin48.Size = new System.Drawing.Size(75, 21);
            this.goin48.TabIndex = 33;
            // 
            // goin32
            // 
            this.goin32.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin32.FormattingEnabled = true;
            this.goin32.Location = new System.Drawing.Point(217, 424);
            this.goin32.Name = "goin32";
            this.goin32.Size = new System.Drawing.Size(75, 21);
            this.goin32.TabIndex = 33;
            // 
            // label281
            // 
            this.label281.AutoSize = true;
            this.label281.Location = new System.Drawing.Point(505, 427);
            this.label281.Name = "label281";
            this.label281.Size = new System.Drawing.Size(42, 13);
            this.label281.TabIndex = 14;
            this.label281.Text = "GoIn64";
            // 
            // label282
            // 
            this.label282.AutoSize = true;
            this.label282.Location = new System.Drawing.Point(336, 427);
            this.label282.Name = "label282";
            this.label282.Size = new System.Drawing.Size(42, 13);
            this.label282.TabIndex = 14;
            this.label282.Text = "GoIn48";
            // 
            // label283
            // 
            this.label283.AutoSize = true;
            this.label283.Location = new System.Drawing.Point(169, 427);
            this.label283.Name = "label283";
            this.label283.Size = new System.Drawing.Size(42, 13);
            this.label283.TabIndex = 14;
            this.label283.Text = "GoIn32";
            // 
            // goin63
            // 
            this.goin63.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin63.FormattingEnabled = true;
            this.goin63.Location = new System.Drawing.Point(553, 397);
            this.goin63.Name = "goin63";
            this.goin63.Size = new System.Drawing.Size(75, 21);
            this.goin63.TabIndex = 29;
            // 
            // goin47
            // 
            this.goin47.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin47.FormattingEnabled = true;
            this.goin47.Location = new System.Drawing.Point(384, 397);
            this.goin47.Name = "goin47";
            this.goin47.Size = new System.Drawing.Size(75, 21);
            this.goin47.TabIndex = 29;
            // 
            // goin31
            // 
            this.goin31.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin31.FormattingEnabled = true;
            this.goin31.Location = new System.Drawing.Point(217, 397);
            this.goin31.Name = "goin31";
            this.goin31.Size = new System.Drawing.Size(75, 21);
            this.goin31.TabIndex = 29;
            // 
            // label284
            // 
            this.label284.AutoSize = true;
            this.label284.Location = new System.Drawing.Point(505, 400);
            this.label284.Name = "label284";
            this.label284.Size = new System.Drawing.Size(42, 13);
            this.label284.TabIndex = 15;
            this.label284.Text = "GoIn63";
            // 
            // label285
            // 
            this.label285.AutoSize = true;
            this.label285.Location = new System.Drawing.Point(336, 400);
            this.label285.Name = "label285";
            this.label285.Size = new System.Drawing.Size(42, 13);
            this.label285.TabIndex = 15;
            this.label285.Text = "GoIn47";
            // 
            // label286
            // 
            this.label286.AutoSize = true;
            this.label286.Location = new System.Drawing.Point(169, 400);
            this.label286.Name = "label286";
            this.label286.Size = new System.Drawing.Size(42, 13);
            this.label286.TabIndex = 15;
            this.label286.Text = "GoIn31";
            // 
            // goin62
            // 
            this.goin62.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin62.FormattingEnabled = true;
            this.goin62.Location = new System.Drawing.Point(553, 370);
            this.goin62.Name = "goin62";
            this.goin62.Size = new System.Drawing.Size(75, 21);
            this.goin62.TabIndex = 25;
            // 
            // goin46
            // 
            this.goin46.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin46.FormattingEnabled = true;
            this.goin46.Location = new System.Drawing.Point(384, 370);
            this.goin46.Name = "goin46";
            this.goin46.Size = new System.Drawing.Size(75, 21);
            this.goin46.TabIndex = 25;
            // 
            // goin30
            // 
            this.goin30.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin30.FormattingEnabled = true;
            this.goin30.Location = new System.Drawing.Point(217, 370);
            this.goin30.Name = "goin30";
            this.goin30.Size = new System.Drawing.Size(75, 21);
            this.goin30.TabIndex = 25;
            // 
            // label287
            // 
            this.label287.AutoSize = true;
            this.label287.Location = new System.Drawing.Point(505, 373);
            this.label287.Name = "label287";
            this.label287.Size = new System.Drawing.Size(42, 13);
            this.label287.TabIndex = 17;
            this.label287.Text = "GoIn62";
            // 
            // label288
            // 
            this.label288.AutoSize = true;
            this.label288.Location = new System.Drawing.Point(336, 373);
            this.label288.Name = "label288";
            this.label288.Size = new System.Drawing.Size(42, 13);
            this.label288.TabIndex = 17;
            this.label288.Text = "GoIn46";
            // 
            // label289
            // 
            this.label289.AutoSize = true;
            this.label289.Location = new System.Drawing.Point(169, 373);
            this.label289.Name = "label289";
            this.label289.Size = new System.Drawing.Size(42, 13);
            this.label289.TabIndex = 17;
            this.label289.Text = "GoIn30";
            // 
            // goin61
            // 
            this.goin61.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin61.FormattingEnabled = true;
            this.goin61.Location = new System.Drawing.Point(553, 343);
            this.goin61.Name = "goin61";
            this.goin61.Size = new System.Drawing.Size(75, 21);
            this.goin61.TabIndex = 21;
            // 
            // goin45
            // 
            this.goin45.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin45.FormattingEnabled = true;
            this.goin45.Location = new System.Drawing.Point(384, 343);
            this.goin45.Name = "goin45";
            this.goin45.Size = new System.Drawing.Size(75, 21);
            this.goin45.TabIndex = 21;
            // 
            // goin29
            // 
            this.goin29.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin29.FormattingEnabled = true;
            this.goin29.Location = new System.Drawing.Point(217, 343);
            this.goin29.Name = "goin29";
            this.goin29.Size = new System.Drawing.Size(75, 21);
            this.goin29.TabIndex = 21;
            // 
            // label290
            // 
            this.label290.AutoSize = true;
            this.label290.Location = new System.Drawing.Point(505, 346);
            this.label290.Name = "label290";
            this.label290.Size = new System.Drawing.Size(42, 13);
            this.label290.TabIndex = 16;
            this.label290.Text = "GoIn61";
            // 
            // label291
            // 
            this.label291.AutoSize = true;
            this.label291.Location = new System.Drawing.Point(336, 346);
            this.label291.Name = "label291";
            this.label291.Size = new System.Drawing.Size(42, 13);
            this.label291.TabIndex = 16;
            this.label291.Text = "GoIn45";
            // 
            // label292
            // 
            this.label292.AutoSize = true;
            this.label292.Location = new System.Drawing.Point(169, 346);
            this.label292.Name = "label292";
            this.label292.Size = new System.Drawing.Size(42, 13);
            this.label292.TabIndex = 16;
            this.label292.Text = "GoIn29";
            // 
            // goin60
            // 
            this.goin60.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin60.FormattingEnabled = true;
            this.goin60.Location = new System.Drawing.Point(553, 316);
            this.goin60.Name = "goin60";
            this.goin60.Size = new System.Drawing.Size(75, 21);
            this.goin60.TabIndex = 27;
            // 
            // goin44
            // 
            this.goin44.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin44.FormattingEnabled = true;
            this.goin44.Location = new System.Drawing.Point(384, 316);
            this.goin44.Name = "goin44";
            this.goin44.Size = new System.Drawing.Size(75, 21);
            this.goin44.TabIndex = 27;
            // 
            // goin28
            // 
            this.goin28.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin28.FormattingEnabled = true;
            this.goin28.Location = new System.Drawing.Point(217, 316);
            this.goin28.Name = "goin28";
            this.goin28.Size = new System.Drawing.Size(75, 21);
            this.goin28.TabIndex = 27;
            // 
            // label293
            // 
            this.label293.AutoSize = true;
            this.label293.Location = new System.Drawing.Point(505, 319);
            this.label293.Name = "label293";
            this.label293.Size = new System.Drawing.Size(42, 13);
            this.label293.TabIndex = 13;
            this.label293.Text = "GoIn60";
            // 
            // label294
            // 
            this.label294.AutoSize = true;
            this.label294.Location = new System.Drawing.Point(336, 319);
            this.label294.Name = "label294";
            this.label294.Size = new System.Drawing.Size(42, 13);
            this.label294.TabIndex = 13;
            this.label294.Text = "GoIn44";
            // 
            // label295
            // 
            this.label295.AutoSize = true;
            this.label295.Location = new System.Drawing.Point(169, 319);
            this.label295.Name = "label295";
            this.label295.Size = new System.Drawing.Size(42, 13);
            this.label295.TabIndex = 13;
            this.label295.Text = "GoIn28";
            // 
            // goin59
            // 
            this.goin59.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin59.FormattingEnabled = true;
            this.goin59.Location = new System.Drawing.Point(553, 289);
            this.goin59.Name = "goin59";
            this.goin59.Size = new System.Drawing.Size(75, 21);
            this.goin59.TabIndex = 23;
            // 
            // goin43
            // 
            this.goin43.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin43.FormattingEnabled = true;
            this.goin43.Location = new System.Drawing.Point(384, 289);
            this.goin43.Name = "goin43";
            this.goin43.Size = new System.Drawing.Size(75, 21);
            this.goin43.TabIndex = 23;
            // 
            // goin27
            // 
            this.goin27.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin27.FormattingEnabled = true;
            this.goin27.Location = new System.Drawing.Point(217, 289);
            this.goin27.Name = "goin27";
            this.goin27.Size = new System.Drawing.Size(75, 21);
            this.goin27.TabIndex = 23;
            // 
            // label296
            // 
            this.label296.AutoSize = true;
            this.label296.Location = new System.Drawing.Point(505, 292);
            this.label296.Name = "label296";
            this.label296.Size = new System.Drawing.Size(42, 13);
            this.label296.TabIndex = 11;
            this.label296.Text = "GoIn59";
            // 
            // label297
            // 
            this.label297.AutoSize = true;
            this.label297.Location = new System.Drawing.Point(336, 292);
            this.label297.Name = "label297";
            this.label297.Size = new System.Drawing.Size(42, 13);
            this.label297.TabIndex = 11;
            this.label297.Text = "GoIn43";
            // 
            // label298
            // 
            this.label298.AutoSize = true;
            this.label298.Location = new System.Drawing.Point(169, 292);
            this.label298.Name = "label298";
            this.label298.Size = new System.Drawing.Size(42, 13);
            this.label298.TabIndex = 11;
            this.label298.Text = "GoIn27";
            // 
            // goin58
            // 
            this.goin58.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin58.FormattingEnabled = true;
            this.goin58.Location = new System.Drawing.Point(553, 262);
            this.goin58.Name = "goin58";
            this.goin58.Size = new System.Drawing.Size(75, 21);
            this.goin58.TabIndex = 31;
            // 
            // goin42
            // 
            this.goin42.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin42.FormattingEnabled = true;
            this.goin42.Location = new System.Drawing.Point(384, 262);
            this.goin42.Name = "goin42";
            this.goin42.Size = new System.Drawing.Size(75, 21);
            this.goin42.TabIndex = 31;
            // 
            // goin26
            // 
            this.goin26.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin26.FormattingEnabled = true;
            this.goin26.Location = new System.Drawing.Point(217, 262);
            this.goin26.Name = "goin26";
            this.goin26.Size = new System.Drawing.Size(75, 21);
            this.goin26.TabIndex = 31;
            // 
            // label299
            // 
            this.label299.AutoSize = true;
            this.label299.Location = new System.Drawing.Point(505, 265);
            this.label299.Name = "label299";
            this.label299.Size = new System.Drawing.Size(42, 13);
            this.label299.TabIndex = 10;
            this.label299.Text = "GoIn58";
            // 
            // label300
            // 
            this.label300.AutoSize = true;
            this.label300.Location = new System.Drawing.Point(336, 265);
            this.label300.Name = "label300";
            this.label300.Size = new System.Drawing.Size(42, 13);
            this.label300.TabIndex = 10;
            this.label300.Text = "GoIn42";
            // 
            // label301
            // 
            this.label301.AutoSize = true;
            this.label301.Location = new System.Drawing.Point(169, 265);
            this.label301.Name = "label301";
            this.label301.Size = new System.Drawing.Size(42, 13);
            this.label301.TabIndex = 10;
            this.label301.Text = "GoIn26";
            // 
            // goin57
            // 
            this.goin57.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin57.FormattingEnabled = true;
            this.goin57.Location = new System.Drawing.Point(553, 235);
            this.goin57.Name = "goin57";
            this.goin57.Size = new System.Drawing.Size(75, 21);
            this.goin57.TabIndex = 19;
            // 
            // goin41
            // 
            this.goin41.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin41.FormattingEnabled = true;
            this.goin41.Location = new System.Drawing.Point(384, 235);
            this.goin41.Name = "goin41";
            this.goin41.Size = new System.Drawing.Size(75, 21);
            this.goin41.TabIndex = 19;
            // 
            // goin25
            // 
            this.goin25.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin25.FormattingEnabled = true;
            this.goin25.Location = new System.Drawing.Point(217, 235);
            this.goin25.Name = "goin25";
            this.goin25.Size = new System.Drawing.Size(75, 21);
            this.goin25.TabIndex = 19;
            // 
            // label302
            // 
            this.label302.AutoSize = true;
            this.label302.Location = new System.Drawing.Point(505, 238);
            this.label302.Name = "label302";
            this.label302.Size = new System.Drawing.Size(42, 13);
            this.label302.TabIndex = 2;
            this.label302.Text = "GoIn57";
            // 
            // label303
            // 
            this.label303.AutoSize = true;
            this.label303.Location = new System.Drawing.Point(336, 238);
            this.label303.Name = "label303";
            this.label303.Size = new System.Drawing.Size(42, 13);
            this.label303.TabIndex = 2;
            this.label303.Text = "GoIn41";
            // 
            // label304
            // 
            this.label304.AutoSize = true;
            this.label304.Location = new System.Drawing.Point(169, 238);
            this.label304.Name = "label304";
            this.label304.Size = new System.Drawing.Size(42, 13);
            this.label304.TabIndex = 2;
            this.label304.Text = "GoIn25";
            // 
            // goin56
            // 
            this.goin56.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin56.FormattingEnabled = true;
            this.goin56.Location = new System.Drawing.Point(553, 208);
            this.goin56.Name = "goin56";
            this.goin56.Size = new System.Drawing.Size(75, 21);
            this.goin56.TabIndex = 18;
            // 
            // goin40
            // 
            this.goin40.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin40.FormattingEnabled = true;
            this.goin40.Location = new System.Drawing.Point(384, 208);
            this.goin40.Name = "goin40";
            this.goin40.Size = new System.Drawing.Size(75, 21);
            this.goin40.TabIndex = 18;
            // 
            // goin24
            // 
            this.goin24.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin24.FormattingEnabled = true;
            this.goin24.Location = new System.Drawing.Point(217, 208);
            this.goin24.Name = "goin24";
            this.goin24.Size = new System.Drawing.Size(75, 21);
            this.goin24.TabIndex = 18;
            // 
            // label305
            // 
            this.label305.AutoSize = true;
            this.label305.Location = new System.Drawing.Point(505, 211);
            this.label305.Name = "label305";
            this.label305.Size = new System.Drawing.Size(42, 13);
            this.label305.TabIndex = 9;
            this.label305.Text = "GoIn56";
            // 
            // label306
            // 
            this.label306.AutoSize = true;
            this.label306.Location = new System.Drawing.Point(336, 211);
            this.label306.Name = "label306";
            this.label306.Size = new System.Drawing.Size(42, 13);
            this.label306.TabIndex = 9;
            this.label306.Text = "GoIn40";
            // 
            // label307
            // 
            this.label307.AutoSize = true;
            this.label307.Location = new System.Drawing.Point(169, 211);
            this.label307.Name = "label307";
            this.label307.Size = new System.Drawing.Size(42, 13);
            this.label307.TabIndex = 9;
            this.label307.Text = "GoIn24";
            // 
            // goin55
            // 
            this.goin55.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin55.FormattingEnabled = true;
            this.goin55.Location = new System.Drawing.Point(553, 181);
            this.goin55.Name = "goin55";
            this.goin55.Size = new System.Drawing.Size(75, 21);
            this.goin55.TabIndex = 20;
            // 
            // goin39
            // 
            this.goin39.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin39.FormattingEnabled = true;
            this.goin39.Location = new System.Drawing.Point(384, 181);
            this.goin39.Name = "goin39";
            this.goin39.Size = new System.Drawing.Size(75, 21);
            this.goin39.TabIndex = 20;
            // 
            // goin23
            // 
            this.goin23.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin23.FormattingEnabled = true;
            this.goin23.Location = new System.Drawing.Point(217, 181);
            this.goin23.Name = "goin23";
            this.goin23.Size = new System.Drawing.Size(75, 21);
            this.goin23.TabIndex = 20;
            // 
            // label308
            // 
            this.label308.AutoSize = true;
            this.label308.Location = new System.Drawing.Point(505, 184);
            this.label308.Name = "label308";
            this.label308.Size = new System.Drawing.Size(42, 13);
            this.label308.TabIndex = 8;
            this.label308.Text = "GoIn55";
            // 
            // label309
            // 
            this.label309.AutoSize = true;
            this.label309.Location = new System.Drawing.Point(336, 184);
            this.label309.Name = "label309";
            this.label309.Size = new System.Drawing.Size(42, 13);
            this.label309.TabIndex = 8;
            this.label309.Text = "GoIn39";
            // 
            // label310
            // 
            this.label310.AutoSize = true;
            this.label310.Location = new System.Drawing.Point(169, 184);
            this.label310.Name = "label310";
            this.label310.Size = new System.Drawing.Size(42, 13);
            this.label310.TabIndex = 8;
            this.label310.Text = "GoIn23";
            // 
            // goin54
            // 
            this.goin54.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin54.FormattingEnabled = true;
            this.goin54.Location = new System.Drawing.Point(553, 154);
            this.goin54.Name = "goin54";
            this.goin54.Size = new System.Drawing.Size(75, 21);
            this.goin54.TabIndex = 22;
            // 
            // goin38
            // 
            this.goin38.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin38.FormattingEnabled = true;
            this.goin38.Location = new System.Drawing.Point(384, 154);
            this.goin38.Name = "goin38";
            this.goin38.Size = new System.Drawing.Size(75, 21);
            this.goin38.TabIndex = 22;
            // 
            // goin22
            // 
            this.goin22.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin22.FormattingEnabled = true;
            this.goin22.Location = new System.Drawing.Point(217, 154);
            this.goin22.Name = "goin22";
            this.goin22.Size = new System.Drawing.Size(75, 21);
            this.goin22.TabIndex = 22;
            // 
            // label311
            // 
            this.label311.AutoSize = true;
            this.label311.Location = new System.Drawing.Point(505, 157);
            this.label311.Name = "label311";
            this.label311.Size = new System.Drawing.Size(42, 13);
            this.label311.TabIndex = 7;
            this.label311.Text = "GoIn54";
            // 
            // label312
            // 
            this.label312.AutoSize = true;
            this.label312.Location = new System.Drawing.Point(336, 157);
            this.label312.Name = "label312";
            this.label312.Size = new System.Drawing.Size(42, 13);
            this.label312.TabIndex = 7;
            this.label312.Text = "GoIn38";
            // 
            // label313
            // 
            this.label313.AutoSize = true;
            this.label313.Location = new System.Drawing.Point(169, 157);
            this.label313.Name = "label313";
            this.label313.Size = new System.Drawing.Size(42, 13);
            this.label313.TabIndex = 7;
            this.label313.Text = "GoIn22";
            // 
            // goin53
            // 
            this.goin53.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin53.FormattingEnabled = true;
            this.goin53.Location = new System.Drawing.Point(553, 127);
            this.goin53.Name = "goin53";
            this.goin53.Size = new System.Drawing.Size(75, 21);
            this.goin53.TabIndex = 24;
            // 
            // goin37
            // 
            this.goin37.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin37.FormattingEnabled = true;
            this.goin37.Location = new System.Drawing.Point(384, 127);
            this.goin37.Name = "goin37";
            this.goin37.Size = new System.Drawing.Size(75, 21);
            this.goin37.TabIndex = 24;
            // 
            // goin21
            // 
            this.goin21.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin21.FormattingEnabled = true;
            this.goin21.Location = new System.Drawing.Point(217, 127);
            this.goin21.Name = "goin21";
            this.goin21.Size = new System.Drawing.Size(75, 21);
            this.goin21.TabIndex = 24;
            // 
            // label314
            // 
            this.label314.AutoSize = true;
            this.label314.Location = new System.Drawing.Point(505, 130);
            this.label314.Name = "label314";
            this.label314.Size = new System.Drawing.Size(42, 13);
            this.label314.TabIndex = 6;
            this.label314.Text = "GoIn53";
            // 
            // label315
            // 
            this.label315.AutoSize = true;
            this.label315.Location = new System.Drawing.Point(336, 130);
            this.label315.Name = "label315";
            this.label315.Size = new System.Drawing.Size(42, 13);
            this.label315.TabIndex = 6;
            this.label315.Text = "GoIn37";
            // 
            // label316
            // 
            this.label316.AutoSize = true;
            this.label316.Location = new System.Drawing.Point(169, 130);
            this.label316.Name = "label316";
            this.label316.Size = new System.Drawing.Size(42, 13);
            this.label316.TabIndex = 6;
            this.label316.Text = "GoIn21";
            // 
            // goin52
            // 
            this.goin52.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin52.FormattingEnabled = true;
            this.goin52.Location = new System.Drawing.Point(553, 100);
            this.goin52.Name = "goin52";
            this.goin52.Size = new System.Drawing.Size(75, 21);
            this.goin52.TabIndex = 26;
            // 
            // goin36
            // 
            this.goin36.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin36.FormattingEnabled = true;
            this.goin36.Location = new System.Drawing.Point(384, 100);
            this.goin36.Name = "goin36";
            this.goin36.Size = new System.Drawing.Size(75, 21);
            this.goin36.TabIndex = 26;
            // 
            // goin20
            // 
            this.goin20.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin20.FormattingEnabled = true;
            this.goin20.Location = new System.Drawing.Point(217, 100);
            this.goin20.Name = "goin20";
            this.goin20.Size = new System.Drawing.Size(75, 21);
            this.goin20.TabIndex = 26;
            // 
            // label317
            // 
            this.label317.AutoSize = true;
            this.label317.Location = new System.Drawing.Point(505, 103);
            this.label317.Name = "label317";
            this.label317.Size = new System.Drawing.Size(42, 13);
            this.label317.TabIndex = 5;
            this.label317.Text = "GoIn52";
            // 
            // label318
            // 
            this.label318.AutoSize = true;
            this.label318.Location = new System.Drawing.Point(336, 103);
            this.label318.Name = "label318";
            this.label318.Size = new System.Drawing.Size(42, 13);
            this.label318.TabIndex = 5;
            this.label318.Text = "GoIn36";
            // 
            // label319
            // 
            this.label319.AutoSize = true;
            this.label319.Location = new System.Drawing.Point(169, 103);
            this.label319.Name = "label319";
            this.label319.Size = new System.Drawing.Size(42, 13);
            this.label319.TabIndex = 5;
            this.label319.Text = "GoIn20";
            // 
            // goin51
            // 
            this.goin51.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin51.FormattingEnabled = true;
            this.goin51.Location = new System.Drawing.Point(553, 73);
            this.goin51.Name = "goin51";
            this.goin51.Size = new System.Drawing.Size(75, 21);
            this.goin51.TabIndex = 28;
            // 
            // goin35
            // 
            this.goin35.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin35.FormattingEnabled = true;
            this.goin35.Location = new System.Drawing.Point(384, 73);
            this.goin35.Name = "goin35";
            this.goin35.Size = new System.Drawing.Size(75, 21);
            this.goin35.TabIndex = 28;
            // 
            // goin19
            // 
            this.goin19.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin19.FormattingEnabled = true;
            this.goin19.Location = new System.Drawing.Point(217, 73);
            this.goin19.Name = "goin19";
            this.goin19.Size = new System.Drawing.Size(75, 21);
            this.goin19.TabIndex = 28;
            // 
            // label320
            // 
            this.label320.AutoSize = true;
            this.label320.Location = new System.Drawing.Point(505, 76);
            this.label320.Name = "label320";
            this.label320.Size = new System.Drawing.Size(42, 13);
            this.label320.TabIndex = 4;
            this.label320.Text = "GoIn51";
            // 
            // label321
            // 
            this.label321.AutoSize = true;
            this.label321.Location = new System.Drawing.Point(336, 76);
            this.label321.Name = "label321";
            this.label321.Size = new System.Drawing.Size(42, 13);
            this.label321.TabIndex = 4;
            this.label321.Text = "GoIn35";
            // 
            // label322
            // 
            this.label322.AutoSize = true;
            this.label322.Location = new System.Drawing.Point(169, 76);
            this.label322.Name = "label322";
            this.label322.Size = new System.Drawing.Size(42, 13);
            this.label322.TabIndex = 4;
            this.label322.Text = "GoIn19";
            // 
            // goin50
            // 
            this.goin50.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin50.FormattingEnabled = true;
            this.goin50.Location = new System.Drawing.Point(553, 46);
            this.goin50.Name = "goin50";
            this.goin50.Size = new System.Drawing.Size(75, 21);
            this.goin50.TabIndex = 30;
            // 
            // goin34
            // 
            this.goin34.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin34.FormattingEnabled = true;
            this.goin34.Location = new System.Drawing.Point(384, 46);
            this.goin34.Name = "goin34";
            this.goin34.Size = new System.Drawing.Size(75, 21);
            this.goin34.TabIndex = 30;
            // 
            // goin18
            // 
            this.goin18.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin18.FormattingEnabled = true;
            this.goin18.Location = new System.Drawing.Point(217, 46);
            this.goin18.Name = "goin18";
            this.goin18.Size = new System.Drawing.Size(75, 21);
            this.goin18.TabIndex = 30;
            // 
            // label323
            // 
            this.label323.AutoSize = true;
            this.label323.Location = new System.Drawing.Point(505, 49);
            this.label323.Name = "label323";
            this.label323.Size = new System.Drawing.Size(42, 13);
            this.label323.TabIndex = 3;
            this.label323.Text = "GoIn50";
            // 
            // label324
            // 
            this.label324.AutoSize = true;
            this.label324.Location = new System.Drawing.Point(336, 49);
            this.label324.Name = "label324";
            this.label324.Size = new System.Drawing.Size(42, 13);
            this.label324.TabIndex = 3;
            this.label324.Text = "GoIn34";
            // 
            // label325
            // 
            this.label325.AutoSize = true;
            this.label325.Location = new System.Drawing.Point(169, 49);
            this.label325.Name = "label325";
            this.label325.Size = new System.Drawing.Size(42, 13);
            this.label325.TabIndex = 3;
            this.label325.Text = "GoIn18";
            // 
            // goin49
            // 
            this.goin49.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin49.FormattingEnabled = true;
            this.goin49.Location = new System.Drawing.Point(553, 19);
            this.goin49.Name = "goin49";
            this.goin49.Size = new System.Drawing.Size(75, 21);
            this.goin49.TabIndex = 32;
            // 
            // label326
            // 
            this.label326.AutoSize = true;
            this.label326.Location = new System.Drawing.Point(505, 22);
            this.label326.Name = "label326";
            this.label326.Size = new System.Drawing.Size(42, 13);
            this.label326.TabIndex = 12;
            this.label326.Text = "GoIn49";
            // 
            // goin33
            // 
            this.goin33.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin33.FormattingEnabled = true;
            this.goin33.Location = new System.Drawing.Point(384, 19);
            this.goin33.Name = "goin33";
            this.goin33.Size = new System.Drawing.Size(75, 21);
            this.goin33.TabIndex = 32;
            // 
            // label327
            // 
            this.label327.AutoSize = true;
            this.label327.Location = new System.Drawing.Point(336, 22);
            this.label327.Name = "label327";
            this.label327.Size = new System.Drawing.Size(42, 13);
            this.label327.TabIndex = 12;
            this.label327.Text = "GoIn33";
            // 
            // goin17
            // 
            this.goin17.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin17.FormattingEnabled = true;
            this.goin17.Location = new System.Drawing.Point(217, 19);
            this.goin17.Name = "goin17";
            this.goin17.Size = new System.Drawing.Size(75, 21);
            this.goin17.TabIndex = 32;
            // 
            // label328
            // 
            this.label328.AutoSize = true;
            this.label328.Location = new System.Drawing.Point(169, 22);
            this.label328.Name = "label328";
            this.label328.Size = new System.Drawing.Size(42, 13);
            this.label328.TabIndex = 12;
            this.label328.Text = "GoIn17";
            // 
            // goin16
            // 
            this.goin16.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin16.FormattingEnabled = true;
            this.goin16.Location = new System.Drawing.Point(54, 424);
            this.goin16.Name = "goin16";
            this.goin16.Size = new System.Drawing.Size(75, 21);
            this.goin16.TabIndex = 1;
            // 
            // label329
            // 
            this.label329.AutoSize = true;
            this.label329.Location = new System.Drawing.Point(6, 427);
            this.label329.Name = "label329";
            this.label329.Size = new System.Drawing.Size(42, 13);
            this.label329.TabIndex = 0;
            this.label329.Text = "GoIn16";
            // 
            // goin15
            // 
            this.goin15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin15.FormattingEnabled = true;
            this.goin15.Location = new System.Drawing.Point(54, 397);
            this.goin15.Name = "goin15";
            this.goin15.Size = new System.Drawing.Size(75, 21);
            this.goin15.TabIndex = 1;
            // 
            // label330
            // 
            this.label330.AutoSize = true;
            this.label330.Location = new System.Drawing.Point(6, 400);
            this.label330.Name = "label330";
            this.label330.Size = new System.Drawing.Size(42, 13);
            this.label330.TabIndex = 0;
            this.label330.Text = "GoIn15";
            // 
            // goin14
            // 
            this.goin14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin14.FormattingEnabled = true;
            this.goin14.Location = new System.Drawing.Point(54, 370);
            this.goin14.Name = "goin14";
            this.goin14.Size = new System.Drawing.Size(75, 21);
            this.goin14.TabIndex = 1;
            // 
            // label331
            // 
            this.label331.AutoSize = true;
            this.label331.Location = new System.Drawing.Point(6, 373);
            this.label331.Name = "label331";
            this.label331.Size = new System.Drawing.Size(42, 13);
            this.label331.TabIndex = 0;
            this.label331.Text = "GoIn14";
            // 
            // goin13
            // 
            this.goin13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin13.FormattingEnabled = true;
            this.goin13.Location = new System.Drawing.Point(54, 343);
            this.goin13.Name = "goin13";
            this.goin13.Size = new System.Drawing.Size(75, 21);
            this.goin13.TabIndex = 1;
            // 
            // label332
            // 
            this.label332.AutoSize = true;
            this.label332.Location = new System.Drawing.Point(6, 346);
            this.label332.Name = "label332";
            this.label332.Size = new System.Drawing.Size(42, 13);
            this.label332.TabIndex = 0;
            this.label332.Text = "GoIn13";
            // 
            // goin12
            // 
            this.goin12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin12.FormattingEnabled = true;
            this.goin12.Location = new System.Drawing.Point(54, 316);
            this.goin12.Name = "goin12";
            this.goin12.Size = new System.Drawing.Size(75, 21);
            this.goin12.TabIndex = 1;
            // 
            // label333
            // 
            this.label333.AutoSize = true;
            this.label333.Location = new System.Drawing.Point(6, 319);
            this.label333.Name = "label333";
            this.label333.Size = new System.Drawing.Size(42, 13);
            this.label333.TabIndex = 0;
            this.label333.Text = "GoIn12";
            // 
            // goin11
            // 
            this.goin11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin11.FormattingEnabled = true;
            this.goin11.Location = new System.Drawing.Point(54, 289);
            this.goin11.Name = "goin11";
            this.goin11.Size = new System.Drawing.Size(75, 21);
            this.goin11.TabIndex = 1;
            // 
            // label334
            // 
            this.label334.AutoSize = true;
            this.label334.Location = new System.Drawing.Point(6, 292);
            this.label334.Name = "label334";
            this.label334.Size = new System.Drawing.Size(42, 13);
            this.label334.TabIndex = 0;
            this.label334.Text = "GoIn11";
            // 
            // goin10
            // 
            this.goin10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin10.FormattingEnabled = true;
            this.goin10.Location = new System.Drawing.Point(54, 262);
            this.goin10.Name = "goin10";
            this.goin10.Size = new System.Drawing.Size(75, 21);
            this.goin10.TabIndex = 1;
            // 
            // label335
            // 
            this.label335.AutoSize = true;
            this.label335.Location = new System.Drawing.Point(6, 265);
            this.label335.Name = "label335";
            this.label335.Size = new System.Drawing.Size(42, 13);
            this.label335.TabIndex = 0;
            this.label335.Text = "GoIn10";
            // 
            // goin9
            // 
            this.goin9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin9.FormattingEnabled = true;
            this.goin9.Location = new System.Drawing.Point(54, 235);
            this.goin9.Name = "goin9";
            this.goin9.Size = new System.Drawing.Size(75, 21);
            this.goin9.TabIndex = 1;
            // 
            // label336
            // 
            this.label336.AutoSize = true;
            this.label336.Location = new System.Drawing.Point(6, 238);
            this.label336.Name = "label336";
            this.label336.Size = new System.Drawing.Size(36, 13);
            this.label336.TabIndex = 0;
            this.label336.Text = "GoIn9";
            // 
            // goin8
            // 
            this.goin8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin8.FormattingEnabled = true;
            this.goin8.Location = new System.Drawing.Point(54, 208);
            this.goin8.Name = "goin8";
            this.goin8.Size = new System.Drawing.Size(75, 21);
            this.goin8.TabIndex = 1;
            // 
            // label337
            // 
            this.label337.AutoSize = true;
            this.label337.Location = new System.Drawing.Point(6, 211);
            this.label337.Name = "label337";
            this.label337.Size = new System.Drawing.Size(36, 13);
            this.label337.TabIndex = 0;
            this.label337.Text = "GoIn8";
            // 
            // goin7
            // 
            this.goin7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin7.FormattingEnabled = true;
            this.goin7.Location = new System.Drawing.Point(54, 181);
            this.goin7.Name = "goin7";
            this.goin7.Size = new System.Drawing.Size(75, 21);
            this.goin7.TabIndex = 1;
            // 
            // label338
            // 
            this.label338.AutoSize = true;
            this.label338.Location = new System.Drawing.Point(6, 184);
            this.label338.Name = "label338";
            this.label338.Size = new System.Drawing.Size(36, 13);
            this.label338.TabIndex = 0;
            this.label338.Text = "GoIn7";
            // 
            // goin6
            // 
            this.goin6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin6.FormattingEnabled = true;
            this.goin6.Location = new System.Drawing.Point(54, 154);
            this.goin6.Name = "goin6";
            this.goin6.Size = new System.Drawing.Size(75, 21);
            this.goin6.TabIndex = 1;
            // 
            // label339
            // 
            this.label339.AutoSize = true;
            this.label339.Location = new System.Drawing.Point(6, 157);
            this.label339.Name = "label339";
            this.label339.Size = new System.Drawing.Size(36, 13);
            this.label339.TabIndex = 0;
            this.label339.Text = "GoIn6";
            // 
            // goin5
            // 
            this.goin5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin5.FormattingEnabled = true;
            this.goin5.Location = new System.Drawing.Point(54, 127);
            this.goin5.Name = "goin5";
            this.goin5.Size = new System.Drawing.Size(75, 21);
            this.goin5.TabIndex = 1;
            // 
            // label340
            // 
            this.label340.AutoSize = true;
            this.label340.Location = new System.Drawing.Point(6, 130);
            this.label340.Name = "label340";
            this.label340.Size = new System.Drawing.Size(36, 13);
            this.label340.TabIndex = 0;
            this.label340.Text = "GoIn5";
            // 
            // goin4
            // 
            this.goin4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin4.FormattingEnabled = true;
            this.goin4.Location = new System.Drawing.Point(54, 100);
            this.goin4.Name = "goin4";
            this.goin4.Size = new System.Drawing.Size(75, 21);
            this.goin4.TabIndex = 1;
            // 
            // label341
            // 
            this.label341.AutoSize = true;
            this.label341.Location = new System.Drawing.Point(6, 103);
            this.label341.Name = "label341";
            this.label341.Size = new System.Drawing.Size(36, 13);
            this.label341.TabIndex = 0;
            this.label341.Text = "GoIn4";
            // 
            // goin3
            // 
            this.goin3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin3.FormattingEnabled = true;
            this.goin3.Location = new System.Drawing.Point(54, 73);
            this.goin3.Name = "goin3";
            this.goin3.Size = new System.Drawing.Size(75, 21);
            this.goin3.TabIndex = 1;
            // 
            // label342
            // 
            this.label342.AutoSize = true;
            this.label342.Location = new System.Drawing.Point(6, 76);
            this.label342.Name = "label342";
            this.label342.Size = new System.Drawing.Size(36, 13);
            this.label342.TabIndex = 0;
            this.label342.Text = "GoIn3";
            // 
            // goin2
            // 
            this.goin2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin2.FormattingEnabled = true;
            this.goin2.Location = new System.Drawing.Point(54, 46);
            this.goin2.Name = "goin2";
            this.goin2.Size = new System.Drawing.Size(75, 21);
            this.goin2.TabIndex = 1;
            // 
            // label343
            // 
            this.label343.AutoSize = true;
            this.label343.Location = new System.Drawing.Point(6, 49);
            this.label343.Name = "label343";
            this.label343.Size = new System.Drawing.Size(36, 13);
            this.label343.TabIndex = 0;
            this.label343.Text = "GoIn2";
            // 
            // goin1
            // 
            this.goin1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin1.FormattingEnabled = true;
            this.goin1.Location = new System.Drawing.Point(54, 19);
            this.goin1.Name = "goin1";
            this.goin1.Size = new System.Drawing.Size(75, 21);
            this.goin1.TabIndex = 1;
            // 
            // label344
            // 
            this.label344.AutoSize = true;
            this.label344.Location = new System.Drawing.Point(6, 22);
            this.label344.Name = "label344";
            this.label344.Size = new System.Drawing.Size(36, 13);
            this.label344.TabIndex = 0;
            this.label344.Text = "GoIn1";
            // 
            // label345
            // 
            this.label345.AutoSize = true;
            this.label345.Location = new System.Drawing.Point(6, 22);
            this.label345.Name = "label345";
            this.label345.Size = new System.Drawing.Size(57, 13);
            this.label345.TabIndex = 6;
            this.label345.Text = "Операция";
            // 
            // operationBGS
            // 
            this.operationBGS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.operationBGS.FormattingEnabled = true;
            this.operationBGS.Location = new System.Drawing.Point(69, 19);
            this.operationBGS.Name = "operationBGS";
            this.operationBGS.Size = new System.Drawing.Size(112, 21);
            this.operationBGS.TabIndex = 7;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(6, 9);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(89, 13);
            this.label36.TabIndex = 10;
            this.label36.Text = "Выбранный БГС";
            // 
            // currentBGS
            // 
            this.currentBGS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.currentBGS.FormattingEnabled = true;
            this.currentBGS.Location = new System.Drawing.Point(101, 6);
            this.currentBGS.Name = "currentBGS";
            this.currentBGS.Size = new System.Drawing.Size(102, 21);
            this.currentBGS.TabIndex = 11;
            // 
            // _ethernetPage
            // 
            this._ethernetPage.Controls.Add(this._reserveGroupBox);
            this._ethernetPage.Controls.Add(this.groupBox51);
            this._ethernetPage.Location = new System.Drawing.Point(4, 22);
            this._ethernetPage.Name = "_ethernetPage";
            this._ethernetPage.Padding = new System.Windows.Forms.Padding(3);
            this._ethernetPage.Size = new System.Drawing.Size(1024, 553);
            this._ethernetPage.TabIndex = 14;
            this._ethernetPage.Text = "Конфигурация Ethernet";
            this._ethernetPage.UseVisualStyleBackColor = true;
            // 
            // _reserveGroupBox
            // 
            this._reserveGroupBox.Controls.Add(this._reserveCB);
            this._reserveGroupBox.Location = new System.Drawing.Point(275, 7);
            this._reserveGroupBox.Name = "_reserveGroupBox";
            this._reserveGroupBox.Size = new System.Drawing.Size(113, 52);
            this._reserveGroupBox.TabIndex = 2;
            this._reserveGroupBox.TabStop = false;
            this._reserveGroupBox.Text = "Резервирование";
            // 
            // _reserveCB
            // 
            this._reserveCB.FormattingEnabled = true;
            this._reserveCB.Location = new System.Drawing.Point(9, 19);
            this._reserveCB.Name = "_reserveCB";
            this._reserveCB.Size = new System.Drawing.Size(89, 21);
            this._reserveCB.TabIndex = 0;
            // 
            // groupBox51
            // 
            this.groupBox51.Controls.Add(this._ipLo1);
            this.groupBox51.Controls.Add(this._ipLo2);
            this.groupBox51.Controls.Add(this._ipHi1);
            this.groupBox51.Controls.Add(this._ipHi2);
            this.groupBox51.Controls.Add(this.label214);
            this.groupBox51.Controls.Add(this.label213);
            this.groupBox51.Controls.Add(this.label212);
            this.groupBox51.Controls.Add(this.label179);
            this.groupBox51.Location = new System.Drawing.Point(8, 6);
            this.groupBox51.Name = "groupBox51";
            this.groupBox51.Size = new System.Drawing.Size(261, 53);
            this.groupBox51.TabIndex = 1;
            this.groupBox51.TabStop = false;
            this.groupBox51.Text = "Конфигурация Ethernet";
            // 
            // _ipLo1
            // 
            this._ipLo1.Location = new System.Drawing.Point(215, 19);
            this._ipLo1.Name = "_ipLo1";
            this._ipLo1.Size = new System.Drawing.Size(38, 20);
            this._ipLo1.TabIndex = 3;
            // 
            // _ipLo2
            // 
            this._ipLo2.Location = new System.Drawing.Point(163, 19);
            this._ipLo2.Name = "_ipLo2";
            this._ipLo2.Size = new System.Drawing.Size(38, 20);
            this._ipLo2.TabIndex = 2;
            // 
            // _ipHi1
            // 
            this._ipHi1.Location = new System.Drawing.Point(112, 19);
            this._ipHi1.Name = "_ipHi1";
            this._ipHi1.Size = new System.Drawing.Size(38, 20);
            this._ipHi1.TabIndex = 1;
            // 
            // _ipHi2
            // 
            this._ipHi2.Location = new System.Drawing.Point(60, 19);
            this._ipHi2.Name = "_ipHi2";
            this._ipHi2.Size = new System.Drawing.Size(38, 20);
            this._ipHi2.TabIndex = 0;
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Location = new System.Drawing.Point(152, 26);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(10, 13);
            this.label214.TabIndex = 9;
            this.label214.Text = ".";
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Location = new System.Drawing.Point(203, 26);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(10, 13);
            this.label213.TabIndex = 9;
            this.label213.Text = ".";
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Location = new System.Drawing.Point(100, 26);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(10, 13);
            this.label212.TabIndex = 9;
            this.label212.Text = ".";
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(6, 22);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(50, 13);
            this.label179.TabIndex = 9;
            this.label179.Text = "IP-адрес";
            // 
            // _dif0DataGreed
            // 
            this._dif0DataGreed.AllowUserToAddRows = false;
            this._dif0DataGreed.AllowUserToDeleteRows = false;
            this._dif0DataGreed.AllowUserToResizeColumns = false;
            this._dif0DataGreed.AllowUserToResizeRows = false;
            this._dif0DataGreed.BackgroundColor = System.Drawing.Color.White;
            this._dif0DataGreed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dif0DataGreed.Location = new System.Drawing.Point(6, 21);
            this._dif0DataGreed.Name = "_dif0DataGreed";
            this._dif0DataGreed.RowHeadersVisible = false;
            this._dif0DataGreed.RowHeadersWidth = 51;
            this._dif0DataGreed.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._dif0DataGreed.RowTemplate.Height = 24;
            this._dif0DataGreed.ShowCellErrors = false;
            this._dif0DataGreed.ShowRowErrors = false;
            this._dif0DataGreed.Size = new System.Drawing.Size(751, 112);
            this._dif0DataGreed.TabIndex = 2;
            // 
            // _dif0AVRColumn
            // 
            this._dif0AVRColumn.HeaderText = "АВР";
            this._dif0AVRColumn.MinimumWidth = 6;
            this._dif0AVRColumn.Name = "_dif0AVRColumn";
            this._dif0AVRColumn.Visible = false;
            this._dif0AVRColumn.Width = 125;
            // 
            // _dif0APVColumn
            // 
            this._dif0APVColumn.HeaderText = "АПВ";
            this._dif0APVColumn.MinimumWidth = 6;
            this._dif0APVColumn.Name = "_dif0APVColumn";
            this._dif0APVColumn.Visible = false;
            this._dif0APVColumn.Width = 125;
            // 
            // _dif0UrovColumn
            // 
            this._dif0UrovColumn.HeaderText = "Уров";
            this._dif0UrovColumn.MinimumWidth = 6;
            this._dif0UrovColumn.Name = "_dif0UrovColumn";
            this._dif0UrovColumn.Visible = false;
            this._dif0UrovColumn.Width = 125;
            // 
            // _dif0OscColumn
            // 
            dataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0OscColumn.DefaultCellStyle = dataGridViewCellStyle90;
            this._dif0OscColumn.HeaderText = "Осциллограф";
            this._dif0OscColumn.MaxDropDownItems = 15;
            this._dif0OscColumn.MinimumWidth = 6;
            this._dif0OscColumn.Name = "_dif0OscColumn";
            this._dif0OscColumn.Width = 90;
            // 
            // _dif0Intg2Column
            // 
            dataGridViewCellStyle91.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0Intg2Column.DefaultCellStyle = dataGridViewCellStyle91;
            this._dif0Intg2Column.HeaderText = "Угол f2";
            this._dif0Intg2Column.MinimumWidth = 6;
            this._dif0Intg2Column.Name = "_dif0Intg2Column";
            this._dif0Intg2Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0Intg2Column.Width = 55;
            // 
            // _dif0Ib2Column
            // 
            dataGridViewCellStyle92.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0Ib2Column.DefaultCellStyle = dataGridViewCellStyle92;
            this._dif0Ib2Column.HeaderText = "Iб2 [Iн стороны]";
            this._dif0Ib2Column.MinimumWidth = 6;
            this._dif0Ib2Column.Name = "_dif0Ib2Column";
            this._dif0Ib2Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0Ib2Column.Width = 115;
            // 
            // _dif0Intg1Column
            // 
            dataGridViewCellStyle93.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0Intg1Column.DefaultCellStyle = dataGridViewCellStyle93;
            this._dif0Intg1Column.HeaderText = "Угол f1";
            this._dif0Intg1Column.MinimumWidth = 6;
            this._dif0Intg1Column.Name = "_dif0Intg1Column";
            this._dif0Intg1Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0Intg1Column.Width = 55;
            // 
            // _dif0Ib1Column
            // 
            dataGridViewCellStyle94.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0Ib1Column.DefaultCellStyle = dataGridViewCellStyle94;
            this._dif0Ib1Column.HeaderText = "Iб1 [Iн стороны]";
            this._dif0Ib1Column.MinimumWidth = 6;
            this._dif0Ib1Column.Name = "_dif0Ib1Column";
            this._dif0Ib1Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0Ib1Column.Width = 115;
            // 
            // _dif0TdColumn
            // 
            dataGridViewCellStyle95.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0TdColumn.DefaultCellStyle = dataGridViewCellStyle95;
            this._dif0TdColumn.HeaderText = "tд [мс]";
            this._dif0TdColumn.MinimumWidth = 6;
            this._dif0TdColumn.Name = "_dif0TdColumn";
            this._dif0TdColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0TdColumn.Width = 50;
            // 
            // _dif0InColumn
            // 
            dataGridViewCellStyle96.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0InColumn.DefaultCellStyle = dataGridViewCellStyle96;
            this._dif0InColumn.HeaderText = "Сторона";
            this._dif0InColumn.MaxDropDownItems = 15;
            this._dif0InColumn.MinimumWidth = 6;
            this._dif0InColumn.Name = "_dif0InColumn";
            this._dif0InColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dif0InColumn.Width = 85;
            // 
            // _dif0IdColumn
            // 
            dataGridViewCellStyle97.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0IdColumn.DefaultCellStyle = dataGridViewCellStyle97;
            this._dif0IdColumn.HeaderText = "Iд [Iн стороны]";
            this._dif0IdColumn.MinimumWidth = 6;
            this._dif0IdColumn.Name = "_dif0IdColumn";
            this._dif0IdColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0IdColumn.Width = 110;
            // 
            // _dif0BlockingColumn
            // 
            dataGridViewCellStyle98.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0BlockingColumn.DefaultCellStyle = dataGridViewCellStyle98;
            this._dif0BlockingColumn.HeaderText = "Блокировка";
            this._dif0BlockingColumn.MaxDropDownItems = 15;
            this._dif0BlockingColumn.MinimumWidth = 6;
            this._dif0BlockingColumn.Name = "_dif0BlockingColumn";
            this._dif0BlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dif0BlockingColumn.Width = 85;
            // 
            // _dif0ModeColumn
            // 
            dataGridViewCellStyle99.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0ModeColumn.DefaultCellStyle = dataGridViewCellStyle99;
            this._dif0ModeColumn.HeaderText = "Состояние";
            this._dif0ModeColumn.MaxDropDownItems = 15;
            this._dif0ModeColumn.MinimumWidth = 6;
            this._dif0ModeColumn.Name = "_dif0ModeColumn";
            this._dif0ModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dif0ModeColumn.Width = 90;
            // 
            // _dif0StageColumn
            // 
            dataGridViewCellStyle100.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle100.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle100.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle100.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle100.SelectionForeColor = System.Drawing.Color.White;
            this._dif0StageColumn.DefaultCellStyle = dataGridViewCellStyle100;
            this._dif0StageColumn.Frozen = true;
            this._dif0StageColumn.HeaderText = "Ступень";
            this._dif0StageColumn.MinimumWidth = 6;
            this._dif0StageColumn.Name = "_dif0StageColumn";
            this._dif0StageColumn.ReadOnly = true;
            this._dif0StageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0StageColumn.Width = 78;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(6, 18);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(61, 13);
            this.label47.TabIndex = 0;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(6, 97);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(189, 13);
            this.label48.TabIndex = 1;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(6, 58);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(74, 13);
            this.label49.TabIndex = 2;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(6, 77);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(135, 13);
            this.label50.TabIndex = 3;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(6, 117);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(37, 13);
            this.label52.TabIndex = 5;
            this.label52.Visible = false;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(6, 137);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(76, 13);
            this.label53.TabIndex = 6;
            // 
            // _modeDTOBTComboBox
            // 
            this._modeDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._modeDTOBTComboBox.FormattingEnabled = true;
            this._modeDTOBTComboBox.Location = new System.Drawing.Point(196, 15);
            this._modeDTOBTComboBox.Name = "_modeDTOBTComboBox";
            this._modeDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._modeDTOBTComboBox.TabIndex = 7;
            // 
            // _stepOnInstantValuesDTOBTComboBox
            // 
            this._stepOnInstantValuesDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._stepOnInstantValuesDTOBTComboBox.FormattingEnabled = true;
            this._stepOnInstantValuesDTOBTComboBox.Location = new System.Drawing.Point(196, 94);
            this._stepOnInstantValuesDTOBTComboBox.Name = "_stepOnInstantValuesDTOBTComboBox";
            this._stepOnInstantValuesDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._stepOnInstantValuesDTOBTComboBox.TabIndex = 8;
            // 
            // _UROVDTOBTComboBox
            // 
            this._UROVDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._UROVDTOBTComboBox.FormattingEnabled = true;
            this._UROVDTOBTComboBox.Location = new System.Drawing.Point(196, 114);
            this._UROVDTOBTComboBox.Name = "_UROVDTOBTComboBox";
            this._UROVDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._UROVDTOBTComboBox.TabIndex = 12;
            this._UROVDTOBTComboBox.Visible = false;
            // 
            // _oscDTOBTComboBox
            // 
            this._oscDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscDTOBTComboBox.FormattingEnabled = true;
            this._oscDTOBTComboBox.Location = new System.Drawing.Point(196, 134);
            this._oscDTOBTComboBox.Name = "_oscDTOBTComboBox";
            this._oscDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._oscDTOBTComboBox.TabIndex = 13;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.ForeColor = System.Drawing.Color.Red;
            this.label54.Location = new System.Drawing.Point(287, 58);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(22, 13);
            this.label54.TabIndex = 14;
            // 
            // _blockingDTOBTComboBox
            // 
            this._blockingDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._blockingDTOBTComboBox.FormattingEnabled = true;
            this._blockingDTOBTComboBox.Location = new System.Drawing.Point(196, 35);
            this._blockingDTOBTComboBox.Name = "_blockingDTOBTComboBox";
            this._blockingDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._blockingDTOBTComboBox.TabIndex = 11;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(6, 38);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(68, 13);
            this.label51.TabIndex = 4;
            // 
            // _constraintDTOBTTextBox
            // 
            this._constraintDTOBTTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._constraintDTOBTTextBox.Location = new System.Drawing.Point(196, 55);
            this._constraintDTOBTTextBox.Name = "_constraintDTOBTTextBox";
            this._constraintDTOBTTextBox.Size = new System.Drawing.Size(90, 20);
            this._constraintDTOBTTextBox.TabIndex = 15;
            this._constraintDTOBTTextBox.Tag = "40";
            this._constraintDTOBTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.ForeColor = System.Drawing.Color.Red;
            this.label55.Location = new System.Drawing.Point(287, 77);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(27, 13);
            this.label55.TabIndex = 15;
            // 
            // _timeEnduranceDTOBTTextBox
            // 
            this._timeEnduranceDTOBTTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._timeEnduranceDTOBTTextBox.Location = new System.Drawing.Point(196, 74);
            this._timeEnduranceDTOBTTextBox.Name = "_timeEnduranceDTOBTTextBox";
            this._timeEnduranceDTOBTTextBox.Size = new System.Drawing.Size(90, 20);
            this._timeEnduranceDTOBTTextBox.TabIndex = 16;
            this._timeEnduranceDTOBTTextBox.Tag = "3276700";
            this._timeEnduranceDTOBTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(6, 157);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(29, 13);
            this.label128.TabIndex = 17;
            this.label128.Visible = false;
            // 
            // _APVDTOBTComboBox
            // 
            this._APVDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._APVDTOBTComboBox.FormattingEnabled = true;
            this._APVDTOBTComboBox.Location = new System.Drawing.Point(196, 154);
            this._APVDTOBTComboBox.Name = "_APVDTOBTComboBox";
            this._APVDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._APVDTOBTComboBox.TabIndex = 18;
            this._APVDTOBTComboBox.Visible = false;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(6, 177);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(28, 13);
            this.label129.TabIndex = 19;
            this.label129.Visible = false;
            // 
            // _AVRDTOBTComboBox
            // 
            this._AVRDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._AVRDTOBTComboBox.FormattingEnabled = true;
            this._AVRDTOBTComboBox.Location = new System.Drawing.Point(196, 174);
            this._AVRDTOBTComboBox.Name = "_AVRDTOBTComboBox";
            this._AVRDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._AVRDTOBTComboBox.TabIndex = 20;
            this._AVRDTOBTComboBox.Visible = false;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(6, 16);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(61, 13);
            this.label56.TabIndex = 8;
            // 
            // _modeDTZComboBox
            // 
            this._modeDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._modeDTZComboBox.FormattingEnabled = true;
            this._modeDTZComboBox.Location = new System.Drawing.Point(148, 13);
            this._modeDTZComboBox.Name = "_modeDTZComboBox";
            this._modeDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._modeDTZComboBox.TabIndex = 9;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(6, 57);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(68, 13);
            this.label61.TabIndex = 16;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(6, 76);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(129, 13);
            this.label59.TabIndex = 17;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.ForeColor = System.Drawing.Color.Red;
            this.label58.Location = new System.Drawing.Point(239, 57);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(22, 13);
            this.label58.TabIndex = 20;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.ForeColor = System.Drawing.Color.Red;
            this.label57.Location = new System.Drawing.Point(239, 76);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(27, 13);
            this.label57.TabIndex = 21;
            // 
            // groupBox8
            // 
            this.groupBox8.Location = new System.Drawing.Point(6, 264);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(261, 107);
            this.groupBox8.TabIndex = 22;
            this.groupBox8.TabStop = false;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(6, 24);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(66, 13);
            this.label62.TabIndex = 2;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(6, 43);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(92, 13);
            this.label63.TabIndex = 3;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(6, 62);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(66, 13);
            this.label64.TabIndex = 4;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(6, 81);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(92, 13);
            this.label65.TabIndex = 5;
            // 
            // _Ib1BeginTextBox
            // 
            this._Ib1BeginTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ib1BeginTextBox.Location = new System.Drawing.Point(141, 21);
            this._Ib1BeginTextBox.Name = "_Ib1BeginTextBox";
            this._Ib1BeginTextBox.Size = new System.Drawing.Size(90, 20);
            this._Ib1BeginTextBox.TabIndex = 15;
            this._Ib1BeginTextBox.Tag = "40";
            this._Ib1BeginTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _K1AngleOfSlopeTextBox
            // 
            this._K1AngleOfSlopeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._K1AngleOfSlopeTextBox.Location = new System.Drawing.Point(141, 40);
            this._K1AngleOfSlopeTextBox.Name = "_K1AngleOfSlopeTextBox";
            this._K1AngleOfSlopeTextBox.Size = new System.Drawing.Size(90, 20);
            this._K1AngleOfSlopeTextBox.TabIndex = 16;
            this._K1AngleOfSlopeTextBox.Tag = "89";
            this._K1AngleOfSlopeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.ForeColor = System.Drawing.Color.Red;
            this.label66.Location = new System.Drawing.Point(232, 24);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(22, 13);
            this.label66.TabIndex = 21;
            // 
            // _Ib2BeginTextBox
            // 
            this._Ib2BeginTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ib2BeginTextBox.Location = new System.Drawing.Point(141, 59);
            this._Ib2BeginTextBox.Name = "_Ib2BeginTextBox";
            this._Ib2BeginTextBox.Size = new System.Drawing.Size(90, 20);
            this._Ib2BeginTextBox.TabIndex = 17;
            this._Ib2BeginTextBox.Tag = "40";
            this._Ib2BeginTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.ForeColor = System.Drawing.Color.Red;
            this.label69.Location = new System.Drawing.Point(232, 62);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(22, 13);
            this.label69.TabIndex = 22;
            // 
            // _K2TangensTextBox
            // 
            this._K2TangensTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._K2TangensTextBox.Location = new System.Drawing.Point(141, 78);
            this._K2TangensTextBox.Name = "_K2TangensTextBox";
            this._K2TangensTextBox.Size = new System.Drawing.Size(90, 20);
            this._K2TangensTextBox.TabIndex = 18;
            this._K2TangensTextBox.Tag = "89";
            this._K2TangensTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.ForeColor = System.Drawing.Color.Red;
            this.label70.Location = new System.Drawing.Point(232, 43);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(11, 13);
            this.label70.TabIndex = 23;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.ForeColor = System.Drawing.Color.Red;
            this.label71.Location = new System.Drawing.Point(232, 81);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(11, 13);
            this.label71.TabIndex = 24;
            // 
            // _constraintDTZTextBox
            // 
            this._constraintDTZTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._constraintDTZTextBox.Location = new System.Drawing.Point(148, 53);
            this._constraintDTZTextBox.Name = "_constraintDTZTextBox";
            this._constraintDTZTextBox.Size = new System.Drawing.Size(90, 20);
            this._constraintDTZTextBox.TabIndex = 13;
            this._constraintDTZTextBox.Tag = "40";
            this._constraintDTZTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox9
            // 
            this.groupBox9.Location = new System.Drawing.Point(6, 104);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(261, 73);
            this.groupBox9.TabIndex = 23;
            this.groupBox9.TabStop = false;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(6, 49);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(123, 13);
            this.label73.TabIndex = 22;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.ForeColor = System.Drawing.Color.Red;
            this.label72.Location = new System.Drawing.Point(232, 49);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(15, 13);
            this.label72.TabIndex = 24;
            // 
            // _I2I1TextBox
            // 
            this._I2I1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I2I1TextBox.Location = new System.Drawing.Point(141, 46);
            this._I2I1TextBox.Name = "_I2I1TextBox";
            this._I2I1TextBox.Size = new System.Drawing.Size(90, 20);
            this._I2I1TextBox.TabIndex = 19;
            this._I2I1TextBox.Tag = "100";
            this._I2I1TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 25);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(75, 13);
            this.label23.TabIndex = 30;
            // 
            // _perBlockI2I1
            // 
            this._perBlockI2I1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._perBlockI2I1.FormattingEnabled = true;
            this._perBlockI2I1.Location = new System.Drawing.Point(141, 19);
            this._perBlockI2I1.Name = "_perBlockI2I1";
            this._perBlockI2I1.Size = new System.Drawing.Size(90, 21);
            this._perBlockI2I1.TabIndex = 36;
            // 
            // _timeEnduranceDTZTextBox
            // 
            this._timeEnduranceDTZTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._timeEnduranceDTZTextBox.Location = new System.Drawing.Point(148, 72);
            this._timeEnduranceDTZTextBox.Name = "_timeEnduranceDTZTextBox";
            this._timeEnduranceDTZTextBox.Size = new System.Drawing.Size(90, 20);
            this._timeEnduranceDTZTextBox.TabIndex = 14;
            this._timeEnduranceDTZTextBox.Tag = "3276700";
            this._timeEnduranceDTZTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(6, 36);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(68, 13);
            this.label78.TabIndex = 24;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(6, 376);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(37, 13);
            this.label77.TabIndex = 25;
            this.label77.Visible = false;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(6, 396);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(76, 13);
            this.label76.TabIndex = 26;
            // 
            // _blockingDTZComboBox
            // 
            this._blockingDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._blockingDTZComboBox.FormattingEnabled = true;
            this._blockingDTZComboBox.Location = new System.Drawing.Point(148, 33);
            this._blockingDTZComboBox.Name = "_blockingDTZComboBox";
            this._blockingDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._blockingDTZComboBox.TabIndex = 27;
            // 
            // _UROVDTZComboBox
            // 
            this._UROVDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._UROVDTZComboBox.FormattingEnabled = true;
            this._UROVDTZComboBox.Location = new System.Drawing.Point(148, 373);
            this._UROVDTZComboBox.Name = "_UROVDTZComboBox";
            this._UROVDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._UROVDTZComboBox.TabIndex = 28;
            this._UROVDTZComboBox.Visible = false;
            // 
            // _oscDTZComboBox
            // 
            this._oscDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscDTZComboBox.FormattingEnabled = true;
            this._oscDTZComboBox.Location = new System.Drawing.Point(148, 393);
            this._oscDTZComboBox.Name = "_oscDTZComboBox";
            this._oscDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._oscDTZComboBox.TabIndex = 29;
            // 
            // _modeI2I1CB
            // 
            this._modeI2I1CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._modeI2I1CB.FormattingEnabled = true;
            this._modeI2I1CB.Location = new System.Drawing.Point(125, 99);
            this._modeI2I1CB.Name = "_modeI2I1CB";
            this._modeI2I1CB.Size = new System.Drawing.Size(90, 21);
            this._modeI2I1CB.TabIndex = 40;
            // 
            // groupBox25
            // 
            this.groupBox25.Location = new System.Drawing.Point(6, 185);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(261, 73);
            this.groupBox25.TabIndex = 41;
            this.groupBox25.TabStop = false;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(6, 49);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(97, 13);
            this.label75.TabIndex = 38;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.ForeColor = System.Drawing.Color.Red;
            this.label74.Location = new System.Drawing.Point(233, 49);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(15, 13);
            this.label74.TabIndex = 39;
            // 
            // _I5I1TextBox
            // 
            this._I5I1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I5I1TextBox.Location = new System.Drawing.Point(141, 46);
            this._I5I1TextBox.Name = "_I5I1TextBox";
            this._I5I1TextBox.Size = new System.Drawing.Size(90, 20);
            this._I5I1TextBox.TabIndex = 37;
            this._I5I1TextBox.Tag = "100";
            this._I5I1TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 24);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(78, 13);
            this.label24.TabIndex = 41;
            // 
            // _perBlockI5I1
            // 
            this._perBlockI5I1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._perBlockI5I1.FormattingEnabled = true;
            this._perBlockI5I1.Location = new System.Drawing.Point(141, 21);
            this._perBlockI5I1.Name = "_perBlockI5I1";
            this._perBlockI5I1.Size = new System.Drawing.Size(90, 21);
            this._perBlockI5I1.TabIndex = 42;
            // 
            // _modeI5I1CB
            // 
            this._modeI5I1CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._modeI5I1CB.FormattingEnabled = true;
            this._modeI5I1CB.Location = new System.Drawing.Point(125, 181);
            this._modeI5I1CB.Name = "_modeI5I1CB";
            this._modeI5I1CB.Size = new System.Drawing.Size(90, 21);
            this._modeI5I1CB.TabIndex = 42;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(6, 416);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(29, 13);
            this.label130.TabIndex = 43;
            this.label130.Visible = false;
            // 
            // _APVDTZComboBox
            // 
            this._APVDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._APVDTZComboBox.FormattingEnabled = true;
            this._APVDTZComboBox.Location = new System.Drawing.Point(148, 413);
            this._APVDTZComboBox.Name = "_APVDTZComboBox";
            this._APVDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._APVDTZComboBox.TabIndex = 44;
            this._APVDTZComboBox.Visible = false;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(6, 436);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(28, 13);
            this.label131.TabIndex = 45;
            this.label131.Visible = false;
            // 
            // _AVRDTZComboBox
            // 
            this._AVRDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._AVRDTZComboBox.FormattingEnabled = true;
            this._AVRDTZComboBox.Location = new System.Drawing.Point(148, 433);
            this._AVRDTZComboBox.Name = "_AVRDTZComboBox";
            this._AVRDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._AVRDTZComboBox.TabIndex = 46;
            this._AVRDTZComboBox.Visible = false;
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "bin";
            this._openConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "Открыть уставки для МР902";
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "bin";
            this._saveConfigurationDlg.FileName = "МР901_уставки";
            this._saveConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._saveConfigurationDlg.Title = "Сохранить  уставки для МР902";
            // 
            // _progressBar
            // 
            this._progressBar.Maximum = 110;
            this._progressBar.Name = "_progressBar";
            this._progressBar.Size = new System.Drawing.Size(100, 16);
            this._progressBar.Step = 1;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _statusStrip
            // 
            this._statusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._progressBar,
            this._statusLabel});
            this._statusStrip.Location = new System.Drawing.Point(0, 620);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this._statusStrip.Size = new System.Drawing.Size(1032, 22);
            this._statusStrip.TabIndex = 26;
            this._statusStrip.Text = "statusStrip1";
            // 
            // _saveToXmlButton
            // 
            this._saveToXmlButton.AllowDrop = true;
            this._saveToXmlButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveToXmlButton.AutoSize = true;
            this._saveToXmlButton.Location = new System.Drawing.Point(916, 594);
            this._saveToXmlButton.Name = "_saveToXmlButton";
            this._saveToXmlButton.Size = new System.Drawing.Size(112, 23);
            this._saveToXmlButton.TabIndex = 0;
            this._saveToXmlButton.Text = "Сохранить в HTML";
            this._saveToXmlButton.UseVisualStyleBackColor = true;
            this._saveToXmlButton.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _resetSetpointsButton
            // 
            this._resetSetpointsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._resetSetpointsButton.AutoSize = true;
            this._resetSetpointsButton.Location = new System.Drawing.Point(423, 594);
            this._resetSetpointsButton.Name = "_resetSetpointsButton";
            this._resetSetpointsButton.Size = new System.Drawing.Size(108, 23);
            this._resetSetpointsButton.TabIndex = 4;
            this._resetSetpointsButton.Text = "Обнулить уставки";
            this._resetSetpointsButton.UseVisualStyleBackColor = true;
            this._resetSetpointsButton.Click += new System.EventHandler(this._clearSetpointsButton_Click);
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeConfigBut.AutoSize = true;
            this._writeConfigBut.Location = new System.Drawing.Point(155, 594);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(136, 23);
            this._writeConfigBut.TabIndex = 5;
            this._writeConfigBut.Text = "Записать в устройство";
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readConfigBut.AutoSize = true;
            this._readConfigBut.Location = new System.Drawing.Point(4, 594);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(145, 23);
            this._readConfigBut.TabIndex = 6;
            this._readConfigBut.Text = "Прочитать из устройства";
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.AllowDrop = true;
            this._saveConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveConfigBut.AutoSize = true;
            this._saveConfigBut.Location = new System.Drawing.Point(802, 594);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(108, 23);
            this._saveConfigBut.TabIndex = 1;
            this._saveConfigBut.Text = "Сохранить в файл";
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._loadConfigBut.AutoSize = true;
            this._loadConfigBut.Location = new System.Drawing.Point(677, 594);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(119, 23);
            this._loadConfigBut.TabIndex = 2;
            this._loadConfigBut.Text = "Загрузить из файла";
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _saveXmlDialog
            // 
            this._saveXmlDialog.Filter = "(*.xml) | *.xml";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.AutoSize = true;
            this.button1.Location = new System.Drawing.Point(537, 594);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(136, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Загрузить баз. уставки";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this._resetSetpointsButton_Click);
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.HeaderText = "№";
            this.dataGridViewTextBoxColumn25.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn25.Width = 40;
            // 
            // dataGridViewComboBoxColumn26
            // 
            this.dataGridViewComboBoxColumn26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn26.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn26.HeaderText = "Тип";
            this.dataGridViewComboBoxColumn26.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn26.Name = "dataGridViewComboBoxColumn26";
            this.dataGridViewComboBoxColumn26.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn26.Width = 32;
            // 
            // dataGridViewComboBoxColumn28
            // 
            this.dataGridViewComboBoxColumn28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn28.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn28.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn28.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn28.Name = "dataGridViewComboBoxColumn28";
            this.dataGridViewComboBoxColumn28.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1032, 642);
            this.Controls.Add(this._saveToXmlButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this._resetSetpointsButton);
            this.Controls.Add(this._writeConfigBut);
            this.Controls.Add(this._readConfigBut);
            this.Controls.Add(this._saveConfigBut);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this._statusStrip);
            this.Controls.Add(this._configurationTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "ConfigurationForm";
            this.Text = "Конфигурация";
            this.Activated += new System.EventHandler(this.ConfigurationForm_Activated);
            this.Load += new System.EventHandler(this.Configuration_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Mr902ConfigurationForm_KeyUp);
            this.VLS2.ResumeLayout(false);
            this.VLS3.ResumeLayout(false);
            this.VLSTabControl.ResumeLayout(false);
            this.VLS1.ResumeLayout(false);
            this.VLS4.ResumeLayout(false);
            this.VLS5.ResumeLayout(false);
            this.VLS6.ResumeLayout(false);
            this.VLS7.ResumeLayout(false);
            this.VLS8.ResumeLayout(false);
            this.VLS9.ResumeLayout(false);
            this.VLS10.ResumeLayout(false);
            this.VLS11.ResumeLayout(false);
            this.VLS12.ResumeLayout(false);
            this.VLS13.ResumeLayout(false);
            this.VLS14.ResumeLayout(false);
            this.VLS15.ResumeLayout(false);
            this.VLS16.ResumeLayout(false);
            this._allDefensesPage.ResumeLayout(false);
            this.groupBox36.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox31.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this._difensesTC.ResumeLayout(false);
            this.tabPage21.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difMDataGrid)).EndInit();
            this.groupBox20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difDDataGrid)).EndInit();
            this.tabPage22.ResumeLayout(false);
            this.groupBox21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._MTZDifensesDataGrid)).EndInit();
            this.tabPage19.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._externalDifensesDataGrid)).EndInit();
            this.groupBox13.ResumeLayout(false);
            this._inputSygnalsPage.ResumeLayout(false);
            this.groupBox34.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._antiBounceDataGridView)).EndInit();
            this.groupBox15.ResumeLayout(false);
            this.groupBox32.ResumeLayout(false);
            this.groupBox32.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals9)).EndInit();
            this.tabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals10)).EndInit();
            this.tabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals11)).EndInit();
            this.tabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals12)).EndInit();
            this.tabPage13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals13)).EndInit();
            this.tabPage14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals14)).EndInit();
            this.tabPage15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals15)).EndInit();
            this.tabPage16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals16)).EndInit();
            this.groupBox14.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals3)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals4)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals5)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals6)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals7)).EndInit();
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals8)).EndInit();
            this._configurationTabControl.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this._joinPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._joinData)).EndInit();
            this.tabPage20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._configTtDgv)).EndInit();
            this._urovPage.ResumeLayout(false);
            this._urovPage.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.urovGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._UROVJoinData)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox33.ResumeLayout(false);
            this.groupBox33.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this._relayIndPage.ResumeLayout(false);
            this._confIndGB.ResumeLayout(false);
            this._confIndGB.PerformLayout();
            this.groupBox175.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).EndInit();
            this.groupBox35.ResumeLayout(false);
            this.groupBox35.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this.tabPage23.ResumeLayout(false);
            this.groupBox55.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._virtualReleDataGrid)).EndInit();
            this.groupBox53.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._rsTriggersDataGrid)).EndInit();
            this._outputSignalsPage.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            this._systemPage.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._oscChannelsWithBaseGrid)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.tabPage17.ResumeLayout(false);
            this.tabPage17.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this._ethernetPage.ResumeLayout(false);
            this._reserveGroupBox.ResumeLayout(false);
            this.groupBox51.ResumeLayout(false);
            this.groupBox51.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dif0DataGreed)).EndInit();
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckedListBox VLScheckedListBox16;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox13;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox15;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox14;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox12;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox11;
        private System.Windows.Forms.TabPage VLS2;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox2;
        private System.Windows.Forms.TabPage VLS3;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox3;
        private System.Windows.Forms.TabControl VLSTabControl;
        private System.Windows.Forms.TabPage VLS1;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox1;
        private System.Windows.Forms.TabPage VLS4;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox4;
        private System.Windows.Forms.TabPage VLS5;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox5;
        private System.Windows.Forms.TabPage VLS6;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox6;
        private System.Windows.Forms.TabPage VLS7;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox7;
        private System.Windows.Forms.TabPage VLS8;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox8;
        private System.Windows.Forms.TabPage VLS9;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox9;
        private System.Windows.Forms.TabPage VLS10;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox10;
        private System.Windows.Forms.TabPage VLS11;
        private System.Windows.Forms.TabPage VLS12;
        private System.Windows.Forms.TabPage VLS13;
        private System.Windows.Forms.TabPage VLS14;
        private System.Windows.Forms.TabPage VLS15;
        private System.Windows.Forms.TabPage VLS16;
        private System.Windows.Forms.TabPage _allDefensesPage;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TabPage _inputSygnalsPage;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.DataGridView _inputSignals9;
        private System.Windows.Forms.DataGridViewTextBoxColumn _signalValueNumILI;
        private System.Windows.Forms.DataGridViewComboBoxColumn _signalValueColILI;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.DataGridView _inputSignals10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn8;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.DataGridView _inputSignals11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn9;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.DataGridView _inputSignals12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn10;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.DataGridView _inputSignals13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn11;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.DataGridView _inputSignals14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn12;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.DataGridView _inputSignals15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn13;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.DataGridView _inputSignals16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn14;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView _inputSignals1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _lsChannelCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _signalValueCol;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView _inputSignals2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView _inputSignals3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView _inputSignals4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView _inputSignals5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.DataGridView _inputSignals6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.DataGridView _inputSignals7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.DataGridView _inputSignals8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn7;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.TabControl _configurationTabControl;
        private System.Windows.Forms.TabPage _joinPage;
        private System.Windows.Forms.TabPage _outputSignalsPage;
        private System.Windows.Forms.TabControl _difensesTC;
        private System.Windows.Forms.TabPage tabPage21;
        private System.Windows.Forms.TabPage tabPage22;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.DataGridView _MTZDifensesDataGrid;
        private System.Windows.Forms.TabPage tabPage19;
        private System.Windows.Forms.DataGridView _dif0DataGreed;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0AVRColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0APVColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0UrovColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0OscColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0Intg2Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0Ib2Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0Intg1Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0Ib1Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0TdColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0InColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0IdColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0BlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0ModeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0StageColumn;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.ComboBox _modeDTOBTComboBox;
        private System.Windows.Forms.ComboBox _stepOnInstantValuesDTOBTComboBox;
        private System.Windows.Forms.ComboBox _UROVDTOBTComboBox;
        private System.Windows.Forms.ComboBox _oscDTOBTComboBox;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.ComboBox _blockingDTOBTComboBox;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.MaskedTextBox _constraintDTOBTTextBox;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.MaskedTextBox _timeEnduranceDTOBTTextBox;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.ComboBox _APVDTOBTComboBox;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.ComboBox _AVRDTOBTComboBox;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.ComboBox _modeDTZComboBox;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.MaskedTextBox _Ib1BeginTextBox;
        private System.Windows.Forms.MaskedTextBox _K1AngleOfSlopeTextBox;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.MaskedTextBox _Ib2BeginTextBox;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.MaskedTextBox _K2TangensTextBox;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.MaskedTextBox _constraintDTZTextBox;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.MaskedTextBox _I2I1TextBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox _perBlockI2I1;
        private System.Windows.Forms.MaskedTextBox _timeEnduranceDTZTextBox;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.ComboBox _blockingDTZComboBox;
        private System.Windows.Forms.ComboBox _UROVDTZComboBox;
        private System.Windows.Forms.ComboBox _oscDTZComboBox;
        private System.Windows.Forms.ComboBox _modeI2I1CB;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.MaskedTextBox _I5I1TextBox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox _perBlockI5I1;
        private System.Windows.Forms.ComboBox _modeI5I1CB;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.ComboBox _APVDTZComboBox;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.ComboBox _AVRDTZComboBox;
        private System.Windows.Forms.TabPage _urovPage;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.DataGridView _externalDifensesDataGrid;
        private System.Windows.Forms.DataGridView _joinData;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox _DZHSH2;
        private System.Windows.Forms.ComboBox _DZHSH1;
        private System.Windows.Forms.ComboBox _DZHSelf;
        private System.Windows.Forms.ComboBox _DZHUrov3;
        private System.Windows.Forms.ComboBox _DZHKontr;
        private System.Windows.Forms.ComboBox _DZHUrov2;
        private System.Windows.Forms.MaskedTextBox _DZHTUrov3;
        private System.Windows.Forms.MaskedTextBox _DZHTUrov2;
        private System.Windows.Forms.MaskedTextBox _DZHTUrov1;
        private System.Windows.Forms.ComboBox _DZHPO;
        private System.Windows.Forms.GroupBox urovGroup;
        private System.Windows.Forms.DataGridView _UROVJoinData;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.TabPage _systemPage;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.MaskedTextBox _oscWriteLength;
        private System.Windows.Forms.ComboBox _oscFix;
        private System.Windows.Forms.ComboBox _oscLength;
        private System.Windows.Forms.TabPage tabPage20;
        private System.Windows.Forms.DataGridView _configTtDgv;
        private System.Windows.Forms.MaskedTextBox _oscSizeTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ToolStripProgressBar _progressBar;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.StatusStrip _statusStrip;
        private System.Windows.Forms.Button _saveToXmlButton;
        private System.Windows.Forms.Button _resetSetpointsButton;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.SaveFileDialog _saveXmlDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifModesColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifOtklColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifBlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifSrabColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifTsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifTvzColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifVozvrColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifVozvrYNColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifOscColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifUROVColumn;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem clearSetpointsItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToHtmlItem;
        private System.Windows.Forms.ComboBox _inpOscComboBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TabPage _relayIndPage;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.CheckBox _fault4CheckBox;
        private System.Windows.Forms.CheckBox _fault3CheckBox;
        private System.Windows.Forms.CheckBox _fault2CheckBox;
        private System.Windows.Forms.CheckBox _fault1CheckBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.MaskedTextBox _impTB;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.DataGridView _outputReleGrid;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.ComboBox _inpResetTtcomboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn _nameTtColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _idTtColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeSrabTtColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _faultTtColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _resetTtCol;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView _difMDataGrid;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.DataGridView _difDDataGrid;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem resetSetpointsItem;
        private System.Windows.Forms.TabPage _ethernetPage;
        private System.Windows.Forms.GroupBox groupBox51;
        private System.Windows.Forms.MaskedTextBox _ipLo1;
        private System.Windows.Forms.MaskedTextBox _ipLo2;
        private System.Windows.Forms.MaskedTextBox _ipHi1;
        private System.Windows.Forms.MaskedTextBox _ipHi2;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.Label label213;
        private System.Windows.Forms.Label label212;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.DataGridView _oscChannelsWithBaseGrid;
        private System.Windows.Forms.TreeView treeViewForVLS;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.TabPage tabPage17;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.ComboBox goin64;
        private System.Windows.Forms.ComboBox goin48;
        private System.Windows.Forms.ComboBox goin32;
        private System.Windows.Forms.Label label281;
        private System.Windows.Forms.Label label282;
        private System.Windows.Forms.Label label283;
        private System.Windows.Forms.ComboBox goin63;
        private System.Windows.Forms.ComboBox goin47;
        private System.Windows.Forms.ComboBox goin31;
        private System.Windows.Forms.Label label284;
        private System.Windows.Forms.Label label285;
        private System.Windows.Forms.Label label286;
        private System.Windows.Forms.ComboBox goin62;
        private System.Windows.Forms.ComboBox goin46;
        private System.Windows.Forms.ComboBox goin30;
        private System.Windows.Forms.Label label287;
        private System.Windows.Forms.Label label288;
        private System.Windows.Forms.Label label289;
        private System.Windows.Forms.ComboBox goin61;
        private System.Windows.Forms.ComboBox goin45;
        private System.Windows.Forms.ComboBox goin29;
        private System.Windows.Forms.Label label290;
        private System.Windows.Forms.Label label291;
        private System.Windows.Forms.Label label292;
        private System.Windows.Forms.ComboBox goin60;
        private System.Windows.Forms.ComboBox goin44;
        private System.Windows.Forms.ComboBox goin28;
        private System.Windows.Forms.Label label293;
        private System.Windows.Forms.Label label294;
        private System.Windows.Forms.Label label295;
        private System.Windows.Forms.ComboBox goin59;
        private System.Windows.Forms.ComboBox goin43;
        private System.Windows.Forms.ComboBox goin27;
        private System.Windows.Forms.Label label296;
        private System.Windows.Forms.Label label297;
        private System.Windows.Forms.Label label298;
        private System.Windows.Forms.ComboBox goin58;
        private System.Windows.Forms.ComboBox goin42;
        private System.Windows.Forms.ComboBox goin26;
        private System.Windows.Forms.Label label299;
        private System.Windows.Forms.Label label300;
        private System.Windows.Forms.Label label301;
        private System.Windows.Forms.ComboBox goin57;
        private System.Windows.Forms.ComboBox goin41;
        private System.Windows.Forms.ComboBox goin25;
        private System.Windows.Forms.Label label302;
        private System.Windows.Forms.Label label303;
        private System.Windows.Forms.Label label304;
        private System.Windows.Forms.ComboBox goin56;
        private System.Windows.Forms.ComboBox goin40;
        private System.Windows.Forms.ComboBox goin24;
        private System.Windows.Forms.Label label305;
        private System.Windows.Forms.Label label306;
        private System.Windows.Forms.Label label307;
        private System.Windows.Forms.ComboBox goin55;
        private System.Windows.Forms.ComboBox goin39;
        private System.Windows.Forms.ComboBox goin23;
        private System.Windows.Forms.Label label308;
        private System.Windows.Forms.Label label309;
        private System.Windows.Forms.Label label310;
        private System.Windows.Forms.ComboBox goin54;
        private System.Windows.Forms.ComboBox goin38;
        private System.Windows.Forms.ComboBox goin22;
        private System.Windows.Forms.Label label311;
        private System.Windows.Forms.Label label312;
        private System.Windows.Forms.Label label313;
        private System.Windows.Forms.ComboBox goin53;
        private System.Windows.Forms.ComboBox goin37;
        private System.Windows.Forms.ComboBox goin21;
        private System.Windows.Forms.Label label314;
        private System.Windows.Forms.Label label315;
        private System.Windows.Forms.Label label316;
        private System.Windows.Forms.ComboBox goin52;
        private System.Windows.Forms.ComboBox goin36;
        private System.Windows.Forms.ComboBox goin20;
        private System.Windows.Forms.Label label317;
        private System.Windows.Forms.Label label318;
        private System.Windows.Forms.Label label319;
        private System.Windows.Forms.ComboBox goin51;
        private System.Windows.Forms.ComboBox goin35;
        private System.Windows.Forms.ComboBox goin19;
        private System.Windows.Forms.Label label320;
        private System.Windows.Forms.Label label321;
        private System.Windows.Forms.Label label322;
        private System.Windows.Forms.ComboBox goin50;
        private System.Windows.Forms.ComboBox goin34;
        private System.Windows.Forms.ComboBox goin18;
        private System.Windows.Forms.Label label323;
        private System.Windows.Forms.Label label324;
        private System.Windows.Forms.Label label325;
        private System.Windows.Forms.ComboBox goin49;
        private System.Windows.Forms.Label label326;
        private System.Windows.Forms.ComboBox goin33;
        private System.Windows.Forms.Label label327;
        private System.Windows.Forms.ComboBox goin17;
        private System.Windows.Forms.Label label328;
        private System.Windows.Forms.ComboBox goin16;
        private System.Windows.Forms.Label label329;
        private System.Windows.Forms.ComboBox goin15;
        private System.Windows.Forms.Label label330;
        private System.Windows.Forms.ComboBox goin14;
        private System.Windows.Forms.Label label331;
        private System.Windows.Forms.ComboBox goin13;
        private System.Windows.Forms.Label label332;
        private System.Windows.Forms.ComboBox goin12;
        private System.Windows.Forms.Label label333;
        private System.Windows.Forms.ComboBox goin11;
        private System.Windows.Forms.Label label334;
        private System.Windows.Forms.ComboBox goin10;
        private System.Windows.Forms.Label label335;
        private System.Windows.Forms.ComboBox goin9;
        private System.Windows.Forms.Label label336;
        private System.Windows.Forms.ComboBox goin8;
        private System.Windows.Forms.Label label337;
        private System.Windows.Forms.ComboBox goin7;
        private System.Windows.Forms.Label label338;
        private System.Windows.Forms.ComboBox goin6;
        private System.Windows.Forms.Label label339;
        private System.Windows.Forms.ComboBox goin5;
        private System.Windows.Forms.Label label340;
        private System.Windows.Forms.ComboBox goin4;
        private System.Windows.Forms.Label label341;
        private System.Windows.Forms.ComboBox goin3;
        private System.Windows.Forms.Label label342;
        private System.Windows.Forms.ComboBox goin2;
        private System.Windows.Forms.Label label343;
        private System.Windows.Forms.ComboBox goin1;
        private System.Windows.Forms.Label label344;
        private System.Windows.Forms.Label label345;
        private System.Windows.Forms.ComboBox operationBGS;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox currentBGS;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button _applyCopySetpoinsButton;
        private System.Windows.Forms.ComboBox _copySetpoinsGroupComboBox;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.ComboBox _setpointsComboBox;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.ComboBox _indComboBox;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox _grUst2ComboBox;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox _grUst1ComboBox;
        private System.Windows.Forms.TabPage tabPage23;
        private System.Windows.Forms.GroupBox groupBox55;
        private System.Windows.Forms.GroupBox groupBox53;
        private System.Windows.Forms.DataGridView _rsTriggersDataGrid;
        private System.Windows.Forms.ComboBox _DZHUROVself;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox _DZHUROVblock;
        private System.Windows.Forms.GroupBox groupBox33;
        private System.Windows.Forms.ComboBox _DZHSHBLOCK1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox _DZHSHBLOCK2;
        private System.Windows.Forms.ComboBox _DZHPOBLOCK1;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.CheckBox _fault5CheckBox;
        private System.Windows.Forms.Label _fault5Label;
        private System.Windows.Forms.GroupBox groupBox35;
        private System.Windows.Forms.CheckBox _resetAlarmCheckBox;
        private System.Windows.Forms.CheckBox _resetSystemCheckBox;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox36;
        private System.Windows.Forms.ComboBox _inpAddCombo;
        private System.Windows.Forms.GroupBox groupBox175;
        private System.Windows.Forms.DataGridView _outputIndicatorsGrid;
        private System.Windows.Forms.ComboBox _DZHConn;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.GroupBox _reserveGroupBox;
        private System.Windows.Forms.ComboBox _reserveCB;
        private System.Windows.Forms.DataGridView _virtualReleDataGrid;
        private System.Windows.Forms.GroupBox groupBox34;
        private System.Windows.Forms.DataGridView _antiBounceDataGridView;
        private System.Windows.Forms.CheckBox _poCheckBox;
        private System.Windows.Forms.CheckBox _sh2CheckBox;
        private System.Windows.Forms.CheckBox _sh1CheckBox;
        private System.Windows.Forms.GroupBox _confIndGB;
        private System.Windows.Forms.ComboBox _configIgSshComboBox;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.MaskedTextBox _tOtklTextBox;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.DataGridViewTextBoxColumn _mtzStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _mtzModesColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _mtzBlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _LogicCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _mtzMeasureColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _mtzICPColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _mtzCharColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _mtzTColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _mtzkColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _mtzOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _mtzUROVColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn _joinITT;
        private System.Windows.Forms.DataGridViewComboBoxColumn _joinSwitchOFF;
        private System.Windows.Forms.DataGridViewComboBoxColumn _joinSwitchOn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _joinJoin;
        private System.Windows.Forms.DataGridViewComboBoxColumn _joinEnter;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _joinResetColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeResetJoinColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn _JoinIUROV;
        private System.Windows.Forms.DataGridViewTextBoxColumn _JoinTUROV;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn baseGreenColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignalCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn baseRedColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _out1IndSignalCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignal2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn baseColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releWaitCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn23;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn24;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn65;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn66;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn67;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewComboBoxColumn _baseCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn22;
        private System.Windows.Forms.Button _wrapBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difMStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _difMModesColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _difMBlockColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difMIcpColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difMIdoColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difMIbColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difMfColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column8;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _difMOchColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difMIochColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iOchMgColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difMtochColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _difMEnterOchColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _difMOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _difMUrovColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _difDModesColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _difDBlockColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDIcpColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDIdoColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDtcpColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDIbColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDfColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _difDBlockGColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDI2gColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _difDBlock5GColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDI5gColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _difDOprNasColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _difDOchColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDIochColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iOchColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDtochColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _difDEnterOchColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _difDOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _difDUrovColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn26;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn28;
    }
}