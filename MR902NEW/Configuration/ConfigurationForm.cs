﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.TreeView;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR902NEW.Configuration.Structures;
using BEMN.MR902NEW.Configuration.Structures.AntiBounce;
using BEMN.MR902NEW.Configuration.Structures.ConfigSystem;
using BEMN.MR902NEW.Configuration.Structures.Connections;
using BEMN.MR902NEW.Configuration.Structures.Defenses;
using BEMN.MR902NEW.Configuration.Structures.Defenses.Differential;
using BEMN.MR902NEW.Configuration.Structures.Defenses.External;
using BEMN.MR902NEW.Configuration.Structures.Defenses.Mtz;
using BEMN.MR902NEW.Configuration.Structures.Goose;
using BEMN.MR902NEW.Configuration.Structures.Ls;
using BEMN.MR902NEW.Configuration.Structures.Osc;
using BEMN.MR902NEW.Configuration.Structures.Oscope;
using BEMN.MR902NEW.Configuration.Structures.RelayIndicator;
using BEMN.MR902NEW.Configuration.Structures.RSTriggers;
using BEMN.MR902NEW.Configuration.Structures.Tt;
using BEMN.MR902NEW.Configuration.Structures.Urov;
using BEMN.MR902NEW.Configuration.Structures.Vls;
using BEMN.MR902NEW.Properties;

namespace BEMN.MR902NEW.Configuration
{
    public partial class ConfigurationForm : Form, IFormView
    {
        #region [Constants]

        private const string FILE_SAVE_FAIL = "Невозможно сохранить файл";
        private const string FILE_LOAD_FAIL = "Невозможно загрузить файл";
        private const string XML_HEAD = "MR902";
        private const string XML_FILE_NAME = "МР902_Уставки_версия_{0}_{1}.bin";
        private const string MR902_BASE_CONFIG_PATH = "\\MR902\\MR902_BaseConfig_v{0}_{1}.bin";

        private const string ERROR_SETPOINTS_VALUE =
            "Обнаружены некорректные уставки. Конфигурация не может быть записана.";

        private const string INVALID_PORT = "Порт недоступен.";
        private const string READ_OK = "Конфигурация успешно прочитана";
        private const string READ_FAIL = "Не удалось прочитать конфигурацию";
        private const string WRITE_OK = "Конфигурация успешно записана";
        private const string WRITE_FAIL = "Невозможно записать конфигурацию";
        private string _ipHi2Mem;
        private string _ipHi1Mem;
        private string _ipLo2Mem;
        private string _ipLo1Mem;

        #endregion [Constants]

        #region Поля


        private Mr902New _device;
        private MemoryEntity<ConfigurationStruct> _configuration;
        private ConfigurationStruct _currentSetpointsStruct;

        private StructUnion<GroupSetpoint> _setpointValidator;
        private StructUnion<ConfigurationStruct> _configurationValidator;

        private List<CheckedListBox> allVlsCheckedListBoxs = new List<CheckedListBox>();
        private SetpointsValidator<AllDefensesSetpointsStruct, GroupSetpoint> _setpointsValidator;
        private ComboboxSelector _groupSetpointSelector;
        #endregion


        #region Конструкторы

        public ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public ConfigurationForm(Mr902New device)
        {
            this.InitializeComponent();
            this._device = device;
            Strings.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);
            
            ConfigurationStruct.DeviceModelType = this._device.DevicePlant;
            
            this._configuration = device.Configuration;

            this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
            this._configuration.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._configuration.RemoveStructQueries();
                this.ConfigurationReadFail();
            });

            this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, ConfigurationWriteOk);
            this._configuration.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._configuration.RemoveStructQueries();
                this.ConfigurationWriteFail();
            });
            
            this._configuration.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
            this._configuration.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);

            this.Initialization();
            
        }

        private void Initialization()
        {
            this._progressBar.Maximum = this._configuration.Slots.Count;
            this._currentSetpointsStruct = new ConfigurationStruct();

            this._groupSetpointSelector = new ComboboxSelector(this._setpointsComboBox, Strings.GroupsNames);

            this._copySetpoinsGroupComboBox.DataSource = Strings.CopyGroupsNames;
            #region [Реле и Индикаторы]

            NewStructValidator<ConfigAddStruct> configAddValidator = new NewStructValidator<ConfigAddStruct>
            (this._toolTip,
                new ControlInfoCombo(this._inpAddCombo, Strings.InpOporSignals),
                new ControlInfoCheck(this._resetSystemCheckBox),
                new ControlInfoCheck(this._resetAlarmCheckBox)
            );

            Func<string, Dictionary<ushort, string>> func = str =>
            {
                if (string.IsNullOrEmpty(str)) return Strings.OscChannelSignals[0];
                int index = Strings.OscBases.IndexOf(str);
                return index != -1 ? Strings.OscChannelSignals[index] : Strings.OscChannelSignals[0];
            };
            
            DgvValidatorWithDepend<AllReleStruct, ReleOutputStruct> relayValidator =
                new DgvValidatorWithDepend<AllReleStruct, ReleOutputStruct>
                (
                    new[] { this._outputReleGrid, this._virtualReleDataGrid },
                    new[] { AllReleStruct.CurrentCount, AllReleStruct.RELAY_MEMORY_COUNT - AllReleStruct.CurrentCount },
                    this._toolTip,
                    new ColumnInfoCombo(Strings.RelayNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.SignalType),
                    new ColumnInfoComboControl(Strings.OscBases, 3),
                    new ColumnInfoDictionaryComboDependent(func, 2),
                    new ColumnInfoText(RulesContainer.TimeRule)
                );

            this._outputReleGrid.CellBeginEdit += this.GridOnCellBeginEdit;
            this._virtualReleDataGrid.CellBeginEdit += this.GridOnCellBeginEdit;

            DgvValidatorWithDepend<AllIndicatorsStruct, IndicatorsStruct> indicatorValidator =
                new DgvValidatorWithDepend<AllIndicatorsStruct, IndicatorsStruct>
                (
                    this._outputIndicatorsGrid,
                    AllIndicatorsStruct.COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.IndNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.ReleyType),
                    new ColumnInfoComboControl(Strings.OscBases, 3),
                    new ColumnInfoDictionaryComboDependent(func, 2),
                    new ColumnInfoComboControl(Strings.OscBases, 5),
                    new ColumnInfoDictionaryComboDependent(func, 4),
                    new ColumnInfoCombo(Strings.ModeRele)
                );
            this._outputIndicatorsGrid.CellBeginEdit += this.GridOnCellBeginEdit;

            NewStructValidator<FaultStruct> faultValidator = new NewStructValidator<FaultStruct>
            (
                this._toolTip,
                new ControlInfoCheck(this._fault1CheckBox),
                new ControlInfoCheck(this._fault2CheckBox),
                new ControlInfoCheck(this._fault3CheckBox),
                new ControlInfoCheck(this._fault4CheckBox),
                new ControlInfoCheck(this._fault5CheckBox),
                new ControlInfoText(this._impTB, RulesContainer.TimeRule)
            );

            #endregion [Реле и Индикаторы]

            #region [ВЛС]

            allVlsCheckedListBoxs.Add(this.VLScheckedListBox1);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox2);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox3);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox4);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox5);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox6);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox7);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox8);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox9);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox10);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox11);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox12);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox13);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox14);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox15);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox16);

            CheckedListBox[] vlsBoxes =
            {
                this.VLScheckedListBox1,
                this.VLScheckedListBox2,
                this.VLScheckedListBox3,
                this.VLScheckedListBox4,
                this.VLScheckedListBox5,
                this.VLScheckedListBox6,
                this.VLScheckedListBox7,
                this.VLScheckedListBox8,
                this.VLScheckedListBox9,
                this.VLScheckedListBox10,
                this.VLScheckedListBox11,
                this.VLScheckedListBox12,
                this.VLScheckedListBox13,
                this.VLScheckedListBox14,
                this.VLScheckedListBox15,
                this.VLScheckedListBox16
            };
            NewCheckedListBoxValidator<OutputLogicStruct>[] vlsValidator =
                new NewCheckedListBoxValidator<OutputLogicStruct>[AllOutputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < AllOutputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                vlsValidator[i] =
                    new NewCheckedListBoxValidator<OutputLogicStruct>(vlsBoxes[i], Strings.VlsSignals);
            }
            StructUnion<AllOutputLogicSignalStruct> vlsUnion =
                new StructUnion<AllOutputLogicSignalStruct>(vlsValidator);

            #endregion [ВЛС]

            #region [ЛС]

            DataGridView[] lsBoxes =
            {
                this._inputSignals1, this._inputSignals2, this._inputSignals3, this._inputSignals4,
                this._inputSignals5, this._inputSignals6, this._inputSignals7, this._inputSignals8,
                this._inputSignals9, this._inputSignals10, this._inputSignals11, this._inputSignals12,
                this._inputSignals13, this._inputSignals14, this._inputSignals15, this._inputSignals16
            };

            foreach (DataGridView gridView in lsBoxes)
            {
                gridView.CellBeginEdit += this.GridOnCellBeginEdit;
            }

            NewDgwValidatior<InputLogicStruct>[] inputLogicValidator =
                new NewDgwValidatior<InputLogicStruct>[AllInputLogicSignalStruct.LOGIC_COUNT];

            for (int i = 0; i < AllInputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                inputLogicValidator[i] = new NewDgwValidatior<InputLogicStruct>
                (
                    lsBoxes[i],
                    InputLogicStruct.DiscrestCount,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.LogicSignalNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.LogicValues)
                );
            }
            StructUnion<AllInputLogicSignalStruct> inputLogicUnion =
                new StructUnion<AllInputLogicSignalStruct>(inputLogicValidator);

            #endregion [ЛС]

            NewDgwValidatior<AllControlTtStruct, ControlTtStruct> ttValidator =
                new NewDgwValidatior<AllControlTtStruct, ControlTtStruct>
                (
                    this._configTtDgv,
                    AllControlTtStruct.TT_COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.TtNames, ColumnsType.NAME),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoCombo(Strings.TtFault),
                    new ColumnInfoCombo(Strings.ResetTT)
                );

            NewStructValidator<ControlTtUnionStruct> newTtValidator = new NewStructValidator<ControlTtUnionStruct>
            (
                this._toolTip,
                new ControlInfoValidator(ttValidator),
                new ControlInfoCombo(this._inpResetTtcomboBox, Strings.SwitchSignals)
            );

            NewStructValidator<UrovStruct> urovValidator = new NewStructValidator<UrovStruct>
            (
                this._toolTip,
                new ControlInfoCombo(this._DZHKontr, Strings.KONTR),
                new ControlInfoCombo(this._DZHSelf, Strings.Forbidden),
                new ControlInfoText(this._DZHTUrov1, RulesContainer.TimeRule),
                new ControlInfoCombo(this._DZHUrov2, Strings.Forbidden),
                new ControlInfoText(this._DZHTUrov2, RulesContainer.TimeRule),
                new ControlInfoCombo(this._DZHUrov3, Strings.Forbidden),
                new ControlInfoText(this._DZHTUrov3, RulesContainer.TimeRule),
                
                new ControlInfoCombo(this._DZHSH1, Strings.InputSignals),
                new ControlInfoCombo(this._DZHSH2, Strings.InputSignals),
                new ControlInfoCombo(this._DZHPO, Strings.InputSignals),

                new ControlInfoCombo(this._DZHSHBLOCK1, Strings.InputSignals),
                new ControlInfoCombo(this._DZHSHBLOCK2, Strings.InputSignals),
                new ControlInfoCombo(this._DZHPOBLOCK1, Strings.InputSignals),

                new ControlInfoCombo(this._DZHUROVself, Strings.Forbidden),
                new ControlInfoCombo(this._DZHUROVblock, Strings.InputSignals),
                new ControlInfoCombo(this._DZHConn, Strings.Forbidden),

                new ControlInfoText(this._tOtklTextBox, RulesContainer.TimeRule)
            );

            NewStructValidator<InputSignalStruct> inputSignalValidator = new NewStructValidator<InputSignalStruct>
            (
                this._toolTip,
                new ControlInfoCombo(this._grUst1ComboBox, Strings.SwitchSignals),
                new ControlInfoCombo(this._grUst2ComboBox, Strings.SwitchSignals),
                new ControlInfoCombo(this._indComboBox, Strings.SwitchSignals)
            );

            #region [Осц]

            NewStructValidator<OscopeConfigStruct> oscopeConfigValidator = new NewStructValidator<OscopeConfigStruct>
            (
                this._toolTip,
                new ControlInfoText(this._oscWriteLength, new CustomUshortRule(0, 99)),
                new ControlInfoCombo(this._oscFix, Strings.OscFix),
                new ControlInfoCombo(this._oscLength, Strings.OscLength),
                new ControlInfoCheck(_sh1CheckBox),
                new ControlInfoCheck(_sh2CheckBox),
                new ControlInfoCheck(_poCheckBox)
            );

            DgvValidatorWithDepend<OscopeAllChannelsStruct, ChannelWithBase> channelsValidator = new DgvValidatorWithDepend<OscopeAllChannelsStruct, ChannelWithBase>
            (
                this._oscChannelsWithBaseGrid,
                OscopeAllChannelsStruct.ChannelsCount,
                this._toolTip,
                new ColumnInfoCombo(Strings.OscChannelWithBaseNames, ColumnsType.NAME),
                new ColumnInfoComboControl(Strings.OscBases, 2),
                new ColumnInfoDictionaryComboDependent(func, 1)
            );
            this._oscChannelsWithBaseGrid.CellBeginEdit += this.GridOnCellBeginEdit;

            NewStructValidator<ChannelsStruct> startOscChannelValidator = new NewStructValidator<ChannelsStruct>
            (
                this._toolTip,
                new ControlInfoCombo(this._inpOscComboBox, Strings.RelaySignals)
            );

            StructUnion<OscopeStruct> oscopeUnion = new StructUnion<OscopeStruct>(oscopeConfigValidator, startOscChannelValidator, channelsValidator);
            #endregion [Осц]

            NewDgwValidatior<AllUrovConnectionStruct, UrovConnectionStruct> urovConnectionValidator =
                new NewDgwValidatior<AllUrovConnectionStruct, UrovConnectionStruct>
                (
                    this._UROVJoinData,
                    AllUrovConnectionStruct.CurrentUrovConnectionCount,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.ConnectionNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.InputSignals),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoText(RulesContainer.TimeRule)
                );

            NewDgwValidatior<AllConnectionStruct, ConnectionStruct> connectionValidator =
                new NewDgwValidatior<AllConnectionStruct, ConnectionStruct>
                (
                    this._joinData,
                    AllConnectionStruct.ConnectionsCount,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.ConnectionNames, ColumnsType.NAME),
                    new ColumnInfoText(RulesContainer.UshortTo65534),
                    new ColumnInfoCombo(Strings.InputSignals),
                    new ColumnInfoCombo(Strings.InputSignals),
                    new ColumnInfoCombo(Strings.Join),
                    new ColumnInfoCombo(Strings.InputSignals),
                    new ColumnInfoCheck(),
                    new ColumnInfoText(RulesContainer.TimeRule)
                );
            
            NewDgwValidatior<AllDifferentialCurrentStruct, DifferentialCurrentStruct> differentialCurrentValidator
                =
                new NewDgwValidatior<AllDifferentialCurrentStruct, DifferentialCurrentStruct>
                (
                    new[] {this._difDDataGrid, this._difMDataGrid},
                    new[] {3, 3},
                    this._toolTip,
                    new ColumnInfoCombo(Strings.DefNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.Mode),
                    new ColumnInfoCombo(Strings.InputSignals),
                    new ColumnInfoCheck(),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(RulesContainer.TimeRule, true, false),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(new CustomUshortRule(0, 45)),
                    new ColumnInfoCheck(true, false),
                    new ColumnInfoText(RulesContainer.UshortTo100, true, false),
                    new ColumnInfoCheck(true, false),
                    new ColumnInfoText(RulesContainer.UshortTo100, true, false),
                    new ColumnInfoCheck(true, false),
                    new ColumnInfoCheck(),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoCombo(Strings.InputSignals),
                    new ColumnInfoCombo(Strings.ModesLightOsc),
                    new ColumnInfoCheck()
                )
                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._difDDataGrid,
                            new TurnOffRule(1, Strings.Mode[0], true,
                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20)
                        ),
                        new TurnOffDgv
                        (
                            this._difMDataGrid,
                            new TurnOffRule(1, Strings.Mode[0], true,
                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20)
                        )
                    },
                    Disabled = new Point[]
                    {
                        new Point(3, 2)
                    }
                };
            NewDgwValidatior<AllMtzStruct, MtzStruct> mtzValidator =
                new NewDgwValidatior<AllMtzStruct, MtzStruct>
                (
                    this._MTZDifensesDataGrid,
                    AllMtzStruct.COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.MtzNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.Mode),
                    new ColumnInfoCombo(Strings.ExtDefSignals),
                    new ColumnInfoCombo(Strings.Logic),
                    new ColumnInfoCombo(Strings.MTZJoin),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoCombo(Strings.Characteristic),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoText(RulesContainer.Ushort100To4000),
                    new ColumnInfoCombo(Strings.ModesLightOsc),
                    new ColumnInfoCheck()
                )
                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._MTZDifensesDataGrid,
                            new TurnOffRule(1, Strings.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                        )
                    }
                };

            NewDgwValidatior<AllExternalDefenseStruct, ExternalDefenseStruct> externalDefenseValidator =
                new NewDgwValidatior<AllExternalDefenseStruct, ExternalDefenseStruct>
                (
                    this._externalDifensesDataGrid,
                    AllExternalDefenseStruct.CurrentCount,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.ExternalnNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.Mode),
                    new ColumnInfoCombo(Strings.Otkl),
                    new ColumnInfoCombo(Strings.ExtDefSignals),
                    new ColumnInfoCombo(Strings.ExtDefSignals),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoCombo(Strings.ExtDefSignals),
                    new ColumnInfoCombo(Strings.YesNo),
                    new ColumnInfoCombo(Strings.ModesLightOsc),
                    new ColumnInfoCombo(Strings.ModesLight)
                )
                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._externalDifensesDataGrid,
                            new TurnOffRule(1, Strings.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                        )
                    }
                };

            DgvValidatorWithDepend<AllAntiBounce, AntiBounce> antiBounceValidatior =
                new DgvValidatorWithDepend<AllAntiBounce, AntiBounce>
                (
                    this._antiBounceDataGridView,
                    AllAntiBounce.AntiBounceCount,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.LogicSignalNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.ModeAntiBounce),
                    new ColumnInfoCombo(Strings.CountAntiBounce)
                )
                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._antiBounceDataGridView,
                            new TurnOffRule(1, Strings.ModeAntiBounce[0], true, 2)
                        )
                    }
                };

            this._setpointValidator = new StructUnion<GroupSetpoint>
            (
                differentialCurrentValidator,
                mtzValidator,
                externalDefenseValidator
            );

            NewDgwValidatior<AllRsTriggersStruct, RsTriggersStruct> rsTriggersValidatior = new NewDgwValidatior<AllRsTriggersStruct, RsTriggersStruct>
            (
                this._rsTriggersDataGrid,
                AllRsTriggersStruct.RSTRIGGERS_COUNT_CONST,
                this._toolTip,
                new ColumnInfoCombo(Strings.RsTriggersName, ColumnsType.NAME),
                new ColumnInfoCombo(Strings.RSPriority),
                new ColumnInfoCombo(Strings.RelaySignals),
                new ColumnInfoCombo(Strings.RelaySignals)
            );

            ComboBox[] bgs =
            {
                this.goin1,this.goin2,this.goin3,this.goin4,this.goin5,this.goin6,this.goin7,this.goin8,this.goin9,this.goin10,
                this.goin11,this.goin12,this.goin13,this.goin14,this.goin15,this.goin16,this.goin17,this.goin18,this.goin19,this.goin20,
                this.goin21,this.goin22,this.goin23,this.goin24,this.goin25,this.goin26,this.goin27,this.goin28,this.goin29,this.goin30,
                this.goin31,this.goin32,this.goin33,this.goin34,this.goin35,this.goin36,this.goin37,this.goin38,this.goin39,this.goin40,
                this.goin41,this.goin42,this.goin43,this.goin44,this.goin45,this.goin46,this.goin47,this.goin48,this.goin49,this.goin50,
                this.goin51,this.goin52,this.goin53,this.goin54,this.goin55,this.goin56,this.goin57,this.goin58,this.goin59,this.goin60,
                this.goin61,this.goin62,this.goin63,this.goin64
            };

            NewStructValidator<Goose> gooseValidator = new NewStructValidator<Goose>
            (
                this._toolTip,
                new ControlInfoCombo(this.operationBGS, Strings.GooseConfig),
                new ControlInfoMultiCombo(bgs, Strings.GooseSignal)
            );

            SetpointsValidator<GooseConfig, Goose> allGooseSetpointValidator = new SetpointsValidator<GooseConfig, Goose>
                (new ComboboxSelector(this.currentBGS, Strings.GooseNames), gooseValidator);

            NewStructValidator<ConfigIPAddress> ethernetValidator = new NewStructValidator<ConfigIPAddress>(
                this._toolTip,
                new ControlInfoText(this._ipLo1, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipLo2, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipHi1, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipHi2, new CustomUshortRule(0, 255)),
                new ControlInfoCombo(this._reserveCB, Strings.Reserve));

            NewStructValidator<ConfigIgSsh> igSshValidator = new NewStructValidator<ConfigIgSsh>(
                this._toolTip,
                new ControlInfoCombo(this._configIgSshComboBox, Strings.ConfigIgssh));

            this._setpointsValidator = new SetpointsValidator<AllDefensesSetpointsStruct, GroupSetpoint>
                (this._groupSetpointSelector, this._setpointValidator);
            

            this._configurationValidator = new StructUnion<ConfigurationStruct>
            (
                urovValidator,
                urovConnectionValidator,
                connectionValidator,
                inputSignalValidator,
                oscopeUnion,
                newTtValidator,
                inputLogicUnion,
                vlsUnion,
                _setpointsValidator,
                relayValidator,
                rsTriggersValidatior,
                indicatorValidator,
                faultValidator,
                antiBounceValidatior,
                ethernetValidator,
                igSshValidator,
                configAddValidator,
                allGooseSetpointValidator
            );
        }

        #endregion

        #region Дополнительные функции
        /// <summary>
        /// Конфигурация успешно записана
        /// </summary>
        private void ConfigurationWriteOk()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = WRITE_OK;
            this._device.SetBit(this._device.DeviceNumber, 0xd00, true, "Сохранить конфигурацию", this._device);
        }

        private void GridOnCellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this.contextMenu.Tag = sender;
        }

        /// <summary>
        /// Сохранение значений IP
        /// </summary>
        public void GetIpAddress()
        {
            this._ipHi2Mem = this._ipHi2.Text;
            this._ipHi1Mem = this._ipHi1.Text;
            this._ipLo2Mem = this._ipLo2.Text;
            this._ipLo1Mem = this._ipLo1.Text;
        }

        public void SetIpAddress()
        {
            this._ipHi2.Text = this._ipHi2Mem;
            this._ipHi1.Text = this._ipHi1Mem;
            this._ipLo2.Text = this._ipLo2Mem;
            this._ipLo1.Text = this._ipLo1Mem;
        }

        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._resetSetpointsButton.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }

        public void ConfigurationReadOk()
        {
            this._statusLabel.Text = READ_OK;
            
            this._currentSetpointsStruct = this._configuration.Value;
            this.ShowConfiguration();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            MessageBox.Show(READ_OK);
            this.IsProcess = false;
        }

        /// <summary>
        /// Ошибка чтения конфигурации
        /// </summary>
        private void ConfigurationReadFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = READ_FAIL;
            MessageBox.Show(READ_FAIL);
        }

        /// <summary>
        /// Ошибка записи конфигурации
        /// </summary>
        private void ConfigurationWriteFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = WRITE_FAIL;
            MessageBox.Show(WRITE_FAIL);
        }

        /// <summary>
        /// Выводит все данные на экран
        /// </summary>
        private void ShowConfiguration()
        {
            this._configurationValidator.Set(this._currentSetpointsStruct);
        }

        /// <summary>
        /// Запуск чтения конфигурации
        /// </summary>
        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.IsProcess = true;
            this._progressBar.Value = 0;
            this._configuration.LoadStruct();
            this._statusLabel.Text = "Идет чтение";
        }

        /// <summary>
        /// Читает все данные с экрана
        /// </summary>
        private bool WriteConfiguration()
        {
            string message;
            
            if (this._configurationValidator.Check(out message, true))
            {
                this._currentSetpointsStruct = this._configurationValidator.Get();
                return true;
            }
            else
            {
                MessageBox.Show(ERROR_SETPOINTS_VALUE, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

        }

        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);
                XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);

                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);
                this._currentSetpointsStruct?.InitStruct(values);
                this.ShowConfiguration();
                this._statusLabel.Text = string.Format("Файл {0} успешно загружен", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName);
            }
        }

        private void SaveInFile()
        {
            this._saveConfigurationDlg.FileName = string.Format(XML_FILE_NAME, this._device.DeviceVersion, Strings.DeviceType);

            if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
            {
                this.WriteConfiguration();
                this.Serialize(this._saveConfigurationDlg.FileName);
            }
        }

        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            try
            {
                var doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MR901"));
                ushort[] values;

                values = this._currentSetpointsStruct?.GetValues();
                
                XmlElement element = doc.CreateElement(XML_HEAD);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("Файл {0} успешно сохранён", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #endregion

        #region Обработчики событий

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.contextMenu.Items.Clear();
            this.contextMenu.Items.AddRange(new ToolStripItem[]
            {
                this.readFromDeviceItem,
                this.writeToDeviceItem,
                this.resetSetpointsItem,
                this.clearSetpointsItem,
                this.readFromFileItem,
                this.writeToFileItem,
                this.writeToHtmlItem
            });
            this.readFromDeviceItem.Enabled = this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }

        private void Configuration_Load(object sender, EventArgs e)
        {
            this.ResetSetpoints(false);
            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this.StartRead();
            }
        }

        private void _oscLength_Changed(object sender, EventArgs e)
        {
            int osclen;

            switch (Strings.DeviceType)
            {
                case "":
                    osclen = 37932 * 2;
                    break;
                case "A1":
                    osclen = 36352 * 2;
                    break;
                default:
                    osclen = 28143 * 2;
                    break;
            }
            
            if (_sh1CheckBox.Checked && _sh2CheckBox.Checked && _poCheckBox.Checked)
            {
                switch (Strings.DeviceType)
                {
                    case "":
                        osclen -= 33306;
                        break;
                    case "A1":
                        osclen -= 31160;
                        break;
                    default:
                        osclen -= 9128;
                        break;
                }
                
                ChangeOscLength(osclen);
                return;
            }

            if (_sh1CheckBox.Checked && _sh2CheckBox.Checked ||
                _sh1CheckBox.Checked && _poCheckBox.Checked ||
                _sh2CheckBox.Checked && _poCheckBox.Checked)
            {
                switch (Strings.DeviceType)
                {
                    case "":
                        osclen -= 26010;
                        break;
                    case "A1":
                        osclen -= 24236;
                        break;
                    default:
                        osclen -= 6434;
                        break;
                }
                ChangeOscLength(osclen);
                return;
            }

            if (_sh1CheckBox.Checked || _sh2CheckBox.Checked || _poCheckBox.Checked)
            {
                switch (Strings.DeviceType)
                {
                    case "":
                        osclen -= 15696;
                        break;
                    case "A1":
                        osclen -= 14542;
                        break;
                    default:
                        osclen -= 3412;
                        break;
                }
                ChangeOscLength(osclen);
                return;
            }

            ChangeOscLength(osclen);
        }

        private void ChangeOscLength(int osclen)
        {
            int index = this._oscLength.SelectedIndex;
            this._oscSizeTextBox.Text = $"{osclen / (index + 2)} мс";
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;

            DialogResult result = MessageBox.Show(string.Format("Записать конфигурацию МР 901 №{0}? " +
                                                                "\nВ устройство будет записан IP-адрес: {1}.{2}.{3}.{4}!",
                    this._device.DeviceNumber, this._ipHi2.Text, this._ipHi1.Text, this._ipLo2.Text, this._ipLo1.Text),
                "Запись", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                if (this.WriteConfiguration())
                {
                    this._statusLabel.Text = "Идёт запись конфигурации";
                    this.IsProcess = true;
                    this._progressBar.Value = 0;

                    this._configuration.Value = this._currentSetpointsStruct;
                    try
                    {
                        this._configuration.SaveStruct();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        throw;
                    }

                }
            }
        }

        private void _clearSetpointsButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Обнулить уставки?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;
            this._configurationValidator.Reset();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
        }

        private void _resetSetpointsButton_Click(object sender, EventArgs e)
        {
            this.GetIpAddress();
            this.ResetSetpoints(true);
            this.SetIpAddress();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
        }

        private void ResetSetpoints(bool isDialog) //загрузка базовых уставок из файла, isDialog - показывает будет ли диалог
        {
            if (isDialog)
            {
                DialogResult res = MessageBox.Show("Загрузить базовые уставки", "Внимание", MessageBoxButtons.YesNo);
                if (res != DialogResult.Yes) return;
            }
            try
            {

                XmlDocument doc = new XmlDocument();
                string path = Path.GetDirectoryName(Application.ExecutablePath) +
                              string.Format(MR902_BASE_CONFIG_PATH, this._device.DeviceVersion, this._device.DevicePlant);
                doc.Load(path);

                XmlNode a = doc.FirstChild.SelectSingleNode(string.Format(XML_HEAD, this._device.DevicePlant));
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);

                this._currentSetpointsStruct.InitStruct(values);
                
                this.ShowConfiguration();
                this._statusLabel.Text = "Базовые уставки успешно загружены";
            }
            catch (Exception ex)
            {
                if (Validator.DEBUG)
                {
                    MessageBox.Show(ex.Message);
                }
                if (isDialog)
                {
                    if (MessageBox.Show("Невозможно загрузить файл стандартной конфигурации. Обнулить уставки?",
                            "Внимание", MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        return;
                    }
                }

                this._configurationValidator.Reset();
                
            }
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveInFile();
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtml();
        }

        private void SaveToHtml()
        {
            try
            {
                if (this.WriteConfiguration())
                {
                    ExportGroupForm exportGroup = new ExportGroupForm();
                    exportGroup.IsExport = true;
                    if (exportGroup.ShowDialog() != DialogResult.OK)
                    {
                        exportGroup.IsExport = false;
                        this.IsProcess = false;
                        this._statusLabel.Text = string.Empty;
                        return;
                    }

                    this._currentSetpointsStruct.DeviceVersion =
                        Common.VersionConverter(this._device.DeviceVersion);
                    this._currentSetpointsStruct.DeviceNumber = this._device.DeviceNumber.ToString();
                    this._currentSetpointsStruct.DeviceModelTypeXml = this._device.DevicePlant;
                    this._currentSetpointsStruct.OscCount = this._oscSizeTextBox.Text;
                    this._currentSetpointsStruct.Group = (int)exportGroup.SelectedGroup;
                    this._currentSetpointsStruct.CountOsc = OscopeAllChannelsStruct.ChannelsCount;
                    this._currentSetpointsStruct.CountRele = AllReleStruct.CurrentCount;
                    this._currentSetpointsStruct.CountConnection = AllConnectionStruct.ConnectionsCount;

                    this.SaveToHtml(this._currentSetpointsStruct, exportGroup.SelectedGroup);
                    exportGroup.IsExport = false;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения");
            }
        }

        private void SaveToHtml(StructBase str, ExportStruct group)
        {
            string fileName;
            if (group == ExportStruct.ALL)
            {
                fileName = string.Format("{0} Уставки версия {1} все группы", "МР902", this._device.DeviceVersion);
                this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.Configurations, fileName, str);
            }
            else
            {
                fileName = string.Format("{0} Уставки версия {1} группа {2}", "МР902", this._device.DeviceVersion, (int)group);
                this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.Configurations_Group, fileName, str);
            }
        }

        private void Mr902ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ContextMenuStrip menu = (ContextMenuStrip) sender;
            if (menu == null) return;
            DataGridView dgv = menu.Tag as DataGridView;
            dgv?.EndEdit();
            menu.Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                DialogResult res = MessageBox.Show("Обнулить уставки?", "Внимание", MessageBoxButtons.YesNo);
                if (res != DialogResult.Yes) return;

                this._configurationValidator.Reset();
                
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            }
            if (e.ClickedItem == this.resetSetpointsItem)
            {
                this.GetIpAddress();
                this.ResetSetpoints(true);
                this.SetIpAddress();
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtml();
            }
        }

        private void ConfigurationForm_Activated(object sender, EventArgs e)
        {
            Strings.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);
            ConfigurationStruct.DeviceModelType = this._device.DevicePlant;
        }

        #endregion


        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Mr902New); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(ConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void VLScheckedListBox_SelectedValueChanged(object sender, EventArgs e)
        {
            TreeViewVLS.StateNodes(this.treeViewForVLS);
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandCurrentTreeNode(this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
        }

        private void treeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Left) return;
            TreeViewVLS.DeleteNode(sender, e, this.contextMenu, this.allVlsCheckedListBoxs);
            TreeViewVLS.StateNodes(this.treeViewForVLS);
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
        }

        private void _wrapBtn_Click(object sender, EventArgs e)
        {
            if (this._wrapBtn.Text == "Свернуть")
            {
                this.treeViewForVLS.CollapseAll();
                this._wrapBtn.Text = "Развернуть";
            }
            else
            {
                this.treeViewForVLS.ExpandAll();
                this._wrapBtn.Text = "Свернуть";
            }
        }

        private void _applyCopySetpoinsButton_Click(object sender, EventArgs e)
        {
            string message;

            if (this._setpointValidator.Check(out message, true))
            {
                GroupSetpoint[] allSetpoints =
                    this.CopySetpoints<AllDefensesSetpointsStruct, GroupSetpoint>(this._setpointValidator, this._setpointsValidator);
                this._currentSetpointsStruct.AllDefensesSetpoints.Setpoints = allSetpoints;
                this._setpointsValidator.Set(this._currentSetpointsStruct.AllDefensesSetpoints);
            }
            else
            {
                MessageBox.Show("Обнаружены некорректные уставки");
                return;
            }

            MessageBox.Show("Копирование завершено");
        }
        private T2[] CopySetpoints<T1, T2>(IValidator union, IValidator setpointValidator)
            where T1 : StructBase, ISetpointContainer<T2> where T2 : StructBase
        {
            T2 temp = (T2)union.Get();
            T2[] allSetpoints = ((T1)setpointValidator.Get()).Setpoints;
            if ((string)this._copySetpoinsGroupComboBox.SelectedItem == Strings.CopyGroupsNames.Last())
            {
                for (int i = 0; i < allSetpoints.Length; i++)
                {
                    allSetpoints[i] = temp.Clone<T2>();
                }
            }
            else
            {
                allSetpoints[this._copySetpoinsGroupComboBox.SelectedIndex] = temp.Clone<T2>();
                allSetpoints[this._setpointsComboBox.SelectedIndex] = temp.Clone<T2>();
            }
            return allSetpoints;
        }

        private void DataErrorGrid(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
    }
}
