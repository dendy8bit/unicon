﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR902NEW.Configuration.Structures;
using BEMN.MR902NEW.Configuration.Structures.AntiBounce;
using BEMN.MR902NEW.Configuration.Structures.ConfigSystem;
using BEMN.MR902NEW.Configuration.Structures.Connections;
using BEMN.MR902NEW.Configuration.Structures.Defenses;
using BEMN.MR902NEW.Configuration.Structures.Defenses.External;
using BEMN.MR902NEW.Configuration.Structures.Goose;
using BEMN.MR902NEW.Configuration.Structures.Ls;
using BEMN.MR902NEW.Configuration.Structures.Osc;
using BEMN.MR902NEW.Configuration.Structures.Oscope;
using BEMN.MR902NEW.Configuration.Structures.RelayIndicator;
using BEMN.MR902NEW.Configuration.Structures.RSTriggers;
using BEMN.MR902NEW.Configuration.Structures.Tt;
using BEMN.MR902NEW.Configuration.Structures.Urov;
using BEMN.MR902NEW.Configuration.Structures.Vls;

namespace BEMN.MR902NEW.Configuration
{
    [XmlRoot(ElementName = "МР902")]
    public class ConfigurationStruct : StructBase
    {
        [XmlElement(ElementName = "Версия")]
        public double DeviceVersion { get; set; }

        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }

        [XmlElement(ElementName = "Тип_устройства")]
        public string Device
        {
            get => "МР902";
            set { }
        }

        [XmlElement(ElementName = "Модель")]
        public string DeviceModelTypeXml { get; set; }

        [XmlElement(ElementName = "Количество_осц_мс")]
        public string OscCount { get; set; }

        [XmlElement(ElementName = "Тип_конфигурации_устройства")]
        public string DeviceType { get; set; }

        [XmlElement(ElementName = "Группа")]
        public int Group { get; set; }

        [XmlElement(ElementName = "Количество_каналов_в_осцилографе")]
        public int CountOsc { get; set; }

        [XmlElement(ElementName = "Количество_реле")]
        public int CountRele { get; set; }

        [XmlElement(ElementName = "Количество_присоединений")]
        public int CountConnection { get; set; }


        private static string _deviceModelType = string.Empty;

        public static string DeviceModelType
        {
            get { return _deviceModelType; }
            set
            {
                _deviceModelType = value;
                InputLogicStruct.SetDeviceDiscretsType(value);
                AllReleStruct.SetDeviceRelaysType(value);
                AllUrovConnectionStruct.SetDeviceConnectionsType(value);
                AllConnectionStruct.SetDeviceConnectionsType(value);
                AllExternalDefenseStruct.SetDeviceExternalDefType(value);
                OscopeAllChannelsStruct.SetDeviceChannelsType(value);
                AllAntiBounce.SetDeviceAntibounceType(value);
            }
        }

        [Layout(0)] private UrovStruct _urov;                                           //конфигурация УРОВ
        [Layout(1)] private AllUrovConnectionStruct _allUrovConnection;                 //конфигурация УРОВ присоединений
        [Layout(2)] private AllConnectionStruct _allConnectionAndTransformer;           //Присоединения и трансформатор
        [Layout(3, Ignore = true, Count = 2)] private ushort[] rez1;
        [Layout(4)] private InputSignalStruct _inputSignal;                             //конфигурация входных сигналов
        [Layout(5)] private OscopeStruct _oscilloscope;                                 //конфигурация осциллографа
        [Layout(6)] private ControlTtUnionStruct _controlTt;                            //конфигурация цепей ТТ
        [Layout(7)] private AllInputLogicSignalStruct _allInputLogicSignal;             //структура входных логических сигналов
        [Layout(8)] private AllOutputLogicSignalStruct _allOutputLogicSignal;           //структура выходных логических сигналов
        [Layout(9)] private AllDefensesSetpointsStruct _allDefensesSetpoints;           //все защиты 
        [Layout(10)] private AllReleStruct _allRele;                                    //все реле
        [Layout(11)] private AllRsTriggersStruct _allRsTriggers;                        //все триггеры
        [Layout(12)] private AllIndicatorsStruct _allIndicators;                        //все индикаторы
        [Layout(13)] private FaultStruct _fault;                                        //реле неисправности
        [Layout(14)] private AllAntiBounce _antiBounce;                                 //антидребезг
        [Layout(15, Ignore = true, Count = 4)] private ushort[] _configNet;
        [Layout(16)] private ConfigIPAddress _ipAddress;
        [Layout(17, Ignore = true, Count = 4)] private ushort[] _configNetAdditionally; //дополнительно для конфиг 2-го 485
        [Layout(18)] private ConfigIgSsh _igSsh;
        [Layout(19, Ignore = true, Count = 112)] private ushort[] _res4;
        [Layout(20)] private ConfigAddStruct _configAdd;
        [Layout(21)] private GooseConfig _gooseConfig;

        [XmlElement(ElementName = "Уров")]
        [BindingProperty(0)]
        public UrovStruct Urov
        {
            get { return this._urov; }
            set { this._urov = value; }
        }
        [XmlElement(ElementName = "Уров_Присоединения")]
        [BindingProperty(1)]
        public AllUrovConnectionStruct AllUrovConnection
        {
            get { return this._allUrovConnection; }
            set { this._allUrovConnection = value; }
        }
        [XmlElement(ElementName = "Присоединения")]
        [BindingProperty(2)]
        public AllConnectionStruct AllConnectionAndTransformer
        {
            get { return this._allConnectionAndTransformer; }
            set { this._allConnectionAndTransformer = value; }
        }
        [XmlElement(ElementName = "Входные_сигналы")]
        [BindingProperty(3)]
        public InputSignalStruct InputSignal
        {
            get { return this._inputSignal; }
            set { this._inputSignal = value; }
        }
        [XmlElement(ElementName = "Конфигурация_осцилографа")]
        [BindingProperty(4)]
        public OscopeStruct Oscilloscope
        {
            get { return this._oscilloscope; }
            set { this._oscilloscope = value; }
        }
        [XmlElement(ElementName = "Цепи_ТТ")]
        [BindingProperty(5)]
        public ControlTtUnionStruct ControlTt
        {
            get { return this._controlTt; }
            set { this._controlTt = value; }
        }
        [XmlElement(ElementName = "Конфигурация_входных_сигналов")]
        [BindingProperty(6)]
        public AllInputLogicSignalStruct AllInputLogicSignal
        {
            get { return this._allInputLogicSignal; }
            set { this._allInputLogicSignal = value; }
        }
        [XmlElement(ElementName = "Все_ВЛС")]
        [BindingProperty(7)]
        public AllOutputLogicSignalStruct AllOutputLogicSignal
        {
            get { return this._allOutputLogicSignal; }
            set { this._allOutputLogicSignal = value; }
        }
        [XmlElement(ElementName = "Защиты")]
        [BindingProperty(8)]
        public AllDefensesSetpointsStruct AllDefensesSetpoints
        {
            get { return this._allDefensesSetpoints; }
            set { this._allDefensesSetpoints = value; }
        }
        [XmlElement(ElementName = "Реле")]
        [BindingProperty(9)]
        public AllReleStruct AllRele
        {
            get { return this._allRele; }
            set { this._allRele = value; }
        }
        /// <summary>
        /// RS-Триггеры
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "RS_триггеры")]
        public AllRsTriggersStruct RsTriggers
        {
            get { return this._allRsTriggers; }
            set { this._allRsTriggers = value; }
        }

        [XmlElement(ElementName = "Индикаторы")]
        [BindingProperty(11)]
        public AllIndicatorsStruct AllIndicators
        {
            get { return this._allIndicators; }
            set { this._allIndicators = value; }
        }

        [XmlElement(ElementName = "Реле_неисправности")]
        [BindingProperty(12)]
        public FaultStruct Fault
        {
            get { return this._fault; }
            set { this._fault = value; }
        }

        [XmlElement(ElementName = "Антидребезг")]
        [BindingProperty(13)]
        public AllAntiBounce AntiBounce
        {
            get { return this._antiBounce; }
            set { this._antiBounce = value; }
        }

        [XmlElement(ElementName = "IP")]
        [BindingProperty(14)]
        public ConfigIPAddress IP
        {
            get { return this._ipAddress; }
            set { this._ipAddress = value; }
        }

        [XmlElement(ElementName = "Конфигурация_Ig")]
        [BindingProperty(15)]
        public ConfigIgSsh IgSsh
        {
            get { return this._igSsh; }
            set { this._igSsh = value; }
        }

        [XmlElement(ElementName = "Опорный_канал")]
        [BindingProperty(16)]
        public ConfigAddStruct ConfigAdd
        {
            get { return this._configAdd; }
            set { this._configAdd = value; }
        }

        [XmlElement(ElementName = "БГС")]
        [BindingProperty(17)]
        public GooseConfig Gooses
        {
            get { return this._gooseConfig; }
            set { this._gooseConfig = value; }
        }

    }
}
