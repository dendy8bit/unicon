using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR902NEW.Configuration.Structures.Connections
{
    /// <summary>
    /// �������������
    /// </summary>
    public class ConnectionStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _config;					    //  ������������ �������������
        [Layout(1)] private ushort _inom;					    //	����������� ��� �������������� ��
        [Layout(2)] private ushort _off;						//	����������� ������������ ��������� �����������
        [Layout(3)] private ushort _on;						    //	����������� ����������� ��������� �����������
        [Layout(4)] private ushort _inp;						//	����, ���� ������������ ������������� == "�� �����"
        [Layout(5)] private ushort _tclr;                       //  �������� ��������� 
        #endregion [Private fields]
        
        #region [Properties]
        /// <summary>
        /// I��
        /// </summary>
        [XmlElement(ElementName = "I��")]
        [BindingProperty(0)]
        public ushort Inom
        {
            get => this._inom;
            set => this._inom = value;
        }

        /// <summary>
        /// ����������
        /// </summary>
        [XmlElement(ElementName = "����������")]
        [BindingProperty(1)]
        public string JoinSwitchoff
        {
            get => Validator.Get(this._off, Strings.InputSignals);
            set => this._off = Validator.Set(value, Strings.InputSignals);
        }

        /// <summary>
        /// ���������
        /// </summary>
        [XmlElement(ElementName = "���������")]
        [BindingProperty(2)]
        public string JoinSwitchon
        {
            get => Validator.Get(this._on, Strings.InputSignals);
            set => this._on = Validator.Set(value, Strings.InputSignals);
        }

        /// <summary>
        /// ��������
        /// </summary>
        [XmlElement(ElementName = "��������")]
        [BindingProperty(3)]
        public string JoinJoin
        {
            get => Validator.Get(this._config, Strings.Join, 0, 1, 2);
            set => this._config = Validator.Set(value, Strings.Join, this._config, 0, 1, 2);
        }

        /// <summary>
        /// ����
        /// </summary>
        [XmlElement(ElementName = "����")]
        [BindingProperty(4)]
        public string JoinEnter
        {
            get => Validator.Get(this._inp, Strings.InputSignals);
            set => this._inp = Validator.Set(value, Strings.InputSignals);
        }

        /// <summary>
        /// ���������
        /// </summary>
        [XmlElement(ElementName = "���������")]
        [BindingProperty(5)]
        public bool ResetJoin
        {
            get => Common.GetBit(this._config, 15);
            set => this._config = Common.SetBit(this._config, 15, value);
        }
        /// <summary>
        /// �������� ���������
        /// </summary>
        [XmlElement(ElementName = "��������_���������")]
        [BindingProperty(6)]
        public int ResetDeley
        {
            get => ValuesConverterCommon.GetWaitTime(this._tclr);
            set => this._tclr = ValuesConverterCommon.SetWaitTime(value);
        }

        #endregion [Properties]
    }
}