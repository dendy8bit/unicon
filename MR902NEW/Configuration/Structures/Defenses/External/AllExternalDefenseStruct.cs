﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR902NEW.Configuration.Structures.Defenses.External
{
    public class AllExternalDefenseStruct : StructBase, IDgvRowsContainer<ExternalDefenseStruct>
    {
        private const int COUNT = 24;

        private static int _currentCount;
        public static int CurrentCount => _currentCount;

        [Layout(0, Count = COUNT)] private ExternalDefenseStruct[] _externalDefenses;

        public ExternalDefenseStruct[] Rows
        {
            get { return this._externalDefenses; }
            set { this._externalDefenses = value; }
        }

        public static void SetDeviceExternalDefType(string type)
        {
            //switch (type)
            //{
            //    case "A1":
            //    case "A2":
            //    case "A3":
            //    case "A4":
            //        _currentCount = 24;
            //        break;
            //    //case "":
            //    //    _currentCount = 24;
            //    //    break;
            //    default:
            //        _currentCount = 24;
            //        break;
            //}
            _currentCount = 24;
        }
    }
}
