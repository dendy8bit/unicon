﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR902NEW.Configuration.Structures.Defenses.External
{
    public class ExternalDefenseStruct : StructBase
    {
        [Layout(0)] public ushort config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] public ushort config1;//конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] public ushort block; //вход блокировки
        [Layout(3)] public ushort ust; //уставка срабатывания_
        [Layout(4)] public ushort time; //время срабатывания_
        [Layout(5)] public ushort u; //уставка возврата
        [Layout(6)] public ushort tu; //время возврата
        [Layout(7)] public ushort rez; //резерв

        #region Внешние защиты

        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string EXTERNAL_MODE
        {
            get { return Validator.Get(this.config, Strings.Mode, 0, 1); }
            set { this.config = Validator.Set(value, Strings.Mode, this.config, 0, 1); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Откл")]
        public string EXTERNAL_OTKL
        {
            get { return Validator.Get(this.config, Strings.Otkl, 4, 5, 6, 7, 8); }
            set { this.config = Validator.Set(value, Strings.Otkl, this.config, 4, 5, 6, 7, 8); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Блокировка")]
        public string EXTERNAL_BLOCK
        {
            get
            {
                return Validator.Get(this.block, Strings.ExtDefSignals);
               
            }
            set
            {
                this.block = Validator.Set(value, Strings.ExtDefSignals);
            }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Сраб")]
        public string EXTERNAL_SRAB
        {
            get
            {
                return Validator.Get(this.ust, Strings.ExtDefSignals);
            }
            set
            {
                this.ust = Validator.Set(value, Strings.ExtDefSignals);

            }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "tср")]
        public int EXTERNAL_TSR
        {
            get { return ValuesConverterCommon.GetWaitTime(this.time); }
            set { this.time = ValuesConverterCommon.SetWaitTime(value); }

        }
        [BindingProperty(5)]
        [XmlElement(ElementName = "tвз")]
        public int EXTERNAL_TVZ
        {
            get { return ValuesConverterCommon.GetWaitTime(this.tu); }
            set { this.tu = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "Вход_Возврат")]
        public string EXTERNAL_VOZVR
        {
            get
            {
                return Validator.Get(this.u, Strings.ExtDefSignals);
            }
            set
            {
                this.u = Validator.Set(value, Strings.ExtDefSignals);
            }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "Возврат")]
        public string EXTERNAL_VOZVR_YN
        {
            get { return Validator.Get(this.config,Strings.YesNo, 3); }
            set { this.config = Validator.Set(value,Strings.YesNo,this.config, 3); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Осц")]
        public string EXTERNAL_OSC
        {
            get { return Validator.Get(this.config, Strings.ModesLightOsc, 14, 15); }
            set { this.config = Validator.Set(value, Strings.ModesLightOsc, this.config, 14, 15); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Уров")]
        public string EXTERNAL_UROV
        {
            get { return Validator.Get(this.config,Strings.ModesLight, 2); }
            set { this.config = Validator.Set(value, Strings.ModesLight,this.config, 2); }
        }



        #endregion 
    }
}