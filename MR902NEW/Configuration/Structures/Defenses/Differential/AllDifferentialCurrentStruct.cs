﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR902NEW.Configuration.Structures.Defenses.Differential
{
    public class AllDifferentialCurrentStruct : StructBase, IDgvRowsContainer<DifferentialCurrentStruct>
    {
        #region [Private fields]

        [Layout(0, Count = 6)] private DifferentialCurrentStruct[] _differentialCurrents;

        #endregion [Private fields]

        public DifferentialCurrentStruct[] Rows
        {
            get { return this._differentialCurrents; }
            set { this._differentialCurrents = value; }
        }
    }
}


