﻿using System.Collections.Generic;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR902NEW.Configuration.Structures.Defenses.Differential;
using BEMN.MR902NEW.Configuration.Structures.Defenses.External;
using BEMN.MR902NEW.Configuration.Structures.Defenses.Mtz;

namespace BEMN.MR902NEW.Configuration.Structures.Defenses
{
    public class GroupSetpoint : StructBase
    {
        [Layout(0)] private AllDifferentialCurrentStruct _allDifferentialCurrent;
        [Layout(1)] private AllMtzStruct _allMtz;
        [Layout(2)] private AllExternalDefenseStruct _allExternalDefense;

        [BindingProperty(0)]
        [XmlElement(ElementName = "Диф")]
        public AllDifferentialCurrentStruct AllDifferentialCurrent
        {
            get { return this._allDifferentialCurrent; }
            set { this._allDifferentialCurrent = value; }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "МТЗ")]
        public AllMtzStruct AllMtz
        {
            get { return this._allMtz; }
            set { this._allMtz = value; }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Внешние")]
        public AllExternalDefenseStruct AllExternalDefense
        {
            get { return this._allExternalDefense; }
            set { this._allExternalDefense = value; }
        }
    }
}
