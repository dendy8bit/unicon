﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR902NEW.Configuration.Structures.Oscope
{
    public class OscopeConfigStruct : StructBase
    {
        [Layout(0)] private ushort _config; //0 - фиксация по первой аварии 1 - фиксация по последней аварии
        [Layout(1)] private ushort _size; //размер осциллограмы
        [Layout(2)] private ushort _percent; //процент от размера осциллограммы

        /// <summary>
        /// Длит. предзаписи
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Предзапись")]
        public ushort Percent
        {
            get { return this._percent; }
            set { this._percent = value; }
        }

        /// <summary>
        /// Фиксация
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Фиксация")]
        public string FixationXml
        {
            get { return Validator.Get(this._config, Strings.OscFix, 0); }
            set { this._config = Validator.Set(value, Strings.OscFix, _config, 0); }
        }


        /// <summary>
        /// количество_осциллограм
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Количество_осциллограм")]
        public string SizeXml
        {
            get { return this._size.ToString(); }
            set { this._size = ushort.Parse(value); }
        }

        /// <summary>
        /// количество_осциллограм
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "СШ1")]
        public bool SH1
        {
            get { return Common.GetBit(this._config, 1); }
            set { this._config = Common.SetBit(this._config, 1, value); }
        }
        /// <summary>
        /// количество_осциллограм
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "СШ2")]
        public bool SH2
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }
        /// <summary>
        /// количество_осциллограм
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "ПО")]
        public bool PO
        {
            get { return Common.GetBit(this._config, 3); }
            set { this._config = Common.SetBit(this._config, 3, value); }
        }
        /// <summary>
        /// Размер страницы в словах(const)
        /// </summary>
        public ushort PageSize
        {
            get
            {
                return 1024;
            }
        }
    }
}
