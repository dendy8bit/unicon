using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR902NEW.Configuration.Structures.RelayIndicator
{
    /// <summary>
    /// ��� ����������
    /// </summary>
    public class AllIndicatorsStruct : StructBase, IDgvRowsContainer<IndicatorsStruct>
    {
        public const int COUNT = 12;

        /// <summary>
        /// ����������
        /// </summary>
        [Layout(0, Count = COUNT)] private IndicatorsStruct[] _indicators;

        /// <summary>
        /// ����������
        /// </summary>
        [XmlArray(ElementName = "���_����������")]
        public IndicatorsStruct[] Rows
        {
            get { return this._indicators; }
            set { this._indicators = value; }
        }
    }
}