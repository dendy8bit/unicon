﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR902NEW.Configuration.Structures.Urov
{

    public class AllUrovConnectionStruct : StructBase, IDgvRowsContainer<UrovConnectionStruct>
    {
        private const int COUNT_MEMORY = 24;
        public static int CurrentUrovConnectionCount { get; private set; }

        [Layout(0, Count = COUNT_MEMORY)] private UrovConnectionStruct[] ust;
        
        public UrovConnectionStruct[] Rows
        {
            get { return this.ust; }
            set { this.ust = value; }
        }

        public static void SetDeviceConnectionsType(string type)
        {
            switch (type)
            {
                case "A1":
                    CurrentUrovConnectionCount = 6;
                    break;
                case "A2":
                    CurrentUrovConnectionCount = 8;
                    break;
                case "A3":
                    CurrentUrovConnectionCount = 8;
                    break;
                case "A4":
                    CurrentUrovConnectionCount = 8;
                    break;
                default:
                    CurrentUrovConnectionCount = 6;
                    break;
            }
        }
    }
}