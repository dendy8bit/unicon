﻿using System.Collections.Generic;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.MBServer;

namespace BEMN.MR902NEW.Configuration.Structures.AntiBounce
{
    public class AllAntiBounce : StructBase, IDgvRowsContainer<AntiBounce>
    {
        private const ushort ANTIBOUNCE_MEM = 48;

        private ushort _valuesTypeFirst;
        private ushort _valuesValuesFirst;
        private ushort _valuesTypeSecond;
        private ushort _valuesValuesSecond;

        public static int AntiBounceCount { get; private set; }

        [Layout(0, Count = ANTIBOUNCE_MEM)] private ushort[] _antiBounces;

        /// <summary>
        /// Каналы
        /// </summary>
        [XmlArray(ElementName = "Все_антидребезги")]
        public AntiBounce[] Rows
        {
            get { return this.GetAntiBounces(); }
            set { this.SetAntiBounces(value); }
        }

        public AntiBounce[] GetAntiBounces()
        {
            List<AntiBounce> antiBounces = new List<AntiBounce>();
            for (int i = 0; i < ANTIBOUNCE_MEM; i++)
            {
                antiBounces.Add(new AntiBounce
                {
                    AntiBounceType = (byte) (Common.GetBits(_antiBounces[i], 7) >> 7),
                    AntiBounceValue = (byte) Common.GetBits(_antiBounces[i], 0, 1, 2, 3, 4, 5, 6)
                });
                antiBounces.Add(new AntiBounce
                {
                    AntiBounceType = (byte) (Common.GetBits(_antiBounces[i], 15) >> 15),
                    AntiBounceValue = (byte) (Common.GetBits(_antiBounces[i], 8, 9, 10, 11, 12, 13, 14) >> 8)
                });
            }
            return antiBounces.ToArray();
        }

        public void SetAntiBounces(AntiBounce[] value)
        {
            int i = 0;

            List<ushort> records = new List<ushort>();
            List<ushort> recordsInWord = new List<ushort>();

            foreach (var antiBounce in value)
            {
                if ((i % 2) == 0)
                {
                    //Added first record
                    _valuesTypeFirst = Validator.Set(antiBounce.TypeAntiBounce, Strings.ModeAntiBounce, _valuesTypeFirst, 7);
                    _valuesValuesFirst = Validator.Set(antiBounce.ValueAntiBounce, Strings.CountAntiBounce, _valuesValuesFirst, 0, 1, 2, 3, 4, 5, 6);
                    records.Add((ushort)(_valuesTypeFirst + _valuesValuesFirst));
                }
                else
                {
                    //Added second record
                    _valuesTypeSecond = Validator.Set(antiBounce.TypeAntiBounce, Strings.ModeAntiBounce, _valuesTypeSecond, 15);
                    _valuesValuesSecond = Validator.Set(antiBounce.ValueAntiBounce, Strings.CountAntiBounce, _valuesValuesSecond, 8, 9, 10, 11, 12, 13, 14);
                    records.Add((ushort)(_valuesTypeSecond + _valuesValuesSecond));
                }
                
                i++;
            }

            //Translate record in word
            for (int j = 0; j < value.Length; j += 2)
            {
                recordsInWord.Add((ushort) (records[j] + records[j + 1]));
            }

            //Write record
            for (int j = 0; j < ANTIBOUNCE_MEM; j++)
            {
                _antiBounces[j] = recordsInWord[j];
            }

        }

        public static void SetDeviceAntibounceType(string type)
        {
            switch (type)
            {
                case "A1":
                    AntiBounceCount = 64;
                    break;
                case "A2":
                    AntiBounceCount = 40;
                    break;
                case "A3":
                    AntiBounceCount = 24;
                    break;
                case "A4":
                    AntiBounceCount = 32;
                    break;
                default:
                    AntiBounceCount = 24;
                    break;
            }
        }

    }
}
