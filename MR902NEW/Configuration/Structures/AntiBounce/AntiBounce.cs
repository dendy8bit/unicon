﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR902NEW.Configuration.Structures.AntiBounce
{
    public class AntiBounce : StructBase
    {
        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Тип")]
        public string TypeAntiBounce
        {
            get { return Validator.Get(AntiBounceType, Strings.ModeAntiBounce); }
            set { this.AntiBounceType = Validator.Set(value, Strings.ModeAntiBounce); }
        }

        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Значение")]
        public string ValueAntiBounce
        {
            get{ return Validator.Get(AntiBounceValue, Strings.CountAntiBounce);}
            set{ this.AntiBounceValue = Validator.Set(value, Strings.CountAntiBounce); }
        }

        public ushort AntiBounceType { get; set; }

        public ushort AntiBounceValue { get; set; }
    }
}
