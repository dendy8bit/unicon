<?xml version="1.0" encoding="windows-1251"?>
<!-- Edited by XMLSpy® -->
<xsl:stylesheet version="1.1"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head/>
      <body>
        <xsl:variable name="type" select="��902/������"/>
        <xsl:variable name="countCon" select="��902/����������_�������������"/>
        <xsl:variable name="countRelay" select="��902/����������_����"/>
        <xsl:variable name="countOsc" select="��902/����������_�������_�_�����������"/>

        <h3>���������� <xsl:value-of select="��902/���_����������"/> . ������ �� <xsl:value-of select="��902/������"/> . ����� <xsl:value-of select="��902/�����_����������"/></h3>
        <p></p>
        
        <b>�������������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="32CD32">
            <th>�������</th>
            <th>I��, �</th>
            <th>������.</th>
            <th>�����.</th>
            <th>��������</th>
            <th>����</th>
            <th>���������</th>
            <th>�������� ���������, ��</th>
          </tr>
          <xsl:for-each select="��902/�������������/Rows/ConnectionStruct">
            <tr align="center">
              <xsl:if test="position() &lt; ($countCon+1)">
                <xsl:choose>
                  <xsl:when test="$type = 'A1' or $type = ''">
                    <xsl:if test="position() &lt; 6">
                      <td>������������� <xsl:value-of select="position()"/></td>
                      <td>
                        <xsl:value-of select="I��"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:value-of select="���������"/>
                      </td>
                      <td>
                        <xsl:value-of select="��������"/>
                      </td>
                      <td>
                        <xsl:value-of select="����"/>
                      </td>
                      <td>
                        <xsl:for-each select="���������">
                          <xsl:if test="current() ='false'">���</xsl:if>
                          <xsl:if test="current() ='true'">��</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="��������_���������"/>
                      </td>
                    </xsl:if>
                    <xsl:if test = "position() = 6">
                      <td>In</td>
                        <td><xsl:value-of select="I��"/></td>
                        <td><xsl:value-of select="����������"/></td>
                        <td><xsl:value-of select="���������"/></td>
                        <td><xsl:value-of select="��������"/></td>
                        <td><xsl:value-of select="����"/></td>
                        <td><xsl:for-each select="���������">
                            <xsl:if test="current() ='false'">���</xsl:if>
                            <xsl:if test="current() ='true'">��</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td><xsl:value-of select="��������_���������"/></td>
                    </xsl:if>
                  </xsl:when>
                  <xsl:otherwise>
                      <td>������������� <xsl:value-of select="position()"/></td>
                      <td><xsl:value-of select="I��"/></td>
                      <td><xsl:value-of select="����������"/></td>
                      <td><xsl:value-of select="���������"/></td>
                      <td><xsl:value-of select="��������"/></td>
                      <td><xsl:value-of select="����"/></td>
                      <td>
                        <xsl:for-each select="���������">
                          <xsl:if test="current() ='false'">���</xsl:if>
                          <xsl:if test="current() ='true'">��</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td><xsl:value-of select="��������_���������"/></td>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:if>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>
        
        <h3>������� �������</h3>
        <b>������� ���������� �������</b>
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������� �</b>
            <table border="1" cellspacing="0">
              <tr bgcolor="FFFFCC">
                <th>����� ��</th>
                <th>������������</th>
              </tr>
              <xsl:for-each select="��902/������������_�������_��������/��">
                <xsl:if test="position() &lt; 9 ">
                  <tr>
                    <td>
                      <xsl:value-of  select="position()"/>
                    </td>
                    <td>
                      <xsl:for-each select="�������">
                        <xsl:if test="current() !='���'">
                          <xsl:if test="current() ='��'">
                            �<xsl:value-of select="position()"/>
                          </xsl:if>
                          <xsl:if test="current() ='������'">
                            ^�<xsl:value-of select="position()"/>
                          </xsl:if>
                        </xsl:if>
                      </xsl:for-each>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:for-each>
            </table>
            <b>���������� ������� ���</b>
            <table border="1" cellspacing="0">
              <tr bgcolor="FFFFCC">
                <th>����� ��</th>
                <th>������������</th>
              </tr>
              <xsl:for-each select="��902/������������_�������_��������/��">
                <xsl:if test="position() &gt; 8 ">
                  <tr>
                    <td>
                      <xsl:value-of  select="position()"/>
                    </td>
                    <td>
                      <xsl:for-each select="�������">
                        <xsl:if test="current() !='���'">
                          <xsl:if test="current() ='��'">
                            �<xsl:value-of select="position()"/>
                          </xsl:if>
                          <xsl:if test="current() ='������'">
                            ^�<xsl:value-of select="position()"/>
                          </xsl:if>
                        </xsl:if>
                      </xsl:for-each>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:for-each>
            </table>
          </td>
        </table>
        <p></p>
        
        <table border="1" cellspacing="0">
          <tr bgcolor="FFFFCC">
            <th>��������� ������ ������� 1</th>
            <th>��������� ������ ������� 2</th>
            <th>����� ���������</th>
            <th>����� ������������� ��</th>
          </tr>
          <tr align="center">
            <td><xsl:value-of select="��902/�������_�������/����_���������_������_�������_1"/></td>
            <td><xsl:value-of select="��902/�������_�������/����_���������_������_�������_2"/></td>
            <td><xsl:value-of select="��902/�������_�������/����_�����_����."/></td>
            <td><xsl:value-of select="��902/����_��/�����_�������������_��"/></td>
          </tr>
        </table>
        <p></p>

        <b>�����������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FF8C00">
            <th>�����</th>
            <th>���</th>
            <th>��������</th>
          </tr>
          <xsl:for-each select="��902/�����������/���_������������/AntiBounce">

            <xsl:choose>

              <xsl:when test="$type = 'A1'">
                <xsl:if test="position() &lt; 65">
                  <tr align ="center">
                    <td>
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select="@���"/>
                    </td>
                    <td>
                      <xsl:value-of select="@��������"/>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:when>

              <xsl:when test="$type = 'A2'">
                <xsl:if test="position() &lt; 41 ">
                  <tr align ="center">
                    <td>
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select="@���"/>
                    </td>
                    <td>
                      <xsl:value-of select="@��������"/>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:when>

              <xsl:when test="$type = 'A3'">
                <xsl:if test="position() &lt; 25 ">
                  <tr align ="center">
                    <td>
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select="@���"/>
                    </td>
                    <td>
                      <xsl:value-of select="@��������"/>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:when>

              <xsl:when test="$type = 'A4'">
                <xsl:if test="position() &lt; 33 ">
                  <tr>
                    <!--align ="center">-->
                    <td>
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select="@���"/>
                    </td>
                    <td>
                      <xsl:value-of select="@��������"/>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:when>

              <xsl:otherwise>
                <xsl:if test="position() &lt; 25 ">
                  <tr align ="center">
                    <td>
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select="@���"/>
                    </td>
                    <td>
                      <xsl:value-of select="@��������"/>
                    </td>
                  </tr>
                </xsl:if>
               </xsl:otherwise>

            </xsl:choose>

          </xsl:for-each>
        </table>


        <h3>�������� ����� TT</h3>
        <table border="1" cellspacing="0">
          <tr bgcolor="FF69B4">
            <th>�������� ����� ��</th>
            <th>l�min, l�</th>
            <th>T��, ��</th>
            <th>�����</th>
            <th>�����</th>
          </tr>
          <xsl:for-each select="��902/����_��/����_��_���/Rows/ControlTtStruct">
            <tr align="center">
              <td>
                <xsl:if test="position() =1">��1</xsl:if>
                <xsl:if test="position() =2">��2</xsl:if>
                <xsl:if test="position() =3">��</xsl:if>
              </td>
              <td>
                <xsl:value-of select="I�min"/>
              </td>
              <td>
                <xsl:value-of select="T��"/>
              </td>
              <td>
                <xsl:value-of select="�������������"/>
              </td>
              <td>
                <xsl:value-of select="�����"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>

        <h2>�������</h2>

        <table border="1" cellspacing="0">
          <tr>
            <th bgcolor="c1ced5">���� �������� �������</th>
            <th>
              <xsl:value-of select="��902/�������_�����/InputAdd"/>
            </th>
          </tr>
        </table>
        <p></p>

        <h3>������</h3>

        <xsl:for-each select="��902/������/������">
          <xsl:variable name="index" select="position()"/>
          <h2 style="color:#FF0000FF">
            ������ ������� <xsl:value-of select="position()"/>
          </h2>

          <table border="1" cellspacing="0">
            <td>
              <b>���������������� ������ �� ����������� ���������</b>
              <table border="1" cellspacing="0">
                <tr bgcolor="B0C4DE">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>���� �� I�3 ��</th>
                  <th>l�>, l�</th>
                  <th>l�>>, l�</th>
                  <th>t��, ��</th>
                  <th>l�, l�</th>
                  <th>f</th>
                  <th>����.������.2</th>
                  <th>l2�, %</th>
                  <th>����.������.5</th>
                  <th>l5�, %</th>
                  <th>�����.�����.</th>
                  <th>������������</th>
                  <th>l�*,l�</th>
                  <th>t��, ��</th>
                  <th>���� �������.</th>
                  <th>���.</th>
                  <th>����</th>
                </tr>
                <xsl:for-each select="���/Rows/DifferentialCurrentStruct">
                  <xsl:if test="position() &lt; 4 ">
                    <tr align="center">
                      <td>
                        <xsl:if test="position() =1">l�1 ��1</xsl:if>
                        <xsl:if test="position() =2">l�2 ��2</xsl:if>
                        <xsl:if test="position() =3">l�3 ��</xsl:if>
                      </td>
                      <td>
                        <xsl:value-of select="�����"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:for-each select="��">
                          <xsl:if test="current() ='false'">���</xsl:if>
                          <xsl:if test="current() ='true'">��</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="�������"/>
                      </td>
                      <td>
                        <xsl:value-of select="�������2"/>
                      </td>
                      <td>
                        <xsl:value-of select="t��"/>
                      </td>
                      <td>
                        <xsl:value-of select="I�"/>
                      </td>
                      <td>
                        <xsl:value-of select="f"/>
                      </td>
                      <td>
                        <xsl:for-each select="����2">
                          <xsl:if test="current() ='false'">���</xsl:if>
                          <xsl:if test="current() ='true'">��</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="����2�������"/>
                      </td>
                      <td>
                        <xsl:for-each select="����5">
                          <xsl:if test="current() ='false'">���</xsl:if>
                          <xsl:if test="current() ='true'">��</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="����5�������"/>
                      </td>
                      <td>
                        <xsl:for-each select="�����">
                          <xsl:if test="current() ='false'">���</xsl:if>
                          <xsl:if test="current() ='true'">��</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:for-each select="�����������">
                          <xsl:if test="current() ='false'">���</xsl:if>
                          <xsl:if test="current() ='true'">��</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="I�"/>
                      </td>
                      <td>
                        <xsl:value-of select="T��"/>
                      </td>
                      <td>
                        <xsl:value-of select="����_��"/>
                      </td>
                      <td>
                        <xsl:value-of select="���"/>
                      </td>
                      <td>
                        <xsl:for-each select="����">
                          <xsl:if test="current() ='false'">���</xsl:if>
                          <xsl:if test="current() ='true'">��</xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>

              <b>���������������� ������ �� ���������� ���������</b>
              <table border="1" cellspacing="0">
                <tr bgcolor="B0C4DE">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>���� �� I�3 ��</th>
                  <th>l�>, l�</th>
                  <th>l�>>, l�</th>
                  <th>l�, l�</th>
                  <th>f</th>
                  <th>������������</th>
                  <th>l�*,l�</th>
                  <th>t��, ��</th>
                  <th>���� �������.</th>
                  <th>���.</th>
                  <th>����</th>
                </tr>
                <xsl:for-each select="���/Rows/DifferentialCurrentStruct">
                  <xsl:if test="position() &gt; 3 ">
                    <tr align="center">
                      <td>
                        <xsl:if test="position() =4">l�1 ��1</xsl:if>
                        <xsl:if test="position() =5">l�2 ��2</xsl:if>
                        <xsl:if test="position() =6">l�3 ��</xsl:if>
                      </td>
                      <td>
                        <xsl:value-of select="�����"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:for-each select="��">
                          <xsl:if test="current() ='false'">���</xsl:if>
                          <xsl:if test="current() ='true'">��</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="�������"/>
                      </td>
                      <td>
                        <xsl:value-of select="�������2"/>
                      </td>
                      <td>
                        <xsl:value-of select="I�"/>
                      </td>
                      <td>
                        <xsl:value-of select="f"/>
                      </td>
                      <td>
                        <xsl:for-each select="�����������">
                          <xsl:if test="current() ='false'">���</xsl:if>
                          <xsl:if test="current() ='true'">��</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="I�"/>
                      </td>
                      <td>
                        <xsl:value-of select="T��"/>
                      </td>
                      <td>
                        <xsl:value-of select="����_��"/>
                      </td>
                      <td>
                        <xsl:value-of select="���"/>
                      </td>
                      <td>
                        <xsl:for-each select="����">
                          <xsl:if test="current() ='false'">���</xsl:if>
                          <xsl:if test="current() ='true'">��</xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>
            </td>
          </table>

          <p></p>
          <b>������ I</b>
          <table border="1" cellspacing="0">
            <tr bgcolor="B0C4DE">
              <th>�������</th>
              <th>�����</th>
              <th>����������</th>
              <th>������</th>
              <th>���������</th>
              <th>l��, l�</th>
              <th>��������������</th>
              <th>t, ��</th>
              <th>k �����. ���-��</th>
              <th>�����������</th>
              <th>����</th>
            </tr>
            <xsl:for-each select="���/Rows/MtzStruct">
              <tr align="center">
                <td>
                  ������� I> <xsl:value-of select="position()"/>
                </td>
                <td>
                  <xsl:value-of select="�����"/>
                </td>
                <td>
                  <xsl:value-of select="����������"/>
                </td>
                <td>
                  <xsl:value-of select="������"/>
                </td>
                <td>
                  <xsl:value-of select="���������"/>
                </td>
                <td>
                  <xsl:value-of select="�������"/>
                </td>
                <td>
                  <xsl:value-of select="��������������"/>
                </td>
                <td>
                  <xsl:value-of select="t��"/>
                </td>
                <td>
                  <xsl:value-of select="�"/>
                </td>
                <td>
                  <xsl:value-of select="���"/>
                </td>
                <td>
                  <xsl:for-each select="����">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
                </td>
              </tr>
            </xsl:for-each>
          </table>

          <p></p>
          <b>�������</b>
          <table border="1" cellspacing="0">
            <tr bgcolor="B0C4DE">
              <th>�������</th>
              <th>�����</th>
              <th>����������</th>
              <th>����������</th>
              <th>������������</th>
              <th>t��, ��</th>
              <th>t��, ��</th>
              <th>������ ��������</th>
              <th>�������</th>
              <th>�����������</th>
              <th>����</th>
            </tr>
            <xsl:for-each select="�������/Rows/ExternalDefenseStruct">
              <tr align="center">
                <td>
                  ������� <xsl:value-of select="position()"/>
                </td>
                <td>
                  <xsl:value-of select="�����"/>
                </td>
                <td>
                  <xsl:value-of select="����"/>
                </td>
                <td>
                  <xsl:value-of select="����������"/>
                </td>
                <td>
                  <xsl:value-of select="����"/>
                </td>
                <td>
                  <xsl:value-of select="t��"/>
                </td>
                <td>
                  <xsl:value-of select="t��"/>
                </td>
                <td>
                  <xsl:value-of select="����_�������"/>
                </td>
                <td>
                  <xsl:value-of select="�������"/>
                </td>
                <td>
                  <xsl:value-of select="���"/>
                </td>
                <td>
                  <xsl:value-of select="����"/>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </xsl:for-each>

        <h3>����</h3>
        <table border="1" cellspacing="0">
          <tr>
            <th bgcolor="B0C4DG">���. ����.</th>
            <td>
              <xsl:value-of select="��902/����/�����"/>
            </td>
          </tr>
        </table>
        <h4>���� ��</h4>
        <table border="1" cellspacing="0">
          <tr>
            <th bgcolor="B0C4DG">���� ���� �� �� ��.</th>
            <td>
              <xsl:value-of select="��902/����/�������������"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="B0C4DG">�� ����</th>
            <td>
              <xsl:value-of select="��902/����/��_����"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="B0C4DG">
              t����1, �� (�� ����)
            </th>
            <td>
              <xsl:value-of select="��902/����/�1"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="B0C4DG">
              ���� 2
            </th>
            <td>
              <xsl:value-of select="��902/����/����_2"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="B0C4DG">
              t����2, �� (�� ����.)
            </th>
            <td>
              <xsl:value-of select="��902/����/�2"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="B0C4DG">
              ���� 3
            </th>
            <td>
              <xsl:value-of select="��902/����/����_3"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="B0C4DG">
              t����3, �� (���. �������.)
            </th>
            <td>
              <xsl:value-of select="��902/����/�3"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="B0C4DG">
              t����, ��
            </th>
            <td>
              <xsl:value-of select="��902/����/�_����������"/>
            </td>
          </tr>

          <table border="1" cellspacing="0">
            <h4>������ �����</h4>
            <tr>
              <th bgcolor="B0C4DG">
                ��1
              </th>
              <td>
                <xsl:value-of select="��902/����/��1_������_�����"/>
              </td>
            </tr>
          </table>
          <table border="1" cellspacing="0">
            <tr>
              <th bgcolor="B0C4DG">
                ��2
              </th>
              <td>
                <xsl:value-of select="��902/����/��2_������_�����"/>
              </td>
            </tr>
          </table>
          <table border="1" cellspacing="0">
            <tr>
              <th bgcolor="B0C4DG">
                ��
              </th>
              <td>
                <xsl:value-of select="��902/����/��_������_�����"/>
              </td>
            </tr>
          </table>

          <h4>����. ����</h4>
          <table border="1" cellspacing="0">
            <tr>
              <th bgcolor="B0C4DG">
                ��1
              </th>
              <td>
                <xsl:value-of select="��902/����/��1_����_����"/>
              </td>
            </tr>
          </table>
          <table border="1" cellspacing="0">
            <tr>
              <th bgcolor="B0C4DG">
                ��2
              </th>
              <td>
                <xsl:value-of select="��902/����/��2_����_����"/>
              </td>
            </tr>
          </table>
          <table border="1" cellspacing="0">
            <tr>
              <th bgcolor="B0C4DG">
                ��
              </th>
              <td>
                <xsl:value-of select="��902/����/��_����_����"/>
              </td>
            </tr>
          </table>
        </table>
        <p></p>

        <b>���� �������������</b>
        <table border="1" cellspacing="0">
          <tr>
            <th bgcolor="B0C400">
              ����
            </th>
            <td>
              <xsl:value-of select="��902/����/����_����_����"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="B0C400">
              �� ����
            </th>
            <td>
              <xsl:value-of select="��902/����/��_����_����_����"/>
            </td>
          </tr>
          <table border="1" cellspacing="0">
            <tr bgcolor="4682B4">
              <th>�������</th>
              <th>��. �����</th>
              <th>l����, l�</th>
              <th>t ����1 ��., �� (�� ����)</th>
              <th>t ����2 ��., �� (�� ����. ���.)</th>
            </tr>
            <xsl:for-each select="��902/����_�������������/Rows/UrovConnectionStruct">
              <xsl:if test="position() &lt; ($countCon+1)">
                <tr>
                <xsl:choose>
                  <xsl:when test="$type = 'A1' or $type = ''">
                    <xsl:if test="position() &lt; 6">
                        <td>
                          ������������� <xsl:value-of select="position()"/>
                        </td>
                        <td>
                          <xsl:value-of select="��_�����"/>
                        </td>
                        <td>
                          <xsl:value-of select="I_����"/>
                        </td>
                        <td>
                          <xsl:value-of select="T1"/>
                        </td>
                        <td>
                          <xsl:value-of select="T2"/>
                        </td>
                    </xsl:if>
                    <xsl:if test="position()=6">
                      <td>In</td>
                      <td>
                        <xsl:value-of select="��_�����"/>
                      </td>
                      <td>
                        <xsl:value-of select="I_����"/>
                      </td>
                      <td>
                        <xsl:value-of select="T1"/>
                      </td>
                      <td>
                        <xsl:value-of select="T2"/>
                      </td>
                    </xsl:if>
                  </xsl:when>
                  <xsl:otherwise>
                      <td>
                        ������������� <xsl:value-of select="position()"/>
                      </td>
                      <td>
                        <xsl:value-of select="��_�����"/>
                      </td>
                      <td>
                        <xsl:value-of select="I_����"/>
                      </td>
                      <td>
                        <xsl:value-of select="T1"/>
                      </td>
                      <td>
                        <xsl:value-of select="T2"/>
                      </td>
                  </xsl:otherwise>
                </xsl:choose>
                </tr>
              </xsl:if>
            </xsl:for-each>
          </table>
        </table>
        <p></p>

        <h3>�������� �������</h3>
        
        <b>�������� ����</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="4682B4">
            <th>�����</th>
            <th>���</th>
            <th>������</th>
            <th>������, ��</th>
          </tr>
          <xsl:for-each select="��902/����/���_����/����_����">
            <xsl:if test="position() &lt; ($countRelay+1)">
              <tr align="center">
                <td><xsl:value-of select="position()"/></td>
                <td><xsl:value-of select= "@���"/></td>
                <td><xsl:value-of select= "@������"/></td>
                <td><xsl:value-of select= "@�����"/></td>
              </tr></xsl:if>
          </xsl:for-each>
        </table>
        <p></p>
      
        <b>����������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="4682B4">
            <th>�����</th>
            <th>���</th>
            <th>������ "�������"</th>
            <th>������ "�������"</th>
            <th>����� ������</th>
          </tr>

          <xsl:for-each select="��902/����������/���_����������/����_���������">
            <tr align="center">
              <td>
                <xsl:value-of select="position()"/>
              </td>
              <td>
                <xsl:value-of select= "@���" />
              </td>
              <td>
                <xsl:value-of select= "@������_�������" />
              </td>
              <td>
                <xsl:value-of select= "@������_�������" />
              </td>
              <td>
                <xsl:value-of select= "@�����_������" />
              </td>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>
        
        <b>���� �������������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="4682B4">
            <th>���������� �������������</th>
            <th>����������� �������������</th>
            <th>������������� ����� ��</th>
            <th>����� ����������� (����)</th>
            <th>������</th>
            <th>������, ��</th>
          </tr>
          <tr align="center">
            <td>
              <xsl:for-each select="��902/����_�������������/@�������������_1">
                <xsl:if test="current() ='false'">���</xsl:if>
                <xsl:if test="current() ='true'">��</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="��902/����_�������������/@�������������_2">
                <xsl:if test="current() ='false'">���</xsl:if>
                <xsl:if test="current() ='true'">��</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="��902/����_�������������/@�������������_3">
                <xsl:if test="current() ='false'">���</xsl:if>
                <xsl:if test="current() ='true'">��</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="��902/����_�������������/@�������������_4">
                <xsl:if test="current() ='false'">���</xsl:if>
                <xsl:if test="current() ='true'">��</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="��902/����_�������������/@�������������_5">
                <xsl:if test="current() ='false'">���</xsl:if>
                <xsl:if test="current() ='true'">��</xsl:if>
              </xsl:for-each>
            </td>
            <td><xsl:value-of select= "��902/����_�������������/@�������_����_�������������" /></td>
          </tr>
        </table>
        <p></p>

        <table border="1" cellspacing="0" cellpadding="4">
          <tr>����� �����������</tr>
          <xsl:for-each select="��902/�������_�����">
            <tr align="left">
              <th bgcolor="90EE90">1. �� ����� � ������ �������</th>
              <td>
                <xsl:for-each select="@�����_��_�����_�_������_�������">
                  <xsl:if test="current() = 'true'">��</xsl:if>
                  <xsl:if test="current() = 'false'">���</xsl:if>
                </xsl:for-each>
              </td>
            </tr>
            <tr align="left">
              <th bgcolor="90EE90">2. �� ����� � ������ ������</th>
              <td>
                <xsl:for-each select="@�����_��_�����_�_������_������">
                  <xsl:if test="current() = 'true'">��</xsl:if>
                  <xsl:if test="current() = 'false'">���</xsl:if>
                </xsl:for-each>
              </td>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr>������������ ����������</tr>
          <th bgcolor="90EE90">���. I�2��2</th>
          <td>
            <xsl:value-of select="��902/������������_Ig/IndConf"/>
          </td>
        </table>
        <p></p>

        <h2>���������� ��������</h2>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr>����������� ����</tr>
          <tr bgcolor="FF8C00">
            <th>�</th>
            <th>���</th>
            <th>������</th>
            <th>T�����., ��</th>
          </tr>
          <xsl:for-each select="��902/����/���_����/����_����">
              <xsl:if test="position() &gt; ($countRelay)">
                <tr align="center">
                  <td>
                    <xsl:value-of select="position()"/>
                  </td>
                  <td>
                    <xsl:value-of select="@���"/>
                  </td>
                  <td>
                    <xsl:value-of select="@������"/>
                  </td>
                  <td>
                    <xsl:value-of select="@�����"/>
                  </td>
                </tr>
              </xsl:if>
          </xsl:for-each>
        </table>
        <p></p>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr>��������������� RS-��������</tr>
          <tr bgcolor="FF8C00">
            <th>�</th>
            <th>���</th>
            <th>���� R</th>
            <th>���� S</th>
          </tr>
          <xsl:for-each select="��902/RS_��������/���_rs_��������/RsTriggersStruct">
            <tr align="center">
              <td>
                <xsl:value-of select="position()"/>
              </td>
              <td>
                <xsl:value-of select="@���"/>
              </td>
              <td>
                <xsl:value-of select="@����_R"/>
              </td>
              <td>
                <xsl:value-of select="@����_S"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>

        <b>���</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="c1ced5">
            <th>�����</th>
            <th>������������</th>
          </tr>
          <xsl:for-each select="��902/���_���/���">
            <xsl:if test="position() &lt; 17">
              <tr>
                <td><xsl:value-of select="position()"/></td>
                <td>
                  <xsl:for-each select="�������">
                    <xsl:value-of select="current()"/>|
                  </xsl:for-each>
                </td>
              </tr>
            </xsl:if>
          </xsl:for-each>
        </table>
        <p></p>
        
        <p></p>
        <h3>�����������</h3>
      

            <h3>��������� ������������</h3>
            <table border="1" cellspacing="0">
              <tr bgcolor="98FB98">
                <th>����������</th>
                <th>����. ����������, %</th>
                <th>������. ��</th>
                <th>���� �����</th>
                <th>��1</th>
                <th>��2</th>
                <th>��</th>
              </tr>
              <tr align="center">
                <td><xsl:value-of select="��902/������������_�����������/������������_���/����������_�����������"/> - <xsl:value-of select="��902/����������_���_��"/></td>
                <td><xsl:value-of select="��902/������������_�����������/������������_���/����������"/></td>
                <td><xsl:value-of select="��902/������������_�����������/������������_���/��������"/></td>
                <td><xsl:value-of select="��902/������������_�����������/����_�������_������������/@�����"/></td>
                <td>
                  <xsl:for-each select="��902/������������_�����������/������������_���/��1">
                    <xsl:if test="current() ='false'">
                      ���
                    </xsl:if>
                    <xsl:if test="current() ='true'">
                      ��
                    </xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="��902/������������_�����������/������������_���/��2">
                    <xsl:if test="current() ='false'">
                      ���
                    </xsl:if>
                    <xsl:if test="current() ='true'">
                      ��
                    </xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="��902/������������_�����������/������������_���/��">
                    <xsl:if test="current() ='false'">
                      ���
                    </xsl:if>
                    <xsl:if test="current() ='true'">
                      ��
                    </xsl:if>
                  </xsl:for-each>
                </td>
              </tr>
            </table>


            <h3>��������������� ���������� ������</h3>
            <table border="1" cellspacing="0">
              <tr bgcolor="98FB98">
                <th>�����</th>
                <th>������</th>
              </tr>
              <xsl:for-each select="��902/������������_�����������/������������_�������/���_������/ChannelWithBase">
                <xsl:if test="position() &lt; ($countOsc)+1">
                <tr align="center">
                  <td><xsl:value-of select="position()"/></td>
                  <td><xsl:value-of select="@�����"/></td>
                </tr>
                </xsl:if>
              </xsl:for-each>
            </table>

        <p></p>
        <b>������������ Ethernet</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="B0C4DE">
            <th>IP-�����</th>
          </tr>
          <xsl:for-each select="��902/IP">
            <tr align="center">
              <td>
                <xsl:value-of select="IpHi2"/>.
                <xsl:value-of select="IpHi1"/>.
                <xsl:value-of select="IpLo2"/>.
                <xsl:value-of select="IpLo1"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>
        
        <b>��������������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="B0C4DE">
            <th>IP-�����</th>
          </tr>
          <xsl:for-each select="��902/IP">
            <tr align="center">
              <td>
                <xsl:value-of select="��������������"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>

      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>