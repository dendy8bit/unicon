<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <body>
        <h3>���������� ��902. ������ ������</h3>
        <p></p>
        <table border="1">
          <tr bgcolor="#c1ced5">
            <th>�����</th>
            <th>����/�����</th>
            <th>���������</th>
            <th>�������</th>
            <th>�������� ������������</th>
            <th>�������� ��������� ������������</th>
            <th>������ �������</th>
            <th>I�a ��1</th>
            <th>I�b ��1</th>
            <th>I�c ��1</th>
            <th>I�a ��1</th>
            <th>I�b ��1</th>
            <th>I�c ��1</th>
            <th>I�a ��2</th>
            <th>I�b ��2</th>
            <th>I�c ��2</th>
            <th>I�a ��2</th>
            <th>I�b ��2</th>
            <th>I�c ��2</th>
            <th>I�a ��</th>
            <th>I�b ��</th>
            <th>I�c ��</th>
            <th>I�a ��</th>
            <th>I�b ��</th>
            <th>I�c ��</th>
            <th>Ia ����.1</th>
            <th>Ib ����.1</th>
            <th>Ic ����.1</th>
            <th>Ia ����.2</th>
			      <th>Ib ����.2</th>
            <th>Ic ����.2</th>
            <th>Ia ����.3</th>
			      <th>Ib ����.3</th>
            <th>Ic ����.3</th>
            <th>Ia ����.4</th>
			      <th>Ib ����.4</th>
            <th>Ic ����.4</th>
            <th>Ia ����.5</th>
			      <th>Ib ����.5</th>
            <th>Ic ����.5</th>
			      <th>In</th>
            <th>��������� 1-8</th>
            <th>��������� 9-16</th>
            <th>��������� 17-24</th>
          </tr>
          
          <xsl:for-each select="DocumentElement/��902_������_������">
            <tr align="center">
              <td><xsl:value-of select="_indexCol"/></td>
              <td><xsl:value-of select="_timeCol"/></td>
              <td><xsl:value-of select="_msg1Col"/></td>
              <td><xsl:value-of select="_msgCol"/></td>
              <td><xsl:value-of select="_codeCol"/></td>
              <td><xsl:value-of select="_typeCol"/></td>
              <td><xsl:value-of select="_IaCol"/></td>
              <td><xsl:value-of select="_Ida1Col"/></td>
              <td><xsl:value-of select="_Idb1Col"/></td>
              <td><xsl:value-of select="_Idc1Col"/></td>
              <td><xsl:value-of select="_Ita1Col"/></td>
              <td><xsl:value-of select="_Itb1Col"/></td>
              <td><xsl:value-of select="_Itc1Col"/></td>
              <td><xsl:value-of select="_Ida2Col"/></td>
              <td><xsl:value-of select="_Idb2Col"/></td>
              <td><xsl:value-of select="_Idc2Col"/></td>
              <td><xsl:value-of select="_Ita2Col"/></td>
              <td><xsl:value-of select="_Itb2Col"/></td>
              <td><xsl:value-of select="_Itc2Col"/></td>
              <td><xsl:value-of select="_Ida3Col"/></td>
              <td><xsl:value-of select="_Idb3Col"/></td>
              <td><xsl:value-of select="_Idc3Col"/></td>
              <td><xsl:value-of select="_Ita3Col"/></td>
              <td><xsl:value-of select="_Itb3Col"/></td>
              <td><xsl:value-of select="_Itc3Col"/></td>
              <td><xsl:value-of select="_I1Col"/></td>
              <td><xsl:value-of select="_I2Col"/></td>
              <td><xsl:value-of select="_I3Col"/></td>
              <td><xsl:value-of select="_I4Col"/></td>
              <td><xsl:value-of select="_I5Col"/></td>
              <td><xsl:value-of select="_I6Col"/></td>
              <td><xsl:value-of select="_I7Col"/></td>
			        <td><xsl:value-of select="_I8Col"/></td>
              <td><xsl:value-of select="_I9Col"/></td>
              <td><xsl:value-of select="_I10Col"/></td>
			        <td><xsl:value-of select="_I11Col"/></td>
              <td><xsl:value-of select="_I12Col"/></td>
              <td><xsl:value-of select="_I13Col"/></td>
			        <td><xsl:value-of select="_I14Col"/></td>
              <td><xsl:value-of select="_I15Col"/></td>
              <td><xsl:value-of select="_I16Col"/></td>
			        <td><xsl:value-of select="_D0Col"/></td>
              <td><xsl:value-of select="_D1Col"/></td>
              <td><xsl:value-of select="_D2Col"/></td>
            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
