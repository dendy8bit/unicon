<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy® -->
<xsl:stylesheet version="1.1"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
<head>
<script type="text/javascript">
	function translateBoolean(value, elementId){
 		var result = "";
 		if(value.toString().toLowerCase() == "true")
  			result = "��";
 		else if(value.toString().toLowerCase() == "false")
  			result = "���";
		else 
			result = "������������ ��������"
 		document.getElementById(elementId).innerHTML = result; 
}
</script>
<script type="text/javascript">
	function translateColor(value, elementId){
 		var result = "";
 		if(value.toString().toLowerCase() == "true")
  			result = "�������";
 		else if(value.toString().toLowerCase() == "false")
  			result = "�������";
		else 
			result = "������������ ��������"
 		document.getElementById(elementId).innerHTML = result; 
}
</script>
</head>
  <body>
    <h3>���������� <xsl:value-of select="ConfigurationStruct/���_����������"/> . ������ �� <xsl:value-of select="ConfigurationStruct/������"/> . ����� <xsl:value-of select="ConfigurationStruct/�����_����������"/></h3>
  <p></p>
  <b>�������������</b>
<table border="1" cellspacing="0">
	<tr bgcolor="32CD32">
		 <th>�������</th>
		 <th>I��, �</th>
		 <th>������.</th>
		 <th>�����.</th>
		 <th>��������</th>
		 <th>����</th>
		 <th>���������</th>
		 <th>�������� ���������, ��</th>
    </tr>
	<xsl:for-each select="ConfigurationStruct/Conn/connMain/ConnectionStruct">
		<xsl:if test="position() &lt;7">
		<tr align="center">
			<xsl:if test="position() &lt; 6">
			<td>������������� <xsl:value-of select="position()"/></td>
			</xsl:if>
			<xsl:if test="position()=6"><td>ln</td></xsl:if>
			<td><xsl:value-of select="Inom"/></td>
			<td><xsl:value-of select="JoinSwitchoff"/></td>
			<td><xsl:value-of select="JoinSwitchon"/></td>
			<td><xsl:value-of select="JoinJoin"/></td>
			<td><xsl:value-of select="JoinEnter"/></td>
			<xsl:element name="td">
			<xsl:attribute name="id">Obn_<xsl:value-of select="position()"/></xsl:attribute>            
			<script>translateBoolean(<xsl:value-of select="ResetJoin"/>,"Obn_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
			<td><xsl:value-of select="ResetDeley"/></td>
		 </tr>
		 </xsl:if>
	</xsl:for-each>
	</table>	
	<p></p>
	<!-- ������� ������� -->
 
  <h3>������� �������</h3>
	
	<b>������� ���������� �������</b>
	<table border="1" cellspacing="0">
	<td>
	<b>���������� ������� �</b>
	<table border="1" cellspacing="0">

      <tr bgcolor="FFFFCC">
         <th>����� ��</th>
         <th>������������</th>
	  </tr>
 
   <xsl:for-each select="ConfigurationStruct/Inp/ToXml/ArrayOfString">  
     <xsl:if test="position() &lt; 9 ">
   	  <tr>	
         <td><xsl:value-of  select="position()"/></td>
	
			 <td>
			 <xsl:for-each select="string">
				<xsl:if test="current() !='���'">
					<xsl:if test="current() ='��'">
					�<xsl:value-of select="position()"/>
					</xsl:if>
					<xsl:if test="current() ='������'">
					^�<xsl:value-of select="position()"/>
					</xsl:if>
				</xsl:if>
			 </xsl:for-each>
			 </td>
	  </tr>
	</xsl:if>	
    </xsl:for-each>
    </table>
	
		<b>���������� ������� ���</b>
	<table border="1" cellspacing="0">

      <tr bgcolor="FFFFCC">
         <th>����� ��</th>
         <th>������������</th>
	  </tr>
 
   <xsl:for-each select="ConfigurationStruct/Inp/ToXml/ArrayOfString">  
     <xsl:if test="position() &gt; 8 ">
   	  <tr>	
         <td><xsl:value-of  select="position()"/></td>
	
			 <td>
			 <xsl:for-each select="string">
				<xsl:if test="current() !='���'">
					<xsl:if test="current() ='��'">
					�<xsl:value-of select="position()"/>
					</xsl:if>
					<xsl:if test="current() ='������'">
					^�<xsl:value-of select="position()"/>
					</xsl:if>
				</xsl:if>
			 </xsl:for-each>
			 </td>
	  </tr>
	</xsl:if>	
    </xsl:for-each>
    </table>
	</td>
	</table>
	<p></p>

		<table border="1" cellspacing="0">
			<tr bgcolor="FFFFCC">
				<th>��������� ������ �������</th>
			</tr>
			<tr align="center">
				<td><xsl:value-of select="ConfigurationStruct/Impsg/GrUst"/></td>
			</tr>
		</table>
	<p></p>
	
		<table border="1" cellspacing="0">
			<tr bgcolor="FFFFCC">
				<th>����� ���������</th>
			</tr>
			<tr align="center">
				<td><xsl:value-of select="ConfigurationStruct/Impsg/SbInd"/></td>
			</tr>
		</table>
	<p></p>
	

	<h3>������. �������� ������</h3>
		<table border="1" cellspacing="0">
		<td>
		<b>���������������� ������ �� ����������� ���������</b>
		<table border="1" cellspacing="0">
			<tr bgcolor="B0C4DE">
				<th>�������</th>
				<th>�����</th>
				<th>����������</th>
				<th>l�>, l�</th>
				<th>l�>>, l�</th>
				<th>t��, ��</th>
				<th>l�, l�</th>
				<th>f</th>
				<th>����.������.2</th>
				<th>l2�, %</th>
				<th>����.������.5</th>
				<th>l5�, %</th>
				<th>�����.�����.</th>
				<th>������������</th>
				<th>l�*,l�</th>
				<th>t��, ��</th>
				<th>���� �������.</th>
				<th>���.</th>
				<th>����</th>		
			</tr>
			
			<xsl:for-each select="ConfigurationStruct/AllDefences/MainGroup/DiffDstv/DifDefStruct">
			<xsl:if test="position() &lt; 4 ">
				<tr align="center">
						<td>
						<xsl:if test="position() =1">l�1 ��1</xsl:if>
						<xsl:if test="position() =2">l�2 ��2</xsl:if>
						<xsl:if test="position() =3">l�3 ��</xsl:if>
						</td>
						<td><xsl:value-of select="DIFM_MODE"/></td>
						<td><xsl:value-of select="DIFM_BLOCK"/></td>
						<td><xsl:value-of select="DIFM_ICP"/></td>
						<td><xsl:value-of select="DIFM_IDO"/></td>
						<td><xsl:value-of select="DIFM_TCP"/></td>
						<td><xsl:value-of select="DIFM_IB"/> </td>
						<td><xsl:value-of select="DIFM_F"/></td>
						<td><xsl:value-of select="DIFM_BLOCK_G"/></td>
						<td><xsl:value-of select="DIFM_I2G"/></td>
						<td><xsl:value-of select="DIFD_BLOCK_5G"/></td>
						<td><xsl:value-of select="DIFD_I5G"/></td>
						<td><xsl:value-of select="DIFD_BLOCK_OPR_NAS"/> </td>
						<td><xsl:value-of select="DIFD_OCH"/></td>
						<td><xsl:value-of select="DIFD_I_OCH"/></td>
						<td><xsl:value-of select="DIFD_T_OCH"/></td>
						<td><xsl:value-of select="DIFD_ENTER_OCH"/></td>
						<td><xsl:value-of select="DIFD_OSC"/></td>
						<td><xsl:value-of select="DIFD_UROV"/> </td>
				</tr>
			</xsl:if>
			</xsl:for-each>	
		</table>
	<b>���� �� l�3 ��</b>
		<table border="1" cellspacing="0">
		<tr bgcolor="B0C4DE">
		<th>l�1 ��1</th>
		<th>l�2 ��2</th>
		</tr>
		<xsl:for-each select="ConfigurationStruct/AllDefences/MainGroup/DiffDstv/DifDefStruct">
		<xsl:if test="position() =1">
		<xsl:element name="td">
						<xsl:attribute name="id">DIRD_<xsl:value-of select="position()"/></xsl:attribute>            
						<script>translateBoolean(<xsl:value-of select="DefenceInstantaneousRunForPo"/>,"DIRD_<xsl:value-of select="position()"/>");</script>
						</xsl:element>
		</xsl:if>
		<xsl:if test="position() =2">
		<xsl:element name="td">
						<xsl:attribute name="id">DARD_<xsl:value-of select="position()"/></xsl:attribute>            
						<script>translateBoolean(<xsl:value-of select="DefenceActualRunForPo"/>,"DARD_<xsl:value-of select="position()"/>");</script>
						</xsl:element>
		</xsl:if>
		</xsl:for-each>
		</table>
	<b>���������������� ������ �� ���������� ���������</b>
		<table border="1" cellspacing="0">
			<tr bgcolor="B0C4DE">
				<th>�������</th>
				<th>�����</th>
				<th>����������</th>
				<th>l�>, l�</th>
				<th>l�>>, l�</th>
				<th>l�, l�</th>
				<th>f</th>
				<th>������������</th>
				<th>l�*,l�</th>
				<th>t��, ��</th>
				<th>���� �������.</th>
				<th>���.</th>
				<th>����</th>		
			</tr>
			
			<xsl:for-each select="ConfigurationStruct/AllDefences/MainGroup/diffMgn/DifDefStruct">
			<xsl:if test="position() &lt; 4 ">
				<tr align="center">
						<td>
						<xsl:if test="position() =1">l�1� ��1</xsl:if>
						<xsl:if test="position() =2">l�2� ��2</xsl:if>
						<xsl:if test="position() =3">l�3� ��</xsl:if>
						</td>
						<td><xsl:value-of select="DIFM_MODE"/></td>
						<td><xsl:value-of select="DIFM_BLOCK"/></td>
						<td><xsl:value-of select="DIFM_ICP"/></td>
						<td><xsl:value-of select="DIFM_IDO"/></td>
						<td><xsl:value-of select="DIFM_IB"/></td>
						<td><xsl:value-of select="DIFM_F"/> </td>
						<td><xsl:value-of select="DIFM_OCH"/></td>
						<td><xsl:value-of select="DIFM_I_OCH"/></td>
						<td><xsl:value-of select="DIFM_T_OCH"/></td>
						<td><xsl:value-of select="DIFD_ENTER_OCH"/></td>
						<td><xsl:value-of select="DIFD_OSC"/></td>
						<td><xsl:value-of select="DIFD_UROV"/> </td>
						
				</tr>
			</xsl:if>
			</xsl:for-each>	
		</table>
		<b>���� �� l�3� ��</b>
		<table border="1" cellspacing="0">
		<tr bgcolor="B0C4DE">
		<th>l�1� ��1</th>
		<th>l�2� ��2</th>
		</tr>
		<xsl:for-each select="ConfigurationStruct/AllDefences/MainGroup/diffMgn/DifDefStruct">
		<xsl:if test="position() =1">
		<xsl:element name="td">
						<xsl:attribute name="id">DIRFPD_<xsl:value-of select="position()"/></xsl:attribute>            
						<script>translateBoolean(<xsl:value-of select="DefenceInstantaneousRunForPo"/>,"DIRFPD_<xsl:value-of select="position()"/>");</script>
						</xsl:element>
		</xsl:if>
		<xsl:if test="position() =2">
		<xsl:element name="td">
						<xsl:attribute name="id">DARFPD_<xsl:value-of select="position()"/></xsl:attribute>            
						<script>translateBoolean(<xsl:value-of select="DefenceActualRunForPo"/>,"DARFPD_<xsl:value-of select="position()"/>");</script>
						</xsl:element>
		</xsl:if>
		</xsl:for-each>
		</table>
		</td>
		</table>
	<p></p>
		<b>I></b>
		<table border="1" cellspacing="0">
			<tr bgcolor="B0C4DE">
				<th>�������</th>
				<th>�����</th>
				<th>����������</th>
				<th>������</th>
				<th>���������</th>
				<th>l��, l�</th>
				<th>��������������</th>
				<th>t, ��</th>
				<th>k �����. ���-��</th>
				<th>�����������</th>
				<th>����</th>
			</tr>
			<xsl:for-each select="ConfigurationStruct/AllDefences/MainGroup/MtzDefStruct/MtzDefStruct">
			<tr align="center">
				<td>������� l> <xsl:value-of select="position()"/></td>
				<td><xsl:value-of select="MTZ_MODE"/></td>
				<td><xsl:value-of select="MTZ_BLOCK"/></td>
				<td><xsl:value-of select="Logic"/></td>
				<td><xsl:value-of select="MTZ_MEASURE"/></td>
				<td><xsl:value-of select="MTZ_ICP"/></td>
				<td><xsl:value-of select="MTZ_CHAR"/></td>
				<td><xsl:value-of select="MTZ_T"/></td>
				<td><xsl:value-of select="MTZ_K"/></td>
				<td><xsl:value-of select="MTZ_OSC"/></td>
				<td><xsl:value-of select="MTZ_UROV"/></td>
			</tr>
			</xsl:for-each>
		</table>
	<p></p>
	<b>�������</b>
		<table border="1" cellspacing="0">
			<tr bgcolor="B0C4DE">
				<th>�������</th>
				<th>�����</th>
				<th>����������</th>
				<th>����������</th>
				<th>����.</th>
				<th>t��, ��</th>
				<th>t��, ��</th>
				<th>����.</th>
				<th>����/���</th>
				<th>�����������</th>
				<th>����</th>
			</tr>
			<xsl:for-each select="ConfigurationStruct/AllDefences/MainGroup/Mtzext/ExtDefStruct">
			<tr align="center">
				<td>������� <xsl:value-of select="position()"/></td>
				<td><xsl:value-of select="EXTERNAL_MODE"/></td>
				<td><xsl:value-of select="EXTERNAL_OTKL"/></td>
				<td><xsl:value-of select="EXTERNAL_BLOCK"/></td>
				<td><xsl:value-of select="EXTERNAL_SRAB"/></td>
				<td><xsl:value-of select="EXTERNAL_TSR"/></td>
				<td><xsl:value-of select="EXTERNAL_TVZ"/></td>
				<td><xsl:value-of select="EXTERNAL_VOZVR"/></td>
				<td><xsl:value-of select="EXTERNAL_VOZVR_YN"/></td>
				<td><xsl:value-of select="EXTERNAL_OSC"/></td>
				<td><xsl:value-of select="EXTERNAL_UROV"/></td>
			</tr>
			</xsl:for-each>
		</table>
		<p></p>
			
		<h3>�������� �������</h3>
		
		<b>�������� ����</b>
		<table border="1" cellspacing="0">
	<tr bgcolor="4682B4">
		 <th>�����</th>
		 <th>���</th>
		 <th>������</th>
		 <th>������, ��</th>
    </tr>

	<xsl:for-each select="ConfigurationStruct/ParamAutomat/RelayStruct/RelayStruct">
		 <tr align="center">
			<td><xsl:value-of select="position()"/></td>
			<td><xsl:value-of select= "Type" /></td>
			<td><xsl:value-of select= "Signal" /></td>
			<td><xsl:value-of select= "Wait" /></td>
		 </tr>
	</xsl:for-each>
	</table>
	<p></p>
	<b>����������</b>
		<table border="1" cellspacing="0">
	<tr bgcolor="4682B4">
		 <th>�����</th>
		 <th>���</th>
		 <th>������</th>
		 <th>����</th>
    </tr>

	<xsl:for-each select="ConfigurationStruct/ParamAutomat/IndicatorStruct/IndicatorStruct">
		 <tr align="center">
			<td><xsl:value-of select="position()"/></td>
			<td><xsl:value-of select= "Type" /></td>
			<td><xsl:value-of select= "Signal" /></td>
			<xsl:element name="td">
						<xsl:attribute name="id">COLOR_<xsl:value-of select="position()"/></xsl:attribute>            
						<script>translateColor(<xsl:value-of select="Color"/>,"COLOR_<xsl:value-of select="position()"/>");</script>
						</xsl:element>
		 </tr>
	</xsl:for-each>
	</table>
	<p></p>
	<b>���� �������������</b>
		<table border="1" cellspacing="0">
	<tr bgcolor="4682B4">
		 <th>���������� �������������</th>
		 <th>����������� �������������</th>
		 <th>������������� ����� ��</th>
		 <th>����� ����������� (����)</th>
		 <th>�������, ��</th>
    </tr>

	<xsl:for-each select="ConfigurationStruct/ParamAutomat">
		  <tr align="center">
		 <td><xsl:value-of select= "OutputN1" /></td>
		 <td><xsl:value-of select= "OutputN2" /></td>
		 <td><xsl:value-of select= "OutputN3" /></td>
		 <td><xsl:value-of select= "OutputN4" /></td>
		 <td><xsl:value-of select= "OutputImp" /></td>
		 </tr>
	</xsl:for-each>
	</table>
	<p></p>
	<b>���</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="4682B4">
         <th>�����</th>
         <th>������������</th>
		 
      </tr>
		 <tr align="center">
		 <td>1</td>
		 <td><xsl:for-each select="ConfigurationStruct/Els/���1">|<xsl:value-of select="current()"/>|</xsl:for-each></td>
		 </tr>
		 <tr align="center">
		 <td>2</td>
		 <td><xsl:for-each select="ConfigurationStruct/Els/���2">|<xsl:value-of select="current()"/>|</xsl:for-each></td>
		 </tr>
		 <tr align="center">
		 <td>3</td>
		 <td><xsl:for-each select="ConfigurationStruct/Els/���3">|<xsl:value-of select="current()"/>|</xsl:for-each></td>
		 </tr>
		 <tr align="center">
		 <td>4</td>
		 <td><xsl:for-each select="ConfigurationStruct/Els/���4">|<xsl:value-of select="current()"/>|</xsl:for-each></td>
		 </tr>
		 <tr align="center">
		 <td>5</td>
		 <td><xsl:for-each select="ConfigurationStruct/Els/���5">|<xsl:value-of select="current()"/>|</xsl:for-each></td>
		 </tr>
		 <tr align="center">
		 <td>6</td>
		 <td><xsl:for-each select="ConfigurationStruct/Els/���6">|<xsl:value-of select="current()"/>|</xsl:for-each></td>
		 </tr>
		 <tr align="center">
		 <td>7</td>
		 <td><xsl:for-each select="ConfigurationStruct/Els/���7">|<xsl:value-of select="current()"/>|</xsl:for-each></td>
		 </tr>
		 <tr align="center">
		 <td>8</td>
		 <td><xsl:for-each select="ConfigurationStruct/Els/���8">|<xsl:value-of select="current()"/>|</xsl:for-each></td>
		 </tr>
		 <tr align="center">
		 <td>10</td>
		 <td><xsl:for-each select="ConfigurationStruct/Els/���9">|<xsl:value-of select="current()"/>|</xsl:for-each></td>
		 </tr>
		 <tr align="center">
		 <td>10</td>
		 <td><xsl:for-each select="ConfigurationStruct/Els/���10">|<xsl:value-of select="current()"/>|</xsl:for-each></td>
		 </tr>
		 <tr align="center">
		 <td>11</td>
		 <td><xsl:for-each select="ConfigurationStruct/Els/���11">|<xsl:value-of select="current()"/>|</xsl:for-each></td>
		 </tr>
		 <tr align="center">
		 <td>12</td>
		 <td><xsl:for-each select="ConfigurationStruct/Els/���12">|<xsl:value-of select="current()"/>|</xsl:for-each></td>
		 </tr>
		 <tr align="center">
		 <td>13</td>
		 <td><xsl:for-each select="ConfigurationStruct/Els/���13">|<xsl:value-of select="current()"/>|</xsl:for-each></td>
		 </tr>
		 <tr align="center">
		 <td>14</td>
		 <td><xsl:for-each select="ConfigurationStruct/Els/���14">|<xsl:value-of select="current()"/>|</xsl:for-each></td>
		 </tr>
		 <tr align="center">
		 <td>15</td>
		 <td><xsl:for-each select="ConfigurationStruct/Els/���15">|<xsl:value-of select="current()"/>|</xsl:for-each></td>
		 </tr>
		 <tr align="center">
		 <td>16</td>
		 <td><xsl:for-each select="ConfigurationStruct/Els/���16">|<xsl:value-of select="current()"/>|</xsl:for-each></td>
		 </tr>

	 </table>   
	 <p></p>
	<h3>�����������</h3>
	<table border="1" cellspacing="0">
	<td>
    <table border="1" cellspacing="0">
      <tr bgcolor="98FB98">
         <th>�����������</th>
         <th>����. ����������, %</th>
		 <th>������. ��</th>
      </tr>
		 <xsl:for-each select="ConfigurationStruct/Osc">
		 <tr align="center">
         <td><xsl:value-of select="OscLength"/></td>
		 <td><xsl:value-of select="OscWLength"/></td>
		 <td><xsl:value-of select="OscFix"/></td>
		 </tr>
		 </xsl:for-each>
	 </table>
	<table border="1" cellspacing="0">	 
	<tr bgcolor="98FB98">
         <th>�����</th>
         <th>������</th>
      </tr>
		 <xsl:for-each select="ConfigurationStruct/Osc/Channels/string">
		 <tr align="center">
         <td><xsl:value-of select="position()"/></td>
		 <td><xsl:value-of select="current()"/></td>
		 </tr>
		 </xsl:for-each>
	 </table>
	 </td>
	 </table>
	 <h3>����</h3>
	 <b>���</b>
	 <table border="1" cellspacing="0">	 
	<tr bgcolor="FFFFE0">
         <th>�����</th>
         <th>�����</th>
		 <th>�� �����</th>
         <th>�� ����</th>
		 <th>t����1, ��</th>
         <th>t����2, ��</th>
		 <th>t����3, ��</th>
         <th>�� ����</th>
		 <th>��1</th>
         <th>��2</th>
		 <th>��</th>
      </tr>
		 <xsl:for-each select="ConfigurationStruct/UrovStruct">
		 <tr align="center">
         <td><xsl:value-of select="DzhModes"/></td>
		 <td><xsl:value-of select="DzhControl"/></td>
		 <td><xsl:value-of select="DzhDiff"/></td>
		 <td><xsl:value-of select="DzhSelf"/></td>
		 <td><xsl:value-of select="DzhT1"/></td>
		 <td><xsl:value-of select="DzhT2"/></td>
		 <td><xsl:value-of select="DzhT3"/></td>
		 <td><xsl:value-of select="DzhSign"/></td>
		 <td><xsl:value-of select="DzhSh1"/></td>
		 <td><xsl:value-of select="DzhSh2"/></td>
		 <td><xsl:value-of select="DzhPo"/></td>
		 </tr>
		 </xsl:for-each>
	 </table>
	 <p></p>
	 <b>�������������</b>
	 <table border="1" cellspacing="0">	 
	<tr bgcolor="4682B4">
         <th>�������</th>
         <th>l����, l�</th>
		 <th>t����, ��</th>
      </tr>
		 <xsl:for-each select="ConfigurationStruct/UrovConn/ust/UrovConnectionStruct">
			<xsl:if test="position() &lt;7">
			<tr align="center">
			<xsl:if test="position() &lt;6">
				<td>������������� <xsl:value-of select="position()"/></td>
				</xsl:if>
				<xsl:if test="position() =6"><td>ln</td></xsl:if>
				<td><xsl:value-of select="UrovJoinI"/></td>
				<td><xsl:value-of select="UrovJoinT"/></td>
				</tr>
			</xsl:if>
		 </xsl:for-each>
	 </table>
	 <p></p>
	 
	 <h3>�������� ����� TT</h3>
	 
	 <table border="1" cellspacing="0">	 
	<tr bgcolor="FF69B4">
         <th>�������� ����� ��</th>
         <th>l�min, l�</th>
		 <th>T��, ��</th>
		 <th>�����</th>
      </tr>
		 <xsl:for-each select="ConfigurationStruct/Ctt/TtChannels/ControlTt">
		 <tr align="center">
		 <td>
				<xsl:if test="position() =1">��1</xsl:if>
				<xsl:if test="position() =2">��2</xsl:if>
				<xsl:if test="position() =3">��</xsl:if>
		 </td>
         <td><xsl:value-of select="Idmin"/></td>
		 <td><xsl:value-of select="Tsrab"/></td>
		 <td><xsl:value-of select="Fault"/></td>
		 </tr>
		 </xsl:for-each>
	 </table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>