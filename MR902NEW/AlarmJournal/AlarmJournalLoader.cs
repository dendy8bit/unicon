﻿using System;
using System.Collections.Generic;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MR902NEW.AlarmJournal.Structures;

namespace BEMN.MR902NEW.AlarmJournal
{
    public class AlarmJournalLoader
    {
        #region [Private fields]
        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<AllAlarmJournalStruct> _allJournalRecords;
        /// <summary>
        /// Структура записи номер записи журнала
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _saveIndex;

        /// <summary>
        /// Текущий номер записи журнала
        /// </summary>
        private int _recordNumber;

        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Успешно прочитаны все записи
        /// </summary>
        public event Action AllJournalReadOk;
        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action ReadJournalFail;
        #endregion [Events]


        #region [Ctor's]
        /// <summary>
        /// Создаёт загрузчик Журнала аварий
        /// </summary>
        /// <param name="allJournalRecords">Объект журнала при размере записи в 1024 слова</param>
        /// <param name="saveIndex">Объект сохранения номера записи журнала</param>
        public AlarmJournalLoader(MemoryEntity<AllAlarmJournalStruct> allJournalRecords, MemoryEntity<OneWordStruct> saveIndex)
        {
            this.JournalRecords = new List<AlarmJournalRecordStruct>();
            //Записи журнала
            this._allJournalRecords = allJournalRecords;
            this._allJournalRecords.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadRecords);
            this._allJournalRecords.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
            
            //запись номера ЖО
            this._saveIndex = saveIndex;
            this._saveIndex.AllWriteOk += HandlerHelper.CreateReadArrayHandler(LoadStruct);
            this._saveIndex.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
        }
        
        private void LoadStruct()
        {
            this._allJournalRecords.LoadStruct();
        }

        #endregion [Ctor's]
        
        #region [Properties]
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<AlarmJournalRecordStruct> JournalRecords { get; }

        #endregion [Properties]
        
        #region [Private MemoryEntity Events Handlers]
        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            this.ReadJournalFail?.Invoke();
        }

        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecords()
        {
            if (this._allJournalRecords.Value.AllJournalRecords.Count != 0)
            {
                this._recordNumber += this._allJournalRecords.Value.AllJournalRecords.Count;
                this.JournalRecords.AddRange(this._allJournalRecords.Value.AllJournalRecords);
                this.SaveIdex();
            }
            else
            {
                if (this.AllJournalReadOk == null) return;
                this.AllJournalReadOk.Invoke();
            }
        }

        #endregion [Private MemoryEntity Events Handlers]
        
        #region [Public members]
        /// <summary>
        /// Запуск чтения журнала аварии
        /// </summary>
        public void StartRead()
        {
            this._recordNumber = 0;
            this.SaveIdex();
        }

        public void ClearEvents()
        {
            this._allJournalRecords?.RemoveStructQueries();
            if (_allJournalRecords != null) 
            {
                this._allJournalRecords.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this.ReadRecords);
                this._allJournalRecords.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
            }
            
            this._saveIndex?.RemoveStructQueries();
            if (_saveIndex != null)
            {
                var allJournalRecords = this._allJournalRecords;
                if (allJournalRecords != null)
                    this._saveIndex.AllWriteOk -=
                        HandlerHelper.CreateReadArrayHandler(allJournalRecords.LoadStruct);
                this._saveIndex.AllWriteFail -= HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
            }
            
        }
        #endregion [Public members]

        internal void Clear()
        {
            this.JournalRecords?.Clear();
        }

        private void SaveIdex()
        {
            this._saveIndex.Value.Word = (ushort)this._recordNumber;
            this._saveIndex.SaveStruct6();
        }
    }
}
