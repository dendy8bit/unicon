﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR902NEW.AlarmJournal.Structures;
using BEMN.MR902NEW.Configuration.Structures.Connections;
using BEMN.MR902NEW.Osc.Structures;
using BEMN.MR902NEW.Properties;
using SchemeEditorSystem.ResourceLibs;

namespace BEMN.MR902NEW.AlarmJournal
{
    public partial class AlarmJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Аварий в журнале - {0}";
        private const string READ_AJ_FAIL = "Невозможно прочитать журнал аварий";
        private const string READ_AJ = "Чтение журнала аварий";
        private const string ALARM_JOURNAL = "Журнал аварий";
        private const string TABLE_NAME = "МР902_журнал_аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string READING_LIST_FILE = "Идет чтение файла списка подписей сигналов ЖА СПЛ";
        private const string DEVICE_NAME = "MR901";
        private const string FAIL_READ =
            "Невозможно прочитать список подписей сигналов ЖА СПЛ. Списки будут сформированы по умолчанию.";
        private const string FAIL_READ_SIGN_RS =
            "Чтение подписей сигналов ЖА СПЛ по RS-485 невозмоэжно. Списки будут сформированы по умолчанию.";
        private const string LIST_FILE_NAME = "jlist.xml";
        #endregion [Constants]

        #region [Private fields]
        private readonly AlarmJournalLoader _journalLoader;
        private readonly CurrentOptionsLoader _currentOptionsLoader;
        private readonly MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        private readonly MemoryEntity<OneWordStruct> _setPageAlarmJournal;

        private readonly MemoryEntity<AllConnectionStruct> _currentConnectionsMemory;
        private AllConnectionStruct _currentConnections;

        private DataTable _table;
        private int _recordNumber;
        private int _failCounter;
        private Mr902New _device;
        private List<string> _messages;
        private FileDriver _fileDriver;
        private int[] _factors;
        private ushort _factorsDiffAndTorm;
        #endregion [Private fields]
        
        #region [Ctor's]
        public AlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public AlarmJournalForm(Mr902New device)
        {
            this.InitializeComponent();
            this._device = device;

            AllConnectionStruct.SetDeviceConnectionsType(Strings.DeviceType);

            this._currentConnectionsMemory = new MemoryEntity<AllConnectionStruct>("Присоединения (ЖА)", device, 0x106C);
            this._currentConnectionsMemory.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._currentConnections = this._currentConnectionsMemory.Value;
                _factors = _currentConnections.GetAllItt;
                this.CurrenOptionLoaded(); // запуск чтения журнала аварии
            });
            this._currentConnectionsMemory.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);

            this._currentOptionsLoader = device.CurrentOptionsLoaderAj;
            this._currentOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, _currentConnectionsMemory.LoadStruct);
            this._currentOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadJournal);
            
            if (_device.AllAlarmJournal != null)
            {
                this._journalLoader = new AlarmJournalLoader(device.AllAlarmJournal, device.SetPageJa);
                this._journalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.ReadAllRecords);
                this._journalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadJournal);
            }
            else
            {
                
                this._alarmJournal = device.AlarmJournal;
                this._alarmJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecords);
                this._alarmJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    if (this._failCounter < 80)
                    {
                        this._failCounter++;
                    }
                    else
                    {
                        this._alarmJournal.RemoveStructQueries();
                        this._failCounter = 0;
                        this.ButtonsEnabled = true;
                    }
                });

                this._setPageAlarmJournal = device.SetPageJa;
                this._setPageAlarmJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._alarmJournal.LoadStruct);
                this._setPageAlarmJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
               
            }
            
            this._fileDriver = new FileDriver(device, this);
        }
        #endregion [Ctor's]
        
        #region [Help members]

        private bool ButtonsEnabled
        {
            set
            {
                this._readAlarmJournalButton.Enabled = this._saveAlarmJournalButton.Enabled =
                    this._loadAlarmJournalButton.Enabled = this._exportButton.Enabled = value;
            }
        }

        private void OnListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "Операция успешно выполнена")
                {
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            XmlRootAttribute root = new XmlRootAttribute(DEVICE_NAME);
                            XmlSerializer serializer = new XmlSerializer(typeof(ListsOfJournals), root);
                            ListsOfJournals lists = (ListsOfJournals)serializer.Deserialize(reader);
                            this._messages = lists.AlarmJournal.MessagesList;
                        }
                    }
                }
                else
                {
                    MessageBox.Show(FAIL_READ, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this._messages = PropFormsStrings.GetNewAlarmList();
                }
            }
            catch
            {
                MessageBox.Show(FAIL_READ, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this._messages = PropFormsStrings.GetNewAlarmList();
            }
            this.StartReadOption();
        }

        private void FailReadJournal()
        {
            this._statusLabel.Text = READ_AJ_FAIL;
            this.ButtonsEnabled = true;
        }

        private void CurrenOptionLoaded()
        {
            this._table.Clear();
            this._recordNumber = this._failCounter = 0;
            if (this._device.AllAlarmJournal != null)
            {
                this._journalLoader.Clear();
                this._journalLoader.StartRead();
            }
            else
            {
                this.SavePageNumber();
            }

        }

        private void ReadRecords()
        {
            try
            {
                if (!this._alarmJournal.Value.IsEmpty)
                {
                    this._recordNumber++;
                    this._failCounter = this._recordNumber;

                    AlarmJournalRecordStruct record = this._alarmJournal.Value;
                    AllConnectionStruct measure = this._currentOptionsLoader[record.GroupOfSetpoint];

                    //_factors = measure.GetAllItt;
                    //_factorsDiffAndTorm = measure.IttJoin;

                    //_factors = _currentConnections.AllItt;

                    //for (int i = 0; i < _factors.Count; i++)
                    //{
                    //    _factors[i] = (ushort)(_factors[i] * 40);
                    //}

                    string triggeredDef = this.GetTriggeredDefence(record);
                    string parameter = this.GetTriggeredParam(record);
                    string parametrValue = this.GetParametrValue(record, _factors);

                    switch (Strings.DeviceType)
                    {
                        case "A1":
                            this._table.Rows.Add
                            (
                                this._recordNumber,
                                record.GetTime,
                                Strings.AlarmJournalMessage[record.Message],
                                triggeredDef,
                                parameter,
                                parametrValue,
                                Strings.GroupsNames[record.GroupOfSetpoint],

                                GetCurrents(record.IdaSH1, _factors, 0),
                                GetCurrents(record.IdbSH1, _factors, 0),
                                GetCurrents(record.IdcSH1, _factors, 0),
                                GetCurrents(record.ItaSH1, _factors, 0),
                                GetCurrents(record.ItbSH1, _factors, 0),
                                GetCurrents(record.ItcSH1, _factors, 0),

                                GetCurrents(record.IdaSH2, _factors, 0),
                                GetCurrents(record.IdbSH2, _factors, 0),
                                GetCurrents(record.IdcSH2, _factors, 0),
                                GetCurrents(record.ItaSH2, _factors, 0),
                                GetCurrents(record.ItbSH2, _factors, 0),
                                GetCurrents(record.ItcSH2, _factors, 0),

                                GetCurrents(record.IdaPO, _factors, 0),
                                GetCurrents(record.IdbPO, _factors, 0),
                                GetCurrents(record.IdcPO, _factors, 0),
                                GetCurrents(record.ItaPO, _factors, 0),
                                GetCurrents(record.ItbPO, _factors, 0),
                                GetCurrents(record.ItcPO, _factors, 0),

                                GetCurrents(record.I1, _factors, 1),
                                GetCurrents(record.I2, _factors, 1),
                                GetCurrents(record.I3, _factors, 1),
                                GetCurrents(record.I4, _factors, 2),
                                GetCurrents(record.I5, _factors, 2),
                                GetCurrents(record.I6, _factors, 2),
                                GetCurrents(record.I7, _factors, 3),
                                GetCurrents(record.I8, _factors, 3),
                                GetCurrents(record.I9, _factors, 3),
                                GetCurrents(record.I10, _factors, 4),
                                GetCurrents(record.I11, _factors, 4),
                                GetCurrents(record.I10, _factors, 4),
                                GetCurrents(record.I13, _factors, 5),
                                GetCurrents(record.I14, _factors, 5),
                                GetCurrents(record.I15, _factors, 5),
                                GetCurrents(record.I16, _factors, 6),

                                record.D1To8,
                                record.D9To16,
                                record.D17To24,
                                record.D25To32,
                                record.D33To40,
                                record.D41To48,
                                record.D49To56,
                                record.D57To64
                            );
                            break;
                        case "A2":
                            this._table.Rows.Add
                            (
                                this._recordNumber,
                                record.GetTime,
                                Strings.AlarmJournalMessage[record.Message],
                                triggeredDef,
                                parameter,
                                parametrValue,
                                Strings.GroupsNames[record.GroupOfSetpoint],

                                GetCurrents(record.IdaSH1, _factors, 0),
                                GetCurrents(record.IdbSH1, _factors, 0),
                                GetCurrents(record.IdcSH1, _factors, 0),
                                GetCurrents(record.ItaSH1, _factors, 0),
                                GetCurrents(record.ItbSH1, _factors, 0),
                                GetCurrents(record.ItcSH1, _factors, 0),

                                GetCurrents(record.IdaSH2, _factors, 0),
                                GetCurrents(record.IdbSH2, _factors, 0),
                                GetCurrents(record.IdcSH2, _factors, 0),
                                GetCurrents(record.ItaSH2, _factors, 0),
                                GetCurrents(record.ItbSH2, _factors, 0),
                                GetCurrents(record.ItcSH2, _factors, 0),

                                GetCurrents(record.IdaPO, _factors, 0),
                                GetCurrents(record.IdbPO, _factors, 0),
                                GetCurrents(record.IdcPO, _factors, 0),
                                GetCurrents(record.ItaPO, _factors, 0),
                                GetCurrents(record.ItbPO, _factors, 0),
                                GetCurrents(record.ItcPO, _factors, 0),

                                GetCurrents(record.I1, _factors, 1),
                                GetCurrents(record.I2, _factors, 1),
                                GetCurrents(record.I3, _factors, 1),
                                GetCurrents(record.I4, _factors, 2),
                                GetCurrents(record.I5, _factors, 2),
                                GetCurrents(record.I6, _factors, 2),
                                GetCurrents(record.I7, _factors, 3),
                                GetCurrents(record.I8, _factors, 3),
                                GetCurrents(record.I9, _factors, 3),
                                GetCurrents(record.I10, _factors, 4),
                                GetCurrents(record.I11, _factors, 4),
                                GetCurrents(record.I10, _factors, 4),
                                GetCurrents(record.I13, _factors, 5),
                                GetCurrents(record.I14, _factors, 5),
                                GetCurrents(record.I15, _factors, 5),
                                GetCurrents(record.I16, _factors, 6),
                                GetCurrents(record.I17, _factors, 6),
                                GetCurrents(record.I18, _factors, 6),
                                GetCurrents(record.I19, _factors, 7),
                                GetCurrents(record.I20, _factors, 7),
                                GetCurrents(record.I21, _factors, 7),
                                GetCurrents(record.I22, _factors, 8),
                                GetCurrents(record.I23, _factors, 8),
                                GetCurrents(record.I24, _factors, 8),

                                record.D1To8,
                                record.D9To16,
                                record.D17To24,
                                record.D25To32,
                                record.D33To40
                            );
                            break;
                        case "A3":
                            this._table.Rows.Add
                               (
                                   this._recordNumber,
                                   record.GetTime,
                                   Strings.AlarmJournalMessage[record.Message],
                                   triggeredDef,
                                   parameter,
                                   parametrValue,
                                   Strings.GroupsNames[record.GroupOfSetpoint],

                                   GetCurrents(record.IdaSH1, _factors, 0),
                                   GetCurrents(record.IdbSH1, _factors, 0),
                                   GetCurrents(record.IdcSH1, _factors, 0),
                                   GetCurrents(record.ItaSH1, _factors, 0),
                                   GetCurrents(record.ItbSH1, _factors, 0),
                                   GetCurrents(record.ItcSH1, _factors, 0),

                                   GetCurrents(record.IdaSH2, _factors, 0),
                                   GetCurrents(record.IdbSH2, _factors, 0),
                                   GetCurrents(record.IdcSH2, _factors, 0),
                                   GetCurrents(record.ItaSH2, _factors, 0),
                                   GetCurrents(record.ItbSH2, _factors, 0),
                                   GetCurrents(record.ItcSH2, _factors, 0),

                                   GetCurrents(record.IdaPO, _factors, 0),
                                   GetCurrents(record.IdbPO, _factors, 0),
                                   GetCurrents(record.IdcPO, _factors, 0),
                                   GetCurrents(record.ItaPO, _factors, 0),
                                   GetCurrents(record.ItbPO, _factors, 0),
                                   GetCurrents(record.ItcPO, _factors, 0),

                                   GetCurrents(record.I1, _factors, 1),
                                   GetCurrents(record.I2, _factors, 1),
                                   GetCurrents(record.I3, _factors, 1),
                                   GetCurrents(record.I4, _factors, 2),
                                   GetCurrents(record.I5, _factors, 2),
                                   GetCurrents(record.I6, _factors, 2),
                                   GetCurrents(record.I7, _factors, 3),
                                   GetCurrents(record.I8, _factors, 3),
                                   GetCurrents(record.I9, _factors, 3),
                                   GetCurrents(record.I10, _factors, 4),
                                   GetCurrents(record.I11, _factors, 4),
                                   GetCurrents(record.I10, _factors, 4),
                                   GetCurrents(record.I13, _factors, 5),
                                   GetCurrents(record.I14, _factors, 5),
                                   GetCurrents(record.I15, _factors, 5),
                                   GetCurrents(record.I16, _factors, 6),
                                   GetCurrents(record.I17, _factors, 6),
                                   GetCurrents(record.I18, _factors, 6),
                                   GetCurrents(record.I19, _factors, 7),
                                   GetCurrents(record.I20, _factors, 7),
                                   GetCurrents(record.I21, _factors, 7),
                                   GetCurrents(record.I22, _factors, 8),
                                   GetCurrents(record.I23, _factors, 8),
                                   GetCurrents(record.I24, _factors, 8),

                                   record.D1To8,
                                   record.D9To16,
                                   record.D17To24
                               );
                            break;
                        case "A4":
                            this._table.Rows.Add
                               (
                                   this._recordNumber,
                                   record.GetTime,
                                   Strings.AlarmJournalMessage[record.Message],
                                   triggeredDef,
                                   parameter,
                                   parametrValue,
                                   Strings.GroupsNames[record.GroupOfSetpoint],

                                   GetCurrents(record.IdaSH1, _factors, 0),
                                   GetCurrents(record.IdbSH1, _factors, 0),
                                   GetCurrents(record.IdcSH1, _factors, 0),
                                   GetCurrents(record.ItaSH1, _factors, 0),
                                   GetCurrents(record.ItbSH1, _factors, 0),
                                   GetCurrents(record.ItcSH1, _factors, 0),

                                   GetCurrents(record.IdaSH2, _factors, 0),
                                   GetCurrents(record.IdbSH2, _factors, 0),
                                   GetCurrents(record.IdcSH2, _factors, 0),
                                   GetCurrents(record.ItaSH2, _factors, 0),
                                   GetCurrents(record.ItbSH2, _factors, 0),
                                   GetCurrents(record.ItcSH2, _factors, 0),

                                   GetCurrents(record.IdaPO, _factors, 0),
                                   GetCurrents(record.IdbPO, _factors, 0),
                                   GetCurrents(record.IdcPO, _factors, 0),
                                   GetCurrents(record.ItaPO, _factors, 0),
                                   GetCurrents(record.ItbPO, _factors, 0),
                                   GetCurrents(record.ItcPO, _factors, 0),

                                   GetCurrents(record.I1, _factors, 1),
                                   GetCurrents(record.I2, _factors, 1),
                                   GetCurrents(record.I3, _factors, 1),
                                   GetCurrents(record.I4, _factors, 2),
                                   GetCurrents(record.I5, _factors, 2),
                                   GetCurrents(record.I6, _factors, 2),
                                   GetCurrents(record.I7, _factors, 3),
                                   GetCurrents(record.I8, _factors, 3),
                                   GetCurrents(record.I9, _factors, 3),
                                   GetCurrents(record.I10, _factors, 4),
                                   GetCurrents(record.I11, _factors, 4),
                                   GetCurrents(record.I10, _factors, 4),
                                   GetCurrents(record.I13, _factors, 5),
                                   GetCurrents(record.I14, _factors, 5),
                                   GetCurrents(record.I15, _factors, 5),
                                   GetCurrents(record.I16, _factors, 6),
                                   GetCurrents(record.I17, _factors, 6),
                                   GetCurrents(record.I18, _factors, 6),
                                   GetCurrents(record.I19, _factors, 7),
                                   GetCurrents(record.I20, _factors, 7),
                                   GetCurrents(record.I21, _factors, 7),
                                   GetCurrents(record.I22, _factors, 8),
                                   GetCurrents(record.I23, _factors, 8),
                                   GetCurrents(record.I24, _factors, 8),

                                   record.D1To8,
                                   record.D9To16,
                                   record.D17To24,
                                   record.D25To32
                               );
                            break;
                        default:
                            this._table.Rows.Add
                               (
                                   this._recordNumber,
                                   record.GetTime,
                                   Strings.AlarmJournalMessage[record.Message],
                                   triggeredDef,
                                   parameter,
                                   parametrValue,
                                   Strings.GroupsNames[record.GroupOfSetpoint],

                                   GetCurrents(record.IdaSH1, _factors, 0),
                                   GetCurrents(record.IdbSH1, _factors, 0),
                                   GetCurrents(record.IdcSH1, _factors, 0),
                                   GetCurrents(record.ItaSH1, _factors, 0),
                                   GetCurrents(record.ItbSH1, _factors, 0),
                                   GetCurrents(record.ItcSH1, _factors, 0),

                                   GetCurrents(record.IdaSH2, _factors, 0),
                                   GetCurrents(record.IdbSH2, _factors, 0),
                                   GetCurrents(record.IdcSH2, _factors, 0),
                                   GetCurrents(record.ItaSH2, _factors, 0),
                                   GetCurrents(record.ItbSH2, _factors, 0),
                                   GetCurrents(record.ItcSH2, _factors, 0),

                                   GetCurrents(record.IdaPO, _factors, 0),
                                   GetCurrents(record.IdbPO, _factors, 0),
                                   GetCurrents(record.IdcPO, _factors, 0),
                                   GetCurrents(record.ItaPO, _factors, 0),
                                   GetCurrents(record.ItbPO, _factors, 0),
                                   GetCurrents(record.ItcPO, _factors, 0),

                                   GetCurrents(record.I1, _factors, 1),
                                   GetCurrents(record.I2, _factors, 1),
                                   GetCurrents(record.I3, _factors, 1),
                                   GetCurrents(record.I4, _factors, 2),
                                   GetCurrents(record.I5, _factors, 2),
                                   GetCurrents(record.I6, _factors, 2),
                                   GetCurrents(record.I7, _factors, 3),
                                   GetCurrents(record.I8, _factors, 3),
                                   GetCurrents(record.I9, _factors, 3),
                                   GetCurrents(record.I10, _factors, 4),
                                   GetCurrents(record.I11, _factors, 4),
                                   GetCurrents(record.I10, _factors, 4),
                                   GetCurrents(record.I13, _factors, 5),
                                   GetCurrents(record.I14, _factors, 5),
                                   GetCurrents(record.I15, _factors, 5),
                                   GetCurrents(record.I16, _factors, 6),

                                   record.D1To8,
                                   record.D9To16,
                                   record.D17To24
                               );
                            break;
                    }

                    this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                    this.statusStrip1.Update();
                    this.SavePageNumber();
                }
                else
                {
                    if (this._table.Rows.Count == 0)
                    {
                        this._statusLabel.Text = JOURNAL_IS_EMPTY;
                    }
                    this.ButtonsEnabled = true;
                }
            }
            catch (Exception e)
            {
                this._table.Rows.Add
                (
                    this._recordNumber - 1, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", ""
                );
            }
        }

        private void ReadAllRecords()
        {
            if (this._journalLoader.JournalRecords?.Count == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
                this.ButtonsEnabled = true;
                return;
            }

            this._recordNumber = 1;
            
            var journalLoaderJournalRecords = this._journalLoader?.JournalRecords;

            if (journalLoaderJournalRecords != null)
                foreach (AlarmJournalRecordStruct record in journalLoaderJournalRecords)
                {
                    try
                    {
                        AllConnectionStruct measure = this._currentOptionsLoader[record.GroupOfSetpoint];

                        //_factors = _currentConnections.AllItt;

                        //_factors = measure.GetAllItt;

                        //for (int i = 0; i < _factors.Count; i++)
                        //{
                        //    _factors[i] = (ushort) (_factors[i] * 40);
                        //}

                        string triggeredDef = this.GetTriggeredDefence(record);
                        string parameter = this.GetTriggeredParam(record);
                        string parametrValue = this.GetParametrValue(record, _factors);

                        switch (Strings.DeviceType)
                        {
                            case "A1":
                                this._table.Rows.Add
                                (
                                    this._recordNumber,
                                    record.GetTime,
                                    Strings.AlarmJournalMessage[record.Message],
                                    triggeredDef,
                                    parameter,
                                    parametrValue,
                                    Strings.GroupsNames[record.GroupOfSetpoint],

                                    GetCurrents(record.IdaSH1, _factors, 0),
                                    GetCurrents(record.IdbSH1, _factors, 0),
                                    GetCurrents(record.IdcSH1, _factors, 0),
                                    GetCurrents(record.ItaSH1, _factors, 0),
                                    GetCurrents(record.ItbSH1, _factors, 0),
                                    GetCurrents(record.ItcSH1, _factors, 0),

                                    GetCurrents(record.IdaSH2, _factors, 0),
                                    GetCurrents(record.IdbSH2, _factors, 0),
                                    GetCurrents(record.IdcSH2, _factors, 0),
                                    GetCurrents(record.ItaSH2, _factors, 0),
                                    GetCurrents(record.ItbSH2, _factors, 0),
                                    GetCurrents(record.ItcSH2, _factors, 0),

                                    GetCurrents(record.IdaPO, _factors, 0),
                                    GetCurrents(record.IdbPO, _factors, 0),
                                    GetCurrents(record.IdcPO, _factors, 0),
                                    GetCurrents(record.ItaPO, _factors, 0),
                                    GetCurrents(record.ItbPO, _factors, 0),
                                    GetCurrents(record.ItcPO, _factors, 0),

                                    GetCurrents(record.I1, _factors, 1),
                                    GetCurrents(record.I2, _factors, 1),
                                    GetCurrents(record.I3, _factors, 1),
                                    GetCurrents(record.I4, _factors, 2),
                                    GetCurrents(record.I5, _factors, 2),
                                    GetCurrents(record.I6, _factors, 2),
                                    GetCurrents(record.I7, _factors, 3),
                                    GetCurrents(record.I8, _factors, 3),
                                    GetCurrents(record.I9, _factors, 3),
                                    GetCurrents(record.I10, _factors, 4),
                                    GetCurrents(record.I11, _factors, 4),
                                    GetCurrents(record.I10, _factors, 4),
                                    GetCurrents(record.I13, _factors, 5),
                                    GetCurrents(record.I14, _factors, 5),
                                    GetCurrents(record.I15, _factors, 5),
                                    GetCurrents(record.I16, _factors, 6),

                                    record.D1To8,
                                    record.D9To16,
                                    record.D17To24,
                                    record.D25To32,
                                    record.D33To40,
                                    record.D41To48,
                                    record.D49To56,
                                    record.D57To64
                                );
                                break;
                            case "A2":
                                this._table.Rows.Add
                                (
                                    this._recordNumber,
                                    record.GetTime,
                                    Strings.AlarmJournalMessage[record.Message],
                                    triggeredDef,
                                    parameter,
                                    parametrValue,
                                    Strings.GroupsNames[record.GroupOfSetpoint],

                                    GetCurrents(record.IdaSH1, _factors, 0),
                                    GetCurrents(record.IdbSH1, _factors, 0),
                                    GetCurrents(record.IdcSH1, _factors, 0),
                                    GetCurrents(record.ItaSH1, _factors, 0),
                                    GetCurrents(record.ItbSH1, _factors, 0),
                                    GetCurrents(record.ItcSH1, _factors, 0),

                                    GetCurrents(record.IdaSH2, _factors, 0),
                                    GetCurrents(record.IdbSH2, _factors, 0),
                                    GetCurrents(record.IdcSH2, _factors, 0),
                                    GetCurrents(record.ItaSH2, _factors, 0),
                                    GetCurrents(record.ItbSH2, _factors, 0),
                                    GetCurrents(record.ItcSH2, _factors, 0),

                                    GetCurrents(record.IdaPO, _factors, 0),
                                    GetCurrents(record.IdbPO, _factors, 0),
                                    GetCurrents(record.IdcPO, _factors, 0),
                                    GetCurrents(record.ItaPO, _factors, 0),
                                    GetCurrents(record.ItbPO, _factors, 0),
                                    GetCurrents(record.ItcPO, _factors, 0),

                                    GetCurrents(record.I1, _factors, 1),
                                    GetCurrents(record.I2, _factors, 1),
                                    GetCurrents(record.I3, _factors, 1),
                                    GetCurrents(record.I4, _factors, 2),
                                    GetCurrents(record.I5, _factors, 2),
                                    GetCurrents(record.I6, _factors, 2),
                                    GetCurrents(record.I7, _factors, 3),
                                    GetCurrents(record.I8, _factors, 3),
                                    GetCurrents(record.I9, _factors, 3),
                                    GetCurrents(record.I10, _factors, 4),
                                    GetCurrents(record.I11, _factors, 4),
                                    GetCurrents(record.I10, _factors, 4),
                                    GetCurrents(record.I13, _factors, 5),
                                    GetCurrents(record.I14, _factors, 5),
                                    GetCurrents(record.I15, _factors, 5),
                                    GetCurrents(record.I16, _factors, 6),
                                    GetCurrents(record.I17, _factors, 6),
                                    GetCurrents(record.I18, _factors, 6),
                                    GetCurrents(record.I19, _factors, 7),
                                    GetCurrents(record.I20, _factors, 7),
                                    GetCurrents(record.I21, _factors, 7),
                                    GetCurrents(record.I22, _factors, 8),
                                    GetCurrents(record.I23, _factors, 8),
                                    GetCurrents(record.I24, _factors, 8),

                                    record.D1To8,
                                    record.D9To16,
                                    record.D17To24,
                                    record.D25To32,
                                    record.D33To40
                                );
                                break;
                            case "A3":
                                this._table.Rows.Add
                                   (
                                       this._recordNumber,
                                       record.GetTime,
                                       Strings.AlarmJournalMessage[record.Message],
                                       triggeredDef,
                                       parameter,
                                       parametrValue,
                                       Strings.GroupsNames[record.GroupOfSetpoint],

                                       GetCurrents(record.IdaSH1, _factors, 0),
                                       GetCurrents(record.IdbSH1, _factors, 0),
                                       GetCurrents(record.IdcSH1, _factors, 0),
                                       GetCurrents(record.ItaSH1, _factors, 0),
                                       GetCurrents(record.ItbSH1, _factors, 0),
                                       GetCurrents(record.ItcSH1, _factors, 0),

                                       GetCurrents(record.IdaSH2, _factors, 0),
                                       GetCurrents(record.IdbSH2, _factors, 0),
                                       GetCurrents(record.IdcSH2, _factors, 0),
                                       GetCurrents(record.ItaSH2, _factors, 0),
                                       GetCurrents(record.ItbSH2, _factors, 0),
                                       GetCurrents(record.ItcSH2, _factors, 0),

                                       GetCurrents(record.IdaPO, _factors, 0),
                                       GetCurrents(record.IdbPO, _factors, 0),
                                       GetCurrents(record.IdcPO, _factors, 0),
                                       GetCurrents(record.ItaPO, _factors, 0),
                                       GetCurrents(record.ItbPO, _factors, 0),
                                       GetCurrents(record.ItcPO, _factors, 0),

                                       GetCurrents(record.I1, _factors, 1),
                                       GetCurrents(record.I2, _factors, 1),
                                       GetCurrents(record.I3, _factors, 1),
                                       GetCurrents(record.I4, _factors, 2),
                                       GetCurrents(record.I5, _factors, 2),
                                       GetCurrents(record.I6, _factors, 2),
                                       GetCurrents(record.I7, _factors, 3),
                                       GetCurrents(record.I8, _factors, 3),
                                       GetCurrents(record.I9, _factors, 3),
                                       GetCurrents(record.I10, _factors, 4),
                                       GetCurrents(record.I11, _factors, 4),
                                       GetCurrents(record.I10, _factors, 4),
                                       GetCurrents(record.I13, _factors, 5),
                                       GetCurrents(record.I14, _factors, 5),
                                       GetCurrents(record.I15, _factors, 5),
                                       GetCurrents(record.I16, _factors, 6),
                                       GetCurrents(record.I17, _factors, 6),
                                       GetCurrents(record.I18, _factors, 6),
                                       GetCurrents(record.I19, _factors, 7),
                                       GetCurrents(record.I20, _factors, 7),
                                       GetCurrents(record.I21, _factors, 7),
                                       GetCurrents(record.I22, _factors, 8),
                                       GetCurrents(record.I23, _factors, 8),
                                       GetCurrents(record.I24, _factors, 8),

                                       record.D1To8,
                                       record.D9To16,
                                       record.D17To24
                                   );
                                break;
                            case "A4":
                                this._table.Rows.Add
                                   (
                                       this._recordNumber,
                                       record.GetTime,
                                       Strings.AlarmJournalMessage[record.Message],
                                       triggeredDef,
                                       parameter,
                                       parametrValue,
                                       Strings.GroupsNames[record.GroupOfSetpoint],

                                       GetCurrents(record.IdaSH1, _factors, 0),
                                       GetCurrents(record.IdbSH1, _factors, 0),
                                       GetCurrents(record.IdcSH1, _factors, 0),
                                       GetCurrents(record.ItaSH1, _factors, 0),
                                       GetCurrents(record.ItbSH1, _factors, 0),
                                       GetCurrents(record.ItcSH1, _factors, 0),

                                       GetCurrents(record.IdaSH2, _factors, 0),
                                       GetCurrents(record.IdbSH2, _factors, 0),
                                       GetCurrents(record.IdcSH2, _factors, 0),
                                       GetCurrents(record.ItaSH2, _factors, 0),
                                       GetCurrents(record.ItbSH2, _factors, 0),
                                       GetCurrents(record.ItcSH2, _factors, 0),

                                       GetCurrents(record.IdaPO, _factors, 0),
                                       GetCurrents(record.IdbPO, _factors, 0),
                                       GetCurrents(record.IdcPO, _factors, 0),
                                       GetCurrents(record.ItaPO, _factors, 0),
                                       GetCurrents(record.ItbPO, _factors, 0),
                                       GetCurrents(record.ItcPO, _factors, 0),

                                       GetCurrents(record.I1, _factors, 1),
                                       GetCurrents(record.I2, _factors, 1),
                                       GetCurrents(record.I3, _factors, 1),
                                       GetCurrents(record.I4, _factors, 2),
                                       GetCurrents(record.I5, _factors, 2),
                                       GetCurrents(record.I6, _factors, 2),
                                       GetCurrents(record.I7, _factors, 3),
                                       GetCurrents(record.I8, _factors, 3),
                                       GetCurrents(record.I9, _factors, 3),
                                       GetCurrents(record.I10, _factors, 4),
                                       GetCurrents(record.I11, _factors, 4),
                                       GetCurrents(record.I10, _factors, 4),
                                       GetCurrents(record.I13, _factors, 5),
                                       GetCurrents(record.I14, _factors, 5),
                                       GetCurrents(record.I15, _factors, 5),
                                       GetCurrents(record.I16, _factors, 6),
                                       GetCurrents(record.I17, _factors, 6),
                                       GetCurrents(record.I18, _factors, 6),
                                       GetCurrents(record.I19, _factors, 7),
                                       GetCurrents(record.I20, _factors, 7),
                                       GetCurrents(record.I21, _factors, 7),
                                       GetCurrents(record.I22, _factors, 8),
                                       GetCurrents(record.I23, _factors, 8),
                                       GetCurrents(record.I24, _factors, 8),

                                       record.D1To8,
                                       record.D9To16,
                                       record.D17To24,
                                       record.D25To32
                                   );
                                break;
                            default:
                                this._table.Rows.Add
                                   (
                                       this._recordNumber,
                                       record.GetTime,
                                       Strings.AlarmJournalMessage[record.Message],
                                       triggeredDef,
                                       parameter,
                                       parametrValue,
                                       Strings.GroupsNames[record.GroupOfSetpoint],

                                       GetCurrents(record.IdaSH1, _factors, 0),
                                       GetCurrents(record.IdbSH1, _factors, 0),
                                       GetCurrents(record.IdcSH1, _factors, 0),
                                       GetCurrents(record.ItaSH1, _factors, 0),
                                       GetCurrents(record.ItbSH1, _factors, 0),
                                       GetCurrents(record.ItcSH1, _factors, 0),

                                       GetCurrents(record.IdaSH2, _factors, 0),
                                       GetCurrents(record.IdbSH2, _factors, 0),
                                       GetCurrents(record.IdcSH2, _factors, 0),
                                       GetCurrents(record.ItaSH2, _factors, 0),
                                       GetCurrents(record.ItbSH2, _factors, 0),
                                       GetCurrents(record.ItcSH2, _factors, 0),

                                       GetCurrents(record.IdaPO, _factors, 0),
                                       GetCurrents(record.IdbPO, _factors, 0),
                                       GetCurrents(record.IdcPO, _factors, 0),
                                       GetCurrents(record.ItaPO, _factors, 0),
                                       GetCurrents(record.ItbPO, _factors, 0),
                                       GetCurrents(record.ItcPO, _factors, 0),

                                       GetCurrents(record.I1, _factors, 1),
                                       GetCurrents(record.I2, _factors, 1),
                                       GetCurrents(record.I3, _factors, 1),
                                       GetCurrents(record.I4, _factors, 2),
                                       GetCurrents(record.I5, _factors, 2),
                                       GetCurrents(record.I6, _factors, 2),
                                       GetCurrents(record.I7, _factors, 3),
                                       GetCurrents(record.I8, _factors, 3),
                                       GetCurrents(record.I9, _factors, 3),
                                       GetCurrents(record.I10, _factors, 4),
                                       GetCurrents(record.I11, _factors, 4),
                                       GetCurrents(record.I10, _factors, 4),
                                       GetCurrents(record.I13, _factors, 5),
                                       GetCurrents(record.I14, _factors, 5),
                                       GetCurrents(record.I15, _factors, 5),
                                       GetCurrents(record.I16, _factors, 6),

                                       record.D1To8,
                                       record.D9To16,
                                       record.D17To24
                                   );
                                break;
                        }
                       
                    }
                    catch (Exception e)
                    {
                        this._table.Rows.Add
                        (
                            this._recordNumber - 1, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "",
                            "", "",
                            "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                            "", "", "", "", "", "", "", "", "", "", ""
                        );
                    }

                    this._recordNumber++;
                }


            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber - 1);
            this.statusStrip1.Update();
            this.ButtonsEnabled = true;
        }

        private string GetTriggeredParam(AlarmJournalRecordStruct record)
        {
            return record.NumberOfTriggeredParametr == 48 ? this._messages[record.ValueOfTriggeredParametr] : Strings.AlarmJournalTrigged[record.NumberOfTriggeredParametr];
        }

        private void SavePageNumber()
        {
            this._setPageAlarmJournal.Value.Word = (ushort)this._recordNumber;
            this._setPageAlarmJournal.SaveStruct();
        }

        private void ReadRecordFromBinFile()
        {
            if (this._journalLoader.JournalRecords.Count == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
                this.ButtonsEnabled = true;
                return;
            }

            this._recordNumber = 1;

            foreach (AlarmJournalRecordStruct record in this._journalLoader.JournalRecords)
            {
                try
                {
                    AllConnectionStruct measure = this._currentOptionsLoader[record.GroupOfSetpoint];

                    _factors = measure.GetAllItt;

                    //for (int i = 0; i < _factors.Count; i++)
                    //{
                    //    _factors[i] = (ushort)(_factors[i] * 40);
                    //}

                    string triggeredDef = this.GetTriggeredDefence(record);
                    string parameter = this.GetTriggeredParam(record);
                    string parametrValue = this.GetParametrValue(record, _factors);

                    // вывод в таблицу. скорее всего, размер таблицы, столбцы, которые надо будет выводить, нужно будет менять динамически

                    switch (Strings.DeviceType)
                    {
                        case "A1":
                            this._table.Rows.Add
                            (
                                this._recordNumber,
                                record.GetTime,
                                Strings.AlarmJournalMessage[record.Message],
                                triggeredDef,
                                parameter,
                                parametrValue,
                                Strings.GroupsNames[record.GroupOfSetpoint],

                                GetCurrents(record.IdaSH1, _factors, 0),
                                GetCurrents(record.IdbSH1, _factors, 0),
                                GetCurrents(record.IdcSH1, _factors, 0),
                                GetCurrents(record.ItaSH1, _factors, 0),
                                GetCurrents(record.ItbSH1, _factors, 0),
                                GetCurrents(record.ItcSH1, _factors, 0),

                                GetCurrents(record.IdaSH2, _factors, 0),
                                GetCurrents(record.IdbSH2, _factors, 0),
                                GetCurrents(record.IdcSH2, _factors, 0),
                                GetCurrents(record.ItaSH2, _factors, 0),
                                GetCurrents(record.ItbSH2, _factors, 0),
                                GetCurrents(record.ItcSH2, _factors, 0),

                                GetCurrents(record.IdaPO, _factors, 0),
                                GetCurrents(record.IdbPO, _factors, 0),
                                GetCurrents(record.IdcPO, _factors, 0),
                                GetCurrents(record.ItaPO, _factors, 0),
                                GetCurrents(record.ItbPO, _factors, 0),
                                GetCurrents(record.ItcPO, _factors, 0),

                                GetCurrents(record.I1, _factors, 1),
                                GetCurrents(record.I2, _factors, 1),
                                GetCurrents(record.I3, _factors, 1),
                                GetCurrents(record.I4, _factors, 2),
                                GetCurrents(record.I5, _factors, 2),
                                GetCurrents(record.I6, _factors, 2),
                                GetCurrents(record.I7, _factors, 3),
                                GetCurrents(record.I8, _factors, 3),
                                GetCurrents(record.I9, _factors, 3),
                                GetCurrents(record.I10, _factors, 4),
                                GetCurrents(record.I11, _factors, 4),
                                GetCurrents(record.I10, _factors, 4),
                                GetCurrents(record.I13, _factors, 5),
                                GetCurrents(record.I14, _factors, 5),
                                GetCurrents(record.I15, _factors, 5),
                                GetCurrents(record.I16, _factors, 6),

                                record.D1To8,
                                record.D9To16,
                                record.D17To24,
                                record.D25To32,
                                record.D33To40,
                                record.D41To48,
                                record.D49To56,
                                record.D57To64
                            );
                            break;
                        case "A2":
                            this._table.Rows.Add
                            (
                                this._recordNumber,
                                record.GetTime,
                                Strings.AlarmJournalMessage[record.Message],
                                triggeredDef,
                                parameter,
                                parametrValue,
                                Strings.GroupsNames[record.GroupOfSetpoint],

                                GetCurrents(record.IdaSH1, _factors, 0),
                                GetCurrents(record.IdbSH1, _factors, 0),
                                GetCurrents(record.IdcSH1, _factors, 0),
                                GetCurrents(record.ItaSH1, _factors, 0),
                                GetCurrents(record.ItbSH1, _factors, 0),
                                GetCurrents(record.ItcSH1, _factors, 0),

                                GetCurrents(record.IdaSH2, _factors, 0),
                                GetCurrents(record.IdbSH2, _factors, 0),
                                GetCurrents(record.IdcSH2, _factors, 0),
                                GetCurrents(record.ItaSH2, _factors, 0),
                                GetCurrents(record.ItbSH2, _factors, 0),
                                GetCurrents(record.ItcSH2, _factors, 0),

                                GetCurrents(record.IdaPO, _factors, 0),
                                GetCurrents(record.IdbPO, _factors, 0),
                                GetCurrents(record.IdcPO, _factors, 0),
                                GetCurrents(record.ItaPO, _factors, 0),
                                GetCurrents(record.ItbPO, _factors, 0),
                                GetCurrents(record.ItcPO, _factors, 0),

                                GetCurrents(record.I1, _factors, 1),
                                GetCurrents(record.I2, _factors, 1),
                                GetCurrents(record.I3, _factors, 1),
                                GetCurrents(record.I4, _factors, 2),
                                GetCurrents(record.I5, _factors, 2),
                                GetCurrents(record.I6, _factors, 2),
                                GetCurrents(record.I7, _factors, 3),
                                GetCurrents(record.I8, _factors, 3),
                                GetCurrents(record.I9, _factors, 3),
                                GetCurrents(record.I10, _factors, 4),
                                GetCurrents(record.I11, _factors, 4),
                                GetCurrents(record.I10, _factors, 4),
                                GetCurrents(record.I13, _factors, 5),
                                GetCurrents(record.I14, _factors, 5),
                                GetCurrents(record.I15, _factors, 5),
                                GetCurrents(record.I16, _factors, 6),
                                GetCurrents(record.I17, _factors, 6),
                                GetCurrents(record.I18, _factors, 6),
                                GetCurrents(record.I19, _factors, 7),
                                GetCurrents(record.I20, _factors, 7),
                                GetCurrents(record.I21, _factors, 7),
                                GetCurrents(record.I22, _factors, 8),
                                GetCurrents(record.I23, _factors, 8),
                                GetCurrents(record.I24, _factors, 8),

                                record.D1To8,
                                record.D9To16,
                                record.D17To24,
                                record.D25To32,
                                record.D33To40
                            );
                            break;
                        case "A3":
                            this._table.Rows.Add
                               (
                                   this._recordNumber,
                                   record.GetTime,
                                   Strings.AlarmJournalMessage[record.Message],
                                   triggeredDef,
                                   parameter,
                                   parametrValue,
                                   Strings.GroupsNames[record.GroupOfSetpoint],

                                   GetCurrents(record.IdaSH1, _factors, 0),
                                   GetCurrents(record.IdbSH1, _factors, 0),
                                   GetCurrents(record.IdcSH1, _factors, 0),
                                   GetCurrents(record.ItaSH1, _factors, 0),
                                   GetCurrents(record.ItbSH1, _factors, 0),
                                   GetCurrents(record.ItcSH1, _factors, 0),

                                   GetCurrents(record.IdaSH2, _factors, 0),
                                   GetCurrents(record.IdbSH2, _factors, 0),
                                   GetCurrents(record.IdcSH2, _factors, 0),
                                   GetCurrents(record.ItaSH2, _factors, 0),
                                   GetCurrents(record.ItbSH2, _factors, 0),
                                   GetCurrents(record.ItcSH2, _factors, 0),

                                   GetCurrents(record.IdaPO, _factors, 0),
                                   GetCurrents(record.IdbPO, _factors, 0),
                                   GetCurrents(record.IdcPO, _factors, 0),
                                   GetCurrents(record.ItaPO, _factors, 0),
                                   GetCurrents(record.ItbPO, _factors, 0),
                                   GetCurrents(record.ItcPO, _factors, 0),

                                   GetCurrents(record.I1, _factors, 1),
                                   GetCurrents(record.I2, _factors, 1),
                                   GetCurrents(record.I3, _factors, 1),
                                   GetCurrents(record.I4, _factors, 2),
                                   GetCurrents(record.I5, _factors, 2),
                                   GetCurrents(record.I6, _factors, 2),
                                   GetCurrents(record.I7, _factors, 3),
                                   GetCurrents(record.I8, _factors, 3),
                                   GetCurrents(record.I9, _factors, 3),
                                   GetCurrents(record.I10, _factors, 4),
                                   GetCurrents(record.I11, _factors, 4),
                                   GetCurrents(record.I10, _factors, 4),
                                   GetCurrents(record.I13, _factors, 5),
                                   GetCurrents(record.I14, _factors, 5),
                                   GetCurrents(record.I15, _factors, 5),
                                   GetCurrents(record.I16, _factors, 6),
                                   GetCurrents(record.I17, _factors, 6),
                                   GetCurrents(record.I18, _factors, 6),
                                   GetCurrents(record.I19, _factors, 7),
                                   GetCurrents(record.I20, _factors, 7),
                                   GetCurrents(record.I21, _factors, 7),
                                   GetCurrents(record.I22, _factors, 8),
                                   GetCurrents(record.I23, _factors, 8),
                                   GetCurrents(record.I24, _factors, 8),

                                   record.D1To8,
                                   record.D9To16,
                                   record.D17To24
                               );
                            break;
                        case "A4":
                            this._table.Rows.Add
                               (
                                   this._recordNumber,
                                   record.GetTime,
                                   Strings.AlarmJournalMessage[record.Message],
                                   triggeredDef,
                                   parameter,
                                   parametrValue,
                                   Strings.GroupsNames[record.GroupOfSetpoint],

                                   GetCurrents(record.IdaSH1, _factors, 0),
                                   GetCurrents(record.IdbSH1, _factors, 0),
                                   GetCurrents(record.IdcSH1, _factors, 0),
                                   GetCurrents(record.ItaSH1, _factors, 0),
                                   GetCurrents(record.ItbSH1, _factors, 0),
                                   GetCurrents(record.ItcSH1, _factors, 0),

                                   GetCurrents(record.IdaSH2, _factors, 0),
                                   GetCurrents(record.IdbSH2, _factors, 0),
                                   GetCurrents(record.IdcSH2, _factors, 0),
                                   GetCurrents(record.ItaSH2, _factors, 0),
                                   GetCurrents(record.ItbSH2, _factors, 0),
                                   GetCurrents(record.ItcSH2, _factors, 0),

                                   GetCurrents(record.IdaPO, _factors, 0),
                                   GetCurrents(record.IdbPO, _factors, 0),
                                   GetCurrents(record.IdcPO, _factors, 0),
                                   GetCurrents(record.ItaPO, _factors, 0),
                                   GetCurrents(record.ItbPO, _factors, 0),
                                   GetCurrents(record.ItcPO, _factors, 0),

                                   GetCurrents(record.I1, _factors, 1),
                                   GetCurrents(record.I2, _factors, 1),
                                   GetCurrents(record.I3, _factors, 1),
                                   GetCurrents(record.I4, _factors, 2),
                                   GetCurrents(record.I5, _factors, 2),
                                   GetCurrents(record.I6, _factors, 2),
                                   GetCurrents(record.I7, _factors, 3),
                                   GetCurrents(record.I8, _factors, 3),
                                   GetCurrents(record.I9, _factors, 3),
                                   GetCurrents(record.I10, _factors, 4),
                                   GetCurrents(record.I11, _factors, 4),
                                   GetCurrents(record.I10, _factors, 4),
                                   GetCurrents(record.I13, _factors, 5),
                                   GetCurrents(record.I14, _factors, 5),
                                   GetCurrents(record.I15, _factors, 5),
                                   GetCurrents(record.I16, _factors, 6),
                                   GetCurrents(record.I17, _factors, 6),
                                   GetCurrents(record.I18, _factors, 6),
                                   GetCurrents(record.I19, _factors, 7),
                                   GetCurrents(record.I20, _factors, 7),
                                   GetCurrents(record.I21, _factors, 7),
                                   GetCurrents(record.I22, _factors, 8),
                                   GetCurrents(record.I23, _factors, 8),
                                   GetCurrents(record.I24, _factors, 8),

                                   record.D1To8,
                                   record.D9To16,
                                   record.D17To24,
                                   record.D25To32
                               );
                            break;
                        default:
                            this._table.Rows.Add
                               (
                                   this._recordNumber,
                                   record.GetTime,
                                   Strings.AlarmJournalMessage[record.Message],
                                   triggeredDef,
                                   parameter,
                                   parametrValue,
                                   Strings.GroupsNames[record.GroupOfSetpoint],

                                   GetCurrents(record.IdaSH1, _factors, 0),
                                   GetCurrents(record.IdbSH1, _factors, 0),
                                   GetCurrents(record.IdcSH1, _factors, 0),
                                   GetCurrents(record.ItaSH1, _factors, 0),
                                   GetCurrents(record.ItbSH1, _factors, 0),
                                   GetCurrents(record.ItcSH1, _factors, 0),

                                   GetCurrents(record.IdaSH2, _factors, 0),
                                   GetCurrents(record.IdbSH2, _factors, 0),
                                   GetCurrents(record.IdcSH2, _factors, 0),
                                   GetCurrents(record.ItaSH2, _factors, 0),
                                   GetCurrents(record.ItbSH2, _factors, 0),
                                   GetCurrents(record.ItcSH2, _factors, 0),

                                   GetCurrents(record.IdaPO, _factors, 0),
                                   GetCurrents(record.IdbPO, _factors, 0),
                                   GetCurrents(record.IdcPO, _factors, 0),
                                   GetCurrents(record.ItaPO, _factors, 0),
                                   GetCurrents(record.ItbPO, _factors, 0),
                                   GetCurrents(record.ItcPO, _factors, 0),

                                   GetCurrents(record.I1, _factors, 1),
                                   GetCurrents(record.I2, _factors, 1),
                                   GetCurrents(record.I3, _factors, 1),
                                   GetCurrents(record.I4, _factors, 2),
                                   GetCurrents(record.I5, _factors, 2),
                                   GetCurrents(record.I6, _factors, 2),
                                   GetCurrents(record.I7, _factors, 3),
                                   GetCurrents(record.I8, _factors, 3),
                                   GetCurrents(record.I9, _factors, 3),
                                   GetCurrents(record.I10, _factors, 4),
                                   GetCurrents(record.I11, _factors, 4),
                                   GetCurrents(record.I10, _factors, 4),
                                   GetCurrents(record.I13, _factors, 5),
                                   GetCurrents(record.I14, _factors, 5),
                                   GetCurrents(record.I15, _factors, 5),
                                   GetCurrents(record.I16, _factors, 6),

                                   record.D1To8,
                                   record.D9To16,
                                   record.D17To24
                               );
                            break;
                    }
                }
                catch (Exception e)
                {
                    this._table.Rows.Add
                    (
                        this._recordNumber - 1, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "", "", "",
                        "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                        "", "", "", "", "", "", "", "", "", "", ""
                    );
                }
                this._recordNumber++;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
            this.statusStrip1.Update();
            this.ButtonsEnabled = true;
        }

        private string GetTriggeredDefence(AlarmJournalRecordStruct record)
        {
            return record.GetTriggedOption;
        }
        
        private string GetParametrValue(AlarmJournalRecordStruct record, int[] factors)
        {
            string value = record.GetValueTriggedOption(factors);
            return value;
        }

        private string GetCurrents(ushort value, int[] factors, int index)
        {
            return ValuesConverterCommon.Analog.GetIBig(value, factors[index]);
        }

        private DataTable GetJournalDataTable()
        {
            DataTable table = new DataTable(TABLE_NAME);
            for (int j = 0; j < this._alarmJournalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._alarmJournalGrid.Columns[j].HeaderText);
            }

            switch (Strings.DeviceType)
            {
                case "A1":
                    this._alarmJournalGrid.Columns["_IaConn6Col"].HeaderText = "Прис.In";
                    this._alarmJournalGrid.Columns["_IbConn6Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_IcConn6Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_IaConn7Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_IbConn7Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_IcConn7Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_IaConn8Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_IbConn8Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_IcConn8Col"].Visible = false;
                    break;
                case "A2":
                    this._alarmJournalGrid.Columns["_d6Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d7Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d8Col"].Visible = false;
                    break;
                case "A3":
                    this._alarmJournalGrid.Columns["_d4Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d5Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d6Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d7Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d8Col"].Visible = false;
                    break;
                case "A4":
                    this._alarmJournalGrid.Columns["_d5Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d6Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d7Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d8Col"].Visible = false;
                    break;
                default:
                    this._alarmJournalGrid.Columns["_IaConn6Col"].HeaderText = "Прис.In";
                    this._alarmJournalGrid.Columns["_IbConn6Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_IcConn6Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_IaConn7Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_IbConn7Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_IcConn7Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_IaConn8Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_IbConn8Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_IcConn8Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d4Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d5Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d6Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d7Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d8Col"].Visible = false;
                    break;
            }

            
            return table;
        }

        #endregion [Help members]

        #region [Event Handlers]
        private void AlarmJournalForm_Load(object sender, EventArgs e)
        {
            this._table = this.GetJournalDataTable();
            this._alarmJournalGrid.DataSource = this._table;

            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                switch (_device.ConnectionPort)
                {
                    case 2:
                        MessageBox.Show(FAIL_READ_SIGN_RS, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        this._messages = PropFormsStrings.GetNewAlarmList();
                        this.StartReadOption();
                        break;
                    default:
                        this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME);
                        break;
                }
                this.ButtonsEnabled = false;
                this._statusLabel.Text = READING_LIST_FILE;
                this.statusStrip1.Update();
            }
        }

        private void _readAlarmJournalButtonClick(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                switch (_device.ConnectionPort)
                {
                    case 2:
                        MessageBox.Show(FAIL_READ_SIGN_RS, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        this._messages = PropFormsStrings.GetNewAlarmList();
                        this.StartReadOption();
                        break;
                    default:
                        this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME);
                        break;
                }
                this.ButtonsEnabled = false;
                this._statusLabel.Text = READING_LIST_FILE;
                this.statusStrip1.Update();
            }
        }

        private void StartReadOption()
        {
            this._statusLabel.Text = READ_AJ;
            this._currentOptionsLoader.StartRead();
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            int j = 0;
            if (this._openAlarmJournalDialog.ShowDialog() != DialogResult.OK) return;
            if (Path.GetExtension(this._openAlarmJournalDialog.FileName).ToLower().Contains("bin"))
            {
                byte[] file = File.ReadAllBytes(this._openAlarmJournalDialog.FileName);
                AlarmJournalRecordStruct journal = new AlarmJournalRecordStruct();
                int size = journal.GetSize();
                List<byte> journalBytes = new List<byte>();
                journalBytes.AddRange(Common.SwapArrayItems(file));
                int countRecord = journalBytes.Count / size;
                for (int i = 0; j < countRecord - 2; i = i + size)
                {
                    journal.InitStruct(journalBytes.GetRange(i, size).ToArray());
                    this.ReadRecordFromBinFile();
                    j++;
                }
            }
            else
            {
                this._table.Clear();
                this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                this._statusLabel.Text = "Журнал загружен. " + string.Format(RECORDS_IN_JOURNAL, this._table.Rows.Count);
            }
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }

            this._saveAlarmJournalDialog.FileName = $"Журнал аварий МР801 {this._device.DevicePlant} v{this._device.DeviceVersion.Replace(".", "_")}";

            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
            }
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }

            this._saveJournalHtmlDialog.FileName = $"Журнал аварий МР801 {this._device.DevicePlant} v{this._device.DeviceVersion.Replace(".", "_")}";

            if (this._saveJournalHtmlDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string xml;

                    using (StringWriter writer = new StringWriter())
                    {
                        this._table.WriteXml(writer);
                        xml = writer.ToString();
                    }

                    HtmlExport.Export(this._saveJournalHtmlDialog.FileName, "МР901. Журнал аварий", xml);
                    this._statusLabel.Text = "Журнал успешно сохранен.";

                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }

            //if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            //{
            //    HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR7AJ);
            //    this._statusLabel.Text = JOURNAL_SAVED;
            //}
        }

        private void AlarmJournalForm_FormClosing(object sender, FormClosedEventArgs e)
        {
            this._alarmJournal?.RemoveStructQueries();
            this._journalLoader?.ClearEvents();
        }
        #endregion [Event Handlers]
        
        #region [IFormView Members]
        public Type FormDevice => typeof(Mr902New);
        public bool Multishow { get; private set; }
        public INodeView[] ChildNodes => new INodeView[] { }; 
        public Type ClassType => typeof(AlarmJournalForm);
        public bool Deletable => false;
        public bool ForceShow => false;
        public Image NodeImage => Framework.Properties.Resources.ja;
        public string NodeName => ALARM_JOURNAL; 
        #endregion [IFormView Members]        
    }
}
