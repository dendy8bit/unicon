﻿namespace BEMN.MR902NEW.AlarmJournal
{
    partial class AlarmJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            this._readAlarmJournalButton = new System.Windows.Forms.Button();
            this._saveAlarmJournalButton = new System.Windows.Forms.Button();
            this._loadAlarmJournalButton = new System.Windows.Forms.Button();
            this._openAlarmJournalDialog = new System.Windows.Forms.OpenFileDialog();
            this._saveAlarmJournalDialog = new System.Windows.Forms.SaveFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._exportButton = new System.Windows.Forms.Button();
            this._saveJournalHtmlDialog = new System.Windows.Forms.SaveFileDialog();
            this._alarmJournalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msg1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._codeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._groupCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ida1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._idb1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._idc1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ita1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._itb1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._itc1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ida2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._idb2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._idc2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.@__ita2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._itb2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._itc2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ida3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._idb3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._idc3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ita3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._itb3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._itc3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaConn1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbConn1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcConn1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaConn2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbConn2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._InConn2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaConn3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbConn3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcConn3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaConn4Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbConn4Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcConn4Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaConn5Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbConn5Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcConn5Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaConn6Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbConn6Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcConn6Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaConn7Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbConn7Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcConn7Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaConn8Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbConn8Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcConn8Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d4Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d5Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d6Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d7Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d8Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // _readAlarmJournalButton
            // 
            this._readAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readAlarmJournalButton.Location = new System.Drawing.Point(12, 539);
            this._readAlarmJournalButton.Name = "_readAlarmJournalButton";
            this._readAlarmJournalButton.Size = new System.Drawing.Size(117, 23);
            this._readAlarmJournalButton.TabIndex = 1;
            this._readAlarmJournalButton.Text = "Прочитать журнал";
            this._readAlarmJournalButton.UseVisualStyleBackColor = true;
            this._readAlarmJournalButton.Click += new System.EventHandler(this._readAlarmJournalButtonClick);
            // 
            // _saveAlarmJournalButton
            // 
            this._saveAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveAlarmJournalButton.Location = new System.Drawing.Point(536, 539);
            this._saveAlarmJournalButton.Name = "_saveAlarmJournalButton";
            this._saveAlarmJournalButton.Size = new System.Drawing.Size(117, 23);
            this._saveAlarmJournalButton.TabIndex = 20;
            this._saveAlarmJournalButton.Text = "Сохранить в файл";
            this._saveAlarmJournalButton.UseVisualStyleBackColor = true;
            this._saveAlarmJournalButton.Click += new System.EventHandler(this._saveAlarmJournalButton_Click);
            // 
            // _loadAlarmJournalButton
            // 
            this._loadAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._loadAlarmJournalButton.Location = new System.Drawing.Point(402, 539);
            this._loadAlarmJournalButton.Name = "_loadAlarmJournalButton";
            this._loadAlarmJournalButton.Size = new System.Drawing.Size(128, 23);
            this._loadAlarmJournalButton.TabIndex = 21;
            this._loadAlarmJournalButton.Text = "Загрузить из файла";
            this._loadAlarmJournalButton.UseVisualStyleBackColor = true;
            this._loadAlarmJournalButton.Click += new System.EventHandler(this._loadAlarmJournalButton_Click);
            // 
            // _openAlarmJournalDialog
            // 
            this._openAlarmJournalDialog.DefaultExt = "xml";
            this._openAlarmJournalDialog.FileName = "Журнал аварий МР901";
            this._openAlarmJournalDialog.Filter = "МР901 Журнал аварий(*.xml)|*.xml|МР801 Журнал аварий(*.bin)|*.bin";
            this._openAlarmJournalDialog.RestoreDirectory = true;
            this._openAlarmJournalDialog.Title = "Открыть журнал  аварий для МР901";
            // 
            // _saveAlarmJournalDialog
            // 
            this._saveAlarmJournalDialog.DefaultExt = "xml";
            this._saveAlarmJournalDialog.FileName = "Журнал аварий МР901";
            this._saveAlarmJournalDialog.Filter = "(Журнал аварий МР901) | *.xml";
            this._saveAlarmJournalDialog.Title = "Сохранить  журнал аварий для МР901";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 571);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(788, 22);
            this.statusStrip1.TabIndex = 22;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(10, 17);
            this._statusLabel.Text = ".";
            // 
            // _exportButton
            // 
            this._exportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._exportButton.Location = new System.Drawing.Point(659, 539);
            this._exportButton.Name = "_exportButton";
            this._exportButton.Size = new System.Drawing.Size(117, 23);
            this._exportButton.TabIndex = 23;
            this._exportButton.Text = "Сохранить в HTML";
            this._exportButton.UseVisualStyleBackColor = true;
            this._exportButton.Click += new System.EventHandler(this._exportButton_Click);
            // 
            // _saveJournalHtmlDialog
            // 
            this._saveJournalHtmlDialog.DefaultExt = "xml";
            this._saveJournalHtmlDialog.FileName = "Журнал аварий МР901";
            this._saveJournalHtmlDialog.Filter = "Журнал аварий МР901 | *.html";
            this._saveJournalHtmlDialog.Title = "Сохранить  журнал аварий для МР901";
            // 
            // _alarmJournalGrid
            // 
            this._alarmJournalGrid.AllowUserToAddRows = false;
            this._alarmJournalGrid.AllowUserToDeleteRows = false;
            this._alarmJournalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._alarmJournalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._alarmJournalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._alarmJournalGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._alarmJournalGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this._alarmJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._alarmJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msg1Col,
            this._msgCol,
            this._codeCol,
            this._typeCol,
            this._groupCol,
            this._Ida1Col,
            this._idb1Col,
            this._idc1Col,
            this._ita1Col,
            this._itb1Col,
            this._itc1Col,
            this._ida2Col,
            this._idb2Col,
            this._idc2Col,
            this.@__ita2Col,
            this._itb2Col,
            this._itc2Col,
            this._ida3Col,
            this._idb3Col,
            this._idc3Col,
            this._ita3Col,
            this._itb3Col,
            this._itc3Col,
            this._IaConn1Col,
            this._IbConn1Col,
            this._IcConn1Col,
            this._IaConn2Col,
            this._IbConn2Col,
            this._InConn2Col,
            this._IaConn3Col,
            this._IbConn3Col,
            this._IcConn3Col,
            this._IaConn4Col,
            this._IbConn4Col,
            this._IcConn4Col,
            this._IaConn5Col,
            this._IbConn5Col,
            this._IcConn5Col,
            this._IaConn6Col,
            this._IbConn6Col,
            this._IcConn6Col,
            this._IaConn7Col,
            this._IbConn7Col,
            this._IcConn7Col,
            this._IaConn8Col,
            this._IbConn8Col,
            this._IcConn8Col,
            this._d1Col,
            this._d2Col,
            this._d3Col,
            this._d4Col,
            this._d5Col,
            this._d6Col,
            this._d7Col,
            this._d8Col});
            this._alarmJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._alarmJournalGrid.Margin = new System.Windows.Forms.Padding(100, 3, 3, 100);
            this._alarmJournalGrid.Name = "_alarmJournalGrid";
            this._alarmJournalGrid.ReadOnly = true;
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle58.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle58.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle58.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle58.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle58.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle58.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._alarmJournalGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle58;
            this._alarmJournalGrid.RowHeadersVisible = false;
            this._alarmJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._alarmJournalGrid.Size = new System.Drawing.Size(788, 528);
            this._alarmJournalGrid.TabIndex = 24;
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "№";
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 24;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "Дата/Время";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._timeCol.DefaultCellStyle = dataGridViewCellStyle2;
            this._timeCol.HeaderText = "Дата/Время";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 77;
            // 
            // _msg1Col
            // 
            this._msg1Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._msg1Col.DataPropertyName = "Сообщение";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msg1Col.DefaultCellStyle = dataGridViewCellStyle3;
            this._msg1Col.HeaderText = "Сообщение";
            this._msg1Col.Name = "_msg1Col";
            this._msg1Col.ReadOnly = true;
            this._msg1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msg1Col.Width = 71;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._msgCol.DataPropertyName = "Сработавшая защита";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msgCol.DefaultCellStyle = dataGridViewCellStyle4;
            this._msgCol.HeaderText = "Сработавшая защита";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msgCol.Width = 110;
            // 
            // _codeCol
            // 
            this._codeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._codeCol.DataPropertyName = "Параметр срабатывания";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._codeCol.DefaultCellStyle = dataGridViewCellStyle5;
            this._codeCol.HeaderText = "Параметр срабатывания";
            this._codeCol.Name = "_codeCol";
            this._codeCol.ReadOnly = true;
            this._codeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._codeCol.Width = 126;
            // 
            // _typeCol
            // 
            this._typeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._typeCol.DataPropertyName = "Значение параметра срабатывания";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._typeCol.DefaultCellStyle = dataGridViewCellStyle6;
            this._typeCol.HeaderText = "Значение параметра срабатывания";
            this._typeCol.Name = "_typeCol";
            this._typeCol.ReadOnly = true;
            this._typeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _groupCol
            // 
            this._groupCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._groupCol.DataPropertyName = "Группа уставок";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._groupCol.DefaultCellStyle = dataGridViewCellStyle7;
            this._groupCol.HeaderText = "Группа уставок";
            this._groupCol.Name = "_groupCol";
            this._groupCol.ReadOnly = true;
            this._groupCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._groupCol.Width = 82;
            // 
            // _Ida1Col
            // 
            this._Ida1Col.DataPropertyName = "Ia диф. СШ1";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Ida1Col.DefaultCellStyle = dataGridViewCellStyle8;
            this._Ida1Col.HeaderText = "Ia диф. СШ1";
            this._Ida1Col.Name = "_Ida1Col";
            this._Ida1Col.ReadOnly = true;
            this._Ida1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ida1Col.Width = 46;
            // 
            // _idb1Col
            // 
            this._idb1Col.DataPropertyName = "Ib диф. СШ1";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._idb1Col.DefaultCellStyle = dataGridViewCellStyle9;
            this._idb1Col.HeaderText = "Ib диф. СШ1";
            this._idb1Col.Name = "_idb1Col";
            this._idb1Col.ReadOnly = true;
            this._idb1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._idb1Col.Width = 46;
            // 
            // _idc1Col
            // 
            this._idc1Col.DataPropertyName = "Ic диф. СШ1";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._idc1Col.DefaultCellStyle = dataGridViewCellStyle10;
            this._idc1Col.HeaderText = "Ic диф. СШ1";
            this._idc1Col.Name = "_idc1Col";
            this._idc1Col.ReadOnly = true;
            this._idc1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._idc1Col.Width = 46;
            // 
            // _ita1Col
            // 
            this._ita1Col.DataPropertyName = "Ia торм. СШ1";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._ita1Col.DefaultCellStyle = dataGridViewCellStyle11;
            this._ita1Col.HeaderText = "Ia торм. СШ1";
            this._ita1Col.Name = "_ita1Col";
            this._ita1Col.ReadOnly = true;
            this._ita1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ita1Col.Width = 50;
            // 
            // _itb1Col
            // 
            this._itb1Col.DataPropertyName = "Ib торм. СШ1";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._itb1Col.DefaultCellStyle = dataGridViewCellStyle12;
            this._itb1Col.HeaderText = "Ib торм. СШ1";
            this._itb1Col.Name = "_itb1Col";
            this._itb1Col.ReadOnly = true;
            this._itb1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._itb1Col.Width = 50;
            // 
            // _itc1Col
            // 
            this._itc1Col.DataPropertyName = "Ic торм. СШ1";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._itc1Col.DefaultCellStyle = dataGridViewCellStyle13;
            this._itc1Col.HeaderText = "Ic торм. СШ1";
            this._itc1Col.Name = "_itc1Col";
            this._itc1Col.ReadOnly = true;
            this._itc1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._itc1Col.Width = 50;
            // 
            // _ida2Col
            // 
            this._ida2Col.DataPropertyName = "Ia диф. СШ2";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._ida2Col.DefaultCellStyle = dataGridViewCellStyle14;
            this._ida2Col.HeaderText = "Ia диф. СШ2";
            this._ida2Col.Name = "_ida2Col";
            this._ida2Col.ReadOnly = true;
            this._ida2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ida2Col.Width = 46;
            // 
            // _idb2Col
            // 
            this._idb2Col.DataPropertyName = "Ib диф. СШ2";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._idb2Col.DefaultCellStyle = dataGridViewCellStyle15;
            this._idb2Col.HeaderText = "Ib диф. СШ2";
            this._idb2Col.Name = "_idb2Col";
            this._idb2Col.ReadOnly = true;
            this._idb2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._idb2Col.Width = 46;
            // 
            // _idc2Col
            // 
            this._idc2Col.DataPropertyName = "Ic диф. СШ2";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._idc2Col.DefaultCellStyle = dataGridViewCellStyle16;
            this._idc2Col.HeaderText = "Ic диф. СШ2";
            this._idc2Col.Name = "_idc2Col";
            this._idc2Col.ReadOnly = true;
            this._idc2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._idc2Col.Width = 46;
            // 
            // __ita2Col
            // 
            this.@__ita2Col.DataPropertyName = "Ia торм. СШ2";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.@__ita2Col.DefaultCellStyle = dataGridViewCellStyle17;
            this.@__ita2Col.HeaderText = "Ia торм. СШ2";
            this.@__ita2Col.Name = "__ita2Col";
            this.@__ita2Col.ReadOnly = true;
            this.@__ita2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.@__ita2Col.Width = 50;
            // 
            // _itb2Col
            // 
            this._itb2Col.DataPropertyName = "Ib торм. СШ2";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._itb2Col.DefaultCellStyle = dataGridViewCellStyle18;
            this._itb2Col.HeaderText = "Ib торм. СШ2";
            this._itb2Col.Name = "_itb2Col";
            this._itb2Col.ReadOnly = true;
            this._itb2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._itb2Col.Width = 50;
            // 
            // _itc2Col
            // 
            this._itc2Col.DataPropertyName = "Ic торм. СШ2";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._itc2Col.DefaultCellStyle = dataGridViewCellStyle19;
            this._itc2Col.HeaderText = "Ic торм. СШ2";
            this._itc2Col.Name = "_itc2Col";
            this._itc2Col.ReadOnly = true;
            this._itc2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._itc2Col.Width = 50;
            // 
            // _ida3Col
            // 
            this._ida3Col.DataPropertyName = "Ia диф. ПО";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._ida3Col.DefaultCellStyle = dataGridViewCellStyle20;
            this._ida3Col.HeaderText = "Ia диф. ПО";
            this._ida3Col.Name = "_ida3Col";
            this._ida3Col.ReadOnly = true;
            this._ida3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ida3Col.Width = 46;
            // 
            // _idb3Col
            // 
            this._idb3Col.DataPropertyName = "Ib диф. ПО";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._idb3Col.DefaultCellStyle = dataGridViewCellStyle21;
            this._idb3Col.HeaderText = "Ib диф. ПО";
            this._idb3Col.Name = "_idb3Col";
            this._idb3Col.ReadOnly = true;
            this._idb3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._idb3Col.Width = 46;
            // 
            // _idc3Col
            // 
            this._idc3Col.DataPropertyName = "Ic диф. ПО";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._idc3Col.DefaultCellStyle = dataGridViewCellStyle22;
            this._idc3Col.HeaderText = "Ic диф. ПО";
            this._idc3Col.Name = "_idc3Col";
            this._idc3Col.ReadOnly = true;
            this._idc3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._idc3Col.Width = 46;
            // 
            // _ita3Col
            // 
            this._ita3Col.DataPropertyName = "Ia торм. ПО";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._ita3Col.DefaultCellStyle = dataGridViewCellStyle23;
            this._ita3Col.HeaderText = "Ia торм. ПО";
            this._ita3Col.Name = "_ita3Col";
            this._ita3Col.ReadOnly = true;
            this._ita3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ita3Col.Width = 50;
            // 
            // _itb3Col
            // 
            this._itb3Col.DataPropertyName = "Ib торм. ПО";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._itb3Col.DefaultCellStyle = dataGridViewCellStyle24;
            this._itb3Col.HeaderText = "Ib торм. ПО";
            this._itb3Col.Name = "_itb3Col";
            this._itb3Col.ReadOnly = true;
            this._itb3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._itb3Col.Width = 50;
            // 
            // _itc3Col
            // 
            this._itc3Col.DataPropertyName = "Ic торм. ПО";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._itc3Col.DefaultCellStyle = dataGridViewCellStyle25;
            this._itc3Col.HeaderText = "Ic торм. ПО";
            this._itc3Col.Name = "_itc3Col";
            this._itc3Col.ReadOnly = true;
            this._itc3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._itc3Col.Width = 50;
            // 
            // _IaConn1Col
            // 
            this._IaConn1Col.DataPropertyName = "Ia Прис.1";
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IaConn1Col.DefaultCellStyle = dataGridViewCellStyle26;
            this._IaConn1Col.HeaderText = "Ia Прис.1";
            this._IaConn1Col.Name = "_IaConn1Col";
            this._IaConn1Col.ReadOnly = true;
            this._IaConn1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaConn1Col.Width = 54;
            // 
            // _IbConn1Col
            // 
            this._IbConn1Col.DataPropertyName = "Ib Прис.1";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IbConn1Col.DefaultCellStyle = dataGridViewCellStyle27;
            this._IbConn1Col.HeaderText = "Ib Прис.1";
            this._IbConn1Col.Name = "_IbConn1Col";
            this._IbConn1Col.ReadOnly = true;
            this._IbConn1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbConn1Col.Width = 54;
            // 
            // _IcConn1Col
            // 
            this._IcConn1Col.DataPropertyName = "Ic Прис.1";
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IcConn1Col.DefaultCellStyle = dataGridViewCellStyle28;
            this._IcConn1Col.HeaderText = "Ic Прис.1";
            this._IcConn1Col.Name = "_IcConn1Col";
            this._IcConn1Col.ReadOnly = true;
            this._IcConn1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcConn1Col.Width = 54;
            // 
            // _IaConn2Col
            // 
            this._IaConn2Col.DataPropertyName = "Ia Прис.2";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IaConn2Col.DefaultCellStyle = dataGridViewCellStyle29;
            this._IaConn2Col.HeaderText = "Ia Прис.2";
            this._IaConn2Col.Name = "_IaConn2Col";
            this._IaConn2Col.ReadOnly = true;
            this._IaConn2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaConn2Col.Width = 54;
            // 
            // _IbConn2Col
            // 
            this._IbConn2Col.DataPropertyName = "Ib Прис.2";
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IbConn2Col.DefaultCellStyle = dataGridViewCellStyle30;
            this._IbConn2Col.HeaderText = "Ib Прис.2";
            this._IbConn2Col.Name = "_IbConn2Col";
            this._IbConn2Col.ReadOnly = true;
            this._IbConn2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbConn2Col.Width = 54;
            // 
            // _InConn2Col
            // 
            this._InConn2Col.DataPropertyName = "Ic Прис.2";
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._InConn2Col.DefaultCellStyle = dataGridViewCellStyle31;
            this._InConn2Col.HeaderText = "Ic Прис.2";
            this._InConn2Col.Name = "_InConn2Col";
            this._InConn2Col.ReadOnly = true;
            this._InConn2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._InConn2Col.Width = 54;
            // 
            // _IaConn3Col
            // 
            this._IaConn3Col.DataPropertyName = "Ia Прис.3";
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IaConn3Col.DefaultCellStyle = dataGridViewCellStyle32;
            this._IaConn3Col.HeaderText = "Ia Прис.3";
            this._IaConn3Col.Name = "_IaConn3Col";
            this._IaConn3Col.ReadOnly = true;
            this._IaConn3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaConn3Col.Width = 54;
            // 
            // _IbConn3Col
            // 
            this._IbConn3Col.DataPropertyName = "Ib Прис.3";
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IbConn3Col.DefaultCellStyle = dataGridViewCellStyle33;
            this._IbConn3Col.HeaderText = "Ib Прис.3";
            this._IbConn3Col.Name = "_IbConn3Col";
            this._IbConn3Col.ReadOnly = true;
            this._IbConn3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbConn3Col.Width = 54;
            // 
            // _IcConn3Col
            // 
            this._IcConn3Col.DataPropertyName = "Ic Прис.3";
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IcConn3Col.DefaultCellStyle = dataGridViewCellStyle34;
            this._IcConn3Col.HeaderText = "Ic Прис.3";
            this._IcConn3Col.Name = "_IcConn3Col";
            this._IcConn3Col.ReadOnly = true;
            this._IcConn3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcConn3Col.Width = 54;
            // 
            // _IaConn4Col
            // 
            this._IaConn4Col.DataPropertyName = "Ia Прис.4";
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IaConn4Col.DefaultCellStyle = dataGridViewCellStyle35;
            this._IaConn4Col.HeaderText = "Ia Прис.4";
            this._IaConn4Col.Name = "_IaConn4Col";
            this._IaConn4Col.ReadOnly = true;
            this._IaConn4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaConn4Col.Width = 54;
            // 
            // _IbConn4Col
            // 
            this._IbConn4Col.DataPropertyName = "Ib Прис.4";
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IbConn4Col.DefaultCellStyle = dataGridViewCellStyle36;
            this._IbConn4Col.HeaderText = "Ib Прис.4";
            this._IbConn4Col.Name = "_IbConn4Col";
            this._IbConn4Col.ReadOnly = true;
            this._IbConn4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbConn4Col.Width = 54;
            // 
            // _IcConn4Col
            // 
            this._IcConn4Col.DataPropertyName = "Ic Прис.4";
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IcConn4Col.DefaultCellStyle = dataGridViewCellStyle37;
            this._IcConn4Col.HeaderText = "Ic Прис.4";
            this._IcConn4Col.Name = "_IcConn4Col";
            this._IcConn4Col.ReadOnly = true;
            this._IcConn4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcConn4Col.Width = 54;
            // 
            // _IaConn5Col
            // 
            this._IaConn5Col.DataPropertyName = "Ia Прис.5";
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IaConn5Col.DefaultCellStyle = dataGridViewCellStyle38;
            this._IaConn5Col.HeaderText = "Ia Прис.5";
            this._IaConn5Col.Name = "_IaConn5Col";
            this._IaConn5Col.ReadOnly = true;
            this._IaConn5Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaConn5Col.Width = 54;
            // 
            // _IbConn5Col
            // 
            this._IbConn5Col.DataPropertyName = "Ib Прис.5";
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IbConn5Col.DefaultCellStyle = dataGridViewCellStyle39;
            this._IbConn5Col.HeaderText = "Ib Прис.5";
            this._IbConn5Col.Name = "_IbConn5Col";
            this._IbConn5Col.ReadOnly = true;
            this._IbConn5Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbConn5Col.Width = 54;
            // 
            // _IcConn5Col
            // 
            this._IcConn5Col.DataPropertyName = "Ic Прис.5";
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IcConn5Col.DefaultCellStyle = dataGridViewCellStyle40;
            this._IcConn5Col.HeaderText = "Ic Прис.5";
            this._IcConn5Col.Name = "_IcConn5Col";
            this._IcConn5Col.ReadOnly = true;
            this._IcConn5Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcConn5Col.Width = 54;
            // 
            // _IaConn6Col
            // 
            this._IaConn6Col.DataPropertyName = "Ia Прис.6";
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IaConn6Col.DefaultCellStyle = dataGridViewCellStyle41;
            this._IaConn6Col.HeaderText = "Ia Прис.6";
            this._IaConn6Col.Name = "_IaConn6Col";
            this._IaConn6Col.ReadOnly = true;
            this._IaConn6Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaConn6Col.Width = 54;
            // 
            // _IbConn6Col
            // 
            this._IbConn6Col.DataPropertyName = "Ib Прис.6";
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IbConn6Col.DefaultCellStyle = dataGridViewCellStyle42;
            this._IbConn6Col.HeaderText = "Ib Прис.6";
            this._IbConn6Col.Name = "_IbConn6Col";
            this._IbConn6Col.ReadOnly = true;
            this._IbConn6Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbConn6Col.Width = 54;
            // 
            // _IcConn6Col
            // 
            this._IcConn6Col.DataPropertyName = "Ic Прис.6";
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IcConn6Col.DefaultCellStyle = dataGridViewCellStyle43;
            this._IcConn6Col.HeaderText = "Ic Прис.6";
            this._IcConn6Col.Name = "_IcConn6Col";
            this._IcConn6Col.ReadOnly = true;
            this._IcConn6Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcConn6Col.Width = 54;
            // 
            // _IaConn7Col
            // 
            this._IaConn7Col.DataPropertyName = "Ia Прис.7";
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IaConn7Col.DefaultCellStyle = dataGridViewCellStyle44;
            this._IaConn7Col.HeaderText = "Ia Прис.7";
            this._IaConn7Col.Name = "_IaConn7Col";
            this._IaConn7Col.ReadOnly = true;
            this._IaConn7Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaConn7Col.Width = 54;
            // 
            // _IbConn7Col
            // 
            this._IbConn7Col.DataPropertyName = "Ib Прис.7";
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IbConn7Col.DefaultCellStyle = dataGridViewCellStyle45;
            this._IbConn7Col.HeaderText = "Ib Прис.7";
            this._IbConn7Col.Name = "_IbConn7Col";
            this._IbConn7Col.ReadOnly = true;
            this._IbConn7Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbConn7Col.Width = 54;
            // 
            // _IcConn7Col
            // 
            this._IcConn7Col.DataPropertyName = "Ic Прис.7";
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IcConn7Col.DefaultCellStyle = dataGridViewCellStyle46;
            this._IcConn7Col.HeaderText = "Ic Прис.7";
            this._IcConn7Col.Name = "_IcConn7Col";
            this._IcConn7Col.ReadOnly = true;
            this._IcConn7Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcConn7Col.Width = 54;
            // 
            // _IaConn8Col
            // 
            this._IaConn8Col.DataPropertyName = "Ia Прис.8";
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IaConn8Col.DefaultCellStyle = dataGridViewCellStyle47;
            this._IaConn8Col.HeaderText = "Ia Прис.8";
            this._IaConn8Col.Name = "_IaConn8Col";
            this._IaConn8Col.ReadOnly = true;
            this._IaConn8Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaConn8Col.Width = 54;
            // 
            // _IbConn8Col
            // 
            this._IbConn8Col.DataPropertyName = "Ib Прис.8";
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IbConn8Col.DefaultCellStyle = dataGridViewCellStyle48;
            this._IbConn8Col.HeaderText = "Ib Прис.8";
            this._IbConn8Col.Name = "_IbConn8Col";
            this._IbConn8Col.ReadOnly = true;
            this._IbConn8Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbConn8Col.Width = 54;
            // 
            // _IcConn8Col
            // 
            this._IcConn8Col.DataPropertyName = "Ic Прис.8";
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IcConn8Col.DefaultCellStyle = dataGridViewCellStyle49;
            this._IcConn8Col.HeaderText = "Ic Прис.8";
            this._IcConn8Col.Name = "_IcConn8Col";
            this._IcConn8Col.ReadOnly = true;
            this._IcConn8Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcConn8Col.Width = 54;
            // 
            // _d1Col
            // 
            this._d1Col.DataPropertyName = "Д1-Д8";
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._d1Col.DefaultCellStyle = dataGridViewCellStyle50;
            this._d1Col.HeaderText = "Д1-Д8";
            this._d1Col.Name = "_d1Col";
            this._d1Col.ReadOnly = true;
            this._d1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d1Col.Width = 46;
            // 
            // _d2Col
            // 
            this._d2Col.DataPropertyName = "Д9-Д16";
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._d2Col.DefaultCellStyle = dataGridViewCellStyle51;
            this._d2Col.HeaderText = "Д9-Д16";
            this._d2Col.Name = "_d2Col";
            this._d2Col.ReadOnly = true;
            this._d2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d2Col.Width = 52;
            // 
            // _d3Col
            // 
            this._d3Col.DataPropertyName = "Д17-Д24";
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._d3Col.DefaultCellStyle = dataGridViewCellStyle52;
            this._d3Col.HeaderText = "Д17-Д24";
            this._d3Col.Name = "_d3Col";
            this._d3Col.ReadOnly = true;
            this._d3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d3Col.Width = 58;
            // 
            // _d4Col
            // 
            this._d4Col.DataPropertyName = "Д25-Д32";
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._d4Col.DefaultCellStyle = dataGridViewCellStyle53;
            this._d4Col.HeaderText = "Д25-Д32";
            this._d4Col.Name = "_d4Col";
            this._d4Col.ReadOnly = true;
            this._d4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d4Col.Width = 58;
            // 
            // _d5Col
            // 
            this._d5Col.DataPropertyName = "Д33-Д40";
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._d5Col.DefaultCellStyle = dataGridViewCellStyle54;
            this._d5Col.HeaderText = "Д33-Д40";
            this._d5Col.Name = "_d5Col";
            this._d5Col.ReadOnly = true;
            this._d5Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d5Col.Width = 58;
            // 
            // _d6Col
            // 
            this._d6Col.DataPropertyName = "Д41-Д48";
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._d6Col.DefaultCellStyle = dataGridViewCellStyle55;
            this._d6Col.HeaderText = "Д41-Д48";
            this._d6Col.Name = "_d6Col";
            this._d6Col.ReadOnly = true;
            this._d6Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d6Col.Width = 58;
            // 
            // _d7Col
            // 
            this._d7Col.DataPropertyName = "Д49-Д56";
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._d7Col.DefaultCellStyle = dataGridViewCellStyle56;
            this._d7Col.HeaderText = "Д49-Д56";
            this._d7Col.Name = "_d7Col";
            this._d7Col.ReadOnly = true;
            this._d7Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d7Col.Width = 58;
            // 
            // _d8Col
            // 
            this._d8Col.DataPropertyName = "Д57-Д64";
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._d8Col.DefaultCellStyle = dataGridViewCellStyle57;
            this._d8Col.HeaderText = "Д57-Д64";
            this._d8Col.Name = "_d8Col";
            this._d8Col.ReadOnly = true;
            this._d8Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d8Col.Width = 58;
            // 
            // AlarmJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 593);
            this.Controls.Add(this._alarmJournalGrid);
            this.Controls.Add(this._exportButton);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._loadAlarmJournalButton);
            this.Controls.Add(this._readAlarmJournalButton);
            this.Controls.Add(this._saveAlarmJournalButton);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "AlarmJournalForm";
            this.Text = "AlarmJournalForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AlarmJournalForm_FormClosing);
            this.Load += new System.EventHandler(this.AlarmJournalForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _readAlarmJournalButton;
        private System.Windows.Forms.Button _saveAlarmJournalButton;
        private System.Windows.Forms.Button _loadAlarmJournalButton;
        private System.Windows.Forms.OpenFileDialog _openAlarmJournalDialog;
        private System.Windows.Forms.SaveFileDialog _saveAlarmJournalDialog;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.Button _exportButton;
        private System.Windows.Forms.SaveFileDialog _saveJournalHtmlDialog;
        private System.Windows.Forms.DataGridView _alarmJournalGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msg1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _codeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _typeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _groupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ida1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _idb1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _idc1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ita1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _itb1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _itc1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ida2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _idb2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _idc2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn __ita2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _itb2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _itc2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ida3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _idb3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _idc3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ita3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _itb3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _itc3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaConn1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbConn1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcConn1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaConn2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbConn2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _InConn2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaConn3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbConn3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcConn3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaConn4Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbConn4Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcConn4Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaConn5Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbConn5Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcConn5Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaConn6Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbConn6Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcConn6Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaConn7Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbConn7Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcConn7Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaConn8Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbConn8Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcConn8Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d4Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d5Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d6Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d7Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d8Col;
    }
}