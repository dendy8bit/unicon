﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR902NEW.Properties;
using BEMN.MR902NEW.SystemJournal.Structures;
using SchemeEditorSystem.ResourceLibs;

namespace BEMN.MR902NEW.SystemJournal
{
    public partial class SystemJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Записей в журнале - {0}";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string READ_FAIL = "Невозможно прочитать журнал";
        private const string TABLE_NAME_SYS = "МР902_журнал_системы";
        private const string NUMBER_SYS = "Номер";
        private const string TIME_SYS = "Время";
        private const string MESSAGE_SYS = "Сообщение";
        private const string SYSTEM_JOURNAL = "Журнал системы";
        private const string READ_SYSTEM_JOURNAL = "Идет чтение журнала системы";
        private const string READING_LIST_FILE = "Идет чтение файла списка подписей сигналов ЖС СПЛ";
        private const string FAIL_READ =
            "Невозможно прочитать список подписей сигналов ЖC СПЛ. Списки будут сформированы по умолчанию.";
        private const string FAIL_READ_SIGN_RS =
            "Чтение подписей сигналов ЖС СПЛ по RS-485 невозмоэжно. Списки будут сформированы по умолчанию.";
        private const string LIST_FILE_NAME = "jlist.xml";
        private const string DEVICE_NAME = "MR901";
        #endregion [Constants]
        
        #region [Private fields]
        private readonly MemoryEntity<OneWordStruct> _saveIndex;
        private readonly MemoryEntity<SystemJournalStruct> _systemJournal;
        private DataTable _dataTable;
        private int _recordNumber;
        private Mr902New _device;
        private FileDriver _fileDriver;
        private JournalLoader _journalLoader;
        #endregion [Private fields]

        public SystemJournalForm()
        {
            this.InitializeComponent();
        }

        public SystemJournalForm(Mr902New device)
        {
            this.InitializeComponent();
            this._device = device;

            if (this._device.Full1SysJournal024 != null)
            {
                this._journalLoader = new JournalLoader(this._device.Full1SysJournal024, this._device.FullSysJournal64, this._device.SaveSysJournalIndex);
                this._journalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.AllReadRecord);
                this._journalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadJournal);
                //subscribe events
                this._systemJournal = null;
            }
            else
            {
                this._journalLoader = null;
                this._systemJournal = device.SystemJournal;
                this._saveIndex = device.SaveSysJournalIndex;
                this._saveIndex.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.StartReadJournal);
                this._saveIndex.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
                this._systemJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);
            }
            this._fileDriver = new FileDriver(this._device, this);
        }

        #region [Properties]
        private bool ButtonsEnabled
        {
            set
            {
                this._readJournalButton.Enabled = this._saveJournalButton.Enabled =
                    this._loadJournalButton.Enabled = this._exportButton.Enabled = value;
            }
        }
        /// <summary>
        /// Счётчик сообщений
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
            set
            {
                this._recordNumber = value;
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                this.statusStrip1.Update();
            }
        }
        #endregion [Properties]
        
        #region [Help members]
        private void FailReadJournal()
        {
            this.RecordNumber = 0;
            this._statusLabel.Text = READ_FAIL;
            this.ButtonsEnabled = true;
        }

        private void StartReadJournal()
        {
            this.RecordNumber = 0;
            this._systemJournal.LoadStructCycleWhile(a => a.IsEmpty);
        }

        private void ReadRecord()
        {
            if (!this._systemJournal.Value.IsEmpty)
            {
                this.RecordNumber++;
                this._dataTable.Rows.Add(
                    this.RecordNumber.ToString(CultureInfo.InvariantCulture),
                    this._systemJournal.Value.GetRecordTime,
                    this._systemJournal.Value.GetRecordMessage
                    );
                this._systemJournalGrid.Refresh();
            }
            else
            {
                this.ButtonsEnabled = true;
            }
        }

        private void AllReadRecord()
        {
            if (this._journalLoader.JournalRecords.Count != 0)
            {
                for (int i = 0; i < this._journalLoader.JournalRecords.Count; i++)
                {
                    this._dataTable.Rows.Add(
                        i + 1,
                        this._journalLoader.JournalRecords[i].GetRecordTime,
                        this._journalLoader.JournalRecords[i].GetRecordMessage
                        );
                }
                this._systemJournalGrid.Refresh();
                this.RecordNumber = this._journalLoader.JournalRecords.Count;
            }
            else
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
            }
            this.ButtonsEnabled = true;
        }

        private DataTable GetJournalDataTable()
        {
            var table = new DataTable(TABLE_NAME_SYS);
            table.Columns.Add(NUMBER_SYS);
            table.Columns.Add(TIME_SYS);
            table.Columns.Add(MESSAGE_SYS);
            return table;
        }

        private void SaveJournalToFile()
        {
            if (DialogResult.OK == this._saveJournalDialog.ShowDialog())
            {
                this._dataTable.WriteXml(this._saveJournalDialog.FileName);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }

        private void LoadJournalFromFile()
        {
            if (this._openJournalDialog.ShowDialog() != DialogResult.OK) return;
            this._dataTable.Clear();
            if (Path.GetExtension(this._openJournalDialog.FileName).ToLower().Contains("bin"))
            {
                byte[] file = File.ReadAllBytes(this._openJournalDialog.FileName);
                SystemJournalStruct journal= new SystemJournalStruct();
                int size = journal.GetSize();
                List<byte> journalBytes = new List<byte>();
                journalBytes.AddRange(Common.SwapArrayItems(file));
                int j = 0;
                this._recordNumber = 0;
                int countRecord = journalBytes.Count / size;
                for (int i = 0; j < countRecord; i = i + size)
                {
                    journal.InitStruct(journalBytes.GetRange(i, size).ToArray());
                    journal.MessagesList = PropFormsStrings.GetNewJournalList();

                    this._systemJournal.Value = journal;
                    this.ReadRecord();
                    j++;
                }
            }
            else
            {
                this._dataTable.Clear();
                this._dataTable.ReadXml(this._openJournalDialog.FileName);
            }
            this.RecordNumber = this._systemJournalGrid.Rows.Count;
        }

        private void OnListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "Операция успешно выполнена")
                {

                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            XmlRootAttribute root = new XmlRootAttribute(DEVICE_NAME);
                            XmlSerializer serializer = new XmlSerializer(typeof (ListsOfJournals), root);
                            ListsOfJournals lists = (ListsOfJournals) serializer.Deserialize(reader);
                           
                            this._journalLoader.MessagesList = lists.SysJournal.MessagesList;
                        }
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                MessageBox.Show(FAIL_READ, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                
                this._journalLoader.MessagesList = PropFormsStrings.GetNewJournalList();
                
            }

            this._statusLabel.Text = READ_SYSTEM_JOURNAL;
            this._journalLoader.StartRead();
        }

        private void StartReadMessagesList()
        {
            this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME);
            this._dataTable.Clear();
            this._statusLabel.Text = READING_LIST_FILE;
            this.ButtonsEnabled = false;
        }

        private void StartRead()
        {
            this._dataTable.Clear();
            this._statusLabel.Text = READ_SYSTEM_JOURNAL;
            this._saveIndex.Value.Word = 0;
            this._saveIndex.SaveStruct();
        }

        #endregion [Help members]

        #region [Events Handlers]
        private void SystemJournalForm_Load(object sender, EventArgs e)
        {
            this._dataTable = this.GetJournalDataTable();
            this._systemJournalGrid.DataSource = this._dataTable;
            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                switch (_device.ConnectionPort)
                {
                    case 2:
                        MessageBox.Show(FAIL_READ_SIGN_RS, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        this._systemJournal.Value.MessagesList = PropFormsStrings.GetNewJournalList();
                        StartRead();
                        break;
                    default:
                        this.StartReadMessagesList();
                        break;
                }
            }
        }

        private void _readJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            switch (_device.ConnectionPort)
            {
                case 2:
                    MessageBox.Show(FAIL_READ_SIGN_RS, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this._systemJournal.Value.MessagesList = PropFormsStrings.GetNewJournalList();
                    StartRead();
                    break;
                default:
                    this.StartReadMessagesList();
                    break;
            }
        }

        private void _saveJournalButton_Click(object sender, EventArgs e)
        {
            this.SaveJournalToFile();
        }

        private void _loadJournalButton_Click(object sender, EventArgs e)
        {
            this.LoadJournalFromFile();
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._dataTable, this._saveJournalHtmlDialog.FileName, Resources.MR902SJ);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }
        #endregion [Events Handlers]

        #region [IFormView Members]
        public Type FormDevice => typeof(Mr902New);
        public bool Multishow { get; private set; }
        public INodeView[] ChildNodes => new INodeView[] { };
        public Type ClassType => typeof(SystemJournalForm);
        public bool Deletable => false;
        public bool ForceShow => false;
        public Image NodeImage => Resources.js;
        public string NodeName => SYSTEM_JOURNAL;

        #endregion [IFormView Members]
    }
}
