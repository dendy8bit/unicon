﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms;
using BEMN.Framework;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MBServer.Queries;
using BEMN.MR902.Osc;
using BEMN.MR902NEW.AlarmJournal;
using BEMN.MR902NEW.AlarmJournal.Structures;
using BEMN.MR902NEW.BSBGL;
using BEMN.MR902NEW.Configuration;
using BEMN.MR902NEW.Configuration.Structures.Connections;
using BEMN.MR902NEW.Configuration.Structures.Defenses;
using BEMN.MR902NEW.Configuration.Structures.Osc;
using BEMN.MR902NEW.Configuration.Structures.Oscope;
using BEMN.MR902NEW.Emulation;
using BEMN.MR902NEW.Emulation.Structures;
using BEMN.MR902NEW.FileSharingService;
using BEMN.MR902NEW.Measuring;
using BEMN.MR902NEW.Osc.Structures;
using BEMN.MR902NEW.Properties;
using BEMN.MR902NEW.SystemJournal;
using BEMN.MR902NEW.SystemJournal.Structures;

namespace BEMN.MR902NEW
{
    public class Mr902New : Device, IDeviceVersion, IDeviceView
    {
        private const string ERROR_SAVE_CONFIG = "Ошибка сохранения конфигурации";
        private const string WRITE_OK = "Конфигурация записана в устройство";

        private const int OSC_JOURNAL_START_ADRESS = 0x800;
        public const int BAUDE_RATE_MAX = 921600;
        public const ushort MEASURE_START_ADDRESS = 0x14FE;
        public const ushort GROUP_SETPOINT_SIZE = 0x53A;
        private ushort _groupSetPointSize;

        private MemoryEntity<OneWordStruct> _refreshOscJournal;
        private MemoryEntity<OscJournalStruct> _oscJournal;
        private MemoryEntity<OscSettingsStruct> _oscOptions;
        private MemoryEntity<SetOscStartPageStruct> _setOscStartPage;
        private MemoryEntity<OscPage> _oscPage;
        private MemoryEntity<OscopeStruct> _oscilloscopeSettings;
        private MemoryEntity<AllConnectionStruct> _connectionsMeasuring;
        private MemoryEntity<OneWordStruct> _groupUstavki;
        private MemoryEntity<FullSystemJournalStruct1024> _full1024;
        private MemoryEntity<FullSystemJournalStruct64> _full64;
        private MemoryEntity<SystemJournalStruct> _systemJournal;
        private MemoryEntity<OneWordStruct> _saveSysJournalIndex;

        private MemoryEntity<DateTimeStruct> _dateTime;
        private MemoryEntity<WriteStructEmul> _writeSructEmulation;
        private MemoryEntity<WriteStructEmul> _writeSructEmulationNull;
        private MemoryEntity<ReadStructEmul> _readSructEmulation;

        #region Programming

        private MemoryEntity<ProgramPageStruct> _programPageStruct;
        private MemoryEntity<SourceProgramStruct> _sourceProgramStruct;
        private MemoryEntity<StartStruct> _programStartStruct;
        private MemoryEntity<LogicProgramSignals> _programSignalsStruct;
        private MemoryEntity<SomeStruct> _stateSpl;
        
        private int _connectionPort;

        public int ConnectionPort
        {
            get { return _connectionPort; }
            set { _connectionPort = value; }
        }
        public MemoryEntity<OneWordStruct> ProgramPageStruct { get; private set; }

        public MemoryEntity<ProgramPageStruct> ProgramPage
        {
            get { return this._programPageStruct; }
        }

        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return this._programStartStruct; }
        }
        public MemoryEntity<LogicProgramSignals> ProgramSignalsStruct
        {
            get { return this._programSignalsStruct; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return this._sourceProgramStruct; }
        }

        public MemoryEntity<SomeStruct> StateSpl
        {
            get { return this._stateSpl; }
        }
        #endregion

        /// <summary>
        /// Загрузчик уставок токов
        /// </summary>
        private CurrentOptionsLoader _currentOptionsLoader;

        public Mr902New()
        {
            HaveVersion = true;
        }

        public Mr902New(Modbus mb)
        {
            MB = mb;
            Init();
            HaveVersion = true;
        }

        private void Init()
        {
            int slotLen = this.MB.BaudeRate == BAUDE_RATE_MAX ? 1024 : 64;

            this.Configuration = new MemoryEntity<ConfigurationStruct>("Конфигурация", this, 0x1000, slotLen);

            this._dateTime = new MemoryEntity<DateTimeStruct>("Время и дата в устройстве", this, 0x0200);
            this._writeSructEmulation = new MemoryEntity<WriteStructEmul>("Запись аналоговых сигналов ", this, 0x5800, slotLen);
            this._writeSructEmulationNull = new MemoryEntity<WriteStructEmul>("Запись нулевых аналоговых сигналов ", this, 0x5800, slotLen);
            this._readSructEmulation = new MemoryEntity<ReadStructEmul>("Чтение времени и статуса эмуляции", this, 0x586E, slotLen);

            this.CurrentOptionsLoaderAj = new CurrentOptionsLoader(this);
            this.AlarmJournal = new MemoryEntity<AlarmJournalRecordStruct>("Запись журнала аварий", this, 0x0700);
            this.SetPageJa = new MemoryEntity<OneWordStruct>("Страница ЖА", this, 0x0700);

            if (this.MB.BaudeRate == BAUDE_RATE_MAX)
            {
                this.AllAlarmJournal = new MemoryEntity<AllAlarmJournalStruct>("Журнал аварий", this, 0x0700, slotLen);
                this.AllOscJournal = new MemoryEntity<AllOscJournalStruct>("Журнал осциллографа (весь)", this, 0x0800, slotLen);
                this._full1024 = new MemoryEntity<FullSystemJournalStruct1024>("Запись журнала системы 1024", this, 0x0600, slotLen);
                this._full64 = null;
            }
            else
            {
                this._full1024 = null;
                this._full64 = new MemoryEntity<FullSystemJournalStruct64>("Запись журнала системы 64", this, 0x0600);
            }

            this._saveSysJournalIndex = new MemoryEntity<OneWordStruct>("Обновление журнала системы", this, 0x0600);
            this._systemJournal = new MemoryEntity<SystemJournalStruct>("Запись журнала системы", this, 0x0600);

            this._refreshOscJournal = new MemoryEntity<OneWordStruct>("Обновление журнала осциллографа", this, OSC_JOURNAL_START_ADRESS);
            this._oscJournal = new MemoryEntity<OscJournalStruct>("Журнал осциллографа", this, OSC_JOURNAL_START_ADRESS);
            this._oscOptions = new MemoryEntity<OscSettingsStruct>("Параметры осциллографа", this, 0x05A0);
            this._setOscStartPage = new MemoryEntity<SetOscStartPageStruct>("Установка стартовой страницы осциллограммы", this, 0x900);
            this._oscPage = new MemoryEntity<OscPage>("Страница осциллографа", this, 0x900);
            this._oscilloscopeSettings = new MemoryEntity<OscopeStruct>("Уставки осциллографа", this, 0x108c);
            this.AllChannels = new MemoryEntity<OscopeAllChannelsStruct>("Все каналы осциллографа", this, 0x110A, slotLen);

            this._groupUstavki = new MemoryEntity<OneWordStruct>("Группа уставок", this, 0x0400);

            this._sourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0x4300, slotLen);
            this._programStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0E00);
            this._programSignalsStruct = new MemoryEntity<LogicProgramSignals>("LoadProgramSignals_", this, 0x4100, slotLen);
            this.ProgramPageStruct = new MemoryEntity<OneWordStruct>("SaveProgrammPage", this, 0x4000, slotLen);

            this.StopSpl = new MemoryEntity<OneWordStruct>("Останов логической программы", this, 0x0D08);
            this.StartSpl = new MemoryEntity<OneWordStruct>("Старт логической программы", this, 0x0D09);

            this._stateSpl = new MemoryEntity<SomeStruct>("Состояние ошибок логики", this, 0x0D17);
            ushort[] values = new ushort[4];
            this._stateSpl.Slots = HelperFunctions.SetSlots(values, 0x0D17);
            this._stateSpl.Values = values;
        }
        public MemoryEntity<OneWordStruct> StopSpl { get; private set; }
        public MemoryEntity<OneWordStruct> StartSpl { get; private set; }
        public MemoryEntity<ConfigurationStruct> Configuration { get; private set; }
        public MemoryEntity<AllOscJournalStruct> AllOscJournal { get; private set; }
        public MemoryEntity<WriteStructEmul> WriteStructEmulation => this._writeSructEmulation;
        public MemoryEntity<WriteStructEmul> WriteStructEmulationNull => this._writeSructEmulationNull;
        public MemoryEntity<ReadStructEmul> ReadStructEmulation => this._readSructEmulation;
        public MemoryEntity<DateTimeStruct> DateTime => this._dateTime;
        public MemoryEntity<OneWordStruct> GroupUstavki => this._groupUstavki;
        public MemoryEntity<FullSystemJournalStruct1024> Full1SysJournal024 => this._full1024;
        public MemoryEntity<FullSystemJournalStruct64> FullSysJournal64 => this._full64;
        public MemoryEntity<SystemJournalStruct> SystemJournal => this._systemJournal;
        public MemoryEntity<OneWordStruct> SaveSysJournalIndex => this._saveSysJournalIndex;
        public CurrentOptionsLoader CurrentOptionsLoaderAj { get; set; }
        public MemoryEntity<AllAlarmJournalStruct> AllAlarmJournal { get; private set; }
        public MemoryEntity<AlarmJournalRecordStruct> AlarmJournal { get; private set; }
        public MemoryEntity<OneWordStruct> SetPageJa { get; private set; }

        public ushort GetStartAddrMeasTrans(int group)
        {
            if (string.IsNullOrEmpty(DeviceVersion)) return MEASURE_START_ADDRESS;
            return (ushort)(MEASURE_START_ADDRESS + this._groupSetPointSize * group);
        }

        public sealed override Modbus MB
        {
            get { return mb; }
            set
            {
                if (value == null) return;
                if (mb != null)
                {
                    mb.CompleteExchange -= this.CompleteExchange;
                }
                mb = value;
                mb.CompleteExchange += this.CompleteExchange;
            }
        }

        private void CompleteExchange(object sender, Query query)
        {
            if (query.name == "Сохранить конфигурацию")
            {
                MessageBox.Show(query.error ? ERROR_SAVE_CONFIG : WRITE_OK);
            }

            if (query.name == "LoadPort" + DeviceNumber)
            {
                Raise(query, null, null, ref _port);

                GetConnectionPort(_port.Value);
            }

            mb_CompleteExchange(sender, query);
        }

        private void GetConnectionPort(ushort[] buffer)
        {
            byte[] byteBuf = Common.TOBYTES(buffer, true);
            System.Text.Decoder dec = System.Text.Encoding.ASCII.GetDecoder();
            char[] charBuf = new char[byteBuf.Length];
            try
            {
                int byteUsed, charUsed;
                bool complete;
                dec.Convert(byteBuf, 0, byteBuf.Length, charBuf, 0, byteBuf.Length, true, out byteUsed, out charUsed,
                    out complete);
            }
            catch (ArgumentNullException)
            {
                throw new ApplicationException("Передан нулевой буфер");
            }

            string[] param;
            try
            {
                param = new string(charBuf, 0, 5).Split(new[] {' ', '\0'}, StringSplitOptions.RemoveEmptyEntries);
                int portNumber = Convert.ToInt32(new String(param[0].Where(Char.IsDigit).ToArray()));
                ConnectionPort = portNumber;
            }
            catch (ArgumentOutOfRangeException)
            {
            }
        }

        public int CurrentsCount
        {
            get
            {
                switch (this.Info.Plant)
                {
                    case "A1":
                        return 16;
                    case "A2":
                    case "A3":
                    case "A4":
                        return 24;
                    default:
                        return 16;
                }
            }
        }

        public int DiskretsCount
        {
            get
            {
                switch (this.Info.Plant)
                {
                    case "A1":
                        return 64;
                    case "A2":
                        return 40;
                    case "A3":
                        return 24;
                    case "A4":
                        return 32;
                    default:
                        return 24;
                }
            }
        }
        

        public MemoryEntity<AllConnectionStruct> ConnectionsMeasuring => _connectionsMeasuring;

        public MemoryEntity<OneWordStruct> RefreshOscJournal => _refreshOscJournal;

        public MemoryEntity<OscJournalStruct> OscJournal => _oscJournal;

        public MemoryEntity<OscSettingsStruct> OscOptions => _oscOptions;

        public MemoryEntity<SetOscStartPageStruct> SetOscStartPage => _setOscStartPage;

        public MemoryEntity<OscPage> OscPage => _oscPage;

        public MemoryEntity<OscopeStruct> OscilloscopeSettings => _oscilloscopeSettings;
        public MemoryEntity<OscopeAllChannelsStruct> AllChannels { get; private set; }

        private static string[] _deviceApparatConfig =
        {
            "T16N0D64R43(A1)",
            "T24N0D40R35(A2)",
            "T24N0D24R51(A3)",
            "T24N0D32R43(A4)",
            "T16N0D24R19(М3)"
        };

        private static string[] _deviceOutString = { "A1", "A2", "A3", "A4", string.Empty };

        private slot _port = new slot(0x05D0, 0x05E0);

        private void LoadPort()
        {
            LoadSlot(DeviceNumber, _port, "LoadPort" + DeviceNumber, this);
        }

        public Type[] Forms
        {
            get
            {
                Strings.CurrentVersion = Common.VersionConverter(DeviceVersion);

                _groupSetPointSize = new GroupSetpoint().GetStructInfo(MB.BaudeRate == BAUDE_RATE_MAX ? 1024 : 64)
                    .FullSize;

                if ((this.DeviceDlgInfo.IsConnectionMode || this.IsConnect)) LoadPort();

                //if ((!this.DeviceDlgInfo.IsConnectionMode || !this.IsConnect) && !Framework.Framework.IsProjectOpening)
                //{
                //    ChoiceDeviceType choice = new ChoiceDeviceType(_deviceApparatConfig, _deviceOutString);
                //    choice.ShowDialog();
                //    DevicePlant = choice.DeviceType;
                //}

                Strings.DeviceType = DevicePlant;

                return new[]
                {
                    typeof(OscilloscopeForm),
                    typeof(EmulationForm),
                    typeof(ConfigurationForm),
                    typeof(MeasuringForm),
                    typeof(SystemJournalForm),
                    typeof(AlarmJournalForm),
                    typeof(BSBGLEF),
                    typeof(FileSharingForm)

                };
                
            }
        }

        public List<string> Versions => new List<string>
        {
            "3.00"
        };

        public Type ClassType => typeof(Mr902New);
        
        public bool ForceShow => false;

        public string NodeName => "МР902 (в.3.xx)";

        public INodeView[] ChildNodes => new INodeView[] { };

        public bool Deletable => true;

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage => Framework.Properties.Resources.mrBig;
    }
    
}
