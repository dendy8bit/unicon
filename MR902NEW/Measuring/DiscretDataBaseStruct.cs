﻿using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR902New.Measuring
{
    public class DiscretDataBaseStruct : StructBase
    {
        private const int BASE = 32;
        private const int ALARM = 16;
        private const int PARAMS = 16;

        [Layout(0, Count = BASE)] private ushort[] _base;
        [Layout(1, Count = ALARM)] private ushort[] _alarm;
        [Layout(2, Count = PARAMS)] private ushort[] _params;

        public bool[] Discrets => Common.GetBitsArray(this._base, 0, 71);

        public bool[] LogicSignals => Common.GetBitsArray(this._base, 112, 143);

        public bool[] ExtLogicSignals => Common.GetBitsArray(this._base, 144, 159);

        public bool[] DiffCurrents => Common.GetBitsArray(this._base, 160, 174);

        public bool[] Currents => Common.GetBitsArray(this._base, 175, 238);

        public bool[] ExternalDefenses => Common.GetBitsArray(this._base, 239, 262);

        public bool[] Ssl => Common.GetBitsArray(this._base, 263, 310);

        public bool[] Urov => Common.GetBitsArray(this._base, 311, 337);

        public bool[] State => Common.GetBitsArray(this._base, 365, 369);

        public bool[] GroupState => Common.GetBitsArray(this._base, 506, 507);

        public bool[] Relays => Common.GetBitsArray(this._base, 375, 470);

        /// <summary>
        /// RS-Тригеры
        /// </summary>
        public bool[] RSTriggers => Common.GetBitsArray(this._base, 96, 111);
        
        /// <summary>
        /// Индикаторы
        /// </summary>
        public List<bool[]> Indicators
        {
            get
            {
                bool[] allIndArray = Common.GetBitsArray(this._base, 471, 494); // все подряд биты
                List<bool[]> retList = new List<bool[]>();
                for (int i = 0; i < allIndArray.Length; i = i + 2)    // выделяем попарно состояния одного диода
                {
                    retList.Add(new[] { allIndArray[i], allIndArray[i + 1] });// (bool зеленый, bool красный)
                }
                return retList;
            }
        }

        public bool[] Control => Common.GetBitsArray(this._base, 497, 506);

        /// <summary>
        /// Неисправности
        /// </summary>
        public bool[] Faults => new[]
        {
            Common.GetBit(this._alarm[0], 0), //аппаратная
            Common.GetBit(this._alarm[0], 1), //программная
            Common.GetBit(this._alarm[0], 2), //измерения ТТ
            Common.GetBit(this._alarm[0], 3), //уров
            Common.GetBit(this._alarm[0], 4), //задачи логики
            Common.GetBit(this._alarm[0], 5), //модуль 1
            Common.GetBit(this._alarm[0], 6), //модуль 2
            Common.GetBit(this._alarm[0], 7), //модуль 3
            Common.GetBit(this._alarm[0], 8), //модуль 4

            Common.GetBit(this._alarm[0], 9), //модуль 5
            Common.GetBit(this._alarm[0], 10), //модуль 6
            Common.GetBit(this._alarm[0], 11), //уставок
            Common.GetBit(this._alarm[0], 12), //группы уставок
            Common.GetBit(this._alarm[0], 13), //пароля
            Common.GetBit(this._alarm[0], 14), //журнала системы
            Common.GetBit(this._alarm[0], 15), //журнала аварий
            Common.GetBit(this._alarm[1], 0), //осциллографа

            this.FaultLogic,
            Common.GetBit(this._alarm[1], 7), //неиспр. СШ1
            Common.GetBit(this._alarm[1], 8), //неиспр. СШ2
            Common.GetBit(this._alarm[1], 9)  //неиспр. ПО
        };

        /// <summary>
        /// Неисправность логики
        /// </summary>
        public bool FaultLogic => Common.GetBit(this._alarm[1], 1) | //ошибка CRC констант программы логики
                                  Common.GetBit(this._alarm[1], 2) | //ошибка CRC разрешения программы логики
                                  Common.GetBit(this._alarm[1], 3) | //ошибка CRC программы логики
                                  Common.GetBit(this._alarm[1], 4) | //ошибка CRC меню логики
                                  Common.GetBit(this._alarm[1], 5);

        public bool[] NeisprTN => Common.GetBitsArray(this._alarm, 30, 35);
    }
}
