﻿
namespace BEMN.MR902NEW.Emulation.Structures
{
    public class AnalogDataSet
    {
        private int _countI;

        private InputAnalogDataI[] _dataI;

        public AnalogDataSet(int countI)
        {
            _countI = countI;

            _dataI = new InputAnalogDataI[_countI];
        }

        public InputAnalogDataI[] DataI
        {
            get { return _dataI; }
        }

        public void SetData(InputAnalogData[] allData)
        {
            for (int i = 0; i < _countI; i++)
            {
                _dataI[i] = new InputAnalogDataI();
                _dataI[i].SetData(allData[i]);
            }
        }

        public InputAnalogData[] GetData()
        {
            InputAnalogData[] ret = new InputAnalogData[WriteStructEmul.COUNT];

            for (int i = 0; i < _countI; i++)
            {
                ret[i] = _dataI[i].GetData();
            }

            if (_countI < WriteStructEmul.COUNT)
            {
                for (int i = _countI; i < WriteStructEmul.COUNT; i++)
                {
                    ret[i] = new InputAnalogData();
                }
            }

            return ret;
        }
    }
}
