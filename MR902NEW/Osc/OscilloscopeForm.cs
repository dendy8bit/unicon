using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.ValidatingClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR902NEW;
using BEMN.MR902NEW.Configuration.Structures.Connections;
using BEMN.MR902NEW.Configuration.Structures.Osc;
using BEMN.MR902NEW.Configuration.Structures.Oscope;
using BEMN.MR902NEW.Osc;
using BEMN.MR902NEW.Osc.Structures;
using BEMN.MR902NEW.Properties;

namespace BEMN.MR902.Osc
{
    public partial class OscilloscopeForm : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "�������������";
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string RECORDS_IN_JOURNAL = "������������ � ������� - {0}";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string READ_OSC_STOPPED = "������ ������������� ����������";
        private const string JOURNAL_IS_EMPTY = "������ ������������ ����";
        private const string READ_OSC_JOURNAL = "���� ������ �������������...";

        #endregion [Constants]

        #region [Private fields]
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscPageLoader _pageLoader;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscJournalLoader _oscJournalLoader;
        /// <summary>
        /// ��������� ������� �����
        /// </summary>
        private readonly MemoryEntity<AllConnectionStruct> _connections;

        private readonly MemoryEntity<OscSettingsStruct> _oscilloscopeSettings;
        private readonly MemoryEntity<OscopeConfigStruct> _oscopeConfig;

        private MemoryEntity<OscopeAllChannelsStruct> _oscopeAllChannel;

        /// <summary>
        /// ������ ���
        /// </summary>
        private CountingList _countingList;

        private Mr902New _device;
        private OscJournalStruct _journalStruct;
        private readonly DataTable _table;
        #endregion [Private fields]


        #region [Ctor's]
        public OscilloscopeForm()
        {
            this.InitializeComponent();
        }

        public OscilloscopeForm(Mr902New device)
        {
            this.InitializeComponent();
            this._device = device;

            // ��������� �������
            this._pageLoader = new OscPageLoader(device.SetOscStartPage, device.OscPage);
            this._pageLoader.PageRead += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadStopped += HandlerHelper.CreateActionHandler(this, this.ReadStop);

            // ��������� �������
            if (device.AllOscJournal != null)
            {
                this._oscJournalLoader = new OscJournalLoader(device.AllOscJournal, device.RefreshOscJournal);
                this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.ReadAllRecords);
            }
            else
            {
                this._oscJournalLoader = new OscJournalLoader(device.OscJournal, device.RefreshOscJournal);
                this._oscJournalLoader.ReadJournalOk += HandlerHelper.CreateActionHandler(this, this.ReadRecord);
                this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.OnAllJournalReadOk);
            }

            // ������� �����������
            this._oscilloscopeSettings = device.OscOptions;
            this._oscilloscopeSettings.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._oscJournalLoader.StartReadJournal();
            });
            this._oscilloscopeSettings.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);

            // ������ ������������
            this._oscopeAllChannel = this._device.AllChannels;
            this._oscopeAllChannel.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._oscilloscopeSettings.LoadStruct();
            });
            this._oscopeAllChannel.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);

            //��������� ������� �����
            this._connections = new MemoryEntity<AllConnectionStruct>("������� ������������� (��)", device, 0x106C);
            this._connections.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._oscopeAllChannel.LoadStruct();
            });
            this._connections.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);

            //������� ����. � ����. �����
            _oscopeConfig = new MemoryEntity<OscopeConfigStruct>("������� ����. � ����. �����", device, 0x1106);
            _oscopeConfig.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _connections.LoadStruct();
            });

            this._table = this.GetJournalDataTable();
        }
        #endregion [Ctor's]

        private void ReadStop()
        {
            this._statusLabel.Text = READ_OSC_STOPPED;
            this._stopReadOsc.Enabled = false;
            this._oscProgressBar.Value = 0;
            this._oscJournalReadButton.Enabled = true;
            this._oscReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
        }

        #region [Help Classes Events Handlers]

        /// <summary>
        /// �������� ���� ������
        /// </summary>
        private void OnAllJournalReadOk()
        {
            if (this._oscJournalLoader.OscCount == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
            }
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
        }
        
        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
        }
        
        /// <summary>
        /// ��������� ���� ������ �������
        /// </summary>
        private void ReadRecord()
        {
            this._oscilloscopeCountCb.Items.Add(this._oscJournalLoader.OscCount);
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._oscJournalLoader.OscCount);
            this._table.Rows.Add(this._oscJournalLoader.GetRecord);
            this._oscJournalDataGrid.Refresh();
        }

        /// <summary>
        /// �������� ���� ������
        /// </summary>
        private void ReadAllRecords()
        {
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
            if (this._oscJournalLoader.OscCount == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
                return;
            }

            for (int i = 1; i <= this._oscJournalLoader.OscCount; i++)
            {
                this._oscilloscopeCountCb.Items.Add(i);
            }
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._oscJournalLoader.OscCount);
            foreach (OscJournalStruct record in this._oscJournalLoader.OscRecords)
            {
                this._table.Rows.Add(record.GetRecord);
                OscJournalStruct.RecordIndex++;
            }
            this._oscJournalDataGrid.Refresh();
        }

        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            AllConnectionStruct.SetDeviceConnectionsType(this._device.DevicePlant);
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            try
            {
                this.CountingList = new CountingList(this._device, this._pageLoader.ReadWords, this._journalStruct, this._connections.Value, _oscopeConfig.Value);
            }
            catch (Exception e)
            {
                MessageBox.Show("������ ������������� ���������� ��� �������", "��������", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            this._oscReadButton.Enabled = true;
            this._oscJournalReadButton.Enabled = true;
            this._oscSaveButton.Enabled = true;
            this._stopReadOsc.Enabled = false;
            this._oscLoadButton.Enabled = true;
            this._oscProgressBar.Value = this._oscProgressBar.Maximum;
        }

        #endregion [Help Classes Events Handlers]
        
        #region [Properties]
        /// <summary>
        /// ���������� ����������� ������� ������������ ��� ������
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// ������ ���
        /// </summary>
        public CountingList CountingList
        {
            get { return this._countingList ?? (this._countingList = new CountingList(0)); }
            set
            {
                this._countingList = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]


        #region [Help members]
        private DataTable GetJournalDataTable()
        {
            var table = new DataTable("��902_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }
        #endregion [Help members]


        #region [Event Handlers]
        /// <summary>
        /// �������� �����
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            this.StartRead();
        }
        /// <summary>
        /// �������� �������������
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (this.CountingList == null)
            {
                MessageBox.Show("������������� �� ���������");
                return;
            }
            try
            {
                if (Validator.GetVersionFromRegistry())
                {
                    string fileName;
                    if (this._countingList.IsLoad)
                    {
                        fileName = this._countingList.FilePath;
                    }
                    else
                    {
                        fileName = Validator.CreateOscFileNameCfg($"��902 v{this._device.DeviceVersion.Replace(".", "_")} ������������� {this._journalStruct.GetDate.Replace(".", "_")} {this._journalStruct.GetTime.Replace(":", "_").Replace(".", "_")}");
                        this._countingList.Save(fileName);
                    }
                    string oscPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe");
                    System.Diagnostics.Process.Start(oscPath, fileName);
                }
                else
                {
                    if (!Settings.Default.OscFlag)
                    {
                        LoadNetForm loadNetForm = new LoadNetForm();
                        loadNetForm.ShowDialog(this);
                        Settings.Default.OscFlag = loadNetForm.OscFlag;
                        Settings.Default.Save();
                    }

                    throw new Exception();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;            
            this._oscilloscopeCountCb.Items.Clear();
            this._oscilloscopeCountCb.SelectedIndex = -1;
            this._table.Clear();
            this._oscJournalLoader.Clear();
            this._oscJournalDataGrid.Refresh();
            this.CanSelectOsc = false;
            this._oscLoadButton.Enabled = false;
            this._oscReadButton.Enabled = false;
            this._oscJournalReadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            //this._connections.LoadStruct();
            _oscopeConfig.LoadStruct();
        }

        /// <summary>
        /// ��������� �������������
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            this._statusLabel.Text = READ_OSC_JOURNAL;
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStruct = this._oscJournalLoader.OscRecords[selectedOsc];
            this._pageLoader.StartRead(this._journalStruct, this._oscilloscopeSettings.Value);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoader.PagesCount;
            //�������� ����������� ���������� ������ ������������
            this._oscReadButton.Enabled = false;
            this._stopReadOsc.Enabled = true;
            this._oscJournalReadButton.Enabled = false;
            this._oscLoadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
        }

        /// <summary>
        /// ��������� ������������� � ����
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        { 
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    this.CountingList.Save(this._saveOscilloscopeDlg.FileName);
                    this._statusLabel.Text = OSC + " ������� ���������";
                }
                catch
                {
                    this._statusLabel.Text = "���������� ��������� ������������";
                }
            }
        }

        /// <summary>
        /// ��������� ������������� �� �����
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;

            try
            {
                this.CountingList = this.CountingList.Load(this._openOscilloscopeDlg.FileName, this._device);
                this._statusLabel.Text = string.Format("������������ ��������� �� ����� {0}", this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
                this._stopReadOsc.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = "���������� ��������� ������������";
            }

        }

        #endregion [Event Handlers]

        /// <summary>
        /// ���������� ������ ������������
        /// </summary>
        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._pageLoader.StopRead();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {   
            this._oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscBeginColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this.checkBox1.Checked;
        }

        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }

        #region [IFormView Members]
        public Type FormDevice => typeof(Mr902New);

        public bool Multishow { get; private set; }

        public Type ClassType => typeof(OscilloscopeForm);

        public bool ForceShow => false;

        public Image NodeImage => Resources.oscilloscope.ToBitmap();

        public string NodeName => OSC;

        public INodeView[] ChildNodes => new INodeView[] { };

        public bool Deletable => false;

        #endregion [IFormView Members]

        private void OscilloscopeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._pageLoader.ClearEvents();
            this._oscJournalLoader.ClearEvents();
        }
    }
}