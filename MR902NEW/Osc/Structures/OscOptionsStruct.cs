﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR902NEW.Osc.Structures
{
    /// <summary>
    /// Параматры осцилографа
    /// </summary>
    public class OscSettingsStruct : StructBase
    {
        #region [Private fields]
        /// <summary>
        /// Общий размер осциллографа
        /// </summary>
        [Layout(0)] private int _loadedFullOscSize;
        /// <summary>
        /// Полный размер осцилографа в страницах
        /// </summary>
        [Layout(1)] private ushort _fullOscSizeInPages;
        /// <summary>
        /// Размер одного отсчёта в словах
        /// </summary>
        [Layout(2)] private ushort _sizeCounting;
        /// <summary>
        /// Длинна одной осциллограммы в отсчётах
        /// </summary>
        [Layout(3)] private ushort _oscLenght;
        /// <summary>
        /// Размер страницы в словах(const)
        /// </summary>
        [Layout(4)] private ushort _pageSize;

        [Layout(5)] private ushort _currentsCount;
        [Layout(6)] private ushort _voltagesCount;
        [Layout(7)] private ushort _discretsCount;
        [Layout(8)] private ushort _channelsCount;
        [Layout(9)] private ushort _currentsVirtCount;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Полный размер осцилографа в страницах
        /// </summary>
        public ushort FullOscSizeInPages => this._fullOscSizeInPages;
        /// <summary>
        /// Размер страницы в словах(const)
        /// </summary>
        public ushort PageSize => 1024;

        /// <summary>
        /// Общий размер осциллографа
        /// </summary>
        public int LoadedFullOscSizeInWords
        {
            get => this._loadedFullOscSize;
            set { this._loadedFullOscSize = value; }
        }

        public ushort SizeCounting => this._sizeCounting;
        public ushort CurrentsCount => this._currentsCount;
        public ushort VoltagesCount => this._voltagesCount;

        public int DiscretsCount => this._discretsCount;

        public ushort CurrentsVirtCount => this._currentsVirtCount;

        public int ChannelsCount
        {
            get
            {
                //int channelsAndDiscrets = this._sizeCounting - (this._voltagesCount + this._currentsCount);
                //return  (int)((channelsAndDiscrets - this._discretsCount/16.0)*16);
                return _channelsCount;
            }
        }

        #endregion [Properties]
    }
}
