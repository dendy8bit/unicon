﻿using System;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.MR902NEW.Configuration.Structures.Connections;
using BEMN.MR902NEW.HelpClasses;

namespace BEMN.MR902NEW.Osc.Structures
{
    /// <summary>
    /// Загружает уставки токов(Iтт)
    /// </summary>
    public class CurrentOptionsLoader
    {
        #region [Private fields]
        
        private const int COUNT_GROUPS = 2;
        private int _numberOfGroup;

        private MemoryEntity<AllConnectionStruct>[] _connections;

        public AllConnectionStruct this[int index]
        {
            get { return this._connections[index].Value; }
        }

        #endregion [Private fields]

        #region [Events]
        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        public event Action LoadOk;
        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        public event Action LoadFail;
        private Mr902New _device;

        #endregion [Events]


        #region [Ctor's]
        public CurrentOptionsLoader(Mr902New device)
        {
            this._device = device;
            this._connections = new MemoryEntity<AllConnectionStruct>[COUNT_GROUPS];
            for (int i = 0; i < this._connections.Length; i++)
            {
                this._connections[i] = new MemoryEntity<AllConnectionStruct>(string.Format("Параметры измерительного трансформатора гр{0}", i + 1),
                    device, this._device.GetStartAddrMeasTrans(i));
                this._connections[i].AllReadOk += this._connections_AllReadOk;
                this._connections[i].AllReadFail += this._connections_AllReadFail;
            }
        }
        #endregion [Ctor's]


        #region [Memory Entity Events Handlers]
        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        void _connections_AllReadFail(object sender)
        {
            LoadFail?.Invoke();
        }
        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        void _connections_AllReadOk(object sender)
        {
            this._numberOfGroup++;
            if (this._numberOfGroup < COUNT_GROUPS)
            {
                this._connections[this._numberOfGroup].LoadStruct();
            }
            else
            {
                this.LoadOk?.Invoke();
            }
        }  
        #endregion [Memory Entity Events Handlers]


        #region [Public members]
        /// <summary>
        /// Запускает загрузку уставок токов(Iтт)
        /// </summary>
        public void StartRead()
        {
            this._numberOfGroup = 0;
            this._connections[0].LoadStruct();
        } 
        #endregion [Public members]
    }
}
