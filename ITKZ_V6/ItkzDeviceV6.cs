﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Framework.BusinessLogic;
using BEMN.Interfaces;
using BEMN.ITKZ_V6.Calibrate;
using BEMN.ITKZ_V6.Configuration;
using BEMN.ITKZ_V6.Measuring;
using BEMN.ITKZ_V6.Osc;
using BEMN.ITKZ_V6.Osc.Structures;
using BEMN.ITKZ_V6.Structures;
using BEMN.MBServer;

namespace BEMN.ITKZ_V6
{
    public class ItkzDeviceV6 : Device, IDeviceView, IDeviceVersion
    {
        #region [Fields]
        private MemoryEntity<OneWordStruct> _iMinWord;
        private readonly MemoryEntity<OneWordStruct> _version;
        #endregion [Fields]

        #region [Properties]
        public MemoryEntity<OneWordStruct> Version => this._version;

        // Конфигурация
        public MemoryEntity<NumAndSpeedDevice> NumAndSpeed { get; } // время и скорость устройства Конфигурация
        public MemoryEntity<Settings> Settings { get; } // входит в структуру Конфигурации и Измерения
        public MemoryEntity<TimeSettings> TimeSettings { get; } // входит в структуру Конфигурации
        public MemoryEntity<TimeSettings> TimeSettingsForOsc { get; } // входит в структуру Конфигурации
        public MemoryEntity<ConfigurationStruct> ConfigurationStruct { get; } // ГЛАВНАЯ СТРУКТУРА КОНФИГУРАЦИИ
        public MemoryEntity<OneWordStruct> ConfigurationCommand { get; } // команды в Конфигурации
        public MemoryEntity<CalibrateNominalValues> CalibrateNominalValues { get; } // номинальные значения в Конфигурации (Блок калибровки по тз)
        public MemoryEntity<MeasuringStruct> MeasuringStructConfiguration { get; } // структура измерений, читаемая на форме Конфигурации
        public MemoryEntity<ReleOutputTime> ReleOutputTime { get; } // Уставка управление выключателем (читается циклически, только для чтения)
        public MemoryEntity<OneWordStruct> ReleOutputTimeSet { get; } // Уставка управление выключателем (для записи)

        public MemoryEntity<OneWordStruct> FinishSetTime { get; } // команда завершения установки времени
        public MemoryEntity<OneWordStruct> AvtoKvit { get; } // для записи команды автоквитирования в Конфигурации
        public MemoryEntity<OneWordStruct> AngleStatus { get; } // для записи команды уставки ширины сектора в Конфигурации
        public MemoryEntity<OneWordStruct> UpdateParam { get; } // для записи команды обновления аварийных параметров при квитировании в Конфигурации
        public MemoryEntity<OneWordStruct> StartSetTime { get; } // команда начала установки времени

        // Измерения
        public MemoryEntity<MeasuringStruct> MeasuringStruct { get; } // ГЛАВНАЯ СТРУКТУРА ИЗМЕРЕНИЯ
        public MemoryEntity<OneWordStruct> MeasuringCommand { get; } // команды в Измерениях
        public MemoryEntity<ReleOutputTime> ReleOutputTimeMeasuring { get; } // Уставка управление выключателем (читается циклически)

        // Калибровка
        public MemoryEntity<VrefUshort> Vref { get; } // независимая структура, используется в Калибровке
        public MemoryEntity<MeasuringStruct> MeasuringStructCalibrate { get; } // структура измерений, читаемая на форме VoltageCalibration
        public MemoryEntity<OneWordStruct> CommandCalibrate { get; } // команда сброса заводских настроек

        // Осциллограф
        public MemoryEntity<OscJournalStruct> OscJournal { get; } // структура одной Осциллограммы
        public MemoryEntity<AllOscJournalStruct> AllOscJournal { get; } // массив из 40 структур Осциллограммы 
        public MemoryEntity<OneWordStruct> RefreshOscJournal { get; } // обновление журнала Осциллографа
        public MemoryEntity<OscPage> OscPage { get; } // страница Осциллографа 
        public MemoryEntity<SetOscStartPageStruct> SetOscStartPage { get; } // начальная страница чтения осциллограммы
        
        #endregion [Properties]

        #region [Constructor]
        public ItkzDeviceV6() { HaveVersion = true; }
        public ItkzDeviceV6(Modbus mb)
        {
            HaveVersion = true;
            this.MB = mb;

            // Версия
            this._version = new MemoryEntity<OneWordStruct>("version", this, 0x0101); // в 6-ой версии чтение версии устройства идет с 0x0101 адреса 
            this._version.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.OnVersionReadOk);
            this._version.AllReadFail += HandlerHelper.CreateReadArrayHandler(() => { TreeManager.OnDeviceVersionLoadFail(this); });
            
            // Конфигурация
            this.NumAndSpeed = new MemoryEntity<NumAndSpeedDevice>("Номер и скорость устройства (конфигурация)", this, 0x0102); // на форме Конфигурации
            this.Settings = new MemoryEntity<Settings>("Настройка (конфигурация)", this, 0x0111); // входит в структуру Конфигурации
            this.TimeSettings = new MemoryEntity<TimeSettings>("Настройка времени (конфигурация)", this, 0x0112); // входит в структуру Конфигурации
            this.TimeSettingsForOsc = new MemoryEntity<TimeSettings>("Настройка времени оциллограф", this, 0x0112); // входит в структуру Конфигурации
            this.ConfigurationStruct = new MemoryEntity<ConfigurationStruct>("Конфигурация усройства", this, 0x0104); // ГЛАВНАЯ СТРУКТУРА КОНФИГУРАЦИИ
            this.ConfigurationCommand = new MemoryEntity<OneWordStruct>("Команда конфигурации", this, 0x0000); // команды в Конфигурации
            this.CalibrateNominalValues = new MemoryEntity<CalibrateNominalValues>("Номинальные значения (конфигурация)", this, 0x0150); // номинальные значения в Конфигурации (Блоке регистров калибровочных коэффициентов)
            this.StartSetTime = new MemoryEntity<OneWordStruct>("Команда начала установки времени (конфигурация)", this, 0x0000); // команда установки времени в в Конфигурации
            this.FinishSetTime = new MemoryEntity<OneWordStruct>("Команда завершения установки времени (конфигурация)", this, 0x0000); // команда установки времени в в Конфигурации
            this.AvtoKvit = new MemoryEntity<OneWordStruct>("Команда автоквитирования (конфигурация)", this, 0x0000); // команда Конфигурации
            this.UpdateParam = new MemoryEntity<OneWordStruct>("Команда обновления аварийных параметров при квитировании (конфигурация)", this, 0x0000); // команда Конфигурации
            this.AngleStatus = new MemoryEntity<OneWordStruct>("Команда уставки ширины сектора (конфигурация)", this, 0x0000); // команда Конфигурации
            this.MeasuringStructConfiguration = new MemoryEntity<MeasuringStruct>("Измерения (конфигурация)", this, 0x0115); // структура измерений, читаемая на форме Конфигурации
            this.ReleOutputTime = new MemoryEntity<ReleOutputTime>("Время управления выключателем (конфигурация)", this, 0x113); // Уставка управление выключателем (читается циклически)
            this.ReleOutputTimeSet = new MemoryEntity<OneWordStruct>("Уставка управление выключателем, установка (конфигурация)", this, 0x113); // Уставка управление выключателем для записи
            
            // Измерения
            this.MeasuringStruct = new MemoryEntity<MeasuringStruct>("Измерения", this, 0x0115); // ГЛАВНАЯ СТРУКТУРА ИЗМЕРЕНИЯ (читается циклически)
            this.MeasuringCommand = new MemoryEntity<OneWordStruct>("Команда измерения", this, 0x0000); // команды в Измерениях
            this.ReleOutputTimeMeasuring = new MemoryEntity<ReleOutputTime>("Время управления выключателем (измерения)", this, 0x113); // Уставка управление выключателем (читается циклически)

            // Калибровка
            this.Vref = new MemoryEntity<VrefUshort>("Значение Vref (калибровка)", this, 0x0153); // поле в Калибровке
            this.MeasuringStructCalibrate = new MemoryEntity<MeasuringStruct>("Измерения (калибровка)", this, 0x0115); // структура измерений, читаемая на форме VoltageCalibration
            this.CommandCalibrate = new MemoryEntity<OneWordStruct>("Сброс заводских настроек (калибровка)", this, 0x0000);

            // Осциллограф
            this.OscJournal = new MemoryEntity<OscJournalStruct>("Одна запись в журнале осциллографа", this, 0x1000);
            this.AllOscJournal = new MemoryEntity<AllOscJournalStruct>("Все записи в журнале осциллографа", this, 0x1000);
            this.RefreshOscJournal = new MemoryEntity<OneWordStruct>("Индекс журнала осциллографа", this, 0x1000);
            this.OscPage = new MemoryEntity<OscPage>("Страница осциллографа", this, 0x1100 /*, slotLen */); // в МР901 чтение страницы с 900 адреса, когда чтение журнала с 800
            this.SetOscStartPage = new MemoryEntity<SetOscStartPageStruct>("Установка стартовой страницы осциллограммы", this, 0x1008);
        }

        #endregion [Constructor]

        private void OnVersionReadOk()
        {
            string value = this._version.Value.Word.ToString("X");
            int[] valueToArray = value.Select(x => int.Parse(x.ToString())).ToArray();
            if (value == "0")
            {
                this.Version1 = 1;
                this.Version2 = 0;
                this.Version3 = 0;
                DeviceVersion = this.Versions[0];
            }
            else
            {
                this.Version1 = valueToArray[0];
                this.Version2 = valueToArray[1];
                this.Version3 = valueToArray[2];
                DeviceVersion = $"{this.Version1}.{this.Version2}.{this.Version3}";
            }
            DeviceType = "ITKZ_V6";
            LoadVersionOk();
        }

        public int Version1 { get; private set; }
        public int Version2 { get; private set; }
        public int Version3 { get; private set; }

        public override void LoadVersion(object deviceObj)
        {
            this._version.LoadStruct();
        }
        
        #region [IDeviceVersion]
        public List<string> Versions
        {
            get
            {
                return new List<string>
                {
                    "6.0.0"
                };
            }
        }

        public Type[] Forms
        {
            get
            {
                return new[]
                {
                    typeof (ItkzConfigurationForm),
                    typeof (CalibrateForm),
                    typeof (ItkzMeasuringForm),
                    typeof (OscilloscopeForm)
                };
            }
        }
        #endregion [IDeviceVersion]

        #region [INodeView]

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType => typeof(ItkzDeviceV6);

        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow => false;

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage => Framework.Properties.Resources.itkz;

        [Browsable(false)]
        public string NodeName => "ИТКЗ исполнение 2 версия 6.xx";

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes => new INodeView[] { };

        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable => true;

        #endregion [INodeView]

        [XmlIgnore]
        public override Modbus MB
        {
            get => mb;
            set
            {
                mb = value;
                if (null != mb)
                {
                    mb.CompleteExchange += mb_CompleteExchange;
                }
            }
        }
    }
}