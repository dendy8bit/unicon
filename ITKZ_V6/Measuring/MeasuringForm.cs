﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Interfaces;

namespace BEMN.ITKZ_V6.Measuring
{
    public partial class ItkzMeasuringForm : Form, IFormView
    {
        private readonly ItkzDeviceV6 _device;

        private readonly LedControl[] _settingsLeds;
        private readonly LedControl[] _statusLeds;
        private readonly LedControl[] _ioStatusLeds;
        private readonly LedControl[] _stStatusLeds;
        private readonly LedControl[] _blinkerLeds;
        
        private enum Command
        {
            NONE, //            0.
            KVITIROVANIE, //    1. 0xaaaa - квитирование прибора
            SET_IN_ON, //       2. 0x04aa - введение ступени индикации по In
            SET_IN_OFF, //      3. 0x0455 - выведение ступени индикации по In
            SET_IN5_ON, //      4. 0x05aa - введение ступени индикации по In5 (гармоника)
            SET_IN5_OFF, //     5. 0x0555 - выведение ступени индикации по In5 (гармоника)
            SET_U0_ON, //       6. 0x08aa - введение ступени индикации по U0
            SET_U0_OFF, //      7. 0x0855 - выведение ступени индикации по U0
            SET_IA_ON, //       8. 0x64aa - введение ступени индикации по Ia
            SET_IA_OFF, //      9. 0x6455 - выведение ступени индикации по Ia
            SET_IB_ON, //      10. 0x65aa - введение ступени индикации по Ib
            SET_IB_OFF, //     11. 0x6555 - выведение ступени индикации по Ib
            SET_IC_ON, //      12. 0x66aa - введение ступени индикации по Ic
            SET_IC_OFF, //     13. 0x6655 - выведение ступени индикации по Ic
            SET_F_ON, //       14. 0x67aa - введение ступеней индикации по всем токам фаз
            SET_F_OFF, //      15. 0x6755 - выведение ступеней индикации по всем токам фаз
            SET_DIR_ON, //     16. 0x14aa - включение режима направленного срабатывания по In
            SET_DIR_OFF, //    17. 0x1455 - выключение режима направленного срабатывания по In
            SET_ENERGY_ON, //  18. 0x15aa - включение режима направленного срабатывания по In с использованием энергии
            SET_ENERGY_OFF, // 19. 0x1555 - включение режима направленного срабатывания по In с использованием энергии
            SET_DIR_1, //      20. 0x24aa - установка направленного срабатывания "от шин"
            SET_DIR_0, //      21. 0x2455 - установка направленного срабатывания "к шинам"
            TRIG_IA, //        22. 0x0701 - имитация срабатывания ступени индикации Ia  
            TRIG_IB, //        23. 0x0702 - имитация срабатывания ступени индикации Ib
            TRIG_IC, //        24. 0x0703 - имитация срабатывания ступени индикации Ic
            TRIG_IN, //        25. 0x0704 - имитация срабатывания ступени индикации In
            TRIG_IN5, //       26. 0x0705 - имитация срабатывания ступени индикации In5
            TRIG_U0, //        27. 0x0706 - имитация срабатывания ступени индикации U0
            RESET_ERRORS //    28. 0x0fff - команда сброса устранимых ошибок
        }

        private Command _currentCmd;

        public ItkzMeasuringForm()
        {
            this.Multishow = false;
            this.InitializeComponent();
        }

        public ItkzMeasuringForm(ItkzDeviceV6 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopLoad; 
            this.Multishow = false;

            this._settingsLeds = new[]
            {
                this._modLedOzz, 
                this._modLedIn, this._modLedIn5, this._modLedU0,
                this._modLedIa, this._modLedIb, this._modLedIc,
                new LedControl(), new LedControl(), new LedControl(), new LedControl(), new LedControl(), new LedControl(), new LedControl(),
                this._modLedUstDir, this._modLedInOutDir
            };

            this._statusLeds = new[]
            {
                this._infoLed1, this._infoLed2, this._infoLed3, this._infoLed4,
                new LedControl(), new LedControl(), new LedControl(), new LedControl(), new LedControl(),
                new LedControl(), new LedControl(), new LedControl(), new LedControl(),
                this._infoLed5, new LedControl(), new LedControl()
            };

            this._ioStatusLeds = new[]
            {
                this._statusLedIa, this._statusLedIb, this._statusLedIc, this._statusLedIn, this._statusLedIn5, this._statusLedU0, this._statusLedEnergy,
                new LedControl(), new LedControl(), new LedControl(), new LedControl(), new LedControl(), new LedControl(), new LedControl(), new LedControl(), new LedControl()
            };

            this._blinkerLeds = new[]
            {
                this._blinkerLed1, this._blinkerLed2, this._blinkerLed3, this._blinkerLed4, this._blinkerLed5, this._blinkerLed6
            };

            this._stStatusLeds = new[]
            {
                this._stupenLedIa, this._stupenLedIb, this._stupenLedIc, this._stupenLedIn, this._stupenLedIn5, this._stupenLedU0,
                new LedControl(), new LedControl(),
                this._directionLedIn, this._dostoverLedIn,
                this._directionLedIn5, this._dostoverLedIn5,
                this._directionLedU0, this._dostoverLedU0,
                new LedControl(), new LedControl()
            };
            
            this._device.MeasuringStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringReadOk);
            this._device.MeasuringStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringReadFail);

            this._device.MeasuringCommand.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.SendCommandOk);
            this._device.MeasuringCommand.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.SendCommandFail);

            this._currentCmd = Command.NONE;
        }

        private void MeasuringReadOk()
        {
            // Все леды
            LedManager.SetLeds(this._settingsLeds, this._device.Settings.Value.SettingsMeasuring);
            LedManager.SetLeds(this._statusLeds, this._device.MeasuringStruct.Value.Status);
            LedManager.SetLeds(this._ioStatusLeds, this._device.MeasuringStruct.Value.IoStatus);
            LedManager.SetLeds(this._stStatusLeds, this._device.MeasuringStruct.Value.StStatus);
            LedManager.SetLeds(this._blinkerLeds, this._device.MeasuringStruct.Value.BlinkerAlarm);
            if (this._device.ReleOutputTimeMeasuring.Value.IsImpulsMode == true) LedManager.SetLeds(this._blinkerLeds, this._device.MeasuringStruct.Value.BlinkerAlarm);
            else LedManager.TurnOffLeds(this._blinkerLeds);

            // Столбец "Время срабатывания ступени"
            this._iaTimeStageLabel.Text = this._device.MeasuringStruct.Value.IaLastvalTime(this._device.TimeSettings.Value);
            this._ibTimeStageLabel.Text = this._device.MeasuringStruct.Value.IbLastvalTime(this._device.TimeSettings.Value); 
            this._icTimeStageLabel.Text = this._device.MeasuringStruct.Value.IcLastvalTime(this._device.TimeSettings.Value); 
            this._inTimeStageLabel.Text = this._device.MeasuringStruct.Value.InLastvalTime(this._device.TimeSettings.Value); 
            this._in5TimeStageLabel.Text = this._device.MeasuringStruct.Value.In5LastvalTime(this._device.TimeSettings.Value); 
            this._u0TimeStageLabel.Text = this._device.MeasuringStruct.Value.U0LastvalTime(this._device.TimeSettings.Value);

            // Столбец "Текущие значения"
            this._curValIa.Text = this._device.MeasuringStruct.Value.IaValue.ToString();
            this._curValIb.Text = this._device.MeasuringStruct.Value.IbValue.ToString();
            this._curValIc.Text = this._device.MeasuringStruct.Value.IcValue.ToString();
            this._curValIn.Text = this._device.MeasuringStruct.Value.InValue.ToString();
            this._curValIn5.Text = this._device.MeasuringStruct.Value.In5Value.ToString();
            this._curValU0.Text = this._device.MeasuringStruct.Value.U0Value.ToString();
            this._curValEnergy.Text = this._device.MeasuringStruct.Value.Energy.ToString();
            
            // Столбец "Значения при последнем срабатывании"
            this._lastValIa.Text = this._device.MeasuringStruct.Value.IaLastval.ToString();
            this._lastValIb.Text = this._device.MeasuringStruct.Value.IbLastval.ToString();
            this._lastValIc.Text = this._device.MeasuringStruct.Value.IcLastval.ToString();
            this._in_lastValIn.Text = this._device.MeasuringStruct.Value.InLastvalIn.ToString();
            this._in_lastValIn5.Text = this._device.MeasuringStruct.Value.InLastvalIn5.ToString();
            this._in_lastValU0.Text = this._device.MeasuringStruct.Value.InLastvalU0.ToString();
            this._inAngle.Text = this._device.MeasuringStruct.Value.InLastvalAngle.ToString();
            this._in5_lastValIn.Text = this._device.MeasuringStruct.Value.In5LastvalIn.ToString();
            this._in5_lastValIn5.Text = this._device.MeasuringStruct.Value.In5LastvalIn5.ToString();
            this._in5_lastValU0.Text = this._device.MeasuringStruct.Value.In5LastvalU0.ToString();
            this._in5Angle.Text = this._device.MeasuringStruct.Value.In5LastvalAngle.ToString();
            this._u0_lastValIn.Text = this._device.MeasuringStruct.Value.U0LastvalIn.ToString();
            this._u0_lastValIn5.Text = this._device.MeasuringStruct.Value.U0LastvalIn5.ToString();
            this._u0_lastValU0.Text = this._device.MeasuringStruct.Value.U0LastvalU0.ToString();
            this._u0Angle.Text = this._device.MeasuringStruct.Value.U0LastvalAngle.ToString();
            this._lastValEnergy.Text = this._device.MeasuringStruct.Value.EnergyLastvalIn.ToString();

            // Строка напротив кнопки Квитирование
            this._u0Phase.Text = this._device.MeasuringStruct.Value.U0Phase.ToString();
            this._inPhase.Text = this._device.MeasuringStruct.Value.InPhase.ToString();
            this._inPhaseShift.Text = this._device.MeasuringStruct.Value.InPhaseShift.ToString();

            // Unix-время
            this._dateTimeLabel.Text = this._device.MeasuringStruct.Value.CurrentTimeUnixMeasuring(this._device.TimeSettings.Value);

            // Поля "Напряжение батареи", "Напряжение питания", "Температура"
            this._vBat.Text = this._device.MeasuringStruct.Value.VBat.ToString();
            this._supplyVoltage.Text = this._device.MeasuringStruct.Value.SupplyVoltage.ToString();
            this._temperature.Text = this._device.MeasuringStruct.Value.Temperature.ToString();

            
            if (this._modLedUstDir.State == LedState.Signaled)
            {
                this._stateUstNaprSrabTB.Text = @"К шинам";
            }
            if (this._modLedUstDir.State == LedState.NoSignaled)
            {
                this._stateUstNaprSrabTB.Text = @"От шин";
            }

            this.groupBoxModOzz.Enabled = this._modLedInOutDir.State == LedState.NoSignaled;


            if (this._modLedOzz.State == LedState.Signaled)
            {
                this._modOzz.Text = @"Угол";
            }
            if (this._modLedOzz.State == LedState.NoSignaled)
            {
                this._modOzz.Text = @"Энергия";
            }
        }
        private void MeasuringReadFail()
        {
            LedManager.TurnOffLeds(this._settingsLeds);
            LedManager.TurnOffLeds(this._statusLeds);
            LedManager.TurnOffLeds(this._ioStatusLeds);
            LedManager.TurnOffLeds(this._stStatusLeds);
            LedManager.TurnOffLeds(this._blinkerLeds);

            this._curValIa.Text = string.Empty;
            this._curValIb.Text = string.Empty;
            this._curValIc.Text = string.Empty;
            this._curValIn.Text = string.Empty;
            this._curValIn5.Text = string.Empty;
            this._curValU0.Text = string.Empty;
            this._curValEnergy.Text = string.Empty;
            
            this._lastValIa.Text = string.Empty;
            this._lastValIb.Text = string.Empty;
            this._lastValIc.Text = string.Empty;
            this._in_lastValIn.Text = string.Empty;
            this._in_lastValIn5.Text = string.Empty;
            this._in_lastValU0.Text = string.Empty;
            this._inAngle.Text = string.Empty;
            this._in5_lastValIn.Text = string.Empty;
            this._in5_lastValIn5.Text = string.Empty;
            this._in5_lastValU0.Text = string.Empty;
            this._in5Angle.Text = string.Empty;
            this._u0_lastValIn.Text = string.Empty;
            this._u0_lastValIn5.Text = string.Empty;
            this._u0_lastValU0.Text = string.Empty;
            this._u0Angle.Text = string.Empty;
            this._lastValEnergy.Text = string.Empty;

            this._iaTimeStageLabel.Text = string.Empty;
            this._ibTimeStageLabel.Text = string.Empty;
            this._icTimeStageLabel.Text = string.Empty;
            this._inTimeStageLabel.Text = string.Empty;
            this._in5TimeStageLabel.Text = string.Empty;
            this._u0TimeStageLabel.Text = string.Empty;

            this._u0Phase.Text = string.Empty;
            this._inPhase.Text = string.Empty;
            this._inPhaseShift.Text = string.Empty;

            this._dateTimeLabel.Text = string.Empty;

            this._vBat.Text = string.Empty;

            if (this._modLedOzz.State == LedState.Off)
            {
                this._modOzz.Text = string.Empty;
            }

            if (this._modLedUstDir.State == LedState.Off)
            {
                this._stateUstNaprSrabTB.Text = string.Empty;
            }
            
            this._currentCmd = Command.NONE;
        }

        private void SendCommandOk()
        {
            switch (this._currentCmd)
            {
                case Command.KVITIROVANIE:
                    MessageBox.Show(@"Квитирование выполнено успешно", @"Квитирование", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_IN_ON:
                    MessageBox.Show(@"Режим ступени индикации In введен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_IN_OFF:
                    MessageBox.Show(@"Режим ступени индикации In выведен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_IN5_ON:
                    MessageBox.Show(@"Режим ступени индикации In5 введен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_IN5_OFF:
                    MessageBox.Show(@"Режим ступени индикации In5 выведен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_U0_ON:
                    MessageBox.Show(@"Режим ступени индикации U0 введен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_U0_OFF:
                    MessageBox.Show(@"Режим ступени индикации U0 выведен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_IA_ON:
                    MessageBox.Show(@"Режим ступени индикации Ia введен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_IA_OFF:
                    MessageBox.Show(@"Режим ступени индикации Ia выведен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_IB_ON:
                    MessageBox.Show(@"Режим ступени индикации Ib введен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_IB_OFF:
                    MessageBox.Show(@"Режим ступени индикации Ib выведен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_IC_ON:
                    MessageBox.Show(@"Режим ступени индикации Ic введен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_IC_OFF:
                    MessageBox.Show(@"Режим ступени индикации Ic выведен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_F_ON:
                    MessageBox.Show(@"Режим всех ступеней индикации введен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_F_OFF:
                    MessageBox.Show(@"Режим всех ступеней индикации выведен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_DIR_ON:
                    MessageBox.Show(@"Режим направленного срабатывания введен успешно", @"Режим направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_DIR_OFF:
                    MessageBox.Show(@"Режим направленного срабатывания выведен успешно", @"Режим направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);                      
                    break;
                case Command.SET_ENERGY_ON:
                    MessageBox.Show(@"Режим направленного срабатывания с использованием энергии введен успешно", @"Режим направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_ENERGY_OFF:
                    MessageBox.Show(@"Режим направленного срабатывания с использованием энергии выведен успешно", @"Режим направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_DIR_1:
                    MessageBox.Show(@"Установленно направление ""От шин"" ", @"Режим уставки направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_DIR_0:
                    MessageBox.Show(@"Установленно направление ""К шинам"" ", @"Режим направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.TRIG_IA:
                    MessageBox.Show(@"Имитация срабатывания ступени Ia выполнена успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.TRIG_IB:
                    MessageBox.Show(@"Имитация срабатывания ступени Ib выполнена успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.TRIG_IC:
                    MessageBox.Show(@"Имитация срабатывания ступени Ic выполнена успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.TRIG_IN:
                    MessageBox.Show(@"Имитация срабатывания ступени In выполнена успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.TRIG_IN5:
                    MessageBox.Show(@"Имитация срабатывания ступени In5 выполнена успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.TRIG_U0:
                    MessageBox.Show(@"Имитация срабатывания ступени U0 выполнена успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.RESET_ERRORS:
                    MessageBox.Show(@"Команда сброса ошибок выполнена успешно", @"Сброс ошибок", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
        }
        private void SendCommandFail()
        {
            switch (this._currentCmd)
            {
                case Command.KVITIROVANIE:
                    MessageBox.Show(@"Невозможно выполнить квитирование", @"Квитирование", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.SET_IN_ON:
                    MessageBox.Show(@"Невозможно выполнить команду ввода режима ступени индикации In!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.SET_IN_OFF:
                    MessageBox.Show(@"Невозможно выполнить команду вывода режима ступени индикации In!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.SET_IN5_ON:
                    MessageBox.Show(@"Невозможно выполнить команду ввода режима ступени индикации In5!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.SET_IN5_OFF:
                    MessageBox.Show(@"Невозможно выполнить команду вывода режима ступени индикации In5!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.SET_U0_ON:
                    MessageBox.Show(@"Невозможно выполнить команду ввода режима ступени индикации U0!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.SET_U0_OFF:
                    MessageBox.Show(@"Невозможно выполнить команду вывода режима ступени индикации U0!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.SET_IA_ON:
                    MessageBox.Show(@"Невозможно выполнить команду ввода режима ступени индикации Ia!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.SET_IA_OFF:
                    MessageBox.Show(@"Невозможно выполнить команду вывода режима ступени индикации Ia!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.SET_IB_ON:
                    MessageBox.Show(@"Невозможно выполнить команду ввода режима ступени индикации Ib!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.SET_IB_OFF:
                    MessageBox.Show(@"Невозможно выполнить команду вывода режима ступени индикации Ib!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.SET_IC_ON:
                    MessageBox.Show(@"Невозможно выполнить команду ввода режима ступени индикации Ic!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.SET_IC_OFF:
                    MessageBox.Show(@"Невозможно выполнить команду вывода режима ступени индикации Ic!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.SET_F_ON:
                    MessageBox.Show(@"Невозможно выполнить команду ввода режима всех ступеней индикации!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.SET_F_OFF:
                    MessageBox.Show(@"Невозможно выполнить команду вывода режима всех ступеней индикации!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.SET_DIR_ON:
                    MessageBox.Show(@"Невозможно выполнить команду ввода режима направленного срабатывания", @"Режим направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_DIR_OFF:
                    MessageBox.Show(@"Невозможно выполнить команду вывода режима направленного срабатывания", @"Режим направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_ENERGY_ON:
                    MessageBox.Show(@"Невозможно выполнить команду ввода режима направленного срабатывания с использованием энергии", @"Режим направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_ENERGY_OFF:
                    MessageBox.Show(@"Невозможно выполнить команду вывода режима направленного срабатывания с использованием энергии", @"Режим направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_DIR_1:
                    MessageBox.Show(@"Невозможно выполнить команду установки режима уставки направленного срабатывания", @"Режим уставки направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_DIR_0:
                    MessageBox.Show(@"Невозможно выполнить команду обнуления режима уставки направленного срабатывания", @"Режим уставки направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.TRIG_IA:
                    MessageBox.Show(@"Невозможно выполнить команду имитация срабатывания ступени Ia", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.TRIG_IB:
                    MessageBox.Show(@"Невозможно выполнить команду имитация срабатывания ступени Ib", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.TRIG_IC:
                    MessageBox.Show(@"Невозможно выполнить команду имитация срабатывания ступени Ic", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.TRIG_IN:
                    MessageBox.Show(@"Невозможно выполнить команду имитация срабатывания ступени In", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.TRIG_IN5:
                    MessageBox.Show(@"Невозможно выполнить команду имитация срабатывания ступени In5", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.TRIG_U0:
                    MessageBox.Show(@"Невозможно выполнить команду имитация срабатывания ступени U0", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.RESET_ERRORS:
                    MessageBox.Show(@"Невозможно выполнить команду сброса устранимых ошибок", @"Сброс ошибок", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
        }


        /// <summary>
        /// Метод содержит структуры, которые будут читаться циклически при подключенном устройстве.
        /// </summary>
        private void StartStopLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.ReleOutputTimeMeasuring.LoadStructCycle();
                this._device.Settings.LoadStructCycle();
                this._device.TimeSettings.LoadStructCycle();
                this._device.MeasuringStruct.LoadStructCycle(/*new TimeSpan(0,0,0,1)*/);
            }
            else
            {
                this._device.ReleOutputTimeMeasuring.RemoveStructQueries();
                this._device.Settings.RemoveStructQueries();
                this._device.TimeSettings.RemoveStructQueries();
                this._device.MeasuringStruct.RemoveStructQueries();
                this.MeasuringReadFail();
            }
        }
      
        private void SendCmd(ushort command)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.MeasuringCommand.Value.Word = command;
            this._device.MeasuringCommand.SaveStruct6();
        }

        #region [FormControls Methods]
        private void ItkzMeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopLoad();
        }

        private void ItkzMeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.ConnectionModeChanged -= this.StartStopLoad;
            this._device.MeasuringStruct.RemoveStructQueries();
            this._device.Settings.RemoveStructQueries();
            this._device.TimeSettings.RemoveStructQueries();
        }

        private void _kvitBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.KVITIROVANIE; // 1
            this.SendCmd(0xAAAA);
        }

        private void _modInInputBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_IN_ON; // 2
            this.SendCmd(0x04AA);
        }

        private void _modInOutputBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_IN_OFF; // 3
            this.SendCmd(0x0455);
        }

        private void _modIn5InputBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_IN5_ON; // 4
            this.SendCmd(0x05AA);
        }

        private void _modIn5OutputBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_IN5_OFF; // 5
            this.SendCmd(0x0555);
        }

        private void _modU0inBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_U0_ON; // 6
            this.SendCmd(0x08AA);
        }

        private void _modU0outBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_U0_OFF; // 7
            this.SendCmd(0x0855);
        }

        private void _modIaInputBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_IA_ON; // 8
            this.SendCmd(0x64AA);
        }

        private void _modIaOutputBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_IA_OFF; // 9
            this.SendCmd(0x6455);
        }

        private void _modIbInputBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_IB_ON; // 10
            this.SendCmd(0x65AA);
        }

        private void _modIbOutputBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_IB_OFF; // 11
            this.SendCmd(0x6555);
        }

        private void _modIcInputBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_IC_ON; // 12
            this.SendCmd(0x66AA);
        }

        private void _modIcOutputBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_IC_OFF; // 13
            this.SendCmd(0x6655);
        }

        private void _modFInputBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_F_ON; // 14
            this.SendCmd(0x67AA);
        }

        private void _modFOutputBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_F_OFF; // 15
            this.SendCmd(0x6755);
        }

        private void _modDirTrigInpBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_DIR_ON; // 16
            this.SendCmd(0x14AA);
        }

        private void _modDirTrigOutBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_DIR_OFF; // 17
            this.SendCmd(0x1455);
        }

        private void _modOzzIputBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_ENERGY_ON; // 18
            this.SendCmd(0x15AA);  
            this._modOzz.Text = @"Энергия";
        }

        private void _modOzzOutputBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_ENERGY_OFF; // 19
            this.SendCmd(0x1555); 
            this._modOzz.Text = @"Угол";
        }

        private void _modDirTrigOneBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_DIR_1; // 20
            this.SendCmd(0x24AA);
            this._stateUstNaprSrabTB.Text = @"От шин";
        }

        private void _modDirTrigZeroBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_DIR_0; // 21
            this.SendCmd(0x2455);
            this._stateUstNaprSrabTB.Text = @"К шинам";
        }

        private void _iaImitationBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.TRIG_IA; // 22
            this.SendCmd(0x0701); 
        }

        private void _ibImitationBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.TRIG_IB; // 23
            this.SendCmd(0x0702); 
        }

        private void _icImitationBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.TRIG_IC; // 24
            this.SendCmd(0x0703); 
        }

        private void _inImitationBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.TRIG_IN; // 25
            this.SendCmd(0x0704); 
        }

        private void _in5ImitationBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.TRIG_IN5; // 26
            this.SendCmd(0x0705); 
        }

        private void _u0ImitationBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.TRIG_U0; // 27
            this.SendCmd(0x0706); 
        }

        private void _resetErrors_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.RESET_ERRORS; // 28
            this.SendCmd(0x0fff);
        }
        #endregion [FormControls Methods]

        #region [IFormView]
        public Type FormDevice => typeof(ItkzDeviceV6);
        public bool Multishow { get; }
        public Type ClassType => typeof(ItkzMeasuringForm);
        public bool ForceShow => false;
        public Image NodeImage => Framework.Properties.Resources.measuring.ToBitmap();
        public string NodeName => "Измерения";
        public INodeView[] ChildNodes => new INodeView[] { };
        public bool Deletable => false;
        #endregion [IFormView]     
    }
}