﻿namespace BEMN.ITKZ_V6.Measuring
{
    partial class ItkzMeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>


        // ВНИМАНИЕ!!! заккоментировано
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (this.components != null))
        //    {
        //        this.components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItkzMeasuringForm));
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._blinkerLed6 = new BEMN.Forms.LedControl();
            this._blinkerLed4 = new BEMN.Forms.LedControl();
            this._blinkerLed3 = new BEMN.Forms.LedControl();
            this._blinkerLed1 = new BEMN.Forms.LedControl();
            this._u0TimeStageLabel = new System.Windows.Forms.Label();
            this._in5TimeStageLabel = new System.Windows.Forms.Label();
            this._inTimeStageLabel = new System.Windows.Forms.Label();
            this._icTimeStageLabel = new System.Windows.Forms.Label();
            this._ibTimeStageLabel = new System.Windows.Forms.Label();
            this._iaTimeStageLabel = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label51 = new System.Windows.Forms.Label();
            this._lastValEnergy = new System.Windows.Forms.TextBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label49 = new System.Windows.Forms.Label();
            this._curValEnergy = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this._in5ImitationBtn = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this._in5_lastValU0 = new System.Windows.Forms.TextBox();
            this._in5Angle = new System.Windows.Forms.TextBox();
            this._in5_lastValIn5 = new System.Windows.Forms.TextBox();
            this._in5_lastValIn = new System.Windows.Forms.TextBox();
            this._directionLedIn5 = new BEMN.Forms.LedControl();
            this._dostoverLedIn5 = new BEMN.Forms.LedControl();
            this.panel9 = new System.Windows.Forms.Panel();
            this._inImitationBtn = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this._in_lastValU0 = new System.Windows.Forms.TextBox();
            this._inAngle = new System.Windows.Forms.TextBox();
            this._in_lastValIn5 = new System.Windows.Forms.TextBox();
            this._in_lastValIn = new System.Windows.Forms.TextBox();
            this._dostoverLedIn = new BEMN.Forms.LedControl();
            this._directionLedIn = new BEMN.Forms.LedControl();
            this.panel8 = new System.Windows.Forms.Panel();
            this._icImitationBtn = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this._lastValIc = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this._ibImitationBtn = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this._lastValIb = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this._iaImitationBtn = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this._lastValIa = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this._curValIn5 = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this._curValIn = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this._curValIc = new System.Windows.Forms.TextBox();
            this._blinkerLed2 = new BEMN.Forms.LedControl();
            this._blinkerLed5 = new BEMN.Forms.LedControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this._curValIb = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this._statusLedIc = new BEMN.Forms.LedControl();
            this._statusLedIb = new BEMN.Forms.LedControl();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this._statusLedIn = new BEMN.Forms.LedControl();
            this._statusLedIn5 = new BEMN.Forms.LedControl();
            this._stupenLedIa = new BEMN.Forms.LedControl();
            this._stupenLedIb = new BEMN.Forms.LedControl();
            this._stupenLedIc = new BEMN.Forms.LedControl();
            this._stupenLedIn = new BEMN.Forms.LedControl();
            this._stupenLedIn5 = new BEMN.Forms.LedControl();
            this.label10 = new System.Windows.Forms.Label();
            this._statusLedIa = new BEMN.Forms.LedControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this._curValIa = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this._u0ImitationBtn = new System.Windows.Forms.Button();
            this.label42 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this._u0_lastValU0 = new System.Windows.Forms.TextBox();
            this._u0Angle = new System.Windows.Forms.TextBox();
            this._u0_lastValIn5 = new System.Windows.Forms.TextBox();
            this._u0_lastValIn = new System.Windows.Forms.TextBox();
            this._directionLedU0 = new BEMN.Forms.LedControl();
            this._dostoverLedU0 = new BEMN.Forms.LedControl();
            this._statusLedU0 = new BEMN.Forms.LedControl();
            this._stupenLedU0 = new BEMN.Forms.LedControl();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label43 = new System.Windows.Forms.Label();
            this._curValU0 = new System.Windows.Forms.TextBox();
            this._statusLedEnergy = new BEMN.Forms.LedControl();
            this.label52 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._modInInputBtn = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this._modInOutputBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._modLedIn = new BEMN.Forms.LedControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._modU0outBtn = new System.Windows.Forms.Button();
            this._modU0inBtn = new System.Windows.Forms.Button();
            this._modLedU0 = new BEMN.Forms.LedControl();
            this.label12 = new System.Windows.Forms.Label();
            this._modIn5OutputBtn = new System.Windows.Forms.Button();
            this._modIn5InputBtn = new System.Windows.Forms.Button();
            this._kvitBtn = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._modLedIn5 = new BEMN.Forms.LedControl();
            this.label25 = new System.Windows.Forms.Label();
            this._inPhaseShift = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._modDirTrigOutBtn = new System.Windows.Forms.Button();
            this._modDirTrigInpBtn = new System.Windows.Forms.Button();
            this._modLedInOutDir = new BEMN.Forms.LedControl();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._stateUstNaprSrabTB = new System.Windows.Forms.TextBox();
            this._modDirTrigOneBtn = new System.Windows.Forms.Button();
            this._modDirTrigZeroBtn = new System.Windows.Forms.Button();
            this._modLedUstDir = new BEMN.Forms.LedControl();
            this.label47 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.groupBoxModOzz = new System.Windows.Forms.GroupBox();
            this._modOzz = new System.Windows.Forms.TextBox();
            this._modOzzOutputBtn = new System.Windows.Forms.Button();
            this._modOzzIputBtn = new System.Windows.Forms.Button();
            this._modLedOzz = new BEMN.Forms.LedControl();
            this.label59 = new System.Windows.Forms.Label();
            this._dateTimeLabel = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this._supplyVoltage = new System.Windows.Forms.TextBox();
            this._temperature = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._resetErrors = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this._infoLed5 = new BEMN.Forms.LedControl();
            this._infoLed4 = new BEMN.Forms.LedControl();
            this._infoLed3 = new BEMN.Forms.LedControl();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this._infoLed2 = new BEMN.Forms.LedControl();
            this.label62 = new System.Windows.Forms.Label();
            this._infoLed1 = new BEMN.Forms.LedControl();
            this.InfoLabel = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this._vBat = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this._u0Phase = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this._inPhase = new System.Windows.Forms.TextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._modLedIa = new BEMN.Forms.LedControl();
            this._modIaOutputBtn = new System.Windows.Forms.Button();
            this.label56 = new System.Windows.Forms.Label();
            this._modIaInputBtn = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._modLedIb = new BEMN.Forms.LedControl();
            this._modIbOutputBtn = new System.Windows.Forms.Button();
            this.label57 = new System.Windows.Forms.Label();
            this._modIbInputBtn = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._modLedIc = new BEMN.Forms.LedControl();
            this._modIcOutputBtn = new System.Windows.Forms.Button();
            this.label58 = new System.Windows.Forms.Label();
            this._modIcInputBtn = new System.Windows.Forms.Button();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this._modFOutputBtn = new System.Windows.Forms.Button();
            this.label60 = new System.Windows.Forms.Label();
            this._modFInputBtn = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBoxModOzz.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 65);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ia";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Каналы";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(161, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 39);
            this.label6.TabIndex = 0;
            this.label6.Text = "Состояние измер. органа";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 88F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 103F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 516F));
            this.tableLayoutPanel1.Controls.Add(this._blinkerLed6, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this._blinkerLed4, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this._blinkerLed3, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this._blinkerLed1, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this._u0TimeStageLabel, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this._in5TimeStageLabel, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this._inTimeStageLabel, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this._icTimeStageLabel, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this._ibTimeStageLabel, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this._iaTimeStageLabel, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel14, 6, 7);
            this.tableLayoutPanel1.Controls.Add(this.panel13, 5, 7);
            this.tableLayoutPanel1.Controls.Add(this.label48, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.panel10, 6, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel9, 6, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel8, 6, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel7, 6, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 5, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 5, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this._blinkerLed2, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this._blinkerLed5, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label16, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label67, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this._statusLedIc, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this._statusLedIb, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this._statusLedIn, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this._statusLedIn5, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this._stupenLedIa, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this._stupenLedIb, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this._stupenLedIc, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this._stupenLedIn, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this._stupenLedIn5, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.label10, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this._statusLedIa, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.label26, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.panel11, 6, 6);
            this.tableLayoutPanel1.Controls.Add(this._statusLedU0, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this._stupenLedU0, 4, 6);
            this.tableLayoutPanel1.Controls.Add(this.panel12, 5, 6);
            this.tableLayoutPanel1.Controls.Add(this._statusLedEnergy, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.label52, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 6, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.28995F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.099313F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.590179F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.590179F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.1621F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.5057F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.5057F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.256882F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(882, 445);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // _blinkerLed6
            // 
            this._blinkerLed6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._blinkerLed6.Location = new System.Drawing.Point(259, 358);
            this._blinkerLed6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._blinkerLed6.Name = "_blinkerLed6";
            this._blinkerLed6.Size = new System.Drawing.Size(13, 13);
            this._blinkerLed6.State = BEMN.Forms.LedState.Off;
            this._blinkerLed6.TabIndex = 52;
            // 
            // _blinkerLed4
            // 
            this._blinkerLed4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._blinkerLed4.Location = new System.Drawing.Point(259, 197);
            this._blinkerLed4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._blinkerLed4.Name = "_blinkerLed4";
            this._blinkerLed4.Size = new System.Drawing.Size(13, 13);
            this._blinkerLed4.State = BEMN.Forms.LedState.Off;
            this._blinkerLed4.TabIndex = 52;
            // 
            // _blinkerLed3
            // 
            this._blinkerLed3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._blinkerLed3.Location = new System.Drawing.Point(259, 138);
            this._blinkerLed3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._blinkerLed3.Name = "_blinkerLed3";
            this._blinkerLed3.Size = new System.Drawing.Size(13, 13);
            this._blinkerLed3.State = BEMN.Forms.LedState.Off;
            this._blinkerLed3.TabIndex = 52;
            // 
            // _blinkerLed1
            // 
            this._blinkerLed1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._blinkerLed1.Location = new System.Drawing.Point(259, 63);
            this._blinkerLed1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._blinkerLed1.Name = "_blinkerLed1";
            this._blinkerLed1.Size = new System.Drawing.Size(13, 13);
            this._blinkerLed1.State = BEMN.Forms.LedState.Off;
            this._blinkerLed1.TabIndex = 52;
            // 
            // _u0TimeStageLabel
            // 
            this._u0TimeStageLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._u0TimeStageLabel.AutoSize = true;
            this._u0TimeStageLabel.Location = new System.Drawing.Point(89, 357);
            this._u0TimeStageLabel.Name = "_u0TimeStageLabel";
            this._u0TimeStageLabel.Size = new System.Drawing.Size(37, 13);
            this._u0TimeStageLabel.TabIndex = 58;
            this._u0TimeStageLabel.Text = "_____";
            this._u0TimeStageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _in5TimeStageLabel
            // 
            this._in5TimeStageLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._in5TimeStageLabel.AutoSize = true;
            this._in5TimeStageLabel.Location = new System.Drawing.Point(89, 276);
            this._in5TimeStageLabel.Name = "_in5TimeStageLabel";
            this._in5TimeStageLabel.Size = new System.Drawing.Size(37, 13);
            this._in5TimeStageLabel.TabIndex = 56;
            this._in5TimeStageLabel.Text = "_____";
            this._in5TimeStageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _inTimeStageLabel
            // 
            this._inTimeStageLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._inTimeStageLabel.AutoSize = true;
            this._inTimeStageLabel.Location = new System.Drawing.Point(89, 196);
            this._inTimeStageLabel.Name = "_inTimeStageLabel";
            this._inTimeStageLabel.Size = new System.Drawing.Size(37, 13);
            this._inTimeStageLabel.TabIndex = 54;
            this._inTimeStageLabel.Text = "_____";
            this._inTimeStageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _icTimeStageLabel
            // 
            this._icTimeStageLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._icTimeStageLabel.AutoSize = true;
            this._icTimeStageLabel.Location = new System.Drawing.Point(89, 137);
            this._icTimeStageLabel.Name = "_icTimeStageLabel";
            this._icTimeStageLabel.Size = new System.Drawing.Size(37, 13);
            this._icTimeStageLabel.TabIndex = 52;
            this._icTimeStageLabel.Text = "_____";
            this._icTimeStageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _ibTimeStageLabel
            // 
            this._ibTimeStageLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._ibTimeStageLabel.AutoSize = true;
            this._ibTimeStageLabel.Location = new System.Drawing.Point(89, 99);
            this._ibTimeStageLabel.Name = "_ibTimeStageLabel";
            this._ibTimeStageLabel.Size = new System.Drawing.Size(37, 13);
            this._ibTimeStageLabel.TabIndex = 50;
            this._ibTimeStageLabel.Text = "_____";
            this._ibTimeStageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _iaTimeStageLabel
            // 
            this._iaTimeStageLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._iaTimeStageLabel.AutoSize = true;
            this._iaTimeStageLabel.Location = new System.Drawing.Point(89, 62);
            this._iaTimeStageLabel.Name = "_iaTimeStageLabel";
            this._iaTimeStageLabel.Size = new System.Drawing.Size(37, 13);
            this._iaTimeStageLabel.TabIndex = 48;
            this._iaTimeStageLabel.Text = "_____";
            this._iaTimeStageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.label51);
            this.panel14.Controls.Add(this._lastValEnergy);
            this.panel14.Location = new System.Drawing.Point(488, 408);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(394, 32);
            this.panel14.TabIndex = 44;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(113, 8);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(24, 13);
            this.label51.TabIndex = 3;
            this.label51.Text = "Дж";
            // 
            // _lastValEnergy
            // 
            this._lastValEnergy.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._lastValEnergy.Location = new System.Drawing.Point(52, 5);
            this._lastValEnergy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._lastValEnergy.Name = "_lastValEnergy";
            this._lastValEnergy.ReadOnly = true;
            this._lastValEnergy.Size = new System.Drawing.Size(55, 20);
            this._lastValEnergy.TabIndex = 2;
            this._lastValEnergy.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.label49);
            this.panel13.Controls.Add(this._curValEnergy);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(383, 408);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(97, 32);
            this.panel13.TabIndex = 42;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(69, 8);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(24, 13);
            this.label49.TabIndex = 3;
            this.label49.Text = "Дж";
            // 
            // _curValEnergy
            // 
            this._curValEnergy.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._curValEnergy.Location = new System.Drawing.Point(8, 5);
            this._curValEnergy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curValEnergy.Name = "_curValEnergy";
            this._curValEnergy.ReadOnly = true;
            this._curValEnergy.Size = new System.Drawing.Size(55, 20);
            this._curValEnergy.TabIndex = 2;
            this._curValEnergy.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label48
            // 
            this.label48.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(12, 420);
            this.label48.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(40, 13);
            this.label48.TabIndex = 39;
            this.label48.Text = "Energy";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this._in5ImitationBtn);
            this.panel10.Controls.Add(this.label41);
            this.panel10.Controls.Add(this.label24);
            this.panel10.Controls.Add(this.label31);
            this.panel10.Controls.Add(this.label32);
            this.panel10.Controls.Add(this.label33);
            this.panel10.Controls.Add(this.label34);
            this.panel10.Controls.Add(this._in5_lastValU0);
            this.panel10.Controls.Add(this._in5Angle);
            this.panel10.Controls.Add(this._in5_lastValIn5);
            this.panel10.Controls.Add(this._in5_lastValIn);
            this.panel10.Controls.Add(this._directionLedIn5);
            this.panel10.Controls.Add(this._dostoverLedIn5);
            this.panel10.Location = new System.Drawing.Point(488, 246);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(394, 73);
            this.panel10.TabIndex = 38;
            // 
            // _in5ImitationBtn
            // 
            this._in5ImitationBtn.Location = new System.Drawing.Point(289, 22);
            this._in5ImitationBtn.Name = "_in5ImitationBtn";
            this._in5ImitationBtn.Size = new System.Drawing.Size(88, 23);
            this._in5ImitationBtn.TabIndex = 39;
            this._in5ImitationBtn.Text = "Имитация In5";
            this._in5ImitationBtn.UseVisualStyleBackColor = true;
            this._in5ImitationBtn.Click += new System.EventHandler(this._in5ImitationBtn_Click);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(139, 46);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(104, 13);
            this.label41.TabIndex = 3;
            this.label41.Text = "Бит достоверности";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(139, 27);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(94, 13);
            this.label24.TabIndex = 33;
            this.label24.Text = "Бит направления";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(5, 47);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(40, 13);
            this.label31.TabIndex = 34;
            this.label31.Text = "3U0, В";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(139, 7);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(64, 13);
            this.label32.TabIndex = 35;
            this.label32.Text = "Угол, град.";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(5, 28);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(35, 13);
            this.label33.TabIndex = 36;
            this.label33.Text = "In5, A";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(5, 9);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 13);
            this.label34.TabIndex = 37;
            this.label34.Text = "In, A";
            // 
            // _in5_lastValU0
            // 
            this._in5_lastValU0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._in5_lastValU0.Location = new System.Drawing.Point(52, 44);
            this._in5_lastValU0.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._in5_lastValU0.Name = "_in5_lastValU0";
            this._in5_lastValU0.ReadOnly = true;
            this._in5_lastValU0.Size = new System.Drawing.Size(55, 20);
            this._in5_lastValU0.TabIndex = 29;
            this._in5_lastValU0.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _in5Angle
            // 
            this._in5Angle.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._in5Angle.Location = new System.Drawing.Point(215, 4);
            this._in5Angle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._in5Angle.Name = "_in5Angle";
            this._in5Angle.ReadOnly = true;
            this._in5Angle.Size = new System.Drawing.Size(55, 20);
            this._in5Angle.TabIndex = 30;
            this._in5Angle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _in5_lastValIn5
            // 
            this._in5_lastValIn5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._in5_lastValIn5.Location = new System.Drawing.Point(52, 25);
            this._in5_lastValIn5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._in5_lastValIn5.Name = "_in5_lastValIn5";
            this._in5_lastValIn5.ReadOnly = true;
            this._in5_lastValIn5.Size = new System.Drawing.Size(55, 20);
            this._in5_lastValIn5.TabIndex = 31;
            this._in5_lastValIn5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _in5_lastValIn
            // 
            this._in5_lastValIn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._in5_lastValIn.Location = new System.Drawing.Point(52, 6);
            this._in5_lastValIn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._in5_lastValIn.Name = "_in5_lastValIn";
            this._in5_lastValIn.ReadOnly = true;
            this._in5_lastValIn.Size = new System.Drawing.Size(55, 20);
            this._in5_lastValIn.TabIndex = 32;
            this._in5_lastValIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _directionLedIn5
            // 
            this._directionLedIn5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._directionLedIn5.Location = new System.Drawing.Point(257, 28);
            this._directionLedIn5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._directionLedIn5.Name = "_directionLedIn5";
            this._directionLedIn5.Size = new System.Drawing.Size(13, 13);
            this._directionLedIn5.State = BEMN.Forms.LedState.Off;
            this._directionLedIn5.TabIndex = 38;
            // 
            // _dostoverLedIn5
            // 
            this._dostoverLedIn5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._dostoverLedIn5.Location = new System.Drawing.Point(257, 46);
            this._dostoverLedIn5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._dostoverLedIn5.Name = "_dostoverLedIn5";
            this._dostoverLedIn5.Size = new System.Drawing.Size(13, 13);
            this._dostoverLedIn5.State = BEMN.Forms.LedState.Off;
            this._dostoverLedIn5.TabIndex = 28;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this._inImitationBtn);
            this.panel9.Controls.Add(this.label40);
            this.panel9.Controls.Add(this.label30);
            this.panel9.Controls.Add(this.label28);
            this.panel9.Controls.Add(this.label29);
            this.panel9.Controls.Add(this.label27);
            this.panel9.Controls.Add(this.label23);
            this.panel9.Controls.Add(this._in_lastValU0);
            this.panel9.Controls.Add(this._inAngle);
            this.panel9.Controls.Add(this._in_lastValIn5);
            this.panel9.Controls.Add(this._in_lastValIn);
            this.panel9.Controls.Add(this._dostoverLedIn);
            this.panel9.Controls.Add(this._directionLedIn);
            this.panel9.Location = new System.Drawing.Point(488, 167);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(394, 71);
            this.panel9.TabIndex = 37;
            // 
            // _inImitationBtn
            // 
            this._inImitationBtn.Location = new System.Drawing.Point(289, 21);
            this._inImitationBtn.Name = "_inImitationBtn";
            this._inImitationBtn.Size = new System.Drawing.Size(88, 23);
            this._inImitationBtn.TabIndex = 29;
            this._inImitationBtn.Text = "Имитация In";
            this._inImitationBtn.UseVisualStyleBackColor = true;
            this._inImitationBtn.Click += new System.EventHandler(this._inImitationBtn_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(139, 47);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(104, 13);
            this.label40.TabIndex = 3;
            this.label40.Text = "Бит достоверности";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(139, 27);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(94, 13);
            this.label30.TabIndex = 3;
            this.label30.Text = "Бит направления";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(5, 47);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(40, 13);
            this.label28.TabIndex = 3;
            this.label28.Text = "3U0, В";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(139, 7);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(64, 13);
            this.label29.TabIndex = 3;
            this.label29.Text = "Угол, град.";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(5, 28);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(35, 13);
            this.label27.TabIndex = 3;
            this.label27.Text = "In5, A";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(5, 8);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(29, 13);
            this.label23.TabIndex = 3;
            this.label23.Text = "In, A";
            // 
            // _in_lastValU0
            // 
            this._in_lastValU0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._in_lastValU0.Location = new System.Drawing.Point(52, 44);
            this._in_lastValU0.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._in_lastValU0.Name = "_in_lastValU0";
            this._in_lastValU0.ReadOnly = true;
            this._in_lastValU0.Size = new System.Drawing.Size(55, 20);
            this._in_lastValU0.TabIndex = 2;
            this._in_lastValU0.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _inAngle
            // 
            this._inAngle.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._inAngle.Location = new System.Drawing.Point(213, 4);
            this._inAngle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._inAngle.Name = "_inAngle";
            this._inAngle.ReadOnly = true;
            this._inAngle.Size = new System.Drawing.Size(55, 20);
            this._inAngle.TabIndex = 2;
            this._inAngle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _in_lastValIn5
            // 
            this._in_lastValIn5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._in_lastValIn5.Location = new System.Drawing.Point(52, 25);
            this._in_lastValIn5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._in_lastValIn5.Name = "_in_lastValIn5";
            this._in_lastValIn5.ReadOnly = true;
            this._in_lastValIn5.Size = new System.Drawing.Size(55, 20);
            this._in_lastValIn5.TabIndex = 2;
            this._in_lastValIn5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _in_lastValIn
            // 
            this._in_lastValIn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._in_lastValIn.Location = new System.Drawing.Point(52, 6);
            this._in_lastValIn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._in_lastValIn.Name = "_in_lastValIn";
            this._in_lastValIn.ReadOnly = true;
            this._in_lastValIn.Size = new System.Drawing.Size(55, 20);
            this._in_lastValIn.TabIndex = 2;
            this._in_lastValIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _dostoverLedIn
            // 
            this._dostoverLedIn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._dostoverLedIn.Location = new System.Drawing.Point(255, 47);
            this._dostoverLedIn.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._dostoverLedIn.Name = "_dostoverLedIn";
            this._dostoverLedIn.Size = new System.Drawing.Size(13, 13);
            this._dostoverLedIn.State = BEMN.Forms.LedState.Off;
            this._dostoverLedIn.TabIndex = 28;
            // 
            // _directionLedIn
            // 
            this._directionLedIn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._directionLedIn.Location = new System.Drawing.Point(255, 28);
            this._directionLedIn.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._directionLedIn.Name = "_directionLedIn";
            this._directionLedIn.Size = new System.Drawing.Size(13, 13);
            this._directionLedIn.State = BEMN.Forms.LedState.Off;
            this._directionLedIn.TabIndex = 28;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this._icImitationBtn);
            this.panel8.Controls.Add(this.label22);
            this.panel8.Controls.Add(this._lastValIc);
            this.panel8.Location = new System.Drawing.Point(488, 129);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(394, 30);
            this.panel8.TabIndex = 36;
            // 
            // _icImitationBtn
            // 
            this._icImitationBtn.Location = new System.Drawing.Point(289, 1);
            this._icImitationBtn.Name = "_icImitationBtn";
            this._icImitationBtn.Size = new System.Drawing.Size(88, 23);
            this._icImitationBtn.TabIndex = 5;
            this._icImitationBtn.Text = "Имитация Ic";
            this._icImitationBtn.UseVisualStyleBackColor = true;
            this._icImitationBtn.Click += new System.EventHandler(this._icImitationBtn_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(113, 6);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(14, 13);
            this.label22.TabIndex = 3;
            this.label22.Text = "А";
            // 
            // _lastValIc
            // 
            this._lastValIc.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._lastValIc.Location = new System.Drawing.Point(52, 4);
            this._lastValIc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._lastValIc.Name = "_lastValIc";
            this._lastValIc.ReadOnly = true;
            this._lastValIc.Size = new System.Drawing.Size(55, 20);
            this._lastValIc.TabIndex = 2;
            this._lastValIc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this._ibImitationBtn);
            this.panel7.Controls.Add(this.label21);
            this.panel7.Controls.Add(this._lastValIb);
            this.panel7.Location = new System.Drawing.Point(488, 91);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(394, 30);
            this.panel7.TabIndex = 35;
            // 
            // _ibImitationBtn
            // 
            this._ibImitationBtn.Location = new System.Drawing.Point(289, 3);
            this._ibImitationBtn.Name = "_ibImitationBtn";
            this._ibImitationBtn.Size = new System.Drawing.Size(88, 23);
            this._ibImitationBtn.TabIndex = 5;
            this._ibImitationBtn.Text = "Имитация Ib";
            this._ibImitationBtn.UseVisualStyleBackColor = true;
            this._ibImitationBtn.Click += new System.EventHandler(this._ibImitationBtn_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(113, 7);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(14, 13);
            this.label21.TabIndex = 3;
            this.label21.Text = "А";
            // 
            // _lastValIb
            // 
            this._lastValIb.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._lastValIb.Location = new System.Drawing.Point(52, 5);
            this._lastValIb.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._lastValIb.Name = "_lastValIb";
            this._lastValIb.ReadOnly = true;
            this._lastValIb.Size = new System.Drawing.Size(55, 20);
            this._lastValIb.TabIndex = 2;
            this._lastValIb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this._iaImitationBtn);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this._lastValIa);
            this.panel6.Location = new System.Drawing.Point(488, 55);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(394, 28);
            this.panel6.TabIndex = 34;
            // 
            // _iaImitationBtn
            // 
            this._iaImitationBtn.Location = new System.Drawing.Point(289, 2);
            this._iaImitationBtn.Name = "_iaImitationBtn";
            this._iaImitationBtn.Size = new System.Drawing.Size(88, 23);
            this._iaImitationBtn.TabIndex = 4;
            this._iaImitationBtn.Text = "Имитация Ia";
            this._iaImitationBtn.UseVisualStyleBackColor = true;
            this._iaImitationBtn.Click += new System.EventHandler(this._iaImitationBtn_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(113, 7);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(14, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "А";
            // 
            // _lastValIa
            // 
            this._lastValIa.Location = new System.Drawing.Point(52, 3);
            this._lastValIa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._lastValIa.Name = "_lastValIa";
            this._lastValIa.ReadOnly = true;
            this._lastValIa.Size = new System.Drawing.Size(55, 20);
            this._lastValIa.TabIndex = 2;
            this._lastValIa.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this._curValIn5);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(383, 246);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(97, 73);
            this.panel5.TabIndex = 33;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(69, 27);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(14, 13);
            this.label19.TabIndex = 3;
            this.label19.Text = "A";
            // 
            // _curValIn5
            // 
            this._curValIn5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._curValIn5.Location = new System.Drawing.Point(8, 25);
            this._curValIn5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curValIn5.Name = "_curValIn5";
            this._curValIn5.ReadOnly = true;
            this._curValIn5.Size = new System.Drawing.Size(55, 20);
            this._curValIn5.TabIndex = 2;
            this._curValIn5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this._curValIn);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(383, 167);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(97, 71);
            this.panel4.TabIndex = 32;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(69, 27);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(14, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "A";
            // 
            // _curValIn
            // 
            this._curValIn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._curValIn.Location = new System.Drawing.Point(8, 24);
            this._curValIn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curValIn.Name = "_curValIn";
            this._curValIn.ReadOnly = true;
            this._curValIn.Size = new System.Drawing.Size(55, 20);
            this._curValIn.TabIndex = 2;
            this._curValIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this._curValIc);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(383, 129);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(97, 30);
            this.panel3.TabIndex = 31;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(69, 8);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(14, 13);
            this.label17.TabIndex = 3;
            this.label17.Text = "А";
            // 
            // _curValIc
            // 
            this._curValIc.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._curValIc.Location = new System.Drawing.Point(8, 5);
            this._curValIc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curValIc.Name = "_curValIc";
            this._curValIc.ReadOnly = true;
            this._curValIc.Size = new System.Drawing.Size(55, 20);
            this._curValIc.TabIndex = 2;
            this._curValIc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _blinkerLed2
            // 
            this._blinkerLed2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._blinkerLed2.Location = new System.Drawing.Point(259, 100);
            this._blinkerLed2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._blinkerLed2.Name = "_blinkerLed2";
            this._blinkerLed2.Size = new System.Drawing.Size(13, 13);
            this._blinkerLed2.State = BEMN.Forms.LedState.Off;
            this._blinkerLed2.TabIndex = 52;
            // 
            // _blinkerLed5
            // 
            this._blinkerLed5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._blinkerLed5.Location = new System.Drawing.Point(259, 277);
            this._blinkerLed5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._blinkerLed5.Name = "_blinkerLed5";
            this._blinkerLed5.Size = new System.Drawing.Size(13, 13);
            this._blinkerLed5.State = BEMN.Forms.LedState.Off;
            this._blinkerLed5.TabIndex = 52;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this._curValIb);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(383, 91);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(97, 30);
            this.panel2.TabIndex = 30;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(69, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "А";
            // 
            // _curValIb
            // 
            this._curValIb.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._curValIb.Location = new System.Drawing.Point(8, 5);
            this._curValIb.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curValIb.Name = "_curValIb";
            this._curValIb.ReadOnly = true;
            this._curValIb.Size = new System.Drawing.Size(55, 20);
            this._curValIb.TabIndex = 2;
            this._curValIb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 278);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "In5";
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(24, 102);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(16, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Ib";
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(24, 140);
            this.label16.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(16, 13);
            this.label16.TabIndex = 5;
            this.label16.Text = "Ic";
            // 
            // label67
            // 
            this.label67.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label67.Location = new System.Drawing.Point(239, 8);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(53, 35);
            this.label67.TabIndex = 52;
            this.label67.Text = "Блинкер аварий";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _statusLedIc
            // 
            this._statusLedIc.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLedIc.Location = new System.Drawing.Point(185, 138);
            this._statusLedIc.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLedIc.Name = "_statusLedIc";
            this._statusLedIc.Size = new System.Drawing.Size(13, 13);
            this._statusLedIc.State = BEMN.Forms.LedState.Off;
            this._statusLedIc.TabIndex = 6;
            // 
            // _statusLedIb
            // 
            this._statusLedIb.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLedIb.Location = new System.Drawing.Point(185, 100);
            this._statusLedIb.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLedIb.Name = "_statusLedIb";
            this._statusLedIb.Size = new System.Drawing.Size(13, 13);
            this._statusLedIb.State = BEMN.Forms.LedState.Off;
            this._statusLedIb.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(385, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Текущ. значение";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 198);
            this.label9.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(16, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "In";
            // 
            // _statusLedIn
            // 
            this._statusLedIn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLedIn.Location = new System.Drawing.Point(185, 197);
            this._statusLedIn.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLedIn.Name = "_statusLedIn";
            this._statusLedIn.Size = new System.Drawing.Size(13, 13);
            this._statusLedIn.State = BEMN.Forms.LedState.Off;
            this._statusLedIn.TabIndex = 18;
            // 
            // _statusLedIn5
            // 
            this._statusLedIn5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLedIn5.Location = new System.Drawing.Point(185, 277);
            this._statusLedIn5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLedIn5.Name = "_statusLedIn5";
            this._statusLedIn5.Size = new System.Drawing.Size(13, 13);
            this._statusLedIn5.State = BEMN.Forms.LedState.Off;
            this._statusLedIn5.TabIndex = 17;
            // 
            // _stupenLedIa
            // 
            this._stupenLedIa.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._stupenLedIa.Location = new System.Drawing.Point(334, 63);
            this._stupenLedIa.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._stupenLedIa.Name = "_stupenLedIa";
            this._stupenLedIa.Size = new System.Drawing.Size(13, 13);
            this._stupenLedIa.State = BEMN.Forms.LedState.Off;
            this._stupenLedIa.TabIndex = 24;
            // 
            // _stupenLedIb
            // 
            this._stupenLedIb.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._stupenLedIb.Location = new System.Drawing.Point(334, 100);
            this._stupenLedIb.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._stupenLedIb.Name = "_stupenLedIb";
            this._stupenLedIb.Size = new System.Drawing.Size(13, 13);
            this._stupenLedIb.State = BEMN.Forms.LedState.Off;
            this._stupenLedIb.TabIndex = 25;
            // 
            // _stupenLedIc
            // 
            this._stupenLedIc.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._stupenLedIc.Location = new System.Drawing.Point(334, 138);
            this._stupenLedIc.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._stupenLedIc.Name = "_stupenLedIc";
            this._stupenLedIc.Size = new System.Drawing.Size(13, 13);
            this._stupenLedIc.State = BEMN.Forms.LedState.Off;
            this._stupenLedIc.TabIndex = 26;
            // 
            // _stupenLedIn
            // 
            this._stupenLedIn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._stupenLedIn.Location = new System.Drawing.Point(334, 197);
            this._stupenLedIn.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._stupenLedIn.Name = "_stupenLedIn";
            this._stupenLedIn.Size = new System.Drawing.Size(13, 13);
            this._stupenLedIn.State = BEMN.Forms.LedState.Off;
            this._stupenLedIn.TabIndex = 27;
            // 
            // _stupenLedIn5
            // 
            this._stupenLedIn5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._stupenLedIn5.Location = new System.Drawing.Point(334, 277);
            this._stupenLedIn5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._stupenLedIn5.Name = "_stupenLedIn5";
            this._stupenLedIn5.Size = new System.Drawing.Size(13, 13);
            this._stupenLedIn5.State = BEMN.Forms.LedState.Off;
            this._stupenLedIn5.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(310, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 39);
            this.label10.TabIndex = 23;
            this.label10.Text = "Состояние ступени индикации";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _statusLedIa
            // 
            this._statusLedIa.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLedIa.Location = new System.Drawing.Point(185, 63);
            this._statusLedIa.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLedIa.Name = "_statusLedIa";
            this._statusLedIa.Size = new System.Drawing.Size(13, 13);
            this._statusLedIa.State = BEMN.Forms.LedState.Off;
            this._statusLedIa.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this._curValIa);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(383, 55);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(97, 28);
            this.panel1.TabIndex = 29;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(69, 6);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "А";
            // 
            // _curValIa
            // 
            this._curValIa.Location = new System.Drawing.Point(8, 3);
            this._curValIa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curValIa.Name = "_curValIa";
            this._curValIa.ReadOnly = true;
            this._curValIa.Size = new System.Drawing.Size(55, 20);
            this._curValIa.TabIndex = 2;
            this._curValIa.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(18, 359);
            this.label26.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(27, 13);
            this.label26.TabIndex = 15;
            this.label26.Text = "3U0";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this._u0ImitationBtn);
            this.panel11.Controls.Add(this.label42);
            this.panel11.Controls.Add(this.label35);
            this.panel11.Controls.Add(this.label36);
            this.panel11.Controls.Add(this.label37);
            this.panel11.Controls.Add(this.label38);
            this.panel11.Controls.Add(this.label39);
            this.panel11.Controls.Add(this._u0_lastValU0);
            this.panel11.Controls.Add(this._u0Angle);
            this.panel11.Controls.Add(this._u0_lastValIn5);
            this.panel11.Controls.Add(this._u0_lastValIn);
            this.panel11.Controls.Add(this._directionLedU0);
            this.panel11.Controls.Add(this._dostoverLedU0);
            this.panel11.Location = new System.Drawing.Point(488, 327);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(394, 73);
            this.panel11.TabIndex = 38;
            // 
            // _u0ImitationBtn
            // 
            this._u0ImitationBtn.Location = new System.Drawing.Point(289, 23);
            this._u0ImitationBtn.Name = "_u0ImitationBtn";
            this._u0ImitationBtn.Size = new System.Drawing.Size(88, 23);
            this._u0ImitationBtn.TabIndex = 39;
            this._u0ImitationBtn.Text = "Имитация U0";
            this._u0ImitationBtn.UseVisualStyleBackColor = true;
            this._u0ImitationBtn.Click += new System.EventHandler(this._u0ImitationBtn_Click);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(139, 48);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(104, 13);
            this.label42.TabIndex = 3;
            this.label42.Text = "Бит достоверности";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(139, 28);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(94, 13);
            this.label35.TabIndex = 33;
            this.label35.Text = "Бит направления";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(3, 47);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(40, 13);
            this.label36.TabIndex = 34;
            this.label36.Text = "3U0, В";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(139, 8);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(64, 13);
            this.label37.TabIndex = 35;
            this.label37.Text = "Угол, град.";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(3, 28);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(35, 13);
            this.label38.TabIndex = 36;
            this.label38.Text = "In5, A";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(3, 8);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(29, 13);
            this.label39.TabIndex = 37;
            this.label39.Text = "In, A";
            // 
            // _u0_lastValU0
            // 
            this._u0_lastValU0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._u0_lastValU0.Location = new System.Drawing.Point(52, 43);
            this._u0_lastValU0.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._u0_lastValU0.Name = "_u0_lastValU0";
            this._u0_lastValU0.ReadOnly = true;
            this._u0_lastValU0.Size = new System.Drawing.Size(55, 20);
            this._u0_lastValU0.TabIndex = 29;
            this._u0_lastValU0.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _u0Angle
            // 
            this._u0Angle.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._u0Angle.Location = new System.Drawing.Point(215, 5);
            this._u0Angle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._u0Angle.Name = "_u0Angle";
            this._u0Angle.ReadOnly = true;
            this._u0Angle.Size = new System.Drawing.Size(55, 20);
            this._u0Angle.TabIndex = 30;
            this._u0Angle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _u0_lastValIn5
            // 
            this._u0_lastValIn5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._u0_lastValIn5.Location = new System.Drawing.Point(52, 24);
            this._u0_lastValIn5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._u0_lastValIn5.Name = "_u0_lastValIn5";
            this._u0_lastValIn5.ReadOnly = true;
            this._u0_lastValIn5.Size = new System.Drawing.Size(55, 20);
            this._u0_lastValIn5.TabIndex = 31;
            this._u0_lastValIn5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _u0_lastValIn
            // 
            this._u0_lastValIn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._u0_lastValIn.Location = new System.Drawing.Point(52, 5);
            this._u0_lastValIn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._u0_lastValIn.Name = "_u0_lastValIn";
            this._u0_lastValIn.ReadOnly = true;
            this._u0_lastValIn.Size = new System.Drawing.Size(55, 20);
            this._u0_lastValIn.TabIndex = 32;
            this._u0_lastValIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _directionLedU0
            // 
            this._directionLedU0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._directionLedU0.Location = new System.Drawing.Point(257, 30);
            this._directionLedU0.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._directionLedU0.Name = "_directionLedU0";
            this._directionLedU0.Size = new System.Drawing.Size(13, 13);
            this._directionLedU0.State = BEMN.Forms.LedState.Off;
            this._directionLedU0.TabIndex = 38;
            // 
            // _dostoverLedU0
            // 
            this._dostoverLedU0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._dostoverLedU0.Location = new System.Drawing.Point(257, 49);
            this._dostoverLedU0.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._dostoverLedU0.Name = "_dostoverLedU0";
            this._dostoverLedU0.Size = new System.Drawing.Size(13, 13);
            this._dostoverLedU0.State = BEMN.Forms.LedState.Off;
            this._dostoverLedU0.TabIndex = 28;
            // 
            // _statusLedU0
            // 
            this._statusLedU0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLedU0.Location = new System.Drawing.Point(185, 358);
            this._statusLedU0.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLedU0.Name = "_statusLedU0";
            this._statusLedU0.Size = new System.Drawing.Size(13, 13);
            this._statusLedU0.State = BEMN.Forms.LedState.Off;
            this._statusLedU0.TabIndex = 17;
            // 
            // _stupenLedU0
            // 
            this._stupenLedU0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._stupenLedU0.Location = new System.Drawing.Point(334, 358);
            this._stupenLedU0.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._stupenLedU0.Name = "_stupenLedU0";
            this._stupenLedU0.Size = new System.Drawing.Size(13, 13);
            this._stupenLedU0.State = BEMN.Forms.LedState.Off;
            this._stupenLedU0.TabIndex = 28;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.label43);
            this.panel12.Controls.Add(this._curValU0);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(383, 327);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(97, 73);
            this.panel12.TabIndex = 33;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(69, 30);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(14, 13);
            this.label43.TabIndex = 3;
            this.label43.Text = "V";
            // 
            // _curValU0
            // 
            this._curValU0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._curValU0.Location = new System.Drawing.Point(8, 25);
            this._curValU0.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curValU0.Name = "_curValU0";
            this._curValU0.ReadOnly = true;
            this._curValU0.Size = new System.Drawing.Size(55, 20);
            this._curValU0.TabIndex = 2;
            this._curValU0.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _statusLedEnergy
            // 
            this._statusLedEnergy.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLedEnergy.Location = new System.Drawing.Point(185, 418);
            this._statusLedEnergy.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLedEnergy.Name = "_statusLedEnergy";
            this._statusLedEnergy.Size = new System.Drawing.Size(13, 13);
            this._statusLedEnergy.State = BEMN.Forms.LedState.Off;
            this._statusLedEnergy.TabIndex = 40;
            // 
            // label52
            // 
            this.label52.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(68, 6);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(80, 39);
            this.label52.TabIndex = 45;
            this.label52.Text = "Время срабатывания ступени";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.Location = new System.Drawing.Point(610, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(265, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Значение при последнем срабатывании";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _modInInputBtn
            // 
            this._modInInputBtn.Location = new System.Drawing.Point(9, 30);
            this._modInInputBtn.Name = "_modInInputBtn";
            this._modInInputBtn.Size = new System.Drawing.Size(67, 26);
            this._modInInputBtn.TabIndex = 16;
            this._modInInputBtn.Text = "Ввести";
            this._modInInputBtn.UseVisualStyleBackColor = true;
            this._modInInputBtn.Click += new System.EventHandler(this._modInInputBtn_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 14);
            this.label11.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(154, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Режим ступени индикации In";
            // 
            // _modInOutputBtn
            // 
            this._modInOutputBtn.Location = new System.Drawing.Point(82, 30);
            this._modInOutputBtn.Name = "_modInOutputBtn";
            this._modInOutputBtn.Size = new System.Drawing.Size(67, 26);
            this._modInOutputBtn.TabIndex = 17;
            this._modInOutputBtn.Text = "Вывести";
            this._modInOutputBtn.UseVisualStyleBackColor = true;
            this._modInOutputBtn.Click += new System.EventHandler(this._modInOutputBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._modLedIn);
            this.groupBox1.Controls.Add(this._modInOutputBtn);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this._modInInputBtn);
            this.groupBox1.Location = new System.Drawing.Point(5, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(186, 60);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            // 
            // _modLedIn
            // 
            this._modLedIn.Location = new System.Drawing.Point(167, 14);
            this._modLedIn.Name = "_modLedIn";
            this._modLedIn.Size = new System.Drawing.Size(13, 13);
            this._modLedIn.State = BEMN.Forms.LedState.Off;
            this._modLedIn.TabIndex = 10;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._modU0outBtn);
            this.groupBox2.Controls.Add(this._modU0inBtn);
            this.groupBox2.Controls.Add(this._modLedU0);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(12, 658);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(191, 73);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            // 
            // _modU0outBtn
            // 
            this._modU0outBtn.Location = new System.Drawing.Point(82, 35);
            this._modU0outBtn.Name = "_modU0outBtn";
            this._modU0outBtn.Size = new System.Drawing.Size(67, 26);
            this._modU0outBtn.TabIndex = 19;
            this._modU0outBtn.Text = "Вывести";
            this._modU0outBtn.UseVisualStyleBackColor = true;
            this._modU0outBtn.Click += new System.EventHandler(this._modU0outBtn_Click);
            // 
            // _modU0inBtn
            // 
            this._modU0inBtn.Location = new System.Drawing.Point(9, 35);
            this._modU0inBtn.Name = "_modU0inBtn";
            this._modU0inBtn.Size = new System.Drawing.Size(67, 26);
            this._modU0inBtn.TabIndex = 18;
            this._modU0inBtn.Text = "Ввести";
            this._modU0inBtn.UseVisualStyleBackColor = true;
            this._modU0inBtn.Click += new System.EventHandler(this._modU0inBtn_Click);
            // 
            // _modLedU0
            // 
            this._modLedU0.Location = new System.Drawing.Point(171, 19);
            this._modLedU0.Name = "_modLedU0";
            this._modLedU0.Size = new System.Drawing.Size(13, 13);
            this._modLedU0.State = BEMN.Forms.LedState.Off;
            this._modLedU0.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 19);
            this.label12.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(159, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Режим ступени индикации U0";
            // 
            // _modIn5OutputBtn
            // 
            this._modIn5OutputBtn.Location = new System.Drawing.Point(82, 35);
            this._modIn5OutputBtn.Name = "_modIn5OutputBtn";
            this._modIn5OutputBtn.Size = new System.Drawing.Size(67, 26);
            this._modIn5OutputBtn.TabIndex = 17;
            this._modIn5OutputBtn.Text = "Вывести";
            this._modIn5OutputBtn.UseVisualStyleBackColor = true;
            this._modIn5OutputBtn.Click += new System.EventHandler(this._modIn5OutputBtn_Click);
            // 
            // _modIn5InputBtn
            // 
            this._modIn5InputBtn.Location = new System.Drawing.Point(9, 35);
            this._modIn5InputBtn.Name = "_modIn5InputBtn";
            this._modIn5InputBtn.Size = new System.Drawing.Size(67, 26);
            this._modIn5InputBtn.TabIndex = 16;
            this._modIn5InputBtn.Text = "Ввести";
            this._modIn5InputBtn.UseVisualStyleBackColor = true;
            this._modIn5InputBtn.Click += new System.EventHandler(this._modIn5InputBtn_Click);
            // 
            // _kvitBtn
            // 
            this._kvitBtn.Location = new System.Drawing.Point(790, 461);
            this._kvitBtn.Name = "_kvitBtn";
            this._kvitBtn.Size = new System.Drawing.Size(95, 26);
            this._kvitBtn.TabIndex = 17;
            this._kvitBtn.Text = "Квитирование";
            this._kvitBtn.UseVisualStyleBackColor = true;
            this._kvitBtn.Click += new System.EventHandler(this._kvitBtn_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._modLedIn5);
            this.groupBox4.Controls.Add(this._modIn5OutputBtn);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this._modIn5InputBtn);
            this.groupBox4.Location = new System.Drawing.Point(12, 579);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(191, 73);
            this.groupBox4.TabIndex = 25;
            this.groupBox4.TabStop = false;
            // 
            // _modLedIn5
            // 
            this._modLedIn5.Location = new System.Drawing.Point(172, 19);
            this._modLedIn5.Name = "_modLedIn5";
            this._modLedIn5.Size = new System.Drawing.Size(13, 13);
            this._modLedIn5.State = BEMN.Forms.LedState.Off;
            this._modLedIn5.TabIndex = 10;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 19);
            this.label25.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(160, 13);
            this.label25.TabIndex = 3;
            this.label25.Text = "Режим ступени индикации In5";
            // 
            // _inPhaseShift
            // 
            this._inPhaseShift.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._inPhaseShift.Location = new System.Drawing.Point(640, 465);
            this._inPhaseShift.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._inPhaseShift.Name = "_inPhaseShift";
            this._inPhaseShift.ReadOnly = true;
            this._inPhaseShift.Size = new System.Drawing.Size(55, 20);
            this._inPhaseShift.TabIndex = 2;
            this._inPhaseShift.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(701, 467);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(49, 13);
            this.label44.TabIndex = 3;
            this.label44.Text = "градусы";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(518, 467);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(116, 13);
            this.label45.TabIndex = 3;
            this.label45.Text = "Разность фаз U0 и In";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._modDirTrigOutBtn);
            this.groupBox5.Controls.Add(this._modDirTrigInpBtn);
            this.groupBox5.Controls.Add(this._modLedInOutDir);
            this.groupBox5.Controls.Add(this.label46);
            this.groupBox5.Location = new System.Drawing.Point(197, 14);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(185, 60);
            this.groupBox5.TabIndex = 27;
            this.groupBox5.TabStop = false;
            // 
            // _modDirTrigOutBtn
            // 
            this._modDirTrigOutBtn.Location = new System.Drawing.Point(82, 30);
            this._modDirTrigOutBtn.Name = "_modDirTrigOutBtn";
            this._modDirTrigOutBtn.Size = new System.Drawing.Size(67, 26);
            this._modDirTrigOutBtn.TabIndex = 19;
            this._modDirTrigOutBtn.Text = "Вывести";
            this._modDirTrigOutBtn.UseVisualStyleBackColor = true;
            this._modDirTrigOutBtn.Click += new System.EventHandler(this._modDirTrigOutBtn_Click);
            // 
            // _modDirTrigInpBtn
            // 
            this._modDirTrigInpBtn.Location = new System.Drawing.Point(9, 30);
            this._modDirTrigInpBtn.Name = "_modDirTrigInpBtn";
            this._modDirTrigInpBtn.Size = new System.Drawing.Size(67, 26);
            this._modDirTrigInpBtn.TabIndex = 18;
            this._modDirTrigInpBtn.Text = "Ввести";
            this._modDirTrigInpBtn.UseVisualStyleBackColor = true;
            this._modDirTrigInpBtn.Click += new System.EventHandler(this._modDirTrigInpBtn_Click);
            // 
            // _modLedInOutDir
            // 
            this._modLedInOutDir.Location = new System.Drawing.Point(164, 14);
            this._modLedInOutDir.Name = "_modLedInOutDir";
            this._modLedInOutDir.Size = new System.Drawing.Size(13, 13);
            this._modLedInOutDir.State = BEMN.Forms.LedState.Off;
            this._modLedInOutDir.TabIndex = 10;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(6, 14);
            this.label46.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(152, 13);
            this.label46.TabIndex = 3;
            this.label46.Text = "Режим направленного сраб.";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._stateUstNaprSrabTB);
            this.groupBox6.Controls.Add(this._modDirTrigOneBtn);
            this.groupBox6.Controls.Add(this._modDirTrigZeroBtn);
            this.groupBox6.Controls.Add(this._modLedUstDir);
            this.groupBox6.Controls.Add(this.label47);
            this.groupBox6.Location = new System.Drawing.Point(665, 14);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(212, 60);
            this.groupBox6.TabIndex = 28;
            this.groupBox6.TabStop = false;
            // 
            // _stateUstNaprSrabTB
            // 
            this._stateUstNaprSrabTB.Location = new System.Drawing.Point(157, 11);
            this._stateUstNaprSrabTB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._stateUstNaprSrabTB.Name = "_stateUstNaprSrabTB";
            this._stateUstNaprSrabTB.ReadOnly = true;
            this._stateUstNaprSrabTB.Size = new System.Drawing.Size(51, 20);
            this._stateUstNaprSrabTB.TabIndex = 20;
            this._stateUstNaprSrabTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _modDirTrigOneBtn
            // 
            this._modDirTrigOneBtn.Location = new System.Drawing.Point(82, 30);
            this._modDirTrigOneBtn.Name = "_modDirTrigOneBtn";
            this._modDirTrigOneBtn.Size = new System.Drawing.Size(67, 26);
            this._modDirTrigOneBtn.TabIndex = 19;
            this._modDirTrigOneBtn.Text = "От шин";
            this._modDirTrigOneBtn.UseVisualStyleBackColor = true;
            this._modDirTrigOneBtn.Click += new System.EventHandler(this._modDirTrigOneBtn_Click);
            // 
            // _modDirTrigZeroBtn
            // 
            this._modDirTrigZeroBtn.Location = new System.Drawing.Point(9, 30);
            this._modDirTrigZeroBtn.Name = "_modDirTrigZeroBtn";
            this._modDirTrigZeroBtn.Size = new System.Drawing.Size(67, 26);
            this._modDirTrigZeroBtn.TabIndex = 18;
            this._modDirTrigZeroBtn.Text = "К шинам";
            this._modDirTrigZeroBtn.UseVisualStyleBackColor = true;
            this._modDirTrigZeroBtn.Click += new System.EventHandler(this._modDirTrigZeroBtn_Click);
            // 
            // _modLedUstDir
            // 
            this._modLedUstDir.Location = new System.Drawing.Point(195, 37);
            this._modLedUstDir.Name = "_modLedUstDir";
            this._modLedUstDir.Size = new System.Drawing.Size(13, 13);
            this._modLedUstDir.State = BEMN.Forms.LedState.Off;
            this._modLedUstDir.TabIndex = 10;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(6, 14);
            this.label47.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(145, 13);
            this.label47.TabIndex = 3;
            this.label47.Text = "Флаг направленного сраб.";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.groupBoxModOzz);
            this.groupBox7.Controls.Add(this.groupBox1);
            this.groupBox7.Controls.Add(this.groupBox6);
            this.groupBox7.Controls.Add(this.groupBox5);
            this.groupBox7.Location = new System.Drawing.Point(12, 493);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(882, 80);
            this.groupBox7.TabIndex = 29;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Ступень In";
            // 
            // groupBoxModOzz
            // 
            this.groupBoxModOzz.Controls.Add(this._modOzz);
            this.groupBoxModOzz.Controls.Add(this._modOzzOutputBtn);
            this.groupBoxModOzz.Controls.Add(this._modOzzIputBtn);
            this.groupBoxModOzz.Controls.Add(this._modLedOzz);
            this.groupBoxModOzz.Controls.Add(this.label59);
            this.groupBoxModOzz.Location = new System.Drawing.Point(388, 14);
            this.groupBoxModOzz.Name = "groupBoxModOzz";
            this.groupBoxModOzz.Size = new System.Drawing.Size(271, 60);
            this.groupBoxModOzz.TabIndex = 29;
            this.groupBoxModOzz.TabStop = false;
            // 
            // _modOzz
            // 
            this._modOzz.Location = new System.Drawing.Point(217, 11);
            this._modOzz.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._modOzz.Name = "_modOzz";
            this._modOzz.ReadOnly = true;
            this._modOzz.Size = new System.Drawing.Size(50, 20);
            this._modOzz.TabIndex = 21;
            this._modOzz.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _modOzzOutputBtn
            // 
            this._modOzzOutputBtn.Location = new System.Drawing.Point(82, 30);
            this._modOzzOutputBtn.Name = "_modOzzOutputBtn";
            this._modOzzOutputBtn.Size = new System.Drawing.Size(67, 26);
            this._modOzzOutputBtn.TabIndex = 19;
            this._modOzzOutputBtn.Text = "Угол";
            this._modOzzOutputBtn.UseVisualStyleBackColor = true;
            this._modOzzOutputBtn.Click += new System.EventHandler(this._modOzzOutputBtn_Click);
            // 
            // _modOzzIputBtn
            // 
            this._modOzzIputBtn.Location = new System.Drawing.Point(9, 30);
            this._modOzzIputBtn.Name = "_modOzzIputBtn";
            this._modOzzIputBtn.Size = new System.Drawing.Size(67, 26);
            this._modOzzIputBtn.TabIndex = 18;
            this._modOzzIputBtn.Text = "Энергия";
            this._modOzzIputBtn.UseVisualStyleBackColor = true;
            this._modOzzIputBtn.Click += new System.EventHandler(this._modOzzIputBtn_Click);
            // 
            // _modLedOzz
            // 
            this._modLedOzz.Location = new System.Drawing.Point(254, 37);
            this._modLedOzz.Name = "_modLedOzz";
            this._modLedOzz.Size = new System.Drawing.Size(13, 13);
            this._modLedOzz.State = BEMN.Forms.LedState.Off;
            this._modLedOzz.TabIndex = 10;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(6, 14);
            this.label59.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(205, 13);
            this.label59.TabIndex = 3;
            this.label59.Text = "Режим определения направления ОЗЗ";
            // 
            // _dateTimeLabel
            // 
            this._dateTimeLabel.AutoSize = true;
            this._dateTimeLabel.Location = new System.Drawing.Point(50, 25);
            this._dateTimeLabel.Name = "_dateTimeLabel";
            this._dateTimeLabel.Size = new System.Drawing.Size(115, 13);
            this._dateTimeLabel.TabIndex = 30;
            this._dateTimeLabel.Text = "00.00.0000 + 00:00:00";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label66);
            this.groupBox8.Controls.Add(this.label65);
            this.groupBox8.Controls.Add(this._supplyVoltage);
            this.groupBox8.Controls.Add(this._temperature);
            this.groupBox8.Controls.Add(this.label53);
            this.groupBox8.Controls.Add(this.label5);
            this.groupBox8.Controls.Add(this._resetErrors);
            this.groupBox8.Controls.Add(this.label4);
            this.groupBox8.Controls.Add(this._infoLed5);
            this.groupBox8.Controls.Add(this._infoLed4);
            this.groupBox8.Controls.Add(this._infoLed3);
            this.groupBox8.Controls.Add(this.label64);
            this.groupBox8.Controls.Add(this.label63);
            this.groupBox8.Controls.Add(this.label61);
            this.groupBox8.Controls.Add(this._infoLed2);
            this.groupBox8.Controls.Add(this.label62);
            this.groupBox8.Controls.Add(this._infoLed1);
            this.groupBox8.Controls.Add(this.InfoLabel);
            this.groupBox8.Controls.Add(this.label50);
            this.groupBox8.Controls.Add(this._vBat);
            this.groupBox8.Controls.Add(this._dateTimeLabel);
            this.groupBox8.Location = new System.Drawing.Point(900, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(212, 725);
            this.groupBox8.TabIndex = 31;
            this.groupBox8.TabStop = false;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(183, 662);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(22, 13);
            this.label66.TabIndex = 51;
            this.label66.Text = "mV";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(183, 700);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(18, 13);
            this.label65.TabIndex = 50;
            this.label65.Text = "°С";
            // 
            // _supplyVoltage
            // 
            this._supplyVoltage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._supplyVoltage.Location = new System.Drawing.Point(126, 659);
            this._supplyVoltage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._supplyVoltage.Name = "_supplyVoltage";
            this._supplyVoltage.ReadOnly = true;
            this._supplyVoltage.Size = new System.Drawing.Size(51, 20);
            this._supplyVoltage.TabIndex = 49;
            this._supplyVoltage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _temperature
            // 
            this._temperature.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._temperature.Location = new System.Drawing.Point(126, 697);
            this._temperature.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._temperature.Name = "_temperature";
            this._temperature.ReadOnly = true;
            this._temperature.Size = new System.Drawing.Size(51, 20);
            this._temperature.TabIndex = 48;
            this._temperature.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(46, 700);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(74, 13);
            this.label53.TabIndex = 47;
            this.label53.Text = "Температура";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 662);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 13);
            this.label5.TabIndex = 46;
            this.label5.Text = "Напряжение питания";
            // 
            // _resetErrors
            // 
            this._resetErrors.Location = new System.Drawing.Point(28, 215);
            this._resetErrors.Name = "_resetErrors";
            this._resetErrors.Size = new System.Drawing.Size(158, 23);
            this._resetErrors.TabIndex = 45;
            this._resetErrors.Text = "Сброс устранимых ошибок";
            this._resetErrors.UseVisualStyleBackColor = true;
            this._resetErrors.Click += new System.EventHandler(this._resetErrors_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(183, 624);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 13);
            this.label4.TabIndex = 44;
            this.label4.Text = "mV";
            // 
            // _infoLed5
            // 
            this._infoLed5.Location = new System.Drawing.Point(192, 179);
            this._infoLed5.Name = "_infoLed5";
            this._infoLed5.Size = new System.Drawing.Size(13, 13);
            this._infoLed5.State = BEMN.Forms.LedState.Off;
            this._infoLed5.TabIndex = 43;
            // 
            // _infoLed4
            // 
            this._infoLed4.Location = new System.Drawing.Point(192, 151);
            this._infoLed4.Name = "_infoLed4";
            this._infoLed4.Size = new System.Drawing.Size(13, 13);
            this._infoLed4.State = BEMN.Forms.LedState.Off;
            this._infoLed4.TabIndex = 42;
            // 
            // _infoLed3
            // 
            this._infoLed3.Location = new System.Drawing.Point(192, 123);
            this._infoLed3.Name = "_infoLed3";
            this._infoLed3.Size = new System.Drawing.Size(13, 13);
            this._infoLed3.State = BEMN.Forms.LedState.Off;
            this._infoLed3.TabIndex = 41;
            // 
            // label64
            // 
            this.label64.Location = new System.Drawing.Point(6, 179);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(143, 13);
            this.label64.TabIndex = 40;
            this.label64.Text = "Флаг реле срабатывания";
            this.toolTip1.SetToolTip(this.label64, "(Отражает срабатывание хотя бы одной ступени).");
            // 
            // label63
            // 
            this.label63.Location = new System.Drawing.Point(6, 151);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(115, 13);
            this.label63.TabIndex = 39;
            this.label63.Text = "Флаг ошибки часов              ";
            this.toolTip1.SetToolTip(this.label63, resources.GetString("label63.ToolTip"));
            // 
            // label61
            // 
            this.label61.Location = new System.Drawing.Point(6, 123);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(180, 21);
            this.label61.TabIndex = 38;
            this.label61.Text = "Флаг ошибки значений EEPROM";
            this.toolTip1.SetToolTip(this.label61, resources.GetString("label61.ToolTip"));
            // 
            // _infoLed2
            // 
            this._infoLed2.Location = new System.Drawing.Point(192, 94);
            this._infoLed2.Name = "_infoLed2";
            this._infoLed2.Size = new System.Drawing.Size(13, 13);
            this._infoLed2.State = BEMN.Forms.LedState.Off;
            this._infoLed2.TabIndex = 37;
            // 
            // label62
            // 
            this.label62.Location = new System.Drawing.Point(6, 94);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(159, 18);
            this.label62.TabIndex = 36;
            this.label62.Text = "Флаг ошибки CRC";
            this.toolTip1.SetToolTip(this.label62, resources.GetString("label62.ToolTip"));
            // 
            // _infoLed1
            // 
            this._infoLed1.Location = new System.Drawing.Point(192, 67);
            this._infoLed1.Name = "_infoLed1";
            this._infoLed1.Size = new System.Drawing.Size(13, 13);
            this._infoLed1.State = BEMN.Forms.LedState.Off;
            this._infoLed1.TabIndex = 35;
            // 
            // InfoLabel
            // 
            this.InfoLabel.Location = new System.Drawing.Point(6, 67);
            this.InfoLabel.Name = "InfoLabel";
            this.InfoLabel.Size = new System.Drawing.Size(180, 13);
            this.InfoLabel.TabIndex = 33;
            this.InfoLabel.Text = "Флаг ошибки внешней EEPROM ";
            this.toolTip1.SetToolTip(this.InfoLabel, "(Память не читается, не пишется\r\nили не проходит верификацию записи). \r\nОписание:" +
        " неустранимая ошибка, \r\nзамыкает реле неисправности,\r\nустанавливает запрет на лю" +
        "бые\r\nдальнейшие операции с  EEPROM.");
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(6, 624);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(115, 13);
            this.label50.TabIndex = 32;
            this.label50.Text = "Напряжение батареи";
            // 
            // _vBat
            // 
            this._vBat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._vBat.Location = new System.Drawing.Point(126, 621);
            this._vBat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._vBat.Name = "_vBat";
            this._vBat.ReadOnly = true;
            this._vBat.Size = new System.Drawing.Size(51, 20);
            this._vBat.TabIndex = 32;
            this._vBat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(20, 467);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(182, 13);
            this.label54.TabIndex = 34;
            this.label54.Text = "Измеренная фаза напряжения U0";
            this.label54.Visible = false;
            // 
            // _u0Phase
            // 
            this._u0Phase.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._u0Phase.Location = new System.Drawing.Point(208, 465);
            this._u0Phase.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._u0Phase.Name = "_u0Phase";
            this._u0Phase.ReadOnly = true;
            this._u0Phase.Size = new System.Drawing.Size(55, 20);
            this._u0Phase.TabIndex = 32;
            this._u0Phase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._u0Phase.Visible = false;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(289, 467);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(138, 13);
            this.label55.TabIndex = 37;
            this.label55.Text = "Измеренная фаза тока In";
            this.label55.Visible = false;
            // 
            // _inPhase
            // 
            this._inPhase.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._inPhase.Location = new System.Drawing.Point(432, 464);
            this._inPhase.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._inPhase.Name = "_inPhase";
            this._inPhase.ReadOnly = true;
            this._inPhase.Size = new System.Drawing.Size(55, 20);
            this._inPhase.TabIndex = 35;
            this._inPhase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._inPhase.Visible = false;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._modLedIa);
            this.groupBox9.Controls.Add(this._modIaOutputBtn);
            this.groupBox9.Controls.Add(this.label56);
            this.groupBox9.Controls.Add(this._modIaInputBtn);
            this.groupBox9.Location = new System.Drawing.Point(209, 579);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(185, 73);
            this.groupBox9.TabIndex = 38;
            this.groupBox9.TabStop = false;
            // 
            // _modLedIa
            // 
            this._modLedIa.Location = new System.Drawing.Point(166, 19);
            this._modLedIa.Name = "_modLedIa";
            this._modLedIa.Size = new System.Drawing.Size(13, 13);
            this._modLedIa.State = BEMN.Forms.LedState.Off;
            this._modLedIa.TabIndex = 10;
            // 
            // _modIaOutputBtn
            // 
            this._modIaOutputBtn.Location = new System.Drawing.Point(82, 35);
            this._modIaOutputBtn.Name = "_modIaOutputBtn";
            this._modIaOutputBtn.Size = new System.Drawing.Size(67, 26);
            this._modIaOutputBtn.TabIndex = 17;
            this._modIaOutputBtn.Text = "Вывести";
            this._modIaOutputBtn.UseVisualStyleBackColor = true;
            this._modIaOutputBtn.Click += new System.EventHandler(this._modIaOutputBtn_Click);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(6, 19);
            this.label56.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(154, 13);
            this.label56.TabIndex = 3;
            this.label56.Text = "Режим ступени индикации Ia";
            // 
            // _modIaInputBtn
            // 
            this._modIaInputBtn.Location = new System.Drawing.Point(9, 35);
            this._modIaInputBtn.Name = "_modIaInputBtn";
            this._modIaInputBtn.Size = new System.Drawing.Size(67, 26);
            this._modIaInputBtn.TabIndex = 16;
            this._modIaInputBtn.Text = "Ввести";
            this._modIaInputBtn.UseVisualStyleBackColor = true;
            this._modIaInputBtn.Click += new System.EventHandler(this._modIaInputBtn_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._modLedIb);
            this.groupBox10.Controls.Add(this._modIbOutputBtn);
            this.groupBox10.Controls.Add(this.label57);
            this.groupBox10.Controls.Add(this._modIbInputBtn);
            this.groupBox10.Location = new System.Drawing.Point(400, 579);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(185, 73);
            this.groupBox10.TabIndex = 39;
            this.groupBox10.TabStop = false;
            // 
            // _modLedIb
            // 
            this._modLedIb.Location = new System.Drawing.Point(166, 19);
            this._modLedIb.Name = "_modLedIb";
            this._modLedIb.Size = new System.Drawing.Size(13, 13);
            this._modLedIb.State = BEMN.Forms.LedState.Off;
            this._modLedIb.TabIndex = 10;
            // 
            // _modIbOutputBtn
            // 
            this._modIbOutputBtn.Location = new System.Drawing.Point(82, 35);
            this._modIbOutputBtn.Name = "_modIbOutputBtn";
            this._modIbOutputBtn.Size = new System.Drawing.Size(67, 26);
            this._modIbOutputBtn.TabIndex = 17;
            this._modIbOutputBtn.Text = "Вывести";
            this._modIbOutputBtn.UseVisualStyleBackColor = true;
            this._modIbOutputBtn.Click += new System.EventHandler(this._modIbOutputBtn_Click);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(6, 19);
            this.label57.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(154, 13);
            this.label57.TabIndex = 3;
            this.label57.Text = "Режим ступени индикации Ib";
            // 
            // _modIbInputBtn
            // 
            this._modIbInputBtn.Location = new System.Drawing.Point(9, 35);
            this._modIbInputBtn.Name = "_modIbInputBtn";
            this._modIbInputBtn.Size = new System.Drawing.Size(67, 26);
            this._modIbInputBtn.TabIndex = 16;
            this._modIbInputBtn.Text = "Ввести";
            this._modIbInputBtn.UseVisualStyleBackColor = true;
            this._modIbInputBtn.Click += new System.EventHandler(this._modIbInputBtn_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._modLedIc);
            this.groupBox11.Controls.Add(this._modIcOutputBtn);
            this.groupBox11.Controls.Add(this.label58);
            this.groupBox11.Controls.Add(this._modIcInputBtn);
            this.groupBox11.Location = new System.Drawing.Point(591, 579);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(185, 73);
            this.groupBox11.TabIndex = 40;
            this.groupBox11.TabStop = false;
            // 
            // _modLedIc
            // 
            this._modLedIc.Location = new System.Drawing.Point(166, 19);
            this._modLedIc.Name = "_modLedIc";
            this._modLedIc.Size = new System.Drawing.Size(13, 13);
            this._modLedIc.State = BEMN.Forms.LedState.Off;
            this._modLedIc.TabIndex = 10;
            // 
            // _modIcOutputBtn
            // 
            this._modIcOutputBtn.Location = new System.Drawing.Point(82, 35);
            this._modIcOutputBtn.Name = "_modIcOutputBtn";
            this._modIcOutputBtn.Size = new System.Drawing.Size(67, 26);
            this._modIcOutputBtn.TabIndex = 17;
            this._modIcOutputBtn.Text = "Вывести";
            this._modIcOutputBtn.UseVisualStyleBackColor = true;
            this._modIcOutputBtn.Click += new System.EventHandler(this._modIcOutputBtn_Click);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(6, 19);
            this.label58.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(154, 13);
            this.label58.TabIndex = 3;
            this.label58.Text = "Режим ступени индикации Ic";
            // 
            // _modIcInputBtn
            // 
            this._modIcInputBtn.Location = new System.Drawing.Point(9, 35);
            this._modIcInputBtn.Name = "_modIcInputBtn";
            this._modIcInputBtn.Size = new System.Drawing.Size(67, 26);
            this._modIcInputBtn.TabIndex = 16;
            this._modIcInputBtn.Text = "Ввести";
            this._modIcInputBtn.UseVisualStyleBackColor = true;
            this._modIcInputBtn.Click += new System.EventHandler(this._modIcInputBtn_Click);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this._modFOutputBtn);
            this.groupBox13.Controls.Add(this.label60);
            this.groupBox13.Controls.Add(this._modFInputBtn);
            this.groupBox13.Location = new System.Drawing.Point(512, 658);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(264, 73);
            this.groupBox13.TabIndex = 41;
            this.groupBox13.TabStop = false;
            // 
            // _modFOutputBtn
            // 
            this._modFOutputBtn.Location = new System.Drawing.Point(188, 35);
            this._modFOutputBtn.Name = "_modFOutputBtn";
            this._modFOutputBtn.Size = new System.Drawing.Size(67, 26);
            this._modFOutputBtn.TabIndex = 17;
            this._modFOutputBtn.Text = "Вывести";
            this._modFOutputBtn.UseVisualStyleBackColor = true;
            this._modFOutputBtn.Click += new System.EventHandler(this._modFOutputBtn_Click);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(6, 19);
            this.label60.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(249, 13);
            this.label60.TabIndex = 3;
            this.label60.Text = "Режим ступеней индикации по всем токам фаз";
            // 
            // _modFInputBtn
            // 
            this._modFInputBtn.Location = new System.Drawing.Point(12, 35);
            this._modFInputBtn.Name = "_modFInputBtn";
            this._modFInputBtn.Size = new System.Drawing.Size(67, 26);
            this._modFInputBtn.TabIndex = 16;
            this._modFInputBtn.Text = "Ввести";
            this._modFInputBtn.UseVisualStyleBackColor = true;
            this._modFInputBtn.Click += new System.EventHandler(this._modFInputBtn_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(960, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(8, 8);
            this.button2.TabIndex = 42;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // ItkzMeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 739);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox13);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.label55);
            this.Controls.Add(this._inPhase);
            this.Controls.Add(this.label54);
            this.Controls.Add(this._u0Phase);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label44);
            this.Controls.Add(this._kvitBtn);
            this.Controls.Add(this._inPhaseShift);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ItkzMeasuringForm";
            this.Text = "Measuring";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ItkzMeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.ItkzMeasuringForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBoxModOzz.ResumeLayout(false);
            this.groupBoxModOzz.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private BEMN.Forms.LedControl _statusLedIc;
        private BEMN.Forms.LedControl _statusLedIb;
        private System.Windows.Forms.Label label2;
        private BEMN.Forms.LedControl _statusLedIa;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private BEMN.Forms.LedControl _statusLedIn;
        private BEMN.Forms.LedControl _statusLedIn5;
        private System.Windows.Forms.Label label10;
        private BEMN.Forms.LedControl _stupenLedIa;
        private BEMN.Forms.LedControl _stupenLedIb;
        private BEMN.Forms.LedControl _stupenLedIc;
        private BEMN.Forms.LedControl _stupenLedIn;
        private BEMN.Forms.LedControl _stupenLedIn5;
        private System.Windows.Forms.Button _modInInputBtn;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button _modInOutputBtn;
        private BEMN.Forms.LedControl _modLedIn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private BEMN.Forms.LedControl _modLedIn5;
        private System.Windows.Forms.Button _modIn5OutputBtn;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button _modIn5InputBtn;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox _in_lastValIn;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox _lastValIc;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox _lastValIb;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox _lastValIa;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox _curValIn5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox _curValIn;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox _curValIc;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox _curValIb;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox _curValIa;
        private System.Windows.Forms.Button _kvitBtn;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox groupBox4;
        private Forms.LedControl _modLedU0;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox _in_lastValU0;
        private System.Windows.Forms.TextBox _inAngle;
        private System.Windows.Forms.TextBox _in_lastValIn5;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox _in5_lastValU0;
        private System.Windows.Forms.TextBox _in5Angle;
        private System.Windows.Forms.TextBox _in5_lastValIn5;
        private System.Windows.Forms.TextBox _in5_lastValIn;
        private Forms.LedControl _directionLedIn5;
        private Forms.LedControl _directionLedIn;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox _u0_lastValU0;
        private System.Windows.Forms.TextBox _u0Angle;
        private System.Windows.Forms.TextBox _u0_lastValIn5;
        private System.Windows.Forms.TextBox _u0_lastValIn;
        private Forms.LedControl _directionLedU0;
        private Forms.LedControl _statusLedU0;
        private Forms.LedControl _stupenLedU0;
        private System.Windows.Forms.Button _modU0outBtn;
        private System.Windows.Forms.Button _modU0inBtn;
        private System.Windows.Forms.Label label40;
        private Forms.LedControl _dostoverLedIn;
        private System.Windows.Forms.Label label41;
        private Forms.LedControl _dostoverLedIn5;
        private System.Windows.Forms.Label label42;
        private Forms.LedControl _dostoverLedU0;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox _inPhaseShift;
        private System.Windows.Forms.TextBox _curValU0;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button _modDirTrigOutBtn;
        private System.Windows.Forms.Button _modDirTrigInpBtn;
        private Forms.LedControl _modLedInOutDir;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button _modDirTrigOneBtn;
        private System.Windows.Forms.Button _modDirTrigZeroBtn;
        private Forms.LedControl _modLedUstDir;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox _stateUstNaprSrabTB;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox _lastValEnergy;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox _curValEnergy;
        private System.Windows.Forms.Label label48;
        private Forms.LedControl _statusLedEnergy;
        private System.Windows.Forms.Label _dateTimeLabel;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox _vBat;
        private System.Windows.Forms.Label InfoLabel;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label _u0TimeStageLabel;
        private System.Windows.Forms.Label _in5TimeStageLabel;
        private System.Windows.Forms.Label _inTimeStageLabel;
        private System.Windows.Forms.Label _icTimeStageLabel;
        private System.Windows.Forms.Label _ibTimeStageLabel;
        private System.Windows.Forms.Label _iaTimeStageLabel;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox _u0Phase;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox _inPhase;
        private System.Windows.Forms.GroupBox groupBox9;
        private Forms.LedControl _modLedIa;
        private System.Windows.Forms.Button _modIaOutputBtn;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Button _modIaInputBtn;
        private System.Windows.Forms.GroupBox groupBox10;
        private Forms.LedControl _modLedIb;
        private System.Windows.Forms.Button _modIbOutputBtn;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Button _modIbInputBtn;
        private System.Windows.Forms.GroupBox groupBox11;
        private Forms.LedControl _modLedIc;
        private System.Windows.Forms.Button _modIcOutputBtn;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Button _modIcInputBtn;
        private System.Windows.Forms.GroupBox groupBoxModOzz;
        private System.Windows.Forms.Button _modOzzOutputBtn;
        private System.Windows.Forms.Button _modOzzIputBtn;
        private Forms.LedControl _modLedOzz;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Button _modFOutputBtn;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Button _modFInputBtn;
        private System.Windows.Forms.Button _in5ImitationBtn;
        private System.Windows.Forms.Button _inImitationBtn;
        private System.Windows.Forms.Button _icImitationBtn;
        private System.Windows.Forms.Button _ibImitationBtn;
        private System.Windows.Forms.Button _iaImitationBtn;
        private System.Windows.Forms.Button _u0ImitationBtn;
        private Forms.LedControl _infoLed5;
        private Forms.LedControl _infoLed4;
        private Forms.LedControl _infoLed3;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label61;
        private Forms.LedControl _infoLed2;
        private System.Windows.Forms.Label label62;
        private Forms.LedControl _infoLed1;
        private System.Windows.Forms.TextBox _modOzz;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button _resetErrors;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox _supplyVoltage;
        private System.Windows.Forms.TextBox _temperature;
        private Forms.LedControl _blinkerLed6;
        private Forms.LedControl _blinkerLed5;
        private Forms.LedControl _blinkerLed4;
        private Forms.LedControl _blinkerLed3;
        private Forms.LedControl _blinkerLed2;
        private Forms.LedControl _blinkerLed1;
        private System.Windows.Forms.Label label67;
    }
}