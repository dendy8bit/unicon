﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.ITKZ_V6.Structures
{
    /// <summary>
    /// Биты регистра TimeSettings.
    /// В измерении:
    /// 0 - TimeZone B0:B3 - модуль значения часового пояса от 0 до 12
    /// 1  
    /// 2 
    /// 3 
    /// 4 - Знак часового пояса: 0 - это "+"
    ///                          1 - это "-"
    /// 5  
    /// 6 - Бит разрешения автоматического перехода на летнее время и обратно: 0 - переход не выполняется
    ///                                                                        1 - переход выполняется
    /// 7
    /// 8 - RtcCalibration B0:B4 - модуль коррекции хода часов от 0 до 31. Определить вес кванта (предположительно несколько секунд в месяц)
    /// 9
    /// 10
    /// 11
    /// 12
    /// 13 - RtcCalibrationSign - знак коррекции хода часов
    /// </summary>
    public class TimeSettings : StructBase
    {
        [Layout(0)] private ushort _timeSettings;

        [BindingProperty(0)]
        public string TimeZoneB0_B3 // (0-вой, 1-ый, 2-ой, 3-ий)
        {
            get { return Validator.Get(this._timeSettings, Strings.TimeZone, 0, 1, 2, 3); }
            set { this._timeSettings = Validator.Set(value, Strings.TimeZone, this._timeSettings, 0, 1, 2, 3); }
        }

        [BindingProperty(1)]
        public string TimeZoneSign // (4-ый бит)
        {
            get
            {
                return Validator.Get(this._timeSettings, Strings.Sign, 4); ;
            }
            set
            {
                this._timeSettings = Validator.Set(value, Strings.Sign, this._timeSettings, 4);
            }
        }

        [BindingProperty(2)]
        public bool SummerAuto // (6-ой бит)
        {
            get { return Common.GetBit(this._timeSettings, 6); }
            set { this._timeSettings = Common.SetBit(this._timeSettings, 6, value); }
        }

        [BindingProperty(3)]
        public string RtcCalibrationB0_B4 // (8-ой, 9-ый, 10-ый, 11-ый, 12-ый бит)
        {
            get { return Validator.Get(this._timeSettings, Strings.СlockCorrectionModule, 8, 9, 10, 11, 12); }
            set { this._timeSettings = Validator.Set(value, Strings.СlockCorrectionModule, this._timeSettings, 8, 9, 10, 11, 12); }
        }

        [BindingProperty(4)]
        public string RtcCalibrationSign // (13-ый бит)
        {
            get { return Validator.Get(this._timeSettings, Strings.Sign, 13); }
            set { this._timeSettings = Validator.Set(value, Strings.Sign, this._timeSettings, 13); }
        }

        // Свойство для Unix-времени
        public ushort ForUnixTime
        {
            get { return this._timeSettings; }
            set { this._timeSettings = value; }
        }
    }
}