﻿using System;
using System.Collections;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.ITKZ_V6.Structures
{
    /// <summary>
    /// Блок регистров настроек.
    /// </summary>
    public class ConfigurationStruct : StructBase
    {
        #region [Fields]

        [Layout(0)] private ushort _iaLimit; // уставка срабатывания измерительного органа (ИО) по току фазы Ia в % от 1 до 100 (0x0104)
        [Layout(1)] private ushort _ibLimit; // уставка срабатывания измерительного органа (ИО) по току фазы Ib в % от 1 до 100 (0x0105)
        [Layout(2)] private ushort _icLimit; // уставка срабатывания измерительного органа (ИО) по току фазы Ic в % от 1 до 100 (0x0106)
        [Layout(3)] private ushort _inLimit; // уставка срабатывания измерительного органа (ИО) по току In в % от 1 до 100 (0x0107)
        [Layout(4)] private ushort _in5Limit; // уставка измерительного органа (ИО) по току In5 гармоники в % от 1 до 100 (0x0108)
        [Layout(5)] private ushort _u0Limit; // уставка срабатывания измерительно органа (ИО) по значению напряжения U0 (0x0109)
        [Layout(6)] private ushort _phShiftLimit; // уставка угла фазового сдвига между напряжением U0 и током In для определения направления мощности (0x010a)

        [Layout(7)] private ushort _iaTime; // уставка времени срабатывания ступени индикации (СИ) по току фазы Ia в мс от 10 до 30000 мс (0x010b)
        [Layout(8)] private ushort _ibTime; // уставка времени срабатывания ступени индикации (СИ) по току фазы Ib в мс от 10 до 30000 мс (0x010c)
        [Layout(9)] private ushort _icTime; // уставка времени срабатывания ступени индикации (СИ) по току фазы Ic в мс от 10 до 30000 мс (0x010d)
        [Layout(10)] private ushort _inTime; // уставка времени срабатывания ступени индикации (СИ) по току фазы In в мс от 10 до 30000 мс (0x010e)
        [Layout(11)] private ushort _in5Time; // уставка времени срабатывания ступени индикации (СИ) по току фазы In5 гармоники в мс от 10 до 30000 мс (0x010f)
        [Layout(12)] private ushort _u0Time; // уставка времени срабатывания ступени индикации (СИ) по напряжению U0 в мс от 10 до 30000 мс (0x0110)

        [Layout(13)] private Settings _settings; // настройки, уставки режимов ступеней индикации (0x0111)
        [Layout(14)] private TimeSettings _timeSettings; // настройки часов (0x0112)

        [Layout(15)] private ushort _releOutputTime; // время включения выходного реле (0x0113)

        [Layout(16)] private ushort _energyLimit; // уставка срабатывания измерительного органа (ИО) ... (0x0114)
        #endregion [Fields]

        #region [Properties] 

        #region [Блок 1]
        [XmlElement(ElementName = "Уставка_фазы_А")]
        [BindingProperty(0)]
        public double IaLimit
        {
            get { return (double)this._iaLimit / 100; }
            set { this._iaLimit = (ushort)Math.Round(value * 100); }
        }

        [XmlElement(ElementName = "Уставка_фазы_B")]
        [BindingProperty(1)]
        public double IbLimit
        {
            get { return (double)this._ibLimit / 100; }
            set { this._ibLimit = (ushort)Math.Round(value * 100); }
        }

        [XmlElement(ElementName = "Уставка_фазы_C")]
        [BindingProperty(2)]
        public double IcLimit
        {
            get { return (double)this._icLimit / 100; }
            set { this._icLimit = (ushort)Math.Round(value * 100); }
        }

        [XmlElement(ElementName = "Уставка_фазы_N")]
        [BindingProperty(3)]
        public double InLimit
        {
            get { return (double)this._inLimit / 100; }
            set { this._inLimit = (ushort)Math.Round(value * 100); }
        }

        [XmlElement(ElementName = "Уставка_фазы_N5")]
        [BindingProperty(4)]
        public double In5Limit
        {
            get { return (double)this._in5Limit / 100; }
            set { this._in5Limit = (ushort)Math.Round(value * 100); }
        }

        [XmlElement(ElementName = "Уставка_фазы_U0")]
        [BindingProperty(5)]
        public double U0Limit
        {
            get { return (double)this._u0Limit / 100; }
            set { this._u0Limit = (ushort)Math.Round(value * 100); }
        }

        [XmlElement(ElementName = "Уставка_угла_фазового_сдвига")]
        [BindingProperty(6)]
        public ushort PhaseShiftLimit
        {
            get { return this._phShiftLimit; }
            set { this._phShiftLimit = value; }
        }
        #endregion [Блок 1]

        #region [Блок 3]
        [XmlElement(ElementName = "Уставка_времени_фазы_A")]
        [BindingProperty(7)]
        public ushort IaTime
        {
            get { return this._iaTime; }
            set { this._iaTime = value; }
        }

        [XmlElement(ElementName = "Уставка_времени_фазы_B")]
        [BindingProperty(8)]
        public ushort IbTime
        {
            get { return this._ibTime; }
            set { this._ibTime = value; }
        }

        [XmlElement(ElementName = "Уставка_времени_фазы_C")]
        [BindingProperty(9)]
        public ushort IcTime
        {
            get { return this._icTime; }
            set { this._icTime = value; }
        }

        [XmlElement(ElementName = "Уставка_времени_фазы_N")]
        [BindingProperty(10)]
        public ushort InTime
        {
            get { return this._inTime; }
            set { this._inTime = value; }
        }

        [XmlElement(ElementName = "Уставка_времени_фазы_N5")]
        [BindingProperty(11)]
        public ushort In5Time
        {
            get { return this._in5Time; }
            set { this._in5Time = value; }
        }

        [XmlElement(ElementName = "Уставка_времени_фазы_U0")]
        [BindingProperty(12)]
        public ushort U0Time
        {
            get { return this._u0Time; }
            set { this._u0Time = value; }
        }
        #endregion [Блок 2]

        #region [Блок 3]

        [BindingProperty(13)]
        public Settings SettingsConfiguration
        {
            get { return this._settings; }
            set { this._settings = value; }
        }

        [BindingProperty(14)]
        public TimeSettings TimeSettingsConfiguration
        {
            get { return this._timeSettings; }
            set { this._timeSettings = value; }
        }
        #endregion [Блок 3]

        [BindingProperty(15)]
        public ushort ReleOutputTime
        {
            get { return this._releOutputTime; }
            set { this._releOutputTime = value; }
        }

        [BindingProperty(16)]
        public int EnergyLimit
        {
            get
            {
                if (this._energyLimit > short.MaxValue)
                {
                    BitArray inverseBitArray = new BitArray(Common.GetBitsArray(this._energyLimit)).Not();
                    ushort temp = Common.BitsToUshort(inverseBitArray);
                    temp++;
                    return -temp;
                }
                return this._energyLimit;
            }
            set
            {
                if (value < 0)
                {
                    BitArray inverseBitArray = new BitArray(Common.GetBitsArray((ushort)-value)).Not();
                    ushort temp = Common.BitsToUshort(inverseBitArray);
                    temp++;
                    this._energyLimit = temp;
                }
                if (value >= 0)
                {
                    this._energyLimit = (ushort)value;
                }
            }
        }

        #endregion [Properties]
    }
}