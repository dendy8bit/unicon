﻿using System;
using System.Collections;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.ITKZ_V6.Structures
{
    /// <summary>
    /// Биты регистра Settings.
    /// В измерении:
    /// 0 - Режим определения направления ОЗЗ: 0 - направление определяется по углу сдвига фазы между U0 и In (для сетей с изолированной нейтралью или резистивным заземлением) 
    ///                                        1 - направление определяется по знаку интеграла энергии E(t)=U0(t)*In(t) в первые 20 мс аварийного процесса (для сетей с ДГК)
    /// 1 - Режим ступени индикации (СИ) тока In: 0 - выведена
    ///                                           1 - введена
    /// 2 - Режим ступени индикации (СИ) тока In5: 0 - выведена
    ///                                            1 - введена
    /// 3 - Режим ступени индикации (СИ) напряжения U0: 0 - выведена
    ///                                                 1 - введена
    /// 4 - Режим ступени индикации (СИ) тока Ia: 0 - выведена
    ///                                           1 - введена
    /// 5 - Режим ступени индикации (СИ) тока Ib: 0 - выведена
    ///                                           1 - введена
    /// 6 - Режим ступени индикации (СИ) тока Ic: 0 - выведена
    ///                                           1 - введена
    /// В конфигурации:
    /// 7 - Уставка обнуления последних значений при квитировании: 1 - не обнулять
    ///                                                            0 - обунулять
    /// 8 - Уставка угла сектора срабатывания, B0:  
    /// 9 - Уставка угла сектора срабатывания, B1:
    /// 10 - Уставка угла сектора срабатывания, B2: 000 - 180
    ///                                             101 - 210
    ///                                             001 - 240
    ///                                             110 - 245
    ///                                             010 - 250
    ///                                             111 - 255
    ///                                             011 - 260
    /// 11 - Бит автоквитирования B0:
    /// 12 - Бит автоквитирования B1:
    /// 13 - Бит автоквитирования B2: 000 - выкл
    ///                               101 - 5 мин
    ///                               001 - 10 мин
    ///                               110 - 30 мин
    ///                               010 - 1 час
    ///                               111 - 1,5 часа
    ///                               011 - 2 часа
    /// В измерении:
    /// 14 - Флаг направления срабатывания: 1 - от шин
    ///                                     0 - к шинам
    /// 15 - Режим направленного срабатывания: 0 - выведен
    ///                                        1 - введен 
    /// </summary>
    public class Settings : StructBase
    {
        [Layout(0)] private ushort _settings;

        public BitArray SettingsMeasuring 
        {
            get
            {
                return new BitArray(BitConverter.GetBytes(this._settings));
            }    
        }

        [BindingProperty(0)]
        public bool LastvalKeep // (7-ой бит) чекбокс Обнуление аварийных значений при квитировании
        {
            get { return !Common.GetBit(this._settings, 7); }
            set { this._settings = Common.SetBit(this._settings, 7, !value); }
        }

        [BindingProperty(1)]
        public string AutoKvitB0_B2 // (11-ый, 12-ый, 13-ый бит) комбобокс Автоквитирование
        {
            get { return Validator.GetForItkzV6(this._settings, Strings.AutoKvitTime, 11, 12, 13); }
            set { this._settings = Validator.Set(value, Strings.AutoKvitTime, this._settings, 11, 12, 13); }
        }

        [BindingProperty(2)]
        public string SectorAngleB0_B2 // (8-ой, 9-ый, 10-ый бит) комбобокс Угол, градусы
        {
            get { return Validator.GetForItkzV6(this._settings, Strings.AngleStatus, 8, 9, 10); }
            set { this._settings = Validator.Set(value, Strings.AngleStatus, this._settings, 8, 9, 10); }
        }
    }
}