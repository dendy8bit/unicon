﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.ITKZ_V6.Structures
{
    public class NumAndSpeedDevice : StructBase
    {
        [Layout(0)] private ushort _devAddr; // Биты 0..7 - Адрес устройства в сети RS-485 (0x0102)
        [Layout(1)] private ushort _devSpd; // Скорость устройства 9600 - 115200 (0x0103)

        [BindingProperty(0)]
        public ushort DeviceAddress
        {
            get { return this._devAddr; }
            set { this._devAddr = value; }
        }

        [BindingProperty(1)]
        public string DeviceSpeed
        {
            get
            {
                ushort val = (ushort)(this._devSpd - 1);
                return Validator.Get(val, Strings.DeviceSpeed);
            }
            set
            {
                ushort val = Validator.Set(value, Strings.DeviceSpeed);
                this._devSpd = (ushort)(val + 1);
            }
        }
    }
}