﻿using System.Collections.Generic;

namespace BEMN.ITKZ_V6.Structures
{
    /// <summary>
    /// Строки комбобоксов Конфигурации.
    /// </summary>
    internal class Strings
    {
        public static List<string> AutoKvitTime
        {
            get
            {
                return new List<string>
                {
                    "Выключено",
                    "10 минут",
                    "1 час",
                    "2 часа",
                    "5 минут",
                    "30 минут",
                    "1,5 часа"
                };
            }
        }

        public static List<string> AngleStatus
        {
            get
            {
                return new List<string>
                {
                    "180",
                    "240",
                    "250",
                    "260",
                    "210",
                    "245",
                    "255"
                };
            }
        }

        public static List<string> DeviceSpeed 
        {
            get
            {
                return new List<string>
                {
                    "9600",
                    "19200",
                    "38400",
                    "57600",
                    "115200"
                };
            }
        }

        public static List<string> TimeZone
        {
            get
            {
                return new List<string>
                {
                    "0",
                    "1",
                    "2",
                    "3",
                    "4",
                    "5",
                    "6",
                    "7",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12"
                };
            }
        }

        public static List<string> Sign 
        {
            get
            {
                return new List<string>
                {
                    "+",
                    "-",
                };
            }
        }

        public static List<string> СlockCorrectionModule
        {
            get
            {
                return new List<string>
                {
                    "0",
                    "1",
                    "2",
                    "3",
                    "4",
                    "5",
                    "6",
                    "7",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "15",
                    "16",
                    "17",
                    "18",
                    "19",
                    "20",
                    "21",
                    "22",
                    "23",
                    "24",
                    "25",
                    "26",
                    "27",
                    "28",
                    "29",
                    "30",
                    "31"
                };
            }           
        }
    }
}