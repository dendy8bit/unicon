﻿using System;
using System.Collections;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.ITKZ_V6.Structures
{
    /// <summary>
    /// Структура измерений.
    /// </summary>
    public class MeasuringStruct : StructBase
    {
        #region [Fields]
        // Блок регистров состояния и текущих значений.

        // Блок 1
        [Layout(0)] private ushort _status; // регистр текущего состояния прибора (выходные реле, кнопка, флаги ошибок) (0x0115)
        [Layout(1)] private ushort _ioStatus; // регистр текущего состояния измерительных органов (0x0116)
        [Layout(2)] private ushort _stStatus; // регистр текущего состояния и уставок ступеней индикации (0x0117)

        // Блок 2
        [Layout(3)] private ushort _iaValue; // измеренное значение тока фазы A в амперах (0x0118)
        [Layout(4)] private ushort _ibValue; // измеренное значение тока фазы B в амперах (0x0119)
        [Layout(5)] private ushort _icValue; // измеренное значение тока фазы C в амперах (0x011a)
        [Layout(6)] private ushort _inValue; // измеренное значение тока N в миллиамперах (0x011b)
        [Layout(7)] private ushort _in5Value; // измеренное значение тока N5 в миллиамперах (0x011c)
        [Layout(8)] private ushort _u0Value; // измеренное значение напряжения U0 в условных вольтах (0x011d)

        // Блок 3
        [Layout(9)] private ushort _u0Phase; // измеренная фаза напряжения U0 (0x011e)
        [Layout(10)] private ushort _inPhase; // измеренная фаза тока In (0x011f)
        [Layout(11)] private ushort _inPhaseShift; // измеренный сдвиг фазы между напряжением U0 и током In (0x0120)

        // Блок 4
        [Layout(12)] private ushort _iaIoTime; // время срабатывания измерительного органа (ИО) по току фазы Ia в мс (0x0121)
        [Layout(13)] private ushort _ibIoTime; // время срабатывания измерительного органа (ИО) по току фазы Ib в мс (0x0122)
        [Layout(14)] private ushort _icIoTime; // время срабатывания измерительного органа (ИО) по току фазы Ic в мс (0x0123)
        [Layout(15)] private ushort _inIoTime; // время срабатывания измерительного органа (ИО) по току фазы In в мс (0x0124)
        [Layout(16)] private ushort _in5IoTime; // время срабатывания измерительного органа (ИО) по току фазы In5 гармоники в мс (0x0125)
        [Layout(17)] private ushort _u0IoTime; // время срабатывания измерительного органа (ИО) по напряжению U0 в мс (0x0126)

        // Блок 5
        [Layout(18)] private ushort _energy; // значение энергии Е0 за период (0x0127)

        // Блок 6 
        [Layout(19)] private ushort _currentTimeUnixL; // текущее время (UTC) в UNIX - формате, младшие 16 бит (0x0128)
        [Layout(20)] private ushort _currentTimeUnixH; // текущее время (UTC) в UNIX - формате, старшие 16 бит (0x0129)

        // Блок 7
        [Layout(21)] private ushort _currentTimeWord1; // текущее время (локальное, с учетом пояса), слово 1: минуты (8 бит) : секунды (8 бит) (0x012a)
        [Layout(22)] private ushort _currentTimeWord2; // текущее время (локальное, с учетом пояса), слово 2: число (8 бит) : часы (8 бит) (0x012b)
        [Layout(23)] private ushort _currentTimeWord3; // текущее время (локальное, с учетом пояса), слово 3: год (12 бит) : месяц (8 бит) (0x012c)

        // Блок 8
        [Layout(24)] private ushort _vBat; // напряжение батареи в милливольтах (0x012d)
        [Layout(25)] private ushort _supplyVoltage; // аналоговое напряжение питания в милливольтах (0x012e)
        [Layout(26)] private ushort _temperature; // температура (процессора) в градусах Цельсия (0x012f)



        // Блок регистров значений при последних аварийных ситуациях.

        // Блок 9
        [Layout(27)] private ushort _iaLastval; // значение тока фазы A при последнем срабатывании ступени индикации (СИ) (0x0130)
        [Layout(28)] private ushort _iaLastvalTimeL; // время срабатывания ступени Ia, младшие 16 бит (0x0131)
        [Layout(29)] private ushort _iaLastvalTimeH; // время срабатывания ступени Ia, старшие 16 бит (0x0132)
        [Layout(30)] private ushort _ibLastval; // значение тока фазы B при последнем срабатывании ступени индикации (СИ) (0x0133)
        [Layout(31)] private ushort _ibLastvalTimeL; // время срабатывания ступени Ib, младшие 16 бит (0x0134)
        [Layout(32)] private ushort _ibLastvalTimeH; // время срабатывания ступени Ib, старшие 16 бит (0x0135)
        [Layout(33)] private ushort _icLastval; // значение тока фазы C при последнем срабатывании ступени индикации (СИ) (0x0136)
        [Layout(34)] private ushort _icLastvalTimeL; // время срабатывания ступени Ic, младшие 16 бит (0x0137)
        [Layout(35)] private ushort _icLastvalTimeH; // время срабатывания ступени Ic, старшие 16 бит (0x0138)

        // Блок 10
        [Layout(36)] private ushort _inLastvalIn; // значение тока In при последнем срабатывании ступени индикации (СИ) по току In (0x0139)
        [Layout(37)] private ushort _inLastvalIn5; // значение тока In5 при последнем срабатывании ступени индикации (СИ) по току In (0x013a)
        [Layout(38)] private ushort _inLastvalU0; // значение напряжения U0 при последнем срабатывании ступени индикации (СИ) по току In (0x013b)
        [Layout(39)] private ushort _inLastvalAngle; // значение угла фазового сдвига между напряжением U0 и током In при последнем срабатывании ступени индикации (СИ) по току In (0x013c)
        [Layout(40)] private ushort _inLastvalTimeL; // время срабатывания ступени In, младшие 16 бит (0x013d)
        [Layout(41)] private ushort _inLastvalTimeH; // время срабатывания ступени In, старшие 16 бит (0x013e)

        // Блок 11
        [Layout(42)] private ushort _in5LastvalIn; // значение тока In при последнем срабатывании ступени индикации (СИ) по току In5 (0x013f)
        [Layout(43)] private ushort _in5LastvalIn5; // значение тока In5 при последнем срабатывании ступени индикации (СИ) по току In5 (0x0140)
        [Layout(44)] private ushort _in5LastvalU0; // значение напряжения U0 при последнем срабатывании ступени индикации (СИ) по току In5 (0x0141)
        [Layout(45)] private ushort _in5LastvalAngle; // значение угла фазового сдвига между напряжением U0 и током In при последнем срабатывании ступени индикации (СИ) по току In5 (0x0142)
        [Layout(46)] private ushort _in5LastvalTimeL; // время срабатывания ступени In5, младшие 16 бит (0x0143)
        [Layout(47)] private ushort _in5LastvalTimeH; // время срабатывания ступени In5, старшие 16 бит (0x0144)

        // Блок 12
        [Layout(48)] private ushort _u0LastvalIn; // значение тока N при последнем срабатывании ступени индикации (СИ) по напряжению U0 (0x0145)
        [Layout(49)] private ushort _u0LastvalIn5; // значение тока N5 при последнем срабатывании ступени индикации (СИ) по напряжению U0 (0x0146)
        [Layout(50)] private ushort _u0LastvalU0; // значение напряжения U0 при последнем срабатывании ступени индексации (СИ) по напряжению U0 (0x0147)
        [Layout(51)] private ushort _u0LastvalAngle; // значение угла фазового сдвига между напряжением U0 и током IN при последнем срабатывании ступени индикации (СИ) по напряжению U0 (0x0148)
        [Layout(52)] private ushort _u0LastvalTimeL; // время срабатывания ступени U0, младшие 16 бит (0x0149)
        [Layout(53)] private ushort _u0LastvalTimeH; // время срабатывания ступени U0, старшие 16 бит (0x014a)

        // Блок 13
        [Layout(54)] private ushort _energyLastvalIn; // значение энергии EN при последнем срабатывании ступени индикации (СИ) по току In (0x014b)

        // Блок 14
        [Layout(55)] private ushort _blinkerAlarm; // блинкер аварий (0x014c)
        [Layout(56)] private ushort _lastvalReserved04d; // зарезервировано (последние значения) (0x014d)
        [Layout(57)] private ushort _lastvalReserved04e; // зарезервировано (последние значения) (0x014e)
        [Layout(58)] private ushort _lastvalReserved04f; // зарезервировано (последние значения) (0x014f)

        #endregion [Fields]

        #region [Properties]

        #region [Блок 1]

        /// <summary>
        /// Биты регистра Status.
        /// 0 - флаг ошибки внешней EEPROM (память не читается, не пишется или не проходит верификацию записи).
        ///     Описание: неустранимая ошибка, замыкает реле неисправности,
        ///               устанавливает запрет на любые дальнейшие операции с EEPROM.
        /// 1 - флаг ошибки CRC (устанавливается при старте, если не совпала рассчетная и прочитанная CRC).
        ///     Описание: устранимая ошибка, НЕ замыкает реле неисправности, указывает на возможное повреждение уставок.
        ///               Снимается записью любых уставок в EEPROM или командами RESET_ERRORS или квитирования.
        /// 2 - флаг ошибки значений EEPROM (устанавливается при старте, если считанные из EEPROM значения вне допустимого диапазона).
        ///     Описание: устранимая ошибка, НЕ замыкает реле неисправности, снимается командами RESET_ERRORS или квитирования.
        /// 3 - флаг ошибки часов (установлен при холодном старте часов, снимается после установки времени).
        ///     Описание: устранимая ошибка, НЕ замыкает реле неисправности.
        ///               Снимается при установке времени и командами RESET_ERRORS или квитирования.
        /// 4 - флаг ошибки аналоговых измерений (устанавливается если аналоговое напряжение питания падает ниже 3.0В).
        ///     Описание: НЕ замыкает реле неисправности, снимается автоматически,
        ///               пока она установлена, действует запрет на срабатывание любых ступеней индикации
        ///               (измерительные органы работают как обычно).
        /// 5 - режим установки времени, устанавливается и снимается соответсвующей командой.
        /// 6 
        /// 7 
        /// 8 
        /// 9 
        /// 10 
        /// 11
        /// 12 
        /// 13 - флаг реле срабатывания (отражает срабатывание хотя бы одной ступени).
        /// 14 - флаг реле неисправности устройства (реле неисправности нормально замкнутое).
        ///      Замыкаем (выключаем), если установлен хотя бы 1 флаг ошибки.
        /// 15 - флаг отображения нажатой и удерживаемой кнопки.
        /// </summary>
        public BitArray Status => new BitArray(BitConverter.GetBytes(this._status));


        /// <summary>
        /// Биты регистра IoStatus.
        /// 0 - состояние измерительного органа (ИО) тока фазы Ia.
        /// 1 - состояние измерительного органа (ИО) тока фазы Ib.
        /// 2 - состояние измерительного органа (ИО) тока фазы Ic.
        /// 3 - состояние измерительного органа (ИО) тока фазы In.
        /// 4 - состояние измерительного органа (ИО) тока фазы In5 (гармоника).
        /// 5 - состояние измерительного органа (ИО) напряжения U0.
        /// 6 - состояние измерительного органа (ИО) энергии EN.
        /// </summary>
        public BitArray IoStatus => new BitArray(BitConverter.GetBytes(this._ioStatus));
        

        /// <summary>
        /// Биты регистра StStatus.
        /// 0 - состояние ступени индикации (СИ) тока фазы Ia.
        /// 1 - состояние ступени индикации (СИ) тока фазы Ib.
        /// 2 - состояние ступени индикации (СИ) тока фазы Ic.
        /// 3 - состояние ступени индикации (СИ) тока фазы In.
        /// 4 - состояние ступени индикации (СИ) тока фазы In5.
        /// 5 - состояние ступени индикации (СИ) напряжения U0.
        /// 6 - режим ступени индикации (СИ) тока In: 0 - выведена,
        ///                                           1 - введена.
        /// 7 - режим ступени индикации (СИ) тока In5 (гармоника): 0 - выведена,
        ///                                                        1 - введена. 
        /// 8 - режим ступени индикации (СИ) напряжения U0: 0 - выведена,
        ///                                                 1 - введена.
        /// </summary>
        public BitArray StStatus => new BitArray(BitConverter.GetBytes(this._stStatus));

        #endregion [Блок 1]

        #region [Блок 2]
        public ushort IaValue => this._iaValue;

        public ushort IbValue => this._ibValue;

        public ushort IcValue => this._icValue;

        public double InValue => Math.Round(this._inValue / Convert.ToDouble(100), 2);

        public double In5Value => Math.Round(this._in5Value / Convert.ToDouble(100), 2);

        public ushort U0Value => this._u0Value;
        #endregion [Блок 2]

        #region [Блок 3]
        public ushort U0Phase => this._u0Phase;

        public ushort InPhase => this._inPhase;

        public ushort InPhaseShift => this._inPhaseShift;
        #endregion [Блок 3]

        #region [Блок 4]
        public ushort IaIoTime => this._iaIoTime;

        public ushort IbIoTime => this._ibIoTime;

        public ushort IcIoTime => this._icIoTime;

        public ushort InIoTime => this._inIoTime;

        public ushort In5IoTime => this._in5IoTime;

        public ushort U0IoTime => this._u0IoTime;
        #endregion [Блок 4]

        #region [Блок 5]
        public int Energy
        {
            get
            {
                if (this._energy > short.MaxValue)
                {
                    BitArray inverseBitArray = new BitArray(Common.GetBitsArray(this._energy)).Not();
                    ushort temp = Common.BitsToUshort(inverseBitArray);
                    temp++;
                    return -temp;
                }
                return this._energy;
            }
        }
        #endregion [Блок 5]

        #region [Блок 6]
        public string CurrentTimeUnix(TimeSettings timeSettings)
        {
            if (this._currentTimeUnixH == 0 && this._currentTimeUnixL == 0) return string.Empty;
            DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(((uint)this._currentTimeUnixH << 16) | this._currentTimeUnixL);
            return dateTimeOffset.DateTime.ToString();
            //return dateTimeOffset.DateTime.AddHours(this.GetHours(timeSettings)).ToString();
        }

        public string CurrentTimeUnixMeasuring(TimeSettings timeSettings)
        {
            if (this._currentTimeUnixH == 0 && this._currentTimeUnixL == 0) return string.Empty;
            DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(((uint)this._currentTimeUnixH << 16) | this._currentTimeUnixL);
            return dateTimeOffset.DateTime.AddHours(this.GetHours(timeSettings)).ToString();
        }
        #endregion [Блок 6]

        #region [Блок 7]

        // в конце

        #endregion [Блок 7]

        #region [Блок 8]
        public ushort VBat => this._vBat;

        public ushort SupplyVoltage => this._supplyVoltage;

        public ushort Temperature => this._temperature;
        #endregion [Блок 8]

        #region [Блок 9]
        public ushort IaLastval => this._iaLastval;

        public string IaLastvalTime(TimeSettings timeSettings)
        {
            if (this._iaLastvalTimeH == 0 && this._iaLastvalTimeL == 0) return "_____";
            DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(Common.UshortUshortToUInt(this._iaLastvalTimeH, this._iaLastvalTimeL));
            return dateTimeOffset.DateTime.AddHours(this.GetHours(timeSettings)).ToString();
        }

        public ushort IbLastval => this._ibLastval;

        public string IbLastvalTime(TimeSettings timeSettings)
        {
            if (this._ibLastvalTimeH == 0 && this._ibLastvalTimeL == 0) return "_____";
            DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(Common.UshortUshortToUInt(this._ibLastvalTimeH, this._ibLastvalTimeL));
            return dateTimeOffset.DateTime.AddHours(this.GetHours(timeSettings)).ToString();
        }

        public ushort IcLastval => this._icLastval;

        public string IcLastvalTime(TimeSettings timeSettings)
        {
            if (this._icLastvalTimeH == 0 && this._icLastvalTimeL == 0) return "_____";
            DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(Common.UshortUshortToUInt(this._icLastvalTimeH, this._icLastvalTimeL));
            return dateTimeOffset.DateTime.AddHours(this.GetHours(timeSettings)).ToString();
        }

        #endregion [Блок 9]

        #region [Блок 10]

        public double InLastvalIn => Math.Round(this._inLastvalIn / Convert.ToDouble(100), 2);

        public double InLastvalIn5 => Math.Round(this._inLastvalIn5 / Convert.ToDouble(100), 2);

        public double InLastvalU0 => this._inLastvalU0;

        public ushort InLastvalAngle => this._inLastvalAngle;

        public string InLastvalTime(TimeSettings timeSettings)
        {
            if (this._inLastvalTimeH == 0 && this._inLastvalTimeL == 0) return "_____";
            DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(Common.UshortUshortToUInt(this._inLastvalTimeH, this._inLastvalTimeL));
            return dateTimeOffset.DateTime.AddHours(this.GetHours(timeSettings)).ToString();
        }

        #endregion [Блок 10]

        #region [Блок 11]
        public double In5LastvalIn => Math.Round(this._in5LastvalIn / Convert.ToDouble(100), 2);

        public double In5LastvalIn5 => Math.Round(this._in5LastvalIn5 / Convert.ToDouble(100), 2);

        public double In5LastvalU0 => this._in5LastvalU0;

        public ushort In5LastvalAngle => this._in5LastvalAngle; 

        public string In5LastvalTime(TimeSettings timeSettings)
        {
            if (this._in5LastvalTimeH == 0 && this._in5LastvalTimeL == 0) return "_____";
            DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(Common.UshortUshortToUInt(this._in5LastvalTimeH, this._in5LastvalTimeL));
            return dateTimeOffset.DateTime.AddHours(this.GetHours(timeSettings)).ToString();
        }
        #endregion [Блок 11]

        #region [Блок 12]
        public double U0LastvalIn => Math.Round(this._u0LastvalIn / Convert.ToDouble(100), 2);

        public double U0LastvalIn5 => Math.Round(this._u0LastvalIn5 / Convert.ToDouble(100), 2);

        public double U0LastvalU0 => this._u0LastvalU0;

        public ushort U0LastvalAngle => this._u0LastvalAngle; 

        public string U0LastvalTime(TimeSettings timeSettings)
        {
            if (this._u0LastvalTimeH == 0 && this._u0LastvalTimeL == 0) return "_____";
            DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(Common.UshortUshortToUInt(this._u0LastvalTimeH, this._u0LastvalTimeL));
            return dateTimeOffset.DateTime.AddHours(this.GetHours(timeSettings)).ToString();
        }

        #endregion [Блок 12]

        #region [Блок 13]
        public int EnergyLastvalIn
        {
            get
            {
                if (this._energyLastvalIn > short.MaxValue)
                {
                    BitArray inverseBitArray = new BitArray(Common.GetBitsArray(this._energyLastvalIn)).Not();
                    ushort temp = Common.BitsToUshort(inverseBitArray);
                    temp++;
                    return -temp;
                }
                return this._energyLastvalIn;
            }
        }

        #endregion [Блок 13]

        #region [Блок 14]
        /// <summary>
        /// Биты регистра BlinkerAlarm.
        /// 0 - Ia.
        /// 1 - Ib.
        /// 2 - Ic.
        /// 3 - In.
        /// 4 - In5 (гармоника).
        /// 5 - U0.
        /// </summary>
        public BitArray BlinkerAlarm => new BitArray(BitConverter.GetBytes(this._blinkerAlarm));
        #endregion [Блок 14]

        #endregion [Properties]

        /// <summary>
        /// Метод, возвращающий количество часов, установленных в настройке времени в конфигурации (от -12 до 12)
        /// </summary>
        public int GetHours(TimeSettings timeSettings)
        {
            int number;
            bool signbool;
            int hour;

            number = Common.GetBits(timeSettings.ForUnixTime, 0, 1, 2, 3); 
            signbool = Common.GetBit(timeSettings.ForUnixTime, 4);

            hour = signbool ? -number : +number; // количество часов
            return hour;
        }
        
        [BindingProperty(0)]
        public ushort CurrentTimeUnixL
        {
            get { return this._currentTimeUnixL; }
            set { this._currentTimeUnixL = value; }
        }

        [BindingProperty(1)]
        public ushort CurrentTimeUnixH
        {
            get { return this._currentTimeUnixH; }
            set { this._currentTimeUnixH = value; }
        }

        [BindingProperty(2)]
        public ushort TimeWord1
        {
            get { return this._currentTimeWord1; }
            set { this._currentTimeWord1 = value; }
        }

        [BindingProperty(3)]
        public ushort TimeWord2
        {
            get { return this._currentTimeWord2; }
            set { this._currentTimeWord2 = value; }
        }

        [BindingProperty(4)]
        public ushort TimeWord3
        {
            get { return this._currentTimeWord3; }
            set { this._currentTimeWord3 = value; }
        }
    }
}