﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.ITKZ_V6.Structures
{
    public class ReleOutputTime : StructBase
    {
        [Layout(0)] private ushort _releOutputTime; // время включения выходного реле (0x0113)

        public bool? IsImpulsMode
        {
            get
            {
                if (this._releOutputTime == 0) return false;
                if (this._releOutputTime >= 100 && this._releOutputTime <= 10000) return true;
                return null;
            }
        }

        public ushort ReleTime
        {
            get => this._releOutputTime;
            set => this._releOutputTime = value;
        }
    }
}
