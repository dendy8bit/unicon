﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.ITKZ_V6.Structures
{
    /// <summary>
    /// Блок регистров калибровочных коэффициентов.
    /// </summary>
    public class CalibrateNominalValues : StructBase
    {
        [Layout(0)] private ushort _ifNomValue; // номинальное значение первичного тока фаз в A (0x0150)
        [Layout(1)] private ushort _inPrimaryKoeff; // номинальное значение первичного тока нуля в A (0x0151)
        [Layout(2)] private ushort _u0NomValue; // номинальное значение напряжения U0 в условных единицах (0x0152)

        [Layout(3)] private ushort _vRef; // величина внутреннего опорного напряжения, используется в рассчетах измеренных напряжений (0x0153)
        // [Layout(4)] private ushort _kalibReserved054; // зарезервировано (калибровки) (0x0154)
        // [Layout(5)] private ushort _kalibReserved055; // зарезервировано (калибровки) (0x0155)
        // [Layout(6)] private ushort _kalibReserved056; // зарезервировано (калибровки) (0x0156)

        [BindingProperty(0)]
        public ushort IfNomValue
        {
            get { return this._ifNomValue; }
            set { this._ifNomValue = value; }
        }

        [BindingProperty(1)]
        public ushort InPrimaryKoeff
        {
            get { return this._inPrimaryKoeff; }
            set { this._inPrimaryKoeff = value; }
        }

        [BindingProperty(2)]
        public ushort U0NomValue
        {
            get { return this._u0NomValue; }
            set { this._u0NomValue = value; }
        }
    }
}