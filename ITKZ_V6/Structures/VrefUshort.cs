﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.ITKZ_V6.Structures
{
    /// <summary>
    /// Внутреннее опорное напряжение
    /// </summary>
    public class VrefUshort : StructBase
    {
        [Layout(0)]
        private ushort _vRef; // величина внутреннего опорного напряжения, используя в рассчетах измеренных напряжений (0x0113) ИСПОЛЬЗУЕТСЯ В КАЛИБРОВКЕ

        [BindingProperty(0)]
        public ushort VRef
        {
            get { return this._vRef; }
            set { this._vRef = value; }
        }
    }
}