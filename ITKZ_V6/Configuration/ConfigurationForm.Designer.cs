﻿namespace BEMN.ITKZ_V6.Configuration
{
    partial class ItkzConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._writeBtn = new System.Windows.Forms.Button();
            this._readFromFileBtn = new System.Windows.Forms.Button();
            this._writeToFileBtn = new System.Windows.Forms.Button();
            this._readBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._readDevNumBtn = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._acceptDeviceSpeedBtn = new System.Windows.Forms.Button();
            this._deviceSpeedCombo = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._deviceNumber = new System.Windows.Forms.MaskedTextBox();
            this._acceptPortNumberBtn = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._findSDFlashBtn = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.groupBoxConfigReleAlarm = new System.Windows.Forms.GroupBox();
            this._releOutputTime = new System.Windows.Forms.MaskedTextBox();
            this._modeReleAlarm = new System.Windows.Forms.MaskedTextBox();
            this.label29 = new System.Windows.Forms.Label();
            this._modReleOutputBtn = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this._modBlinkerBtn = new System.Windows.Forms.Button();
            this._modLedRele = new BEMN.Forms.LedControl();
            this.label59 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this._u0 = new System.Windows.Forms.MaskedTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this._in = new System.Windows.Forms.MaskedTextBox();
            this.updateFirmware = new System.Windows.Forms.TextBox();
            this._if = new System.Windows.Forms.MaskedTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this._updateFirmwareBtn = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this._energyLimit = new System.Windows.Forms.MaskedTextBox();
            this._updateParamCheckBox = new System.Windows.Forms.CheckBox();
            this._updateKvitLabel = new System.Windows.Forms.Label();
            this.GroupBox9 = new System.Windows.Forms.GroupBox();
            this._sectorAngleCombo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this._autoKvitCombo = new System.Windows.Forms.ComboBox();
            this._aoutoKvitLabel = new System.Windows.Forms.Label();
            this._versionLabel = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._phase = new System.Windows.Forms.MaskedTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._u0Limit = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this._tu0 = new System.Windows.Forms.MaskedTextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this._icTimeLimit = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this._ibTimeLimit = new System.Windows.Forms.MaskedTextBox();
            this._in5TimeLimit = new System.Windows.Forms.MaskedTextBox();
            this._iaTimeLimit = new System.Windows.Forms.MaskedTextBox();
            this._inTimeLimit = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._in5Limit = new System.Windows.Forms.MaskedTextBox();
            this._inLimit = new System.Windows.Forms.MaskedTextBox();
            this._icLimit = new System.Windows.Forms.MaskedTextBox();
            this._ibLimit = new System.Windows.Forms.MaskedTextBox();
            this._iaLimit = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._autoTranzitionCheck = new System.Windows.Forms.CheckBox();
            this._signCorrectionModuleCombo = new System.Windows.Forms.ComboBox();
            this._timeZoneCombo = new System.Windows.Forms.ComboBox();
            this._signTimeZoneCombo = new System.Windows.Forms.ComboBox();
            this._clockCorrectionModuleCombo = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this._acceptTimeSettingsBtn = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._currentTime = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this._time = new System.Windows.Forms.MaskedTextBox();
            this._date = new System.Windows.Forms.MaskedTextBox();
            this._newTimeBtn = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this._unixTimeL = new System.Windows.Forms.MaskedTextBox();
            this._unixTimeH = new System.Windows.Forms.MaskedTextBox();
            this._currentTimeWord3 = new System.Windows.Forms.MaskedTextBox();
            this._currentTimeWord2 = new System.Windows.Forms.MaskedTextBox();
            this._currentTimeWord1 = new System.Windows.Forms.MaskedTextBox();
            this._unixTime = new System.Windows.Forms.MaskedTextBox();
            this.openFileDialogSDFlash = new System.Windows.Forms.OpenFileDialog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBoxConfigReleAlarm.SuspendLayout();
            this.GroupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // _writeBtn
            // 
            this._writeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeBtn.Location = new System.Drawing.Point(6, 522);
            this._writeBtn.Name = "_writeBtn";
            this._writeBtn.Size = new System.Drawing.Size(194, 23);
            this._writeBtn.TabIndex = 1;
            this._writeBtn.Text = "Записать уставки в устройство";
            this.toolTip1.SetToolTip(this._writeBtn, "Записать конфигурацию в устройство (CTRL+W)");
            this._writeBtn.UseVisualStyleBackColor = true;
            this._writeBtn.Click += new System.EventHandler(this._writeBtn_Click);
            // 
            // _readFromFileBtn
            // 
            this._readFromFileBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readFromFileBtn.Location = new System.Drawing.Point(261, 493);
            this._readFromFileBtn.Name = "_readFromFileBtn";
            this._readFromFileBtn.Size = new System.Drawing.Size(200, 23);
            this._readFromFileBtn.TabIndex = 6;
            this._readFromFileBtn.Text = "Загрузить конфигурацию из файла";
            this.toolTip1.SetToolTip(this._readFromFileBtn, "Загрузить конфигурацию из файла (CTRL+O)");
            this._readFromFileBtn.UseVisualStyleBackColor = true;
            this._readFromFileBtn.Click += new System.EventHandler(this._readFromFileBtn_Click);
            // 
            // _writeToFileBtn
            // 
            this._writeToFileBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeToFileBtn.Location = new System.Drawing.Point(261, 522);
            this._writeToFileBtn.Name = "_writeToFileBtn";
            this._writeToFileBtn.Size = new System.Drawing.Size(200, 23);
            this._writeToFileBtn.TabIndex = 6;
            this._writeToFileBtn.Text = "Сохранить конфигурацию в файл";
            this.toolTip1.SetToolTip(this._writeToFileBtn, "Сохранить конфигурацию в файл (CTRL+S)");
            this._writeToFileBtn.UseVisualStyleBackColor = true;
            this._writeToFileBtn.Click += new System.EventHandler(this._writeToFileBtn_Click);
            // 
            // _readBtn
            // 
            this._readBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readBtn.Location = new System.Drawing.Point(6, 493);
            this._readBtn.Name = "_readBtn";
            this._readBtn.Size = new System.Drawing.Size(194, 23);
            this._readBtn.TabIndex = 6;
            this._readBtn.Text = "Прочитать уставки из устройства";
            this.toolTip1.SetToolTip(this._readBtn, "Прочитать конфигурацию из устройства (CTRL+R)");
            this._readBtn.UseVisualStyleBackColor = true;
            this._readBtn.Click += new System.EventHandler(this._readBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._readDevNumBtn);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Location = new System.Drawing.Point(479, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(93, 217);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Параметры";
            // 
            // _readDevNumBtn
            // 
            this._readDevNumBtn.Location = new System.Drawing.Point(9, 186);
            this._readDevNumBtn.Name = "_readDevNumBtn";
            this._readDevNumBtn.Size = new System.Drawing.Size(75, 23);
            this._readDevNumBtn.TabIndex = 9;
            this._readDevNumBtn.Text = "Прочитать";
            this._readDevNumBtn.UseVisualStyleBackColor = true;
            this._readDevNumBtn.Click += new System.EventHandler(this._readDevNumBtn_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._acceptDeviceSpeedBtn);
            this.groupBox4.Controls.Add(this._deviceSpeedCombo);
            this.groupBox4.Location = new System.Drawing.Point(6, 103);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(81, 72);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Скорость";
            // 
            // _acceptDeviceSpeedBtn
            // 
            this._acceptDeviceSpeedBtn.Location = new System.Drawing.Point(3, 43);
            this._acceptDeviceSpeedBtn.Name = "_acceptDeviceSpeedBtn";
            this._acceptDeviceSpeedBtn.Size = new System.Drawing.Size(75, 23);
            this._acceptDeviceSpeedBtn.TabIndex = 6;
            this._acceptDeviceSpeedBtn.Text = "Установить";
            this._acceptDeviceSpeedBtn.UseVisualStyleBackColor = true;
            this._acceptDeviceSpeedBtn.Click += new System.EventHandler(this._acceptDeviceSpeedBtn_Click);
            // 
            // _deviceSpeedCombo
            // 
            this._deviceSpeedCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._deviceSpeedCombo.FormattingEnabled = true;
            this._deviceSpeedCombo.Location = new System.Drawing.Point(6, 19);
            this._deviceSpeedCombo.Name = "_deviceSpeedCombo";
            this._deviceSpeedCombo.Size = new System.Drawing.Size(69, 21);
            this._deviceSpeedCombo.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._deviceNumber);
            this.groupBox3.Controls.Add(this._acceptPortNumberBtn);
            this.groupBox3.Location = new System.Drawing.Point(6, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(81, 79);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Номер";
            // 
            // _deviceNumber
            // 
            this._deviceNumber.Location = new System.Drawing.Point(6, 19);
            this._deviceNumber.Mask = "000";
            this._deviceNumber.Name = "_deviceNumber";
            this._deviceNumber.PromptChar = ' ';
            this._deviceNumber.Size = new System.Drawing.Size(69, 20);
            this._deviceNumber.TabIndex = 1;
            // 
            // _acceptPortNumberBtn
            // 
            this._acceptPortNumberBtn.Location = new System.Drawing.Point(3, 44);
            this._acceptPortNumberBtn.Name = "_acceptPortNumberBtn";
            this._acceptPortNumberBtn.Size = new System.Drawing.Size(75, 23);
            this._acceptPortNumberBtn.TabIndex = 0;
            this._acceptPortNumberBtn.Text = "Установить";
            this._acceptPortNumberBtn.UseVisualStyleBackColor = true;
            this._acceptPortNumberBtn.Click += new System.EventHandler(this._acceptPortNumberBtn_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "ИТКЗ уставки";
            this.openFileDialog.Filter = "(*.xml) | *.xml";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.FileName = "ИТКЗ уставки";
            this.saveFileDialog.Filter = "(*.xml) | *.xml";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._findSDFlashBtn);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.groupBoxConfigReleAlarm);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this._u0);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this._in);
            this.groupBox1.Controls.Add(this.updateFirmware);
            this.groupBox1.Controls.Add(this._if);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this._updateFirmwareBtn);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this._energyLimit);
            this.groupBox1.Controls.Add(this._updateParamCheckBox);
            this.groupBox1.Controls.Add(this._updateKvitLabel);
            this.groupBox1.Controls.Add(this.GroupBox9);
            this.groupBox1.Controls.Add(this._autoKvitCombo);
            this.groupBox1.Controls.Add(this._aoutoKvitLabel);
            this.groupBox1.Controls.Add(this._versionLabel);
            this.groupBox1.Controls.Add(this.groupBox8);
            this.groupBox1.Controls.Add(this.groupBox7);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this._readFromFileBtn);
            this.groupBox1.Controls.Add(this._writeToFileBtn);
            this.groupBox1.Controls.Add(this._writeBtn);
            this.groupBox1.Controls.Add(this._readBtn);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(467, 551);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Уставки";
            // 
            // _findSDFlashBtn
            // 
            this._findSDFlashBtn.Location = new System.Drawing.Point(376, 246);
            this._findSDFlashBtn.Name = "_findSDFlashBtn";
            this._findSDFlashBtn.Size = new System.Drawing.Size(83, 23);
            this._findSDFlashBtn.TabIndex = 58;
            this._findSDFlashBtn.Text = "Указать путь";
            this._findSDFlashBtn.UseVisualStyleBackColor = true;
            this._findSDFlashBtn.Click += new System.EventHandler(this._findSDFlashBtn_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(317, 352);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(14, 13);
            this.label26.TabIndex = 57;
            this.label26.Text = "V";
            // 
            // groupBoxConfigReleAlarm
            // 
            this.groupBoxConfigReleAlarm.Controls.Add(this._releOutputTime);
            this.groupBoxConfigReleAlarm.Controls.Add(this._modeReleAlarm);
            this.groupBoxConfigReleAlarm.Controls.Add(this.label29);
            this.groupBoxConfigReleAlarm.Controls.Add(this._modReleOutputBtn);
            this.groupBoxConfigReleAlarm.Controls.Add(this.label30);
            this.groupBoxConfigReleAlarm.Controls.Add(this._modBlinkerBtn);
            this.groupBoxConfigReleAlarm.Controls.Add(this._modLedRele);
            this.groupBoxConfigReleAlarm.Controls.Add(this.label59);
            this.groupBoxConfigReleAlarm.Location = new System.Drawing.Point(6, 375);
            this.groupBoxConfigReleAlarm.Name = "groupBoxConfigReleAlarm";
            this.groupBoxConfigReleAlarm.Size = new System.Drawing.Size(276, 112);
            this.groupBoxConfigReleAlarm.TabIndex = 49;
            this.groupBoxConfigReleAlarm.TabStop = false;
            this.groupBoxConfigReleAlarm.Text = "Режим работы реле аварий";
            // 
            // _releOutputTime
            // 
            this._releOutputTime.Location = new System.Drawing.Point(193, 44);
            this._releOutputTime.Mask = "000000";
            this._releOutputTime.Name = "_releOutputTime";
            this._releOutputTime.PromptChar = ' ';
            this._releOutputTime.Size = new System.Drawing.Size(49, 20);
            this._releOutputTime.TabIndex = 32;
            // 
            // _modeReleAlarm
            // 
            this._modeReleAlarm.Location = new System.Drawing.Point(8, 19);
            this._modeReleAlarm.Name = "_modeReleAlarm";
            this._modeReleAlarm.PromptChar = ' ';
            this._modeReleAlarm.ReadOnly = true;
            this._modeReleAlarm.Size = new System.Drawing.Size(83, 20);
            this._modeReleAlarm.TabIndex = 20;
            // 
            // label29
            // 
            this.label29.Location = new System.Drawing.Point(248, 46);
            this.label29.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(21, 13);
            this.label29.TabIndex = 22;
            this.label29.Text = "мс";
            // 
            // _modReleOutputBtn
            // 
            this._modReleOutputBtn.Location = new System.Drawing.Point(152, 78);
            this._modReleOutputBtn.Name = "_modReleOutputBtn";
            this._modReleOutputBtn.Size = new System.Drawing.Size(117, 23);
            this._modReleOutputBtn.TabIndex = 19;
            this._modReleOutputBtn.Text = "Импульсный";
            this._modReleOutputBtn.UseVisualStyleBackColor = true;
            this._modReleOutputBtn.Click += new System.EventHandler(this._modReleOutputBtn_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 47);
            this.label30.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(181, 13);
            this.label30.TabIndex = 3;
            this.label30.Text = "Время управления выключателем";
            // 
            // _modBlinkerBtn
            // 
            this._modBlinkerBtn.Location = new System.Drawing.Point(6, 78);
            this._modBlinkerBtn.Name = "_modBlinkerBtn";
            this._modBlinkerBtn.Size = new System.Drawing.Size(117, 24);
            this._modBlinkerBtn.TabIndex = 18;
            this._modBlinkerBtn.Text = "Блинкера";
            this._modBlinkerBtn.UseVisualStyleBackColor = true;
            this._modBlinkerBtn.Click += new System.EventHandler(this._modBlinkerBtn_Click);
            // 
            // _modLedRele
            // 
            this._modLedRele.Location = new System.Drawing.Point(95, 23);
            this._modLedRele.Name = "_modLedRele";
            this._modLedRele.Size = new System.Drawing.Size(13, 13);
            this._modLedRele.State = BEMN.Forms.LedState.Off;
            this._modLedRele.TabIndex = 10;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(108, 14);
            this.label59.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(0, 13);
            this.label59.TabIndex = 3;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(317, 329);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(14, 13);
            this.label25.TabIndex = 56;
            this.label25.Text = "A";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(317, 307);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(14, 13);
            this.label24.TabIndex = 55;
            this.label24.Text = "A";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(11, 352);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(212, 13);
            this.label23.TabIndex = 54;
            this.label23.Text = "Номинальное значение напряжения U0 ";
            // 
            // _u0
            // 
            this._u0.Location = new System.Drawing.Point(259, 349);
            this._u0.Mask = "0000000";
            this._u0.Name = "_u0";
            this._u0.PromptChar = ' ';
            this._u0.Size = new System.Drawing.Size(55, 20);
            this._u0.TabIndex = 51;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(11, 329);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(240, 13);
            this.label21.TabIndex = 53;
            this.label21.Text = "Номинальное значение первичного тока нуля";
            // 
            // _in
            // 
            this._in.Location = new System.Drawing.Point(259, 326);
            this._in.Mask = "0000000";
            this._in.Name = "_in";
            this._in.PromptChar = ' ';
            this._in.Size = new System.Drawing.Size(55, 20);
            this._in.TabIndex = 50;
            // 
            // updateFirmware
            // 
            this.updateFirmware.Location = new System.Drawing.Point(259, 223);
            this.updateFirmware.Name = "updateFirmware";
            this.updateFirmware.Size = new System.Drawing.Size(200, 20);
            this.updateFirmware.TabIndex = 16;
            this.updateFirmware.Text = "z:\\!! Прошивки\\Лапунов\\sdflash\\bin\\SDFlash.exe";
            // 
            // _if
            // 
            this._if.Location = new System.Drawing.Point(259, 303);
            this._if.Mask = "0000000";
            this._if.Name = "_if";
            this._if.PromptChar = ' ';
            this._if.Size = new System.Drawing.Size(55, 20);
            this._if.TabIndex = 49;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 307);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(240, 13);
            this.label22.TabIndex = 52;
            this.label22.Text = "Номинальное значение первичного тока фаз ";
            // 
            // _updateFirmwareBtn
            // 
            this._updateFirmwareBtn.Location = new System.Drawing.Point(320, 271);
            this._updateFirmwareBtn.Name = "_updateFirmwareBtn";
            this._updateFirmwareBtn.Size = new System.Drawing.Size(139, 23);
            this._updateFirmwareBtn.TabIndex = 9;
            this._updateFirmwareBtn.Text = "Обновление прошивки";
            this._updateFirmwareBtn.UseVisualStyleBackColor = true;
            this._updateFirmwareBtn.Click += new System.EventHandler(this._updateFirmwareBtn_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 196);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(94, 13);
            this.label17.TabIndex = 14;
            this.label17.Text = "Уставка энергии";
            // 
            // _energyLimit
            // 
            this._energyLimit.Location = new System.Drawing.Point(113, 193);
            this._energyLimit.Mask = "0000000";
            this._energyLimit.Name = "_energyLimit";
            this._energyLimit.PromptChar = ' ';
            this._energyLimit.Size = new System.Drawing.Size(55, 20);
            this._energyLimit.TabIndex = 13;
            // 
            // _updateParamCheckBox
            // 
            this._updateParamCheckBox.AutoSize = true;
            this._updateParamCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._updateParamCheckBox.Location = new System.Drawing.Point(238, 273);
            this._updateParamCheckBox.Name = "_updateParamCheckBox";
            this._updateParamCheckBox.Size = new System.Drawing.Size(15, 14);
            this._updateParamCheckBox.TabIndex = 15;
            this._updateParamCheckBox.UseVisualStyleBackColor = true;
            // 
            // _updateKvitLabel
            // 
            this._updateKvitLabel.AutoSize = true;
            this._updateKvitLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._updateKvitLabel.Location = new System.Drawing.Point(7, 271);
            this._updateKvitLabel.Name = "_updateKvitLabel";
            this._updateKvitLabel.Size = new System.Drawing.Size(173, 26);
            this._updateKvitLabel.TabIndex = 14;
            this._updateKvitLabel.Text = "Обнуление аварийных значений \r\nпри квитировании";
            // 
            // GroupBox9
            // 
            this.GroupBox9.Controls.Add(this._sectorAngleCombo);
            this.GroupBox9.Controls.Add(this.label14);
            this.GroupBox9.Location = new System.Drawing.Point(259, 154);
            this.GroupBox9.Name = "GroupBox9";
            this.GroupBox9.Size = new System.Drawing.Size(200, 63);
            this.GroupBox9.TabIndex = 13;
            this.GroupBox9.TabStop = false;
            this.GroupBox9.Text = "Уставка ширины сектора направленного срабатывания";
            // 
            // _sectorAngleCombo
            // 
            this._sectorAngleCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sectorAngleCombo.FormattingEnabled = true;
            this._sectorAngleCombo.Location = new System.Drawing.Point(93, 34);
            this._sectorAngleCombo.Name = "_sectorAngleCombo";
            this._sectorAngleCombo.Size = new System.Drawing.Size(69, 21);
            this._sectorAngleCombo.TabIndex = 17;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 37);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "Угол, градусы";
            // 
            // _autoKvitCombo
            // 
            this._autoKvitCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._autoKvitCombo.FormattingEnabled = true;
            this._autoKvitCombo.Location = new System.Drawing.Point(147, 246);
            this._autoKvitCombo.Name = "_autoKvitCombo";
            this._autoKvitCombo.Size = new System.Drawing.Size(106, 21);
            this._autoKvitCombo.TabIndex = 12;
            // 
            // _aoutoKvitLabel
            // 
            this._aoutoKvitLabel.AutoSize = true;
            this._aoutoKvitLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this._aoutoKvitLabel.Location = new System.Drawing.Point(7, 246);
            this._aoutoKvitLabel.Name = "_aoutoKvitLabel";
            this._aoutoKvitLabel.Size = new System.Drawing.Size(134, 16);
            this._aoutoKvitLabel.TabIndex = 11;
            this._aoutoKvitLabel.Text = "Автоквитирование";
            // 
            // _versionLabel
            // 
            this._versionLabel.AutoSize = true;
            this._versionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._versionLabel.Location = new System.Drawing.Point(6, 223);
            this._versionLabel.Name = "_versionLabel";
            this._versionLabel.Size = new System.Drawing.Size(78, 16);
            this._versionLabel.TabIndex = 8;
            this._versionLabel.Text = "Версия ПО";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._phase);
            this.groupBox8.Controls.Add(this.label13);
            this.groupBox8.Location = new System.Drawing.Point(259, 91);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(200, 62);
            this.groupBox8.TabIndex = 10;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Уставка угла положительного направления мощности";
            // 
            // _phase
            // 
            this._phase.Location = new System.Drawing.Point(91, 32);
            this._phase.Mask = "CCCCC";
            this._phase.Name = "_phase";
            this._phase.PromptChar = ' ';
            this._phase.Size = new System.Drawing.Size(50, 20);
            this._phase.TabIndex = 16;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 35);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 13);
            this.label13.TabIndex = 15;
            this.label13.Text = "Угол, градусы";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._u0Limit);
            this.groupBox7.Controls.Add(this.label11);
            this.groupBox7.Controls.Add(this.label12);
            this.groupBox7.Controls.Add(this._tu0);
            this.groupBox7.Location = new System.Drawing.Point(259, 19);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(200, 72);
            this.groupBox7.TabIndex = 10;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Уставки срабатывания ИО по значению напряжения U0";
            // 
            // _u0Limit
            // 
            this._u0Limit.Location = new System.Drawing.Point(91, 27);
            this._u0Limit.Mask = "CCCCC";
            this._u0Limit.Name = "_u0Limit";
            this._u0Limit.PromptChar = ' ';
            this._u0Limit.Size = new System.Drawing.Size(50, 20);
            this._u0Limit.TabIndex = 10;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Уставка U0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 49);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Т, мс";
            // 
            // _tu0
            // 
            this._tu0.Location = new System.Drawing.Point(91, 46);
            this._tu0.Mask = "0000000";
            this._tu0.Name = "_tu0";
            this._tu0.PromptChar = ' ';
            this._tu0.Size = new System.Drawing.Size(50, 20);
            this._tu0.TabIndex = 13;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this._icTimeLimit);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this._ibTimeLimit);
            this.groupBox5.Controls.Add(this._in5TimeLimit);
            this.groupBox5.Controls.Add(this._iaTimeLimit);
            this.groupBox5.Controls.Add(this._inTimeLimit);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Location = new System.Drawing.Point(120, 19);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(133, 166);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Уставки времени срабатывания ступени индикации по току фазы, мс";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(22, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "In5";
            // 
            // _icTimeLimit
            // 
            this._icTimeLimit.Location = new System.Drawing.Point(37, 96);
            this._icTimeLimit.Mask = "0000000";
            this._icTimeLimit.Name = "_icTimeLimit";
            this._icTimeLimit.PromptChar = ' ';
            this._icTimeLimit.Size = new System.Drawing.Size(53, 20);
            this._icTimeLimit.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 121);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "In";
            // 
            // _ibTimeLimit
            // 
            this._ibTimeLimit.Location = new System.Drawing.Point(37, 74);
            this._ibTimeLimit.Mask = "0000000";
            this._ibTimeLimit.Name = "_ibTimeLimit";
            this._ibTimeLimit.PromptChar = ' ';
            this._ibTimeLimit.Size = new System.Drawing.Size(53, 20);
            this._ibTimeLimit.TabIndex = 13;
            // 
            // _in5TimeLimit
            // 
            this._in5TimeLimit.Location = new System.Drawing.Point(37, 140);
            this._in5TimeLimit.Mask = "0000000";
            this._in5TimeLimit.Name = "_in5TimeLimit";
            this._in5TimeLimit.PromptChar = ' ';
            this._in5TimeLimit.Size = new System.Drawing.Size(53, 20);
            this._in5TimeLimit.TabIndex = 14;
            // 
            // _iaTimeLimit
            // 
            this._iaTimeLimit.Location = new System.Drawing.Point(37, 52);
            this._iaTimeLimit.Mask = "0000000";
            this._iaTimeLimit.Name = "_iaTimeLimit";
            this._iaTimeLimit.PromptChar = ' ';
            this._iaTimeLimit.Size = new System.Drawing.Size(53, 20);
            this._iaTimeLimit.TabIndex = 10;
            // 
            // _inTimeLimit
            // 
            this._inTimeLimit.Location = new System.Drawing.Point(37, 118);
            this._inTimeLimit.Mask = "0000000";
            this._inTimeLimit.Name = "_inTimeLimit";
            this._inTimeLimit.PromptChar = ' ';
            this._inTimeLimit.Size = new System.Drawing.Size(53, 20);
            this._inTimeLimit.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Ic";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Ib";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Ia";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this._in5Limit);
            this.groupBox6.Controls.Add(this._inLimit);
            this.groupBox6.Controls.Add(this._icLimit);
            this.groupBox6.Controls.Add(this._ibLimit);
            this.groupBox6.Controls.Add(this._iaLimit);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Location = new System.Drawing.Point(6, 19);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(108, 166);
            this.groupBox6.TabIndex = 8;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Уставки срабатывания от макс. значения, (0,01...1) Imax";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 144);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "In5";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "In";
            // 
            // _in5Limit
            // 
            this._in5Limit.Location = new System.Drawing.Point(37, 140);
            this._in5Limit.Name = "_in5Limit";
            this._in5Limit.PromptChar = ' ';
            this._in5Limit.Size = new System.Drawing.Size(53, 20);
            this._in5Limit.TabIndex = 10;
            // 
            // _inLimit
            // 
            this._inLimit.Location = new System.Drawing.Point(37, 118);
            this._inLimit.Name = "_inLimit";
            this._inLimit.PromptChar = ' ';
            this._inLimit.Size = new System.Drawing.Size(53, 20);
            this._inLimit.TabIndex = 9;
            // 
            // _icLimit
            // 
            this._icLimit.Location = new System.Drawing.Point(37, 96);
            this._icLimit.Name = "_icLimit";
            this._icLimit.PromptChar = ' ';
            this._icLimit.Size = new System.Drawing.Size(53, 20);
            this._icLimit.TabIndex = 8;
            // 
            // _ibLimit
            // 
            this._ibLimit.Location = new System.Drawing.Point(37, 74);
            this._ibLimit.Name = "_ibLimit";
            this._ibLimit.PromptChar = ' ';
            this._ibLimit.Size = new System.Drawing.Size(53, 20);
            this._ibLimit.TabIndex = 7;
            // 
            // _iaLimit
            // 
            this._iaLimit.Location = new System.Drawing.Point(37, 52);
            this._iaLimit.Name = "_iaLimit";
            this._iaLimit.PromptChar = ' ';
            this._iaLimit.Size = new System.Drawing.Size(53, 20);
            this._iaLimit.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ic";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ib";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ia";
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.readFromFileItem,
            this.writeToFileItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 92);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "Прочитать из устройства";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "Записать в устройство";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "Загрузить из файл";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "Записать в файл";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.groupBox12);
            this.groupBox10.Controls.Add(this.groupBox11);
            this.groupBox10.Location = new System.Drawing.Point(6, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(408, 145);
            this.groupBox10.TabIndex = 8;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Настройка часов";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._autoTranzitionCheck);
            this.groupBox12.Controls.Add(this._signCorrectionModuleCombo);
            this.groupBox12.Controls.Add(this._timeZoneCombo);
            this.groupBox12.Controls.Add(this._signTimeZoneCombo);
            this.groupBox12.Controls.Add(this._clockCorrectionModuleCombo);
            this.groupBox12.Controls.Add(this.label16);
            this.groupBox12.Controls.Add(this.label15);
            this.groupBox12.Controls.Add(this._acceptTimeSettingsBtn);
            this.groupBox12.Location = new System.Drawing.Point(6, 12);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(271, 127);
            this.groupBox12.TabIndex = 49;
            this.groupBox12.TabStop = false;
            // 
            // _autoTranzitionCheck
            // 
            this._autoTranzitionCheck.AutoSize = true;
            this._autoTranzitionCheck.Location = new System.Drawing.Point(8, 15);
            this._autoTranzitionCheck.Name = "_autoTranzitionCheck";
            this._autoTranzitionCheck.Size = new System.Drawing.Size(242, 17);
            this._autoTranzitionCheck.TabIndex = 9;
            this._autoTranzitionCheck.Text = "Автоматический переход на летнее время";
            this._autoTranzitionCheck.UseVisualStyleBackColor = true;
            // 
            // _signCorrectionModuleCombo
            // 
            this._signCorrectionModuleCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._signCorrectionModuleCombo.FormattingEnabled = true;
            this._signCorrectionModuleCombo.Location = new System.Drawing.Point(171, 71);
            this._signCorrectionModuleCombo.Name = "_signCorrectionModuleCombo";
            this._signCorrectionModuleCombo.Size = new System.Drawing.Size(33, 21);
            this._signCorrectionModuleCombo.TabIndex = 18;
            // 
            // _timeZoneCombo
            // 
            this._timeZoneCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._timeZoneCombo.FormattingEnabled = true;
            this._timeZoneCombo.Location = new System.Drawing.Point(210, 44);
            this._timeZoneCombo.Name = "_timeZoneCombo";
            this._timeZoneCombo.Size = new System.Drawing.Size(39, 21);
            this._timeZoneCombo.TabIndex = 16;
            // 
            // _signTimeZoneCombo
            // 
            this._signTimeZoneCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._signTimeZoneCombo.FormattingEnabled = true;
            this._signTimeZoneCombo.Location = new System.Drawing.Point(171, 44);
            this._signTimeZoneCombo.Name = "_signTimeZoneCombo";
            this._signTimeZoneCombo.Size = new System.Drawing.Size(33, 21);
            this._signTimeZoneCombo.TabIndex = 17;
            // 
            // _clockCorrectionModuleCombo
            // 
            this._clockCorrectionModuleCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._clockCorrectionModuleCombo.FormattingEnabled = true;
            this._clockCorrectionModuleCombo.Location = new System.Drawing.Point(210, 71);
            this._clockCorrectionModuleCombo.Name = "_clockCorrectionModuleCombo";
            this._clockCorrectionModuleCombo.Size = new System.Drawing.Size(39, 21);
            this._clockCorrectionModuleCombo.TabIndex = 17;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 74);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(163, 13);
            this.label16.TabIndex = 19;
            this.label16.Text = "Модуль коррекции хода часов:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 47);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(140, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Значение часового пояса:";
            // 
            // _acceptTimeSettingsBtn
            // 
            this._acceptTimeSettingsBtn.Location = new System.Drawing.Point(7, 97);
            this._acceptTimeSettingsBtn.Name = "_acceptTimeSettingsBtn";
            this._acceptTimeSettingsBtn.Size = new System.Drawing.Size(97, 23);
            this._acceptTimeSettingsBtn.TabIndex = 0;
            this._acceptTimeSettingsBtn.Text = "Установить";
            this._acceptTimeSettingsBtn.UseVisualStyleBackColor = true;
            this._acceptTimeSettingsBtn.Click += new System.EventHandler(this._acceptTimeSettingsBtn_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._currentTime);
            this.groupBox11.Controls.Add(this.label19);
            this.groupBox11.Controls.Add(this.label20);
            this.groupBox11.Controls.Add(this._time);
            this.groupBox11.Controls.Add(this._date);
            this.groupBox11.Controls.Add(this._newTimeBtn);
            this.groupBox11.Location = new System.Drawing.Point(283, 12);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(117, 127);
            this.groupBox11.TabIndex = 49;
            this.groupBox11.TabStop = false;
            // 
            // _currentTime
            // 
            this._currentTime.AutoSize = true;
            this._currentTime.Location = new System.Drawing.Point(6, 12);
            this._currentTime.Name = "_currentTime";
            this._currentTime.Size = new System.Drawing.Size(106, 13);
            this._currentTime.TabIndex = 49;
            this._currentTime.Text = "00.00.0000 00:00:00";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(5, 36);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(36, 13);
            this.label19.TabIndex = 33;
            this.label19.Text = "Дата:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(5, 62);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(43, 13);
            this.label20.TabIndex = 34;
            this.label20.Text = "Время:";
            // 
            // _time
            // 
            this._time.Location = new System.Drawing.Point(58, 59);
            this._time.Mask = "00:00:00  ";
            this._time.Name = "_time";
            this._time.Size = new System.Drawing.Size(51, 20);
            this._time.TabIndex = 32;
            // 
            // _date
            // 
            this._date.Location = new System.Drawing.Point(45, 33);
            this._date.Mask = "00/00/0000";
            this._date.Name = "_date";
            this._date.Size = new System.Drawing.Size(64, 20);
            this._date.TabIndex = 14;
            // 
            // _newTimeBtn
            // 
            this._newTimeBtn.Location = new System.Drawing.Point(8, 97);
            this._newTimeBtn.Name = "_newTimeBtn";
            this._newTimeBtn.Size = new System.Drawing.Size(101, 23);
            this._newTimeBtn.TabIndex = 35;
            this._newTimeBtn.Text = "Установить";
            this._newTimeBtn.UseVisualStyleBackColor = true;
            this._newTimeBtn.Click += new System.EventHandler(this._newTimeBtn_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(482, 236);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 13);
            this.label18.TabIndex = 9;
            this.label18.Text = "Для МР-сети:";
            this.label18.Visible = false;
            // 
            // _unixTimeL
            // 
            this._unixTimeL.Location = new System.Drawing.Point(493, 302);
            this._unixTimeL.Mask = "00000000000000";
            this._unixTimeL.Name = "_unixTimeL";
            this._unixTimeL.PromptChar = ' ';
            this._unixTimeL.Size = new System.Drawing.Size(64, 20);
            this._unixTimeL.TabIndex = 43;
            this._unixTimeL.Visible = false;
            // 
            // _unixTimeH
            // 
            this._unixTimeH.Location = new System.Drawing.Point(493, 328);
            this._unixTimeH.Mask = "00000000000000";
            this._unixTimeH.Name = "_unixTimeH";
            this._unixTimeH.PromptChar = ' ';
            this._unixTimeH.Size = new System.Drawing.Size(64, 20);
            this._unixTimeH.TabIndex = 44;
            this._unixTimeH.Visible = false;
            // 
            // _currentTimeWord3
            // 
            this._currentTimeWord3.Location = new System.Drawing.Point(493, 420);
            this._currentTimeWord3.Mask = "00000000000000";
            this._currentTimeWord3.Name = "_currentTimeWord3";
            this._currentTimeWord3.PromptChar = ' ';
            this._currentTimeWord3.Size = new System.Drawing.Size(64, 20);
            this._currentTimeWord3.TabIndex = 45;
            this._currentTimeWord3.Visible = false;
            // 
            // _currentTimeWord2
            // 
            this._currentTimeWord2.Location = new System.Drawing.Point(493, 394);
            this._currentTimeWord2.Mask = "00000000000000";
            this._currentTimeWord2.Name = "_currentTimeWord2";
            this._currentTimeWord2.PromptChar = ' ';
            this._currentTimeWord2.Size = new System.Drawing.Size(64, 20);
            this._currentTimeWord2.TabIndex = 46;
            this._currentTimeWord2.Visible = false;
            // 
            // _currentTimeWord1
            // 
            this._currentTimeWord1.Location = new System.Drawing.Point(493, 362);
            this._currentTimeWord1.Mask = "00000000000000";
            this._currentTimeWord1.Name = "_currentTimeWord1";
            this._currentTimeWord1.PromptChar = ' ';
            this._currentTimeWord1.Size = new System.Drawing.Size(64, 20);
            this._currentTimeWord1.TabIndex = 47;
            this._currentTimeWord1.Visible = false;
            // 
            // _unixTime
            // 
            this._unixTime.Location = new System.Drawing.Point(493, 267);
            this._unixTime.Mask = "00000000000000";
            this._unixTime.Name = "_unixTime";
            this._unixTime.PromptChar = ' ';
            this._unixTime.Size = new System.Drawing.Size(64, 20);
            this._unixTime.TabIndex = 48;
            this._unixTime.Visible = false;
            // 
            // openFileDialogSDFlash
            // 
            this.openFileDialogSDFlash.Filter = "(*.exe) | *.exe";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(6, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(587, 588);
            this.tabControl1.TabIndex = 50;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this._unixTime);
            this.tabPage1.Controls.Add(this._unixTimeL);
            this.tabPage1.Controls.Add(this._currentTimeWord1);
            this.tabPage1.Controls.Add(this._unixTimeH);
            this.tabPage1.Controls.Add(this._currentTimeWord2);
            this.tabPage1.Controls.Add(this._currentTimeWord3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(579, 562);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Конфигурация";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.groupBox10);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(579, 562);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Настройка часов";
            // 
            // ItkzConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 599);
            this.ContextMenuStrip = this.contextMenu;
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ItkzConfigurationForm";
            this.Text = "ConfigurationForm";
            this.Activated += new System.EventHandler(this.ItkzConfigurationForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ItkzConfigurationForm_FormClosing);
            this.Load += new System.EventHandler(this.ItkzConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ITKZConfigurationForm_KeyUp);
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxConfigReleAlarm.ResumeLayout(false);
            this.groupBoxConfigReleAlarm.PerformLayout();
            this.GroupBox9.ResumeLayout(false);
            this.GroupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.contextMenu.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _readBtn;
        private System.Windows.Forms.Button _writeBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button _acceptPortNumberBtn;
        private System.Windows.Forms.MaskedTextBox _deviceNumber;
        private System.Windows.Forms.Button _readFromFileBtn;
        private System.Windows.Forms.Button _writeToFileBtn;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox _deviceSpeedCombo;
        private System.Windows.Forms.Button _acceptDeviceSpeedBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox _icTimeLimit;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox _ibTimeLimit;
        private System.Windows.Forms.MaskedTextBox _in5TimeLimit;
        private System.Windows.Forms.MaskedTextBox _iaTimeLimit;
        private System.Windows.Forms.MaskedTextBox _inTimeLimit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox _in5Limit;
        private System.Windows.Forms.MaskedTextBox _inLimit;
        private System.Windows.Forms.MaskedTextBox _icLimit;
        private System.Windows.Forms.MaskedTextBox _ibLimit;
        private System.Windows.Forms.MaskedTextBox _iaLimit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.MaskedTextBox _u0Limit;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MaskedTextBox _tu0;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.MaskedTextBox _phase;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label _versionLabel;
        private System.Windows.Forms.ComboBox _autoKvitCombo;
        private System.Windows.Forms.Label _aoutoKvitLabel;
        private System.Windows.Forms.GroupBox GroupBox9;
        private System.Windows.Forms.ComboBox _sectorAngleCombo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox _updateParamCheckBox;
        private System.Windows.Forms.Label _updateKvitLabel;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox _clockCorrectionModuleCombo;
        private System.Windows.Forms.CheckBox _autoTranzitionCheck;
        private System.Windows.Forms.ComboBox _timeZoneCombo;
        private System.Windows.Forms.Button _acceptTimeSettingsBtn;
        private System.Windows.Forms.ComboBox _signTimeZoneCombo;
        private System.Windows.Forms.ComboBox _signCorrectionModuleCombo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.MaskedTextBox _energyLimit;
        private System.Windows.Forms.Button _updateFirmwareBtn;
        private System.Windows.Forms.TextBox updateFirmware;
        private System.Windows.Forms.Button _readDevNumBtn;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.MaskedTextBox _time;
        private System.Windows.Forms.MaskedTextBox _date;
        private System.Windows.Forms.Button _newTimeBtn;
        private System.Windows.Forms.MaskedTextBox _unixTimeL;
        private System.Windows.Forms.MaskedTextBox _unixTimeH;
        private System.Windows.Forms.MaskedTextBox _currentTimeWord3;
        private System.Windows.Forms.MaskedTextBox _currentTimeWord2;
        private System.Windows.Forms.MaskedTextBox _currentTimeWord1;
        private System.Windows.Forms.MaskedTextBox _unixTime;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label _currentTime;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.MaskedTextBox _u0;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.MaskedTextBox _in;
        private System.Windows.Forms.MaskedTextBox _if;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button _findSDFlashBtn;
        private System.Windows.Forms.OpenFileDialog openFileDialogSDFlash;
        private System.Windows.Forms.GroupBox groupBoxConfigReleAlarm;
        private System.Windows.Forms.MaskedTextBox _modeReleAlarm;
        private System.Windows.Forms.Button _modReleOutputBtn;
        private System.Windows.Forms.Button _modBlinkerBtn;
        private Forms.LedControl _modLedRele;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.MaskedTextBox _releOutputTime;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
    }
}