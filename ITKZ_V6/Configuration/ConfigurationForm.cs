﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules.Byte;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Int;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Framework.BusinessLogic;
using BEMN.Interfaces;
using BEMN.ITKZ_V6.Structures;
using BEMN.MBServer;

namespace BEMN.ITKZ_V6.Configuration
{
    public partial class ItkzConfigurationForm : Form, IFormView
    {
        #region [Feilds]

        private readonly ItkzDeviceV6 _device;
        private readonly MemoryEntity<OneWordStruct> _version;

        private readonly NewStructValidator<NumAndSpeedDevice> _numAndSpeedValidator;
        private readonly NewStructValidator<Settings> _settingsValidator;
        private readonly NewStructValidator<TimeSettings> _timeSettingsValidator;
        private readonly NewStructValidator<ConfigurationStruct> _configStructValidator;
        private readonly NewStructValidator<MeasuringStruct> _currentTimeValidator;
        private readonly NewStructValidator<CalibrateNominalValues> _nominalValuesValidator; 

        private int _hours;
        private uint _unixSeconds;
        private ulong _uint64Word;
        private ushort _firstWord;
        private ushort _secondWord;
        private ushort _timeWord1;
        private ushort _timeWord2;
        private ushort _timeWord3;

        #endregion [Feilds]

        #region [Constructor's]
        public ItkzConfigurationForm()
        {
            this.InitializeComponent();
        }

        public ItkzConfigurationForm(ItkzDeviceV6 device)
        {
            this.InitializeComponent();
            this._device = device;

            this._device.ReleOutputTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReleOutputTimeReadOk);
            this._device.ReleOutputTime.AllReadFail +=HandlerHelper.CreateReadArrayHandler(this, this.ReleOutputTimeReadFail);

            this._device.ReleOutputTimeSet.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ReleOutputTimeWriteOk);
            this._device.ReleOutputTimeSet.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () => this._releOutputTime.Text = this._device.ReleOutputTimeSet.Value.Word.ToString());

            // События при чтении/записи номера и скорости устройства
            this._device.NumAndSpeed.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadDevNumOk);
            this._device.NumAndSpeed.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show(@"Невозможно прочитать номер и скорость устройства", @"Внимание", MessageBoxButtons.OK,
                    MessageBoxIcon.Error));
            this._device.NumAndSpeed.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._device.ConfigurationCommand.Value.Word = 0x5555;
                this._device.ConfigurationCommand.SaveStruct6();
            });



            // События при чтении версии устройства
            this._version = this._device.Version;
            this._version.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadVersionComplite);
            this._version.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show(@"Ошибка чтения версии!", @"Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            });



            // События при чтении/записи конфигурации устройства
            this._device.ConfigurationStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadConfigurationStructOk); // при удачном чтении, заполняет поля структуру ConfigStruct значениями
            this._device.ConfigurationStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => 
                MessageBox.Show(@"Ошибка чтения конфигурации!", @"Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error)); // при неудачной попытке чтения конфигурации, вызывает диалоговое окно с сообщением об ошибке

            this._device.ConfigurationStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () => 
                MessageBox.Show(@"Конфигурация успешно записана", @"Запись в устройство", MessageBoxButtons.OK, MessageBoxIcon.Information)); // при удачной записи
            this._device.ConfigurationStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show(@"Невозможно записать конфигурацию!", @"Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error)); // при неудачной попытке записи конфигурации
         


            // События при подтверждении записи команд конфигурации
            this._device.ConfigurationCommand.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show(@"Запись в устройство прошла успешно.", @"Запись в устройство", MessageBoxButtons.OK, MessageBoxIcon.Information);
            });
            this._device.ConfigurationCommand.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show(@"Невозможно выполнить команду подтверждения записи!", @"Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error);
            });



            // События при записи команды завершения установки времени
            this._device.FinishSetTime.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () => 
                MessageBox.Show(@"Время установлено", @"Запись в устройство", MessageBoxButtons.OK, MessageBoxIcon.Information));
            this._device.FinishSetTime.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show(@"Не удалось записать время", @"Запись в устройство", MessageBoxButtons.OK, MessageBoxIcon.Error));



            // Обработка полей формы
            ToolTip toolTip = new ToolTip();

            // в комментариях [BindingProperty(...)], где ... - порядковый номер
            this._numAndSpeedValidator = new NewStructValidator<NumAndSpeedDevice>
            (
                toolTip,
                new ControlInfoText(this._deviceNumber, new CustomByteRule(1, 247)), // 0 Номер 
                new ControlInfoCombo(this._deviceSpeedCombo, Strings.DeviceSpeed) // 1 Скорость 
            );

            this._settingsValidator = new NewStructValidator<Settings>
            (
                toolTip,
                new ControlInfoCheck(this._updateParamCheckBox), // 0 Обнуление аварийных значений при квитировании (check)
                new ControlInfoCombo(this._autoKvitCombo, Strings.AutoKvitTime), // 1 Автоквитирование (combo)
                new ControlInfoCombo(this._sectorAngleCombo, Strings.AngleStatus) // 2 Угол, градусы (combo);
            );

            this._timeSettingsValidator = new NewStructValidator<TimeSettings>
            (
                toolTip,
                new ControlInfoCombo(this._timeZoneCombo, Strings.TimeZone), // 0 Значение часового пояса (combo);
                new ControlInfoCombo(this._signTimeZoneCombo, Strings.Sign), // 1 + / - (combo)
                new ControlInfoCheck(this._autoTranzitionCheck), // 2 Автоматический переход на летнее время (check)
                new ControlInfoCombo(this._signCorrectionModuleCombo, Strings.Sign), // 3 + / - (combo);
                new ControlInfoCombo(this._clockCorrectionModuleCombo, Strings.СlockCorrectionModule) // 4 Модуль коррекции хода часов (combo);
            );

            this._configStructValidator = new NewStructValidator<ConfigurationStruct>
            (
                toolTip,
                new ControlInfoText(this._iaLimit, new CustomDoubleRule(0.01, 1)), // 0
                new ControlInfoText(this._ibLimit, new CustomDoubleRule(0.01, 1)), // 1
                new ControlInfoText(this._icLimit, new CustomDoubleRule(0.01, 1)), // 2
                new ControlInfoText(this._inLimit, new CustomDoubleRule(0.01, 1)), // 3
                new ControlInfoText(this._in5Limit, new CustomDoubleRule(0.01, 1)), // 4

                new ControlInfoText(this._u0Limit, new CustomDoubleRule(0.01, 1)), // 5 Уставка U0
                new ControlInfoText(this._phase, new CustomUshortRule(0, 359)), // 6 Угол, градусы (text)

                new ControlInfoText(this._iaTimeLimit, new CustomUshortRule(10, 30000)), // 7
                new ControlInfoText(this._ibTimeLimit, new CustomUshortRule(10, 30000)), // 8
                new ControlInfoText(this._icTimeLimit, new CustomUshortRule(10, 30000)), // 9
                new ControlInfoText(this._inTimeLimit, new CustomUshortRule(10, 30000)), // 10
                new ControlInfoText(this._in5TimeLimit, new CustomUshortRule(10, 30000)), // 11

                new ControlInfoText(this._tu0, new CustomUshortRule(10, 30000)), // 12 Т, мс
                new ControlInfoValidator(this._settingsValidator), // 13 
                new ControlInfoValidator(this._timeSettingsValidator), // 14
                new ControlInfoText(this._releOutputTime, new CustomDifficultUshortRule(0, 100, 10000)), // 15
                new ControlInfoText(this._energyLimit, new CustomIntRule(0, short.MaxValue)) // 16
            );


            // ЗНАЧЕНИЯ ИЗ СКРЫТЫХ НА ФОРМЕ КОМПОНЕНТОВ MaskedTextBox
            this._currentTimeValidator = new NewStructValidator<MeasuringStruct>
            (
                toolTip,
                new ControlInfoText(this._unixTimeL, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)), // 0
                new ControlInfoText(this._unixTimeH, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)), // 1
                new ControlInfoText(this._currentTimeWord1, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)), // 2
                new ControlInfoText(this._currentTimeWord2, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)), // 3
                new ControlInfoText(this._currentTimeWord3, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)) // 4
            );


            // Из блока калибровочных коэффициентов
            this._nominalValuesValidator = new NewStructValidator<CalibrateNominalValues>
            (new ToolTip(),
                new ControlInfoText(this._if, new CustomUshortRule(100, 3000)),
                new ControlInfoText(this._in, new CustomUshortRule(1, 300)),
                new ControlInfoText(this._u0, new CustomUshortRule(1000, 20000)));
        }

        #endregion [Constructor's]

        #region [Methods]

        public void ReleOutputTimeReadOk()
        {
            LedManager.SetLed(this._modLedRele, this._device.ReleOutputTime.Value.IsImpulsMode);
            switch (this._modLedRele.State)
            {
                case LedState.Signaled:
                    this._modeReleAlarm.Text = @"Блинкера";
                    break;
                case LedState.NoSignaled:
                    this._modeReleAlarm.Text = @"Импульсный";
                    break;
                default:
                    this._modeReleAlarm.Text = "";
                    break;
            }
        }

        public void ReleOutputTimeReadFail()
        {
            LedManager.TurnOffLed(this._modLedRele);
            this._modeReleAlarm.Text = string.Empty;
        }

        private void ReleOutputTimeWriteOk()
        {
            this._device.ReleOutputTimeSet.LoadStruct();
        }
        
        public void ReadVersionComplite()
        {
            this._versionLabel.Text = @"Версия ПО - " + this._device.DeviceVersion;
        }

        private void ReadDevNumOk()
        {
            this._numAndSpeedValidator.Set(this._device.NumAndSpeed.Value);
        }

        private void ReadConfigurationStructOk()
        {
            this._nominalValuesValidator.Set(this._device.CalibrateNominalValues.Value);
            this._configStructValidator.Set(this._device.ConfigurationStruct.Value);
            this._currentTime.Text =
                this._device.MeasuringStructConfiguration.Value.CurrentTimeUnix(this._device.TimeSettings.Value);
        }

        private void WriteConfigurationStructOk()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            string str;
            if (this._configStructValidator.Check(out str, true) && this._nominalValuesValidator.Check(out str, true))
            {
                this._device.CalibrateNominalValues.Value = this._nominalValuesValidator.Get();
                this._device.CalibrateNominalValues.SaveStruct();

                this._device.ConfigurationStruct.Value = this._configStructValidator.Get();
                this._device.ConfigurationStruct.SaveStruct();

                int updateParam = this._updateParamCheckBox.Checked ? 0 : 1;
                this._device.UpdateParam.Value.Word = Commands.UpdateParam[updateParam];
                this._device.UpdateParam.SaveStruct6();

                this._device.AngleStatus.Value.Word = Commands.AngleStatus[this._sectorAngleCombo.SelectedIndex];
                this._device.AngleStatus.SaveStruct6();

                this._device.AvtoKvit.Value.Word = Commands.AutoKvitTime[this._autoKvitCombo.SelectedIndex];
                this._device.AvtoKvit.SaveStruct6();
            }
            else
            {
                MessageBox.Show(@"Введены неверные уставки. Невозможно записать конфигурацию!", @"Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void ForDataTime()
        {
            this._date.Text = DateTime.Today.ToString();

            int hour = DateTime.Now.Hour;
            int minute = DateTime.Now.Minute;
            int second = DateTime.Now.Second;

            string hourString = hour < 10 ? $"0{hour}" : $"{hour}";
            string minuteString = minute < 10 ? $"0{minute}" : $"{minute}";
            string secondString = second < 10 ? $"0{second}" : $"{second}";

            this._time.Text = $@"{hourString}:{minuteString}:{secondString}";
        }


        // Важно!!! Последовательность чтения структур играет большую роль
        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.TimeSettings.LoadStruct();
            this._device.NumAndSpeed.LoadStruct();
            this._device.MeasuringStructConfiguration.LoadStruct();
            this.ForDataTime();
            this._device.CalibrateNominalValues.LoadStruct();
            this._device.ConfigurationStruct.LoadStruct();
            this._device.ReleOutputTime.LoadStructCycle(new TimeSpan(0, 0, 0, 0, 100));
        }

        private void ReadFromFile()
        {
            if (this.openFileDialog.ShowDialog() != DialogResult.OK) return;
            this.Deserialize(this.openFileDialog.FileName, "ITKZ");
        }

        private void SaveInFile()
        {
            if (this.saveFileDialog.ShowDialog() != DialogResult.OK) return;
            if (this._configStructValidator.Check() && this._nominalValuesValidator.Check())
            {
                StructBase _configValues = this._configStructValidator.Get();
                StructBase _nominalValues = this._nominalValuesValidator.Get();
                this.Serialize(this.saveFileDialog.FileName, _configValues, _nominalValues, "ITKZ");
            }
            else
            {
                MessageBox.Show(@"Обнаружены неккоректные уставки. Конфигурация не может быть сохранена.",
                    @"Сохранение уставок", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }     
        }

        private void Serialize(string binFileName, StructBase configStruct, StructBase nominalStruct, string head)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement(head));

                List<ushort> valuesConfig = new List<ushort>();
                valuesConfig.AddRange(configStruct.GetValues());

                List<ushort> valuesNominal = new List<ushort>();
                valuesNominal.AddRange(nominalStruct.GetValues());

                XmlElement elementConfig = doc.CreateElement($"{head}_SET_POINTS");
                elementConfig.InnerText = Convert.ToBase64String(Common.TOBYTES(valuesConfig.ToArray(), false));
                XmlElement elementNominal = doc.CreateElement($"{head}_NOMINAL_VALUES");
                elementNominal.InnerText = Convert.ToBase64String(Common.TOBYTES(valuesNominal.ToArray(), false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(elementConfig);
                doc.DocumentElement.AppendChild(elementNominal);
                doc.Save(binFileName);

                MessageBox.Show(@"Запись конфигурации прошла успешно", @"Запись конфигурации", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show(@"Ошибка записи конфигурации", @"Сохранение крнфигурации", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Deserialize(string xmlFileName, string head)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(xmlFileName);

                XmlNode a = doc.FirstChild.SelectSingleNode($"{head}_SET_POINTS");
                var valuesConfig = Convert.FromBase64String(a.InnerText);

                XmlNode b = doc.FirstChild.SelectSingleNode($"{head}_NOMINAL_VALUES");
                var valuesNominal = Convert.FromBase64String(b.InnerText);

                // Сколько структур десериализуем, столько и переменных
                int configLen = this._configStructValidator.Get().GetValues().Length * 2; // размер структуры в байтах
                int nominalLen = this._nominalValuesValidator.Get().GetValues().Length * 2; // размер структуры в байтах
                
                byte[] configValues = new byte[configLen];
                byte[] nominalValues = new byte[nominalLen];

                Array.ConstrainedCopy(valuesConfig, 0, configValues, 0, configLen);
                Array.ConstrainedCopy(valuesNominal, 0, nominalValues, 0, nominalLen);

                ConfigurationStruct configNum = new ConfigurationStruct();
                CalibrateNominalValues nominalNum = new CalibrateNominalValues();

                configNum.InitStruct(configValues);
                nominalNum.InitStruct(nominalValues);

                this._configStructValidator.Set(configNum);
                this._nominalValuesValidator.Set(nominalNum);

                MessageBox.Show(@"Файл конфигурации загружен успешно", @"Загрузка конфигурации", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show(@"Невозможно загрузить файл конфигурации", @"Загрузка конфигурации", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CurrentTime()
        {
            try
            {
                ushort year, month, day, hour, minute, second;
                day = Convert.ToUInt16(this._date.Text.Substring(0, 2));
                month = Convert.ToUInt16(this._date.Text.Substring(3, 2));
                year = Convert.ToUInt16(this._date.Text.Substring(6, 4));
                if (_signTimeZoneCombo.SelectedIndex == 0)
                {
                    hour = (ushort)(Convert.ToUInt16(this._time.Text.Substring(0, 2)) - Convert.ToInt16(this._timeZoneCombo.SelectedItem));
                }
                else
                {
                    hour = (ushort)(Convert.ToUInt16(this._time.Text.Substring(0, 2)) + Convert.ToInt16(this._timeZoneCombo.SelectedItem));
                }
                minute = Convert.ToUInt16(this._time.Text.Substring(3, 2));
                second = Convert.ToUInt16(this._time.Text.Substring(6, 2));

                DateTime dateTime = new DateTime(year, month, day, hour, minute, second);
                TimeSpan timeSpan = new TimeSpan(0);
                DateTimeOffset dateTimeOffset = new DateTimeOffset(dateTime, timeSpan);

                this._unixSeconds = (uint)dateTimeOffset.AddHours(this._hours).ToUnixTimeSeconds();

                this._unixTime.Text = this._unixSeconds.ToString();

                if (year < 2000 || year > 2106)
                {
                    MessageBox.Show(@"Год с 2000 по 2106!", @"Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                byte[] bytes = BitConverter.GetBytes(this._unixSeconds);
                this._firstWord = Common.TOWORD(bytes[3], bytes[2]);
                this._secondWord = Common.TOWORD(bytes[1], bytes[0]);

                this._timeWord1 = Common.TOWORD((byte)minute, (byte)second);
                this._timeWord2 = Common.TOWORD((byte)day, (byte)hour);
                this._timeWord3 = (ushort)((ushort)(year << 4) | month); // 12 бит - год, 4 бита - месяц

                // для проверки 
                this._unixTimeL.Text = this._secondWord.ToString();
                this._unixTimeH.Text = this._firstWord.ToString();
                this._currentTimeWord1.Text = this._timeWord1.ToString();
                this._currentTimeWord2.Text = this._timeWord2.ToString();
                this._currentTimeWord3.Text = this._timeWord3.ToString();


                this._device.StartSetTime.Value.Word = 0x0901; // команда начала установки времени
                this._device.StartSetTime.SaveStruct6();

                this._device.MeasuringStructConfiguration.Value = this._currentTimeValidator.Get();
                this._device.MeasuringStructConfiguration.SaveStruct();

                //Thread.Sleep(3000); // задержка, без неё время не будет записываться

                this._device.FinishSetTime.Value.Word = 0x0902; // команда завершения установки времени
                this._device.FinishSetTime.SaveStruct(new TimeSpan(0, 0, 0, 1));
            }
            catch (Exception)
            {
                MessageBox.Show(@"Не корректно заданы параметры даты/времени", @"Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion [Methods]

        #region [FormControls Methods]
        private void _readBtn_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void _writeBtn_Click(object sender, EventArgs e)
        {
            this.WriteConfigurationStructOk();
        }
        
        private void _acceptPortNumberBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this._numAndSpeedValidator.Check())
            {
                this._device.NumAndSpeed.SaveOneWord(Convert.ToUInt16(this._deviceNumber.Text));
            }
            else
            {
                MessageBox.Show(@"Неверно задан номер устройства", @"Внимание", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void _acceptDeviceSpeedBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.ConfigurationCommand.Value.Word = Commands.DeviceSpeed[this._deviceSpeedCombo.SelectedIndex];
            this._device.ConfigurationCommand.SaveStruct6();
        }

        private void _readDevNumBtn_Click(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this._device.NumAndSpeed.LoadStruct();
        }

        private void _readFromFileBtn_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void _writeToFileBtn_Click(object sender, EventArgs e)
        {
            this.SaveInFile();
        }

        private void _acceptTimeSettingsBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.TimeSettings.Value = this._timeSettingsValidator.Get();
            this._device.TimeSettings.SaveStruct6();
        }

        private void _updateFirmwareBtn_Click(object sender, EventArgs e)
        {
            this.CreateProcess();       
        }

        private void CreateProcess()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;  
            DialogResult result = MessageBox.Show(@"Обновить прошивку?", @"Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (result == DialogResult.No) return;

            string pathToFileString = this.updateFirmware.Text; // путь к SDFlash.exe

            try
            {
                if (ExistSDFlash())
                {
                    Process.Start(pathToFileString);
                }
                else
                {
                    MessageBox.Show("Приложение \"SDFlash\" уже запущено.", "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception) // Если SDFlash не найден по указанному пути
            {
                MessageBox.Show(@"Перед началом прошивки запустите программу SDFlash.exe");
                return;
            }

            this._device.AvtoKvit.Value.Word = 0xf00f;  // 0xf00f - команда обновления прошивки
            this._device.AvtoKvit.SaveStruct6();

            MessageBox.Show(@"По завершению программирования переподключитесь к устройству", "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Information);
            TreeNode node = TreeManager.GetDeviceNode(this._device);
            TreeManager.FreePort(node);
            this._device.MB.Dispose(); // освобождаем порт

            //if (this._device.MB.Port.Opened) return;
            //this._device.MB.Port.Open();
        }


        /// <summary>
        /// Проверяет, запущен ли SDFlash.exe.
        /// Возвращает true - не запущен, false - запущен.
        /// </summary>
        private static bool ExistSDFlash()
        {
            string processName = "SDFlash";
            Process[] p = Process.GetProcessesByName(processName);
            return p.Length < 1;
        }


        private void ITKZConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfigurationStructOk();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfigurationStructOk();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
            }
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled = this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }

        private void ItkzConfigurationForm_Activated(object sender, EventArgs e)
        {
            this._versionLabel.Text = @"Версия ПО - " + this._device.DeviceVersion;
        }

        private void ItkzConfigurationForm_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.Path != string.Empty)
                this.updateFirmware.Text = Properties.Settings.Default.Path;
            if (Device.AutoloadConfig)
            {
                this.StartRead();                
            }
        }

        private void _newTimeBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.CurrentTime();
        }

        private void _findSDFlashBtn_Click(object sender, EventArgs e)
        {
            if (this.openFileDialogSDFlash.ShowDialog() != DialogResult.OK) return;
            this.updateFirmware.Text = Properties.Settings.Default.Path = this.openFileDialogSDFlash.FileName;
            Properties.Settings.Default.Save();
        }

        private void _modReleOutputBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (int.Parse(this._releOutputTime.Text) < 100 || int.Parse(this._releOutputTime.Text) > 10000)
            {
                this._device.ReleOutputTimeSet.Value.Word = 500;
                this._device.ReleOutputTimeSet.SaveStruct6();
            }
            else
            {
                this._device.ReleOutputTimeSet.Value.Word = ushort.Parse(this._releOutputTime.Text);
                this._device.ReleOutputTimeSet.SaveStruct6();
            }
        }

        private void _modBlinkerBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.ReleOutputTimeSet.Value.Word = 0;
            this._device.ReleOutputTimeSet.SaveStruct6();
        }

        private void ItkzConfigurationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.ReleOutputTime.RemoveStructQueries();
        }
        #endregion [FormControls Methods]

        #region [IFormView]
        public Type FormDevice => typeof(ItkzDeviceV6);
        public bool Multishow { get; private set; }
        public Type ClassType => typeof(ItkzConfigurationForm);
        public bool ForceShow => false;
        public Image NodeImage => Framework.Properties.Resources.config.ToBitmap();
        public string NodeName => "Конфигурация";
        public INodeView[] ChildNodes => new INodeView[] { };
        public bool Deletable => false;
        #endregion [IFormView]
  
    }
}