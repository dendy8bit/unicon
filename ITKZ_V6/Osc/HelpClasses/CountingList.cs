using System;
using System.Globalization;
using System.IO;
using System.Text;
using BEMN.ITKZ_V6.Osc.Structures;
using BEMN.ITKZ_V6.Structures;

namespace BEMN.ITKZ_V6.Osc.HelpClasses
{
    /// <summary>
    /// ��������� �������� ������������
    /// </summary>
    public class CountingList
    {
        /// <summary>
        /// ������������
        /// </summary>
        public short[][] _currents;

        /// <summary>
        /// ����������
        /// </summary>
        private short[][] _voltages;

        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        public int _count;

        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        private int _alarm;

        private string _hdrString;
        
        private ItkzDeviceV6 _device;

        private OscJournalStruct _oscJournal;
        private TimeSettings _timeSettings;

        #region [Ctor's]

        public CountingList(ItkzDeviceV6 device, short[] current, short[] voltage, OscJournalStruct oscJournalStruct, TimeSettings timeSetiings)
        {
            this._device = device;
            this._oscJournal = oscJournalStruct;
            this._timeSettings = timeSetiings;

            //����� ���������� ��������
            this._count = 96;

            this._currents = new short[1][];
            this._currents[0] = current;
            
            this._voltages = new short[1][];
            this._voltages[0] = voltage;
            
            this._hdrString = $"���� v{this._device.DeviceVersion} {oscJournalStruct.CurrentTimeUnix(timeSetiings)}";
        }
        public CountingList(int count)
        {
            this._currents = new short[1][];

            this._voltages = new short[1][];

            this._count = count;
        }
        #endregion [Ctor's]
        
        public void Save(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine(this._hdrString);
                hdrFile.WriteLine("T1N1");
                hdrFile.WriteLine(1251);
            }

            NumberFormatInfo format = new NumberFormatInfo { NumberDecimalSeparator = "." };

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
            {
                cgfFile.WriteLine("ITKZ,1,1991");
                cgfFile.WriteLine($"{1 + 1},{1 + 1}A,0D");

                int index = 1;
                
                cgfFile.WriteLine("{0},In,,,A,{1},0,0,-32768,32767,1,1,P", index, 1);
                index++;

                cgfFile.WriteLine("{0},U0,,,V,{1},0,0,-32768,32767,1,1,P", index, 1);
                index++;
                
                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this._count);

                cgfFile.WriteLine(this._oscJournal.GetFormattedDateTime(this._timeSettings));
                cgfFile.WriteLine(this._oscJournal.Alarm(this._timeSettings));
                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < this._count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}", i, i * 1000);
                    foreach (short[] current in this._currents)
                    {
                        datFile.Write(",{0}", current[i]);
                    }
                    foreach (short[] voltage in this._voltages)
                    {
                        datFile.Write(",{0}", voltage[i]);
                    }

                    datFile.WriteLine();
                }
            }
        }

        public bool IsLoad { get; private set; }
        public string FilePath { get; private set; }

        public CountingList Load(string filePath, ItkzDeviceV6 device)
        {
            this._device = device;

            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string hdrString = hdrStrings[0];
            
            string[] analogChannels = hdrStrings[hdrStrings.Length - 2].Split(new[] { 'T', 'N' }, StringSplitOptions.RemoveEmptyEntries);
            int currentsCount = int.Parse(analogChannels[0]);
            int voltagesCount = int.Parse(analogChannels[1]);

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);
            
            int indexCounts = 2 + currentsCount + voltagesCount + 2;
            int counts = int.Parse(cfgStrings[indexCounts].Replace("1000,", string.Empty));
            int alarm = 0;
            try
            {
                string startTime = cfgStrings[indexCounts + 1].Split(',')[1];
                string runTime = cfgStrings[indexCounts + 2].Split(',')[1];

                TimeSpan a = DateTime.Parse(runTime) - DateTime.Parse(startTime);
                alarm = (int)a.TotalMilliseconds;
            }
            catch (Exception)
            {
                // ignored
            }

            CountingList result = new CountingList(counts) { _alarm = alarm };

            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);

            short[][] currents = new short[currentsCount][];
            short[][] voltages = new short[voltagesCount][];

            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new short[96];
            }

            for (int i = 0; i < voltages.Length; i++)
            {
                voltages[i] = new short[96];
            }


            for (int i = 0; i < datStrings.Length; i++)
            {
                string[] means = datStrings[i].Split(',');
                for (int j = 0; j < currentsCount; j++)
                {
                    currents[j][i] = (short)Convert.ToDouble(means[j + 1], CultureInfo.InvariantCulture);
                }

                for (int j = 0; j < voltagesCount; j++)
                {
                    voltages[j][i] = (short)Convert.ToDouble(means[j + 2], CultureInfo.InvariantCulture);
                }
            }

            result._currents = currents;
            result._voltages = voltages;
            result._hdrString = hdrString;
            result.IsLoad = true;
            result.FilePath = filePath;
            return result;
        }
    }
}