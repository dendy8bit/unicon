using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Forms.ValidatingClasses;
using BEMN.Interfaces;
using BEMN.ITKZ_V6.Osc.HelpClasses;
using BEMN.ITKZ_V6.Osc.Loaders;
using BEMN.ITKZ_V6.Osc.Structures;
using BEMN.ITKZ_V6.Structures;
using Settings = BEMN.ITKZ_V6.Properties.Settings;

namespace BEMN.ITKZ_V6.Osc
{
    public partial class OscilloscopeForm : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "�������������";
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string RECORDS_IN_JOURNAL = "������������ � ������� - {0}";
        private const string JOURNAL_IS_EMPTY = "������ ������������ ����";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string READ_OSC_STOPPED = "������ ������������� ����������";
        private const string READ_OSC_ERROR = "������ ������ �������������";
        #endregion [Constants]
        
        #region [Private fields]
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscPageLoader _pageLoader;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscJournalLoader _oscJournalLoader;
        /// <summary>
        /// ������ ���
        /// </summary>
        private CountingList _countingList;
        private OscJournalStruct  _journalStruct;
        private readonly DataTable _table;

        private ItkzDeviceV6 _device;

        private MemoryEntity<TimeSettings> _timeSettings;

        private int _portNumber;

        #endregion [Private fields]

        #region [Ctor's]
        public OscilloscopeForm()
        {
            this.InitializeComponent();
        }

        public OscilloscopeForm(ItkzDeviceV6 device)
        {
            this.InitializeComponent();
            this._device = device;
            
            // ��������� �������
            this._pageLoader = new OscPageLoader(device.SetOscStartPage, device.OscPage);
            this._pageLoader.PageRead += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadStopped += HandlerHelper.CreateActionHandler(this, this.ReadStop);
            
            this._oscJournalLoader = new OscJournalLoader(device.AllOscJournal);
            this._oscJournalLoader.ReadJournalOk += HandlerHelper.CreateActionHandler(this, this.ReadRecord);
            this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.OnAllJournalReadOk);
            this._oscJournalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);

            this._timeSettings = device.TimeSettingsForOsc;
            this._timeSettings.AllReadOk +=  HandlerHelper.CreateReadArrayHandler(this, _oscJournalLoader.StartReadJournal);
            this._timeSettings.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show(READ_OSC_FAIL, "������!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            });
            
            this._table = this.GetJournalDataTable();
        }

        #endregion [Ctor's]
        
        #region [Help Classes Events Handlers]
        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._oscJournalReadButton.Enabled = true;
            this._statusLabel.Text = READ_OSC_FAIL;
        }

        private void ReadStop()
        {
            this._statusLabel.Text = this._pageLoader.Error ? READ_OSC_ERROR : READ_OSC_STOPPED;
            this._stopReadOsc.Enabled = false;
            this._oscJournalReadButton.Enabled = true;
            this._oscReadButton.Enabled = true;
            this._oscProgressBar.Value = 0;
        }

        /// <summary>
        /// �������� ���� ������
        /// </summary>
        private void OnAllJournalReadOk()
        {
            if (this._oscJournalLoader.GetRecord.Length == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
                this._oscJournalReadButton.Enabled = true;
            }
            else
            {
                this.ReadRecord();
            }
            
        }

        /// <summary>
        /// ��������� ���� ������ �������
        /// </summary>
        private void ReadRecord()
        {
            int i = 1;
            
            foreach (var journal in this._oscJournalLoader.GetRecord)
            {
                this._oscilloscopeCountCb.Items.Add(i);
                this._table.Rows.Add(i++,
                    journal.CurrentTimeUnix(this._timeSettings.Value),
                    journal.OscInStStatus,
                    journal.OscIn5StStatus,
                    journal.OscU0StStatus,
                    journal.OscInDir,
                    journal.OscInDirValid,
                    journal.OscInLastval,
                    journal.OscU0Lastval,
                    journal.OscPhaseShift,
                    journal.OscEnergy);
            }
            
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, i - 1);
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;

            this._oscJournalDataGrid.Refresh();
        }
        
        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            try
            {
                this._journalStruct = this._oscJournalLoader.GetRecord[this._oscilloscopeCountCb.SelectedIndex];
                this.CountingList = new CountingList(this._device, _journalStruct.InData, _journalStruct.U0Data, this._journalStruct, this._timeSettings.Value);

                this._oscShowButton.Enabled = true;
                this._oscSaveButton.Enabled = true;
            }
            catch (Exception e)
            {
                MessageBox.Show("������ ������������� ���������� ��� �������", "��������", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
            this._oscProgressBar.Value = this._oscProgressBar.Maximum;
        }

        #endregion [Help Classes Events Handlers]
        
        #region [Properties]
        /// <summary>
        /// ���������� ����������� ������� ������������ ��� ������
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                try
                {
                    this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;

                }
                catch (Exception e)
                {
                }
                
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// ������ ���
        /// </summary>
        public CountingList CountingList
        {
            get { return this._countingList ?? (this._countingList = new CountingList(0)); }
            set
            {
                this._countingList = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]

        #region [Help members]
        private DataTable GetJournalDataTable()
        {
            DataTable table = new DataTable("��901_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }
        #endregion [Help members]
        
        #region [Event Handlers]
        /// <summary>
        /// �������� �����
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            this.StartRead();
        }

        private void OscilloscopeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._pageLoader.ClearEvents();
            this._oscJournalLoader.ClearEvents();
        }

        /// <summary>
        /// �������� �������������
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            //if (this.CountingList == null)
            //{
            //    MessageBox.Show("������������� �� ���������");
            //    return;
            //}
            try
            {
                if (Validator.GetVersionFromRegistry())
                {
                    string fileName;
                    if (this._countingList.IsLoad)
                    {
                        fileName = this._countingList.FilePath;
                    }
                    else
                    {
                        fileName = Validator.CreateOscFileNameCfg("ITKZ V6 ������������");
                        this._countingList.Save(fileName);
                    }
                    string oscPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe");
                    System.Diagnostics.Process.Start(oscPath, fileName);
                }
                else
                {
                    if (!Settings.Default.OscFlag)
                    {
                        LoadNetForm loadNetForm = new LoadNetForm();
                        loadNetForm.ShowDialog(this);
                        Settings.Default.OscFlag = loadNetForm.OscFlag;
                        Settings.Default.Save();
                    }

                    throw new Exception();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._oscJournalReadButton.Enabled = false;
            this._oscilloscopeCountCb.Items.Clear();
            this._oscilloscopeCountCb.SelectedIndex = -1;
            this._table.Clear();
            this._oscJournalLoader.Clear();
            this._oscJournalDataGrid.Refresh();
            this.CanSelectOsc = false;
            this._oscSaveButton.Enabled = false;
            this._oscJournalReadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscLoadButton.Enabled = false;
            this._oscReadButton.Enabled = false;
            //_oscJournalLoader.StartReadJournal();
            this._timeSettings.LoadStruct();

        }
        
        /// <summary>
        /// ��������� �������������
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            OscReadOk();
        }

        /// <summary>
        /// ��������� ������������� � ����
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        {

            this._saveOscilloscopeDlg.FileName = $"ITKZ v{this._device.DeviceVersion.Replace(".", "_")} ������������� {this._journalStruct.CurrentTimeUnix(this._timeSettings.Value).Replace(".", "_").Replace(":", "_")}";
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    this._countingList.Save(this._saveOscilloscopeDlg.FileName);
                    this._statusLabel.Text = "������������ ���������";
                }
                catch (Exception)
                {
                    this._statusLabel.Text = "������ ����������";
                }
            }
        }

        /// <summary>
        /// ��������� ������������� �� �����
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                this.CountingList = this.CountingList.Load(this._openOscilloscopeDlg.FileName, this._device);
                this._statusLabel.Text = string.Format("������������ ��������� �� ����� {0}", this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
                this._stopReadOsc.Enabled = false;
            }
            catch (Exception ex)
            {
                this._statusLabel.Text = "���������� ��������� ������������";
            }

        }

        /// <summary>
        /// ���������� ������ ������������
        /// </summary>
        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._pageLoader.StopRead();
        }

        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }
        #endregion [Event Handlers]
        
        #region [IFormView Members]
        public Type FormDevice => typeof(ItkzDeviceV6);

        public bool Multishow { get; private set; }

        public Type ClassType => typeof(OscilloscopeForm);

        public bool ForceShow => false;

        public Image NodeImage => Framework.Properties.Resources.oscilloscope.ToBitmap();

        public string NodeName => OSC;

        public INodeView[] ChildNodes => new INodeView[] { };

        public bool Deletable => false;

        #endregion [IFormView Members]       
    }
}