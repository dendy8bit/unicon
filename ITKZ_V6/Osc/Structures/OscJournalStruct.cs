﻿using System;
using System.Collections;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.ITKZ_V6.Structures;
using BEMN.MBServer;

namespace BEMN.ITKZ_V6.Osc.Structures
{
    public class OscJournalStruct : StructBase
    {
        #region [Constants]
        private const string NUMBER_PATTERN = "{0}";
        private const int FULL_OSC_SIZE = 1600; 
        private const int PAGE_SIZE_IN_WORD = 400;

        /// <summary>
        /// Размер одной страницы 
        /// </summary>
        private const int OSCLEN = 400;

        #endregion [Constants]

        public static int RecordIndex;

        #region [Private fields]

        [Layout(0)] private ushort _oscTimeUnixH;
        [Layout(1)] private ushort _oscTimeUnixL;
        [Layout(2)] private ushort _oscStatus; // возможно будет резерв (выводить не нужно)
        [Layout(3)] private ushort _oscStStatus;
        [Layout(4)] private ushort _oscInLastval;
        [Layout(5)] private ushort _oscU0Lastval;
        [Layout(6)] private ushort _oscPhaseShift;
        [Layout(7)] private short _oscEnergy;
        [Layout(8, Count = 96)] private short[] _InData;
        [Layout(9, Count = 96)] private short[] _U0Data;

        #endregion [Private fields]

        #region [Properties fields]

        public short[] InData => this._InData;
        public short[] U0Data => this._U0Data;

        /// <summary>
        /// Метод, возвращающий количество часов, установленных в настройке времени в конфигурации (от -12 до 12)
        /// </summary>
        public int GetHours(TimeSettings timeSettings)
        {
            int number;
            bool signbool;
            int hour;

            number = Common.GetBits(timeSettings.ForUnixTime, 0, 1, 2, 3);
            signbool = Common.GetBit(timeSettings.ForUnixTime, 4);

            hour = signbool ? -number : number; // количество часов
            return hour;
        }

        public string CurrentTimeUnix(TimeSettings timeSettings)
        {
            if (this._oscTimeUnixH == 0 && this._oscTimeUnixL == 0) return string.Empty;
            DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(((uint)this._oscTimeUnixL << 16) | this._oscTimeUnixH);
            dateTimeOffset.DateTime.AddHours(this.GetHours(timeSettings)).ToString();

            return dateTimeOffset.DateTime.AddHours(this.GetHours(timeSettings)).ToString();
        }

        public string GetFormattedDateTime(TimeSettings timeSettings)
        {
            if (this._oscTimeUnixH == 0 && this._oscTimeUnixL == 0) return string.Empty;
            DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(((uint)this._oscTimeUnixL << 16) | this._oscTimeUnixH);
            dateTimeOffset.DateTime.AddHours(this.GetHours(timeSettings)).ToString();

            return string.Format("{0:D2}/{1:D2}/{2:D2},{3:D2}:{4:D2}:{5:D2}.{6:D3}",
                dateTimeOffset.Month,
                dateTimeOffset.Day,
                dateTimeOffset.Year,
                dateTimeOffset.Hour,
                dateTimeOffset.Minute,
                dateTimeOffset.Second,
                dateTimeOffset.Millisecond);
        }

        public string Alarm(TimeSettings timeSettings)
        {
            if (this._oscTimeUnixH == 0 && this._oscTimeUnixL == 0) return string.Empty;
            DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(((uint)this._oscTimeUnixL << 16) | this._oscTimeUnixH);
            dateTimeOffset.DateTime.AddMilliseconds(36).ToString();

            return string.Format("{0:D2}/{1:D2}/{2:D2},{3:D2}:{4:D2}:{5:D2}.{6:D3}",
                dateTimeOffset.Month,
                dateTimeOffset.Day,
                dateTimeOffset.Year,
                dateTimeOffset.Hour,
                dateTimeOffset.Minute,
                dateTimeOffset.Second,
                dateTimeOffset.Millisecond);
        }

        public string DateTime
        {
            get
            {
                return DateTimeOffset.FromUnixTimeSeconds(((uint)this._oscTimeUnixL << 16) | this._oscTimeUnixH).DateTime.ToString();
            }
        }

        public uint UnixSeconds
        {
            get
            {
                return (((uint)this._oscTimeUnixL << 16) | this._oscTimeUnixH);
            }
        }

        /// <summary>
        /// Биты регистра StStatus.
        ///  0 - состояние ступени индикации (СИ) тока фазы Ia.
        ///  1 - состояние ступени индикации (СИ) тока фазы Ib.
        ///  2 - состояние ступени индикации (СИ) тока фазы Ic.
        ///  3 - состояние ступени индикации (СИ) тока фазы In.
        ///  4 - состояние ступени индикации (СИ) тока фазы In5.
        ///  5 - состояние ступени индикации (СИ) напряжения U0.
        /// 
        ///  8 - флаг направления мощности при последнем срабатывании ступени тока In
        ///  9 - флаг достоверности направления мощности
        /// 10 - флаг направления мощности при последнем срабатывании ступени тока In5
        /// 11 - флаг достоверности направления мощности
        /// 12 - флаг направления мощности при последнем срабатывании ступени напряжения U0
        /// 13 - флаг достоверности направления мощности
        /// </summary>
        public BitArray StStatus
        {
            get { return new BitArray(BitConverter.GetBytes(this._oscStStatus)); }
        }
        
        public string OscInStStatus 
        {
            get
            {
                bool value = Common.GetBit(this._oscStStatus, 3);
                return value ? "Да" : "Нет";
            }
            //set { this._oscStStatus = Common.SetBit(this._oscStStatus, 3, value); }
        }

        public string OscIn5StStatus
        {
            get
            {
                bool value = Common.GetBit(this._oscStStatus, 4);
                return value ? "Да" : "Нет";
            }
        }

        public string OscU0StStatus
        {
            get
            {
                bool value = Common.GetBit(this._oscStStatus, 5);
                return value ? "Да" : "Нет";
            }
        }

        public string OscInDir
        {
            get
            {
                bool value = Common.GetBit(this._oscStStatus, 8);
                return value ? "Да" : "Нет";
            }
        }

        public string OscInDirValid
        {
            get
            {
                bool value = Common.GetBit(this._oscStStatus, 9);
                return value ? "Да" : "Нет";
            }
        }

        public ushort OscInLastval
        {
            get { return this._oscInLastval; }
        }

        public ushort OscU0Lastval
        {
            get { return this._oscU0Lastval; }
        }

        public ushort OscPhaseShift
        {
            get { return this._oscPhaseShift; }
        }

        public int OscEnergy
        {
            get
            {
                if (this._oscEnergy > short.MaxValue)
                {
                    BitArray inverseBitArray = new BitArray(Common.GetBitsArray(this._oscEnergy)).Not(); // проверить написанный метод GetBitsArray, как работает
                    ushort temp = Common.BitsToUshort(inverseBitArray);
                    temp++;
                    return -temp;
                }
                return this._oscEnergy;
            }
        }

        #endregion [Properties fields]


        #region [Properties properties]

        /// <summary>
        /// Одна запись журнала осциллографа
        /// </summary>
        public object[] GetRecord
        {
            get
            {
                return new object[]
                {
                    this.GetNumber, // номер
                    this.DateTime, // дата и время
                    this.OscInStStatus, // состояние СИ тока In
                    this.OscIn5StStatus, // состояние СИ тока In5
                    this.OscU0StStatus, // состояние СИ напряжения U0
                    this.OscInDir, // флаг направления In
                    this.OscInDirValid, // флаг достоверности In
                    this.OscInLastval, // In при последнем срабатывании
                    this.OscU0Lastval, // U0 при последнем срабатывании
                    this.OscPhaseShift, // угол фазового сдвига
                    this.OscEnergy  // энергия тип int
                };
            }
        }


        /// <summary>
        /// Полный размер осцилографа (в страницах)
        /// </summary>
        public ushort FullOscSizeInPages
        {
            get { return FULL_OSC_SIZE / PAGE_SIZE_IN_WORD; }
        }


        public bool IsEmpty
        {
            get { return this._oscTimeUnixH == 0 && _oscTimeUnixL == 0; }
        }

        /// <summary>
        /// Начальная страница осциллограммы
        /// </summary>
        public ushort OscStartIndex
        {
            get { return 1000 + OSCLEN; } // возможно надо поменять 1000+400+400+400 
        }

        /// <summary>
        /// Номер соощения
        /// </summary>
        private string GetNumber
        {
            get
            {
                string result = string.Format(NUMBER_PATTERN, RecordIndex + 1);
                if (RecordIndex == 0)
                {
                    result += "(посл.)";
                }
                return result;
            }
        }
        #endregion [Properties properties]
    }
}
