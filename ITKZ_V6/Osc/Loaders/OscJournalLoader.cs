﻿using System;
using System.Collections.Generic;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.ITKZ_V6.Osc.Structures;

namespace BEMN.ITKZ_V6.Osc.Loaders
{
    /// <summary>
    /// Загружает журнал осцилограммы
    /// </summary>
    public class OscJournalLoader
    {
        #region [Private fields]
        /// <summary>
        /// все записи журнала осциллграфа
        /// </summary>
        private readonly MemoryEntity<AllOscJournalStruct> _oscJournal;
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _refreshOscJournal;
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        private readonly List<OscJournalStruct> _oscRecords;
        /// <summary>
        /// Текущий номер записи журнала осциллографа
        /// </summary>
        private int _recordNumber;
        #endregion [Private fields]

        #region [Events]
        /// <summary>
        /// Успешно прочитана одна запись журнала осциллографа
        /// </summary>
        public event Action ReadJournalOk;
        /// <summary>
        /// Успешно прочитаны все записи
        /// </summary>
        public event Action AllJournalReadOk;
        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action ReadJournalFail;
        
        #endregion [Events]

        #region [Ctor's]
        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        /// <param name="OscJournal">Журнал осциллографа</param>
        /// <param name="refreshOscJournal">Объект сброса журнала</param>
        public OscJournalLoader(MemoryEntity<AllOscJournalStruct> oscJournal)
        {
            this._oscRecords = new List<OscJournalStruct>();
            //Записи журнала
            this._oscJournal = oscJournal;
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
        }
        
        #endregion [Ctor's]


        #region [Properties]
        /// <summary>
        /// Количество записей в журнале осциллографа
        /// </summary>
        public int OscCount
        {
            get { return this._oscRecords.Count; }
        }
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<OscJournalStruct> OscRecords
        {
            get { return this._oscRecords; }
        }

        public OscJournalStruct[] GetRecord
        {
            get { return this._oscJournal.Value.AllJournalStructs.ToArray(); }
        }

        #endregion [Properties]


        #region [Private MemoryEntity Events Handlers]
        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            if (this.ReadJournalFail != null)
                this.ReadJournalFail.Invoke();
        }
        
        private void ReadRecord()
        {
            if (this.AllJournalReadOk == null) return;
            this.AllJournalReadOk.Invoke();
        }

        #endregion [Private MemoryEntity Events Handlers]

        #region [Public members]
        /// <summary>
        /// Запуск чтения журнала осциллографа
        /// </summary>
        public void StartReadJournal()
        {
            this._recordNumber = 0;
            OscJournalStruct.RecordIndex = 0;

            this._oscJournal.LoadStruct();
        }
        
        public void ClearEvents()
        {
            this._oscJournal.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
            this._oscJournal.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
        }

        #endregion [Public members]

        internal void Clear()
        {
            this._oscRecords.Clear();
        }
    }
}
