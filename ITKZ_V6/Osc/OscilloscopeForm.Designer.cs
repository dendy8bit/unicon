namespace BEMN.ITKZ_V6.Osc
{
    partial class OscilloscopeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._oscJournalDataGrid = new System.Windows.Forms.DataGridView();
            this._oscNumColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscIcStStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscIn5StStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscU0StStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscInDir = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscInDirValid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscInLastval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscU0Lastval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscPhaseShift = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._energyCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscJournalReadButton = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._stopReadOsc = new System.Windows.Forms.Button();
            this._oscShowButton = new System.Windows.Forms.Button();
            this._oscLoadButton = new System.Windows.Forms.Button();
            this._oscSaveButton = new System.Windows.Forms.Button();
            this._oscProgressBar = new System.Windows.Forms.ProgressBar();
            this._oscReadButton = new System.Windows.Forms.Button();
            this._oscilloscopeCountLabel = new System.Windows.Forms.Label();
            this._oscilloscopeCountCb = new System.Windows.Forms.ComboBox();
            this._saveOscilloscopeDlg = new System.Windows.Forms.SaveFileDialog();
            this._openOscilloscopeDlg = new System.Windows.Forms.OpenFileDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this._fileProcessDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._oscJournalDataGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._oscJournalDataGrid);
            this.groupBox1.Controls.Add(this._oscJournalReadButton);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(811, 311);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "������ ������������";
            // 
            // _oscJournalDataGrid
            // 
            this._oscJournalDataGrid.AllowUserToAddRows = false;
            this._oscJournalDataGrid.AllowUserToDeleteRows = false;
            this._oscJournalDataGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._oscJournalDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._oscJournalDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._oscNumColumn,
            this._dateCol,
            this._oscIcStStatus,
            this._oscIn5StStatus,
            this._oscU0StStatus,
            this._oscInDir,
            this._oscInDirValid,
            this._oscInLastval,
            this._oscU0Lastval,
            this._oscPhaseShift,
            this._energyCol});
            this._oscJournalDataGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this._oscJournalDataGrid.Location = new System.Drawing.Point(3, 16);
            this._oscJournalDataGrid.Name = "_oscJournalDataGrid";
            this._oscJournalDataGrid.ReadOnly = true;
            this._oscJournalDataGrid.RowHeadersVisible = false;
            this._oscJournalDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._oscJournalDataGrid.RowTemplate.Height = 24;
            this._oscJournalDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._oscJournalDataGrid.Size = new System.Drawing.Size(805, 260);
            this._oscJournalDataGrid.TabIndex = 5;
            this._oscJournalDataGrid.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this._oscJournalDataGrid_RowEnter);
            // 
            // _oscNumColumn
            // 
            this._oscNumColumn.DataPropertyName = "_oscNumColumn";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            this._oscNumColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this._oscNumColumn.Frozen = true;
            this._oscNumColumn.HeaderText = "�";
            this._oscNumColumn.Name = "_oscNumColumn";
            this._oscNumColumn.ReadOnly = true;
            this._oscNumColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._oscNumColumn.Width = 30;
            // 
            // _dateCol
            // 
            this._dateCol.DataPropertyName = "_dateCol";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._dateCol.DefaultCellStyle = dataGridViewCellStyle6;
            this._dateCol.HeaderText = "���� �����";
            this._dateCol.Name = "_dateCol";
            this._dateCol.ReadOnly = true;
            this._dateCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dateCol.Width = 120;
            // 
            // _oscIcStStatus
            // 
            this._oscIcStStatus.DataPropertyName = "_oscIcStStatus";
            this._oscIcStStatus.HeaderText = "��������� �� ���� In";
            this._oscIcStStatus.Name = "_oscIcStStatus";
            this._oscIcStStatus.ReadOnly = true;
            // 
            // _oscIn5StStatus
            // 
            this._oscIn5StStatus.DataPropertyName = "_oscIn5StStatus";
            this._oscIn5StStatus.HeaderText = "��������� �� ���� In5";
            this._oscIn5StStatus.Name = "_oscIn5StStatus";
            this._oscIn5StStatus.ReadOnly = true;
            // 
            // _oscU0StStatus
            // 
            this._oscU0StStatus.DataPropertyName = "_oscU0StStatus";
            this._oscU0StStatus.HeaderText = "��������� �� ���������� U0";
            this._oscU0StStatus.Name = "_oscU0StStatus";
            this._oscU0StStatus.ReadOnly = true;
            this._oscU0StStatus.Width = 120;
            // 
            // _oscInDir
            // 
            this._oscInDir.DataPropertyName = "_oscInDir";
            this._oscInDir.HeaderText = "���� �����������";
            this._oscInDir.Name = "_oscInDir";
            this._oscInDir.ReadOnly = true;
            // 
            // _oscInDirValid
            // 
            this._oscInDirValid.DataPropertyName = "_oscInDirValid";
            this._oscInDirValid.HeaderText = "���� �������������";
            this._oscInDirValid.Name = "_oscInDirValid";
            this._oscInDirValid.ReadOnly = true;
            // 
            // _oscInLastval
            // 
            this._oscInLastval.DataPropertyName = "_oscInLastval";
            this._oscInLastval.HeaderText = "In ��� ��������� ������������";
            this._oscInLastval.Name = "_oscInLastval";
            this._oscInLastval.ReadOnly = true;
            // 
            // _oscU0Lastval
            // 
            this._oscU0Lastval.DataPropertyName = "_oscU0Lastval";
            this._oscU0Lastval.HeaderText = "U0 ��� ��������� ������������";
            this._oscU0Lastval.Name = "_oscU0Lastval";
            this._oscU0Lastval.ReadOnly = true;
            // 
            // _oscPhaseShift
            // 
            this._oscPhaseShift.DataPropertyName = "_oscPhaseShift";
            this._oscPhaseShift.HeaderText = "���� �������� ������";
            this._oscPhaseShift.Name = "_oscPhaseShift";
            this._oscPhaseShift.ReadOnly = true;
            // 
            // _energyCol
            // 
            this._energyCol.DataPropertyName = "_energyCol";
            this._energyCol.HeaderText = "�������";
            this._energyCol.Name = "_energyCol";
            this._energyCol.ReadOnly = true;
            // 
            // _oscJournalReadButton
            // 
            this._oscJournalReadButton.Location = new System.Drawing.Point(646, 282);
            this._oscJournalReadButton.Name = "_oscJournalReadButton";
            this._oscJournalReadButton.Size = new System.Drawing.Size(155, 23);
            this._oscJournalReadButton.TabIndex = 4;
            this._oscJournalReadButton.Text = "���������� ������";
            this._oscJournalReadButton.UseVisualStyleBackColor = true;
            this._oscJournalReadButton.Click += new System.EventHandler(this._oscJournalReadButton_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 444);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(816, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(16, 17);
            this._statusLabel.Text = "...";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._stopReadOsc);
            this.groupBox2.Controls.Add(this._oscShowButton);
            this.groupBox2.Controls.Add(this._oscLoadButton);
            this.groupBox2.Controls.Add(this._oscSaveButton);
            this.groupBox2.Controls.Add(this._oscProgressBar);
            this.groupBox2.Controls.Add(this._oscReadButton);
            this.groupBox2.Location = new System.Drawing.Point(3, 34);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(811, 77);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "�������������";
            // 
            // _stopReadOsc
            // 
            this._stopReadOsc.Enabled = false;
            this._stopReadOsc.Location = new System.Drawing.Point(320, 19);
            this._stopReadOsc.Name = "_stopReadOsc";
            this._stopReadOsc.Size = new System.Drawing.Size(155, 23);
            this._stopReadOsc.TabIndex = 12;
            this._stopReadOsc.Text = "���������� ������";
            this._stopReadOsc.UseVisualStyleBackColor = true;
            this._stopReadOsc.Visible = false;
            this._stopReadOsc.Click += new System.EventHandler(this._stopReadOsc_Click);
            // 
            // _oscShowButton
            // 
            this._oscShowButton.Enabled = false;
            this._oscShowButton.Location = new System.Drawing.Point(6, 48);
            this._oscShowButton.Name = "_oscShowButton";
            this._oscShowButton.Size = new System.Drawing.Size(155, 23);
            this._oscShowButton.TabIndex = 10;
            this._oscShowButton.Text = "�������� �������������";
            this._oscShowButton.UseVisualStyleBackColor = true;
            this._oscShowButton.Click += new System.EventHandler(this._oscShowButton_Click);
            // 
            // _oscLoadButton
            // 
            this._oscLoadButton.Location = new System.Drawing.Point(647, 48);
            this._oscLoadButton.Name = "_oscLoadButton";
            this._oscLoadButton.Size = new System.Drawing.Size(155, 23);
            this._oscLoadButton.TabIndex = 9;
            this._oscLoadButton.Text = "��������� �� �����";
            this._oscLoadButton.UseVisualStyleBackColor = true;
            this._oscLoadButton.Click += new System.EventHandler(this._oscLoadButton_Click);
            // 
            // _oscSaveButton
            // 
            this._oscSaveButton.Enabled = false;
            this._oscSaveButton.Location = new System.Drawing.Point(647, 19);
            this._oscSaveButton.Name = "_oscSaveButton";
            this._oscSaveButton.Size = new System.Drawing.Size(155, 23);
            this._oscSaveButton.TabIndex = 7;
            this._oscSaveButton.Text = "��������� � ����";
            this._oscSaveButton.UseVisualStyleBackColor = true;
            this._oscSaveButton.Click += new System.EventHandler(this._oscSaveButton_Click);
            // 
            // _oscProgressBar
            // 
            this._oscProgressBar.Location = new System.Drawing.Point(167, 48);
            this._oscProgressBar.Name = "_oscProgressBar";
            this._oscProgressBar.Size = new System.Drawing.Size(474, 23);
            this._oscProgressBar.Step = 1;
            this._oscProgressBar.TabIndex = 6;
            this._oscProgressBar.Visible = false;
            // 
            // _oscReadButton
            // 
            this._oscReadButton.Enabled = false;
            this._oscReadButton.Location = new System.Drawing.Point(6, 19);
            this._oscReadButton.Name = "_oscReadButton";
            this._oscReadButton.Size = new System.Drawing.Size(155, 23);
            this._oscReadButton.TabIndex = 5;
            this._oscReadButton.Text = "��������� �������������";
            this._oscReadButton.UseVisualStyleBackColor = true;
            this._oscReadButton.Click += new System.EventHandler(this._oscReadButton_Click);
            // 
            // _oscilloscopeCountLabel
            // 
            this._oscilloscopeCountLabel.AutoSize = true;
            this._oscilloscopeCountLabel.Enabled = false;
            this._oscilloscopeCountLabel.Location = new System.Drawing.Point(252, 10);
            this._oscilloscopeCountLabel.Name = "_oscilloscopeCountLabel";
            this._oscilloscopeCountLabel.Size = new System.Drawing.Size(235, 13);
            this._oscilloscopeCountLabel.TabIndex = 8;
            this._oscilloscopeCountLabel.Text = "�������� ����� �������� ������������� :";
            // 
            // _oscilloscopeCountCb
            // 
            this._oscilloscopeCountCb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscilloscopeCountCb.Enabled = false;
            this._oscilloscopeCountCb.FormattingEnabled = true;
            this._oscilloscopeCountCb.Location = new System.Drawing.Point(493, 7);
            this._oscilloscopeCountCb.Name = "_oscilloscopeCountCb";
            this._oscilloscopeCountCb.Size = new System.Drawing.Size(56, 21);
            this._oscilloscopeCountCb.TabIndex = 9;
            // 
            // _saveOscilloscopeDlg
            // 
            this._saveOscilloscopeDlg.FileName = "������������� ��901";
            this._saveOscilloscopeDlg.Filter = "���� �������������|*.hdr";
            this._saveOscilloscopeDlg.Title = "��������� �������������";
            // 
            // _openOscilloscopeDlg
            // 
            this._openOscilloscopeDlg.Filter = "���� �������������|*.hdr";
            this._openOscilloscopeDlg.Title = "������� �������������";
            // 
            // splitContainer1
            // 
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel2.Controls.Add(this._oscilloscopeCountCb);
            this.splitContainer1.Panel2.Controls.Add(this._oscilloscopeCountLabel);
            this.splitContainer1.Size = new System.Drawing.Size(816, 437);
            this.splitContainer1.SplitterDistance = 317;
            this.splitContainer1.TabIndex = 10;
            // 
            // OscilloscopeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 466);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "OscilloscopeForm";
            this.Text = "������ ������������";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OscilloscopeForm_FormClosing);
            this.Load += new System.EventHandler(this.OscilloscopeForm_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._oscJournalDataGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button _oscLoadButton;
        private System.Windows.Forms.Button _oscSaveButton;
        private System.Windows.Forms.ProgressBar _oscProgressBar;
        private System.Windows.Forms.Button _oscReadButton;
        private System.Windows.Forms.Button _oscJournalReadButton;
        private System.Windows.Forms.Button _oscShowButton;
        private System.Windows.Forms.Label _oscilloscopeCountLabel;
        private System.Windows.Forms.ComboBox _oscilloscopeCountCb;
        private System.Windows.Forms.Button _stopReadOsc;
        private System.Windows.Forms.SaveFileDialog _saveOscilloscopeDlg;
        private System.Windows.Forms.OpenFileDialog _openOscilloscopeDlg;
        private System.Windows.Forms.DataGridView _oscJournalDataGrid;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.OpenFileDialog _fileProcessDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscNumColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dateCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscIcStStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscIn5StStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscU0StStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscInDir;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscInDirValid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscInLastval;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscU0Lastval;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscPhaseShift;
        private System.Windows.Forms.DataGridViewTextBoxColumn _energyCol;
    }
}