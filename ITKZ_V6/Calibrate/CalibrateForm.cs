﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Interfaces;

namespace BEMN.ITKZ_V6.Calibrate
{
    public partial class CalibrateForm : Form, IFormView
    {
        private CoefficientsPanel _coefPanel;
        private MeasuringPanel _measuringPanel;
        private VoltageCalibration _voltageCalibration;
        private string _deviceName;
        private string _deviceVersion;
        private ItkzDeviceV6 _device;

        private bool _isPasswordChecked;
        private PasswordForm _passwordForm;
        private bool _isShowedMessage;
        private string _password = "1234";

        internal enum Step
        {
            Step1,Step2,Step3,Step4,Step5,Step6
        }

        private Step _currentStep;

        public CalibrateForm()
        {
            this.InitializeComponent();
        }

        public CalibrateForm(ItkzDeviceV6 device)
        {
            this.InitializeComponent();

            this._device = device;
            this._deviceName = device.DeviceType;
            this._deviceVersion = device.DeviceVersion;
            
            this._coefPanel = new CoefficientsPanel(device);
            this._coefPanel.Dock = DockStyle.Fill;
            this._measuringPanel = new MeasuringPanel(device);
            this._measuringPanel.Dock = DockStyle.Fill;
            
            this._voltageCalibration = new VoltageCalibration(device);

            this._device.CommandCalibrate.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show(@"Заводские настройки сброшены!", @"Внимание", MessageBoxButtons.OK,
                    MessageBoxIcon.Information));
            this._device.CommandCalibrate.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show(@"Не удалось сбросить заводские настройки!", @"Внимание", MessageBoxButtons.OK,
                    MessageBoxIcon.Error));
        }

        private void CalibrateForm_Load(object sender, EventArgs e)
        {
            this.SetStartState();
        }

        private void SetStartState()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._reset.Enabled = false;
            this._continueBtn.Enabled = true;
            this._coefPanel.SetStartState();
            this._coefPanel.ReadCoeff(this.StepDone);
            this.pannelGroupBox.Controls.Clear();
            this.pannelGroupBox.Controls.Add(this._coefPanel);
            this._currentStep = Step.Step1;
        }

        private void _continueBtn_Click(object sender, EventArgs e)
        {
            this._continueBtn.Enabled = false;
            switch (this._currentStep)
            {
                case Step.Step2:
                    {
                        if (!this._coefPanel.IsDefault)
                        {
                            DialogResult res = MessageBox.Show(@"Устройство уже скалибровано. Будут записаны коэффициенты по умолчанию. Начать калибровку?",
                                @"Калибровка", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (res == DialogResult.No)
                            {
                                this.SetStartState();
                                return;
                            }
                        }
                        this._reset.Enabled = true;
                        this._coefPanel.WriteDefaultCoefficients(this.StepDone);
                    }
                    break;
                case Step.Step4:
                    {
                        this._measuringPanel.CalculateCoefficients();
                        this.StepDone(true);
                    }
                    break;
                case Step.Step5:
                    {
                        this._reset.Enabled = false;
                        this._coefPanel.WriteCoeffInDevice(this.StepDone);
                    }
                    break;
                case Step.Step6:
                    this._continueBtn.Text = @"Далее";
                    this.SetStartState();
                    break;
                default:
                    this.SetStartState();
                    break;
            }
        }

        private void StepDone(bool result)
        {
            this._continueBtn.Enabled = true;
            if (result)
            {
                this.IncrementStep();
            }
            else
            {
                if (this._currentStep == Step.Step1)
                {
                    MessageBox.Show(@"Невозможно прочитать текущие коэффициенты в устройстве.", @"Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show(@"Невозможно выполнить шаг. Калибровка будет сброшена", @"Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.SetStartState();
                }
            }
        }

        private void IncrementStep()
        {
            switch (this._currentStep)
            {
                case Step.Step1:
                    {
                        this._currentStep = Step.Step2;
                        this._infoRichTextBox.Text = Info.STEP2_INFO;
                    }
                    break;
                case Step.Step2:
                    {
                        this._currentStep = Step.Step3;
                        this._continueBtn.Enabled = false;
                        this.pannelGroupBox.Controls.Remove(this._coefPanel);
                        this.pannelGroupBox.Controls.Add(this._measuringPanel);
                        this._measuringPanel.SetStartParametersMeasuring(this.StepDone);
                        this._infoRichTextBox.Text = Info.STEP3_INFO;
                    }
                    break;
                case Step.Step3:
                    {
                        this._currentStep = Step.Step4;
                        this._infoRichTextBox.Text = Info.STEP4_INFO;
                    }
                    break;
                case Step.Step4:
                    {
                        this._currentStep = Step.Step5;
                        this._infoRichTextBox.Text = Info.STEP5_INFO;
                        this._coefPanel.SetReadCoefficients(this._measuringPanel.Coefficients.Coefficients);
                        this.pannelGroupBox.Controls.Remove(this._measuringPanel);
                        this.pannelGroupBox.Controls.Add(this._coefPanel);
                    }
                    break;
                case Step.Step5:
                    this._currentStep = Step.Step6;
                        this._infoRichTextBox.Text = Info.STEP6_INFO;
                        this._continueBtn.Text = @"Завершить";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void CalibrateForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._measuringPanel.StopMeasuring();
        }

        private void _reset_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show(@"Вы уверены, что хотите начать заново калибровку устройства? Все текущие данные будут утеряны.", @"Сброс калибровки", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (res == DialogResult.Yes)
            {
                this.SetStartState();
            }
        }

        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof (ItkzDeviceV6); }
        }

        public bool Multishow { get; set; }

        public Type ClassType
        {
            get { return typeof (CalibrateForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Framework.Properties.Resources.calibrate; }
        }

        public string NodeName
        {
            get { return "Калибровка"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion [IFormView Members]

        private void _saveInFile_Click(object sender, EventArgs e)
        {
            this.saveFileDialog.FileName = $"Калибровочные коэффициенты {this._deviceName} {this._deviceVersion}";
            if (DialogResult.OK == this.saveFileDialog.ShowDialog())
            {
                this._coefPanel.SaveInFile(this.saveFileDialog.FileName, this._deviceName);
            }
        }

        private void _loadFromFile_Click(object sender, EventArgs e)
        {
            this.openFileDialog.FileName = $"Калибровочные коэффициенты {this._deviceName} {this._deviceVersion}";
            if (DialogResult.OK == this.openFileDialog.ShowDialog())
            {
                this._coefPanel.LoadFromFile(this.openFileDialog.FileName, this._deviceName);
            }
        }

        private void _saveInDevice_Click(object sender, EventArgs e)
        {
            if (this._coefPanel.Visible && this._coefPanel.Enabled)
            {
                var dialogRes = MessageBox.Show(@"Записать калибровочные коэффициенты в устройство?",
                    @"Запись коэффициентов", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogRes == DialogResult.Yes)
                {
                    this._coefPanel.WriteCoeffInDevice(this.CoefsWritten);
                }
            }
        }

        private void CoefsWritten(bool res)
        {
            MessageBox.Show(res ? "Коэффициенты записаны в устройство успешно" : "Не удалось записать коэффициенты в устройство",
                @"Запись коэффициентов", MessageBoxButtons.OK, res ? MessageBoxIcon.Information : MessageBoxIcon.Error);
        }

        private void _calibrBtn_Click(object sender, EventArgs e)
        {
            this._voltageCalibration.Show();
        }

        private void _resetSettingsBtn_Click(object sender, EventArgs e)
        {
            if (!this.CheckPassword()) return;
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.CommandCalibrate.Value.Word = 0x0ff0;
            this._device.CommandCalibrate.SaveStruct6();
        }

        private bool CheckPassword()
        {
            if (!this._isPasswordChecked)
            {
                if (!this._isShowedMessage)
                {
                    MessageBox.Show("Для активации дополнительного функционала, требуется ввести пароль!", "Внимание!",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this._isShowedMessage = true;
                }
                this._passwordForm = new PasswordForm();
                if (this._passwordForm.ShowDialog() == DialogResult.Cancel) return false;

                if (this._passwordForm.Password != this._password)
                {
                    MessageBox.Show("Введен неверный пароль. Обратитесь к поставщику производителя", "Внимание!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                MessageBox.Show("Пароль принят!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this._isPasswordChecked = true;
            }

            return true;
        }
    }
}