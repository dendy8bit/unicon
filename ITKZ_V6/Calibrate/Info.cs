﻿namespace BEMN.ITKZ_V6.Calibrate
{
    public static class Info
    {
        public static string PANEL1_INFO1 = "Текущие коэффициенты в устройстве:";
        
        public static string STEP2_INFO =
            "Прочитаны текущие коэффициенты в устройстве. Чтобы начать калибровку, нажмите кнопку \"Далее\". Если в процессе калибровки нужна начать заново, нажмите \"Сбросить\".";
        public static string STEP3_INFO =
            "Для начала калибровки введите необходимые величины токов и время измерения. Чтобы начать измерение токов по текущей фазе, нажмите кнопку \"Измерить\".";
        public static string STEP4_INFO =
            "Измерение токов и расчет коэффициентов завершено. Чтобы продолжить, нажмите кнопку \"Далее\".";
        public static string STEP5_INFO =
            "Перед сохранением калибровочных коэффициентов в устройство можно их подкорректировать. Чтобы сохранить нажмите  кнопку \"Далее\".";
        public static string STEP6_INFO =
            "Калибровка устройства завершена успешно.";

        public static string MEASURING_IA = "Идет измерение токов для канала Ia";
        public static string MEASURING_IA_COMPLETE = "Измерение токов для канала Ia завершено";
        public static string MEASURING_IB = "Идет измерение токов для канала Ib";
        public static string MEASURING_IB_COMPLETE = "Измерение токов для канала Ib завершено";
        public static string MEASURING_IC = "Идет измерение токов для канала Ic";
        public static string MEASURING_IC_COMPLETE = "Измерение токов для канала Ic завершено";
        public static string MEASURING_COMPLETE = "Измерение токов и расчет коэффициентов завершены";
    }
}
