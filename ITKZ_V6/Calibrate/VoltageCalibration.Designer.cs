﻿namespace BEMN.ITKZ_V6.Calibrate
{
    partial class VoltageCalibration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._writeBtn = new System.Windows.Forms.Button();
            this._closeFormBtn = new System.Windows.Forms.Button();
            this._vBat = new System.Windows.Forms.MaskedTextBox();
            this._supplyVoltage = new System.Windows.Forms.MaskedTextBox();
            this._vRef = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _writeBtn
            // 
            this._writeBtn.Location = new System.Drawing.Point(16, 103);
            this._writeBtn.Name = "_writeBtn";
            this._writeBtn.Size = new System.Drawing.Size(75, 23);
            this._writeBtn.TabIndex = 0;
            this._writeBtn.Text = "Установить";
            this._writeBtn.UseVisualStyleBackColor = true;
            this._writeBtn.Click += new System.EventHandler(this._writeBtn_Click);
            // 
            // _closeFormBtn
            // 
            this._closeFormBtn.Location = new System.Drawing.Point(269, 103);
            this._closeFormBtn.Name = "_closeFormBtn";
            this._closeFormBtn.Size = new System.Drawing.Size(75, 23);
            this._closeFormBtn.TabIndex = 1;
            this._closeFormBtn.Text = "Закрыть";
            this._closeFormBtn.UseVisualStyleBackColor = true;
            this._closeFormBtn.Click += new System.EventHandler(this._closeFormBtn_Click);
            // 
            // _vBat
            // 
            this._vBat.Location = new System.Drawing.Point(266, 10);
            this._vBat.Name = "_vBat";
            this._vBat.ReadOnly = true;
            this._vBat.Size = new System.Drawing.Size(58, 20);
            this._vBat.TabIndex = 2;
            // 
            // _supplyVoltage
            // 
            this._supplyVoltage.Location = new System.Drawing.Point(266, 36);
            this._supplyVoltage.Name = "_supplyVoltage";
            this._supplyVoltage.ReadOnly = true;
            this._supplyVoltage.Size = new System.Drawing.Size(58, 20);
            this._supplyVoltage.TabIndex = 3;
            // 
            // _vRef
            // 
            this._vRef.Location = new System.Drawing.Point(266, 67);
            this._vRef.Name = "_vRef";
            this._vRef.Size = new System.Drawing.Size(58, 20);
            this._vRef.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(134, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Напряжение батареи:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(73, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(179, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Аналоговое напряжение питания:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(239, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Величина внутреннего опорного напряжения:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(327, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "mV";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(327, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "mV";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(327, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "mV";
            // 
            // VoltageCalibration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 145);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._vRef);
            this.Controls.Add(this._supplyVoltage);
            this.Controls.Add(this._vBat);
            this.Controls.Add(this._closeFormBtn);
            this.Controls.Add(this._writeBtn);
            this.Name = "VoltageCalibration";
            this.Text = "VoltageCalibration";
            this.Activated += new System.EventHandler(this.VoltageCalibration_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VoltageCalibration_FormClosing);
            this.Load += new System.EventHandler(this.VoltageCalibration_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _writeBtn;
        private System.Windows.Forms.Button _closeFormBtn;
        private System.Windows.Forms.MaskedTextBox _vBat;
        private System.Windows.Forms.MaskedTextBox _supplyVoltage;
        private System.Windows.Forms.MaskedTextBox _vRef;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}