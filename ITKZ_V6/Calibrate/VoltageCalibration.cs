﻿using System;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.ITKZ_V6.Structures;

namespace BEMN.ITKZ_V6.Calibrate
{
    public partial class VoltageCalibration : Form
    {
        private ItkzDeviceV6 _device;
        private NewStructValidator<VrefUshort> _vRefValidator;

        public VoltageCalibration(ItkzDeviceV6 device)
        {
            this.InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopLoad;

            this._device.MeasuringStructCalibrate.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringReadOk);
            this._device.MeasuringStructCalibrate.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringReadFail);

            this._device.Vref.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadVrefStructOk);
            this._device.Vref.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.WriteVrefStructOk);

            this._device.Vref.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show(@"Ошибка чтения значения 'Внутреннего опорного напряжения'!", @"Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error));
            this._device.Vref.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show(@"Невозможно записать значение 'Внутреннего опорного напряжения' в устройство!", @"Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error)); 
            

            this._vRefValidator = new NewStructValidator<VrefUshort>
            (
                new ToolTip(),
                new ControlInfoText(this._vRef, new CustomUshortRule(1100, 1355))
            );
        }
        
        public VoltageCalibration()
        {
            this.InitializeComponent();
        }

        private void ReadVrefStructOk()
        {
            this._vRefValidator.Set(this._device.Vref.Value);
            this._vRef.Text = this._device.Vref.Value.VRef.ToString();
        }

        private void WriteVrefStructOk()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            string str;
            if (this._vRefValidator.Check(out str, true))
            {
                this._device.Vref.Value = this._vRefValidator.Get();
                this._device.Vref.SaveStruct6();
            }
            else
            {
                MessageBox.Show(@"Введено неверное значение!", @"Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void MeasuringReadOk()
        {
            this._vBat.Text = this._device.MeasuringStructCalibrate.Value.VBat.ToString();
            this._supplyVoltage.Text = this._device.MeasuringStructCalibrate.Value.SupplyVoltage.ToString();
        }

        private void MeasuringReadFail()
        {
            this._vBat.Text = string.Empty;
            this._supplyVoltage.Text = string.Empty;
        }


        private void StartStopLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.MeasuringStructCalibrate.LoadStructCycle();
                this._vBat.Text = this._device.MeasuringStructCalibrate.Value.VBat.ToString();
                this._supplyVoltage.Text = this._device.MeasuringStructCalibrate.Value.SupplyVoltage.ToString();
            }
            else
            {
                this._device.MeasuringStructCalibrate.RemoveStructQueries();
            }
        }

        private void _closeFormBtn_Click(object sender, EventArgs e)
        {
            this._device.ConnectionModeChanged -= this.StartStopLoad;
            this._device.MeasuringStructCalibrate.RemoveStructQueries();

            Hide();
        }

        private void _writeBtn_Click(object sender, EventArgs e)
        {
            this.WriteVrefStructOk();
        }

        private void VoltageCalibration_Load(object sender, EventArgs e)
        {
            this._device.Vref.LoadStruct();
            this.StartStopLoad();
        }

        private void VoltageCalibration_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this._device.ConnectionModeChanged -= this.StartStopLoad;
                this._device.MeasuringStructCalibrate.RemoveStructQueries();
                e.Cancel = true;
                Hide();
            }
        }

        private void VoltageCalibration_Activated(object sender, EventArgs e)
        {
            this.StartStopLoad();
        }
    }
}