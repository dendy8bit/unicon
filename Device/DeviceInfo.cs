using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using BEMN.MBServer;

namespace BEMN.Devices
{
    public class DeviceInfo
    {
        #region [Fields]
        private string _deviceType = string.Empty; // ����������
        private string _plant = string.Empty; // �����������
        private string _version = string.Empty; // ������
        #endregion [Fields]

        #region [Properties]
        public string Type
        {
            get { return this._deviceType; }
            set { this._deviceType = value; }
        }

        public string Plant
        {
            get { return this._plant; }
            set { this._plant = value; }
        }

        public string Version
        {
            get { return this._version; }
            set { this._version = value; }
        }

        public int CurrentsCount => this.GetCountFromString("T");
        public int VoltagesCount => this.GetCountFromString("N");
        public int DiskretsCount => this.GetCountFromString("D");
        public int RelaysCount => this.GetCountFromString("R");

        private int GetCountFromString(string chanelName)
        {
            if (this.Plant == null) return 0;

            string count = string.Empty;
            int index = this.Plant.IndexOf(chanelName, StringComparison.InvariantCulture);
            while (true)
            {
                if (index < 0 || index >= this.Plant.Length - 1) break;
                char d = this.Plant[++index];
                if (char.IsDigit(d))
                {
                    count += d;
                }
                else
                {
                    break;
                }
            }
            return string.IsNullOrEmpty(count) ? 0 : int.Parse(count);
        }

        public static bool IsAvailableShowAllInformation { get; private set; }
        
        [XmlIgnore]
        public bool IsAz { get; private set; }

        public string DeviceConfiguration { get; set; }
        #endregion [Properties] 

        #region [Methods]
        public override string ToString() => this.Type + " " + this.Version + " " + this.Plant;

        public static DeviceInfo GetVersion(byte[] byteBuf) // ����������� ����������, ����������� � ������
        {
            DeviceInfo ret = new DeviceInfo();
            System.Text.Decoder dec = System.Text.Encoding.ASCII.GetDecoder();
            char[] charBuf = new char[byteBuf.Length];
            try
            {
                int byteUsed, charUsed;
                bool complete;
                dec.Convert(byteBuf, 0, byteBuf.Length, charBuf, 0, byteBuf.Length, true, out byteUsed, out charUsed, out complete);
            }
            catch (ArgumentNullException)
            {
                throw new ApplicationException("������� ������� �����");
            }
            string[] param;
            try
            {
                if (charBuf.Length >= 64)
                {
                    IsAvailableShowAllInformation = true;
                    param = new string(charBuf, 0, 64).Split(new[] { ' ', '\0' }, StringSplitOptions.RemoveEmptyEntries);
                }
                else
                {
                    IsAvailableShowAllInformation = false;
                    param = new string(charBuf, 0, 30).Split(new[] { ' ', '\0' }, StringSplitOptions.RemoveEmptyEntries);
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                param = new string(charBuf, 0, 16).Split(new[] { ' ', '\0' }, StringSplitOptions.RemoveEmptyEntries);
            }

            if (param[1] == "801" && param[3] == "CAZ" || param[4] == "CAZ")
            {
                ret.IsAz = true;
                ret._deviceType = param[0] + param[1];
                Regex typeRegex = new Regex(@"T\d+N\d+D\d+R\d+");
                ret._plant = typeRegex.Match(param.Length == 8 ? param[7] : param.Last()).Value;
                ret._version = param.Length == 10 ? param[6] : param[5];

                return ret;
            }

            if (param[0] == "MLK" || param[0] == "KL" || param[0] == "KL4_3I")
            {
                List<string> version = new List<string>();
                version.AddRange(new string(charBuf, 0, 16).Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries));
                version.AddRange(new string(charBuf, 16, 16).Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries));
                version.AddRange(new string(charBuf, 32, 16).Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries));
                switch (param[0])
                {
                    case "MLK":
                    {
                        string vers = version[9];
                        vers = vers.Remove(0, 1);
                        switch (vers)
                        {
                            case "A":
                                ret._version = "���10";
                                break;
                            case "B":
                                ret._version = "���11";
                                break;
                            case "C":
                                ret._version = "���12";
                                break;
                            case "D":
                                ret._version = "���13";
                                break;
                        }
                        break;
                    }
                    case "KL":
                    {
                        string vers = version[9];
                        vers = vers.Remove(0, 1);
                        switch (vers)
                        {
                            case "A":
                                ret._version = "��27";
                                break;
                        }
                        break;
                    }
                    default:
                        ret._version = "��� ��� " + version[3];
                        break;
                }
                ret._plant = version[3];
                ret._deviceType = version[0];
            }
            else if (param[0] == "LM65")
            {
                ret._deviceType = param[0];
                ret._version = param[3];
            }
            else if (param[0] == "MR" && param[1] == "5" && param.Length > 6) 
            {
                ret._deviceType = param[0] + param[1];
                ret._version = param[6];
            }
            else if (param[0] == "MR" && param[1] == "801")
            {
                ret._deviceType = param[0] + param[1];

                bool b = int.TryParse(param[3], out int i);

                ret._plant = "N" + (b ? i.ToString() : string.Empty);

                try
                {
                    string param2 = param[2];
                    StringBuilder sb = new StringBuilder(param2);

                    if (sb.Length > 1)
                    {
                        ret._version = param[4];
                    }
                    else
                    {
                        ret._version = char.IsDigit(param[5][0]) ? param[5] : param[4];
                    }
                }
                catch (Exception e)
                {
                    ret._version = char.IsDigit(param[5][0]) ? param[5] : param[4];
                }

                ret.DeviceConfiguration = OptimizeDeviceConfigString(param.Last());
            }
            else if (param[0] == "MR" && param[1] == "761" && param.Length < 8)
            {
                ret._deviceType = param[0] + param[1];
                try
                {
                    string param2 = param[2];
                    StringBuilder sb = new StringBuilder(param2);

                    Regex typeRegex = new Regex(@"T\d+N\d+D\d+R\d+");
                    ret._plant = typeRegex.Match(param[6]).Value;

                    if (sb.Length > 1)
                    {
                        ret._version = param[4];
                    }
                    else
                    {
                        ret._version = char.IsDigit(param[5][0]) ? param[5] : param[4];
                    }
                }
                catch (Exception e)
                {
                    if (param.Length < 6)
                    {
                        ret._version = param[4];
                    }
                    else
                    {
                        ret._version = char.IsDigit(param[5][0]) ? param[5] : param[4];
                    }
                }

                ret.DeviceConfiguration = ret._plant;
            }
            else if (param[0] == "MR" && param[1] == "761OBR")
            {
                ret._deviceType = param[0] + param[1];

                bool b = int.TryParse(param[3], out int i);

                ret._plant = "N" + (b ? i.ToString() : string.Empty);
                if (ret._plant == "N")
                {
                    ret._version = param[4];
                }
                else
                {
                    ret._version = char.IsDigit(param[5][0]) ? param[5] : param[4];
                }

                ret.DeviceConfiguration = OptimizeDeviceConfigString(param.Last());
            }
            else if ((param[1] == "901" || param[1] == "902") && param.Length >= 6)
            {
                try
                {
                    ret._deviceType = param[0] + param[1];
                    if (param.Length < 8 && (param[5].StartsWith("3") || param[6].StartsWith("3")) && param[1] != "902")
                    {
                        Regex typeRegex = new Regex(@"T\d+N\d+D\d+R\d+");
                        ret._plant = typeRegex.Match(param.Length == 8 ? param[7] : param.Last()).Value;
                        ret._version = param.Length > 8 ? param[6] : param[5];
                    }
                    else
                    {
                        if ((param[5].StartsWith("3") || param[6].StartsWith("3")) && param[1] != "902")
                        {
                            Regex typeRegex = new Regex(@"T\d+N\d+D\d+R\d+");
                            ret._plant = typeRegex.Match(param.Length == 8 ? param[7] : param.Last()).Value;
                            ret._version = param.Length > 8 ? param[6] : param[5];
                            ret.DeviceConfiguration = OptimizeDeviceConfigString(param.Last());
                        }
                        else
                        {
                            if (param[2].Contains("?"))
                            {
                                Regex typeRegex = new Regex(@"T\d+N\d+D\d+R\d+");
                                ret._plant = typeRegex.Match(param.Length == 8 ? param[7] : param.Last()).Value;
                                ret._version = param[4];
                            }
                            else
                            {
                                ret._plant = param[2].StartsWith("A") ? param[2] : string.Empty;
                                ret._version = param.Length > 6 ? param[6] : param[5];
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    ret._plant = param[2].StartsWith("A") ? param[2] : string.Empty;
                    ret._version = param.Length > 6 ? param[6] : param[5];
                }

            }
            else if (param[0] == "TEZ")
            {
                try
                {
                    ret._deviceType = "���-24";
                    ret._version = param[5] == "1.4" ? "1.4 � ����" : "�� 1.4";
                }
                catch (Exception e)
                {
                    ret._deviceType = param[0] + param[1];
                    ret._plant = param[3];
                    ret._version = param[4];
                }
            }

            // ��� 2-�� ��������� � ��������� ???
            else if (param[0] == "MR" && param[1] == "771" && param[2].EndsWith("?") && param.Length == 7)
            {
                ret._deviceType = param[0] + param[1]; 
                ret._version = param[4];
                Regex typeRegex = new Regex(@"T\d+N\d+D\d+R\d+");
                ret._plant = typeRegex.Match(param[6]).Value;
            }

            // ��� 2-�� ��������� � ��������� ???
            else if (param[0] == "MR" && param[1] == "771" && param[2].EndsWith("?") && param.Length == 5)
            {
                ret._deviceType = param[0] + param[1];
                ret._version = param[4];
                ret._plant = string.Empty;
            }

            else if (param.Length == 5)
            {
                ret._deviceType = param[0] + param[1];
                ret._plant = param[3];
                ret._version = param[4].Any(char.IsLetter) ? param[4].Remove(param[4].Length - 1) : param[4];
            }
            else if (param.Length >= 8 && (param[5] == "A" || param[5] == "B")) // ��� �������� � �������� ������� (��761 >= 3.06)
            {
                ret._deviceType = param[0] + param[1];
                Regex typeRegex = new Regex(@"T\d+N\d+D\d+R\d+");
                ret._plant = typeRegex.Match(param.Length == 8 ? param[7] : param.Last()).Value;
                ret._version = param[4];
            }
            else if (param.Length >= 8)
            {
                ret._deviceType = param[0] + param[1];
                if (param[2].Contains("?"))
                {
                    Regex typeRegex = new Regex(@"T\d+N\d+D\d+R\d+");
                    ret._plant = typeRegex.Match(param.Length == 8 ? param[7] : param.Last()).Value;
                    if (ret._plant == "") ret._plant = typeRegex.Match(param[6]).Value;
                    ret._version = param[4];
                }
                else
                {
                    Regex typeRegex = new Regex(@"T\d+N\d+D\d+R\d+");

                    ret._plant = typeRegex.Match(param.Length == 8 ? param[7] : param[8]).Value;
                    if (ret._plant == "") ret._plant = typeRegex.Match(param[7]).Value;
                    ret._version = param[5];
                }
                
            }
            else if (param.Length > 6 && param.Length < 8 && param[1] == "761")
            {
                ret._deviceType = param[0] + param[1];
                Regex typeRegex = new Regex(@"T\d+N\d+D\d+R\d+");
                ret._plant = typeRegex.Match(param[6]).Value;
                ret._version = param[4];
            }
            else if (param.Length > 6 && param.Length < 8 && param[1] == "762")
            {
                ret._deviceType = param[0] + param[1];
                Regex typeRegex = new Regex(@"T\d+N\d+D\d+R\d+");
                ret._plant = typeRegex.Match(param[6]).Value;
                ret._version = param[4];
            }
            else if (param.Length > 6 && param.Length < 8 && param[1] == "763")
            {
                ret._deviceType = param[0] + param[1];
                Regex typeRegex = new Regex(@"T\d+N\d+D\d+R\d+");
                ret._plant = typeRegex.Match(param[6]).Value;
                ret._version = param[4];
            }
            else if (param.Length > 5 && param.Length < 8)
            {
                ret._deviceType = param[0] + param[1];
                ret._plant = string.Empty;
                ret._version = param[5];
            }
            else
            {
                ret._deviceType = param[0];
                ret._version = param[3];
                ret._plant = param[1];
            }

            return ret;
        }

        private static string OptimizeDeviceConfigString(string value)
        {
            string ret = string.Empty;
            int i = 0;
            for (; i < value.Length; i++)
            {
                if (value[i] == 'T') break;
            }
            ret = new string(value.Skip(i).ToArray());
            if (value == "$HT6N3D42R35-0" || value == "$HT12N4D26R19-0" || value == "$HT12N5D58R51_0" || value == "$HT12N6D58R51_0" || value == "$HT9N8D58R51_0" ||
                value == "$HT0N0D74R35-0" || value == "$HT0N0D74R35_0" || value == "$HT0N0D106R67_0" || value == "$HT0N0D114R59_0")
                ret = ret.Remove(ret.Length - 2);
            return string.IsNullOrEmpty(ret) ? ret : ret.Split('_')[0];
        }

        public static DeviceInfo GetVersion(ushort[] buffer)
        {
            byte[] byteBuf = Common.TOBYTES(buffer, true);
            return GetVersion(byteBuf);
        }
        #endregion [Methods]
    }
}