﻿using System;
using System.Collections.Generic;

namespace BEMN.Devices
{
    /// <summary>
    /// Интерфейс выбора версии устройства
    /// </summary>
   public interface IDeviceVersion
    {
       /// <summary>
       /// Список форм для текущей версии
       /// </summary>
       Type[] Forms { get; }
       /// <summary>
       /// Список версий устройства
       /// </summary>
       List<string> Versions { get; } 
    }
}
