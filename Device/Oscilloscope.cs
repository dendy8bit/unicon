using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Xml;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MBServer.Queries;

namespace BEMN.Devices
{
    public class Oscilloscope : IDisposable
    {
        

        public double KoefI
        {
            get { return this._koefI; }
            set { this._koefI = value; }
        }

        public double KoefU
        {
            get { return this._koefU; }
            set { this._koefU = value; }
        }

        public double KoefShkaliI
        {
            get { return this._koefShkaliI; }
            set { this._koefShkaliI = value; }
        }

        public double KoefShkaliU
        {
            get { return this._koefShkaliU; }
            set { this._koefShkaliU = value; }
        }

        public int KoefAvary
        {
            get
            {
                //if (_koefAvary != 0)
                //{
                //    return _koefAvary;
                //}
                //else
                //{
                    if ((this._device.Info.Version == "MR500") || (this._device.Info.Version == "MR600") && this._deviceSave != "1")
                    {
                        return 32768 - 2048 * this.KoefZashiti;
                    }
                    else
                    {
                        return this.OscilloscopeSize - this.DurationAfterCrash * this.KoefZashiti;
                    }  
                //}
                
                //OscilloscopeSize - DurationAfterCrash * KoefZashiti;
            }
            set { 
                this._koefAvary = value;
                
            }
        }

        public int RezervAvary
        {
            get
            {
                if (this._rezerv != 0)
                {
                    return this._rezerv;
                }
                else
                {
                    if ((this._device.Info.Type == "MR500") || (this._device.Info.Type == "MR600"))
                    {
                        return 32768 - 2048 * this.KoefZashiti;
                    }
                    else
                    {
                        if ((this._device.Info.Type == "MTZ610.5N") || (this._device.Info.Type == "MRN610N") && this._deviceSave != "1")
                        {
                            return 32768 - 1024 * this.KoefZashiti;
                        }else if (this._koefAvary == 0) 
                        { 
                            return this.OscilloscopeSize - this.DurationAfterCrash * this.KoefZashiti; 
                        }else 
                        { 
                            return this._koefAvary; 
                        }
                        
                    }
                }
            }
            set
            {
                this._rezerv = value;
            }
        }



        public int KoefZashiti
        {
            get
            {
                if ((this._device.DeviceType == "MR500") || (this._device.DeviceType == "MR600") && this._deviceSave != "1")
                {
                    this._koefZash = 10;
                }
                else
                {
                    if (((this._device.DeviceType == "MTZ610.5N") || (this._device.DeviceType == "MRN610N")) && this._deviceSave != "1")
                    {
                        this._koefZash = 8;
                    }
                    else
                    {
                        this._koefZash = 18;
                    }
                }
                return this._koefZash;
            }
           set
            {
                this._koefZash = value;
            }
        }


        public StringCollection AnalogChannels
        {
            get
            {
                return this._analogChannels;
            }
            set
            {
                this._analogChannels = value;
            }
        }

        private StringCollection _analogChannels = new StringCollection();

        public StringCollection AnalogUChannels
        {
            get
            {
                return this._analogUChannels;
            }
            set
            {
                this._analogUChannels = value;
            }
        }
        private StringCollection _analogUChannels = new StringCollection();



        public StringCollection DiskretChannels
        {
            get
            {
                
                return this._diskretChannels;
            }
            set
            {
                this._diskretChannels = value;
            }
        }
        private StringCollection _diskretChannels = new StringCollection();

        public Oscilloscope(Device device)
        {
            this._device = device;
        }
        private Device _device;

        public Device Device
        {
            get { return this._device; }
            set { this._device = value; }
        }
        private string _deviceSave;
        public string Dev
        {
            get
            {
                return this._deviceSave;
            }
            set
            {
                this._deviceSave = value;
            }
        }
        public void Sbros()
        {

        }

        public void InitCrush()
        {
            

        }
        private string _osccr;

        public string OscCr
        {
            get { return this._osccr; }
            set { this._osccr = value; }
        }


        private string  _deviceName;

        public string DeviceName
        {
            get { return this._deviceName; }
            set { this._deviceName = value; }
        }

        private string _dateTime;

        public string DateTime
        {
            get { return this._dateTime; }
            set { this._dateTime = value; }
        }

        private string _dateTimeNew;

        public string DateTimeNew
        {
            get { return this._dateTimeNew; }
            set { this._dateTimeNew = value; }
        }

        private string _deviceKtt;

        public string DeviceKtt
        {
            get { return this._deviceKtt; }
            set { this._deviceKtt = value; }
        }

        private string _deviceKttnp;

        public string DeviceKttnp
        {
            get { return this._deviceKttnp; }
            set { this._deviceKttnp = value; }
        }

        private string _deviceKtn;

        public string DeviceKtn
        {
            get { return this._deviceKtn; }
            set { this._deviceKtn = value; }
        }

        private string _deviceKtnnp;

        public string DeviceKtnnp
        {
            get { return this._deviceKtt; }
            set { this._deviceKtnnp = value; }
        }

        private byte[] _buf;

        public byte[] _BUF
        {
            get
            {
                return this._buf;
            }
            set
            {
                this._buf = value;
            }
        }

        protected Device.slot _numOsc = new Device.slot(0x3F02, 0x3F03);
        protected Device.slot _oscCount = new Device.slot(0x3F00, 0x3F03);
        protected Device.slot _konfCount = new Device.slot(0x1274, 0x1275);
        protected Device.slot _oscSettings = new Device.slot(0x3C00, 0x3C0E);
        protected Device.slot _oscSettings1 = new Device.slot(0x3900, 0x3910);
        protected Device.slot _oscJournal = new Device.slot(0x3800, 0x3820);
        private Device.slot _oscCrush = new Device.slot(0x3800, 0x3801);
        private Device.slot _oscCrush56 = new Device.slot(0x4000, 0x4001);

        public event Handler OscilloscopeSettingsLoadOk;
        public event Handler OscilloscopeSettingsLoadFail;

        public event Handler OscilloscopeDataLoadOk;
        public event Handler OscilloscopeDataLoadFail;

        public event IndexHandler OscilloscopeDataIndexLoadOk;
        public event IndexHandler OscilloscopeDataIndexLoadFail;

        private int _osciloscopeDataIndex = 0;
        private bool _oscilloscopeDataHaveErrors = false;


        protected int _oscilloscopeSize = 32768;
        protected int _settingsLength = 16;
        protected int _journalLength = 24;
        protected int _exchangeCnt = 256;
        protected int _pointCount = 3276;
        protected int _analogChannelsCnt = 5;
        protected int _uAnalogChannelsCnt = 5;
        //protected int _pixelCnt;
        protected int _diskretChannelsCnt = 17;

        protected ushort SLOTS_START;
        protected ushort SLOT_SIZE;

        protected ushort[] _settingsBuffer;
        protected ushort[] _journalBuffer;
        protected byte[] _oscilloscopeData;
        protected List<double[]> _analogData;
        protected List<double[]> _analogUData;
        protected List<double[]> _analogUabcData;
        protected List<double[]> _analogU0Data;
        protected List<double[]> _comTradeData;
        protected List<double[]> _comTradeDataCfg;
        protected double _koefI;
        protected double _koefU;
        protected double _koefShkaliI;
        protected double _koefShkaliU;
        protected int _koefAvary = 0;
        protected int _koefZash = 0;
        private int _rezerv = 0;


        protected List<BitArray> _diskretData;
        protected Device.slot[] _oscilloscopeSlots;

        protected Dictionary<string, string> _journal = new Dictionary<string, string>();

        public virtual void CreateJournal()
        {

        }

        public Dictionary<string, string> Journal
        {
            get
            {
                return this._journal;
            }
        }
        public int PointCount
        {
            get
            {
                return this._pointCount;
            }
            set
            {
                this._pointCount = value;
            }
        }

        public int ExchangeCount
        {
            get
            {
                return this._exchangeCnt;
            }
        }

        public virtual void NewInitialize()
        {
            this._oscilloscopeSize = this.OscilloscopeSize;
            if ((this._device.DeviceType != "MR500") && (this._device.DeviceType != "MR600") && this._deviceSave != "1")
            {
                this._settingsLength = 14;
            }
            else
            {
                this._settingsLength = 16;
            }
            this._journalLength = 24;
            this._exchangeCnt = this._oscilloscopeSize / 128;
            if ((this._device.DeviceType != "MTZ610.5N") && (this._device.DeviceType != "MTZ610.5N") && this._deviceSave != "1")
            {
                this._pointCount = this.OscilloscopeSize / 8;
            }
            else
            {
                this._pointCount = this.OscilloscopeSize / 18;
            }
            
            this._analogChannelsCnt = this.OscilloscopeAnalog +1;
            this._uAnalogChannelsCnt = 5;
            this._diskretChannelsCnt = this.OscilloscopeDiskret+1;
            this.SLOTS_START = 0x4000;
            this.SLOT_SIZE = 0x40;

            //_pixelCnt = (_analogChannelsCnt * _pointCount - 8) / 5;
            this._analogUData = new List<double[]>(this._uAnalogChannelsCnt);
            this._analogUabcData = new List<double[]>(3);
            this._analogU0Data = new List<double[]>(1);
            this._analogData = new List<double[]>(this._analogChannelsCnt);
            this._comTradeData = new List<double[]>(this._pointCount);
            this._comTradeDataCfg = new List<double[]>(this._uAnalogChannelsCnt - 1 + this._analogChannelsCnt-1);
            this._diskretData = new List<BitArray>(this._diskretChannelsCnt);

            this._oscilloscopeData = new byte[this._oscilloscopeSize];
            this._oscilloscopeSlots = new Device.slot[this._exchangeCnt];


            for (int i = 0; i < this._analogChannelsCnt; i++)
            {
                this._analogData.Add(new double[this._pointCount]);
            }

            for (int i = 0; i < this._uAnalogChannelsCnt; i++)
            {
                this._analogUData.Add(new double[this._pointCount]);
            }
            for (int i = 0; i < this._pointCount; i++)
            {
                this._comTradeData.Add(new double[this._analogChannelsCnt - 1 + this._uAnalogChannelsCnt - 1 + this._diskretChannelsCnt -1]);
            }
            for (int i = 0; i < 8; i++)
            {
                this._comTradeDataCfg.Add(new double[2]);
            }


            Device.slot.InitSlots(this._oscilloscopeSlots, this.SLOTS_START, this.SLOT_SIZE, this._exchangeCnt);
        }

        public virtual void Initialize()
        {
            this._oscilloscopeSize = 32768;

            this._journalLength = 24;
            this._exchangeCnt = this._oscilloscopeSize / 128;
            if ((this._device.DeviceType != "MTZ610.5N") && (this._device.DeviceType != "MTZ610.5N") && this._deviceSave != "1")
            {
                
                this._pointCount = 4095;
            }
            else
            {
                this._pointCount = 3276;
            }
            
            this._analogChannelsCnt = 5;
            this._uAnalogChannelsCnt = 5;
            this._settingsLength = 17;

            this._diskretChannelsCnt = 16;
            this.SLOTS_START = 0x4000;
            this.SLOT_SIZE = 0x40;

            this._analogUData = new List<double[]>(this._uAnalogChannelsCnt);
            this._analogData = new List<double[]>(this._analogChannelsCnt);
            this._diskretData = new List<BitArray>(this._diskretChannelsCnt);
            this._oscilloscopeData = new byte[this._oscilloscopeSize];
            this._oscilloscopeSlots = new Device.slot[this._exchangeCnt];
            this._comTradeData = new List<double[]>(this._pointCount);
            this._comTradeDataCfg = new List<double[]>(this._uAnalogChannelsCnt - 1 + this._analogChannelsCnt - 1);


            for (int i = 0; i < this._analogChannelsCnt; i++)
            {
                this._analogData.Add(new double[this._pointCount]);
            }

            for (int i = 0; i < this._uAnalogChannelsCnt; i++)
            {
                this._analogUData.Add(new double[this._pointCount]);
            }
            for (int i = 0; i < this._pointCount; i++)
            {
                this._comTradeData.Add(new double[this._analogChannelsCnt - 1 + this._uAnalogChannelsCnt - 1 + this._diskretChannelsCnt - 1]);
            }
            for (int i = 0; i < 8; i++)
            {
                this._comTradeDataCfg.Add(new double[2]);
            }
            Device.slot.InitSlots(this._oscilloscopeSlots, this.SLOTS_START, this.SLOT_SIZE, this._exchangeCnt);
        }

        public virtual void SetBuffers(ushort[] settings, ushort[] journal)
        {
            this._settingsBuffer = settings;
            this._journalBuffer = journal;
            this.CreateJournal();
        }

        public byte[] OscilloscopeData
        {
            get
            {
                return this._oscilloscopeData;
            }
            set
            {
                this._oscilloscopeData = value;
                if ((this._device.DeviceType == "MTZ610.5N") || (this._device.DeviceType == "44"))
                {
                    this.PrepareData_OldVers(this._oscilloscopeData);
                }
                else
                {
                    this.PrepareData(this._oscilloscopeData);
                }
            }
        }

        public List<double[]> AnalogData
        {
            get
            {
                return this._analogData;
            }
        }

        public List<double[]> AnalogUData
        {
            get
            {
                return this._analogUData;
            }

        }

        public List<BitArray> DiskretData
        {
            get
            {
                return this._diskretData;
            }
        }

        public List<double[]> ComTradeData
        {
            get
            {
                return this._comTradeData;
            }
            set
            {
                this._comTradeData = value;
            }
        }

        public List<double[]> ComTradeDataCfg
        {
            get
            {
                return this._comTradeDataCfg;
            }
            set
            {
                this._comTradeDataCfg = value;
            }
        }

        protected virtual void PrepareData_OldVers(byte[] data)
        {
            for (int i = 0; i < 17; i++)
            {
                this._diskretData.Add(new BitArray(this._pointCount));
            }
            for (int i = 0; i < this._pointCount; i++)
            {
                int channel = 0;
                //�������
                double value = BitConverter.ToUInt16(data, i * 10 + 0);
                this._analogData[channel++][i] = (value / 128 - 256) * 1.41421;
                value = BitConverter.ToUInt16(data, i * 10 + 2);
                this._analogData[channel++][i] = (value / 128 - 256) * 1.41421;
                value = BitConverter.ToUInt16(data, i * 10 + 4);
                this._analogData[channel++][i] = (value / 128 - 256) * 1.41421;
                value = BitConverter.ToUInt16(data, i * 10 + 6);
                this._analogData[channel][i] = (value / 128 - 256) * 1.41421;
                //��������

                double high = BitConverter.ToInt16(data, i * 10 + 8);
                for (int j = 0; j < 8; j++)
                {
                    this._diskretData[j][i] = 0 == ((int)high >> j & 1) ? false : true;
                }
                double low = BitConverter.ToInt16(data, i * 10 + 9);
                for (int j = 0; j < 8; j++)
                {
                    this._diskretData[j + 8][i] = 0 == ((int)low >> j & 1) ? false : true;
                }
            }
        }

        protected virtual void PrepareData(byte[] data)
        {
            for (int i = 0; i < 17; i++)
            {
                this._diskretData.Add(new BitArray(this._pointCount));
            }
            for (int i = 0; i < this._pointCount; i++)
            {
                int channel = 0;
                //�������
                double value = BitConverter.ToUInt16(data, i * 10 + 0);
                this._analogData[channel++][i] = (value / 128 - 256) * 1.41421;
                value = BitConverter.ToUInt16(data, i * 10 + 2);
                this._analogData[channel++][i] = (value / 128 - 256) * 1.41421;
                value = BitConverter.ToUInt16(data, i * 10 + 4);
                this._analogData[channel++][i] = (value / 128 - 256) * 1.41421;
                value = BitConverter.ToUInt16(data, i * 10 + 6);
                this._analogData[channel][i] = (value / 128 - 256) * 1.41421;
                //��������

                double high = BitConverter.ToInt16(data, i * 10 + 8);
                for (int j = 0; j < 8; j++)
                {
                    this._diskretData[j][i] = 0 == ((int)high >> j & 1) ? false : true;
                }
                double low = BitConverter.ToInt16(data, i * 10 + 9);
                for (int j = 0; j < 8; j++)
                {
                    this._diskretData[j + 8][i] = 0 == ((int)low >> j & 1) ? false : true;
                }
            }

        }

        public virtual void SetData(Device.slot[] dataSlots)
        {
            byte[] buffer = new byte[this._oscilloscopeSize];
            int slotSize = 0x80;
            for (int i = 0; i < dataSlots.Length; i++)
            {
                Array.ConstrainedCopy(Common.TOBYTES(dataSlots[i].Value, false), 0, this._oscilloscopeData, i * slotSize, slotSize);
            }
            // ��������� ������������
            Array.ConstrainedCopy(this._oscilloscopeData, this.OscilloscopeBegin, buffer, 0, this._oscilloscopeSize - this.OscilloscopeBegin);
            Array.ConstrainedCopy(this._oscilloscopeData, 0, buffer, this._oscilloscopeSize - this.OscilloscopeBegin, this.OscilloscopeBegin);
            Array.ConstrainedCopy(buffer, 0, this._oscilloscopeData, 0, this._oscilloscopeSize);
            this.OscilloscopeData = this._oscilloscopeData;
        }


        public ushort FrequencyDiskret
        {
            get
            {
                return this._settingsBuffer[8];
            }
        }
        public virtual ushort AnalogCount
        {
            get
            {
                return this._settingsBuffer[9];
            }
        }
        public ushort DiskretCount
        {
            get
            {
                return this._settingsBuffer[10];
            }
        }
        public virtual ushort OscilloscopeKonfCount
        {
            get
            {
                return this._konfCount.Value[0];
            }
        }

        public virtual double POscilloscopeCrush
        {
            set
            {
                this._oscCrush.Value[0] = (ushort)value;
            }
        }

        public virtual double POscilloscopeCrush56
        {
            set
            {
                this._oscCrush56.Value[0] = (ushort)value;
            }
        }

        public void CrushOsc()
        {
            this._device.SaveSlot6(this._device.DeviceNumber, this._oscCrush, "����� ������������� - ����������� ���",this._device);
        }

        public void CrushOsc56()
        {
            this._device.SaveSlot6(this._device.DeviceNumber, this._oscCrush56, "����� ������������� - ����������� ���",this._device);
        }

        public virtual ushort OscilloscopeCount
        {
            get
            {
                return this._oscCount.Value[0];
            }
            set
            {
                this._oscCount.Value[0] = value;
            }
        }


        public virtual ushort OscilloscopeAnalog
        {
            get
            {
                return this._oscSettings.Value[9];
            }
        }

        public virtual ushort OscilloscopeDiskret
        {
            get
            {
                return this._oscSettings.Value[10];
            }
        }

        public virtual ushort OscilloscopeSize
        {
            get
            {
                if ((this._device.DeviceType == "MR500" || this._device.DeviceType == "MR600") && this._deviceSave != "1")
                {
                    return 32768;
                }
                else
                {
                    if (this._device.DeviceType == "MTZ610.5N" || this._device.DeviceType == "MTZ610.5N" && this._deviceSave != "1")
                    {
                        return 32768;
                    }
                    else
                    {
                        return (ushort)(this._oscSettings.Value[11] * 2);
                    }
                }
            }
            
        }
        public virtual ushort OscilloscopeCrash
        {
            get
            {
                return this._oscSettings.Value[12];
            }
        }




        public virtual ushort OscilloscopeBegin
        {
            get
            {
                int k;
                int otsch;
                int ret;
                if ((this._device.DeviceType == "MR500") || (this._device.DeviceType == "MR600") && this._deviceSave != "1")
                {
                    otsch = 10;
                }
                else
                {
                    if ((this._device.DeviceType == "MTZ610.5N") || (this._device.DeviceType == "MTZ610.5N") && this._deviceSave != "1")
                    {
                        otsch = 8;
                    }
                    else
                    {
                        otsch = 18;
                    }
                    
                }

                if ((this._device.DeviceType == "MR500") || (this._device.DeviceType == "MR600") && this._deviceSave != "1")
                {
                    ret = this.PositionCrash + this.DurationAfterCrash * 10 + 8;
                }
                else
                {
                    if ((this._device.DeviceType == "MTZ610.5N") || (this._device.DeviceType == "MTZ610.5N") && this._deviceSave != "1")
                    {
                        ret = this.PositionCrash + this.DurationAfterCrash * 8 + this.OscilloscopeSize % otsch;
                    }
                    else
                    {
                        switch (this._konfCount.Value[0])
                        {
                            case 0:
                                k = 1;
                                break;
                            case 1:
                                k = 1;
                                break;
                            case 2:
                                k = 2;
                                break;
                            default:
                                k = 0;
                                break;
                        }
                        ret = this.PositionCrash + (this.DurationAfterCrash + k) * otsch + this.OscilloscopeSize % otsch;
                    } 
                }
                while (ret >= this._oscilloscopeSize)
                {
                    ret -= this._oscilloscopeSize;
                }
                return (ushort)ret;
            }
        }

        public virtual ushort PositionCrash
        {
            get
            {
                ushort ret = 0;
                if ((this._device.DeviceType == "MR500") || (this._device.DeviceType == "MR600") && this._deviceSave != "1")
                {
                    try 
                    {
                        ret = this._oscJournal.Value[1] >= this._oscilloscopeSize ? (ushort)(this._oscJournal.Value[1] - this._oscilloscopeSize) : this._oscJournal.Value[1];
                    }
                    catch (Exception) 
                    {

                    }
                    return ret;
                }
                else
                {
                    if ((this._device.DeviceType == "MTZ610.5N") || (this._device.DeviceType == "MTZ610.5N") && this._deviceSave != "1")
                    {
                        ret = this._oscJournal.Value[1] >= this._oscilloscopeSize ? (ushort)(this._oscJournal.Value[1] - this._oscilloscopeSize) : this._oscJournal.Value[1];
                        return ret;
                    }
                    else
                    {
                        ret = this._oscSettings.Value[12] >= this._oscilloscopeSize ? (ushort)(this._oscSettings.Value[12] - this._oscilloscopeSize) : this._oscSettings.Value[12];
                        return (ushort)(ret * 2);
                    }
                }
            }
        }
        public virtual ushort DurationAfterCrash
        {
            get
            {
                if ((this._device.DeviceType == "MR500") || (this._device.DeviceType == "MR600") && this._deviceSave != "1")
                {
                    try { return 2048; }
                    catch (Exception)
                    {
                        return 0;
                    }
                    
                }
                else
                {
                    if ((this._device.DeviceType == "MTZ610.5N") || (this._device.DeviceType == "MTZ610.5N") && this._deviceSave != "1")
                    {
                        return 1024;
                    }
                    else
                    {
                        return this._oscSettings.Value[13];
                    }
                    
                }
            }
        }
        public virtual ushort OscilloscopeAddress
        {
            get
            {

                return this._journalBuffer[1];
            }
        }

        public ushort NumOsc
        {
            get
            {
                return this._numOsc.Value[0];
            }
            set
            {
                this._numOsc.Value[0] = value;
            }
        }

        public ushort OScK
        {
            get 
            {
                return this._oscCount.Value[0];
            }
        }

        public ushort OScKM5
        {
            get
            {
                return this._oscJournal.Value[0];
            }
        }

        public virtual void LoadOscilloscopeSettings()
        {
            if ((this._device.DeviceType != "MR500") && (this._device.DeviceType != "MR600") && (this._device.DeviceType != "MTZ610.5N") && (this._deviceSave != "1"))
            {
                this._device.LoadSlot(this._device.DeviceNumber, this._konfCount, "�" + this._device.DeviceNumber + " ��������� ������������",this._device);
                if ((this._device.DeviceType != "MR600") && (this._device.DeviceType != "MTZ610.5N") && (this._deviceSave != "1"))
                {
                    this._device.LoadSlot(this._device.DeviceNumber, this._oscCount, "�" + this._device.DeviceNumber + " ��������� ����� ������������", this._device);
                }
                if (this.OscilloscopeCount != 0)
                {
                    this._device.LoadSlot(this._device.DeviceNumber, this._oscSettings, "�" + this._device.DeviceNumber + " ��������� ��������� ������������", this._device);
                }
            }
            else
            {
                this._oscSettings = new Device.slot(0x3900, 0x390E);
                if ((this._device.DeviceType != "MR600") && (this._device.DeviceType != "MTZ610.5N") && (this._deviceSave != "1"))
                {
                    this._device.LoadSlot(this._device.DeviceNumber, this._oscCount, "�" + this._device.DeviceNumber + " ��������� ����� ������������", this._device);
                }
                this._device.LoadSlot(this._device.DeviceNumber, this._oscSettings, "�" + this._device.DeviceNumber + " ��������� ��������� ������������", this._device);
            }

            this._device.LoadSlot(this._device.DeviceNumber, this._oscJournal, "�" + this._device.DeviceNumber + " ��������� ������ ������������", this._device);
        }

        public void LoadCount()
        {
            this._device.LoadSlot(this._device.DeviceNumber, this._oscCount, "�" + this._device.DeviceNumber + " ��������� ����� ������������", this._device);
        }

        public virtual void LoadOscilloscopeData(ushort num)
        {
            this._device.LoadSlot(this._device.DeviceNumber, this._konfCount, "�" + this._device.DeviceNumber + " ��������� ����������",this._device);
        }

        public virtual void LoadOscilloscopeNum(ushort num)
        {
            this.NumOsc = num;
            this._device.SaveSlot6(this._device.DeviceNumber, this._numOsc, "SaveNum" + this._device.DeviceNumber,this._device);
        }

        public virtual void Serialize(string filename)
        {
            XmlDocument doc = new XmlDocument();
            IDeviceView idevice = (IDeviceView)this._device;
            XmlElement device = doc.CreateElement(idevice.NodeName);
            XmlElement journal = doc.CreateElement("Journal");
            Dictionary<string, string>.Enumerator ienum = this.Journal.GetEnumerator();

            while (ienum.MoveNext())
            {
                XmlElement journalItem = doc.CreateElement(ienum.Current.Key);
                journalItem.InnerText = ienum.Current.Value;
                journal.AppendChild(journalItem);
            }
            XmlElement data = doc.CreateElement("OscilloscopeData");
            data.InnerText = Convert.ToBase64String(this.OscilloscopeData);

            device.AppendChild(journal);
            device.AppendChild(data);
            doc.AppendChild(device);
            doc.Save(filename);
        }

        public virtual void Deserialize(string filename)
        {
            IDeviceView idevice = (IDeviceView)this._device;
            XmlDocument doc = new XmlDocument();
            doc.Load(filename);
            if (idevice.NodeName != doc.DocumentElement.Name)
            {
                throw new Exception(doc.DocumentElement.Name + " not acceptable");
            }
            XmlElement journal = doc.DocumentElement["Journal"];
            this._journal.Clear();
            foreach (XmlElement el in journal)
            {
                this._journal.Add(el.Name, el.InnerText);
            }

            this.OscilloscopeData = Convert.FromBase64String(doc.DocumentElement["OscilloscopeData"].InnerText);
        }

        public virtual void LoadOscilloscopeData()
        {

            if (null == this._settingsBuffer)
            {
                throw new InvalidOperationException("Load journal settings first");
            }
            this._osciloscopeDataIndex = 0;
            this._oscilloscopeDataHaveErrors = false;

            for (int i = 0; i < this._exchangeCnt; i++)
            {
                Query query = new Query();
                query.name = "�" + this._device.DeviceNumber + " ������ �������������"+i;
                query.device = this._device.DeviceNumber;
                query.func = 0x04;
                query.priority = 10;
                query.dataStart = this._oscilloscopeSlots[i].Start;
                this._oscilloscopeSlots[i].Loaded = false;
                query.globalSize = this._oscilloscopeSlots[i].Size;
                this._device.MB.AddQuery(query);
            }

        }
        public virtual void OnMbCompleteExchange(object sender, Query query)
        {
            #region �������������

            if (query.name == "�" + this._device.DeviceNumber +" ��������� ����� ������������")
            {
                this._oscCount.Value = Common.TOWORDS(query.readBuffer, true);
            }

            if (query.name == "�" + this._device.DeviceNumber +" ��������� ������������")
            {
                this._konfCount.Value = Common.TOWORDS(query.readBuffer, true);
            }

            if ("�" + this._device.DeviceNumber + " ������ �������������"+this._osciloscopeDataIndex == query.name)
            {
                this._osciloscopeDataIndex++;

                if (0 == query.fail)
                {
                    if (null != this.OscilloscopeDataIndexLoadOk)
                    {
                        this._oscilloscopeSlots[this._osciloscopeDataIndex - 1].Value = Common.TOWORDS(query.readBuffer, true);
                        this.OscilloscopeDataIndexLoadOk(this, this._osciloscopeDataIndex);
                    }
                }
                else
                {
                    this._oscilloscopeDataHaveErrors = true;
                    if (null != this.OscilloscopeDataIndexLoadFail)
                    {
                        this.OscilloscopeDataIndexLoadFail(this, this._osciloscopeDataIndex);
                    }
                }
                if (this._exchangeCnt == this._osciloscopeDataIndex)
                {
                    if (this._oscilloscopeDataHaveErrors)
                    {
                        if (null != this.OscilloscopeDataLoadFail)
                        {
                            this.OscilloscopeDataLoadFail(this);
                        }
                    }
                    else
                    {
                        if (null != this.OscilloscopeDataLoadOk)
                        {
                            this.SetData(this._oscilloscopeSlots);
                            this.OscilloscopeDataLoadOk(this);
                        }

                    }
                }
            }
            if ("�" + this._device.DeviceNumber + " ��������� ��������� ������������" == query.name)
            {
                if (0 == query.fail)
                {
                    this._oscSettings.Value = Common.TOWORDS(query.readBuffer, true);
                }
            }




            if ("�" + this._device.DeviceNumber + " ��������� ������ ������������" == query.name)
            {
                if (0 == query.fail)
                {
                    this._oscJournal.Value = Common.TOWORDS(query.readBuffer, true);
                    this.SetBuffers(this._oscSettings.Value, this._oscJournal.Value);

                    if (null != this.OscilloscopeSettingsLoadOk)
                    {
                        this.OscilloscopeSettingsLoadOk(this);
                    }
                }
                else
                {
                    if (null != this.OscilloscopeSettingsLoadFail)
                    {
                        this.OscilloscopeSettingsLoadFail(this);
                    }
                }
            }
            #endregion
        
        }

        #region IDisposable Members

        public void Dispose()
        {
            this._device.MB.RemoveQuery("�" + this._device.DeviceNumber + " ������ �������������");
        }

        #endregion
    }
}
