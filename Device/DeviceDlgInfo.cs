﻿using System;
using System.Xml;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.Devices
{
    [Serializable]
    public class DeviceDlgInfo : INodeSerializable
    {
        public bool IsConnectionMode { get; set; }
        public byte DeviceNumber { get; set; }
        public byte PortNumber { get; set; }
        public bool IsOkPressed { get; set; }
        public ModbusType ModbusType { get; set; }
        public string Ip { get; set; }
        public int TcpPort { get; set; }
        public int ReceiveTimeout { get; set; }

        public DeviceDlgInfo()
        {
            this.IsConnectionMode = true;
            this.DeviceNumber = this.PortNumber = 0;
            this.IsOkPressed = false;
            this.ModbusType = ModbusType.ModbusRtu;
            this.Ip = string.Empty;
            this.TcpPort = 502;
            this.ReceiveTimeout = 3000;
        }


        public XmlElement ToXml(XmlDocument doc)
        {
            XmlElement info = doc.CreateElement("DeviceDlgInfo");

            XmlElement conn = doc.CreateElement("IsConnectionMode");
            conn.InnerText = this.IsConnectionMode.ToString();
            info.AppendChild(conn);

            XmlElement devNum = doc.CreateElement("DeviceNumber");
            devNum.InnerText = this.DeviceNumber.ToString();
            info.AppendChild(devNum);

            XmlElement portNum = doc.CreateElement("PortNumber");
            portNum.InnerText = this.PortNumber.ToString();
            info.AppendChild(portNum);

            XmlElement okPres = doc.CreateElement("IsOkPressed");
            okPres.InnerText = this.IsOkPressed.ToString();
            info.AppendChild(okPres);

            XmlElement modbus = doc.CreateElement("ModbusType");
            modbus.InnerText = this.ModbusType.ToString();
            info.AppendChild(modbus);

            XmlElement ip = doc.CreateElement("IP");
            ip.InnerText = this.Ip;
            info.AppendChild(ip);

            XmlElement tcpPort = doc.CreateElement("TcpPort");
            tcpPort.InnerText = this.TcpPort.ToString();
            info.AppendChild(tcpPort);

            XmlElement timeout = doc.CreateElement("ReceiveTimeout");
            timeout.InnerText = this.ReceiveTimeout.ToString();
            info.AppendChild(timeout);

            return info;
        }

        public void FromXml(XmlElement element)
        {
            foreach (XmlElement childNode in element.ChildNodes)
            {
                switch (childNode.Name)
                {
                    case "IsConnectionMode":
                        this.IsConnectionMode = Convert.ToBoolean(childNode.InnerText);
                        break;
                    case "DeviceNumber":
                        this.DeviceNumber = Convert.ToByte(childNode.InnerText);
                        break;
                    case "PortNumber":
                        this.PortNumber = Convert.ToByte(childNode.InnerText);
                        break;
                    case "IsOkPressed":
                        this.IsOkPressed = Convert.ToBoolean(childNode.InnerText);
                        break;
                    case "ModbusType":
                        this.ModbusType = (ModbusType) Enum.Parse(typeof (ModbusType), childNode.InnerText);
                        break;
                    case "IP":
                        this.Ip = childNode.InnerText;
                        break;
                    case "TcpPort":
                        this.TcpPort = Convert.ToInt32(childNode.InnerText);
                        break;
                    case "ReceiveTimeout":
                        this.ReceiveTimeout = Convert.ToInt32(childNode.InnerText);
                        break;
                }
            }
        }
    }
}
