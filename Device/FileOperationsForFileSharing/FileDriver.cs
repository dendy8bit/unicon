﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace BEMN.Devices.FileOperationsForFileSharing
{
    internal enum FileAccess
    {
        READ_FILE,
        WRITE_FILE,
        DELETE_FILE,
        SEND_COMMAND
    }

    internal enum Command
    {
        GETELEMENTDIR,
        FILEOPEN,
        FILECLOSE,
        FILEREAD,
        FILEWRITE,
        FILEDEL,
        CREATEDIR,
        NEWPASSWORD
    }
    
    public class FileDriver
    {
        #region Const
        private const string GETELEMENTDIR_FIRST = "GETELEMENTDIR 0:";
        private const string GETELEMENTDIR = "GETELEMENTDIR";
        #endregion

        #region Internal class
        internal class FileInfo
        {
            public FileAccess Access { get; private set; }
            public ushort StartSessionNum { get; private set; }
            public string Directory { get; private set; }
            public string FileName { get; private set; }
            public string Password;

            public FileInfo(string directory, string fileName, ushort num, FileAccess access, string password = "АААА")
            {
                this.Directory = directory;
                this.FileName = fileName;
                this.StartSessionNum = num;
                this.Access = access;
                this.Password = password;
            }
        }

        internal class DirectoryInfo
        {
            public FileAccess Access { get; private set; }
            public ushort StartSessionNum { get; private set; }
            public string Directory { get; private set; }

            public DirectoryInfo(string directory, ushort num, FileAccess access)
            {
                this.Directory = directory;
                this.StartSessionNum = num;
                this.Access = access;
            }
        }

        #endregion

        #region Private Fields
        private Control _control;
        //private Operator[] _allOperators;
        private DirectoryOperator _directoryOperator;
        private SessionNumberOperator _sessionNumberOperator;
        private DescriptorOperator _descriptorOperator;
        private ReadDataOperator _readDataOperator;
        private CloseOperator _closeOperator;
        private WriteDataOperator _writeDataOperator;
        private DeleteFileOperator _deleteFileOperator;
        private SendCommandOperator _commandOperator;
        private Dictionary<int, string> _messagesDictionary;
        private Dictionary<string, int> _openedFiles;
        private string _selectedFile;
        private byte[] _writeBytes;
        private FileAccess _access;
        private Command _accessCommand;
        private Device _device;
        List<string> list = new List<string>();

        public string Password;
        public string OldPassword;

        private Action<byte[], string> _completeReadDelegate;
        private Action<bool, string> _completeAction;
        private Action<List<string>, string> _completeGetAllFilesInDirAction;
        private Action<string, string> _completeSetCommandAction;

        #endregion
        /// <summary>
        /// Драйвер чтения, записи и удаленя файлов на устройстве
        /// </summary>
        /// <param name="device">Устройство</param>
        /// <param name="invControl">Окно или контрол, в котором идет работа</param>
        public FileDriver(Device device, Control invControl)
        {
            _device = device;
            this._control = invControl;
            this._openedFiles = new Dictionary<string, int>();
            this.InitErrorMessages();

            this._directoryOperator = new DirectoryOperator(device);
            this._sessionNumberOperator = new SessionNumberOperator(device);
            this._descriptorOperator = new DescriptorOperator(device);
            this._readDataOperator = new ReadDataOperator(device);
            this._closeOperator = new CloseOperator(device);
            this._writeDataOperator = new WriteDataOperator(device);
            this._deleteFileOperator = new DeleteFileOperator(device);

            //this._allOperators = new Operator[]
            //{
            //    this._directoryOperator, this._sessionNumberOperator, this._descriptorOperator, this._readDataOperator,
            //    this._closeOperator, this._writeDataOperator, this._deleteFileOperator
            //};
        }

        public Dictionary<int, string> MessagesDictionary
        {
            get { return this._messagesDictionary; }
        }

        private void InitErrorMessages()
        {
            this._messagesDictionary = new Dictionary<int, string>
            {
                {0,"Операция успешно выполнена"},
                {1,"Обращение к зарезервированной памяти"},
                {2,"Подана неверная команда"},
                {5,"Слишком много открытых файлов"},
                {6,"Файл еще не открыт"},
                {7,"Неверный пароль"},
                {8,"Ошибка дескриптора файла"},
                {9,"Ошибка CRC"},
                {101,"Произошла невосстановимая ошибка на низком уровне"},
                {102,"Ошибка структуры FAT на томе или рабочая область испорчена"},
                {103,"Диск не готов"},
                {104,"Файл не найден"},
                {105,"Не найден путь"},
                {106,"Указанная строка содержит недопустимое имя"},
                {107,"Отканано в доступе"},
                {108,"Файл или папка с таким именем уже существуют"},
                {109,"Предоставленная структура объекта\nфайла/директории ошибочна"},
                {110,"Дествие произведено на защищенном\nот записи носителе данных"},
                {111,"Указан недопустимый номер диска"},
                {112,"Рабочая область логического диска\nне зарегистрирована"},
                {113,"На диске нет рабочего тома с файловой системой FAT"},
                {114,"Функция остановлена перед началом форматирования"},
                {115,"Функция остановлена из-за таймаута\nв безопасном управлении потоками"},
                {116,"Доступ к файлу отклонен управлением\nсовместного доступа к файлу.\nПерезагрузите, пожалуйста, устройство"},
                {117,"Недостаточно памяти для выполнения операции"},
                {118,"Количество открытых файлов достигло\nмаксимального количества"},
                {119,"Указанный параметр недопустим"},
                {224,"Операция не была выполнена"},
                {255,"Нет связи с устройством, невозможно выполнить операцию"}
            };
        }

        public bool ReadFile(Action<byte[], string> onOperationComplete, string fileName)
        {
            if (this._openedFiles.ContainsKey(fileName))
                return false;
            this._selectedFile = fileName;
            this._completeReadDelegate = onOperationComplete;
            this._access = FileAccess.READ_FILE;
            this._directoryOperator.Perform(this.DirectoryRead, null);
            return true;
        }

        public bool GetAllElementInDir(Action<List<string>, string> onOperationComplete, string command)
        {
            _commandOperator = new SendCommandOperator(_device, command);
            list.Clear();
            this._completeGetAllFilesInDirAction = onOperationComplete;
            this._accessCommand = Command.GETELEMENTDIR;
            this._commandOperator.Perform(OutputAllFilesInDirectory, null);
            return true;
        }

        public bool SendCommand(Action <string, string> onOperationComplete, string command)
        {
            if (string.IsNullOrEmpty(command)) return false;

            var output = Regex.Replace(command.Split()[0], @"[^0-9a-zA-Z\ ]+", "");

            switch (output)
            {
                case "NEWPASSWORD":
                    _commandOperator = new SendCommandOperator(_device, command);
                    _completeSetCommandAction = onOperationComplete;
                    _access = FileAccess.SEND_COMMAND;
                    _accessCommand = Command.NEWPASSWORD;
                    this._sessionNumberOperator.Perform(this.SessionNumRead, null);
                    break;
                case "FILEDEL":
                    _commandOperator = new SendCommandOperator(_device, command);
                    _completeSetCommandAction = onOperationComplete;
                    _access = FileAccess.SEND_COMMAND;
                    _accessCommand = Command.FILEDEL;
                    this._sessionNumberOperator.Perform(this.SessionNumRead, null);
                    break;
                default:
                    _commandOperator = new SendCommandOperator(_device, command);
                    _completeSetCommandAction = onOperationComplete;
                    this._commandOperator.Perform(SendCommandIsDone, null);
                    break;
            }
            return true;
        }

        public bool WriteFile(Action<bool, string> onOperationComplete, byte[] wBytes, string fileName)
        {
            if (this._openedFiles.ContainsKey(fileName))
                return false;
            this._selectedFile = fileName;
            this._completeAction = onOperationComplete;
            this._access = FileAccess.WRITE_FILE;
            this._directoryOperator.Perform(this.DirectoryRead, null);
            this._writeBytes = wBytes;
            return true;
        }

        private void SendCommandIsDone(int key)
        {
            this._control.BeginInvoke(this._completeSetCommandAction, _commandOperator.FileInDirectory, this._messagesDictionary[key]);
        }

        private void OutputAllFilesInDirectory(int key)
        {
            string[] arg = _commandOperator.FileInDirectory.Split();

            if (arg[0] != ";")
            {
                list.Add(_commandOperator.FileInDirectory);
                _commandOperator = new SendCommandOperator(_device, "GETELEMENTDIR");
                this._commandOperator.Perform(OutputAllFilesInDirectory, null);
            }
            else
            {
                this._control.BeginInvoke(this._completeGetAllFilesInDirAction, list, this._messagesDictionary[key]);
            }
        }

        private void DirectoryRead(int key)
        {
            if (key == 0)
            {
                this._sessionNumberOperator.Perform(this.SessionNumRead, null);
            }
            else
            {
                if (this._access == FileAccess.READ_FILE)
                {
                    this.OperationReadComplete(key);
                }
                else
                {
                    this.OperationWriteComplete(key);
                }
            }
        }

        private void SessionNumRead(int key)
        {
            if (key == 0)
            {
                switch (_access)
                {
                    case FileAccess.DELETE_FILE:
                        this._deleteFileOperator.Perform(this.OperationWriteComplete,
                            new FileInfo(this._directoryOperator.Directory, this._selectedFile,
                                this._sessionNumberOperator.SessionNumber, this._access, Password));
                        break;
                    case FileAccess.SEND_COMMAND:
                        switch (_accessCommand)
                        {
                            case Command.NEWPASSWORD:
                                _commandOperator.NewPassword = Password;
                                _commandOperator.OldPassword = OldPassword;
                                _commandOperator.Perform(SendCommandIsDone, _sessionNumberOperator.SessionNumber);
                                break;
                            default:
                                _commandOperator.Perform(SendCommandIsDone, null);
                                break;
                        }
                        break;
                    default:
                        this._descriptorOperator.Perform(this.DescriptorRead,
                            new FileInfo(this._directoryOperator.Directory + "\\boot\\", this._selectedFile,
                                this._sessionNumberOperator.SessionNumber, this._access, Password));
                        break;
                }
            }
            else
            {
                switch (_access)
                {
                    case FileAccess.READ_FILE:
                        this.OperationReadComplete(key);
                        break;
                    case FileAccess.WRITE_FILE:
                        this.OperationWriteComplete(key);
                        break;
                    case FileAccess.SEND_COMMAND:
                        break;
                    default:
                       
                        break;
                } 
            }
        }

        private void DescriptorRead(int key)
        {
            if (key == 0)
            {
                this._openedFiles.Add(this._selectedFile, key);
                if (this._access == FileAccess.READ_FILE)
                {
                    this._readDataOperator.Perform(this.OnOperationDataComplete, this._descriptorOperator.Descriptor);
                }
                else
                {
                    this._writeDataOperator.Perform(this.OnOperationDataComplete,
                        new WriteDataOperator.WriteFileOption(this._descriptorOperator.Descriptor, this._writeBytes));
                }
            }
            else
            {
                if (this._access == FileAccess.READ_FILE)
                {
                    this.OperationReadComplete(key);
                }
                else
                {
                    this.OperationWriteComplete(key);
                }
            }
        }

        private void OnOperationDataComplete(int key)
        {
            if (key == 0)
            {
                this._closeOperator.Perform(this.OnCloseFileComplete, this._descriptorOperator.Descriptor);
            }
            else if (this._access == FileAccess.READ_FILE)
            {
                this.OperationReadComplete(key);
            }
            else
            {
                this.OperationWriteComplete(key);
            }
        }

        private void OnCloseFileComplete(int key)
        {
            if (key == 0)
            {
                this._openedFiles.Remove(this._selectedFile);
            }
            if (this._access == FileAccess.READ_FILE)
            {
                this.OperationReadComplete(key);
            }
            else
            {
                this.OperationWriteComplete(key);
            }
        }
        
        private void OperationReadComplete(int key)
        {
            this._control.BeginInvoke(this._completeReadDelegate, key == 0 ? this._readDataOperator.ReadBytes : new byte[0], this._messagesDictionary[key]);
        }

        private void OperationWriteComplete(int key)
        {
            this._control.BeginInvoke(this._completeAction, key == 0, this._messagesDictionary[key]);
        }

        private void OperationSendCommandComplete(int key)
        {
            this._control.BeginInvoke(this._completeAction, _commandOperator.FileInDirectory, this._messagesDictionary[key]);
        }

        public void DeleteFile(Action<bool, string> onDelComplete, string fileName)
        {
            this._selectedFile = fileName;
            this._completeAction = onDelComplete;
            this._access = FileAccess.DELETE_FILE;
            this._directoryOperator.Perform(this.DirectoryRead, null);
        }
        
        public void CloseAll(Action<bool, string> onOperationComplete)
        {
            if (this._openedFiles.Count != 0)
            {
                this._selectedFile = this._openedFiles.Keys.First();
                this._closeOperator.Perform(this.OnFileClosed, this._openedFiles[this._selectedFile]);
            }
            else
            {
                this._control.BeginInvoke(this._completeAction, true, string.Empty);
            }
        }

        private void OnFileClosed(int key)
        {
            if (key == 0)
            {
                this._openedFiles.Remove(this._selectedFile);
                if (this._openedFiles.Count != 0)
                {
                    this._selectedFile = this._openedFiles.Keys.First();
                    this._closeOperator.Perform(this.OnFileClosed, this._openedFiles[this._selectedFile]);
                }
                else
                {
                    this._control.BeginInvoke(this._completeAction, true, this._messagesDictionary[key]);
                }
            }
            else
            {
                string message = string.Format("Не удалось закрыть файл {0}. {1}", this._selectedFile,
                    this._messagesDictionary[key]);
                this._control.BeginInvoke(this._completeAction, false, message);
            }
        }
    }
}
