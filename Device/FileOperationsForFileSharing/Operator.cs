﻿using System;
using System.Collections.Generic;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Interfaces;
using BEMN.MBServer;
using DataStruct = BEMN.Devices.FileOperationsForFileSharing.DataStruct;
using OperatorHelpClasses = BEMN.Devices.FileOperationsForFileSharing.OperatorHelpClasses;
using StateStruct = BEMN.Devices.FileOperationsForFileSharing.StateStruct;

namespace BEMN.Devices.FileOperationsForFileSharing
{
    public abstract class Operator
    {
        private const uint USB_BAUND_RATE = 921600;
        public Device CurrentDevice { get; protected set; }
        public abstract string CmdStr { get; }
        protected abstract string StateQeryName { get; }
        protected abstract string DataQeryName { get; }
        protected MemoryEntity<SomeStruct> Cmd { get; set; }
        protected MemoryEntity<StateStruct> State { get; set; }
        protected MemoryEntity<DataStruct> Data { get; set; }

        public int NumException { get; protected set; }
        protected Action<int> completeAction;
        protected bool operationComplete;
        protected ushort dataLen;
        protected int slotLen;

        protected Operator(Device device)
        {
            this.slotLen = ((IDeviceView) device).NodeName == "МР761ОБР" || device.MB.BaudeRate != USB_BAUND_RATE
                ? StructBase.MAX_SLOT_LENGHT_DEFAULT
                : 1024;
            this.CurrentDevice = device;
            this.operationComplete = false;
            this.NumException = 0;
        }

        protected void StartLoadState()
        {
            this.State.LoadStructCycle();
        }

        protected void OperationFail()
        {
            this.operationComplete = false;
            this.NumException = 255;
            this.OnOperationComplete();
        }
        protected void OnOperationComplete()
        {
            this.completeAction.Invoke(this.NumException);
        }

        protected void SetCmd()
        {
            string cmdStr = this.CmdStr; // команда посылаемая в устройство в виде строки
            char[] cmdChar = cmdStr.ToCharArray(); // преобразуем в массив символов
            byte[] bCmd = new byte[cmdChar.Length+1]; // выделяем память под массив байт размерностью на 1 больше
            for (int i = 0; i < bCmd.Length; i++) // заполняем массив байт (каждой букве соответстувет какое-то число)
            {
                if (i >= cmdChar.Length)
                {
                    bCmd[i] = (byte)'\0';
                }
                else
                {
                    bCmd[i] = (byte)cmdChar[i];
                }
            }
            
            SomeStruct cmd = new SomeStruct(); // создаем структуру
            cmd.InitStruct(bCmd); // в свойство Values структуры cmd записываем массив слов, получившийся из массива байт (размерность в 2 раза меньше)
            this.Cmd.Value = cmd; // передаем в MemoryEntity
            this.Cmd.Slots = OperatorHelpClasses.SetSlots(cmd.Values, 0x5000, this.slotLen);
            this.Cmd.SaveStruct(); // записываем слова
        }
        
        protected virtual void ReadState()
        {
            string[] state = this.GetStatesStrings();
            if (state.Length == 0) return;
            this.State.RemoveStructQueries();
            if (this.CheckState(state))
            {
                if (state.Length > 4)
                {
                    this.dataLen = Convert.ToUInt16(state[5]);
                }
                this.Data.LoadStruct();
            }
            else
            {
                this.operationComplete = false;
                this.OnOperationComplete();
            }
        }

        protected string[] GetStatesStrings()
        {
            List<char> listChar = new List<char>();
            byte[] readedBytes = Common.TOBYTES(this.State.Value.StateVal, false);
            foreach (byte b in readedBytes)
            {
                if ((char)b != '\0')
                {
                    listChar.Add((char)b);
                }
                else break;
            }
            return new string(listChar.ToArray()).Split(new[] { ' ', ':' }, StringSplitOptions.RemoveEmptyEntries);
        }

        protected bool CheckState(string[] states)
        {
            int res;
            if (int.TryParse(states[1], out res) && res == 0)
            {
                this.NumException = 0;
                return true;
            }
            this.NumException = res != 0 ? res : 255;
            return false;
        }

        protected string GetDataString(byte[] readBytes)
        {
            List<char> list = new List<char>();
            for (int i = 0; i < this.dataLen; i++)
            {
                if (readBytes[i] != '\0')
                {
                    list.Add((char)readBytes[i]);
                }
            }
            return new string(list.ToArray());
        }
    }
}
