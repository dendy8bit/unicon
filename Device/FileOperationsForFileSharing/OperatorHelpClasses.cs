﻿using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Devices.FileOperationsForFileSharing
{
    public static class OperatorHelpClasses
    {
        public static List<Device.slot> SetSlots(ushort[] values, ushort startAdress, int slotLen)
        {
            List<Device.slot> slotsRet = new List<Device.slot>();
            if (values.Length <= slotLen)
            {
                Device.slot sl = new Device.slot(startAdress, (ushort)(startAdress + values.Length)) { Value = values };
                slotsRet.Add(sl);
            }
            else
            {
                int arrayLength = values.Length / slotLen;
                int lastSlotLength = values.Length % slotLen;
                ushort startAddr = startAdress;
                for (int i = 0; i < arrayLength; i++)
                {
                    Device.slot sl = new Device.slot(startAddr, (ushort)(startAddr + slotLen));
                    sl.Value = values.Skip(i * slotLen).Take(slotLen).ToArray();
                    slotsRet.Add(sl);
                    startAddr += (ushort)slotLen;
                }
                if (lastSlotLength != 0)
                {
                    Device.slot sl = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                    sl.Value = values.Skip(slotsRet.Count * slotLen).Take(lastSlotLength).ToArray();
                    slotsRet.Add(sl);
                }
            }
            return slotsRet;
        }
    }

    public class CmdStruct : StructBase
    {
        public const ushort LEN_USHORT = 0x100;
        [Layout(0, Count = LEN_USHORT)] private ushort[] _cmdVal;

        public ushort[] CmdVal
        {
            get { return this._cmdVal; }
            set { this._cmdVal = value; }
        }
    }

    public class StateStruct : StructBase
    {
        public const ushort LEN_USHORT = 0x80;
        [Layout(0, Count = LEN_USHORT)] private ushort[] _stateVal;

        public ushort[] StateVal
        {
            get { return this._stateVal; }
            set { this._stateVal = value; }
        }
    }

    public class DataStruct : StructBase
    {
        public const ushort LEN_USHORT = 0x400;
        [Layout(0, Count = LEN_USHORT)] private ushort[] _dataVal;

        public ushort[] DataVal
        {
            get { return this._dataVal; }
            set { this._dataVal = value; }
        }
    }
}
