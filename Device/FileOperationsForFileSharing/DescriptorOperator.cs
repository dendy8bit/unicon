﻿using System;
using System.Linq;
using System.Text;
using BEMN.MBServer;
using BEMN.MR7.FileSharingService.FileOperations;

namespace BEMN.Devices.FileOperationsForFileSharing
{
    internal class DescriptorOperator : OperatorReadBase
    {
        private const string FILEOPEN_PATTERN = "FILEOPEN {0};{1};{2}{3}";
        private ushort _currentSessionNum;

        private FileDriver.FileInfo _fileInfo;

        public override string CmdStr
        {
            get
            {
                if (this._fileInfo != null)
                {
                    string access = this._fileInfo.Access == FileAccess.READ_FILE ? "1" : "10";
                    return string.Format(FILEOPEN_PATTERN, this.GeneratePassword(_fileInfo.Password), access, this._fileInfo.Directory,
                        this._fileInfo.FileName);
                }
                else
                {
                    return FILEOPEN_PATTERN.Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries)[0];
                }
            }
        }

        protected override string StateQeryName
        {
            get { return "FileOpenState"; }
        }

        protected override string DataQeryName
        {
            get { return "FileOpenDescriptor"; }
        }

        public int Descriptor { get; private set; }

        public DescriptorOperator(Device device) : base(device)
        {
        }

        public override void Perform(Action<int> complete, object inData)
        {
            completeAction = complete;
            operationComplete = false;
            //InitMemoryEntity(CurrentDevice);
            this._fileInfo = (FileDriver.FileInfo)inData;
            this._currentSessionNum = this._fileInfo.StartSessionNum;
            SetCmd();
        }

        private string GeneratePassword(string password = "АААА")
        {
            if (password == null) password = "АААА";
            string pass = "АААА"; //русские буквы А
            Encoding ascii = Encoding.GetEncoding("windows-1251");
            byte[] asciiBytes = ascii.GetBytes(password);
            string passCode = string.Empty;
            foreach (byte c in asciiBytes)
            {
                this._currentSessionNum = (byte)((this._currentSessionNum * 99 + 53) % 256);
                passCode += ((byte)(c + this._currentSessionNum)).ToString("X2");
            }
            return passCode;
        }

        protected override void ReadData()
        {
            byte[] readBytes = Common.TOBYTES(Data.Value.DataVal, false).Take(dataLen).ToArray();
            this.Descriptor = Convert.ToInt32(GetDataString(readBytes));
            operationComplete = true;
            //RemoveMemoryEntity();
            OnOperationComplete();
        }
    }
}
