﻿using System;
using BEMN.Devices;
using BEMN.Devices.FileOperationsForFileSharing;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;

namespace BEMN.MR7.FileSharingService.FileOperations
{
    internal abstract class OperatorReadAllFilesInDirectory : Operator
    {
        public OperatorReadAllFilesInDirectory(Device device) : base(device)
        {
            Cmd = new MemoryEntity<SomeStruct>(CmdStr, device, 0x5000, slotLen);
            Cmd.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this.StartLoadState);
            Cmd.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this.OperationFail);

            State = new MemoryEntity<StateStruct>(StateQeryName, device, 0x5100, slotLen);
            State.AllReadOk += HandlerHelper.CreateReadArrayHandler(ReadState);
            State.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.OperationFail);

            Data = new MemoryEntity<DataStruct>(DataQeryName, device, 0x5200, slotLen);
            Data.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadData);
            Data.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.OperationFail);
        }

        protected abstract void ReadData();
        public abstract void Perform(Action<int> complete, object inData);
    }
}
