﻿using System;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;

namespace BEMN.Devices.FileOperationsForFileSharing
{
    internal abstract class OperatorReadBase : Operator
    {
        protected OperatorReadBase(Device device) : base(device)
        {
            //Cmd = new MemoryEntity<CmdStruct>(CmdStr, device, 0x5000, len);
            Cmd = new MemoryEntity<SomeStruct>(CmdStr, device, 0x5000, slotLen);
            Cmd.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this.StartLoadState);
            Cmd.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this.OperationFail);

            State = new MemoryEntity<StateStruct>(StateQeryName, device, 0x5100, slotLen);
            State.AllReadOk += HandlerHelper.CreateReadArrayHandler(ReadState);
            State.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.OperationFail);

            Data = new MemoryEntity<DataStruct>(DataQeryName, device, 0x5200, slotLen);
            Data.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadData);
            Data.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.OperationFail);
        }
        
        protected abstract void ReadData();
        public abstract void Perform(Action<int> complete, object inData);
    }
}
