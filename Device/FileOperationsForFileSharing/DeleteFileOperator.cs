﻿using System;
using System.Text;
using BEMN.MR7.FileSharingService.FileOperations;

namespace BEMN.Devices.FileOperationsForFileSharing
{
    internal class DeleteFileOperator : OperatorReadBase
    {
        private const string FILEDEL_PATTERN = "FILEDEL {0};{1}{2}"; 
        private FileDriver.FileInfo _fileInfo;
        private ushort _currentSessionNum;
        
        public override string CmdStr
        {
            get
            {
                if (this._fileInfo != null)
                {
                    return string.Format(FILEDEL_PATTERN, this.GeneratePassword(_fileInfo.Password), this._fileInfo.Directory,
                        this._fileInfo.FileName);
                }
                return FILEDEL_PATTERN.Split(' ')[0];
            }
        }

        protected override string StateQeryName
        {
            get { return "DeleteState"; }
        }

        protected override string DataQeryName
        {
            get { return "DeleteReadData"; }
        }

        public DeleteFileOperator(Device device) : base(device)
        {
            
        }

        public override void Perform(Action<int> complete, object inData)
        {
            completeAction = complete;
            this._fileInfo = (FileDriver.FileInfo) inData;
            this._currentSessionNum = this._fileInfo.StartSessionNum;
            SetCmd();
        }

        protected override void ReadState()
        {
            string[] state = this.GetStatesStrings();
            if (state.Length == 0) return;
            State.RemoveStructQueries();
            operationComplete = CheckState(state);
            OnOperationComplete();
        }

        private string GeneratePassword(string password = "АААА")
        {
            //string pass = "АААА"; //русские буквы А

            Encoding ascii = Encoding.GetEncoding("windows-1251");
            byte[] asciiBytes = ascii.GetBytes(password);
            string passCode = string.Empty;
            foreach (byte c in asciiBytes)
            {
                this._currentSessionNum = (byte)((this._currentSessionNum * 99 + 53) % 256);
                passCode += ((byte)(c + this._currentSessionNum)).ToString("X2");
            }
            return passCode;
        }
        
        protected override void ReadData()
        {
            
        }
    }
}
