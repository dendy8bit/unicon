﻿using System;
using System.Linq;
using BEMN.MBServer;
using BEMN.MR7.FileSharingService.FileOperations;

namespace BEMN.Devices.FileOperationsForFileSharing
{
    internal class WriteDataOperator : OperatorWriteBase
    {
        private const string FILEWRITE_PATTERN = "FILEWRITE {0};{1};{2}"; // дескриптор;длина в байтах;CRC
        private const ushort MAX_LEN = 2048;
        private int _descriptor;
        private ushort _crc;
        private int _curLen;
        private int _residue;
        private int _count;
        private int _counter;
        private byte[] _writeBytes;

        internal class WriteFileOption
        {
            public int Descriptor { get; private set; }
            public byte[] WriteBytes { get; private set; }

            public WriteFileOption(int descriptor, byte[] data)
            {
                this.Descriptor = descriptor;
                this.WriteBytes = data;
            }
        }

        public WriteDataOperator(Device device) : base(device)
        {
            this._crc = 0;
            this._writeBytes = new byte[0];
        }

        public override string CmdStr
        {
            get
            {
                if (this._crc == 0 || this._writeBytes.Length == 0)
                {
                    return FILEWRITE_PATTERN.Split(' ')[0];
                }
                return string.Format(FILEWRITE_PATTERN, this._descriptor, this._curLen, this._crc);
            }
        }

        protected override string StateQeryName
        {
            get { return "WriteDataState"; }
        }

        protected override string DataQeryName
        {
            get { return "WriteData"; }
        }
        
        public override void Perform(Action<int> complete, object inData)
        {
            completeAction = complete;
            operationComplete = false;
            WriteFileOption option = (WriteFileOption) inData;
            this._writeBytes = option.WriteBytes;
            this._descriptor = option.Descriptor;
            //InitMemoryEntity(CurrentDevice);
            this.StartWriteData();
        }

        private void StartWriteData()
        {
            this._count = this._writeBytes.Length/MAX_LEN;
            this._counter = 0;
            this._residue = this._writeBytes.Length%MAX_LEN;
            if (this._residue != 0)
            {
                this._count++;
            }
            this.WriteData();
        }

        protected override void WriteData()
        {
            if (this._count > this._counter)
            {
                this._curLen = this._writeBytes.Skip(MAX_LEN*this._counter).ToArray().Length >= MAX_LEN
                    ? MAX_LEN
                    : this._residue;
                byte[] writeData = new byte[MAX_LEN];
                byte[] buf = this._writeBytes.Skip(MAX_LEN*this._counter).Take(this._curLen).ToArray();
                for (int i = 0; i < buf.Length; i++)
                {
                    writeData[i] = buf[i];
                }
                this._crc = CRC16.CalcCrcFast(buf, buf.Length);
                byte[] crcBytes = Common.TOBYTE(this._crc);
                this._crc = Common.TOWORD(crcBytes[1], crcBytes[0]);
                DataStruct data = new DataStruct();
                data.InitStruct(writeData);
                Data.Value = data;
                Data.SaveStruct();
                this._counter++;
            }
            else
            {
                operationComplete = true;
                //RemoveMemoryEntity();
                OnOperationComplete();
            }
        }

        protected override void ReadState()
        {
            string[] state = GetStatesStrings();
            if (state.Length == 0) return;
            State.RemoveStructQueries();
            if (CheckState(state))
            {
                this.WriteData();
            }
            else
            {
                operationComplete = false;
                OnOperationComplete();
            }
        }
    }
}
