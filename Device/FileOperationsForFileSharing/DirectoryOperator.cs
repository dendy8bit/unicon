﻿using System;
using System.Linq;
using BEMN.MBServer;
using BEMN.MR7.FileSharingService.FileOperations;

namespace BEMN.Devices.FileOperationsForFileSharing
{
    internal class DirectoryOperator : OperatorReadBase
    {
        private string _directory;
        /// <summary>
        /// Возвращает директорию. Если чтение не выполнено, возвращает пустую строку
        /// </summary>
        public string Directory
        {
            get { return operationComplete ? this._directory : string.Empty; }
        }

        public override string CmdStr
        {
            get { return "GETDIR"; }
        }
        protected override string StateQeryName
        {
            get { return "StateGetDir"; }
        }
        protected override string DataQeryName
        {
            get { return "Directory"; }
        }

        public DirectoryOperator(Device device):base(device)
        {
        }
        
        public override void Perform(Action<int> complete, object data)
        {
            completeAction = complete;
            operationComplete = false;
            SetCmd();
        }
        
        protected override void ReadData()
        {
            byte[] readBytes = Common.TOBYTES(Data.Value.DataVal, false).Take(dataLen).ToArray();
            this._directory = GetDataString(readBytes).Replace("/", "");
            operationComplete = true;
            OnOperationComplete();
        }
    }
}
