﻿using System;
using System.Linq;
using System.Text;
using BEMN.MBServer;

namespace BEMN.Devices.FileOperationsForFileSharing
{
    class SendCommandOperator : OperatorReadBase
    {
        private string _command;
        private string _password;
        private int _currentSessionNum;
        public string FileInDirectory { get; private set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }

        public SendCommandOperator(Device device, string command, string password = "АААА") : base(device)
        {
            _command = command;
            _password = password;
        }

        public override string CmdStr
        {
            get
            {
                switch (_command)
                {
                    case "NEWPASSWORD":
                        return _command + " " + GeneratePassword(OldPassword) + ";" + GeneratePassword(NewPassword);
                    default:
                        return _command;

                }
            }
        }

        protected override string StateQeryName => "CommandState";
        protected override string DataQeryName => "DataCommand";
        protected override void ReadData()
        {
            byte[] readBytes = Common.TOBYTES(Data.Value.DataVal, false).Take(dataLen).ToArray();
            FileInDirectory = GetDataString(readBytes).Replace("/", "");
            operationComplete = true;
            OnOperationComplete();
        }

        public override void Perform(Action<int> complete, object inData)
        {
            completeAction = complete;
            operationComplete = false;
            if (inData != null) _currentSessionNum = (ushort) inData;
            SetCmd();
        }

        private string GeneratePassword(string password = "АААА")
        {
            if (password == null) password = "АААА";
            string pass = "АААА"; //русские буквы А
            Encoding ascii = Encoding.GetEncoding("windows-1251");
            byte[] asciiBytes = ascii.GetBytes(password);
            string passCode = string.Empty;
            foreach (byte c in asciiBytes)
            {
                this._currentSessionNum = (byte)((this._currentSessionNum * 99 + 53) % 256);
                passCode += ((byte)(c + this._currentSessionNum)).ToString("X2");
            }
            return passCode;
        }
    }
}
