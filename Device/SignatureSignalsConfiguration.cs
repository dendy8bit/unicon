﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace BEMN.Devices
{
    /// <summary>
    /// Класс, содержащий все подписи сигналов
    /// </summary>
    public class ListsOfConfigurations
    {
        private string _deviceName;

        /// <summary>
        /// Список подписей сигналов для устройства. Сериализует и десериализует файл
        /// </summary>
        /// <param name="device"></param>
        /// <param name="count"></param> для каждого девайся свой размер
        public ListsOfConfigurations(string device) : this()
        {
            this._deviceName = device;
        }

        private ListsOfConfigurations()
        {
            this.Version = 0;
            this.SignalsList = new SignalsList();
        }
        /// <summary>
        /// Версия устройства
        /// </summary>
        [XmlAttribute("version")]
        public double Version { get; set; }

        /// <summary>
        /// Подписи списка системного журнала
        /// </summary>
        [XmlElement("bd_list")]
        public SignalsList SignalsList { get; set; }

        /// <summary>
        /// Сериализует объект класса для сохранения в файл
        /// </summary>
        /// <param name="writer">XmlTextWriter, с помощью которого записываем</param>
        public void XmlToFile(XmlWriter writer)
        {
            writer.WriteStartDocument();
            writer.WriteStartElement(this._deviceName);
            writer.WriteAttributeString("version", this.Version.ToString(CultureInfo.InvariantCulture));

            writer.WriteStartElement("bd_list");
            this.SignalsList.XmlToFile(writer);
            writer.WriteEndElement();

            writer.WriteEndElement();
            writer.Flush();
        }
        /// <summary>
        /// Десериализация объекта из файла
        /// </summary>
        /// <param name="reader">XmlTextReader, с помощью которого читаем</param>
        public void XmlFromFile(XmlReader reader)
        {
            do
            {
                reader.Read();
            }
            while (reader.Name != this._deviceName || reader.EOF);
            if (reader.EOF) throw new FileNotFoundException("Данный файл подписей не является файлом подписей устройства " + this._deviceName);
            this.Version = Convert.ToDouble(reader.GetAttribute("version"), CultureInfo.InvariantCulture);
            reader.Read();//пробел
            reader.Read();//вход в тег signals_list
            this.SignalsList.XmlFromFile(reader);
        }
    }

    public class SignalsList
    {
        public SignalsList()
        {
            this.MessagesList = new List<string>();
            this.Len = 9;
            this.Count = 0x256; // дефолтное значение, может быть и 0x512
        }

        /// <summary>
        /// Длина одной подписи
        /// </summary>
        [XmlAttribute("lenStr")]
        public int Len { get; set; }
        /// <summary>
        /// Количество всех подписей
        /// </summary>
        [XmlAttribute("cntStr")]
        public int Count { get; set; }
        /// <summary>
        /// Список самих подписей для сериализации
        /// </summary>
        [XmlElement("n")]
        public List<string> MessagesList { get; set; }

        /// <summary>
        /// Сериализует объект класса для сохранения в файл
        /// </summary>
        /// <param name="writer">XmlTextWriter, с помощью которого записываем</param>
        public void XmlToFile(XmlWriter writer)
        {
            writer.WriteAttributeString("lenStr", this.Len.ToString());
            writer.WriteAttributeString("cntStr", this.Count.ToString());
            foreach (string message in this.MessagesList)
            {
                writer.WriteStartElement("n");
                writer.WriteCData(message);
                writer.WriteEndElement();
            }
        }
        /// <summary>
        /// Десериализация объекта из файла
        /// </summary>
        /// <param name="reader">XmlTextReader, с помощью которого читаем</param>
        public void XmlFromFile(XmlReader reader)
        {
            this.Len = Convert.ToInt32(reader.GetAttribute("lenStr"));
            this.Count = Convert.ToInt32(reader.GetAttribute("cntStr"));
            this.MessagesList.Clear();
            try
            {
                for (int i = 0; i < this.Count; i++)
                {
                    reader.Read();//пробел
                    reader.Read();//вход в тег
                    string cdatamessage = reader.ReadString();
                    this.MessagesList.Add(cdatamessage);
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
