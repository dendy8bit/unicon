using System;
using System.ComponentModel;
using System.Globalization;

namespace BEMN.Devices
{
    public class BooleanTypeConverter : BooleanConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context,
          CultureInfo culture,
          object value,
          Type destType)
        {
            return (bool)value ?
              "����" : "���";
        }

        public override object ConvertFrom(ITypeDescriptorContext context,
          CultureInfo culture,
          object value)
        {
            return (string)value == "����";
        }
    }
    public class RussianObjectConverter : TypeConverter
    {
        /// <summary>
        /// ������ � ������
        /// </summary>
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
        {
            return destType == typeof(string);
        }

        /// <summary>
        /// � ������ ���
        /// </summary>
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture,
           object value, Type destType)
        {
            return "< ������... >";
        }
    }
    public class RussianExpandableObjectConverter : ExpandableObjectConverter
    {
        /// <summary>
        /// ������ � ������
        /// </summary>
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
        {
            return destType == typeof(string);
        }

        /// <summary>
        /// � ������ ���
        /// </summary>
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture,
           object value, Type destType)
        {
            return "< ������... >";
        }
    }
}


