using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MBServer.Exceptions;
using BEMN.MBServer.Queries;

namespace BEMN.Devices
{
    #region [Delegates]

    public delegate void OnDeviceIndexEventHandler(int index);

    public delegate void OnDeviceEventHandler();

    /// <summary>
    /// ������� ����������� ���� ����������. 
    /// deviceType - ��������� ������������� ���� ����������
    /// deviceNumber - ����� ����������
    /// </summary>
    public delegate void DetermineHandler(object sender, string deviceType, byte deviceNumber);

    /// <summary>
    /// ������� �������� ����� ������� �� ������  ����������.
    /// </summary>
    public delegate void Handler(object sender);

    /// <summary>
    /// ������� �������� ����� ������� �� ���������� ����� ������.
    /// </summary>
    public delegate void StructHandler(object structObject, out object currentObject);

    /// <summary>
    /// ������� ������/������ ��������� ��� ������
    /// </summary>
    public delegate void SaveOrLoadProgramPicon();

    /// <summary>
    /// ������� ������ ������������ ��������
    /// </summary>
    /// <param name="data"></param>
    public delegate void LoadCounterOk(ushort[] data);

    /// <summary>
    /// ������� �������� ����� ������� � ������������� ������  ����������.
    /// </summary>
    public delegate void IndexHandler(object sender, int index);
    
    /// <summary>
    /// ������� ����� ������ ����������
    /// </summary>
    /// <param name="sender">����������</param>
    /// <param name="oldNumber">���������� �����</param>
    /// <param name="newNumber">����� �����</param>
    public delegate void DeviceNumberChangedHandler(object sender, byte oldNumber, byte newNumber);

    public delegate void PortNumberChangedHandler(object sender, PortNumberChangedEventArgs e);

    public delegate void CaptionPropertyChanged();

    //internal delegate void ActionSlotHandler(byte device, slot qslot, string qname, object deviceObj);
    #endregion [Delegates]

    #region [Class PortNumberChangedEventArgs]
    public class PortNumberChangedEventArgs : EventArgs
    {
        #region [Fields]
        private byte _oldNumber;
        private byte _newNumber;
        #endregion

        #region [Properties]
        public byte OldNumber
        {
            get { return this._oldNumber; }
            set { this._oldNumber = value; }
        }
        public byte NewNumber
        {
            get { return this._newNumber; }
            set { this._newNumber = value; }
        }
        #endregion [Properties]

        #region [Constructors]
                public PortNumberChangedEventArgs(byte oldValue, byte newValue)
                {
                    this._oldNumber = oldValue;
                    this._newNumber = newValue;
                }
        #endregion [Constructors]
    }
    #endregion [Class PortNumberChangedEventArgs]
    
    #region[Class Device]
    /// <summary>
    /// ������� ����� ��� ���� ���������. ������������� �����������
    /// ����� �� ��������� Modbus.
    /// </summary>
    public class Device : INodeSerializable, IDisposable, IDeviceInfo
    {
        #region [Events]
        public event DeviceNumberChangedHandler DeviceNumberChanged;
        public event PortNumberChangedHandler PortNumberChanged;
        public event Action ConnectionModeChanged;
        public event Handler _versionOk;
        public event Handler _versionFail;

        public event Handler _versionTaskOk;
        public event Handler _versionTaskFail;

        public event Action<Device> DeviceConnectStateChanged;
        #endregion [Events]

        #region [Fields]
        private string _caption = string.Empty;
        DeviceDlgInfo _dlgInfo;
        private byte _deviceNumber;
        DeviceInfo _info = new DeviceInfo();
        protected Modbus mb;
        public slot _infoSlot = new slot(0x500, 0x500 + 0x10);
        private bool _isConnect;
        private static bool _autoLoadConfig = true;

        #endregion [Fields]

        #region [Properties]
        public DeviceDlgInfo DeviceDlgInfo
        {
            get { return this._dlgInfo; }
            set
            {
                if (this._dlgInfo != null && this._dlgInfo.IsConnectionMode != value.IsConnectionMode)
                {
                    this._dlgInfo = value;
                    if (this.ConnectionModeChanged != null && this.MB.Port != null)
                        this.ConnectionModeChanged();
                }
                else
                {
                    this._dlgInfo = value;
                }
            }
        }
        public string Caption
        {
            get { return this._caption; }
            set { this._caption = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        [Description("����� ���������� �� Modbus"), Category("�����")]
        [DisplayName("�����")]
        public virtual byte DeviceNumber
        {
            get { return this._deviceNumber; }
            set
            {
                byte oldValue = this._deviceNumber;
                this._deviceNumber = value;
                this.DeviceNumberChanged?.Invoke(this, oldValue, this._deviceNumber);
            }
        }

        [Browsable(false)]
        public virtual DeviceInfo Info
        {
            get { return this._info; }
            set { this._info = value; }
        }

        [XmlIgnore]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [DisplayName("��������")]
        [Description("��������� ��������� ��-����")]
        [Category("�����")]
        public virtual Modbus MB
        {
            get { return this.mb; }
            set
            {
                try
                {
                    this.mb = value;
                }
                catch (InvalidPortException e)
                {
                    throw e;
                }
                if (this.mb != null)
                {
                    this.mb.CompleteExchange += this.mb_CompleteExchange;
                }
            }
        }

        [Description("����� �����"), Category("�����")]
        [DisplayName("����")]
        public byte PortNum
        {
            get { return this.MB.Port.PortNum; }
            set
            {
                byte oldNumber = this.MB.Port.PortNum;
                if (!this.MB.IsPortInvalid)
                {
                    this.MB.Port.Close();
                }
                this.MB.Port = Modbus.SerialServer.GetNamedPort(value, (uint)Modbus.ProcessID);
                byte newNumber = this.MB.Port.PortNum;
                PortNumberChangedEventArgs args = new PortNumberChangedEventArgs(oldNumber, newNumber);
                this.PortNumberChanged?.Invoke(this, args);
            }
        }
        public TimeSpan DefaultTimeSpan { get; set; } = new TimeSpan(0, 0, 0, 0, 0);

        [Browsable(false)]
        public bool IsActive => this.mb.IsActive;

        [XmlIgnore]
        [Browsable(false)]
        public bool HaveVersion { get; set; }

        public bool IsConnect => this._isConnect;

        public static bool AutoloadConfig => _autoLoadConfig;

        #endregion [Properties]

        #region [Constructors]
        public Device(){}
        public Device(Modbus mb)
        {
            this.MB = mb;
            this._deviceNumber = 0;
        }
        #endregion [Constructors]

        #region [Methods]
        public static bool IsDeviceConnectedToPort(Device device)
        {
            return null != device &&
                   null != device.MB &&
                   null != device.MB.Port &&
                   device.MB.Port.Opened;
        }
        public void SetStartConnectionState(bool connectionMode)
        {
            this._isConnect = connectionMode;
        }
        public static void SetAutiloadSetpoint(bool value)
        {
            _autoLoadConfig = value;
        }

        public virtual void LoadVersion(object deviceObj)
        {
            System.Threading.Thread.Sleep(500);
            this.LoadSlot(this.DeviceNumber, this._infoSlot, "version" + this.DeviceNumber, deviceObj);
        }

        public virtual void LoadVersionTask(object deviceObj)
        {
            Modbus.VersionReadSuccess = false;
            System.Threading.Thread.Sleep(500);
            this.LoadSlot(this.DeviceNumber, this._infoSlot, "version on task" + this.DeviceNumber, deviceObj);
        }

        public void MakeQueryQuick(string qname)
        {
            Query q = this.MB.GetQuery(qname);
            if (null != q)
            {
                q.raiseSpan = new TimeSpan(1000);
            }
        }
        public void MakeQuerySpan(string qname, TimeSpan ff)
        {
            Query q = this.MB.GetQuery(qname);
            if (null != q)
            {
                q.raiseSpan = ff;
            }
        }
        public void MakeQuerySlow(string qname)
        {
            Query q = this.MB.GetQuery(qname);
            if (null != q)
            {
                q.raiseSpan = new TimeSpan(0, 0, 2);
            }
        }
        public void MakeQuerySlow(string qname, int seconds)
        {
            Query q = this.MB.GetQuery(qname);
            if (null != q)
            {
                q.raiseSpan = new TimeSpan(0, 0, seconds);
            }
        }

        private void RaiseDeviceConnectStateChanged()
        {
            this.DeviceConnectStateChanged?.Invoke(this);
        }

        protected internal virtual void mb_CompleteExchange(object sender, Query query)
        {
            // ���� ���� ��������� ������
            if (query.fail != 0)
            {
                // ���� ����� "�����������"
                if (this._isConnect)
                {
                    // ����� "�������"
                    this._isConnect = false;
                    this.RaiseDeviceConnectStateChanged();
                }
            }
            else
            {
                if (!this._isConnect)
                {
                    this._isConnect = true;
                    this.RaiseDeviceConnectStateChanged();
                }
            }

            if (query.name == "version on task" + this.DeviceNumber)
            {
                Modbus.VersionReadSuccess = this._isConnect = !query.error;
                this._infoSlot.Loaded = 0 == query.fail;
                this.RaiseReverse(query, this._versionTaskOk, this._versionTaskFail, ref this._infoSlot);
            }

            if (query.name == "version" + this.DeviceNumber)
            {
                this._isConnect = !query.error;
                this._infoSlot.Loaded = 0 == query.fail;
                this.RaiseReverse(query, this._versionOk, this._versionFail, ref this._infoSlot);
            }
        }

        public void LoadVersionOk()
        {
            this._versionOk?.Invoke(this);
        }
        public void InitVersion()
        {
            try
            {
                this._info = DeviceInfo.GetVersion(this._infoSlot.Value);
                IDeviceVersion version = this as IDeviceVersion;
                if (this.HaveVersion && version != null)
                {
                    int count = version.Versions.Count - 1;
                    string[] separator = { "-" };
                    List<string> newVersion = new List<string>();
                    string[] str = version.Versions[count].Split(separator, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string s in str)
                    {
                        string res = s.Where(simbol => !char.IsLetter(simbol)).Aggregate("", (current, simbol) => current + simbol);
                        newVersion.Add(res);
                    }
                    count = newVersion.Count - 1;

                    double y = Convert.ToDouble(newVersion[count], CultureInfo.InvariantCulture);
                    string ver = this._info.Version.Where(s => !char.IsLetter(s)).Aggregate("", (current, s) => current + s);
                    double x = Convert.ToDouble(ver, CultureInfo.InvariantCulture);
                    if (x > y)
                    {
                        MessageBox.Show("������ ������ ������� ����� �������� ����������� � ������������ �����������,����� �������� ������",
                            "��������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch (Exception)
            {

            }
        }
        public void LoadSlot(byte device, slot qslot, string qname, object deviceObj)
        {
            this.mb.AddQuery(this.CreateQuery(device, qslot, qname, 0x04, 10, this.DefaultTimeSpan, false, deviceObj));
        }
        public void LoadSlot(byte device, slot qslot, string qname, object deviceObj, TimeSpan span)
        {
            this.mb.AddQuery(this.CreateQuery(device, qslot, qname, 0x04, 10, span, false, deviceObj));
        }
        public void LoadBitSlot(byte device, slot qslot, string qname, object deviceObj)
        {
            this.mb.AddQuery(this.CreateQuery(device, qslot, qname, 0x01, 10, this.DefaultTimeSpan, false, deviceObj));
        }
        protected int GetIndex(string qname, string pattern)
        {
            int ret = -1;
            Match m = Regex.Match(qname, pattern + "(?<index>\\d+)_\\d+");
            if (m.Success)
            {
                ret = Convert.ToInt32(m.Result("${index}"));
            }
            return ret;
        }
        protected bool IsIndexQuery(string qname, string pattern)
        {
            return Regex.IsMatch(qname, pattern + "(?<index>\\d+)_\\d+");
        }
        protected void OnLoadIndexQuery(Query query, slot[] indexSlots, int cnt, string qname, IndexHandler indexOk,
            IndexHandler indexFail)
        {
            int index = this.GetIndex(query.name, qname);
            if (0 != query.fail && null != indexFail)
            {
                indexFail(this, index);
            }
            else
            {
                indexSlots[index].Value = Common.TOWORDS(query.readBuffer, true);
                indexOk?.Invoke(this, index);
            }
        }
        protected void OnSaveIndexQuery(Query query, string qname, IndexHandler indexOk, IndexHandler indexFail)
        {
            int index = this.GetIndex(query.name, qname);
            if (0 != query.fail && null != indexFail)
            {
                indexFail(this, index);
            }
            else
            {
                indexOk?.Invoke(this, index);
            }
        }
        public void LoadBitSlotCycle(byte device, slot qslot, string qname, TimeSpan span, ushort priority,
            object deviceObj)
        {
            Query query = this.CreateBitsQuery(device, qslot, qname, 0x01, priority, span, true, deviceObj);
            query.raiseSpan = span;
            query.priority = priority;
            this.mb.AddQuery(query);
        }

        public void LoadBitsCycle(byte device, slot qslot, string qname, TimeSpan span, ushort priority,
            object deviceObj)
        {
            Query query = this.CreateBitsQuery(device, qslot, qname, 0x01, priority, span, true, deviceObj);
            query.raiseSpan = span;
            query.priority = priority;
            this.mb.AddQuery(query);
        }

        public void SetBit(byte device, ushort dataStart, bool bit, string qname, object deviceObj, TimeSpan span = new TimeSpan())
        {
            Query query = new Query();
            query.name = qname;
            query.device = device;
            query.deviceObj = deviceObj;
            query.func = 0x05;
            query.priority = 10;
            query.dataStart = dataStart;
            query.raiseSpan = span;
            query.writeBuffer = bit ? new ushort[] { 0xFF } : new ushort[] { 0 };
            this.mb.AddQuery(query);
        }

        protected Query CreateQuery(byte device, slot qslot, string qname, byte func, ushort priority,
            TimeSpan timeSpan, bool isCycle, object deviceObj)
        {
            bool exist = false;
            Query query = new Query();
            query.name = qname;
            query.device = device;
            query.deviceObj = deviceObj;
            query.func = func;
            query.priority = priority;
            query.dataStart = qslot.Start;
            query.raiseSpan = timeSpan;
            query.bCycle = isCycle;
            qslot.Loaded = false;
            query.writeBuffer = qslot.Value;
            query.globalSize = qslot.Size;
            return query;
        }

        protected Query CreateBitsQuery(byte device, slot qslot, string qname, byte func, ushort priority,
            TimeSpan timeSpan, bool isCycle, object deviceObj)
        {
            bool exist = false;
            Query query = new Query();
            query.name = qname;
            query.device = device;
            query.deviceObj = deviceObj;
            query.func = func;
            query.priority = priority;
            query.raiseSpan = timeSpan;
            query.bCycle = isCycle;
            query.dataStart = qslot.Start;
            query.isReadBits = true;
            query.writeBuffer = qslot.Value;
            query.globalSize = qslot.Size;
            qslot.Loaded = false;
            return query;
        }

        public void SaveSlot(byte device, slot qslot, string qname, object deviceObj, TimeSpan span = new TimeSpan())
        {
            bool res = span == this.DefaultTimeSpan || span == TimeSpan.Zero;
            this.mb.AddQuery(this.CreateQuery(device, qslot, qname, 0x10, 10, res ? this.DefaultTimeSpan : span, false, deviceObj));
        }

        public void SaveSlot6(byte device, slot qslot, string qname, object deviceObj)
        {
            this.mb.AddQuery(this.CreateQuery(device, qslot, qname, 0x06, 10, this.DefaultTimeSpan, false, deviceObj));
        }

        public void SaveSlot6TimeSpan(byte device, slot qslot, string qname, object deviceObj, TimeSpan span = new TimeSpan())
        {
            bool res = span == this.DefaultTimeSpan || span == TimeSpan.Zero;
            this.mb.AddQuery(this.CreateQuery(device, qslot, qname, 0x06, 10, res ? this.DefaultTimeSpan : span, false, deviceObj));
        }

        //��� ������
        public void SaveSlot5(byte device, slot qslot, string qname, object deviceObj)
        {
            this.mb.AddQuery(this.CreateQuery(device, qslot, qname, 0x05, 10, this.DefaultTimeSpan, false, deviceObj));
        }

        public void SaveSlot5TimeSpan(byte device, slot qslot, string qname, object deviceObj, TimeSpan span = new TimeSpan())
        {
            bool res = span == this.DefaultTimeSpan || span == TimeSpan.Zero;
            this.mb.AddQuery(this.CreateQuery(device, qslot, qname, 0x05, 10, res ? this.DefaultTimeSpan : span, false, deviceObj));
        }

        public void LoadSlotCycle(byte device, slot qslot, string qname, TimeSpan span, ushort priority,
            object deviceObj)
        {
            Query query = this.CreateQuery(device, qslot, qname, 0x04, priority, span, true, deviceObj);
            this.mb.AddQuery(query);
        }

        protected void Raise(Query query, Handler evOk, Handler evFail)
        {
            if (0 != query.fail)
            {
                evFail?.Invoke(this);
            }
            else
            {
                if (null != evOk && query.fail == 0)
                {
                    evOk(this);
                }
            }
        }
        public void Raise(Query query, Handler evOk, Handler evFail, ref slot loadSlot)
        {
            if (0 != query.fail) // ���� ���������� ��������� ������� > 0
            {
                evFail?.Invoke(this); // ���������� ������� Fail
            }
            else
            {
                if (0 != query.fail) return;
                if (null != loadSlot) // � ���� �� ��� ��������
                {
                    loadSlot.Loaded = true; // ���� ��������
                    loadSlot.Value = Common.TOWORDS(query.readBuffer, true);
                    evOk?.Invoke(this);
                }
                else
                {
                    evOk?.Invoke(this);
                }
            }
        }
        protected void Raise(Query query, Handler evOk, Handler evFail, ref slot loadSlot, bool error)
        {
            if (0 != query.fail) // ���� ���������� ��������� ������� != 0, �.�. > 0
            {
                evFail?.Invoke(this); // ���� ���������� ��������� ������ device - �������� ������� Fail
            }
            else
            {
                if (0 != query.fail) return; // ���� ���������� ��������� ������� != 0, �.�. > 0 - ���������
                if (null != loadSlot)
                {
                    loadSlot.Loaded = true;
                    loadSlot.Value = Common.TOWORDS(query.readBuffer, true);
                    if (!error)
                    {
                        evOk?.Invoke(this);
                    }
                    else
                    {
                        evFail?.Invoke(this);
                    }
                }
                else
                {
                    evOk?.Invoke(this);
                }
            }
        }
        protected void RaiseReverse(Query query, Handler evOk, Handler evFail, ref slot loadSlot)
        {
            if (0 != query.fail)
            {
                evFail?.Invoke(this);
            }
            else
            {
                if (0 != query.fail) return;
                if (null != loadSlot)
                {
                    loadSlot.Loaded = true;
                    loadSlot.Value = Common.TOWORDS(query.readBuffer, false);
                }
                evOk?.Invoke(this);
            }
        }
        #endregion [Methods]

        #region [INodeSerializable]
        public virtual XmlElement ToXml(XmlDocument doc)
        {
            XmlElement ret = doc.CreateElement("Device");
            if (null != this.mb)
            {
                XmlElement mbEl = this.mb.ToXml(doc);
                ret.AppendChild(mbEl);
            }
            //
            XmlElement numEl = doc.CreateElement("Number");
            numEl.InnerText = this.DeviceNumber.ToString();
            ret.AppendChild(numEl);
            //
            XmlAttribute type = doc.CreateAttribute("Type");
            type.InnerText = GetType().ToString();
            ret.Attributes.Append(type);
            //
            XmlElement plant = doc.CreateElement("Plant");
            plant.InnerText = this.DevicePlant;
            ret.AppendChild(plant);
            //
            XmlElement vers = doc.CreateElement("DeviceVers");
            vers.InnerText = this.DeviceVersion;
            ret.AppendChild(vers);
            //
            XmlElement caption = doc.CreateElement("Caption");
            caption.InnerText = this.Caption;
            ret.AppendChild(caption);
            //
            XmlElement info = this.DeviceDlgInfo.ToXml(doc);
            ret.AppendChild(info);

            return ret;
        }

        public virtual void FromXml(XmlElement element)
        {
            bool tcpError = false;
            foreach (XmlElement cel in element.ChildNodes)
            {
                switch (cel.Name)
                {
                    case "Caption":
                        {
                            this.Caption = cel.InnerText;
                            break;
                        }
                    case "Number":
                        {
                            this.DeviceNumber = Convert.ToByte(cel.InnerText);
                            break;
                        }
                    case "Modbus":
                        {
                            Modbus modbus = new Modbus();
                            try
                            {
                                modbus.FromXml(cel);
                            }

                            catch (InvalidPortException e)
                            {
                                throw e;
                            }
                            catch (SocketException)
                            {
                                tcpError = true;
                            }
                            finally
                            {
                                this.MB = modbus;
                                this.MB.CompleteExchange -= this.mb_CompleteExchange;// ��� ������������� ������� ������������� � �������� ����� ��� ���� ������� �� ��� �������
                                                                                     // ������� ����� ��� ����������
                            }
                            break;
                        }
                    case "DeviceVers":
                        {
                            this.DeviceVersion = cel.InnerText;
                            break;
                        }
                    case "DeviceDlgInfo":
                        {
                            if (this.DeviceDlgInfo == null)
                                this.DeviceDlgInfo = new DeviceDlgInfo();
                            this.DeviceDlgInfo.FromXml(cel);
                            break;
                        }
                    case "Plant":
                        this.DevicePlant = cel.InnerText;
                        break;
                    default:
                        break;
                }
            }
            if (tcpError)
            {
                MessageBox.Show(string.Format("��� ���������� {0} �� ������� ������������ �����������", this.DeviceNumber));
            }
        }
        #endregion [INodeSerializable]

        #region [IDisposable]
        public void Dispose()
        {
            this.MB.Dispose();
        }
        #endregion [IDisposable]
       
        #region [IDeviceInfo]
        [Description("���������o"), Category("��������")]
        [DisplayName("����������")]
        public virtual string DeviceType
        {
            get { return this._info.Type; }
            set { this._info.Type = value; }
        }


        [Description("������ ����������"), Category("��������")]
        [DisplayName("������")]
        public virtual string DeviceVersion
        {
            get { return this._info.Version; }
            set { this._info.Version = value; }
        }

        [Description("������ ��������"), Category("��������")]
        [DisplayName("��������")]
        public virtual string DevicePlant
        {
            get { return this._info.Plant; }
            set { this._info.Plant = value; }
        }

        #endregion [IDeviceInfo]

        #region [Class slot in Class Device]
        /// <summary>
        /// ����� - ���� ������ ��� ����������. 
        /// </summary>
        public class slot
        {
            #region [Fields] 

            private ushort _start;
            private ushort _end;
            private ushort _size;

            #endregion [Fields]

            #region [Properties]

            /// <summary>
            /// ��������� ����� �����
            /// </summary>
            public ushort Start => this._start;

            /// <summary>
            /// �������� ����� �����
            /// </summary>
            public ushort End => this._end;

            /// <summary>
            /// ������ ������ �����
            /// </summary>
            public ushort Size => this._size;

            /// <summary>
            /// �������� �������� � �����
            /// </summary>
            public ushort[] Value { get; set; }

            /// <summary>
            /// ����������, ����������� ��� ��� ,���. �� ���������� � ����
            /// </summary>
            public bool Loaded { get; set; } = false;

            #endregion [Properties]

            #region [Constructors]
            /// <summary>
            /// ������� ����, �������� ������.
            /// </summary>
            /// <param name="start"> ��������� ����� ������  </param>
            /// <param name="end"> �������� ����� ������ </param>
            public slot(ushort start, ushort end)
            {
                ApplicationException e = new ApplicationException("��������� ����� ������ ���� ������ ���������");
                if (end <= start)
                {
                    throw e;
                }

                this._start = start;
                this._end = end;
                this._size = (ushort)(end - start);
                this.Value = new ushort[this._size];
            }
            #endregion [Constructors]            

            #region [Methods]
            /// <summary>
            /// ��������� ����
            /// </summary>
            /// <param name="newSlot">���� - ����� </param>
            public void Copy(slot newSlot)
            {
                newSlot = MemberwiseClone() as slot;
            }

            /// <summary>
            /// ������� ������ �����.
            /// </summary>
            /// <param name="qslots"> ������������������� �����</param>
            /// <param name="start">��������� ����� ������</param>
            /// <param name="blockSize"></param>
            /// <param name="count">���-�� ������</param>
            public static void InitSlots(slot[] qslots, ushort start, ushort blockSize, int count)
            {
                for (int i = 0; i < count; i++)
                {
                    qslots[i] = new slot(start, start += blockSize);
                }
            }
            #endregion [Methods]
        }
        #endregion [Class slot in Class Device]

        #region [Class SlotArray in class Device]
        public class SlotArray
        {
            private slot[] _slots;
            public slot this[int index]
            {
                get { return _slots[index]; }
                set { this._slots[index] = value; }
            }
            
            public SlotArray(slot[] slots)
            {
                this.Size = slots.Length;
            }

            public int Size { get; set; }

            public slot GetSlot(int index)
            {
                return this[index];
            }
        }
        #endregion [Class SlotArray in class Device]
    }
    #endregion [Class Device]      
}