﻿namespace BEMN.Devices.StructHelperClasses.Interfaces
{
    public interface IStructInit
    {
        void InitStruct(byte[] array); // проинициализировать структуру, byte[] array - структура в виде массива байт
        ushort[] GetValues(); // получить массив слов структуры
    }
}