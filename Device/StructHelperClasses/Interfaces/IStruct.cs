﻿namespace BEMN.Devices.StructHelperClasses.Interfaces
{
    public interface IStruct
    {
        StructInfo GetStructInfo(int slotLen);
        object GetSlots(ushort start, bool slotArray, int slotLen);
    }
}