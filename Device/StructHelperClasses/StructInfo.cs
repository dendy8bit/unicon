﻿namespace BEMN.Devices.StructHelperClasses
{
    public struct StructInfo
    {
        public ushort FullSize; // размер структуры с словах
        public bool SlotsArray; // false - структура <= 64 слова, true - структура > 64 слова
    }
}