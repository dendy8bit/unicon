﻿using System;
using System.Runtime.InteropServices;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.Devices.Structures;
using BEMN.MBServer;

namespace BEMN.Devices.StructHelperClasses
{
    public static class StructHelper
    {
        public static StructInfo GetStructInfo(Type structType, int slotLen = StructBase.MAX_SLOT_LENGHT_DEFAULT)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort) (Marshal.SizeOf(structType)/2); // размер структуры в словах
            sInfo.SlotsArray = sInfo.FullSize > slotLen; // если структура меньше 64 слов, используется 1 слот, если больше массив слотов
            return sInfo; // возвращает объект с информацией о размере структуры в словах и информацией о использовании массива слотов или одного слота
        }

        public static object GetSlots(ushort start, bool slotArray, Type structType, int slotLenght = StructBase.MAX_SLOT_LENGHT_DEFAULT)
        {
            if (slotArray)
            {
                int arrayLength = Marshal.SizeOf(structType)/2/slotLenght; // количество слотов по 64 слова (по умолчанию 64 слова)
                int lastSlotLength = Marshal.SizeOf(structType)/2%slotLenght; // длина последнего слота (чаще не 64 слова)
                Device.slot[] slots;
                ushort startAddr = start; // как начальный индекс, увеличивается на размер слота
                if (lastSlotLength != 0) 
                {
                    arrayLength++; 
                    slots = new Device.slot[arrayLength];

                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort) (startAddr + slotLenght)); 
                        startAddr += (ushort) slotLenght;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort) (startAddr + lastSlotLength));
                }
                else
                {
                    slots = new Device.slot[arrayLength];
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort) (startAddr + slotLenght));
                        startAddr += (ushort) slotLenght;
                    }
                }
                return slots;
            }
            else
            {
                return new Device.slot(start, (ushort) (start + Marshal.SizeOf(structType)/2));
            }
        }

        public static int GetInt(byte[] array, ref int index)
        {
            ushort first = GetUshort(array, ref index);
            ushort second = GetUshort(array, ref index);
            return  Common.UshortUshortToInt(second,first);
        }

        public static ushort[] IntToUshorts(int value)
        {
            ushort highUshort = (ushort)(value >> 16);
            ushort lowUshort = (ushort)(value & 0xFFFF);
            return new[] {lowUshort, highUshort};
        }

        public static void GetIntArray(byte[] array, int[] result, ref int index)
        {
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = GetInt(array, ref index);
            }
        }

        public static void GetUintArray(byte[] array, uint[] result, ref int index)
        {
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = (uint)GetInt(array, ref index);
            }
        }

        public static ushort GetUshort(byte[] array, ref int index)
        {
            ushort result = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof (ushort);
            return result;
        }
        public static void GetUshortArray(byte[] array, ushort[] result, ref int index)
        {
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = GetUshort(array, ref index);
            }
        }

        public static void GetShortArray(byte[] array, short[] result, ref int index)
        {
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = (short)GetUshort(array, ref index);
            }
        }

        /// <summary>
        /// Получить структуру с проинициализированными значениями
        /// </summary>
        public static T GetOneStruct<T>(byte[] array, ref int index, T obj, int slotLen = StructBase.MAX_SLOT_LENGHT_DEFAULT) where T: IStructInit, IStruct, new()
        {
            T result =(T)Activator.CreateInstance(obj.GetType()); // создать экземпляр структуры
            int size = result.GetStructInfo(slotLen).FullSize * 2; // размер структуры в байтах
            byte[] oneStruct = new byte[size];
            Array.ConstrainedCopy(array, index, oneStruct, 0, size);
            index += size;
            
            result.InitStruct(oneStruct);
            return result;
        }
        
        //TODO сделать все стурктуры классами и удалить этот метод
        public static void GetArrayStruct<T>(byte[] array, T[] destination, ref int index)
            where T : IStructInit, IStruct, new()
        {
            for (int i = 0; i < destination.Length; i++)
            {
                destination[i] = GetOneStruct(array, ref index, destination[i]);
            }
        }

        public static void GetArrayStruct<T>(byte[] array, T[] destination, ref int index, int slotLen)
            where T : StructBase
        {
            for (int i = 0; i < destination.Length; i++)
            {
                destination[i] = StructBase.GetOneStruct(array, ref index, destination[i], slotLen);
            }
        }
    }
}