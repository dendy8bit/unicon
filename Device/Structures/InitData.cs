﻿using System.Collections.Generic;
using System.Reflection;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Devices.Structures
{
    public class InitData
    {
        private readonly SortedDictionary<LayoutAttribute, FieldInfo> _fields;
        private List<int> _ignored;
        private readonly int _size = -1;

        public InitData(int size, SortedDictionary<LayoutAttribute, FieldInfo> fields,
            SortedDictionary<int, PropertyInfo> properties, List<int> ignored)
        {
            this._size = size;
            this._fields = fields;
            this.Properties = properties;
            this._ignored = ignored;
        }

        public int Size
        {
            get { return this._size; }
        }

        public SortedDictionary<LayoutAttribute, FieldInfo> Fields
        {
            get { return this._fields; }
        }
        
        public SortedDictionary<int, PropertyInfo> Properties { get; private set; }

        public List<int> Ignored
        {
            get { return this._ignored; }
        }
    }
}
