﻿using BEMN.Devices.Structures.Attributes;

namespace BEMN.Devices.Structures
{
    /// <summary>
    /// Страница осциллографа
    /// </summary>
    public class OscPage : StructBase
    {
        #region [Private fields]
        
        private const int BIG_SLOT_LENGHT = 125;
        [Layout(0, Count = 1024)] private ushort[] _words;

        #endregion [Private fields]
        

        #region [Properties]
        /// <summary>
        /// Страница осцилограммы в виде массива слов
        /// </summary>
        public ushort[] Words
        {
            get { return this._words; }
        }
        #endregion [Properties]

        public override object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return base.GetSlots(start, slotArray, slotLen < BIG_SLOT_LENGHT ? BIG_SLOT_LENGHT : slotLen);
        }
    }
}
