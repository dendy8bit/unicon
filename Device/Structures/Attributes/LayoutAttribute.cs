﻿using System;

namespace BEMN.Devices.Structures.Attributes
{
    public class LayoutAttribute : Attribute
    {
        private int _position;
        public int Count { get; set; }
        public bool Ignore { get; set; }

        public LayoutAttribute(int position)
        {
            this._position = position;
        }

        public int Position
        {
            get { return this._position; }
        }
    }
}

