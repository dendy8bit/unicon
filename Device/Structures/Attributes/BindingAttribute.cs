﻿using System;

namespace BEMN.Devices.Structures.Attributes
{
    public class BindingPropertyAttribute: Attribute
    {
        private readonly int _position;

        public BindingPropertyAttribute(int position)
        {
            this._position = position;
        }

        public int Position
        {
            get { return this._position; }
        }
    }
}
