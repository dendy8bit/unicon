﻿using System;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.Devices.Structures
{
    //TODO переделать на класс
    public struct SomeStruct : IStruct, IStructInit
    {
        private ushort[] _values;

        public SomeStruct(int i)
        {
            this._values = new ushort[i];
        }

        public SomeStruct(byte[] array)
        {
            this._values = new ushort[] {};
            this.InitStruct(array);
        }

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            StructInfo sInfo = new StructInfo();
            this._values = this._values ?? new ushort[1];
            sInfo.FullSize = (ushort)(this._values.Length);
            sInfo.SlotsArray = sInfo.FullSize > slotLen;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            if (!slotArray)
            {
                return new Device.slot(start, (ushort)(start + this._values.Length));
            }
            else
            {
                int arrayLength = this._values.Length / slotLength;
                int lastSlotLength = this._values.Length % slotLength;
                Device.slot[] slots;
                ushort startAddr = start;
                if (lastSlotLength != 0)
                {
                    arrayLength++;
                    slots = new Device.slot[arrayLength];

                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                else
                {
                    slots = new Device.slot[arrayLength];
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                return slots;
            }
        }
        #endregion [IStruct Members]

        #region [IStructInit]

        public void InitStruct(byte[] array)
        {
            if (array.Length%2 != 0)
            {
                byte[] buffer = array;
                array = new byte[array.Length+1];
                Array.ConstrainedCopy(buffer,0,array,0,buffer.Length);
            }
            this._values = new ushort[array.Length/2];
            int ind = 0;
            for (int i = 0; i < this._values.Length; i++)
            {
                this._values[i] = Common.TOWORD(array[ind+1], array[ind]);
                ind += sizeof (ushort);
            }
        }

        public ushort[] GetValues()
        {
            return this._values;
        }
        #endregion

        public ushort[] Values
        {
            get { return this._values; }
            set { this._values = value; }
        }
    }
}
