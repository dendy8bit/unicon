﻿using System;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Devices.Structures
{
    public class DateTimeStructOld : StructBase
    {
        private const string TIME_PATTERN = "{0:d2}:{1:d2}:{2:d2}";
        private const string DATE_PATTERN = "{0:d2}.{1:d2}.{2:d2}";

        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;

        public string Date
        {
            get { return string.Format(DATE_PATTERN, this._date, this._month, this._year); }
        }

        public string Time
        {
            get { return string.Format(TIME_PATTERN, this._hour, this._minute, this._second); }
        }

        public DateTime SetDateTimeNow()
        {
            DateTime dateTime = DateTime.Now;
            this._date = Convert.ToUInt16(dateTime.Day);
            this._month = Convert.ToUInt16(dateTime.Month);
            this._year = Convert.ToUInt16(dateTime.Year - 2000);
            this._hour = Convert.ToUInt16(dateTime.Hour);
            this._minute = Convert.ToUInt16(dateTime.Minute);
            this._second = Convert.ToUInt16(dateTime.Second);
            return dateTime;
        }
        public DateTime SetDateTimeNow(DateTime dateTime)
        {
            this._date = Convert.ToUInt16(dateTime.Day);
            this._month = Convert.ToUInt16(dateTime.Month);
            this._year = Convert.ToUInt16(dateTime.Year - 2000);
            this._hour = Convert.ToUInt16(dateTime.Hour);
            this._minute = Convert.ToUInt16(dateTime.Minute);
            this._second = Convert.ToUInt16(dateTime.Second);
            return dateTime;
        }
    }
}
