﻿using BEMN.Devices.Structures.Attributes;

namespace BEMN.Devices.Structures
{
    /// <summary>
    /// Структура для чтения/ записа одного слова
    /// </summary>
    public class OneWordStruct : StructBase// IStruct, IStructInit
    {   
        [Layout(0)] private ushort _word;

        public OneWordStruct()
        {
            
        }

        public OneWordStruct(ushort word)
        {
            this._word = word;
        }
        
        [BindingProperty(0)]
        public ushort Word
        {
            get { return this._word; }
            set { this._word = value; }
        }
    }
}
