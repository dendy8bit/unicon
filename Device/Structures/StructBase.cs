﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.Xml.Serialization;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.Devices.Structures
{
    /// <summary>
    /// Базовый класс для структур
    /// </summary>
    [Serializable]
    public class StructBase : IStruct, IStructInit
    {
        #region [Constants]
        public const int MAX_SLOT_LENGHT_DEFAULT = 64;
        #endregion [Constants]


        #region [Static Members]
        /// <summary>
        /// Коллекция проинициализированных типов
        /// </summary>
        protected static Dictionary<Type, InitData> InitData = new Dictionary<Type, InitData>();

        public static T GetOneStruct<T>(byte[] array, ref int index, T obj, int slotLen) where T : StructBase
        {
            T result = (T)Activator.CreateInstance(obj.GetType());
            int size = result.GetStructInfo(slotLen).FullSize * 2 - result._ignored.Count;
            byte[] oneStruct = new byte[size];
            Array.ConstrainedCopy(array, index, oneStruct, 0, size);
            index += size;
            result.InitStruct(oneStruct);
            return result;
        }

        #endregion [Static Members]


        #region [Private fields]
        /// <summary>
        /// Коллекция полей структуры
        /// </summary>
        private SortedDictionary<LayoutAttribute, FieldInfo> _fields;
        /// <summary>
        /// Размер структуры в байстах
        /// </summary>
        private int _sizeInBytes = -1;
        /// <summary>
        /// Пропускаемые адреса
        /// </summary>
        private List<int> _ignored;
        /// <summary>
        /// Максимальный размер слота
        /// </summary>
        private int _maxSlotLength;
        #endregion [Private fields]


        #region [Ctor's]

        public StructBase() : this(MAX_SLOT_LENGHT_DEFAULT)
        {
        }

        public StructBase(int slotLen)
        {
            this._maxSlotLength = slotLen;
            int size = this.GetStructInfo(this._maxSlotLength).FullSize * 2;
            this._ignored = InitData[GetType()].Ignored;
            this.InitStruct(new byte[size]);
        }
        #endregion [Ctor's]


        #region [Properties]
        /// <summary>
        /// Коллекция свойств структуры
        /// </summary>
        [XmlIgnore]
        public SortedDictionary<int, PropertyInfo> Properties { get; private set; }
        [XmlIgnore]
        public List<int> IgnoredAddresses => this._ignored;

        #endregion [Properties]


        #region [Public members]
        /// <summary>
        /// Создаёт клон структуры
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual T Clone<T>() where T : StructBase
        {
            StructBase result = Activator.CreateInstance<T>();
            byte[] values = Common.TOBYTES(this.GetValues(), false);
            result.InitStruct(values);
            return (T)result;
        } 
        #endregion [Public members]

        
        #region [Help members]
        /// <summary>
        /// Создаёт слоты из диапазона адресов
        /// </summary>
        /// <param name="pair">Диапазон</param>
        /// <param name="slotLenght">Размер одного слота</param>
        /// <returns>Перечесление слотов</returns>
        private IEnumerable<Device.slot> CreateSlotsAdd(AddressPair pair, int slotLenght)
        {
            int arrayLength = pair.Size / slotLenght;
            int lastSlotLength = pair.Size % slotLenght;
            List<Device.slot> slots = new List<Device.slot>();
            ushort start = pair.Start;
            for (int i = 0; i < arrayLength; i++)
            {
                slots.Add(new Device.slot(start, (ushort)(start + slotLenght)));
                start += (ushort)slotLenght;
            }
            if (lastSlotLength != 0)
            {
                slots.Add(new Device.slot(start, (ushort)(start + lastSlotLength)));
            }

            return slots.ToArray();
        }

        /// <summary>
        /// Размер структуры в байтах
        /// </summary>
        /// <returns></returns>
        public int GetSize()
        {
            if (this._sizeInBytes > -1)
            {
                return this._sizeInBytes;
            }

            if (InitData.ContainsKey(this.GetType()))
            {
                InitData data = InitData[this.GetType()];
                this._sizeInBytes = data.Size;
                this._fields = data.Fields;
                this.Properties = data.Properties;
                return this._sizeInBytes;
            }
            this.InitFields();
            this.InitProperties();
            InitData.Add(GetType(), new InitData(this._sizeInBytes, this._fields, this.Properties, this._ignored));
            return this._sizeInBytes;
        }


        /// <summary>
        /// Получение данных о полях и размере структуры
        /// </summary>
        private void InitFields()
        {
            this._fields = this._fields ?? new SortedDictionary<LayoutAttribute, FieldInfo>(new LayoutComparer());
            this._ignored = this._ignored ?? new List<int>();
            FieldInfo[] fields = GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
            int result = 0;
            foreach (FieldInfo fieldInfo in fields)
            {
                try
                {
                    object[] atrs = fieldInfo.GetCustomAttributes(false);
                    LayoutAttribute layout = atrs.OfType<LayoutAttribute>().FirstOrDefault();
                    if (layout != null)
                    {
                        this._fields.Add(layout, fieldInfo);

                        List<int> childIgnor;
                        if (fieldInfo.FieldType.IsArray)
                        {
                            Type fieldType = fieldInfo.FieldType.GetElementType();
                            int typeSize = this.GetTypeSize(fieldType, out childIgnor);
                            if (layout.Ignore)
                            {
                                IEnumerable<int> addIgnor = Enumerable.Range(result, layout.Count * typeSize);
                                this._ignored.AddRange(addIgnor);
                            }
                            result += layout.Count * typeSize;
                        }
                        else
                        {
                            int typeSize = this.GetTypeSize(fieldInfo.FieldType, out childIgnor);
                            if (layout.Ignore)
                            {
                                IEnumerable<int> addIgnor = Enumerable.Range(result, typeSize);
                                this._ignored.AddRange(addIgnor);

                            }
                            else
                            {
                                if (childIgnor != null)
                                {
                                    this._ignored.AddRange(childIgnor.Select(o => o + result));
                                }
                            }

                            result += typeSize;
                        }
                    }
                }
                catch (Exception)
                {
                }
            }
            this._sizeInBytes = result;
            this._ignored = this._ignored.Distinct().ToList();
            this._ignored.Sort();
        }
        
        /// <summary>
        /// Получение данных о полях и размере структуры
        /// </summary>
        private void InitProperties()
        {
            this.Properties = this.Properties ?? new SortedDictionary<int, PropertyInfo>();
            
            PropertyInfo[] properties = GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
            
            foreach (PropertyInfo property in properties)
            {
                try
                {
                    object[] atrs = property.GetCustomAttributes(false);
                    BindingPropertyAttribute bindingAtt = atrs.OfType<BindingPropertyAttribute>().FirstOrDefault();
                    if (bindingAtt != null)
                    {
                        this.Properties.Add(bindingAtt.Position, property);
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show(string.Format("В классе {0} свойство {1} дублирует индекс", this.GetType(), property));
                }
            }
        }

        /// <summary>
        /// Рассчёт размера объекта класса в байтах
        /// </summary>
        /// <param name="type">Класс</param>
        /// <param name="ignored">Пропускаемые адреса</param>
        /// <returns>Размер</returns>
        private int GetTypeSize(Type type, out List<int> ignored)
        {
            if (type.BaseType == typeof(StructBase))
            {
                StructBase a = Activator.CreateInstance(type, true) as StructBase;
                int size = a.GetSize();
                ignored = a._ignored;
                return size;
            }
            ignored = null;
            if (type == typeof(int)||type == typeof(uint))
            {
                return sizeof(int);
            }
            if (type == typeof(ushort)||type == typeof(short))
            {
                return sizeof(short);
            }
            if (type == typeof (byte))
            {
                return sizeof (byte);
            }
            return 0;
        } 
        #endregion [Help members]

       
        #region [Interface Members]

        /// <summary>
        /// Инициализирует структуру данными
        /// </summary>
        /// <param name="array">данные</param>
        public virtual void InitStruct(byte[] array)
        {
            int index = 0;
            try
            {
                foreach (KeyValuePair<LayoutAttribute, FieldInfo> field in this._fields)
                {
                    if (field.Key.Ignore)
                    {
                        continue;
                    }
                    if (field.Value.FieldType == typeof (byte))
                    {
                        field.Value.SetValue(this, array[index]);
                        index++;
                    }
                    if (field.Value.FieldType == typeof (short))
                    {
                        field.Value.SetValue(this, (short) StructHelper.GetUshort(array, ref index));
                    }
                    if (field.Value.FieldType == typeof (ushort))
                    {
                        field.Value.SetValue(this, StructHelper.GetUshort(array, ref index));
                    }
                    if (field.Value.FieldType == typeof (int))
                    {
                        field.Value.SetValue(this, StructHelper.GetInt(array, ref index));
                    }
                    if (field.Value.FieldType == typeof(uint))
                    {
                        field.Value.SetValue(this, (uint)StructHelper.GetInt(array, ref index));
                    }
                    if (field.Value.FieldType.BaseType == typeof (StructBase))
                    {
                        StructBase type = Activator.CreateInstance(field.Value.FieldType, true) as StructBase;
                        StructBase value = GetOneStruct(array, ref index, type, this._maxSlotLength);
                        field.Value.SetValue(this, value);
                    }

                    if (field.Value.FieldType.IsArray)
                    {
                        Type fieldType = field.Value.FieldType.GetElementType();
                        if (fieldType.BaseType == typeof (StructBase))
                        {
                            StructBase[] value =
                                Activator.CreateInstance(field.Value.FieldType, field.Key.Count) as StructBase[];
                            for (int i = 0; i < value.Length; i++)
                            {
                                value[i] = Activator.CreateInstance(fieldType) as StructBase;
                            }
                            StructHelper.GetArrayStruct(array, value, ref index, this._maxSlotLength);
                            field.Value.SetValue(this, value);
                        }
                        if (field.Value.FieldType.GetElementType() == typeof (ushort))
                        {
                            ushort[] value = new ushort[field.Key.Count];
                            StructHelper.GetUshortArray(array, value, ref index);
                            field.Value.SetValue(this, value);
                        }
                        if (field.Value.FieldType.GetElementType() == typeof (short))
                        {
                            short[] value = new short[field.Key.Count];
                            StructHelper.GetShortArray(array, value, ref index);
                            field.Value.SetValue(this, value);
                        }
                        if (field.Value.FieldType.GetElementType() == typeof (int))
                        {
                            int[] value = new int[field.Key.Count];
                            StructHelper.GetIntArray(array, value, ref index);
                            field.Value.SetValue(this, value);
                        }
                        if (field.Value.FieldType.GetElementType() == typeof(uint))
                        {
                            uint[] value = new uint[field.Key.Count];
                            StructHelper.GetUintArray(array, value, ref index);
                            field.Value.SetValue(this, value);
                        }
                        if (field.Value.FieldType.GetElementType() == typeof (byte))
                        {
                            byte[] value = new byte[field.Key.Count];
                            for (int i = 0; i < value.Length; i++)
                            {
                                value[i] = array[index];
                                index++;
                            }
                            field.Value.SetValue(this, value);
                        }
                    }
                }
            }
            catch (ArgumentException)
            {
                // закончился массив байт, остальные структуры игнорируем.
            }
            catch (IndexOutOfRangeException)
            {
                // закончился массив байт, остальные структуры игнорируем.
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка инициализации структуры. " + e.Message, "Внимание", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
        }


        /// <summary>
        /// Преобразует данные структуры в массив слов с учетом игнорируемых полей
        /// </summary>
        /// <returns>Массив слов</returns>
        public virtual ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>(this._sizeInBytes / 2);
            var fields = this._fields.Where(field => !field.Key.Ignore).ToList();
            foreach (KeyValuePair<LayoutAttribute, FieldInfo> field in fields)
            {
                this.AddValueInRetMas(result, field);
            }
            return result.ToArray();
        }
        
        private void AddValueInRetMas(List<ushort> ret, KeyValuePair<LayoutAttribute, FieldInfo> field)
        {
            if (field.Value.FieldType == typeof(ushort))
            {
                ret.Add((ushort)field.Value.GetValue(this));
            }
            if (field.Value.FieldType == typeof(int) || field.Value.FieldType.BaseType == typeof(uint))
            {
                ret.AddRange(StructHelper.IntToUshorts((int)field.Value.GetValue(this)));
            }
            if (field.Value.FieldType.BaseType == typeof(StructBase))
            {
                ret.AddRange(((StructBase)field.Value.GetValue(this)).GetValues());
            }
            if (field.Value.FieldType.IsArray)
            {
                Type fieldType = field.Value.FieldType.GetElementType();

                if (fieldType.BaseType == typeof(StructBase))
                {
                    StructBase[] value = (StructBase[])field.Value.GetValue(this);
                    for (int i = 0; i < value.Length; i++)
                    {
                        ret.AddRange(value[i].GetValues());
                    }
                }

                if (field.Value.FieldType.GetElementType() == typeof(ushort))
                {
                    ushort[] value;
                    if (field.Key.Ignore)
                    {
                        value = new ushort[field.Key.Count];
                    }
                    else
                    {
                        value = (ushort[])field.Value.GetValue(this);
                    }
                    ret.AddRange(value);
                }
            }
        }
        
        public virtual object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            //размер 1-го слота
            List<ushort> wordIgnore = this._ignored.Select(o => (ushort)(o / 2 + start)).Distinct().ToList();
            int fullSize = this.GetSize() / 2;

            List<Device.slot> res = new List<Device.slot>();
            IEnumerable<AddressPair> pairs = AddressPair.CreatePairs(start, (ushort)(start + fullSize), wordIgnore);

            foreach (AddressPair addressPair in pairs)
            {
                IEnumerable<Device.slot> temp = this.CreateSlotsAdd(addressPair, slotLen);
                res.AddRange(temp);
            }
            return res.ToArray();
        }

        public virtual StructInfo GetStructInfo(int slotLen = MAX_SLOT_LENGHT_DEFAULT)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(this.GetSize() / 2);
            sInfo.SlotsArray = sInfo.FullSize > slotLen;
            return sInfo;
        }
        #endregion [Interface Members]


        #region [Nested types]
        private class AddressPair
        {
            private ushort _start;
            private ushort _end;

            public AddressPair(ushort start, ushort end)
            {
                this._start = start;
                this._end = end;
            }

            public static IEnumerable<AddressPair> CreatePairs(ushort startAddress, ushort endAddress, List<ushort> ignored)
            {
                List<AddressPair> res = new List<AddressPair>();
                if (ignored.Count == 0)
                {
                    res.Add(new AddressPair(startAddress, endAddress));
                    return res;
                }
                res.Add(new AddressPair(startAddress, ignored[0]));
                for (int i = 0; i < ignored.Count - 1; i++)
                {
                    if (ignored[i] == ignored[i + 1] - 1)
                    {
                        continue;
                    }
                    else
                    {
                        res.Add(new AddressPair((ushort)(ignored[i] + 1), ignored[i + 1]));
                    }
                }
                res.Add(new AddressPair((ushort)(ignored.Last() + 1), endAddress));
                return res;
            }

            public ushort Start
            {
                get { return this._start; }
            }

            public ushort End
            {
                get { return this._end; }
            }
            public int Size
            {
                get { return this._end - this._start; }
            }
        }

        private class LayoutComparer : Comparer<LayoutAttribute>
        {
            public override int Compare(LayoutAttribute x, LayoutAttribute y)
            {
                return x.Position.CompareTo(y.Position);
            }
        }
        
        #endregion [Nested types]
    }
}
