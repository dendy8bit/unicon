﻿using BEMN.Devices.Structures.Attributes;

namespace BEMN.Devices.Structures
{
    /// <summary>
    /// Устанавливает страницу осцилограммы
    /// </summary>
    public class SetOscStartPageStruct : StructBase

    {
        #region [Private fields]
        [Layout(0)] private ushort _pageIndex; 
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// Установить страницу осцилограммы
        /// </summary>
        public ushort PageIndex
        {
            get { return this._pageIndex; }
            set { this._pageIndex = value; }
        } 
        #endregion [Properties]
    }
}
