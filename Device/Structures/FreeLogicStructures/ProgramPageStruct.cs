﻿using BEMN.Devices.Structures.Attributes;

namespace BEMN.Devices.Structures.FreeLogicStructures
{
    public class ProgramPageStruct : StructBase
    {
        [Layout(0, Count = 1)]
        private ushort[] _programPage;

        [BindingProperty(0)]
        public ushort[] ProgramPage
        {
            get { return this._programPage; }
            set { this._programPage = value; }
        }
    }
}
