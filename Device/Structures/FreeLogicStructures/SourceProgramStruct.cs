﻿using BEMN.Devices.Structures.Attributes;

namespace BEMN.Devices.Structures.FreeLogicStructures
{
    public class SourceProgramStruct:StructBase
    {
        [Layout(0, Count = 1024)]
        private ushort[] _program;

        [BindingProperty(0)]
        public ushort[] ProgramStorage
        {
            get { return this._program; }
            set { this._program = value; }
        }
    }
}
