﻿using BEMN.Devices.Structures.Attributes;

namespace BEMN.Devices.Structures.FreeLogicStructures
{
    public class ProgramStorageStruct:StructBase
    {
        [Layout(0, Count = 8192)]
        private ushort[] _programStorage;

        [BindingProperty(0)]
        public ushort[] ProgramStorage
        {
            get { return this._programStorage; }
            set { this._programStorage = value; }
        }
    }
}
