﻿using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.Devices.Structures.FreeLogicStructures
{
    public struct ProgramSignalsStruct: IStruct,IStructInit
    {
        private ushort[] _values;

        public ushort[] Values
        {
            get { return this._values; }
            set { this._values = value; }
        }


        public void InitStruct(byte[] array)
        {
            this._values = new ushort[array.Length / 2];
            int ind = 0;
            for (int i = 0; i < this._values.Length; i++)
            {
                this._values[i] = Common.TOWORD(array[ind + 1], array[ind]);
                ind += sizeof(ushort);
            }
        }

        public ProgramSignalsStruct(int countValues)
        {
            this._values = new ushort[countValues];
        }

        public ushort[] GetValues()
        {
            return this._values;
        }

        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, GetType(), slotLen);
        }
    }
}
