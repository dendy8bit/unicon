﻿using BEMN.Devices.Structures.Attributes;

namespace BEMN.Devices.Structures.FreeLogicStructures
{
    public class StartStruct : StructBase
    {
        [Layout(0, Count = 1)]
        private ushort[] _startProgramCommand;

        [BindingProperty(0)]
        public ushort[] StartProgramCommand
        {
            get { return this._startProgramCommand; }
            set { this._startProgramCommand = value; }
        }
    }
}
