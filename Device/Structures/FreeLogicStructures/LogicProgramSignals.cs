﻿using BEMN.Devices.Structures.Attributes;

namespace BEMN.Devices.Structures.FreeLogicStructures
{
    public class LogicProgramSignals : StructBase
    {
        [Layout(0)] ushort[] _values = new ushort[1];

        public LogicProgramSignals()
        {
        }

        public LogicProgramSignals(int countValues)
        {
            this._values = new ushort[countValues];
        }

        public LogicProgramSignals(byte[] array)
        {
            this.InitStruct(array);
        }

        public sealed override void InitStruct(byte[] array)
        {
            this._values = new ushort[array.Length/2];
            base.InitStruct(array);
        }

        public ushort[] Values
        {
            get { return this._values; }
            set { this._values = value; }
        }
    }
}
