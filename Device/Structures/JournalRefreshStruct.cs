﻿using System;
using System.Runtime.InteropServices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;

namespace BEMN.Devices.Structures
{
    /// <summary>
    /// При записи сбрасывает журнал на начало
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct JournalRefreshStruct : IStruct, IStructInit
    {
        #region [Private fields]
        private readonly ushort _reserv; 
        #endregion [Private fields]


        #region IStruct Members
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, GetType(), slotLen);
        }
        #endregion IStruct Members


        #region IStructInit Members
        public void InitStruct(byte[] array)
        {
            throw new NotImplementedException();
        }

        public ushort[] GetValues()
        {
            return new ushort[]{0};
        }
        #endregion IStructInit Members
    }
}
