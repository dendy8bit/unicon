﻿namespace BEMN.Devices
{
    /// <summary>
    /// Структура для чтения/ записа одного слова
    /// </summary>
    public struct OneWordStruct : IStruct, IStructInit
    {
        public OneWordStruct(ushort word)
        {
            this._word = word;
        }

        #region [Fields]
        private ushort _word; 
        #endregion [Fields]

        public ushort Word
        {
            get { return this._word; }
            set { this._word = value; }
        }

        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, slotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]


        #region [IStructInit Members]

        public void InitStruct(byte[] array)
        {
            int index = 0;

            this._word = StructHelper.GetUshort(array, ref index);
        }

        public ushort[] GetValues()
        {
            return new[]
                {
                    this._word
                };
        }
        #endregion [IStructInit Members]
    }
}
