﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.Devices.Structures;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MBServer.Queries;

namespace BEMN.Devices.MemoryEntityClasses
{
    public class MemoryEntity<T> : IDeviceInfo where T : IStruct, IStructInit, new()
    {
        private const string READ_PATTERN = "{0}_{1}:Чтение";
        private const string WRITE_PATTERN = "{0}_{1}:Запись";
        private int _slotLen;

        #region Поля

        private Device _device;
        private string _queryName;
        private int _valuesIndex;
        private List<string> _slotNames;
        private int _slotIndex;
        private T _structObj;
        private ushort[] _values;
        private List<Device.slot> _slots;
        private bool _allReadOk = true;
        private bool _allWriteOk = true;
        private OperationType _status = OperationType.NONE;
        private ushort _startAddress;
        private Func<T, bool> _condition;
        #endregion
        
        #region Конструкторы
        
        public MemoryEntity(string queryName, Device device, ushort startAdress, int slotLen = StructBase.MAX_SLOT_LENGHT_DEFAULT)
        {
            this._device = device;
            this._slotLen = slotLen;
            if (this._device != null && this._device.MB != null)
            {
                this._device.MB.CompleteExchange += this._mb_CompleteExchange;
            }
            this.BaseInitialize(queryName, startAdress);
            this._structObj = new T(); // получаем объект структуры
            this._slots = new List<Device.slot>();
            this._slotNames = new List<string>();
            this.SetSlots();
        }

        private void BaseInitialize(string queryName, ushort startAdress)
        {
            this._queryName = queryName;
            this._startAddress = startAdress;

            this.ReadOk += this.OnReadOk;
            this.ReadFail += this.OnReadFail;
            this.WriteOk += this.StObj_WriteOk;
            this.WriteFail += this.StObj_WriteFail;
        }
        
        private void SetSlots()
        {
            StructInfo sInf = this._structObj.GetStructInfo(this._slotLen);
            if (sInf.FullSize > 0)
            {
                this.Values = new ushort[sInf.FullSize];
            }
            object slots = this._structObj.GetSlots(this._startAddress, sInf.SlotsArray, this._slotLen);
            if (slots != null)
            {
                this._slots.Clear();
                Device.slot[] slotsArray = slots as Device.slot[];
                if (slotsArray != null)
                {
                    this._slots.AddRange(slotsArray);
                }
                else
                {
                    this._slots.Add(slots as Device.slot);
                }
            }
        }
        #endregion

        #region Свойства

        public string DeviceVersion => this._device.DeviceVersion;
        public string DeviceType => this._device.DeviceType;
        public string DevicePlant => this._device.DevicePlant;

        public string Caption => this._device.Caption;

        public T Value
        {
            get { return this._structObj; }
            set { this._structObj = value; }
        }

        public Device DeviceObj
        {
            get { return this._device; }
            set { this._device = value; }
        }
        
        public ushort[] Values
        {
            get { return this._values; }
            set { this._values = value; }
        }

        public List<Device.slot> Slots
        {
            get { return this._slots; }
            set { this._slots = value; }
        }

        public virtual ushort StartAddress
        {
            get { return this._startAddress; }
            set
            {
                this._startAddress = value;
                this.SetSlots();
            }
        }
        #endregion
        
        #region Ивенты
        public event Handler ReadOk;
        public event Handler ReadFail;
        public event Handler WriteOk;
        public event Handler WriteFail;


        public event MemoryEntityOperationComplite AllReadOk;
        public event MemoryEntityOperationComplite AllReadFail;
        public event MemoryEntityOperationComplite AllWriteOk;
        public event MemoryEntityOperationComplite AllWriteFail;
        private bool _cycle;
        #endregion

        #region Обработчики событий
        void StObj_WriteFail(object sender)
        {
            this._allWriteOk = false;
            this._slotIndex++;
            if (this._slotIndex >= this._slots.Count )
            {
                this._slotIndex = 0;
                this.OnRaiseAllWriteFail();
            }
        }

        void StObj_WriteOk(object sender)
        {
            this._allWriteOk = true;
            this._slotIndex++;
            if (this._slotIndex >= this._slots.Count)
            {
                this._slotIndex = 0;
                if (this._allWriteOk)
                {
                    this.OnRaiseAllWriteOk();
                }
                else
                {
                    this.OnRaiseAllWriteFail();
                }
            }
        }

        private void OnReadFail(object sender)
        {
            this._allReadOk = false;
            this._slotIndex++;

            if (this._cycle)
            {
                if (this._slotIndex >= this._slots.Count)
                {
                    this._slotIndex = 0;
                    this.OnRaiseAllReadFail();
                }
            }
            else
            {
                //this._device.RemoveQuerys();
                this.RemoveStructQueries();
                this.OnRaiseAllReadFail();
            }
        }

        private void OnReadOk(object sender)
        {
            try
            {
                Array.ConstrainedCopy(this._slots[this._slotIndex].Value, 0, this._values, this._valuesIndex, this._slots[this._slotIndex].Value.Length);

                if (this._slotIndex < this._slots.Count)
                {
                    this._valuesIndex += this._slots[this._slotIndex].Value.Length;
                }
                this._slotIndex++;
                if (this._slotIndex >= this._slots.Count)
                {
                    this._slotIndex = 0;
                    this._valuesIndex = 0;
                    this._structObj.InitStruct(Common.TOBYTES(this._values, false));
                    
                    if (this._allReadOk)
                    {
                        this.OnRaiseAllReadOk();

                    }
                    else
                    {
                        this.OnRaiseAllReadFail();
                        this._allReadOk = true;
                    }
                }
            }
            catch(Exception e)
            {
                this._slotIndex = this._slots.Count - 1;
            }
        }
        
        private void MemoryEntity_ReadOk(object sender)
        {
            if (this._condition.Invoke(this.Value))
            {
                //this._device.RemoveQuerys();
                this.RemoveStructQueries();
                this.ReadOk -= this.MemoryEntity_ReadOk;
            }
        }
        #endregion


        public void LoadStructCycleWhile(Func<T, bool> condition)
        {
            this._condition = condition;
            this.ReadOk += this.MemoryEntity_ReadOk;
            this.LoadStructCycle();

        }
        
        public void LoadStruct()
        {
            this._valuesIndex = this._slotIndex = 0;
            this._allReadOk = true;
            this.InitReadSlotsNames();
            for (int i = 0; i < this._slots.Count; i++)
            {
                this._device.LoadSlot(this._device.DeviceNumber, this._slots[i], string.Format(READ_PATTERN, this._queryName, i), this);
            }
        }
        public void LoadStruct(TimeSpan span)
        {
            this._valuesIndex = this._slotIndex = 0;
            this._allReadOk = true;
            this.InitReadSlotsNames();
            for (int i = 0; i < this._slots.Count; i++)
            {
                this._device.LoadSlot(this._device.DeviceNumber, this._slots[i], string.Format(READ_PATTERN, this._queryName, i), this, span);
            }
        }

        private void InitReadSlotsNames()
        {
            this._status = OperationType.READ;
            this._slotNames = this._slotNames ?? new List<string>();
          
            for (int i = 0; i < this._slots.Count; i++)
            {
                if (!this._slotNames.Contains(string.Format(READ_PATTERN, this._queryName, i)))
                {
                    this._slotNames.Add(string.Format(READ_PATTERN, this._queryName, i));
                }
            }
        }

        /// <summary>
        /// Выводит диалоговое окно и устанавливает бит в true по адресу
        /// </summary>
        /// <param name="adress">Адрес(битный)</param>
        /// <param name="requestName">Название запроса(подпись диалогового окна)</param>
        public void SetBitByAdress(ushort adress, string requestName, bool isDialog = false)
        {
            if (isDialog)
            {
                if (MessageBox.Show(string.Format("{0}?", requestName), string.Empty, MessageBoxButtons.OKCancel) ==
                    DialogResult.OK)
                {
                    this._device.SetBit(this._device.DeviceNumber, adress, true, requestName, this, this._device.DefaultTimeSpan);
                }
            }
            else
            {
                this._device.SetBit(this._device.DeviceNumber, adress, true, requestName, this, this._device.DefaultTimeSpan);
            }
        }
        
        /// <summary>
        /// Циклическое чтение структуры
        /// </summary>
        public void LoadStructCycle(TimeSpan span = new TimeSpan())
        {
            this._slotIndex = this._valuesIndex = 0;
            this.InitReadSlotsNames();
            this._cycle = true;
            bool c = span == this._device.DefaultTimeSpan || span == TimeSpan.Zero;
            for (int i = 0; i < this._slots.Count; i++)
            {
                this._device.LoadSlotCycle(this._device.DeviceNumber, this._slots[i], string.Format(READ_PATTERN, this._queryName, i),
                    c ? this._device.DefaultTimeSpan : span, 10, this);
            }
        }
        /// <summary>
        /// Циклическое чтение битов
        /// </summary>
        public void LoadBitsCycle(TimeSpan span = new TimeSpan())
        {
            this.InitReadSlotsNames();
            this._cycle = true;
            for (int i = 0; i < this._slots.Count; i++)
            {
                this._device.LoadBitsCycle(this._device.DeviceNumber, this._slots[i], string.Format(READ_PATTERN, this._queryName, i),
                    span, 10, this);
            }
        }

        public void RemoveStructQueries()
        {
            if (this._slotNames == null) 
                return;

            foreach (string name in this._slotNames)
            {
                this._device.MB.RemoveQuery(name);
            }
            this._slotNames.Clear();
        }
        /// <summary>
        /// Запись структуры
        /// </summary>
        public void SaveStruct()
        {
            List<string> temp = this.PrepareSave();
            for (int i = 0; i < this._slots.Count; i++)
            {
                this._device.SaveSlot(this._device.DeviceNumber, this._slots[i], temp[i], this);
            }
        } 
        public void SaveStruct(TimeSpan span)
        {
            List<string> temp = this.PrepareSave();
            for (int i = 0; i < this._slots.Count; i++)
            {
                this._device.SaveSlot(this._device.DeviceNumber, this._slots[i], temp[i], this, span);
            }
        }

        public void SaveStruct5()
        {
            List<string> temp = this.PrepareSave();
            for (int i = 0; i < this._slots.Count; i++)
            {
                this._device.SaveSlot5(this._device.DeviceNumber, this._slots[i], temp[i], this);
            }
        }

        public void SaveStruct5(TimeSpan span)
        {
            List<string> temp = this.PrepareSave();
            for (int i = 0; i < this._slots.Count; i++)
            {
                this._device.SaveSlot5TimeSpan(this._device.DeviceNumber, this._slots[i], temp[i], this, span);
            }
        }

        public void SaveStruct6()
        {
            List<string> temp = this.PrepareSave();
            for (int i = 0; i < this._slots.Count; i++)
            {
                this._device.SaveSlot6(this._device.DeviceNumber, this._slots[i], temp[i], this);
            }
        }
        public void SaveStruct6(TimeSpan span)
        {
            List<string> temp = this.PrepareSave();
            for (int i = 0; i < this._slots.Count; i++)
            {
                this._device.SaveSlot6TimeSpan(this._device.DeviceNumber, this._slots[i], temp[i], this, span);
            }
        }
        /// <summary>
        /// Сохраняет в устройство одно 16-ти разрядное слово.
        /// Если в структуре массив слов, то указывается номер перезаписываемого слова.
        /// Если нет, то index по умолчанию 0
        /// </summary>
        /// <param name="value">Сохраняемое значение</param>
        /// <param name="index">Номер перезаписываемого слова</param>
        public void SaveOneWord(ushort value, ushort index = 0)
        {
            this._status = OperationType.WRITE;
            int nameInd = 0;
            if (this._slotNames.Count != 0)
            {
                nameInd = this._slotNames.Count - 1;
            }
            for (int i = 0; i < this._slots.Count; i++)
            {
                string currentName = string.Format(WRITE_PATTERN, this._queryName, i);
                if (this._slotNames.Contains(currentName)) continue;
                this._slotNames.Add(currentName);
                nameInd = this._slotNames.Count - 1;
            }
            Device.slot word = new Device.slot((ushort)(this._startAddress + index), (ushort)(this._startAddress + index + 1));
            word.Value[0] = value;
            this._device.SaveSlot6(this._device.DeviceNumber, word, this._slotNames[nameInd], this);
        }
        
        private List<string> PrepareSave()
        {
            this._status = OperationType.WRITE;
            this._slotNames = this._slotNames ?? new List<string>();


            for (int i = 0; i < this._slots.Count; i++)
            {
                string currentName = string.Format(WRITE_PATTERN, this._queryName, i);
                if (!this._slotNames.Contains(currentName))
                {
                    this._slotNames.Add(currentName);
                }
            }

            this._values = this._structObj.GetValues();

            int valIndex = 0;
            for (int i = 0; i < this._slots.Count; i++)
            {
                Array.ConstrainedCopy(this._values, valIndex, this._slots[i].Value, 0, this._slots[i].Value.Length);
                valIndex += this._slots[i].Value.Length;
            }
            List<string> temp = this._slotNames.FindAll(p =>
            {
                string[] st = p.Split(':');
                return st[st.Length - 1] == "Запись";
            });
            return temp;
        }


        private void _mb_CompleteExchange(object sender, Query query)
        {
            if (this != query.deviceObj)
            {
                return;
            }

            if (this._slotNames != null && this._slotNames.Contains(query.name))
            {
                switch (query.func)
                {
                    case 1:
                    case 4:
                        {
                            this._status = OperationType.READ;
                            break;
                        }
                    case 5:
                    case 6:
                    case 16:
                        {
                            this._status = OperationType.WRITE;
                            break;
                        }
                }
                if (this._status == OperationType.READ)
                {
                    List<string> temp = this._slotNames.FindAll(p =>
                        {
                            string[] st = p.Split(':');
                            return st[st.Length - 1] == "Чтение";
                        });
                    Device.slot result = this._slots[temp.IndexOf(query.name)];
                    this._device.Raise(query, this.ReadOk, this.ReadFail, ref result);
                }
                else if (this._status == OperationType.WRITE)
                {
                    if (query.fail == 0)
                    {
                        this.OnRaiseWriteOk();
                    }
                    else
                    {
                        this.OnRaiseWriteFail();
                    }
                }
            }
            if (!query.bDelete && query.bCycle && this._slotNames != null && this._slotNames.Count == 0)
            {
                query.bDelete = true;
                query.bSuspend = true;
            }
            this._device.mb_CompleteExchange(sender, query);
        }

        /// <summary>
        /// Очищает слоты и названия запросов, а также очищает предыдущие запросы
        /// </summary>
        public void Clear()
        {
            this.RemoveStructQueries();
            this._slots.Clear();
        }

        #region [Запуск событий]
        private void OnRaiseWriteFail()
        {
            this.WriteFail?.Invoke(this);
        }

        private void OnRaiseWriteOk()
        {
            this.WriteOk?.Invoke(this);
        }

        private void OnRaiseAllWriteOk()
        {
            this.AllWriteOk?.Invoke(this);
        }

        private void OnRaiseAllWriteFail()
        {
            this.AllWriteFail?.Invoke(this);
        }

        private void OnRaiseAllReadFail()
        {
            this.AllReadFail?.Invoke(this);
        }

        private void OnRaiseAllReadOk()
        {
            this.AllReadOk?.Invoke(this);
        }
        #endregion [Запуск событий]
    }
}
