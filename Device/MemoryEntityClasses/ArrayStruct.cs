﻿using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.Devices.MemoryEntityClasses
{
    public struct ArrayStruct : IStruct, IStructInit
    {
        private const ushort SLOT_LENGHT = 125;
        public static ushort FullSize;
        
        public ushort[] Data { get; private set; }

        public StructInfo GetStructInfo(int slotLen)
        {
            int len = slotLen > SLOT_LENGHT ? slotLen : SLOT_LENGHT;
            return new StructInfo
            {
                FullSize = FullSize,
                SlotsArray = FullSize > len
            };
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            ushort len = slotLen > SLOT_LENGHT ? (ushort)slotLen : SLOT_LENGHT;
            if (!slotArray)
            {
                return new Device.slot(start, (ushort) (start + FullSize));
            }
            int arrayLength = FullSize / len;
            int lastSlotLength = FullSize % len;
            Device.slot[] slots;
            ushort startAddr = start;
            if (lastSlotLength != 0)
            {
                arrayLength++;
                slots = new Device.slot[arrayLength];

                for (int i = 0; i < arrayLength - 1; i++)
                {
                    slots[i] = new Device.slot(startAddr, (ushort)(startAddr + len));
                    startAddr += len;
                }
                slots[arrayLength - 1] = new Device.slot(startAddr, (ushort) (startAddr + lastSlotLength));
            }
            else
            {
                slots = new Device.slot[arrayLength];
                for (int i = 0; i < arrayLength; i++)
                {
                    slots[i] = new Device.slot(startAddr, (ushort)(startAddr + len));
                    startAddr += len;
                }
            }
            return slots;
        }

        public void InitStruct(byte[] array)
        {
            this.Data = Common.TOWORDS(array, false);
        }

        public ushort[] GetValues()
        {
            return this.Data;
        }
    }
}
