﻿using System;
using System.Windows.Forms;

namespace BEMN.Devices.MemoryEntityClasses
{
    public static class HandlerHelper
    {
        /// <summary>
        /// Создаёт делегат вызывающий метод в потоке контрола
        /// </summary>
        /// <param name="control">Контрол</param>
        /// <param name="method">Метод</param>
        /// <returns></returns>
        public static MemoryEntityOperationComplite CreateReadArrayHandler(Control control, Action method)
        {
            return o =>
                {
                    try
                    {
                        if (control.IsHandleCreated)
                        {
                            IAsyncResult res = control.BeginInvoke(method);
                            res.AsyncWaitHandle.WaitOne(5000);
                            control.EndInvoke(res);
                            res.AsyncWaitHandle.Close();
                        }
                    }
                    catch
                    {
                        
                    }
                };
        }
        /// <summary>
        /// Создаёт делегат вызывающий метод в потоке контрола
        /// </summary>
        /// <param name="control">Контрол</param>
        /// <param name="method">Метод</param>
        /// <returns></returns>
        public static Action CreateActionHandler(Control control, Action method)
        {
            return () =>
                {
                    try
                    {
                        if (control.IsHandleCreated)
                        {
                            IAsyncResult res = control.BeginInvoke(method);
                            res.AsyncWaitHandle.WaitOne(5000);
                            control.EndInvoke(res);
                            res.AsyncWaitHandle.Close();
                        }
                    }
                    catch
                    {
                        
                    }

                };
        }

        public static Handler CreateHandler(Control control, Action method)
        {
            return o =>
            {
                try
                {
                    if (control.IsHandleCreated)
                    {
                        IAsyncResult res = control.BeginInvoke(method);
                        res.AsyncWaitHandle.WaitOne(5000);
                        control.EndInvoke(res);
                        res.AsyncWaitHandle.Close();
                    }
                }
                catch
                {
                    
                }

            };
        }

        /// <summary>
        /// Создаёт делегат вызывающий метод. "Съедает" исключения
        /// </summary>
        /// <param name="method">Метод</param>
        /// <returns></returns>
        public static MemoryEntityOperationComplite CreateReadArrayHandler(Action method)
        {
            return o =>
            {
                try
                {
                    IAsyncResult res = method.BeginInvoke(null, null);
                    res.AsyncWaitHandle.WaitOne(5000);
                    method.EndInvoke(res);
                    res.AsyncWaitHandle.Close();
                }
                catch 
                {}               
            };
        }
    }
}
