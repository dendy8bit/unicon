﻿using System;

namespace BEMN.Devices.MemoryEntityClasses.FileOperations
{
    internal class CloseOperator : OperatorReadBase
    {
        private const string FILECLOSE_CMD_PATTERN = "FILECLOSE {0}";
        private int _descriptor;

        public override string CmdStr
        {
            get { return string.Format(FILECLOSE_CMD_PATTERN, this._descriptor); }
        }

        protected override string StateQeryName
        {
            get { return "CloseFileState"; }
        }

        protected override string DataQeryName
        {
            get { return "CloseFileData"; }
        }

        public CloseOperator(Device device):base(device)
        {
        }
        
        public override void Perform(Action<int> complete, object inData)
        {
            completeAction = complete;
            this._descriptor = Convert.ToInt32(inData);
            //InitMemoryEntity(CurrentDevice);
            SetCmd();
        }

        protected override void ReadState()
        {
            string[] state = this.GetStatesStrings();
            if (state.Length == 0) return;
            State.RemoveStructQueries();
            operationComplete = CheckState(state);
            //RemoveMemoryEntity();
            OnOperationComplete();
        }
        
        protected override void ReadData(){ }
    }
}
