﻿using System;
using System.Linq;
using BEMN.MBServer;

namespace BEMN.Devices.MemoryEntityClasses.FileOperations
{
    internal class SessionNumberOperator : OperatorReadBase
    {
        private ushort _sessionNumber;
        /// <summary>
        /// Возвращает номер сессии. Возвращает 0, если не было чтения
        /// </summary>
        public ushort SessionNumber
        {
            get { return operationComplete ? this._sessionNumber : (ushort)0; }
        }

        public SessionNumberOperator(Device device) : base(device)
        {
        }

        public override string CmdStr
        {
            get { return "GETNUMBER"; }
        }

        protected override string StateQeryName
        {
            get { return "GetNumberState"; }
        }

        protected override string DataQeryName
        {
            get { return "GetNumberData"; }
        }

        public override void Perform(Action<int> complete, object inData)
        {
            completeAction = complete;
            operationComplete = false;
            SetCmd();
        }

        protected override void ReadData()
        {
            byte[] readBytes = Common.TOBYTES(Data.Value.DataVal, false).Take(dataLen).ToArray();
            string str = GetDataString(readBytes);
            this._sessionNumber = Convert.ToUInt16(str);
            operationComplete = true;
            OnOperationComplete();
        }
    }
}
