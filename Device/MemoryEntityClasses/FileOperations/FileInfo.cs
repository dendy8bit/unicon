﻿using System.Collections.Generic;
using BEMN.MBServer;

namespace BEMN.Devices.MemoryEntityClasses.FileOperations
{
    public class FileInfo
    {
        private string _attribute;
        private string _date;
        private string _time;

        public string FileName { get; private set; }

        public string Attribute
        {
            get { return _attribute; }
            private set { _attribute = GetAttribute(value); }
        }
        public int Size { get; private set; }
        public string Date
        {
            get { return _date; }
            private set { _date = GetDate(value); }
        }
        public string Time
        {
            get { return _time; }
            private set { _time = GetTime(value); }
        }

        public FileInfo(string[] info)
        {
            FileName = info[0];
            Attribute = info[1];
            Size = int.Parse(info[2]);
            Date = info[3];
            Time = info[4];
        }

        private string GetDate(string strDate)
        {
            ushort value;
            if (ushort.TryParse(strDate, out value))
            {
                int day = Common.GetBits(value, 0, 1, 2, 3, 4);
                int month = Common.GetBits(value, 5, 6, 7, 8) >> 5;
                int year = Common.GetBits(value, 9, 10, 11, 12, 13, 14, 15) >> 9 - 20;
                return string.Format("{0:D2}.{1:D2}.{2:D2}", day, month, year);
            }
            else
            {
                return strDate;
            }
        }

        private string GetTime(string strTime)
        {
            ushort value;
            if (ushort.TryParse(strTime, out value))
            {
                int sec = Common.GetBits(value, 0, 1, 2, 3, 4) * 2;
                int min = Common.GetBits(value, 5, 6, 7, 8, 9, 10) >> 5;
                int hour = Common.GetBits(value, 11, 12, 13, 14, 15) >> 11;
                return string.Format("{0:D2}:{1:D2}:{2:D2}", hour, min, sec);
            }
            else
            {
                return strTime;
            }
        }

        private string GetAttribute(string strAttr)
        {
            ushort ind;
            if (ushort.TryParse(strAttr, out ind))
            {
                Dictionary<ushort, string> typeDictionary = new Dictionary<ushort, string>
                {
                    {0x01, "Read only"},
                    {0x02, "Hidden"},
                    {0x04, "System"},
                    {0x08, "Volume label"},
                    {0x0F, "LNF Entry"},
                    {0x10, "Directory"},
                    {0x20, "Archive"},
                    {0x3F, "Mask of defined bits"}
                };
                return typeDictionary[ind];
            }
            else
            {
                return strAttr;
            }
        }
    }
}
