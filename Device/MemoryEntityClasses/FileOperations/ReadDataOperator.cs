﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.MBServer;

namespace BEMN.Devices.MemoryEntityClasses.FileOperations
{
    internal class ReadDataOperator : OperatorReadBase
    {
        private const string FILEREAD_CMD_PATTERN = "FILEREAD {0};{1}";
        private const int DATA_LEN = 2048;
        private readonly List<byte> _readBytes;
        private int _descriptor;

        public byte[] ReadBytes
        {
            get { return this._readBytes.ToArray(); }
        }

        public ReadDataOperator(Device device) : base(device)
        {
            this._readBytes = new List<byte>();
        }

        public override string CmdStr
        {
            get { return string.Format(FILEREAD_CMD_PATTERN, this._descriptor, DATA_LEN); }
        }

        protected override string StateQeryName
        {
            get { return "ReadDataState"; }
        }

        protected override string DataQeryName
        {
            get { return "ReadData"; }
        }

        public override void Perform(Action<int> complete, object inData)
        {
            completeAction = complete;
            this._readBytes.Clear();
            this._descriptor = Convert.ToInt32(inData);
            //InitMemoryEntity(CurrentDevice);
            SetCmd();
        }
        
        protected override void ReadData()
        {
            if (dataLen != 0)
            {
                byte[] buff = Common.TOBYTES(Data.Value.DataVal, false);
                this._readBytes.AddRange(buff.Take(dataLen));
                SetCmd();
            }
            else
            {
                operationComplete = true;
                //RemoveMemoryEntity();
                OnOperationComplete();
            }
        }
    }
}
