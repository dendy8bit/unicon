﻿namespace BEMN.Devices.MemoryEntityClasses
{
    public enum OperationType
    {
        NONE = 0,
        READ = 1,
        WRITE = 2
    }
}