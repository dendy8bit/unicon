using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml;
using BEMN.Interfaces;

namespace BEMN.Devices
{
    public class TypeNotUniqueException : ApplicationException
    {
        private string _dllName;
        public string DllName => this._dllName;

        public TypeNotUniqueException(string msg, string dllName)
            : base(msg)
        {
            this._dllName = dllName;
        }
    }
    public class DllLoader
    {
        // ����������� ��� ����������
        private static List<Type> _deviceTypes = new List<Type>();
        private static List<Type> _formTypes = new List<Type>();
        private static List<Type> _moduleTypes = new List<Type>();
        private static List<Type> _connectModuleTypes = new List<Type>();
        private static List<Type> _nodeTypes = new List<Type>();

        /// <summary>
        /// ������ ���� ����� ���� ���������.
        /// ���������� ����� �����: ���������� ���� ��������� (�������� �����) + ���������� ���� ������� ���������� (�������� �����).
        /// _nodeTypes.Count = _deviceTypes.Count + _formTypes.Count;
        /// </summary>
        public static List<Type> NodeTypes => _nodeTypes;

        /// <summary>
        /// ������ ���� ��������� � �������.
        /// </summary>
        public static List<Type> DeviceTypes => _deviceTypes;

        /// <summary>
        /// ������ ���� ���� ���� ���������.
        /// </summary>
        public static List<Type> FormTypes => _formTypes;

        public static List<Type> ConnectModuleTypes => _connectModuleTypes;

        public static List<Type> ModuleTypes => _moduleTypes;

        public static Type GetType(string typeStr)
        {
            Type ret = null;

            foreach (string dllName in DllNames)
            {
                Assembly curAssmbl = Assembly.LoadFrom(dllName);
                ret = curAssmbl.GetType(typeStr);
                if (null != ret)
                {
                    break;
                }
            }
            return ret;
        }

        /// <summary>
        /// ������ ���� *.dll � PiconFramework->bin->Debug->Devices.
        /// </summary>
        protected static List<string> DllNames
        {
            get
            {
                List<string> ret = new List<string>(Directory.GetFiles("Devices", "*.dll"));
                ret.Add("Device.dll"); 
                return ret;
            }
        }

        private static bool _dllListChanged = false;

        public static void LoadDllData()
        {
            ParseTypes();
            _dllListChanged = IsDllListChanged();
            if (_dllListChanged)
            {
                GetDataFromDll();
                SaveCache();
            }
            else
            {
                LoadCache();
            }
        }

        public class ImageHelper
        {
            /// <summary>
            /// ����� ����������� ������ � ��������.
            /// </summary>
            public static Image FromString(string imgString)
            {
                byte[] bytes = Convert.FromBase64String(imgString);
                Image ret = Image.FromStream(new MemoryStream(bytes));
                return ret;
            }
            /// <summary>
            /// ����� ����������� �������� � ������.
            /// </summary>
            public static string ToString(Image img)
            {
                SerializationInfo si = new SerializationInfo(img.GetType(), new FormatterConverter());
                ((ISerializable) img).GetObjectData(si, new StreamingContext());
                byte[] bytes = (byte[]) si.GetValue("Data", typeof (byte[]));
                string ret = Convert.ToBase64String(bytes);
                return ret;
            }
        }

        /// <summary>
        /// ���� ���� �� ���� PiconFramework\bin\Debug\cache.xml ����������,
        /// �� ��������� ��� � ���������� doc � ������ �� ���� ������
        /// ���� ��������� ����� ����� (<Dlls> *.dll </Dlls>),
        /// ���������� ���������� ������ �� ������� ���� *.dll � PiconFramework->bin->Debug->Devices.
        /// true, ���� ������ �� �����.
        /// </summary>
        public static bool IsDllListChanged()
        {
            bool ret = true;
            XmlDocument doc = new XmlDocument();
            
            if (File.Exists("cache.xml"))
            {
                List<string> cachedDllNames = new List<string>();
                doc.Load("cache.xml");
                XmlNode dllList = doc.SelectSingleNode("/Cache/Dlls");
                for (int i = 0; i < dllList.ChildNodes.Count; i++)
                {
                    XmlNode dllName = dllList.ChildNodes[i];
                    cachedDllNames.Add(dllName.InnerText);
                }
                ret = !IsListEquals(cachedDllNames, DllNames); // ret = !false, ���� ������ �� ����� 
            }
            return ret;
        }

        /// <summary>
        /// ���������� 2 ������ 
        /// </summary>
        static bool IsListEquals(List<string> list1, List<string> list2)
        {
            bool ret = true;
            if (list1.Count != list2.Count)
            {
                ret = false;
            }
            else
            {
                for (int i = 0; i < list1.Count; i++)
                {
                    if (list2[i] != list1[i])
                    {
                        ret = false;
                        break;
                    }
                }
            }
            return ret;
        }

        /// <summary>
        /// ���������� ��� �������� ���� �� ������� _formImagesHash.
        /// </summary>
        public static List<Image> GetFormImages()
        {
            return DictionaryHelper<Type, Image>.GetDictionaryValues(_formImagesHash);
        }

        /// <summary>
        /// ���������� ��� �������� ��������� �� ������� _deviceImagesHash.
        /// </summary>
        public static List<Image> GetDeviceImages()
        {
            return DictionaryHelper<Type, Image>.GetDictionaryValues(_deviceImagesHash);
        }
        /// <summary>
        /// ���������� ����� ���� ���� ��������� �� ������� _formNamesHash.
        /// </summary>
        public static List<string> GetFormNames()
        {
            return DictionaryHelper<Type, string>.GetDictionaryValues(_formNamesHash);
        }

        /// <summary>
        /// ���������� ����� ���� ��������� �� ������� _deviceNamesHash.
        /// </summary>
        public static List<string> GetDeviceNames()
        {
            return DictionaryHelper<Type, string>.GetDictionaryValues(_deviceNamesHash);
        }

        public static List<bool> GetFormForceShow()
        {
            return DictionaryHelper<Type, bool>.GetDictionaryValues(_formForceShowHash);
        }
        
        /// <summary>
        /// ���������� �������� ���� ����� �� ������� _nodeImageHash.
        /// </summary>
        public static List<Image> GetNodeImages()
        {
            return DictionaryHelper<Type, Image>.GetDictionaryValues(_nodeImagesHash);
        }

        /// <summary>
        /// ���������� ����� ���� ����� �� ������� _nodeNamesHash.
        /// </summary>
        public static List<string> GetNodeNames()
        {
            return DictionaryHelper<Type, string>.GetDictionaryValues(_nodeNamesHash);
        }

        class DictionaryHelper<TKey, TValue>
        {
            /// <summary>
            /// ����� �������� List �� ���������� �� Dictionary.
            /// </summary>
            public static List<TValue> GetDictionaryValues(Dictionary<TKey, TValue> dictionary)
            {
                List<TValue> ret = new List<TValue>();
                foreach (TValue var in dictionary.Values)
                {
                    ret.Add(var);
                }
                return ret;
            }
        };

        class NodeHelper<T>
        {
            public static XmlElement CreateXmlNodeFromList(XmlDocument doc, List<T> list, string rootName, string nodesName)
            {
                XmlElement ret = doc.CreateElement(rootName);
                foreach (T var in list)
                {
                    XmlElement node = doc.CreateElement(nodesName);
                    if (var is Image)
                    {
                        node.InnerText = ImageHelper.ToString(var as Image);
                    }
                    else
                    {
                        node.InnerText = var.ToString();
                    }
                    ret.AppendChild(node);
                }
                return ret;
            }

            /// <summary>
            /// ���������� �������, ��� Key - ����, Value - ��� ����/��� ����������/��� �����. �� ����� cache.xml.
            /// </summary>
            public static Dictionary<Type, string> CreateStringHashFromXmlNode(XmlElement node, List<Type> types)
            {
                Dictionary<Type, string> ret = new Dictionary<Type, string>(types.Count);
                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    XmlNode chNode = node.ChildNodes[i];
                    ret.Add(types[i], chNode.InnerText);
                }
                return ret;
            }

            /// <summary>
            /// ���������� �������, ��� Key - ����, Value - �������� ����/�������� �����/�������� ����������. �� ����� cache.xml.
            /// </summary>
            public static Dictionary<Type, Image> CreateImageHashFromXmlNode(XmlElement node, List<Type> types)
            {
                Dictionary<Type, Image> ret = new Dictionary<Type, Image>(types.Count);
                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    XmlNode chNode = node.ChildNodes[i];
                    ret.Add(types[i], ImageHelper.FromString(chNode.InnerText));
                }
                return ret;
            }

            public static Dictionary<Type, bool> CreateBoolHashFromXmlNode(XmlElement node, List<Type> types)
            {
                Dictionary<Type, bool> ret = new Dictionary<Type, bool>(types.Count);
                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    XmlNode chNode = node.ChildNodes[i];
                    ret.Add(types[i], bool.Parse(chNode.InnerText));
                }
                return ret;
            }
        };

        public static void LoadCache()
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load("cache.xml");
                XmlElement names = (XmlElement) doc.SelectSingleNode("/Cache/Nodes/Names");
                _nodeNamesHash = NodeHelper<object>.CreateStringHashFromXmlNode(names, NodeTypes);
                XmlElement images = (XmlElement) doc.SelectSingleNode("/Cache/Nodes/Images");
                _nodeImagesHash = NodeHelper<object>.CreateImageHashFromXmlNode(images, NodeTypes);

                names = (XmlElement) doc.SelectSingleNode("/Cache/Devices/Names");
                _deviceNamesHash = NodeHelper<object>.CreateStringHashFromXmlNode(names, DeviceTypes);
                images = (XmlElement) doc.SelectSingleNode("/Cache/Devices/Images");
                _deviceImagesHash = NodeHelper<object>.CreateImageHashFromXmlNode(images, DeviceTypes);

                names = (XmlElement) doc.SelectSingleNode("/Cache/Forms/Names");
                _formNamesHash = NodeHelper<object>.CreateStringHashFromXmlNode(names, FormTypes);
                images = (XmlElement) doc.SelectSingleNode("/Cache/Forms/Images");
                _formImagesHash = NodeHelper<object>.CreateImageHashFromXmlNode(images, FormTypes);
                XmlElement forceShows = (XmlElement) doc.SelectSingleNode("/Cache/Forms/ForceShows");
                _formForceShowHash = NodeHelper<object>.CreateBoolHashFromXmlNode(forceShows, FormTypes);
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// ��������/�������������� ����� cache.xml, ���������� ��� �������.
        /// </summary>
        public static void SaveCache()
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("Cache"));
            XmlElement dlls = doc.CreateElement("Dlls");
            for (int i = 0; i < DllNames.Count; i++)
            {
                XmlElement dll = doc.CreateElement("Name");
                dll.InnerText = DllNames[i];
                dlls.AppendChild(dll);
            }
            doc.DocumentElement.AppendChild(dlls);

            List<string> namesList = GetNodeNames();
            List<Image> imagesList = GetNodeImages();

            XmlElement nodes = doc.CreateElement("Nodes");
            nodes.AppendChild(NodeHelper<string>.CreateXmlNodeFromList(doc, namesList, "Names", "Name"));
            nodes.AppendChild(NodeHelper<Image>.CreateXmlNodeFromList(doc, imagesList, "Images", "Image"));

            namesList = GetDeviceNames();
            imagesList = GetDeviceImages();
            XmlElement devices = doc.CreateElement("Devices");
            devices.AppendChild(NodeHelper<string>.CreateXmlNodeFromList(doc, namesList, "Names", "Name"));
            devices.AppendChild(NodeHelper<Image>.CreateXmlNodeFromList(doc, imagesList, "Images", "Image"));

            namesList = GetFormNames();
            imagesList = GetFormImages();
            List<bool> forceShowList = GetFormForceShow();
            XmlElement forms = doc.CreateElement("Forms");
            forms.AppendChild(NodeHelper<string>.CreateXmlNodeFromList(doc, namesList, "Names", "Name"));
            forms.AppendChild(NodeHelper<Image>.CreateXmlNodeFromList(doc, imagesList, "Images", "Image"));
            forms.AppendChild(NodeHelper<bool>.CreateXmlNodeFromList(doc, forceShowList, "ForceShows", "ForceShow"));

            doc.DocumentElement.AppendChild(nodes);
            doc.DocumentElement.AppendChild(devices);
            doc.DocumentElement.AppendChild(forms);

            doc.Save("cache.xml");
        }

        // ����������� ��� ������
        public static Dictionary<Type, string> FormNameHash => _formNamesHash;
        public static Dictionary<Type, bool> FormForceShowHash => _formForceShowHash;
        public static Dictionary<Type, string> DeviceNameHash => _deviceNamesHash;
        public static Dictionary<Type, Image> DeviceImageHash => _deviceImagesHash;

        /// <summary>
        /// ���������, �������� �� �������� ��� ����������� �������� ����.
        /// </summary>
        public static bool IsInheritedFrom(Type childType, Type baseType)
        {
            bool ret;

            if (childType == typeof (object) || null == childType.BaseType)
            {
                ret = false;
            }
            else if (childType.BaseType.ToString() == baseType.ToString())
            {
                ret = true;
            }
            else
            {
                ret = IsInheritedFrom(childType.BaseType, baseType);
            }
            return ret;
        }

        /// <summary>
        /// ����������, �������� �� ��������� ��� ����������� ���������� INodeView.
        /// ������: public class MyForm : INodeView - true.
        /// </summary>
        public static bool IsCorrectDeviceType(Type curType)
        {
            bool ret = false;

            if (IsInheritedFrom(curType, typeof (Device)))
            {
                if (typeof (INodeView).IsAssignableFrom(curType))
                {
                    ret = true;
                }
            }
            return ret;
        }

        /// <summary>
        /// ����������, �������� �� ��������� ��� ����������� ���������� IFormView.
        /// ������: public class MyForm : Form, IFormView - true.
        /// </summary>
        public static bool IsCorrectFormType(Type curType)
        {
            return typeof (IFormView).IsAssignableFrom(curType);
        }

        /// <summary>
        /// ����������, �������� �� ��������� ��� ����������� ���������� INodeView.
        /// �������: public interface IFormView : INodeView
        ///          public interface IDeviceView : INodeView
        ///          public class MyClass : INodeView 
        ///          public class MyClass : IDeviceView
        /// </summary>
        public static bool IsNodeType(Type curType)
        {
            return typeof (INodeView).IsAssignableFrom(curType);
        }

        /// <summary>
        /// �������� ���� ����, ������������� ���� ����������
        /// </summary>

        public static Type[] GetFormTypes(Type deviceType)
        {
            return _formTypeHash[deviceType].ToArray();
        }

        /// <summary>
        /// �������, ��� Key - �������� ���� (����������), Value - �������� ����.
        /// </summary>
        private static Dictionary<Type, Image> _deviceImagesHash = new Dictionary<Type, Image>();
        /// <summary>
        /// �������, ��� Key - �������� ���� (����������), Value - ��� ���� ����������, ��-�� (NodeName).
        /// </summary>
        private static Dictionary<Type, string> _deviceNamesHash = new Dictionary<Type, string>();
        /// <summary>
        /// �������, ��� Key - �������� ��� �������� ����, Value - �������� ����.
        /// </summary>
        private static Dictionary<Type, Image> _nodeImagesHash = new Dictionary<Type, Image>();
        /// <summary>
        /// �������, ��� Key - �������� ��� �������� ����, Value - ��� ���� ���������� ��� ��� ����, ��-�� (NodeName).
        /// </summary>
        private static Dictionary<Type, string> _nodeNamesHash = new Dictionary<Type, string>();
        /// <summary>
        /// �������, ��� Key - ����������, Value - ������ ����, ����������� � ����������.
        /// </summary>
        private static Dictionary<Type, List<Type>> _formTypeHash = new Dictionary<Type, List<Type>>();
        /// <summary>
        /// �������, ��� Key - �������� ����, Value - ����� �� �������� ���� (�� ��������� false).
        /// </summary>
        private static Dictionary<Type, bool> _formForceShowHash = new Dictionary<Type, bool>();
        /// <summary>
        /// �������, ��� Key - �������� ����, Value - �������� ���� �����.
        /// </summary>
        private static Dictionary<Type, Image> _formImagesHash = new Dictionary<Type, Image>();
        /// <summary>
        /// �������, ��� Key - �������� ����, Value - ��� ���� �����, ��-�� (NodeName).
        /// </summary>
        private static Dictionary<Type, string> _formNamesHash = new Dictionary<Type, string>();
        /// <summary>
        /// ������� ������ �� �������� NodeTypes. 
        /// </summary>
        public static void GetDataFromDll()
        {
            _deviceNamesHash.Clear();
            _deviceImagesHash.Clear();
            _formForceShowHash.Clear();
            _formImagesHash.Clear();
            _formNamesHash.Clear();
            _nodeImagesHash.Clear();
            _nodeNamesHash.Clear();
            foreach (Type nodeType in NodeTypes)
            {
                object nodeObject = Activator.CreateInstance(nodeType); // ������ ��������� ������� (nodeType)
                INodeView nodeView = (INodeView) nodeObject; // �������� � ���� INodeView, ����� �������� ������ � ��� ���������
                _nodeImagesHash.Add(nodeType, nodeView.NodeImage); // ��������� �������� ��� �������� ���� � ��� ��������
                _nodeNamesHash.Add(nodeType, nodeView.NodeName); // ��������� �������� ��� �������� ���� � ��� ���

                if (IsCorrectDeviceType(nodeType)) // ���� ��� ����� ����������
                {
                    _deviceNamesHash.Add(nodeType, nodeView.NodeName); // ��������� �������� ���� (����������) � ��� ����
                    _deviceImagesHash.Add(nodeType, nodeView.NodeImage); // ��������� �������� ���� (����������) � ��������������� ��� ��������
                }
                if (IsCorrectFormType(nodeType)) // ���� ��� ����� �����, ����������� ��������� IFormView
                {
                    IFormView formView = (IFormView) nodeObject;  // �������� � ���� IFormView, ����� �������� ������ � ��� ���������
                    _formNamesHash.Add(nodeType, formView.NodeName); // ��������� �������� ���� (�����) � � ���
                    _formImagesHash.Add(nodeType, formView.NodeImage); // ��������� �������� ���� (�����) � � ��������
                    _formForceShowHash.Add(nodeType, formView.ForceShow); // ��������� �������� ���� (�����) � �������� false
                }
            }
        }

        /// <summary>
        /// ������� *.dll ���� ���������. ���������� ����� � ������� ������ DllLoader.cs
        /// </summary>
        public static void ParseTypes()
        {
            string[] files = DllNames.ToArray(); // ������ ���� *.dll � PiconFramework->bin->Debug->Devices.
            _deviceTypes.Clear();
            _formTypes.Clear();
            _moduleTypes.Clear();
            _connectModuleTypes.Clear();

            foreach (string dllName in files)  
            {
                Assembly curAssmbl = Assembly.LoadFrom(dllName); // ������� ������
                try
                {
                    Type[] types = curAssmbl.GetTypes(); // ������ ����� �������� ���������� ������
                    List<Type> formType = new List<Type>(types.Length);
                    Type deviceType = null;
                  
                    // �������: � _deviceTypes - ������ ���� ��������� � �������.
                    //          �  deviceType - ����� �������� ����������.
                    
                    //          � _formTypes - ������ ���� ����, ����������� � �����������.
                    //          �  formType - ������ ���� �������� ����������.
                    
                    //          � _nodeTypes - ������ ���� ����� ������ (���������� � �� �����).
                    // ��������� ���� ������������.
                    
                    foreach (Type curType in types)
                    {
                        if (IsCorrectDeviceType(curType))
                        {
                            _deviceTypes.Add(curType);
                            deviceType = curType;
                        }
                        if (IsCorrectFormType(curType))
                        {
                            _formTypes.Add(curType);
                            formType.Add(curType);
                        }
                        if (IsNodeType(curType))
                        {
                            _nodeTypes.Add(curType);
                        }
                    }
                    if (null != deviceType)
                    {
                        _formTypeHash.Add(deviceType, formType);
                    }
                }
                catch (ArgumentException)
                {
                    throw new TypeNotUniqueException("�������� ���� �����", dllName);
                }
                catch (ReflectionTypeLoadException reflection)
                {
                    var exc = reflection.LoaderExceptions;
                }
            }
        }
    }
}