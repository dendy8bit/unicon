﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using Scheme;

namespace BEMN.Utils
{
    public class BaseSource : BEMN.Utils.Source
    {
        float _scale;
        int _width;
        int _height;

        private Commands.Creator _DelCreate;

        public BaseSource()
        {
            Scale = 1;
            _objList = new VisualLayout(600, 600);
            //_DelCreate = Globals.C
        }

        public float Scale{
            get
            {
                return _scale;
            }
            set
            {
                if (value > 0.05 && value < 20) _scale = value;                
            }
        }

        iVisualObject _objList;

        public iVisualObject ObjList
        {
            get
            {
                return _objList;
            }
            set
            {
                if (value != null) _objList = value;
            }
        }

        public int Width
        {
            get
            {
                return _width;
            }
            set
            {
                if (value > 1) _width = value;
                (_objList as VisualLayout)._width = _width;
            }
        }

        public int Height
        {
            get
            {
                return _height;
            }
            set
            {
                if (value > 1) _height = value;
                (_objList as VisualLayout)._height = _height;
            }
        }

        public void UpdateValue()
        {            
            StringWriter _strWriter = new StringWriter();
            XmlTextWriter w = new XmlTextWriter(_strWriter);
            w.WriteStartDocument();
            w.WriteStartElement("Layout");

            Iterator it = new MovablesIterator(Globals.StaticList);
            it.First();
            while (!it.IsDone())
            {
                it.GetItem().SaveToXML(w);
                it.Next();
            }

            it = new LineIterator(Globals.StaticList);
            it.First();
            it.Next();
            while (!it.IsDone())
            {
                it.GetItem().SaveToXML(w);
                it.Next();
            }
            w.WriteEndElement();
            w.WriteEndDocument();
            w.Close();

            Value = _strWriter.ToString();
        }

        public void UpdateGraph()
        {
            int points;
            int lines;
            int i;
            StringReader _strReader = new StringReader(Value);
            XmlTextReader reader = new XmlTextReader(_strReader);
            reader.Read();

            while (reader.Name != "Layout")
            {
                reader.Read();
            }

            lines = Convert.ToInt32(reader.GetAttribute("lines"));
            points = Convert.ToInt32(reader.GetAttribute("points"));

            if (!(points == 0 && lines == 0))
            {
                Iterator it = new PreorderIterator(Globals.StaticList);
                it.First();

                reader.Read();

                for (i = 0; i < points; i++)
                {
                    it.Insert(_DelCreate(reader));
                }

                for (i = 0; i < lines; i++)
                {
                    it.Insert(_DelCreate(reader));
                }
            }
            reader.Close();
        }
    }
}
