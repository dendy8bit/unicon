﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace Oscilloscope.View
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static bool DEBUG => false;

        public static string BasePath { get; private set; } // путь с OpenFileDialog.FileName  (C:\Users\Melnichenko\Desktop\Журналы, уставки, осциллограммы\Осциллограмма МР 5 в50.hdr) - пример

        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            try
            {
                if (DEBUG)
                {
                    OpenFileDialog op = new OpenFileDialog();
                    if (op.ShowDialog().GetValueOrDefault())
                    {
                        BasePath = op.FileName;
                    }
                }
                else
                {
                    string res = e.Args.Aggregate(string.Empty, (current, arg) => current + " " + arg);
                    BasePath = res;
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            if (BasePath == null)
            {
                Environment.Exit(0);
            }
        }


        private void App_OnExit(object sender, ExitEventArgs e)
        {
            if (!string.IsNullOrEmpty(BasePath))
            {
                if (string.Compare(Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "TempOsc"), Path.GetDirectoryName(BasePath), true) == 0)
                {
                    File.Delete(Path.ChangeExtension(BasePath, "hdr"));
                    File.Delete(Path.ChangeExtension(BasePath, "cfg"));
                    File.Delete(Path.ChangeExtension(BasePath, "dat"));
                }
            }
        }
    }
}
