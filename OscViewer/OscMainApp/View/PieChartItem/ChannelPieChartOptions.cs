﻿using System;
using System.ComponentModel;
using System.Windows;

namespace Oscilloscope.View.PieChartItem
{
    [Serializable]
    public class ChannelPieChartOptions : INotifyPropertyChanged
    {
        private AnalogChannel _channel;
        private string _name;
        private bool _a;
        private bool _b;
        private bool _c;
        private bool _n;

        public override string ToString()
        {
            return _name;
        }
        public AnalogChannel Channel
        {
            get { return _channel; }
        }

        public ChannelPieChartOptions(string name)
        {
            _name = name;
        }

        public ChannelPieChartOptions(AnalogChannel channel)
        {
            _channel = channel;
            this._name = channel.Name;
        }

        public Point[] Values { get { return Channel.FirstHarmonic; } }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;

            }
        }

        public bool A
        {
            get { return _a; }
            set
            {
                _a = value;
                if (value)
                {
                    B = false;
                    C = false;
                    OnAisTrue(this);
                }
                RaisePropertyChanged("A");
            }
        }

        public bool B
        {
            get { return _b; }
            set
            {
                _b = value;
                if (value)
                {
                    A = false;
                    C = false;
                    OnBisTrue(this);
                }
                RaisePropertyChanged("B");
            }
        }

        public bool C
        {
            get { return _c; }
            set
            {
                _c = value;
                if (value)
                {
                    B = false;
                    A = false;
                    OnCisTrue(this);
                }
                RaisePropertyChanged("C");
            }
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        public event Action<ChannelPieChartOptions> AisTrue;
        public event Action<ChannelPieChartOptions> BisTrue;
        public event Action<ChannelPieChartOptions> CisTrue;

        protected virtual void OnBisTrue(ChannelPieChartOptions obj)
        {
            BisTrue?.Invoke(obj);
        }

        protected virtual void OnCisTrue(ChannelPieChartOptions obj)
        {
            CisTrue?.Invoke(obj);
        }
        
        protected virtual void OnAisTrue(ChannelPieChartOptions obj)
        {
            AisTrue?.Invoke(obj);
        }

        private void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}