﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using Oscilloscope.View.MainItem;
using Oscilloscope.View.PieChartItem.Characteristics;

namespace Oscilloscope.View.PieChartItem
{
    /// <summary>
    /// Логика взаимодействия для PieChart.xaml
    /// </summary>
    public partial class PieChart
    {
        private static readonly Pen BlackPen = new Pen(Brushes.Black, 0.5);
 
        private PieChartOptions _options;

        private List<PieChannelInfo> _infos = new List<PieChannelInfo>();
        private readonly object _syns = new object();
        private double _factor;
        private Point _realMid;
        private int _index;
        private int _zoom = 1;
        private Size _size;

        static PieChart()
        {
            BlackPen.Freeze();
        }

        private WorkplaceControl _currentWorkplace;
        public WorkplaceControl CurrentWorkplace
        {
            get { return this._currentWorkplace; }
            set
            {
                this._currentWorkplace = value;
                this.Options = this._currentWorkplace.CurrentPieChartOptions;
            }
        }

        private List<MenuItem> _menuItems = new List<MenuItem>(); 

        private PieChartOptions Options
        {
            get { return this._options; }
            set
            {
                this._options = value;
                this._infos = this._options.Infos;
                this._options.VisiblyOptions.NeedRedraw += this.Redraw;

                foreach (MenuItem menuItem in this._menuItems)
                {
                    this.ChannelsMenuRoot.Items.Remove(menuItem);
                }

                foreach (PieChannelInfo info in this._infos)
                {
                    this._menuItems.Add(info.ChannelMenuItem);
                    this.ChannelsMenuRoot.Items.Add(info.ChannelMenuItem);
                }

                this.Marker.Lenght = this._options.Lenght-1;
                this.InfoGrid.ItemsSource = this._infos.Where(o => o.IsVisibly).ToArray();
                this.Redraw();
                this._currentWorkplace.AddedChannels = this.Options.AddedChannel.ToArray();
            }
        }


        private void Redraw()
        {

            if (this.MainGrid.ActualHeight - this.InfoGrid.ActualHeight > this.MainGrid.ActualWidth - this.InfoGrid.ActualWidth)
            {
                Grid.SetRow(this.InfoGrid, 2);
                Grid.SetColumn(this.InfoGrid, 0);
                this._size = new Size(this.MainGrid.ActualWidth, this.MainGrid.ActualHeight - this.InfoGrid.ActualHeight - this.MarkerRow.ActualHeight);
            }
            else
            {
                Grid.SetRow(this.InfoGrid, 1);
                Grid.SetColumn(this.InfoGrid, 1);
                this._size = new Size(this.MainGrid.ActualWidth - this.InfoGrid.ActualWidth, this.MainGrid.ActualHeight - this.MarkerRow.ActualHeight);
            }
            if (this.Options==null)
            {
                this.Marker.Visibility = Visibility.Hidden;
            }
            if ((this._size.Height < 1.0) || (this._size.Width < 1.0))
            {
                return;
            }
            Task<RenderTargetBitmap> task = new Task<RenderTargetBitmap>(this.Rend);
            task.ContinueWith(
                o =>
                {
                    Dispatcher.Invoke(new Action(() =>
                    {
                        this.image.Source = o.Result;
                    }));
                });
            task.Start();
        }

        public void SetGraph(int obj)
        {
            if (this.Options == null)
            {
                return;
            }
            IEnumerable<PieChannelInfo> curInfos = this._infos.Where(o => o.IsVisibly);
            this.InfoGrid.ItemsSource = curInfos.ToArray();
            this._index = obj;
            foreach (PieChannelInfo pieChannelInfo in curInfos)
            {
                pieChannelInfo.SetIndex(obj);
            }

            this.Marker.Value = obj;
            this.Redraw();
        }

        private void InfoGrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.Redraw();
        }

        public PieChart()
        {
            this.InitializeComponent();
        }

        public Action<int> MoveMarker; 
        private void Marker_Move(int obj)
        {
            if (this.MoveMarker != null)
            {
                this.MoveMarker.Invoke(obj);
            }
        }

        /// <summary>
        /// Меню - Настройка
        /// </summary>
        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            if (this.Options == null)
            {
                MessageBox.Show("Осциллограмма не загружена");
                return;
            }
            PieChartOptionsForm optionsForm = new PieChartOptionsForm();
            optionsForm.Marker1 = this.CurrentWorkplace.MarkersBar.Markers.Marker1;
            optionsForm.Marker2 = this.CurrentWorkplace.MarkersBar.Markers.Marker2;
            optionsForm.ChartOptions = this.Options;

            if (optionsForm.ShowDialog().GetValueOrDefault())
            {
                this.Options = optionsForm.ChartOptions;
            }
            this.Redraw();
        }

        private void UserControl_SizeChanged_1(object sender, SizeChangedEventArgs e)
        {
            this.Redraw();
        }


        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter="Файл параметров круговой диаграммы|*.pco";

            if (dialog.ShowDialog().Value)
            {
                this.Options.Save(dialog.FileName);
            }
        }

        private void ShowAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (PieChannelInfo info in this._infos)
            {
                info.IsVisibly = true;
            }
        }

        private void HideAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (PieChannelInfo info in this._infos)
            {
                info.IsVisibly = false;
            }
        }
        
        private bool _move;
        private Point _startPosition;
        private Point _offset = new Point(0,0);
        private Point _midTrans = new Point(0, 0);
        

        private void image_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this._move = true;
            Point temp = e.GetPosition(this.image);
            temp.Offset(-this._midTrans.X, -this._midTrans.Y);
            this._startPosition = temp;

        }

        private void image_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this._move = false;
        }

        private void image_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Point newPosition = e.GetPosition(this.image);
            if ((this._zoom > 1)&&(this._move))
            {
               newPosition.Offset(-this._startPosition.X,-this._startPosition.Y);
                this._midTrans = newPosition;
                this.Redraw();
            }
        }


        private void image_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {

            if ((e.Delta < 0) && (this._zoom > 1))
            {
                Point temp = e.GetPosition(this.image);
                Point off = new Point((this._realMid.X - temp.X), (this._realMid.Y - temp.Y));
                this._offset = new Point(this._offset.X - (off.X /this._zoom), this._offset.Y - (off.Y /this._zoom));
                this._zoom--;
            }
            if (e.Delta > 0)
            {
               Point temp = e.GetPosition(this.image);
                Point off = new Point((this._realMid.X - temp.X), (this._realMid.Y - temp.Y));
                this._offset = new Point(this._offset.X +(off.X /this._zoom)  , this._offset.Y + (off.Y /this._zoom));
                this._zoom++;
                }
            if (this._zoom == 1)
            {
                this._offset = new Point(0, 0);
                this._midTrans=new Point(0,0);
            }

            this.Redraw();
        }


        private RenderTargetBitmap Rend()
        {
            lock (this._syns)
            {
                DrawingVisual drawingVisual = new DrawingVisual();
                const int border = 20;
                int radius = (int)((Math.Min(this._size.Width, this._size.Height) - border) / 2) *this._zoom;

                this._realMid = new Point(this._size.Width / 2, this._size.Height / 2);

                this._realMid.Offset(this._offset.X, this._offset.Y);

                this._realMid.Offset(this._midTrans.X, this._midTrans.Y);

                using (DrawingContext drawingContext = drawingVisual.RenderOpen())
                {
                    //Заливка цветом
                    drawingContext.DrawRectangle(Brushes.White, null,
                                                 new Rect(0, 0, this._size.Width, this._size.Height));
                    drawingContext.DrawLine(BlackPen, new Point(this._realMid.X - radius, this._realMid.Y), new Point(this._realMid.X + radius, this._realMid.Y));
                    drawingContext.DrawLine(BlackPen, new Point(this._realMid.X, this._realMid.Y - radius), new Point(this._realMid.X, this._realMid.Y + radius));

                    FormattedText formattedText = new FormattedText(
                        "-R",
                        CultureInfo.GetCultureInfo("en-us"),
                        FlowDirection.LeftToRight,
                        new Typeface("Verdana"),
                        12,
                        Brushes.Black);
                    drawingContext.DrawText(formattedText, new Point(this._realMid.X - radius - border / 2.0, this._realMid.Y - formattedText.Height));

                    formattedText = new FormattedText(
                        "R, Ом",
                        CultureInfo.GetCultureInfo("en-us"),
                        FlowDirection.RightToLeft,
                        new Typeface("Verdana"),
                        12,
                        Brushes.Black);
                    drawingContext.DrawText(formattedText, new Point(border / 2.0 + radius + this._realMid.X, this._realMid.Y - formattedText.Height));

                    formattedText = new FormattedText(
                        "X, Ом",
                        CultureInfo.GetCultureInfo("en-us"),
                        FlowDirection.LeftToRight,
                        new Typeface("Verdana"),
                        12,
                        Brushes.Black);
                    drawingContext.DrawText(formattedText, new Point(this._realMid.X+10, this._realMid.Y - radius));

                    formattedText = new FormattedText(
                        "-X",
                        CultureInfo.GetCultureInfo("en-us"),
                        FlowDirection.LeftToRight,
                        new Typeface("Verdana"),
                        12,
                        Brushes.Black);
                    drawingContext.DrawText(formattedText, new Point(this._realMid.X+10, radius + this._realMid.Y - formattedText.Height));

                    if (this.Options!= null && this.Options.IsCorrect)
                    {
                        IEnumerable<Point> charasteristicsMaxPoints = this.Options.CharacteristicsVisibly.Select(o => o.MaxPoint);
                        IEnumerable<Point> result = charasteristicsMaxPoints;
                        foreach (PieChannelInfo info in this._infos.Where(i=>i.IsVisibly))
                        {
                            result = result.Concat(info.Values.Where((e, index) => index >= this._options.StartTime && index < this._options.EndTime));
                        }
                        double max = 0.0;
                        if (result.Count() > 0)
                        {
                            max = result.Max(o => Math.Max(Math.Abs(o.X), Math.Abs(o.Y)));
                            
                            double midValue = AnalogChannel.GetMiddleValue(max);

                            this._factor = radius/max;
                            double temp = 0;
                            double addedKoef = 1/(double) this._zoom;
                            while (temp <= 2 - addedKoef)
                            {
                                temp += addedKoef;
                                double resultPOint = midValue*temp;
                                if (resultPOint < 10)
                                {
                                    resultPOint = Math.Round(resultPOint, 2);
                                }
                                else
                                {
                                    if (resultPOint < 100)
                                    {
                                        resultPOint = Math.Round(resultPOint, 1);
                                    }
                                    else
                                    {
                                        resultPOint = Math.Round(resultPOint);
                                    }
                                }
                                this.DrawAxis(drawingContext, resultPOint);
                            }



                            foreach (PieChannelInfo info in this._infos)
                            {
                                if (info.IsVisibly)
                                {
                                    Point[] points =
                                        info.Values.Select(
                                            o => new Point(o.X*this._factor + this._realMid.X, this._realMid.Y - o.Y*this._factor))
                                            .ToArray();
                                    for (int i = this._options.StartTime; i < this._options.EndTime - 1; i++)
                                    {
                                        if (i == this._index)
                                        {
                                            this.DrawPointer(drawingContext, points[i], info.ChannelPen);
                                        }
                                        if (((int) points[i].X == (int) points[i + 1].X) &&
                                            ((int) points[i].Y == (int) points[i + 1].Y))
                                        {
                                            continue;
                                        }
                                        drawingContext.DrawLine(info.ChannelPen, points[i], points[i + 1]);


                                    }
                                }
                            }

                            foreach (ICharacteristic characteristic in this.Options.CharacteristicsVisibly)
                            {
                                characteristic.Draw(drawingContext, this._realMid, this._factor, radius);
                            }
                        }
                    }
                }

                RenderTargetBitmap bmp = new RenderTargetBitmap((int) this._size.Width, (int) this._size.Height, 0, 0,
                                                                PixelFormats.Pbgra32);
                bmp.Render(drawingVisual);

                bmp.Freeze();
                return bmp;
            }
        }
        private void DrawPointer(DrawingContext drawingContext, Point point, Pen pen)
        {
            const int r = 2;
            const int s = 2;
            const int l = 4;
            drawingContext.DrawEllipse(pen.Brush, BlackPen, point, r, r);
            drawingContext.DrawLine(pen, new Point(point.X, point.Y - s-r), new Point(point.X, point.Y - s-l-r));//1
            drawingContext.DrawLine(pen, new Point(point.X - s - r, point.Y), new Point(point.X - s - l - r, point.Y));//2
            drawingContext.DrawLine(pen, new Point(point.X, point.Y + s+r), new Point(point.X, point.Y + s + l+r));//3
            drawingContext.DrawLine(pen, new Point(point.X + s + r, point.Y), new Point(point.X + s + l + r, point.Y));//4
        }

        private void DrawAxis(DrawingContext drawingContext, double midValue)
        {
            FormattedText formattedText = new FormattedText(
                midValue.ToString(CultureInfo.InvariantCulture),
                CultureInfo.GetCultureInfo("en-us"),
                FlowDirection.LeftToRight,
                new Typeface("Verdana"),
                12,
                Brushes.Black);

            drawingContext.DrawText(formattedText,
                                    new Point(this._realMid.X + midValue*this._factor - formattedText.Width/2, this._realMid.Y + 5));
            drawingContext.DrawLine(BlackPen, new Point(this._realMid.X + midValue*this._factor, this._realMid.Y + 5),
                                    new Point(this._realMid.X + midValue*this._factor, this._realMid.Y - 5));



            drawingContext.DrawText(formattedText,
                                    new Point(this._realMid.X - 5 - formattedText.Width, this._realMid.Y - midValue*this._factor - formattedText.Height/2));
            drawingContext.DrawLine(BlackPen, new Point(this._realMid.X + 5, this._realMid.Y - midValue*this._factor),
                                    new Point(this._realMid.X - 5, this._realMid.Y - midValue*this._factor));


            formattedText = new FormattedText(
                (-midValue).ToString(CultureInfo.InvariantCulture),
                CultureInfo.GetCultureInfo("en-us"),
                FlowDirection.LeftToRight,
                new Typeface("Verdana"),
                12,
                Brushes.Black);

            drawingContext.DrawText(formattedText,
                                    new Point(this._realMid.X - midValue*this._factor - formattedText.Width/2, this._realMid.Y + 5));
            drawingContext.DrawLine(BlackPen, new Point(this._realMid.X - midValue*this._factor, this._realMid.Y + 5),
                                    new Point(this._realMid.X - midValue*this._factor, this._realMid.Y - 5));

            drawingContext.DrawText(formattedText,
                                    new Point(this._realMid.X - 5 - formattedText.Width, this._realMid.Y + midValue*this._factor - formattedText.Height/2));
            drawingContext.DrawLine(BlackPen, new Point(this._realMid.X + 5, this._realMid.Y + midValue*this._factor),
                                    new Point(this._realMid.X - 5, this._realMid.Y + midValue*this._factor));
        }


        private void MainGrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.Redraw();
        }
        /// <summary>
        /// Включение маркера
        /// </summary>
        private void MarkerMenuItem_Checked(object sender, RoutedEventArgs e)
        {
            this.Marker.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Отключение маркера
        /// </summary>
        private void MarkerMenuItem_Unchecked(object sender, RoutedEventArgs e)
        {
            this.Marker.Visibility = Visibility.Hidden;

        }
        /// <summary>
        /// Оптимизация масштаба
        /// </summary>
        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            this._zoom = 1;
            this._offset = new Point(0, 0);
            this._midTrans = new Point(0, 0);
            this.Redraw();
        }

        private void MenuItem_Click_6(object sender, RoutedEventArgs er)
        {
            this._zoom = 1;
            this._offset = new Point(0, 0);
            this._midTrans = new Point(0, 0);

            double cMax = 0.0;
            try
            {
                cMax = this.Options.CharacteristicsVisibly.Select(o => Math.Max(o.MaxPoint.X, o.MaxPoint.Y)).Max();
            }
            catch (Exception)
            {
                
         
            }
              

            IEnumerable<Point> charasteristicsMaxPoints = this.Options.CharacteristicsVisibly.Select(o => o.MaxPoint);
            IEnumerable<Point> result = charasteristicsMaxPoints;
            foreach (PieChannelInfo info in this._infos)
            {
                if (info.IsVisibly)
                {
                    result = result.Concat(info.Values.Where((e, index) => index >= this._options.StartTime && index < this._options.EndTime));
                }

            }
            double max = result.Max(o => Math.Max(Math.Abs(o.X), Math.Abs(o.Y)));
            if (max>cMax)
            {
                this._zoom = (int) Math.Ceiling(max/cMax);
            }
            this.Redraw();
        }
    }
}
