﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace LightSensor
{
    public partial class InOutLightForm : Form, IFormView
    {
        private LightSensorDevice _device;

        public InOutLightForm()
        {
            this.InitializeComponent();
        }

        public InOutLightForm(LightSensorDevice device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.Lux.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.LuxReadOk);
            this._device.Lux.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._statusLabel.Text = "Невозможно прочитать значение";
            });
        }

        private void LuxReadOk()
        {
            this._statusLabel.Text = "Идет измерение";
            this._luxBox.Text = this._device.Lux.Value.Word.ToString();
        }

        private void InOutLightForm_Load(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.Lux.LoadStructCycle();
            }
        }

        private void InOutLightForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.Lux.RemoveStructQueries();
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(LightSensorDevice); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(InOutLightForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Измерения"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}
