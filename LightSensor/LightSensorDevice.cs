﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Interfaces;
using BEMN.MBServer;
using LightSensor.Structures;

namespace LightSensor
{
    public class LightSensorDevice : Device, IDeviceView, IDeviceVersion
    {
        private MemoryEntity<OneWordStruct> _lux;
        private MemoryEntity<VersionDevice> _version;
        private MemoryEntity<UsartConfig> _configRs485;
        private MemoryEntity<ConfigLightSensorStruct> _configLightSensor; 

        public LightSensorDevice()
        {
            HaveVersion = true;
        }

        public LightSensorDevice(Modbus mb) : this(mb, false)
        {
            HaveVersion = true;
        }

        public LightSensorDevice(Modbus mb, bool log)
        {
            this.MB = mb;
            this.MB.Logging = log;
            this.InitMemories();
        }

        private void InitMemories()
        {
            this._lux = new MemoryEntity<OneWordStruct>("Измерение освещенности", this, 0x0000);
            this._configRs485 = new MemoryEntity<UsartConfig>("RS-485", this, 0x1000);
            this._version = new MemoryEntity<VersionDevice>("Версия прошивки, устройства и загрузчика", this, 0x1F00);
            this._configLightSensor = new MemoryEntity<ConfigLightSensorStruct>("Конфигурация датчика освещения", this, 0x1008);
        }

        #region Properties

        public MemoryEntity<OneWordStruct> Lux
        {
            get { return this._lux; }
        } 

        public MemoryEntity<VersionDevice> Version
        {
            get { return this._version; }
        }

        public MemoryEntity<UsartConfig> ConfigRs485
        {
            get { return this._configRs485; }
        }

        public MemoryEntity<ConfigLightSensorStruct> ConfigLightSensor
        {
            get { return this._configLightSensor; }
        }

        #endregion

        #region Read version
        public override void LoadVersion(object deviceObj)
        {
            System.Threading.Thread.Sleep(500);
            LoadSlot(DeviceNumber, new slot(0x1F00, 0x1F08), "version" + DeviceNumber, this);
            this._version.LoadStruct();
        }
        #endregion

        #region IDeviceVersion Members
        public Type[] Forms
        {
            get
            {
                return new[]
                {
                    typeof (ConfigurationForm),
                    typeof (InOutLightForm)
                };
            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                {
                    "1.1",
                    "1.2"
                };
            }
        }
        #endregion

        #region INodeView Members

        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(LightSensorDevice); }
        }

        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [Browsable(false)]
        public Image NodeImage
        {
            get { return Properties.Resources.mr100.ToBitmap(); }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "Датчик освещения"; }
        }

        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion
    }
}
