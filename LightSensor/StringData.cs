﻿using System.Collections.Generic;

namespace LightSensor
{
    public class StringData
    {
        public static double DevVersion = 0;

        public static List<string> Speeds
        {
            get
            {
                return new List<string>(new string[]
                {
                    "600",
                    "1200",
                    "2400",
                    "4800",
                    "9600",
                    "19200",
                    "38400",
                    "76800",
                    "900",
                    "1800",
                    "3600",
                    "7200",
                    "14400",
                    "28800",
                    "57600",
                    "115200"
                });
            }
        }

        public static List<string> DataBits
        {
            get
            {
                return new List<string>(new string[]
                {
                    "5 бит",
                    "6 бит",
                    "7 бит",
                    "8 бит",
                    "9 бит"
                });
            }
        }

        public static List<string> StopBits
        {
            get
            {
                return new List<string>(new string[]
                {
                    "1 бит",
                    "2 бита"
                });
            }
        }

        public static List<string> Paritet
        {
            get
            {
                return new List<string>(new string[]
                {
                    "Нет",
                    "Есть"
                });
            }
        }

        public static List<string> ParitetChet
        {
            get
            {
                return new List<string>(new string[]
                {
                    "Четный",
                    "Нечетный"
                });
            }
        }
    }
}
