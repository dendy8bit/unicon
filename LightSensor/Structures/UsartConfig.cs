﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace LightSensor.Structures
{
    public class UsartConfig : StructBase
    {
        #region Private fields
        /*   15 _ _ _ _ _ _8|7  _ _ _ _ _ _  0      Init bits
             |_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|
               | | | | | | | ||| | | | | | | |______00  скорость передачи фикс. разр.0|	(от 0 до 15)
               | | | | | | | ||| | | | | | |________01	скорость передачи фикс. разр.1|	0-600, 1-1200, 2-2400, 3-4800, 4-9600, 5-19200,
               | | | | | | | ||| | | | | |__________02	скорость передачи фикс. разр.2|	6-38400, 7-76800, 8-900, 9-1800, 10-3600, 11-7200,
               | | | | | | | ||| | | | |____________03	скорость передачи фикс. разр.3|	12-14400, 13-28800, 14-57600, 15-115200
               | | | | | | | ||| | | |______________04	количество бит в байте разр.0 |	0 - 5 бит, 1 - 6 бит
               | | | | | | | ||| | |________________05	количество бит в байте разр.1 |	2 - 7 бит, 3 - 8 бит
               | | | | | | | ||| |__________________06	количество бит в байте разр.2 |	7 - 9 бит
               | | | | | | | |||____________________07	количество стоп бит 		  |	0 - 1 бит, 1 - 2 бита
               |_|_|_|_|_|_|_|_ _ _ _ _ _ _ _ _ _ _
               | | | | | | | |______________________08  паритет разр.0					|	0 - чётный, 1 - нечётный
               | | | | | | |________________________09	паритет разр.1					|	0 - паритета нет, 1 - паритет есть
               | | | | | |__________________________10	удвоение скорости передачи		|	0 - без удвоения, 1 - с удвоением
               | | | | |____________________________11	режим передачи 					|	0 - асинхронный, 1 - синхронный
               | | | |______________________________12	полярность синхронизации		|	0 - по возрастанию, 1 - по убыванию
               | | |________________________________13	ведомый, ведущий				|	0 - ведомый, 1 - ведущий опрос
               | |__________________________________14	интерфейс						|	0 - USART, 1 - SPI
               |____________________________________15	режим работы					|	0 - обычный, 1 - мультипроцессорный
        */
        [Layout(0)] private ushort _init;
        [Layout(1)] private ushort _reserve;
        [Layout(2)] private ushort _address;	//	адрес устройства
        [Layout(3)] private ushort _timeAuts;   //	таймауты - ст.байт до отправки, мл. байт после
        #endregion

        #region Properties

        [BindingProperty(0)]
        public string Speed
        {
            get
            {
                return Validator.Get(this._init, StringData.Speeds, 0, 1, 2, 3);
            }
            set
            {
                this._init = Validator.Set(value, StringData.Speeds, this._init, 0, 1, 2, 3);
            }
        }

        [BindingProperty(1)]
        public string DataBits
        {
            get
            {
                return Validator.Get(this._init, StringData.DataBits, 4, 5, 6);
            }
            set
            {
                this._init = Validator.Set(value, StringData.DataBits, this._init, 4, 5, 6);
            }
        }

        [BindingProperty(2)]
        public string StopBits
        {
            get
            {
                return Validator.Get(this._init, StringData.StopBits, 7);
            }
            set
            {
                this._init = Validator.Set(value, StringData.StopBits, this._init, 7);
            }
        }

        [BindingProperty(3)]
        public string ParitetChet
        {
            get
            {
                return Validator.Get(this._init, StringData.ParitetChet, 8);
            }
            set
            {
                this._init = Validator.Set(value, StringData.ParitetChet, this._init, 8);
            }
        }

        [BindingProperty(4)]
        public string ParitetOnOff
        {
            get
            {
                return Validator.Get(this._init, StringData.Paritet, 9);
            }
            set
            {
                this._init = Validator.Set(value, StringData.Paritet, this._init, 9);
            }
        }

        [BindingProperty(5)]
        public byte Address
        {
            get
            {
                return Common.LOBYTE(this._address);
            }
            set
            {
                byte hiByte = Common.HIBYTE(this._address);
                this._address = Common.TOWORD(hiByte, value);
            }
        }

        [BindingProperty(6)]
        public byte ToSendAfter
        {
            get
            {
                return Common.LOBYTE(this._timeAuts);
            }
            set
            {
                byte hiByte = Common.HIBYTE(this._timeAuts);
                this._timeAuts = Common.TOWORD(hiByte, value);
            }
        }

        [BindingProperty(7)]
        public byte ToSendBefore
        {
            get
            {
                return Common.HIBYTE(this._timeAuts);
            }
            set
            {
                byte loByte = Common.LOBYTE(this._timeAuts);
                this._timeAuts = Common.TOWORD(value, loByte);
            }
        }
        #endregion
    }
}
