﻿using System;
using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace LightSensor.Structures
{
    public class ConfigLightSensorStruct: StructBase
    {
        [Layout(0)] private ushort _tWait;
        [Layout(1)] private ushort _alphaLow;
        [Layout(2)] private ushort _alphaHigh;
        [Layout(3)] private ushort _mtreg;
        [Layout(4)] private ushort _accuracy;

        [BindingProperty(0)] 
        public ushort TWait
        {
            get { return this._tWait; }
            set { this._tWait = value; }
        }

        [BindingProperty(1)]
        public float Alpha
        {
            get
            {
                List<byte> alphaBytes = new List<byte>();
                alphaBytes.AddRange(Common.TOBYTE(this._alphaLow, false));
                alphaBytes.AddRange(Common.TOBYTE(this._alphaHigh, false));
                float alpha = BitConverter.ToSingle(alphaBytes.ToArray(), 0);
                return alpha;
            }
            set
            {
                byte[] alpha = BitConverter.GetBytes(value);
                this._alphaLow = Common.TOWORD(alpha[1], alpha[0]);
                this._alphaHigh = Common.TOWORD(alpha[3], alpha[2]);
            }
        }

        [BindingProperty(2)]
        public ushort MTReg
        {
            get { return this._mtreg; }
            set { this._mtreg = value; }
        }

        [BindingProperty(3)]
        public ushort Accuracy
        {
            get { return this._accuracy; }
            set { this._accuracy = value; }
        }
    }
}
