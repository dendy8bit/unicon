﻿using System;
using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace LightSensor.Structures
{
    public class VersionDevice : StructBase
    { 
        [Layout(0, Count = 8)] private ushort[] _versionInfo;
        
        #region Properties for version of application

        [BindingProperty(0)]
        public string AppName
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 0; i < 8; i++)
                {
                    words.Add(this._versionInfo[i]);
                }
                string appName = this.GetParam(words.ToArray())[0];
                return appName;
            }
        }

        [BindingProperty(1)]
        public string AppPodtip
        {
            get
            {
                try
                {
                    List<ushort> words = new List<ushort>();
                    for (int i = 0; i < 8; i++)
                    {
                        words.Add(this._versionInfo[i]);
                    }
                    string appPodtip = this.GetParam(words.ToArray())[1];
                    return appPodtip;
                }
                catch (Exception)
                {
                    return "SA";
                }
            }
        }

        [BindingProperty(2)]
        public string AppMod
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 0; i < 8; i++)
                {
                    words.Add(this._versionInfo[i]);
                }
                string appMod = this.GetParam(words.ToArray())[2];
                return appMod;
            }
        }

        [BindingProperty(3)]
        public string AppVersion
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 0; i < 8; i++)
                {
                    words.Add(this._versionInfo[i]);
                }
                string appVersion = this.GetParam(words.ToArray())[3];
                return appVersion;
            }
        }
        #endregion
        
        #region Methods
        string[] GetParam(ushort[] words)
        {
            System.Text.Decoder dec = System.Text.Encoding.ASCII.GetDecoder();
            byte[] byteBuf = Common.TOBYTES(words, false);
            char[] charBuf = new char[byteBuf.Length];

            try
            {
                int byteUsed;
                int charUsed;
                bool complete;
                dec.Convert(byteBuf, 0, byteBuf.Length, charBuf, 0, byteBuf.Length, true, out byteUsed, out charUsed,
                    out complete);
            }
            catch (ArgumentNullException)
            {
                throw new ApplicationException("Передан нулевой буфер");
            }

            string[] param = new string(charBuf, 0, 16).Split(new [] {" "},
                StringSplitOptions.RemoveEmptyEntries);
            return param;
        }
        #endregion
    }
}
