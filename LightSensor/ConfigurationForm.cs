﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Byte;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using LightSensor.Structures;

namespace LightSensor
{
    public partial class ConfigurationForm : Form, IFormView
    {
        private LightSensorDevice _device;
        private NewStructValidator<UsartConfig> _configRs485Validator;
        private NewStructValidator<ConfigLightSensorStruct> _configLightSensorValidator;

        public ConfigurationForm()
        {
            this.Multishow = false;
            this.InitializeComponent();
        }

        public ConfigurationForm(LightSensorDevice device)
        {
            this.InitializeComponent();

            this._device = device;
            this.Multishow = false;

            #region RS485

            this._device.ConfigRs485.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRs485Ok);
            this._device.ConfigRs485.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._statusLabel.Text = ConfigurationStringConst.READ_RS485_FAIL;
                this._statusLabel.BackColor = Color.IndianRed;
            });
            this._device.ConfigRs485.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._statusLabel.Text = ConfigurationStringConst.WRITE_RS485_OK;
                this._statusLabel.BackColor = Color.LightGreen;
            });
            this._device.ConfigRs485.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._statusLabel.Text = ConfigurationStringConst.WRITE_RS485_FAIL;
                this._statusLabel.BackColor = Color.IndianRed;
            });

            this._device.ConfigRs485.ReadOk += HandlerHelper.CreateHandler(this, this.PerformStep);
            this._device.ConfigRs485.ReadFail += HandlerHelper.CreateHandler(this, this.PerformStep);
            this._device.ConfigRs485.WriteOk += HandlerHelper.CreateHandler(this, this.PerformStep);
            this._device.ConfigRs485.WriteFail += HandlerHelper.CreateHandler(this, this.PerformStep);
            #endregion

            #region ConfigLightSensor

            this._device.ConfigLightSensor.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigLightSensorReadOk);
            this._device.ConfigLightSensor.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._statusLabel.Text = ConfigurationStringConst.READ_SETPOINT_FAIL;
                this._statusLabel.BackColor = Color.IndianRed;
            });
            this._device.ConfigLightSensor.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._statusLabel.Text = ConfigurationStringConst.WRITE_SETPOINT_OK;
                this._statusLabel.BackColor = Color.LightGreen;
            });
            this._device.ConfigLightSensor.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._statusLabel.Text = ConfigurationStringConst.WRITE_SETPOINT_FAIL;
                this._statusLabel.BackColor = Color.IndianRed;
            });
            this._device.ConfigLightSensor.ReadOk += HandlerHelper.CreateHandler(this, this.PerformStep);
            this._device.ConfigLightSensor.ReadFail += HandlerHelper.CreateHandler(this, this.PerformStep);
            #endregion

            #region Version

            this._device.Version.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadVersionComplite);
            this._device.Version.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._appName.Text = this._appMod.Text = this._appPodtip.Text = this._appRevision.Text = ConfigurationStringConst.NAN;
                this._statusLabel.Text = ConfigurationStringConst.READ_VERSION_FAIL;
                this._statusLabel.BackColor = Color.IndianRed;
            });

            this._device.Version.ReadOk += HandlerHelper.CreateHandler(this, this.PerformStep);
            this._device.Version.ReadFail += HandlerHelper.CreateHandler(this, this.PerformStep);
            #endregion

            this.InitValidators();
        }

        private void InitValidators()
        {
            ToolTip toolTip = new ToolTip();

            this._configRs485Validator = new NewStructValidator<UsartConfig>(
                toolTip,
                new ControlInfoCombo(this._RS485speedsCombo, StringData.Speeds),
                new ControlInfoCombo(this._RS485dataBitsCombo, StringData.DataBits),
                new ControlInfoCombo(this._RS485stopBitsCombo, StringData.StopBits),
                new ControlInfoCombo(this._RS485paritetCHETCombo, StringData.ParitetChet),
                new ControlInfoCombo(this._RS485paritetYNCombo, StringData.Paritet),
                new ControlInfoText(this._RS485AddressTB, new CustomByteRule(1, 247)),
                new ControlInfoText(this._RS485ToSendAfterTB, new CustomByteRule(0, 255)),
                new ControlInfoText(this._RS485ToSendBeforeTB, new CustomByteRule(0, 255))
            );

            this._configLightSensorValidator = new NewStructValidator<ConfigLightSensorStruct>(
                toolTip,
                new ControlInfoText(this._tWait, new CustomUshortRule(0, 500)),
                new ControlInfoText(this._alpha, new CustomFloatRule(0, 1)),
                new ControlInfoText(this._mtreg, new CustomUshortRule(31, 254)),
                new ControlInfoText(this._accuracy, new CustomUshortRule(96, 144))
                );
        }

        private void ConfigForm_Load(object sender, EventArgs e)
        {
            this._configRs485Validator.Reset();
            this._configLightSensorValidator.Reset();
            if (Device.AutoloadConfig)
            {
                this.ReadAll();
            }
        }

        private void _readAllBtnClick(object sender, EventArgs e)
        {
            this.ReadAll();
        }

        private void ReadAll()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._progressBar.Maximum = this._device.ConfigRs485.Slots.Count +
                                   this._device.Version.Slots.Count + this._device.ConfigLightSensor.Slots.Count;
            this._progressBar.Value = 0;
            this._device.ConfigRs485.LoadStruct();
            this._device.ConfigLightSensor.LoadStruct();
            this._device.Version.LoadStruct();
        }

        private void _writeAllBtnClick(object sender, EventArgs e)
        {
            this.WriteAll();
        }

        private void WriteAll()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._progressBar.Maximum = this._device.ConfigRs485.Slots.Count + this._device.ConfigLightSensor.Slots.Count;
            try
            {
                string mes;
                if (this._configRs485Validator.Check(out mes, true))
                {
                    this._device.ConfigRs485.Value = this._configRs485Validator.Get();

                }
                else throw new Exception("Invalid RS485-config data");
                if (this._configLightSensorValidator.Check(out mes, true))
                {
                    this._device.ConfigLightSensor.Value = this._configLightSensorValidator.Get();
                }
                else throw new Exception("Invalid light sensor config data");

                this._device.ConfigRs485.SaveStruct();
                this._device.ConfigLightSensor.SaveStruct();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        #region RS485

        public void ReadRs485Ok()
        {
            this._statusLabel.Text = ConfigurationStringConst.READ_RS485_OK;
            this._statusLabel.BackColor = Color.LightGreen;
            this._configRs485Validator.Set(this._device.ConfigRs485.Value);
        }

        private void _RS485WriteBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            try
            {
                string mes;
                if (this._configRs485Validator.Check(out mes, true))
                {
                    this._device.ConfigRs485.Value = this._configRs485Validator.Get();
                    this._device.ConfigRs485.SaveStruct();
                }
                else throw new Exception("Invalid RS485-config data");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void _RS485paritetYNCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cb = sender as ComboBox;
            if (cb == null || cb.SelectedIndex == -1) return;
            this._RS485paritetCHETCombo.Enabled = cb.SelectedIndex != 0;
        }

        private void _readRs485Button_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._statusLabel.Text = string.Empty;
            this._statusLabel.BackColor = Color.Transparent;
            this._progressBar.Maximum = this._device.ConfigRs485.Slots.Count;
            this._device.ConfigRs485.LoadStruct();
        }
        #endregion RS485

        #region ConfigLightSensor

        private void ConfigLightSensorReadOk()
        {
            this._statusLabel.Text = ConfigurationStringConst.READ_SETPOINT_OK;
            this._statusLabel.BackColor = Color.LightGreen;
            this._configLightSensorValidator.Set(this._device.ConfigLightSensor.Value);
        }

        private void _lightSensorWriteBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            try
            {
                string mes;
                if (this._configLightSensorValidator.Check(out mes, true))
                {
                    this._device.ConfigLightSensor.Value = this._configLightSensorValidator.Get();
                    this._device.ConfigLightSensor.SaveStruct();
                }
                else throw new Exception("Invalid light sensor config data");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void _lightSensorReadButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._statusLabel.Text = string.Empty;
            this._statusLabel.BackColor = Color.Transparent;
            this._progressBar.Maximum = this._device.ConfigLightSensor.Slots.Count;
            this._device.ConfigLightSensor.LoadStruct();
        }
        #endregion

        #region Версия
        public void ReadVersionComplite()
        {
            this._appName.Text = this._device.Version.Value.AppName;
            this._appPodtip.Text = this._device.Version.Value.AppPodtip;
            this._appMod.Text = this._device.Version.Value.AppMod;
            this._appRevision.Text = this._device.Version.Value.AppVersion;
            
            this._statusLabel.Text = ConfigurationStringConst.READ_VERSION_OK;
            this._statusLabel.BackColor = Color.LightGreen;
        }

        private void _readVersionBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._statusLabel.Text = string.Empty;
            this._statusLabel.BackColor = Color.Transparent;
            this._progressBar.Maximum = this._device.Version.Slots.Count;
            this._device.Version.LoadStruct();
        }
        #endregion Версия

        private void PerformStep()
        {
            this._progressBar.PerformStep();
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(LightSensorDevice); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(ConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}
