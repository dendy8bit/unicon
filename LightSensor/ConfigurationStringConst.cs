﻿namespace LightSensor
{
    public static class ConfigurationStringConst
    {
        #region Константы
        public const string NAN = "NaN";
        public const string READ_RS485_FAIL = "Не удалось загрузить параметры RS-485";
        public const string READ_RS485_OK = "Конфигурация RS-485 загружена успешно";
        public const string READ_SETPOINT_FAIL = "Не удалось загрузить конфигурацию ДО";
        public const string READ_SETPOINT_OK = "Конфигурация ДО загружена успешно";
        public const string READ_VERSION_OK = "Версия загружена успешно";
        public const string READ_VERSION_FAIL = "Не удалось прочитать версию";
        public const string WRITE_RS485_OK = "Конфигурация RS-485 записана успешно";
        public const string WRITE_RS485_FAIL = "Не удалось записать конфигурацию RS-485";
        public const string WRITE_SETPOINT_OK = "Конфигурация ДО записана успешно";
        public const string WRITE_SETPOINT_FAIL = "Не удалось записать конфигурацию ДО";
        public const string DEFUULT_CONFIG_SAVE_OK = "Конфигурация по умолчанию записана успешно";
        #endregion
    }
}
