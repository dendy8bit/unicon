﻿namespace LightSensor
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._versionPage = new System.Windows.Forms.TabPage();
            this._readVersionBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._appRevision = new System.Windows.Forms.MaskedTextBox();
            this._appMod = new System.Windows.Forms.MaskedTextBox();
            this._appPodtip = new System.Windows.Forms.MaskedTextBox();
            this._appName = new System.Windows.Forms.MaskedTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.setpointPage = new System.Windows.Forms.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this._accuracy = new System.Windows.Forms.MaskedTextBox();
            this._mtreg = new System.Windows.Forms.MaskedTextBox();
            this._alpha = new System.Windows.Forms.MaskedTextBox();
            this._tWait = new System.Windows.Forms.MaskedTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this._readLightSensorBtn = new System.Windows.Forms.Button();
            this._writeLightSensorBtn = new System.Windows.Forms.Button();
            this._RS485Page = new System.Windows.Forms.TabPage();
            this._RS485WriteBtn = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._RS485dataBitsCombo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this._RS485ToSendAfterTB = new System.Windows.Forms.MaskedTextBox();
            this._RS485ToSendBeforeTB = new System.Windows.Forms.MaskedTextBox();
            this._RS485AddressTB = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this._RS485paritetCHETCombo = new System.Windows.Forms.ComboBox();
            this._RS485paritetYNCombo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this._RS485stopBitsCombo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this._RS485speedsCombo = new System.Windows.Forms.ComboBox();
            this._readRs485Button = new System.Windows.Forms.Button();
            this._configurationTabControl = new System.Windows.Forms.TabControl();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._readAllBtn = new System.Windows.Forms.Button();
            this._writeAllBtn = new System.Windows.Forms.Button();
            this._versionPage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.setpointPage.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this._RS485Page.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this._configurationTabControl.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _versionPage
            // 
            this._versionPage.Controls.Add(this._readVersionBtn);
            this._versionPage.Controls.Add(this.groupBox1);
            this._versionPage.Location = new System.Drawing.Point(4, 22);
            this._versionPage.Name = "_versionPage";
            this._versionPage.Padding = new System.Windows.Forms.Padding(3);
            this._versionPage.Size = new System.Drawing.Size(402, 252);
            this._versionPage.TabIndex = 5;
            this._versionPage.Text = "Версия";
            this._versionPage.UseVisualStyleBackColor = true;
            // 
            // _readVersionBtn
            // 
            this._readVersionBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._readVersionBtn.Location = new System.Drawing.Point(7, 222);
            this._readVersionBtn.Name = "_readVersionBtn";
            this._readVersionBtn.Size = new System.Drawing.Size(75, 23);
            this._readVersionBtn.TabIndex = 17;
            this._readVersionBtn.Text = "Прочитать";
            this._readVersionBtn.UseVisualStyleBackColor = true;
            this._readVersionBtn.Click += new System.EventHandler(this._readVersionBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._appRevision);
            this.groupBox1.Controls.Add(this._appMod);
            this.groupBox1.Controls.Add(this._appPodtip);
            this.groupBox1.Controls.Add(this._appName);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(7, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(192, 115);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Версия программы";
            // 
            // _appRevision
            // 
            this._appRevision.Location = new System.Drawing.Point(126, 87);
            this._appRevision.Name = "_appRevision";
            this._appRevision.ReadOnly = true;
            this._appRevision.Size = new System.Drawing.Size(60, 20);
            this._appRevision.TabIndex = 12;
            // 
            // _appMod
            // 
            this._appMod.Location = new System.Drawing.Point(126, 64);
            this._appMod.Name = "_appMod";
            this._appMod.ReadOnly = true;
            this._appMod.Size = new System.Drawing.Size(60, 20);
            this._appMod.TabIndex = 12;
            // 
            // _appPodtip
            // 
            this._appPodtip.Location = new System.Drawing.Point(126, 41);
            this._appPodtip.Name = "_appPodtip";
            this._appPodtip.ReadOnly = true;
            this._appPodtip.Size = new System.Drawing.Size(60, 20);
            this._appPodtip.TabIndex = 12;
            // 
            // _appName
            // 
            this._appName.Location = new System.Drawing.Point(126, 18);
            this._appName.Name = "_appName";
            this._appName.ReadOnly = true;
            this._appName.Size = new System.Drawing.Size(60, 20);
            this._appName.TabIndex = 12;
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(115, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(5, 96);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Устройство : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Версия прошивки :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Модификация";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Подтип";
            // 
            // setpointPage
            // 
            this.setpointPage.Controls.Add(this.groupBox13);
            this.setpointPage.Controls.Add(this._readLightSensorBtn);
            this.setpointPage.Controls.Add(this._writeLightSensorBtn);
            this.setpointPage.Location = new System.Drawing.Point(4, 22);
            this.setpointPage.Name = "setpointPage";
            this.setpointPage.Padding = new System.Windows.Forms.Padding(3);
            this.setpointPage.Size = new System.Drawing.Size(402, 252);
            this.setpointPage.TabIndex = 7;
            this.setpointPage.Text = "Конфигурация датчика освещения";
            this.setpointPage.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this._accuracy);
            this.groupBox13.Controls.Add(this._mtreg);
            this.groupBox13.Controls.Add(this._alpha);
            this.groupBox13.Controls.Add(this._tWait);
            this.groupBox13.Controls.Add(this.label12);
            this.groupBox13.Controls.Add(this.label29);
            this.groupBox13.Controls.Add(this.label5);
            this.groupBox13.Controls.Add(this.label27);
            this.groupBox13.Controls.Add(this.label28);
            this.groupBox13.Location = new System.Drawing.Point(6, 6);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(390, 154);
            this.groupBox13.TabIndex = 20;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Уставки";
            // 
            // _accuracy
            // 
            this._accuracy.Location = new System.Drawing.Point(139, 97);
            this._accuracy.Name = "_accuracy";
            this._accuracy.Size = new System.Drawing.Size(63, 20);
            this._accuracy.TabIndex = 21;
            // 
            // _mtreg
            // 
            this._mtreg.Location = new System.Drawing.Point(139, 71);
            this._mtreg.Name = "_mtreg";
            this._mtreg.Size = new System.Drawing.Size(63, 20);
            this._mtreg.TabIndex = 21;
            // 
            // _alpha
            // 
            this._alpha.Location = new System.Drawing.Point(139, 45);
            this._alpha.Name = "_alpha";
            this._alpha.Size = new System.Drawing.Size(63, 20);
            this._alpha.TabIndex = 21;
            // 
            // _tWait
            // 
            this._tWait.Location = new System.Drawing.Point(139, 19);
            this._tWait.Name = "_tWait";
            this._tWait.Size = new System.Drawing.Size(63, 20);
            this._tWait.TabIndex = 21;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 100);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(113, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Точность измерения";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(208, 22);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(21, 13);
            this.label29.TabIndex = 3;
            this.label29.Text = "мс";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Значение MTReg";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(127, 13);
            this.label27.TabIndex = 3;
            this.label27.Text = "Время преобразования";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 48);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(112, 13);
            this.label28.TabIndex = 3;
            this.label28.Text = "Коэффициент альфа";
            // 
            // _readLightSensorBtn
            // 
            this._readLightSensorBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._readLightSensorBtn.Location = new System.Drawing.Point(7, 222);
            this._readLightSensorBtn.Name = "_readLightSensorBtn";
            this._readLightSensorBtn.Size = new System.Drawing.Size(75, 23);
            this._readLightSensorBtn.TabIndex = 20;
            this._readLightSensorBtn.Text = "Прочитать";
            this._readLightSensorBtn.Click += new System.EventHandler(this._lightSensorReadButton_Click);
            // 
            // _writeLightSensorBtn
            // 
            this._writeLightSensorBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._writeLightSensorBtn.Location = new System.Drawing.Point(88, 222);
            this._writeLightSensorBtn.Name = "_writeLightSensorBtn";
            this._writeLightSensorBtn.Size = new System.Drawing.Size(75, 23);
            this._writeLightSensorBtn.TabIndex = 20;
            this._writeLightSensorBtn.Text = "Записать";
            this._writeLightSensorBtn.Click += new System.EventHandler(this._lightSensorWriteBtnClick);
            // 
            // _RS485Page
            // 
            this._RS485Page.Controls.Add(this._RS485WriteBtn);
            this._RS485Page.Controls.Add(this.groupBox3);
            this._RS485Page.Controls.Add(this._readRs485Button);
            this._RS485Page.Location = new System.Drawing.Point(4, 22);
            this._RS485Page.Name = "_RS485Page";
            this._RS485Page.Padding = new System.Windows.Forms.Padding(3);
            this._RS485Page.Size = new System.Drawing.Size(402, 252);
            this._RS485Page.TabIndex = 0;
            this._RS485Page.Text = "RS-485";
            this._RS485Page.UseVisualStyleBackColor = true;
            // 
            // _RS485WriteBtn
            // 
            this._RS485WriteBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._RS485WriteBtn.Location = new System.Drawing.Point(88, 222);
            this._RS485WriteBtn.Name = "_RS485WriteBtn";
            this._RS485WriteBtn.Size = new System.Drawing.Size(75, 23);
            this._RS485WriteBtn.TabIndex = 10;
            this._RS485WriteBtn.Text = "Записать";
            this._RS485WriteBtn.UseVisualStyleBackColor = true;
            this._RS485WriteBtn.Click += new System.EventHandler(this._RS485WriteBtnClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._RS485dataBitsCombo);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this._RS485ToSendAfterTB);
            this.groupBox3.Controls.Add(this._RS485ToSendBeforeTB);
            this.groupBox3.Controls.Add(this._RS485AddressTB);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this._RS485paritetCHETCombo);
            this.groupBox3.Controls.Add(this._RS485paritetYNCombo);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this._RS485stopBitsCombo);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this._RS485speedsCombo);
            this.groupBox3.Location = new System.Drawing.Point(7, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(287, 210);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Конфигурация RS-485";
            // 
            // _RS485dataBitsCombo
            // 
            this._RS485dataBitsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._RS485dataBitsCombo.FormattingEnabled = true;
            this._RS485dataBitsCombo.Location = new System.Drawing.Point(181, 42);
            this._RS485dataBitsCombo.Name = "_RS485dataBitsCombo";
            this._RS485dataBitsCombo.Size = new System.Drawing.Size(97, 21);
            this._RS485dataBitsCombo.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Количество бит данных";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 164);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(163, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "Таймаут после выдачи данных";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 145);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(145, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Таймаут до выдачи данных";
            // 
            // _RS485ToSendAfterTB
            // 
            this._RS485ToSendAfterTB.Location = new System.Drawing.Point(181, 161);
            this._RS485ToSendAfterTB.Name = "_RS485ToSendAfterTB";
            this._RS485ToSendAfterTB.Size = new System.Drawing.Size(97, 20);
            this._RS485ToSendAfterTB.TabIndex = 23;
            this._RS485ToSendAfterTB.Tag = "255";
            this._RS485ToSendAfterTB.Text = "0";
            // 
            // _RS485ToSendBeforeTB
            // 
            this._RS485ToSendBeforeTB.Location = new System.Drawing.Point(181, 142);
            this._RS485ToSendBeforeTB.Name = "_RS485ToSendBeforeTB";
            this._RS485ToSendBeforeTB.Size = new System.Drawing.Size(97, 20);
            this._RS485ToSendBeforeTB.TabIndex = 22;
            this._RS485ToSendBeforeTB.Tag = "255";
            this._RS485ToSendBeforeTB.Text = "0";
            // 
            // _RS485AddressTB
            // 
            this._RS485AddressTB.Location = new System.Drawing.Point(181, 123);
            this._RS485AddressTB.Name = "_RS485AddressTB";
            this._RS485AddressTB.Size = new System.Drawing.Size(97, 20);
            this._RS485AddressTB.TabIndex = 19;
            this._RS485AddressTB.Tag = "255";
            this._RS485AddressTB.Text = "1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 126);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Адрес устройства";
            // 
            // _RS485paritetCHETCombo
            // 
            this._RS485paritetCHETCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._RS485paritetCHETCombo.Enabled = false;
            this._RS485paritetCHETCombo.FormattingEnabled = true;
            this._RS485paritetCHETCombo.Location = new System.Drawing.Point(181, 102);
            this._RS485paritetCHETCombo.Name = "_RS485paritetCHETCombo";
            this._RS485paritetCHETCombo.Size = new System.Drawing.Size(97, 21);
            this._RS485paritetCHETCombo.TabIndex = 15;
            // 
            // _RS485paritetYNCombo
            // 
            this._RS485paritetYNCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._RS485paritetYNCombo.FormattingEnabled = true;
            this._RS485paritetYNCombo.Location = new System.Drawing.Point(181, 82);
            this._RS485paritetYNCombo.Name = "_RS485paritetYNCombo";
            this._RS485paritetYNCombo.Size = new System.Drawing.Size(97, 21);
            this._RS485paritetYNCombo.TabIndex = 14;
            this._RS485paritetYNCombo.SelectedIndexChanged += new System.EventHandler(this._RS485paritetYNCombo_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 85);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Паритет";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Количество стоп бит";
            // 
            // _RS485stopBitsCombo
            // 
            this._RS485stopBitsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._RS485stopBitsCombo.FormattingEnabled = true;
            this._RS485stopBitsCombo.Location = new System.Drawing.Point(181, 62);
            this._RS485stopBitsCombo.Name = "_RS485stopBitsCombo";
            this._RS485stopBitsCombo.Size = new System.Drawing.Size(97, 21);
            this._RS485stopBitsCombo.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Скорость передачи";
            // 
            // _RS485speedsCombo
            // 
            this._RS485speedsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._RS485speedsCombo.FormattingEnabled = true;
            this._RS485speedsCombo.Location = new System.Drawing.Point(181, 22);
            this._RS485speedsCombo.Name = "_RS485speedsCombo";
            this._RS485speedsCombo.Size = new System.Drawing.Size(97, 21);
            this._RS485speedsCombo.TabIndex = 9;
            // 
            // _readRs485Button
            // 
            this._readRs485Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._readRs485Button.Location = new System.Drawing.Point(7, 222);
            this._readRs485Button.Name = "_readRs485Button";
            this._readRs485Button.Size = new System.Drawing.Size(75, 23);
            this._readRs485Button.TabIndex = 15;
            this._readRs485Button.Text = "Прочитать";
            this._readRs485Button.UseVisualStyleBackColor = true;
            this._readRs485Button.Click += new System.EventHandler(this._readRs485Button_Click);
            // 
            // _configurationTabControl
            // 
            this._configurationTabControl.Controls.Add(this._RS485Page);
            this._configurationTabControl.Controls.Add(this.setpointPage);
            this._configurationTabControl.Controls.Add(this._versionPage);
            this._configurationTabControl.Location = new System.Drawing.Point(12, 12);
            this._configurationTabControl.Name = "_configurationTabControl";
            this._configurationTabControl.SelectedIndex = 0;
            this._configurationTabControl.Size = new System.Drawing.Size(410, 278);
            this._configurationTabControl.TabIndex = 17;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._progressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 327);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(434, 22);
            this.statusStrip1.TabIndex = 18;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _progressBar
            // 
            this._progressBar.Name = "_progressBar";
            this._progressBar.Size = new System.Drawing.Size(80, 16);
            this._progressBar.Step = 1;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _readAllBtn
            // 
            this._readAllBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._readAllBtn.Location = new System.Drawing.Point(12, 301);
            this._readAllBtn.Name = "_readAllBtn";
            this._readAllBtn.Size = new System.Drawing.Size(180, 23);
            this._readAllBtn.TabIndex = 15;
            this._readAllBtn.Text = "Прочитать всю конфигурацию";
            this._readAllBtn.UseVisualStyleBackColor = true;
            this._readAllBtn.Click += new System.EventHandler(this._readAllBtnClick);
            // 
            // _writeAllBtn
            // 
            this._writeAllBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._writeAllBtn.Location = new System.Drawing.Point(198, 301);
            this._writeAllBtn.Name = "_writeAllBtn";
            this._writeAllBtn.Size = new System.Drawing.Size(166, 23);
            this._writeAllBtn.TabIndex = 10;
            this._writeAllBtn.Text = "Записать всю конфигурацию";
            this._writeAllBtn.UseVisualStyleBackColor = true;
            this._writeAllBtn.Click += new System.EventHandler(this._writeAllBtnClick);
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 349);
            this.Controls.Add(this._writeAllBtn);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._configurationTabControl);
            this.Controls.Add(this._readAllBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "ConfigurationForm";
            this.Text = "Configuration";
            this.Load += new System.EventHandler(this.ConfigForm_Load);
            this._versionPage.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.setpointPage.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this._RS485Page.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this._configurationTabControl.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabPage _versionPage;
        private System.Windows.Forms.Button _readVersionBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MaskedTextBox _appRevision;
        private System.Windows.Forms.MaskedTextBox _appMod;
        private System.Windows.Forms.MaskedTextBox _appPodtip;
        private System.Windows.Forms.MaskedTextBox _appName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage setpointPage;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.MaskedTextBox _alpha;
        private System.Windows.Forms.MaskedTextBox _tWait;
        private System.Windows.Forms.Button _readLightSensorBtn;
        private System.Windows.Forms.Button _writeLightSensorBtn;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TabPage _RS485Page;
        private System.Windows.Forms.Button _RS485WriteBtn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox _RS485dataBitsCombo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox _RS485ToSendAfterTB;
        private System.Windows.Forms.MaskedTextBox _RS485ToSendBeforeTB;
        private System.Windows.Forms.MaskedTextBox _RS485AddressTB;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox _RS485paritetCHETCombo;
        private System.Windows.Forms.ComboBox _RS485paritetYNCombo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox _RS485stopBitsCombo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox _RS485speedsCombo;
        private System.Windows.Forms.Button _readRs485Button;
        private System.Windows.Forms.TabControl _configurationTabControl;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar _progressBar;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.MaskedTextBox _accuracy;
        private System.Windows.Forms.MaskedTextBox _mtreg;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button _readAllBtn;
        private System.Windows.Forms.Button _writeAllBtn;
    }
}