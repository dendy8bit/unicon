namespace BEMN.TZL.Forms
{
    partial class VVOscilloscopeResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MAINTABLE = new System.Windows.Forms.TableLayoutPanel();
            this.hScrollBar4 = new System.Windows.Forms.HScrollBar();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MAINPANEL = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.discrPanel = new System.Windows.Forms.Panel();
            this._discretChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.button4 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.UYminus = new System.Windows.Forms.Button();
            this.UYPlus = new System.Windows.Forms.Button();
            this.UScroll = new System.Windows.Forms.VScrollBar();
            this.panel4 = new System.Windows.Forms.Panel();
            this._voltageChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.S1Yminus = new System.Windows.Forms.Button();
            this.S1YPlus = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this._s1TokChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.S1Scroll = new System.Windows.Forms.VScrollBar();
            this.MarkersTable = new System.Windows.Forms.TableLayoutPanel();
            this.Marker1CB = new System.Windows.Forms.CheckBox();
            this.Marker2CB = new System.Windows.Forms.CheckBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this._positionM1End = new System.Windows.Forms.Label();
            this._positionM1Start = new System.Windows.Forms.Label();
            this._zoom1ChB = new System.Windows.Forms.CheckBox();
            this.Marker1TrackBar = new System.Windows.Forms.TrackBar();
            this.panel7 = new System.Windows.Forms.Panel();
            this._positionM2End = new System.Windows.Forms.Label();
            this._positionM2Start = new System.Windows.Forms.Label();
            this._zoom2ChB = new System.Windows.Forms.CheckBox();
            this.Marker2TrackBar = new System.Windows.Forms.TrackBar();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.DeltaLabel = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.TimeMarker2 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.TimeMarker1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._m2DiscretPanel = new System.Windows.Forms.Panel();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.Marker2Un = new System.Windows.Forms.Label();
            this.Marker2Uc = new System.Windows.Forms.Label();
            this.Marker2Ub = new System.Windows.Forms.Label();
            this.Marker2Ua = new System.Windows.Forms.Label();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.Marker2S1In = new System.Windows.Forms.Label();
            this.Marker2S1Ic = new System.Windows.Forms.Label();
            this.Marker2S1Ib = new System.Windows.Forms.Label();
            this.Marker2S1Ia = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._m1DiscretPanel = new System.Windows.Forms.Panel();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.Marker1Un = new System.Windows.Forms.Label();
            this.Marker1Uc = new System.Windows.Forms.Label();
            this.Marker1Ub = new System.Windows.Forms.Label();
            this.Marker1Ua = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.Marker1S1In = new System.Windows.Forms.Label();
            this.Marker1S1Ic = new System.Windows.Forms.Label();
            this.Marker1S1Ib = new System.Windows.Forms.Label();
            this.Marker1S1Ia = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.button43 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this._avaryOscCheck = new System.Windows.Forms.CheckBox();
            this.Xplus = new System.Windows.Forms.Button();
            this.Xminus = new System.Windows.Forms.Button();
            this.S1ShowCB = new System.Windows.Forms.CheckBox();
            this.UShowCB = new System.Windows.Forms.CheckBox();
            this.DiscretShowCB = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.MarkerCB = new System.Windows.Forms.CheckBox();
            this.FlagCB = new System.Windows.Forms.CheckBox();
            this._avaryJournalCheck = new System.Windows.Forms.CheckBox();
            this.MAINTABLE.SuspendLayout();
            this.MAINPANEL.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.discrPanel.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.MarkersTable.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Marker1TrackBar)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Marker2TrackBar)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // MAINTABLE
            // 
            this.MAINTABLE.ColumnCount = 2;
            this.MAINTABLE.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MAINTABLE.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.MAINTABLE.Controls.Add(this.hScrollBar4, 0, 2);
            this.MAINTABLE.Controls.Add(this.menuStrip1, 0, 0);
            this.MAINTABLE.Controls.Add(this.MAINPANEL, 0, 1);
            this.MAINTABLE.Controls.Add(this.panel3, 1, 1);
            this.MAINTABLE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MAINTABLE.Location = new System.Drawing.Point(0, 0);
            this.MAINTABLE.Margin = new System.Windows.Forms.Padding(0);
            this.MAINTABLE.Name = "MAINTABLE";
            this.MAINTABLE.RowCount = 2;
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MAINTABLE.Size = new System.Drawing.Size(1182, 772);
            this.MAINTABLE.TabIndex = 0;
            // 
            // hScrollBar4
            // 
            this.hScrollBar4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.hScrollBar4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hScrollBar4.LargeChange = 1;
            this.hScrollBar4.Location = new System.Drawing.Point(0, 752);
            this.hScrollBar4.Maximum = 10;
            this.hScrollBar4.Minimum = 10;
            this.hScrollBar4.Name = "hScrollBar4";
            this.hScrollBar4.Size = new System.Drawing.Size(1182, 20);
            this.hScrollBar4.TabIndex = 20;
            this.hScrollBar4.Value = 10;
            this.hScrollBar4.Visible = false;
            this.hScrollBar4.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar4_Scroll);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Silver;
            this.MAINTABLE.SetColumnSpan(this.menuStrip1, 2);
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1182, 24);
            this.menuStrip1.TabIndex = 18;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // MAINPANEL
            // 
            this.MAINPANEL.Controls.Add(this.panel1);
            this.MAINPANEL.Controls.Add(this.MarkersTable);
            this.MAINPANEL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MAINPANEL.Location = new System.Drawing.Point(3, 28);
            this.MAINPANEL.Name = "MAINPANEL";
            this.MAINPANEL.Size = new System.Drawing.Size(1176, 721);
            this.MAINPANEL.TabIndex = 32;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 59);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1176, 662);
            this.panel1.TabIndex = 32;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.tableLayoutPanel2);
            this.panel2.Controls.Add(this.tableLayoutPanel16);
            this.panel2.Controls.Add(this.tableLayoutPanel13);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1037, 1000);
            this.panel2.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.Controls.Add(this.discrPanel, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel8, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 406);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1037, 588);
            this.tableLayoutPanel2.TabIndex = 35;
            // 
            // discrPanel
            // 
            this.discrPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.discrPanel.AutoScroll = true;
            this.tableLayoutPanel2.SetColumnSpan(this.discrPanel, 3);
            this.discrPanel.Controls.Add(this._discretChart);
            this.discrPanel.Controls.Add(this.label2);
            this.discrPanel.Controls.Add(this.label3);
            this.discrPanel.Controls.Add(this.label5);
            this.discrPanel.Controls.Add(this.label6);
            this.discrPanel.Controls.Add(this.label7);
            this.discrPanel.Controls.Add(this.label8);
            this.discrPanel.Controls.Add(this.label9);
            this.discrPanel.Controls.Add(this.label10);
            this.discrPanel.Controls.Add(this.label11);
            this.discrPanel.Controls.Add(this.label12);
            this.discrPanel.Controls.Add(this.label13);
            this.discrPanel.Controls.Add(this.label14);
            this.discrPanel.Controls.Add(this.label15);
            this.discrPanel.Controls.Add(this.label16);
            this.discrPanel.Controls.Add(this.label17);
            this.discrPanel.Controls.Add(this.label18);
            this.discrPanel.Controls.Add(this.label116);
            this.discrPanel.Controls.Add(this.label117);
            this.discrPanel.Controls.Add(this.label118);
            this.discrPanel.Controls.Add(this.label119);
            this.discrPanel.Controls.Add(this.label120);
            this.discrPanel.Controls.Add(this.label121);
            this.discrPanel.Controls.Add(this.label122);
            this.discrPanel.Controls.Add(this.label123);
            this.discrPanel.Controls.Add(this.label124);
            this.discrPanel.Controls.Add(this.label125);
            this.discrPanel.Controls.Add(this.label126);
            this.discrPanel.Controls.Add(this.label127);
            this.discrPanel.Controls.Add(this.label128);
            this.discrPanel.Controls.Add(this.label129);
            this.discrPanel.Controls.Add(this.label130);
            this.discrPanel.Controls.Add(this.label131);
            this.discrPanel.Controls.Add(this.label100);
            this.discrPanel.Controls.Add(this.label101);
            this.discrPanel.Controls.Add(this.label102);
            this.discrPanel.Controls.Add(this.label103);
            this.discrPanel.Controls.Add(this.label104);
            this.discrPanel.Controls.Add(this.label105);
            this.discrPanel.Controls.Add(this.label106);
            this.discrPanel.Controls.Add(this.label107);
            this.discrPanel.Controls.Add(this.label108);
            this.discrPanel.Controls.Add(this.label109);
            this.discrPanel.Controls.Add(this.label110);
            this.discrPanel.Controls.Add(this.label111);
            this.discrPanel.Controls.Add(this.label112);
            this.discrPanel.Controls.Add(this.label113);
            this.discrPanel.Controls.Add(this.label114);
            this.discrPanel.Controls.Add(this.label115);
            this.discrPanel.Controls.Add(this.label84);
            this.discrPanel.Controls.Add(this.label85);
            this.discrPanel.Controls.Add(this.label86);
            this.discrPanel.Controls.Add(this.label87);
            this.discrPanel.Controls.Add(this.label88);
            this.discrPanel.Controls.Add(this.label89);
            this.discrPanel.Controls.Add(this.label90);
            this.discrPanel.Controls.Add(this.label91);
            this.discrPanel.Controls.Add(this.label92);
            this.discrPanel.Controls.Add(this.label93);
            this.discrPanel.Controls.Add(this.label94);
            this.discrPanel.Controls.Add(this.label95);
            this.discrPanel.Controls.Add(this.label96);
            this.discrPanel.Controls.Add(this.label97);
            this.discrPanel.Controls.Add(this.label98);
            this.discrPanel.Controls.Add(this.label99);
            this.discrPanel.Controls.Add(this.label68);
            this.discrPanel.Controls.Add(this.label69);
            this.discrPanel.Controls.Add(this.label70);
            this.discrPanel.Controls.Add(this.label71);
            this.discrPanel.Controls.Add(this.label72);
            this.discrPanel.Controls.Add(this.label73);
            this.discrPanel.Controls.Add(this.label74);
            this.discrPanel.Controls.Add(this.label75);
            this.discrPanel.Controls.Add(this.label76);
            this.discrPanel.Controls.Add(this.label77);
            this.discrPanel.Controls.Add(this.label78);
            this.discrPanel.Controls.Add(this.label79);
            this.discrPanel.Controls.Add(this.label80);
            this.discrPanel.Controls.Add(this.label81);
            this.discrPanel.Controls.Add(this.label82);
            this.discrPanel.Controls.Add(this.label83);
            this.discrPanel.Controls.Add(this.label52);
            this.discrPanel.Controls.Add(this.label53);
            this.discrPanel.Controls.Add(this.label54);
            this.discrPanel.Controls.Add(this.label55);
            this.discrPanel.Controls.Add(this.label56);
            this.discrPanel.Controls.Add(this.label57);
            this.discrPanel.Controls.Add(this.label58);
            this.discrPanel.Controls.Add(this.label59);
            this.discrPanel.Controls.Add(this.label60);
            this.discrPanel.Controls.Add(this.label61);
            this.discrPanel.Controls.Add(this.label62);
            this.discrPanel.Controls.Add(this.label63);
            this.discrPanel.Controls.Add(this.label64);
            this.discrPanel.Controls.Add(this.label65);
            this.discrPanel.Controls.Add(this.label66);
            this.discrPanel.Controls.Add(this.label67);
            this.discrPanel.Controls.Add(this.label36);
            this.discrPanel.Controls.Add(this.label37);
            this.discrPanel.Controls.Add(this.label38);
            this.discrPanel.Controls.Add(this.label39);
            this.discrPanel.Controls.Add(this.label40);
            this.discrPanel.Controls.Add(this.label41);
            this.discrPanel.Controls.Add(this.label42);
            this.discrPanel.Controls.Add(this.label43);
            this.discrPanel.Controls.Add(this.label44);
            this.discrPanel.Controls.Add(this.label45);
            this.discrPanel.Controls.Add(this.label46);
            this.discrPanel.Controls.Add(this.label47);
            this.discrPanel.Controls.Add(this.label48);
            this.discrPanel.Controls.Add(this.label49);
            this.discrPanel.Controls.Add(this.label50);
            this.discrPanel.Controls.Add(this.label51);
            this.discrPanel.Controls.Add(this.label20);
            this.discrPanel.Controls.Add(this.label21);
            this.discrPanel.Controls.Add(this.label22);
            this.discrPanel.Controls.Add(this.label23);
            this.discrPanel.Controls.Add(this.label24);
            this.discrPanel.Controls.Add(this.label25);
            this.discrPanel.Controls.Add(this.label26);
            this.discrPanel.Controls.Add(this.label27);
            this.discrPanel.Controls.Add(this.label28);
            this.discrPanel.Controls.Add(this.label29);
            this.discrPanel.Controls.Add(this.label30);
            this.discrPanel.Controls.Add(this.label31);
            this.discrPanel.Controls.Add(this.label32);
            this.discrPanel.Controls.Add(this.label33);
            this.discrPanel.Controls.Add(this.label34);
            this.discrPanel.Controls.Add(this.label35);
            this.discrPanel.Location = new System.Drawing.Point(3, 38);
            this.discrPanel.Name = "discrPanel";
            this.discrPanel.Size = new System.Drawing.Size(991, 547);
            this.discrPanel.TabIndex = 17;
            // 
            // _discretChart
            // 
            this._discretChart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discretChart.BkGradient = false;
            this._discretChart.BkGradientAngle = 90;
            this._discretChart.BkGradientColor = System.Drawing.Color.White;
            this._discretChart.BkGradientRate = 0.5F;
            this._discretChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._discretChart.BkRestrictedToChartPanel = false;
            this._discretChart.BkShinePosition = 1F;
            this._discretChart.BkTransparency = 0F;
            this._discretChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._discretChart.BorderExteriorLength = 0;
            this._discretChart.BorderGradientAngle = 225;
            this._discretChart.BorderGradientLightPos1 = 1F;
            this._discretChart.BorderGradientLightPos2 = -1F;
            this._discretChart.BorderGradientRate = 0.5F;
            this._discretChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._discretChart.BorderLightIntermediateBrightness = 0F;
            this._discretChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._discretChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._discretChart.ChartPanelBkTransparency = 0F;
            this._discretChart.ControlShadow = false;
            this._discretChart.CoordinateAxesVisible = true;
            this._discretChart.CoordinateAxisColor = System.Drawing.Color.LightGray;
            this._discretChart.CoordinateXOrigin = 50D;
            this._discretChart.CoordinateYMax = 129D;
            this._discretChart.CoordinateYMin = 0D;
            this._discretChart.CoordinateYOrigin = 65.5D;
            this._discretChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._discretChart.FooterColor = System.Drawing.Color.Black;
            this._discretChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._discretChart.FooterVisible = false;
            this._discretChart.ForeColor = System.Drawing.Color.Black;
            this._discretChart.GridColor = System.Drawing.Color.LightGray;
            this._discretChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._discretChart.GridVisible = true;
            this._discretChart.GridXSubTicker = 0;
            this._discretChart.GridXTicker = 10;
            this._discretChart.GridYSubTicker = 0;
            this._discretChart.GridYTicker = 2;
            this._discretChart.HeaderColor = System.Drawing.Color.Black;
            this._discretChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._discretChart.HeaderVisible = false;
            this._discretChart.InnerBorderDarkColor = System.Drawing.Color.Gray;
            this._discretChart.InnerBorderLength = 0;
            this._discretChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._discretChart.LegendBkColor = System.Drawing.Color.White;
            this._discretChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._discretChart.LegendVisible = false;
            this._discretChart.Location = new System.Drawing.Point(69, 0);
            this._discretChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._discretChart.MiddleBorderLength = 0;
            this._discretChart.Name = "_discretChart";
            this._discretChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._discretChart.OuterBorderLength = 0;
            this._discretChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._discretChart.Precision = 0;
            this._discretChart.RoundRadius = 10;
            this._discretChart.ShadowColor = System.Drawing.Color.DimGray;
            this._discretChart.ShadowDepth = 8;
            this._discretChart.ShadowRate = 0.5F;
            this._discretChart.Size = new System.Drawing.Size(902, 4000);
            this._discretChart.TabIndex = 157;
            this._discretChart.Text = "daS_Net_XYChart5";
            this._discretChart.XMax = 100D;
            this._discretChart.XMin = 0D;
            this._discretChart.XScaleColor = System.Drawing.Color.DimGray;
            this._discretChart.XScaleVisible = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 3499);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 156;
            this.label2.Text = "��� 9";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 3530);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 155;
            this.label3.Text = "��� 10";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(0, 3560);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 154;
            this.label5.Text = "��� 11";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(0, 3591);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 153;
            this.label6.Text = "��� 12";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(0, 3622);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 152;
            this.label7.Text = "��� 13";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(0, 3653);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 151;
            this.label8.Text = "��� 14";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(0, 3685);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 13);
            this.label9.TabIndex = 150;
            this.label9.Text = "��� 15";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(0, 3716);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 13);
            this.label10.TabIndex = 149;
            this.label10.Text = "��� 16";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(0, 3747);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 148;
            this.label11.Text = "��� 17";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(0, 3778);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 13);
            this.label12.TabIndex = 147;
            this.label12.Text = "��� 18";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(0, 3808);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 13);
            this.label13.TabIndex = 146;
            this.label13.Text = "��� 19";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(0, 3839);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 145;
            this.label14.Text = "��� 20";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(0, 3870);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(44, 13);
            this.label15.TabIndex = 144;
            this.label15.Text = "��� 21";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(0, 3901);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 13);
            this.label16.TabIndex = 143;
            this.label16.Text = "��� 22";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(0, 3931);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 13);
            this.label17.TabIndex = 142;
            this.label17.Text = "��� 23";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(0, 3962);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(44, 13);
            this.label18.TabIndex = 141;
            this.label18.Text = "��� 24";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(4, 27);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(67, 13);
            this.label116.TabIndex = 140;
            this.label116.Text = "����. ����.";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(4, 58);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(61, 13);
            this.label117.TabIndex = 139;
            this.label117.Text = "���. ����.";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(4, 89);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(48, 13);
            this.label118.TabIndex = 138;
            this.label118.Text = "������.";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(4, 120);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(44, 13);
            this.label119.TabIndex = 137;
            this.label119.Text = "��. ���.";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(4, 150);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(44, 13);
            this.label120.TabIndex = 136;
            this.label120.Text = "������";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(4, 181);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(52, 13);
            this.label121.TabIndex = 135;
            this.label121.Text = "����-���";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(4, 212);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(35, 13);
            this.label122.TabIndex = 134;
            this.label122.Text = "����.";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(4, 243);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(40, 13);
            this.label123.TabIndex = 133;
            this.label123.Text = "�����";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(4, 275);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(53, 13);
            this.label124.TabIndex = 132;
            this.label124.Text = "��� ���.";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(4, 306);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(59, 13);
            this.label125.TabIndex = 131;
            this.label125.Text = "��� ����.";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(4, 337);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(59, 13);
            this.label126.TabIndex = 130;
            this.label126.Text = "��� ����.";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(4, 368);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(44, 13);
            this.label127.TabIndex = 129;
            this.label127.Text = "������";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(4, 398);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(31, 13);
            this.label128.TabIndex = 128;
            this.label128.Text = "���";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(4, 429);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(37, 13);
            this.label129.TabIndex = 127;
            this.label129.Text = "����";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(4, 460);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(54, 13);
            this.label130.TabIndex = 126;
            this.label130.Text = "���. ���";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(4, 491);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(48, 13);
            this.label131.TabIndex = 125;
            this.label131.Text = "�����-�";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(4, 521);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(25, 13);
            this.label100.TabIndex = 124;
            this.label100.Text = "� 1";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(4, 552);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(25, 13);
            this.label101.TabIndex = 123;
            this.label101.Text = "� 2";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(4, 583);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(25, 13);
            this.label102.TabIndex = 122;
            this.label102.Text = "� 3";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(4, 614);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(25, 13);
            this.label103.TabIndex = 121;
            this.label103.Text = "� 4";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(4, 644);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(25, 13);
            this.label104.TabIndex = 120;
            this.label104.Text = "� 5";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(4, 675);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(25, 13);
            this.label105.TabIndex = 119;
            this.label105.Text = "� 6";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(4, 706);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(25, 13);
            this.label106.TabIndex = 118;
            this.label106.Text = "� 7";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(4, 737);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(25, 13);
            this.label107.TabIndex = 117;
            this.label107.Text = "� 8";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(4, 769);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(25, 13);
            this.label108.TabIndex = 116;
            this.label108.Text = "� 9";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(4, 800);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(31, 13);
            this.label109.TabIndex = 115;
            this.label109.Text = "� 10";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(4, 831);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(31, 13);
            this.label110.TabIndex = 114;
            this.label110.Text = "� 11";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(4, 862);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(31, 13);
            this.label111.TabIndex = 113;
            this.label111.Text = "� 12";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(4, 892);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(31, 13);
            this.label112.TabIndex = 112;
            this.label112.Text = "� 13";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(4, 923);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(31, 13);
            this.label113.TabIndex = 111;
            this.label113.Text = "� 14";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(4, 954);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(31, 13);
            this.label114.TabIndex = 110;
            this.label114.Text = "� 15";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(4, 985);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(31, 13);
            this.label115.TabIndex = 109;
            this.label115.Text = "� 16";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(4, 1017);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(28, 13);
            this.label84.TabIndex = 108;
            this.label84.Text = "��1";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(4, 1048);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(28, 13);
            this.label85.TabIndex = 107;
            this.label85.Text = "��2";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(4, 1079);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(28, 13);
            this.label86.TabIndex = 106;
            this.label86.Text = "��3";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(4, 1110);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(28, 13);
            this.label87.TabIndex = 105;
            this.label87.Text = "��4";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(4, 1140);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(28, 13);
            this.label88.TabIndex = 104;
            this.label88.Text = "��5";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(4, 1171);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(28, 13);
            this.label89.TabIndex = 103;
            this.label89.Text = "��6";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(4, 1202);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(28, 13);
            this.label90.TabIndex = 102;
            this.label90.Text = "��7";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(4, 1233);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(28, 13);
            this.label91.TabIndex = 101;
            this.label91.Text = "��8";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(4, 1265);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(38, 13);
            this.label92.TabIndex = 100;
            this.label92.Text = "��� 1";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(4, 1296);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(38, 13);
            this.label93.TabIndex = 99;
            this.label93.Text = "��� 2";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(4, 1327);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(38, 13);
            this.label94.TabIndex = 98;
            this.label94.Text = "��� 3";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(4, 1358);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(38, 13);
            this.label95.TabIndex = 97;
            this.label95.Text = "��� 4";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(4, 1388);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(38, 13);
            this.label96.TabIndex = 96;
            this.label96.Text = "��� 5";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(4, 1419);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(38, 13);
            this.label97.TabIndex = 95;
            this.label97.Text = "��� 6";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(4, 1450);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(38, 13);
            this.label98.TabIndex = 94;
            this.label98.Text = "��� 7";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(4, 1481);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(38, 13);
            this.label99.TabIndex = 93;
            this.label99.Text = "��� 8";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(4, 1514);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(35, 13);
            this.label68.TabIndex = 92;
            this.label68.Text = "I> ��";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(4, 1545);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(16, 13);
            this.label69.TabIndex = 91;
            this.label69.Text = "I>";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(4, 1576);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(41, 13);
            this.label70.TabIndex = 90;
            this.label70.Text = "I>> ��";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(4, 1607);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(22, 13);
            this.label71.TabIndex = 89;
            this.label71.Text = "I>>";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(3, 1637);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(47, 13);
            this.label72.TabIndex = 88;
            this.label72.Text = "I>>> ��";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(3, 1668);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(28, 13);
            this.label73.TabIndex = 87;
            this.label73.Text = "I>>>";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(3, 1699);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(53, 13);
            this.label74.TabIndex = 86;
            this.label74.Text = "I>>>> ��";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(3, 1730);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(34, 13);
            this.label75.TabIndex = 85;
            this.label75.Text = "I>>>>";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(2, 1762);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(41, 13);
            this.label76.TabIndex = 84;
            this.label76.Text = "I2> ��";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(2, 1793);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(22, 13);
            this.label77.TabIndex = 83;
            this.label77.Text = "I2>";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(2, 1824);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(47, 13);
            this.label78.TabIndex = 82;
            this.label78.Text = "I2>> ��";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(2, 1855);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(28, 13);
            this.label79.TabIndex = 81;
            this.label79.Text = "I2>>";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(2, 1885);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(41, 13);
            this.label80.TabIndex = 80;
            this.label80.Text = "I0> ��";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(2, 1916);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(22, 13);
            this.label81.TabIndex = 79;
            this.label81.Text = "I0>";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(2, 1947);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(47, 13);
            this.label82.TabIndex = 78;
            this.label82.Text = "I0>> ��";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(2, 1978);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(28, 13);
            this.label83.TabIndex = 77;
            this.label83.Text = "I0>>";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(2, 2011);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(41, 13);
            this.label52.TabIndex = 76;
            this.label52.Text = "In> ��";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(2, 2042);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(22, 13);
            this.label53.TabIndex = 75;
            this.label53.Text = "In>";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(2, 2073);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(47, 13);
            this.label54.TabIndex = 74;
            this.label54.Text = "In>> ��";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(2, 2104);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(28, 13);
            this.label55.TabIndex = 73;
            this.label55.Text = "In>>";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(1, 2134);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(41, 13);
            this.label56.TabIndex = 72;
            this.label56.Text = "Ig> ��";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(1, 2165);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(22, 13);
            this.label57.TabIndex = 71;
            this.label57.Text = "Ig>";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(1, 2196);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(49, 13);
            this.label58.TabIndex = 70;
            this.label58.Text = "I1/I2 ��";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(1, 2227);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(30, 13);
            this.label59.TabIndex = 69;
            this.label59.Text = "I1/I2";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(1, 2259);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(38, 13);
            this.label60.TabIndex = 68;
            this.label60.Text = "F> ��";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(1, 2290);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(19, 13);
            this.label61.TabIndex = 67;
            this.label61.Text = "F>";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(1, 2321);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(44, 13);
            this.label62.TabIndex = 66;
            this.label62.Text = "F>> ��";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(1, 2352);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(25, 13);
            this.label63.TabIndex = 65;
            this.label63.Text = "F>>";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(1, 2382);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(38, 13);
            this.label64.TabIndex = 64;
            this.label64.Text = "F< ��";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(1, 2413);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(19, 13);
            this.label65.TabIndex = 63;
            this.label65.Text = "F<";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(1, 2444);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(44, 13);
            this.label66.TabIndex = 62;
            this.label66.Text = "F<< ��";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(1, 2475);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(25, 13);
            this.label67.TabIndex = 61;
            this.label67.Text = "F<<";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(1, 2506);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(40, 13);
            this.label36.TabIndex = 60;
            this.label36.Text = "U> ��";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(1, 2537);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(21, 13);
            this.label37.TabIndex = 59;
            this.label37.Text = "U>";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(1, 2568);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(46, 13);
            this.label38.TabIndex = 58;
            this.label38.Text = "U>> ��";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(1, 2599);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(27, 13);
            this.label39.TabIndex = 57;
            this.label39.Text = "U>>";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(1, 2629);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(40, 13);
            this.label40.TabIndex = 56;
            this.label40.Text = "U< ��";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(1, 2660);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(21, 13);
            this.label41.TabIndex = 55;
            this.label41.Text = "U<";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(1, 2691);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(46, 13);
            this.label42.TabIndex = 54;
            this.label42.Text = "U<< ��";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(1, 2722);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(27, 13);
            this.label43.TabIndex = 53;
            this.label43.Text = "U<<";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(1, 2754);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(46, 13);
            this.label44.TabIndex = 52;
            this.label44.Text = "U2> ��";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(1, 2785);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(27, 13);
            this.label45.TabIndex = 51;
            this.label45.Text = "U2>";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(1, 2816);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(52, 13);
            this.label46.TabIndex = 50;
            this.label46.Text = "U2>> ��";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(1, 2847);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(33, 13);
            this.label47.TabIndex = 49;
            this.label47.Text = "U2>>";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(1, 2877);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(46, 13);
            this.label48.TabIndex = 48;
            this.label48.Text = "U0< ��";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(1, 2908);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(27, 13);
            this.label49.TabIndex = 47;
            this.label49.Text = "U0<";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(1, 2939);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(52, 13);
            this.label50.TabIndex = 46;
            this.label50.Text = "U0<< ��";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(1, 2970);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(33, 13);
            this.label51.TabIndex = 45;
            this.label51.Text = "U0<<";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(1, 3004);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(30, 13);
            this.label20.TabIndex = 44;
            this.label20.Text = "�� 1";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(1, 3035);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(30, 13);
            this.label21.TabIndex = 43;
            this.label21.Text = "�� 2";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(1, 3066);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(30, 13);
            this.label22.TabIndex = 42;
            this.label22.Text = "�� 3";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(1, 3097);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(30, 13);
            this.label23.TabIndex = 41;
            this.label23.Text = "�� 4";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(1, 3127);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(30, 13);
            this.label24.TabIndex = 40;
            this.label24.Text = "�� 5";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(1, 3158);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(30, 13);
            this.label25.TabIndex = 39;
            this.label25.Text = "�� 6";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(1, 3189);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(30, 13);
            this.label26.TabIndex = 38;
            this.label26.Text = "�� 7";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(1, 3220);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(30, 13);
            this.label27.TabIndex = 37;
            this.label27.Text = "�� 8";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(1, 3252);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(38, 13);
            this.label28.TabIndex = 36;
            this.label28.Text = "��� 1";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(1, 3283);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(38, 13);
            this.label29.TabIndex = 35;
            this.label29.Text = "��� 2";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(1, 3314);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(38, 13);
            this.label30.TabIndex = 34;
            this.label30.Text = "��� 3";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(1, 3345);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(38, 13);
            this.label31.TabIndex = 33;
            this.label31.Text = "��� 4";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(1, 3375);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(38, 13);
            this.label32.TabIndex = 32;
            this.label32.Text = "��� 5";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(1, 3406);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(38, 13);
            this.label33.TabIndex = 31;
            this.label33.Text = "��� 6";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(1, 3437);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(38, 13);
            this.label34.TabIndex = 30;
            this.label34.Text = "��� 7";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(1, 3468);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(38, 13);
            this.label35.TabIndex = 29;
            this.label35.Text = "��� 8";
            // 
            // panel8
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.panel8, 2);
            this.panel8.Controls.Add(this.label19);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(971, 29);
            this.panel8.TabIndex = 18;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(445, 8);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(293, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "������� � ��������� \"���������� 0\" �� ������������.";
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 4;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel16.Controls.Add(this.tableLayoutPanel12, 3, 1);
            this.tableLayoutPanel16.Controls.Add(this.UScroll, 2, 1);
            this.tableLayoutPanel16.Controls.Add(this.tableLayoutPanel6, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.panel4, 0, 1);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(0, 203);
            this.tableLayoutPanel16.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 2;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(1037, 203);
            this.tableLayoutPanel16.TabIndex = 33;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel16.SetColumnSpan(this.tableLayoutPanel6, 4);
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Controls.Add(this.button4, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.button29, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.button30, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.button32, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(143, 28);
            this.tableLayoutPanel6.TabIndex = 26;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Red;
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.Location = new System.Drawing.Point(73, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(29, 22);
            this.button4.TabIndex = 13;
            this.button4.Text = "Uc";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.uButton_Click);
            // 
            // button29
            // 
            this.button29.BackColor = System.Drawing.Color.Blue;
            this.button29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button29.Location = new System.Drawing.Point(108, 3);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(32, 22);
            this.button29.TabIndex = 11;
            this.button29.Text = "Un";
            this.button29.UseVisualStyleBackColor = false;
            this.button29.Click += new System.EventHandler(this.uButton_Click);
            // 
            // button30
            // 
            this.button30.BackColor = System.Drawing.Color.Yellow;
            this.button30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button30.Location = new System.Drawing.Point(3, 3);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(29, 22);
            this.button30.TabIndex = 10;
            this.button30.Text = "Ua";
            this.button30.UseVisualStyleBackColor = false;
            this.button30.Click += new System.EventHandler(this.uButton_Click);
            // 
            // button32
            // 
            this.button32.BackColor = System.Drawing.Color.Green;
            this.button32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button32.Location = new System.Drawing.Point(38, 3);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(29, 22);
            this.button32.TabIndex = 9;
            this.button32.Text = "Ub";
            this.button32.UseVisualStyleBackColor = false;
            this.button32.Click += new System.EventHandler(this.uButton_Click);
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 1;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.UYminus, 0, 2);
            this.tableLayoutPanel12.Controls.Add(this.UYPlus, 0, 1);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(1000, 37);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 5;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(34, 163);
            this.tableLayoutPanel12.TabIndex = 34;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.Location = new System.Drawing.Point(9, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 15);
            this.label4.TabIndex = 5;
            this.label4.Text = "Y";
            // 
            // UYminus
            // 
            this.UYminus.BackColor = System.Drawing.Color.ForestGreen;
            this.UYminus.Cursor = System.Windows.Forms.Cursors.Default;
            this.UYminus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UYminus.Enabled = false;
            this.UYminus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.UYminus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UYminus.Location = new System.Drawing.Point(3, 63);
            this.UYminus.Name = "UYminus";
            this.UYminus.Size = new System.Drawing.Size(28, 24);
            this.UYminus.TabIndex = 1;
            this.UYminus.Text = "-";
            this.UYminus.UseVisualStyleBackColor = false;
            this.UYminus.Click += new System.EventHandler(this.UYminus_Click);
            // 
            // UYPlus
            // 
            this.UYPlus.BackColor = System.Drawing.Color.ForestGreen;
            this.UYPlus.Cursor = System.Windows.Forms.Cursors.Default;
            this.UYPlus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UYPlus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.UYPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UYPlus.Location = new System.Drawing.Point(3, 33);
            this.UYPlus.Name = "UYPlus";
            this.UYPlus.Size = new System.Drawing.Size(28, 24);
            this.UYPlus.TabIndex = 0;
            this.UYPlus.Text = "+";
            this.UYPlus.UseVisualStyleBackColor = false;
            this.UYPlus.Click += new System.EventHandler(this.UYPlus_Click);
            // 
            // UScroll
            // 
            this.UScroll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.UScroll.Enabled = false;
            this.UScroll.LargeChange = 1;
            this.UScroll.Location = new System.Drawing.Point(977, 34);
            this.UScroll.Maximum = 10;
            this.UScroll.Minimum = 10;
            this.UScroll.Name = "UScroll";
            this.UScroll.Size = new System.Drawing.Size(17, 169);
            this.UScroll.TabIndex = 30;
            this.UScroll.Value = 10;
            this.UScroll.Scroll += new System.Windows.Forms.ScrollEventHandler(this.UScroll_Scroll);
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel16.SetColumnSpan(this.panel4, 2);
            this.panel4.Controls.Add(this._voltageChart);
            this.panel4.Location = new System.Drawing.Point(3, 37);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(971, 163);
            this.panel4.TabIndex = 35;
            // 
            // _voltageChart
            // 
            this._voltageChart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._voltageChart.BkGradient = false;
            this._voltageChart.BkGradientAngle = 90;
            this._voltageChart.BkGradientColor = System.Drawing.Color.White;
            this._voltageChart.BkGradientRate = 0.5F;
            this._voltageChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._voltageChart.BkRestrictedToChartPanel = false;
            this._voltageChart.BkShinePosition = 1F;
            this._voltageChart.BkTransparency = 0F;
            this._voltageChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._voltageChart.BorderExteriorLength = 0;
            this._voltageChart.BorderGradientAngle = 225;
            this._voltageChart.BorderGradientLightPos1 = 1F;
            this._voltageChart.BorderGradientLightPos2 = -1F;
            this._voltageChart.BorderGradientRate = 0.5F;
            this._voltageChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._voltageChart.BorderLightIntermediateBrightness = 0F;
            this._voltageChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._voltageChart.ChartPanelBackColor = System.Drawing.Color.Silver;
            this._voltageChart.ChartPanelBkTransparency = 0F;
            this._voltageChart.ControlShadow = false;
            this._voltageChart.CoordinateAxesVisible = true;
            this._voltageChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._voltageChart.CoordinateXOrigin = 50D;
            this._voltageChart.CoordinateYMax = 100D;
            this._voltageChart.CoordinateYMin = -100D;
            this._voltageChart.CoordinateYOrigin = 0D;
            this._voltageChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._voltageChart.FooterColor = System.Drawing.Color.Black;
            this._voltageChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._voltageChart.FooterVisible = false;
            this._voltageChart.GridColor = System.Drawing.Color.Gray;
            this._voltageChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._voltageChart.GridVisible = true;
            this._voltageChart.GridXSubTicker = 0;
            this._voltageChart.GridXTicker = 10;
            this._voltageChart.GridYSubTicker = 0;
            this._voltageChart.GridYTicker = 10;
            this._voltageChart.HeaderColor = System.Drawing.Color.Black;
            this._voltageChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._voltageChart.HeaderVisible = false;
            this._voltageChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._voltageChart.InnerBorderLength = 0;
            this._voltageChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._voltageChart.LegendBkColor = System.Drawing.Color.White;
            this._voltageChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._voltageChart.LegendVisible = false;
            this._voltageChart.Location = new System.Drawing.Point(69, -3);
            this._voltageChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._voltageChart.MiddleBorderLength = 0;
            this._voltageChart.Name = "_voltageChart";
            this._voltageChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._voltageChart.OuterBorderLength = 0;
            this._voltageChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._voltageChart.Precision = 0;
            this._voltageChart.RoundRadius = 10;
            this._voltageChart.ShadowColor = System.Drawing.Color.DimGray;
            this._voltageChart.ShadowDepth = 8;
            this._voltageChart.ShadowRate = 0.5F;
            this._voltageChart.Size = new System.Drawing.Size(902, 163);
            this._voltageChart.TabIndex = 34;
            this._voltageChart.Text = "daS_Net_XYChart3";
            this._voltageChart.XMax = 100D;
            this._voltageChart.XMin = 0D;
            this._voltageChart.XScaleColor = System.Drawing.Color.Black;
            this._voltageChart.XScaleVisible = true;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 4;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel13.Controls.Add(this.tableLayoutPanel8, 3, 1);
            this.tableLayoutPanel13.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.panel5, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.S1Scroll, 2, 1);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel13.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 2;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(1037, 203);
            this.tableLayoutPanel13.TabIndex = 27;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.S1Yminus, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.S1YPlus, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(1000, 37);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 4;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(34, 163);
            this.tableLayoutPanel8.TabIndex = 31;
            // 
            // S1Yminus
            // 
            this.S1Yminus.BackColor = System.Drawing.Color.ForestGreen;
            this.S1Yminus.Cursor = System.Windows.Forms.Cursors.Default;
            this.S1Yminus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.S1Yminus.Enabled = false;
            this.S1Yminus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.S1Yminus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.S1Yminus.Location = new System.Drawing.Point(3, 59);
            this.S1Yminus.Name = "S1Yminus";
            this.S1Yminus.Size = new System.Drawing.Size(28, 24);
            this.S1Yminus.TabIndex = 1;
            this.S1Yminus.Text = "-";
            this.S1Yminus.UseVisualStyleBackColor = false;
            this.S1Yminus.Click += new System.EventHandler(this.S1Yminus_Click);
            // 
            // S1YPlus
            // 
            this.S1YPlus.BackColor = System.Drawing.Color.ForestGreen;
            this.S1YPlus.Cursor = System.Windows.Forms.Cursors.Default;
            this.S1YPlus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.S1YPlus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.S1YPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.S1YPlus.Location = new System.Drawing.Point(3, 29);
            this.S1YPlus.Name = "S1YPlus";
            this.S1YPlus.Size = new System.Drawing.Size(28, 24);
            this.S1YPlus.TabIndex = 0;
            this.S1YPlus.Text = "+";
            this.S1YPlus.UseVisualStyleBackColor = false;
            this.S1YPlus.Click += new System.EventHandler(this.S1YPlus_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.Location = new System.Drawing.Point(9, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Y";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel13.SetColumnSpan(this.tableLayoutPanel3, 4);
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.button1, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.button37, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.button38, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.button39, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(143, 28);
            this.tableLayoutPanel3.TabIndex = 23;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(73, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 22);
            this.button1.TabIndex = 13;
            this.button1.Text = "Ic";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.s1Button_Click);
            // 
            // button37
            // 
            this.button37.BackColor = System.Drawing.Color.Blue;
            this.button37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button37.Location = new System.Drawing.Point(108, 3);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(32, 22);
            this.button37.TabIndex = 11;
            this.button37.Text = "In";
            this.button37.UseVisualStyleBackColor = false;
            this.button37.Click += new System.EventHandler(this.s1Button_Click);
            // 
            // button38
            // 
            this.button38.BackColor = System.Drawing.Color.Yellow;
            this.button38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button38.Location = new System.Drawing.Point(3, 3);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(29, 22);
            this.button38.TabIndex = 10;
            this.button38.Text = "Ia";
            this.button38.UseVisualStyleBackColor = false;
            this.button38.Click += new System.EventHandler(this.s1Button_Click);
            // 
            // button39
            // 
            this.button39.BackColor = System.Drawing.Color.Green;
            this.button39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button39.Location = new System.Drawing.Point(38, 3);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(29, 22);
            this.button39.TabIndex = 9;
            this.button39.Text = "Ib";
            this.button39.UseVisualStyleBackColor = false;
            this.button39.Click += new System.EventHandler(this.s1Button_Click);
            // 
            // panel5
            // 
            this.tableLayoutPanel13.SetColumnSpan(this.panel5, 2);
            this.panel5.Controls.Add(this._s1TokChart);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 37);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(971, 163);
            this.panel5.TabIndex = 33;
            // 
            // _s1TokChart
            // 
            this._s1TokChart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._s1TokChart.BkGradient = false;
            this._s1TokChart.BkGradientAngle = 90;
            this._s1TokChart.BkGradientColor = System.Drawing.Color.White;
            this._s1TokChart.BkGradientRate = 0.5F;
            this._s1TokChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._s1TokChart.BkRestrictedToChartPanel = false;
            this._s1TokChart.BkShinePosition = 1F;
            this._s1TokChart.BkTransparency = 0F;
            this._s1TokChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._s1TokChart.BorderExteriorLength = 0;
            this._s1TokChart.BorderGradientAngle = 225;
            this._s1TokChart.BorderGradientLightPos1 = 1F;
            this._s1TokChart.BorderGradientLightPos2 = -1F;
            this._s1TokChart.BorderGradientRate = 0.5F;
            this._s1TokChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._s1TokChart.BorderLightIntermediateBrightness = 0F;
            this._s1TokChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._s1TokChart.ChartPanelBackColor = System.Drawing.Color.Silver;
            this._s1TokChart.ChartPanelBkTransparency = 0F;
            this._s1TokChart.ControlShadow = false;
            this._s1TokChart.CoordinateAxesVisible = true;
            this._s1TokChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._s1TokChart.CoordinateXOrigin = 50D;
            this._s1TokChart.CoordinateYMax = 100D;
            this._s1TokChart.CoordinateYMin = -100D;
            this._s1TokChart.CoordinateYOrigin = 0D;
            this._s1TokChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._s1TokChart.FooterColor = System.Drawing.Color.Black;
            this._s1TokChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._s1TokChart.FooterVisible = false;
            this._s1TokChart.GridColor = System.Drawing.Color.Gray;
            this._s1TokChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._s1TokChart.GridVisible = true;
            this._s1TokChart.GridXSubTicker = 0;
            this._s1TokChart.GridXTicker = 10;
            this._s1TokChart.GridYSubTicker = 0;
            this._s1TokChart.GridYTicker = 10;
            this._s1TokChart.HeaderColor = System.Drawing.Color.Black;
            this._s1TokChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._s1TokChart.HeaderVisible = false;
            this._s1TokChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._s1TokChart.InnerBorderLength = 0;
            this._s1TokChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._s1TokChart.LegendBkColor = System.Drawing.Color.White;
            this._s1TokChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._s1TokChart.LegendVisible = false;
            this._s1TokChart.Location = new System.Drawing.Point(69, 0);
            this._s1TokChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._s1TokChart.MiddleBorderLength = 0;
            this._s1TokChart.Name = "_s1TokChart";
            this._s1TokChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._s1TokChart.OuterBorderLength = 0;
            this._s1TokChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._s1TokChart.Precision = 0;
            this._s1TokChart.RoundRadius = 10;
            this._s1TokChart.ShadowColor = System.Drawing.Color.DimGray;
            this._s1TokChart.ShadowDepth = 8;
            this._s1TokChart.ShadowRate = 0.5F;
            this._s1TokChart.Size = new System.Drawing.Size(902, 163);
            this._s1TokChart.TabIndex = 36;
            this._s1TokChart.Text = "daS_Net_XYChart1";
            this._s1TokChart.XMax = 100D;
            this._s1TokChart.XMin = 0D;
            this._s1TokChart.XScaleColor = System.Drawing.Color.Black;
            this._s1TokChart.XScaleVisible = true;
            // 
            // S1Scroll
            // 
            this.S1Scroll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.S1Scroll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.S1Scroll.Enabled = false;
            this.S1Scroll.LargeChange = 1;
            this.S1Scroll.Location = new System.Drawing.Point(979, 34);
            this.S1Scroll.Minimum = 100;
            this.S1Scroll.Name = "S1Scroll";
            this.S1Scroll.Size = new System.Drawing.Size(16, 169);
            this.S1Scroll.TabIndex = 32;
            this.S1Scroll.Value = 100;
            this.S1Scroll.Scroll += new System.Windows.Forms.ScrollEventHandler(this.S1Scroll_Scroll);
            // 
            // MarkersTable
            // 
            this.MarkersTable.BackColor = System.Drawing.Color.LightGray;
            this.MarkersTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.MarkersTable.ColumnCount = 2;
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.MarkersTable.Controls.Add(this.Marker1CB, 5, 0);
            this.MarkersTable.Controls.Add(this.Marker2CB, 5, 1);
            this.MarkersTable.Controls.Add(this.panel6, 0, 0);
            this.MarkersTable.Controls.Add(this.panel7, 0, 1);
            this.MarkersTable.Dock = System.Windows.Forms.DockStyle.Top;
            this.MarkersTable.Location = new System.Drawing.Point(0, 0);
            this.MarkersTable.Margin = new System.Windows.Forms.Padding(0);
            this.MarkersTable.Name = "MarkersTable";
            this.MarkersTable.RowCount = 2;
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.78571F));
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.21429F));
            this.MarkersTable.Size = new System.Drawing.Size(1176, 59);
            this.MarkersTable.TabIndex = 31;
            this.MarkersTable.Visible = false;
            // 
            // Marker1CB
            // 
            this.Marker1CB.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Marker1CB.AutoSize = true;
            this.Marker1CB.BackColor = System.Drawing.Color.LightGray;
            this.Marker1CB.Location = new System.Drawing.Point(1120, 6);
            this.Marker1CB.Name = "Marker1CB";
            this.Marker1CB.Size = new System.Drawing.Size(44, 17);
            this.Marker1CB.TabIndex = 0;
            this.Marker1CB.Text = "� 1";
            this.Marker1CB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Marker1CB.UseVisualStyleBackColor = false;
            this.Marker1CB.CheckedChanged += new System.EventHandler(this.Marker1CB_CheckedChanged);
            // 
            // Marker2CB
            // 
            this.Marker2CB.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Marker2CB.AutoSize = true;
            this.Marker2CB.BackColor = System.Drawing.Color.LightGray;
            this.Marker2CB.Location = new System.Drawing.Point(1120, 35);
            this.Marker2CB.Name = "Marker2CB";
            this.Marker2CB.Size = new System.Drawing.Size(44, 17);
            this.Marker2CB.TabIndex = 1;
            this.Marker2CB.Text = "� 2";
            this.Marker2CB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Marker2CB.UseVisualStyleBackColor = false;
            this.Marker2CB.CheckedChanged += new System.EventHandler(this.Marker2CB_CheckedChanged);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this._positionM1End);
            this.panel6.Controls.Add(this._positionM1Start);
            this.panel6.Controls.Add(this._zoom1ChB);
            this.panel6.Controls.Add(this.Marker1TrackBar);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(4, 4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1102, 22);
            this.panel6.TabIndex = 4;
            // 
            // _positionM1End
            // 
            this._positionM1End.BackColor = System.Drawing.Color.Red;
            this._positionM1End.Location = new System.Drawing.Point(461, 0);
            this._positionM1End.Name = "_positionM1End";
            this._positionM1End.Size = new System.Drawing.Size(1, 25);
            this._positionM1End.TabIndex = 8;
            this._positionM1End.Text = "|";
            this._positionM1End.Visible = false;
            // 
            // _positionM1Start
            // 
            this._positionM1Start.BackColor = System.Drawing.Color.Red;
            this._positionM1Start.Location = new System.Drawing.Point(282, 0);
            this._positionM1Start.Name = "_positionM1Start";
            this._positionM1Start.Size = new System.Drawing.Size(1, 25);
            this._positionM1Start.TabIndex = 7;
            this._positionM1Start.Text = "|";
            this._positionM1Start.Visible = false;
            // 
            // _zoom1ChB
            // 
            this._zoom1ChB.BackColor = System.Drawing.Color.LightGray;
            this._zoom1ChB.Location = new System.Drawing.Point(4, 3);
            this._zoom1ChB.Name = "_zoom1ChB";
            this._zoom1ChB.Size = new System.Drawing.Size(37, 17);
            this._zoom1ChB.TabIndex = 4;
            this._zoom1ChB.Text = "x1";
            this._zoom1ChB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._zoom1ChB.UseVisualStyleBackColor = true;
            this._zoom1ChB.CheckedChanged += new System.EventHandler(this._zoom1ChB_CheckedChanged);
            // 
            // Marker1TrackBar
            // 
            this.Marker1TrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Marker1TrackBar.BackColor = System.Drawing.Color.Silver;
            this.Marker1TrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Marker1TrackBar.Location = new System.Drawing.Point(57, -1);
            this.Marker1TrackBar.Name = "Marker1TrackBar";
            this.Marker1TrackBar.Size = new System.Drawing.Size(1045, 45);
            this.Marker1TrackBar.TabIndex = 3;
            this.Marker1TrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.Marker1TrackBar.Scroll += new System.EventHandler(this.Marker1TrackBar_Scroll);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this._positionM2End);
            this.panel7.Controls.Add(this._positionM2Start);
            this.panel7.Controls.Add(this._zoom2ChB);
            this.panel7.Controls.Add(this.Marker2TrackBar);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(4, 33);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1102, 22);
            this.panel7.TabIndex = 5;
            // 
            // _positionM2End
            // 
            this._positionM2End.BackColor = System.Drawing.Color.Red;
            this._positionM2End.Location = new System.Drawing.Point(461, 0);
            this._positionM2End.Name = "_positionM2End";
            this._positionM2End.Size = new System.Drawing.Size(1, 25);
            this._positionM2End.TabIndex = 7;
            this._positionM2End.Text = "|";
            this._positionM2End.Visible = false;
            // 
            // _positionM2Start
            // 
            this._positionM2Start.BackColor = System.Drawing.Color.Red;
            this._positionM2Start.Location = new System.Drawing.Point(282, 0);
            this._positionM2Start.Name = "_positionM2Start";
            this._positionM2Start.Size = new System.Drawing.Size(1, 25);
            this._positionM2Start.TabIndex = 6;
            this._positionM2Start.Text = "|";
            this._positionM2Start.Visible = false;
            // 
            // _zoom2ChB
            // 
            this._zoom2ChB.BackColor = System.Drawing.Color.LightGray;
            this._zoom2ChB.Location = new System.Drawing.Point(4, 3);
            this._zoom2ChB.Name = "_zoom2ChB";
            this._zoom2ChB.Size = new System.Drawing.Size(38, 17);
            this._zoom2ChB.TabIndex = 5;
            this._zoom2ChB.Text = "x1";
            this._zoom2ChB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._zoom2ChB.UseVisualStyleBackColor = true;
            this._zoom2ChB.CheckedChanged += new System.EventHandler(this._zoom2ChB_CheckedChanged);
            // 
            // Marker2TrackBar
            // 
            this.Marker2TrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Marker2TrackBar.BackColor = System.Drawing.Color.Silver;
            this.Marker2TrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Marker2TrackBar.LargeChange = 0;
            this.Marker2TrackBar.Location = new System.Drawing.Point(57, -1);
            this.Marker2TrackBar.Maximum = 3400;
            this.Marker2TrackBar.Name = "Marker2TrackBar";
            this.Marker2TrackBar.Size = new System.Drawing.Size(1045, 45);
            this.Marker2TrackBar.TabIndex = 4;
            this.Marker2TrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.Marker2TrackBar.Scroll += new System.EventHandler(this.Marker2TrackBar_Scroll);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(1185, 28);
            this.panel3.Name = "panel3";
            this.MAINTABLE.SetRowSpan(this.panel3, 2);
            this.panel3.Size = new System.Drawing.Size(1, 741);
            this.panel3.TabIndex = 33;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.LightGray;
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox1.Size = new System.Drawing.Size(1, 741);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "���������� ��������";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBox7);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox4.Location = new System.Drawing.Point(0, 647);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1, 94);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "�����";
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.DeltaLabel);
            this.groupBox7.Location = new System.Drawing.Point(6, 52);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(0, 36);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "������";
            // 
            // DeltaLabel
            // 
            this.DeltaLabel.AutoSize = true;
            this.DeltaLabel.Location = new System.Drawing.Point(94, 13);
            this.DeltaLabel.Name = "DeltaLabel";
            this.DeltaLabel.Size = new System.Drawing.Size(0, 13);
            this.DeltaLabel.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox6.Controls.Add(this.TimeMarker2);
            this.groupBox6.Location = new System.Drawing.Point(151, 13);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(134, 33);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "������ 2";
            // 
            // TimeMarker2
            // 
            this.TimeMarker2.AutoSize = true;
            this.TimeMarker2.Location = new System.Drawing.Point(34, 14);
            this.TimeMarker2.Name = "TimeMarker2";
            this.TimeMarker2.Size = new System.Drawing.Size(0, 13);
            this.TimeMarker2.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox5.Controls.Add(this.TimeMarker1);
            this.groupBox5.Location = new System.Drawing.Point(6, 13);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(135, 33);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "������ 1";
            // 
            // TimeMarker1
            // 
            this.TimeMarker1.AutoSize = true;
            this.TimeMarker1.Location = new System.Drawing.Point(30, 14);
            this.TimeMarker1.Name = "TimeMarker1";
            this.TimeMarker1.Size = new System.Drawing.Size(0, 13);
            this.TimeMarker1.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.groupBox10);
            this.groupBox3.Controls.Add(this.groupBox13);
            this.groupBox3.Controls.Add(this.groupBox17);
            this.groupBox3.Location = new System.Drawing.Point(151, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(134, 630);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "������2";
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox10.Controls.Add(this._m2DiscretPanel);
            this.groupBox10.Location = new System.Drawing.Point(6, 168);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(121, 452);
            this.groupBox10.TabIndex = 45;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "��������";
            // 
            // _m2DiscretPanel
            // 
            this._m2DiscretPanel.AutoScroll = true;
            this._m2DiscretPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._m2DiscretPanel.Location = new System.Drawing.Point(3, 16);
            this._m2DiscretPanel.Name = "_m2DiscretPanel";
            this._m2DiscretPanel.Size = new System.Drawing.Size(115, 433);
            this._m2DiscretPanel.TabIndex = 0;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.Marker2Un);
            this.groupBox13.Controls.Add(this.Marker2Uc);
            this.groupBox13.Controls.Add(this.Marker2Ub);
            this.groupBox13.Controls.Add(this.Marker2Ua);
            this.groupBox13.Location = new System.Drawing.Point(6, 90);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(121, 72);
            this.groupBox13.TabIndex = 48;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "����������";
            // 
            // Marker2Un
            // 
            this.Marker2Un.AutoSize = true;
            this.Marker2Un.Location = new System.Drawing.Point(6, 53);
            this.Marker2Un.Name = "Marker2Un";
            this.Marker2Un.Size = new System.Drawing.Size(10, 13);
            this.Marker2Un.TabIndex = 19;
            this.Marker2Un.Text = " ";
            // 
            // Marker2Uc
            // 
            this.Marker2Uc.AutoSize = true;
            this.Marker2Uc.Location = new System.Drawing.Point(6, 40);
            this.Marker2Uc.Name = "Marker2Uc";
            this.Marker2Uc.Size = new System.Drawing.Size(10, 13);
            this.Marker2Uc.TabIndex = 18;
            this.Marker2Uc.Text = " ";
            // 
            // Marker2Ub
            // 
            this.Marker2Ub.AutoSize = true;
            this.Marker2Ub.Location = new System.Drawing.Point(6, 27);
            this.Marker2Ub.Name = "Marker2Ub";
            this.Marker2Ub.Size = new System.Drawing.Size(10, 13);
            this.Marker2Ub.TabIndex = 17;
            this.Marker2Ub.Text = " ";
            // 
            // Marker2Ua
            // 
            this.Marker2Ua.AutoSize = true;
            this.Marker2Ua.Location = new System.Drawing.Point(6, 12);
            this.Marker2Ua.Name = "Marker2Ua";
            this.Marker2Ua.Size = new System.Drawing.Size(10, 13);
            this.Marker2Ua.TabIndex = 16;
            this.Marker2Ua.Text = " ";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.Marker2S1In);
            this.groupBox17.Controls.Add(this.Marker2S1Ic);
            this.groupBox17.Controls.Add(this.Marker2S1Ib);
            this.groupBox17.Controls.Add(this.Marker2S1Ia);
            this.groupBox17.Location = new System.Drawing.Point(6, 12);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(121, 72);
            this.groupBox17.TabIndex = 45;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "����";
            // 
            // Marker2S1In
            // 
            this.Marker2S1In.AutoSize = true;
            this.Marker2S1In.Location = new System.Drawing.Point(6, 53);
            this.Marker2S1In.Name = "Marker2S1In";
            this.Marker2S1In.Size = new System.Drawing.Size(10, 13);
            this.Marker2S1In.TabIndex = 7;
            this.Marker2S1In.Text = " ";
            // 
            // Marker2S1Ic
            // 
            this.Marker2S1Ic.AutoSize = true;
            this.Marker2S1Ic.Location = new System.Drawing.Point(6, 40);
            this.Marker2S1Ic.Name = "Marker2S1Ic";
            this.Marker2S1Ic.Size = new System.Drawing.Size(10, 13);
            this.Marker2S1Ic.TabIndex = 6;
            this.Marker2S1Ic.Text = " ";
            // 
            // Marker2S1Ib
            // 
            this.Marker2S1Ib.AutoSize = true;
            this.Marker2S1Ib.Location = new System.Drawing.Point(6, 27);
            this.Marker2S1Ib.Name = "Marker2S1Ib";
            this.Marker2S1Ib.Size = new System.Drawing.Size(10, 13);
            this.Marker2S1Ib.TabIndex = 5;
            this.Marker2S1Ib.Text = " ";
            // 
            // Marker2S1Ia
            // 
            this.Marker2S1Ia.AutoSize = true;
            this.Marker2S1Ia.Location = new System.Drawing.Point(6, 12);
            this.Marker2S1Ia.Name = "Marker2S1Ia";
            this.Marker2S1Ia.Size = new System.Drawing.Size(10, 13);
            this.Marker2S1Ia.TabIndex = 4;
            this.Marker2S1Ia.Text = " ";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.groupBox9);
            this.groupBox2.Controls.Add(this.groupBox11);
            this.groupBox2.Controls.Add(this.groupBox8);
            this.groupBox2.Location = new System.Drawing.Point(7, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(134, 630);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "������1";
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox9.Controls.Add(this._m1DiscretPanel);
            this.groupBox9.Location = new System.Drawing.Point(6, 168);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(122, 452);
            this.groupBox9.TabIndex = 44;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "��������";
            // 
            // _m1DiscretPanel
            // 
            this._m1DiscretPanel.AutoScroll = true;
            this._m1DiscretPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._m1DiscretPanel.Location = new System.Drawing.Point(3, 16);
            this._m1DiscretPanel.Name = "_m1DiscretPanel";
            this._m1DiscretPanel.Size = new System.Drawing.Size(116, 433);
            this._m1DiscretPanel.TabIndex = 0;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.Marker1Un);
            this.groupBox11.Controls.Add(this.Marker1Uc);
            this.groupBox11.Controls.Add(this.Marker1Ub);
            this.groupBox11.Controls.Add(this.Marker1Ua);
            this.groupBox11.Location = new System.Drawing.Point(6, 90);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(122, 72);
            this.groupBox11.TabIndex = 43;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "����������";
            // 
            // Marker1Un
            // 
            this.Marker1Un.AutoSize = true;
            this.Marker1Un.Location = new System.Drawing.Point(6, 53);
            this.Marker1Un.Name = "Marker1Un";
            this.Marker1Un.Size = new System.Drawing.Size(10, 13);
            this.Marker1Un.TabIndex = 19;
            this.Marker1Un.Text = " ";
            // 
            // Marker1Uc
            // 
            this.Marker1Uc.AutoSize = true;
            this.Marker1Uc.Location = new System.Drawing.Point(6, 40);
            this.Marker1Uc.Name = "Marker1Uc";
            this.Marker1Uc.Size = new System.Drawing.Size(10, 13);
            this.Marker1Uc.TabIndex = 18;
            this.Marker1Uc.Text = " ";
            // 
            // Marker1Ub
            // 
            this.Marker1Ub.AutoSize = true;
            this.Marker1Ub.Location = new System.Drawing.Point(6, 27);
            this.Marker1Ub.Name = "Marker1Ub";
            this.Marker1Ub.Size = new System.Drawing.Size(10, 13);
            this.Marker1Ub.TabIndex = 17;
            this.Marker1Ub.Text = " ";
            // 
            // Marker1Ua
            // 
            this.Marker1Ua.AutoSize = true;
            this.Marker1Ua.Location = new System.Drawing.Point(6, 12);
            this.Marker1Ua.Name = "Marker1Ua";
            this.Marker1Ua.Size = new System.Drawing.Size(10, 13);
            this.Marker1Ua.TabIndex = 16;
            this.Marker1Ua.Text = " ";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.Marker1S1In);
            this.groupBox8.Controls.Add(this.Marker1S1Ic);
            this.groupBox8.Controls.Add(this.Marker1S1Ib);
            this.groupBox8.Controls.Add(this.Marker1S1Ia);
            this.groupBox8.Location = new System.Drawing.Point(5, 12);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(123, 72);
            this.groupBox8.TabIndex = 40;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "����";
            // 
            // Marker1S1In
            // 
            this.Marker1S1In.AutoSize = true;
            this.Marker1S1In.Location = new System.Drawing.Point(6, 53);
            this.Marker1S1In.Name = "Marker1S1In";
            this.Marker1S1In.Size = new System.Drawing.Size(10, 13);
            this.Marker1S1In.TabIndex = 7;
            this.Marker1S1In.Text = " ";
            // 
            // Marker1S1Ic
            // 
            this.Marker1S1Ic.AutoSize = true;
            this.Marker1S1Ic.Location = new System.Drawing.Point(6, 40);
            this.Marker1S1Ic.Name = "Marker1S1Ic";
            this.Marker1S1Ic.Size = new System.Drawing.Size(10, 13);
            this.Marker1S1Ic.TabIndex = 6;
            this.Marker1S1Ic.Text = " ";
            // 
            // Marker1S1Ib
            // 
            this.Marker1S1Ib.AutoSize = true;
            this.Marker1S1Ib.Location = new System.Drawing.Point(6, 27);
            this.Marker1S1Ib.Name = "Marker1S1Ib";
            this.Marker1S1Ib.Size = new System.Drawing.Size(10, 13);
            this.Marker1S1Ib.TabIndex = 5;
            this.Marker1S1Ib.Text = " ";
            // 
            // Marker1S1Ia
            // 
            this.Marker1S1Ia.AutoSize = true;
            this.Marker1S1Ia.Location = new System.Drawing.Point(6, 12);
            this.Marker1S1Ia.Name = "Marker1S1Ia";
            this.Marker1S1Ia.Size = new System.Drawing.Size(0, 13);
            this.Marker1S1Ia.TabIndex = 4;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.button43, 0, 1);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel9.TabIndex = 0;
            // 
            // button43
            // 
            this.button43.BackColor = System.Drawing.Color.ForestGreen;
            this.button43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button43.Location = new System.Drawing.Point(3, 23);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(194, 74);
            this.button43.TabIndex = 1;
            this.button43.Text = "-";
            this.button43.UseVisualStyleBackColor = false;
            // 
            // button44
            // 
            this.button44.BackColor = System.Drawing.Color.ForestGreen;
            this.button44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button44.Location = new System.Drawing.Point(3, 3);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(28, 24);
            this.button44.TabIndex = 0;
            this.button44.Text = "+";
            this.button44.UseVisualStyleBackColor = false;
            // 
            // _avaryOscCheck
            // 
            this._avaryOscCheck.BackColor = System.Drawing.Color.Silver;
            this._avaryOscCheck.Location = new System.Drawing.Point(443, 4);
            this._avaryOscCheck.Name = "_avaryOscCheck";
            this._avaryOscCheck.Size = new System.Drawing.Size(63, 16);
            this._avaryOscCheck.TabIndex = 1;
            this._avaryOscCheck.Text = "�.���";
            this._avaryOscCheck.UseVisualStyleBackColor = false;
            this._avaryOscCheck.CheckedChanged += new System.EventHandler(this._avaryOscCheck_CheckedChanged);
            // 
            // Xplus
            // 
            this.Xplus.BackColor = System.Drawing.Color.ForestGreen;
            this.Xplus.Cursor = System.Windows.Forms.Cursors.Default;
            this.Xplus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Xplus.Location = new System.Drawing.Point(149, 2);
            this.Xplus.Name = "Xplus";
            this.Xplus.Size = new System.Drawing.Size(33, 20);
            this.Xplus.TabIndex = 2;
            this.Xplus.Text = "X -";
            this.Xplus.UseVisualStyleBackColor = false;
            this.Xplus.Click += new System.EventHandler(this.Xplus_Click);
            // 
            // Xminus
            // 
            this.Xminus.BackColor = System.Drawing.Color.ForestGreen;
            this.Xminus.Cursor = System.Windows.Forms.Cursors.Default;
            this.Xminus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Xminus.Location = new System.Drawing.Point(107, 2);
            this.Xminus.Name = "Xminus";
            this.Xminus.Size = new System.Drawing.Size(33, 20);
            this.Xminus.TabIndex = 3;
            this.Xminus.Text = "X +";
            this.Xminus.UseVisualStyleBackColor = false;
            this.Xminus.Click += new System.EventHandler(this.Xminus_Click);
            // 
            // S1ShowCB
            // 
            this.S1ShowCB.BackColor = System.Drawing.Color.Silver;
            this.S1ShowCB.Checked = true;
            this.S1ShowCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.S1ShowCB.Location = new System.Drawing.Point(210, 4);
            this.S1ShowCB.Name = "S1ShowCB";
            this.S1ShowCB.Size = new System.Drawing.Size(77, 16);
            this.S1ShowCB.TabIndex = 4;
            this.S1ShowCB.Text = "����";
            this.S1ShowCB.UseVisualStyleBackColor = false;
            this.S1ShowCB.CheckedChanged += new System.EventHandler(this.S1ShowCB_CheckedChanged);
            // 
            // UShowCB
            // 
            this.UShowCB.BackColor = System.Drawing.Color.Silver;
            this.UShowCB.Checked = true;
            this.UShowCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.UShowCB.Location = new System.Drawing.Point(269, 4);
            this.UShowCB.Name = "UShowCB";
            this.UShowCB.Size = new System.Drawing.Size(90, 16);
            this.UShowCB.TabIndex = 7;
            this.UShowCB.Text = "����������";
            this.UShowCB.UseVisualStyleBackColor = false;
            this.UShowCB.CheckedChanged += new System.EventHandler(this.UShowCB_CheckedChanged);
            // 
            // DiscretShowCB
            // 
            this.DiscretShowCB.BackColor = System.Drawing.Color.Silver;
            this.DiscretShowCB.Checked = true;
            this.DiscretShowCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DiscretShowCB.Location = new System.Drawing.Point(363, 4);
            this.DiscretShowCB.Name = "DiscretShowCB";
            this.DiscretShowCB.Size = new System.Drawing.Size(80, 16);
            this.DiscretShowCB.TabIndex = 8;
            this.DiscretShowCB.Text = "��������";
            this.DiscretShowCB.UseVisualStyleBackColor = false;
            this.DiscretShowCB.CheckedChanged += new System.EventHandler(this.DiscretShowCB_CheckedChanged);
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ShowAlways = true;
            this.toolTip1.StripAmpersands = true;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // MarkerCB
            // 
            this.MarkerCB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MarkerCB.BackColor = System.Drawing.Color.Silver;
            this.MarkerCB.Location = new System.Drawing.Point(1103, 4);
            this.MarkerCB.Name = "MarkerCB";
            this.MarkerCB.Size = new System.Drawing.Size(73, 17);
            this.MarkerCB.TabIndex = 11;
            this.MarkerCB.Text = "�������";
            this.MarkerCB.UseVisualStyleBackColor = false;
            this.MarkerCB.CheckedChanged += new System.EventHandler(this.MarkerCB_CheckedChanged);
            // 
            // FlagCB
            // 
            this.FlagCB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FlagCB.BackColor = System.Drawing.Color.Silver;
            this.FlagCB.Location = new System.Drawing.Point(1040, 4);
            this.FlagCB.Name = "FlagCB";
            this.FlagCB.Size = new System.Drawing.Size(58, 17);
            this.FlagCB.TabIndex = 12;
            this.FlagCB.Text = "�����";
            this.FlagCB.UseVisualStyleBackColor = false;
            // 
            // _avaryJournalCheck
            // 
            this._avaryJournalCheck.BackColor = System.Drawing.Color.Silver;
            this._avaryJournalCheck.Location = new System.Drawing.Point(505, 4);
            this._avaryJournalCheck.Name = "_avaryJournalCheck";
            this._avaryJournalCheck.Size = new System.Drawing.Size(53, 17);
            this._avaryJournalCheck.TabIndex = 13;
            this._avaryJournalCheck.Text = "�.��";
            this._avaryJournalCheck.UseVisualStyleBackColor = false;
            this._avaryJournalCheck.Visible = false;
            // 
            // VVOscilloscopeResultForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1182, 772);
            this.Controls.Add(this._avaryJournalCheck);
            this.Controls.Add(this.FlagCB);
            this.Controls.Add(this.MarkerCB);
            this.Controls.Add(this.DiscretShowCB);
            this.Controls.Add(this.UShowCB);
            this.Controls.Add(this.S1ShowCB);
            this.Controls.Add(this.Xminus);
            this.Controls.Add(this.Xplus);
            this.Controls.Add(this._avaryOscCheck);
            this.Controls.Add(this.MAINTABLE);
            this.Name = "VVOscilloscopeResultForm";
            this.Text = "�������������";
            this.Load += new System.EventHandler(this.OscilloscopeResultForm_Load);
            this.MAINTABLE.ResumeLayout(false);
            this.MAINTABLE.PerformLayout();
            this.MAINPANEL.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.discrPanel.ResumeLayout(false);
            this.discrPanel.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.MarkersTable.ResumeLayout(false);
            this.MarkersTable.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Marker1TrackBar)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Marker2TrackBar)).EndInit();
            this.panel3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel MAINTABLE;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.HScrollBar hScrollBar4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.CheckBox _avaryOscCheck;
        private System.Windows.Forms.Button Xplus;
        private System.Windows.Forms.Button Xminus;
        private System.Windows.Forms.CheckBox S1ShowCB;
        private System.Windows.Forms.CheckBox UShowCB;
        private System.Windows.Forms.CheckBox DiscretShowCB;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel MAINPANEL;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.VScrollBar S1Scroll;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button S1Yminus;
        private System.Windows.Forms.Button S1YPlus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel MarkersTable;
        private System.Windows.Forms.CheckBox Marker1CB;
        private System.Windows.Forms.CheckBox Marker2CB;
        private System.Windows.Forms.CheckBox MarkerCB;
        private System.Windows.Forms.CheckBox FlagCB;
        private System.Windows.Forms.CheckBox _avaryJournalCheck;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label Marker1S1In;
        private System.Windows.Forms.Label Marker1S1Ic;
        private System.Windows.Forms.Label Marker1S1Ib;
        private System.Windows.Forms.Label Marker1S1Ia;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label Marker1Un;
        private System.Windows.Forms.Label Marker1Uc;
        private System.Windows.Forms.Label Marker1Ub;
        private System.Windows.Forms.Label Marker1Ua;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label Marker2Un;
        private System.Windows.Forms.Label Marker2Uc;
        private System.Windows.Forms.Label Marker2Ub;
        private System.Windows.Forms.Label Marker2Ua;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Label Marker2S1In;
        private System.Windows.Forms.Label Marker2S1Ic;
        private System.Windows.Forms.Label Marker2S1Ib;
        private System.Windows.Forms.Label Marker2S1Ia;
        private System.Windows.Forms.Label TimeMarker2;
        private System.Windows.Forms.Label TimeMarker1;
        private System.Windows.Forms.Label DeltaLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Panel discrPanel;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TrackBar Marker1TrackBar;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TrackBar Marker2TrackBar;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox _zoom1ChB;
        private System.Windows.Forms.CheckBox _zoom2ChB;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label _positionM1End;
        private System.Windows.Forms.Label _positionM1Start;
        private System.Windows.Forms.Label _positionM2End;
        private System.Windows.Forms.Label _positionM2Start;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Panel _m2DiscretPanel;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Panel _m1DiscretPanel;
        private BEMN_XY_Chart.DAS_Net_XYChart _discretChart;
        private BEMN_XY_Chart.DAS_Net_XYChart _s1TokChart;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button UYminus;
        private System.Windows.Forms.Button UYPlus;
        private System.Windows.Forms.VScrollBar UScroll;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Panel panel4;
        private BEMN_XY_Chart.DAS_Net_XYChart _voltageChart;









    }
}