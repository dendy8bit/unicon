﻿using System;
using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.TZL.OldOsc.Structers
{
    public class OscJournalStruct: StructBase
    {
        #region [Constants]
        private const string DATE_PATTERN = "{0:d2}.{1:d2}.{2:d2}";
        private const string TIME_PATTERN = "{0:d2}:{1:d2}:{2:d2}.{3:d2}";
        private const string NUMBER_PATTERN = "{0}";
        #endregion [Constants]

        public static int RecordIndex;

        #region [Private fields]
        [Layout(0)]
        private ushort _resstage;
        [Layout(1)]
        private ushort _year;
        [Layout(2)]
        private ushort _month;
        [Layout(3)]
        private ushort _date;
        [Layout(4)]
        private ushort _hour;
        [Layout(5)]
        private ushort _minute;
        [Layout(6)]
        private ushort _second;
        [Layout(7)]
        private ushort _millisecond;
        /// <summary>
        /// Частота дискретизации
        /// </summary>
        [Layout(8)]
        private ushort _fDiscret;
        /// <summary>
        /// Количество аналоговых сигналов
        /// </summary>
        [Layout(9)]
        private ushort _aCount;
        /// <summary>
        /// Количество дискретных сигналов
        /// </summary>
        [Layout(10)]
        private ushort _dCount;
        /// <summary>
        /// Размер осциллограммы (p´), в словах
        /// </summary>
        [Layout(11)]
        private ushort _len;
        /// <summary>
        /// Положение аварии относительно начала (a´), в словах
        /// </summary>
        [Layout(12)]
        private ushort _after;
        /// <summary>
        /// Количество отсчетов после аварии (k´)
        /// </summary>
        [Layout(13)]
        private ushort _afterFault;

        #endregion [Private fields]

        #region [Public Properties]


        public string Stage
        {
            get { return Stages[this._resstage]; }
        }
        /// <summary>
        /// Номер соощения
        /// </summary>
        public string GetNumber
        {
            get
            {
                var result = string.Format(NUMBER_PATTERN, RecordIndex + 1);
                if (RecordIndex == 0)
                {
                    result += "(посл.)";
                }
                return result;
            }
        }

        /// <summary>
        /// Время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                return string.Format
                (
                    TIME_PATTERN,
                    this._hour,
                    this._minute,
                    this._second,
                    this._millisecond 
                );
            }
        }

        /// <summary>
        /// Дата сообщения
        /// </summary>
        public string GetDate
        {
            get
            {
                return string.Format
                (
                    DATE_PATTERN,
                    this._date,
                    this._month,
                    this._year
                );
            }
        }

        public string GetFormattedDateTimeAlarm(int alarm)
        {
            try
            {
                DateTime a = new DateTime(this._year, this._month, this._date, this._hour, this._minute, this._second, this._millisecond < 100 ? this._millisecond * 10 : this._millisecond);
                DateTime result = a.AddMilliseconds(alarm);
                return string.Format("{0:D2}/{1:D2}/{2:D2},{3:D2}:{4:D2}:{5:D2}.{6:D3}",
                    result.Month,
                    result.Day,
                    result.Year,
                    result.Hour,
                    result.Minute,
                    result.Second,
                    result.Millisecond);

            }
            catch (Exception)
            {
                return GetFormattedDateTime;
            }

        }

        public string GetFormattedDateTime
        {
            get
            {
                return string.Format("{0:D2}/{1:D2}/{2:D2},{3:D2}:{4:D2}:{5:D2}.{6:D3}",
                    this._month,
                    this._date,
                    this._year,
                    this._hour,
                    this._minute,
                    this._second,
                    this._millisecond < 100 ? this._millisecond * 10 : this._millisecond);
            }
        }

        /// <summary>
        /// Размер осц. в мс
        /// </summary>
        public int SizeTime
        {
            // вычитается единица для того,чтобы исключить неверные данные, воторые в конце
            get { return this._len / this.SizeCountdown - 2; }

        }

        /// <summary>
        /// Авария в мс
        /// </summary>
        public int FaultTime
        {
            // вычитается единица для того,чтобы исключить неверные данные, которые идут в осциллограмму вначале самой осциллограмме
            get
            {
                return (this._len - this._afterFault * this.SizeCountdown - this._len % this.SizeCountdown) / this.SizeCountdown - 1;
            }
        }

        /// <summary>
        /// Размер осциллограммы (p´), в словах
        /// </summary>
        public ushort Len
        {
            get { return _len; }
        }

        /// <summary>
        /// Положение аварии относительно начала (a´), в словах
        /// </summary>
        public int After
        {
            get { return _after; }
        }

        /// <summary>
        /// Количество отсчетов после аварии (k´)
        /// </summary>
        public int AfterFault
        {
            get { return this._afterFault; }
        }


        /// <summary>
        /// "REZ" Размер одного отсчёта (в словах)
        /// </summary>
        public ushort SizeCountdown
        {
            get { return 9; }
        }

        #endregion [Private Properties]
        private static List<string> Stages
        {
            get
            {
                return new List<string>
                {
                    "",
                    "I>",
                    "I>>",
                    "I>>>",
                    "I>>>>",
                    "I2>",
                    "I2>>",
                    "I0>",
                    "I0>>",
                    "In>",
                    "In>>",
                    "Ig>",
                    "I2/I1",
                    "F>",
                    "F>>",
                    "F<",
                    "F<<",
                    "U>",
                    "U>>",
                    "U<",
                    "U<<",
                    "U2>",
                    "U2>>",
                    "U0>",
                    "U0>>",
                    "ВЗ-1",
                    "ВЗ-2",
                    "ВЗ-3",
                    "ВЗ-4",
                    "ВЗ-5",
                    "ВЗ-6",
                    "ВЗ-7",
                    "ВЗ-8",
                    "Резерв",
                    "Резерв",
                    "Резерв",
                    "Резерв",
                    "Резерв",
                    "Резерв",
                    "Резерв",
                    "Резерв"
                };
            }

        }
    }
}
