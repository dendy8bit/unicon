﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;

namespace BEMN.TZL.OldOsc.Structers
{
    public class MeasureTransStruct: StructBase
    {
        #region [Public field]

        [Layout(0)]
        private ushort _configTt;
        [Layout(1)]
        private ushort _tt;
        [Layout(2)]
        private ushort _ttnp;
        [Layout(3)]
        private ushort _maxI;
        [Layout(4)]
        private ushort _rez1;
        [Layout(5)]
        private ushort _rez2;
        [Layout(6)]
        private ushort _rez3;
        [Layout(7)]
        private ushort _rez4;
        [Layout(8)]
        private ushort _configTn;
        [Layout(9)]
        private ushort _tn;
        [Layout(10)]
        private ushort _externalFaultTn;
        [Layout(11)]
        private ushort _tnnp;

        #endregion [Public field]

        public ushort Tt
        {
            get { return this._tt; }
        }

        public ushort Ttnp
        {
            get { return this._ttnp; }
        }

        /// <summary>
        /// ТН
        /// </summary>
        public double Tn
        {
            get
            {
                var value = ValuesConverterCommon.GetKth(this._tn);
                return Common.GetBit(this._tn, 15)
                    ? value * 1000
                    : value;
            }
        }
        /// <summary>
        /// ТННП
        /// </summary>
        public double Tnnp
        {
            get
            {
                var value = ValuesConverterCommon.GetKth(this._tnnp);
                return Common.GetBit(this._tnnp, 15)
                    ? value * 1000
                    : value;
            }
        }
    }
}
