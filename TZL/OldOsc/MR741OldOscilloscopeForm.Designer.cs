﻿namespace BEMN.TZL.OldOsc
{
    partial class MR741OldOscilloscopeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._resetOsc = new System.Windows.Forms.Button();
            this._oscJournalDataGrid = new System.Windows.Forms.DataGridView();
            this._oscNumColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._stage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscPointColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscLengthColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscReadyColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscStartColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscEndColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscBeginColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscJournalReadButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._oscShowButton = new System.Windows.Forms.Button();
            this._oscLoadButton = new System.Windows.Forms.Button();
            this._oscSaveButton = new System.Windows.Forms.Button();
            this._oscProgressBar = new System.Windows.Forms.ProgressBar();
            this._oscReadButton = new System.Windows.Forms.Button();
            this._oscilloscopeCountCb = new System.Windows.Forms.ComboBox();
            this._oscilloscopeCountLabel = new System.Windows.Forms.Label();
            this._saveOscilloscopeDlg = new System.Windows.Forms.SaveFileDialog();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._openOscilloscopeDlg = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._oscJournalDataGrid)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel2.Controls.Add(this._oscilloscopeCountCb);
            this.splitContainer1.Panel2.Controls.Add(this._oscilloscopeCountLabel);
            this.splitContainer1.Size = new System.Drawing.Size(816, 437);
            this.splitContainer1.SplitterDistance = 317;
            this.splitContainer1.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._resetOsc);
            this.groupBox1.Controls.Add(this._oscJournalDataGrid);
            this.groupBox1.Controls.Add(this._oscJournalReadButton);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(811, 311);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Журнал осциллографа";
            // 
            // _resetOsc
            // 
            this._resetOsc.Enabled = false;
            this._resetOsc.Location = new System.Drawing.Point(485, 282);
            this._resetOsc.Name = "_resetOsc";
            this._resetOsc.Size = new System.Drawing.Size(155, 23);
            this._resetOsc.TabIndex = 11;
            this._resetOsc.Text = "Сбросить осциллограмму";
            this._resetOsc.UseVisualStyleBackColor = true;
            this._resetOsc.Click += new System.EventHandler(this._resetOsc_Click);
            // 
            // _oscJournalDataGrid
            // 
            this._oscJournalDataGrid.AllowUserToAddRows = false;
            this._oscJournalDataGrid.AllowUserToDeleteRows = false;
            this._oscJournalDataGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._oscJournalDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._oscJournalDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._oscNumColumn,
            this._dateCol,
            this._oscTimeColumn,
            this._stage,
            this._oscPointColumn,
            this._oscLengthColumn,
            this._oscReadyColumn,
            this._oscStartColumn,
            this._oscEndColumn,
            this._oscBeginColumn});
            this._oscJournalDataGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this._oscJournalDataGrid.Location = new System.Drawing.Point(3, 16);
            this._oscJournalDataGrid.Name = "_oscJournalDataGrid";
            this._oscJournalDataGrid.ReadOnly = true;
            this._oscJournalDataGrid.RowHeadersVisible = false;
            this._oscJournalDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._oscJournalDataGrid.RowTemplate.Height = 24;
            this._oscJournalDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._oscJournalDataGrid.Size = new System.Drawing.Size(805, 260);
            this._oscJournalDataGrid.TabIndex = 5;
            this._oscJournalDataGrid.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this._oscJournalDataGrid_RowEnter_1);
            // 
            // _oscNumColumn
            // 
            this._oscNumColumn.DataPropertyName = "_oscNumColumn";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this._oscNumColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this._oscNumColumn.Frozen = true;
            this._oscNumColumn.HeaderText = "№";
            this._oscNumColumn.Name = "_oscNumColumn";
            this._oscNumColumn.ReadOnly = true;
            this._oscNumColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._oscNumColumn.Width = 70;
            // 
            // _dateCol
            // 
            this._dateCol.DataPropertyName = "_dateCol";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._dateCol.DefaultCellStyle = dataGridViewCellStyle2;
            this._dateCol.HeaderText = "Дата";
            this._dateCol.Name = "_dateCol";
            this._dateCol.ReadOnly = true;
            this._dateCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _oscTimeColumn
            // 
            this._oscTimeColumn.DataPropertyName = "_oscTimeColumn";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._oscTimeColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this._oscTimeColumn.HeaderText = "Время";
            this._oscTimeColumn.Name = "_oscTimeColumn";
            this._oscTimeColumn.ReadOnly = true;
            this._oscTimeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._oscTimeColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._oscTimeColumn.Width = 125;
            // 
            // _stage
            // 
            this._stage.DataPropertyName = "_stage";
            this._stage.HeaderText = "Ступень";
            this._stage.Name = "_stage";
            this._stage.ReadOnly = true;
            // 
            // _oscPointColumn
            // 
            this._oscPointColumn.DataPropertyName = "_oscPointColumn";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._oscPointColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this._oscPointColumn.HeaderText = "Размер, мс";
            this._oscPointColumn.MinimumWidth = 100;
            this._oscPointColumn.Name = "_oscPointColumn";
            this._oscPointColumn.ReadOnly = true;
            this._oscPointColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._oscPointColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _oscLengthColumn
            // 
            this._oscLengthColumn.DataPropertyName = "_oscLengthColumn";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._oscLengthColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this._oscLengthColumn.HeaderText = "Авария, мс";
            this._oscLengthColumn.Name = "_oscLengthColumn";
            this._oscLengthColumn.ReadOnly = true;
            this._oscLengthColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._oscLengthColumn.Width = 70;
            // 
            // _oscReadyColumn
            // 
            this._oscReadyColumn.DataPropertyName = "_oscReadyColumn";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._oscReadyColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this._oscReadyColumn.HeaderText = "тт";
            this._oscReadyColumn.Name = "_oscReadyColumn";
            this._oscReadyColumn.ReadOnly = true;
            this._oscReadyColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._oscReadyColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._oscReadyColumn.Width = 80;
            // 
            // _oscStartColumn
            // 
            this._oscStartColumn.DataPropertyName = "_oscStartColumn";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._oscStartColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this._oscStartColumn.HeaderText = "ттнп";
            this._oscStartColumn.Name = "_oscStartColumn";
            this._oscStartColumn.ReadOnly = true;
            this._oscStartColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._oscStartColumn.Width = 80;
            // 
            // _oscEndColumn
            // 
            this._oscEndColumn.DataPropertyName = "_oscEndColumn";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._oscEndColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this._oscEndColumn.HeaderText = "Ктн";
            this._oscEndColumn.Name = "_oscEndColumn";
            this._oscEndColumn.ReadOnly = true;
            this._oscEndColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._oscEndColumn.Width = 110;
            // 
            // _oscBeginColumn
            // 
            this._oscBeginColumn.DataPropertyName = "_oscBeginColumn";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._oscBeginColumn.DefaultCellStyle = dataGridViewCellStyle9;
            this._oscBeginColumn.HeaderText = "Ктннп";
            this._oscBeginColumn.Name = "_oscBeginColumn";
            this._oscBeginColumn.ReadOnly = true;
            this._oscBeginColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _oscJournalReadButton
            // 
            this._oscJournalReadButton.Location = new System.Drawing.Point(646, 282);
            this._oscJournalReadButton.Name = "_oscJournalReadButton";
            this._oscJournalReadButton.Size = new System.Drawing.Size(155, 23);
            this._oscJournalReadButton.TabIndex = 4;
            this._oscJournalReadButton.Text = "Перечитать журнал";
            this._oscJournalReadButton.UseVisualStyleBackColor = true;
            this._oscJournalReadButton.Click += new System.EventHandler(this._oscJournalReadButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._oscShowButton);
            this.groupBox2.Controls.Add(this._oscLoadButton);
            this.groupBox2.Controls.Add(this._oscSaveButton);
            this.groupBox2.Controls.Add(this._oscProgressBar);
            this.groupBox2.Controls.Add(this._oscReadButton);
            this.groupBox2.Location = new System.Drawing.Point(3, 34);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(811, 77);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Осциллограмма";
            // 
            // _oscShowButton
            // 
            this._oscShowButton.Enabled = false;
            this._oscShowButton.Location = new System.Drawing.Point(6, 48);
            this._oscShowButton.Name = "_oscShowButton";
            this._oscShowButton.Size = new System.Drawing.Size(155, 23);
            this._oscShowButton.TabIndex = 10;
            this._oscShowButton.Text = "Показать осциллограмму";
            this._oscShowButton.UseVisualStyleBackColor = true;
            this._oscShowButton.Click += new System.EventHandler(this._oscShowButton_Click);
            // 
            // _oscLoadButton
            // 
            this._oscLoadButton.Location = new System.Drawing.Point(647, 48);
            this._oscLoadButton.Name = "_oscLoadButton";
            this._oscLoadButton.Size = new System.Drawing.Size(155, 23);
            this._oscLoadButton.TabIndex = 9;
            this._oscLoadButton.Text = "Загрузить из файла";
            this._oscLoadButton.UseVisualStyleBackColor = true;
            this._oscLoadButton.Click += new System.EventHandler(this._oscLoadButton_Click);
            // 
            // _oscSaveButton
            // 
            this._oscSaveButton.Enabled = false;
            this._oscSaveButton.Location = new System.Drawing.Point(647, 19);
            this._oscSaveButton.Name = "_oscSaveButton";
            this._oscSaveButton.Size = new System.Drawing.Size(155, 23);
            this._oscSaveButton.TabIndex = 7;
            this._oscSaveButton.Text = "Сохранить в файл";
            this._oscSaveButton.UseVisualStyleBackColor = true;
            this._oscSaveButton.Click += new System.EventHandler(this._oscSaveButton_Click);
            // 
            // _oscProgressBar
            // 
            this._oscProgressBar.Location = new System.Drawing.Point(167, 48);
            this._oscProgressBar.Name = "_oscProgressBar";
            this._oscProgressBar.Size = new System.Drawing.Size(474, 23);
            this._oscProgressBar.Step = 1;
            this._oscProgressBar.TabIndex = 6;
            // 
            // _oscReadButton
            // 
            this._oscReadButton.Enabled = false;
            this._oscReadButton.Location = new System.Drawing.Point(6, 19);
            this._oscReadButton.Name = "_oscReadButton";
            this._oscReadButton.Size = new System.Drawing.Size(155, 23);
            this._oscReadButton.TabIndex = 5;
            this._oscReadButton.Text = "Прочитать осциллограмму";
            this._oscReadButton.UseVisualStyleBackColor = true;
            this._oscReadButton.Click += new System.EventHandler(this._oscReadButton_Click);
            // 
            // _oscilloscopeCountCb
            // 
            this._oscilloscopeCountCb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscilloscopeCountCb.Enabled = false;
            this._oscilloscopeCountCb.FormattingEnabled = true;
            this._oscilloscopeCountCb.Location = new System.Drawing.Point(493, 7);
            this._oscilloscopeCountCb.Name = "_oscilloscopeCountCb";
            this._oscilloscopeCountCb.Size = new System.Drawing.Size(56, 21);
            this._oscilloscopeCountCb.TabIndex = 9;
            // 
            // _oscilloscopeCountLabel
            // 
            this._oscilloscopeCountLabel.AutoSize = true;
            this._oscilloscopeCountLabel.Enabled = false;
            this._oscilloscopeCountLabel.Location = new System.Drawing.Point(252, 10);
            this._oscilloscopeCountLabel.Name = "_oscilloscopeCountLabel";
            this._oscilloscopeCountLabel.Size = new System.Drawing.Size(235, 13);
            this._oscilloscopeCountLabel.TabIndex = 8;
            this._oscilloscopeCountLabel.Text = "Выберите номер читаемой осциллограммы :";
            // 
            // _saveOscilloscopeDlg
            // 
            this._saveOscilloscopeDlg.FileName = "Осциллограмма МР 741";
            this._saveOscilloscopeDlg.Filter = "Файл осциллограммы|*.hdr";
            this._saveOscilloscopeDlg.Title = "Сохранить осциллограмму";
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(16, 17);
            this._statusLabel.Text = "...";
            // 
            // _openOscilloscopeDlg
            // 
            this._openOscilloscopeDlg.Filter = "Файл осциллограммы|*.hdr";
            this._openOscilloscopeDlg.Title = "Открыть осциллограмму";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 446);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(821, 22);
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // MR741OldOscilloscopeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 468);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "MR741OldOscilloscopeForm";
            this.Text = "MR741OldOscilloscopeForm";
            this.Load += new System.EventHandler(this.OscilloscopeForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._oscJournalDataGrid)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView _oscJournalDataGrid;
        private System.Windows.Forms.Button _oscJournalReadButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button _oscShowButton;
        private System.Windows.Forms.Button _oscLoadButton;
        private System.Windows.Forms.Button _oscSaveButton;
        private System.Windows.Forms.ProgressBar _oscProgressBar;
        private System.Windows.Forms.Button _oscReadButton;
        private System.Windows.Forms.ComboBox _oscilloscopeCountCb;
        private System.Windows.Forms.Label _oscilloscopeCountLabel;
        private System.Windows.Forms.SaveFileDialog _saveOscilloscopeDlg;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.OpenFileDialog _openOscilloscopeDlg;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Button _resetOsc;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscNumColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dateCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscTimeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _stage;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscPointColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscLengthColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscReadyColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscStartColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscEndColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscBeginColumn;
    }
}