﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.ValidatingClasses;
using BEMN.Interfaces;
using BEMN.TZL.OldOsc.HelpClasses;
using BEMN.TZL.OldOsc.Loaders;
using BEMN.TZL.OldOsc.Structers;
using BEMN.TZL.Properties;

namespace BEMN.TZL.OldOsc
{
    public partial class MR741OldOscilloscopeForm : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "Осциллограмма";
        private const string OSC_JOURNAL_IS_EMPTY = "Журнал осциллографа пуст";
        private const string READ_OSC_FAIL = "Невозможно прочитать журнал осциллографа";
        private const string READING_OSC = "Идет чтение осциллограммы";
        private const string RECORDS_IN_JOURNAL = "Осциллограмм в журнале - {0}";
        private const string OSC_LOAD_SUCCESSFUL = "Осцилограмма успешно загружена";
        private const string FAIL_LOAD_OSC = "Невозможно загрузить осцилограмму";

        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// Загрузчик страниц
        /// </summary>
        private readonly OldOscPageLoader _pageLoader;
        /// <summary>
        /// Загрузчик журнала
        /// </summary>
        private readonly OldOscJournalLoader _oldOscJournalLoader;
        /// <summary>
        /// Загрузчик уставок токов
        /// </summary>
        private readonly CurrentOptionsLoader _currentOptionsLoader;

        /// <summary>
        /// Данные осц
        /// </summary>
        private CountingList _countingList;

        private OscJournalStruct _journalStruct;
        private readonly DataTable _table;
        private readonly OldOscOptionsLoader _oscopeOptionsLoader;
        private readonly TZL _device;
        private readonly MemoryEntity<OneWordStruct> _resetOscStruct;
        #endregion [Private fields]


        #region [Ctor's]
        public MR741OldOscilloscopeForm()
        {
            this.InitializeComponent();
        }

        public MR741OldOscilloscopeForm(TZL device)
        {

            this.InitializeComponent();
            this._device = device;
            this._resetOscStruct = device.ResetOsc;
            //Загрузчик журнала
            this._oldOscJournalLoader = new OldOscJournalLoader(device);
            this._oldOscJournalLoader.ReadRecordOk += HandlerHelper.CreateActionHandler(this, this.ReadRecord);
            this._oldOscJournalLoader.AllReadOk += HandlerHelper.CreateActionHandler(this, this.AllJournalReadOk);
            this._oldOscJournalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            //Конфигурация осцилографа
            this._oscopeOptionsLoader = new OldOscOptionsLoader(device);
            this._oscopeOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, () =>
            {
                if (this._oscopeOptionsLoader.OscCount == 0)
                {
                    this._statusLabel.Text = OSC_JOURNAL_IS_EMPTY;
                    this._oscJournalReadButton.Enabled = true;
                    this._resetOsc.Enabled = false;
                }
                else
                {
                    this._resetOsc.Enabled = true;
                    this._oldOscJournalLoader.StartReadJournal(this._oscopeOptionsLoader.OscCount);
                }
            });
            this._oscopeOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            //Загрузчик уставок токов
            this._currentOptionsLoader = new CurrentOptionsLoader(device);
            this._currentOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, this._oscopeOptionsLoader.StartRead);
            this._currentOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            //загрузчик осциллограмм
            this._pageLoader = new OldOscPageLoader(this._device);
            this._pageLoader.SlotReadSuccessful += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadFail += HandlerHelper.CreateActionHandler(this, this.OscReadFail);
            //сброс осциллографа
            this._resetOscStruct.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                MessageBox.Show("Неудалось сбросить осциллограмму");
            });
            this._resetOscStruct.WriteOk += HandlerHelper.CreateHandler(this, () =>
            {
                this._resetOsc.Enabled = false;
                this.StartRead();
            });
            this._table = this.GetJournalDataTable();
        }
        #endregion [Ctor's]    


        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(TZL); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(TZL); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return OSC; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }
        #endregion [IFormView Members]


        #region [Help Classes Events Handlers]
        /// <summary>
        /// Невозможно прочитать журнал - выводим сообщение об ошибке
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
            this._oscJournalReadButton.Enabled = true;
        }

        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            this._oscilloscopeCountCb.Items.Add(this._oldOscJournalLoader.RecordNumber);
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._oldOscJournalLoader.RecordNumber);
            this._table.Rows.Add
            (
                this._oldOscJournalLoader.RecordStruct.GetNumber,
                this._oldOscJournalLoader.RecordStruct.GetDate,
                this._oldOscJournalLoader.RecordStruct.GetTime,
                this._oldOscJournalLoader.RecordStruct.Stage,
                this._oldOscJournalLoader.RecordStruct.SizeTime,
                this._oldOscJournalLoader.RecordStruct.FaultTime,
                this._currentOptionsLoader.MeasureStruct.Tt,
                this._currentOptionsLoader.MeasureStruct.Ttnp,
                this._currentOptionsLoader.MeasureStruct.Tn,
                this._currentOptionsLoader.MeasureStruct.Tnnp
            );
            this._oscJournalDataGrid.Refresh();
        }

        private void AllJournalReadOk()
        {
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
        }

        /// <summary>
        /// Осцилограмма успешно загружена из устройства
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            this.CountingList = new CountingList(this._pageLoader.ResultArray, this._journalStruct, this._currentOptionsLoader.MeasureStruct);

            this._oscSaveButton.Enabled = true;
            this._oscShowButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
            this._oscJournalReadButton.Enabled = true;
            this._oscReadButton.Enabled = true;
        }
        /// <summary>
        /// Осцилограмма успешно загружена из устройства
        /// </summary>
        private void OscReadFail()
        {
            this._statusLabel.Text = FAIL_LOAD_OSC;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
        }

        #endregion [Help Classes Events Handlers]


        #region [Properties]
        /// <summary>
        /// Определяет возможность выбрать осцилограмму для чтения
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// Данные осц
        /// </summary>
        public CountingList CountingList
        {
            get { return this._countingList; }
            set
            {
                this._countingList = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]


        #region [Help members]
        private DataTable GetJournalDataTable()
        {
            var table = new DataTable("МР741_журнал_осциллографа");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }
        #endregion [Help members]


        #region [Event Handlers]
        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }
        /// <summary>
        /// Загрузка формы
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            this.StartRead();
        }
        /// <summary>
        /// Показать осциллограмму
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (this.CountingList == null)
            {
                this.CountingList = new CountingList(new ushort[1800], new OscJournalStruct(), new MeasureTransStruct());
            }
            if (Validator.GetVersionFromRegistry())
            {
                string fileName;
                if (this._countingList.IsLoad)
                {
                    fileName = this._countingList.FilePath;
                }
                else
                {
                    fileName = Validator.CreateOscFileNameCfg($"МР741 v{this._device.DeviceVersion} Осциллограмма");
                    this._countingList.Save(fileName);
                }
                System.Diagnostics.Process.Start(
                    Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"),
                    fileName);
            }
            else
            {
                if (!Settings.Default.OscFlag)
                {
                    LoadNetForm loadNetForm = new LoadNetForm();
                    loadNetForm.ShowDialog(this);
                    Settings.Default.OscFlag = loadNetForm.OscFlag;
                    Settings.Default.Save();
                }
            }
        }

        /// <summary>
        /// Перечитать журнал
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._oscJournalReadButton.Enabled = false;
            this._oscilloscopeCountCb.Items.Clear();
            this._oscilloscopeCountCb.SelectedIndex = -1;
            this._table.Clear();
            this._oldOscJournalLoader.Reset();
            this._oscJournalDataGrid.Refresh();
            this.CanSelectOsc = false;
            this._oscReadButton.Enabled = false;
            this._oscJournalReadButton.Enabled = false;
            this._oscLoadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._currentOptionsLoader.StartRead();
        }

        /// <summary>
        /// Прочитать осциллограмму
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStruct = this._oldOscJournalLoader.OscRecords[selectedOsc];
            this._pageLoader.StartRead(this._journalStruct, this._oscopeOptionsLoader.AllOscCount, selectedOsc);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoader.SlotCount;
            this._statusLabel.Text = READING_OSC;
            this._oscReadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscLoadButton.Enabled = false;
            this._oscJournalReadButton.Enabled = false;
        }

        /// <summary>
        /// Сохранить осциллограмму в файл
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        {
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                this._countingList.Save(this._saveOscilloscopeDlg.FileName);
            }
        }

        /// <summary>
        /// Загрузить осциллограмму из файла
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                this.CountingList = CountingList.Load(this._openOscilloscopeDlg.FileName);
                this._statusLabel.Text = string.Format("Осцилограммы загружена из файла {0}",
                    this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = FAIL_LOAD_OSC;
            }
        }

        #endregion [Event Handlers]

        private void _resetOsc_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Сбросить осциллограмму?", "Внимание", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                this._resetOscStruct.Value.Word = 0;
                this._resetOscStruct.SaveStruct6();

            }
        }

        private void _oscJournalDataGrid_RowEnter_1(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }
    }
}
