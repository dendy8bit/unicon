using BEMN_XY_Chart;

namespace BEMN.TZL
{
    partial class ChartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChartForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this._toolBar = new System.Windows.Forms.ToolStrip();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this._labelShkalaI = new System.Windows.Forms.Label();
            this._analogChannelsPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this._analogChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this._labelShkalaU = new System.Windows.Forms.Label();
            this._uAnalogChannelsUPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this._analogUChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.splitContainer7 = new System.Windows.Forms.SplitContainer();
            this.splitContainer8 = new System.Windows.Forms.SplitContainer();
            this._diskretChannelsUPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this._diskretChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this._oxScroll = new System.Windows.Forms.HScrollBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.DeltaLabel = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.label111 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label98 = new System.Windows.Forms.Label();
            this.TimeLabel2 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.label106 = new System.Windows.Forms.Label();
            this.TimeLabel1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.D2Label16 = new System.Windows.Forms.Label();
            this.D2Label15 = new System.Windows.Forms.Label();
            this.D2Label14 = new System.Windows.Forms.Label();
            this.D2Label13 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.D2Label12 = new System.Windows.Forms.Label();
            this.D2Label11 = new System.Windows.Forms.Label();
            this.D2Label10 = new System.Windows.Forms.Label();
            this.D2Label9 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.D2Label8 = new System.Windows.Forms.Label();
            this.D2Label7 = new System.Windows.Forms.Label();
            this.D2Label6 = new System.Windows.Forms.Label();
            this.D2Label5 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.D2Label4 = new System.Windows.Forms.Label();
            this.D2Label3 = new System.Windows.Forms.Label();
            this.D2Label2 = new System.Windows.Forms.Label();
            this.D2Label1 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.D1Label13 = new System.Windows.Forms.Label();
            this.D1Label14 = new System.Windows.Forms.Label();
            this.D1Label15 = new System.Windows.Forms.Label();
            this.D1Label16 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.D1Label9 = new System.Windows.Forms.Label();
            this.D1Label10 = new System.Windows.Forms.Label();
            this.D1Label11 = new System.Windows.Forms.Label();
            this.D1Label12 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.D1Label5 = new System.Windows.Forms.Label();
            this.D1Label6 = new System.Windows.Forms.Label();
            this.D1Label7 = new System.Windows.Forms.Label();
            this.D1Label8 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.D1Label1 = new System.Windows.Forms.Label();
            this.D1Label2 = new System.Windows.Forms.Label();
            this.D1Label3 = new System.Windows.Forms.Label();
            this.D1Label4 = new System.Windows.Forms.Label();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label43 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.UoLabel2 = new System.Windows.Forms.Label();
            this.UcLabel2 = new System.Windows.Forms.Label();
            this.UbLabel2 = new System.Windows.Forms.Label();
            this.UaLabel2 = new System.Windows.Forms.Label();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.UaLabel1 = new System.Windows.Forms.Label();
            this.UbLabel1 = new System.Windows.Forms.Label();
            this.UcLabel1 = new System.Windows.Forms.Label();
            this.UoLabel1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.labelI4_1 = new System.Windows.Forms.Label();
            this.labelI3_1 = new System.Windows.Forms.Label();
            this.labelI2_1 = new System.Windows.Forms.Label();
            this.labelI1_1 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.labelI1 = new System.Windows.Forms.Label();
            this.labelI2 = new System.Windows.Forms.Label();
            this.labelI3 = new System.Windows.Forms.Label();
            this.labelI4 = new System.Windows.Forms.Label();
            this._oyScroll = new System.Windows.Forms.VScrollBar();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this._toolBar.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.Panel2.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            this.splitContainer7.Panel1.SuspendLayout();
            this.splitContainer7.Panel2.SuspendLayout();
            this.splitContainer7.SuspendLayout();
            this.splitContainer8.Panel1.SuspendLayout();
            this.splitContainer8.Panel2.SuspendLayout();
            this.splitContainer8.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.AutoScrollMinSize = new System.Drawing.Size(20, 20);
            this.splitContainer1.Panel1.Controls.Add(this._toolBar);
            this.splitContainer1.Panel1MinSize = 45;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.AutoScrollMinSize = new System.Drawing.Size(20, 0);
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel2MinSize = 45;
            this.splitContainer1.Size = new System.Drawing.Size(984, 740);
            this.splitContainer1.SplitterDistance = 45;
            this.splitContainer1.TabIndex = 11;
            // 
            // _toolBar
            // 
            this._toolBar.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this._toolBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this._toolBar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this._toolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton7,
            this.toolStripButton1,
            this.toolStripSeparator1,
            this.toolStripButton6,
            this.toolStripSeparator3,
            this.toolStripLabel1,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripSeparator2,
            this.toolStripLabel2,
            this.toolStripButton4,
            this.toolStripButton5,
            this.toolStripSeparator4});
            this._toolBar.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this._toolBar.Location = new System.Drawing.Point(0, 0);
            this._toolBar.MaximumSize = new System.Drawing.Size(0, 45);
            this._toolBar.MinimumSize = new System.Drawing.Size(0, 45);
            this._toolBar.Name = "_toolBar";
            this._toolBar.Size = new System.Drawing.Size(984, 45);
            this._toolBar.TabIndex = 11;
            this._toolBar.TabStop = true;
            this._toolBar.Text = "toolStrip1";
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(23, 42);
            this.toolStripButton7.Text = "�������� ��� �������������";
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 42);
            this.toolStripButton1.Text = "������������� � ����. ��������";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 45);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(23, 42);
            this.toolStripButton6.Text = "������������ ������ �����";
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 45);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(23, 42);
            this.toolStripLabel1.Text = "X : ";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 42);
            this.toolStripButton2.Text = "��������� �� �";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 42);
            this.toolStripButton3.Text = "��������� �� �";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 45);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(23, 42);
            this.toolStripLabel2.Text = "Y : ";
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(23, 42);
            this.toolStripButton4.Text = "��������� �� Y";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(23, 42);
            this.toolStripButton5.Text = "��������� �� Y";
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 45);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.panel1);
            this.splitContainer2.Panel2.Controls.Add(this._oyScroll);
            this.splitContainer2.Panel2MinSize = 18;
            this.splitContainer2.Size = new System.Drawing.Size(984, 691);
            this.splitContainer2.SplitterDistance = 716;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.splitContainer4);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.splitContainer7);
            this.splitContainer3.Size = new System.Drawing.Size(716, 691);
            this.splitContainer3.SplitterDistance = 424;
            this.splitContainer3.TabIndex = 7;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.splitContainer5);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.splitContainer6);
            this.splitContainer4.Size = new System.Drawing.Size(716, 424);
            this.splitContainer4.SplitterDistance = 203;
            this.splitContainer4.TabIndex = 1;
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            this.splitContainer5.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this._labelShkalaI);
            this.splitContainer5.Panel1.Controls.Add(this._analogChannelsPanel);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.label3);
            this.splitContainer5.Panel2.Controls.Add(this._analogChart);
            this.splitContainer5.Size = new System.Drawing.Size(716, 203);
            this.splitContainer5.SplitterDistance = 25;
            this.splitContainer5.TabIndex = 0;
            // 
            // _labelShkalaI
            // 
            this._labelShkalaI.AutoSize = true;
            this._labelShkalaI.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._labelShkalaI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._labelShkalaI.Location = new System.Drawing.Point(0, 12);
            this._labelShkalaI.Name = "_labelShkalaI";
            this._labelShkalaI.Size = new System.Drawing.Size(41, 13);
            this._labelShkalaI.TabIndex = 13;
            this._labelShkalaI.Text = "label2";
            // 
            // _analogChannelsPanel
            // 
            this._analogChannelsPanel.ColumnCount = 5;
            this._analogChannelsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._analogChannelsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._analogChannelsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._analogChannelsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._analogChannelsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this._analogChannelsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this._analogChannelsPanel.Location = new System.Drawing.Point(480, 0);
            this._analogChannelsPanel.Name = "_analogChannelsPanel";
            this._analogChannelsPanel.RowCount = 2;
            this._analogChannelsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._analogChannelsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._analogChannelsPanel.Size = new System.Drawing.Size(236, 25);
            this._analogChannelsPanel.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Right;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(685, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "t �c";
            // 
            // _analogChart
            // 
            this._analogChart.BackColor = System.Drawing.SystemColors.Control;
            this._analogChart.BkGradient = false;
            this._analogChart.BkGradientAngle = 90;
            this._analogChart.BkGradientColor = System.Drawing.Color.White;
            this._analogChart.BkGradientRate = 0.5F;
            this._analogChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._analogChart.BkRestrictedToChartPanel = false;
            this._analogChart.BkShinePosition = 1F;
            this._analogChart.BkTransparency = 0F;
            this._analogChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._analogChart.BorderExteriorLength = 0;
            this._analogChart.BorderGradientAngle = 225;
            this._analogChart.BorderGradientLightPos1 = 0F;
            this._analogChart.BorderGradientLightPos2 = -1F;
            this._analogChart.BorderGradientRate = 0.5F;
            this._analogChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._analogChart.BorderLightIntermediateBrightness = 0F;
            this._analogChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._analogChart.ChartPanelBackColor = System.Drawing.Color.White;
            this._analogChart.ChartPanelBkTransparency = 0F;
            this._analogChart.ControlShadow = false;
            this._analogChart.CoordinateAxesVisible = true;
            this._analogChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._analogChart.CoordinateXOrigin = 1800D;
            this._analogChart.CoordinateYMax = 1000D;
            this._analogChart.CoordinateYMin = -1000D;
            this._analogChart.CoordinateYOrigin = 0D;
            this._analogChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._analogChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._analogChart.FooterColor = System.Drawing.Color.Black;
            this._analogChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._analogChart.FooterVisible = false;
            this._analogChart.GridColor = System.Drawing.Color.Gray;
            this._analogChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this._analogChart.GridVisible = true;
            this._analogChart.GridXSubTicker = 0;
            this._analogChart.GridXTicker = 20;
            this._analogChart.GridYSubTicker = 0;
            this._analogChart.GridYTicker = 15;
            this._analogChart.HeaderColor = System.Drawing.Color.Black;
            this._analogChart.HeaderFont = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._analogChart.HeaderTitle = "���������� �������";
            this._analogChart.HeaderVisible = false;
            this._analogChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._analogChart.InnerBorderLength = 0;
            this._analogChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._analogChart.LegendBkColor = System.Drawing.Color.White;
            this._analogChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_TOP;
            this._analogChart.LegendVisible = false;
            this._analogChart.Location = new System.Drawing.Point(0, 0);
            this._analogChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._analogChart.MiddleBorderLength = 0;
            this._analogChart.Name = "_analogChart";
            this._analogChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._analogChart.OuterBorderLength = 0;
            this._analogChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._analogChart.Precision = 0;
            this._analogChart.RoundRadius = 10;
            this._analogChart.ShadowColor = System.Drawing.Color.DimGray;
            this._analogChart.ShadowDepth = 8;
            this._analogChart.ShadowRate = 0.5F;
            this._analogChart.Size = new System.Drawing.Size(716, 174);
            this._analogChart.TabIndex = 2;
            this._analogChart.Text = "daS_Net_XYChart1";
            this._analogChart.XMax = 3278D;
            this._analogChart.XMin = 0D;
            this._analogChart.XScaleColor = System.Drawing.Color.Black;
            this._analogChart.XScaleVisible = true;
            this._analogChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this._analogChart_MouseClick);
            this._analogChart.MouseDown += new System.Windows.Forms.MouseEventHandler(this._analogChart_MouseDown);
            this._analogChart.MouseMove += new System.Windows.Forms.MouseEventHandler(this._analogChart_MouseMove);
            this._analogChart.MouseUp += new System.Windows.Forms.MouseEventHandler(this._analogChart_MouseUp);
            // 
            // splitContainer6
            // 
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.Location = new System.Drawing.Point(0, 0);
            this.splitContainer6.Name = "splitContainer6";
            this.splitContainer6.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.Controls.Add(this._labelShkalaU);
            this.splitContainer6.Panel1.Controls.Add(this._uAnalogChannelsUPanel);
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.Controls.Add(this.label2);
            this.splitContainer6.Panel2.Controls.Add(this._analogUChart);
            this.splitContainer6.Size = new System.Drawing.Size(716, 217);
            this.splitContainer6.SplitterDistance = 25;
            this.splitContainer6.TabIndex = 2;
            // 
            // _labelShkalaU
            // 
            this._labelShkalaU.AutoSize = true;
            this._labelShkalaU.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._labelShkalaU.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._labelShkalaU.Location = new System.Drawing.Point(0, 12);
            this._labelShkalaU.Name = "_labelShkalaU";
            this._labelShkalaU.Size = new System.Drawing.Size(41, 13);
            this._labelShkalaU.TabIndex = 14;
            this._labelShkalaU.Text = "label2";
            // 
            // _uAnalogChannelsUPanel
            // 
            this._uAnalogChannelsUPanel.ColumnCount = 5;
            this._uAnalogChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._uAnalogChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._uAnalogChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._uAnalogChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._uAnalogChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this._uAnalogChannelsUPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this._uAnalogChannelsUPanel.Location = new System.Drawing.Point(480, 0);
            this._uAnalogChannelsUPanel.Name = "_uAnalogChannelsUPanel";
            this._uAnalogChannelsUPanel.RowCount = 2;
            this._uAnalogChannelsUPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._uAnalogChannelsUPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._uAnalogChannelsUPanel.Size = new System.Drawing.Size(236, 25);
            this._uAnalogChannelsUPanel.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Right;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(685, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "t �c";
            // 
            // _analogUChart
            // 
            this._analogUChart.BkGradient = false;
            this._analogUChart.BkGradientAngle = 90;
            this._analogUChart.BkGradientColor = System.Drawing.Color.White;
            this._analogUChart.BkGradientRate = 0.5F;
            this._analogUChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._analogUChart.BkRestrictedToChartPanel = false;
            this._analogUChart.BkShinePosition = 1F;
            this._analogUChart.BkTransparency = 0F;
            this._analogUChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._analogUChart.BorderExteriorLength = 0;
            this._analogUChart.BorderGradientAngle = 225;
            this._analogUChart.BorderGradientLightPos1 = 1F;
            this._analogUChart.BorderGradientLightPos2 = -1F;
            this._analogUChart.BorderGradientRate = 0.5F;
            this._analogUChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._analogUChart.BorderLightIntermediateBrightness = 0F;
            this._analogUChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._analogUChart.ChartPanelBackColor = System.Drawing.Color.White;
            this._analogUChart.ChartPanelBkTransparency = 0F;
            this._analogUChart.ControlShadow = false;
            this._analogUChart.CoordinateAxesVisible = true;
            this._analogUChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._analogUChart.CoordinateXOrigin = 1800D;
            this._analogUChart.CoordinateYMax = 1000D;
            this._analogUChart.CoordinateYMin = -1000D;
            this._analogUChart.CoordinateYOrigin = 0D;
            this._analogUChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._analogUChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._analogUChart.FooterColor = System.Drawing.Color.Black;
            this._analogUChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._analogUChart.FooterVisible = false;
            this._analogUChart.GridColor = System.Drawing.Color.Gray;
            this._analogUChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this._analogUChart.GridVisible = true;
            this._analogUChart.GridXSubTicker = 0;
            this._analogUChart.GridXTicker = 20;
            this._analogUChart.GridYSubTicker = 0;
            this._analogUChart.GridYTicker = 15;
            this._analogUChart.HeaderColor = System.Drawing.Color.Black;
            this._analogUChart.HeaderFont = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._analogUChart.HeaderTitle = "���������� �������";
            this._analogUChart.HeaderVisible = false;
            this._analogUChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._analogUChart.InnerBorderLength = 0;
            this._analogUChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._analogUChart.LegendBkColor = System.Drawing.Color.White;
            this._analogUChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_TOP;
            this._analogUChart.LegendVisible = false;
            this._analogUChart.Location = new System.Drawing.Point(0, 0);
            this._analogUChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._analogUChart.MiddleBorderLength = 0;
            this._analogUChart.Name = "_analogUChart";
            this._analogUChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._analogUChart.OuterBorderLength = 0;
            this._analogUChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._analogUChart.Precision = 0;
            this._analogUChart.RoundRadius = 10;
            this._analogUChart.ShadowColor = System.Drawing.Color.DimGray;
            this._analogUChart.ShadowDepth = 8;
            this._analogUChart.ShadowRate = 0.5F;
            this._analogUChart.Size = new System.Drawing.Size(716, 188);
            this._analogUChart.TabIndex = 3;
            this._analogUChart.Text = "daS_Net_XYChart1";
            this._analogUChart.XMax = 3278D;
            this._analogUChart.XMin = 0D;
            this._analogUChart.XScaleColor = System.Drawing.Color.Black;
            this._analogUChart.XScaleVisible = true;
            this._analogUChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this._analogUChart_MouseClick);
            this._analogUChart.MouseDown += new System.Windows.Forms.MouseEventHandler(this._analogUChart_MouseDown);
            this._analogUChart.MouseMove += new System.Windows.Forms.MouseEventHandler(this._analogUChart_MouseMove);
            this._analogUChart.MouseUp += new System.Windows.Forms.MouseEventHandler(this._analogUChart_MouseUp);
            // 
            // splitContainer7
            // 
            this.splitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer7.Location = new System.Drawing.Point(0, 0);
            this.splitContainer7.Name = "splitContainer7";
            this.splitContainer7.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer7.Panel1
            // 
            this.splitContainer7.Panel1.Controls.Add(this.splitContainer8);
            // 
            // splitContainer7.Panel2
            // 
            this.splitContainer7.Panel2.Controls.Add(this._oxScroll);
            this.splitContainer7.Panel2MinSize = 18;
            this.splitContainer7.Size = new System.Drawing.Size(716, 263);
            this.splitContainer7.SplitterDistance = 235;
            this.splitContainer7.TabIndex = 3;
            // 
            // splitContainer8
            // 
            this.splitContainer8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer8.Location = new System.Drawing.Point(0, 0);
            this.splitContainer8.Name = "splitContainer8";
            this.splitContainer8.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer8.Panel1
            // 
            this.splitContainer8.Panel1.Controls.Add(this._diskretChannelsUPanel);
            // 
            // splitContainer8.Panel2
            // 
            this.splitContainer8.Panel2.Controls.Add(this.label1);
            this.splitContainer8.Panel2.Controls.Add(this._diskretChart);
            this.splitContainer8.Size = new System.Drawing.Size(716, 235);
            this.splitContainer8.SplitterDistance = 25;
            this.splitContainer8.TabIndex = 0;
            // 
            // _diskretChannelsUPanel
            // 
            this._diskretChannelsUPanel.ColumnCount = 17;
            this._diskretChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._diskretChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._diskretChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._diskretChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._diskretChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._diskretChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._diskretChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._diskretChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._diskretChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._diskretChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._diskretChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._diskretChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._diskretChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._diskretChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._diskretChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._diskretChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this._diskretChannelsUPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this._diskretChannelsUPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this._diskretChannelsUPanel.Location = new System.Drawing.Point(2, 0);
            this._diskretChannelsUPanel.Name = "_diskretChannelsUPanel";
            this._diskretChannelsUPanel.RowCount = 2;
            this._diskretChannelsUPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._diskretChannelsUPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._diskretChannelsUPanel.Size = new System.Drawing.Size(714, 25);
            this._diskretChannelsUPanel.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Right;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(685, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "t �c";
            // 
            // _diskretChart
            // 
            this._diskretChart.BkGradient = false;
            this._diskretChart.BkGradientAngle = 90;
            this._diskretChart.BkGradientColor = System.Drawing.Color.White;
            this._diskretChart.BkGradientRate = 1F;
            this._diskretChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._diskretChart.BkRestrictedToChartPanel = false;
            this._diskretChart.BkShinePosition = 1F;
            this._diskretChart.BkTransparency = 0F;
            this._diskretChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._diskretChart.BorderExteriorLength = 0;
            this._diskretChart.BorderGradientAngle = 225;
            this._diskretChart.BorderGradientLightPos1 = 1F;
            this._diskretChart.BorderGradientLightPos2 = -1F;
            this._diskretChart.BorderGradientRate = 1F;
            this._diskretChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._diskretChart.BorderLightIntermediateBrightness = 0F;
            this._diskretChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._diskretChart.ChartPanelBackColor = System.Drawing.Color.White;
            this._diskretChart.ChartPanelBkTransparency = 0F;
            this._diskretChart.ControlShadow = false;
            this._diskretChart.CoordinateAxesVisible = true;
            this._diskretChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._diskretChart.CoordinateXOrigin = 50D;
            this._diskretChart.CoordinateYMax = 100D;
            this._diskretChart.CoordinateYMin = -100D;
            this._diskretChart.CoordinateYOrigin = 0D;
            this._diskretChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._diskretChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._diskretChart.FooterColor = System.Drawing.Color.Black;
            this._diskretChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._diskretChart.FooterVisible = false;
            this._diskretChart.GridColor = System.Drawing.Color.Gray;
            this._diskretChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this._diskretChart.GridVisible = true;
            this._diskretChart.GridXSubTicker = 0;
            this._diskretChart.GridXTicker = 20;
            this._diskretChart.GridYSubTicker = 1;
            this._diskretChart.GridYTicker = 18;
            this._diskretChart.HeaderColor = System.Drawing.Color.Black;
            this._diskretChart.HeaderFont = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this._diskretChart.HeaderVisible = false;
            this._diskretChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._diskretChart.InnerBorderLength = 0;
            this._diskretChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._diskretChart.LegendBkColor = System.Drawing.Color.White;
            this._diskretChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_TOP;
            this._diskretChart.LegendVisible = false;
            this._diskretChart.Location = new System.Drawing.Point(0, 0);
            this._diskretChart.Margin = new System.Windows.Forms.Padding(0);
            this._diskretChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._diskretChart.MiddleBorderLength = 0;
            this._diskretChart.Name = "_diskretChart";
            this._diskretChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._diskretChart.OuterBorderLength = 0;
            this._diskretChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._diskretChart.Precision = 0;
            this._diskretChart.RoundRadius = 10;
            this._diskretChart.ShadowColor = System.Drawing.Color.DimGray;
            this._diskretChart.ShadowDepth = 8;
            this._diskretChart.ShadowRate = 0.5F;
            this._diskretChart.Size = new System.Drawing.Size(716, 206);
            this._diskretChart.TabIndex = 3;
            this._diskretChart.Text = "daS_Net_XYChart2";
            this._diskretChart.XMax = 100D;
            this._diskretChart.XMin = 0D;
            this._diskretChart.XScaleColor = System.Drawing.Color.Black;
            this._diskretChart.XScaleVisible = true;
            this._diskretChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this._diskretChart_MouseClick);
            this._diskretChart.MouseDown += new System.Windows.Forms.MouseEventHandler(this._diskretChart_MouseDown);
            this._diskretChart.MouseMove += new System.Windows.Forms.MouseEventHandler(this._diskretChart_MouseMove);
            this._diskretChart.MouseUp += new System.Windows.Forms.MouseEventHandler(this._diskretChart_MouseUp);
            // 
            // _oxScroll
            // 
            this._oxScroll.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._oxScroll.LargeChange = 1;
            this._oxScroll.Location = new System.Drawing.Point(0, 4);
            this._oxScroll.Maximum = 0;
            this._oxScroll.MaximumSize = new System.Drawing.Size(0, 20);
            this._oxScroll.MinimumSize = new System.Drawing.Size(0, 20);
            this._oxScroll.Name = "_oxScroll";
            this._oxScroll.Size = new System.Drawing.Size(716, 20);
            this._oxScroll.TabIndex = 3;
            this._oxScroll.Scroll += new System.Windows.Forms.ScrollEventHandler(this._oxScroll_Scroll);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(23, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(239, 694);
            this.panel1.TabIndex = 14;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox8);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox15);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(3, -3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(233, 694);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.groupBox18);
            this.groupBox8.Controls.Add(this.groupBox17);
            this.groupBox8.Controls.Add(this.groupBox10);
            this.groupBox8.Controls.Add(this.groupBox14);
            this.groupBox8.Location = new System.Drawing.Point(6, 572);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(221, 98);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.DeltaLabel);
            this.groupBox18.Controls.Add(this.label95);
            this.groupBox18.Location = new System.Drawing.Point(6, 64);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(209, 28);
            this.groupBox18.TabIndex = 38;
            this.groupBox18.TabStop = false;
            // 
            // DeltaLabel
            // 
            this.DeltaLabel.AutoSize = true;
            this.DeltaLabel.ForeColor = System.Drawing.Color.Red;
            this.DeltaLabel.Location = new System.Drawing.Point(123, 10);
            this.DeltaLabel.Name = "DeltaLabel";
            this.DeltaLabel.Size = new System.Drawing.Size(0, 13);
            this.DeltaLabel.TabIndex = 1;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label95.Location = new System.Drawing.Point(68, 10);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(54, 13);
            this.label95.TabIndex = 0;
            this.label95.Text = "������ =";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.label111);
            this.groupBox17.Location = new System.Drawing.Point(0, 0);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(221, 28);
            this.groupBox17.TabIndex = 37;
            this.groupBox17.TabStop = false;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label111.Location = new System.Drawing.Point(90, 11);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(40, 13);
            this.label111.TabIndex = 0;
            this.label111.Text = "�����";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label98);
            this.groupBox10.Controls.Add(this.TimeLabel2);
            this.groupBox10.Location = new System.Drawing.Point(113, 27);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(102, 37);
            this.groupBox10.TabIndex = 36;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "������ 2";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(6, 17);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(49, 13);
            this.label98.TabIndex = 39;
            this.label98.Text = "����� =";
            // 
            // TimeLabel2
            // 
            this.TimeLabel2.AutoSize = true;
            this.TimeLabel2.BackColor = System.Drawing.Color.Transparent;
            this.TimeLabel2.ForeColor = System.Drawing.Color.Red;
            this.TimeLabel2.Location = new System.Drawing.Point(55, 17);
            this.TimeLabel2.Name = "TimeLabel2";
            this.TimeLabel2.Size = new System.Drawing.Size(0, 13);
            this.TimeLabel2.TabIndex = 35;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.label106);
            this.groupBox14.Controls.Add(this.TimeLabel1);
            this.groupBox14.Location = new System.Drawing.Point(6, 27);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(102, 37);
            this.groupBox14.TabIndex = 35;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "������ 1";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(6, 17);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(49, 13);
            this.label106.TabIndex = 35;
            this.label106.Text = "����� =";
            // 
            // TimeLabel1
            // 
            this.TimeLabel1.AutoSize = true;
            this.TimeLabel1.BackColor = System.Drawing.Color.Transparent;
            this.TimeLabel1.ForeColor = System.Drawing.Color.Red;
            this.TimeLabel1.Location = new System.Drawing.Point(55, 17);
            this.TimeLabel1.Name = "TimeLabel1";
            this.TimeLabel1.Size = new System.Drawing.Size(0, 13);
            this.TimeLabel1.TabIndex = 31;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Controls.Add(this.groupBox9);
            this.groupBox3.Controls.Add(this.groupBox11);
            this.groupBox3.Location = new System.Drawing.Point(6, 245);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(221, 327);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Location = new System.Drawing.Point(0, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(221, 28);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label13.Location = new System.Drawing.Point(74, 10);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(59, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "��������";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label42);
            this.groupBox9.Controls.Add(this.label44);
            this.groupBox9.Controls.Add(this.label45);
            this.groupBox9.Controls.Add(this.label46);
            this.groupBox9.Controls.Add(this.D2Label16);
            this.groupBox9.Controls.Add(this.D2Label15);
            this.groupBox9.Controls.Add(this.D2Label14);
            this.groupBox9.Controls.Add(this.D2Label13);
            this.groupBox9.Controls.Add(this.label84);
            this.groupBox9.Controls.Add(this.label85);
            this.groupBox9.Controls.Add(this.label86);
            this.groupBox9.Controls.Add(this.label87);
            this.groupBox9.Controls.Add(this.D2Label12);
            this.groupBox9.Controls.Add(this.D2Label11);
            this.groupBox9.Controls.Add(this.D2Label10);
            this.groupBox9.Controls.Add(this.D2Label9);
            this.groupBox9.Controls.Add(this.label76);
            this.groupBox9.Controls.Add(this.label77);
            this.groupBox9.Controls.Add(this.label78);
            this.groupBox9.Controls.Add(this.label79);
            this.groupBox9.Controls.Add(this.D2Label8);
            this.groupBox9.Controls.Add(this.D2Label7);
            this.groupBox9.Controls.Add(this.D2Label6);
            this.groupBox9.Controls.Add(this.D2Label5);
            this.groupBox9.Controls.Add(this.label16);
            this.groupBox9.Controls.Add(this.label17);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Controls.Add(this.label19);
            this.groupBox9.Controls.Add(this.D2Label4);
            this.groupBox9.Controls.Add(this.D2Label3);
            this.groupBox9.Controls.Add(this.D2Label2);
            this.groupBox9.Controls.Add(this.D2Label1);
            this.groupBox9.Location = new System.Drawing.Point(113, 31);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(102, 291);
            this.groupBox9.TabIndex = 36;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "������ 2";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(6, 270);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(37, 13);
            this.label42.TabIndex = 68;
            this.label42.Text = "�16 =";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 252);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(37, 13);
            this.label44.TabIndex = 67;
            this.label44.Text = "�15 =";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(6, 236);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(37, 13);
            this.label45.TabIndex = 66;
            this.label45.Text = "�14 =";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(6, 219);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(37, 13);
            this.label46.TabIndex = 65;
            this.label46.Text = "�13 =";
            // 
            // D2Label16
            // 
            this.D2Label16.AutoSize = true;
            this.D2Label16.BackColor = System.Drawing.Color.Transparent;
            this.D2Label16.ForeColor = System.Drawing.Color.Black;
            this.D2Label16.Location = new System.Drawing.Point(50, 270);
            this.D2Label16.Name = "D2Label16";
            this.D2Label16.Size = new System.Drawing.Size(0, 13);
            this.D2Label16.TabIndex = 64;
            // 
            // D2Label15
            // 
            this.D2Label15.AutoSize = true;
            this.D2Label15.BackColor = System.Drawing.Color.Transparent;
            this.D2Label15.ForeColor = System.Drawing.Color.Black;
            this.D2Label15.Location = new System.Drawing.Point(50, 253);
            this.D2Label15.Name = "D2Label15";
            this.D2Label15.Size = new System.Drawing.Size(0, 13);
            this.D2Label15.TabIndex = 63;
            // 
            // D2Label14
            // 
            this.D2Label14.AutoSize = true;
            this.D2Label14.BackColor = System.Drawing.Color.Transparent;
            this.D2Label14.ForeColor = System.Drawing.Color.Black;
            this.D2Label14.Location = new System.Drawing.Point(50, 236);
            this.D2Label14.Name = "D2Label14";
            this.D2Label14.Size = new System.Drawing.Size(0, 13);
            this.D2Label14.TabIndex = 62;
            // 
            // D2Label13
            // 
            this.D2Label13.AutoSize = true;
            this.D2Label13.BackColor = System.Drawing.Color.Transparent;
            this.D2Label13.ForeColor = System.Drawing.Color.Black;
            this.D2Label13.Location = new System.Drawing.Point(50, 219);
            this.D2Label13.Name = "D2Label13";
            this.D2Label13.Size = new System.Drawing.Size(0, 13);
            this.D2Label13.TabIndex = 61;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(6, 202);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(37, 13);
            this.label84.TabIndex = 60;
            this.label84.Text = "�12 =";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(6, 184);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(37, 13);
            this.label85.TabIndex = 59;
            this.label85.Text = "�11 =";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(6, 168);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(37, 13);
            this.label86.TabIndex = 58;
            this.label86.Text = "�10 =";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(6, 151);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(31, 13);
            this.label87.TabIndex = 57;
            this.label87.Text = "�9 =";
            // 
            // D2Label12
            // 
            this.D2Label12.AutoSize = true;
            this.D2Label12.BackColor = System.Drawing.Color.Transparent;
            this.D2Label12.ForeColor = System.Drawing.Color.Black;
            this.D2Label12.Location = new System.Drawing.Point(50, 202);
            this.D2Label12.Name = "D2Label12";
            this.D2Label12.Size = new System.Drawing.Size(0, 13);
            this.D2Label12.TabIndex = 56;
            // 
            // D2Label11
            // 
            this.D2Label11.AutoSize = true;
            this.D2Label11.BackColor = System.Drawing.Color.Transparent;
            this.D2Label11.ForeColor = System.Drawing.Color.Black;
            this.D2Label11.Location = new System.Drawing.Point(50, 185);
            this.D2Label11.Name = "D2Label11";
            this.D2Label11.Size = new System.Drawing.Size(0, 13);
            this.D2Label11.TabIndex = 55;
            // 
            // D2Label10
            // 
            this.D2Label10.AutoSize = true;
            this.D2Label10.BackColor = System.Drawing.Color.Transparent;
            this.D2Label10.ForeColor = System.Drawing.Color.Black;
            this.D2Label10.Location = new System.Drawing.Point(50, 168);
            this.D2Label10.Name = "D2Label10";
            this.D2Label10.Size = new System.Drawing.Size(0, 13);
            this.D2Label10.TabIndex = 54;
            // 
            // D2Label9
            // 
            this.D2Label9.AutoSize = true;
            this.D2Label9.BackColor = System.Drawing.Color.Transparent;
            this.D2Label9.ForeColor = System.Drawing.Color.Black;
            this.D2Label9.Location = new System.Drawing.Point(50, 151);
            this.D2Label9.Name = "D2Label9";
            this.D2Label9.Size = new System.Drawing.Size(0, 13);
            this.D2Label9.TabIndex = 53;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(6, 133);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(31, 13);
            this.label76.TabIndex = 52;
            this.label76.Text = "�8 =";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(6, 116);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(31, 13);
            this.label77.TabIndex = 51;
            this.label77.Text = "�7 =";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(6, 100);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(31, 13);
            this.label78.TabIndex = 50;
            this.label78.Text = "�6 =";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(6, 83);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(31, 13);
            this.label79.TabIndex = 49;
            this.label79.Text = "�5 =";
            // 
            // D2Label8
            // 
            this.D2Label8.AutoSize = true;
            this.D2Label8.BackColor = System.Drawing.Color.Transparent;
            this.D2Label8.ForeColor = System.Drawing.Color.Black;
            this.D2Label8.Location = new System.Drawing.Point(50, 134);
            this.D2Label8.Name = "D2Label8";
            this.D2Label8.Size = new System.Drawing.Size(0, 13);
            this.D2Label8.TabIndex = 48;
            // 
            // D2Label7
            // 
            this.D2Label7.AutoSize = true;
            this.D2Label7.BackColor = System.Drawing.Color.Transparent;
            this.D2Label7.ForeColor = System.Drawing.Color.Black;
            this.D2Label7.Location = new System.Drawing.Point(50, 117);
            this.D2Label7.Name = "D2Label7";
            this.D2Label7.Size = new System.Drawing.Size(0, 13);
            this.D2Label7.TabIndex = 47;
            // 
            // D2Label6
            // 
            this.D2Label6.AutoSize = true;
            this.D2Label6.BackColor = System.Drawing.Color.Transparent;
            this.D2Label6.ForeColor = System.Drawing.Color.Black;
            this.D2Label6.Location = new System.Drawing.Point(50, 100);
            this.D2Label6.Name = "D2Label6";
            this.D2Label6.Size = new System.Drawing.Size(0, 13);
            this.D2Label6.TabIndex = 46;
            // 
            // D2Label5
            // 
            this.D2Label5.AutoSize = true;
            this.D2Label5.BackColor = System.Drawing.Color.Transparent;
            this.D2Label5.ForeColor = System.Drawing.Color.Black;
            this.D2Label5.Location = new System.Drawing.Point(50, 83);
            this.D2Label5.Name = "D2Label5";
            this.D2Label5.Size = new System.Drawing.Size(0, 13);
            this.D2Label5.TabIndex = 45;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 66);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(31, 13);
            this.label16.TabIndex = 42;
            this.label16.Text = "�4 =";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 49);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 13);
            this.label17.TabIndex = 41;
            this.label17.Text = "�3 =";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 33);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(31, 13);
            this.label18.TabIndex = 40;
            this.label18.Text = "�2 =";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(31, 13);
            this.label19.TabIndex = 39;
            this.label19.Text = "�1 =";
            // 
            // D2Label4
            // 
            this.D2Label4.AutoSize = true;
            this.D2Label4.BackColor = System.Drawing.Color.Transparent;
            this.D2Label4.ForeColor = System.Drawing.Color.Black;
            this.D2Label4.Location = new System.Drawing.Point(50, 67);
            this.D2Label4.Name = "D2Label4";
            this.D2Label4.Size = new System.Drawing.Size(0, 13);
            this.D2Label4.TabIndex = 38;
            // 
            // D2Label3
            // 
            this.D2Label3.AutoSize = true;
            this.D2Label3.BackColor = System.Drawing.Color.Transparent;
            this.D2Label3.ForeColor = System.Drawing.Color.Black;
            this.D2Label3.Location = new System.Drawing.Point(50, 50);
            this.D2Label3.Name = "D2Label3";
            this.D2Label3.Size = new System.Drawing.Size(0, 13);
            this.D2Label3.TabIndex = 37;
            // 
            // D2Label2
            // 
            this.D2Label2.AutoSize = true;
            this.D2Label2.BackColor = System.Drawing.Color.Transparent;
            this.D2Label2.ForeColor = System.Drawing.Color.Black;
            this.D2Label2.Location = new System.Drawing.Point(50, 33);
            this.D2Label2.Name = "D2Label2";
            this.D2Label2.Size = new System.Drawing.Size(0, 13);
            this.D2Label2.TabIndex = 36;
            // 
            // D2Label1
            // 
            this.D2Label1.AutoSize = true;
            this.D2Label1.BackColor = System.Drawing.Color.Transparent;
            this.D2Label1.ForeColor = System.Drawing.Color.Black;
            this.D2Label1.Location = new System.Drawing.Point(50, 16);
            this.D2Label1.Name = "D2Label1";
            this.D2Label1.Size = new System.Drawing.Size(0, 13);
            this.D2Label1.TabIndex = 35;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label10);
            this.groupBox11.Controls.Add(this.label14);
            this.groupBox11.Controls.Add(this.label15);
            this.groupBox11.Controls.Add(this.label29);
            this.groupBox11.Controls.Add(this.D1Label13);
            this.groupBox11.Controls.Add(this.D1Label14);
            this.groupBox11.Controls.Add(this.D1Label15);
            this.groupBox11.Controls.Add(this.D1Label16);
            this.groupBox11.Controls.Add(this.label68);
            this.groupBox11.Controls.Add(this.label69);
            this.groupBox11.Controls.Add(this.label70);
            this.groupBox11.Controls.Add(this.label71);
            this.groupBox11.Controls.Add(this.D1Label9);
            this.groupBox11.Controls.Add(this.D1Label10);
            this.groupBox11.Controls.Add(this.D1Label11);
            this.groupBox11.Controls.Add(this.D1Label12);
            this.groupBox11.Controls.Add(this.label60);
            this.groupBox11.Controls.Add(this.label61);
            this.groupBox11.Controls.Add(this.label62);
            this.groupBox11.Controls.Add(this.label63);
            this.groupBox11.Controls.Add(this.D1Label5);
            this.groupBox11.Controls.Add(this.D1Label6);
            this.groupBox11.Controls.Add(this.D1Label7);
            this.groupBox11.Controls.Add(this.D1Label8);
            this.groupBox11.Controls.Add(this.label52);
            this.groupBox11.Controls.Add(this.label53);
            this.groupBox11.Controls.Add(this.label54);
            this.groupBox11.Controls.Add(this.label55);
            this.groupBox11.Controls.Add(this.D1Label1);
            this.groupBox11.Controls.Add(this.D1Label2);
            this.groupBox11.Controls.Add(this.D1Label3);
            this.groupBox11.Controls.Add(this.D1Label4);
            this.groupBox11.Location = new System.Drawing.Point(6, 31);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(102, 291);
            this.groupBox11.TabIndex = 35;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "������ 1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 269);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 13);
            this.label10.TabIndex = 64;
            this.label10.Text = "�16 =";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 252);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 13);
            this.label14.TabIndex = 63;
            this.label14.Text = "�15 =";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 235);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 13);
            this.label15.TabIndex = 62;
            this.label15.Text = "�14 =";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 218);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(37, 13);
            this.label29.TabIndex = 61;
            this.label29.Text = "�13 =";
            // 
            // D1Label13
            // 
            this.D1Label13.AutoSize = true;
            this.D1Label13.BackColor = System.Drawing.Color.Transparent;
            this.D1Label13.ForeColor = System.Drawing.Color.Black;
            this.D1Label13.Location = new System.Drawing.Point(55, 218);
            this.D1Label13.Name = "D1Label13";
            this.D1Label13.Size = new System.Drawing.Size(0, 13);
            this.D1Label13.TabIndex = 57;
            // 
            // D1Label14
            // 
            this.D1Label14.AutoSize = true;
            this.D1Label14.BackColor = System.Drawing.Color.Transparent;
            this.D1Label14.ForeColor = System.Drawing.Color.Black;
            this.D1Label14.Location = new System.Drawing.Point(55, 235);
            this.D1Label14.Name = "D1Label14";
            this.D1Label14.Size = new System.Drawing.Size(0, 13);
            this.D1Label14.TabIndex = 58;
            // 
            // D1Label15
            // 
            this.D1Label15.AutoSize = true;
            this.D1Label15.BackColor = System.Drawing.Color.Transparent;
            this.D1Label15.ForeColor = System.Drawing.Color.Black;
            this.D1Label15.Location = new System.Drawing.Point(55, 252);
            this.D1Label15.Name = "D1Label15";
            this.D1Label15.Size = new System.Drawing.Size(0, 13);
            this.D1Label15.TabIndex = 59;
            // 
            // D1Label16
            // 
            this.D1Label16.AutoSize = true;
            this.D1Label16.BackColor = System.Drawing.Color.Transparent;
            this.D1Label16.ForeColor = System.Drawing.Color.Black;
            this.D1Label16.Location = new System.Drawing.Point(55, 269);
            this.D1Label16.Name = "D1Label16";
            this.D1Label16.Size = new System.Drawing.Size(0, 13);
            this.D1Label16.TabIndex = 60;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(6, 201);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(37, 13);
            this.label68.TabIndex = 56;
            this.label68.Text = "�12 =";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(6, 184);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(37, 13);
            this.label69.TabIndex = 55;
            this.label69.Text = "�11 =";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(6, 167);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(37, 13);
            this.label70.TabIndex = 54;
            this.label70.Text = "�10 =";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(6, 150);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(31, 13);
            this.label71.TabIndex = 53;
            this.label71.Text = "�9 =";
            // 
            // D1Label9
            // 
            this.D1Label9.AutoSize = true;
            this.D1Label9.BackColor = System.Drawing.Color.Transparent;
            this.D1Label9.ForeColor = System.Drawing.Color.Black;
            this.D1Label9.Location = new System.Drawing.Point(55, 150);
            this.D1Label9.Name = "D1Label9";
            this.D1Label9.Size = new System.Drawing.Size(0, 13);
            this.D1Label9.TabIndex = 49;
            // 
            // D1Label10
            // 
            this.D1Label10.AutoSize = true;
            this.D1Label10.BackColor = System.Drawing.Color.Transparent;
            this.D1Label10.ForeColor = System.Drawing.Color.Black;
            this.D1Label10.Location = new System.Drawing.Point(55, 167);
            this.D1Label10.Name = "D1Label10";
            this.D1Label10.Size = new System.Drawing.Size(0, 13);
            this.D1Label10.TabIndex = 50;
            // 
            // D1Label11
            // 
            this.D1Label11.AutoSize = true;
            this.D1Label11.BackColor = System.Drawing.Color.Transparent;
            this.D1Label11.ForeColor = System.Drawing.Color.Black;
            this.D1Label11.Location = new System.Drawing.Point(55, 184);
            this.D1Label11.Name = "D1Label11";
            this.D1Label11.Size = new System.Drawing.Size(0, 13);
            this.D1Label11.TabIndex = 51;
            // 
            // D1Label12
            // 
            this.D1Label12.AutoSize = true;
            this.D1Label12.BackColor = System.Drawing.Color.Transparent;
            this.D1Label12.ForeColor = System.Drawing.Color.Black;
            this.D1Label12.Location = new System.Drawing.Point(55, 201);
            this.D1Label12.Name = "D1Label12";
            this.D1Label12.Size = new System.Drawing.Size(0, 13);
            this.D1Label12.TabIndex = 52;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(6, 134);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(31, 13);
            this.label60.TabIndex = 48;
            this.label60.Text = "�8 =";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(6, 117);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(31, 13);
            this.label61.TabIndex = 47;
            this.label61.Text = "�7 =";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(6, 100);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(31, 13);
            this.label62.TabIndex = 46;
            this.label62.Text = "�6 =";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(6, 83);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(31, 13);
            this.label63.TabIndex = 45;
            this.label63.Text = "�5 =";
            // 
            // D1Label5
            // 
            this.D1Label5.AutoSize = true;
            this.D1Label5.BackColor = System.Drawing.Color.Transparent;
            this.D1Label5.ForeColor = System.Drawing.Color.Black;
            this.D1Label5.Location = new System.Drawing.Point(55, 83);
            this.D1Label5.Name = "D1Label5";
            this.D1Label5.Size = new System.Drawing.Size(0, 13);
            this.D1Label5.TabIndex = 41;
            // 
            // D1Label6
            // 
            this.D1Label6.AutoSize = true;
            this.D1Label6.BackColor = System.Drawing.Color.Transparent;
            this.D1Label6.ForeColor = System.Drawing.Color.Black;
            this.D1Label6.Location = new System.Drawing.Point(55, 100);
            this.D1Label6.Name = "D1Label6";
            this.D1Label6.Size = new System.Drawing.Size(0, 13);
            this.D1Label6.TabIndex = 42;
            // 
            // D1Label7
            // 
            this.D1Label7.AutoSize = true;
            this.D1Label7.BackColor = System.Drawing.Color.Transparent;
            this.D1Label7.ForeColor = System.Drawing.Color.Black;
            this.D1Label7.Location = new System.Drawing.Point(55, 117);
            this.D1Label7.Name = "D1Label7";
            this.D1Label7.Size = new System.Drawing.Size(0, 13);
            this.D1Label7.TabIndex = 43;
            // 
            // D1Label8
            // 
            this.D1Label8.AutoSize = true;
            this.D1Label8.BackColor = System.Drawing.Color.Transparent;
            this.D1Label8.ForeColor = System.Drawing.Color.Black;
            this.D1Label8.Location = new System.Drawing.Point(55, 134);
            this.D1Label8.Name = "D1Label8";
            this.D1Label8.Size = new System.Drawing.Size(0, 13);
            this.D1Label8.TabIndex = 44;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(6, 67);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(31, 13);
            this.label52.TabIndex = 38;
            this.label52.Text = "�4 =";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(6, 50);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(31, 13);
            this.label53.TabIndex = 37;
            this.label53.Text = "�3 =";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(6, 33);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(31, 13);
            this.label54.TabIndex = 36;
            this.label54.Text = "�2 =";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(6, 16);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(31, 13);
            this.label55.TabIndex = 35;
            this.label55.Text = "�1 =";
            // 
            // D1Label1
            // 
            this.D1Label1.AutoSize = true;
            this.D1Label1.BackColor = System.Drawing.Color.Transparent;
            this.D1Label1.ForeColor = System.Drawing.Color.Black;
            this.D1Label1.Location = new System.Drawing.Point(55, 16);
            this.D1Label1.Name = "D1Label1";
            this.D1Label1.Size = new System.Drawing.Size(0, 13);
            this.D1Label1.TabIndex = 31;
            // 
            // D1Label2
            // 
            this.D1Label2.AutoSize = true;
            this.D1Label2.BackColor = System.Drawing.Color.Transparent;
            this.D1Label2.ForeColor = System.Drawing.Color.Black;
            this.D1Label2.Location = new System.Drawing.Point(55, 33);
            this.D1Label2.Name = "D1Label2";
            this.D1Label2.Size = new System.Drawing.Size(0, 13);
            this.D1Label2.TabIndex = 32;
            // 
            // D1Label3
            // 
            this.D1Label3.AutoSize = true;
            this.D1Label3.BackColor = System.Drawing.Color.Transparent;
            this.D1Label3.ForeColor = System.Drawing.Color.Black;
            this.D1Label3.Location = new System.Drawing.Point(55, 50);
            this.D1Label3.Name = "D1Label3";
            this.D1Label3.Size = new System.Drawing.Size(0, 13);
            this.D1Label3.TabIndex = 33;
            // 
            // D1Label4
            // 
            this.D1Label4.AutoSize = true;
            this.D1Label4.BackColor = System.Drawing.Color.Transparent;
            this.D1Label4.ForeColor = System.Drawing.Color.Black;
            this.D1Label4.Location = new System.Drawing.Point(55, 67);
            this.D1Label4.Name = "D1Label4";
            this.D1Label4.Size = new System.Drawing.Size(0, 13);
            this.D1Label4.TabIndex = 34;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.groupBox13);
            this.groupBox15.Controls.Add(this.groupBox12);
            this.groupBox15.Controls.Add(this.groupBox16);
            this.groupBox15.Location = new System.Drawing.Point(6, 124);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(221, 121);
            this.groupBox15.TabIndex = 2;
            this.groupBox15.TabStop = false;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label43);
            this.groupBox13.Location = new System.Drawing.Point(0, 4);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(221, 28);
            this.groupBox13.TabIndex = 3;
            this.groupBox13.TabStop = false;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label43.Location = new System.Drawing.Point(74, 10);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(71, 13);
            this.label43.TabIndex = 0;
            this.label43.Text = "����������";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label35);
            this.groupBox12.Controls.Add(this.label36);
            this.groupBox12.Controls.Add(this.label37);
            this.groupBox12.Controls.Add(this.label38);
            this.groupBox12.Controls.Add(this.UoLabel2);
            this.groupBox12.Controls.Add(this.UcLabel2);
            this.groupBox12.Controls.Add(this.UbLabel2);
            this.groupBox12.Controls.Add(this.UaLabel2);
            this.groupBox12.Location = new System.Drawing.Point(113, 31);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(102, 85);
            this.groupBox12.TabIndex = 36;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "������ 2";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 66);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(30, 13);
            this.label35.TabIndex = 42;
            this.label35.Text = "Un =";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(6, 48);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(30, 13);
            this.label36.TabIndex = 41;
            this.label36.Text = "Uc =";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(6, 32);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(30, 13);
            this.label37.TabIndex = 40;
            this.label37.Text = "Ub =";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(6, 15);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(30, 13);
            this.label38.TabIndex = 39;
            this.label38.Text = "Ua =";
            // 
            // UoLabel2
            // 
            this.UoLabel2.AutoSize = true;
            this.UoLabel2.BackColor = System.Drawing.Color.Transparent;
            this.UoLabel2.ForeColor = System.Drawing.Color.Black;
            this.UoLabel2.Location = new System.Drawing.Point(50, 67);
            this.UoLabel2.Name = "UoLabel2";
            this.UoLabel2.Size = new System.Drawing.Size(0, 13);
            this.UoLabel2.TabIndex = 38;
            // 
            // UcLabel2
            // 
            this.UcLabel2.AutoSize = true;
            this.UcLabel2.BackColor = System.Drawing.Color.Transparent;
            this.UcLabel2.ForeColor = System.Drawing.Color.Black;
            this.UcLabel2.Location = new System.Drawing.Point(50, 49);
            this.UcLabel2.Name = "UcLabel2";
            this.UcLabel2.Size = new System.Drawing.Size(0, 13);
            this.UcLabel2.TabIndex = 37;
            // 
            // UbLabel2
            // 
            this.UbLabel2.AutoSize = true;
            this.UbLabel2.BackColor = System.Drawing.Color.Transparent;
            this.UbLabel2.ForeColor = System.Drawing.Color.Black;
            this.UbLabel2.Location = new System.Drawing.Point(50, 32);
            this.UbLabel2.Name = "UbLabel2";
            this.UbLabel2.Size = new System.Drawing.Size(0, 13);
            this.UbLabel2.TabIndex = 36;
            // 
            // UaLabel2
            // 
            this.UaLabel2.AutoSize = true;
            this.UaLabel2.BackColor = System.Drawing.Color.Transparent;
            this.UaLabel2.ForeColor = System.Drawing.Color.Black;
            this.UaLabel2.Location = new System.Drawing.Point(50, 15);
            this.UaLabel2.Name = "UaLabel2";
            this.UaLabel2.Size = new System.Drawing.Size(0, 13);
            this.UaLabel2.TabIndex = 35;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.label48);
            this.groupBox16.Controls.Add(this.label49);
            this.groupBox16.Controls.Add(this.label50);
            this.groupBox16.Controls.Add(this.label51);
            this.groupBox16.Controls.Add(this.UaLabel1);
            this.groupBox16.Controls.Add(this.UbLabel1);
            this.groupBox16.Controls.Add(this.UcLabel1);
            this.groupBox16.Controls.Add(this.UoLabel1);
            this.groupBox16.Location = new System.Drawing.Point(6, 31);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(102, 85);
            this.groupBox16.TabIndex = 35;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "������ 1";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(6, 66);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(30, 13);
            this.label48.TabIndex = 38;
            this.label48.Text = "Un =";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(6, 49);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(30, 13);
            this.label49.TabIndex = 37;
            this.label49.Text = "Uc =";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(6, 32);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(30, 13);
            this.label50.TabIndex = 36;
            this.label50.Text = "Ub =";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(6, 15);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(30, 13);
            this.label51.TabIndex = 35;
            this.label51.Text = "Ua =";
            // 
            // UaLabel1
            // 
            this.UaLabel1.AutoSize = true;
            this.UaLabel1.BackColor = System.Drawing.Color.Transparent;
            this.UaLabel1.ForeColor = System.Drawing.Color.Black;
            this.UaLabel1.Location = new System.Drawing.Point(55, 15);
            this.UaLabel1.Name = "UaLabel1";
            this.UaLabel1.Size = new System.Drawing.Size(0, 13);
            this.UaLabel1.TabIndex = 31;
            // 
            // UbLabel1
            // 
            this.UbLabel1.AutoSize = true;
            this.UbLabel1.BackColor = System.Drawing.Color.Transparent;
            this.UbLabel1.ForeColor = System.Drawing.Color.Black;
            this.UbLabel1.Location = new System.Drawing.Point(55, 32);
            this.UbLabel1.Name = "UbLabel1";
            this.UbLabel1.Size = new System.Drawing.Size(0, 13);
            this.UbLabel1.TabIndex = 32;
            // 
            // UcLabel1
            // 
            this.UcLabel1.AutoSize = true;
            this.UcLabel1.BackColor = System.Drawing.Color.Transparent;
            this.UcLabel1.ForeColor = System.Drawing.Color.Black;
            this.UcLabel1.Location = new System.Drawing.Point(55, 49);
            this.UcLabel1.Name = "UcLabel1";
            this.UcLabel1.Size = new System.Drawing.Size(0, 13);
            this.UcLabel1.TabIndex = 33;
            // 
            // UoLabel1
            // 
            this.UoLabel1.AutoSize = true;
            this.UoLabel1.BackColor = System.Drawing.Color.Transparent;
            this.UoLabel1.ForeColor = System.Drawing.Color.Black;
            this.UoLabel1.Location = new System.Drawing.Point(55, 66);
            this.UoLabel1.Name = "UoLabel1";
            this.UoLabel1.Size = new System.Drawing.Size(0, 13);
            this.UoLabel1.TabIndex = 34;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Location = new System.Drawing.Point(6, 7);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(221, 28);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label30.Location = new System.Drawing.Point(90, 10);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(32, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = "����";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox7);
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Location = new System.Drawing.Point(6, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(221, 117);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label28);
            this.groupBox7.Controls.Add(this.label27);
            this.groupBox7.Controls.Add(this.label26);
            this.groupBox7.Controls.Add(this.label25);
            this.groupBox7.Controls.Add(this.labelI4_1);
            this.groupBox7.Controls.Add(this.labelI3_1);
            this.groupBox7.Controls.Add(this.labelI2_1);
            this.groupBox7.Controls.Add(this.labelI1_1);
            this.groupBox7.Location = new System.Drawing.Point(113, 27);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(102, 85);
            this.groupBox7.TabIndex = 36;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "������ 2";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 65);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(25, 13);
            this.label28.TabIndex = 42;
            this.label28.Text = "In =";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 48);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(25, 13);
            this.label27.TabIndex = 41;
            this.label27.Text = "Ic =";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 31);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(25, 13);
            this.label26.TabIndex = 40;
            this.label26.Text = "Ib =";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 14);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(25, 13);
            this.label25.TabIndex = 39;
            this.label25.Text = "Ia =";
            // 
            // labelI4_1
            // 
            this.labelI4_1.AutoSize = true;
            this.labelI4_1.BackColor = System.Drawing.Color.Transparent;
            this.labelI4_1.ForeColor = System.Drawing.Color.Black;
            this.labelI4_1.Location = new System.Drawing.Point(50, 66);
            this.labelI4_1.Name = "labelI4_1";
            this.labelI4_1.Size = new System.Drawing.Size(0, 13);
            this.labelI4_1.TabIndex = 38;
            // 
            // labelI3_1
            // 
            this.labelI3_1.AutoSize = true;
            this.labelI3_1.BackColor = System.Drawing.Color.Transparent;
            this.labelI3_1.ForeColor = System.Drawing.Color.Black;
            this.labelI3_1.Location = new System.Drawing.Point(50, 49);
            this.labelI3_1.Name = "labelI3_1";
            this.labelI3_1.Size = new System.Drawing.Size(0, 13);
            this.labelI3_1.TabIndex = 37;
            // 
            // labelI2_1
            // 
            this.labelI2_1.AutoSize = true;
            this.labelI2_1.BackColor = System.Drawing.Color.Transparent;
            this.labelI2_1.ForeColor = System.Drawing.Color.Black;
            this.labelI2_1.Location = new System.Drawing.Point(50, 31);
            this.labelI2_1.Name = "labelI2_1";
            this.labelI2_1.Size = new System.Drawing.Size(0, 13);
            this.labelI2_1.TabIndex = 36;
            // 
            // labelI1_1
            // 
            this.labelI1_1.AutoSize = true;
            this.labelI1_1.BackColor = System.Drawing.Color.Transparent;
            this.labelI1_1.ForeColor = System.Drawing.Color.Black;
            this.labelI1_1.Location = new System.Drawing.Point(50, 14);
            this.labelI1_1.Name = "labelI1_1";
            this.labelI1_1.Size = new System.Drawing.Size(0, 13);
            this.labelI1_1.TabIndex = 35;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.label22);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.labelI1);
            this.groupBox6.Controls.Add(this.labelI2);
            this.groupBox6.Controls.Add(this.labelI3);
            this.groupBox6.Controls.Add(this.labelI4);
            this.groupBox6.Location = new System.Drawing.Point(6, 27);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(102, 85);
            this.groupBox6.TabIndex = 35;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "������ 1";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 66);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(25, 13);
            this.label24.TabIndex = 38;
            this.label24.Text = "In =";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 49);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(25, 13);
            this.label23.TabIndex = 37;
            this.label23.Text = "Ic =";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 32);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(25, 13);
            this.label22.TabIndex = 36;
            this.label22.Text = "Ib =";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 15);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(25, 13);
            this.label21.TabIndex = 35;
            this.label21.Text = "Ia =";
            // 
            // labelI1
            // 
            this.labelI1.AutoSize = true;
            this.labelI1.BackColor = System.Drawing.Color.Transparent;
            this.labelI1.ForeColor = System.Drawing.Color.Black;
            this.labelI1.Location = new System.Drawing.Point(55, 15);
            this.labelI1.Name = "labelI1";
            this.labelI1.Size = new System.Drawing.Size(0, 13);
            this.labelI1.TabIndex = 31;
            // 
            // labelI2
            // 
            this.labelI2.AutoSize = true;
            this.labelI2.BackColor = System.Drawing.Color.Transparent;
            this.labelI2.ForeColor = System.Drawing.Color.Black;
            this.labelI2.Location = new System.Drawing.Point(55, 32);
            this.labelI2.Name = "labelI2";
            this.labelI2.Size = new System.Drawing.Size(0, 13);
            this.labelI2.TabIndex = 32;
            // 
            // labelI3
            // 
            this.labelI3.AutoSize = true;
            this.labelI3.BackColor = System.Drawing.Color.Transparent;
            this.labelI3.ForeColor = System.Drawing.Color.Black;
            this.labelI3.Location = new System.Drawing.Point(55, 49);
            this.labelI3.Name = "labelI3";
            this.labelI3.Size = new System.Drawing.Size(0, 13);
            this.labelI3.TabIndex = 33;
            // 
            // labelI4
            // 
            this.labelI4.AutoSize = true;
            this.labelI4.BackColor = System.Drawing.Color.Transparent;
            this.labelI4.ForeColor = System.Drawing.Color.Black;
            this.labelI4.Location = new System.Drawing.Point(55, 66);
            this.labelI4.Name = "labelI4";
            this.labelI4.Size = new System.Drawing.Size(0, 13);
            this.labelI4.TabIndex = 34;
            // 
            // _oyScroll
            // 
            this._oyScroll.Dock = System.Windows.Forms.DockStyle.Left;
            this._oyScroll.LargeChange = 1;
            this._oyScroll.Location = new System.Drawing.Point(0, 0);
            this._oyScroll.Maximum = 0;
            this._oyScroll.MaximumSize = new System.Drawing.Size(20, 0);
            this._oyScroll.MinimumSize = new System.Drawing.Size(20, 0);
            this._oyScroll.Name = "_oyScroll";
            this._oyScroll.Size = new System.Drawing.Size(20, 691);
            this._oyScroll.TabIndex = 13;
            this._oyScroll.Scroll += new System.Windows.Forms.ScrollEventHandler(this._oyScroll_Scroll);
            // 
            // ChartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 740);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximumSize = new System.Drawing.Size(1000, 779);
            this.MinimumSize = new System.Drawing.Size(1000, 736);
            this.Name = "ChartForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "������� �������������";
            this.Load += new System.EventHandler(this.ChartForm_Load);
            this.SizeChanged += new System.EventHandler(this.ChartForm_SizeChanged);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this._toolBar.ResumeLayout(false);
            this._toolBar.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            this.splitContainer4.ResumeLayout(false);
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel1.PerformLayout();
            this.splitContainer5.Panel2.ResumeLayout(false);
            this.splitContainer5.Panel2.PerformLayout();
            this.splitContainer5.ResumeLayout(false);
            this.splitContainer6.Panel1.ResumeLayout(false);
            this.splitContainer6.Panel1.PerformLayout();
            this.splitContainer6.Panel2.ResumeLayout(false);
            this.splitContainer6.Panel2.PerformLayout();
            this.splitContainer6.ResumeLayout(false);
            this.splitContainer7.Panel1.ResumeLayout(false);
            this.splitContainer7.Panel2.ResumeLayout(false);
            this.splitContainer7.ResumeLayout(false);
            this.splitContainer8.Panel1.ResumeLayout(false);
            this.splitContainer8.Panel2.ResumeLayout(false);
            this.splitContainer8.Panel2.PerformLayout();
            this.splitContainer8.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.Label _labelShkalaI;
        private System.Windows.Forms.TableLayoutPanel _analogChannelsPanel;
        private DAS_Net_XYChart _analogChart;
        private System.Windows.Forms.SplitContainer splitContainer6;
        private System.Windows.Forms.Label _labelShkalaU;
        private System.Windows.Forms.TableLayoutPanel _uAnalogChannelsUPanel;
        private DAS_Net_XYChart _analogUChart;
        private System.Windows.Forms.SplitContainer splitContainer7;
        private System.Windows.Forms.SplitContainer splitContainer8;
        private System.Windows.Forms.TableLayoutPanel _diskretChannelsUPanel;
        private DAS_Net_XYChart _diskretChart;
        private System.Windows.Forms.HScrollBar _oxScroll;
        private System.Windows.Forms.VScrollBar _oyScroll;
        private System.Windows.Forms.ToolStrip _toolBar;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        public System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label labelI4_1;
        private System.Windows.Forms.Label labelI3_1;
        private System.Windows.Forms.Label labelI2_1;
        private System.Windows.Forms.Label labelI1_1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label labelI1;
        private System.Windows.Forms.Label labelI2;
        private System.Windows.Forms.Label labelI3;
        private System.Windows.Forms.Label labelI4;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label UoLabel2;
        private System.Windows.Forms.Label UcLabel2;
        private System.Windows.Forms.Label UbLabel2;
        private System.Windows.Forms.Label UaLabel2;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label UaLabel1;
        private System.Windows.Forms.Label UbLabel1;
        private System.Windows.Forms.Label UcLabel1;
        private System.Windows.Forms.Label UoLabel1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label D2Label4;
        private System.Windows.Forms.Label D2Label3;
        private System.Windows.Forms.Label D2Label2;
        private System.Windows.Forms.Label D2Label1;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label D1Label1;
        private System.Windows.Forms.Label D1Label2;
        private System.Windows.Forms.Label D1Label3;
        private System.Windows.Forms.Label D1Label4;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label D2Label12;
        private System.Windows.Forms.Label D2Label11;
        private System.Windows.Forms.Label D2Label10;
        private System.Windows.Forms.Label D2Label9;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label D2Label8;
        private System.Windows.Forms.Label D2Label7;
        private System.Windows.Forms.Label D2Label6;
        private System.Windows.Forms.Label D2Label5;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label D1Label9;
        private System.Windows.Forms.Label D1Label10;
        private System.Windows.Forms.Label D1Label11;
        private System.Windows.Forms.Label D1Label12;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label D1Label5;
        private System.Windows.Forms.Label D1Label6;
        private System.Windows.Forms.Label D1Label7;
        private System.Windows.Forms.Label D1Label8;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label TimeLabel2;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label TimeLabel1;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label D2Label16;
        private System.Windows.Forms.Label D2Label15;
        private System.Windows.Forms.Label D2Label14;
        private System.Windows.Forms.Label D2Label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label D1Label13;
        private System.Windows.Forms.Label D1Label14;
        private System.Windows.Forms.Label D1Label15;
        private System.Windows.Forms.Label D1Label16;
        private System.Windows.Forms.Label DeltaLabel;

    }
}