﻿using System;
using System.Runtime.InteropServices;

namespace BEMN.TZL
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct OscJournal
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public int[] datatime;
        public int ready;
        public int point;
        public int begin;
        public int len;
        public int after;
        public Int32 alm;
        public Int32 rez;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SystemJournal
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public Int16[] datatime;
        public Int16 mes;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct AlarmJournal
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public Int16[] datatime;
        public Int16 difIndex;
        public Int16 paramIndex;
        public Int16 paramVal;
        public Int16 ustGroup;
        public Int16 Ida;
        public Int16 Idb;
        public Int16 Idc;
        public Int16 Iba;
        public Int16 Ibb;
        public Int16 Ibc;
        public Int16 Is1a;
        public Int16 Is1b;
        public Int16 Is1c;
        public Int16 Is2a;
        public Int16 Is2b;
        public Int16 Is2c;
        public Int16 Is3a;
        public Int16 Is3b;
        public Int16 Is3c;
        public Int16 Is1n;
        public Int16 Is10;
        public Int16 Is2n;
        public Int16 Is20;
        public Int16 Is3n;
        public Int16 Is30;
        public Int16 Is1d0;
        public Int16 Is2d0;
        public Int16 Is3d0;
        public Int16 Is1b0;
        public Int16 Is2b0;
        public Int16 Is3b0;
        public Int16 Ua;
        public Int16 Ub;
        public Int16 Uc;
        public Int16 Uab;
        public Int16 Ubc;
        public Int16 Uca;
        public Int16 Un;
        public Int16 U0;
        public Int16 U2;
        public Int16 F;
        public Int16 D1;
        public Int16 D2;
        public Int16 rez;
    }

    public struct AnalogBDStruct
    {
        public Int16 Ida;
        public Int16 Idb;
        public Int16 Idc;
        public Int16 Id1a;
        public Int16 Id1b;
        public Int16 Id1c;
        public Int16 Id2a;
        public Int16 Id2b;
        public Int16 Id2c;
        public Int16 Id5a;
        public Int16 Id5b;
        public Int16 Id5c;
        public Int16 Iba;
        public Int16 Ibb;
        public Int16 Ibc;
        public Int16 Is1a;
        public Int16 Is1b;
        public Int16 Is1c;
        public Int16 Is1n;
        public Int16 Is10;
        public Int16 Is2a;
        public Int16 Is2b;
        public Int16 Is2c;
        public Int16 Is2n;
        public Int16 Is20;
        public Int16 Is3a;
        public Int16 Is3b;
        public Int16 Is3c;
        public Int16 Is3n;
        public Int16 Is30;
        public Int16 Uab;
        public Int16 Ubc;
        public Int16 Uca;
        public Int16 U0;
        public Int16 U2;
        public Int16 I0;
        public Int16 Ia;
        public Int16 Ib;
        public Int16 Ic;
        public Int16 I20;
        public Int16 I2a;
        public Int16 I2b;
        public Int16 I2c;
        public Int16 I30;
        public Int16 I3a;
        public Int16 I3b;
        public Int16 I3c;
        public Int16 Un;
        public Int16 Ua;
        public Int16 Ub;
        public Int16 Uc;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct BDBIT_CTRL
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public Int16[] @base;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public Int16[] alarm;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public Int16[] param;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public Int16[] control;
    }
}
