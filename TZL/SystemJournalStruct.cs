﻿using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Forms.SystemJournal;

namespace BEMN.TZL
{
    public class SystemJournalStructTZL : SystemJournalAbstract
    {

        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d3}";
        private const string MESSAGE_WITH_CODE_PATTERN = "{0}";

        #endregion [Constants]

        public override string GetRecordTime => string.Format
        (
            DATE_TIME_PATTERN,
            _date,
            _month,
            _year,
            _hour,
            _minute,
            _second,
            _millisecond
        );

        public override string GetRecordMessage
        {
            get
            {
                if ((this.Message == 7) || (this.Message == 9)
                    || (this.Message == 11) || (this.Message == 13)
                    || (this.Message == 15))
                {
                    return string.Format(MESSAGE_WITH_CODE_PATTERN, StringsSjTZL.Message[this.Message], this.ModuleErrorCode);
                }
                if (this.Message >= 500)
                {
                    return this.MessagesList[this.Message - 500];
                }
                return StringsSjTZL.Message.Count > this.Message ? StringsSjTZL.Message[this.Message] : this.Message.ToString();
            }
        }
    }
}