namespace BEMN.TZL
{
    partial class AlarmJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._openSysJounralDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveSysJournalDlg = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this._journalCntLabel = new System.Windows.Forms.Label();
            this._journalProgress = new System.Windows.Forms.ProgressBar();
            this._deserializeBut = new System.Windows.Forms.Button();
            this._serializeBut = new System.Windows.Forms.Button();
            this._readBut = new System.Windows.Forms.Button();
            this._journalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._codeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._InCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._FCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UabCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UbcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UcaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._U0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._U1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._U2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UnCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._InSignals1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._InSignals2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exportButton = new System.Windows.Forms.Button();
            this._saveJournalHtmlDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // _openSysJounralDlg
            // 
            this._openSysJounralDlg.DefaultExt = "xml";
            this._openSysJounralDlg.Filter = "(*.xml) | *.xml";
            this._openSysJounralDlg.RestoreDirectory = true;
            this._openSysJounralDlg.Title = "������� ������  ������ ��� ��741";
            // 
            // _saveSysJournalDlg
            // 
            this._saveSysJournalDlg.DefaultExt = "xml";
            this._saveSysJournalDlg.FileName = "��741_������_������";
            this._saveSysJournalDlg.Filter = "(*.xml) | *.xml";
            this._saveSysJournalDlg.Title = "���������  ������ ������ ��� ��741";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "xml";
            this.openFileDialog1.Filter = "(*.xml) | *.xml";
            this.openFileDialog1.RestoreDirectory = true;
            this.openFileDialog1.Title = "������� ������  ������ ��� ��741";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "xml";
            this.saveFileDialog1.FileName = "��741_������_������";
            this.saveFileDialog1.Filter = "(*.xml) | *.xml";
            this.saveFileDialog1.Title = "���������  ������ ������ ��� ��741";
            // 
            // _journalCntLabel
            // 
            this._journalCntLabel.AutoSize = true;
            this._journalCntLabel.Location = new System.Drawing.Point(112, 360);
            this._journalCntLabel.Name = "_journalCntLabel";
            this._journalCntLabel.Size = new System.Drawing.Size(13, 13);
            this._journalCntLabel.TabIndex = 23;
            this._journalCntLabel.Text = "0";
            // 
            // _journalProgress
            // 
            this._journalProgress.Location = new System.Drawing.Point(6, 356);
            this._journalProgress.Name = "_journalProgress";
            this._journalProgress.Size = new System.Drawing.Size(100, 17);
            this._journalProgress.TabIndex = 22;
            // 
            // _deserializeBut
            // 
            this._deserializeBut.Location = new System.Drawing.Point(607, 324);
            this._deserializeBut.Name = "_deserializeBut";
            this._deserializeBut.Size = new System.Drawing.Size(126, 23);
            this._deserializeBut.TabIndex = 21;
            this._deserializeBut.Text = "��������� �� �����";
            this._deserializeBut.UseVisualStyleBackColor = true;
            this._deserializeBut.Click += new System.EventHandler(this._deserializeBut_Click);
            // 
            // _serializeBut
            // 
            this._serializeBut.Location = new System.Drawing.Point(490, 324);
            this._serializeBut.Name = "_serializeBut";
            this._serializeBut.Size = new System.Drawing.Size(111, 23);
            this._serializeBut.TabIndex = 20;
            this._serializeBut.Text = "��������� � ����";
            this._serializeBut.UseVisualStyleBackColor = true;
            this._serializeBut.Click += new System.EventHandler(this._serializeBut_Click);
            // 
            // _readBut
            // 
            this._readBut.Location = new System.Drawing.Point(5, 324);
            this._readBut.Name = "_readBut";
            this._readBut.Size = new System.Drawing.Size(75, 23);
            this._readBut.TabIndex = 19;
            this._readBut.Text = "���������";
            this._readBut.UseVisualStyleBackColor = true;
            this._readBut.Click += new System.EventHandler(this._readBut_Click);
            // 
            // _journalGrid
            // 
            this._journalGrid.AllowUserToAddRows = false;
            this._journalGrid.AllowUserToDeleteRows = false;
            this._journalGrid.BackgroundColor = System.Drawing.Color.White;
            this._journalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._journalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msgCol,
            this._codeCol,
            this._typeCol,
            this._IaCol,
            this._IbCol,
            this._IcCol,
            this._I0Col,
            this._I1Col,
            this._I2Col,
            this._IgCol,
            this._InCol,
            this._FCol,
            this._UabCol,
            this._UbcCol,
            this._UcaCol,
            this._U0Col,
            this._U1Col,
            this._U2Col,
            this._UnCol,
            this._InSignals1,
            this._InSignals2});
            this._journalGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this._journalGrid.Location = new System.Drawing.Point(0, 0);
            this._journalGrid.Name = "_journalGrid";
            this._journalGrid.RowHeadersVisible = false;
            this._journalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._journalGrid.Size = new System.Drawing.Size(737, 318);
            this._journalGrid.TabIndex = 18;
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "�";
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "�";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 43;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "�����";
            this._timeCol.HeaderText = "�����";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 46;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._msgCol.DataPropertyName = "���������";
            this._msgCol.HeaderText = "���������";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msgCol.Width = 71;
            // 
            // _codeCol
            // 
            this._codeCol.DataPropertyName = "��� �����������";
            this._codeCol.HeaderText = "��� �����������";
            this._codeCol.Name = "_codeCol";
            this._codeCol.ReadOnly = true;
            this._codeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._codeCol.Width = 112;
            // 
            // _typeCol
            // 
            this._typeCol.DataPropertyName = "��� �����������";
            this._typeCol.HeaderText = "��� �����������";
            this._typeCol.Name = "_typeCol";
            this._typeCol.ReadOnly = true;
            this._typeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._typeCol.Width = 112;
            // 
            // _IaCol
            // 
            this._IaCol.DataPropertyName = "Ia{A}";
            this._IaCol.HeaderText = "Ia{A}";
            this._IaCol.Name = "_IaCol";
            this._IaCol.ReadOnly = true;
            this._IaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaCol.Width = 56;
            // 
            // _IbCol
            // 
            this._IbCol.DataPropertyName = "Ib{A}";
            this._IbCol.HeaderText = "Ib{A}";
            this._IbCol.Name = "_IbCol";
            this._IbCol.ReadOnly = true;
            this._IbCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbCol.Width = 56;
            // 
            // _IcCol
            // 
            this._IcCol.DataPropertyName = "Ic{A}";
            this._IcCol.HeaderText = "Ic{A}";
            this._IcCol.Name = "_IcCol";
            this._IcCol.ReadOnly = true;
            this._IcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcCol.Width = 56;
            // 
            // _I0Col
            // 
            this._I0Col.DataPropertyName = "I0{A}";
            this._I0Col.HeaderText = "I0{A}";
            this._I0Col.Name = "_I0Col";
            this._I0Col.ReadOnly = true;
            this._I0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I0Col.Width = 56;
            // 
            // _I1Col
            // 
            this._I1Col.DataPropertyName = "I1{A}";
            this._I1Col.HeaderText = "I1{A}";
            this._I1Col.Name = "_I1Col";
            this._I1Col.ReadOnly = true;
            this._I1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I1Col.Width = 56;
            // 
            // _I2Col
            // 
            this._I2Col.DataPropertyName = "I2{A}";
            this._I2Col.HeaderText = "I2{A}";
            this._I2Col.Name = "_I2Col";
            this._I2Col.ReadOnly = true;
            this._I2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I2Col.Width = 56;
            // 
            // _IgCol
            // 
            this._IgCol.DataPropertyName = "Ig{A}";
            this._IgCol.HeaderText = "Ig{A}";
            this._IgCol.Name = "_IgCol";
            this._IgCol.ReadOnly = true;
            this._IgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IgCol.Width = 56;
            // 
            // _InCol
            // 
            this._InCol.DataPropertyName = "In{A}";
            this._InCol.HeaderText = "In{A}";
            this._InCol.Name = "_InCol";
            this._InCol.ReadOnly = true;
            this._InCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._InCol.Width = 56;
            // 
            // _FCol
            // 
            this._FCol.DataPropertyName = "F{��}";
            this._FCol.HeaderText = "F{��}";
            this._FCol.Name = "_FCol";
            this._FCol.ReadOnly = true;
            this._FCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._FCol.Width = 58;
            // 
            // _UabCol
            // 
            this._UabCol.DataPropertyName = "Uab{B}";
            this._UabCol.HeaderText = "Uab{B}";
            this._UabCol.Name = "_UabCol";
            this._UabCol.ReadOnly = true;
            this._UabCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UabCol.Width = 67;
            // 
            // _UbcCol
            // 
            this._UbcCol.DataPropertyName = "Ubc{B}";
            this._UbcCol.HeaderText = "Ubc{B}";
            this._UbcCol.Name = "_UbcCol";
            this._UbcCol.ReadOnly = true;
            this._UbcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UbcCol.Width = 67;
            // 
            // _UcaCol
            // 
            this._UcaCol.DataPropertyName = "Uca{B}";
            this._UcaCol.HeaderText = "Uca{B}";
            this._UcaCol.Name = "_UcaCol";
            this._UcaCol.ReadOnly = true;
            this._UcaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UcaCol.Width = 67;
            // 
            // _U0Col
            // 
            this._U0Col.DataPropertyName = "U0{B}";
            this._U0Col.HeaderText = "U0{B}";
            this._U0Col.Name = "_U0Col";
            this._U0Col.ReadOnly = true;
            this._U0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._U0Col.Width = 61;
            // 
            // _U1Col
            // 
            this._U1Col.DataPropertyName = "U1{B}";
            this._U1Col.HeaderText = "U1{B}";
            this._U1Col.Name = "_U1Col";
            this._U1Col.ReadOnly = true;
            this._U1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._U1Col.Width = 61;
            // 
            // _U2Col
            // 
            this._U2Col.DataPropertyName = "U2{B}";
            this._U2Col.HeaderText = "U2{B}";
            this._U2Col.Name = "_U2Col";
            this._U2Col.ReadOnly = true;
            this._U2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._U2Col.Width = 61;
            // 
            // _UnCol
            // 
            this._UnCol.DataPropertyName = "Un{B}";
            this._UnCol.HeaderText = "Un{B}";
            this._UnCol.Name = "_UnCol";
            this._UnCol.ReadOnly = true;
            this._UnCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UnCol.Width = 61;
            // 
            // _InSignals1
            // 
            this._InSignals1.DataPropertyName = "��.������� 1-8";
            this._InSignals1.HeaderText = "��.������� 1-8";
            this._InSignals1.Name = "_InSignals1";
            this._InSignals1.ReadOnly = true;
            this._InSignals1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._InSignals1.Width = 99;
            // 
            // _InSignals2
            // 
            this._InSignals2.DataPropertyName = "��.������� 9-16";
            this._InSignals2.HeaderText = "��.������� 9-16";
            this._InSignals2.Name = "_InSignals2";
            this._InSignals2.ReadOnly = true;
            this._InSignals2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._InSignals2.Width = 105;
            // 
            // _exportButton
            // 
            this._exportButton.Location = new System.Drawing.Point(357, 324);
            this._exportButton.Name = "_exportButton";
            this._exportButton.Size = new System.Drawing.Size(127, 23);
            this._exportButton.TabIndex = 24;
            this._exportButton.Text = "��������� � HTML";
            this._exportButton.UseVisualStyleBackColor = true;
            this._exportButton.Click += new System.EventHandler(this._exportButton_Click);
            // 
            // _saveJournalHtmlDialog
            // 
            this._saveJournalHtmlDialog.FileName = "��741 ������ ������";
            this._saveJournalHtmlDialog.Filter = "������ ������ �� 741 | *.html";
            // 
            // AlarmJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(737, 379);
            this.Controls.Add(this._exportButton);
            this.Controls.Add(this._journalCntLabel);
            this.Controls.Add(this._journalProgress);
            this.Controls.Add(this._deserializeBut);
            this.Controls.Add(this._serializeBut);
            this.Controls.Add(this._readBut);
            this.Controls.Add(this._journalGrid);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(753, 417);
            this.Name = "AlarmJournalForm";
            this.Text = "AlarmJournalForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AlarmJournalForm_FormClosing);
            this.Load += new System.EventHandler(this.AlarmJournalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog _openSysJounralDlg;
        private System.Windows.Forms.SaveFileDialog _saveSysJournalDlg;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label _journalCntLabel;
        private System.Windows.Forms.ProgressBar _journalProgress;
        private System.Windows.Forms.Button _deserializeBut;
        private System.Windows.Forms.Button _serializeBut;
        private System.Windows.Forms.Button _readBut;
        private System.Windows.Forms.DataGridView _journalGrid;
        private System.Windows.Forms.Button _exportButton;
        private System.Windows.Forms.SaveFileDialog _saveJournalHtmlDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _codeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _typeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _InCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _FCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UabCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UbcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UcaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _U0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _U1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _U2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UnCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _InSignals1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _InSignals2;
    }
}