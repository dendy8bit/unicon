﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AssemblyResources {
    using System;
    
    
    /// <summary>
    ///   Класс ресурса со строгой типизацией для поиска локализованных строк и т.д.
    /// </summary>
    // Этот класс создан автоматически классом StronglyTypedResourceBuilder
    // с помощью такого средства, как ResGen или Visual Studio.
    // Чтобы добавить или удалить член, измените файл .ResX и снова запустите ResGen
    // с параметром /str или перестройте свой проект VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Возвращает кэшированный экземпляр ResourceManager, использованный этим классом.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("BEMN.TZL.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Перезаписывает свойство CurrentUICulture текущего потока для всех
        ///   обращений к ресурсу с помощью этого класса ресурса со строгой типизацией.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;
        ///&lt;Belemn_TZL_BSBGL_Blocks_Library Count=&quot;12&quot;&gt;
        ///  &lt;Block name=&quot;&quot; type=&quot;&amp;amp;&quot; group=&quot;Простая логика&quot; propType=&quot;MultInp2Out&quot;  description=&quot;Логический элемент И&quot; posX=&quot;10&quot; posY=&quot;10&quot;&gt;
        ///    &lt;PinData count=&quot;3&quot;&gt;
        ///      &lt;Pin position=&quot;1&quot; name=&quot;&quot; type=&quot;direct&quot; orient=&quot;right&quot; /&gt;
        ///      &lt;Pin position=&quot;1&quot; name=&quot;&quot; type=&quot;direct&quot; orient=&quot;left&quot; /&gt;
        ///      &lt;Pin position=&quot;2&quot; name=&quot;&quot; type=&quot;direct&quot; orient=&quot;left&quot; /&gt;
        ///    &lt;/PinData&gt;
        ///    &lt;UserData/&gt;
        ///  &lt;/Block&gt;
        ///  &lt;Block name=&quot;&quot; type=&quot;|&quot; gro [остаток строки не уместился]&quot;;.
        /// </summary>
        internal static string BlockLib {
            get {
                return ResourceManager.GetString("BlockLib", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Drawing.Icon, аналогичного (Значок).
        /// </summary>
        internal static System.Drawing.Icon config {
            get {
                object obj = ResourceManager.GetObject("config", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap go1 {
            get {
                object obj = ResourceManager.GetObject("go1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap ja {
            get {
                object obj = ResourceManager.GetObject("ja", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap js {
            get {
                object obj = ResourceManager.GetObject("js", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Drawing.Icon, аналогичного (Значок).
        /// </summary>
        internal static System.Drawing.Icon measuring {
            get {
                object obj = ResourceManager.GetObject("measuring", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap mr100 {
            get {
                object obj = ResourceManager.GetObject("mr100", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на &lt;?xml version=&quot;1.0&quot; encoding=&quot;WINDOWS-1251&quot;?&gt;
        ///&lt;xsl:stylesheet version=&quot;1.0&quot;
        ///xmlns:xsl=&quot;http://www.w3.org/1999/XSL/Transform&quot;&gt;
        ///&lt;xsl:template match=&quot;/&quot;&gt;
        ///  &lt;html&gt;
        ///&lt;head&gt;
        /// 
        ///&lt;/head&gt;
        ///&lt;body&gt;
        /// &lt;h3&gt;Устройство МР741. Журнал аварий&lt;/h3&gt;
        ///	&lt;p&gt;&lt;/p&gt;
        ///    &lt;table border=&quot;1&quot;&gt;
        ///      &lt;tr bgcolor=&quot;#c1ced5&quot;&gt;
        ///         &lt;th&gt;Номер&lt;/th&gt;
        ///         &lt;th&gt;Время&lt;/th&gt;
        ///         &lt;th&gt;Сообщение&lt;/th&gt;
        ///		     &lt;th&gt;Код повреждения&lt;/th&gt;
        ///		     &lt;th&gt;Тип повреждения&lt;/th&gt;
        ///		     &lt;th&gt;Ia,A&lt;/th&gt;
        ///		     &lt;th&gt;Ib,A&lt;/th&gt;
        ///		     &lt;th&gt;Ic,A&lt;/th&gt;
        ///	 [остаток строки не уместился]&quot;;.
        /// </summary>
        internal static string MR741AJ {
            get {
                return ResourceManager.GetString("MR741AJ", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на &lt;?xml version=&quot;1.0&quot; encoding=&quot;WINDOWS-1251&quot;?&gt;
        ///&lt;xsl:stylesheet version=&quot;1.0&quot;
        ///xmlns:xsl=&quot;http://www.w3.org/1999/XSL/Transform&quot;&gt;
        ///&lt;xsl:template match=&quot;/&quot;&gt;
        ///  &lt;html&gt;
        ///&lt;head&gt;
        /// 
        ///&lt;/head&gt;
        ///&lt;body&gt;
        /// &lt;h3&gt;Устройство МР741. Журнал аварий&lt;/h3&gt;
        ///	&lt;p&gt;&lt;/p&gt;
        ///    &lt;table border=&quot;1&quot;&gt;
        ///      &lt;tr bgcolor=&quot;#c1ced5&quot;&gt;
        ///         &lt;th&gt;Номер&lt;/th&gt;
        ///         &lt;th&gt;Время&lt;/th&gt;
        ///         &lt;th&gt;Сообщение&lt;/th&gt;
        ///		     &lt;th&gt;Код повреждения&lt;/th&gt;
        ///		     &lt;th&gt;Тип повреждения&lt;/th&gt;
        ///		     &lt;th&gt;Ia,A&lt;/th&gt;
        ///		     &lt;th&gt;Ib,A&lt;/th&gt;
        ///		     &lt;th&gt;Ic,A&lt;/th&gt;
        ///	 [остаток строки не уместился]&quot;;.
        /// </summary>
        internal static string MR741AJOld {
            get {
                return ResourceManager.GetString("MR741AJOld", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на &lt;?xml version=&quot;1.0&quot; encoding=&quot;WINDOWS-1251&quot;?&gt;
        ///&lt;!-- Edited by XMLSpy® --&gt;
        ///&lt;xsl:stylesheet version=&quot;1.0&quot;
        ///xmlns:xsl=&quot;http://www.w3.org/1999/XSL/Transform&quot;&gt;
        ///  &lt;xsl:template match=&quot;/&quot;&gt;
        ///    &lt;html&gt;
        ///      &lt;head&gt;
        ///       &lt;script type=&quot;text/javascript&quot;&gt;
        ///	function translateBoolean(value, elementId){
        /// 		var result = &quot;&quot;;
        /// 		if(value.toString().toLowerCase() == &quot;true&quot;)
        ///  			result = &quot;Да&quot;;
        /// 		else if(value.toString().toLowerCase() == &quot;false&quot;)
        ///  			result = &quot;Нет&quot;;
        ///		else 
        ///			result = &quot;некорректное значение&quot;
        /// [остаток строки не уместился]&quot;;.
        /// </summary>
        internal static string MR741Main {
            get {
                return ResourceManager.GetString("MR741Main", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на &lt;?xml version=&quot;1.0&quot; encoding=&quot;WINDOWS-1251&quot;?&gt;
        ///&lt;!-- Edited by XMLSpy® --&gt;
        ///&lt;xsl:stylesheet version=&quot;1.0&quot;
        ///xmlns:xsl=&quot;http://www.w3.org/1999/XSL/Transform&quot;&gt;
        ///  &lt;xsl:template match=&quot;/&quot;&gt;
        ///    &lt;html&gt;
        ///      &lt;head&gt;
        ///        &lt;script type=&quot;text/javascript&quot;&gt;
        ///          function translateBoolean(value, elementId){
        ///          var result = &quot;&quot;;
        ///          if(value.toString().toLowerCase() == &quot;true&quot;)
        ///          result = &quot;Да&quot;;
        ///          else if(value.toString().toLowerCase() == &quot;false&quot;)
        ///          result = &quot;Нет&quot;;
        ///      [остаток строки не уместился]&quot;;.
        /// </summary>
        internal static string MR741Res {
            get {
                return ResourceManager.GetString("MR741Res", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на &lt;?xml version=&quot;1.0&quot; encoding=&quot;WINDOWS-1251&quot;?&gt;
        ///&lt;!-- Edited by XMLSpy® --&gt;
        ///&lt;xsl:stylesheet version=&quot;1.0&quot;
        ///xmlns:xsl=&quot;http://www.w3.org/1999/XSL/Transform&quot;&gt;
        ///&lt;xsl:template match=&quot;/&quot;&gt;
        ///  &lt;html&gt;
        ///&lt;head&gt;
        /// 
        ///&lt;/head&gt;
        ///&lt;body&gt;
        /// &lt;h3&gt;Устройство МР741. Журнал системы&lt;/h3&gt;
        ///	&lt;p&gt;&lt;/p&gt;
        ///    &lt;table border=&quot;1&quot;&gt;
        ///      &lt;tr bgcolor=&quot;#c1ced5&quot;&gt;
        ///         &lt;th&gt;Номер&lt;/th&gt;
        ///         &lt;th&gt;Время&lt;/th&gt;
        ///         &lt;th&gt;Сообщение&lt;/th&gt;
        ///	&lt;/tr&gt;
        ///      
        ///      
        ///	  &lt;xsl:for-each select=&quot;DocumentElement/МР741_журнал_системы&quot;&gt;
        ///	  &lt;tr&gt;
        ///       [остаток строки не уместился]&quot;;.
        /// </summary>
        internal static string MR741SJ {
            get {
                return ResourceManager.GetString("MR741SJ", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на &lt;?xml version=&quot;1.0&quot; encoding=&quot;WINDOWS-1251&quot;?&gt;
        ///&lt;!-- Edited by XMLSpy® --&gt;
        ///&lt;xsl:stylesheet version=&quot;1.0&quot;
        ///xmlns:xsl=&quot;http://www.w3.org/1999/XSL/Transform&quot;&gt;
        ///&lt;xsl:template match=&quot;/&quot;&gt;
        ///  &lt;html&gt;
        ///&lt;head&gt;
        /// 
        ///&lt;/head&gt;
        ///&lt;body&gt;
        /// &lt;h3&gt;Устройство МР741. Журнал системы&lt;/h3&gt;
        ///	&lt;p&gt;&lt;/p&gt;
        ///    &lt;table border=&quot;1&quot;&gt;
        ///      &lt;tr bgcolor=&quot;#c1ced5&quot;&gt;
        ///         &lt;th&gt;Номер&lt;/th&gt;
        ///         &lt;th&gt;Время&lt;/th&gt;
        ///         &lt;th&gt;Сообщение&lt;/th&gt;
        ///	&lt;/tr&gt;
        ///      
        ///      
        ///	  &lt;xsl:for-each select=&quot;DocumentElement/МР74X_журнал_системы&quot;&gt;
        ///	  &lt;tr&gt;
        ///       [остаток строки не уместился]&quot;;.
        /// </summary>
        internal static string MR741SJold {
            get {
                return ResourceManager.GetString("MR741SJold", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Drawing.Icon, аналогичного (Значок).
        /// </summary>
        internal static System.Drawing.Icon oscilloscope {
            get {
                object obj = ResourceManager.GetObject("oscilloscope", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Drawing.Icon, аналогичного (Значок).
        /// </summary>
        internal static System.Drawing.Icon programming {
            get {
                object obj = ResourceManager.GetObject("programming", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap startEmul {
            get {
                object obj = ResourceManager.GetObject("startEmul", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap stopSpl {
            get {
                object obj = ResourceManager.GetObject("stopSpl", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
    }
}
