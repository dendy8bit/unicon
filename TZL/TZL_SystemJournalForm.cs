using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.TZL.Properties;
using BEMN.MBServer;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.Remoting.Messaging;
using System.Web.UI.Design;
using System.Xml;
using AssemblyResources;
using BEMN.Devices.Structures;
using SchemeEditorSystem.ResourceLibs;


namespace BEMN.TZL
{
    public partial class SystemJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "������� � ������� - {0}";
        private const string JOURNAL_SAVED = "������ ��������";
        private const string READ_FAIL = "���������� ��������� ������";
        private const string TABLE_NAME_SYS = "��741_������_�������";
        private const string NUMBER_SYS = "�����";
        private const string TIME_SYS = "�����";
        private const string MESSAGE_SYS = "���������";
        private const string SYSTEM_JOURNAL = "������ �������";
        private const string DEVICE_NAME = "MR741";
        #endregion [Constants]

        #region [Private fields]

        private readonly TZL _device;
        private readonly MemoryEntity<OneWordStruct> _indexSysJournal;
        private readonly MemoryEntity<SystemJournalStructTZL> _systemJournal;
        private DataTable _dataTable;
        private int _recordNumber;
        private string OldOrNew;
        //private DataTable _table = new DataTable("��741_������_�������");

        #endregion

        

        public SystemJournalForm()
        {
            this.InitializeComponent();
        }

        public SystemJournalForm(TZL device)
        {
            this.InitializeComponent();
            this._device = device;
            
            this._dataTable = this.GetJournalDataTable();

            this._device.SystemJournalLoadOk += HandlerHelper.CreateHandler(this, this.OnSysJournalLoadOk);
            this._device.SystemJournalRecordLoadOk += this._device_SystemJournalRecordLoadOk;

        }

        private void OnSysJournalLoadOk()
        {
            //this._readSysJournalBut.Enabled = true;
            this._journalProgress.Value = TZL.SYSTEMJOURNAL_RECORD_CNT;
            
            if (this._dataTable.Rows.Count == 0)
            {
                MessageBox.Show("������ ����.");
                _deserializeSysJournalBut.Enabled = true;
                _readSysJournalBut.Enabled = true;
                return;
            }

            this.ButtonsEnabled = true;
            _deserializeSysJournalBut.Enabled = true;
        }

        void _device_SystemJournalRecordLoadOk(object sender, int index)
        {
            try
            {
                Invoke(new OnDeviceIndexEventHandler(this.OnSysJournalIndexLoadOk), index);
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnSysJournalIndexLoadOk(int i)
        {
            this._journalProgress.Increment(1);
            this._journalCntLabel.Text = (i + 1).ToString();
            this._statusLabel.Text = "������� � �������: " + (i + 1);
            this._dataTable.Rows.Add(new object[] { i + 1, this._device.SystemJournal[i].time, this._device.SystemJournal[i].msg });
        }
        

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(TZL); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(SystemJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.js; }
        }

        public string NodeName
        {
            get { return "������ �������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _serializeSysJournalBut_Click(object sender, EventArgs e)
        {
            this.SaveJournalToFile();
        }
        
        
        private void _deserializeSysJournalBut_Click(object sender, EventArgs e)
        {
            this.LoadJournalFromFile();
        }
        
        private void LoadJournalFromFile()
        {
            if (this._openJournalDialog.ShowDialog() != DialogResult.OK) return;
            this._dataTable.Clear();
            try
            {
                if (Path.GetExtension(this._openJournalDialog.FileName).ToLower().Contains("bin"))
                {
                    byte[] file = File.ReadAllBytes(this._openJournalDialog.FileName);
                    SystemJournalStructTZL journal = new SystemJournalStructTZL();
                    int size = journal.GetSize();
                    List<byte> journalBytes = new List<byte>();
                    journalBytes.AddRange(Common.SwapArrayItems(file));
                    int j = 0;
                    this._recordNumber = 0;
                    int countRecord = journalBytes.Count / size;
                    for (int i = 0; j < countRecord; i = i + size)
                    {
                        journal.InitStruct(journalBytes.GetRange(i, size).ToArray());
                        journal.MessagesList = PropFormsStrings.GetNewJournalList();

                        this._systemJournal.Value = journal;
                        this.ReadRecordFromBinFile();
                        j++;
                    }
                }
                else
                {
                    XmlDocument xd = new XmlDocument();
                    xd.Load(this._openJournalDialog.FileName);

                    XmlElement xRoot = xd.DocumentElement;

                    if (xRoot.FirstChild.Name != this._dataTable.TableName)
                    {
                        this._dataTable.TableName = xRoot.FirstChild.Name;
                    }

                    this._dataTable.ReadXml(this._openJournalDialog.FileName);
                    this.RecordNumber = this._dataTable.Rows.Count;
                    if (RecordNumber != 0) ButtonsEnabled = true;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("���������� ��������� ������ �������.",
                    "��������!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        public int RecordNumber
        {
            get { return this._recordNumber; }
            set
            {
                this._recordNumber = value;
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                this.statusStrip.Update();
            }
        }

        private void SystemJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.RemoveSystemJournal();
        }

        private void _readSysJournalBut_Click(object sender, EventArgs e)
        {
            this.LoadJournal();
        }

        private void LoadJournal()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            //this._readSysJournalBut.Enabled = false;
            //this._device.RemoveSystemJournal();
            //this._journalProgress.Value = 0;
            //this._journalProgress.Maximum = TZL.SYSTEMJOURNAL_RECORD_CNT;
            //this._dataTable.Clear();
            //this._recordNumber = 0;
            //this._systemJournal.LoadStruct();

            ButtonsEnabled = false;
            _deserializeSysJournalBut.Enabled = false;

            this._readSysJournalBut.Enabled = false;
            this._device.SystemJournal = new TZL.CSystemJournal();
            this._device.RemoveSystemJournal();
            this._journalProgress.Value = 0;
            this._journalProgress.Maximum = TZL.SYSTEMJOURNAL_RECORD_CNT;
            this._dataTable.Rows.Clear();
            this._device.LoadSystemJournal();
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                if (this._dataTable.TableName == "��74X_������_�������")
                {
                    HtmlExport.Export(this._dataTable, this._saveJournalHtmlDialog.FileName, Resources.MR741SJold);
                }
                else if (this._dataTable.TableName == "��741_������_�������")
                {
                    HtmlExport.Export(this._dataTable, this._saveJournalHtmlDialog.FileName, Resources.MR741SJ);
                }
                else
                {
                    MessageBox.Show("������ ����.");
                    ButtonsEnabled = false;
                    return;
                }
                this._journalCntLabel.Text = "������ ��������";
                ButtonsEnabled = true;
            }
        }

        private void ReadRecordFromBinFile()
        {
            if (!this._systemJournal.Value.IsEmpty)
            {
                this.RecordNumber++;
                this._dataTable.Rows.Add(
                    this.RecordNumber.ToString(CultureInfo.InvariantCulture),
                    this._systemJournal.Value.GetRecordTime,
                    this._systemJournal.Value.GetRecordMessage);
                this._systemJournalGrid.Update();
            }
            else
            {
                this.ButtonsEnabled = true;
            }
        }

        private bool ButtonsEnabled
        {
            set
            {
                this._serializeSysJournalBut.Enabled = 
                    this._readSysJournalBut.Enabled = this._exportButton.Enabled = value;
            }
        }

        private void SaveJournalToFile()
        {
            if (DialogResult.OK == this._saveJournalDialog.ShowDialog())
            {
                this._dataTable.WriteXml(this._saveJournalDialog.FileName);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }

        private DataTable GetJournalDataTable()
        {
            var table = new DataTable(TABLE_NAME_SYS);
            table.Columns.Add(NUMBER_SYS);
            table.Columns.Add(TIME_SYS);
            table.Columns.Add(MESSAGE_SYS);
            return table;
        }

        private void SystemJournalForm_Load(object sender, EventArgs e)
        {
            this._dataTable = this.GetJournalDataTable();
            this._systemJournalGrid.DataSource = this._dataTable;

            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this.LoadJournal();
                return;
            }

            ButtonsEnabled = false;
        }

        private void SystemJournalForm_Activated(object sender, EventArgs e)
        {
            StringsSjTZL.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);
        }
    }
}