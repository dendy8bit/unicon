<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <script type="text/javascript">
          function translateBoolean(value, elementId){
          var result = "";
          if(value.toString().toLowerCase() == "true")
          result = "��";
          else if(value.toString().toLowerCase() == "false")
          result = "���";
          else
          result = "������������ ��������"
          document.getElementById(elementId).innerHTML = result;
          }
        </script>
      </head>
      <body>
       <h3>
         ���������� ��741. ������ �� <xsl:value-of select="TZL/Version"/>
        </h3>

        <!--�������  �������-->
        <h3><b>������� ������� </b></h3>
        <b>��������� ����</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FFFFCC">
            <th>��������� ��� ��, �</th>
            <th>��������� ��� ����, �</th>
            <th>����. ��� ��������, In</th>
            <th>��� ��</th>
          </tr>

          <tr align="center">
            <td><xsl:value-of select="TZL/TTtoDevice"/></td>
            <td><xsl:value-of select="TZL/TTNPtoDevice"/></td>
            <td><xsl:value-of select="TZL/������������_���"/></td>
            <td><xsl:value-of select="TZL/���_�T"/></td>
          </tr>

        </table>
        <p></p>
        <b>��� ��</b>
        <table border="1" cellspacing="0">

          <tr bgcolor="FFFFCC">
            <th>���������� TH </th>
            <th>���������� THH�</th>
            <th>������.��</th>
            <th>������.��H�</th>
            <th>��� ��</th>
          </tr>

          <tr align="center">
            <td><xsl:value-of select="TZL/��"/></td>
            <td><xsl:value-of select="TZL/����"/></td>
            <td><xsl:value-of select="TZL/�������������_��"/></td>
            <td><xsl:value-of select="TZL/�������������_����"/></td>
            <td><xsl:value-of select="TZL/���_��"/></td>
          </tr>

        </table>
        <p></p>
        <b>����������� ����� �����������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FFFFCC">
            <th>������������ ���</th>
            <th>���(��/��)</th>
          </tr>

          <tr align="center">
            <td>
              <xsl:value-of select="TZL/���_���"/>
            </td>
            <td>
              <xsl:value-of select="TZL/���"/>
            </td>
          </tr>
        </table>
        
        <p></p>
        <b>������� ���������� �������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FFFFCC">
            <th>����� ��</th>
            <th>������������</th>
          </tr>
          <xsl:for-each select="TZL/InpSignals/ArrayOfString">
            <tr>
              <xsl:choose>
                <xsl:when test="position() > 4">
                   <td><b><xsl:value-of select="position()"/> ���</b></td>
                </xsl:when>
                <xsl:otherwise>
                  <td><b><xsl:value-of select="position()"/> �</b></td>
                </xsl:otherwise>
              </xsl:choose>
              <td>
                <xsl:for-each select="string">
                  <xsl:if test="current() !='���'">
                    <xsl:if test="current() ='��'">
                      �<xsl:value-of select="position()"/>
                    </xsl:if>
                    <xsl:if test="current() ='������'">
                      ^�<xsl:value-of select="position()"/>
                    </xsl:if>
                  </xsl:if>
                </xsl:for-each>
              </td>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>
        
        <b>������� �������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FFFFCC">
            <th>���� ���������</th>
            <th>���� ��������</th>
            <th>������� ���������</th>
            <th>������� ��������</th>
            <th>����� ������������</th>
            <th>������ �������</th>
            <xsl:if test="TZL/Version > 3.00">
              <th>���������� ����</th>
            </xsl:if>
          </tr>

          <tr align="center">
            <td><xsl:value-of select="TZL/����_���������"/></td>
            <td><xsl:value-of select="TZL/����_��������"/></td>
            <td><xsl:value-of select="TZL/�������_��������"/></td>
            <td><xsl:value-of select="TZL/�������_���������"/></td>
            <td><xsl:value-of select="TZL/�����_������������"/></td>
            <td><xsl:value-of select="TZL/������������_�������"/></td>
            <xsl:if test="TZL/Version > 3.00">
              <td>
                <xsl:value-of select="TZL/����������_����"/>
              </td>
            </xsl:if>
          </tr>
        </table>
        
        <p></p>

        <table border="1" cellspacing="0">
          <td>
            <b>������� ����������</b>
            <table border="1" cellspacing="0">
              <tr bgcolor="FFFFCC">
                <th>�� ������</th>
                <th>�� �����</th>
                <th>�������</th>
                <th>�� ����</th>

              </tr>

              <tr align="center">
                <td>
                  <xsl:value-of select="TZL/������_��_������"/>
                </td>
                <td>
                  <xsl:value-of select="TZL/������_��_�����"/>
                </td>
                <td>
                  <xsl:value-of select="TZL/������_��_��������"/>
                </td>
                <td>
                  <xsl:value-of select="TZL/������_��_����"/>
                </td>
              </tr>
            </table>

            <b>�����������</b>
            <table border="1" cellspacing="0">
              <tr bgcolor="FFFFCC">
                <th>��������� ���������</th>
                <th>��������� ��������</th>
                <th>�������������</th>
                <th>����������</th>
                <th>����� ����, ��</th>
                <th>��� ����, In</th>
                <th>������� ��, ��</th>
                <th>����. �����, ��</th>
                <th>�������� ����� ���-�</th>

              </tr>

              <tr align="center">
                <td>
                  <xsl:value-of select="TZL/�����������_���������"/>
                </td>
                <td>
                  <xsl:value-of select="TZL/�����������_��������"/>
                </td>
                <td>
                  <xsl:value-of select="TZL/�����������_������"/>
                </td>
                <td>
                  <xsl:value-of select="TZL/�����������_����������"/>
                </td>
                <td>
                  <xsl:value-of select="TZL/�����������_�����_����"/>
                </td>
                <td>
                  <xsl:value-of select="TZL/�����������_���_����"/>
                </td>
                <td>
                  <xsl:value-of select="TZL/�����������_�������"/>
                </td>
                <td>
                  <xsl:value-of select="TZL/�����������_������������"/>
                </td>
                <td>
                  <xsl:value-of select="TZL/���������"/>
                </td>
              </tr>
            </table>
          </td>
        </table>
        <p></p>
        
        <b>������������ ����������� ������ ��� ���</b>
        <table border="1">
          <tr bgcolor="#ced5c1">
            <th>����� 1-16</th>
          </tr>
          <tr align="center">
            <td>
              <xsl:value-of select="TZL/�����"/>
            </td>
          </tr>
        </table>

        <p></p>
        <h3>������������ ������������</h3>
        <table border="1">
          <tr bgcolor="#ced5c1">
            <th>����� � ������������ ������������</th>
            <th>��������</th>
            <th>������������ ����������, %</th>
          </tr>
          <tr align="center">
            <td>
              <xsl:if test="TZL/DeviceVersion &lt;2.00">
                <xsl:value-of select="TZL/OscilloscopePeriod"/>
              </xsl:if>
              <xsl:if test="(TZL/DeviceVersion &gt;=2.00)">
                <xsl:value-of select="TZL/OscilloscopePeriod3_0"/>
              </xsl:if>
              <xsl:if test="(contains(TZL/DeviceVersion,'S'))">
                <xsl:value-of select="TZL/OscilloscopePeriodS"/>
              </xsl:if>
            </td>
            <td>
              <xsl:value-of select="TZL/OscilloscopeFix"/>
            </td>
            <td>
              <xsl:value-of select="TZL/OscilloscopePercent"/>
            </td>
          </tr>
        </table>
        <p></p>

        <h3>�������� �������</h3>
        
        <b>�������� ���������� �������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="#00FFFF">
            <th>�����</th>
            <th>������������</th>
          </tr>
          <xsl:for-each select="TZL/VlsXml/ArrayOfString">
            <tr>
              <td>
                <xsl:value-of select="position()"/>
              </td>
              <td>
                <xsl:for-each select="string">
                  <xsl:value-of select="current()"/>|
                </xsl:for-each>
              </td>
            </tr>
          </xsl:for-each>
        </table>
        
        <b>������������ �������� ����</b>
        <table border="1">
          <tr bgcolor="#c1ced5">
            <th>����� ����</th>
            <th>���</th>
            <th>������</th>
            <th>�������, ��</th>
          </tr>
          <xsl:for-each select="TZL/��������_����">
            <tr align="center">
              <td><xsl:value-of select="position()"/></td>
              <td><xsl:value-of select="@���"/></td>
              <td><xsl:value-of select="@������_����"/></td>
              <td><xsl:value-of select="@�������"/></td>
            </tr>
          </xsl:for-each>
        </table>
        
        <p></p>
        <b>���� �������������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FFFFCC">
            <th>����������</th>
            <th>������ ���</th>
            <th>�����������</th>
            <th>�����������</th>
            <th>����������</th>
            <th>�������</th>
            <th>������, ��</th>
          </tr>
          <tr align="center">
            <td>
              <xsl:variable  name = "v1" select = "TZL/�������������[1]" />
              <xsl:if test="$v1 = 'false' " >��� </xsl:if>
              <xsl:if test="$v1 = 'true' " > ���� </xsl:if>
            </td>
            <td>
              <xsl:variable  name = "v2" select= "TZL/�������������[2]" />
              <xsl:if test="$v2 = 'false' " >��� </xsl:if>
              <xsl:if test="$v2 = 'true' " > ���� </xsl:if>
            </td>
            <td>
              <xsl:variable  name = "v3" select= "TZL/�������������[3]" />
              <xsl:if test="$v3 = 'false' " >��� </xsl:if>
              <xsl:if test="$v3 = 'true' " > ���� </xsl:if>
            </td>
            <td>
              <xsl:variable  name = "v4" select= "TZL/�������������[5]" />
              <xsl:if test="$v4 = 'false' " >��� </xsl:if>
              <xsl:if test="$v4 = 'true' " > ���� </xsl:if>
            </td>
            <td>
              <xsl:variable  name = "v5" select = "TZL/�������������[7]" />
              <xsl:if test="$v5 = 'false' " >��� </xsl:if>
              <xsl:if test="$v5 = 'true' " > ���� </xsl:if>
            </td>
            <td>
              <xsl:variable  name = "v6" select= "TZL/�������������[8]" />
              <xsl:if test="$v6 = 'false' " >��� </xsl:if>
              <xsl:if test="$v6 = 'true' " > ���� </xsl:if>
            </td>
            <td><xsl:value-of select="TZL/�������_�������������"/></td>
          </tr>
        </table>
        
        <p></p>
        <b>������������ ������������ �����������</b>
        <table border="1">
      <tr bgcolor="#c1ced5">
        <th>����� ����������</th>
        <th>���</th>
        <th>������</th>
        <th>����� �����. �� ���. ���.</th>
        <th>����� �����. �� ��</th>
        <th>����� �����. �� ��</th>
      </tr>
      <xsl:for-each select="TZL/��������_����������">
        <tr align="center">
          <td>
            <xsl:value-of select="position()"/>
          </td>
          <td>
            <xsl:value-of select="@���"/>
          </td>
          <td>
            <xsl:value-of select="@������_����������"/>
          </td>
           <td><xsl:for-each select="@���������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
          <td><xsl:for-each select="@������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
            <td><xsl:for-each select="@�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
        </tr>
      </xsl:for-each>
    </table>
        <p></p>

        <h3>������������ ������� �����</h3>
        <table border="1">
          <tr bgcolor="#bcbcbc">
            <th>����� ��</th>
            <th>�����</th>
            <th>����-��</th>
            <th>���� ����.</th>
            <th>T����, ��</th>
            <th>�������</th>
            <th>��� �� ��������</th>
            <th>���� �����.</th>
            <th>������, ��</th>
            <th>���.</th>
            <th>����</th>
            <th>���</th>
            <th>���</th>
            <th>�����</th>
          </tr>
          <xsl:for-each select="TZL/�������_������">
            <tr align="center">
              <td>
                <xsl:value-of select="position()"/>
              </td>
              <td>
                <xsl:if test="../DeviceVersion &lt;1.11">
                  <xsl:value-of select="@�����"/>
                </xsl:if>
                <xsl:if test="../DeviceVersion =1.11">
                  <xsl:value-of select="@�����_v1.11"/>
                </xsl:if>
                <xsl:if test="(../DeviceVersion &gt;=1.12)or(contains(../DeviceVersion,'S'))">
                  <xsl:value-of select="@�����_v1.12"/>
                </xsl:if>
              </td>
              <td>
                <xsl:value-of select="@����������_�����"/>
              </td>
              <td>
                <xsl:value-of select="@������������_�����"/>
              </td>
              <td>
                <xsl:value-of select="@������������_�����"/>
              </td>
                <td><xsl:for-each select="@�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:for-each select="@�������_���"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>    
              <td>
                <xsl:value-of select="@�������_�����"/>
              </td>
              <td>
                <xsl:value-of select="@�������_�����"/>
              </td>
              <td><xsl:for-each select="@���"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td><xsl:for-each select="@����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td><xsl:for-each select="@���"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:for-each select="@���"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:for-each select="@�����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
            </tr>
          </xsl:for-each>
        </table>
        <h3>������������ ����������</h3>
        <h4>������������ ���</h4>
        <table border="1">
          <tr bgcolor="#bcbcbc">
            <th>������������ ���</th>
            <th>����. ����-��</th>
            <th>� ����-�� ����� ������� ���-�, ��</th>
            <th>T�����, ��</th>
            <th>T1����, ��</th>
            <th>T2����, ��</th>
            <th>T3����, ��</th>
            <th>T4����, ��</th>
            <th>��� �� ����������. ����-�</th>
          </tr>
          <tr align="center">
            <td>
              <xsl:value-of select="TZL/������������_���"/>
            </td>
            <td>
              <xsl:value-of select="TZL/����������_���"/>
            </td>
            <td>
              <xsl:value-of select="TZL/�����_����������_���"/>
            </td>
            <td>
              <xsl:value-of select="TZL/�����_����������_���"/>
            </td>
            <td>
              <xsl:value-of select="TZL/�����_1_�����_���"/>
            </td>
            <td>
              <xsl:value-of select="TZL/�����_2_�����_���"/>
            </td>
            <td>
              <xsl:value-of select="TZL/�����_3_�����_���"/>
            </td>
            <td>
              <xsl:value-of select="TZL/�����_4_�����_���"/>
            </td>
            <td>
              <xsl:value-of select="TZL/������_���_��_��������������"/>
            </td>
          </tr>
        </table>
        <p></p>
        <h4>������������ ���</h4>
        <table border="1">
          <tr bgcolor="#bcbcbc">
            <th>���� ��� �� ����.�������</th>
            <th>���� ��� �� ����.���. ����-�</th>
            <th>���� ��� �� ����������. ����-�</th>
            <th>���� ��� �� ������</th>
            <th>������ ����-��</th>
            <th>������ ������ ����-��</th>
            <th>������. ������ ����-�� �� ����.���-�</th>
            <th>������ �����</th>
            <th>������ ����������</th>
            <th>�����, ��</th>
            <th>������ ��������</th>
            <th>������, ��</th>
            <th>�����, ��</th>
          </tr>
          <tr align="center">
            <td id="td1">
              <script>
                translateBoolean(<xsl:value-of select="TZL/����������_�������_���"/>,"td1");
              </script>
            </td>
            <td id="td2">
              <script>
                translateBoolean(<xsl:value-of select="TZL/�������_����������_�����������_���"/>,"td2");
              </script>
            </td>
            <td id="td3">
              <script>
                translateBoolean(<xsl:value-of select="TZL/��������������_���"/>,"td3");
              </script>
            </td>
            <td id="td4">
              <script>
                translateBoolean(<xsl:value-of select="TZL/������������_������_���"/>,"td4");
              </script>
            </td>
            <td>
              <xsl:value-of select="TZL/����������_���"/>
            </td>
            <td>
              <xsl:value-of select="TZL/�����_����������_���"/>
            </td>
            <td id="td5">
              <script>
                translateBoolean(<xsl:value-of select="TZL/����������_������_���_��_���������_�����������"/>,"td5");
              </script>
            </td>
            <td>
              <xsl:value-of select="TZL/������_���"/>
            </td>
            <td>
              <xsl:value-of select="TZL/������������_���"/>
            </td>
            <td>
              <xsl:value-of select="TZL/�����_������������_���"/>
            </td>
            <td>
              <xsl:value-of select="TZL/�������_���"/>
            </td>
            <td>
              <xsl:value-of select="TZL/�����_��������_���"/>
            </td>
            <td>
              <xsl:value-of select="TZL/�����_����������_���"/>
            </td>
          </tr>
        </table>
        <p></p>
        <h4>������������ ���</h4>
        <table border="1">
          <tr bgcolor="#bcbcbc">
            <th>������������ ���</th>
            <th>������� �� ���� ���, I�</th>
          </tr>
          <tr align="center">
            <td>
              <xsl:if test="TZL/DeviceVersion &lt; 1.14">
                <xsl:value-of select="TZL/������������_���"/>
              </xsl:if>
              <xsl:if test="(TZL/DeviceVersion &gt;=1.14)or(contains(TZL/DeviceVersion,'S'))">
                <xsl:value-of select="TZL/������������_���114"/>
              </xsl:if>
            </td>
            <td>
              <xsl:value-of select="TZL/�������_���"/>
            </td>
          </tr>
        </table>
        <p></p>

        <h3>������������ �����</h3>
        <h4>���� ��. �������� �������</h4>
        <table border="1">
          <tr bgcolor="#d0d0d0">
            <th>I</th>
            <th>I2</th>
            <th>I0</th>
            <th>In</th>
          </tr>
          <tr align="center">
            <td><xsl:value-of select="TZL/����_I_���"/></td>
            <td><xsl:value-of select="TZL/����_I2_���"/></td>
            <td><xsl:value-of select="TZL/����_I0_���"/></td>
            <td><xsl:value-of select="TZL/����_In_���"/></td>
          </tr>
        </table>
        <h4>������������ ������� �����. �������� �������</h4>
        <table border="1">
          <tr bgcolor="#d0d0d0">
            <th>C������ ���.������</th>
            <th>�����</th>
            <th>����-��</th>
            <th>���� �� U</th>
            <th>���. �����, �</th>
            <th>�����������</th>
            <th>����������</th>
            <th>��������</th>
            <th>���. ����., I�</th>
            <th>���-��</th>
            <th>T����, ��/����.</th>
            <th>���.</th>
            <th>T�����., ��</th>
            <th>�����������</th>
            <th>����</th>
            <th>���</th>
            <th>���</th>
          </tr>
          <xsl:for-each select="TZL/�������_������_��������">
            <tr align="center">
              <td>
                <xsl:if test="position() &lt;=4">
                  I><xsl:value-of select="position()"/>
                </xsl:if>
                <xsl:if test="position() =5">I2></xsl:if>
                <xsl:if test="position() =6">I2>></xsl:if>
                <xsl:if test="position() =7">I0></xsl:if>
                <xsl:if test="position() =8">I0>></xsl:if>
                <xsl:if test="position() =9">In></xsl:if>
                <xsl:if test="position() =10">In>></xsl:if>
                <xsl:if test="position() =11">Ig></xsl:if>
                <xsl:if test="position() =12">I2/I1</xsl:if>
              </td>
              <td>
                <xsl:if test="../DeviceVersion &lt;1.11">
                  <xsl:value-of select="@�����"/>
                </xsl:if>
                <xsl:if test="(../DeviceVersion &gt;= 1.11)or(contains(../DeviceVersion,'S'))">
                  <xsl:value-of select="@�����_v1.11"/>
                </xsl:if>
              </td>
              <td>
                <xsl:value-of select="@����������_�����"/>
              </td>
              <td><xsl:for-each select="@����_��_U"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td>
                <xsl:value-of select="@����_��_U_�������"/>
              </td>
              <td>
                <xsl:value-of select="@�����������"/>
              </td>
              <td>
                <xsl:value-of select="@����������_��������"/>
              </td>
              <td>
                <xsl:value-of select="@��������"/>
              </td>
              <td>
                <xsl:value-of select="@������������_�������"/>
              </td>
              <td>
                <xsl:value-of select="@��������������"/>
              </td>
              <td>
                <xsl:value-of select="@������������_�����"/>
              </td>
              <td><xsl:for-each select="@���������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td>
                <xsl:value-of select="@���������_�����"/>
              </td>


              <xsl:if test="../DeviceVersion &lt;1.11">
                <xsl:element name="td">
                  <xsl:attribute name="id">
                    OscI_<xsl:value-of select="position()"/>
                  </xsl:attribute>
                  <script>
                    translateBoolean(<xsl:value-of select="@���"/>,"OscI_<xsl:value-of select="position()"/>");
                  </script>
                </xsl:element>
              </xsl:if>
              <xsl:if test="(../DeviceVersion &gt;=1.11)or(contains(../DeviceVersion,'S'))">
                <td>
                  <xsl:value-of select="@�����������_v1_11"/>
                </td>
              </xsl:if>
              <td><xsl:for-each select="@����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              
              <td><xsl:for-each select="@���"> <xsl:if test="current() ='false'">��� </xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                
              <td><xsl:for-each select="@���"> <xsl:if test="current() ='false'">��� </xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>  
            </tr>
          </xsl:for-each>
        </table>
        <p></p>
        
          <h4>������������ ����� �� ����������. �������� �������</h4>


        <b>���������� �� U &lt; 5�</b>
        <table border = "1">
            <tr bgcolor="#d0d0d0">
              <th>U&lt;</th>
              <th>U&lt;&lt;</th>
            </tr>
            <tr align="center">
                  <td>
                    <xsl:for-each select="TZL/������_����������_��������">
                    <xsl:if test ="position() = 3">
                      <xsl:value-of select="@����������_v1.11"/>
                    </xsl:if>
                     </xsl:for-each>
                  </td>
                  <td>
                    <xsl:for-each select="TZL/������_����������_��������">
                    <xsl:if test ="position() = 4">
                      <xsl:value-of select="@����������_v1.11"/>
                    </xsl:if>
                    </xsl:for-each>
                  </td>
            </tr>
          </table>
          <p></p>
        <p>
          <table border="1">
            <tr bgcolor="#d0d0d0">
              <th>C������ ���.������</th>
              <th>�����</th>
              <th>����������</th>
              <th>��������</th>
              <th>������� ����., �</th>
              <th>����� ����., ��</th>
              <th>����.</th>
              <th>��� ��</th>
              <th>������� ��������, �</th>
              <th>����� ��������, ��</th>
              <th>�����������</th>
              <th>����</th>
              <th>���</th>
              <th>���</th>
              <xsl:if test="(TZL/DeviceVersion &gt;=1.11)or(contains(TZL/DeviceVersion,'S'))">
                <!--<th>����-�� Uabc ���� 5�</th>-->
                <th>�����</th>
              </xsl:if>
            </tr>
            <xsl:for-each select="TZL/������_����������_��������">
              <tr align="center">
                <td>
                  <xsl:if test="position() =1">U></xsl:if>
                  <xsl:if test="position() =2">U>></xsl:if>
                  <xsl:if test="position() =3">U&lt;</xsl:if>
                  <xsl:if test="position() =4">U&lt;&lt;</xsl:if>
                  <xsl:if test="position() =5">U2></xsl:if>
                  <xsl:if test="position() =6">U2>></xsl:if>
                  <xsl:if test="position() =7">U0></xsl:if>
                  <xsl:if test="position() =8">U0>></xsl:if>
                </td>
                <td>
                  <xsl:if test="../DeviceVersion &lt;1.11">
                    <xsl:value-of select="@�����"/>
                  </xsl:if>
                  <xsl:if test="(../DeviceVersion &gt;=1.11)or(contains(../DeviceVersion,'S'))">
                    <xsl:value-of select="@�����_v1.11"/>
                  </xsl:if>
                </td>
                <td>
                  <xsl:value-of select="@����������_�����"/>
                </td>
                <td>
                  <xsl:if test="../DeviceVersion &lt;1.11">
                    <xsl:value-of select="@��������"/>
                  </xsl:if>
                  <xsl:if test="(../DeviceVersion &gt;=1.11)or(contains(../DeviceVersion,'S'))">
                    <xsl:value-of select="@��������_v1.11"/>
                  </xsl:if>
                </td>
                <td>
                  <xsl:value-of select="@������������_�������"/>
                </td>
                <td>
                  <xsl:value-of select="@������������_�����"/>
                </td>
                <td><xsl:for-each select="@�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>  
                  <td><xsl:for-each select="@���_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>  
                <td>
                  <xsl:value-of select="@�������_�������"/>
                </td>
                <td>
                  <xsl:value-of select="@�������_�����"/>
                </td>
                <xsl:if test="../DeviceVersion &lt;1.11">
                  <xsl:element name="td">
                    <xsl:attribute name="id">
                      OscU_<xsl:value-of select="position()"/>
                    </xsl:attribute>
                    <script>
                      translateBoolean(<xsl:value-of select="@���"/>,"OscU_<xsl:value-of select="position()"/>");
                    </script>
                  </xsl:element>
                </xsl:if>
                <xsl:if test="(../DeviceVersion &gt;=1.11)or(contains(../DeviceVersion,'S'))">
                  <td>
                    <xsl:value-of select="@�����������_v1_11"/>
                  </td>
                </xsl:if>
                
               <td><xsl:for-each select="@����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>  

               <td><xsl:for-each select="@���"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>  

               <td><xsl:for-each select="@���"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>  
                
                <xsl:if test="(../DeviceVersion &gt;=1.11)or(contains(../DeviceVersion,'S'))">
                  <!--<td>
                    <xsl:value-of select="@����������_v1.11"/>
                  </td>-->
                  <td><xsl:for-each select="@Reset"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                </xsl:if>
              </tr>
            </xsl:for-each>
          </table>
        </p>
        <p>
          <h4>������������ ����� �� �������. �������� �������</h4>
          <table border="1">
            <tr bgcolor="#d0d0d0">
              <th>C������ ���.������</th>
              <th>�����</th>
              <th>����������</th>
              <th>������� ����., ��</th>
              <th>����� ����., ��</th>
              <th>����.</th>
              <th>��� ��</th>
              <th>����� ��������, ��</th>
              <th>������� ��, ��</th>
              <th>�����������</th>
              <th>����</th>
              <th>���</th>
              <th>���</th>
              <xsl:if test="(TZL/DeviceVersion &gt;=1.11)or(contains(TZL/DeviceVersion,'S'))">
                <th>�����</th>
              </xsl:if>
            </tr>
            <xsl:for-each select="TZL/������_��_�������_��������">
              <tr align="center">
                <td>
                  <xsl:if test="position() =1">F></xsl:if>
                  <xsl:if test="position() =2">F>></xsl:if>
                  <xsl:if test="position() =3">F&lt;</xsl:if>
                  <xsl:if test="position() =4">F&lt;&lt;</xsl:if>
                </td>
                <td>
                  <xsl:if test="../DeviceVersion &lt;1.1">
                    <xsl:value-of select="@�����"/>
                  </xsl:if>
                  <xsl:if test="../DeviceVersion =1.1">
                    <xsl:value-of select="@�����_v1.1"/>
                  </xsl:if>
                  <xsl:if test="(../DeviceVersion &gt;=1.11)or(contains(../DeviceVersion,'S'))">
                    <xsl:value-of select="@�����_v1.11"/>
                  </xsl:if>
                </td>
                <td>
                  <xsl:value-of select="@����������_�����"/>
                </td>
                <td>
                  <xsl:value-of select="@������������_�������"/>
                </td>
                <td>
                  <xsl:value-of select="@������������_�����"/>
                </td>
                <td><xsl:for-each select="@�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:for-each select="@�������_���"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td>
                  <xsl:value-of select="@�������_�����"/>
                </td>
                <td>
                  <xsl:value-of select="@������������_���"/>
                </td>
                <xsl:if test="../DeviceVersion &lt;1.11">
                  <xsl:element name="td">
                    <xsl:attribute name="id">
                      OscF_<xsl:value-of select="position()"/>
                    </xsl:attribute>
                    <script>
                      translateBoolean(<xsl:value-of select="@���"/>,"OscF_<xsl:value-of select="position()"/>");
                    </script>
                  </xsl:element>
                </xsl:if>
                <xsl:if test="(../DeviceVersion &gt;=1.11)or(contains(../DeviceVersion,'S'))">
                  <td>
                    <xsl:value-of select="@�����������_v1_11"/>
                  </td>
                </xsl:if>
                <td><xsl:for-each select="@����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:for-each select="@���"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                    <td><xsl:for-each select="@���"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <xsl:if test="(../DeviceVersion &gt;=1.11)or(contains(../DeviceVersion,'S'))">
                  <td><xsl:for-each select="@Reset"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>    
                </xsl:if>
              </tr>
            </xsl:for-each>
          </table>
        </p><p>
          <h4>���� ��. ��������� �������</h4>
          <table border="1">
            <tr bgcolor="#d0d0d0">
              <th>I</th>
              <th>I2</th>
              <th>I0</th>
              <th>In</th>
            </tr>
            <tr align="center">
              <td><xsl:value-of select="TZL/����_I_���"/></td>
              <td><xsl:value-of select="TZL/����_I2_���"/></td>
              <td><xsl:value-of select="TZL/����_I0_���"/></td>
              <td><xsl:value-of select="TZL/����_In_���"/></td>
            </tr>
          </table>
          
          <h4>������������ ������� �����. ��������� �������</h4>
          <table border="1">    
            <tr bgcolor="#d0d0d0">
               <th>C������ ���.������</th>
                  <th>�����</th>
                  <th>����-��</th>
                  <th>���� �� U</th>
                  <th>���. �����, �</th>
                  <th>�����������</th>
                  <th>����������</th>
                  <th>��������</th>
                  <th>���. ����., I�</th>
                  <th>���-��</th>
                  <th>T����, ��/����.</th>
                  <th>���.</th>
                  <th>T�����., ��</th>
                  <th>�����������</th>
                  <th>����</th>
                  <th>���</th>
                  <th>���</th>
            </tr>
            <xsl:for-each select="TZL/�������_������_���������">
              <tr align ="center">
                <td>
                  <xsl:if test="position() &lt;=4">
                    I><xsl:value-of select="position()"/>
                  </xsl:if>
                  <xsl:if test="position() =5">I2></xsl:if>
                  <xsl:if test="position() =6">I2>></xsl:if>
                  <xsl:if test="position() =7">I0></xsl:if>
                  <xsl:if test="position() =8">I0>></xsl:if>
                  <xsl:if test="position() =9">In></xsl:if>
                  <xsl:if test="position() =10">In>></xsl:if>
                  <xsl:if test="position() =11">Ig></xsl:if>
                  <xsl:if test="position() =12">I2/I1</xsl:if>
                </td>
            <td><xsl:if test="../DeviceVersion &lt;1.11"><xsl:value-of select="@�����"/></xsl:if><xsl:if test="(../DeviceVersion &gt;=1.11)or(contains(../DeviceVersion,'S'))"><xsl:value-of select="@�����_v1.11"/></xsl:if></td>
           <td><xsl:value-of select="@����������_�����"/></td>
           <td>
             <xsl:for-each select="@����_��_U">
               <xsl:if test="current() ='false'">���	</xsl:if>
               <xsl:if test="current() ='true'">��</xsl:if>
             </xsl:for-each>
           </td>
           <td><xsl:value-of select="@����_��_U_�������"/></td>
           <td><xsl:value-of select="@�����������"/></td>
           <td><xsl:value-of select="@����������_��������"/></td>
           <td><xsl:value-of select="@��������"/></td>
           <td><xsl:value-of select="@������������_�������"/></td>
           <td><xsl:value-of select="@��������������"/></td>
           <td><xsl:value-of select="@������������_�����"/></td>
           <xsl:element name="td">
               <xsl:attribute name="id">Uskorenierez_<xsl:value-of select="position()"/></xsl:attribute>            
               <script>translateBoolean(<xsl:value-of select="@���������"/>,"Uskorenierez_<xsl:value-of select="position()"/>");</script>
           </xsl:element>
           <td><xsl:value-of select="@���������_�����"/></td>


	<xsl:if test="../DeviceVersion &lt;1.11">
        <xsl:element name="td">
             <xsl:attribute name="id">OscI_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"OscI_<xsl:value-of select="position()"/>");</script>
        </xsl:element>
	</xsl:if>
        <xsl:if test="(../DeviceVersion &gt;=1.11)or(contains(../DeviceVersion,'S'))">
        <td><xsl:value-of select="@�����������_v1_11"/></td>
        </xsl:if>
         <xsl:element name="td">
             <xsl:attribute name="id">UROVIrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@����"/>,"UROVIrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">apvIrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"apvIrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">avrIrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"avrIrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
      </tr>
      </xsl:for-each>
      </table>
 </p>
<h4>������������ ����� �� ����������. ��������� �������</h4>
        
          <b>���������� �� U &lt; 5�</b>
        <table border = "1">
            <tr bgcolor="#d0d0d0">
              <th>U&lt;</th>
              <th>U&lt;&lt;</th>
            </tr>
            <tr align="center">
                  <td>
                    <xsl:for-each select="TZL/������_����������_���������">
                    <xsl:if test ="position() = 3">
                      <xsl:value-of select="@����������_v1.11"/>
                    </xsl:if>
                     </xsl:for-each>
                  </td>
                  <td>
                    <xsl:for-each select="TZL/������_����������_���������">
                    <xsl:if test ="position() = 4">
                      <xsl:value-of select="@����������_v1.11"/>
                    </xsl:if>
                    </xsl:for-each>
                  </td>
            </tr>
          </table>
        <p></p>
<table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������ ���.������</th>
              <th>�����</th>
              <th>����������</th>
              <th>��������</th>
              <th>������� ����., �</th>
              <th>����� ����., ��</th>
              <th>����.</th>
              <th>��� ��</th>
              <th>������� ��������, �</th>
              <th>����� ��������, ��</th>
              <th>�����������</th>
              <th>����</th>
              <th>���</th>
              <th>���</th>
              <xsl:if test="(TZL/DeviceVersion &gt;=1.11)or(contains(TZL/DeviceVersion,'S'))">
                <!--<th>����-�� Uabc ���� 5�</th>-->
                <th>�����</th>
              </xsl:if>
      </tr>
      <xsl:for-each select="TZL/������_����������_���������">
       <tr align ="center">
          <td>
                  <xsl:if test="position() =1">U></xsl:if>
                  <xsl:if test="position() =2">U>></xsl:if>
                  <xsl:if test="position() =3">U&lt;</xsl:if>
                  <xsl:if test="position() =4">U&lt;&lt;</xsl:if>
                  <xsl:if test="position() =5">U2></xsl:if>
                  <xsl:if test="position() =6">U2>></xsl:if>
                  <xsl:if test="position() =7">U0></xsl:if>
                  <xsl:if test="position() =8">U0>></xsl:if>
                </td>
          <td><xsl:if test="../DeviceVersion &lt;1.11"><xsl:value-of select="@�����"/></xsl:if><xsl:if test="(../DeviceVersion &gt;=1.11)or(contains(../DeviceVersion,'S'))"><xsl:value-of select="@�����_v1.11"/></xsl:if></td>
         <td><xsl:value-of select="@����������_�����"/></td>
          <td><xsl:if test="../DeviceVersion &lt;1.11"><xsl:value-of select="@��������"/></xsl:if><xsl:if test="(../DeviceVersion &gt;=1.11)or(contains(../DeviceVersion,'S'))"><xsl:value-of select="@��������_v1.11"/></xsl:if></td>
         <td><xsl:value-of select="@������������_�������"/></td>
         <td><xsl:value-of select="@������������_�����"/></td>
 	 <xsl:element name="td">
             <xsl:attribute name="id">vozvratUrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@�������"/>,"vozvratUrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
 	 <xsl:element name="td">
             <xsl:attribute name="id">APVvozvratUrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���_�������"/>,"APVvozvratUrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <td><xsl:value-of select="@�������_�������"/></td>
         <td><xsl:value-of select="@�������_�����"/></td>
	<xsl:if test="../DeviceVersion &lt;1.11">
        <xsl:element name="td">
             <xsl:attribute name="id">OscUrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"OscUrez_<xsl:value-of select="position()"/>");</script>
        </xsl:element>
	</xsl:if>
        <xsl:if test="(../DeviceVersion &gt;=1.11)or(contains(../DeviceVersion,'S'))">
        <td><xsl:value-of select="@�����������_v1_11"/></td>
        </xsl:if>
         <xsl:element name="td">
             <xsl:attribute name="id">UROVUrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@����"/>,"UROVUrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">apvUrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"apvUrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">avrUrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"avrUrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:if test="(../DeviceVersion &gt;=1.11)or(contains(../DeviceVersion,'S'))">
         <!--<td><xsl:value-of select="@����������_v1.11"/></td>-->
         <xsl:element name="td">
             <xsl:attribute name="id">resetUrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@Reset"/>,"resetUrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
        </xsl:if>
      </tr>
      </xsl:for-each>
      </table>
<h4>������������ ����� �� �������. ��������� �������</h4>
<table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������ ���.������</th>
              <th>�����</th>
              <th>����������</th>
              <th>������� ����., ��</th>
              <th>����� ����., ��</th>
              <th>����.</th>
              <th>��� ��</th>
              <th>����� ��������, ��</th>
              <th>������� ��, ��</th>
              <th>�����������</th>
              <th>����</th>
              <th>���</th>
              <th>���</th>
              <xsl:if test="(TZL/DeviceVersion &gt;=1.11)or(contains(TZL/DeviceVersion,'S'))">
                <th>�����</th>
              </xsl:if>
      </tr>
      <xsl:for-each select="TZL/������_��_�������_���������">
       <tr align ="center">
          <td>
                  <xsl:if test="position() =1">F></xsl:if>
                  <xsl:if test="position() =2">F>></xsl:if>
                  <xsl:if test="position() =3">F&lt;</xsl:if>
                  <xsl:if test="position() =4">F&lt;&lt;</xsl:if>
                </td>
          <td><xsl:if test="../DeviceVersion &lt;1.1"><xsl:value-of select="@�����"/></xsl:if><xsl:if test="../DeviceVersion =1.1"><xsl:value-of select="@�����_v1.1"/></xsl:if><xsl:if test="(../DeviceVersion &gt;=1.11)or(contains(../DeviceVersion,'S'))"><xsl:value-of select="@�����_v1.11"/></xsl:if></td>
         <td><xsl:value-of select="@����������_�����"/></td>
         <td><xsl:value-of select="@������������_�������"/></td>
         <td><xsl:value-of select="@������������_�����"/></td>
 	 <xsl:element name="td">
             <xsl:attribute name="id">vozvratFrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@�������"/>,"vozvratFrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
 	 <xsl:element name="td">
             <xsl:attribute name="id">APVvozvratFrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@�������_���"/>,"APVvozvratFrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <td><xsl:value-of select="@������������_���"/></td>
         <td><xsl:value-of select="@�������_�����"/></td>
	<xsl:if test="../DeviceVersion &lt;1.11">
        <xsl:element name="td">
             <xsl:attribute name="id">OscFrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"OscFrez_<xsl:value-of select="position()"/>");</script>
        </xsl:element>
	</xsl:if>
        <xsl:if test="(../DeviceVersion &gt;=1.11)or(contains(../DeviceVersion,'S'))">
        <td><xsl:value-of select="@�����������_v1_11"/></td>
        </xsl:if>
         <xsl:element name="td">
             <xsl:attribute name="id">UROVFrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@����"/>,"UROVFrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">apvFrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"apvFrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">avrFrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"avrFrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:if test="(../DeviceVersion &gt;=1.11)or(contains(../DeviceVersion,'S'))">
         <xsl:element name="td">
             <xsl:attribute name="id">resetFrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@Reset"/>,"resetFrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
        </xsl:if>
      </tr>
      </xsl:for-each>
      </table>
    </body>
  </html>
</xsl:template>
</xsl:stylesheet>
