namespace BEMN.TZL
{
    partial class SystemJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this._saveJournalDialog = new System.Windows.Forms.SaveFileDialog();
            this._openJournalDialog = new System.Windows.Forms.OpenFileDialog();
            this._deserializeSysJournalBut = new System.Windows.Forms.Button();
            this._serializeSysJournalBut = new System.Windows.Forms.Button();
            this._readSysJournalBut = new System.Windows.Forms.Button();
            this._systemJournalGrid = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exportButton = new System.Windows.Forms.Button();
            this._saveJournalHtmlDialog = new System.Windows.Forms.SaveFileDialog();
            this.statusStrip = new System.Windows.Forms.ToolStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripLabel();
            this._journalCntLabel = new System.Windows.Forms.Label();
            this._journalProgress = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this._systemJournalGrid)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _saveJournalDialog
            // 
            this._saveJournalDialog.DefaultExt = "xml";
            this._saveJournalDialog.FileName = "��741_�������������";
            this._saveJournalDialog.Filter = "��741- ������ ������� | *.xml";
            this._saveJournalDialog.Title = "��������� ������ ������� ��� ��741";
            // 
            // _openJournalDialog
            // 
            this._openJournalDialog.DefaultExt = "xml";
            this._openJournalDialog.Filter = "��741 ������ �������(*.xml)|*.xml|��741 ������ �������(*.bin)|*.bin";
            this._openJournalDialog.RestoreDirectory = true;
            this._openJournalDialog.Title = "������� ������ ������� ��� ��741";
            // 
            // _deserializeSysJournalBut
            // 
            this._deserializeSysJournalBut.Location = new System.Drawing.Point(250, 311);
            this._deserializeSysJournalBut.Name = "_deserializeSysJournalBut";
            this._deserializeSysJournalBut.Size = new System.Drawing.Size(118, 23);
            this._deserializeSysJournalBut.TabIndex = 15;
            this._deserializeSysJournalBut.Text = "��������� �� �����";
            this._deserializeSysJournalBut.UseVisualStyleBackColor = true;
            this._deserializeSysJournalBut.Click += new System.EventHandler(this._deserializeSysJournalBut_Click);
            // 
            // _serializeSysJournalBut
            // 
            this._serializeSysJournalBut.Location = new System.Drawing.Point(123, 311);
            this._serializeSysJournalBut.Name = "_serializeSysJournalBut";
            this._serializeSysJournalBut.Size = new System.Drawing.Size(111, 23);
            this._serializeSysJournalBut.TabIndex = 14;
            this._serializeSysJournalBut.Text = "��������� � ����";
            this._serializeSysJournalBut.UseVisualStyleBackColor = true;
            this._serializeSysJournalBut.Click += new System.EventHandler(this._serializeSysJournalBut_Click);
            // 
            // _readSysJournalBut
            // 
            this._readSysJournalBut.Location = new System.Drawing.Point(5, 311);
            this._readSysJournalBut.Name = "_readSysJournalBut";
            this._readSysJournalBut.Size = new System.Drawing.Size(102, 23);
            this._readSysJournalBut.TabIndex = 13;
            this._readSysJournalBut.Text = "���������";
            this._readSysJournalBut.UseVisualStyleBackColor = true;
            this._readSysJournalBut.Click += new System.EventHandler(this._readSysJournalBut_Click);
            // 
            // _systemJournalGrid
            // 
            this._systemJournalGrid.AllowUserToAddRows = false;
            this._systemJournalGrid.AllowUserToDeleteRows = false;
            this._systemJournalGrid.BackgroundColor = System.Drawing.Color.White;
            this._systemJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._systemJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this._systemJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._systemJournalGrid.Name = "_systemJournalGrid";
            this._systemJournalGrid.RowHeadersVisible = false;
            this._systemJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._systemJournalGrid.Size = new System.Drawing.Size(499, 303);
            this._systemJournalGrid.TabIndex = 12;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "�����";
            this.Column1.HeaderText = "�";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 30;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "�����";
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.Column2.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column2.HeaderText = "����/�����";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Width = 140;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.DataPropertyName = "���������";
            this.Column3.HeaderText = "���������";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _exportButton
            // 
            this._exportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._exportButton.Location = new System.Drawing.Point(374, 311);
            this._exportButton.Name = "_exportButton";
            this._exportButton.Size = new System.Drawing.Size(121, 23);
            this._exportButton.TabIndex = 23;
            this._exportButton.Text = "��������� � HTML";
            this._exportButton.UseVisualStyleBackColor = true;
            this._exportButton.Click += new System.EventHandler(this._exportButton_Click);
            // 
            // _saveJournalHtmlDialog
            // 
            this._saveJournalHtmlDialog.DefaultExt = "xml";
            this._saveJournalHtmlDialog.FileName = "��741 ������ �������";
            this._saveJournalHtmlDialog.Filter = "��741 ������ ������� | *.html";
            this._saveJournalHtmlDialog.Title = "���������  ������ ������� ��� ��741";
            // 
            // statusStrip
            // 
            this.statusStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 350);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(499, 25);
            this.statusStrip.TabIndex = 24;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(13, 22);
            this._statusLabel.Text = "  ";
            // 
            // _journalCntLabel
            // 
            this._journalCntLabel.AutoSize = true;
            this._journalCntLabel.Location = new System.Drawing.Point(141, 355);
            this._journalCntLabel.Name = "_journalCntLabel";
            this._journalCntLabel.Size = new System.Drawing.Size(13, 13);
            this._journalCntLabel.TabIndex = 17;
            this._journalCntLabel.Text = "0";
            this._journalCntLabel.Visible = false;
            // 
            // _journalProgress
            // 
            this._journalProgress.Location = new System.Drawing.Point(12, 353);
            this._journalProgress.Name = "_journalProgress";
            this._journalProgress.Size = new System.Drawing.Size(123, 16);
            this._journalProgress.TabIndex = 16;
            this._journalProgress.Visible = false;
            // 
            // SystemJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 375);
            this.Controls.Add(this._exportButton);
            this.Controls.Add(this._journalCntLabel);
            this.Controls.Add(this._journalProgress);
            this.Controls.Add(this._deserializeSysJournalBut);
            this.Controls.Add(this._serializeSysJournalBut);
            this.Controls.Add(this._readSysJournalBut);
            this.Controls.Add(this._systemJournalGrid);
            this.Controls.Add(this.statusStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "SystemJournalForm";
            this.Text = "SystemJournalForm";
            this.Activated += new System.EventHandler(this.SystemJournalForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SystemJournalForm_FormClosing);
            this.Load += new System.EventHandler(this.SystemJournalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._systemJournalGrid)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog _saveJournalDialog;
        private System.Windows.Forms.OpenFileDialog _openJournalDialog;
        private System.Windows.Forms.Button _deserializeSysJournalBut;
        private System.Windows.Forms.Button _serializeSysJournalBut;
        private System.Windows.Forms.Button _readSysJournalBut;
        private System.Windows.Forms.DataGridView _systemJournalGrid;
        private System.Windows.Forms.Button _exportButton;
        private System.Windows.Forms.SaveFileDialog _saveJournalHtmlDialog;
        private System.Windows.Forms.ToolStrip statusStrip;
        private System.Windows.Forms.ToolStripLabel _statusLabel;
        private System.Windows.Forms.Label _journalCntLabel;
        private System.Windows.Forms.ProgressBar _journalProgress;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}