namespace BEMN.TZL.Forms
{
    partial class OscilloscopeResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MAINTABLE = new System.Windows.Forms.TableLayoutPanel();
            this.hScrollBar4 = new System.Windows.Forms.HScrollBar();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MAINPANEL = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.button16 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this._discretChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.UYminus = new System.Windows.Forms.Button();
            this.UYPlus = new System.Windows.Forms.Button();
            this._voltageChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.UScroll = new System.Windows.Forms.VScrollBar();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.button4 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.S1Scroll = new System.Windows.Forms.VScrollBar();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.S1Yminus = new System.Windows.Forms.Button();
            this.S1YPlus = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this._s1TokChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.MarkersTable = new System.Windows.Forms.TableLayoutPanel();
            this.Marker1CB = new System.Windows.Forms.CheckBox();
            this.Marker2CB = new System.Windows.Forms.CheckBox();
            this.Marker1TrackBar = new System.Windows.Forms.TrackBar();
            this.Marker2TrackBar = new System.Windows.Forms.TrackBar();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.DeltaLabel = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.TimeMarker2 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.TimeMarker1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.Marker2D24 = new System.Windows.Forms.Label();
            this.Marker2D23 = new System.Windows.Forms.Label();
            this.Marker2D22 = new System.Windows.Forms.Label();
            this.Marker2D21 = new System.Windows.Forms.Label();
            this.Marker2D12 = new System.Windows.Forms.Label();
            this.Marker2D11 = new System.Windows.Forms.Label();
            this.Marker2D10 = new System.Windows.Forms.Label();
            this.Marker2D9 = new System.Windows.Forms.Label();
            this.Marker2D20 = new System.Windows.Forms.Label();
            this.Marker2D19 = new System.Windows.Forms.Label();
            this.Marker2D18 = new System.Windows.Forms.Label();
            this.Marker2D17 = new System.Windows.Forms.Label();
            this.Marker2D16 = new System.Windows.Forms.Label();
            this.Marker2D15 = new System.Windows.Forms.Label();
            this.Marker2D14 = new System.Windows.Forms.Label();
            this.Marker2D13 = new System.Windows.Forms.Label();
            this.Marker2D8 = new System.Windows.Forms.Label();
            this.Marker2D7 = new System.Windows.Forms.Label();
            this.Marker2D6 = new System.Windows.Forms.Label();
            this.Marker2D5 = new System.Windows.Forms.Label();
            this.Marker2D4 = new System.Windows.Forms.Label();
            this.Marker2D3 = new System.Windows.Forms.Label();
            this.Marker2D2 = new System.Windows.Forms.Label();
            this.Marker2D1 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.Marker2Un = new System.Windows.Forms.Label();
            this.Marker2Uc = new System.Windows.Forms.Label();
            this.Marker2Ub = new System.Windows.Forms.Label();
            this.Marker2Ua = new System.Windows.Forms.Label();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.Marker2S1In = new System.Windows.Forms.Label();
            this.Marker2S1Ic = new System.Windows.Forms.Label();
            this.Marker2S1Ib = new System.Windows.Forms.Label();
            this.Marker2S1Ia = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.Marker1D24 = new System.Windows.Forms.Label();
            this.Marker1D23 = new System.Windows.Forms.Label();
            this.Marker1D22 = new System.Windows.Forms.Label();
            this.Marker1D21 = new System.Windows.Forms.Label();
            this.Marker1D12 = new System.Windows.Forms.Label();
            this.Marker1D11 = new System.Windows.Forms.Label();
            this.Marker1D10 = new System.Windows.Forms.Label();
            this.Marker1D9 = new System.Windows.Forms.Label();
            this.Marker1D20 = new System.Windows.Forms.Label();
            this.Marker1D19 = new System.Windows.Forms.Label();
            this.Marker1D18 = new System.Windows.Forms.Label();
            this.Marker1D17 = new System.Windows.Forms.Label();
            this.Marker1D16 = new System.Windows.Forms.Label();
            this.Marker1D15 = new System.Windows.Forms.Label();
            this.Marker1D14 = new System.Windows.Forms.Label();
            this.Marker1D13 = new System.Windows.Forms.Label();
            this.Marker1D8 = new System.Windows.Forms.Label();
            this.Marker1D7 = new System.Windows.Forms.Label();
            this.Marker1D6 = new System.Windows.Forms.Label();
            this.Marker1D5 = new System.Windows.Forms.Label();
            this.Marker1D4 = new System.Windows.Forms.Label();
            this.Marker1D3 = new System.Windows.Forms.Label();
            this.Marker1D2 = new System.Windows.Forms.Label();
            this.Marker1D1 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.Marker1Un = new System.Windows.Forms.Label();
            this.Marker1Uc = new System.Windows.Forms.Label();
            this.Marker1Ub = new System.Windows.Forms.Label();
            this.Marker1Ua = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.Marker1S1In = new System.Windows.Forms.Label();
            this.Marker1S1Ic = new System.Windows.Forms.Label();
            this.Marker1S1Ib = new System.Windows.Forms.Label();
            this.Marker1S1Ia = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.button43 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this._avaryOscCheck = new System.Windows.Forms.CheckBox();
            this.Xplus = new System.Windows.Forms.Button();
            this.Xminus = new System.Windows.Forms.Button();
            this.S1ShowCB = new System.Windows.Forms.CheckBox();
            this.UShowCB = new System.Windows.Forms.CheckBox();
            this.DiscretShowCB = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.MarkerCB = new System.Windows.Forms.CheckBox();
            this.FlagCB = new System.Windows.Forms.CheckBox();
            this._avaryJournalCheck = new System.Windows.Forms.CheckBox();
            this.MAINTABLE.SuspendLayout();
            this.MAINPANEL.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.MarkersTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Marker1TrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Marker2TrackBar)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // MAINTABLE
            // 
            this.MAINTABLE.ColumnCount = 2;
            this.MAINTABLE.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MAINTABLE.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.MAINTABLE.Controls.Add(this.hScrollBar4, 0, 2);
            this.MAINTABLE.Controls.Add(this.menuStrip1, 0, 0);
            this.MAINTABLE.Controls.Add(this.MAINPANEL, 0, 1);
            this.MAINTABLE.Controls.Add(this.panel3, 1, 1);
            this.MAINTABLE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MAINTABLE.Location = new System.Drawing.Point(0, 0);
            this.MAINTABLE.Margin = new System.Windows.Forms.Padding(0);
            this.MAINTABLE.Name = "MAINTABLE";
            this.MAINTABLE.RowCount = 2;
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MAINTABLE.Size = new System.Drawing.Size(865, 624);
            this.MAINTABLE.TabIndex = 0;
            // 
            // hScrollBar4
            // 
            this.hScrollBar4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.hScrollBar4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hScrollBar4.LargeChange = 1;
            this.hScrollBar4.Location = new System.Drawing.Point(0, 604);
            this.hScrollBar4.Maximum = 10;
            this.hScrollBar4.Minimum = 10;
            this.hScrollBar4.Name = "hScrollBar4";
            this.hScrollBar4.Size = new System.Drawing.Size(865, 20);
            this.hScrollBar4.TabIndex = 20;
            this.hScrollBar4.Value = 10;
            this.hScrollBar4.Visible = false;
            this.hScrollBar4.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar4_Scroll);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Silver;
            this.MAINTABLE.SetColumnSpan(this.menuStrip1, 2);
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(865, 24);
            this.menuStrip1.TabIndex = 18;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // MAINPANEL
            // 
            this.MAINPANEL.Controls.Add(this.panel1);
            this.MAINPANEL.Controls.Add(this.MarkersTable);
            this.MAINPANEL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MAINPANEL.Location = new System.Drawing.Point(3, 28);
            this.MAINPANEL.Name = "MAINPANEL";
            this.MAINPANEL.Size = new System.Drawing.Size(859, 573);
            this.MAINPANEL.TabIndex = 32;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 59);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(859, 514);
            this.panel1.TabIndex = 32;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.splitter5);
            this.panel2.Controls.Add(this.tableLayoutPanel2);
            this.panel2.Controls.Add(this.splitter4);
            this.panel2.Controls.Add(this.tableLayoutPanel16);
            this.panel2.Controls.Add(this.splitter3);
            this.panel2.Controls.Add(this.tableLayoutPanel13);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(771, 1000);
            this.panel2.TabIndex = 0;
            // 
            // splitter5
            // 
            this.splitter5.BackColor = System.Drawing.Color.Black;
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter5.Location = new System.Drawing.Point(0, 1000);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(771, 3);
            this.splitter5.TabIndex = 36;
            this.splitter5.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel7, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this._discretChart, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 412);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(771, 588);
            this.tableLayoutPanel2.TabIndex = 35;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel7.ColumnCount = 17;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel7, 4);
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Controls.Add(this.button16, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.button28, 15, 0);
            this.tableLayoutPanel7.Controls.Add(this.button26, 14, 0);
            this.tableLayoutPanel7.Controls.Add(this.button24, 13, 0);
            this.tableLayoutPanel7.Controls.Add(this.button25, 12, 0);
            this.tableLayoutPanel7.Controls.Add(this.button17, 11, 0);
            this.tableLayoutPanel7.Controls.Add(this.button9, 10, 0);
            this.tableLayoutPanel7.Controls.Add(this.button10, 9, 0);
            this.tableLayoutPanel7.Controls.Add(this.button8, 8, 0);
            this.tableLayoutPanel7.Controls.Add(this.button6, 7, 0);
            this.tableLayoutPanel7.Controls.Add(this.button7, 6, 0);
            this.tableLayoutPanel7.Controls.Add(this.button15, 4, 0);
            this.tableLayoutPanel7.Controls.Add(this.button14, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.button13, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.button12, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.button11, 5, 0);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(765, 29);
            this.tableLayoutPanel7.TabIndex = 16;
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.Blue;
            this.button16.Location = new System.Drawing.Point(102, 3);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(27, 23);
            this.button16.TabIndex = 11;
            this.button16.Text = "4";
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.discretButton_Click);
            // 
            // button28
            // 
            this.button28.BackColor = System.Drawing.Color.DarkTurquoise;
            this.button28.Location = new System.Drawing.Point(498, 3);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(27, 23);
            this.button28.TabIndex = 23;
            this.button28.Text = "16";
            this.button28.UseVisualStyleBackColor = false;
            this.button28.Click += new System.EventHandler(this.discretButton_Click);
            // 
            // button26
            // 
            this.button26.BackColor = System.Drawing.Color.DarkRed;
            this.button26.Location = new System.Drawing.Point(465, 3);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(27, 23);
            this.button26.TabIndex = 21;
            this.button26.Text = "15";
            this.button26.UseVisualStyleBackColor = false;
            this.button26.Click += new System.EventHandler(this.discretButton_Click);
            // 
            // button24
            // 
            this.button24.BackColor = System.Drawing.Color.LightSeaGreen;
            this.button24.Location = new System.Drawing.Point(432, 3);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(27, 23);
            this.button24.TabIndex = 19;
            this.button24.Text = "14";
            this.button24.UseVisualStyleBackColor = false;
            this.button24.Click += new System.EventHandler(this.discretButton_Click);
            // 
            // button25
            // 
            this.button25.BackColor = System.Drawing.Color.PaleVioletRed;
            this.button25.Location = new System.Drawing.Point(399, 3);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(27, 23);
            this.button25.TabIndex = 20;
            this.button25.Text = "13";
            this.button25.UseVisualStyleBackColor = false;
            this.button25.Click += new System.EventHandler(this.discretButton_Click);
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.DarkGreen;
            this.button17.Location = new System.Drawing.Point(366, 3);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(27, 23);
            this.button17.TabIndex = 12;
            this.button17.Text = "12";
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.discretButton_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.DarkOrange;
            this.button9.Location = new System.Drawing.Point(333, 3);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(27, 23);
            this.button9.TabIndex = 4;
            this.button9.Text = "11";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.discretButton_Click);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.DarkViolet;
            this.button10.Location = new System.Drawing.Point(300, 3);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(27, 23);
            this.button10.TabIndex = 26;
            this.button10.Text = "10";
            this.button10.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Indigo;
            this.button8.Location = new System.Drawing.Point(267, 3);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(27, 23);
            this.button8.TabIndex = 3;
            this.button8.Text = "9";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.discretButton_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.LightBlue;
            this.button6.Location = new System.Drawing.Point(234, 3);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(27, 23);
            this.button6.TabIndex = 1;
            this.button6.Text = "8";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.discretButton_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.LightGreen;
            this.button7.Location = new System.Drawing.Point(201, 3);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(27, 23);
            this.button7.TabIndex = 2;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.discretButton_Click);
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.LightPink;
            this.button15.Location = new System.Drawing.Point(135, 3);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(27, 23);
            this.button15.TabIndex = 10;
            this.button15.Text = "5";
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.discretButton_Click);
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.Red;
            this.button14.Location = new System.Drawing.Point(69, 3);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(27, 23);
            this.button14.TabIndex = 27;
            this.button14.Text = "3";
            this.button14.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Yellow;
            this.button13.Location = new System.Drawing.Point(3, 3);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(27, 23);
            this.button13.TabIndex = 8;
            this.button13.Text = "1";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.discretButton_Click);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.Green;
            this.button12.Location = new System.Drawing.Point(36, 3);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(27, 23);
            this.button12.TabIndex = 25;
            this.button12.Text = "2";
            this.button12.UseVisualStyleBackColor = false;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.button11.Location = new System.Drawing.Point(168, 3);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(27, 23);
            this.button11.TabIndex = 24;
            this.button11.Text = "6";
            this.button11.UseVisualStyleBackColor = false;
            // 
            // _discretChart
            // 
            this._discretChart.BkGradient = false;
            this._discretChart.BkGradientAngle = 90;
            this._discretChart.BkGradientColor = System.Drawing.Color.White;
            this._discretChart.BkGradientRate = 0.5F;
            this._discretChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._discretChart.BkRestrictedToChartPanel = false;
            this._discretChart.BkShinePosition = 1F;
            this._discretChart.BkTransparency = 0F;
            this._discretChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._discretChart.BorderExteriorLength = 0;
            this._discretChart.BorderGradientAngle = 225;
            this._discretChart.BorderGradientLightPos1 = 1F;
            this._discretChart.BorderGradientLightPos2 = -1F;
            this._discretChart.BorderGradientRate = 0.5F;
            this._discretChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._discretChart.BorderLightIntermediateBrightness = 0F;
            this._discretChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._discretChart.ChartPanelBackColor = System.Drawing.Color.Silver;
            this._discretChart.ChartPanelBkTransparency = 0F;
            this._discretChart.ControlShadow = false;
            this._discretChart.CoordinateAxesVisible = true;
            this._discretChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._discretChart.CoordinateXOrigin = 50D;
            this._discretChart.CoordinateYMax = 100D;
            this._discretChart.CoordinateYMin = -100D;
            this._discretChart.CoordinateYOrigin = 0D;
            this._discretChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._discretChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._discretChart.FooterColor = System.Drawing.Color.Black;
            this._discretChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._discretChart.FooterVisible = false;
            this._discretChart.GridColor = System.Drawing.Color.Gray;
            this._discretChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._discretChart.GridVisible = true;
            this._discretChart.GridXSubTicker = 0;
            this._discretChart.GridXTicker = 10;
            this._discretChart.GridYSubTicker = 0;
            this._discretChart.GridYTicker = 6;
            this._discretChart.HeaderColor = System.Drawing.Color.Black;
            this._discretChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._discretChart.HeaderVisible = false;
            this._discretChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._discretChart.InnerBorderLength = 0;
            this._discretChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._discretChart.LegendBkColor = System.Drawing.Color.White;
            this._discretChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._discretChart.LegendVisible = false;
            this._discretChart.Location = new System.Drawing.Point(15, 38);
            this._discretChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._discretChart.MiddleBorderLength = 0;
            this._discretChart.Name = "_discretChart";
            this._discretChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._discretChart.OuterBorderLength = 0;
            this._discretChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._discretChart.Precision = 0;
            this._discretChart.RoundRadius = 10;
            this._discretChart.ShadowColor = System.Drawing.Color.DimGray;
            this._discretChart.ShadowDepth = 8;
            this._discretChart.ShadowRate = 0.5F;
            this._discretChart.Size = new System.Drawing.Size(693, 547);
            this._discretChart.TabIndex = 11;
            this._discretChart.Text = "daS_Net_XYChart5";
            this._discretChart.XMax = 100D;
            this._discretChart.XMin = 0D;
            this._discretChart.XScaleColor = System.Drawing.Color.Black;
            this._discretChart.XScaleVisible = true;
            this._discretChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this._Chart_MouseClick);
            // 
            // splitter4
            // 
            this.splitter4.BackColor = System.Drawing.Color.Black;
            this.splitter4.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter4.Location = new System.Drawing.Point(0, 409);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(771, 3);
            this.splitter4.TabIndex = 34;
            this.splitter4.TabStop = false;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 4;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel16.Controls.Add(this.tableLayoutPanel12, 3, 1);
            this.tableLayoutPanel16.Controls.Add(this._voltageChart, 1, 1);
            this.tableLayoutPanel16.Controls.Add(this.UScroll, 2, 1);
            this.tableLayoutPanel16.Controls.Add(this.tableLayoutPanel6, 0, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(0, 206);
            this.tableLayoutPanel16.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 2;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(771, 203);
            this.tableLayoutPanel16.TabIndex = 33;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 1;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.UYminus, 0, 2);
            this.tableLayoutPanel12.Controls.Add(this.UYPlus, 0, 1);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(734, 37);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 5;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(34, 163);
            this.tableLayoutPanel12.TabIndex = 34;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.Location = new System.Drawing.Point(9, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 15);
            this.label4.TabIndex = 5;
            this.label4.Text = "Y";
            // 
            // UYminus
            // 
            this.UYminus.BackColor = System.Drawing.Color.ForestGreen;
            this.UYminus.Cursor = System.Windows.Forms.Cursors.Default;
            this.UYminus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UYminus.Enabled = false;
            this.UYminus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.UYminus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UYminus.Location = new System.Drawing.Point(3, 63);
            this.UYminus.Name = "UYminus";
            this.UYminus.Size = new System.Drawing.Size(28, 24);
            this.UYminus.TabIndex = 1;
            this.UYminus.Text = "-";
            this.UYminus.UseVisualStyleBackColor = false;
            this.UYminus.Click += new System.EventHandler(this.UYminus_Click);
            // 
            // UYPlus
            // 
            this.UYPlus.BackColor = System.Drawing.Color.ForestGreen;
            this.UYPlus.Cursor = System.Windows.Forms.Cursors.Default;
            this.UYPlus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UYPlus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.UYPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UYPlus.Location = new System.Drawing.Point(3, 33);
            this.UYPlus.Name = "UYPlus";
            this.UYPlus.Size = new System.Drawing.Size(28, 24);
            this.UYPlus.TabIndex = 0;
            this.UYPlus.Text = "+";
            this.UYPlus.UseVisualStyleBackColor = false;
            this.UYPlus.Click += new System.EventHandler(this.UYPlus_Click);
            // 
            // _voltageChart
            // 
            this._voltageChart.BkGradient = false;
            this._voltageChart.BkGradientAngle = 90;
            this._voltageChart.BkGradientColor = System.Drawing.Color.White;
            this._voltageChart.BkGradientRate = 0.5F;
            this._voltageChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._voltageChart.BkRestrictedToChartPanel = false;
            this._voltageChart.BkShinePosition = 1F;
            this._voltageChart.BkTransparency = 0F;
            this._voltageChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._voltageChart.BorderExteriorLength = 0;
            this._voltageChart.BorderGradientAngle = 225;
            this._voltageChart.BorderGradientLightPos1 = 1F;
            this._voltageChart.BorderGradientLightPos2 = -1F;
            this._voltageChart.BorderGradientRate = 0.5F;
            this._voltageChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._voltageChart.BorderLightIntermediateBrightness = 0F;
            this._voltageChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._voltageChart.ChartPanelBackColor = System.Drawing.Color.Silver;
            this._voltageChart.ChartPanelBkTransparency = 0F;
            this._voltageChart.ControlShadow = false;
            this._voltageChart.CoordinateAxesVisible = true;
            this._voltageChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._voltageChart.CoordinateXOrigin = 50D;
            this._voltageChart.CoordinateYMax = 100D;
            this._voltageChart.CoordinateYMin = -100D;
            this._voltageChart.CoordinateYOrigin = 0D;
            this._voltageChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._voltageChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._voltageChart.FooterColor = System.Drawing.Color.Black;
            this._voltageChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._voltageChart.FooterVisible = false;
            this._voltageChart.GridColor = System.Drawing.Color.Gray;
            this._voltageChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._voltageChart.GridVisible = true;
            this._voltageChart.GridXSubTicker = 0;
            this._voltageChart.GridXTicker = 10;
            this._voltageChart.GridYSubTicker = 0;
            this._voltageChart.GridYTicker = 10;
            this._voltageChart.HeaderColor = System.Drawing.Color.Black;
            this._voltageChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._voltageChart.HeaderVisible = false;
            this._voltageChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._voltageChart.InnerBorderLength = 0;
            this._voltageChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._voltageChart.LegendBkColor = System.Drawing.Color.White;
            this._voltageChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._voltageChart.LegendVisible = false;
            this._voltageChart.Location = new System.Drawing.Point(15, 37);
            this._voltageChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._voltageChart.MiddleBorderLength = 0;
            this._voltageChart.Name = "_voltageChart";
            this._voltageChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._voltageChart.OuterBorderLength = 0;
            this._voltageChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._voltageChart.Precision = 0;
            this._voltageChart.RoundRadius = 10;
            this._voltageChart.ShadowColor = System.Drawing.Color.DimGray;
            this._voltageChart.ShadowDepth = 8;
            this._voltageChart.ShadowRate = 0.5F;
            this._voltageChart.Size = new System.Drawing.Size(693, 163);
            this._voltageChart.TabIndex = 31;
            this._voltageChart.Text = "daS_Net_XYChart3";
            this._voltageChart.XMax = 100D;
            this._voltageChart.XMin = 0D;
            this._voltageChart.XScaleColor = System.Drawing.Color.Black;
            this._voltageChart.XScaleVisible = true;
            this._voltageChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this._Chart_MouseClick);
            // 
            // UScroll
            // 
            this.UScroll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.UScroll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.UScroll.Enabled = false;
            this.UScroll.LargeChange = 1;
            this.UScroll.Location = new System.Drawing.Point(713, 34);
            this.UScroll.Maximum = 10;
            this.UScroll.Minimum = 10;
            this.UScroll.Name = "UScroll";
            this.UScroll.Size = new System.Drawing.Size(15, 169);
            this.UScroll.TabIndex = 30;
            this.UScroll.Value = 10;
            this.UScroll.Scroll += new System.Windows.Forms.ScrollEventHandler(this.UScroll_Scroll);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel16.SetColumnSpan(this.tableLayoutPanel6, 4);
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Controls.Add(this.button4, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.button29, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.button30, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.button32, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(143, 28);
            this.tableLayoutPanel6.TabIndex = 26;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Red;
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.Location = new System.Drawing.Point(73, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(29, 22);
            this.button4.TabIndex = 13;
            this.button4.Text = "Uc";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.uButton_Click);
            // 
            // button29
            // 
            this.button29.BackColor = System.Drawing.Color.Blue;
            this.button29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button29.Location = new System.Drawing.Point(108, 3);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(32, 22);
            this.button29.TabIndex = 11;
            this.button29.Text = "Un";
            this.button29.UseVisualStyleBackColor = false;
            this.button29.Click += new System.EventHandler(this.uButton_Click);
            // 
            // button30
            // 
            this.button30.BackColor = System.Drawing.Color.Yellow;
            this.button30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button30.Location = new System.Drawing.Point(3, 3);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(29, 22);
            this.button30.TabIndex = 10;
            this.button30.Text = "Ua";
            this.button30.UseVisualStyleBackColor = false;
            this.button30.Click += new System.EventHandler(this.uButton_Click);
            // 
            // button32
            // 
            this.button32.BackColor = System.Drawing.Color.Green;
            this.button32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button32.Location = new System.Drawing.Point(38, 3);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(29, 22);
            this.button32.TabIndex = 9;
            this.button32.Text = "Ub";
            this.button32.UseVisualStyleBackColor = false;
            this.button32.Click += new System.EventHandler(this.uButton_Click);
            // 
            // splitter3
            // 
            this.splitter3.BackColor = System.Drawing.Color.Black;
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(0, 203);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(771, 3);
            this.splitter3.TabIndex = 32;
            this.splitter3.TabStop = false;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 4;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel13.Controls.Add(this.S1Scroll, 2, 1);
            this.tableLayoutPanel13.Controls.Add(this.tableLayoutPanel8, 3, 1);
            this.tableLayoutPanel13.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this._s1TokChart, 1, 1);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel13.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 2;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(771, 203);
            this.tableLayoutPanel13.TabIndex = 27;
            // 
            // S1Scroll
            // 
            this.S1Scroll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.S1Scroll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.S1Scroll.Enabled = false;
            this.S1Scroll.LargeChange = 1;
            this.S1Scroll.Location = new System.Drawing.Point(713, 34);
            this.S1Scroll.Minimum = 100;
            this.S1Scroll.Name = "S1Scroll";
            this.S1Scroll.Size = new System.Drawing.Size(15, 169);
            this.S1Scroll.TabIndex = 32;
            this.S1Scroll.Value = 100;
            this.S1Scroll.Scroll += new System.Windows.Forms.ScrollEventHandler(this.S1Scroll_Scroll);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.S1Yminus, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.S1YPlus, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(734, 37);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 4;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(34, 163);
            this.tableLayoutPanel8.TabIndex = 31;
            // 
            // S1Yminus
            // 
            this.S1Yminus.BackColor = System.Drawing.Color.ForestGreen;
            this.S1Yminus.Cursor = System.Windows.Forms.Cursors.Default;
            this.S1Yminus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.S1Yminus.Enabled = false;
            this.S1Yminus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.S1Yminus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.S1Yminus.Location = new System.Drawing.Point(3, 59);
            this.S1Yminus.Name = "S1Yminus";
            this.S1Yminus.Size = new System.Drawing.Size(28, 24);
            this.S1Yminus.TabIndex = 1;
            this.S1Yminus.Text = "-";
            this.S1Yminus.UseVisualStyleBackColor = false;
            this.S1Yminus.Click += new System.EventHandler(this.S1Yminus_Click);
            // 
            // S1YPlus
            // 
            this.S1YPlus.BackColor = System.Drawing.Color.ForestGreen;
            this.S1YPlus.Cursor = System.Windows.Forms.Cursors.Default;
            this.S1YPlus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.S1YPlus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.S1YPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.S1YPlus.Location = new System.Drawing.Point(3, 29);
            this.S1YPlus.Name = "S1YPlus";
            this.S1YPlus.Size = new System.Drawing.Size(28, 24);
            this.S1YPlus.TabIndex = 0;
            this.S1YPlus.Text = "+";
            this.S1YPlus.UseVisualStyleBackColor = false;
            this.S1YPlus.Click += new System.EventHandler(this.S1YPlus_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.Location = new System.Drawing.Point(9, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Y";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel13.SetColumnSpan(this.tableLayoutPanel3, 4);
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.button1, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.button37, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.button38, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.button39, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(166, 28);
            this.tableLayoutPanel3.TabIndex = 23;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(100, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(28, 22);
            this.button1.TabIndex = 13;
            this.button1.Text = "Ic";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.s1Button_Click);
            // 
            // button37
            // 
            this.button37.BackColor = System.Drawing.Color.Blue;
            this.button37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button37.Location = new System.Drawing.Point(134, 3);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(29, 22);
            this.button37.TabIndex = 11;
            this.button37.Text = "In";
            this.button37.UseVisualStyleBackColor = false;
            this.button37.Click += new System.EventHandler(this.s1Button_Click);
            // 
            // button38
            // 
            this.button38.BackColor = System.Drawing.Color.Yellow;
            this.button38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button38.Location = new System.Drawing.Point(32, 3);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(28, 22);
            this.button38.TabIndex = 10;
            this.button38.Text = "Ia";
            this.button38.UseVisualStyleBackColor = false;
            this.button38.Click += new System.EventHandler(this.s1Button_Click);
            // 
            // button39
            // 
            this.button39.BackColor = System.Drawing.Color.Green;
            this.button39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button39.Location = new System.Drawing.Point(66, 3);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(28, 22);
            this.button39.TabIndex = 9;
            this.button39.Text = "Ib";
            this.button39.UseVisualStyleBackColor = false;
            this.button39.Click += new System.EventHandler(this.s1Button_Click);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.Location = new System.Drawing.Point(3, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 14);
            this.label5.TabIndex = 14;
            this.label5.Text = "C 1";
            // 
            // _s1TokChart
            // 
            this._s1TokChart.BkGradient = false;
            this._s1TokChart.BkGradientAngle = 90;
            this._s1TokChart.BkGradientColor = System.Drawing.Color.White;
            this._s1TokChart.BkGradientRate = 0.5F;
            this._s1TokChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._s1TokChart.BkRestrictedToChartPanel = false;
            this._s1TokChart.BkShinePosition = 1F;
            this._s1TokChart.BkTransparency = 0F;
            this._s1TokChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._s1TokChart.BorderExteriorLength = 0;
            this._s1TokChart.BorderGradientAngle = 225;
            this._s1TokChart.BorderGradientLightPos1 = 1F;
            this._s1TokChart.BorderGradientLightPos2 = -1F;
            this._s1TokChart.BorderGradientRate = 0.5F;
            this._s1TokChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._s1TokChart.BorderLightIntermediateBrightness = 0F;
            this._s1TokChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._s1TokChart.ChartPanelBackColor = System.Drawing.Color.Silver;
            this._s1TokChart.ChartPanelBkTransparency = 0F;
            this._s1TokChart.ControlShadow = false;
            this._s1TokChart.CoordinateAxesVisible = true;
            this._s1TokChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._s1TokChart.CoordinateXOrigin = 50D;
            this._s1TokChart.CoordinateYMax = 100D;
            this._s1TokChart.CoordinateYMin = -100D;
            this._s1TokChart.CoordinateYOrigin = 0D;
            this._s1TokChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._s1TokChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._s1TokChart.FooterColor = System.Drawing.Color.Black;
            this._s1TokChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._s1TokChart.FooterVisible = false;
            this._s1TokChart.GridColor = System.Drawing.Color.Gray;
            this._s1TokChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._s1TokChart.GridVisible = true;
            this._s1TokChart.GridXSubTicker = 0;
            this._s1TokChart.GridXTicker = 10;
            this._s1TokChart.GridYSubTicker = 0;
            this._s1TokChart.GridYTicker = 10;
            this._s1TokChart.HeaderColor = System.Drawing.Color.Black;
            this._s1TokChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._s1TokChart.HeaderVisible = false;
            this._s1TokChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._s1TokChart.InnerBorderLength = 0;
            this._s1TokChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._s1TokChart.LegendBkColor = System.Drawing.Color.White;
            this._s1TokChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._s1TokChart.LegendVisible = false;
            this._s1TokChart.Location = new System.Drawing.Point(15, 37);
            this._s1TokChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._s1TokChart.MiddleBorderLength = 0;
            this._s1TokChart.Name = "_s1TokChart";
            this._s1TokChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._s1TokChart.OuterBorderLength = 0;
            this._s1TokChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._s1TokChart.Precision = 0;
            this._s1TokChart.RoundRadius = 10;
            this._s1TokChart.ShadowColor = System.Drawing.Color.DimGray;
            this._s1TokChart.ShadowDepth = 8;
            this._s1TokChart.ShadowRate = 0.5F;
            this._s1TokChart.Size = new System.Drawing.Size(693, 163);
            this._s1TokChart.TabIndex = 33;
            this._s1TokChart.Text = "daS_Net_XYChart1";
            this._s1TokChart.XMax = 100D;
            this._s1TokChart.XMin = 0D;
            this._s1TokChart.XScaleColor = System.Drawing.Color.Black;
            this._s1TokChart.XScaleVisible = true;
            this._s1TokChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this._Chart_MouseClick);
            // 
            // MarkersTable
            // 
            this.MarkersTable.BackColor = System.Drawing.Color.LightGray;
            this.MarkersTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.MarkersTable.ColumnCount = 2;
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.MarkersTable.Controls.Add(this.Marker1CB, 5, 0);
            this.MarkersTable.Controls.Add(this.Marker2CB, 5, 1);
            this.MarkersTable.Controls.Add(this.Marker1TrackBar, 0, 0);
            this.MarkersTable.Controls.Add(this.Marker2TrackBar, 0, 1);
            this.MarkersTable.Dock = System.Windows.Forms.DockStyle.Top;
            this.MarkersTable.Location = new System.Drawing.Point(0, 0);
            this.MarkersTable.Margin = new System.Windows.Forms.Padding(0);
            this.MarkersTable.Name = "MarkersTable";
            this.MarkersTable.RowCount = 2;
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.78571F));
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.21429F));
            this.MarkersTable.Size = new System.Drawing.Size(859, 59);
            this.MarkersTable.TabIndex = 31;
            this.MarkersTable.Visible = false;
            // 
            // Marker1CB
            // 
            this.Marker1CB.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Marker1CB.AutoSize = true;
            this.Marker1CB.BackColor = System.Drawing.Color.LightGray;
            this.Marker1CB.Location = new System.Drawing.Point(803, 6);
            this.Marker1CB.Name = "Marker1CB";
            this.Marker1CB.Size = new System.Drawing.Size(44, 17);
            this.Marker1CB.TabIndex = 0;
            this.Marker1CB.Text = "� 1";
            this.Marker1CB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Marker1CB.UseVisualStyleBackColor = false;
            this.Marker1CB.CheckedChanged += new System.EventHandler(this.Marker1CB_CheckedChanged);
            // 
            // Marker2CB
            // 
            this.Marker2CB.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Marker2CB.AutoSize = true;
            this.Marker2CB.BackColor = System.Drawing.Color.LightGray;
            this.Marker2CB.Location = new System.Drawing.Point(803, 35);
            this.Marker2CB.Name = "Marker2CB";
            this.Marker2CB.Size = new System.Drawing.Size(44, 17);
            this.Marker2CB.TabIndex = 1;
            this.Marker2CB.Text = "� 2";
            this.Marker2CB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Marker2CB.UseVisualStyleBackColor = false;
            this.Marker2CB.CheckedChanged += new System.EventHandler(this.Marker2CB_CheckedChanged);
            // 
            // Marker1TrackBar
            // 
            this.Marker1TrackBar.BackColor = System.Drawing.Color.Silver;
            this.Marker1TrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Marker1TrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Marker1TrackBar.Location = new System.Drawing.Point(4, 4);
            this.Marker1TrackBar.Name = "Marker1TrackBar";
            this.Marker1TrackBar.Size = new System.Drawing.Size(785, 22);
            this.Marker1TrackBar.TabIndex = 2;
            this.Marker1TrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.Marker1TrackBar.Scroll += new System.EventHandler(this.Marker1TrackBar_Scroll);
            // 
            // Marker2TrackBar
            // 
            this.Marker2TrackBar.BackColor = System.Drawing.Color.Silver;
            this.Marker2TrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Marker2TrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Marker2TrackBar.LargeChange = 0;
            this.Marker2TrackBar.Location = new System.Drawing.Point(4, 33);
            this.Marker2TrackBar.Maximum = 3400;
            this.Marker2TrackBar.Name = "Marker2TrackBar";
            this.Marker2TrackBar.Size = new System.Drawing.Size(785, 22);
            this.Marker2TrackBar.TabIndex = 3;
            this.Marker2TrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.Marker2TrackBar.Scroll += new System.EventHandler(this.Marker2TrackBar_Scroll);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(868, 28);
            this.panel3.Name = "panel3";
            this.MAINTABLE.SetRowSpan(this.panel3, 2);
            this.panel3.Size = new System.Drawing.Size(1, 593);
            this.panel3.TabIndex = 33;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.LightGray;
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(3, -1);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox1.Size = new System.Drawing.Size(0, 591);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "���������� ��������";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBox7);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox4.Location = new System.Drawing.Point(0, 506);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(0, 85);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "�����";
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.DeltaLabel);
            this.groupBox7.Location = new System.Drawing.Point(6, 46);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(0, 33);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "������";
            // 
            // DeltaLabel
            // 
            this.DeltaLabel.AutoSize = true;
            this.DeltaLabel.Location = new System.Drawing.Point(94, 13);
            this.DeltaLabel.Name = "DeltaLabel";
            this.DeltaLabel.Size = new System.Drawing.Size(0, 13);
            this.DeltaLabel.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox6.Controls.Add(this.TimeMarker2);
            this.groupBox6.Location = new System.Drawing.Point(121, 13);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(110, 33);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "������ 2";
            // 
            // TimeMarker2
            // 
            this.TimeMarker2.AutoSize = true;
            this.TimeMarker2.Location = new System.Drawing.Point(34, 14);
            this.TimeMarker2.Name = "TimeMarker2";
            this.TimeMarker2.Size = new System.Drawing.Size(0, 13);
            this.TimeMarker2.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox5.Controls.Add(this.TimeMarker1);
            this.groupBox5.Location = new System.Drawing.Point(6, 13);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(110, 33);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "������ 1";
            // 
            // TimeMarker1
            // 
            this.TimeMarker1.AutoSize = true;
            this.TimeMarker1.Location = new System.Drawing.Point(30, 14);
            this.TimeMarker1.Name = "TimeMarker1";
            this.TimeMarker1.Size = new System.Drawing.Size(0, 13);
            this.TimeMarker1.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.groupBox12);
            this.groupBox3.Controls.Add(this.groupBox13);
            this.groupBox3.Controls.Add(this.groupBox17);
            this.groupBox3.Location = new System.Drawing.Point(121, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(110, 489);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "������2";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.Marker2D24);
            this.groupBox12.Controls.Add(this.Marker2D23);
            this.groupBox12.Controls.Add(this.Marker2D22);
            this.groupBox12.Controls.Add(this.Marker2D21);
            this.groupBox12.Controls.Add(this.Marker2D12);
            this.groupBox12.Controls.Add(this.Marker2D11);
            this.groupBox12.Controls.Add(this.Marker2D10);
            this.groupBox12.Controls.Add(this.Marker2D9);
            this.groupBox12.Controls.Add(this.Marker2D20);
            this.groupBox12.Controls.Add(this.Marker2D19);
            this.groupBox12.Controls.Add(this.Marker2D18);
            this.groupBox12.Controls.Add(this.Marker2D17);
            this.groupBox12.Controls.Add(this.Marker2D16);
            this.groupBox12.Controls.Add(this.Marker2D15);
            this.groupBox12.Controls.Add(this.Marker2D14);
            this.groupBox12.Controls.Add(this.Marker2D13);
            this.groupBox12.Controls.Add(this.Marker2D8);
            this.groupBox12.Controls.Add(this.Marker2D7);
            this.groupBox12.Controls.Add(this.Marker2D6);
            this.groupBox12.Controls.Add(this.Marker2D5);
            this.groupBox12.Controls.Add(this.Marker2D4);
            this.groupBox12.Controls.Add(this.Marker2D3);
            this.groupBox12.Controls.Add(this.Marker2D2);
            this.groupBox12.Controls.Add(this.Marker2D1);
            this.groupBox12.Location = new System.Drawing.Point(6, 162);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(98, 181);
            this.groupBox12.TabIndex = 49;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "��������";
            // 
            // Marker2D24
            // 
            this.Marker2D24.AutoSize = true;
            this.Marker2D24.Location = new System.Drawing.Point(49, 162);
            this.Marker2D24.Name = "Marker2D24";
            this.Marker2D24.Size = new System.Drawing.Size(10, 13);
            this.Marker2D24.TabIndex = 87;
            this.Marker2D24.Text = " ";
            // 
            // Marker2D23
            // 
            this.Marker2D23.AutoSize = true;
            this.Marker2D23.Location = new System.Drawing.Point(49, 149);
            this.Marker2D23.Name = "Marker2D23";
            this.Marker2D23.Size = new System.Drawing.Size(10, 13);
            this.Marker2D23.TabIndex = 86;
            this.Marker2D23.Text = " ";
            // 
            // Marker2D22
            // 
            this.Marker2D22.AutoSize = true;
            this.Marker2D22.Location = new System.Drawing.Point(49, 136);
            this.Marker2D22.Name = "Marker2D22";
            this.Marker2D22.Size = new System.Drawing.Size(10, 13);
            this.Marker2D22.TabIndex = 85;
            this.Marker2D22.Text = " ";
            // 
            // Marker2D21
            // 
            this.Marker2D21.AutoSize = true;
            this.Marker2D21.Location = new System.Drawing.Point(49, 121);
            this.Marker2D21.Name = "Marker2D21";
            this.Marker2D21.Size = new System.Drawing.Size(10, 13);
            this.Marker2D21.TabIndex = 84;
            this.Marker2D21.Text = " ";
            // 
            // Marker2D12
            // 
            this.Marker2D12.AutoSize = true;
            this.Marker2D12.Location = new System.Drawing.Point(2, 162);
            this.Marker2D12.Name = "Marker2D12";
            this.Marker2D12.Size = new System.Drawing.Size(10, 13);
            this.Marker2D12.TabIndex = 83;
            this.Marker2D12.Text = " ";
            // 
            // Marker2D11
            // 
            this.Marker2D11.AutoSize = true;
            this.Marker2D11.Location = new System.Drawing.Point(2, 149);
            this.Marker2D11.Name = "Marker2D11";
            this.Marker2D11.Size = new System.Drawing.Size(10, 13);
            this.Marker2D11.TabIndex = 82;
            this.Marker2D11.Text = " ";
            // 
            // Marker2D10
            // 
            this.Marker2D10.AutoSize = true;
            this.Marker2D10.Location = new System.Drawing.Point(2, 136);
            this.Marker2D10.Name = "Marker2D10";
            this.Marker2D10.Size = new System.Drawing.Size(10, 13);
            this.Marker2D10.TabIndex = 81;
            this.Marker2D10.Text = " ";
            // 
            // Marker2D9
            // 
            this.Marker2D9.AutoSize = true;
            this.Marker2D9.Location = new System.Drawing.Point(2, 121);
            this.Marker2D9.Name = "Marker2D9";
            this.Marker2D9.Size = new System.Drawing.Size(10, 13);
            this.Marker2D9.TabIndex = 80;
            this.Marker2D9.Text = " ";
            // 
            // Marker2D20
            // 
            this.Marker2D20.AutoSize = true;
            this.Marker2D20.Location = new System.Drawing.Point(49, 108);
            this.Marker2D20.Name = "Marker2D20";
            this.Marker2D20.Size = new System.Drawing.Size(10, 13);
            this.Marker2D20.TabIndex = 79;
            this.Marker2D20.Text = " ";
            // 
            // Marker2D19
            // 
            this.Marker2D19.AutoSize = true;
            this.Marker2D19.Location = new System.Drawing.Point(49, 95);
            this.Marker2D19.Name = "Marker2D19";
            this.Marker2D19.Size = new System.Drawing.Size(10, 13);
            this.Marker2D19.TabIndex = 78;
            this.Marker2D19.Text = " ";
            // 
            // Marker2D18
            // 
            this.Marker2D18.AutoSize = true;
            this.Marker2D18.Location = new System.Drawing.Point(49, 82);
            this.Marker2D18.Name = "Marker2D18";
            this.Marker2D18.Size = new System.Drawing.Size(10, 13);
            this.Marker2D18.TabIndex = 77;
            this.Marker2D18.Text = " ";
            // 
            // Marker2D17
            // 
            this.Marker2D17.AutoSize = true;
            this.Marker2D17.Location = new System.Drawing.Point(49, 67);
            this.Marker2D17.Name = "Marker2D17";
            this.Marker2D17.Size = new System.Drawing.Size(10, 13);
            this.Marker2D17.TabIndex = 76;
            this.Marker2D17.Text = " ";
            // 
            // Marker2D16
            // 
            this.Marker2D16.AutoSize = true;
            this.Marker2D16.Location = new System.Drawing.Point(49, 54);
            this.Marker2D16.Name = "Marker2D16";
            this.Marker2D16.Size = new System.Drawing.Size(10, 13);
            this.Marker2D16.TabIndex = 75;
            this.Marker2D16.Text = " ";
            // 
            // Marker2D15
            // 
            this.Marker2D15.AutoSize = true;
            this.Marker2D15.Location = new System.Drawing.Point(49, 41);
            this.Marker2D15.Name = "Marker2D15";
            this.Marker2D15.Size = new System.Drawing.Size(10, 13);
            this.Marker2D15.TabIndex = 74;
            this.Marker2D15.Text = " ";
            // 
            // Marker2D14
            // 
            this.Marker2D14.AutoSize = true;
            this.Marker2D14.Location = new System.Drawing.Point(49, 28);
            this.Marker2D14.Name = "Marker2D14";
            this.Marker2D14.Size = new System.Drawing.Size(10, 13);
            this.Marker2D14.TabIndex = 73;
            this.Marker2D14.Text = " ";
            // 
            // Marker2D13
            // 
            this.Marker2D13.AutoSize = true;
            this.Marker2D13.Location = new System.Drawing.Point(49, 13);
            this.Marker2D13.Name = "Marker2D13";
            this.Marker2D13.Size = new System.Drawing.Size(10, 13);
            this.Marker2D13.TabIndex = 72;
            this.Marker2D13.Text = " ";
            // 
            // Marker2D8
            // 
            this.Marker2D8.AutoSize = true;
            this.Marker2D8.Location = new System.Drawing.Point(2, 108);
            this.Marker2D8.Name = "Marker2D8";
            this.Marker2D8.Size = new System.Drawing.Size(10, 13);
            this.Marker2D8.TabIndex = 71;
            this.Marker2D8.Text = " ";
            // 
            // Marker2D7
            // 
            this.Marker2D7.AutoSize = true;
            this.Marker2D7.Location = new System.Drawing.Point(2, 95);
            this.Marker2D7.Name = "Marker2D7";
            this.Marker2D7.Size = new System.Drawing.Size(10, 13);
            this.Marker2D7.TabIndex = 70;
            this.Marker2D7.Text = " ";
            // 
            // Marker2D6
            // 
            this.Marker2D6.AutoSize = true;
            this.Marker2D6.Location = new System.Drawing.Point(2, 82);
            this.Marker2D6.Name = "Marker2D6";
            this.Marker2D6.Size = new System.Drawing.Size(10, 13);
            this.Marker2D6.TabIndex = 69;
            this.Marker2D6.Text = " ";
            // 
            // Marker2D5
            // 
            this.Marker2D5.AutoSize = true;
            this.Marker2D5.Location = new System.Drawing.Point(2, 67);
            this.Marker2D5.Name = "Marker2D5";
            this.Marker2D5.Size = new System.Drawing.Size(10, 13);
            this.Marker2D5.TabIndex = 68;
            this.Marker2D5.Text = " ";
            // 
            // Marker2D4
            // 
            this.Marker2D4.AutoSize = true;
            this.Marker2D4.Location = new System.Drawing.Point(2, 54);
            this.Marker2D4.Name = "Marker2D4";
            this.Marker2D4.Size = new System.Drawing.Size(10, 13);
            this.Marker2D4.TabIndex = 67;
            this.Marker2D4.Text = " ";
            // 
            // Marker2D3
            // 
            this.Marker2D3.AutoSize = true;
            this.Marker2D3.Location = new System.Drawing.Point(2, 41);
            this.Marker2D3.Name = "Marker2D3";
            this.Marker2D3.Size = new System.Drawing.Size(10, 13);
            this.Marker2D3.TabIndex = 66;
            this.Marker2D3.Text = " ";
            // 
            // Marker2D2
            // 
            this.Marker2D2.AutoSize = true;
            this.Marker2D2.Location = new System.Drawing.Point(2, 28);
            this.Marker2D2.Name = "Marker2D2";
            this.Marker2D2.Size = new System.Drawing.Size(10, 13);
            this.Marker2D2.TabIndex = 65;
            this.Marker2D2.Text = " ";
            // 
            // Marker2D1
            // 
            this.Marker2D1.AutoSize = true;
            this.Marker2D1.Location = new System.Drawing.Point(2, 13);
            this.Marker2D1.Name = "Marker2D1";
            this.Marker2D1.Size = new System.Drawing.Size(10, 13);
            this.Marker2D1.TabIndex = 64;
            this.Marker2D1.Text = " ";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.Marker2Un);
            this.groupBox13.Controls.Add(this.Marker2Uc);
            this.groupBox13.Controls.Add(this.Marker2Ub);
            this.groupBox13.Controls.Add(this.Marker2Ua);
            this.groupBox13.Location = new System.Drawing.Point(6, 90);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(98, 72);
            this.groupBox13.TabIndex = 48;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "����������";
            // 
            // Marker2Un
            // 
            this.Marker2Un.AutoSize = true;
            this.Marker2Un.Location = new System.Drawing.Point(6, 53);
            this.Marker2Un.Name = "Marker2Un";
            this.Marker2Un.Size = new System.Drawing.Size(10, 13);
            this.Marker2Un.TabIndex = 19;
            this.Marker2Un.Text = " ";
            // 
            // Marker2Uc
            // 
            this.Marker2Uc.AutoSize = true;
            this.Marker2Uc.Location = new System.Drawing.Point(6, 40);
            this.Marker2Uc.Name = "Marker2Uc";
            this.Marker2Uc.Size = new System.Drawing.Size(10, 13);
            this.Marker2Uc.TabIndex = 18;
            this.Marker2Uc.Text = " ";
            // 
            // Marker2Ub
            // 
            this.Marker2Ub.AutoSize = true;
            this.Marker2Ub.Location = new System.Drawing.Point(6, 27);
            this.Marker2Ub.Name = "Marker2Ub";
            this.Marker2Ub.Size = new System.Drawing.Size(10, 13);
            this.Marker2Ub.TabIndex = 17;
            this.Marker2Ub.Text = " ";
            // 
            // Marker2Ua
            // 
            this.Marker2Ua.AutoSize = true;
            this.Marker2Ua.Location = new System.Drawing.Point(6, 12);
            this.Marker2Ua.Name = "Marker2Ua";
            this.Marker2Ua.Size = new System.Drawing.Size(10, 13);
            this.Marker2Ua.TabIndex = 16;
            this.Marker2Ua.Text = " ";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.Marker2S1In);
            this.groupBox17.Controls.Add(this.Marker2S1Ic);
            this.groupBox17.Controls.Add(this.Marker2S1Ib);
            this.groupBox17.Controls.Add(this.Marker2S1Ia);
            this.groupBox17.Location = new System.Drawing.Point(6, 12);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(98, 72);
            this.groupBox17.TabIndex = 45;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "������� 1";
            // 
            // Marker2S1In
            // 
            this.Marker2S1In.AutoSize = true;
            this.Marker2S1In.Location = new System.Drawing.Point(6, 53);
            this.Marker2S1In.Name = "Marker2S1In";
            this.Marker2S1In.Size = new System.Drawing.Size(10, 13);
            this.Marker2S1In.TabIndex = 7;
            this.Marker2S1In.Text = " ";
            // 
            // Marker2S1Ic
            // 
            this.Marker2S1Ic.AutoSize = true;
            this.Marker2S1Ic.Location = new System.Drawing.Point(6, 40);
            this.Marker2S1Ic.Name = "Marker2S1Ic";
            this.Marker2S1Ic.Size = new System.Drawing.Size(10, 13);
            this.Marker2S1Ic.TabIndex = 6;
            this.Marker2S1Ic.Text = " ";
            // 
            // Marker2S1Ib
            // 
            this.Marker2S1Ib.AutoSize = true;
            this.Marker2S1Ib.Location = new System.Drawing.Point(6, 27);
            this.Marker2S1Ib.Name = "Marker2S1Ib";
            this.Marker2S1Ib.Size = new System.Drawing.Size(10, 13);
            this.Marker2S1Ib.TabIndex = 5;
            this.Marker2S1Ib.Text = " ";
            // 
            // Marker2S1Ia
            // 
            this.Marker2S1Ia.AutoSize = true;
            this.Marker2S1Ia.Location = new System.Drawing.Point(6, 12);
            this.Marker2S1Ia.Name = "Marker2S1Ia";
            this.Marker2S1Ia.Size = new System.Drawing.Size(10, 13);
            this.Marker2S1Ia.TabIndex = 4;
            this.Marker2S1Ia.Text = " ";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.groupBox16);
            this.groupBox2.Controls.Add(this.groupBox11);
            this.groupBox2.Controls.Add(this.groupBox8);
            this.groupBox2.Location = new System.Drawing.Point(7, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(110, 489);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "������1";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.Marker1D24);
            this.groupBox16.Controls.Add(this.Marker1D23);
            this.groupBox16.Controls.Add(this.Marker1D22);
            this.groupBox16.Controls.Add(this.Marker1D21);
            this.groupBox16.Controls.Add(this.Marker1D12);
            this.groupBox16.Controls.Add(this.Marker1D11);
            this.groupBox16.Controls.Add(this.Marker1D10);
            this.groupBox16.Controls.Add(this.Marker1D9);
            this.groupBox16.Controls.Add(this.Marker1D20);
            this.groupBox16.Controls.Add(this.Marker1D19);
            this.groupBox16.Controls.Add(this.Marker1D18);
            this.groupBox16.Controls.Add(this.Marker1D17);
            this.groupBox16.Controls.Add(this.Marker1D16);
            this.groupBox16.Controls.Add(this.Marker1D15);
            this.groupBox16.Controls.Add(this.Marker1D14);
            this.groupBox16.Controls.Add(this.Marker1D13);
            this.groupBox16.Controls.Add(this.Marker1D8);
            this.groupBox16.Controls.Add(this.Marker1D7);
            this.groupBox16.Controls.Add(this.Marker1D6);
            this.groupBox16.Controls.Add(this.Marker1D5);
            this.groupBox16.Controls.Add(this.Marker1D4);
            this.groupBox16.Controls.Add(this.Marker1D3);
            this.groupBox16.Controls.Add(this.Marker1D2);
            this.groupBox16.Controls.Add(this.Marker1D1);
            this.groupBox16.Location = new System.Drawing.Point(6, 162);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(98, 181);
            this.groupBox16.TabIndex = 44;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "��������";
            // 
            // Marker1D24
            // 
            this.Marker1D24.AutoSize = true;
            this.Marker1D24.Location = new System.Drawing.Point(49, 162);
            this.Marker1D24.Name = "Marker1D24";
            this.Marker1D24.Size = new System.Drawing.Size(10, 13);
            this.Marker1D24.TabIndex = 63;
            this.Marker1D24.Text = " ";
            // 
            // Marker1D23
            // 
            this.Marker1D23.AutoSize = true;
            this.Marker1D23.Location = new System.Drawing.Point(49, 149);
            this.Marker1D23.Name = "Marker1D23";
            this.Marker1D23.Size = new System.Drawing.Size(10, 13);
            this.Marker1D23.TabIndex = 62;
            this.Marker1D23.Text = " ";
            // 
            // Marker1D22
            // 
            this.Marker1D22.AutoSize = true;
            this.Marker1D22.Location = new System.Drawing.Point(49, 136);
            this.Marker1D22.Name = "Marker1D22";
            this.Marker1D22.Size = new System.Drawing.Size(10, 13);
            this.Marker1D22.TabIndex = 61;
            this.Marker1D22.Text = " ";
            // 
            // Marker1D21
            // 
            this.Marker1D21.AutoSize = true;
            this.Marker1D21.Location = new System.Drawing.Point(49, 121);
            this.Marker1D21.Name = "Marker1D21";
            this.Marker1D21.Size = new System.Drawing.Size(10, 13);
            this.Marker1D21.TabIndex = 60;
            this.Marker1D21.Text = " ";
            // 
            // Marker1D12
            // 
            this.Marker1D12.AutoSize = true;
            this.Marker1D12.Location = new System.Drawing.Point(2, 162);
            this.Marker1D12.Name = "Marker1D12";
            this.Marker1D12.Size = new System.Drawing.Size(10, 13);
            this.Marker1D12.TabIndex = 59;
            this.Marker1D12.Text = " ";
            // 
            // Marker1D11
            // 
            this.Marker1D11.AutoSize = true;
            this.Marker1D11.Location = new System.Drawing.Point(2, 149);
            this.Marker1D11.Name = "Marker1D11";
            this.Marker1D11.Size = new System.Drawing.Size(10, 13);
            this.Marker1D11.TabIndex = 58;
            this.Marker1D11.Text = " ";
            // 
            // Marker1D10
            // 
            this.Marker1D10.AutoSize = true;
            this.Marker1D10.Location = new System.Drawing.Point(2, 136);
            this.Marker1D10.Name = "Marker1D10";
            this.Marker1D10.Size = new System.Drawing.Size(10, 13);
            this.Marker1D10.TabIndex = 57;
            this.Marker1D10.Text = " ";
            // 
            // Marker1D9
            // 
            this.Marker1D9.AutoSize = true;
            this.Marker1D9.Location = new System.Drawing.Point(2, 121);
            this.Marker1D9.Name = "Marker1D9";
            this.Marker1D9.Size = new System.Drawing.Size(10, 13);
            this.Marker1D9.TabIndex = 56;
            this.Marker1D9.Text = " ";
            // 
            // Marker1D20
            // 
            this.Marker1D20.AutoSize = true;
            this.Marker1D20.Location = new System.Drawing.Point(49, 108);
            this.Marker1D20.Name = "Marker1D20";
            this.Marker1D20.Size = new System.Drawing.Size(10, 13);
            this.Marker1D20.TabIndex = 55;
            this.Marker1D20.Text = " ";
            // 
            // Marker1D19
            // 
            this.Marker1D19.AutoSize = true;
            this.Marker1D19.Location = new System.Drawing.Point(49, 95);
            this.Marker1D19.Name = "Marker1D19";
            this.Marker1D19.Size = new System.Drawing.Size(10, 13);
            this.Marker1D19.TabIndex = 54;
            this.Marker1D19.Text = " ";
            // 
            // Marker1D18
            // 
            this.Marker1D18.AutoSize = true;
            this.Marker1D18.Location = new System.Drawing.Point(49, 82);
            this.Marker1D18.Name = "Marker1D18";
            this.Marker1D18.Size = new System.Drawing.Size(10, 13);
            this.Marker1D18.TabIndex = 53;
            this.Marker1D18.Text = " ";
            // 
            // Marker1D17
            // 
            this.Marker1D17.AutoSize = true;
            this.Marker1D17.Location = new System.Drawing.Point(49, 67);
            this.Marker1D17.Name = "Marker1D17";
            this.Marker1D17.Size = new System.Drawing.Size(10, 13);
            this.Marker1D17.TabIndex = 52;
            this.Marker1D17.Text = " ";
            // 
            // Marker1D16
            // 
            this.Marker1D16.AutoSize = true;
            this.Marker1D16.Location = new System.Drawing.Point(49, 54);
            this.Marker1D16.Name = "Marker1D16";
            this.Marker1D16.Size = new System.Drawing.Size(10, 13);
            this.Marker1D16.TabIndex = 51;
            this.Marker1D16.Text = " ";
            // 
            // Marker1D15
            // 
            this.Marker1D15.AutoSize = true;
            this.Marker1D15.Location = new System.Drawing.Point(49, 41);
            this.Marker1D15.Name = "Marker1D15";
            this.Marker1D15.Size = new System.Drawing.Size(10, 13);
            this.Marker1D15.TabIndex = 50;
            this.Marker1D15.Text = " ";
            // 
            // Marker1D14
            // 
            this.Marker1D14.AutoSize = true;
            this.Marker1D14.Location = new System.Drawing.Point(49, 28);
            this.Marker1D14.Name = "Marker1D14";
            this.Marker1D14.Size = new System.Drawing.Size(10, 13);
            this.Marker1D14.TabIndex = 49;
            this.Marker1D14.Text = " ";
            // 
            // Marker1D13
            // 
            this.Marker1D13.AutoSize = true;
            this.Marker1D13.Location = new System.Drawing.Point(49, 13);
            this.Marker1D13.Name = "Marker1D13";
            this.Marker1D13.Size = new System.Drawing.Size(10, 13);
            this.Marker1D13.TabIndex = 48;
            this.Marker1D13.Text = " ";
            // 
            // Marker1D8
            // 
            this.Marker1D8.AutoSize = true;
            this.Marker1D8.Location = new System.Drawing.Point(2, 108);
            this.Marker1D8.Name = "Marker1D8";
            this.Marker1D8.Size = new System.Drawing.Size(10, 13);
            this.Marker1D8.TabIndex = 47;
            this.Marker1D8.Text = " ";
            // 
            // Marker1D7
            // 
            this.Marker1D7.AutoSize = true;
            this.Marker1D7.Location = new System.Drawing.Point(2, 95);
            this.Marker1D7.Name = "Marker1D7";
            this.Marker1D7.Size = new System.Drawing.Size(10, 13);
            this.Marker1D7.TabIndex = 46;
            this.Marker1D7.Text = " ";
            // 
            // Marker1D6
            // 
            this.Marker1D6.AutoSize = true;
            this.Marker1D6.Location = new System.Drawing.Point(2, 82);
            this.Marker1D6.Name = "Marker1D6";
            this.Marker1D6.Size = new System.Drawing.Size(10, 13);
            this.Marker1D6.TabIndex = 45;
            this.Marker1D6.Text = " ";
            // 
            // Marker1D5
            // 
            this.Marker1D5.AutoSize = true;
            this.Marker1D5.Location = new System.Drawing.Point(2, 67);
            this.Marker1D5.Name = "Marker1D5";
            this.Marker1D5.Size = new System.Drawing.Size(10, 13);
            this.Marker1D5.TabIndex = 44;
            this.Marker1D5.Text = " ";
            // 
            // Marker1D4
            // 
            this.Marker1D4.AutoSize = true;
            this.Marker1D4.Location = new System.Drawing.Point(2, 54);
            this.Marker1D4.Name = "Marker1D4";
            this.Marker1D4.Size = new System.Drawing.Size(10, 13);
            this.Marker1D4.TabIndex = 43;
            this.Marker1D4.Text = " ";
            // 
            // Marker1D3
            // 
            this.Marker1D3.AutoSize = true;
            this.Marker1D3.Location = new System.Drawing.Point(2, 41);
            this.Marker1D3.Name = "Marker1D3";
            this.Marker1D3.Size = new System.Drawing.Size(10, 13);
            this.Marker1D3.TabIndex = 42;
            this.Marker1D3.Text = " ";
            // 
            // Marker1D2
            // 
            this.Marker1D2.AutoSize = true;
            this.Marker1D2.Location = new System.Drawing.Point(2, 28);
            this.Marker1D2.Name = "Marker1D2";
            this.Marker1D2.Size = new System.Drawing.Size(10, 13);
            this.Marker1D2.TabIndex = 41;
            this.Marker1D2.Text = " ";
            // 
            // Marker1D1
            // 
            this.Marker1D1.AutoSize = true;
            this.Marker1D1.Location = new System.Drawing.Point(2, 13);
            this.Marker1D1.Name = "Marker1D1";
            this.Marker1D1.Size = new System.Drawing.Size(10, 13);
            this.Marker1D1.TabIndex = 40;
            this.Marker1D1.Text = " ";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.Marker1Un);
            this.groupBox11.Controls.Add(this.Marker1Uc);
            this.groupBox11.Controls.Add(this.Marker1Ub);
            this.groupBox11.Controls.Add(this.Marker1Ua);
            this.groupBox11.Location = new System.Drawing.Point(6, 90);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(98, 72);
            this.groupBox11.TabIndex = 43;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "����������";
            // 
            // Marker1Un
            // 
            this.Marker1Un.AutoSize = true;
            this.Marker1Un.Location = new System.Drawing.Point(6, 53);
            this.Marker1Un.Name = "Marker1Un";
            this.Marker1Un.Size = new System.Drawing.Size(10, 13);
            this.Marker1Un.TabIndex = 19;
            this.Marker1Un.Text = " ";
            // 
            // Marker1Uc
            // 
            this.Marker1Uc.AutoSize = true;
            this.Marker1Uc.Location = new System.Drawing.Point(6, 40);
            this.Marker1Uc.Name = "Marker1Uc";
            this.Marker1Uc.Size = new System.Drawing.Size(10, 13);
            this.Marker1Uc.TabIndex = 18;
            this.Marker1Uc.Text = " ";
            // 
            // Marker1Ub
            // 
            this.Marker1Ub.AutoSize = true;
            this.Marker1Ub.Location = new System.Drawing.Point(6, 27);
            this.Marker1Ub.Name = "Marker1Ub";
            this.Marker1Ub.Size = new System.Drawing.Size(10, 13);
            this.Marker1Ub.TabIndex = 17;
            this.Marker1Ub.Text = " ";
            // 
            // Marker1Ua
            // 
            this.Marker1Ua.AutoSize = true;
            this.Marker1Ua.Location = new System.Drawing.Point(6, 12);
            this.Marker1Ua.Name = "Marker1Ua";
            this.Marker1Ua.Size = new System.Drawing.Size(10, 13);
            this.Marker1Ua.TabIndex = 16;
            this.Marker1Ua.Text = " ";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.Marker1S1In);
            this.groupBox8.Controls.Add(this.Marker1S1Ic);
            this.groupBox8.Controls.Add(this.Marker1S1Ib);
            this.groupBox8.Controls.Add(this.Marker1S1Ia);
            this.groupBox8.Location = new System.Drawing.Point(5, 12);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(98, 72);
            this.groupBox8.TabIndex = 40;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "������� 1";
            // 
            // Marker1S1In
            // 
            this.Marker1S1In.AutoSize = true;
            this.Marker1S1In.Location = new System.Drawing.Point(6, 53);
            this.Marker1S1In.Name = "Marker1S1In";
            this.Marker1S1In.Size = new System.Drawing.Size(10, 13);
            this.Marker1S1In.TabIndex = 7;
            this.Marker1S1In.Text = " ";
            // 
            // Marker1S1Ic
            // 
            this.Marker1S1Ic.AutoSize = true;
            this.Marker1S1Ic.Location = new System.Drawing.Point(6, 40);
            this.Marker1S1Ic.Name = "Marker1S1Ic";
            this.Marker1S1Ic.Size = new System.Drawing.Size(10, 13);
            this.Marker1S1Ic.TabIndex = 6;
            this.Marker1S1Ic.Text = " ";
            // 
            // Marker1S1Ib
            // 
            this.Marker1S1Ib.AutoSize = true;
            this.Marker1S1Ib.Location = new System.Drawing.Point(6, 27);
            this.Marker1S1Ib.Name = "Marker1S1Ib";
            this.Marker1S1Ib.Size = new System.Drawing.Size(10, 13);
            this.Marker1S1Ib.TabIndex = 5;
            this.Marker1S1Ib.Text = " ";
            // 
            // Marker1S1Ia
            // 
            this.Marker1S1Ia.AutoSize = true;
            this.Marker1S1Ia.Location = new System.Drawing.Point(6, 12);
            this.Marker1S1Ia.Name = "Marker1S1Ia";
            this.Marker1S1Ia.Size = new System.Drawing.Size(0, 13);
            this.Marker1S1Ia.TabIndex = 4;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.button43, 0, 1);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel9.TabIndex = 0;
            // 
            // button43
            // 
            this.button43.BackColor = System.Drawing.Color.ForestGreen;
            this.button43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button43.Location = new System.Drawing.Point(3, 23);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(194, 74);
            this.button43.TabIndex = 1;
            this.button43.Text = "-";
            this.button43.UseVisualStyleBackColor = false;
            // 
            // button44
            // 
            this.button44.BackColor = System.Drawing.Color.ForestGreen;
            this.button44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button44.Location = new System.Drawing.Point(3, 3);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(28, 24);
            this.button44.TabIndex = 0;
            this.button44.Text = "+";
            this.button44.UseVisualStyleBackColor = false;
            // 
            // _avaryOscCheck
            // 
            this._avaryOscCheck.BackColor = System.Drawing.Color.Silver;
            this._avaryOscCheck.Location = new System.Drawing.Point(443, 4);
            this._avaryOscCheck.Name = "_avaryOscCheck";
            this._avaryOscCheck.Size = new System.Drawing.Size(63, 16);
            this._avaryOscCheck.TabIndex = 1;
            this._avaryOscCheck.Text = "�.���";
            this._avaryOscCheck.UseVisualStyleBackColor = false;
            this._avaryOscCheck.CheckedChanged += new System.EventHandler(this._avaryOscCheck_CheckedChanged);
            // 
            // Xplus
            // 
            this.Xplus.BackColor = System.Drawing.Color.ForestGreen;
            this.Xplus.Cursor = System.Windows.Forms.Cursors.Default;
            this.Xplus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Xplus.Location = new System.Drawing.Point(149, 2);
            this.Xplus.Name = "Xplus";
            this.Xplus.Size = new System.Drawing.Size(33, 20);
            this.Xplus.TabIndex = 2;
            this.Xplus.Text = "X -";
            this.Xplus.UseVisualStyleBackColor = false;
            this.Xplus.Click += new System.EventHandler(this.Xplus_Click);
            // 
            // Xminus
            // 
            this.Xminus.BackColor = System.Drawing.Color.ForestGreen;
            this.Xminus.Cursor = System.Windows.Forms.Cursors.Default;
            this.Xminus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Xminus.Location = new System.Drawing.Point(107, 2);
            this.Xminus.Name = "Xminus";
            this.Xminus.Size = new System.Drawing.Size(33, 20);
            this.Xminus.TabIndex = 3;
            this.Xminus.Text = "X +";
            this.Xminus.UseVisualStyleBackColor = false;
            this.Xminus.Click += new System.EventHandler(this.Xminus_Click);
            // 
            // S1ShowCB
            // 
            this.S1ShowCB.BackColor = System.Drawing.Color.Silver;
            this.S1ShowCB.Checked = true;
            this.S1ShowCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.S1ShowCB.Location = new System.Drawing.Point(215, 4);
            this.S1ShowCB.Name = "S1ShowCB";
            this.S1ShowCB.Size = new System.Drawing.Size(77, 16);
            this.S1ShowCB.TabIndex = 4;
            this.S1ShowCB.Text = "����";
            this.S1ShowCB.UseVisualStyleBackColor = false;
            this.S1ShowCB.CheckedChanged += new System.EventHandler(this.S1ShowCB_CheckedChanged);
            // 
            // UShowCB
            // 
            this.UShowCB.BackColor = System.Drawing.Color.Silver;
            this.UShowCB.Checked = true;
            this.UShowCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.UShowCB.Location = new System.Drawing.Point(269, 4);
            this.UShowCB.Name = "UShowCB";
            this.UShowCB.Size = new System.Drawing.Size(90, 16);
            this.UShowCB.TabIndex = 7;
            this.UShowCB.Text = "����������";
            this.UShowCB.UseVisualStyleBackColor = false;
            this.UShowCB.CheckedChanged += new System.EventHandler(this.UShowCB_CheckedChanged);
            // 
            // DiscretShowCB
            // 
            this.DiscretShowCB.BackColor = System.Drawing.Color.Silver;
            this.DiscretShowCB.Checked = true;
            this.DiscretShowCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DiscretShowCB.Location = new System.Drawing.Point(363, 4);
            this.DiscretShowCB.Name = "DiscretShowCB";
            this.DiscretShowCB.Size = new System.Drawing.Size(80, 16);
            this.DiscretShowCB.TabIndex = 8;
            this.DiscretShowCB.Text = "��������";
            this.DiscretShowCB.UseVisualStyleBackColor = false;
            this.DiscretShowCB.CheckedChanged += new System.EventHandler(this.DiscretShowCB_CheckedChanged);
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ShowAlways = true;
            this.toolTip1.StripAmpersands = true;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // MarkerCB
            // 
            this.MarkerCB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MarkerCB.BackColor = System.Drawing.Color.Silver;
            this.MarkerCB.Location = new System.Drawing.Point(786, 4);
            this.MarkerCB.Name = "MarkerCB";
            this.MarkerCB.Size = new System.Drawing.Size(73, 17);
            this.MarkerCB.TabIndex = 11;
            this.MarkerCB.Text = "�������";
            this.MarkerCB.UseVisualStyleBackColor = false;
            this.MarkerCB.CheckedChanged += new System.EventHandler(this.MarkerCB_CheckedChanged);
            // 
            // FlagCB
            // 
            this.FlagCB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FlagCB.BackColor = System.Drawing.Color.Silver;
            this.FlagCB.Location = new System.Drawing.Point(723, 4);
            this.FlagCB.Name = "FlagCB";
            this.FlagCB.Size = new System.Drawing.Size(58, 17);
            this.FlagCB.TabIndex = 12;
            this.FlagCB.Text = "�����";
            this.FlagCB.UseVisualStyleBackColor = false;
            // 
            // _avaryJournalCheck
            // 
            this._avaryJournalCheck.BackColor = System.Drawing.Color.Silver;
            this._avaryJournalCheck.Location = new System.Drawing.Point(505, 4);
            this._avaryJournalCheck.Name = "_avaryJournalCheck";
            this._avaryJournalCheck.Size = new System.Drawing.Size(53, 17);
            this._avaryJournalCheck.TabIndex = 13;
            this._avaryJournalCheck.Text = "�.��";
            this._avaryJournalCheck.UseVisualStyleBackColor = false;
            this._avaryJournalCheck.Visible = false;
            this._avaryJournalCheck.CheckedChanged += new System.EventHandler(this._avaryJournalCheck_CheckedChanged);
            // 
            // OscilloscopeResultForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(865, 624);
            this.Controls.Add(this._avaryJournalCheck);
            this.Controls.Add(this.FlagCB);
            this.Controls.Add(this.MarkerCB);
            this.Controls.Add(this.DiscretShowCB);
            this.Controls.Add(this.UShowCB);
            this.Controls.Add(this.S1ShowCB);
            this.Controls.Add(this.Xminus);
            this.Controls.Add(this.Xplus);
            this.Controls.Add(this._avaryOscCheck);
            this.Controls.Add(this.MAINTABLE);
            this.Name = "OscilloscopeResultForm";
            this.Text = "�������������";
            this.Load += new System.EventHandler(this.OscilloscopeResultForm_Load);
            this.MAINTABLE.ResumeLayout(false);
            this.MAINTABLE.PerformLayout();
            this.MAINPANEL.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.MarkersTable.ResumeLayout(false);
            this.MarkersTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Marker1TrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Marker2TrackBar)).EndInit();
            this.panel3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel MAINTABLE;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.HScrollBar hScrollBar4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.CheckBox _avaryOscCheck;
        private System.Windows.Forms.Button Xplus;
        private System.Windows.Forms.Button Xminus;
        private System.Windows.Forms.CheckBox S1ShowCB;
        private System.Windows.Forms.CheckBox UShowCB;
        private System.Windows.Forms.CheckBox DiscretShowCB;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel MAINPANEL;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private BEMN_XY_Chart.DAS_Net_XYChart _discretChart;
        private System.Windows.Forms.Splitter splitter4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button UYminus;
        private System.Windows.Forms.Button UYPlus;
        private BEMN_XY_Chart.DAS_Net_XYChart _voltageChart;
        private System.Windows.Forms.VScrollBar UScroll;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.VScrollBar S1Scroll;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button S1Yminus;
        private System.Windows.Forms.Button S1YPlus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel MarkersTable;
        private System.Windows.Forms.CheckBox Marker1CB;
        private System.Windows.Forms.CheckBox Marker2CB;
        private System.Windows.Forms.TrackBar Marker1TrackBar;
        private System.Windows.Forms.TrackBar Marker2TrackBar;
        private System.Windows.Forms.CheckBox MarkerCB;
        private System.Windows.Forms.CheckBox FlagCB;
        private System.Windows.Forms.CheckBox _avaryJournalCheck;
        private BEMN_XY_Chart.DAS_Net_XYChart _s1TokChart;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label Marker1S1In;
        private System.Windows.Forms.Label Marker1S1Ic;
        private System.Windows.Forms.Label Marker1S1Ib;
        private System.Windows.Forms.Label Marker1S1Ia;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label Marker1Un;
        private System.Windows.Forms.Label Marker1Uc;
        private System.Windows.Forms.Label Marker1Ub;
        private System.Windows.Forms.Label Marker1Ua;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label Marker1D24;
        private System.Windows.Forms.Label Marker1D23;
        private System.Windows.Forms.Label Marker1D22;
        private System.Windows.Forms.Label Marker1D21;
        private System.Windows.Forms.Label Marker1D12;
        private System.Windows.Forms.Label Marker1D11;
        private System.Windows.Forms.Label Marker1D10;
        private System.Windows.Forms.Label Marker1D9;
        private System.Windows.Forms.Label Marker1D20;
        private System.Windows.Forms.Label Marker1D19;
        private System.Windows.Forms.Label Marker1D18;
        private System.Windows.Forms.Label Marker1D17;
        private System.Windows.Forms.Label Marker1D16;
        private System.Windows.Forms.Label Marker1D15;
        private System.Windows.Forms.Label Marker1D14;
        private System.Windows.Forms.Label Marker1D13;
        private System.Windows.Forms.Label Marker1D8;
        private System.Windows.Forms.Label Marker1D7;
        private System.Windows.Forms.Label Marker1D6;
        private System.Windows.Forms.Label Marker1D5;
        private System.Windows.Forms.Label Marker1D4;
        private System.Windows.Forms.Label Marker1D3;
        private System.Windows.Forms.Label Marker1D2;
        private System.Windows.Forms.Label Marker1D1;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label Marker2Un;
        private System.Windows.Forms.Label Marker2Uc;
        private System.Windows.Forms.Label Marker2Ub;
        private System.Windows.Forms.Label Marker2Ua;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Label Marker2S1In;
        private System.Windows.Forms.Label Marker2S1Ic;
        private System.Windows.Forms.Label Marker2S1Ib;
        private System.Windows.Forms.Label Marker2S1Ia;
        private System.Windows.Forms.Label Marker2D24;
        private System.Windows.Forms.Label Marker2D23;
        private System.Windows.Forms.Label Marker2D22;
        private System.Windows.Forms.Label Marker2D21;
        private System.Windows.Forms.Label Marker2D12;
        private System.Windows.Forms.Label Marker2D11;
        private System.Windows.Forms.Label Marker2D10;
        private System.Windows.Forms.Label Marker2D9;
        private System.Windows.Forms.Label Marker2D20;
        private System.Windows.Forms.Label Marker2D19;
        private System.Windows.Forms.Label Marker2D18;
        private System.Windows.Forms.Label Marker2D17;
        private System.Windows.Forms.Label Marker2D16;
        private System.Windows.Forms.Label Marker2D15;
        private System.Windows.Forms.Label Marker2D14;
        private System.Windows.Forms.Label Marker2D13;
        private System.Windows.Forms.Label Marker2D8;
        private System.Windows.Forms.Label Marker2D7;
        private System.Windows.Forms.Label Marker2D6;
        private System.Windows.Forms.Label Marker2D5;
        private System.Windows.Forms.Label Marker2D4;
        private System.Windows.Forms.Label Marker2D3;
        private System.Windows.Forms.Label Marker2D2;
        private System.Windows.Forms.Label Marker2D1;
        private System.Windows.Forms.Label TimeMarker2;
        private System.Windows.Forms.Label TimeMarker1;
        private System.Windows.Forms.Label DeltaLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;









    }
}