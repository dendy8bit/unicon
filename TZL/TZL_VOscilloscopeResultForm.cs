using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN_XY_Chart;
using System.Collections;
using System.Linq;
using BEMN.TZL.NewOsc.HelpClasses;

namespace BEMN.TZL.Forms
{
    public partial class OscilloscopeResultForm : Form
    {
        CountingList _countingList;
        List<char[]> _discrets; 
        private List<int> _avaryBeginList = new List<int>();
        ArrayList _arrayX;
        ArrayList _arrayYa;
        ArrayList _arrayYb;
        ArrayList _arrayYc;
        ArrayList _arrayYn;
        int _maxX;
        int _S1maxY;
        int _S1minY;
        int _S2maxY;
        int _S2minY;
        int _S3maxY;
        int _S3minY;
        int _UmaxY;
        int _UminY;
        double _maxCurrentX;
        double _S1maxCurrentY;
        double _S1minCurrentY;
        double _S2maxCurrentY;
        double _S2minCurrentY;
        double _S3maxCurrentY;
        double _S3minCurrentY;
        double _UmaxCurrentY;
        double _UminCurrentY;
        double _S1chartStepY = 0;
        double _S2chartStepY = 0;
        double _S3chartStepY = 0;
        double _UchartStepY = 0;

        public OscilloscopeResultForm()
        {
            InitializeComponent();
        }

        public OscilloscopeResultForm(CountingList countingList,List<int> avaryBeginList)
        {
            InitializeComponent();
            _countingList = countingList;
            for (int i = 0; i < _countingList.Discrets.Length; i++)
            {
                int j = 0;
                int lenght=0;
                string arr="";
                while (_countingList.Discrets[i].Length > lenght)
                {
                    arr += Convert.ToString(_countingList.Discrets[i][j], 2);
                    j++;
                }
                _discrets.Add(arr.ToCharArray());
            }
            _avaryBeginList = avaryBeginList;
        }

        private Color GetColor(int index)
        {
            Color color = new Color();
            switch (index)
            {
                case 0:
                    color = Color.Yellow;
                    break;
                case 1:
                    color = Color.Green;
                    break;
                case 2:
                    color = Color.Red;
                    break;
                case 3:
                    color = Color.Blue;
                    break;
                case 4:
                    color = Color.LightPink;
                    break;
                case 5:
                    color = Color.LightGoldenrodYellow;
                    break;
                case 6:
                    color = Color.LightGreen;
                    break;
                case 7:
                    color = Color.LightBlue;
                    break;
                case 8:
                    color = Color.Indigo;
                    break;
                case 9:
                    color = Color.DarkViolet;
                    break;
                case 10:
                    color = Color.DarkOrange;
                    break;
                case 11:
                    color = Color.DarkBlue;
                    break;
                case 12:
                    color = Color.PaleVioletRed;
                    break;
                case 13:
                    color = Color.LightSeaGreen;
                    break;
                case 14:
                    color = Color.DarkRed;
                    break;
                case 15:
                    color = Color.DarkTurquoise;
                    break;
                case 16:
                    color = Color.Goldenrod;
                    break;
                case 17:
                    color = Color.Lime;
                    break;
                case 18:
                    color = Color.Magenta;
                    break;
                case 19:
                    color = Color.Aqua;
                    break;
                case 20:
                    color = Color.Olive;
                    break;
                case 21:
                    color = Color.Khaki;
                    break;
                case 22:
                    color = Color.MediumAquamarine;
                    break;
                case 23:
                    color = Color.OliveDrab;
                    break;
                case 24:
                    color = Color.Black;
                    break;
                case 25:
                    color = Color.Gray;
                    break;
                case 26:
                    color = Color.Gray;
                    break;
                default:
                    break;
            }
            return color;
        }

        private DAS_Net_XYChart.DAS_XYCurveVariable CreateCurve(string name, int curveIndex)
        {
            BEMN_XY_Chart.DAS_Net_XYChart.DAS_XYCurveVariable _curve = new BEMN_XY_Chart.DAS_Net_XYChart.DAS_XYCurveVariable();
            _curve.cColor = GetColor(curveIndex);
            _curve.strCurveName = name;
            _curve.bLineVisible = true;
            _curve.iPointNumber = 100;
            _curve.iPointSize = 5;
            _curve.iLineWidth = 1;
            _curve.ePointStyle = BEMN_XY_Chart.DAS_CurvePointStyle.BPTS_NONE;
            _curve.bVisible = true;
            _curve.iCurPriority = 0;
            _curve.dblMax = 1000.0;
            _curve.dblMin = 0;
            _curve.bYScaleVisible = true;
            _curve.bYAtStart = true;
            _curve.bAreaMode = false;
            _curve.dblAreaBaseValue = 0.0;

            return _curve;
        }

        private DAS_Net_XYChart.DAS_XYCurveVariable CreateLineCurve(string name, Color curveColor, bool visible, int lineWidth)
        {
            BEMN_XY_Chart.DAS_Net_XYChart.DAS_XYCurveVariable _curve = new BEMN_XY_Chart.DAS_Net_XYChart.DAS_XYCurveVariable();
            _curve.cColor = curveColor;
            _curve.strCurveName = name;
            _curve.bLineVisible = visible;
            _curve.iPointNumber = 100;
            _curve.iPointSize = 5;
            _curve.iLineWidth = lineWidth;
            _curve.ePointStyle = BEMN_XY_Chart.DAS_CurvePointStyle.BPTS_NONE;
            _curve.bVisible = true;
            _curve.iCurPriority = 0;
            _curve.dblMax = 1000.0;
            _curve.dblMin = 0;
            _curve.bYScaleVisible = true;
            _curve.bYAtStart = true;
            _curve.bAreaMode = false;
            _curve.dblAreaBaseValue = 0.0;

            return _curve;
        }

        private DAS_Net_XYChart.DAS_XYCurveVariable CurveVisible(string name, int curveIndex)
        {
            BEMN_XY_Chart.DAS_Net_XYChart.DAS_XYCurveVariable _curve = new BEMN_XY_Chart.DAS_Net_XYChart.DAS_XYCurveVariable();
            _curve.cColor = GetColor(curveIndex);
            _curve.strCurveName = name;
            _curve.bLineVisible = false;
            _curve.iPointNumber = 100;
            _curve.iPointSize = 5;
            _curve.iLineWidth = 1;
            _curve.ePointStyle = BEMN_XY_Chart.DAS_CurvePointStyle.BPTS_NONE;
            _curve.bVisible = true;
            _curve.iCurPriority = 0;
            _curve.dblMax = 1000.0;
            _curve.dblMin = 0;
            _curve.bYScaleVisible = true;
            _curve.bYAtStart = true;
            _curve.bAreaMode = false;
            _curve.dblAreaBaseValue = 0.0;

            return _curve;
        }

        public void AddLineCurves(DAS_Net_XYChart _chart, string name, Color color, int lineWidth)
        {
            _chart.AddCurve(name, CreateLineCurve(name, color, true, lineWidth));
        }

        public void AddLinePoints(DAS_Net_XYChart _chart, string name, int value)
        {
            _chart.AddCurvePoint(name, value, _chart.CoordinateYMin);
            _chart.AddCurvePoint(name, value, _chart.CoordinateYMax);
            //_chart.Update();
        }

        public void VisibleLineCurves(DAS_Net_XYChart _chart, string name)
        {
            _chart.UpdateCurveProperty(name, CreateLineCurve(name,Color.Black,false, 0));
            _chart.Update();
        }
        
        public void UpdateLinePoints(DAS_Net_XYChart _chart, string name, int value)
        {
            _chart.RefreshCurvePoint(name, new ArrayList() { value, value }, new ArrayList() { _chart.CoordinateYMin, _chart.CoordinateYMax });
        }

        public void InitS1ChartLimits(DAS_Net_XYChart _chart) 
        {
            double[] _max = new double[]{(double)(_countingList.Currents[0].Max()),(double)(_countingList.Currents[1].Max()),
                                          (double)(_countingList.Currents[2].Max()),(double)(_countingList.Currents[3].Max())};
            double[] _min = new double[]{(double)(_countingList.Currents[0].Min()),(double)(_countingList.Currents[1].Min()),
                                          (double)(_countingList.Currents[2].Min()),(double)(_countingList.Currents[3].Min())};

            _chart.CoordinateYMax = GetMax(_max);
            _chart.CoordinateYMin = GetMin(_min);
            _chart.XMin = 0;
            _chart.XMax = _countingList.Currents[0].Length;
        }

        public void AddS1Curves(DAS_Net_XYChart _chart)
        {
            _chart.AddCurve(CountingList.INames[0], CreateCurve(CountingList.INames[0], 0));
            _chart.AddCurve(CountingList.INames[1], CreateCurve(CountingList.INames[1], 1));
            _chart.AddCurve(CountingList.INames[2], CreateCurve(CountingList.INames[2], 2));
            _chart.AddCurve(CountingList.INames[3], CreateCurve(CountingList.INames[3], 3));
        }

        public void AddS1Points(DAS_Net_XYChart _chart)
        {
            int _step = 1;
            if ((int)((_s1TokChart.XMax - _s1TokChart.XMin) / _s1TokChart.Width) > _step)
            {
                _step = (int)((_s1TokChart.XMax - _s1TokChart.XMin) / _s1TokChart.Width);
            }
            for (int i = (int)_s1TokChart.XMin; i <= _s1TokChart.XMax; i += _step)
            {
                if (i <= _countingList.Currents[0].Length - 1)
                {
                    _chart.AddCurvePoint(CountingList.INames[0], i, _countingList.Currents[0][i]);
                    _chart.AddCurvePoint(CountingList.INames[1], i, _countingList.Currents[1][i]);
                    _chart.AddCurvePoint(CountingList.INames[2], i, _countingList.Currents[2][i]);
                    _chart.AddCurvePoint(CountingList.INames[3], i, _countingList.Currents[3][i]);
                }
            }
            _chart.Update();
        }

        public void RefreshS1Points()
        {
            int _step = 1;
            _arrayX = new ArrayList();
            _arrayYa = new ArrayList();
            _arrayYb = new ArrayList();
            _arrayYc = new ArrayList();
            _arrayYn = new ArrayList();
            if ((int)((_s1TokChart.XMax - _s1TokChart.XMin) / _s1TokChart.Width) > _step) 
            {
                _step = (int)((_s1TokChart.XMax - _s1TokChart.XMin) / _s1TokChart.Width);
            }
            if (_s1TokChart.XMin < 0)
            {
                _s1TokChart.XMin = 0;
            }
            for (int i = (int)_s1TokChart.XMin; i <= _s1TokChart.XMax; i += _step)
            {
                try
                {
                    if (i <= _countingList.Currents[0].Length - 1)
                    {
                        _arrayX.Add(i);
                        _arrayYa.Add(_countingList.Currents[0][i]);
                        _arrayYb.Add(_countingList.Currents[1][i]);
                        _arrayYc.Add(_countingList.Currents[2][i]);
                        _arrayYn.Add(_countingList.Currents[3][i]);

                    }
                }
                catch { }
            } try
            {
                _s1TokChart.RefreshCurvePoint(CountingList.INames[0], _arrayX, _arrayYa);
                _s1TokChart.RefreshCurvePoint(CountingList.INames[1], _arrayX, _arrayYb);
                _s1TokChart.RefreshCurvePoint(CountingList.INames[2], _arrayX, _arrayYc);
                _s1TokChart.RefreshCurvePoint(CountingList.INames[3], _arrayX, _arrayYn);
                _s1TokChart.Refresh();
            }
            catch { }
        }

        public void InitUChartLimits(DAS_Net_XYChart _chart)
        {
            double[] _max = new double[]{_countingList.Voltages[0].Max(),_countingList.Voltages[1].Max(),
                                          _countingList.Voltages[2].Max(),_countingList.Voltages[3].Max()};
            double[] _min = new double[]{_countingList.Voltages[0].Min(),_countingList.Voltages[1].Min(),
                                          _countingList.Voltages[2].Min(),_countingList.Voltages[3].Min()};

            _chart.CoordinateYMax = GetMax(_max);
            _chart.CoordinateYMin = GetMin(_min);
            _chart.XMin = 0;
            _chart.XMax = _countingList.Voltages[0].Length;
        }

        public void AddUCurves(DAS_Net_XYChart _chart)
        {
            _chart.AddCurve(CountingList.UNames[0], CreateCurve(CountingList.UNames[0], 0));
            _chart.AddCurve(CountingList.UNames[1], CreateCurve(CountingList.UNames[1], 1));
            _chart.AddCurve(CountingList.UNames[2], CreateCurve(CountingList.UNames[2], 2));
            _chart.AddCurve(CountingList.UNames[3], CreateCurve(CountingList.UNames[3], 3));
        }

        public void AddUPoints(DAS_Net_XYChart _chart)
        {
            int _step = 1;
            if ((int)((_voltageChart.XMax - _voltageChart.XMin) / _voltageChart.Width) > _step)
            {
                _step = (int)((_voltageChart.XMax - _voltageChart.XMin) / _voltageChart.Width);
            }
            for (int i = (int)_voltageChart.XMin; i <= _voltageChart.XMax; i += _step)
            {
                if (i <= _countingList.Voltages[0].Length - 1)
                {
                    _chart.AddCurvePoint(CountingList.UNames[0], i, _countingList.Voltages[0][i]);
                    _chart.AddCurvePoint(CountingList.UNames[1], i, _countingList.Voltages[1][i]);
                    _chart.AddCurvePoint(CountingList.UNames[2], i, _countingList.Voltages[2][i]);
                    _chart.AddCurvePoint(CountingList.UNames[3], i, _countingList.Voltages[3][i]);
                }
            }
            _chart.Update();
        }

        public void RefreshUPoints()
        {
            int _step = 1;
            _arrayX = new ArrayList();
            _arrayYa = new ArrayList();
            _arrayYb = new ArrayList();
            _arrayYc = new ArrayList();
            _arrayYn = new ArrayList();
            if ((int)((_voltageChart.XMax - _voltageChart.XMin) / _voltageChart.Width) > _step)
            {
                _step = (int)((_voltageChart.XMax - _voltageChart.XMin) / _voltageChart.Width);
            }
            if (_voltageChart.XMin < 0)
            {
                _voltageChart.XMin = 0;
            }
            for (int i = (int)_voltageChart.XMin; i <= _voltageChart.XMax; i += _step)
            {
                try
                {
                    if (i <= _countingList.Voltages[0].Length - 1)
                    {
                        _arrayX.Add(i);
                        _arrayYa.Add(_countingList.Voltages[0]);
                        _arrayYb.Add(_countingList.Voltages[1]);
                        _arrayYc.Add(_countingList.Voltages[2]);
                        _arrayYn.Add(_countingList.Voltages[3]);
                    }
                }
                catch { }
            }
            try
            {
                _voltageChart.RefreshCurvePoint(CountingList.UNames[0], _arrayX, _arrayYa);
                _voltageChart.RefreshCurvePoint(CountingList.UNames[1], _arrayX, _arrayYb);
                _voltageChart.RefreshCurvePoint(CountingList.UNames[2], _arrayX, _arrayYc);
                _voltageChart.RefreshCurvePoint(CountingList.UNames[3], _arrayX, _arrayYn);
                _voltageChart.Refresh();
            }
            catch { }
        }

        public void InitDiscretChartLimits(DAS_Net_XYChart _chart)
        {
            _chart.CoordinateYMax = 17;
            _chart.CoordinateYMin = 0;
            _chart.GridYTicker = 17;
            _chart.XMin = 0;
            _chart.XMax = _countingList.Discrets[0].LongLength;
            _chart.CoordinateYOrigin = 17;
        }

        public void AddDiscretCurves(DAS_Net_XYChart _chart)
        {
            for (int i = 0; i <= _countingList.Count; i++) 
            {
                _chart.AddCurve("D" + i.ToString(), CreateCurve("D" + i.ToString(), i));
            }
        }

        public void AddDiscretPoints(DAS_Net_XYChart _chart)
        {
            int _step = 1;
            if ((int)((_discretChart.XMax - _discretChart.XMin) / _discretChart.Width) > _step)
            {
                _step = (int)((_discretChart.XMax - _discretChart.XMin) / _discretChart.Width);
            }
            if (_discretChart.XMin < 0)
            {
                _discretChart.XMin = 0;
            }
            for (int j = 0; j < _countingList.Count; j++)
            {
                for (int i = (int)_discretChart.XMin; i <= _discretChart.XMax; i += _step)
                {
                    if (i <= _countingList.Discrets[0].LongLength - 1)
                    {
                        var arr = new BitArray(BitConverter.GetBytes(_countingList.Discrets[j][i]));
                        for (int e = 0; e < arr.Length; e++)
                        {
                            if (arr[e])
                            {
                                _chart.AddCurvePoint("D" + j.ToString(), i, j + 1.5);
                            }

                            else
                            {
                                _chart.AddCurvePoint("D" + j.ToString(), i, j + 1);
                            }
                        }
                    }
                }
            }
            _chart.Update();
        }

        public void RefreshDiscretPoints()
        {
            int _step = 1;

            if ((int)((_discretChart.XMax - _discretChart.XMin) / _discretChart.Width) > _step)
            {
                _step = (int)((_discretChart.XMax - _discretChart.XMin) / _discretChart.Width);
            }
            if (_discretChart.XMin < 0)
            {
                _discretChart.XMin = 0;
            }
            try
            {
                for (int j = 0; j < _countingList.Discrets.Length ; j++)
                {
                    _arrayX = new ArrayList();
                    _arrayYa = new ArrayList();
                    for (int i = (int)_discretChart.XMin; i <= _discretChart.XMax; i += _step)
                    {
                        if (i <= _countingList.Discrets[0].Length - 1)
                        {
                            try
                            {
                                var arr = new BitArray(BitConverter.GetBytes(_countingList.Discrets[j][i]));
                                for (int e = 0; e < arr.Length; e++)
                                {
                                    if (arr[e])
                                    {
                                        _arrayX.Add(i);
                                        _arrayYa.Add(j + 1.5);
                                    }
                                    else
                                    {
                                        _arrayX.Add(i);
                                        _arrayYa.Add(j + 1);
                                    }
                                }
                            }
                            catch { }
                        }
                    }
                    _discretChart.RefreshCurvePoint("D" + j.ToString(), _arrayX, _arrayYa);
                }
                _discretChart.Refresh();
            }
            catch { }
        }

        public double GetMax(double[] _array) 
        {
            double _rez = 100;
            for (int i = 0; i < _array.Length; i++) 
            {
                _rez = Math.Max(_rez, _array[i]);
            }
            return _rez;
        }

        public double GetMin(double[] _array)
        {
            double _rez = -100;
            for (int i = 0; i < _array.Length; i++)
            {
                _rez = Math.Min(_rez, _array[i]);
            }
            return _rez;
        }

        public void DrawOsc()
        {
            AddS1Points(_s1TokChart);
            AddUPoints(_voltageChart);
            AddDiscretPoints(_discretChart);
        }

        public void RefreshAllOsc()
        {
            if (S1ShowCB.Checked)
            {
                RefreshS1Points();
            }
            if (UShowCB.Checked)
            {
                RefreshUPoints();
            }
            if (DiscretShowCB.Checked)
            {
                RefreshDiscretPoints();
            }
        }

        Label[] M1S1Labels;
        Label[] M1S2Labels;
        Label[] M1S3Labels;
        Label[] M1ULabels;
        Label[] M1DLabels;
        Label[] M2S1Labels;
        Label[] M2S2Labels;
        Label[] M2S3Labels;
        Label[] M2ULabels;
        Label[] M2DLabels;
        List<double[]> S1Values;
        List<double[]> S2Values;
        List<double[]> S3Values;
        List<double[]> UValues;
        string[] IChanals;
        string[] UChanals;
        string[] DChanals;
        CheckBox[] CheckBoxes;
        private void OscilloscopeResultForm_Load(object sender, EventArgs e)
        {
            IChanals = new string[] { "Ia","Ib","Ic","In"};
            UChanals = new string[] { "Ua", "Ub", "Uc", "Un" };
            DChanals = new string[] { "�1", "�2",  "�3",  "�4",
                                    "�5",   "�6",  "�7",  "�8",
                                    "�9",   "�10", "�11", "�12",
                                    "�13",  "�14", "�15", "�16",
                                    "�17",  "�18", "�19", "�20",
                                    "�21",  "�22", "�23", "�24"};
            M1S1Labels = new Label[] { Marker1S1Ia, Marker1S1Ib, Marker1S1Ic, Marker1S1In };
            M1ULabels = new Label[] { Marker1Ua, Marker1Ub, Marker1Uc, Marker1Un };
            M1DLabels = new Label[] {   Marker1D1, Marker1D2, Marker1D3, Marker1D4, 
                                        Marker1D5, Marker1D6, Marker1D7, Marker1D8,
                                        Marker1D9, Marker1D10, Marker1D11, Marker1D12,
                                        Marker1D13, Marker1D14, Marker1D15, Marker1D16,
                                        Marker1D17, Marker1D18, Marker1D19, Marker1D20,
                                        Marker1D21, Marker1D22, Marker1D23, Marker1D24,};
            M2S1Labels = new Label[] { Marker2S1Ia, Marker2S1Ib, Marker2S1Ic, Marker2S1In };
            M2ULabels = new Label[] { Marker2Ua, Marker2Ub, Marker2Uc, Marker2Un };
            M2DLabels = new Label[] {   Marker2D1, Marker2D2, Marker2D3, Marker2D4, 
                                        Marker2D5, Marker2D6, Marker2D7, Marker2D8,
                                        Marker2D9, Marker2D10, Marker2D11, Marker2D12,
                                        Marker2D13, Marker2D14, Marker2D15, Marker2D16,
                                        Marker2D17, Marker2D18, Marker2D19, Marker2D20,
                                        Marker2D21, Marker2D22, Marker2D23, Marker2D24,};
            CheckBoxes = new CheckBox[] { S1ShowCB, UShowCB };

            try
            {
                S1Values = new List<double[]>() { _countingList.Currents[0], _countingList.Currents[1], _countingList.Currents[2], _countingList.Currents[3] };
                UValues = new List<double[]>() { _countingList.Voltages[0], _countingList.Voltages[1], _countingList.Voltages[2], _countingList.Voltages[3] };
            }
            catch { }
            this.WindowState = FormWindowState.Maximized;
            panel2.Width = MAINPANEL.Width - 20;
            try
            {
                InitS1ChartLimits(_s1TokChart);
                InitUChartLimits(_voltageChart);
                InitDiscretChartLimits(_discretChart);

                AddS1Curves(_s1TokChart);
                AddUCurves(_voltageChart);
                AddDiscretCurves(_discretChart);

                DrawOsc();

            }
            catch { }
            hScrollBar4.Maximum = (int)_s1TokChart.XMax;
            hScrollBar4.Minimum = (int)_s1TokChart.XMax;

            S1Scroll.Maximum = (int)_s1TokChart.CoordinateYMax;
            S1Scroll.Minimum = (int)_s1TokChart.CoordinateYMax;

            UScroll.Maximum = (int)_voltageChart.CoordinateYMax;
            UScroll.Minimum = (int)_voltageChart.CoordinateYMax;

            _maxCurrentX = _s1TokChart.XMax;
            _S1maxCurrentY = _s1TokChart.CoordinateYMax;
            _S1minCurrentY = _s1TokChart.CoordinateYMin;
            _S1minY = (int)_s1TokChart.CoordinateYMin;
            _UmaxCurrentY = _voltageChart.CoordinateYMax;
            _UminCurrentY = _voltageChart.CoordinateYMin;
            _UminY = (int)_voltageChart.CoordinateYMin;

            _maxX = (int)_maxCurrentX;
            _S1maxY = (int)_S1maxCurrentY;
            _S2maxY = (int)_S2maxCurrentY;
            _S3maxY = (int)_S3maxCurrentY;
            _UmaxY = (int)_UmaxCurrentY;
            _s1TokChart.CoordinateXOrigin = _s1TokChart.XMin + _s1TokChart.XMax / 2;
            _voltageChart.CoordinateXOrigin = _voltageChart.XMin + _voltageChart.XMax / 2;
            _discretChart.CoordinateXOrigin = _discretChart.XMin + _discretChart.XMax / 2;
            _s1TokChart.CoordinateYOrigin = (_s1TokChart.CoordinateYMax + _s1TokChart.CoordinateYMin) / 2;
            _voltageChart.CoordinateYOrigin = (_voltageChart.CoordinateYMax + _voltageChart.CoordinateYMin) / 2;
            Xplus.Enabled = false;
        }

        private void hScrollBar4_Scroll(object sender, ScrollEventArgs e)
        {
            double min = _xStep * (e.NewValue - hScrollBar4.Minimum);
            double max = min + (_xStep * _s1TokChart.GridXTicker);
            _s1TokChart.XMin = min;
            _s1TokChart.XMax = max;
            _voltageChart.XMin = min;
            _voltageChart.XMax = max;
            _discretChart.XMin = min;
            _discretChart.XMax = max;
            _s1TokChart.CoordinateXOrigin = (_s1TokChart.XMin + _s1TokChart.XMax) / 2;
            _voltageChart.CoordinateXOrigin = (_voltageChart.XMin + _voltageChart.XMax) / 2;
            _discretChart.CoordinateXOrigin = (_discretChart.XMin + _discretChart.XMax) / 2;
            RefreshAllOsc();
        }

        private void discretButton_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Button _but = (System.Windows.Forms.Button)sender;

                if (_but.BackColor != Color.White)
                {
                    _but.BackColor = Color.White;
                    _discretChart.UpdateCurveProperty("D" + (Convert.ToInt32(_but.Text) - 1), CurveVisible("D" + (Convert.ToInt32(_but.Text) - 1), Convert.ToInt32(_but.Text) - 1));
                }
                else
                {
                    _but.BackColor = GetColor((Convert.ToInt32(_but.Text) - 1));
                    _discretChart.UpdateCurveProperty("D" + (Convert.ToInt32(_but.Text) - 1), CreateCurve("D" + (Convert.ToInt32(_but.Text) - 1), Convert.ToInt32(_but.Text) - 1));
                }
            }
            catch { }
        }

        private void s1Button_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Button _but = (System.Windows.Forms.Button)sender;

                switch (_but.Text)
                {
                    case "Ia":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                _s1TokChart.UpdateCurveProperty(CountingList.INames[0], CurveVisible(CountingList.INames[0], 0));
                            }
                            else
                            {
                                _but.BackColor = GetColor(0);
                                _s1TokChart.UpdateCurveProperty(CountingList.INames[0], CreateCurve(CountingList.INames[0], 0));
                            }
                            break;
                        }
                    case "Ib":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                _s1TokChart.UpdateCurveProperty(CountingList.INames[1], CurveVisible(CountingList.INames[1], 1));
                            }
                            else
                            {
                                _but.BackColor = GetColor(1);
                                _s1TokChart.UpdateCurveProperty(CountingList.INames[1], CreateCurve(CountingList.INames[1], 1));
                            }
                            break;
                        }
                    case "Ic":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                _s1TokChart.UpdateCurveProperty(CountingList.INames[2], CurveVisible(CountingList.INames[2], 2));
                            }
                            else
                            {
                                _but.BackColor = GetColor(2);
                                _s1TokChart.UpdateCurveProperty(CountingList.INames[2], CreateCurve(CountingList.INames[2], 2));
                            }
                            break;
                        }
                    case "In":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                _s1TokChart.UpdateCurveProperty(CountingList.INames[3], CurveVisible(CountingList.INames[3], 3));
                            }
                            else
                            {
                                _but.BackColor = GetColor(3);
                                _s1TokChart.UpdateCurveProperty(CountingList.INames[3], CreateCurve(CountingList.INames[3], 3));
                            }
                            break;
                        }
                }
            }
            catch { }
        }

        private void uButton_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Button _but = (System.Windows.Forms.Button)sender;

                switch (_but.Text)
                {
                    case "Ua":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                _voltageChart.UpdateCurveProperty(CountingList.UNames[0], CurveVisible(CountingList.UNames[0], 0));
                            }
                            else
                            {
                                _but.BackColor = GetColor(0);
                                _voltageChart.UpdateCurveProperty(CountingList.UNames[0], CreateCurve(CountingList.UNames[0], 0));
                            }
                            break;
                        }
                    case "Ub":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                _voltageChart.UpdateCurveProperty(CountingList.UNames[1], CurveVisible(CountingList.UNames[1], 1));
                            }
                            else
                            {
                                _but.BackColor = GetColor(1);
                                _voltageChart.UpdateCurveProperty(CountingList.UNames[1], CreateCurve(CountingList.UNames[1], 1));
                            }
                            break;
                        }
                    case "Uc":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                _voltageChart.UpdateCurveProperty(CountingList.UNames[2], CurveVisible(CountingList.UNames[2], 2));
                            }
                            else
                            {
                                _but.BackColor = GetColor(2);
                                _voltageChart.UpdateCurveProperty(CountingList.UNames[2], CreateCurve(CountingList.UNames[2], 2));
                            }
                            break;
                        }
                    case "Un":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                _voltageChart.UpdateCurveProperty(CountingList.UNames[3], CurveVisible(CountingList.UNames[3], 3));
                            }
                            else
                            {
                                _but.BackColor = GetColor(3);
                                _voltageChart.UpdateCurveProperty(CountingList.UNames[3], CreateCurve(CountingList.UNames[3], 3));
                            }
                            break;
                        }
                }
            }
            catch { }
        }

        public double CalculateYStep(BEMN_XY_Chart.DAS_Net_XYChart _chart)
        {
            double rez = 0;
            if (_chart.CoordinateYMax > 0 && (_chart.CoordinateYMin >= 0 || _chart.CoordinateYMin <= 0))
            {
                rez = (_chart.CoordinateYMax - _chart.CoordinateYMin) / _chart.GridYTicker;
            }
            else if (_chart.CoordinateYMax < 0 && _chart.CoordinateYMin < 0)
            {
                rez = Math.Abs(_chart.CoordinateYMin - _chart.CoordinateYMax) / _chart.GridYTicker;
            }
            return rez;
        }

        double _xStep = 0;
        public double CalculateXStep(BEMN_XY_Chart.DAS_Net_XYChart _chart)
        {
            double rez = 0;
            if (_chart.XMax > 0 && (_chart.XMin >= 0 || _chart.XMin <= 0))
            {
                rez = (_chart.XMax - _chart.XMin) / _chart.GridXTicker;
            }
            else if (_chart.XMax < 0 && _chart.XMin < 0)
            {
                rez = Math.Abs(_chart.XMin - _chart.XMax) / _chart.GridXTicker;
            }
            return rez;
        }

        double _S1YStep = 0;
        private void S1Scroll_Scroll(object sender, ScrollEventArgs e)
        {
            _s1TokChart.CoordinateYMin = (_S1maxY - (_S1YStep * _s1TokChart.GridYTicker)) - _S1YStep * (e.NewValue - S1Scroll.Minimum);
            _s1TokChart.CoordinateYMax = _S1maxY - _S1YStep * (e.NewValue - S1Scroll.Minimum);
            _s1TokChart.CoordinateYOrigin = (_s1TokChart.CoordinateYMin + _s1TokChart.CoordinateYMax) / 2;
            if (Marker1CB.Checked && MarkerCB.Checked)
            {
                UpdateLinePoints(_s1TokChart, "������1", Marker1TrackBar.Value);
            }
            if (Marker2CB.Checked && MarkerCB.Checked)
            {
                UpdateLinePoints(_s1TokChart, "������2", Marker2TrackBar.Value);
            }
            if (_avaryJournalCheck.Checked)
            {
                for (int i = 0; i < _avaryBeginList.Count; i++)
                {
                    UpdateLinePoints(_s1TokChart, "������ " + i, _avaryBeginList[i]);
                }
            }
            RefreshS1Points();
        }

        double _UYStep = 0;
        private void UScroll_Scroll(object sender, ScrollEventArgs e)
        {
            _voltageChart.CoordinateYMin = (_UmaxY - (_UYStep * _voltageChart.GridYTicker)) - _UYStep * (e.NewValue - UScroll.Minimum);
            _voltageChart.CoordinateYMax = _UmaxY - _UYStep * (e.NewValue - UScroll.Minimum);
            _voltageChart.CoordinateYOrigin = (_voltageChart.CoordinateYMin + _voltageChart.CoordinateYMax) / 2;
            if (Marker1CB.Checked && MarkerCB.Checked)
            {
                UpdateLinePoints(_voltageChart, "������1", Marker1TrackBar.Value);
            }
            if (Marker2CB.Checked && MarkerCB.Checked)
            {
                UpdateLinePoints(_voltageChart, "������2", Marker2TrackBar.Value);
            }
            if (_avaryJournalCheck.Checked)
            {
                for (int i = 0; i < _avaryBeginList.Count; i++)
                {
                    UpdateLinePoints(_voltageChart, "������ " + i, _avaryBeginList[i]);
                }
            }
            RefreshUPoints();
        }

        int S1clicCount = 0;
        private void S1YPlus_Click(object sender, EventArgs e)
        {
            try
            {
                if (S1clicCount < 5)
                {
                    S1Yminus.Enabled = true;
                    S1Scroll.Enabled = true;
                    _s1TokChart.CoordinateYOrigin = (_s1TokChart.CoordinateYMax + _s1TokChart.CoordinateYMin) / 2;
                    _S1maxCurrentY = _S1maxCurrentY / 2;
                    _S1minCurrentY = _S1minCurrentY / 2;
                    int centr = 0;
                    if (_S1maxY > 0 && _S1minY > 0)
                    {
                        centr = (_S1maxY - _S1minY) / 2;
                    }
                    if (_S1maxY < 0 && _S1minY < 0)
                    {
                        centr = (_S1maxY + _S1minY) / 2;
                    }
                    if (_S1maxY > 0 && _S1minY < 0)
                    {
                        centr = (-(_S1maxY - _S1minY)) / 2;
                    }
                    _s1TokChart.CoordinateYMax = _s1TokChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, S1clicCount + 1));
                    _s1TokChart.CoordinateYMin = _s1TokChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, S1clicCount + 1));
                    _S1chartStepY = _s1TokChart.CoordinateYMax - _s1TokChart.CoordinateYMin;
                    _s1TokChart.CoordinateYOrigin = (_s1TokChart.CoordinateYMax + _s1TokChart.CoordinateYMin) / 2;
                    RefreshS1Points();
                    _S1YStep = CalculateYStep(_s1TokChart);
                    S1Scroll.Minimum -= _s1TokChart.GridYTicker * (int)Math.Pow(2, S1clicCount);
                    S1Scroll.Value = (int)((_S1maxY - _s1TokChart.CoordinateYMax) / _S1YStep + S1Scroll.Minimum);
                    S1Scroll.Refresh();
                    S1clicCount++;
                    if (Marker1CB.Checked && MarkerCB.Checked)
                    {
                        UpdateLinePoints(_s1TokChart, "������1", Marker1TrackBar.Value);
                    }
                    if (Marker2CB.Checked && MarkerCB.Checked)
                    {
                        UpdateLinePoints(_s1TokChart, "������2", Marker2TrackBar.Value);
                    }
                    if (_avaryJournalCheck.Checked)
                    {
                        for (int i = 0; i < _avaryBeginList.Count; i++)
                        {
                            UpdateLinePoints(_s1TokChart, "������ " + i, _avaryBeginList[i]);
                        }
                    }
                    if (S1clicCount == 5)
                    {
                        S1YPlus.Enabled = false;
                    }
                }
            }
            catch { }
        }

        private void S1Yminus_Click(object sender, EventArgs e)
        {
            try
            {
                if (S1Scroll.Minimum != _S1maxY && S1Scroll.Maximum != _S1minY)
                {
                    S1YPlus.Enabled = true;
                    S1clicCount--;
                    _S1maxCurrentY = _S1maxCurrentY * 2;
                    _S1minCurrentY = _S1minCurrentY * 2;
                    int centr = 0;
                    if (_S1maxY > 0 && _S1minY > 0)
                    {
                        centr = (_S1maxY - _S1minY) / 2;
                    }
                    if (_S1maxY < 0 && _S1minY < 0)
                    {
                        centr = (_S1maxY + _S1minY) / 2;
                    }
                    if (_S1maxY > 0 && _S1minY < 0)
                    {
                        centr = (-(_S1maxY - _S1minY)) / 2;
                    }
                    if (_s1TokChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, S1clicCount)) <= _S1maxY)
                    {
                        if (_s1TokChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, S1clicCount)) >= _S1minY)
                        {
                            _s1TokChart.CoordinateYMax = _s1TokChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, S1clicCount));//*= 2;
                            _s1TokChart.CoordinateYMin = _s1TokChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, S1clicCount));//*= 2;
                        }
                        else
                        {
                            _s1TokChart.CoordinateYMin = _S1minY;
                            _s1TokChart.CoordinateYMax = _s1TokChart.CoordinateYMin + (Math.Abs(_S1maxCurrentY) + Math.Abs(_S1minCurrentY));
                        }
                    }
                    else
                    {
                        _s1TokChart.CoordinateYMax = _S1maxY;
                        _s1TokChart.CoordinateYMin = _s1TokChart.CoordinateYMax - (Math.Abs(_S1maxCurrentY) + Math.Abs(_S1minCurrentY));
                    }
                    _S1chartStepY = _s1TokChart.CoordinateYMax - _s1TokChart.CoordinateYMin;
                    _s1TokChart.CoordinateYOrigin = (_s1TokChart.CoordinateYMax + _s1TokChart.CoordinateYMin) / 2;
                    RefreshS1Points();
                    _S1YStep = CalculateYStep(_s1TokChart);

                    //if (S1clicCount != 0)
                    //{
                    try
                    {
                        S1Scroll.Minimum += _s1TokChart.GridYTicker * (int)Math.Pow(2, S1clicCount);
                        S1Scroll.Value = (int)((_S1maxY - _s1TokChart.CoordinateYMax) / _S1YStep + S1Scroll.Minimum);
                        S1Scroll.Refresh();
                    }
                    catch { }
                    //}
                    if (Marker1CB.Checked && MarkerCB.Checked)
                    {
                        UpdateLinePoints(_s1TokChart, "������1", Marker1TrackBar.Value);
                    }
                    if (Marker2CB.Checked && MarkerCB.Checked)
                    {
                        UpdateLinePoints(_s1TokChart, "������2", Marker2TrackBar.Value);
                    }
                    if (_avaryJournalCheck.Checked)
                    {
                        for (int i = 0; i < _avaryBeginList.Count; i++)
                        {
                            UpdateLinePoints(_s1TokChart, "������ " + i, _avaryBeginList[i]);
                        }
                    }
                    if (S1clicCount == 0)
                    {
                        S1Yminus.Enabled = false;
                        S1Scroll.Enabled = false;
                    }
                }
            }
            catch { }
        }

        int UclicCount = 0;
        private void UYPlus_Click(object sender, EventArgs e)
        {
            try
            {
                if (UclicCount < 5)
                {
                    UYminus.Enabled = true;
                    UScroll.Enabled = true;

                    _voltageChart.CoordinateYOrigin = (_voltageChart.CoordinateYMax + _voltageChart.CoordinateYMin) / 2;
                    _UmaxCurrentY = _UmaxCurrentY / 2;
                    _UminCurrentY = _UminCurrentY / 2;
                    double centr = 0;
                    if (_UmaxY > 0 && _UminY > 0)
                    {
                        centr = (_UmaxY - _UminY) / 2;
                    }
                    if (_UmaxY < 0 && _UminY < 0)
                    {
                        centr = (_UmaxY + _UminY) / 2;
                    }
                    if (_UmaxY > 0 && _UminY < 0)
                    {
                        centr = (-(_UmaxY - _UminY)) / 2;
                    }
                    _voltageChart.CoordinateYMax = _voltageChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, UclicCount + 1));
                    _voltageChart.CoordinateYMin = _voltageChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, UclicCount + 1));
                    _UchartStepY = _voltageChart.CoordinateYMax - _voltageChart.CoordinateYMin;
                    _voltageChart.CoordinateYOrigin = (_voltageChart.CoordinateYMax + _voltageChart.CoordinateYMin) / 2;
                    RefreshUPoints();
                    _UYStep = CalculateYStep(_voltageChart);

                    UScroll.Minimum -= _voltageChart.GridYTicker * (int)Math.Pow(2, UclicCount);
                    UclicCount++;
                    UScroll.Value = (int)((_UmaxY - _voltageChart.CoordinateYMax) / _UYStep + UScroll.Minimum);
                    if (Marker1CB.Checked && MarkerCB.Checked)
                    {
                        UpdateLinePoints(_voltageChart, "������1", Marker1TrackBar.Value);
                    }
                    if (Marker2CB.Checked && MarkerCB.Checked)
                    {
                        UpdateLinePoints(_voltageChart, "������2", Marker2TrackBar.Value);
                    }
                    if (_avaryJournalCheck.Checked)
                    {
                        for (int i = 0; i < _avaryBeginList.Count; i++)
                        {
                            UpdateLinePoints(_voltageChart, "������ " + i, _avaryBeginList[i]);
                        }
                    }
                    if (UclicCount == 5)
                    {
                        UYPlus.Enabled = false;
                    }
                }
            }
            catch { }
        }

        private void UYminus_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    if (UScroll.Minimum != _UmaxY && UScroll.Maximum != _UminY)
            //    {
            //        UYPlus.Enabled = true;
            //        UclicCount--;
                    
            //        _UmaxCurrentY = _UmaxCurrentY * 2;
            //        _UminCurrentY = _UminCurrentY * 2;
            //        double centr = 0;
            //        if (_UmaxY > 0 && _UminY > 0)
            //        {
            //            centr = (_UmaxY - _UminY) / 2;
            //        }
            //        if (_UmaxY < 0 && _UminY < 0)
            //        {
            //            centr = (_UmaxY + _UminY) / 2;
            //        }
            //        if (_UmaxY > 0 && _UminY < 0)
            //        {
            //            centr = (-(_UmaxY - _UminY)) / 2;
            //        }
            //        if (_voltageChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, UclicCount)) <= _UmaxY)
            //        {
            //            if (_voltageChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, UclicCount)) >= _UminY)
            //            {
            //                _voltageChart.CoordinateYMax = _voltageChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, UclicCount));//*= 2;
            //                _voltageChart.CoordinateYMin = _voltageChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, UclicCount));//*= 2;
            //            }
            //            else
            //            {
            //                _voltageChart.CoordinateYMin = _UminY;
            //                _voltageChart.CoordinateYMax = _voltageChart.CoordinateYMin + (Math.Abs(_UmaxCurrentY) + Math.Abs(UclicCount));
            //            }
            //        }
            //        else
            //        {
            //            _voltageChart.CoordinateYMax = _UmaxY;
            //            _voltageChart.CoordinateYMin = _voltageChart.CoordinateYMax - (Math.Abs(_UmaxCurrentY) + Math.Abs(UclicCount));
            //        }
            //        _UchartStepY = _voltageChart.CoordinateYMax - _voltageChart.CoordinateYMin;
            //        _voltageChart.CoordinateYOrigin = (_voltageChart.CoordinateYMax + _voltageChart.CoordinateYMin) / 2;
            //        RefreshUPoints();
            //        _UYStep = CalculateYStep(_voltageChart);

            //        //if (UclicCount != 0)
            //        //{
            //            UScroll.Minimum += _voltageChart.GridYTicker * (int)Math.Pow(2, UclicCount);
            //            UScroll.Value = (int)((_UmaxY - _voltageChart.CoordinateYMax) / _UYStep + UScroll.Minimum);
            //        //}
            //        if (Marker1CB.Checked && MarkerCB.Checked)
            //        {
            //            UpdateLinePoints(_voltageChart, "������1", Marker1TrackBar.Value);
            //        }
            //        if (Marker2CB.Checked && MarkerCB.Checked)
            //        {
            //            UpdateLinePoints(_voltageChart, "������2", Marker2TrackBar.Value);
            //        }
            //        if (_avaryJournalCheck.Checked)
            //        {
            //            for (int i = 0; i < _avaryBeginList.Count; i++)
            //            {
            //                UpdateLinePoints(_voltageChart, "������ " + i, _avaryBeginList[i]);
            //            }
            //        }
            //        if (UclicCount == 0)
            //        {
            //            UYminus.Enabled = false;
            //            UScroll.Visible = false;
            //        }
            //    }
            //}
            //catch { }

            try
            {
                if (UScroll.Minimum != _UmaxY && UScroll.Maximum != _UminY)
                {
                    UYPlus.Enabled = true;
                    UclicCount--;
                    _UmaxCurrentY = _UmaxCurrentY * 2;
                    _UminCurrentY = _UminCurrentY * 2;
                    int centr = 0;
                    if (_UmaxY > 0 && _UminY > 0)
                    {
                        centr = (_UmaxY - _UminY) / 2;
                    }
                    if (_UmaxY < 0 && _UminY < 0)
                    {
                        centr = (_UmaxY + _UminY) / 2;
                    }
                    if (_UmaxY > 0 && _UminY < 0)
                    {
                        centr = (-(_UmaxY - _UminY)) / 2;
                    }
                    if (_voltageChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, UclicCount)) <= _UmaxY)
                    {
                        if (_voltageChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, UclicCount)) >= _UminY)
                        {
                            _voltageChart.CoordinateYMax = _voltageChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, UclicCount));//*= 2;
                            _voltageChart.CoordinateYMin = _voltageChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, UclicCount));//*= 2;
                        }
                        else
                        {
                            _voltageChart.CoordinateYMin = _UminY;
                            _voltageChart.CoordinateYMax = _voltageChart.CoordinateYMin + (Math.Abs(_UmaxCurrentY) + Math.Abs(_UminCurrentY));
                        }
                    }
                    else
                    {
                        _voltageChart.CoordinateYMax = _UmaxY;
                        _voltageChart.CoordinateYMin = _voltageChart.CoordinateYMax - (Math.Abs(_UmaxCurrentY) + Math.Abs(_UminCurrentY));
                    }
                    _UchartStepY = _voltageChart.CoordinateYMax - _voltageChart.CoordinateYMin;
                    _voltageChart.CoordinateYOrigin = (_voltageChart.CoordinateYMax + _voltageChart.CoordinateYMin) / 2;
                    RefreshUPoints();
                    _UYStep = CalculateYStep(_voltageChart);

                    //if (S1clicCount != 0)
                    //{
                    try
                    {
                        UScroll.Minimum += _voltageChart.GridYTicker * (int)Math.Pow(2, UclicCount);
                        UScroll.Value = (int)((_UmaxY - _voltageChart.CoordinateYMax) / _UYStep + UScroll.Minimum);
                        UScroll.Refresh();
                    }
                    catch { }
                    //}
                    if (Marker1CB.Checked && MarkerCB.Checked)
                    {
                        UpdateLinePoints(_s1TokChart, "������1", Marker1TrackBar.Value);
                    }
                    if (Marker2CB.Checked && MarkerCB.Checked)
                    {
                        UpdateLinePoints(_s1TokChart, "������2", Marker2TrackBar.Value);
                    }
                    if (_avaryJournalCheck.Checked)
                    {
                        for (int i = 0; i < _avaryBeginList.Count; i++)
                        {
                            UpdateLinePoints(_voltageChart, "������ " + i, _avaryBeginList[i]);
                        }
                    }
                    if (UclicCount == 0)
                    {
                        UYminus.Enabled = false;
                        UScroll.Enabled = false;
                    }
                }
            }
            catch { }
        }

        int XclicCount = 0;
        private void Xplus_Click(object sender, EventArgs e)
        {
            try
            {
                Xminus.Enabled = true;
                XclicCount--;
                hScrollBar4.Minimum += _s1TokChart.GridXTicker * (int)Math.Pow(2, XclicCount);//(int)(_maxX / (_maxCurrentX * 2));

                int centr = _maxX / 2;
                double max = _s1TokChart.CoordinateXOrigin + Math.Abs(centr / (int)Math.Pow(2, XclicCount));
                double min = _s1TokChart.CoordinateXOrigin - Math.Abs(centr / (int)Math.Pow(2, XclicCount));

                if (min < 0)
                {
                    max = Math.Abs(min) + Math.Abs(max);
                    min = 0;
                }
                if (max > _maxX)
                {
                    min = _maxX - (max - min);
                    max = _maxX;
                }

                _s1TokChart.XMax = max;
                _s1TokChart.XMin = min;
                _voltageChart.XMax = max;
                _voltageChart.XMin = min;
                _discretChart.XMax = max;
                _discretChart.XMin = min;

                if (XclicCount == 0)
                {
                    Xplus.Enabled = false;
                    hScrollBar4.Visible = false;
                }
                try
                {

                    _s1TokChart.CoordinateXOrigin = (_s1TokChart.XMin + _s1TokChart.XMax) / 2;
                    _voltageChart.CoordinateXOrigin = (_voltageChart.XMin + _voltageChart.XMax) / 2;
                    _discretChart.CoordinateXOrigin = (_discretChart.XMin + _discretChart.XMax) / 2;
                }
                catch { }
                _maxCurrentX = _s1TokChart.XMax;
                _xStep = CalculateXStep(_s1TokChart);
                try
                {
                    hScrollBar4.Value = (int)(_s1TokChart.XMin / _xStep + hScrollBar4.Minimum);//= hScrollBar4.Minimum;
                }
                catch { }
                RefreshAllOsc();
            }
            catch { }
        }

        private void Xminus_Click(object sender, EventArgs e)
        {
            try
            {
                hScrollBar4.Visible = true;
                hScrollBar4.Minimum -= _s1TokChart.GridXTicker * (int)Math.Pow(2, XclicCount);
                XclicCount++;

                int centr = _maxX / 2;
                double max = _s1TokChart.CoordinateXOrigin + Math.Abs(centr / (int)Math.Pow(2, XclicCount));
                double min = _s1TokChart.CoordinateXOrigin - Math.Abs(centr / (int)Math.Pow(2, XclicCount));

                if (min < 0)
                {
                    max = Math.Abs(min) + Math.Abs(max);
                    min = 0;
                }
                if (max > _maxX)
                {
                    min = _maxX - (max - min);
                    max = _maxX;
                }

                _s1TokChart.XMax = max;
                _s1TokChart.XMin = min;
                _voltageChart.XMax = max;
                _voltageChart.XMin = min;
                _discretChart.XMax = max;
                _discretChart.XMin = min;

                Xplus.Enabled = true;
                _s1TokChart.CoordinateXOrigin = (_s1TokChart.XMin + _s1TokChart.XMax) / 2;
                _voltageChart.CoordinateXOrigin = (_voltageChart.XMin + _voltageChart.XMax) / 2;
                _discretChart.CoordinateXOrigin = (_discretChart.XMin + _discretChart.XMax) / 2;
                _maxCurrentX = _s1TokChart.XMax;

                _xStep = CalculateXStep(_s1TokChart);
                if (_xStep <= 10)
                {
                    Xminus.Enabled = false;
                }
                try
                {
                    hScrollBar4.Value = (int)(_s1TokChart.XMin / _xStep + hScrollBar4.Minimum);
                }
                catch { }
                RefreshAllOsc();
            }
            catch { }
        }

        private void _avaryOscCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (_avaryOscCheck.Checked)
            {
                for (int i = 0; i < _avaryBeginList.Count; i++)
                {
                    AddLineCurves(_s1TokChart, "������ " + i, Color.Aqua, 2);
                    AddLinePoints(_s1TokChart, "������ " + i, _avaryBeginList[i]);
                    AddLineCurves(_voltageChart, "������ " + i, Color.Aqua, 2);
                    AddLinePoints(_voltageChart, "������ " + i, _avaryBeginList[i]);
                    AddLineCurves(_discretChart, "������ " + i, Color.Aqua, 2);
                    AddLinePoints(_discretChart, "������ " + i, _avaryBeginList[i]);
                }
            }
            else
            {
                for (int i = 0; i < _avaryBeginList.Count; i++)
                {
                    _s1TokChart.ResetCurve("������ " + i);
                    _voltageChart.ResetCurve("������ " + i);
                    _discretChart.ResetCurve("������ " + i);
                }
            }
        }

        private void S1ShowCB_CheckedChanged(object sender, EventArgs e)
        {
            if (S1ShowCB.Checked)
            {
                tableLayoutPanel13.Visible = true;
                if (Marker1CB.Checked)
                {
                    LabelResults(M1S1Labels, IChanals, S1Values, Marker1TrackBar.Value, 0,"A");
                }
                if (Marker2CB.Checked)
                {
                    LabelResults(M2S1Labels, IChanals, S1Values, Marker2TrackBar.Value, 0,"A");
                }
            }
            else 
            {
                tableLayoutPanel13.Visible = false;
                LabelReset(M1S1Labels);

                LabelReset(M2S1Labels);

                panel3.Update();
            }
            RefreshAllOsc();
        }

        private void UShowCB_CheckedChanged(object sender, EventArgs e)
        {
            if (UShowCB.Checked)
            {
                tableLayoutPanel16.Visible = true;
                splitter4.Visible = true;
                if (Marker1CB.Checked)
                {
                    LabelResults(M1ULabels, UChanals, UValues, Marker1TrackBar.Value, 1,"B");
                }
                if (Marker2CB.Checked)
                {
                    LabelResults(M2ULabels, UChanals, UValues, Marker2TrackBar.Value, 1,"B");
                }
            }
            else
            {
                splitter4.SplitPosition = splitter4.MinExtra;
                tableLayoutPanel16.Visible = false;
                splitter4.Visible = false;
                LabelReset(M1ULabels);

                LabelReset(M2ULabels);

                panel3.Update();
            }
            RefreshAllOsc();
        }

        private void DiscretShowCB_CheckedChanged(object sender, EventArgs e)
        {
            if (DiscretShowCB.Checked)
            {
                tableLayoutPanel2.Visible = true;
                splitter5.Visible = true;
                if (Marker1CB.Checked)
                {
                    LabelResults(M1DLabels, DChanals, _discrets, Marker1TrackBar.Value);
                }
                if (Marker2CB.Checked)
                {
                    LabelResults(M2DLabels, DChanals, _discrets, Marker2TrackBar.Value);
                }
            }
            else
            {
                splitter5.SplitPosition = splitter5.MinExtra;
                tableLayoutPanel2.Visible = false;
                splitter5.Visible = false;
                LabelReset(M1DLabels);

                LabelReset(M2DLabels);

                panel3.Update();
            }
            RefreshAllOsc();
        }

        private List<ToolTip> _tolTips = new List<ToolTip>();
        private void _Chart_MouseClick(object sender, MouseEventArgs e)
        {
            if (FlagCB.Checked)
            {
                try
                {
                    BEMN_XY_Chart.DAS_Net_XYChart _chart = (BEMN_XY_Chart.DAS_Net_XYChart)sender;

                    if (e.Button == MouseButtons.Left)
                    {
                        toolTip1 = new ToolTip();
                        toolTip1.ShowAlways = true;
                        double _length = Math.Abs(_chart.XMax) + Math.Abs(_chart.XMin);
                        double x = (_length / _chart.Width * e.X) + _chart.XMin;
                        if (e.X < Math.Abs(_chart.XMin) && _chart.XMin < 0)
                        {
                            x = (Math.Round(_length / _chart.Width * (e.X + 1), 2) + _chart.XMin);
                        }
                        toolTip1.Show(Math.Round(x).ToString() + " +/-" + Math.Round(_length / _chart.Width).ToString() + "��", _chart, e.X, e.Y);
                        _tolTips.Add(toolTip1);
                    }
                    else
                    {
                        for (int i = 0; i < _tolTips.Count; i++)
                            _tolTips[i].RemoveAll();
                    }
                }
                catch { }
            }
        }

        private void MarkerCB_CheckedChanged(object sender, EventArgs e)
        {
            if (MarkerCB.Checked)
            {
                MarkersTable.Visible = true;
                MarkersTable.Update();
                Marker1TrackBar.Maximum = _maxX;
                Marker1TrackBar.Update();
                Marker2TrackBar.Maximum = _maxX;
                Marker2TrackBar.Update();
                MAINTABLE.ColumnStyles[1].Width = 250;
                MAINTABLE.Update();
                _s1TokChart.Update();
                _voltageChart.Update();
                _discretChart.Update();
            }
            else
            {
                MarkersTable.Visible = false;
                MarkersTable.Update();
                Marker1CB.Checked = false;
                Marker2CB.Checked = false;
                _s1TokChart.ResetCurve("������1");
                _s1TokChart.ResetCurve("������2");
                _voltageChart.ResetCurve("������1");
                _voltageChart.ResetCurve("������2");
                _discretChart.ResetCurve("������1");
                _discretChart.ResetCurve("������2");
                MAINTABLE.ColumnStyles[1].Width = 0;
                MAINTABLE.Update();
            }
        }

        private void LabelResults(Label[] labels, string[] chanels, List<double[]> values, int index, int box, string measure)
        {
            try
            {
                if (CheckBoxes[box].Checked)
                {
                    for (int i = 0; i < labels.Length; i++)
                    {
                        labels[i].Text = chanels[i] + " = " + ValuesConverterCommon.Analog.DoubleToString3(values[i][index])+measure ;
                        labels[i].Update();
                    }
                }
            }
            catch { }
        }

        private void LabelReset(Label[] labels)
        {
            try
            {
                for (int i = 0; i < labels.Length; i++)
                {
                    labels[i].Text = " ";
                    labels[i].Update();
                }
            }
            catch { }
        }

        private void LabelResults(Label[] labels, string[] chanels, List<char[]> values, int index)
        {
            try
            {
                
                if (DiscretShowCB.Checked)
                {
                    for (int i = 0; i < values.Count; i++)
                    {
                        
                            labels[i].Text = chanels[i] + " = " + values[i][index];
                        }
                }
            }
            catch { }
        }

        public int BoolToInt(bool val) 
        {
            if (val)
                return 1;
            else
                return 0;
        }

        private void Marker1TrackBar_Scroll(object sender, EventArgs e)
        {
            if (Marker1CB.Checked)
            {
                LabelResults(M1S1Labels, IChanals, S1Values, Marker1TrackBar.Value,0,"A");

           //     LabelResults(M1S2Labels, IChanals, S2Values, Marker1TrackBar.Value,1);

           //     LabelResults(M1S3Labels, IChanals, S3Values, Marker1TrackBar.Value,2);

                LabelResults(M1ULabels, UChanals, UValues, Marker1TrackBar.Value,1,"B");
                try
                {
                    LabelResults(M1DLabels, DChanals, _discrets, Marker1TrackBar.Value);
                }
                catch { }
                panel3.Update();

                UpdateLinePoints(_s1TokChart, "������1", Marker1TrackBar.Value);
                //_s1TokChart.Update();
                _s1TokChart.Refresh();
                UpdateLinePoints(_voltageChart, "������1", Marker1TrackBar.Value);
                //_voltageChart.Update();
                _voltageChart.Refresh();
                UpdateLinePoints(_discretChart, "������1", Marker1TrackBar.Value);
                //_discretChart.Update();
                _discretChart.Refresh();
                TimeMarker1.Text = Marker1TrackBar.Value + " ��.";
                //TimeMarker1.Update();
                TimeMarker1.Refresh();
            }
            if (Marker1CB.Checked && Marker2CB.Checked)
            {
                DeltaLabel.Text = Math.Abs(Marker1TrackBar.Value - Marker2TrackBar.Value) + " ��.";
                DeltaLabel.Update();
            }
        }

        private void Marker2TrackBar_Scroll(object sender, EventArgs e)
        {
            if (Marker2CB.Checked)
            {
                LabelResults(M2S1Labels, IChanals, S1Values, Marker2TrackBar.Value,0,"A");

              //  LabelResults(M2S2Labels, IChanals, S2Values, Marker2TrackBar.Value,1);

              //  LabelResults(M2S3Labels, IChanals, S3Values, Marker2TrackBar.Value,2);

                LabelResults(M2ULabels, UChanals, UValues, Marker2TrackBar.Value,1,"B");
                try
                {
                    LabelResults(M2DLabels, DChanals, _discrets, Marker2TrackBar.Value);
                }
                catch { }
                panel3.Update();

                UpdateLinePoints(_s1TokChart, "������2", Marker2TrackBar.Value);
                _s1TokChart.Update();
                UpdateLinePoints(_voltageChart, "������2", Marker2TrackBar.Value);
                _voltageChart.Update();
                UpdateLinePoints(_discretChart, "������2", Marker2TrackBar.Value);
                _discretChart.Update();
                TimeMarker2.Text = Marker2TrackBar.Value + " ��.";
                TimeMarker2.Update();
            }
            if (Marker1CB.Checked && Marker2CB.Checked)
            {
                DeltaLabel.Text = Math.Abs(Marker1TrackBar.Value - Marker2TrackBar.Value) + " ��.";
                DeltaLabel.Update();
            }
        }

        private void Marker1CB_CheckedChanged(object sender, EventArgs e)
        {
            if (Marker1CB.Checked)
            {
                LabelResults(M1S1Labels, IChanals, S1Values, Marker1TrackBar.Value,0,"A");

               // LabelResults(M1S2Labels, IChanals, S2Values, Marker1TrackBar.Value,1);

               // LabelResults(M1S3Labels, IChanals, S3Values, Marker1TrackBar.Value,2);

                LabelResults(M1ULabels, UChanals, UValues, Marker1TrackBar.Value,1,"B");

                try
                {
                    LabelResults(M1DLabels, DChanals, _discrets, Marker1TrackBar.Value);
                }
                catch { }
                AddLineCurves(_s1TokChart, "������1", Color.Black, 1);
                AddLineCurves(_voltageChart, "������1", Color.Black, 1);
                AddLineCurves(_discretChart, "������1", Color.Black, 1);
                AddLinePoints(_s1TokChart, "������1", Marker1TrackBar.Value);
                AddLinePoints(_voltageChart, "������1", Marker1TrackBar.Value);
                AddLinePoints(_discretChart, "������1", Marker1TrackBar.Value);
                TimeMarker1.Text = Marker1TrackBar.Value + " ��.";
                TimeMarker1.Update();
                if (Marker2CB.Checked)
                {
                    DeltaLabel.Text = Math.Abs(Marker1TrackBar.Value - Marker2TrackBar.Value) + " ��.";
                    DeltaLabel.Update();
                }
            }
            else 
            {
                LabelReset(M1S1Labels);

                LabelReset(M1S2Labels);

                LabelReset(M1S3Labels);

                LabelReset(M1ULabels);

                LabelReset(M1DLabels);

                _s1TokChart.ResetCurve("������1");
                _voltageChart.ResetCurve("������1");
                _discretChart.ResetCurve("������1");
                TimeMarker1.Text = "";
                TimeMarker1.Update();
                DeltaLabel.Text = "";
                DeltaLabel.Update();
            }
        }

        private void Marker2CB_CheckedChanged(object sender, EventArgs e)
        {
            if (Marker2CB.Checked)
            {
                LabelResults(M2S1Labels, IChanals, S1Values, Marker2TrackBar.Value,0,"A");

              //  LabelResults(M2S2Labels, IChanals, S2Values, Marker2TrackBar.Value,1);

             //   LabelResults(M2S3Labels, IChanals, S3Values, Marker2TrackBar.Value,2);

                LabelResults(M2ULabels, UChanals, UValues, Marker2TrackBar.Value,1,"B");
                try
                {
                    LabelResults(M2DLabels, DChanals, _discrets, Marker2TrackBar.Value);
                }
                catch { }
                AddLineCurves(_s1TokChart, "������2", Color.White, 1);
                AddLineCurves(_voltageChart, "������2", Color.White, 1);
                AddLineCurves(_discretChart, "������2", Color.White, 1);
                AddLinePoints(_s1TokChart, "������2", Marker2TrackBar.Value);
                AddLinePoints(_voltageChart, "������2", Marker2TrackBar.Value);
                AddLinePoints(_discretChart,"������2", Marker2TrackBar.Value);
                TimeMarker2.Text = Marker2TrackBar.Value + " ��.";
                TimeMarker2.Update();
                if (Marker1CB.Checked) 
                {
                    DeltaLabel.Text = Math.Abs(Marker1TrackBar.Value - Marker2TrackBar.Value) + " ��.";
                    DeltaLabel.Update();
                }
            }
            else
            {
                LabelReset(M2S1Labels);

                LabelReset(M2S2Labels);

                LabelReset(M2S3Labels);

                LabelReset(M2ULabels);

                LabelReset(M2DLabels);

                _s1TokChart.ResetCurve("������2");
                _voltageChart.ResetCurve("������2");
                _discretChart.ResetCurve("������2");
                TimeMarker2.Text = "";
                TimeMarker2.Update();
                DeltaLabel.Text = "";
                DeltaLabel.Update();
            }
        }

        private void _avaryJournalCheck_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}