namespace BEMN.TZL
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle77 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle78 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle79 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle76 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle80 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle85 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle91 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle92 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle93 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle94 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle100 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle101 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle95 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle96 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle97 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle98 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle99 = new System.Windows.Forms.DataGridViewCellStyle();
            this.apv_blocking = new System.Windows.Forms.ComboBox();
            this.apv_conf = new System.Windows.Forms.ComboBox();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._TT_Box = new System.Windows.Forms.MaskedTextBox();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._processLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._exchangeProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this._readConfigBut = new System.Windows.Forms.Button();
            this.label56 = new System.Windows.Forms.Label();
            this.apv_time_4krat = new System.Windows.Forms.MaskedTextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.apv_time_3krat = new System.Windows.Forms.MaskedTextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.apv_time_2krat = new System.Windows.Forms.MaskedTextBox();
            this._tabControl = new System.Windows.Forms.TabControl();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetSetPointStruct = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToHtmlItem = new System.Windows.Forms.ToolStripMenuItem();
            this._inSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._logicSignalsDataGrid1 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this._logicSignalsDataGrid2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this._logicSignalsDataGrid3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this._logicSignalsDataGrid4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this._logicSignalsDataGrid5 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this._logicSignalsDataGrid6 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this._logicSignalsDataGrid7 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this._logicSignalsDataGrid8 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.oscGroupBox = new System.Windows.Forms.GroupBox();
            this.oscFix = new System.Windows.Forms.ComboBox();
            this.oscPercent = new System.Windows.Forms.MaskedTextBox();
            this.oscLength = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this._oscKonfComb = new System.Windows.Forms.ComboBox();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this._keysDataGrid = new System.Windows.Forms.DataGridView();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._HUD_Box = new System.Windows.Forms.MaskedTextBox();
            this._OMP_TypeBox = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._manageSignalsSDTU_Combo = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this._manageSignalsExternalCombo = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this._manageSignalsKeyCombo = new System.Windows.Forms.ComboBox();
            this.label45 = new System.Windows.Forms.Label();
            this._manageSignalsButtonCombo = new System.Windows.Forms.ComboBox();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._Sost_ON_OFF = new System.Windows.Forms.ComboBox();
            this._Sost_ON_OFF_Label = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this._switcherDurationBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherTokBox = new System.Windows.Forms.MaskedTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this._switcherTimeBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherImpulseBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherBlockCombo = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this._switcherErrorCombo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this._switcherStateOnCombo = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this._switcherStateOffCombo = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._exBlockSdtuCombo = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this._constraintGroupCombo = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this._signalizationCombo = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this._extOffCombo = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this._extOnCombo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this._keyOnCombo = new System.Windows.Forms.ComboBox();
            this._keyOffCombo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._TN_typeCombo = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this._TNNP_dispepairCombo = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this._TN_dispepairCombo = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._TNNP_Box = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this._TN_Box = new System.Windows.Forms.MaskedTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._TT_typeCombo = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._maxTok_Box = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._TTNP_Box = new System.Windows.Forms.MaskedTextBox();
            this._outputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.VLSTabControl = new System.Windows.Forms.TabControl();
            this.VLS1 = new System.Windows.Forms.TabPage();
            this._outputLogicCheckList1 = new System.Windows.Forms.CheckedListBox();
            this.VLS2 = new System.Windows.Forms.TabPage();
            this._outputLogicCheckList2 = new System.Windows.Forms.CheckedListBox();
            this.VLS3 = new System.Windows.Forms.TabPage();
            this._outputLogicCheckList3 = new System.Windows.Forms.CheckedListBox();
            this.VLS4 = new System.Windows.Forms.TabPage();
            this._outputLogicCheckList4 = new System.Windows.Forms.CheckedListBox();
            this.VLS5 = new System.Windows.Forms.TabPage();
            this._outputLogicCheckList5 = new System.Windows.Forms.CheckedListBox();
            this.VLS6 = new System.Windows.Forms.TabPage();
            this._outputLogicCheckList6 = new System.Windows.Forms.CheckedListBox();
            this.VLS7 = new System.Windows.Forms.TabPage();
            this._outputLogicCheckList7 = new System.Windows.Forms.CheckedListBox();
            this.VLS8 = new System.Windows.Forms.TabPage();
            this._outputLogicCheckList8 = new System.Windows.Forms.CheckedListBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this._releDispepairBox = new System.Windows.Forms.MaskedTextBox();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._dispepairCheckList = new System.Windows.Forms.CheckedListBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._outputIndicatorsGrid = new System.Windows.Forms.DataGridView();
            this._outIndNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outIndTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndResetCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndAlarmCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndSystemCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releImpulseCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDefensePage = new System.Windows.Forms.TabPage();
            this._externalDefenseGrid = new System.Windows.Forms.DataGridView();
            this._extDefNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefBlockingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefWorkingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefWorkingTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefReturnCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAPVreturnCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefReturnNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefReturnTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefOSCcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefUROVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAPVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAVRcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefResetcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._automaticPage = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.avr_disconnection = new System.Windows.Forms.MaskedTextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.avr_time_return = new System.Windows.Forms.MaskedTextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.avr_time_abrasion = new System.Windows.Forms.MaskedTextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.avr_return = new System.Windows.Forms.ComboBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.avr_abrasion = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this.avr_reset_blocking = new System.Windows.Forms.ComboBox();
            this.avr_permit_reset_switch = new System.Windows.Forms.CheckBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.avr_abrasion_switch = new System.Windows.Forms.CheckBox();
            this.avr_supply_off = new System.Windows.Forms.CheckBox();
            this.avr_self_off = new System.Windows.Forms.CheckBox();
            this.avr_switch_off = new System.Windows.Forms.CheckBox();
            this.avr_blocking = new System.Windows.Forms.ComboBox();
            this.avr_start = new System.Windows.Forms.ComboBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.lzsh_confV114 = new System.Windows.Forms.ComboBox();
            this.label57 = new System.Windows.Forms.Label();
            this.lzsh_constraint = new System.Windows.Forms.MaskedTextBox();
            this.lzsh_conf = new System.Windows.Forms.ComboBox();
            this.label48 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.label54 = new System.Windows.Forms.Label();
            this.apv_time_1krat = new System.Windows.Forms.MaskedTextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.apv_time_ready = new System.Windows.Forms.MaskedTextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.apv_time_blocking = new System.Windows.Forms.MaskedTextBox();
            this.apv_self_off = new System.Windows.Forms.ComboBox();
            this._defendTabPage = new System.Windows.Forms.TabPage();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this._tokDefendTabPage = new System.Windows.Forms.TabPage();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this._tokDefenseInbox = new System.Windows.Forms.MaskedTextBox();
            this.label24 = new System.Windows.Forms.Label();
            this._tokDefenseI2box = new System.Windows.Forms.MaskedTextBox();
            this.label23 = new System.Windows.Forms.Label();
            this._tokDefenseI0box = new System.Windows.Forms.MaskedTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this._tokDefenseIbox = new System.Windows.Forms.MaskedTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this._tokDefenseGrid4 = new System.Windows.Forms.DataGridView();
            this._tokDefense4NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4ModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense4BlockNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense4UpuskCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4PuskConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4WorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4WorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4SpeedupCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4SpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4OSCCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4OSCv11Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense4UROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4APVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4AVRCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseGrid3 = new System.Windows.Forms.DataGridView();
            this._tokDefense3NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3ModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3BlockingNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3UpuskCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3PuskConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3DirectionCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3BlockingExistCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3ParameterCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3WorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3FeatureCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3WorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3SpeedupCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3SpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3OSCCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3OSCv11Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3UROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3APVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3AVRCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseGrid1 = new System.Windows.Forms.DataGridView();
            this._tokDefenseNameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseBlockNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseU_PuskCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefensePuskConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseDirectionCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseBlockExistCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseParameterCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseWorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseFeatureCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseWorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseSpeedUpCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseSpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseOSCCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseOSCv11Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseUROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseAPVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseAVRCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._defendVoltageTabPage = new System.Windows.Forms.TabPage();
            this._blockDDL = new System.Windows.Forms.GroupBox();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this._U2CB = new System.Windows.Forms.ComboBox();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this._U1CB = new System.Windows.Forms.ComboBox();
            this._voltageDefensesGrid2 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesBlockingNumberCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesParameterCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesWorkConstraintCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesWorkTimeCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnCol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVreturnCol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesReturnConstraintCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnTimeCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesOSCCol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesOSC�11Col2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesUROVcol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVcol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAVRcol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesResetCol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesGrid1 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesBlockingNumberCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesParameterCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesWorkConstraintCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesWorkTimeCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVReturlCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesReturnConstraintCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnTimeCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesOSCCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesOSCv11Col1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesUROVCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAVRCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesResetCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesGrid3 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesBlockingNumberCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesParameterCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesWorkConstraintCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesTimeConstraintCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnCol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVreturnCol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesReturnConstraintCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnTimeCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesOSCCol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesOSCv11Col3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesUROVcol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVcol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAVRcol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesResetcol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequencyDefendTabPage = new System.Windows.Forms.TabPage();
            this._frequenceDefensesGrid = new System.Windows.Forms.DataGridView();
            this._frequenceDefensesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesMode = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesBlockingNumber = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesWorkConstraint = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesWorkTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesReturn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesAPVReturn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesReturnTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesConstraintAPV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesOSC = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesOSCv11 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesUROV = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesAPV = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesAVR = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesReset = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this._groupChangeButton = new System.Windows.Forms.Button();
            this._reserveRadioBtnGrop = new System.Windows.Forms.RadioButton();
            this._mainRadioBtnGroup = new System.Windows.Forms.RadioButton();
            this._saveToXmlButton = new System.Windows.Forms.Button();
            this._saveXmlDialog = new System.Windows.Forms.SaveFileDialog();
            this._resetSetpointsButton = new System.Windows.Forms.Button();
            this._diskretChannelCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._diskretValueCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._keyValue = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._statusStrip.SuspendLayout();
            this._tabControl.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this._inSignalsPage.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid3)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid4)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid5)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid6)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid7)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid8)).BeginInit();
            this.oscGroupBox.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._keysDataGrid)).BeginInit();
            this.groupBox18.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._outputSignalsPage.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.VLSTabControl.SuspendLayout();
            this.VLS1.SuspendLayout();
            this.VLS2.SuspendLayout();
            this.VLS3.SuspendLayout();
            this.VLS4.SuspendLayout();
            this.VLS5.SuspendLayout();
            this.VLS6.SuspendLayout();
            this.VLS7.SuspendLayout();
            this.VLS8.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this._externalDefensePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).BeginInit();
            this._automaticPage.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this._defendTabPage.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this._tokDefendTabPage.SuspendLayout();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid1)).BeginInit();
            this._defendVoltageTabPage.SuspendLayout();
            this._blockDDL.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid3)).BeginInit();
            this._frequencyDefendTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._frequenceDefensesGrid)).BeginInit();
            this.groupBox24.SuspendLayout();
            this.SuspendLayout();
            // 
            // apv_blocking
            // 
            this.apv_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apv_blocking.FormattingEnabled = true;
            this.apv_blocking.Location = new System.Drawing.Point(146, 40);
            this.apv_blocking.Name = "apv_blocking";
            this.apv_blocking.Size = new System.Drawing.Size(87, 21);
            this.apv_blocking.TabIndex = 3;
            // 
            // apv_conf
            // 
            this.apv_conf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apv_conf.FormattingEnabled = true;
            this.apv_conf.Location = new System.Drawing.Point(146, 19);
            this.apv_conf.Name = "apv_conf";
            this.apv_conf.Size = new System.Drawing.Size(87, 21);
            this.apv_conf.TabIndex = 1;
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveConfigBut.Location = new System.Drawing.Point(666, 663);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(125, 23);
            this._saveConfigBut.TabIndex = 10;
            this._saveConfigBut.Text = "��������� � ����";
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._loadConfigBut.Location = new System.Drawing.Point(535, 663);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(125, 23);
            this._loadConfigBut.TabIndex = 9;
            this._loadConfigBut.Text = "��������� �� �����";
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _toolTip
            // 
            this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // _TT_Box
            // 
            this._TT_Box.Location = new System.Drawing.Point(147, 12);
            this._TT_Box.Name = "_TT_Box";
            this._TT_Box.Size = new System.Drawing.Size(48, 20);
            this._TT_Box.TabIndex = 0;
            this._TT_Box.Tag = "1500";
            this._TT_Box.Text = "0";
            this._TT_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._toolTip.SetToolTip(this._TT_Box, "��������� ��� ��������������");
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeConfigBut.Location = new System.Drawing.Point(159, 663);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(149, 23);
            this._writeConfigBut.TabIndex = 8;
            this._writeConfigBut.Text = "�������� � ����������";
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "bin";
            this._saveConfigurationDlg.FileName = "��741_�������";
            this._saveConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._saveConfigurationDlg.Title = "���������  ������� ��� ��741";
            // 
            // _processLabel
            // 
            this._processLabel.Name = "_processLabel";
            this._processLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _exchangeProgressBar
            // 
            this._exchangeProgressBar.Maximum = 70;
            this._exchangeProgressBar.Name = "_exchangeProgressBar";
            this._exchangeProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "bin";
            this._openConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "������� ������� ��� ��741";
            // 
            // _statusStrip
            // 
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._exchangeProgressBar,
            this._processLabel});
            this._statusStrip.Location = new System.Drawing.Point(0, 689);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this._statusStrip.Size = new System.Drawing.Size(926, 22);
            this._statusStrip.TabIndex = 11;
            this._statusStrip.Text = "statusStrip1";
            // 
            // _readConfigBut
            // 
            this._readConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readConfigBut.Location = new System.Drawing.Point(4, 663);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(149, 23);
            this._readConfigBut.TabIndex = 7;
            this._readConfigBut.Text = "��������� �� ����������";
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(8, 166);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(101, 13);
            this.label56.TabIndex = 33;
            this.label56.Text = "����� 4 �����, ��";
            // 
            // apv_time_4krat
            // 
            this.apv_time_4krat.Location = new System.Drawing.Point(146, 161);
            this.apv_time_4krat.Name = "apv_time_4krat";
            this.apv_time_4krat.Size = new System.Drawing.Size(87, 20);
            this.apv_time_4krat.TabIndex = 23;
            this.apv_time_4krat.Tag = "3000000";
            this.apv_time_4krat.Text = "0";
            this.apv_time_4krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(8, 146);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(101, 13);
            this.label52.TabIndex = 32;
            this.label52.Text = "����� 3 �����, ��";
            // 
            // apv_time_3krat
            // 
            this.apv_time_3krat.Location = new System.Drawing.Point(146, 141);
            this.apv_time_3krat.Name = "apv_time_3krat";
            this.apv_time_3krat.Size = new System.Drawing.Size(87, 20);
            this.apv_time_3krat.TabIndex = 22;
            this.apv_time_3krat.Tag = "3000000";
            this.apv_time_3krat.Text = "0";
            this.apv_time_3krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(8, 126);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(101, 13);
            this.label53.TabIndex = 31;
            this.label53.Text = "����� 2 �����, ��";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(8, 186);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(132, 13);
            this.label55.TabIndex = 34;
            this.label55.Text = "������ ��� �� �������.";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(8, 26);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(80, 13);
            this.label47.TabIndex = 25;
            this.label47.Text = "������������";
            // 
            // apv_time_2krat
            // 
            this.apv_time_2krat.Location = new System.Drawing.Point(146, 121);
            this.apv_time_2krat.Name = "apv_time_2krat";
            this.apv_time_2krat.Size = new System.Drawing.Size(87, 20);
            this.apv_time_2krat.TabIndex = 21;
            this.apv_time_2krat.Tag = "3000000";
            this.apv_time_2krat.Text = "0";
            this.apv_time_2krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _tabControl
            // 
            this._tabControl.ContextMenuStrip = this.contextMenu;
            this._tabControl.Controls.Add(this._inSignalsPage);
            this._tabControl.Controls.Add(this._outputSignalsPage);
            this._tabControl.Controls.Add(this._externalDefensePage);
            this._tabControl.Controls.Add(this._automaticPage);
            this._tabControl.Controls.Add(this._defendTabPage);
            this._tabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._tabControl.Location = new System.Drawing.Point(0, 0);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(926, 661);
            this._tabControl.TabIndex = 6;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.resetSetPointStruct,
            this.readFromFileItem,
            this.writeToFileItem,
            this.writeToHtmlItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 136);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "��������� �� ����������";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "�������� � ����������";
            // 
            // resetSetPointStruct
            // 
            this.resetSetPointStruct.Name = "resetSetPointStruct";
            this.resetSetPointStruct.Size = new System.Drawing.Size(212, 22);
            this.resetSetPointStruct.Text = "��������� �������";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "��������� �� �����";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "��������� � ����";
            // 
            // writeToHtmlItem
            // 
            this.writeToHtmlItem.Name = "writeToHtmlItem";
            this.writeToHtmlItem.Size = new System.Drawing.Size(212, 22);
            this.writeToHtmlItem.Text = "��������� � HTML";
            // 
            // _inSignalsPage
            // 
            this._inSignalsPage.Controls.Add(this.groupBox16);
            this._inSignalsPage.Controls.Add(this.oscGroupBox);
            this._inSignalsPage.Controls.Add(this.groupBox23);
            this._inSignalsPage.Controls.Add(this.groupBox19);
            this._inSignalsPage.Controls.Add(this.groupBox18);
            this._inSignalsPage.Controls.Add(this.groupBox7);
            this._inSignalsPage.Controls.Add(this.groupBox4);
            this._inSignalsPage.Controls.Add(this.groupBox3);
            this._inSignalsPage.Controls.Add(this.groupBox2);
            this._inSignalsPage.Controls.Add(this.groupBox1);
            this._inSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._inSignalsPage.Name = "_inSignalsPage";
            this._inSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._inSignalsPage.Size = new System.Drawing.Size(918, 635);
            this._inSignalsPage.TabIndex = 0;
            this._inSignalsPage.Text = "������� �������";
            this._inSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.tabControl2);
            this.groupBox16.Location = new System.Drawing.Point(426, 6);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(257, 428);
            this.groupBox16.TabIndex = 23;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "���������� �������";
            // 
            // tabControl2
            // 
            this.tabControl2.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Controls.Add(this.tabPage7);
            this.tabControl2.Controls.Add(this.tabPage8);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(3, 16);
            this.tabControl2.Multiline = true;
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(251, 409);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._logicSignalsDataGrid1);
            this.tabPage1.Location = new System.Drawing.Point(4, 49);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(243, 356);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "��1 �";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // _logicSignalsDataGrid1
            // 
            this._logicSignalsDataGrid1.AllowUserToAddRows = false;
            this._logicSignalsDataGrid1.AllowUserToDeleteRows = false;
            this._logicSignalsDataGrid1.AllowUserToResizeColumns = false;
            this._logicSignalsDataGrid1.AllowUserToResizeRows = false;
            this._logicSignalsDataGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._logicSignalsDataGrid1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._logicSignalsDataGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this._logicSignalsDataGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._logicSignalsDataGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._diskretChannelCol,
            this._diskretValueCol});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._logicSignalsDataGrid1.DefaultCellStyle = dataGridViewCellStyle2;
            this._logicSignalsDataGrid1.Location = new System.Drawing.Point(43, 3);
            this._logicSignalsDataGrid1.MultiSelect = false;
            this._logicSignalsDataGrid1.Name = "_logicSignalsDataGrid1";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._logicSignalsDataGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this._logicSignalsDataGrid1.RowHeadersVisible = false;
            this._logicSignalsDataGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._logicSignalsDataGrid1.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this._logicSignalsDataGrid1.RowTemplate.Height = 20;
            this._logicSignalsDataGrid1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._logicSignalsDataGrid1.ShowCellErrors = false;
            this._logicSignalsDataGrid1.ShowCellToolTips = false;
            this._logicSignalsDataGrid1.ShowEditingIcon = false;
            this._logicSignalsDataGrid1.ShowRowErrors = false;
            this._logicSignalsDataGrid1.Size = new System.Drawing.Size(159, 347);
            this._logicSignalsDataGrid1.TabIndex = 0;
            this._logicSignalsDataGrid1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this._logicSignalsDataGrid2);
            this.tabPage2.Location = new System.Drawing.Point(4, 49);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(243, 356);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "��2 �";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // _logicSignalsDataGrid2
            // 
            this._logicSignalsDataGrid2.AllowUserToAddRows = false;
            this._logicSignalsDataGrid2.AllowUserToDeleteRows = false;
            this._logicSignalsDataGrid2.AllowUserToResizeColumns = false;
            this._logicSignalsDataGrid2.AllowUserToResizeRows = false;
            this._logicSignalsDataGrid2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._logicSignalsDataGrid2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._logicSignalsDataGrid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this._logicSignalsDataGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._logicSignalsDataGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewComboBoxColumn1});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._logicSignalsDataGrid2.DefaultCellStyle = dataGridViewCellStyle6;
            this._logicSignalsDataGrid2.Location = new System.Drawing.Point(43, 3);
            this._logicSignalsDataGrid2.MultiSelect = false;
            this._logicSignalsDataGrid2.Name = "_logicSignalsDataGrid2";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._logicSignalsDataGrid2.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this._logicSignalsDataGrid2.RowHeadersVisible = false;
            this._logicSignalsDataGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._logicSignalsDataGrid2.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this._logicSignalsDataGrid2.RowTemplate.Height = 20;
            this._logicSignalsDataGrid2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._logicSignalsDataGrid2.ShowCellErrors = false;
            this._logicSignalsDataGrid2.ShowCellToolTips = false;
            this._logicSignalsDataGrid2.ShowEditingIcon = false;
            this._logicSignalsDataGrid2.ShowRowErrors = false;
            this._logicSignalsDataGrid2.Size = new System.Drawing.Size(159, 347);
            this._logicSignalsDataGrid2.TabIndex = 1;
            this._logicSignalsDataGrid2.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "�";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 24;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.HeaderText = "��������";
            this.dataGridViewComboBoxColumn1.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this._logicSignalsDataGrid3);
            this.tabPage3.Location = new System.Drawing.Point(4, 49);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(243, 356);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "��3 �";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // _logicSignalsDataGrid3
            // 
            this._logicSignalsDataGrid3.AllowUserToAddRows = false;
            this._logicSignalsDataGrid3.AllowUserToDeleteRows = false;
            this._logicSignalsDataGrid3.AllowUserToResizeColumns = false;
            this._logicSignalsDataGrid3.AllowUserToResizeRows = false;
            this._logicSignalsDataGrid3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._logicSignalsDataGrid3.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._logicSignalsDataGrid3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this._logicSignalsDataGrid3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._logicSignalsDataGrid3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewComboBoxColumn2});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._logicSignalsDataGrid3.DefaultCellStyle = dataGridViewCellStyle10;
            this._logicSignalsDataGrid3.Location = new System.Drawing.Point(43, 3);
            this._logicSignalsDataGrid3.MultiSelect = false;
            this._logicSignalsDataGrid3.Name = "_logicSignalsDataGrid3";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._logicSignalsDataGrid3.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this._logicSignalsDataGrid3.RowHeadersVisible = false;
            this._logicSignalsDataGrid3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._logicSignalsDataGrid3.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this._logicSignalsDataGrid3.RowTemplate.Height = 20;
            this._logicSignalsDataGrid3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._logicSignalsDataGrid3.ShowCellErrors = false;
            this._logicSignalsDataGrid3.ShowCellToolTips = false;
            this._logicSignalsDataGrid3.ShowEditingIcon = false;
            this._logicSignalsDataGrid3.ShowRowErrors = false;
            this._logicSignalsDataGrid3.Size = new System.Drawing.Size(159, 347);
            this._logicSignalsDataGrid3.TabIndex = 1;
            this._logicSignalsDataGrid3.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "�";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 24;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.HeaderText = "��������";
            this.dataGridViewComboBoxColumn2.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this._logicSignalsDataGrid4);
            this.tabPage4.Location = new System.Drawing.Point(4, 49);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(243, 356);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "��4 �";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // _logicSignalsDataGrid4
            // 
            this._logicSignalsDataGrid4.AllowUserToAddRows = false;
            this._logicSignalsDataGrid4.AllowUserToDeleteRows = false;
            this._logicSignalsDataGrid4.AllowUserToResizeColumns = false;
            this._logicSignalsDataGrid4.AllowUserToResizeRows = false;
            this._logicSignalsDataGrid4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._logicSignalsDataGrid4.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._logicSignalsDataGrid4.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this._logicSignalsDataGrid4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._logicSignalsDataGrid4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewComboBoxColumn3});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._logicSignalsDataGrid4.DefaultCellStyle = dataGridViewCellStyle14;
            this._logicSignalsDataGrid4.Location = new System.Drawing.Point(43, 3);
            this._logicSignalsDataGrid4.MultiSelect = false;
            this._logicSignalsDataGrid4.Name = "_logicSignalsDataGrid4";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._logicSignalsDataGrid4.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this._logicSignalsDataGrid4.RowHeadersVisible = false;
            this._logicSignalsDataGrid4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._logicSignalsDataGrid4.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this._logicSignalsDataGrid4.RowTemplate.Height = 20;
            this._logicSignalsDataGrid4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._logicSignalsDataGrid4.ShowCellErrors = false;
            this._logicSignalsDataGrid4.ShowCellToolTips = false;
            this._logicSignalsDataGrid4.ShowEditingIcon = false;
            this._logicSignalsDataGrid4.ShowRowErrors = false;
            this._logicSignalsDataGrid4.Size = new System.Drawing.Size(159, 347);
            this._logicSignalsDataGrid4.TabIndex = 1;
            this._logicSignalsDataGrid4.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "�";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 24;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.HeaderText = "��������";
            this.dataGridViewComboBoxColumn3.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this._logicSignalsDataGrid5);
            this.tabPage5.Location = new System.Drawing.Point(4, 49);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(243, 356);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "��5 ���";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // _logicSignalsDataGrid5
            // 
            this._logicSignalsDataGrid5.AllowUserToAddRows = false;
            this._logicSignalsDataGrid5.AllowUserToDeleteRows = false;
            this._logicSignalsDataGrid5.AllowUserToResizeColumns = false;
            this._logicSignalsDataGrid5.AllowUserToResizeRows = false;
            this._logicSignalsDataGrid5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._logicSignalsDataGrid5.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._logicSignalsDataGrid5.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this._logicSignalsDataGrid5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._logicSignalsDataGrid5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewComboBoxColumn4});
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._logicSignalsDataGrid5.DefaultCellStyle = dataGridViewCellStyle18;
            this._logicSignalsDataGrid5.Location = new System.Drawing.Point(43, 3);
            this._logicSignalsDataGrid5.MultiSelect = false;
            this._logicSignalsDataGrid5.Name = "_logicSignalsDataGrid5";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._logicSignalsDataGrid5.RowHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this._logicSignalsDataGrid5.RowHeadersVisible = false;
            this._logicSignalsDataGrid5.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._logicSignalsDataGrid5.RowsDefaultCellStyle = dataGridViewCellStyle20;
            this._logicSignalsDataGrid5.RowTemplate.Height = 20;
            this._logicSignalsDataGrid5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._logicSignalsDataGrid5.ShowCellErrors = false;
            this._logicSignalsDataGrid5.ShowCellToolTips = false;
            this._logicSignalsDataGrid5.ShowEditingIcon = false;
            this._logicSignalsDataGrid5.ShowRowErrors = false;
            this._logicSignalsDataGrid5.Size = new System.Drawing.Size(159, 347);
            this._logicSignalsDataGrid5.TabIndex = 1;
            this._logicSignalsDataGrid5.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "�";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Width = 24;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.HeaderText = "��������";
            this.dataGridViewComboBoxColumn4.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this._logicSignalsDataGrid6);
            this.tabPage6.Location = new System.Drawing.Point(4, 49);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(243, 356);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "��6 ���";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // _logicSignalsDataGrid6
            // 
            this._logicSignalsDataGrid6.AllowUserToAddRows = false;
            this._logicSignalsDataGrid6.AllowUserToDeleteRows = false;
            this._logicSignalsDataGrid6.AllowUserToResizeColumns = false;
            this._logicSignalsDataGrid6.AllowUserToResizeRows = false;
            this._logicSignalsDataGrid6.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._logicSignalsDataGrid6.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._logicSignalsDataGrid6.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this._logicSignalsDataGrid6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._logicSignalsDataGrid6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewComboBoxColumn5});
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._logicSignalsDataGrid6.DefaultCellStyle = dataGridViewCellStyle22;
            this._logicSignalsDataGrid6.Location = new System.Drawing.Point(43, 3);
            this._logicSignalsDataGrid6.MultiSelect = false;
            this._logicSignalsDataGrid6.Name = "_logicSignalsDataGrid6";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._logicSignalsDataGrid6.RowHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this._logicSignalsDataGrid6.RowHeadersVisible = false;
            this._logicSignalsDataGrid6.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._logicSignalsDataGrid6.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this._logicSignalsDataGrid6.RowTemplate.Height = 20;
            this._logicSignalsDataGrid6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._logicSignalsDataGrid6.ShowCellErrors = false;
            this._logicSignalsDataGrid6.ShowCellToolTips = false;
            this._logicSignalsDataGrid6.ShowEditingIcon = false;
            this._logicSignalsDataGrid6.ShowRowErrors = false;
            this._logicSignalsDataGrid6.Size = new System.Drawing.Size(159, 347);
            this._logicSignalsDataGrid6.TabIndex = 1;
            this._logicSignalsDataGrid6.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "�";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Width = 24;
            // 
            // dataGridViewComboBoxColumn5
            // 
            this.dataGridViewComboBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.HeaderText = "��������";
            this.dataGridViewComboBoxColumn5.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this._logicSignalsDataGrid7);
            this.tabPage7.Location = new System.Drawing.Point(4, 49);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(243, 356);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "��7 ���";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // _logicSignalsDataGrid7
            // 
            this._logicSignalsDataGrid7.AllowUserToAddRows = false;
            this._logicSignalsDataGrid7.AllowUserToDeleteRows = false;
            this._logicSignalsDataGrid7.AllowUserToResizeColumns = false;
            this._logicSignalsDataGrid7.AllowUserToResizeRows = false;
            this._logicSignalsDataGrid7.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._logicSignalsDataGrid7.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._logicSignalsDataGrid7.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this._logicSignalsDataGrid7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._logicSignalsDataGrid7.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewComboBoxColumn6});
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._logicSignalsDataGrid7.DefaultCellStyle = dataGridViewCellStyle26;
            this._logicSignalsDataGrid7.Location = new System.Drawing.Point(43, 3);
            this._logicSignalsDataGrid7.MultiSelect = false;
            this._logicSignalsDataGrid7.Name = "_logicSignalsDataGrid7";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._logicSignalsDataGrid7.RowHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this._logicSignalsDataGrid7.RowHeadersVisible = false;
            this._logicSignalsDataGrid7.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._logicSignalsDataGrid7.RowsDefaultCellStyle = dataGridViewCellStyle28;
            this._logicSignalsDataGrid7.RowTemplate.Height = 20;
            this._logicSignalsDataGrid7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._logicSignalsDataGrid7.ShowCellErrors = false;
            this._logicSignalsDataGrid7.ShowCellToolTips = false;
            this._logicSignalsDataGrid7.ShowEditingIcon = false;
            this._logicSignalsDataGrid7.ShowRowErrors = false;
            this._logicSignalsDataGrid7.Size = new System.Drawing.Size(159, 347);
            this._logicSignalsDataGrid7.TabIndex = 1;
            this._logicSignalsDataGrid7.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "�";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Width = 24;
            // 
            // dataGridViewComboBoxColumn6
            // 
            this.dataGridViewComboBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.HeaderText = "��������";
            this.dataGridViewComboBoxColumn6.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this._logicSignalsDataGrid8);
            this.tabPage8.Location = new System.Drawing.Point(4, 49);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(243, 356);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "��8 ���";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // _logicSignalsDataGrid8
            // 
            this._logicSignalsDataGrid8.AllowUserToAddRows = false;
            this._logicSignalsDataGrid8.AllowUserToDeleteRows = false;
            this._logicSignalsDataGrid8.AllowUserToResizeColumns = false;
            this._logicSignalsDataGrid8.AllowUserToResizeRows = false;
            this._logicSignalsDataGrid8.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._logicSignalsDataGrid8.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._logicSignalsDataGrid8.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle29;
            this._logicSignalsDataGrid8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._logicSignalsDataGrid8.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewComboBoxColumn7});
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._logicSignalsDataGrid8.DefaultCellStyle = dataGridViewCellStyle30;
            this._logicSignalsDataGrid8.Location = new System.Drawing.Point(43, 3);
            this._logicSignalsDataGrid8.MultiSelect = false;
            this._logicSignalsDataGrid8.Name = "_logicSignalsDataGrid8";
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._logicSignalsDataGrid8.RowHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this._logicSignalsDataGrid8.RowHeadersVisible = false;
            this._logicSignalsDataGrid8.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._logicSignalsDataGrid8.RowsDefaultCellStyle = dataGridViewCellStyle32;
            this._logicSignalsDataGrid8.RowTemplate.Height = 20;
            this._logicSignalsDataGrid8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._logicSignalsDataGrid8.ShowCellErrors = false;
            this._logicSignalsDataGrid8.ShowCellToolTips = false;
            this._logicSignalsDataGrid8.ShowEditingIcon = false;
            this._logicSignalsDataGrid8.ShowRowErrors = false;
            this._logicSignalsDataGrid8.Size = new System.Drawing.Size(159, 347);
            this._logicSignalsDataGrid8.TabIndex = 1;
            this._logicSignalsDataGrid8.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "�";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Width = 24;
            // 
            // dataGridViewComboBoxColumn7
            // 
            this.dataGridViewComboBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn7.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn7.HeaderText = "��������";
            this.dataGridViewComboBoxColumn7.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            // 
            // oscGroupBox
            // 
            this.oscGroupBox.Controls.Add(this.oscFix);
            this.oscGroupBox.Controls.Add(this.oscPercent);
            this.oscGroupBox.Controls.Add(this.oscLength);
            this.oscGroupBox.Controls.Add(this.label32);
            this.oscGroupBox.Controls.Add(this.label31);
            this.oscGroupBox.Controls.Add(this.label30);
            this.oscGroupBox.Location = new System.Drawing.Point(217, 276);
            this.oscGroupBox.Name = "oscGroupBox";
            this.oscGroupBox.Size = new System.Drawing.Size(203, 158);
            this.oscGroupBox.TabIndex = 22;
            this.oscGroupBox.TabStop = false;
            this.oscGroupBox.Text = "��������� ������������";
            this.oscGroupBox.Visible = false;
            // 
            // oscFix
            // 
            this.oscFix.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.oscFix.FormattingEnabled = true;
            this.oscFix.Location = new System.Drawing.Point(114, 52);
            this.oscFix.Name = "oscFix";
            this.oscFix.Size = new System.Drawing.Size(83, 21);
            this.oscFix.TabIndex = 0;
            // 
            // oscPercent
            // 
            this.oscPercent.Location = new System.Drawing.Point(149, 79);
            this.oscPercent.Name = "oscPercent";
            this.oscPercent.Size = new System.Drawing.Size(48, 20);
            this.oscPercent.TabIndex = 0;
            this.oscPercent.Tag = "99";
            this.oscPercent.Text = "1";
            // 
            // oscLength
            // 
            this.oscLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.oscLength.FormattingEnabled = true;
            this.oscLength.Location = new System.Drawing.Point(114, 25);
            this.oscLength.Name = "oscLength";
            this.oscLength.Size = new System.Drawing.Size(83, 21);
            this.oscLength.TabIndex = 0;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 82);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(137, 13);
            this.label32.TabIndex = 4;
            this.label32.Text = "����. ���������� ���., %";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 55);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(84, 13);
            this.label31.TabIndex = 4;
            this.label31.Text = "�������� ���.";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 28);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(46, 13);
            this.label30.TabIndex = 4;
            this.label30.Text = "������";
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this._oscKonfComb);
            this.groupBox23.Location = new System.Drawing.Point(217, 276);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(203, 51);
            this.groupBox23.TabIndex = 20;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "���������� ������������";
            // 
            // _oscKonfComb
            // 
            this._oscKonfComb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscKonfComb.FormattingEnabled = true;
            this._oscKonfComb.Items.AddRange(new object[] {
            "1 �������������",
            "1 ���������������� ���.",
            "2 ���������������� ���."});
            this._oscKonfComb.Location = new System.Drawing.Point(15, 19);
            this._oscKonfComb.Name = "_oscKonfComb";
            this._oscKonfComb.Size = new System.Drawing.Size(175, 21);
            this._oscKonfComb.TabIndex = 0;
            this._oscKonfComb.SelectedIndexChanged += new System.EventHandler(this._oscKonfComb_SelectedIndexChanged);
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this._keysDataGrid);
            this.groupBox19.Location = new System.Drawing.Point(689, 6);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(145, 374);
            this.groupBox19.TabIndex = 18;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "�����";
            // 
            // _keysDataGrid
            // 
            this._keysDataGrid.AllowUserToAddRows = false;
            this._keysDataGrid.AllowUserToDeleteRows = false;
            this._keysDataGrid.AllowUserToResizeColumns = false;
            this._keysDataGrid.AllowUserToResizeRows = false;
            this._keysDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._keysDataGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._keysDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle33;
            this._keysDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._keysDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this._keyValue});
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._keysDataGrid.DefaultCellStyle = dataGridViewCellStyle34;
            this._keysDataGrid.Location = new System.Drawing.Point(6, 19);
            this._keysDataGrid.MultiSelect = false;
            this._keysDataGrid.Name = "_keysDataGrid";
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._keysDataGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle35;
            this._keysDataGrid.RowHeadersVisible = false;
            this._keysDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._keysDataGrid.RowsDefaultCellStyle = dataGridViewCellStyle36;
            this._keysDataGrid.RowTemplate.Height = 20;
            this._keysDataGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._keysDataGrid.ShowCellErrors = false;
            this._keysDataGrid.ShowCellToolTips = false;
            this._keysDataGrid.ShowEditingIcon = false;
            this._keysDataGrid.ShowRowErrors = false;
            this._keysDataGrid.Size = new System.Drawing.Size(132, 346);
            this._keysDataGrid.TabIndex = 1;
            this._keysDataGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this._HUD_Box);
            this.groupBox18.Controls.Add(this._OMP_TypeBox);
            this.groupBox18.Controls.Add(this.label26);
            this.groupBox18.Controls.Add(this.label27);
            this.groupBox18.Location = new System.Drawing.Point(217, 201);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(203, 69);
            this.groupBox18.TabIndex = 17;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "����������� ����� �����������";
            // 
            // _HUD_Box
            // 
            this._HUD_Box.Location = new System.Drawing.Point(114, 42);
            this._HUD_Box.Name = "_HUD_Box";
            this._HUD_Box.Size = new System.Drawing.Size(83, 20);
            this._HUD_Box.TabIndex = 20;
            this._HUD_Box.Tag = "1";
            this._HUD_Box.Text = "0";
            this._HUD_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _OMP_TypeBox
            // 
            this._OMP_TypeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._OMP_TypeBox.FormattingEnabled = true;
            this._OMP_TypeBox.Location = new System.Drawing.Point(114, 20);
            this._OMP_TypeBox.Name = "_OMP_TypeBox";
            this._OMP_TypeBox.Size = new System.Drawing.Size(83, 21);
            this._OMP_TypeBox.TabIndex = 19;
            this._OMP_TypeBox.SelectedIndexChanged += new System.EventHandler(this._OMP_TypeBox_SelectedIndexChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(7, 45);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(70, 13);
            this.label26.TabIndex = 18;
            this.label26.Text = "��� (��/��)";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 23);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(108, 13);
            this.label27.TabIndex = 16;
            this.label27.Text = "������������ ���";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._manageSignalsSDTU_Combo);
            this.groupBox7.Controls.Add(this.label43);
            this.groupBox7.Controls.Add(this._manageSignalsExternalCombo);
            this.groupBox7.Controls.Add(this.label44);
            this.groupBox7.Controls.Add(this._manageSignalsKeyCombo);
            this.groupBox7.Controls.Add(this.label45);
            this.groupBox7.Controls.Add(this._manageSignalsButtonCombo);
            this.groupBox7.Controls.Add(this.label46);
            this.groupBox7.Location = new System.Drawing.Point(8, 393);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(203, 101);
            this.groupBox7.TabIndex = 16;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "������� ����������";
            // 
            // _manageSignalsSDTU_Combo
            // 
            this._manageSignalsSDTU_Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsSDTU_Combo.FormattingEnabled = true;
            this._manageSignalsSDTU_Combo.Location = new System.Drawing.Point(71, 75);
            this._manageSignalsSDTU_Combo.Name = "_manageSignalsSDTU_Combo";
            this._manageSignalsSDTU_Combo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsSDTU_Combo.TabIndex = 23;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(6, 79);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(54, 13);
            this.label43.TabIndex = 22;
            this.label43.Text = "�� ����";
            // 
            // _manageSignalsExternalCombo
            // 
            this._manageSignalsExternalCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsExternalCombo.FormattingEnabled = true;
            this._manageSignalsExternalCombo.Location = new System.Drawing.Point(71, 56);
            this._manageSignalsExternalCombo.Name = "_manageSignalsExternalCombo";
            this._manageSignalsExternalCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsExternalCombo.TabIndex = 21;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(5, 60);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(52, 13);
            this.label44.TabIndex = 20;
            this.label44.Text = "�������";
            // 
            // _manageSignalsKeyCombo
            // 
            this._manageSignalsKeyCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsKeyCombo.FormattingEnabled = true;
            this._manageSignalsKeyCombo.Location = new System.Drawing.Point(71, 37);
            this._manageSignalsKeyCombo.Name = "_manageSignalsKeyCombo";
            this._manageSignalsKeyCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsKeyCombo.TabIndex = 19;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(6, 41);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(54, 13);
            this.label45.TabIndex = 18;
            this.label45.Text = "�� �����";
            // 
            // _manageSignalsButtonCombo
            // 
            this._manageSignalsButtonCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsButtonCombo.FormattingEnabled = true;
            this._manageSignalsButtonCombo.Location = new System.Drawing.Point(71, 17);
            this._manageSignalsButtonCombo.Name = "_manageSignalsButtonCombo";
            this._manageSignalsButtonCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsButtonCombo.TabIndex = 17;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(6, 22);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(59, 13);
            this.label46.TabIndex = 16;
            this.label46.Text = "�� ������";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._Sost_ON_OFF);
            this.groupBox4.Controls.Add(this._Sost_ON_OFF_Label);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this._switcherDurationBox);
            this.groupBox4.Controls.Add(this._switcherTokBox);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this._switcherTimeBox);
            this.groupBox4.Controls.Add(this._switcherImpulseBox);
            this.groupBox4.Controls.Add(this._switcherBlockCombo);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this._switcherErrorCombo);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this._switcherStateOnCombo);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this._switcherStateOffCombo);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Location = new System.Drawing.Point(217, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(203, 192);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "�����������";
            // 
            // _Sost_ON_OFF
            // 
            this._Sost_ON_OFF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._Sost_ON_OFF.FormattingEnabled = true;
            this._Sost_ON_OFF.Location = new System.Drawing.Point(125, 165);
            this._Sost_ON_OFF.Name = "_Sost_ON_OFF";
            this._Sost_ON_OFF.Size = new System.Drawing.Size(71, 21);
            this._Sost_ON_OFF.TabIndex = 26;
            this._Sost_ON_OFF.Visible = false;
            // 
            // _Sost_ON_OFF_Label
            // 
            this._Sost_ON_OFF_Label.AutoSize = true;
            this._Sost_ON_OFF_Label.Location = new System.Drawing.Point(4, 168);
            this._Sost_ON_OFF_Label.Name = "_Sost_ON_OFF_Label";
            this._Sost_ON_OFF_Label.Size = new System.Drawing.Size(117, 13);
            this._Sost_ON_OFF_Label.TabIndex = 25;
            this._Sost_ON_OFF_Label.Text = "�������� ����� ���-�";
            this._Sost_ON_OFF_Label.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(4, 148);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(112, 13);
            this.label19.TabIndex = 23;
            this.label19.Text = "����. ���������, ��";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(4, 110);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 13);
            this.label17.TabIndex = 19;
            this.label17.Text = "��� ����, In";
            // 
            // _switcherDurationBox
            // 
            this._switcherDurationBox.Location = new System.Drawing.Point(125, 147);
            this._switcherDurationBox.Name = "_switcherDurationBox";
            this._switcherDurationBox.Size = new System.Drawing.Size(71, 20);
            this._switcherDurationBox.TabIndex = 22;
            this._switcherDurationBox.Tag = "3000000";
            this._switcherDurationBox.Text = "0";
            this._switcherDurationBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherTokBox
            // 
            this._switcherTokBox.Location = new System.Drawing.Point(125, 107);
            this._switcherTokBox.Name = "_switcherTokBox";
            this._switcherTokBox.Size = new System.Drawing.Size(71, 20);
            this._switcherTokBox.TabIndex = 18;
            this._switcherTokBox.Tag = "40";
            this._switcherTokBox.Text = "0";
            this._switcherTokBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(4, 129);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(90, 13);
            this.label20.TabIndex = 21;
            this.label20.Text = "������� ��, ��";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(4, 91);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(93, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "����� ����, ��";
            // 
            // _switcherTimeBox
            // 
            this._switcherTimeBox.Location = new System.Drawing.Point(125, 87);
            this._switcherTimeBox.Name = "_switcherTimeBox";
            this._switcherTimeBox.Size = new System.Drawing.Size(71, 20);
            this._switcherTimeBox.TabIndex = 16;
            this._switcherTimeBox.Tag = "3000000";
            this._switcherTimeBox.Text = "0";
            this._switcherTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherImpulseBox
            // 
            this._switcherImpulseBox.Location = new System.Drawing.Point(125, 127);
            this._switcherImpulseBox.Name = "_switcherImpulseBox";
            this._switcherImpulseBox.Size = new System.Drawing.Size(71, 20);
            this._switcherImpulseBox.TabIndex = 20;
            this._switcherImpulseBox.Tag = "3000000";
            this._switcherImpulseBox.Text = "0";
            this._switcherImpulseBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherBlockCombo
            // 
            this._switcherBlockCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherBlockCombo.FormattingEnabled = true;
            this._switcherBlockCombo.Location = new System.Drawing.Point(125, 68);
            this._switcherBlockCombo.Name = "_switcherBlockCombo";
            this._switcherBlockCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherBlockCombo.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "����������";
            // 
            // _switcherErrorCombo
            // 
            this._switcherErrorCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherErrorCombo.FormattingEnabled = true;
            this._switcherErrorCombo.Location = new System.Drawing.Point(125, 49);
            this._switcherErrorCombo.Name = "_switcherErrorCombo";
            this._switcherErrorCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherErrorCombo.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(4, 53);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "�������������";
            // 
            // _switcherStateOnCombo
            // 
            this._switcherStateOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherStateOnCombo.FormattingEnabled = true;
            this._switcherStateOnCombo.Location = new System.Drawing.Point(125, 30);
            this._switcherStateOnCombo.Name = "_switcherStateOnCombo";
            this._switcherStateOnCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherStateOnCombo.TabIndex = 11;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 32);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(113, 13);
            this.label15.TabIndex = 10;
            this.label15.Text = "��������� ��������";
            // 
            // _switcherStateOffCombo
            // 
            this._switcherStateOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherStateOffCombo.FormattingEnabled = true;
            this._switcherStateOffCombo.Location = new System.Drawing.Point(125, 11);
            this._switcherStateOffCombo.Name = "_switcherStateOffCombo";
            this._switcherStateOffCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherStateOffCombo.TabIndex = 9;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 15);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(118, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "��������� ���������";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._exBlockSdtuCombo);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this._constraintGroupCombo);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this._signalizationCombo);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this._extOffCombo);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this._extOnCombo);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this._keyOnCombo);
            this.groupBox3.Controls.Add(this._keyOffCombo);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(8, 232);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(203, 155);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "������� �������";
            // 
            // _exBlockSdtuCombo
            // 
            this._exBlockSdtuCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._exBlockSdtuCombo.FormattingEnabled = true;
            this._exBlockSdtuCombo.Location = new System.Drawing.Point(124, 127);
            this._exBlockSdtuCombo.Name = "_exBlockSdtuCombo";
            this._exBlockSdtuCombo.Size = new System.Drawing.Size(71, 21);
            this._exBlockSdtuCombo.TabIndex = 13;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(7, 132);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(102, 13);
            this.label29.TabIndex = 12;
            this.label29.Text = "���������� ����";
            // 
            // _constraintGroupCombo
            // 
            this._constraintGroupCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._constraintGroupCombo.FormattingEnabled = true;
            this._constraintGroupCombo.Location = new System.Drawing.Point(124, 107);
            this._constraintGroupCombo.Name = "_constraintGroupCombo";
            this._constraintGroupCombo.Size = new System.Drawing.Size(71, 21);
            this._constraintGroupCombo.TabIndex = 11;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 112);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "������ �������";
            // 
            // _signalizationCombo
            // 
            this._signalizationCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._signalizationCombo.FormattingEnabled = true;
            this._signalizationCombo.Location = new System.Drawing.Point(124, 88);
            this._signalizationCombo.Name = "_signalizationCombo";
            this._signalizationCombo.Size = new System.Drawing.Size(71, 21);
            this._signalizationCombo.TabIndex = 9;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 91);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(112, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "����� ������������";
            // 
            // _extOffCombo
            // 
            this._extOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._extOffCombo.FormattingEnabled = true;
            this._extOffCombo.Location = new System.Drawing.Point(124, 69);
            this._extOffCombo.Name = "_extOffCombo";
            this._extOffCombo.Size = new System.Drawing.Size(71, 21);
            this._extOffCombo.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "����. ���������";
            // 
            // _extOnCombo
            // 
            this._extOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._extOnCombo.FormattingEnabled = true;
            this._extOnCombo.Location = new System.Drawing.Point(124, 50);
            this._extOnCombo.Name = "_extOnCombo";
            this._extOnCombo.Size = new System.Drawing.Size(71, 21);
            this._extOnCombo.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 72);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "����. ��������";
            // 
            // _keyOnCombo
            // 
            this._keyOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._keyOnCombo.FormattingEnabled = true;
            this._keyOnCombo.Location = new System.Drawing.Point(124, 31);
            this._keyOnCombo.Name = "_keyOnCombo";
            this._keyOnCombo.Size = new System.Drawing.Size(71, 21);
            this._keyOnCombo.TabIndex = 1;
            // 
            // _keyOffCombo
            // 
            this._keyOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._keyOffCombo.FormattingEnabled = true;
            this._keyOffCombo.Location = new System.Drawing.Point(124, 11);
            this._keyOffCombo.Name = "_keyOffCombo";
            this._keyOffCombo.Size = new System.Drawing.Size(71, 21);
            this._keyOffCombo.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "���� ���������";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "���� ��������";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._TN_typeCombo);
            this.groupBox2.Controls.Add(this.label42);
            this.groupBox2.Controls.Add(this._TNNP_dispepairCombo);
            this.groupBox2.Controls.Add(this.label41);
            this.groupBox2.Controls.Add(this._TN_dispepairCombo);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this._TNNP_Box);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this._TN_Box);
            this.groupBox2.Location = new System.Drawing.Point(8, 104);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(203, 122);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "��� ��";
            // 
            // _TN_typeCombo
            // 
            this._TN_typeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TN_typeCombo.FormattingEnabled = true;
            this._TN_typeCombo.Location = new System.Drawing.Point(56, 98);
            this._TN_typeCombo.Name = "_TN_typeCombo";
            this._TN_typeCombo.Size = new System.Drawing.Size(139, 21);
            this._TN_typeCombo.TabIndex = 12;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(6, 101);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(44, 13);
            this.label42.TabIndex = 11;
            this.label42.Text = "��� ��";
            // 
            // _TNNP_dispepairCombo
            // 
            this._TNNP_dispepairCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TNNP_dispepairCombo.FormattingEnabled = true;
            this._TNNP_dispepairCombo.Location = new System.Drawing.Point(94, 77);
            this._TNNP_dispepairCombo.Name = "_TNNP_dispepairCombo";
            this._TNNP_dispepairCombo.Size = new System.Drawing.Size(101, 21);
            this._TNNP_dispepairCombo.TabIndex = 10;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(6, 81);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(82, 13);
            this.label41.TabIndex = 9;
            this.label41.Text = "������. ����";
            // 
            // _TN_dispepairCombo
            // 
            this._TN_dispepairCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TN_dispepairCombo.FormattingEnabled = true;
            this._TN_dispepairCombo.Location = new System.Drawing.Point(94, 56);
            this._TN_dispepairCombo.Name = "_TN_dispepairCombo";
            this._TN_dispepairCombo.Size = new System.Drawing.Size(101, 21);
            this._TN_dispepairCombo.TabIndex = 8;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 60);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(66, 13);
            this.label39.TabIndex = 7;
            this.label39.Text = "������. ��";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "����������� ����";
            // 
            // _TNNP_Box
            // 
            this._TNNP_Box.Location = new System.Drawing.Point(135, 36);
            this._TNNP_Box.Name = "_TNNP_Box";
            this._TNNP_Box.Size = new System.Drawing.Size(60, 20);
            this._TNNP_Box.TabIndex = 5;
            this._TNNP_Box.Tag = "128000";
            this._TNNP_Box.Text = "0";
            this._TNNP_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "����������� ��";
            // 
            // _TN_Box
            // 
            this._TN_Box.Location = new System.Drawing.Point(135, 16);
            this._TN_Box.Name = "_TN_Box";
            this._TN_Box.Size = new System.Drawing.Size(60, 20);
            this._TN_Box.TabIndex = 2;
            this._TN_Box.Tag = "128000";
            this._TN_Box.Text = "0";
            this._TN_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._TT_typeCombo);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this._maxTok_Box);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._TTNP_Box);
            this.groupBox1.Controls.Add(this._TT_Box);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(203, 95);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "��������� ����";
            // 
            // _TT_typeCombo
            // 
            this._TT_typeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TT_typeCombo.FormattingEnabled = true;
            this._TT_typeCombo.Location = new System.Drawing.Point(58, 71);
            this._TT_typeCombo.Name = "_TT_typeCombo";
            this._TT_typeCombo.Size = new System.Drawing.Size(137, 21);
            this._TT_typeCombo.TabIndex = 14;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 74);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(43, 13);
            this.label25.TabIndex = 13;
            this.label25.Text = "��� �T";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "����. ��� ��������, I�";
            // 
            // _maxTok_Box
            // 
            this._maxTok_Box.Location = new System.Drawing.Point(147, 52);
            this._maxTok_Box.Name = "_maxTok_Box";
            this._maxTok_Box.Size = new System.Drawing.Size(48, 20);
            this._maxTok_Box.TabIndex = 5;
            this._maxTok_Box.Tag = "40";
            this._maxTok_Box.Text = "0";
            this._maxTok_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "��������� ��� ��, A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "��������� ��� ����, A";
            // 
            // _TTNP_Box
            // 
            this._TTNP_Box.Location = new System.Drawing.Point(147, 32);
            this._TTNP_Box.Name = "_TTNP_Box";
            this._TTNP_Box.Size = new System.Drawing.Size(48, 20);
            this._TTNP_Box.TabIndex = 2;
            this._TTNP_Box.Tag = "100";
            this._TTNP_Box.Text = "0";
            this._TTNP_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _outputSignalsPage
            // 
            this._outputSignalsPage.Controls.Add(this.groupBox5);
            this._outputSignalsPage.Controls.Add(this.groupBox6);
            this._outputSignalsPage.Controls.Add(this.groupBox9);
            this._outputSignalsPage.Controls.Add(this.groupBox8);
            this._outputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._outputSignalsPage.Name = "_outputSignalsPage";
            this._outputSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._outputSignalsPage.Size = new System.Drawing.Size(918, 635);
            this._outputSignalsPage.TabIndex = 1;
            this._outputSignalsPage.Text = "�������� �������";
            this._outputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.VLSTabControl);
            this.groupBox5.Location = new System.Drawing.Point(441, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(191, 592);
            this.groupBox5.TabIndex = 16;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "������� ���������� �������";
            // 
            // VLSTabControl
            // 
            this.VLSTabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.VLSTabControl.Controls.Add(this.VLS1);
            this.VLSTabControl.Controls.Add(this.VLS2);
            this.VLSTabControl.Controls.Add(this.VLS3);
            this.VLSTabControl.Controls.Add(this.VLS4);
            this.VLSTabControl.Controls.Add(this.VLS5);
            this.VLSTabControl.Controls.Add(this.VLS6);
            this.VLSTabControl.Controls.Add(this.VLS7);
            this.VLSTabControl.Controls.Add(this.VLS8);
            this.VLSTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VLSTabControl.Location = new System.Drawing.Point(3, 16);
            this.VLSTabControl.Multiline = true;
            this.VLSTabControl.Name = "VLSTabControl";
            this.VLSTabControl.SelectedIndex = 0;
            this.VLSTabControl.Size = new System.Drawing.Size(185, 573);
            this.VLSTabControl.TabIndex = 18;
            // 
            // VLS1
            // 
            this.VLS1.Controls.Add(this._outputLogicCheckList1);
            this.VLS1.Location = new System.Drawing.Point(4, 49);
            this.VLS1.Name = "VLS1";
            this.VLS1.Padding = new System.Windows.Forms.Padding(3);
            this.VLS1.Size = new System.Drawing.Size(177, 520);
            this.VLS1.TabIndex = 0;
            this.VLS1.Text = "��� 1";
            this.VLS1.UseVisualStyleBackColor = true;
            // 
            // _outputLogicCheckList1
            // 
            this._outputLogicCheckList1.CheckOnClick = true;
            this._outputLogicCheckList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._outputLogicCheckList1.FormattingEnabled = true;
            this._outputLogicCheckList1.Location = new System.Drawing.Point(3, 3);
            this._outputLogicCheckList1.Name = "_outputLogicCheckList1";
            this._outputLogicCheckList1.ScrollAlwaysVisible = true;
            this._outputLogicCheckList1.Size = new System.Drawing.Size(171, 514);
            this._outputLogicCheckList1.TabIndex = 6;
            // 
            // VLS2
            // 
            this.VLS2.Controls.Add(this._outputLogicCheckList2);
            this.VLS2.Location = new System.Drawing.Point(4, 49);
            this.VLS2.Name = "VLS2";
            this.VLS2.Padding = new System.Windows.Forms.Padding(3);
            this.VLS2.Size = new System.Drawing.Size(177, 520);
            this.VLS2.TabIndex = 1;
            this.VLS2.Text = "��� 2";
            this.VLS2.UseVisualStyleBackColor = true;
            // 
            // _outputLogicCheckList2
            // 
            this._outputLogicCheckList2.CheckOnClick = true;
            this._outputLogicCheckList2.Dock = System.Windows.Forms.DockStyle.Fill;
            this._outputLogicCheckList2.FormattingEnabled = true;
            this._outputLogicCheckList2.Location = new System.Drawing.Point(3, 3);
            this._outputLogicCheckList2.Name = "_outputLogicCheckList2";
            this._outputLogicCheckList2.ScrollAlwaysVisible = true;
            this._outputLogicCheckList2.Size = new System.Drawing.Size(171, 514);
            this._outputLogicCheckList2.TabIndex = 6;
            // 
            // VLS3
            // 
            this.VLS3.Controls.Add(this._outputLogicCheckList3);
            this.VLS3.Location = new System.Drawing.Point(4, 49);
            this.VLS3.Name = "VLS3";
            this.VLS3.Size = new System.Drawing.Size(177, 520);
            this.VLS3.TabIndex = 2;
            this.VLS3.Text = "��� 3";
            this.VLS3.UseVisualStyleBackColor = true;
            // 
            // _outputLogicCheckList3
            // 
            this._outputLogicCheckList3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._outputLogicCheckList3.CheckOnClick = true;
            this._outputLogicCheckList3.FormattingEnabled = true;
            this._outputLogicCheckList3.Location = new System.Drawing.Point(3, 3);
            this._outputLogicCheckList3.Name = "_outputLogicCheckList3";
            this._outputLogicCheckList3.ScrollAlwaysVisible = true;
            this._outputLogicCheckList3.Size = new System.Drawing.Size(171, 514);
            this._outputLogicCheckList3.TabIndex = 6;
            // 
            // VLS4
            // 
            this.VLS4.Controls.Add(this._outputLogicCheckList4);
            this.VLS4.Location = new System.Drawing.Point(4, 49);
            this.VLS4.Name = "VLS4";
            this.VLS4.Size = new System.Drawing.Size(177, 520);
            this.VLS4.TabIndex = 3;
            this.VLS4.Text = "��� 4";
            this.VLS4.UseVisualStyleBackColor = true;
            // 
            // _outputLogicCheckList4
            // 
            this._outputLogicCheckList4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._outputLogicCheckList4.CheckOnClick = true;
            this._outputLogicCheckList4.FormattingEnabled = true;
            this._outputLogicCheckList4.Location = new System.Drawing.Point(3, 3);
            this._outputLogicCheckList4.Name = "_outputLogicCheckList4";
            this._outputLogicCheckList4.ScrollAlwaysVisible = true;
            this._outputLogicCheckList4.Size = new System.Drawing.Size(171, 514);
            this._outputLogicCheckList4.TabIndex = 6;
            // 
            // VLS5
            // 
            this.VLS5.Controls.Add(this._outputLogicCheckList5);
            this.VLS5.Location = new System.Drawing.Point(4, 49);
            this.VLS5.Name = "VLS5";
            this.VLS5.Size = new System.Drawing.Size(177, 520);
            this.VLS5.TabIndex = 4;
            this.VLS5.Text = "��� 5";
            this.VLS5.UseVisualStyleBackColor = true;
            // 
            // _outputLogicCheckList5
            // 
            this._outputLogicCheckList5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._outputLogicCheckList5.CheckOnClick = true;
            this._outputLogicCheckList5.FormattingEnabled = true;
            this._outputLogicCheckList5.Location = new System.Drawing.Point(3, 3);
            this._outputLogicCheckList5.Name = "_outputLogicCheckList5";
            this._outputLogicCheckList5.ScrollAlwaysVisible = true;
            this._outputLogicCheckList5.Size = new System.Drawing.Size(171, 514);
            this._outputLogicCheckList5.TabIndex = 6;
            // 
            // VLS6
            // 
            this.VLS6.Controls.Add(this._outputLogicCheckList6);
            this.VLS6.Location = new System.Drawing.Point(4, 49);
            this.VLS6.Name = "VLS6";
            this.VLS6.Size = new System.Drawing.Size(177, 520);
            this.VLS6.TabIndex = 5;
            this.VLS6.Text = "��� 6";
            this.VLS6.UseVisualStyleBackColor = true;
            // 
            // _outputLogicCheckList6
            // 
            this._outputLogicCheckList6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._outputLogicCheckList6.CheckOnClick = true;
            this._outputLogicCheckList6.FormattingEnabled = true;
            this._outputLogicCheckList6.Location = new System.Drawing.Point(3, 3);
            this._outputLogicCheckList6.Name = "_outputLogicCheckList6";
            this._outputLogicCheckList6.ScrollAlwaysVisible = true;
            this._outputLogicCheckList6.Size = new System.Drawing.Size(171, 514);
            this._outputLogicCheckList6.TabIndex = 6;
            // 
            // VLS7
            // 
            this.VLS7.Controls.Add(this._outputLogicCheckList7);
            this.VLS7.Location = new System.Drawing.Point(4, 49);
            this.VLS7.Name = "VLS7";
            this.VLS7.Size = new System.Drawing.Size(177, 520);
            this.VLS7.TabIndex = 6;
            this.VLS7.Text = "��� 7";
            this.VLS7.UseVisualStyleBackColor = true;
            // 
            // _outputLogicCheckList7
            // 
            this._outputLogicCheckList7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._outputLogicCheckList7.CheckOnClick = true;
            this._outputLogicCheckList7.FormattingEnabled = true;
            this._outputLogicCheckList7.Location = new System.Drawing.Point(3, 3);
            this._outputLogicCheckList7.Name = "_outputLogicCheckList7";
            this._outputLogicCheckList7.ScrollAlwaysVisible = true;
            this._outputLogicCheckList7.Size = new System.Drawing.Size(171, 514);
            this._outputLogicCheckList7.TabIndex = 6;
            // 
            // VLS8
            // 
            this.VLS8.Controls.Add(this._outputLogicCheckList8);
            this.VLS8.Location = new System.Drawing.Point(4, 49);
            this.VLS8.Name = "VLS8";
            this.VLS8.Size = new System.Drawing.Size(177, 520);
            this.VLS8.TabIndex = 7;
            this.VLS8.Text = "��� 8";
            this.VLS8.UseVisualStyleBackColor = true;
            // 
            // _outputLogicCheckList8
            // 
            this._outputLogicCheckList8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._outputLogicCheckList8.CheckOnClick = true;
            this._outputLogicCheckList8.FormattingEnabled = true;
            this._outputLogicCheckList8.Location = new System.Drawing.Point(3, 3);
            this._outputLogicCheckList8.Name = "_outputLogicCheckList8";
            this._outputLogicCheckList8.ScrollAlwaysVisible = true;
            this._outputLogicCheckList8.Size = new System.Drawing.Size(171, 514);
            this._outputLogicCheckList8.TabIndex = 6;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox22);
            this.groupBox6.Controls.Add(this.groupBox21);
            this.groupBox6.Location = new System.Drawing.Point(638, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(180, 195);
            this.groupBox6.TabIndex = 16;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "���� �������������";
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.label11);
            this.groupBox22.Controls.Add(this._releDispepairBox);
            this.groupBox22.Location = new System.Drawing.Point(6, 12);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(167, 38);
            this.groupBox22.TabIndex = 8;
            this.groupBox22.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "������, ��";
            // 
            // _releDispepairBox
            // 
            this._releDispepairBox.Location = new System.Drawing.Point(79, 12);
            this._releDispepairBox.Name = "_releDispepairBox";
            this._releDispepairBox.Size = new System.Drawing.Size(83, 20);
            this._releDispepairBox.TabIndex = 9;
            this._releDispepairBox.Tag = "3000000";
            this._releDispepairBox.Text = "0";
            this._releDispepairBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._dispepairCheckList);
            this.groupBox21.Location = new System.Drawing.Point(6, 50);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(167, 144);
            this.groupBox21.TabIndex = 8;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "������";
            // 
            // _dispepairCheckList
            // 
            this._dispepairCheckList.CheckOnClick = true;
            this._dispepairCheckList.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dispepairCheckList.FormattingEnabled = true;
            this._dispepairCheckList.Items.AddRange(new object[] {
            "1. ���������� ������.",
            "2. ������ ���",
            "3. ������������� ��",
            "4. ������",
            "5. ������. �����������",
            "6. ������",
            "7. ������. ����������",
            "8. ������. �������"});
            this._dispepairCheckList.Location = new System.Drawing.Point(3, 16);
            this._dispepairCheckList.Name = "_dispepairCheckList";
            this._dispepairCheckList.Size = new System.Drawing.Size(161, 125);
            this._dispepairCheckList.TabIndex = 5;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._outputIndicatorsGrid);
            this.groupBox9.Location = new System.Drawing.Point(8, 368);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(424, 250);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "����������";
            // 
            // _outputIndicatorsGrid
            // 
            this._outputIndicatorsGrid.AllowUserToAddRows = false;
            this._outputIndicatorsGrid.AllowUserToDeleteRows = false;
            this._outputIndicatorsGrid.AllowUserToResizeColumns = false;
            this._outputIndicatorsGrid.AllowUserToResizeRows = false;
            this._outputIndicatorsGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle37.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._outputIndicatorsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle37;
            this._outputIndicatorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputIndicatorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._outIndNumberCol,
            this._outIndTypeCol,
            this._outIndSignalCol,
            this._outIndResetCol,
            this._outIndAlarmCol,
            this._outIndSystemCol});
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._outputIndicatorsGrid.DefaultCellStyle = dataGridViewCellStyle38;
            this._outputIndicatorsGrid.Location = new System.Drawing.Point(8, 14);
            this._outputIndicatorsGrid.Name = "_outputIndicatorsGrid";
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle39.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._outputIndicatorsGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle39;
            this._outputIndicatorsGrid.RowHeadersVisible = false;
            this._outputIndicatorsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputIndicatorsGrid.RowsDefaultCellStyle = dataGridViewCellStyle40;
            this._outputIndicatorsGrid.RowTemplate.Height = 24;
            this._outputIndicatorsGrid.ShowCellErrors = false;
            this._outputIndicatorsGrid.ShowRowErrors = false;
            this._outputIndicatorsGrid.Size = new System.Drawing.Size(410, 230);
            this._outputIndicatorsGrid.TabIndex = 0;
            this._outputIndicatorsGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // _outIndNumberCol
            // 
            this._outIndNumberCol.HeaderText = "�";
            this._outIndNumberCol.Name = "_outIndNumberCol";
            this._outIndNumberCol.ReadOnly = true;
            this._outIndNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndNumberCol.Width = 20;
            // 
            // _outIndTypeCol
            // 
            this._outIndTypeCol.HeaderText = "���";
            this._outIndTypeCol.Items.AddRange(new object[] {
            "�����������",
            "�������",
            "XXXXX"});
            this._outIndTypeCol.Name = "_outIndTypeCol";
            this._outIndTypeCol.Width = 120;
            // 
            // _outIndSignalCol
            // 
            this._outIndSignalCol.HeaderText = "������";
            this._outIndSignalCol.Name = "_outIndSignalCol";
            this._outIndSignalCol.Width = 140;
            // 
            // _outIndResetCol
            // 
            this._outIndResetCol.HeaderText = "����� ���.";
            this._outIndResetCol.Name = "_outIndResetCol";
            this._outIndResetCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._outIndResetCol.Width = 40;
            // 
            // _outIndAlarmCol
            // 
            this._outIndAlarmCol.HeaderText = "����� ��";
            this._outIndAlarmCol.Name = "_outIndAlarmCol";
            this._outIndAlarmCol.Width = 40;
            // 
            // _outIndSystemCol
            // 
            this._outIndSystemCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._outIndSystemCol.HeaderText = "����� ��";
            this._outIndSystemCol.Name = "_outIndSystemCol";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._outputReleGrid);
            this.groupBox8.Location = new System.Drawing.Point(8, 5);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(427, 357);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "�������� ����";
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle41.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle41.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._outputReleGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle41;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this._releSignalCol,
            this._releImpulseCol});
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle42.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle42.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._outputReleGrid.DefaultCellStyle = dataGridViewCellStyle42;
            this._outputReleGrid.Location = new System.Drawing.Point(9, 13);
            this._outputReleGrid.Name = "_outputReleGrid";
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle43.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle43.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle43.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle43.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle43.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._outputReleGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle43;
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle44;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(412, 338);
            this._outputReleGrid.TabIndex = 0;
            this._outputReleGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // _releNumberCol
            // 
            this._releNumberCol.HeaderText = "�";
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 25;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releTypeCol.HeaderText = "���";
            this._releTypeCol.Items.AddRange(new object[] {
            "�����������",
            "�������",
            "XXXXX"});
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 120;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releSignalCol.HeaderText = "������";
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 140;
            // 
            // _releImpulseCol
            // 
            this._releImpulseCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._releImpulseCol.HeaderText = "T�����, ��";
            this._releImpulseCol.Name = "_releImpulseCol";
            this._releImpulseCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _externalDefensePage
            // 
            this._externalDefensePage.Controls.Add(this._externalDefenseGrid);
            this._externalDefensePage.Location = new System.Drawing.Point(4, 22);
            this._externalDefensePage.Name = "_externalDefensePage";
            this._externalDefensePage.Size = new System.Drawing.Size(918, 635);
            this._externalDefensePage.TabIndex = 2;
            this._externalDefensePage.Text = "������� ������";
            this._externalDefensePage.UseVisualStyleBackColor = true;
            // 
            // _externalDefenseGrid
            // 
            this._externalDefenseGrid.AllowUserToAddRows = false;
            this._externalDefenseGrid.AllowUserToDeleteRows = false;
            this._externalDefenseGrid.AllowUserToResizeColumns = false;
            this._externalDefenseGrid.AllowUserToResizeRows = false;
            this._externalDefenseGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._externalDefenseGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._externalDefenseGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._externalDefenseGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle45.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle45.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle45.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle45.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle45.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDefenseGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle45;
            this._externalDefenseGrid.ColumnHeadersHeight = 30;
            this._externalDefenseGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._externalDefenseGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extDefNumberCol,
            this._exDefModeCol,
            this._exDefBlockingCol,
            this._exDefWorkingCol,
            this._exDefWorkingTimeCol,
            this._exDefReturnCol,
            this._exDefAPVreturnCol,
            this._exDefReturnNumberCol,
            this._exDefReturnTimeCol,
            this._exDefOSCcol,
            this._exDefUROVcol,
            this._exDefAPVcol,
            this._exDefAVRcol,
            this._exDefResetcol});
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle47.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle47.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle47.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle47.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle47.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDefenseGrid.DefaultCellStyle = dataGridViewCellStyle47;
            this._externalDefenseGrid.Location = new System.Drawing.Point(3, 3);
            this._externalDefenseGrid.MinimumSize = new System.Drawing.Size(912, 321);
            this._externalDefenseGrid.MultiSelect = false;
            this._externalDefenseGrid.Name = "_externalDefenseGrid";
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle48.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle48.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle48.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle48.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle48.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDefenseGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle48;
            this._externalDefenseGrid.RowHeadersVisible = false;
            this._externalDefenseGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDefenseGrid.RowsDefaultCellStyle = dataGridViewCellStyle49;
            this._externalDefenseGrid.RowTemplate.Height = 24;
            this._externalDefenseGrid.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._externalDefenseGrid.Size = new System.Drawing.Size(912, 321);
            this._externalDefenseGrid.TabIndex = 0;
            this._externalDefenseGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // _extDefNumberCol
            // 
            this._extDefNumberCol.HeaderText = "�";
            this._extDefNumberCol.Name = "_extDefNumberCol";
            this._extDefNumberCol.ReadOnly = true;
            this._extDefNumberCol.Width = 43;
            // 
            // _exDefModeCol
            // 
            this._exDefModeCol.HeaderText = "�����";
            this._exDefModeCol.Name = "_exDefModeCol";
            this._exDefModeCol.Width = 48;
            // 
            // _exDefBlockingCol
            // 
            this._exDefBlockingCol.HeaderText = "����������";
            this._exDefBlockingCol.Name = "_exDefBlockingCol";
            this._exDefBlockingCol.Width = 74;
            // 
            // _exDefWorkingCol
            // 
            dataGridViewCellStyle46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._exDefWorkingCol.DefaultCellStyle = dataGridViewCellStyle46;
            this._exDefWorkingCol.HeaderText = "������������";
            this._exDefWorkingCol.Name = "_exDefWorkingCol";
            this._exDefWorkingCol.Width = 87;
            // 
            // _exDefWorkingTimeCol
            // 
            this._exDefWorkingTimeCol.HeaderText = "����� ������.";
            this._exDefWorkingTimeCol.Name = "_exDefWorkingTimeCol";
            this._exDefWorkingTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._exDefWorkingTimeCol.Width = 87;
            // 
            // _exDefReturnCol
            // 
            this._exDefReturnCol.HeaderText = "����.";
            this._exDefReturnCol.Name = "_exDefReturnCol";
            this._exDefReturnCol.Width = 41;
            // 
            // _exDefAPVreturnCol
            // 
            this._exDefAPVreturnCol.HeaderText = "��� ��";
            this._exDefAPVreturnCol.Name = "_exDefAPVreturnCol";
            this._exDefAPVreturnCol.Width = 52;
            // 
            // _exDefReturnNumberCol
            // 
            this._exDefReturnNumberCol.HeaderText = "���� ��������";
            this._exDefReturnNumberCol.Name = "_exDefReturnNumberCol";
            this._exDefReturnNumberCol.Width = 87;
            // 
            // _exDefReturnTimeCol
            // 
            this._exDefReturnTimeCol.HeaderText = "����� ��������";
            this._exDefReturnTimeCol.Name = "_exDefReturnTimeCol";
            this._exDefReturnTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._exDefReturnTimeCol.Width = 96;
            // 
            // _exDefOSCcol
            // 
            this._exDefOSCcol.HeaderText = "���.";
            this._exDefOSCcol.Name = "_exDefOSCcol";
            this._exDefOSCcol.Visible = false;
            this._exDefOSCcol.Width = 36;
            // 
            // _exDefUROVcol
            // 
            this._exDefUROVcol.HeaderText = "����";
            this._exDefUROVcol.Name = "_exDefUROVcol";
            this._exDefUROVcol.Width = 43;
            // 
            // _exDefAPVcol
            // 
            this._exDefAPVcol.HeaderText = "���";
            this._exDefAPVcol.Name = "_exDefAPVcol";
            this._exDefAPVcol.Width = 35;
            // 
            // _exDefAVRcol
            // 
            this._exDefAVRcol.HeaderText = "���";
            this._exDefAVRcol.Name = "_exDefAVRcol";
            this._exDefAVRcol.Width = 34;
            // 
            // _exDefResetcol
            // 
            this._exDefResetcol.HeaderText = "�����";
            this._exDefResetcol.Name = "_exDefResetcol";
            this._exDefResetcol.Visible = false;
            this._exDefResetcol.Width = 44;
            // 
            // _automaticPage
            // 
            this._automaticPage.Controls.Add(this.groupBox11);
            this._automaticPage.Controls.Add(this.groupBox13);
            this._automaticPage.Controls.Add(this.groupBox14);
            this._automaticPage.Location = new System.Drawing.Point(4, 22);
            this._automaticPage.Name = "_automaticPage";
            this._automaticPage.Size = new System.Drawing.Size(918, 635);
            this._automaticPage.TabIndex = 3;
            this._automaticPage.Text = "����������";
            this._automaticPage.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label58);
            this.groupBox11.Controls.Add(this.label59);
            this.groupBox11.Controls.Add(this.avr_disconnection);
            this.groupBox11.Controls.Add(this.label61);
            this.groupBox11.Controls.Add(this.avr_time_return);
            this.groupBox11.Controls.Add(this.label62);
            this.groupBox11.Controls.Add(this.avr_time_abrasion);
            this.groupBox11.Controls.Add(this.label63);
            this.groupBox11.Controls.Add(this.avr_return);
            this.groupBox11.Controls.Add(this.label64);
            this.groupBox11.Controls.Add(this.label65);
            this.groupBox11.Controls.Add(this.avr_abrasion);
            this.groupBox11.Controls.Add(this.label66);
            this.groupBox11.Controls.Add(this.avr_reset_blocking);
            this.groupBox11.Controls.Add(this.avr_permit_reset_switch);
            this.groupBox11.Controls.Add(this.groupBox12);
            this.groupBox11.Controls.Add(this.avr_blocking);
            this.groupBox11.Controls.Add(this.avr_start);
            this.groupBox11.Location = new System.Drawing.Point(265, 1);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(256, 313);
            this.groupBox11.TabIndex = 24;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "������������ ���";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(9, 288);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(123, 13);
            this.label58.TabIndex = 42;
            this.label58.Text = "����� ����������, ��";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(9, 148);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(131, 13);
            this.label59.TabIndex = 35;
            this.label59.Text = "������ ��� (�� �������)";
            // 
            // avr_disconnection
            // 
            this.avr_disconnection.Location = new System.Drawing.Point(152, 286);
            this.avr_disconnection.Name = "avr_disconnection";
            this.avr_disconnection.Size = new System.Drawing.Size(87, 20);
            this.avr_disconnection.TabIndex = 31;
            this.avr_disconnection.Tag = "3000000";
            this.avr_disconnection.Text = "0";
            this.avr_disconnection.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(9, 268);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(110, 13);
            this.label61.TabIndex = 41;
            this.label61.Text = "����� ��������, ��";
            // 
            // avr_time_return
            // 
            this.avr_time_return.Location = new System.Drawing.Point(152, 266);
            this.avr_time_return.Name = "avr_time_return";
            this.avr_time_return.Size = new System.Drawing.Size(87, 20);
            this.avr_time_return.TabIndex = 30;
            this.avr_time_return.Tag = "3000000";
            this.avr_time_return.Text = "0";
            this.avr_time_return.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(9, 248);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(49, 13);
            this.label62.TabIndex = 40;
            this.label62.Text = "�������";
            // 
            // avr_time_abrasion
            // 
            this.avr_time_abrasion.Location = new System.Drawing.Point(152, 225);
            this.avr_time_abrasion.Name = "avr_time_abrasion";
            this.avr_time_abrasion.Size = new System.Drawing.Size(87, 20);
            this.avr_time_abrasion.TabIndex = 29;
            this.avr_time_abrasion.Tag = "3000000";
            this.avr_time_abrasion.Text = "0";
            this.avr_time_abrasion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(9, 228);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(136, 13);
            this.label63.TabIndex = 39;
            this.label63.Text = "����� ������������, ��";
            // 
            // avr_return
            // 
            this.avr_return.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_return.FormattingEnabled = true;
            this.avr_return.Location = new System.Drawing.Point(152, 245);
            this.avr_return.Name = "avr_return";
            this.avr_return.Size = new System.Drawing.Size(87, 21);
            this.avr_return.TabIndex = 28;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(9, 208);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(81, 13);
            this.label64.TabIndex = 38;
            this.label64.Text = "������������";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(9, 188);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(101, 13);
            this.label65.TabIndex = 37;
            this.label65.Text = "����� ����������";
            // 
            // avr_abrasion
            // 
            this.avr_abrasion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_abrasion.FormattingEnabled = true;
            this.avr_abrasion.Location = new System.Drawing.Point(152, 204);
            this.avr_abrasion.Name = "avr_abrasion";
            this.avr_abrasion.Size = new System.Drawing.Size(87, 21);
            this.avr_abrasion.TabIndex = 26;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(9, 168);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(68, 13);
            this.label66.TabIndex = 36;
            this.label66.Text = "����������";
            // 
            // avr_reset_blocking
            // 
            this.avr_reset_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_reset_blocking.FormattingEnabled = true;
            this.avr_reset_blocking.Location = new System.Drawing.Point(152, 183);
            this.avr_reset_blocking.Name = "avr_reset_blocking";
            this.avr_reset_blocking.Size = new System.Drawing.Size(87, 21);
            this.avr_reset_blocking.TabIndex = 24;
            // 
            // avr_permit_reset_switch
            // 
            this.avr_permit_reset_switch.Location = new System.Drawing.Point(12, 115);
            this.avr_permit_reset_switch.Name = "avr_permit_reset_switch";
            this.avr_permit_reset_switch.Size = new System.Drawing.Size(238, 24);
            this.avr_permit_reset_switch.TabIndex = 22;
            this.avr_permit_reset_switch.Text = "���������� ������ �� ���. �����������";
            this.avr_permit_reset_switch.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.avr_abrasion_switch);
            this.groupBox12.Controls.Add(this.avr_supply_off);
            this.groupBox12.Controls.Add(this.avr_self_off);
            this.groupBox12.Controls.Add(this.avr_switch_off);
            this.groupBox12.Location = new System.Drawing.Point(6, 13);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(233, 100);
            this.groupBox12.TabIndex = 19;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "���������� �������";
            // 
            // avr_abrasion_switch
            // 
            this.avr_abrasion_switch.Location = new System.Drawing.Point(6, 72);
            this.avr_abrasion_switch.Name = "avr_abrasion_switch";
            this.avr_abrasion_switch.Size = new System.Drawing.Size(186, 24);
            this.avr_abrasion_switch.TabIndex = 21;
            this.avr_abrasion_switch.Text = "�� ������";
            this.avr_abrasion_switch.UseVisualStyleBackColor = true;
            // 
            // avr_supply_off
            // 
            this.avr_supply_off.Location = new System.Drawing.Point(6, 15);
            this.avr_supply_off.Name = "avr_supply_off";
            this.avr_supply_off.Size = new System.Drawing.Size(139, 24);
            this.avr_supply_off.TabIndex = 18;
            this.avr_supply_off.Text = "�� �������";
            this.avr_supply_off.UseVisualStyleBackColor = true;
            // 
            // avr_self_off
            // 
            this.avr_self_off.Location = new System.Drawing.Point(6, 53);
            this.avr_self_off.Name = "avr_self_off";
            this.avr_self_off.Size = new System.Drawing.Size(186, 24);
            this.avr_self_off.TabIndex = 20;
            this.avr_self_off.Text = "��������������";
            this.avr_self_off.UseVisualStyleBackColor = true;
            // 
            // avr_switch_off
            // 
            this.avr_switch_off.Location = new System.Drawing.Point(6, 34);
            this.avr_switch_off.Name = "avr_switch_off";
            this.avr_switch_off.Size = new System.Drawing.Size(221, 24);
            this.avr_switch_off.TabIndex = 19;
            this.avr_switch_off.Text = "�� ����������";
            this.avr_switch_off.UseVisualStyleBackColor = true;
            // 
            // avr_blocking
            // 
            this.avr_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_blocking.FormattingEnabled = true;
            this.avr_blocking.Location = new System.Drawing.Point(152, 162);
            this.avr_blocking.Name = "avr_blocking";
            this.avr_blocking.Size = new System.Drawing.Size(87, 21);
            this.avr_blocking.TabIndex = 3;
            // 
            // avr_start
            // 
            this.avr_start.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_start.FormattingEnabled = true;
            this.avr_start.Location = new System.Drawing.Point(152, 141);
            this.avr_start.Name = "avr_start";
            this.avr_start.Size = new System.Drawing.Size(87, 21);
            this.avr_start.TabIndex = 1;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.lzsh_confV114);
            this.groupBox13.Controls.Add(this.label57);
            this.groupBox13.Controls.Add(this.lzsh_constraint);
            this.groupBox13.Controls.Add(this.lzsh_conf);
            this.groupBox13.Controls.Add(this.label48);
            this.groupBox13.Location = new System.Drawing.Point(3, 228);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(256, 86);
            this.groupBox13.TabIndex = 23;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "��������� ���";
            // 
            // lzsh_confV114
            // 
            this.lzsh_confV114.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lzsh_confV114.FormattingEnabled = true;
            this.lzsh_confV114.Location = new System.Drawing.Point(6, 36);
            this.lzsh_confV114.Name = "lzsh_confV114";
            this.lzsh_confV114.Size = new System.Drawing.Size(87, 21);
            this.lzsh_confV114.TabIndex = 28;
            this.lzsh_confV114.Visible = false;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(157, 18);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(65, 13);
            this.label57.TabIndex = 27;
            this.label57.Text = "�������, I�";
            // 
            // lzsh_constraint
            // 
            this.lzsh_constraint.Location = new System.Drawing.Point(146, 36);
            this.lzsh_constraint.Name = "lzsh_constraint";
            this.lzsh_constraint.Size = new System.Drawing.Size(87, 20);
            this.lzsh_constraint.TabIndex = 24;
            this.lzsh_constraint.Tag = "40";
            this.lzsh_constraint.Text = "0";
            this.lzsh_constraint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lzsh_conf
            // 
            this.lzsh_conf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lzsh_conf.FormattingEnabled = true;
            this.lzsh_conf.Location = new System.Drawing.Point(6, 36);
            this.lzsh_conf.Name = "lzsh_conf";
            this.lzsh_conf.Size = new System.Drawing.Size(87, 21);
            this.lzsh_conf.TabIndex = 18;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(9, 18);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(80, 13);
            this.label48.TabIndex = 26;
            this.label48.Text = "������������";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.label55);
            this.groupBox14.Controls.Add(this.label47);
            this.groupBox14.Controls.Add(this.label56);
            this.groupBox14.Controls.Add(this.apv_time_4krat);
            this.groupBox14.Controls.Add(this.label52);
            this.groupBox14.Controls.Add(this.apv_time_3krat);
            this.groupBox14.Controls.Add(this.label53);
            this.groupBox14.Controls.Add(this.apv_time_2krat);
            this.groupBox14.Controls.Add(this.label54);
            this.groupBox14.Controls.Add(this.apv_time_1krat);
            this.groupBox14.Controls.Add(this.label50);
            this.groupBox14.Controls.Add(this.label51);
            this.groupBox14.Controls.Add(this.apv_time_ready);
            this.groupBox14.Controls.Add(this.label49);
            this.groupBox14.Controls.Add(this.apv_time_blocking);
            this.groupBox14.Controls.Add(this.apv_self_off);
            this.groupBox14.Controls.Add(this.apv_blocking);
            this.groupBox14.Controls.Add(this.apv_conf);
            this.groupBox14.Location = new System.Drawing.Point(3, 1);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(256, 221);
            this.groupBox14.TabIndex = 22;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "������������ ���";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(8, 106);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(101, 13);
            this.label54.TabIndex = 30;
            this.label54.Text = "����� 1 �����, ��";
            // 
            // apv_time_1krat
            // 
            this.apv_time_1krat.Location = new System.Drawing.Point(146, 101);
            this.apv_time_1krat.Name = "apv_time_1krat";
            this.apv_time_1krat.Size = new System.Drawing.Size(87, 20);
            this.apv_time_1krat.TabIndex = 20;
            this.apv_time_1krat.Tag = "3000000";
            this.apv_time_1krat.Text = "0";
            this.apv_time_1krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(8, 86);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(120, 13);
            this.label50.TabIndex = 29;
            this.label50.Text = "����� ����������, ��";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(8, 66);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(123, 13);
            this.label51.TabIndex = 28;
            this.label51.Text = "����� ����������, ��";
            // 
            // apv_time_ready
            // 
            this.apv_time_ready.Location = new System.Drawing.Point(146, 81);
            this.apv_time_ready.Name = "apv_time_ready";
            this.apv_time_ready.Size = new System.Drawing.Size(87, 20);
            this.apv_time_ready.TabIndex = 19;
            this.apv_time_ready.Tag = "3000000";
            this.apv_time_ready.Text = "0";
            this.apv_time_ready.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(8, 46);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(68, 13);
            this.label49.TabIndex = 27;
            this.label49.Text = "����������";
            // 
            // apv_time_blocking
            // 
            this.apv_time_blocking.Location = new System.Drawing.Point(146, 61);
            this.apv_time_blocking.Name = "apv_time_blocking";
            this.apv_time_blocking.Size = new System.Drawing.Size(87, 20);
            this.apv_time_blocking.TabIndex = 18;
            this.apv_time_blocking.Tag = "3000000";
            this.apv_time_blocking.Text = "0";
            this.apv_time_blocking.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // apv_self_off
            // 
            this.apv_self_off.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apv_self_off.FormattingEnabled = true;
            this.apv_self_off.Location = new System.Drawing.Point(146, 181);
            this.apv_self_off.Name = "apv_self_off";
            this.apv_self_off.Size = new System.Drawing.Size(87, 21);
            this.apv_self_off.TabIndex = 17;
            // 
            // _defendTabPage
            // 
            this._defendTabPage.Controls.Add(this.groupBox27);
            this._defendTabPage.Controls.Add(this.groupBox24);
            this._defendTabPage.Location = new System.Drawing.Point(4, 22);
            this._defendTabPage.Name = "_defendTabPage";
            this._defendTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._defendTabPage.Size = new System.Drawing.Size(918, 635);
            this._defendTabPage.TabIndex = 8;
            this._defendTabPage.Text = "������";
            this._defendTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox27
            // 
            this.groupBox27.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox27.Controls.Add(this.tabControl1);
            this.groupBox27.Location = new System.Drawing.Point(9, 69);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(899, 560);
            this.groupBox27.TabIndex = 1;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "������";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this._tokDefendTabPage);
            this.tabControl1.Controls.Add(this._defendVoltageTabPage);
            this.tabControl1.Controls.Add(this._frequencyDefendTabPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 16);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(893, 541);
            this.tabControl1.TabIndex = 0;
            // 
            // _tokDefendTabPage
            // 
            this._tokDefendTabPage.Controls.Add(this.groupBox15);
            this._tokDefendTabPage.Controls.Add(this._tokDefenseGrid4);
            this._tokDefendTabPage.Controls.Add(this._tokDefenseGrid3);
            this._tokDefendTabPage.Controls.Add(this._tokDefenseGrid1);
            this._tokDefendTabPage.Location = new System.Drawing.Point(4, 22);
            this._tokDefendTabPage.Name = "_tokDefendTabPage";
            this._tokDefendTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._tokDefendTabPage.Size = new System.Drawing.Size(885, 515);
            this._tokDefendTabPage.TabIndex = 0;
            this._tokDefendTabPage.Text = "������� ������";
            this._tokDefendTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this._tokDefenseInbox);
            this.groupBox15.Controls.Add(this.label24);
            this.groupBox15.Controls.Add(this._tokDefenseI2box);
            this.groupBox15.Controls.Add(this.label23);
            this.groupBox15.Controls.Add(this._tokDefenseI0box);
            this.groupBox15.Controls.Add(this.label22);
            this.groupBox15.Controls.Add(this._tokDefenseIbox);
            this.groupBox15.Controls.Add(this.label21);
            this.groupBox15.Location = new System.Drawing.Point(6, 6);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(261, 41);
            this.groupBox15.TabIndex = 8;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "������������ ���� (���� ��)";
            // 
            // _tokDefenseInbox
            // 
            this._tokDefenseInbox.Location = new System.Drawing.Point(216, 15);
            this._tokDefenseInbox.Name = "_tokDefenseInbox";
            this._tokDefenseInbox.Size = new System.Drawing.Size(37, 20);
            this._tokDefenseInbox.TabIndex = 7;
            this._tokDefenseInbox.Tag = "360";
            this._tokDefenseInbox.Text = "0";
            this._tokDefenseInbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(200, 18);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(16, 13);
            this.label24.TabIndex = 6;
            this.label24.Text = "In";
            // 
            // _tokDefenseI2box
            // 
            this._tokDefenseI2box.Location = new System.Drawing.Point(152, 15);
            this._tokDefenseI2box.Name = "_tokDefenseI2box";
            this._tokDefenseI2box.Size = new System.Drawing.Size(37, 20);
            this._tokDefenseI2box.TabIndex = 5;
            this._tokDefenseI2box.Tag = "360";
            this._tokDefenseI2box.Text = "0";
            this._tokDefenseI2box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(136, 18);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(16, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "I2";
            // 
            // _tokDefenseI0box
            // 
            this._tokDefenseI0box.Location = new System.Drawing.Point(91, 15);
            this._tokDefenseI0box.Name = "_tokDefenseI0box";
            this._tokDefenseI0box.Size = new System.Drawing.Size(37, 20);
            this._tokDefenseI0box.TabIndex = 3;
            this._tokDefenseI0box.Tag = "360";
            this._tokDefenseI0box.Text = "0";
            this._tokDefenseI0box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(75, 18);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(16, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "I0";
            // 
            // _tokDefenseIbox
            // 
            this._tokDefenseIbox.Location = new System.Drawing.Point(31, 15);
            this._tokDefenseIbox.Name = "_tokDefenseIbox";
            this._tokDefenseIbox.Size = new System.Drawing.Size(37, 20);
            this._tokDefenseIbox.TabIndex = 1;
            this._tokDefenseIbox.Tag = "360";
            this._tokDefenseIbox.Text = "0";
            this._tokDefenseIbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(15, 18);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(10, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "I";
            // 
            // _tokDefenseGrid4
            // 
            this._tokDefenseGrid4.AllowUserToAddRows = false;
            this._tokDefenseGrid4.AllowUserToDeleteRows = false;
            this._tokDefenseGrid4.AllowUserToResizeColumns = false;
            this._tokDefenseGrid4.AllowUserToResizeRows = false;
            this._tokDefenseGrid4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tokDefenseGrid4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid4.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle50.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle50.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle50.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle50.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle50.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid4.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle50;
            this._tokDefenseGrid4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefense4NameCol,
            this._tokDefense4ModeCol,
            this._tokDefense4BlockNumberCol,
            this._tokDefense4UpuskCol,
            this._tokDefense4PuskConstraintCol,
            this._tokDefense4WorkConstraintCol,
            this._tokDefense4WorkTimeCol,
            this._tokDefense4SpeedupCol,
            this._tokDefense4SpeedupTimeCol,
            this._tokDefense4OSCCol,
            this._tokDefense4OSCv11Col,
            this._tokDefense4UROVCol,
            this._tokDefense4APVCol,
            this._tokDefense4AVRCol});
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle54.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle54.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle54.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle54.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle54.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._tokDefenseGrid4.DefaultCellStyle = dataGridViewCellStyle54;
            this._tokDefenseGrid4.Location = new System.Drawing.Point(6, 396);
            this._tokDefenseGrid4.MultiSelect = false;
            this._tokDefenseGrid4.Name = "_tokDefenseGrid4";
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle55.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle55.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle55.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle55.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle55.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid4.RowHeadersDefaultCellStyle = dataGridViewCellStyle55;
            this._tokDefenseGrid4.RowHeadersVisible = false;
            this._tokDefenseGrid4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid4.RowsDefaultCellStyle = dataGridViewCellStyle56;
            this._tokDefenseGrid4.RowTemplate.Height = 24;
            this._tokDefenseGrid4.Size = new System.Drawing.Size(873, 116);
            this._tokDefenseGrid4.TabIndex = 7;
            this._tokDefenseGrid4.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // _tokDefense4NameCol
            // 
            this._tokDefense4NameCol.Frozen = true;
            this._tokDefense4NameCol.HeaderText = "";
            this._tokDefense4NameCol.Name = "_tokDefense4NameCol";
            this._tokDefense4NameCol.ReadOnly = true;
            this._tokDefense4NameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4NameCol.Width = 5;
            // 
            // _tokDefense4ModeCol
            // 
            this._tokDefense4ModeCol.HeaderText = "�����";
            this._tokDefense4ModeCol.Name = "_tokDefense4ModeCol";
            this._tokDefense4ModeCol.Width = 48;
            // 
            // _tokDefense4BlockNumberCol
            // 
            this._tokDefense4BlockNumberCol.HeaderText = "����������";
            this._tokDefense4BlockNumberCol.Name = "_tokDefense4BlockNumberCol";
            this._tokDefense4BlockNumberCol.Width = 74;
            // 
            // _tokDefense4UpuskCol
            // 
            this._tokDefense4UpuskCol.HeaderText = "���� �� U";
            this._tokDefense4UpuskCol.Name = "_tokDefense4UpuskCol";
            this._tokDefense4UpuskCol.Width = 50;
            // 
            // _tokDefense4PuskConstraintCol
            // 
            dataGridViewCellStyle51.NullValue = null;
            this._tokDefense4PuskConstraintCol.DefaultCellStyle = dataGridViewCellStyle51;
            this._tokDefense4PuskConstraintCol.HeaderText = "������� �����, �";
            this._tokDefense4PuskConstraintCol.Name = "_tokDefense4PuskConstraintCol";
            this._tokDefense4PuskConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4PuskConstraintCol.Width = 82;
            // 
            // _tokDefense4WorkConstraintCol
            // 
            dataGridViewCellStyle52.NullValue = null;
            this._tokDefense4WorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle52;
            this._tokDefense4WorkConstraintCol.HeaderText = "���.����., I�";
            this._tokDefense4WorkConstraintCol.Name = "_tokDefense4WorkConstraintCol";
            this._tokDefense4WorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4WorkConstraintCol.Width = 69;
            // 
            // _tokDefense4WorkTimeCol
            // 
            dataGridViewCellStyle53.NullValue = null;
            this._tokDefense4WorkTimeCol.DefaultCellStyle = dataGridViewCellStyle53;
            this._tokDefense4WorkTimeCol.HeaderText = "t����, ��/����.";
            this._tokDefense4WorkTimeCol.Name = "_tokDefense4WorkTimeCol";
            this._tokDefense4WorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4WorkTimeCol.Width = 85;
            // 
            // _tokDefense4SpeedupCol
            // 
            this._tokDefense4SpeedupCol.HeaderText = "���.";
            this._tokDefense4SpeedupCol.Name = "_tokDefense4SpeedupCol";
            this._tokDefense4SpeedupCol.Width = 36;
            // 
            // _tokDefense4SpeedupTimeCol
            // 
            this._tokDefense4SpeedupTimeCol.HeaderText = "t���, ��";
            this._tokDefense4SpeedupTimeCol.Name = "_tokDefense4SpeedupTimeCol";
            this._tokDefense4SpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4SpeedupTimeCol.Width = 48;
            // 
            // _tokDefense4OSCCol
            // 
            this._tokDefense4OSCCol.HeaderText = "Osc";
            this._tokDefense4OSCCol.Name = "_tokDefense4OSCCol";
            this._tokDefense4OSCCol.Visible = false;
            this._tokDefense4OSCCol.Width = 32;
            // 
            // _tokDefense4OSCv11Col
            // 
            this._tokDefense4OSCv11Col.HeaderText = "�����������";
            this._tokDefense4OSCv11Col.Name = "_tokDefense4OSCv11Col";
            this._tokDefense4OSCv11Col.Visible = false;
            this._tokDefense4OSCv11Col.Width = 82;
            // 
            // _tokDefense4UROVCol
            // 
            this._tokDefense4UROVCol.HeaderText = "����";
            this._tokDefense4UROVCol.Name = "_tokDefense4UROVCol";
            this._tokDefense4UROVCol.Width = 43;
            // 
            // _tokDefense4APVCol
            // 
            this._tokDefense4APVCol.HeaderText = "���";
            this._tokDefense4APVCol.Name = "_tokDefense4APVCol";
            this._tokDefense4APVCol.Width = 35;
            // 
            // _tokDefense4AVRCol
            // 
            this._tokDefense4AVRCol.HeaderText = "���";
            this._tokDefense4AVRCol.Name = "_tokDefense4AVRCol";
            this._tokDefense4AVRCol.Width = 34;
            // 
            // _tokDefenseGrid3
            // 
            this._tokDefenseGrid3.AllowUserToAddRows = false;
            this._tokDefenseGrid3.AllowUserToDeleteRows = false;
            this._tokDefenseGrid3.AllowUserToResizeColumns = false;
            this._tokDefenseGrid3.AllowUserToResizeRows = false;
            this._tokDefenseGrid3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tokDefenseGrid3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid3.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._tokDefenseGrid3.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle57.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle57.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle57.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle57.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle57.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle57;
            this._tokDefenseGrid3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefense3NameCol,
            this._tokDefense3ModeCol,
            this._tokDefense3BlockingNumberCol,
            this._tokDefense3UpuskCol,
            this._tokDefense3PuskConstraintCol,
            this._tokDefense3DirectionCol,
            this._tokDefense3BlockingExistCol,
            this._tokDefense3ParameterCol,
            this._tokDefense3WorkConstraintCol,
            this._tokDefense3FeatureCol,
            this._tokDefense3WorkTimeCol,
            this._tokDefense3SpeedupCol,
            this._tokDefense3SpeedupTimeCol,
            this._tokDefense3OSCCol,
            this._tokDefense3OSCv11Col,
            this._tokDefense3UROVCol,
            this._tokDefense3APVCol,
            this._tokDefense3AVRCol});
            dataGridViewCellStyle62.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle62.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle62.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle62.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle62.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle62.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle62.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._tokDefenseGrid3.DefaultCellStyle = dataGridViewCellStyle62;
            this._tokDefenseGrid3.Location = new System.Drawing.Point(6, 203);
            this._tokDefenseGrid3.MultiSelect = false;
            this._tokDefenseGrid3.Name = "_tokDefenseGrid3";
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle63.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle63.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle63.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle63.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle63.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle63.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid3.RowHeadersDefaultCellStyle = dataGridViewCellStyle63;
            this._tokDefenseGrid3.RowHeadersVisible = false;
            this._tokDefenseGrid3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid3.RowsDefaultCellStyle = dataGridViewCellStyle64;
            this._tokDefenseGrid3.RowTemplate.Height = 24;
            this._tokDefenseGrid3.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._tokDefenseGrid3.Size = new System.Drawing.Size(873, 187);
            this._tokDefenseGrid3.TabIndex = 6;
            this._tokDefenseGrid3.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // _tokDefense3NameCol
            // 
            this._tokDefense3NameCol.Frozen = true;
            this._tokDefense3NameCol.HeaderText = "";
            this._tokDefense3NameCol.Name = "_tokDefense3NameCol";
            this._tokDefense3NameCol.ReadOnly = true;
            this._tokDefense3NameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3NameCol.Width = 5;
            // 
            // _tokDefense3ModeCol
            // 
            this._tokDefense3ModeCol.HeaderText = "�����";
            this._tokDefense3ModeCol.Name = "_tokDefense3ModeCol";
            this._tokDefense3ModeCol.Width = 48;
            // 
            // _tokDefense3BlockingNumberCol
            // 
            this._tokDefense3BlockingNumberCol.HeaderText = "����������";
            this._tokDefense3BlockingNumberCol.Name = "_tokDefense3BlockingNumberCol";
            this._tokDefense3BlockingNumberCol.Width = 74;
            // 
            // _tokDefense3UpuskCol
            // 
            this._tokDefense3UpuskCol.HeaderText = "���� �� U";
            this._tokDefense3UpuskCol.Name = "_tokDefense3UpuskCol";
            this._tokDefense3UpuskCol.Width = 50;
            // 
            // _tokDefense3PuskConstraintCol
            // 
            dataGridViewCellStyle58.NullValue = null;
            this._tokDefense3PuskConstraintCol.DefaultCellStyle = dataGridViewCellStyle58;
            this._tokDefense3PuskConstraintCol.HeaderText = "���. �����, �";
            this._tokDefense3PuskConstraintCol.Name = "_tokDefense3PuskConstraintCol";
            this._tokDefense3PuskConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3PuskConstraintCol.Width = 63;
            // 
            // _tokDefense3DirectionCol
            // 
            this._tokDefense3DirectionCol.HeaderText = "�����������";
            this._tokDefense3DirectionCol.Name = "_tokDefense3DirectionCol";
            this._tokDefense3DirectionCol.Width = 81;
            // 
            // _tokDefense3BlockingExistCol
            // 
            this._tokDefense3BlockingExistCol.HeaderText = "����������";
            this._tokDefense3BlockingExistCol.Name = "_tokDefense3BlockingExistCol";
            this._tokDefense3BlockingExistCol.Width = 74;
            // 
            // _tokDefense3ParameterCol
            // 
            this._tokDefense3ParameterCol.HeaderText = "��������";
            this._tokDefense3ParameterCol.Name = "_tokDefense3ParameterCol";
            this._tokDefense3ParameterCol.Width = 64;
            // 
            // _tokDefense3WorkConstraintCol
            // 
            dataGridViewCellStyle59.NullValue = null;
            this._tokDefense3WorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle59;
            this._tokDefense3WorkConstraintCol.HeaderText = "���.����., I�(��)";
            this._tokDefense3WorkConstraintCol.Name = "_tokDefense3WorkConstraintCol";
            this._tokDefense3WorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3WorkConstraintCol.Width = 86;
            // 
            // _tokDefense3FeatureCol
            // 
            this._tokDefense3FeatureCol.HeaderText = "���-��";
            this._tokDefense3FeatureCol.Name = "_tokDefense3FeatureCol";
            this._tokDefense3FeatureCol.Width = 47;
            // 
            // _tokDefense3WorkTimeCol
            // 
            dataGridViewCellStyle60.NullValue = null;
            this._tokDefense3WorkTimeCol.DefaultCellStyle = dataGridViewCellStyle60;
            this._tokDefense3WorkTimeCol.HeaderText = "t����, ��/����.";
            this._tokDefense3WorkTimeCol.Name = "_tokDefense3WorkTimeCol";
            this._tokDefense3WorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3WorkTimeCol.Width = 85;
            // 
            // _tokDefense3SpeedupCol
            // 
            this._tokDefense3SpeedupCol.HeaderText = "���.";
            this._tokDefense3SpeedupCol.Name = "_tokDefense3SpeedupCol";
            this._tokDefense3SpeedupCol.Width = 36;
            // 
            // _tokDefense3SpeedupTimeCol
            // 
            dataGridViewCellStyle61.NullValue = null;
            this._tokDefense3SpeedupTimeCol.DefaultCellStyle = dataGridViewCellStyle61;
            this._tokDefense3SpeedupTimeCol.HeaderText = "t���, ��";
            this._tokDefense3SpeedupTimeCol.Name = "_tokDefense3SpeedupTimeCol";
            this._tokDefense3SpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3SpeedupTimeCol.Width = 48;
            // 
            // _tokDefense3OSCCol
            // 
            this._tokDefense3OSCCol.HeaderText = "���.";
            this._tokDefense3OSCCol.Name = "_tokDefense3OSCCol";
            this._tokDefense3OSCCol.Visible = false;
            this._tokDefense3OSCCol.Width = 36;
            // 
            // _tokDefense3OSCv11Col
            // 
            this._tokDefense3OSCv11Col.HeaderText = "�����������";
            this._tokDefense3OSCv11Col.Name = "_tokDefense3OSCv11Col";
            this._tokDefense3OSCv11Col.Visible = false;
            this._tokDefense3OSCv11Col.Width = 82;
            // 
            // _tokDefense3UROVCol
            // 
            this._tokDefense3UROVCol.HeaderText = "����";
            this._tokDefense3UROVCol.Name = "_tokDefense3UROVCol";
            this._tokDefense3UROVCol.Width = 43;
            // 
            // _tokDefense3APVCol
            // 
            this._tokDefense3APVCol.HeaderText = "���";
            this._tokDefense3APVCol.Name = "_tokDefense3APVCol";
            this._tokDefense3APVCol.Width = 35;
            // 
            // _tokDefense3AVRCol
            // 
            this._tokDefense3AVRCol.HeaderText = "���";
            this._tokDefense3AVRCol.Name = "_tokDefense3AVRCol";
            this._tokDefense3AVRCol.Width = 34;
            // 
            // _tokDefenseGrid1
            // 
            this._tokDefenseGrid1.AllowUserToAddRows = false;
            this._tokDefenseGrid1.AllowUserToDeleteRows = false;
            this._tokDefenseGrid1.AllowUserToResizeColumns = false;
            this._tokDefenseGrid1.AllowUserToResizeRows = false;
            this._tokDefenseGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tokDefenseGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._tokDefenseGrid1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle65.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle65.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle65.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle65.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle65.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle65.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle65.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle65;
            this._tokDefenseGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefenseNameCol,
            this._tokDefenseModeCol,
            this._tokDefenseBlockNumberCol,
            this._tokDefenseU_PuskCol,
            this._tokDefensePuskConstraintCol,
            this._tokDefenseDirectionCol,
            this._tokDefenseBlockExistCol,
            this._tokDefenseParameterCol,
            this._tokDefenseWorkConstraintCol,
            this._tokDefenseFeatureCol,
            this._tokDefenseWorkTimeCol,
            this._tokDefenseSpeedUpCol,
            this._tokDefenseSpeedupTimeCol,
            this._tokDefenseOSCCol,
            this._tokDefenseOSCv11Col,
            this._tokDefenseUROVCol,
            this._tokDefenseAPVCol,
            this._tokDefenseAVRCol});
            dataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle70.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle70.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle70.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle70.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle70.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._tokDefenseGrid1.DefaultCellStyle = dataGridViewCellStyle70;
            this._tokDefenseGrid1.Location = new System.Drawing.Point(6, 53);
            this._tokDefenseGrid1.MultiSelect = false;
            this._tokDefenseGrid1.Name = "_tokDefenseGrid1";
            dataGridViewCellStyle71.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle71.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle71.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle71.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle71.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle71.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle71;
            this._tokDefenseGrid1.RowHeadersVisible = false;
            this._tokDefenseGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid1.RowsDefaultCellStyle = dataGridViewCellStyle72;
            this._tokDefenseGrid1.RowTemplate.Height = 24;
            this._tokDefenseGrid1.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._tokDefenseGrid1.Size = new System.Drawing.Size(873, 144);
            this._tokDefenseGrid1.TabIndex = 5;
            this._tokDefenseGrid1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // _tokDefenseNameCol
            // 
            this._tokDefenseNameCol.Frozen = true;
            this._tokDefenseNameCol.HeaderText = "";
            this._tokDefenseNameCol.Name = "_tokDefenseNameCol";
            this._tokDefenseNameCol.ReadOnly = true;
            this._tokDefenseNameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseNameCol.Width = 5;
            // 
            // _tokDefenseModeCol
            // 
            this._tokDefenseModeCol.HeaderText = "�����";
            this._tokDefenseModeCol.Name = "_tokDefenseModeCol";
            this._tokDefenseModeCol.Width = 48;
            // 
            // _tokDefenseBlockNumberCol
            // 
            this._tokDefenseBlockNumberCol.HeaderText = "����������";
            this._tokDefenseBlockNumberCol.Name = "_tokDefenseBlockNumberCol";
            this._tokDefenseBlockNumberCol.Width = 74;
            // 
            // _tokDefenseU_PuskCol
            // 
            this._tokDefenseU_PuskCol.HeaderText = "���� �� U";
            this._tokDefenseU_PuskCol.Name = "_tokDefenseU_PuskCol";
            this._tokDefenseU_PuskCol.Width = 50;
            // 
            // _tokDefensePuskConstraintCol
            // 
            dataGridViewCellStyle66.NullValue = null;
            this._tokDefensePuskConstraintCol.DefaultCellStyle = dataGridViewCellStyle66;
            this._tokDefensePuskConstraintCol.HeaderText = "���. �����, �";
            this._tokDefensePuskConstraintCol.Name = "_tokDefensePuskConstraintCol";
            this._tokDefensePuskConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefensePuskConstraintCol.Width = 63;
            // 
            // _tokDefenseDirectionCol
            // 
            this._tokDefenseDirectionCol.HeaderText = "�����������";
            this._tokDefenseDirectionCol.Name = "_tokDefenseDirectionCol";
            this._tokDefenseDirectionCol.Width = 81;
            // 
            // _tokDefenseBlockExistCol
            // 
            this._tokDefenseBlockExistCol.HeaderText = "����������";
            this._tokDefenseBlockExistCol.Name = "_tokDefenseBlockExistCol";
            this._tokDefenseBlockExistCol.Width = 74;
            // 
            // _tokDefenseParameterCol
            // 
            this._tokDefenseParameterCol.HeaderText = "��������";
            this._tokDefenseParameterCol.Name = "_tokDefenseParameterCol";
            this._tokDefenseParameterCol.Width = 64;
            // 
            // _tokDefenseWorkConstraintCol
            // 
            dataGridViewCellStyle67.NullValue = null;
            this._tokDefenseWorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle67;
            this._tokDefenseWorkConstraintCol.HeaderText = "���.����., I�";
            this._tokDefenseWorkConstraintCol.Name = "_tokDefenseWorkConstraintCol";
            this._tokDefenseWorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseWorkConstraintCol.Width = 69;
            // 
            // _tokDefenseFeatureCol
            // 
            this._tokDefenseFeatureCol.HeaderText = "���-��";
            this._tokDefenseFeatureCol.Name = "_tokDefenseFeatureCol";
            this._tokDefenseFeatureCol.Width = 47;
            // 
            // _tokDefenseWorkTimeCol
            // 
            dataGridViewCellStyle68.NullValue = null;
            this._tokDefenseWorkTimeCol.DefaultCellStyle = dataGridViewCellStyle68;
            this._tokDefenseWorkTimeCol.HeaderText = "t����, ��/����.";
            this._tokDefenseWorkTimeCol.Name = "_tokDefenseWorkTimeCol";
            this._tokDefenseWorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseWorkTimeCol.Width = 85;
            // 
            // _tokDefenseSpeedUpCol
            // 
            this._tokDefenseSpeedUpCol.HeaderText = "���.";
            this._tokDefenseSpeedUpCol.Name = "_tokDefenseSpeedUpCol";
            this._tokDefenseSpeedUpCol.Width = 36;
            // 
            // _tokDefenseSpeedupTimeCol
            // 
            dataGridViewCellStyle69.NullValue = null;
            this._tokDefenseSpeedupTimeCol.DefaultCellStyle = dataGridViewCellStyle69;
            this._tokDefenseSpeedupTimeCol.HeaderText = "t���, ��";
            this._tokDefenseSpeedupTimeCol.Name = "_tokDefenseSpeedupTimeCol";
            this._tokDefenseSpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseSpeedupTimeCol.Width = 48;
            // 
            // _tokDefenseOSCCol
            // 
            this._tokDefenseOSCCol.HeaderText = "���.";
            this._tokDefenseOSCCol.Name = "_tokDefenseOSCCol";
            this._tokDefenseOSCCol.Visible = false;
            this._tokDefenseOSCCol.Width = 36;
            // 
            // _tokDefenseOSCv11Col
            // 
            this._tokDefenseOSCv11Col.HeaderText = "����������";
            this._tokDefenseOSCv11Col.Name = "_tokDefenseOSCv11Col";
            this._tokDefenseOSCv11Col.Visible = false;
            this._tokDefenseOSCv11Col.Width = 76;
            // 
            // _tokDefenseUROVCol
            // 
            this._tokDefenseUROVCol.HeaderText = "����";
            this._tokDefenseUROVCol.Name = "_tokDefenseUROVCol";
            this._tokDefenseUROVCol.Width = 43;
            // 
            // _tokDefenseAPVCol
            // 
            this._tokDefenseAPVCol.HeaderText = "���";
            this._tokDefenseAPVCol.Name = "_tokDefenseAPVCol";
            this._tokDefenseAPVCol.Width = 35;
            // 
            // _tokDefenseAVRCol
            // 
            this._tokDefenseAVRCol.HeaderText = "���";
            this._tokDefenseAVRCol.Name = "_tokDefenseAVRCol";
            this._tokDefenseAVRCol.Width = 34;
            // 
            // _defendVoltageTabPage
            // 
            this._defendVoltageTabPage.Controls.Add(this._blockDDL);
            this._defendVoltageTabPage.Controls.Add(this._voltageDefensesGrid2);
            this._defendVoltageTabPage.Controls.Add(this._voltageDefensesGrid1);
            this._defendVoltageTabPage.Controls.Add(this._voltageDefensesGrid3);
            this._defendVoltageTabPage.Location = new System.Drawing.Point(4, 22);
            this._defendVoltageTabPage.Name = "_defendVoltageTabPage";
            this._defendVoltageTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._defendVoltageTabPage.Size = new System.Drawing.Size(885, 515);
            this._defendVoltageTabPage.TabIndex = 1;
            this._defendVoltageTabPage.Text = "������ �� ����������";
            this._defendVoltageTabPage.UseVisualStyleBackColor = true;
            // 
            // _blockDDL
            // 
            this._blockDDL.Controls.Add(this.groupBox26);
            this._blockDDL.Controls.Add(this.groupBox25);
            this._blockDDL.Location = new System.Drawing.Point(6, 6);
            this._blockDDL.Name = "_blockDDL";
            this._blockDDL.Size = new System.Drawing.Size(273, 67);
            this._blockDDL.TabIndex = 14;
            this._blockDDL.TabStop = false;
            this._blockDDL.Text = "���������� �� U< 5�";
            this._blockDDL.Visible = false;
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this._U2CB);
            this.groupBox26.Location = new System.Drawing.Point(140, 15);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(126, 46);
            this.groupBox26.TabIndex = 1;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "U<<";
            // 
            // _U2CB
            // 
            this._U2CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._U2CB.FormattingEnabled = true;
            this._U2CB.Items.AddRange(new object[] {
            "�������",
            "��������"});
            this._U2CB.Location = new System.Drawing.Point(6, 20);
            this._U2CB.Name = "_U2CB";
            this._U2CB.Size = new System.Drawing.Size(113, 21);
            this._U2CB.TabIndex = 1;
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this._U1CB);
            this.groupBox25.Location = new System.Drawing.Point(7, 15);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(126, 46);
            this.groupBox25.TabIndex = 0;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "U<";
            // 
            // _U1CB
            // 
            this._U1CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._U1CB.Items.AddRange(new object[] {
            "�������",
            "��������"});
            this._U1CB.Location = new System.Drawing.Point(7, 20);
            this._U1CB.Name = "_U1CB";
            this._U1CB.Size = new System.Drawing.Size(113, 21);
            this._U1CB.TabIndex = 0;
            // 
            // _voltageDefensesGrid2
            // 
            this._voltageDefensesGrid2.AllowUserToAddRows = false;
            this._voltageDefensesGrid2.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid2.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid2.AllowUserToResizeRows = false;
            this._voltageDefensesGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._voltageDefensesGrid2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._voltageDefensesGrid2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._voltageDefensesGrid2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle73.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle73.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle73.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle73.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle73.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle73.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle73.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle73;
            this._voltageDefensesGrid2.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol2,
            this._voltageDefensesModeCol2,
            this._voltageDefensesBlockingNumberCol2,
            this._voltageDefensesParameterCol2,
            this._voltageDefensesWorkConstraintCol2,
            this._voltageDefensesWorkTimeCol2,
            this._voltageDefensesReturnCol2,
            this._voltageDefensesAPVreturnCol2,
            this._voltageDefensesReturnConstraintCol2,
            this._voltageDefensesReturnTimeCol2,
            this._voltageDefensesOSCCol2,
            this._voltageDefensesOSC�11Col2,
            this._voltageDefensesUROVcol2,
            this._voltageDefensesAPVcol2,
            this._voltageDefensesAVRcol2,
            this._voltageDefensesResetCol2});
            dataGridViewCellStyle77.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle77.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle77.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle77.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle77.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle77.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle77.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesGrid2.DefaultCellStyle = dataGridViewCellStyle77;
            this._voltageDefensesGrid2.Location = new System.Drawing.Point(6, 241);
            this._voltageDefensesGrid2.MultiSelect = false;
            this._voltageDefensesGrid2.Name = "_voltageDefensesGrid2";
            dataGridViewCellStyle78.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle78.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle78.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle78.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle78.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle78.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle78.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid2.RowHeadersDefaultCellStyle = dataGridViewCellStyle78;
            this._voltageDefensesGrid2.RowHeadersVisible = false;
            this._voltageDefensesGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle79.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid2.RowsDefaultCellStyle = dataGridViewCellStyle79;
            this._voltageDefensesGrid2.RowTemplate.Height = 24;
            this._voltageDefensesGrid2.Size = new System.Drawing.Size(873, 129);
            this._voltageDefensesGrid2.TabIndex = 12;
            this._voltageDefensesGrid2.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // _voltageDefensesNameCol2
            // 
            dataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._voltageDefensesNameCol2.DefaultCellStyle = dataGridViewCellStyle74;
            this._voltageDefensesNameCol2.Frozen = true;
            this._voltageDefensesNameCol2.HeaderText = "";
            this._voltageDefensesNameCol2.Name = "_voltageDefensesNameCol2";
            this._voltageDefensesNameCol2.ReadOnly = true;
            this._voltageDefensesNameCol2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesNameCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol2.Width = 5;
            // 
            // _voltageDefensesModeCol2
            // 
            this._voltageDefensesModeCol2.HeaderText = "�����";
            this._voltageDefensesModeCol2.Name = "_voltageDefensesModeCol2";
            this._voltageDefensesModeCol2.Width = 48;
            // 
            // _voltageDefensesBlockingNumberCol2
            // 
            this._voltageDefensesBlockingNumberCol2.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol2.Name = "_voltageDefensesBlockingNumberCol2";
            this._voltageDefensesBlockingNumberCol2.Width = 74;
            // 
            // _voltageDefensesParameterCol2
            // 
            dataGridViewCellStyle75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesParameterCol2.DefaultCellStyle = dataGridViewCellStyle75;
            this._voltageDefensesParameterCol2.HeaderText = "��������";
            this._voltageDefensesParameterCol2.Name = "_voltageDefensesParameterCol2";
            this._voltageDefensesParameterCol2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesParameterCol2.Width = 64;
            // 
            // _voltageDefensesWorkConstraintCol2
            // 
            this._voltageDefensesWorkConstraintCol2.HeaderText = "������� ����., �";
            this._voltageDefensesWorkConstraintCol2.Name = "_voltageDefensesWorkConstraintCol2";
            this._voltageDefensesWorkConstraintCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkConstraintCol2.Width = 99;
            // 
            // _voltageDefensesWorkTimeCol2
            // 
            dataGridViewCellStyle76.NullValue = "0";
            this._voltageDefensesWorkTimeCol2.DefaultCellStyle = dataGridViewCellStyle76;
            this._voltageDefensesWorkTimeCol2.HeaderText = "���, ��";
            this._voltageDefensesWorkTimeCol2.Name = "_voltageDefensesWorkTimeCol2";
            this._voltageDefensesWorkTimeCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkTimeCol2.Width = 52;
            // 
            // _voltageDefensesReturnCol2
            // 
            this._voltageDefensesReturnCol2.HeaderText = "����.";
            this._voltageDefensesReturnCol2.Name = "_voltageDefensesReturnCol2";
            this._voltageDefensesReturnCol2.Width = 41;
            // 
            // _voltageDefensesAPVreturnCol2
            // 
            this._voltageDefensesAPVreturnCol2.HeaderText = "��� ��";
            this._voltageDefensesAPVreturnCol2.Name = "_voltageDefensesAPVreturnCol2";
            this._voltageDefensesAPVreturnCol2.Width = 52;
            // 
            // _voltageDefensesReturnConstraintCol2
            // 
            this._voltageDefensesReturnConstraintCol2.HeaderText = "������� ��������, �";
            this._voltageDefensesReturnConstraintCol2.Name = "_voltageDefensesReturnConstraintCol2";
            this._voltageDefensesReturnConstraintCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnConstraintCol2.Width = 119;
            // 
            // _voltageDefensesReturnTimeCol2
            // 
            this._voltageDefensesReturnTimeCol2.HeaderText = "������, ��";
            this._voltageDefensesReturnTimeCol2.Name = "_voltageDefensesReturnTimeCol2";
            this._voltageDefensesReturnTimeCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnTimeCol2.Width = 70;
            // 
            // _voltageDefensesOSCCol2
            // 
            this._voltageDefensesOSCCol2.HeaderText = "���.";
            this._voltageDefensesOSCCol2.Name = "_voltageDefensesOSCCol2";
            this._voltageDefensesOSCCol2.Visible = false;
            this._voltageDefensesOSCCol2.Width = 36;
            // 
            // _voltageDefensesOSC�11Col2
            // 
            this._voltageDefensesOSC�11Col2.HeaderText = "�����������";
            this._voltageDefensesOSC�11Col2.Name = "_voltageDefensesOSC�11Col2";
            this._voltageDefensesOSC�11Col2.Visible = false;
            this._voltageDefensesOSC�11Col2.Width = 82;
            // 
            // _voltageDefensesUROVcol2
            // 
            this._voltageDefensesUROVcol2.HeaderText = "����";
            this._voltageDefensesUROVcol2.Name = "_voltageDefensesUROVcol2";
            this._voltageDefensesUROVcol2.Width = 43;
            // 
            // _voltageDefensesAPVcol2
            // 
            this._voltageDefensesAPVcol2.HeaderText = "���";
            this._voltageDefensesAPVcol2.Name = "_voltageDefensesAPVcol2";
            this._voltageDefensesAPVcol2.Width = 35;
            // 
            // _voltageDefensesAVRcol2
            // 
            this._voltageDefensesAVRcol2.HeaderText = "���";
            this._voltageDefensesAVRcol2.Name = "_voltageDefensesAVRcol2";
            this._voltageDefensesAVRcol2.Width = 34;
            // 
            // _voltageDefensesResetCol2
            // 
            this._voltageDefensesResetCol2.HeaderText = "�����";
            this._voltageDefensesResetCol2.Name = "_voltageDefensesResetCol2";
            this._voltageDefensesResetCol2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesResetCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._voltageDefensesResetCol2.Visible = false;
            this._voltageDefensesResetCol2.Width = 63;
            // 
            // _voltageDefensesGrid1
            // 
            this._voltageDefensesGrid1.AllowUserToAddRows = false;
            this._voltageDefensesGrid1.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid1.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid1.AllowUserToResizeRows = false;
            this._voltageDefensesGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._voltageDefensesGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._voltageDefensesGrid1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle80.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle80.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle80.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle80.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle80.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle80.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle80.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle80;
            this._voltageDefensesGrid1.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol1,
            this._voltageDefensesModeCol1,
            this._voltageDefensesBlockingNumberCol1,
            this._voltageDefensesParameterCol1,
            this._voltageDefensesWorkConstraintCol1,
            this._voltageDefensesWorkTimeCol1,
            this._voltageDefensesReturnCol1,
            this._voltageDefensesAPVReturlCol1,
            this._voltageDefensesReturnConstraintCol1,
            this._voltageDefensesReturnTimeCol1,
            this._voltageDefensesOSCCol1,
            this._voltageDefensesOSCv11Col1,
            this._voltageDefensesUROVCol1,
            this._voltageDefensesAPVCol1,
            this._voltageDefensesAVRCol1,
            this._voltageDefensesResetCol1});
            dataGridViewCellStyle84.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle84.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle84.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle84.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle84.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle84.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle84.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesGrid1.DefaultCellStyle = dataGridViewCellStyle84;
            this._voltageDefensesGrid1.Location = new System.Drawing.Point(6, 79);
            this._voltageDefensesGrid1.MultiSelect = false;
            this._voltageDefensesGrid1.Name = "_voltageDefensesGrid1";
            dataGridViewCellStyle85.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle85.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle85.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle85.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle85.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle85.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle85.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle85;
            this._voltageDefensesGrid1.RowHeadersVisible = false;
            this._voltageDefensesGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle86.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid1.RowsDefaultCellStyle = dataGridViewCellStyle86;
            this._voltageDefensesGrid1.RowTemplate.Height = 24;
            this._voltageDefensesGrid1.Size = new System.Drawing.Size(873, 156);
            this._voltageDefensesGrid1.TabIndex = 11;
            this._voltageDefensesGrid1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // _voltageDefensesNameCol1
            // 
            dataGridViewCellStyle81.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesNameCol1.DefaultCellStyle = dataGridViewCellStyle81;
            this._voltageDefensesNameCol1.Frozen = true;
            this._voltageDefensesNameCol1.HeaderText = "";
            this._voltageDefensesNameCol1.Name = "_voltageDefensesNameCol1";
            this._voltageDefensesNameCol1.ReadOnly = true;
            this._voltageDefensesNameCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol1.Width = 5;
            // 
            // _voltageDefensesModeCol1
            // 
            this._voltageDefensesModeCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._voltageDefensesModeCol1.HeaderText = "�����";
            this._voltageDefensesModeCol1.Name = "_voltageDefensesModeCol1";
            // 
            // _voltageDefensesBlockingNumberCol1
            // 
            this._voltageDefensesBlockingNumberCol1.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol1.Name = "_voltageDefensesBlockingNumberCol1";
            this._voltageDefensesBlockingNumberCol1.Width = 74;
            // 
            // _voltageDefensesParameterCol1
            // 
            dataGridViewCellStyle82.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesParameterCol1.DefaultCellStyle = dataGridViewCellStyle82;
            this._voltageDefensesParameterCol1.HeaderText = "��������";
            this._voltageDefensesParameterCol1.Name = "_voltageDefensesParameterCol1";
            this._voltageDefensesParameterCol1.Width = 64;
            // 
            // _voltageDefensesWorkConstraintCol1
            // 
            this._voltageDefensesWorkConstraintCol1.HeaderText = "������� ����., �";
            this._voltageDefensesWorkConstraintCol1.Name = "_voltageDefensesWorkConstraintCol1";
            this._voltageDefensesWorkConstraintCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkConstraintCol1.Width = 99;
            // 
            // _voltageDefensesWorkTimeCol1
            // 
            dataGridViewCellStyle83.NullValue = "0";
            this._voltageDefensesWorkTimeCol1.DefaultCellStyle = dataGridViewCellStyle83;
            this._voltageDefensesWorkTimeCol1.HeaderText = "���, ��";
            this._voltageDefensesWorkTimeCol1.Name = "_voltageDefensesWorkTimeCol1";
            this._voltageDefensesWorkTimeCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkTimeCol1.Width = 52;
            // 
            // _voltageDefensesReturnCol1
            // 
            this._voltageDefensesReturnCol1.HeaderText = "����.";
            this._voltageDefensesReturnCol1.Name = "_voltageDefensesReturnCol1";
            this._voltageDefensesReturnCol1.Width = 41;
            // 
            // _voltageDefensesAPVReturlCol1
            // 
            this._voltageDefensesAPVReturlCol1.HeaderText = "��� ��";
            this._voltageDefensesAPVReturlCol1.Name = "_voltageDefensesAPVReturlCol1";
            this._voltageDefensesAPVReturlCol1.Width = 52;
            // 
            // _voltageDefensesReturnConstraintCol1
            // 
            this._voltageDefensesReturnConstraintCol1.HeaderText = "������� ��������, �";
            this._voltageDefensesReturnConstraintCol1.Name = "_voltageDefensesReturnConstraintCol1";
            this._voltageDefensesReturnConstraintCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnConstraintCol1.Width = 119;
            // 
            // _voltageDefensesReturnTimeCol1
            // 
            this._voltageDefensesReturnTimeCol1.HeaderText = "������, ��";
            this._voltageDefensesReturnTimeCol1.Name = "_voltageDefensesReturnTimeCol1";
            this._voltageDefensesReturnTimeCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnTimeCol1.Width = 70;
            // 
            // _voltageDefensesOSCCol1
            // 
            this._voltageDefensesOSCCol1.HeaderText = "���.";
            this._voltageDefensesOSCCol1.Name = "_voltageDefensesOSCCol1";
            this._voltageDefensesOSCCol1.Visible = false;
            this._voltageDefensesOSCCol1.Width = 36;
            // 
            // _voltageDefensesOSCv11Col1
            // 
            this._voltageDefensesOSCv11Col1.HeaderText = "�����������";
            this._voltageDefensesOSCv11Col1.Name = "_voltageDefensesOSCv11Col1";
            this._voltageDefensesOSCv11Col1.Visible = false;
            this._voltageDefensesOSCv11Col1.Width = 82;
            // 
            // _voltageDefensesUROVCol1
            // 
            this._voltageDefensesUROVCol1.HeaderText = "����";
            this._voltageDefensesUROVCol1.Name = "_voltageDefensesUROVCol1";
            this._voltageDefensesUROVCol1.Width = 43;
            // 
            // _voltageDefensesAPVCol1
            // 
            this._voltageDefensesAPVCol1.HeaderText = "���";
            this._voltageDefensesAPVCol1.Name = "_voltageDefensesAPVCol1";
            this._voltageDefensesAPVCol1.Width = 35;
            // 
            // _voltageDefensesAVRCol1
            // 
            this._voltageDefensesAVRCol1.HeaderText = "���";
            this._voltageDefensesAVRCol1.Name = "_voltageDefensesAVRCol1";
            this._voltageDefensesAVRCol1.Width = 34;
            // 
            // _voltageDefensesResetCol1
            // 
            this._voltageDefensesResetCol1.HeaderText = "�����";
            this._voltageDefensesResetCol1.Name = "_voltageDefensesResetCol1";
            this._voltageDefensesResetCol1.Visible = false;
            this._voltageDefensesResetCol1.Width = 44;
            // 
            // _voltageDefensesGrid3
            // 
            this._voltageDefensesGrid3.AllowUserToAddRows = false;
            this._voltageDefensesGrid3.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid3.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid3.AllowUserToResizeRows = false;
            this._voltageDefensesGrid3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._voltageDefensesGrid3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._voltageDefensesGrid3.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._voltageDefensesGrid3.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle87.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle87.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle87.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle87.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle87.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle87.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle87.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle87;
            this._voltageDefensesGrid3.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol3,
            this._voltageDefensesModeCol3,
            this._voltageDefensesBlockingNumberCol3,
            this._voltageDefensesParameterCol3,
            this._voltageDefensesWorkConstraintCol3,
            this._voltageDefensesTimeConstraintCol3,
            this._voltageDefensesReturnCol3,
            this._voltageDefensesAPVreturnCol3,
            this._voltageDefensesReturnConstraintCol3,
            this._voltageDefensesReturnTimeCol3,
            this._voltageDefensesOSCCol3,
            this._voltageDefensesOSCv11Col3,
            this._voltageDefensesUROVcol3,
            this._voltageDefensesAPVcol3,
            this._voltageDefensesAVRcol3,
            this._voltageDefensesResetcol3});
            dataGridViewCellStyle91.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle91.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle91.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle91.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle91.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle91.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle91.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesGrid3.DefaultCellStyle = dataGridViewCellStyle91;
            this._voltageDefensesGrid3.Location = new System.Drawing.Point(6, 376);
            this._voltageDefensesGrid3.MultiSelect = false;
            this._voltageDefensesGrid3.Name = "_voltageDefensesGrid3";
            dataGridViewCellStyle92.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle92.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle92.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle92.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle92.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle92.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle92.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid3.RowHeadersDefaultCellStyle = dataGridViewCellStyle92;
            this._voltageDefensesGrid3.RowHeadersVisible = false;
            this._voltageDefensesGrid3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle93.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid3.RowsDefaultCellStyle = dataGridViewCellStyle93;
            this._voltageDefensesGrid3.RowTemplate.Height = 24;
            this._voltageDefensesGrid3.Size = new System.Drawing.Size(873, 118);
            this._voltageDefensesGrid3.TabIndex = 13;
            this._voltageDefensesGrid3.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // _voltageDefensesNameCol3
            // 
            dataGridViewCellStyle88.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._voltageDefensesNameCol3.DefaultCellStyle = dataGridViewCellStyle88;
            this._voltageDefensesNameCol3.Frozen = true;
            this._voltageDefensesNameCol3.HeaderText = "";
            this._voltageDefensesNameCol3.Name = "_voltageDefensesNameCol3";
            this._voltageDefensesNameCol3.ReadOnly = true;
            this._voltageDefensesNameCol3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesNameCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol3.Width = 5;
            // 
            // _voltageDefensesModeCol3
            // 
            this._voltageDefensesModeCol3.HeaderText = "�����";
            this._voltageDefensesModeCol3.Name = "_voltageDefensesModeCol3";
            this._voltageDefensesModeCol3.Width = 48;
            // 
            // _voltageDefensesBlockingNumberCol3
            // 
            this._voltageDefensesBlockingNumberCol3.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol3.Name = "_voltageDefensesBlockingNumberCol3";
            this._voltageDefensesBlockingNumberCol3.Width = 74;
            // 
            // _voltageDefensesParameterCol3
            // 
            dataGridViewCellStyle89.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesParameterCol3.DefaultCellStyle = dataGridViewCellStyle89;
            this._voltageDefensesParameterCol3.HeaderText = "��������";
            this._voltageDefensesParameterCol3.Name = "_voltageDefensesParameterCol3";
            this._voltageDefensesParameterCol3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesParameterCol3.Width = 64;
            // 
            // _voltageDefensesWorkConstraintCol3
            // 
            this._voltageDefensesWorkConstraintCol3.HeaderText = "������� ����., �";
            this._voltageDefensesWorkConstraintCol3.Name = "_voltageDefensesWorkConstraintCol3";
            this._voltageDefensesWorkConstraintCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkConstraintCol3.Width = 99;
            // 
            // _voltageDefensesTimeConstraintCol3
            // 
            dataGridViewCellStyle90.NullValue = "0";
            this._voltageDefensesTimeConstraintCol3.DefaultCellStyle = dataGridViewCellStyle90;
            this._voltageDefensesTimeConstraintCol3.HeaderText = "���, ��";
            this._voltageDefensesTimeConstraintCol3.Name = "_voltageDefensesTimeConstraintCol3";
            this._voltageDefensesTimeConstraintCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesTimeConstraintCol3.Width = 52;
            // 
            // _voltageDefensesReturnCol3
            // 
            this._voltageDefensesReturnCol3.HeaderText = "����.";
            this._voltageDefensesReturnCol3.Name = "_voltageDefensesReturnCol3";
            this._voltageDefensesReturnCol3.Width = 41;
            // 
            // _voltageDefensesAPVreturnCol3
            // 
            this._voltageDefensesAPVreturnCol3.HeaderText = "��� ��";
            this._voltageDefensesAPVreturnCol3.Name = "_voltageDefensesAPVreturnCol3";
            this._voltageDefensesAPVreturnCol3.Width = 52;
            // 
            // _voltageDefensesReturnConstraintCol3
            // 
            this._voltageDefensesReturnConstraintCol3.HeaderText = "������� ��������, �";
            this._voltageDefensesReturnConstraintCol3.Name = "_voltageDefensesReturnConstraintCol3";
            this._voltageDefensesReturnConstraintCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnConstraintCol3.Width = 119;
            // 
            // _voltageDefensesReturnTimeCol3
            // 
            this._voltageDefensesReturnTimeCol3.HeaderText = "������, ��";
            this._voltageDefensesReturnTimeCol3.Name = "_voltageDefensesReturnTimeCol3";
            this._voltageDefensesReturnTimeCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnTimeCol3.Width = 70;
            // 
            // _voltageDefensesOSCCol3
            // 
            this._voltageDefensesOSCCol3.HeaderText = "���.";
            this._voltageDefensesOSCCol3.Name = "_voltageDefensesOSCCol3";
            this._voltageDefensesOSCCol3.Visible = false;
            this._voltageDefensesOSCCol3.Width = 36;
            // 
            // _voltageDefensesOSCv11Col3
            // 
            this._voltageDefensesOSCv11Col3.HeaderText = "�����������";
            this._voltageDefensesOSCv11Col3.Name = "_voltageDefensesOSCv11Col3";
            this._voltageDefensesOSCv11Col3.Visible = false;
            this._voltageDefensesOSCv11Col3.Width = 82;
            // 
            // _voltageDefensesUROVcol3
            // 
            this._voltageDefensesUROVcol3.HeaderText = "����";
            this._voltageDefensesUROVcol3.Name = "_voltageDefensesUROVcol3";
            this._voltageDefensesUROVcol3.Width = 43;
            // 
            // _voltageDefensesAPVcol3
            // 
            this._voltageDefensesAPVcol3.HeaderText = "���";
            this._voltageDefensesAPVcol3.Name = "_voltageDefensesAPVcol3";
            this._voltageDefensesAPVcol3.Width = 35;
            // 
            // _voltageDefensesAVRcol3
            // 
            this._voltageDefensesAVRcol3.HeaderText = "���";
            this._voltageDefensesAVRcol3.Name = "_voltageDefensesAVRcol3";
            this._voltageDefensesAVRcol3.Width = 34;
            // 
            // _voltageDefensesResetcol3
            // 
            this._voltageDefensesResetcol3.HeaderText = "�����";
            this._voltageDefensesResetcol3.Name = "_voltageDefensesResetcol3";
            this._voltageDefensesResetcol3.Visible = false;
            this._voltageDefensesResetcol3.Width = 44;
            // 
            // _frequencyDefendTabPage
            // 
            this._frequencyDefendTabPage.Controls.Add(this._frequenceDefensesGrid);
            this._frequencyDefendTabPage.Location = new System.Drawing.Point(4, 22);
            this._frequencyDefendTabPage.Name = "_frequencyDefendTabPage";
            this._frequencyDefendTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._frequencyDefendTabPage.Size = new System.Drawing.Size(885, 515);
            this._frequencyDefendTabPage.TabIndex = 2;
            this._frequencyDefendTabPage.Text = "������ �� �������";
            this._frequencyDefendTabPage.UseVisualStyleBackColor = true;
            // 
            // _frequenceDefensesGrid
            // 
            this._frequenceDefensesGrid.AllowUserToAddRows = false;
            this._frequenceDefensesGrid.AllowUserToDeleteRows = false;
            this._frequenceDefensesGrid.AllowUserToResizeColumns = false;
            this._frequenceDefensesGrid.AllowUserToResizeRows = false;
            this._frequenceDefensesGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._frequenceDefensesGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this._frequenceDefensesGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle94.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle94.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle94.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle94.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle94.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle94.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle94.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._frequenceDefensesGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle94;
            this._frequenceDefensesGrid.ColumnHeadersHeight = 30;
            this._frequenceDefensesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._frequenceDefensesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._frequenceDefensesName,
            this._frequenceDefensesMode,
            this._frequenceDefensesBlockingNumber,
            this._frequenceDefensesWorkConstraint,
            this._frequenceDefensesWorkTime,
            this._frequenceDefensesReturn,
            this._frequenceDefensesAPVReturn,
            this._frequenceDefensesReturnTime,
            this._frequenceDefensesConstraintAPV,
            this._frequenceDefensesOSC,
            this._frequenceDefensesOSCv11,
            this._frequenceDefensesUROV,
            this._frequenceDefensesAPV,
            this._frequenceDefensesAVR,
            this._frequenceDefensesReset});
            this._frequenceDefensesGrid.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle100.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle100.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle100.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle100.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle100.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle100.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle100.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesGrid.DefaultCellStyle = dataGridViewCellStyle100;
            this._frequenceDefensesGrid.Location = new System.Drawing.Point(6, 6);
            this._frequenceDefensesGrid.MultiSelect = false;
            this._frequenceDefensesGrid.Name = "_frequenceDefensesGrid";
            dataGridViewCellStyle101.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle101.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle101.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle101.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle101.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle101.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle101.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._frequenceDefensesGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle101;
            this._frequenceDefensesGrid.RowHeadersVisible = false;
            this._frequenceDefensesGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._frequenceDefensesGrid.RowTemplate.Height = 24;
            this._frequenceDefensesGrid.Size = new System.Drawing.Size(873, 208);
            this._frequenceDefensesGrid.TabIndex = 6;
            this._frequenceDefensesGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorGrid);
            // 
            // _frequenceDefensesName
            // 
            this._frequenceDefensesName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle95.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._frequenceDefensesName.DefaultCellStyle = dataGridViewCellStyle95;
            this._frequenceDefensesName.Frozen = true;
            this._frequenceDefensesName.HeaderText = "";
            this._frequenceDefensesName.MaxInputLength = 1;
            this._frequenceDefensesName.Name = "_frequenceDefensesName";
            this._frequenceDefensesName.ReadOnly = true;
            this._frequenceDefensesName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesName.Width = 45;
            // 
            // _frequenceDefensesMode
            // 
            this._frequenceDefensesMode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._frequenceDefensesMode.HeaderText = "�����";
            this._frequenceDefensesMode.Name = "_frequenceDefensesMode";
            this._frequenceDefensesMode.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _frequenceDefensesBlockingNumber
            // 
            this._frequenceDefensesBlockingNumber.HeaderText = "����������";
            this._frequenceDefensesBlockingNumber.Name = "_frequenceDefensesBlockingNumber";
            this._frequenceDefensesBlockingNumber.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesBlockingNumber.Width = 74;
            // 
            // _frequenceDefensesWorkConstraint
            // 
            dataGridViewCellStyle96.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesWorkConstraint.DefaultCellStyle = dataGridViewCellStyle96;
            this._frequenceDefensesWorkConstraint.HeaderText = "������� ����., ��";
            this._frequenceDefensesWorkConstraint.MaxInputLength = 8;
            this._frequenceDefensesWorkConstraint.Name = "_frequenceDefensesWorkConstraint";
            this._frequenceDefensesWorkConstraint.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesWorkConstraint.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesWorkConstraint.Width = 104;
            // 
            // _frequenceDefensesWorkTime
            // 
            dataGridViewCellStyle97.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle97.NullValue = "0";
            this._frequenceDefensesWorkTime.DefaultCellStyle = dataGridViewCellStyle97;
            this._frequenceDefensesWorkTime.HeaderText = "���, ��";
            this._frequenceDefensesWorkTime.MaxInputLength = 8;
            this._frequenceDefensesWorkTime.Name = "_frequenceDefensesWorkTime";
            this._frequenceDefensesWorkTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesWorkTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesWorkTime.Width = 52;
            // 
            // _frequenceDefensesReturn
            // 
            this._frequenceDefensesReturn.HeaderText = "����.";
            this._frequenceDefensesReturn.Name = "_frequenceDefensesReturn";
            this._frequenceDefensesReturn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesReturn.Width = 41;
            // 
            // _frequenceDefensesAPVReturn
            // 
            this._frequenceDefensesAPVReturn.HeaderText = "��� ��";
            this._frequenceDefensesAPVReturn.Name = "_frequenceDefensesAPVReturn";
            this._frequenceDefensesAPVReturn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesAPVReturn.Width = 52;
            // 
            // _frequenceDefensesReturnTime
            // 
            dataGridViewCellStyle98.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesReturnTime.DefaultCellStyle = dataGridViewCellStyle98;
            this._frequenceDefensesReturnTime.HeaderText = "������, ��";
            this._frequenceDefensesReturnTime.Name = "_frequenceDefensesReturnTime";
            this._frequenceDefensesReturnTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesReturnTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesReturnTime.Width = 70;
            // 
            // _frequenceDefensesConstraintAPV
            // 
            dataGridViewCellStyle99.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesConstraintAPV.DefaultCellStyle = dataGridViewCellStyle99;
            this._frequenceDefensesConstraintAPV.HeaderText = "������� ��, ��";
            this._frequenceDefensesConstraintAPV.Name = "_frequenceDefensesConstraintAPV";
            this._frequenceDefensesConstraintAPV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesConstraintAPV.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesConstraintAPV.Width = 91;
            // 
            // _frequenceDefensesOSC
            // 
            this._frequenceDefensesOSC.HeaderText = "���.";
            this._frequenceDefensesOSC.Name = "_frequenceDefensesOSC";
            this._frequenceDefensesOSC.Visible = false;
            this._frequenceDefensesOSC.Width = 36;
            // 
            // _frequenceDefensesOSCv11
            // 
            this._frequenceDefensesOSCv11.HeaderText = "�����������";
            this._frequenceDefensesOSCv11.Name = "_frequenceDefensesOSCv11";
            this._frequenceDefensesOSCv11.Visible = false;
            this._frequenceDefensesOSCv11.Width = 82;
            // 
            // _frequenceDefensesUROV
            // 
            this._frequenceDefensesUROV.HeaderText = "����";
            this._frequenceDefensesUROV.Name = "_frequenceDefensesUROV";
            this._frequenceDefensesUROV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesUROV.Width = 43;
            // 
            // _frequenceDefensesAPV
            // 
            this._frequenceDefensesAPV.HeaderText = "���";
            this._frequenceDefensesAPV.Name = "_frequenceDefensesAPV";
            this._frequenceDefensesAPV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesAPV.Width = 35;
            // 
            // _frequenceDefensesAVR
            // 
            this._frequenceDefensesAVR.HeaderText = "���";
            this._frequenceDefensesAVR.Name = "_frequenceDefensesAVR";
            this._frequenceDefensesAVR.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesAVR.Width = 34;
            // 
            // _frequenceDefensesReset
            // 
            this._frequenceDefensesReset.HeaderText = "�����";
            this._frequenceDefensesReset.Name = "_frequenceDefensesReset";
            this._frequenceDefensesReset.Visible = false;
            this._frequenceDefensesReset.Width = 44;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this._groupChangeButton);
            this.groupBox24.Controls.Add(this._reserveRadioBtnGrop);
            this.groupBox24.Controls.Add(this._mainRadioBtnGroup);
            this.groupBox24.Location = new System.Drawing.Point(9, 7);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(464, 56);
            this.groupBox24.TabIndex = 0;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "������ �������";
            // 
            // _groupChangeButton
            // 
            this._groupChangeButton.Location = new System.Drawing.Point(252, 20);
            this._groupChangeButton.Name = "_groupChangeButton";
            this._groupChangeButton.Size = new System.Drawing.Size(180, 23);
            this._groupChangeButton.TabIndex = 2;
            this._groupChangeButton.Text = "�������� --> ���������";
            this._groupChangeButton.UseVisualStyleBackColor = true;
            // 
            // _reserveRadioBtnGrop
            // 
            this._reserveRadioBtnGrop.AutoSize = true;
            this._reserveRadioBtnGrop.Location = new System.Drawing.Point(129, 23);
            this._reserveRadioBtnGrop.Name = "_reserveRadioBtnGrop";
            this._reserveRadioBtnGrop.Size = new System.Drawing.Size(80, 17);
            this._reserveRadioBtnGrop.TabIndex = 1;
            this._reserveRadioBtnGrop.Text = "���������";
            this._reserveRadioBtnGrop.UseVisualStyleBackColor = true;
            // 
            // _mainRadioBtnGroup
            // 
            this._mainRadioBtnGroup.AutoSize = true;
            this._mainRadioBtnGroup.Checked = true;
            this._mainRadioBtnGroup.Location = new System.Drawing.Point(22, 23);
            this._mainRadioBtnGroup.Name = "_mainRadioBtnGroup";
            this._mainRadioBtnGroup.Size = new System.Drawing.Size(75, 17);
            this._mainRadioBtnGroup.TabIndex = 0;
            this._mainRadioBtnGroup.TabStop = true;
            this._mainRadioBtnGroup.Text = "��������";
            this._mainRadioBtnGroup.UseVisualStyleBackColor = true;
            this._mainRadioBtnGroup.CheckedChanged += new System.EventHandler(this._mainRadioBtnGroup_CheckedChanged);
            // 
            // _saveToXmlButton
            // 
            this._saveToXmlButton.AllowDrop = true;
            this._saveToXmlButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveToXmlButton.AutoSize = true;
            this._saveToXmlButton.Location = new System.Drawing.Point(797, 663);
            this._saveToXmlButton.Name = "_saveToXmlButton";
            this._saveToXmlButton.Size = new System.Drawing.Size(125, 23);
            this._saveToXmlButton.TabIndex = 34;
            this._saveToXmlButton.Text = "��������� � HTML";
            this._saveToXmlButton.UseVisualStyleBackColor = true;
            this._saveToXmlButton.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _saveXmlDialog
            // 
            this._saveXmlDialog.Filter = "(*.html) | *.html";
            // 
            // _resetSetpointsButton
            // 
            this._resetSetpointsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._resetSetpointsButton.AutoSize = true;
            this._resetSetpointsButton.Location = new System.Drawing.Point(314, 663);
            this._resetSetpointsButton.Name = "_resetSetpointsButton";
            this._resetSetpointsButton.Size = new System.Drawing.Size(149, 23);
            this._resetSetpointsButton.TabIndex = 39;
            this._resetSetpointsButton.Text = "�������� �������";
            this._resetSetpointsButton.UseVisualStyleBackColor = true;
            this._resetSetpointsButton.Click += new System.EventHandler(this._resetSetpointsButton_Click);
            // 
            // _diskretChannelCol
            // 
            this._diskretChannelCol.HeaderText = "�";
            this._diskretChannelCol.Name = "_diskretChannelCol";
            this._diskretChannelCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._diskretChannelCol.Width = 24;
            // 
            // _diskretValueCol
            // 
            this._diskretValueCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._diskretValueCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._diskretValueCol.HeaderText = "��������";
            this._diskretValueCol.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this._diskretValueCol.Name = "_diskretValueCol";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "�";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 24;
            // 
            // _keyValue
            // 
            this._keyValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._keyValue.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._keyValue.HeaderText = "��������";
            this._keyValue.Items.AddRange(new object[] {
            "���",
            "��"});
            this._keyValue.Name = "_keyValue";
            this._keyValue.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 711);
            this.Controls.Add(this._resetSetpointsButton);
            this.Controls.Add(this._saveToXmlButton);
            this.Controls.Add(this._saveConfigBut);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this._writeConfigBut);
            this.Controls.Add(this._statusStrip);
            this.Controls.Add(this._readConfigBut);
            this.Controls.Add(this._tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(940, 726);
            this.Name = "ConfigurationForm";
            this.Text = "ConfigurationForm";
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TzlConfigurationForm_KeyUp);
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this._tabControl.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this._inSignalsPage.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid3)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid4)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid5)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid6)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid7)).EndInit();
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid8)).EndInit();
            this.oscGroupBox.ResumeLayout(false);
            this.oscGroupBox.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._keysDataGrid)).EndInit();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._outputSignalsPage.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.VLSTabControl.ResumeLayout(false);
            this.VLS1.ResumeLayout(false);
            this.VLS2.ResumeLayout(false);
            this.VLS3.ResumeLayout(false);
            this.VLS4.ResumeLayout(false);
            this.VLS5.ResumeLayout(false);
            this.VLS6.ResumeLayout(false);
            this.VLS7.ResumeLayout(false);
            this.VLS8.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).EndInit();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this._externalDefensePage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).EndInit();
            this._automaticPage.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this._defendTabPage.ResumeLayout(false);
            this.groupBox27.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this._tokDefendTabPage.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid1)).EndInit();
            this._defendVoltageTabPage.ResumeLayout(false);
            this._blockDDL.ResumeLayout(false);
            this.groupBox26.ResumeLayout(false);
            this.groupBox25.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid3)).EndInit();
            this._frequencyDefendTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._frequenceDefensesGrid)).EndInit();
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox apv_blocking;
        private System.Windows.Forms.ComboBox apv_conf;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.ToolStripStatusLabel _processLabel;
        private System.Windows.Forms.ToolStripProgressBar _exchangeProgressBar;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.StatusStrip _statusStrip;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.MaskedTextBox apv_time_4krat;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.MaskedTextBox apv_time_3krat;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.MaskedTextBox apv_time_2krat;
        private System.Windows.Forms.TabControl _tabControl;
        private System.Windows.Forms.TabPage _outputSignalsPage;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView _outputIndicatorsGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignalCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndResetCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndAlarmCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndSystemCol;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView _outputReleGrid;
        private System.Windows.Forms.TabPage _externalDefensePage;
        private System.Windows.Forms.DataGridView _externalDefenseGrid;
        private System.Windows.Forms.TabPage _automaticPage;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.MaskedTextBox avr_disconnection;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.MaskedTextBox avr_time_return;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.MaskedTextBox avr_time_abrasion;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.ComboBox avr_return;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.ComboBox avr_abrasion;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.ComboBox avr_reset_blocking;
        private System.Windows.Forms.CheckBox avr_permit_reset_switch;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.CheckBox avr_abrasion_switch;
        private System.Windows.Forms.CheckBox avr_supply_off;
        private System.Windows.Forms.CheckBox avr_self_off;
        private System.Windows.Forms.CheckBox avr_switch_off;
        private System.Windows.Forms.ComboBox avr_blocking;
        private System.Windows.Forms.ComboBox avr_start;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.MaskedTextBox lzsh_constraint;
        private System.Windows.Forms.ComboBox lzsh_conf;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.MaskedTextBox apv_time_1krat;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.MaskedTextBox apv_time_ready;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.MaskedTextBox apv_time_blocking;
        private System.Windows.Forms.ComboBox apv_self_off;
        private System.Windows.Forms.TabPage _inSignalsPage;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.DataGridView _keysDataGrid;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.MaskedTextBox _HUD_Box;
        private System.Windows.Forms.ComboBox _OMP_TypeBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox _manageSignalsSDTU_Combo;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox _manageSignalsExternalCombo;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox _manageSignalsKeyCombo;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.ComboBox _manageSignalsButtonCombo;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.DataGridView _logicSignalsDataGrid1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.MaskedTextBox _switcherDurationBox;
        private System.Windows.Forms.MaskedTextBox _switcherTokBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.MaskedTextBox _switcherTimeBox;
        private System.Windows.Forms.MaskedTextBox _switcherImpulseBox;
        private System.Windows.Forms.ComboBox _switcherBlockCombo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox _switcherErrorCombo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox _switcherStateOnCombo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox _switcherStateOffCombo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox _constraintGroupCombo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox _signalizationCombo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox _extOffCombo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox _extOnCombo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox _keyOffCombo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox _keyOnCombo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox _TN_typeCombo;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.ComboBox _TNNP_dispepairCombo;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox _TN_dispepairCombo;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox _TNNP_Box;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox _TN_Box;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox _maxTok_Box;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox _TTNP_Box;
        private System.Windows.Forms.MaskedTextBox _TT_Box;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.ComboBox _oscKonfComb;
        private System.Windows.Forms.ComboBox _Sost_ON_OFF;
        private System.Windows.Forms.Label _Sost_ON_OFF_Label;
        private System.Windows.Forms.ComboBox _TT_typeCombo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox lzsh_confV114;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extDefNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefBlockingCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefWorkingCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefWorkingTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefReturnCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAPVreturnCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefReturnNumberCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefReturnTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefOSCcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefUROVcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAPVcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAVRcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefResetcol;
        private System.Windows.Forms.ComboBox oscLength;
        private System.Windows.Forms.MaskedTextBox oscPercent;
        private System.Windows.Forms.ComboBox oscFix;
        private System.Windows.Forms.Button _saveToXmlButton;
        private System.Windows.Forms.SaveFileDialog _saveXmlDialog;
        private System.Windows.Forms.TabPage _defendTabPage;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage _tokDefendTabPage;
        private System.Windows.Forms.TabPage _defendVoltageTabPage;
        private System.Windows.Forms.TabPage _frequencyDefendTabPage;
        private System.Windows.Forms.Button _groupChangeButton;
        private System.Windows.Forms.RadioButton _reserveRadioBtnGrop;
        private System.Windows.Forms.RadioButton _mainRadioBtnGroup;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.MaskedTextBox _tokDefenseInbox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.MaskedTextBox _tokDefenseI2box;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.MaskedTextBox _tokDefenseI0box;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.MaskedTextBox _tokDefenseIbox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataGridView _tokDefenseGrid4;
        private System.Windows.Forms.DataGridView _tokDefenseGrid3;
        private System.Windows.Forms.DataGridView _tokDefenseGrid1;
        private System.Windows.Forms.GroupBox _blockDDL;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.ComboBox _U2CB;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.ComboBox _U1CB;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid2;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid1;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid3;
        private System.Windows.Forms.DataGridView _frequenceDefensesGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3NameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3ModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3BlockingNumberCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3UpuskCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3PuskConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3DirectionCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3BlockingExistCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3ParameterCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3WorkConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3FeatureCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3WorkTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3SpeedupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3SpeedupTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3OSCCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3OSCv11Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3UROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3APVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3AVRCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseNameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseBlockNumberCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseU_PuskCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefensePuskConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseDirectionCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseBlockExistCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseParameterCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseWorkConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseFeatureCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseWorkTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseSpeedUpCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseSpeedupTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseOSCCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseOSCv11Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseUROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseAPVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseAVRCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4NameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense4ModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense4BlockNumberCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4UpuskCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4PuskConstraintCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4WorkConstraintCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4WorkTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4SpeedupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4SpeedupTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4OSCCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense4OSCv11Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4UROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4APVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4AVRCol;
        private System.Windows.Forms.ComboBox _exBlockSdtuCombo;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToHtmlItem;
        private System.Windows.Forms.Button _resetSetpointsButton;
        private System.Windows.Forms.ToolStripMenuItem resetSetPointStruct;
        private System.Windows.Forms.GroupBox oscGroupBox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox _releDispepairBox;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.CheckedListBox _dispepairCheckList;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.DataGridView _logicSignalsDataGrid2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridView _logicSignalsDataGrid3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridView _logicSignalsDataGrid4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridView _logicSignalsDataGrid5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridView _logicSignalsDataGrid6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridView _logicSignalsDataGrid7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.DataGridView _logicSignalsDataGrid8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn7;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TabControl VLSTabControl;
        private System.Windows.Forms.TabPage VLS1;
        private System.Windows.Forms.CheckedListBox _outputLogicCheckList1;
        private System.Windows.Forms.TabPage VLS2;
        private System.Windows.Forms.CheckedListBox _outputLogicCheckList2;
        private System.Windows.Forms.TabPage VLS3;
        private System.Windows.Forms.CheckedListBox _outputLogicCheckList3;
        private System.Windows.Forms.TabPage VLS4;
        private System.Windows.Forms.CheckedListBox _outputLogicCheckList4;
        private System.Windows.Forms.TabPage VLS5;
        private System.Windows.Forms.CheckedListBox _outputLogicCheckList5;
        private System.Windows.Forms.TabPage VLS6;
        private System.Windows.Forms.CheckedListBox _outputLogicCheckList6;
        private System.Windows.Forms.TabPage VLS7;
        private System.Windows.Forms.CheckedListBox _outputLogicCheckList7;
        private System.Windows.Forms.TabPage VLS8;
        private System.Windows.Forms.CheckedListBox _outputLogicCheckList8;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releImpulseCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesParameterCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkConstraintCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkTimeCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesReturnCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVreturnCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnConstraintCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnTimeCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesOSCCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesOSC�11Col2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesUROVcol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVcol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAVRcol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesResetCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesParameterCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkConstraintCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkTimeCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesReturnCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVReturlCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnConstraintCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnTimeCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesOSCCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesOSCv11Col1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesUROVCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAVRCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesResetCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesName;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesMode;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesBlockingNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesWorkConstraint;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesWorkTime;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesReturn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesAPVReturn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesReturnTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesConstraintAPV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesOSC;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesOSCv11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesUROV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesAPV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesAVR;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesReset;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesParameterCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkConstraintCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesTimeConstraintCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesReturnCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVreturnCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnConstraintCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnTimeCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesOSCCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesOSCv11Col3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesUROVcol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVcol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAVRcol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesResetcol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _diskretChannelCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _diskretValueCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _keyValue;
    }
}